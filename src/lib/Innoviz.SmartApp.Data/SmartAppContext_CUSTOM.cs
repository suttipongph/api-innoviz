using Innoviz.SmartApp.Data.Models;
using Microsoft.EntityFrameworkCore;
namespace Innoviz.SmartApp.Data
{
	public partial class SmartAppContext
	{
		#region Dbset
		public virtual DbSet<ActionHistory> ActionHistory { get; set; }
		public virtual DbSet<AddressCountry> AddressCountry { get; set; }
		public virtual DbSet<AddressDistrict> AddressDistrict { get; set; }
		public virtual DbSet<AddressPostalCode> AddressPostalCode { get; set; }
		public virtual DbSet<AddressProvince> AddressProvince { get; set; }
		public virtual DbSet<AddressSubDistrict> AddressSubDistrict { get; set; }
		public virtual DbSet<AddressTrans> AddressTrans { get; set; }
		public virtual DbSet<AgingReportSetup> AgingReportSetup { get; set; }
		public virtual DbSet<AgreementTableInfo> AgreementTableInfo { get; set; }
		public virtual DbSet<AgreementTableInfoText> AgreementTableInfoText { get; set; }
		public virtual DbSet<AgreementType> AgreementType { get; set; }
		public virtual DbSet<ApplicationTable> ApplicationTable { get; set; }
		public virtual DbSet<AssignmentAgreementLine> AssignmentAgreementLine { get; set; }
		public virtual DbSet<AssignmentAgreementSettle> AssignmentAgreementSettle { get; set; }
		public virtual DbSet<AssignmentAgreementTable> AssignmentAgreementTable { get; set; }
		public virtual DbSet<AssignmentMethod> AssignmentMethod { get; set; }
		public virtual DbSet<Attachment> Attachment { get; set; }
		public virtual DbSet<AuthorizedPersonTrans> AuthorizedPersonTrans { get; set; }
		public virtual DbSet<AuthorizedPersonType> AuthorizedPersonType { get; set; }
		public virtual DbSet<BankGroup> BankGroup { get; set; }
		public virtual DbSet<BankType> BankType { get; set; }
		public virtual DbSet<BillingResponsibleBy> BillingResponsibleBy { get; set; }
		public virtual DbSet<BlacklistStatus> BlacklistStatus { get; set; }
		public virtual DbSet<BookmarkDocument> BookmarkDocument { get; set; }
		public virtual DbSet<BookmarkDocumentTemplateLine> BookmarkDocumentTemplateLine { get; set; }
		public virtual DbSet<BookmarkDocumentTemplateTable> BookmarkDocumentTemplateTable { get; set; }
		public virtual DbSet<BookmarkDocumentTrans> BookmarkDocumentTrans { get; set; }
		public virtual DbSet<BusinessCollateralAgmLine> BusinessCollateralAgmLine { get; set; }
		public virtual DbSet<BusinessCollateralAgmTable> BusinessCollateralAgmTable { get; set; }
		public virtual DbSet<BusinessCollateralStatus> BusinessCollateralStatus { get; set; }
		public virtual DbSet<BusinessCollateralSubType> BusinessCollateralSubType { get; set; }
		public virtual DbSet<BusinessCollateralType> BusinessCollateralType { get; set; }
		public virtual DbSet<BusinessSegment> BusinessSegment { get; set; }
		public virtual DbSet<BusinessSize> BusinessSize { get; set; }
		public virtual DbSet<BusinessType> BusinessType { get; set; }
		public virtual DbSet<BusinessUnit> BusinessUnit { get; set; }
		public virtual DbSet<BuyerAgreementLine> BuyerAgreementLine { get; set; }
		public virtual DbSet<BuyerAgreementTable> BuyerAgreementTable { get; set; }
		public virtual DbSet<BuyerAgreementTrans> BuyerAgreementTrans { get; set; }
		public virtual DbSet<BuyerCreditLimitByProduct> BuyerCreditLimitByProduct { get; set; }
		public virtual DbSet<BuyerInvoiceTable> BuyerInvoiceTable { get; set; }
		public virtual DbSet<BuyerReceiptTable> BuyerReceiptTable { get; set; }
		public virtual DbSet<BuyerTable> BuyerTable { get; set; }
		public virtual DbSet<CalendarGroup> CalendarGroup { get; set; }
		public virtual DbSet<CalendarNonWorkingDate> CalendarNonWorkingDate { get; set; }
		public virtual DbSet<CAReqAssignmentOutstanding> CAReqAssignmentOutstanding { get; set; }
		public virtual DbSet<CAReqCreditOutStanding> CAReqCreditOutStanding { get; set; }
		public virtual DbSet<CAReqRetentionOutstanding> CAReqRetentionOutstanding { get; set; }
		public virtual DbSet<ChequeTable> ChequeTable { get; set; }
		public virtual DbSet<CollectionFollowUp> CollectionFollowUp { get; set; }
		public virtual DbSet<CollectionGroup> CollectionGroup { get; set; }
		public virtual DbSet<CompanyBank> CompanyBank { get; set; }
		public virtual DbSet<CompanyParameter> CompanyParameter { get; set; }
		public virtual DbSet<CompanySignature> CompanySignature { get; set; }
		public virtual DbSet<ConsortiumLine> ConsortiumLine { get; set; }
		public virtual DbSet<ConsortiumTable> ConsortiumTable { get; set; }
		public virtual DbSet<ConsortiumTrans> ConsortiumTrans { get; set; }
		public virtual DbSet<ContactPersonTrans> ContactPersonTrans { get; set; }
		public virtual DbSet<ContactTrans> ContactTrans { get; set; }
		public virtual DbSet<CreditAppLine> CreditAppLine { get; set; }
		public virtual DbSet<CreditAppReqAssignment> CreditAppReqAssignment { get; set; }
		public virtual DbSet<CreditAppReqBusinessCollateral> CreditAppReqBusinessCollateral { get; set; }
		public virtual DbSet<CreditAppRequestLine> CreditAppRequestLine { get; set; }
		public virtual DbSet<CreditAppRequestLineAmend> CreditAppRequestLineAmend { get; set; }
		public virtual DbSet<CreditAppRequestTable> CreditAppRequestTable { get; set; }
		public virtual DbSet<CreditAppRequestTableAmend> CreditAppRequestTableAmend { get; set; }
		public virtual DbSet<CreditAppTable> CreditAppTable { get; set; }
		public virtual DbSet<CreditAppTrans> CreditAppTrans { get; set; }
		public virtual DbSet<CreditLimitType> CreditLimitType { get; set; }
		public virtual DbSet<CreditScoring> CreditScoring { get; set; }
		public virtual DbSet<CreditTerm> CreditTerm { get; set; }
		public virtual DbSet<CreditType> CreditType { get; set; }
		public virtual DbSet<Currency> Currency { get; set; }
		public virtual DbSet<CustBank> CustBank { get; set; }
		public virtual DbSet<CustBusinessCollateral> CustBusinessCollateral { get; set; }
		public virtual DbSet<CustGroup> CustGroup { get; set; }
		public virtual DbSet<CustomerCreditLimitByProduct> CustomerCreditLimitByProduct { get; set; }
		public virtual DbSet<CustomerRefundTable> CustomerRefundTable { get; set; }
		public virtual DbSet<CustomerTable> CustomerTable { get; set; }
		public virtual DbSet<CustTrans> CustTrans { get; set; }
		public virtual DbSet<CustVisitingTrans> CustVisitingTrans { get; set; }
		public virtual DbSet<Department> Department { get; set; }
		public virtual DbSet<DocumentConditionTemplateLine> DocumentConditionTemplateLine { get; set; }
		public virtual DbSet<DocumentConditionTemplateTable> DocumentConditionTemplateTable { get; set; }
		public virtual DbSet<DocumentConditionTrans> DocumentConditionTrans { get; set; }
		public virtual DbSet<DocumentProcess> DocumentProcess { get; set; }
		public virtual DbSet<DocumentReason> DocumentReason { get; set; }
		public virtual DbSet<DocumentStatus> DocumentStatus { get; set; }
		public virtual DbSet<DocumentTemplateTable> DocumentTemplateTable { get; set; }
		public virtual DbSet<DocumentType> DocumentType { get; set; }
		public virtual DbSet<EmployeeTable> EmployeeTable { get; set; }
		public virtual DbSet<EmplTeam> EmplTeam { get; set; }
		public virtual DbSet<ExchangeRate> ExchangeRate { get; set; }
		public virtual DbSet<ExposureGroup> ExposureGroup { get; set; }
		public virtual DbSet<ExposureGroupByProduct> ExposureGroupByProduct { get; set; }
		public virtual DbSet<FinancialCreditTrans> FinancialCreditTrans { get; set; }
		public virtual DbSet<FinancialStatementTrans> FinancialStatementTrans { get; set; }
		public virtual DbSet<Gender> Gender { get; set; }
		public virtual DbSet<GradeClassification> GradeClassification { get; set; }
		public virtual DbSet<GuarantorAgreementLine> GuarantorAgreementLine { get; set; }
		public virtual DbSet<GuarantorAgreementLineAffiliate> GuarantorAgreementLineAffiliate { get; set; }
		public virtual DbSet<GuarantorAgreementTable> GuarantorAgreementTable { get; set; }
		public virtual DbSet<GuarantorTrans> GuarantorTrans { get; set; }
		public virtual DbSet<GuarantorType> GuarantorType { get; set; }
		public virtual DbSet<Intercompany> Intercompany { get; set; }
		public virtual DbSet<InterestRealizedTrans> InterestRealizedTrans { get; set; }
		public virtual DbSet<InterestType> InterestType { get; set; }
		public virtual DbSet<InterestTypeValue> InterestTypeValue { get; set; }
		public virtual DbSet<IntroducedBy> IntroducedBy { get; set; }
		public virtual DbSet<InvoiceLine> InvoiceLine { get; set; }
		public virtual DbSet<InvoiceNumberSeqSetup> InvoiceNumberSeqSetup { get; set; }
		public virtual DbSet<InvoiceRevenueType> InvoiceRevenueType { get; set; }
		public virtual DbSet<InvoiceSettlementDetail> InvoiceSettlementDetail { get; set; }
		public virtual DbSet<InvoiceTable> InvoiceTable { get; set; }
		public virtual DbSet<InvoiceType> InvoiceType { get; set; }
		public virtual DbSet<JobCheque> JobCheque { get; set; }
		public virtual DbSet<JobType> JobType { get; set; }
		public virtual DbSet<JointVentureTrans> JointVentureTrans { get; set; }
		public virtual DbSet<KYCSetup> KYCSetup { get; set; }
		public virtual DbSet<Language> Language { get; set; }
		public virtual DbSet<LeaseType> LeaseType { get; set; }
		public virtual DbSet<LedgerDimension> LedgerDimension { get; set; }
		public virtual DbSet<LedgerFiscalPeriod> LedgerFiscalPeriod { get; set; }
		public virtual DbSet<LedgerFiscalYear> LedgerFiscalYear { get; set; }
		public virtual DbSet<LineOfBusiness> LineOfBusiness { get; set; }
		public virtual DbSet<MainAgreementTable> MainAgreementTable { get; set; }
		public virtual DbSet<MaritalStatus> MaritalStatus { get; set; }
		public virtual DbSet<MemoTrans> MemoTrans { get; set; }
		public virtual DbSet<MessengerJobTable> MessengerJobTable { get; set; }
		public virtual DbSet<MessengerTable> MessengerTable { get; set; }
		public virtual DbSet<MethodOfPayment> MethodOfPayment { get; set; }
		public virtual DbSet<MigrationTable> MigrationTable { get; set; }
		public virtual DbSet<MigrationLogTable> MigrationLogTable { get; set; }
		public virtual DbSet<Nationality> Nationality { get; set; }
		public virtual DbSet<NCBAccountStatus> NCBAccountStatus { get; set; }
		public virtual DbSet<NCBTrans> NCBTrans { get; set; }
		public virtual DbSet<NumberSeqParameter> NumberSeqParameter { get; set; }
		public virtual DbSet<NumberSeqSegment> NumberSeqSegment { get; set; }
		public virtual DbSet<NumberSeqSetupByProductType> NumberSeqSetupByProductType { get; set; }
		public virtual DbSet<NumberSeqTable> NumberSeqTable { get; set; }
		public virtual DbSet<Occupation> Occupation { get; set; }
		public virtual DbSet<Ownership> Ownership { get; set; }
		public virtual DbSet<OwnerTrans> OwnerTrans { get; set; }
		public virtual DbSet<ParentCompany> ParentCompany { get; set; }
		public virtual DbSet<PaymentDetail> PaymentDetail { get; set; }
		public virtual DbSet<PaymentHistory> PaymentHistory { get; set; }
		public virtual DbSet<PaymentFrequency> PaymentFrequency { get; set; }
		public virtual DbSet<ProcessTrans> ProcessTrans { get; set; }
		public virtual DbSet<ProductSettledTrans> ProductSettledTrans { get; set; }
		public virtual DbSet<ProductSubType> ProductSubType { get; set; }
		public virtual DbSet<ProdUnit> ProdUnit { get; set; }
		public virtual DbSet<ProjectProgressTable> ProjectProgressTable { get; set; }
		public virtual DbSet<ProjectReferenceTrans> ProjectReferenceTrans { get; set; }
		public virtual DbSet<PropertyType> PropertyType { get; set; }
		public virtual DbSet<PurchaseLine> PurchaseLine { get; set; }
		public virtual DbSet<PurchaseTable> PurchaseTable { get; set; }
		public virtual DbSet<Race> Race { get; set; }
		public virtual DbSet<ReceiptLine> ReceiptLine { get; set; }
		public virtual DbSet<ReceiptTable> ReceiptTable { get; set; }
		public virtual DbSet<ReceiptTempPaymDetail> ReceiptTempPaymDetail { get; set; }
		public virtual DbSet<ReceiptTempTable> ReceiptTempTable { get; set; }
		public virtual DbSet<RegistrationType> RegistrationType { get; set; }
		public virtual DbSet<RelatedPersonTable> RelatedPersonTable { get; set; }
		public virtual DbSet<RetentionConditionSetup> RetentionConditionSetup { get; set; }
		public virtual DbSet<RetentionConditionTrans> RetentionConditionTrans { get; set; }
		public virtual DbSet<RetentionTrans> RetentionTrans { get; set; }
		public virtual DbSet<ServiceFeeConditionTrans> ServiceFeeConditionTrans { get; set; }
		public virtual DbSet<ServiceFeeCondTemplateLine> ServiceFeeCondTemplateLine { get; set; }
		public virtual DbSet<ServiceFeeCondTemplateTable> ServiceFeeCondTemplateTable { get; set; }
		public virtual DbSet<ServiceFeeTrans> ServiceFeeTrans { get; set; }
		public virtual DbSet<TaxInvoiceLine> TaxInvoiceLine { get; set; }
		public virtual DbSet<TaxInvoiceTable> TaxInvoiceTable { get; set; }
		public virtual DbSet<TaxReportTrans> TaxReportTrans { get; set; }
		public virtual DbSet<TaxTable> TaxTable { get; set; }
		public virtual DbSet<TaxValue> TaxValue { get; set; }
		public virtual DbSet<Territory> Territory { get; set; }
		public virtual DbSet<VendBank> VendBank { get; set; }
		public virtual DbSet<VendGroup> VendGroup { get; set; }
		public virtual DbSet<VendorPaymentTrans> VendorPaymentTrans { get; set; }
		public virtual DbSet<VendorTable> VendorTable { get; set; }
		public virtual DbSet<VerificationLine> VerificationLine { get; set; }
		public virtual DbSet<VerificationTable> VerificationTable { get; set; }
		public virtual DbSet<VerificationTrans> VerificationTrans { get; set; }
		public virtual DbSet<VerificationType> VerificationType { get; set; }
		public virtual DbSet<WithdrawalLine> WithdrawalLine { get; set; }
		public virtual DbSet<WithdrawalTable> WithdrawalTable { get; set; }
		public virtual DbSet<WithholdingTaxGroup> WithholdingTaxGroup { get; set; }
		public virtual DbSet<WithholdingTaxTable> WithholdingTaxTable { get; set; }
		public virtual DbSet<WithholdingTaxValue> WithholdingTaxValue { get; set; }
		public virtual DbSet<Staging_MainAgreementTable> Staging_MainAgreementTable { get; set; }
		public virtual DbSet<Staging_CustomerTable> Staging_CustomerTable { get; set; }
		public virtual DbSet<Staging_AgreementTableInfo> Staging_AgreementTableInfo { get; set; }
        public virtual DbSet<Staging_CreditAppRequestTable> Staging_CreditAppRequestTable { get; set; }
        public virtual DbSet<Staging_CreditAppRequestLine> Staging_CreditAppRequestLine { get; set; }
		public virtual DbSet<Staging_CreditAppReqBusinessCollateral> Staging_CreditAppReqBusinessCollateral { get; set; }
		public virtual DbSet<Staging_ServiceFeeConditionTrans> Staging_ServiceFeeConditionTrans { get; set; }
		public virtual DbSet<Staging_NCBTrans> Staging_NCBTrans { get; set; }
		public virtual DbSet<Staging_CustBusinessCollateral> Staging_CustBusinessCollateral { get; set; }
		public virtual DbSet<Staging_AssignmentAgreementTable> Staging_AssignmentAgreementTable { get; set; }
		public virtual DbSet<Staging_AssignmentAgreementLine> Staging_AssignmentAgreementLine { get; set; }
		public virtual DbSet<Staging_AssignmentAgreementSettle> Staging_AssignmentAgreementSettle { get; set; }
		public virtual DbSet<Staging_CreditAppTable> Staging_CreditAppTable { get; set; }
		public virtual DbSet<Staging_BusinessCollateralAgmTable> Staging_BusinessCollateralAgmTable { get; set; }
		public virtual DbSet<Staging_BusinessCollateralAgmLine> Staging_BusinessCollateralAgmLine { get; set; }
		public virtual DbSet<Staging_RetentionConditionTrans> Staging_RetentionConditionTrans { get; set; }
		public virtual DbSet<Staging_FinancialCreditTrans> Staging_FinancialCreditTrans { get; set; }
		public virtual DbSet<Staging_CustVisitingTrans> Staging_CustVisitingTrans { get; set; }
		public virtual DbSet<Staging_CreditAppLine> Staging_CreditAppLine { get; set; }
		public virtual DbSet<Staging_CreditAppTrans> Staging_CreditAppTrans { get; set; }
		public virtual DbSet<Staging_VerificationTable> Staging_VerificationTable { get; set; }
		public virtual DbSet<Staging_VerificationLine> Staging_VerificationLine { get; set; }
		public virtual DbSet<Staging_GuarantorAgreementTable> Staging_GuarantorAgreementTable { get; set; }
		public virtual DbSet<Staging_BuyerInvoiceTable> Staging_BuyerInvoiceTable { get; set; }
		public virtual DbSet<Staging_PurchaseTable> Staging_PurchaseTable { get; set; }
		public virtual DbSet<Staging_GuarantorAgreementLine> Staging_GuarantorAgreementLine { get; set; }
		public virtual DbSet<Staging_GuarantorAgreementLineAffiliate> Staging_GuarantorAgreementLineAffiliate { get; set; }
		public virtual DbSet<Staging_InvoiceTable> Staging_InvoiceTable { get; set; }
		public virtual DbSet<Staging_PurchaseLine> Staging_PurchaseLine { get; set; }
		public virtual DbSet<Staging_InvoiceSettlementDetail> Staging_InvoiceSettlementDetail { get; set; }
		public virtual DbSet<Staging_PaymentDetail> Staging_PaymentDetail { get; set; }
		public virtual DbSet<Staging_VerificationTrans> Staging_VerificationTrans { get; set; }
		public virtual DbSet<Staging_InvoiceLine> Staging_InvoiceLine { get; set; }
		public virtual DbSet<Staging_ProductSettledTrans> Staging_ProductSettledTrans { get; set; }
		public virtual DbSet<Staging_ServiceFeeTrans> Staging_ServiceFeeTrans { get; set; }
		public virtual DbSet<Staging_WithdrawalTable> Staging_WithdrawalTable { get; set; }
		public virtual DbSet<Staging_BuyerTable> Staging_BuyerTable { get; set; }
		public virtual DbSet<Staging_CustomerCreditLimitByProduct> Staging_CustomerCreditLimitByProduct { get; set; }
		public virtual DbSet<Staging_FinancialStatementTrans> Staging_FinancialStatementTrans { get; set; }
		public virtual DbSet<Staging_JointVentureTrans> Staging_JointVentureTrans { get; set; }
		public virtual DbSet<Staging_ConsortiumTable> Staging_ConsortiumTable { get; set; }
		public virtual DbSet<Staging_OwnerTrans> Staging_OwnerTrans { get; set; }
		public virtual DbSet<Staging_TaxReportTrans> Staging_TaxReportTrans { get; set; }
		public virtual DbSet<Staging_BuyerCreditLimitByProduct> Staging_BuyerCreditLimitByProduct { get; set; }
		public virtual DbSet<Staging_BuyerAgreementLine> Staging_BuyerAgreementLine { get; set; }
		public virtual DbSet<Staging_BuyerAgreementTable> Staging_BuyerAgreementTable { get; set; }
		public virtual DbSet<Staging_ConsortiumLine> Staging_ConsortiumLine { get; set; }
		public virtual DbSet<Staging_EmployeeTable> Staging_EmployeeTable { get; set; }
		public virtual DbSet<Staging_AddressTrans> Staging_AddressTrans { get; set; }
        public virtual DbSet<Staging_RelatedPersonTable> Staging_RelatedPersonTable { get; set; }
		public virtual DbSet<DocumentReturnMethod> DocumentReturnMethods { get; set; }
		public virtual DbSet<StagingTable> StagingTable { get; set; }
		public virtual DbSet<StagingTableVendorInfo> StagingTableVendorInfo { get; set; }
		public virtual DbSet<StagingTransText> StagingTransText { get; set; }
		public virtual DbSet<Staging_VendorTable> Staging_VendorTable { get; set; }
		public virtual DbSet<Staging_ContactPersonTrans> Staging_ContactPersonTrans { get; set; }
		public virtual DbSet<Staging_ContactTrans> Staging_ContactTrans { get; set; }
		public virtual DbSet<DocumentReturnLine> DocumentReturnLine { get; set; }
		public virtual DbSet<DocumentReturnTable> DocumentReturnTable { get; set; }
		public virtual DbSet<CAReqBuyerCreditOutstanding> CAReqBuyerCreditOutstanding { get; set; }
		public virtual DbSet<Staging_AuthorizedPersonTrans> Staging_AuthorizedPersonTrans { get; set; }
		public virtual DbSet<Staging_WithdrawalLine> Staging_WithdrawalLine { get; set; }
		public virtual DbSet<Staging_ProjectReferenceTrans> Staging_ProjectReferenceTrans { get; set; }
		public virtual DbSet<Staging_CustBank> Staging_CustBank { get; set; }
		public virtual DbSet<Staging_VendBank> Staging_VendBank { get; set; }
		public virtual DbSet<Staging_ChequeTable> Staging_ChequeTable { get; set; }
		public virtual DbSet<Staging_ProjectProgressTable> Staging_ProjectProgressTable { get; set; }
		public virtual DbSet<IntercompanyInvoiceTable> IntercompanyInvoiceTable { get; set; }
		public virtual DbSet<IntercompanyInvoiceSettlement> IntercompanyInvoiceSettlement { get; set; }
		public virtual DbSet<FreeTextInvoiceTable> FreeTextInvoiceTable { get; set; }
		public virtual DbSet<Staging_ProjectProgressTable> Staging_BuyerAgreementTrans { get; set; }
		public virtual DbSet<IntercompanyInvoiceAdjustment> IntercompanyInvoiceAdjustment { get; set; }
		public virtual DbSet<StagingTableIntercoInvSettle> StagingTableIntercoInvSettle { get; set; }
		public virtual DbSet<Staging_IntercompanyInvoiceTable> Staging_IntercompanyInvoiceTable { get; set; }
		public virtual DbSet<Staging_InterestRealizedTrans> Staging_InterestRealizedTrans { get; set; }
		public virtual DbSet<Staging_GuarantorTrans> Staging_GuarantorTrans { get; set; }
		public virtual DbSet<Staging_DocumentConditionTrans> Staging_DocumentConditionTrans { get; set; }
		public virtual DbSet<Staging_ConsortiumTrans> Staging_ConsortiumTrans { get; set; }


		#endregion Dbset
		#region on model creating
		protected void OnModelCreating_CUSTOM(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<ActionHistory>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);

				entity.Property(e => e.ActionHistoryGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.ActionHistoryCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ActionHistoryCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.ActionHistoryBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ActionHistoryBusinessUnit");
			});
			modelBuilder.Entity<AddressCountry>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);

				entity.Property(e => e.AddressCountryGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.AddressCountryCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AddressCountryCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.AddressCountryBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AddressCountryBusinessUnit");
			});
			modelBuilder.Entity<AddressDistrict>(entity =>
			{
				entity.HasIndex(e => e.AddressProvinceGUID);

				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);

				entity.Property(e => e.AddressDistrictGUID).ValueGeneratedNever();

				entity.HasOne(d => d.AddressProvince)
					.WithMany(p => p.AddressDistrictAddressProvince)
					.HasForeignKey(d => d.AddressProvinceGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AddressDistrictAddressProvince");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.AddressDistrictCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AddressDistrictCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.AddressDistrictBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AddressDistrictBusinessUnit");
			});
			modelBuilder.Entity<AddressPostalCode>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.AddressSubDistrictGUID);

				entity.Property(e => e.AddressPostalCodeGUID).ValueGeneratedNever();

				entity.HasOne(d => d.AddressSubDistrict)
					.WithMany(p => p.AddressPostalCodeAddressSubDistrict)
					.HasForeignKey(d => d.AddressSubDistrictGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AddressPostalCodeAddressSubDistrict");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.AddressPostalCodeCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AddressPostalCodeCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.AddressPostalCodeBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AddressPostalCodeBusinessUnit");
			});
			modelBuilder.Entity<AddressProvince>(entity =>
			{
				entity.HasIndex(e => e.AddressCountryGUID);

				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);

				entity.Property(e => e.AddressProvinceGUID).ValueGeneratedNever();

				entity.HasOne(d => d.AddressCountry)
					.WithMany(p => p.AddressProvinceAddressCountry)
					.HasForeignKey(d => d.AddressCountryGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AddressProvinceAddressCountry");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.AddressProvinceCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AddressProvinceCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.AddressProvinceBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AddressProvinceBusinessUnit");
			});
			modelBuilder.Entity<AddressSubDistrict>(entity =>
			{
				entity.HasIndex(e => e.AddressDistrictGUID);

				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);

				entity.Property(e => e.AddressSubDistrictGUID).ValueGeneratedNever();

				entity.HasOne(d => d.AddressDistrict)
					.WithMany(p => p.AddressSubDistrictAddressDistrict)
					.HasForeignKey(d => d.AddressDistrictGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AddressSubDistrictAddressDistrict");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.AddressSubDistrictCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AddressSubDistrictCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.AddressSubDistrictBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AddressSubDistrictBusinessUnit");
			});
			modelBuilder.Entity<AddressTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.AddressCountryGUID);
				entity.HasIndex(e => e.AddressDistrictGUID);
				entity.HasIndex(e => e.AddressPostalCodeGUID);
				entity.HasIndex(e => e.AddressProvinceGUID);
				entity.HasIndex(e => e.AddressSubDistrictGUID);
				entity.HasIndex(e => e.AddressTransGUID);
				entity.HasIndex(e => e.OwnershipGUID);
				entity.HasIndex(e => e.PropertyTypeGUID);
				entity.HasIndex(e => e.RefGUID);

				entity.Property(e => e.AddressTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.AddressTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AddressTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.AddressTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AddressTransBusinessUnit");

				entity.HasOne(d => d.AddressCountry)
					.WithMany(p => p.AddressTransAddressCountry)
					.HasForeignKey(d => d.AddressCountryGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AddressTransAddressCountry");

				entity.HasOne(d => d.AddressDistrict)
					.WithMany(p => p.AddressTransAddressDistrict)
					.HasForeignKey(d => d.AddressDistrictGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AddressTransAddressDistrict");

				entity.HasOne(d => d.AddressPostalCode)
					.WithMany(p => p.AddressTransAddressPostalCode)
					.HasForeignKey(d => d.AddressPostalCodeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AddressTransAddressPostalCode");

				entity.HasOne(d => d.AddressProvince)
					.WithMany(p => p.AddressTransAddressProvince)
					.HasForeignKey(d => d.AddressProvinceGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AddressTransAddressProvince");

				entity.HasOne(d => d.AddressSubDistrict)
					.WithMany(p => p.AddressTransAddressSubDistrict)
					.HasForeignKey(d => d.AddressSubDistrictGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AddressTransAddressSubDistrict");

				entity.HasOne(d => d.Ownership)
					.WithMany(p => p.AddressTransOwnership)
					.HasForeignKey(d => d.OwnershipGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AddressTransOwnership");

				entity.HasOne(d => d.PropertyType)
					.WithMany(p => p.AddressTransPropertyType)
					.HasForeignKey(d => d.PropertyTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AddressTransPropertyType");
			});
			modelBuilder.Entity<AgreementTableInfo>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.AgreementTableInfoGUID);
				entity.HasIndex(e => e.AuthorizedPersonTransBuyer1GUID);
				entity.HasIndex(e => e.AuthorizedPersonTransBuyer2GUID);
				entity.HasIndex(e => e.AuthorizedPersonTransBuyer3GUID);
				entity.HasIndex(e => e.AuthorizedPersonTransCompany1GUID);
				entity.HasIndex(e => e.AuthorizedPersonTransCompany2GUID);
				entity.HasIndex(e => e.AuthorizedPersonTransCompany3GUID);
				entity.HasIndex(e => e.AuthorizedPersonTransCustomer1GUID);
				entity.HasIndex(e => e.AuthorizedPersonTransCustomer2GUID);
				entity.HasIndex(e => e.AuthorizedPersonTransCustomer3GUID);
				entity.HasIndex(e => e.AuthorizedPersonTypeBuyerGUID);
				entity.HasIndex(e => e.AuthorizedPersonTypeCompanyGUID);
				entity.HasIndex(e => e.AuthorizedPersonTypeCustomerGUID);
				entity.HasIndex(e => e.CompanyBankGUID);
				entity.HasIndex(e => e.RefGUID);
				entity.HasIndex(e => e.WitnessCompany1GUID);
				entity.HasIndex(e => e.WitnessCompany2GUID);

				entity.Property(e => e.AgreementTableInfoGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.AgreementTableInfoCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AgreementTableInfoCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.AgreementTableInfoBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AgreementTableInfoBusinessUnit");

				entity.HasOne(d => d.AuthorizedPersonTransBuyer1)
					.WithMany(p => p.AgreementTableInfoAuthorizedPersonTransBuyer1)
					.HasForeignKey(d => d.AuthorizedPersonTransBuyer1GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AgreementTableInfoAuthorizedPersonTransBuyer1");

				entity.HasOne(d => d.AuthorizedPersonTransBuyer2)
					.WithMany(p => p.AgreementTableInfoAuthorizedPersonTransBuyer2)
					.HasForeignKey(d => d.AuthorizedPersonTransBuyer2GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AgreementTableInfoAuthorizedPersonTransBuyer2");

				entity.HasOne(d => d.AuthorizedPersonTransBuyer3)
					.WithMany(p => p.AgreementTableInfoAuthorizedPersonTransBuyer3)
					.HasForeignKey(d => d.AuthorizedPersonTransBuyer3GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AgreementTableInfoAuthorizedPersonTransBuyer3");

				entity.HasOne(d => d.AuthorizedPersonTransCompany1)
					.WithMany(p => p.AgreementTableInfoAuthorizedPersonTransCompany1)
					.HasForeignKey(d => d.AuthorizedPersonTransCompany1GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AgreementTableInfoAuthorizedPersonTransCompany1");

				entity.HasOne(d => d.AuthorizedPersonTransCompany2)
					.WithMany(p => p.AgreementTableInfoAuthorizedPersonTransCompany2)
					.HasForeignKey(d => d.AuthorizedPersonTransCompany2GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AgreementTableInfoAuthorizedPersonTransCompany2");

				entity.HasOne(d => d.AuthorizedPersonTransCompany3)
					.WithMany(p => p.AgreementTableInfoAuthorizedPersonTransCompany3)
					.HasForeignKey(d => d.AuthorizedPersonTransCompany3GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AgreementTableInfoAuthorizedPersonTransCompany3");

				entity.HasOne(d => d.AuthorizedPersonTransCustomer1)
					.WithMany(p => p.AgreementTableInfoAuthorizedPersonTransCustomer1)
					.HasForeignKey(d => d.AuthorizedPersonTransCustomer1GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AgreementTableInfoAuthorizedPersonTransCustomer1");

				entity.HasOne(d => d.AuthorizedPersonTransCustomer2)
					.WithMany(p => p.AgreementTableInfoAuthorizedPersonTransCustomer2)
					.HasForeignKey(d => d.AuthorizedPersonTransCustomer2GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AgreementTableInfoAuthorizedPersonTransCustomer2");

				entity.HasOne(d => d.AuthorizedPersonTransCustomer3)
					.WithMany(p => p.AgreementTableInfoAuthorizedPersonTransCustomer3)
					.HasForeignKey(d => d.AuthorizedPersonTransCustomer3GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AgreementTableInfoAuthorizedPersonTransCustomer3");

				entity.HasOne(d => d.AuthorizedPersonTypeBuyer)
					.WithMany(p => p.AgreementTableInfoAuthorizedPersonTypeBuyer)
					.HasForeignKey(d => d.AuthorizedPersonTypeBuyerGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AgreementTableInfoAuthorizedPersonTypeBuyer");

				entity.HasOne(d => d.AuthorizedPersonTypeCompany)
					.WithMany(p => p.AgreementTableInfoAuthorizedPersonTypeCompany)
					.HasForeignKey(d => d.AuthorizedPersonTypeCompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AgreementTableInfoAuthorizedPersonTypeCompany");

				entity.HasOne(d => d.AuthorizedPersonTypeCustomer)
					.WithMany(p => p.AgreementTableInfoAuthorizedPersonTypeCustomer)
					.HasForeignKey(d => d.AuthorizedPersonTypeCustomerGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AgreementTableInfoAuthorizedPersonTypeCustomer");

				entity.HasOne(d => d.CompanyBank)
					.WithMany(p => p.AgreementTableInfoCompanyBank)
					.HasForeignKey(d => d.CompanyBankGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AgreementTableInfoCompanyBank");

				entity.HasOne(d => d.WitnessCompany1)
					.WithMany(p => p.AgreementTableInfoWitnessCompany1)
					.HasForeignKey(d => d.WitnessCompany1GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AgreementTableInfoWitnessCompany1");

				entity.HasOne(d => d.WitnessCompany2)
					.WithMany(p => p.AgreementTableInfoWitnessCompany2)
					.HasForeignKey(d => d.WitnessCompany2GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AgreementTableInfoWitnessCompany2");
			});
			modelBuilder.Entity<AgreementTableInfoText>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.AgreementTableInfoGUID);
				entity.HasIndex(e => e.AgreementTableInfoTextGUID);

				entity.Property(e => e.AgreementTableInfoTextGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.AgreementTableInfoTextCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AgreementTableInfoTextCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.AgreementTableInfoTextBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AgreementTableInfoTextBusinessUnit");
			});
			modelBuilder.Entity<AgreementType>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);

				entity.Property(e => e.AgreementTypeGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.AgreementTypeCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AgreementTypeCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.AgreementTypeBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AgreementTypeBusinessUnit");
			});
			modelBuilder.Entity<ApplicationTable>(entity =>
			{
				entity.HasIndex(e => e.AgreementTypeGUID);
				entity.HasIndex(e => e.BillingAddressGUID);
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.CreditResultGUID);
				entity.HasIndex(e => e.CurrencyGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.DeliveryAddressGUID);
				entity.HasIndex(e => e.Dimension1GUID);
				entity.HasIndex(e => e.Dimension2GUID);
				entity.HasIndex(e => e.Dimension3GUID);
				entity.HasIndex(e => e.Dimension4GUID);
				entity.HasIndex(e => e.Dimension5GUID);
				entity.HasIndex(e => e.DocumentReasonGUID);
				entity.HasIndex(e => e.DocumentStatusGUID);
				entity.HasIndex(e => e.IntroducedByGUID);
				entity.HasIndex(e => e.InvoiceAddressGUID);
				entity.HasIndex(e => e.LanguageGUID);
				entity.HasIndex(e => e.LeaseSubTypeGUID);
				entity.HasIndex(e => e.LeaseTypeGUID);
				entity.HasIndex(e => e.MailingInvoiceAddressGUID)
					.HasName("IX_ApplicationTable_MaillingInvoiceAddressGUID");
				entity.HasIndex(e => e.MailingReceiptAddressGUID)
					.HasName("IX_ApplicationTable_MaillingReceiptAddressGUID");
				entity.HasIndex(e => e.NCBAccountStatusGUID);
				entity.HasIndex(e => e.OriginalAgreementGUID);
				entity.HasIndex(e => e.PaymentFrequencyGUID);
				entity.HasIndex(e => e.PropertyAddressGUID);
				entity.HasIndex(e => e.ProspectTableGUID);
				entity.HasIndex(e => e.ReceiptAddressGUID);
				entity.HasIndex(e => e.RegisterAddressGUID);
				entity.HasIndex(e => e.ResponsibleBy);
				entity.HasIndex(e => e.WorkingAddressGUID);

				entity.Property(e => e.ApplicationTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.AgreementType)
					.WithMany(p => p.ApplicationTableAgreementType)
					.HasForeignKey(d => d.AgreementTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ApplicationTableAgreementType");

				entity.HasOne(d => d.BillingAddress)
					.WithMany(p => p.ApplicationTableBillingAddress)
					.HasForeignKey(d => d.BillingAddressGUID)
					.HasConstraintName("FK_ApplicationTableBillingAddress");

				entity.HasOne(d => d.Currency)
					.WithMany(p => p.ApplicationTableCurrency)
					.HasForeignKey(d => d.CurrencyGUID)
					.HasConstraintName("FK_ApplicationTableCurrency");

				entity.HasOne(d => d.CustomerTable)
					.WithMany(p => p.ApplicationTableCustomerTable)
					.HasForeignKey(d => d.CustomerTableGUID)
					.HasConstraintName("FK_ApplicationTableCustomerTable");

				entity.HasOne(d => d.DeliveryAddress)
					.WithMany(p => p.ApplicationTableDeliveryAddress)
					.HasForeignKey(d => d.DeliveryAddressGUID)
					.HasConstraintName("FK_ApplicationTableDeliveryAddress");

				entity.HasOne(d => d.LedgerDimension1)
					.WithMany(p => p.ApplicationTableDimension1)
					.HasForeignKey(d => d.Dimension1GUID)
					.HasConstraintName("FK_ApplicationTableLedgerDimension1");

				entity.HasOne(d => d.LedgerDimension2)
					.WithMany(p => p.ApplicationTableDimension2)
					.HasForeignKey(d => d.Dimension2GUID)
					.HasConstraintName("FK_ApplicationTableLedgerDimension2");

				entity.HasOne(d => d.LedgerDimension3)
					.WithMany(p => p.ApplicationTableDimension3)
					.HasForeignKey(d => d.Dimension3GUID)
					.HasConstraintName("FK_ApplicationTableLedgerDimension3");

				entity.HasOne(d => d.LedgerDimension4)
					.WithMany(p => p.ApplicationTableDimension4)
					.HasForeignKey(d => d.Dimension4GUID)
					.HasConstraintName("FK_ApplicationTableLedgerDimension4");

				entity.HasOne(d => d.LedgerDimension5)
					.WithMany(p => p.ApplicationTableDimension5)
					.HasForeignKey(d => d.Dimension5GUID)
					.HasConstraintName("FK_ApplicationTableLedgerDimension5");

				entity.HasOne(d => d.DocumentReason)
					.WithMany(p => p.ApplicationTableDocumentReason)
					.HasForeignKey(d => d.DocumentReasonGUID)
					.HasConstraintName("FK_ApplicationTableDocumentReason");

				entity.HasOne(d => d.DocumentStatus)
					.WithMany(p => p.ApplicationTableDocumentStatus)
					.HasForeignKey(d => d.DocumentStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ApplicationTableDocumentStatus");

				entity.HasOne(d => d.IntroducedBy)
					.WithMany(p => p.ApplicationTableIntroducedBy)
					.HasForeignKey(d => d.IntroducedByGUID)
					.HasConstraintName("FK_ApplicationTableIntroducedBy");

				entity.HasOne(d => d.InvoiceAddress)
					.WithMany(p => p.ApplicationTableInvoiceAddress)
					.HasForeignKey(d => d.InvoiceAddressGUID)
					.HasConstraintName("FK_ApplicationTableInvoiceAddress");

				entity.HasOne(d => d.Language)
					.WithMany(p => p.ApplicationTableLanguage)
					.HasForeignKey(d => d.LanguageGUID)
					.HasConstraintName("FK_ApplicationTableLanguage");

				entity.HasOne(d => d.LeaseType)
					.WithMany(p => p.ApplicationTableLeaseType)
					.HasForeignKey(d => d.LeaseTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ApplicationTableLeaseType");

				entity.HasOne(d => d.MailingInvoiceAddress)
					.WithMany(p => p.ApplicationTableMailingInvoiceAddress)
					.HasForeignKey(d => d.MailingInvoiceAddressGUID)
					.HasConstraintName("FK_ApplicationTableMailingInvoiceAddress");

				entity.HasOne(d => d.MailingReceiptAddress)
					.WithMany(p => p.ApplicationTableMailingReceiptAddress)
					.HasForeignKey(d => d.MailingReceiptAddressGUID)
					.HasConstraintName("FK_ApplicationTableMailingReceiptAddress");

				entity.HasOne(d => d.NCBAccountStatus)
					.WithMany(p => p.ApplicationTableNCBAccountStatus)
					.HasForeignKey(d => d.NCBAccountStatusGUID)
					.HasConstraintName("FK_ApplicationTableNCBAccountStatus");

				entity.HasOne(d => d.PaymentFrequency)
					.WithMany(p => p.ApplicationTablePaymentFrequency)
					.HasForeignKey(d => d.PaymentFrequencyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ApplicationTablePaymentFrequency");

				entity.HasOne(d => d.PropertyAddress)
					.WithMany(p => p.ApplicationTablePropertyAddress)
					.HasForeignKey(d => d.PropertyAddressGUID)
					.HasConstraintName("FK_ApplicationTablePropertyAddress");

				entity.HasOne(d => d.ReceiptAddress)
					.WithMany(p => p.ApplicationTableReceiptAddress)
					.HasForeignKey(d => d.ReceiptAddressGUID)
					.HasConstraintName("FK_ApplicationTableReceiptAddress");

				entity.HasOne(d => d.RegisterAddress)
					.WithMany(p => p.ApplicationTableRegisterAddress)
					.HasForeignKey(d => d.RegisterAddressGUID)
					.HasConstraintName("FK_ApplicationTableRegisterAddress");

				entity.HasOne(d => d.WorkingAddress)
					.WithMany(p => p.ApplicationTableWorkingAddress)
					.HasForeignKey(d => d.WorkingAddressGUID)
					.HasConstraintName("FK_ApplicationTableWorkingAddress");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.ApplicationTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ApplicationTableCodeCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.ApplicationTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ApplicationTableBusinessUnit");
			});
			modelBuilder.Entity<AssignmentAgreementLine>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.AssignmentAgreementLineGUID);
				entity.HasIndex(e => e.AssignmentAgreementTableGUID);
				entity.HasIndex(e => e.BuyerAgreementTableGUID);

				entity.Property(e => e.AssignmentAgreementLineGUID).ValueGeneratedNever();

				entity.HasOne(d => d.AssignmentAgreementTable)
					.WithMany(p => p.AssignmentAgreementLineAssignmentAgreementTable)
					.HasForeignKey(d => d.AssignmentAgreementTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AssignmentAgreementLineAssignmentAgreementTable");

				entity.HasOne(d => d.BuyerAgreementTable)
					.WithMany(p => p.AssignmentAgreementLineBuyerAgreementTable)
					.HasForeignKey(d => d.BuyerAgreementTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AssignmentAgreementLineBuyerAgreementTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.AssignmentAgreementLineCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AssignmentAgreementLineCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.AssignmentAgreementLineBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AssignmentAgreementLineBusinessUnit");
			});
			modelBuilder.Entity<AssignmentAgreementSettle>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.AssignmentAgreementSettleGUID);
				entity.HasIndex(e => e.AssignmentAgreementTableGUID);
				entity.HasIndex(e => e.BuyerAgreementTableGUID);
				entity.HasIndex(e => e.DocumentReasonGUID);
				entity.HasIndex(e => e.RefAssignmentAgreementSettleGUID);
				entity.HasIndex(e => e.RefGUID);
				entity.HasIndex(e => e.InvoiceTableGUID);

				entity.Property(e => e.AssignmentAgreementSettleGUID).ValueGeneratedNever();

				entity.HasOne(d => d.AssignmentAgreementTable)
					.WithMany(p => p.AssignmentAgreementSettleAssignmentAgreementTable)
					.HasForeignKey(d => d.AssignmentAgreementTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AssignmentAgreementSettleAssignmentAgreementTable");

				entity.HasOne(d => d.BuyerAgreementTable)
					.WithMany(p => p.AssignmentAgreementSettleBuyerAgreementTable)
					.HasForeignKey(d => d.BuyerAgreementTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AssignmentAgreementSettleBuyerAgreementTable");

				entity.HasOne(d => d.DocumentReason)
					.WithMany(p => p.AssignmentAgreementSettleDocumentReason)
					.HasForeignKey(d => d.DocumentReasonGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AssignmentAgreementSettleDocumentReason");

				entity.HasOne(d => d.RefAssignmentAgreementSettle)
					.WithMany(p => p.AssignmentAgreementSettleRefAssignmentAgreementSettle)
					.HasForeignKey(d => d.RefAssignmentAgreementSettleGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AssignmentAgreementSettleAssignmentAgreementSettle");

				entity.HasOne(d => d.InvoiceTable)
					.WithMany(p => p.AssignmentAgreementSettleInvoiceTable)
					.HasForeignKey(d => d.InvoiceTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AssignmentAgreementSettleInvoiceTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.AssignmentAgreementSettleCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AssignmentAgreementSettleCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.AssignmentAgreementSettleBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AssignmentAgreementSettleBusinessUnit");

			});
			modelBuilder.Entity<AssignmentAgreementTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.AssignmentAgreementTableGUID);
				entity.HasIndex(e => e.AssignmentMethodGUID);
				entity.HasIndex(e => e.BuyerTableGUID);
				entity.HasIndex(e => e.ConsortiumTableGUID);
				entity.HasIndex(e => e.CustBankGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.DocumentReasonGUID);
				entity.HasIndex(e => e.DocumentStatusGUID);
				entity.HasIndex(e => e.RefAssignmentAgreementTableGUID);
				entity.HasIndex(e => e.RefCreditAppRequestTableGUID);
				entity.HasIndex(e => e.CreditAppReqAssignmentGUID);

				entity.Property(e => e.AssignmentAgreementTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.AssignmentMethod)
					.WithMany(p => p.AssignmentAgreementTableAssignmentMethod)
					.HasForeignKey(d => d.AssignmentMethodGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AssignmentAgreementTableAssignmentMethod");

				entity.HasOne(d => d.BuyerTable)
					.WithMany(p => p.AssignmentAgreementTableBuyerTable)
					.HasForeignKey(d => d.BuyerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AssignmentAgreementTableBuyerTable");

				entity.HasOne(d => d.ConsortiumTable)
					.WithMany(p => p.AssignmentAgreementTableConsortiumTable)
					.HasForeignKey(d => d.ConsortiumTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AssignmentAgreementTableConsortiumTable");

				entity.HasOne(d => d.CustBank)
					.WithMany(p => p.AssignmentAgreementTableCustBank)
					.HasForeignKey(d => d.CustBankGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AssignmentAgreementTableCustBank");

				entity.HasOne(d => d.CustomerTable)
					.WithMany(p => p.AssignmentAgreementTableCustomerTable)
					.HasForeignKey(d => d.CustomerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AssignmentAgreementTableCustomerTable");

				entity.HasOne(d => d.DocumentReason)
					.WithMany(p => p.AssignmentAgreementTableDocumentReason)
					.HasForeignKey(d => d.DocumentReasonGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AssignmentAgreementTableDocumentReason");

				entity.HasOne(d => d.DocumentStatus)
					.WithMany(p => p.AssignmentAgreementTableDocumentStatus)
					.HasForeignKey(d => d.DocumentStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AssignmentAgreementTableDocumentStatus");

				entity.HasOne(d => d.RefAssignmentAgreementTable)
					.WithMany(p => p.AssignmentAgreementTableRefAssignmentAgreementTable)
					.HasForeignKey(d => d.RefAssignmentAgreementTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AssignmentAgreementTableAssignmentAgreementTable");

				entity.HasOne(d => d.RefCreditAppRequestTable)
					.WithMany(p => p.AssignmentAgreementTableRefCreditAppRequestTable)
					.HasForeignKey(d => d.RefCreditAppRequestTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AssignmentAgreementTableRefCreditAppRequestTable");

				entity.HasOne(d => d.CreditAppReqAssignment)
					.WithMany(p => p.AssignmentAgreementTableCreditAppReqAssignment)
					.HasForeignKey(d => d.CreditAppReqAssignmentGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AssignmentAgreementTableCreditAppReqAssignment");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.AssignmentAgreementTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AssignmentAgreementTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.AssignmentAgreementTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AssignmentAgreementTableBusinessUnit");
			});
			modelBuilder.Entity<AssignmentMethod>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.AssignmentMethodGUID);

				entity.Property(e => e.AssignmentMethodGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.AssignmentMethodCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AssignmentMethodCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.AssignmentMethodBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AssignmentMethodBusinessUnit");
			});
			modelBuilder.Entity<Attachment>(entity =>
			{
				entity.Property(e => e.AttachmentGUID).ValueGeneratedNever();

				entity.Property(e => e.Base64Data).IsUnicode(false);
			});
			modelBuilder.Entity<AuthorizedPersonTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.AuthorizedPersonTransGUID);
				entity.HasIndex(e => e.AuthorizedPersonTypeGUID);
				entity.HasIndex(e => e.RefAuthorizedPersonTransGUID);
				entity.HasIndex(e => e.RefGUID);
				entity.HasIndex(e => e.RelatedPersonTableGUID);
				entity.HasIndex(e => e.ReplacedAuthorizedPersonTransGUID);

				entity.Property(e => e.AuthorizedPersonTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.AuthorizedPersonType)
					.WithMany(p => p.AuthorizedPersonTransAuthorizedPersonType)
					.HasForeignKey(d => d.AuthorizedPersonTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AuthorizedPersonTransAuthorizedPersonType");

				entity.HasOne(d => d.RelatedPersonTable)
					.WithMany(p => p.AuthorizedPersonTransRelatedPersonTable)
					.HasForeignKey(d => d.RelatedPersonTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AuthorizedPersonTransRelatedPersonTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.AuthorizedPersonTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AuthorizedPersonTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.AuthorizedPersonTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AuthorizedPersonTransBusinessUnit");
			});
			modelBuilder.Entity<AuthorizedPersonType>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.AuthorizedPersonTypeGUID);

				entity.Property(e => e.AuthorizedPersonTypeGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.AuthorizedPersonTypeCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AuthorizedPersonTypeCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.AuthorizedPersonTypeBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AuthorizedPersonTypeBusinessUnit");
			});
			modelBuilder.Entity<BankGroup>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);

				entity.Property(e => e.BankGroupGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.BankGroupCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BankGroupCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.BankGroupBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BankGroupBusinessUnit");
			});
			modelBuilder.Entity<BankType>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BankTypeGUID);

				entity.Property(e => e.BankTypeGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.BankTypeCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BankTypeCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.BankTypeBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BankTypeBusinessUnit");
			});
			modelBuilder.Entity<BillingResponsibleBy>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BillingResponsibleByGUID);

				entity.Property(e => e.BillingResponsibleByGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.BillingResponsibleByCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BillingResponsibleByCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.BillingResponsibleByBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BillingResponsibleByBusinessUnit");
			});
			modelBuilder.Entity<BlacklistStatus>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BlacklistStatusGUID);

				entity.Property(e => e.BlacklistStatusGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.BlacklistStatusCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BlacklistStatusCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.BlacklistStatusBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BlacklistStatusBusinessUnit");
			});
			modelBuilder.Entity<BookmarkDocument>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BookmarkDocumentGUID);

				entity.Property(e => e.BookmarkDocumentGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.BookmarkDocumentCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BookmarkDocumentCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.BookmarkDocumentBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BookmarkDocumentBusinessUnit");
			});
			modelBuilder.Entity<BookmarkDocumentTemplateLine>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BookmarkDocumentGUID);
				entity.HasIndex(e => e.BookmarkDocumentTemplateLineGUID);
				entity.HasIndex(e => e.BookmarkDocumentTemplateTableGUID);
				entity.HasIndex(e => e.DocumentTemplateTableGUID);

				entity.Property(e => e.BookmarkDocumentTemplateLineGUID).ValueGeneratedNever();

				entity.HasOne(d => d.BookmarkDocument)
					.WithMany(p => p.BookmarkDocumentTemplateLineBookmarkDocument)
					.HasForeignKey(d => d.BookmarkDocumentGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BookmarkDocumentTemplateLineBookmarkDocument");

				entity.HasOne(d => d.BookmarkDocumentTemplateTable)
					.WithMany(p => p.BookmarkDocumentTemplateLineBookmarkDocumentTemplateTable)
					.HasForeignKey(d => d.BookmarkDocumentTemplateTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BookmarkDocumentTemplateLineBookmarkDocumentTemplateTable");

				entity.HasOne(d => d.DocumentTemplateTable)
					.WithMany(p => p.BookmarkDocumentTemplateLineDocumentTemplateTable)
					.HasForeignKey(d => d.DocumentTemplateTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BookmarkDocumentTemplateLineDocumentTemplateTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.BookmarkDocumentTemplateLineCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BookmarkDocumentTemplateLineCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.BookmarkDocumentTemplateLineBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BookmarkDocumentTemplateLineBusinessUnit");
			});
			modelBuilder.Entity<BookmarkDocumentTemplateTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BookmarkDocumentTemplateTableGUID);

				entity.Property(e => e.BookmarkDocumentTemplateTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.BookmarkDocumentTemplateTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BookmarkDocumentTemplateTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.BookmarkDocumentTemplateTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BookmarkDocumentTemplateTableBusinessUnit");
			});
			modelBuilder.Entity<BookmarkDocumentTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BookmarkDocumentGUID);
				entity.HasIndex(e => e.BookmarkDocumentTransGUID);
				entity.HasIndex(e => e.DocumentStatusGUID);
				entity.HasIndex(e => e.DocumentTemplateTableGUID);
				entity.HasIndex(e => e.RefGUID);

				entity.Property(e => e.BookmarkDocumentTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.BookmarkDocument)
					.WithMany(p => p.BookmarkDocumentTransBookmarkDocument)
					.HasForeignKey(d => d.BookmarkDocumentGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BookmarkDocumentTransBookmarkDocument");

				entity.HasOne(d => d.DocumentStatus)
					.WithMany(p => p.BookmarkDocumentTransDocumentStatus)
					.HasForeignKey(d => d.DocumentStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BookmarkDocumentTransDocumentStatus");

				entity.HasOne(d => d.DocumentTemplateTable)
					.WithMany(p => p.BookmarkDocumentTransDocumentTemplateTable)
					.HasForeignKey(d => d.DocumentTemplateTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BookmarkDocumentTransDocumentTemplateTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.BookmarkDocumentTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BookmarkDocumentTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.BookmarkDocumentTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BookmarkDocumentTransBusinessUnit");
			});
			modelBuilder.Entity<BusinessCollateralAgmLine>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BankGroupGUID);
				entity.HasIndex(e => e.BankTypeGUID);
				entity.HasIndex(e => e.BusinessCollateralAgmLineGUID);
				entity.HasIndex(e => e.BusinessCollateralAgmTableGUID);
				entity.HasIndex(e => e.BusinessCollateralSubTypeGUID);
				entity.HasIndex(e => e.BusinessCollateralTypeGUID);
				entity.HasIndex(e => e.BuyerTableGUID);
				entity.HasIndex(e => e.CreditAppReqBusinessCollateralGUID);
				entity.HasIndex(e => e.BusinessCollateralStatusGUID);

				entity.Property(e => e.BusinessCollateralAgmLineGUID).ValueGeneratedNever();

				entity.HasOne(d => d.BankGroup)
					.WithMany(p => p.BusinessCollateralAgmLineBankGroup)
					.HasForeignKey(d => d.BankGroupGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessCollateralAgmLineBankGroup");

				entity.HasOne(d => d.BankType)
					.WithMany(p => p.BusinessCollateralAgmLineBankType)
					.HasForeignKey(d => d.BankTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessCollateralAgmLineBankType");

				entity.HasOne(d => d.BusinessCollateralAgmTable)
					.WithMany(p => p.BusinessCollateralAgmLineBusinessCollateralAgmTable)
					.HasForeignKey(d => d.BusinessCollateralAgmTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessCollateralAgmLineBusinessCollateralAgmTable");

				entity.HasOne(d => d.BusinessCollateralSubType)
					.WithMany(p => p.BusinessCollateralAgmLineBusinessCollateralSubType)
					.HasForeignKey(d => d.BusinessCollateralSubTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessCollateralAgmLineBusinessCollateralSubType");

				entity.HasOne(d => d.BusinessCollateralType)
					.WithMany(p => p.BusinessCollateralAgmLineBusinessCollateralType)
					.HasForeignKey(d => d.BusinessCollateralTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessCollateralAgmLineBusinessCollateralType");

				entity.HasOne(d => d.BuyerTable)
					.WithMany(p => p.BusinessCollateralAgmLineBuyerTable)
					.HasForeignKey(d => d.BuyerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessCollateralAgmLineBuyerTable");

				entity.HasOne(d => d.CreditAppReqBusinessCollateral)
					.WithMany(p => p.BusinessCollateralAgmLineCreditAppReqBusinessCollateral)
					.HasForeignKey(d => d.CreditAppReqBusinessCollateralGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessCollateralAgmLineCreditAppReqBusinessCollateral");

				entity.HasOne(d => d.BusinessCollateralStatus)
					.WithMany(p => p.BusinessCollateralAgmLineBusinessCollateralStatus)
					.HasForeignKey(d => d.BusinessCollateralStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessCollateralAgmLineBusinessCollateralStatus");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.BusinessCollateralAgmLineCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessCollateralAgmLineCodeCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.BusinessCollateralAgmLineBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessCollateralAgmLineBusinessUnit");
			});
			modelBuilder.Entity<BusinessCollateralAgmTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BusinessCollateralAgmTableGUID);
				entity.HasIndex(e => e.ConsortiumTableGUID);
				entity.HasIndex(e => e.CreditAppRequestTableGUID);
				entity.HasIndex(e => e.CreditAppTableGUID);
				entity.HasIndex(e => e.CreditLimitTypeGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.DocumentReasonGUID);
				entity.HasIndex(e => e.DocumentStatusGUID);
				entity.HasIndex(e => e.RefBusinessCollateralAgmTableGUID);

				entity.Property(e => e.BusinessCollateralAgmTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.ConsortiumTable)
					.WithMany(p => p.BusinessCollateralAgmTableConsortiumTable)
					.HasForeignKey(d => d.ConsortiumTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessCollateralAgmTableConsortiumTable");

				entity.HasOne(d => d.CreditAppRequestTable)
					.WithMany(p => p.BusinessCollateralAgmTableCreditAppRequestTable)
					.HasForeignKey(d => d.CreditAppRequestTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessCollateralAgmTableCreditAppRequestTable");

				entity.HasOne(d => d.CreditAppTable)
					.WithMany(p => p.BusinessCollateralAgmTableCreditAppTable)
					.HasForeignKey(d => d.CreditAppTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessCollateralAgmTableCreditAppTable");

				entity.HasOne(d => d.CreditLimitType)
					.WithMany(p => p.BusinessCollateralAgmTableCreditLimitType)
					.HasForeignKey(d => d.CreditLimitTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessCollateralAgmTableCreditLimitType");

				entity.HasOne(d => d.DocumentReason)
					.WithMany(p => p.BusinessCollateralAgmTableDocumentReason)
					.HasForeignKey(d => d.DocumentReasonGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessCollateralAgmTableDocumentReason");

				entity.HasOne(d => d.DocumentStatus)
					.WithMany(p => p.BusinessCollateralAgmTableDocumentStatus)
					.HasForeignKey(d => d.DocumentStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessCollateralAgmTableDocumentStatus");

				entity.HasOne(d => d.RefBusinessCollateralAgmTable)
					.WithMany(p => p.BusinessCollateralAgmTableRefBusinessCollateralAgmTable)
					.HasForeignKey(d => d.RefBusinessCollateralAgmTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessCollateralAgmTableBusinessCollateralAgmTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.BusinessCollateralAgmTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessCollateralAgmTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.BusinessCollateralAgmTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessCollateralAgmTableBusinessUnit");
			});
			modelBuilder.Entity<BusinessCollateralStatus>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BusinessCollateralStatusGUID);

				entity.Property(e => e.BusinessCollateralStatusGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.BusinessCollateralStatusCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessCollateralStatusCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.BusinessCollateralStatusBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessCollateralStatusBusinessUnit");
			});
			modelBuilder.Entity<BusinessCollateralSubType>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BusinessCollateralSubTypeGUID);
				entity.HasIndex(e => e.BusinessCollateralTypeGUID);

				entity.Property(e => e.BusinessCollateralSubTypeGUID).ValueGeneratedNever();

				entity.HasOne(d => d.BusinessCollateralType)
					.WithMany(p => p.BusinessCollateralSubTypeBusinessCollateralType)
					.HasForeignKey(d => d.BusinessCollateralTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessCollateralSubTypeBusinessCollateralType");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.BusinessCollateralSubTypeCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessCollateralSubTypeCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.BusinessCollateralSubTypeBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessCollateralSubTypeBusinessUnit");
			});
			modelBuilder.Entity<BusinessCollateralType>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BusinessCollateralTypeGUID);

				entity.Property(e => e.BusinessCollateralTypeGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.BusinessCollateralTypeCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessCollateralTypeCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.BusinessCollateralTypeBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessCollateralTypeBusinessUnit");
			});
			modelBuilder.Entity<BusinessSegment>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BusinessSegmentGUID);

				entity.Property(e => e.BusinessSegmentGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.BusinessSegmentCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessSegmentCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.BusinessSegmentBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessSegmentBusinessUnit");
			});
			modelBuilder.Entity<BusinessSize>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BusinessSizeGUID);

				entity.Property(e => e.BusinessSizeGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.BusinessSizeCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessSizeCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.BusinessSizeBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessSizeBusinessUnit");
			});
			modelBuilder.Entity<BusinessType>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BusinessTypeGUID);

				entity.Property(e => e.BusinessTypeGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.BusinessTypeCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessTypeCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.BusinessTypeBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BusinessTypeBusinessUnit");
			});
			modelBuilder.Entity<BuyerAgreementLine>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BuyerAgreementLineGUID);
				entity.HasIndex(e => e.BuyerAgreementTableGUID);

				entity.Property(e => e.BuyerAgreementLineGUID).ValueGeneratedNever();

				entity.HasOne(d => d.BuyerAgreementTable)
					.WithMany(p => p.BuyerAgreementLineBuyerAgreementTable)
					.HasForeignKey(d => d.BuyerAgreementTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerAgreementLineBuyerAgreementTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.BuyerAgreementLineCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerAgreementLineCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.BuyerAgreementLineBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerAgreementLineBusinessUnit");
			});
			modelBuilder.Entity<BuyerAgreementTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BuyerAgreementTableGUID);
				entity.HasIndex(e => e.BuyerTableGUID);
				entity.HasIndex(e => e.CustomerTableGUID);

				entity.Property(e => e.BuyerAgreementTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.BuyerTable)
					.WithMany(p => p.BuyerAgreementTableBuyerTable)
					.HasForeignKey(d => d.BuyerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerAgreementTableBuyerTable");

				entity.HasOne(d => d.CustomerTable)
					.WithMany(p => p.BuyerAgreementTableCustomerTable)
					.HasForeignKey(d => d.CustomerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerAgreementTableCustomerTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.BuyerAgreementTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerAgreementTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.BuyerAgreementTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerAgreementTableBusinessUnit");
			});
			modelBuilder.Entity<BuyerAgreementTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BuyerAgreementLineGUID);
				entity.HasIndex(e => e.BuyerAgreementTableGUID);
				entity.HasIndex(e => e.BuyerAgreementTransGUID);
				entity.HasIndex(e => e.RefGUID);

				entity.Property(e => e.BuyerAgreementTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.BuyerAgreementLine)
					.WithMany(p => p.BuyerAgreementTransBuyerAgreementLine)
					.HasForeignKey(d => d.BuyerAgreementLineGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerAgreementTransBuyerAgreementLine");

				entity.HasOne(d => d.BuyerAgreementTable)
					.WithMany(p => p.BuyerAgreementTransBuyerAgreementTable)
					.HasForeignKey(d => d.BuyerAgreementTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerAgreementTransBuyerAgreementTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.BuyerAgreementTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerAgreementTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.BuyerAgreementTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerAgreementTransBusinessUnit");
			});
			modelBuilder.Entity<BuyerCreditLimitByProduct>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BuyerCreditLimitByProductGUID);
				entity.HasIndex(e => e.BuyerTableGUID);

				entity.Property(e => e.BuyerCreditLimitByProductGUID).ValueGeneratedNever();

				entity.HasOne(d => d.BuyerTable)
					.WithMany(p => p.BuyerCreditLimitByProductBuyerTable)
					.HasForeignKey(d => d.BuyerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerCreditLimitByProductBuyerTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.BuyerCreditLimitByProductCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerCreditLimitByProductCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.BuyerCreditLimitByProductBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerCreditLimitByProductBusinessUnit");
			});
			modelBuilder.Entity<BuyerInvoiceTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BuyerAgreementLineGUID);
				entity.HasIndex(e => e.BuyerAgreementTableGUID);
				entity.HasIndex(e => e.BuyerInvoiceTableGUID);
				entity.HasIndex(e => e.CreditAppLineGUID);

				entity.Property(e => e.BuyerInvoiceTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.BuyerAgreementLine)
					.WithMany(p => p.BuyerInvoiceTableBuyerAgreementLine)
					.HasForeignKey(d => d.BuyerAgreementLineGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerInvoiceTableBuyerAgreementLine");

				entity.HasOne(d => d.BuyerAgreementTable)
					.WithMany(p => p.BuyerInvoiceTableBuyerAgreementTable)
					.HasForeignKey(d => d.BuyerAgreementTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerInvoiceTableBuyerAgreementTable");

				entity.HasOne(d => d.CreditAppLine)
					.WithMany(p => p.BuyerInvoiceTableCreditAppLine)
					.HasForeignKey(d => d.CreditAppLineGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerInvoiceTableCreditAppLine");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.BuyerInvoiceTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerInvoiceTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.BuyerInvoiceTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerInvoiceTableBusinessUnit");
			});
			modelBuilder.Entity<BuyerReceiptTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.AssignmentAgreementTableGUID);
				entity.HasIndex(e => e.BuyerAgreementTableGUID);
				entity.HasIndex(e => e.BuyerReceiptTableGUID);
				entity.HasIndex(e => e.BuyerTableGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.MethodOfPaymentGUID);
				entity.HasIndex(e => e.RefGUID);

				entity.Property(e => e.BuyerReceiptTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.AssignmentAgreementTable)
					.WithMany(p => p.BuyerReceiptTableAssignmentAgreementTable)
					.HasForeignKey(d => d.AssignmentAgreementTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerReceiptTableAssignmentAgreementTable");

				entity.HasOne(d => d.BuyerAgreementTable)
					.WithMany(p => p.BuyerReceiptTableBuyerAgreementTable)
					.HasForeignKey(d => d.BuyerAgreementTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerReceiptTableBuyerAgreementTable");

				entity.HasOne(d => d.BuyerTable)
					.WithMany(p => p.BuyerReceiptTableBuyerTable)
					.HasForeignKey(d => d.BuyerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerReceiptTableBuyerTable");

				entity.HasOne(d => d.CustomerTable)
					.WithMany(p => p.BuyerReceiptTableCustomerTable)
					.HasForeignKey(d => d.CustomerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerReceiptTableCustomerTable");

				entity.HasOne(d => d.MethodOfPayment)
					.WithMany(p => p.BuyerReceiptTableMethodOfPayment)
					.HasForeignKey(d => d.MethodOfPaymentGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerReceiptTableMethodOfPayment");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.BuyerReceiptTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerReceiptTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.BuyerReceiptTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerReceiptTableBusinessUnit");
			});
			modelBuilder.Entity<BuyerTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BlacklistStatusGUID);
				entity.HasIndex(e => e.BusinessSegmentGUID);
				entity.HasIndex(e => e.BusinessSizeGUID);
				entity.HasIndex(e => e.BusinessTypeGUID);
				entity.HasIndex(e => e.BuyerTableGUID);
				entity.HasIndex(e => e.CreditScoringGUID);
				entity.HasIndex(e => e.CurrencyGUID);
				entity.HasIndex(e => e.DocumentStatusGUID);
				entity.HasIndex(e => e.KYCSetupGUID);
				entity.HasIndex(e => e.LineOfBusinessGUID);

				entity.Property(e => e.BuyerTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.BlacklistStatus)
					.WithMany(p => p.BuyerTableBlacklistStatus)
					.HasForeignKey(d => d.BlacklistStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerTableBlacklistStatus");

				entity.HasOne(d => d.BusinessSegment)
					.WithMany(p => p.BuyerTableBusinessSegment)
					.HasForeignKey(d => d.BusinessSegmentGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerTableBusinessSegment");

				entity.HasOne(d => d.BusinessSize)
					.WithMany(p => p.BuyerTableBusinessSize)
					.HasForeignKey(d => d.BusinessSizeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerTableBusinessSize");

				entity.HasOne(d => d.BusinessType)
					.WithMany(p => p.BuyerTableBusinessType)
					.HasForeignKey(d => d.BusinessTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerTableBusinessType");

				entity.HasOne(d => d.CreditScoring)
					.WithMany(p => p.BuyerTableCreditScoring)
					.HasForeignKey(d => d.CreditScoringGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerTableCreditScoring");

				entity.HasOne(d => d.Currency)
					.WithMany(p => p.BuyerTableCurrency)
					.HasForeignKey(d => d.CurrencyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerTableCurrency");

				entity.HasOne(d => d.DocumentStatus)
					.WithMany(p => p.BuyerTableDocumentStatus)
					.HasForeignKey(d => d.DocumentStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerTableDocumentStatus");

				entity.HasOne(d => d.KYCSetup)
					.WithMany(p => p.BuyerTableKYCSetup)
					.HasForeignKey(d => d.KYCSetupGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerTableKYC");

				entity.HasOne(d => d.LineOfBusiness)
					.WithMany(p => p.BuyerTableLineOfBusiness)
					.HasForeignKey(d => d.LineOfBusinessGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerTableLineOfBusiness");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.BuyerTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.BuyerTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_BuyerTableBusinessUnit");
			});
			modelBuilder.Entity<CalendarGroup>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.CalendarGroupGUID);

				entity.Property(e => e.CalendarGroupGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CalendarGroupCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CalendarGroupCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CalendarGroupBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CalendarGroupBusinessUnit");
			});
			modelBuilder.Entity<CalendarNonWorkingDate>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.CalendarGroupGUID);
				entity.HasIndex(e => e.CalendarNonWorkingDateGUID);

				entity.Property(e => e.CalendarNonWorkingDateGUID).ValueGeneratedNever();

				entity.HasOne(d => d.CalendarGroup)
					.WithMany(p => p.CalendarNonWorkingDateCalendarGroup)
					.HasForeignKey(d => d.CalendarGroupGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CalendarNonWorkingDateCalendarGroup");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CalendarNonWorkingDateCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CalendarNonWorkingDateCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CalendarNonWorkingDateBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CalendarNonWorkingDateBusinessUnit");
			});
			modelBuilder.Entity<CAReqAssignmentOutstanding>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.AssignmentAgreementTableGUID);
				entity.HasIndex(e => e.BuyerTableGUID);
				entity.HasIndex(e => e.CAReqAssignmentOutstandingGUID);
				entity.HasIndex(e => e.CreditAppRequestTableGUID);
				entity.HasIndex(e => e.CustomerTableGUID);

				entity.Property(e => e.CAReqAssignmentOutstandingGUID).ValueGeneratedNever();

				entity.HasOne(d => d.AssignmentAgreementTable)
					.WithMany(p => p.CAReqAssignmentOutstandingAssignmentAgreementTable)
					.HasForeignKey(d => d.AssignmentAgreementTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CAReqAssignmentOutstandingAssignmentAgreementTable");

				entity.HasOne(d => d.BuyerTable)
					.WithMany(p => p.CAReqAssignmentOutstandingBuyerTable)
					.HasForeignKey(d => d.BuyerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CAReqAssignmentOutstandingBuyerTable");

				entity.HasOne(d => d.CreditAppRequestTable)
					.WithMany(p => p.CAReqAssignmentOutstandingCreditAppRequestTable)
					.HasForeignKey(d => d.CreditAppRequestTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CAReqAssignmentOutstandingCreditAppRequestTable");

				entity.HasOne(d => d.CustomerTable)
					.WithMany(p => p.CAReqAssignmentOutstandingCustomerTable)
					.HasForeignKey(d => d.CustomerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CAReqAssignmentOutstandingCustomerTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CAReqAssignmentOutstandingCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CAReqAssignmentOutstandingCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CAReqAssignmentOutstandingBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CAReqAssignmentOutstandingBusinessUnit");
			});
			modelBuilder.Entity<CAReqCreditOutStanding>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.CreditAppRequestTableGUID);
				entity.HasIndex(e => e.CreditAppTableGUID);
				entity.HasIndex(e => e.CreditLimitTypeGUID);
				entity.HasIndex(e => e.CustomerTableGUID);

				entity.Property(e => e.CAReqCreditOutStandingGUID).ValueGeneratedNever();

				entity.HasOne(d => d.CreditAppRequestTable)
					.WithMany(p => p.CAReqCreditOutStandingCreditAppRequestTable)
					.HasForeignKey(d => d.CreditAppRequestTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CAReqCreditOutStandingCreditAppRequestTable");

				entity.HasOne(d => d.CreditAppTable)
					.WithMany(p => p.CAReqCreditOutStandingCreditAppTable)
					.HasForeignKey(d => d.CreditAppTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CAReqCreditOutStandingCreditAppTable");

				entity.HasOne(d => d.CreditLimitType)
					.WithMany(p => p.CAReqCreditOutStandingCreditLimitType)
					.HasForeignKey(d => d.CreditLimitTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CAReqCreditOutStandingCreditLimitType");

				entity.HasOne(d => d.CustomerTable)
					.WithMany(p => p.CAReqCreditOutStandingCustomerTable)
					.HasForeignKey(d => d.CustomerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CAReqCreditOutStandingCustomerTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CAReqCreditOutStandingCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CAReqCreditOutStandingCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CAReqCreditOutStandingBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CAReqCreditOutStandingBusinessUnit");
			});
			modelBuilder.Entity<CAReqRetentionOutstanding>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.CreditAppRequestTableGUID);
				entity.HasIndex(e => e.CreditAppTableGUID);
				entity.HasIndex(e => e.CustomerTableGUID);

				entity.Property(e => e.CAReqRetentionOutstandingGUID).ValueGeneratedNever();

				entity.HasOne(d => d.CreditAppRequestTable)
					.WithMany(p => p.CAReqRetentionOutstandingCreditAppRequestTable)
					.HasForeignKey(d => d.CreditAppRequestTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CAReqRetentionOutstandingCreditAppRequestTable");

				entity.HasOne(d => d.CreditAppTable)
					.WithMany(p => p.CAReqRetentionOutstandingCreditAppTable)
					.HasForeignKey(d => d.CreditAppTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CAReqRetentionOutstandingCreditAppTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CAReqRetentionOutstandingCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CAReqRetentionOutstandingCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CAReqRetentionOutstandingBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CAReqRetentionOutstandingBusinessUnit");
			});
			modelBuilder.Entity<ChequeTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BuyerTableGUID);
				entity.HasIndex(e => e.ChequeBankGroupGUID);
				entity.HasIndex(e => e.ChequeTableGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.DocumentStatusGUID);
				entity.HasIndex(e => e.RefGUID);
				entity.HasIndex(e => e.RefPDCGUID);

				entity.Property(e => e.ChequeTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.BuyerTable)
					.WithMany(p => p.ChequeTableBuyerTable)
					.HasForeignKey(d => d.BuyerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ChequeTableBuyerTable");

				entity.HasOne(d => d.ChequeBankGroup)
					.WithMany(p => p.ChequeTableChequeBankGroup)
					.HasForeignKey(d => d.ChequeBankGroupGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ChequeTableBankGroup");

				entity.HasOne(d => d.CustomerTable)
					.WithMany(p => p.ChequeTableCustomerTable)
					.HasForeignKey(d => d.CustomerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ChequeTableCustomerTable");

				entity.HasOne(d => d.DocumentStatus)
					.WithMany(p => p.ChequeTableDocumentStatus)
					.HasForeignKey(d => d.DocumentStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ChequeTableDocumentStatus");

				entity.HasOne(d => d.RefPDC)
					.WithMany(p => p.ChequeTableRefPDC)
					.HasForeignKey(d => d.RefPDCGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ChequeTableChequeTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.ChequeTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ChequeTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.ChequeTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ChequeTableBusinessUnit");
			});
			modelBuilder.Entity<CollectionFollowUp>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BuyerTableGUID);
				entity.HasIndex(e => e.ChequeBankGroupGUID);
				entity.HasIndex(e => e.CollectionFollowUpGUID);
				entity.HasIndex(e => e.CreditAppLineGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.MethodOfPaymentGUID);
				entity.HasIndex(e => e.RefGUID);

				entity.Property(e => e.CollectionFollowUpGUID).ValueGeneratedNever();

				entity.HasOne(d => d.BuyerTable)
					.WithMany(p => p.CollectionFollowUpBuyerTable)
					.HasForeignKey(d => d.BuyerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CollectionFollowUpBuyerTable");

				entity.HasOne(d => d.ChequeBankGroup)
					.WithMany(p => p.CollectionFollowUpChequeBankGroup)
					.HasForeignKey(d => d.ChequeBankGroupGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CollectionFollowUpBankGroup");

				entity.HasOne(d => d.CreditAppLine)
					.WithMany(p => p.CollectionFollowUpCreditAppLine)
					.HasForeignKey(d => d.CreditAppLineGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CollectionFollowUpCreditAppLine");

				entity.HasOne(d => d.CustomerTable)
					.WithMany(p => p.CollectionFollowUpCustomerTable)
					.HasForeignKey(d => d.CustomerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CollectionFollowUpCustomerTable");

				entity.HasOne(d => d.MethodOfPayment)
					.WithMany(p => p.CollectionFollowUpMethodOfPayment)
					.HasForeignKey(d => d.MethodOfPaymentGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CollectionFollowUpMethodOfPayment");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CollectionFollowUpCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CollectionFollowUpCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CollectionFollowUpBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CollectionFollowUpBusinessUnit");
			});
			modelBuilder.Entity<CollectionGroup>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.Property(e => e.CollectionGroupGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CollectionGroupCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CollectionGroupCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CollectionGroupBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CollectionGroupBusinessUnit");
			});
			modelBuilder.Entity<CompanyBank>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BankGroupGUID);
				entity.HasIndex(e => e.BankTypeGUID);
				entity.HasIndex(e => e.CompanyBankGUID);

				entity.Property(e => e.CompanyBankGUID).ValueGeneratedNever();

				entity.HasOne(d => d.BankGroup)
					.WithMany(p => p.CompanyBankBankGroup)
					.HasForeignKey(d => d.BankGroupGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CompanyBankBankGroup");

				entity.HasOne(d => d.BankType)
					.WithMany(p => p.CompanyBankBankType)
					.HasForeignKey(d => d.BankTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CompanyBankBankType");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CompanyBankCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CompanyBankCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CompanyBankBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CompanyBankBusinessUnit");
			});
			modelBuilder.Entity<CompanyParameter>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.CollectionInvTypeGUID);
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.CompulsoryInvTypeGUID);
				entity.HasIndex(e => e.CompulsoryTaxGUID);
				entity.HasIndex(e => e.DepositReturnInvTypeGUID);
				entity.HasIndex(e => e.DiscountMethodOfPaymentGUID);
				entity.HasIndex(e => e.HomeCurrencyGUID);
				entity.HasIndex(e => e.InitInvAdvInvTypeGUID);
				entity.HasIndex(e => e.InitInvDepInvTypeGUID);
				entity.HasIndex(e => e.InitInvDownInvTypeGUID);
				entity.HasIndex(e => e.InstallAdvInvTypeGUID);
				entity.HasIndex(e => e.InstallInvTypeGUID);
				entity.HasIndex(e => e.InsuranceInvTypeGUID);
				entity.HasIndex(e => e.InsuranceTaxGUID);
				entity.HasIndex(e => e.MaintenanceTaxGUID);
				entity.HasIndex(e => e.PenaltyInvTypeGUID);
				entity.HasIndex(e => e.VehicleTaxInvTypeGUID);
				entity.HasIndex(e => e.VehicleTaxServiceFeeTaxGUID);
				entity.HasIndex(e => e.WaiveMethodOfPaymentGUID);
				entity.HasIndex(e => e.EarlyToleranceMethodOfPaymentGUID);
				entity.HasIndex(e => e.OperReportSignatureGUID);

				entity.Property(e => e.CompanyParameterGUID).ValueGeneratedNever();

				entity.HasIndex(e => e.EarlyToleranceMethodOfPaymentGUID);

				entity.HasOne(d => d.CollectionInvType)
					.WithMany(p => p.CompanyParameterCollectionInvType)
					.HasForeignKey(d => d.CollectionInvTypeGUID)
					.HasConstraintName("FK_CompanyParameterCollectionInvType");

				entity.HasOne(d => d.CompulsoryInvType)
					.WithMany(p => p.CompanyParameterCompulsoryInvType)
					.HasForeignKey(d => d.CompulsoryInvTypeGUID)
					.HasConstraintName("FK_CompanyParameterCompulsoryInvType");

				entity.HasOne(d => d.CompulsoryTax)
					.WithMany(p => p.CompanyParameterCompulsoryTax)
					.HasForeignKey(d => d.CompulsoryTaxGUID)
					.HasConstraintName("FK_CompanyParameterCompulsoryTax");

				entity.HasOne(d => d.DepositReturnInvType)
					.WithMany(p => p.CompanyParameterDepositReturnInvType)
					.HasForeignKey(d => d.DepositReturnInvTypeGUID)
					.HasConstraintName("FK_CompanyParameterDepositReturnInvType");

				entity.HasOne(d => d.DiscountMethodOfPayment)
					.WithMany(p => p.CompanyParameterDiscountMethodOfPayment)
					.HasForeignKey(d => d.DiscountMethodOfPaymentGUID)
					.HasConstraintName("FK_CompanyParameterDiscountMethodOfPayment");

				entity.HasOne(d => d.EarlyPayOffToleranceInvType)
					.WithMany(p => p.CompanyParameterEarlyPayOffToleranceInvType)
					.HasForeignKey(d => d.EarlyPayOffToleranceInvTypeGUID)
					.HasConstraintName("FK_CompanyParameterEarlyPayOffToleranceInvType");

				entity.HasOne(d => d.HomeCurrency)
					.WithMany(p => p.CompanyParameterHomeCurrency)
					.HasForeignKey(d => d.HomeCurrencyGUID)
					.HasConstraintName("FK_CompanyParameterCurrency");

				entity.HasOne(d => d.InitInvAdvInvType)
					.WithMany(p => p.CompanyParameterInitInvAdvInvType)
					.HasForeignKey(d => d.InitInvAdvInvTypeGUID)
					.HasConstraintName("FK_CompanyParameterInitInvAdvInvType");

				entity.HasOne(d => d.InitInvDepInvType)
					.WithMany(p => p.CompanyParameterInitInvDepInvType)
					.HasForeignKey(d => d.InitInvDepInvTypeGUID)
					.HasConstraintName("FK_CompanyParameterInitInvDepInvType");

				entity.HasOne(d => d.InitInvDownInvType)
					.WithMany(p => p.CompanyParameterInitInvDownInvType)
					.HasForeignKey(d => d.InitInvDownInvTypeGUID)
					.HasConstraintName("FK_CompanyParameterInitInvDownInvType");

				entity.HasOne(d => d.InstallAdvInvType)
					.WithMany(p => p.CompanyParameterInstallAdvInvType)
					.HasForeignKey(d => d.InstallAdvInvTypeGUID)
					.HasConstraintName("FK_CompanyParameterInstallAdvInvType");

				entity.HasOne(d => d.InstallInvType)
					.WithMany(p => p.CompanyParameterInstallInvType)
					.HasForeignKey(d => d.InstallInvTypeGUID)
					.HasConstraintName("FK_CompanyParameterInstallInvType");

				entity.HasOne(d => d.InsuranceInvType)
					.WithMany(p => p.CompanyParameterInsuranceInvType)
					.HasForeignKey(d => d.InsuranceInvTypeGUID)
					.HasConstraintName("FK_CompanyParameterInsuranceInvType");

				entity.HasOne(d => d.InsuranceTax)
					.WithMany(p => p.CompanyParameterInsuranceTax)
					.HasForeignKey(d => d.InsuranceTaxGUID)
					.HasConstraintName("FK_CompanyParameterInsuranceTax");

				entity.HasOne(d => d.MaintenanceTax)
					.WithMany(p => p.CompanyParameterMaintenanceTax)
					.HasForeignKey(d => d.MaintenanceTaxGUID)
					.HasConstraintName("FK_CompanyParameterMaintenanceTax");

				entity.HasOne(d => d.PenaltyInvType)
					.WithMany(p => p.CompanyParameterPenaltyInvType)
					.HasForeignKey(d => d.PenaltyInvTypeGUID)
					.HasConstraintName("FK_CompanyParameterPenaltyInvType");

				entity.HasOne(d => d.VehicleTaxInvType)
					.WithMany(p => p.CompanyParameterVehicleTaxInvType)
					.HasForeignKey(d => d.VehicleTaxInvTypeGUID)
					.HasConstraintName("FK_CompanyParameterVehicleTaxInvType");

				entity.HasOne(d => d.VehicleTaxServiceFeeTax)
					.WithMany(p => p.CompanyParameterVehicleTaxServiceFeeTax)
					.HasForeignKey(d => d.VehicleTaxServiceFeeTaxGUID)
					.HasConstraintName("FK_CompanyParameterVehicleTaxServiceFeeTax");

				entity.HasOne(d => d.WaiveMethodOfPayment)
					.WithMany(p => p.CompanyParameterWaiveMethodOfPayment)
					.HasForeignKey(d => d.WaiveMethodOfPaymentGUID)
					.HasConstraintName("FK_CompanyParameterWaiveMethodOfPayment");

				entity.HasOne(d => d.EarlyToleranceMethodOfPayment)
					.WithMany(p => p.CompanyParameterEarlyToleranceMethodOfPayment)
					.HasForeignKey(d => d.EarlyToleranceMethodOfPaymentGUID)
					.HasConstraintName("FK_CompanyParameterEarlyToleranceMethodOfPayment");

				//LIT
				entity.HasOne(d => d.CompanyCalendarGroup)
					.WithMany(p => p.CompanyParameterCompanyCalendarGroup)
					.HasForeignKey(d => d.CompanyCalendarGroupGUID)
					.HasConstraintName("FK_CompanyParameterCompanyCalendarGroup");

				entity.HasOne(d => d.BOTCalendarGroup)
					.WithMany(p => p.CompanyParameterBOTCalendarGroup)
					.HasForeignKey(d => d.BOTCalendarGroupGUID)
					.HasConstraintName("FK_CompanyParameterBOTCalendarGroup");

				entity.HasOne(d => d.CreditReqInvRevenueType)
					.WithMany(p => p.CompanyParameterCreditReqInvRevenueType)
					.HasForeignKey(d => d.CreditReqInvRevenueTypeGUID)
					.HasConstraintName("FK_CompanyParameterCreditReqInvRevenueType");

				entity.HasOne(d => d.ServiceFeeInvType)
					.WithMany(p => p.CompanyParameterServiceFeeInvType)
					.HasForeignKey(d => d.ServiceFeeInvTypeGUID)
					.HasConstraintName("FK_CompanyParameterServiceFeeInvType");

				entity.HasOne(d => d.SettlementMethodOfPayment)
					.WithMany(p => p.CompanyParameterSettlementMethodOfPayment)
					.HasForeignKey(d => d.SettlementMethodOfPaymentGUID)
					.HasConstraintName("FK_CompanyParameterSettlementMethodOfPayment");

				entity.HasOne(d => d.SuspenseMethodOfPayment)
					.WithMany(p => p.CompanyParameterSuspenseMethodOfPayment)
					.HasForeignKey(d => d.SuspenseMethodOfPaymentGUID)
					.HasConstraintName("FK_CompanyParameterSuspenseMethodOfPayment");

				entity.HasOne(d => d.FTBillingFeeInvRevenueType)
					.WithMany(p => p.CompanyParameterFTBillingFeeInvRevenueType)
					.HasForeignKey(d => d.FTBillingFeeInvRevenueTypeGUID)
					.HasConstraintName("FK_CompanyParameterFTBillingFeeInvRevenueType");

				entity.HasOne(d => d.FTInterestInvType)
					.WithMany(p => p.CompanyParameterFTInterestInvType)
					.HasForeignKey(d => d.FTInterestInvTypeGUID)
					.HasConstraintName("FK_CompanyParameterFTInterestInvType");

				entity.HasOne(d => d.FTInterestRefundInvType)
					.WithMany(p => p.CompanyParameterFTInterestRefundInvType)
					.HasForeignKey(d => d.FTInterestRefundInvTypeGUID)
					.HasConstraintName("FK_CompanyParameterFTInterestRefundInvType");

				entity.HasOne(d => d.FTFeeInvType)
					.WithMany(p => p.CompanyParameterFTFeeInvType)
					.HasForeignKey(d => d.FTFeeInvTypeGUID)
					.HasConstraintName("FK_CompanyParameterFTFeeInvType");

				entity.HasOne(d => d.FTInvType)
					.WithMany(p => p.CompanyParameterFTInvType)
					.HasForeignKey(d => d.FTInvTypeGUID)
					.HasConstraintName("FK_CompanyParameterFTInvType");

				entity.HasOne(d => d.FTReceiptFeeInvRevenueType)
					.WithMany(p => p.CompanyParameterFTReceiptFeeInvRevenueType)
					.HasForeignKey(d => d.FTReceiptFeeInvRevenueTypeGUID)
					.HasConstraintName("FK_CompanyParameterFTReceiptFeeInvRevenueType");

				entity.HasOne(d => d.FTReserveInvRevenueType)
					.WithMany(p => p.CompanyParameterFTReserveInvRevenueType)
					.HasForeignKey(d => d.FTReserveInvRevenueTypeGUID)
					.HasConstraintName("FK_CompanyParameterFTReserveInvRevenueType");

				entity.HasOne(d => d.FTReserveToBeRefundedInvType)
					.WithMany(p => p.CompanyParameterFTReserveToBeRefundedInvType)
					.HasForeignKey(d => d.FTReserveToBeRefundedInvTypeGUID)
					.HasConstraintName("FK_CompanyParameterFTReserveToBeRefundedInvType");

				entity.HasOne(d => d.FTRetentionInvType)
					.WithMany(p => p.CompanyParameterFTRetentionInvType)
					.HasForeignKey(d => d.FTRetentionInvTypeGUID)
					.HasConstraintName("FK_CompanyParameterFTRetentionInvType");

				entity.HasOne(d => d.FTUnearnedInterestInvType)
					.WithMany(p => p.CompanyParameterFTUnearnedInterestInvType)
					.HasForeignKey(d => d.FTUnearnedInterestInvTypeGUID)
					.HasConstraintName("FK_CompanyParameterFTUnearnedInterestInvType");

				entity.HasOne(d => d.PFInvType)
					.WithMany(p => p.CompanyParameterPFInvType)
					.HasForeignKey(d => d.PFInvTypeGUID)
					.HasConstraintName("FK_CompanyParameterPFInvType");

				entity.HasOne(d => d.PFUnearnedInterestInvType)
					.WithMany(p => p.CompanyParameterPFUnearnedInterestInvType)
					.HasForeignKey(d => d.PFUnearnedInterestInvTypeGUID)
					.HasConstraintName("FK_CompanyParameterPFUnearnedInterestInvType");

				entity.HasOne(d => d.PFInterestInvType)
					.WithMany(p => p.CompanyParameterPFInterestInvType)
					.HasForeignKey(d => d.PFInterestInvTypeGUID)
					.HasConstraintName("FK_CompanyParameterPFInterestInvType");

				entity.HasOne(d => d.PFInterestRefundInvType)
					.WithMany(p => p.CompanyParameterPFInterestRefundInvType)
					.HasForeignKey(d => d.PFInterestRefundInvTypeGUID)
					.HasConstraintName("FK_CompanyParameterPFInterestRefundInvType");

				entity.HasOne(d => d.PFRetentionInvType)
					.WithMany(p => p.CompanyParameterPFRetentionInvType)
					.HasForeignKey(d => d.PFRetentionInvTypeGUID)
					.HasConstraintName("FK_CompanyParameterPFRetentionInvType");

				entity.HasOne(d => d.PFExtendCreditTerm)
					.WithMany(p => p.CompanyParameterPFExtendCreditTerm)
					.HasForeignKey(d => d.PFExtendCreditTermGUID)
					.HasConstraintName("FK_CompanyParameterPFExtendCreditTerm");

				entity.HasOne(d => d.PFTermExtensionInvRevenueType)
					.WithMany(p => p.CompanyParameterPFTermExtensionInvRevenueType)
					.HasForeignKey(d => d.PFTermExtensionInvRevenueTypeGUID)
					.HasConstraintName("FK_CompanyParameterPFTermExtensionInvRevenueType");

				entity.HasOne(d => d.BondRetentionInvType)
					.WithMany(p => p.CompanyParameterBondRetentionInvType)
					.HasForeignKey(d => d.BondRetentionInvTypeGUID)
					.HasConstraintName("FK_CompanyParameterBondRetentionInvType");

				entity.HasOne(d => d.LCRetentionInvType)
					.WithMany(p => p.CompanyParameterLCRetentionInvType)
					.HasForeignKey(d => d.LCRetentionInvTypeGUID)
					.HasConstraintName("FK_CompanyParameterLCRetentionInvType");

				entity.HasOne(d => d.OperReportSignature)
					.WithMany(p => p.CompanyParameterOperReportSignature)
					.HasForeignKey(d => d.OperReportSignatureGUID)
					.HasConstraintName("FK_CompanyParameterOperReportSignature");
				//LIT

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CompanyParameterCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CompanyParameterCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CompanyParameterBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CompanyParameterBusinessUnit");
			});
			modelBuilder.Entity<CompanySignature>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BranchGUID);
				entity.HasIndex(e => e.EmployeeTableGUID);

				entity.Property(e => e.CompanySignatureGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Branch)
					.WithMany(p => p.CompanySignatureBranch)
					.HasForeignKey(d => d.BranchGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CompanySignatureBranch");

				entity.HasOne(d => d.EmployeeTable)
					.WithMany(p => p.CompanySignatureEmployeeTable)
					.HasForeignKey(d => d.EmployeeTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CompanySignatureEmployeeTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CompanySignatureCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CompanySignatureCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CompanySignatureBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CompanySignatureBusinessUnit");
			});
			modelBuilder.Entity<ConsortiumLine>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.AuthorizedPersonTypeGUID);
				entity.HasIndex(e => e.ConsortiumLineGUID);
				entity.HasIndex(e => e.ConsortiumTableGUID);

				entity.Property(e => e.ConsortiumLineGUID).ValueGeneratedNever();

				entity.HasOne(d => d.AuthorizedPersonType)
					.WithMany(p => p.ConsortiumLineAuthorizedPersonType)
					.HasForeignKey(d => d.AuthorizedPersonTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ConsortiumLineAuthorizedPersonType");

				entity.HasOne(d => d.ConsortiumTable)
					.WithMany(p => p.ConsortiumLineConsortiumTable)
					.HasForeignKey(d => d.ConsortiumTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ConsortiumLineConsortiumTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.ConsortiumLineCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ConsortiumLineCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.ConsortiumLineBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ConsortiumLineBusinessUnit");
			});
			modelBuilder.Entity<ConsortiumTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.ConsortiumTableGUID);
				entity.HasIndex(e => e.DocumentStatusGUID);

				entity.Property(e => e.ConsortiumTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.DocumentStatus)
					.WithMany(p => p.ConsortiumTableDocumentStatus)
					.HasForeignKey(d => d.DocumentStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ConsortiumTableDocumentStatus");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.ConsortiumTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ConsortiumTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.ConsortiumTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ConsortiumTableBusinessUnit");
			});
			modelBuilder.Entity<ConsortiumTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.AuthorizedPersonTypeGUID);
				entity.HasIndex(e => e.ConsortiumLineGUID);
				entity.HasIndex(e => e.ConsortiumTransGUID);
				entity.HasIndex(e => e.RefGUID);

				entity.Property(e => e.ConsortiumTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.AuthorizedPersonType)
					.WithMany(p => p.ConsortiumTransAuthorizedPersonType)
					.HasForeignKey(d => d.AuthorizedPersonTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ConsortiumTransAuthorizedPersonType");

				entity.HasOne(d => d.ConsortiumLine)
					.WithMany(p => p.ConsortiumTransConsortiumLine)
					.HasForeignKey(d => d.ConsortiumLineGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ConsortiumTransConsortiumLine");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.ConsortiumTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ConsortiumTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.ConsortiumTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ConsortiumTransBusinessUnit");
			});
			modelBuilder.Entity<ContactPersonTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.ContactPersonTransGUID);
				entity.HasIndex(e => e.RefGUID);
				entity.HasIndex(e => e.RelatedPersonTableGUID);

				entity.Property(e => e.ContactPersonTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.RelatedPersonTable)
					.WithMany(p => p.ContactPersonTransRelatedPersonTable)
					.HasForeignKey(d => d.RelatedPersonTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ContactPersonTransRelatedPersonTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.ContactPersonTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ContactPersonTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.ContactPersonTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ContactPersonTransBusinessUnit");
			});
			modelBuilder.Entity<ContactTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.ContactTransGUID);
				entity.HasIndex(e => e.RefGUID);

				entity.Property(e => e.ContactTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.ContactTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ContactTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.ContactTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ContactTransBusinessUnit");
			});
			modelBuilder.Entity<CreditAppLine>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.AssignmentAgreementTableGUID);
				entity.HasIndex(e => e.AssignmentMethodGUID);
				entity.HasIndex(e => e.BillingAddressGUID);
				entity.HasIndex(e => e.BillingContactPersonGUID);
				entity.HasIndex(e => e.BillingResponsibleByGUID);
				entity.HasIndex(e => e.BuyerTableGUID);
				entity.HasIndex(e => e.CreditAppLineGUID);
				entity.HasIndex(e => e.CreditAppTableGUID);
				entity.HasIndex(e => e.CreditTermGUID);
				entity.HasIndex(e => e.InvoiceAddressGUID);
				entity.HasIndex(e => e.MailingReceiptAddressGUID);
				entity.HasIndex(e => e.MethodOfPaymentGUID);
				entity.HasIndex(e => e.ReceiptAddressGUID);
				entity.HasIndex(e => e.ReceiptContactPersonGUID);
				entity.HasIndex(e => e.RefCreditAppRequestLineGUID);

				entity.Property(e => e.CreditAppLineGUID).ValueGeneratedNever();

				entity.HasOne(d => d.AssignmentAgreementTable)
					.WithMany(p => p.CreditAppLineAssignmentAgreementTable)
					.HasForeignKey(d => d.AssignmentAgreementTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppLineAssignmentAgreementTable");

				entity.HasOne(d => d.AssignmentMethod)
					.WithMany(p => p.CreditAppLineAssignmentMethod)
					.HasForeignKey(d => d.AssignmentMethodGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppLineAssignmentMethod");

				entity.HasOne(d => d.BillingAddress)
					.WithMany(p => p.CreditAppLineBillingAddress)
					.HasForeignKey(d => d.BillingAddressGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppLineBillingAddress");

				entity.HasOne(d => d.BillingContactPerson)
					.WithMany(p => p.CreditAppLineBillingContactPerson)
					.HasForeignKey(d => d.BillingContactPersonGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppLineBillingContactPerson");

				entity.HasOne(d => d.BillingResponsibleBy)
					.WithMany(p => p.CreditAppLineBillingResponsibleBy)
					.HasForeignKey(d => d.BillingResponsibleByGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppLineBillingResponsibleBy");

				entity.HasOne(d => d.BuyerTable)
					.WithMany(p => p.CreditAppLineBuyerTable)
					.HasForeignKey(d => d.BuyerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppLineBuyerTable");

				entity.HasOne(d => d.CreditAppTable)
					.WithMany(p => p.CreditAppLineCreditAppTable)
					.HasForeignKey(d => d.CreditAppTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppLineCreditAppTable");

				entity.HasOne(d => d.CreditTerm)
					.WithMany(p => p.CreditAppLineCreditTerm)
					.HasForeignKey(d => d.CreditTermGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppLineCreditTerm");

				entity.HasOne(d => d.InvoiceAddress)
					.WithMany(p => p.CreditAppLineInvoiceAddress)
					.HasForeignKey(d => d.InvoiceAddressGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppLineInvoiceAddress");

				entity.HasOne(d => d.MailingReceiptAddress)
					.WithMany(p => p.CreditAppLineMailingReceiptAddress)
					.HasForeignKey(d => d.MailingReceiptAddressGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppLineMailingReceiptAddress");

				entity.HasOne(d => d.MethodOfPayment)
					.WithMany(p => p.CreditAppLineMethodOfPayment)
					.HasForeignKey(d => d.MethodOfPaymentGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppLineMethodOfPayment");

				entity.HasOne(d => d.ReceiptAddress)
					.WithMany(p => p.CreditAppLineReceiptAddress)
					.HasForeignKey(d => d.ReceiptAddressGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppLineReceiptAddress");

				entity.HasOne(d => d.ReceiptContactPerson)
					.WithMany(p => p.CreditAppLineReceiptContactPerson)
					.HasForeignKey(d => d.ReceiptContactPersonGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppLineReceiptContactPerson");

				entity.HasOne(d => d.RefCreditAppRequestLine)
					.WithMany(p => p.CreditAppLineRefCreditAppRequestLine)
					.HasForeignKey(d => d.RefCreditAppRequestLineGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppLineRefCreditAppRequestLine");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CreditAppLineCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppLineCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CreditAppLineBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppLineBusinessUnit");
			});
			modelBuilder.Entity<CreditAppReqAssignment>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.AssignmentAgreementTableGUID);
				entity.HasIndex(e => e.AssignmentMethodGUID);
				entity.HasIndex(e => e.BuyerAgreementTableGUID);
				entity.HasIndex(e => e.BuyerTableGUID);
				entity.HasIndex(e => e.CreditAppReqAssignmentGUID);
				entity.HasIndex(e => e.CreditAppRequestTableGUID);
				entity.HasIndex(e => e.CustomerTableGUID);

				entity.Property(e => e.CreditAppReqAssignmentGUID).ValueGeneratedNever();

				entity.HasOne(d => d.AssignmentAgreementTable)
					.WithMany(p => p.CreditAppReqAssignmentAssignmentAgreementTable)
					.HasForeignKey(d => d.AssignmentAgreementTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppReqAssignmentAssignmentAgreementTable");

				entity.HasOne(d => d.AssignmentMethod)
					.WithMany(p => p.CreditAppReqAssignmentAssignmentMethod)
					.HasForeignKey(d => d.AssignmentMethodGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppReqAssignmentAssignmentMethod");

				entity.HasOne(d => d.BuyerAgreementTable)
					.WithMany(p => p.CreditAppReqAssignmentBuyerAgreementTable)
					.HasForeignKey(d => d.BuyerAgreementTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppReqAssignmentBuyerAgreementTable");

				entity.HasOne(d => d.BuyerTable)
					.WithMany(p => p.CreditAppReqAssignmentBuyerTable)
					.HasForeignKey(d => d.BuyerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppReqAssignmentBuyerTable");

				entity.HasOne(d => d.CreditAppRequestTable)
					.WithMany(p => p.CreditAppReqAssignmentCreditAppRequestTable)
					.HasForeignKey(d => d.CreditAppRequestTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppReqAssignmentCreditAppRequestTable");

				entity.HasOne(d => d.CustomerTable)
					.WithMany(p => p.CreditAppReqAssignmentCustomerTable)
					.HasForeignKey(d => d.CustomerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppReqAssignmentCustomerTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CreditAppReqAssignmentCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppReqAssignmentCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CreditAppReqAssignmentBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppReqAssignmentBusinessUnit");
			});
			modelBuilder.Entity<CreditAppReqBusinessCollateral>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BankGroupGUID);
				entity.HasIndex(e => e.BankTypeGUID);
				entity.HasIndex(e => e.BusinessCollateralSubTypeGUID);
				entity.HasIndex(e => e.BusinessCollateralTypeGUID);
				entity.HasIndex(e => e.BuyerTableGUID);
				entity.HasIndex(e => e.CreditAppReqBusinessCollateralGUID);
				entity.HasIndex(e => e.CreditAppRequestTableGUID);
				entity.HasIndex(e => e.CustBusinessCollateralGUID);
				entity.HasIndex(e => e.CustomerTableGUID);

				entity.Property(e => e.CreditAppReqBusinessCollateralGUID).ValueGeneratedNever();

				entity.HasOne(d => d.BankGroup)
					.WithMany(p => p.CreditAppReqBusinessCollateralBankGroup)
					.HasForeignKey(d => d.BankGroupGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppReqBusinessCollateralBankGroup");

				entity.HasOne(d => d.BankType)
					.WithMany(p => p.CreditAppReqBusinessCollateralBankType)
					.HasForeignKey(d => d.BankTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppReqBusinessCollateralBankType");

				entity.HasOne(d => d.BusinessCollateralSubType)
					.WithMany(p => p.CreditAppReqBusinessCollateralBusinessCollateralSubType)
					.HasForeignKey(d => d.BusinessCollateralSubTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppReqBusinessCollateralBusinessCollateralSubType");

				entity.HasOne(d => d.BusinessCollateralType)
					.WithMany(p => p.CreditAppReqBusinessCollateralBusinessCollateralType)
					.HasForeignKey(d => d.BusinessCollateralTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppReqBusinessCollateralBusinessCollateralType");

				entity.HasOne(d => d.BuyerTable)
					.WithMany(p => p.CreditAppReqBusinessCollateralBuyerTable)
					.HasForeignKey(d => d.BuyerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppReqBusinessCollateralBuyerTable");

				entity.HasOne(d => d.CreditAppRequestTable)
					.WithMany(p => p.CreditAppReqBusinessCollateralCreditAppRequestTable)
					.HasForeignKey(d => d.CreditAppRequestTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppReqBusinessCollateralCreditAppRequestTable");

				entity.HasOne(d => d.CustBusinessCollateral)
					.WithMany(p => p.CreditAppReqBusinessCollateralCustBusinessCollateral)
					.HasForeignKey(d => d.CustBusinessCollateralGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppReqBusinessCollateralCustBusinessCollateral");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CreditAppReqBusinessCollateralCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppReqBusinessCollateralCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CreditAppReqBusinessCollateralBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppReqBusinessCollateralBusinessUnit");
			});
			modelBuilder.Entity<CreditAppRequestLine>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.AssignmentAgreementTableGUID);
				entity.HasIndex(e => e.AssignmentMethodGUID);
				entity.HasIndex(e => e.BillingAddressGUID);
				entity.HasIndex(e => e.BillingContactPersonGUID);
				entity.HasIndex(e => e.BillingResponsibleByGUID);
				entity.HasIndex(e => e.BlacklistStatusGUID);
				entity.HasIndex(e => e.BuyerCreditTermGUID);
				entity.HasIndex(e => e.BuyerTableGUID);
				entity.HasIndex(e => e.CreditAppRequestLineGUID);
				entity.HasIndex(e => e.CreditAppRequestTableGUID);
				entity.HasIndex(e => e.CreditScoringGUID);
				entity.HasIndex(e => e.InvoiceAddressGUID);
				entity.HasIndex(e => e.KYCSetupGUID);
				entity.HasIndex(e => e.MailingReceiptAddressGUID);
				entity.HasIndex(e => e.MethodOfPaymentGUID);
				entity.HasIndex(e => e.ReceiptAddressGUID);
				entity.HasIndex(e => e.ReceiptContactPersonGUID);
				entity.HasIndex(e => e.RefCreditAppLineGUID);

				entity.Property(e => e.CreditAppRequestLineGUID).ValueGeneratedNever();

				entity.HasOne(d => d.AssignmentAgreementTable)
					.WithMany(p => p.CreditAppRequestLineAssignmentAgreementTable)
					.HasForeignKey(d => d.AssignmentAgreementTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestLineAssignmentAgreementTable");

				entity.HasOne(d => d.AssignmentMethod)
					.WithMany(p => p.CreditAppRequestLineAssignmentMethod)
					.HasForeignKey(d => d.AssignmentMethodGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestLineAssignmentMethod");

				entity.HasOne(d => d.BillingAddress)
					.WithMany(p => p.CreditAppRequestLineBillingAddress)
					.HasForeignKey(d => d.BillingAddressGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestLineBillingAddress");

				entity.HasOne(d => d.BillingContactPerson)
					.WithMany(p => p.CreditAppRequestLineBillingContactPerson)
					.HasForeignKey(d => d.BillingContactPersonGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestLineBillingContactPerson");

				entity.HasOne(d => d.BillingResponsibleBy)
					.WithMany(p => p.CreditAppRequestLineBillingResponsibleBy)
					.HasForeignKey(d => d.BillingResponsibleByGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestLineBillingResponsibleBy");

				entity.HasOne(d => d.BlacklistStatus)
					.WithMany(p => p.CreditAppRequestLineBlacklistStatus)
					.HasForeignKey(d => d.BlacklistStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestLineBlacklistStatus");

				entity.HasOne(d => d.BuyerCreditTerm)
					.WithMany(p => p.CreditAppRequestLineBuyerCreditTerm)
					.HasForeignKey(d => d.BuyerCreditTermGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestLineCreditTerm");

				entity.HasOne(d => d.BuyerTable)
					.WithMany(p => p.CreditAppRequestLineBuyerTable)
					.HasForeignKey(d => d.BuyerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestLineBuyerTable");

				entity.HasOne(d => d.CreditAppRequestTable)
					.WithMany(p => p.CreditAppRequestLineCreditAppRequestTable)
					.HasForeignKey(d => d.CreditAppRequestTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestLineCreditAppRequestTable");

				entity.HasOne(d => d.CreditScoring)
					.WithMany(p => p.CreditAppRequestLineCreditScoring)
					.HasForeignKey(d => d.CreditScoringGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestLineCreditScoring");

				entity.HasOne(d => d.InvoiceAddress)
					.WithMany(p => p.CreditAppRequestLineInvoiceAddress)
					.HasForeignKey(d => d.InvoiceAddressGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestLineInvoiceAddress");

				entity.HasOne(d => d.KYCSetup)
					.WithMany(p => p.CreditAppRequestLineKYCSetup)
					.HasForeignKey(d => d.KYCSetupGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestLineKYC");

				entity.HasOne(d => d.MailingReceiptAddress)
					.WithMany(p => p.CreditAppRequestLineMailingReceiptAddress)
					.HasForeignKey(d => d.MailingReceiptAddressGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestLineMailingReceiptAddress");

				entity.HasOne(d => d.MethodOfPayment)
					.WithMany(p => p.CreditAppRequestLineMethodOfPayment)
					.HasForeignKey(d => d.MethodOfPaymentGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestLineMethodOfPayment");

				entity.HasOne(d => d.ReceiptAddress)
					.WithMany(p => p.CreditAppRequestLineReceiptAddress)
					.HasForeignKey(d => d.ReceiptAddressGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestLineReceiptAddress");

				entity.HasOne(d => d.ReceiptContactPerson)
					.WithMany(p => p.CreditAppRequestLineReceiptContactPerson)
					.HasForeignKey(d => d.ReceiptContactPersonGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestLineReceiptContactPerson");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CreditAppRequestLineCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestLineCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CreditAppRequestLineBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestLineBusinessUnit");

			});
			modelBuilder.Entity<CreditAppRequestLineAmend>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.CreditAppRequestLineAmendGUID);
				entity.HasIndex(e => e.CreditAppRequestLineGUID);
				entity.HasIndex(e => e.OriginalAssignmentMethodGUID);
				entity.HasIndex(e => e.OriginalBillingAddressGUID);
				entity.HasIndex(e => e.OriginalMethodOfPaymentGUID);
				entity.HasIndex(e => e.OriginalReceiptAddressGUID);

				entity.Property(e => e.CreditAppRequestLineAmendGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CreditAppRequestLineAmendCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestLineAmendCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CreditAppRequestLineAmendBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestLineAmendBusinessUnit");

			});
			modelBuilder.Entity<CreditAppRequestTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.ApplicationTableGUID);
				entity.HasIndex(e => e.AssignmentMethodGUID);
				entity.HasIndex(e => e.BankAccountControlGUID);
				entity.HasIndex(e => e.BillingAddressGUID);
				entity.HasIndex(e => e.BillingContactPersonGUID);
				entity.HasIndex(e => e.BlacklistStatusGUID);
				entity.HasIndex(e => e.ConsortiumTableGUID);
				entity.HasIndex(e => e.CreditAppRequestTableGUID);
				entity.HasIndex(e => e.CreditLimitTypeGUID);
				entity.HasIndex(e => e.CreditScoringGUID);
				entity.HasIndex(e => e.CreditTermGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.Dimension1GUID);
				entity.HasIndex(e => e.Dimension2GUID);
				entity.HasIndex(e => e.Dimension3GUID);
				entity.HasIndex(e => e.Dimension4GUID);
				entity.HasIndex(e => e.Dimension5GUID);
				entity.HasIndex(e => e.DocumentReasonGUID);
				entity.HasIndex(e => e.DocumentStatusGUID);
				entity.HasIndex(e => e.ExtensionServiceFeeCondTemplateGUID);
				entity.HasIndex(e => e.InterestTypeGUID);
				entity.HasIndex(e => e.InvoiceAddressGUID);
				entity.HasIndex(e => e.KYCGUID);
				entity.HasIndex(e => e.MailingReceiptAddressGUID);
				entity.HasIndex(e => e.PDCBankGUID);
				entity.HasIndex(e => e.ProductSubTypeGUID);
				entity.HasIndex(e => e.ReceiptAddressGUID);
				entity.HasIndex(e => e.ReceiptContactPersonGUID);
				entity.HasIndex(e => e.RefCreditAppTableGUID);
				entity.HasIndex(e => e.RegisteredAddressGUID);

				entity.Property(e => e.CreditAppRequestTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.ApplicationTable)
					.WithMany(p => p.CreditAppRequestTableApplicationTable)
					.HasForeignKey(d => d.ApplicationTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableApplicationTable");

				entity.HasOne(d => d.AssignmentMethod)
					.WithMany(p => p.CreditAppRequestTableAssignmentMethod)
					.HasForeignKey(d => d.AssignmentMethodGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableAssignmentMethod");

				entity.HasOne(d => d.BankAccountControl)
					.WithMany(p => p.CreditAppRequestTableBankAccountControl)
					.HasForeignKey(d => d.BankAccountControlGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableBankAccountControl");

				entity.HasOne(d => d.BillingAddress)
					.WithMany(p => p.CreditAppRequestTableBillingAddress)
					.HasForeignKey(d => d.BillingAddressGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableBillingAddress");

				entity.HasOne(d => d.BillingContactPerson)
					.WithMany(p => p.CreditAppRequestTableBillingContactPerson)
					.HasForeignKey(d => d.BillingContactPersonGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableBillingContactPerson");

				entity.HasOne(d => d.BlacklistStatus)
					.WithMany(p => p.CreditAppRequestTableBlacklistStatus)
					.HasForeignKey(d => d.BlacklistStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableBlacklistStatus");

				entity.HasOne(d => d.ConsortiumTable)
					.WithMany(p => p.CreditAppRequestTableConsortiumTable)
					.HasForeignKey(d => d.ConsortiumTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableConsortiumTable");

				entity.HasOne(d => d.CreditLimitType)
					.WithMany(p => p.CreditAppRequestTableCreditLimitType)
					.HasForeignKey(d => d.CreditLimitTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableCreditLimitType");

				entity.HasOne(d => d.CreditScoring)
					.WithMany(p => p.CreditAppRequestTableCreditScoring)
					.HasForeignKey(d => d.CreditScoringGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableCreditScoring");

				entity.HasOne(d => d.CreditTerm)
					.WithMany(p => p.CreditAppRequestTableCreditTerm)
					.HasForeignKey(d => d.CreditTermGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableCreditTerm");

				entity.HasOne(d => d.LedgerDimension1)
					.WithMany(p => p.CreditAppRequestTableDimension1)
					.HasForeignKey(d => d.Dimension1GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableLedgerDimension1");

				entity.HasOne(d => d.LedgerDimension2)
					.WithMany(p => p.CreditAppRequestTableDimension2)
					.HasForeignKey(d => d.Dimension2GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableLedgerDimension2");

				entity.HasOne(d => d.LedgerDimension3)
					.WithMany(p => p.CreditAppRequestTableDimension3)
					.HasForeignKey(d => d.Dimension3GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableLedgerDimension3");

				entity.HasOne(d => d.LedgerDimension4)
					.WithMany(p => p.CreditAppRequestTableDimension4)
					.HasForeignKey(d => d.Dimension4GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableLedgerDimension4");

				entity.HasOne(d => d.LedgerDimension5)
					.WithMany(p => p.CreditAppRequestTableDimension5)
					.HasForeignKey(d => d.Dimension5GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableLedgerDimension5");

				entity.HasOne(d => d.DocumentReason)
					.WithMany(p => p.CreditAppRequestTableDocumentReason)
					.HasForeignKey(d => d.DocumentReasonGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableDocumentReason");

				entity.HasOne(d => d.DocumentStatus)
					.WithMany(p => p.CreditAppRequestTableDocumentStatus)
					.HasForeignKey(d => d.DocumentStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableDocumentStatus");

				entity.HasOne(d => d.ExtensionServiceFeeCondTemplate)
					.WithMany(p => p.CreditAppRequestTableExtensionServiceFeeCondTemplate)
					.HasForeignKey(d => d.ExtensionServiceFeeCondTemplateGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableServiceFeeCondTemplateTable");

				entity.HasOne(d => d.InterestType)
					.WithMany(p => p.CreditAppRequestTableInterestType)
					.HasForeignKey(d => d.InterestTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableInterestType");

				entity.HasOne(d => d.InvoiceAddress)
					.WithMany(p => p.CreditAppRequestTableInvoiceAddress)
					.HasForeignKey(d => d.InvoiceAddressGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableInvoiceAddress");

				entity.HasOne(d => d.KYCSetup)
					.WithMany(p => p.CreditAppRequestTableKYCSetup)
					.HasForeignKey(d => d.KYCGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableKYCSetup");

				entity.HasOne(d => d.MailingReceiptAddress)
					.WithMany(p => p.CreditAppRequestTableMailingReceiptAddress)
					.HasForeignKey(d => d.MailingReceiptAddressGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableMailingReceiptAddress");

				entity.HasOne(d => d.PDCBank)
					.WithMany(p => p.CreditAppRequestTablePDCBank)
					.HasForeignKey(d => d.PDCBankGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTablePDCBank");

				entity.HasOne(d => d.ProductSubType)
					.WithMany(p => p.CreditAppRequestTableProductSubType)
					.HasForeignKey(d => d.ProductSubTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableProductSubType");

				entity.HasOne(d => d.ReceiptAddress)
					.WithMany(p => p.CreditAppRequestTableReceiptAddress)
					.HasForeignKey(d => d.ReceiptAddressGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableReceiptAddress");

				entity.HasOne(d => d.ReceiptContactPerson)
					.WithMany(p => p.CreditAppRequestTableReceiptContactPerson)
					.HasForeignKey(d => d.ReceiptContactPersonGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableReceiptContactPerson");

				entity.HasOne(d => d.RefCreditAppTable)
					.WithMany(p => p.CreditAppRequestTableRefCreditAppTable)
					.HasForeignKey(d => d.RefCreditAppTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableRefCreditAppTable");

				entity.HasOne(d => d.RegisteredAddress)
					.WithMany(p => p.CreditAppRequestTableRegisteredAddress)
					.HasForeignKey(d => d.RegisteredAddressGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableRegisteredAddress");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CreditAppRequestTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CreditAppRequestTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableBusinessUnit");
			});
			modelBuilder.Entity<CreditAppRequestTableAmend>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.CreditAppRequestTableAmendGUID);
				entity.HasIndex(e => e.CreditAppRequestTableGUID);
				entity.HasIndex(e => e.OriginalInterestTypeGUID);
				entity.HasIndex(e => e.CNReasonGUID);

				entity.Property(e => e.CreditAppRequestTableAmendGUID).ValueGeneratedNever();

				entity.HasOne(d => d.DocumentReason)
					.WithMany(p => p.CreditAppRequestTableAmendDocumentReason)
					.HasForeignKey(d => d.CNReasonGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableAmendDocumentReason");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CreditAppRequestTableAmendCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableAmendCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CreditAppRequestTableAmendBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppRequestTableAmendBusinessUnit");
			});
			modelBuilder.Entity<CreditAppTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.ApplicationTableGUID);
				entity.HasIndex(e => e.AssignmentMethodGUID);
				entity.HasIndex(e => e.BankAccountControlGUID);
				entity.HasIndex(e => e.BillingAddressGUID);
				entity.HasIndex(e => e.BillingContactPersonGUID);
				entity.HasIndex(e => e.ConsortiumTableGUID);
				entity.HasIndex(e => e.CreditAppTableGUID);
				entity.HasIndex(e => e.CreditLimitTypeGUID);
				entity.HasIndex(e => e.CreditTermGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.Dimension1GUID);
				entity.HasIndex(e => e.Dimension2GUID);
				entity.HasIndex(e => e.Dimension3GUID);
				entity.HasIndex(e => e.Dimension4GUID);
				entity.HasIndex(e => e.Dimension5GUID);
				entity.HasIndex(e => e.DocumentReasonGUID);
				entity.HasIndex(e => e.ExtensionServiceFeeCondTemplateGUID);
				entity.HasIndex(e => e.InterestTypeGUID);
				entity.HasIndex(e => e.InvoiceAddressGUID);
				entity.HasIndex(e => e.MailingReceipAddressGUID);
				entity.HasIndex(e => e.PDCBankGUID);
				entity.HasIndex(e => e.ProductSubTypeGUID);
				entity.HasIndex(e => e.ReceiptAddressGUID);
				entity.HasIndex(e => e.ReceiptContactPersonGUID);
				entity.HasIndex(e => e.RefCreditAppRequestTableGUID);
				entity.HasIndex(e => e.RegisteredAddressGUID);

				entity.Property(e => e.CreditAppTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.ApplicationTable)
					.WithMany(p => p.CreditAppTableApplicationTable)
					.HasForeignKey(d => d.ApplicationTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTableApplicationTable");

				entity.HasOne(d => d.AssignmentMethod)
					.WithMany(p => p.CreditAppTableAssignmentMethod)
					.HasForeignKey(d => d.AssignmentMethodGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTableAssignmentMethod");

				entity.HasOne(d => d.BankAccountControl)
					.WithMany(p => p.CreditAppTableBankAccountControl)
					.HasForeignKey(d => d.BankAccountControlGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTableBankAccountControl");

				entity.HasOne(d => d.BillingAddress)
					.WithMany(p => p.CreditAppTableBillingAddress)
					.HasForeignKey(d => d.BillingAddressGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTableBillingAddress");

				entity.HasOne(d => d.BillingContactPerson)
					.WithMany(p => p.CreditAppTableBillingContactPerson)
					.HasForeignKey(d => d.BillingContactPersonGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTableBillingContactPerson");

				entity.HasOne(d => d.ConsortiumTable)
					.WithMany(p => p.CreditAppTableConsortiumTable)
					.HasForeignKey(d => d.ConsortiumTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTableConsortiumTable");

				entity.HasOne(d => d.CreditLimitType)
					.WithMany(p => p.CreditAppTableCreditLimitType)
					.HasForeignKey(d => d.CreditLimitTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTableCreditLimitType");

				entity.HasOne(d => d.CreditTerm)
					.WithMany(p => p.CreditAppTableCreditTerm)
					.HasForeignKey(d => d.CreditTermGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTableCreditTerm");

				entity.HasOne(d => d.LedgerDimension1)
					.WithMany(p => p.CreditAppTableDimension1)
					.HasForeignKey(d => d.Dimension1GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTableLedgerDimension1");

				entity.HasOne(d => d.LedgerDimension2)
					.WithMany(p => p.CreditAppTableDimension2)
					.HasForeignKey(d => d.Dimension2GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTableLedgerDimension2");

				entity.HasOne(d => d.LedgerDimension3)
					.WithMany(p => p.CreditAppTableDimension3)
					.HasForeignKey(d => d.Dimension3GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTableLedgerDimension3");

				entity.HasOne(d => d.LedgerDimension4)
					.WithMany(p => p.CreditAppTableDimension4)
					.HasForeignKey(d => d.Dimension4GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTableLedgerDimension4");

				entity.HasOne(d => d.LedgerDimension5)
					.WithMany(p => p.CreditAppTableDimension5)
					.HasForeignKey(d => d.Dimension5GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTableLedgerDimension5");

				entity.HasOne(d => d.DocumentReason)
					.WithMany(p => p.CreditAppTableDocumentReason)
					.HasForeignKey(d => d.DocumentReasonGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTableDocumentReason");

				entity.HasOne(d => d.ExtensionServiceFeeCondTemplate)
					.WithMany(p => p.CreditAppTableExtensionServiceFeeCondTemplate)
					.HasForeignKey(d => d.ExtensionServiceFeeCondTemplateGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTableServiceFeeCondTemplateTable");

				entity.HasOne(d => d.InterestType)
					.WithMany(p => p.CreditAppTableInterestType)
					.HasForeignKey(d => d.InterestTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTableInterestType");

				entity.HasOne(d => d.InvoiceAddress)
					.WithMany(p => p.CreditAppTableInvoiceAddress)
					.HasForeignKey(d => d.InvoiceAddressGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTableInvoiceAddress");

				entity.HasOne(d => d.MailingReceipAddress)
					.WithMany(p => p.CreditAppTableMailingReceipAddress)
					.HasForeignKey(d => d.MailingReceipAddressGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTableMailingReceipAddress");

				entity.HasOne(d => d.PDCBank)
					.WithMany(p => p.CreditAppTablePDCBank)
					.HasForeignKey(d => d.PDCBankGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTablePDCBank");

				entity.HasOne(d => d.ProductSubType)
					.WithMany(p => p.CreditAppTableProductSubType)
					.HasForeignKey(d => d.ProductSubTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTableProductSubType");

				entity.HasOne(d => d.ReceiptAddress)
					.WithMany(p => p.CreditAppTableReceiptAddress)
					.HasForeignKey(d => d.ReceiptAddressGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTableReceiptAddress");

				entity.HasOne(d => d.ReceiptContactPerson)
					.WithMany(p => p.CreditAppTableReceiptContactPerson)
					.HasForeignKey(d => d.ReceiptContactPersonGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTableReceiptContactPerson");

				entity.HasOne(d => d.RefCreditAppRequestTable)
					.WithMany(p => p.CreditAppTableRefCreditAppRequestTable)
					.HasForeignKey(d => d.RefCreditAppRequestTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTableRefCreditAppRequestTable");

				entity.HasOne(d => d.RegisteredAddress)
					.WithMany(p => p.CreditAppTableRegisteredAddress)
					.HasForeignKey(d => d.RegisteredAddressGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTableRegisteredAddress");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CreditAppTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CreditAppTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTableBusinessUnit");
			});
			modelBuilder.Entity<CreditAppTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BuyerTableGUID);
				entity.HasIndex(e => e.CreditAppLineGUID);
				entity.HasIndex(e => e.CreditAppTableGUID);
				entity.HasIndex(e => e.CreditAppTransGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.RefGUID);

				entity.Property(e => e.CreditAppTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.BuyerTable)
					.WithMany(p => p.CreditAppTransBuyerTable)
					.HasForeignKey(d => d.BuyerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTransBuyerTable");

				entity.HasOne(d => d.CreditAppLine)
					.WithMany(p => p.CreditAppTransCreditAppLine)
					.HasForeignKey(d => d.CreditAppLineGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTransCreditAppLine");

				entity.HasOne(d => d.CreditAppTable)
					.WithMany(p => p.CreditAppTransCreditAppTable)
					.HasForeignKey(d => d.CreditAppTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTransCreditAppTable");

				entity.HasOne(d => d.CustomerTable)
					.WithMany(p => p.CreditAppTransCustomerTable)
					.HasForeignKey(d => d.CustomerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTransCustomerTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CreditAppTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CreditAppTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditAppTransBusinessUnit");

			});
			modelBuilder.Entity<CreditLimitType>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.CreditLimitTypeGUID);
				entity.HasIndex(e => e.ParentCreditLimitTypeGUID);

				entity.Property(e => e.CreditLimitTypeGUID).ValueGeneratedNever();

				entity.HasOne(d => d.ParentCreditLimitType)
					.WithMany(p => p.CreditLimitTypeParentCreditLimitType)
					.HasForeignKey(d => d.ParentCreditLimitTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditLimitTypeParentCreditLimitType");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CreditLimitTypeCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditLimitTypeCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CreditLimitTypeBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditLimitTypeBusinessUnit");
			});
			modelBuilder.Entity<CreditScoring>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.CreditScoringGUID);

				entity.Property(e => e.CreditScoringGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CreditScoringCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditScoringCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CreditScoringBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditScoringBusinessUnit");
			});
			modelBuilder.Entity<CreditTerm>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.CreditTermGUID);

				entity.Property(e => e.CreditTermGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CreditTermCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditTermCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CreditTermBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditTermBusinessUnit");
			});
			modelBuilder.Entity<CreditType>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.CreditTypeGUID);

				entity.Property(e => e.CreditTypeGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CreditTypeCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditTypeCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CreditTypeBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CreditTypeBusinessUnit");
			});
			modelBuilder.Entity<Currency>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);

				entity.Property(e => e.CurrencyGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CurrencyCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CurrencyCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CurrencyBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CurrencyBusinessUnit");
			});
			modelBuilder.Entity<CustBank>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BankGroupGUID);
				entity.HasIndex(e => e.BankTypeGUID);
				entity.HasIndex(e => e.CustBankGUID);
				entity.HasIndex(e => e.CustomerTableGUID);

				entity.Property(e => e.CustBankGUID).ValueGeneratedNever();

				entity.HasOne(d => d.BankGroup)
					.WithMany(p => p.CustBankBankGroup)
					.HasForeignKey(d => d.BankGroupGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustBankBankGroup");

				entity.HasOne(d => d.BankType)
					.WithMany(p => p.CustBankBankType)
					.HasForeignKey(d => d.BankTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustBankBankType");

				entity.HasOne(d => d.CustomerTable)
					.WithMany(p => p.CustBankCustomerTable)
					.HasForeignKey(d => d.CustomerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustBankCustomerTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CustBankCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustBankCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CustBankBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustBankBusinessUnit");
			});
			modelBuilder.Entity<CustBusinessCollateral>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BankGroupGUID);
				entity.HasIndex(e => e.BankTypeGUID);
				entity.HasIndex(e => e.BusinessCollateralAgmLineGUID);
				entity.HasIndex(e => e.BusinessCollateralStatusGUID);
				entity.HasIndex(e => e.BusinessCollateralSubTypeGUID);
				entity.HasIndex(e => e.BusinessCollateralTypeGUID);
				entity.HasIndex(e => e.BuyerTableGUID);
				entity.HasIndex(e => e.CreditAppRequestTableGUID);
				entity.HasIndex(e => e.CustBusinessCollateralGUID);
				entity.HasIndex(e => e.CustomerTableGUID);

				entity.Property(e => e.CustBusinessCollateralGUID).ValueGeneratedNever();

				entity.HasOne(d => d.BankGroup)
					.WithMany(p => p.CustBusinessCollateralBankGroup)
					.HasForeignKey(d => d.BankGroupGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustBusinessCollateralBankGroup");

				entity.HasOne(d => d.BankType)
					.WithMany(p => p.CustBusinessCollateralBankType)
					.HasForeignKey(d => d.BankTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustBusinessCollateralBankType");

				entity.HasOne(d => d.BusinessCollateralAgmLine)
					.WithMany(p => p.CustBusinessCollateralBusinessCollateralAgmLine)
					.HasForeignKey(d => d.BusinessCollateralAgmLineGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustBusinessCollateralBusinessCollateralAgmLine");

				entity.HasOne(d => d.BusinessCollateralStatus)
					.WithMany(p => p.CustBusinessCollateralBusinessCollateralStatus)
					.HasForeignKey(d => d.BusinessCollateralStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustBusinessCollateralBusinessCollateralStatus");

				entity.HasOne(d => d.BusinessCollateralSubType)
					.WithMany(p => p.CustBusinessCollateralBusinessCollateralSubType)
					.HasForeignKey(d => d.BusinessCollateralSubTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustBusinessCollateralBusinessCollateralSubType");

				entity.HasOne(d => d.BusinessCollateralType)
					.WithMany(p => p.CustBusinessCollateralBusinessCollateralType)
					.HasForeignKey(d => d.BusinessCollateralTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustBusinessCollateralBusinessCollateralType");

				entity.HasOne(d => d.BuyerTable)
					.WithMany(p => p.CustBusinessCollateralBuyerTable)
					.HasForeignKey(d => d.BuyerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustBusinessCollateralBuyerTable");

				entity.HasOne(d => d.CreditAppRequestTable)
					.WithMany(p => p.CustBusinessCollateralCreditAppRequestTable)
					.HasForeignKey(d => d.CreditAppRequestTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustBusinessCollateralCreditAppRequestTable");

				entity.HasOne(d => d.CustomerTable)
					.WithMany(p => p.CustBusinessCollateralCustomerTable)
					.HasForeignKey(d => d.CustomerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustBusinessCollateralCustomerTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CustBusinessCollateralCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustBusinessCollateralCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CustBusinessCollateralBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustBusinessCollateralBusinessUnit");
			});
			modelBuilder.Entity<CustGroup>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.NumberSeqTableGUID);

				entity.Property(e => e.CustGroupGUID).ValueGeneratedNever();

				entity.HasOne(d => d.NumberSeqTable)
					.WithMany(p => p.CustGroupNumberSeqTable)
					.HasForeignKey(d => d.NumberSeqTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustGroupNumberSeqTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CustGroupCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustGroupCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CustGroupBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustGroupBusinessUnit");
			});
			modelBuilder.Entity<CustomerCreditLimitByProduct>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.CustomerCreditLimitByProductGUID);
				entity.HasIndex(e => e.CustomerTableGUID);

				entity.Property(e => e.CustomerCreditLimitByProductGUID).ValueGeneratedNever();

				entity.HasOne(d => d.CustomerTable)
					.WithMany(p => p.CustomerCreditLimitByProductCustomerTable)
					.HasForeignKey(d => d.CustomerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerCreditLimitByProductCustomerTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CustomerCreditLimitByProductCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerCreditLimitByProductCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CustomerCreditLimitByProductBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerCreditLimitByProductBusinessUnit");
			});
			modelBuilder.Entity<CustomerRefundTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.CreditAppTableGUID);
				entity.HasIndex(e => e.CurrencyGUID);
				entity.HasIndex(e => e.CustomerRefundTableGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.Dimension1GUID);
				entity.HasIndex(e => e.Dimension2GUID);
				entity.HasIndex(e => e.Dimension3GUID);
				entity.HasIndex(e => e.Dimension4GUID);
				entity.HasIndex(e => e.Dimension5GUID);
				entity.HasIndex(e => e.DocumentReasonGUID);
				entity.HasIndex(e => e.DocumentStatusGUID);
				entity.HasIndex(e => e.OperReportSignatureGUID);

				entity.Property(e => e.CustomerRefundTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.CreditAppTable)
					.WithMany(p => p.CustomerRefundTableCreditAppTable)
					.HasForeignKey(d => d.CreditAppTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerRefundTableCreditAppTable");

				entity.HasOne(d => d.Currency)
					.WithMany(p => p.CustomerRefundTableCurrency)
					.HasForeignKey(d => d.CurrencyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerRefundTableCurrency");

				entity.HasOne(d => d.LedgerDimension1)
					.WithMany(p => p.CustomerRefundTableDimension1)
					.HasForeignKey(d => d.Dimension1GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerRefundTableLedgerDimension1");

				entity.HasOne(d => d.LedgerDimension2)
					.WithMany(p => p.CustomerRefundTableDimension2)
					.HasForeignKey(d => d.Dimension2GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerRefundTableLedgerDimension2");

				entity.HasOne(d => d.LedgerDimension3)
					.WithMany(p => p.CustomerRefundTableDimension3)
					.HasForeignKey(d => d.Dimension3GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerRefundTableLedgerDimension3");

				entity.HasOne(d => d.LedgerDimension4)
					.WithMany(p => p.CustomerRefundTableDimension4)
					.HasForeignKey(d => d.Dimension4GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerRefundTableLedgerDimension4");

				entity.HasOne(d => d.LedgerDimension5)
					.WithMany(p => p.CustomerRefundTableDimension5)
					.HasForeignKey(d => d.Dimension5GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerRefundTableLedgerDimension5");

				entity.HasOne(d => d.DocumentReason)
					.WithMany(p => p.CustomerRefundTableDocumentReason)
					.HasForeignKey(d => d.DocumentReasonGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerRefundTableDocumentReason");

				entity.HasOne(d => d.DocumentStatus)
					.WithMany(p => p.CustomerRefundTableDocumentStatus)
					.HasForeignKey(d => d.DocumentStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerRefundTableDocumentStatus");

				entity.HasOne(d => d.EmployeeTable)
					.WithMany(p => p.CustomerRefundTableEmployeeTable)
					.HasForeignKey(d => d.OperReportSignatureGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerRefundTableEmployeeTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CustomerRefundTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerRefundTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CustomerRefundTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerRefundTableBusinessUnit");
			});
			modelBuilder.Entity<CustomerTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BlacklistStatusGUID);
				entity.HasIndex(e => e.BusinessSegmentGUID);
				entity.HasIndex(e => e.BusinessSizeGUID);
				entity.HasIndex(e => e.BusinessTypeGUID);
				entity.HasIndex(e => e.CreditScoringGUID);
				entity.HasIndex(e => e.CurrencyGUID);
				entity.HasIndex(e => e.CustGroupGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.Dimension1GUID);
				entity.HasIndex(e => e.Dimension2GUID);
				entity.HasIndex(e => e.Dimension3GUID);
				entity.HasIndex(e => e.Dimension4GUID);
				entity.HasIndex(e => e.Dimension5GUID);
				entity.HasIndex(e => e.DocumentStatusGUID);
				entity.HasIndex(e => e.ExposureGroupGUID);
				entity.HasIndex(e => e.GenderGUID);
				entity.HasIndex(e => e.GradeClassificationGUID);
				entity.HasIndex(e => e.IntroducedByGUID);
				entity.HasIndex(e => e.KYCSetupGUID);
				entity.HasIndex(e => e.LineOfBusinessGUID);
				entity.HasIndex(e => e.MaritalStatusGUID);
				entity.HasIndex(e => e.MethodOfPaymentGUID);
				entity.HasIndex(e => e.NationalityGUID);
				entity.HasIndex(e => e.NCBAccountStatusGUID);
				entity.HasIndex(e => e.OccupationGUID);
				entity.HasIndex(e => e.ParentCompanyGUID);
				entity.HasIndex(e => e.RaceGUID);
				entity.HasIndex(e => e.RegistrationTypeGUID);
				entity.HasIndex(e => e.ResponsibleByGUID);
				entity.HasIndex(e => e.TerritoryGUID);
				entity.HasIndex(e => e.VendorTableGUID);
				entity.HasIndex(e => e.WithholdingTaxGroupGUID);

				entity.Property(e => e.CustomerTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.BlacklistStatus)
					.WithMany(p => p.CustomerTableBlacklistStatus)
					.HasForeignKey(d => d.BlacklistStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableBlacklistStatus");

				entity.HasOne(d => d.BusinessSegment)
					.WithMany(p => p.CustomerTableBusinessSegment)
					.HasForeignKey(d => d.BusinessSegmentGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableBusinessSegment");

				entity.HasOne(d => d.BusinessSize)
					.WithMany(p => p.CustomerTableBusinessSize)
					.HasForeignKey(d => d.BusinessSizeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableBusinessSize");

				entity.HasOne(d => d.BusinessType)
					.WithMany(p => p.CustomerTableBusinessType)
					.HasForeignKey(d => d.BusinessTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableBusinessType");

				entity.HasOne(d => d.CreditScoring)
					.WithMany(p => p.CustomerTableCreditScoring)
					.HasForeignKey(d => d.CreditScoringGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableCreditScoring");

				entity.HasOne(d => d.Currency)
					.WithMany(p => p.CustomerTableCurrency)
					.HasForeignKey(d => d.CurrencyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableCurrency");

				entity.HasOne(d => d.CustGroup)
					.WithMany(p => p.CustomerTableCustGroup)
					.HasForeignKey(d => d.CustGroupGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableCustGroup");
				entity.HasOne(d => d.LedgerDimension1)
					.WithMany(p => p.CustomerTableDimension1)
					.HasForeignKey(d => d.Dimension1GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableLedgerDimension1");

				entity.HasOne(d => d.LedgerDimension2)
					.WithMany(p => p.CustomerTableDimension2)
					.HasForeignKey(d => d.Dimension2GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableLedgerDimension2");

				entity.HasOne(d => d.LedgerDimension3)
					.WithMany(p => p.CustomerTableDimension3)
					.HasForeignKey(d => d.Dimension3GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableLedgerDimension3");

				entity.HasOne(d => d.LedgerDimension4)
					.WithMany(p => p.CustomerTableDimension4)
					.HasForeignKey(d => d.Dimension4GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableLedgerDimension4");

				entity.HasOne(d => d.LedgerDimension5)
					.WithMany(p => p.CustomerTableDimension5)
					.HasForeignKey(d => d.Dimension5GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableLedgerDimension5");

				entity.HasOne(d => d.DocumentStatus)
					.WithMany(p => p.CustomerTableDocumentStatus)
					.HasForeignKey(d => d.DocumentStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableDocumentStatus");

				entity.HasOne(d => d.ExposureGroup)
					.WithMany(p => p.CustomerTableExposureGroup)
					.HasForeignKey(d => d.ExposureGroupGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableExposureGroup");

				entity.HasOne(d => d.Gender)
					.WithMany(p => p.CustomerTableGender)
					.HasForeignKey(d => d.GenderGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableGender");

				entity.HasOne(d => d.GradeClassification)
					.WithMany(p => p.CustomerTableGradeClassification)
					.HasForeignKey(d => d.GradeClassificationGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableGradeClassification");

				entity.HasOne(d => d.IntroducedBy)
					.WithMany(p => p.CustomerTableIntroducedBy)
					.HasForeignKey(d => d.IntroducedByGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableIntroducedBy");

				entity.HasOne(d => d.KYCSetup)
					.WithMany(p => p.CustomerTableKYCSetup)
					.HasForeignKey(d => d.KYCSetupGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableKYC");

				entity.HasOne(d => d.LineOfBusiness)
					.WithMany(p => p.CustomerTableLineOfBusiness)
					.HasForeignKey(d => d.LineOfBusinessGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableLineOfBusiness");

				entity.HasOne(d => d.MaritalStatus)
					.WithMany(p => p.CustomerTableMaritalStatus)
					.HasForeignKey(d => d.MaritalStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableMaritalStatus");

				entity.HasOne(d => d.MethodOfPayment)
					.WithMany(p => p.CustomerTableMethodOfPayment)
					.HasForeignKey(d => d.MethodOfPaymentGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableMethodOfPayment");

				entity.HasOne(d => d.Nationality)
					.WithMany(p => p.CustomerTableNationality)
					.HasForeignKey(d => d.NationalityGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableNationality");

				entity.HasOne(d => d.NCBAccountStatus)
					.WithMany(p => p.CustomerTableNCBAccountStatus)
					.HasForeignKey(d => d.NCBAccountStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableNCBAccountStatus");

				entity.HasOne(d => d.Occupation)
					.WithMany(p => p.CustomerTableOccupation)
					.HasForeignKey(d => d.OccupationGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableOccupation");

				entity.HasOne(d => d.ParentCompany)
					.WithMany(p => p.CustomerTableParentCompany)
					.HasForeignKey(d => d.ParentCompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableParentCompany");

				entity.HasOne(d => d.Race)
					.WithMany(p => p.CustomerTableRace)
					.HasForeignKey(d => d.RaceGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableRace");

				entity.HasOne(d => d.RegistrationType)
					.WithMany(p => p.CustomerTableRegistrationType)
					.HasForeignKey(d => d.RegistrationTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableRegistrationType");

				entity.HasOne(d => d.ResponsibleBy)
					.WithMany(p => p.CustomerTableResponsibleBy)
					.HasForeignKey(d => d.ResponsibleByGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableEmployeeTable");

				entity.HasOne(d => d.Territory)
					.WithMany(p => p.CustomerTableTerritory)
					.HasForeignKey(d => d.TerritoryGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableTerritory");

				entity.HasOne(d => d.VendorTable)
					.WithMany(p => p.CustomerTableVendorTable)
					.HasForeignKey(d => d.VendorTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableVendorTable");

				entity.HasOne(d => d.WithholdingTaxGroup)
					.WithMany(p => p.CustomerTableWithholdingTaxGroup)
					.HasForeignKey(d => d.WithholdingTaxGroupGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableWithholdingTaxGroup");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CustomerTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CustomerTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustomerTableBusinessUnit");
			});
			modelBuilder.Entity<CustTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BranchGUID);
				entity.HasIndex(e => e.BuyerTableGUID);
				entity.HasIndex(e => e.CreditAppLineGUID);
				entity.HasIndex(e => e.CreditAppTableGUID);
				entity.HasIndex(e => e.CurrencyGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.CustTransGUID);
				entity.HasIndex(e => e.InvoiceTableGUID);
				entity.HasIndex(e => e.InvoiceTypeGUID);

				entity.Property(e => e.CustTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Branch)
					.WithMany(p => p.CustTransBranch)
					.HasForeignKey(d => d.BranchGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustTransBranch");

				entity.HasOne(d => d.BuyerTable)
					.WithMany(p => p.CustTransBuyerTable)
					.HasForeignKey(d => d.BuyerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustTransBuyerTable");

				entity.HasOne(d => d.CreditAppLine)
					.WithMany(p => p.CustTransCreditAppLine)
					.HasForeignKey(d => d.CreditAppLineGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustTransCreditAppLine");

				entity.HasOne(d => d.CreditAppTable)
					.WithMany(p => p.CustTransCreditAppTable)
					.HasForeignKey(d => d.CreditAppTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustTransCreditAppTable");

				entity.HasOne(d => d.Currency)
					.WithMany(p => p.CustTransCurrency)
					.HasForeignKey(d => d.CurrencyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustTransCurrency");

				entity.HasOne(d => d.CustomerTable)
					.WithMany(p => p.CustTransCustomerTable)
					.HasForeignKey(d => d.CustomerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustTransCustomerTable");

				entity.HasOne(d => d.InvoiceTable)
					.WithMany(p => p.CustTransInvoiceTable)
					.HasForeignKey(d => d.InvoiceTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustTransInvoiceTable");

				entity.HasOne(d => d.InvoiceType)
					.WithMany(p => p.CustTransInvoiceType)
					.HasForeignKey(d => d.InvoiceTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustTransInvoiceType");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CustTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CustTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustTransBusinessUnit");
			});
			modelBuilder.Entity<CustVisitingTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.CustVisitingTransGUID);
				entity.HasIndex(e => e.RefGUID);
				entity.HasIndex(e => e.ResponsibleByGUID);

				entity.Property(e => e.CustVisitingTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.ResponsibleBy)
					.WithMany(p => p.CustVisitingTransResponsibleBy)
					.HasForeignKey(d => d.ResponsibleByGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustVisitingTransEmployeeTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CustVisitingTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustVisitingTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CustVisitingTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CustVisitingTransBusinessUnit");
			});
			modelBuilder.Entity<Department>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.DepartmentGUID);

				entity.Property(e => e.DepartmentGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.DepartmentCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DepartmentCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.DepartmentBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DepartmentBusinessUnit");
			});
			modelBuilder.Entity<DocumentConditionTemplateLine>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.DocumentConditionTemplateLineGUID);
				entity.HasIndex(e => e.DocumentConditionTemplateTableGUID);
				entity.HasIndex(e => e.DocumentTypeGUID);

				entity.Property(e => e.DocumentConditionTemplateLineGUID).ValueGeneratedNever();

				entity.HasOne(d => d.DocumentConditionTemplateTable)
					.WithMany(p => p.DocumentConditionTemplateLineDocumentConditionTemplateTable)
					.HasForeignKey(d => d.DocumentConditionTemplateTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentConditionTemplateLineDocumentConditionTemplateTable");

				entity.HasOne(d => d.DocumentType)
					.WithMany(p => p.DocumentConditionTemplateLineDocumentType)
					.HasForeignKey(d => d.DocumentTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentConditionTemplateLineDocumentType");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.DocumentConditionTemplateLineCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentConditionTemplateLineCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.DocumentConditionTemplateLineBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentConditionTemplateLineBusinessUnit");
			});
			modelBuilder.Entity<DocumentConditionTemplateTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.DocumentConditionTemplateTableGUID);

				entity.Property(e => e.DocumentConditionTemplateTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.DocumentConditionTemplateTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentConditionTemplateTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.DocumentConditionTemplateTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentConditionTemplateTableBusinessUnit");
			});
			modelBuilder.Entity<DocumentConditionTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.DocumentConditionTransGUID);
				entity.HasIndex(e => e.DocumentTypeGUID);
				entity.HasIndex(e => e.RefDocumentConditionTransGUID);
				entity.HasIndex(e => e.RefGUID);

				entity.Property(e => e.DocumentConditionTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.DocumentType)
					.WithMany(p => p.DocumentConditionTransDocumentType)
					.HasForeignKey(d => d.DocumentTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentConditionTransDocumentType");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.DocumentConditionTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentConditionTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.DocumentConditionTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentConditionTransBusinessUnit");
			});
			modelBuilder.Entity<DocumentProcess>(entity =>
			{
				entity.Property(e => e.DocumentProcessGUID).ValueGeneratedNever();
			});
			modelBuilder.Entity<DocumentReason>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);

				entity.Property(e => e.DocumentReasonGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.DocumentReasonCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentReasonCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.DocumentReasonBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentReasonBusinessUnit");
			});
			modelBuilder.Entity<DocumentStatus>(entity =>
			{
				entity.HasIndex(e => e.DocumentProcessGUID);

				entity.Property(e => e.DocumentStatusGUID).ValueGeneratedNever();

				entity.HasOne(d => d.DocumentProcess)
					.WithMany(p => p.DocumentStatusDocumentProcess)
					.HasForeignKey(d => d.DocumentProcessGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentStatusDocumentProcess");
			});
			modelBuilder.Entity<DocumentTemplateTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.TemplateId);

				entity.Property(e => e.DocumentTemplateTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.DocumentTemplateTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentTemplateTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.DocumentTemplateTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentTemplateTableBusinessUnit");
			});
			modelBuilder.Entity<DocumentType>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.DocumentTypeGUID);

				entity.Property(e => e.DocumentTypeGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.DocumentTypeCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentTypeCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.DocumentTypeBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentTypeBusinessUnit");
			});
			modelBuilder.Entity<EmployeeTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.Dimension1GUID);
				entity.HasIndex(e => e.Dimension2GUID);
				entity.HasIndex(e => e.Dimension3GUID);
				entity.HasIndex(e => e.Dimension4GUID);
				entity.HasIndex(e => e.Dimension5GUID);
				entity.HasIndex(e => e.EmplTeamGUID);

				entity.Property(e => e.EmployeeTableGUID).ValueGeneratedNever();

				entity.Property(e => e.Signature).IsUnicode(false);

				entity.HasOne(d => d.BusinessUnitGU)
					.WithMany(p => p.EmployeeTable)
					.HasForeignKey(d => d.BusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_EmployeeTableBusinessUnitGU");

				entity.HasOne(d => d.LedgerDimension1)
					.WithMany(p => p.EmployeeTableDimension1)
					.HasForeignKey(d => d.Dimension1GUID)
					.HasConstraintName("FK_EmployeeTableLedgerDimension1");

				entity.HasOne(d => d.LedgerDimension2)
					.WithMany(p => p.EmployeeTableDimension2)
					.HasForeignKey(d => d.Dimension2GUID)
					.HasConstraintName("FK_EmployeeTableLedgerDimension2");

				entity.HasOne(d => d.LedgerDimension3)
					.WithMany(p => p.EmployeeTableDimension3)
					.HasForeignKey(d => d.Dimension3GUID)
					.HasConstraintName("FK_EmployeeTableLedgerDimension3");

				entity.HasOne(d => d.LedgerDimension4)
					.WithMany(p => p.EmployeeTableDimension4)
					.HasForeignKey(d => d.Dimension4GUID)
					.HasConstraintName("FK_EmployeeTableLedgerDimension4");

				entity.HasOne(d => d.LedgerDimension5)
					.WithMany(p => p.EmployeeTableDimension5)
					.HasForeignKey(d => d.Dimension5GUID)
					.HasConstraintName("FK_EmployeeTableLedgerDimension5");

				entity.HasOne(d => d.EmplTeam)
					.WithMany(p => p.EmployeeTableEmplTeam)
					.HasForeignKey(d => d.EmplTeamGUID)
					.HasConstraintName("FK_EmployeeTableEmplTeam");

				entity.HasOne(d => d.User)
					.WithMany(p => p.EmployeeTable)
					.HasForeignKey(d => d.UserId)
					.HasConstraintName("FK_EmployeeTableSysUserTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.EmployeeTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_EmployeeTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.EmployeeTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_EmployeeTableBusinessUnit");
			});
			modelBuilder.Entity<EmplTeam>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);

				entity.Property(e => e.EmplTeamGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.EmplTeamCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_EmplTeamCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.EmplTeamBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_EmplTeamBusinessUnit");
			});
			modelBuilder.Entity<ExchangeRate>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);

				entity.HasIndex(e => e.CurrencyGUID);

				entity.HasIndex(e => e.HomeCurrencyGUID);

				entity.Property(e => e.ExchangeRateGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Currency)
					.WithMany(p => p.ExchangeRateCurrency)
					.HasForeignKey(d => d.CurrencyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ExchangeRateCurrency");

				entity.HasOne(d => d.HomeCurrency)
					.WithMany(p => p.ExchangeRateHomeCurrency)
					.HasForeignKey(d => d.HomeCurrencyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ExchangeRateHomeCurrency");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.ExchangeRateCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ExchangeRateCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.ExchangeRateBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ExchangeRateBusinessUnit");
			});
			modelBuilder.Entity<ExposureGroup>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.ExposureGroupGUID);

				entity.Property(e => e.ExposureGroupGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.ExposureGroupCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ExposureGroupCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.ExposureGroupBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ExposureGroupBusinessUnit");
			});
			modelBuilder.Entity<ExposureGroupByProduct>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.ExposureGroupByProductGUID);
				entity.HasIndex(e => e.ExposureGroupGUID);

				entity.Property(e => e.ExposureGroupByProductGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.ExposureGroupByProductCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ExposureGroupByProductCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.ExposureGroupByProductBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ExposureGroupByProductBusinessUnit");
			});
			modelBuilder.Entity<FinancialCreditTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BankGroupGUID);
				entity.HasIndex(e => e.CreditTypeGUID);
				entity.HasIndex(e => e.FinancialCreditTransGUID);
				entity.HasIndex(e => e.RefGUID);

				entity.Property(e => e.FinancialCreditTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.BankGroup)
					.WithMany(p => p.FinancialCreditTransBankGroup)
					.HasForeignKey(d => d.BankGroupGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_FinancialCreditTransBankGroup");

				entity.HasOne(d => d.CreditType)
					.WithMany(p => p.FinancialCreditTransCreditType)
					.HasForeignKey(d => d.CreditTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_FinancialCreditTransCreditType");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.FinancialCreditTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_FinancialCreditTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.FinancialCreditTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_FinancialCreditTransBusinessUnit");
			});
			modelBuilder.Entity<FinancialStatementTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.FinancialStatementTransGUID);
				entity.HasIndex(e => e.RefGUID);

				entity.Property(e => e.FinancialStatementTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.FinancialStatementTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_FinancialStatementTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.FinancialStatementTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_FinancialStatementTransBusinessUnit");
			});
			modelBuilder.Entity<Gender>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.GenderGUID);

				entity.Property(e => e.GenderGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.GenderCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_GenderCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.GenderBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_GenderBusinessUnit");
			});
			modelBuilder.Entity<GradeClassification>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.GradeClassificationGUID);

				entity.Property(e => e.GradeClassificationGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.GradeClassificationCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_GradeClassificationCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.GradeClassificationBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_GradeClassificationBusinessUnit");
			});
			modelBuilder.Entity<GuarantorAgreementLine>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.GuarantorAgreementLineGUID);
				entity.HasIndex(e => e.GuarantorAgreementTableGUID);
				entity.HasIndex(e => e.GuarantorTransGUID);
				entity.HasIndex(e => e.NationalityGUID);
				entity.HasIndex(e => e.RaceGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.CreditAppTableGUID);

				entity.Property(e => e.GuarantorAgreementLineGUID).ValueGeneratedNever();

				entity.HasOne(d => d.GuarantorAgreementTable)
					.WithMany(p => p.GuarantorAgreementLineGuarantorAgreementTable)
					.HasForeignKey(d => d.GuarantorAgreementTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_GuarantorAgreementLineGuarantorAgreementTable");

				entity.HasOne(d => d.GuarantorTrans)
					.WithMany(p => p.GuarantorAgreementLineGuarantorTrans)
					.HasForeignKey(d => d.GuarantorTransGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_GuarantorAgreementLineGuarantorTrans");

				entity.HasOne(d => d.Nationality)
					.WithMany(p => p.GuarantorAgreementLineNationality)
					.HasForeignKey(d => d.NationalityGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_GuarantorAgreementLineNationality");

				entity.HasOne(d => d.Race)
					.WithMany(p => p.GuarantorAgreementLineRace)
					.HasForeignKey(d => d.RaceGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_GuarantorAgreementLineRace");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.GuarantorAgreementLineCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_GuarantorAgreementLineCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.GuarantorAgreementLineBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_GuarantorAgreementLineBusinessUnit");
			});
			modelBuilder.Entity<GuarantorAgreementLineAffiliate>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.GuarantorAgreementLineAffiliateGUID);
				entity.HasIndex(e => e.GuarantorAgreementLineGUID);
				entity.HasIndex(e => e.MainAgreementTableGUID);

				entity.Property(e => e.GuarantorAgreementLineAffiliateGUID).ValueGeneratedNever();

				entity.HasOne(d => d.GuarantorAgreementLine)
					.WithMany(p => p.GuarantorAgreementLineAffiliateGuarantorAgreementLine)
					.HasForeignKey(d => d.GuarantorAgreementLineGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_GuarantorAgreementLineAffiliateGuarantorAgreementLine");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.GuarantorAgreementLineAffiliateCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_GuarantorAgreementLineAffiliateCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.GuarantorAgreementLineAffiliateBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_GuarantorAgreementLineAffiliateBusinessUnit");
			});
			modelBuilder.Entity<GuarantorAgreementTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.DocumentReasonGUID);
				entity.HasIndex(e => e.DocumentStatusGUID);
				entity.HasIndex(e => e.GuarantorAgreementTableGUID);
				entity.HasIndex(e => e.MainAgreementTableGUID);
				entity.HasIndex(e => e.ParentCompanyGUID);
				entity.HasIndex(e => e.RefGuarantorAgreementTableGUID);
				entity.HasIndex(e => e.WitnessCompany1GUID);
				entity.HasIndex(e => e.WitnessCompany2GUID);

				entity.Property(e => e.GuarantorAgreementTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.DocumentReason)
					.WithMany(p => p.GuarantorAgreementTableDocumentReason)
					.HasForeignKey(d => d.DocumentReasonGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_GuarantorAgreementTableDocumentReason");

				entity.HasOne(d => d.DocumentStatus)
					.WithMany(p => p.GuarantorAgreementTableDocumentStatus)
					.HasForeignKey(d => d.DocumentStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_GuarantorAgreementTableDocumentStatus");

				entity.HasOne(d => d.MainAgreementTable)
					.WithMany(p => p.GuarantorAgreementTableMainAgreementTable)
					.HasForeignKey(d => d.MainAgreementTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_GuarantorAgreementTableMainAgreementTable");

				entity.HasOne(d => d.RefGuarantorAgreementTable)
					.WithMany(p => p.GuarantorAgreementTableRefGuarantorAgreementTable)
					.HasForeignKey(d => d.RefGuarantorAgreementTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_GuarantorAgreementTableGuarantorAgreementTable");

				entity.HasOne(d => d.WitnessCompany1)
					.WithMany(p => p.GuarantorAgreementTableWitnessCompany1)
					.HasForeignKey(d => d.WitnessCompany1GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_GuarantorAgreementTableEmployeeTable1");

				entity.HasOne(d => d.WitnessCompany2)
					.WithMany(p => p.GuarantorAgreementTableWitnessCompany2)
					.HasForeignKey(d => d.WitnessCompany2GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_GuarantorAgreementTableEmployeeTable2");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.GuarantorAgreementTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_GuarantorAgreementTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.GuarantorAgreementTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_GuarantorAgreementTableBusinessUnit");
			});
			modelBuilder.Entity<GuarantorTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.GuarantorTransGUID);
				entity.HasIndex(e => e.GuarantorTypeGUID);
				entity.HasIndex(e => e.RefGuarantorTransGUID);
				entity.HasIndex(e => e.RefGUID);
				entity.HasIndex(e => e.RelatedPersonTableGUID);

				entity.Property(e => e.GuarantorTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.GuarantorType)
					.WithMany(p => p.GuarantorTransGuarantorType)
					.HasForeignKey(d => d.GuarantorTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_GuarantorTransGuarantorType");

				entity.HasOne(d => d.RelatedPersonTable)
					.WithMany(p => p.GuarantorTransRelatedPersonTable)
					.HasForeignKey(d => d.RelatedPersonTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_GuarantorTransRelatedPersonTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.GuarantorTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_GuarantorTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.GuarantorTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_GuarantorTransBusinessUnit");
			});
			modelBuilder.Entity<GuarantorType>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.GuarantorTypeGUID);

				entity.Property(e => e.GuarantorTypeGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.GuarantorTypeCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_GuarantorTypeCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.GuarantorTypeBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_GuarantorTypeBusinessUnit");
			});
			modelBuilder.Entity<Intercompany>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.IntercompanyGUID);

				entity.Property(e => e.IntercompanyGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.IntercompanyCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntercompanyCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.IntercompanyBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntercompanyBusinessUnit");
			});
			modelBuilder.Entity<InterestRealizedTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.InterestRealizedTransGUID);
				entity.HasIndex(e => e.RefGUID);
				entity.HasIndex(e => e.Dimension1GUID);
				entity.HasIndex(e => e.Dimension2GUID);
				entity.HasIndex(e => e.Dimension3GUID);
				entity.HasIndex(e => e.Dimension4GUID);
				entity.HasIndex(e => e.Dimension5GUID);

				entity.Property(e => e.InterestRealizedTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.LedgerDimension1)
					.WithMany(p => p.InterestRealizedTransDimension1)
					.HasForeignKey(d => d.Dimension1GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InterestRealizedTransLedgerDimension1");

				entity.HasOne(d => d.LedgerDimension2)
					.WithMany(p => p.InterestRealizedTransDimension2)
					.HasForeignKey(d => d.Dimension2GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InterestRealizedTransLedgerDimension2");

				entity.HasOne(d => d.LedgerDimension3)
					.WithMany(p => p.InterestRealizedTransDimension3)
					.HasForeignKey(d => d.Dimension3GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InterestRealizedTransLedgerDimension3");

				entity.HasOne(d => d.LedgerDimension4)
					.WithMany(p => p.InterestRealizedTransDimension4)
					.HasForeignKey(d => d.Dimension4GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InterestRealizedTransLedgerDimension4");

				entity.HasOne(d => d.LedgerDimension5)
					.WithMany(p => p.InterestRealizedTransDimension5)
					.HasForeignKey(d => d.Dimension5GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InterestRealizedTransLedgerDimension5");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.InterestRealizedTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InterestRealizedTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.InterestRealizedTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InterestRealizedTransBusinessUnit");
			});
			modelBuilder.Entity<InterestType>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.InterestTypeGUID);

				entity.Property(e => e.InterestTypeGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.InterestTypeCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InterestTypeCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.InterestTypeBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InterestTypeBusinessUnit");
			});
			modelBuilder.Entity<InterestTypeValue>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.InterestTypeGUID);
				entity.HasIndex(e => e.InterestTypeValueGUID);

				entity.Property(e => e.InterestTypeValueGUID).ValueGeneratedNever();

				entity.HasOne(d => d.InterestType)
					.WithMany(p => p.InterestTypeValueInterestType)
					.HasForeignKey(d => d.InterestTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InterestTypeValueInterestType");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.InterestTypeValueCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InterestTypeValueCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.InterestTypeValueBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InterestTypeValueBusinessUnit");
			});
			modelBuilder.Entity<IntroducedBy>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.VendorTableGUID);

				entity.Property(e => e.IntroducedByGUID).ValueGeneratedNever();

				entity.HasOne(d => d.VendorTable)
					.WithMany(p => p.IntroducedByVendorTable)
					.HasForeignKey(d => d.VendorTableGUID)
					.HasConstraintName("FK_IntroducedByVendorTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.IntroducedByCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntroducedByCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.IntroducedByBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntroducedByBusinessUnit");
			});
			modelBuilder.Entity<InvoiceLine>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BranchGUID);
				entity.HasIndex(e => e.Dimension1GUID);
				entity.HasIndex(e => e.Dimension2GUID);
				entity.HasIndex(e => e.Dimension3GUID);
				entity.HasIndex(e => e.Dimension4GUID);
				entity.HasIndex(e => e.Dimension5GUID);
				entity.HasIndex(e => e.InvoiceLineGUID);
				entity.HasIndex(e => e.InvoiceRevenueTypeGUID);
				entity.HasIndex(e => e.InvoiceTableGUID);
				entity.HasIndex(e => e.ProdUnitGUID);
				entity.HasIndex(e => e.TaxTableGUID);
				entity.HasIndex(e => e.WithholdingTaxTableGUID);

				entity.Property(e => e.InvoiceLineGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Branch)
					.WithMany(p => p.InvoiceLineBranch)
					.HasForeignKey(d => d.BranchGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceLineBranch");

				entity.HasOne(d => d.LedgerDimension1)
					.WithMany(p => p.InvoiceLineDimension1)
					.HasForeignKey(d => d.Dimension1GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceLineLedgerDimension1");

				entity.HasOne(d => d.LedgerDimension2)
					.WithMany(p => p.InvoiceLineDimension2)
					.HasForeignKey(d => d.Dimension2GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceLineLedgerDimension2");

				entity.HasOne(d => d.LedgerDimension3)
					.WithMany(p => p.InvoiceLineDimension3)
					.HasForeignKey(d => d.Dimension3GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceLineLedgerDimension3");

				entity.HasOne(d => d.LedgerDimension4)
					.WithMany(p => p.InvoiceLineDimension4)
					.HasForeignKey(d => d.Dimension4GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceLineLedgerDimension4");

				entity.HasOne(d => d.LedgerDimension5)
					.WithMany(p => p.InvoiceLineDimension5)
					.HasForeignKey(d => d.Dimension5GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceLineLedgerDimension5");

				entity.HasOne(d => d.InvoiceTable)
					.WithMany(p => p.InvoiceLineInvoiceTable)
					.HasForeignKey(d => d.InvoiceTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceLineInvoiceTable");

				entity.HasOne(d => d.ProdUnit)
					.WithMany(p => p.InvoiceLineProdUnit)
					.HasForeignKey(d => d.ProdUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceLineProdUnit");

				entity.HasOne(d => d.TaxTable)
					.WithMany(p => p.InvoiceLineTaxTable)
					.HasForeignKey(d => d.TaxTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceLineTaxTable");

				entity.HasOne(d => d.WithholdingTaxTable)
					.WithMany(p => p.InvoiceLineWithholdingTaxTable)
					.HasForeignKey(d => d.WithholdingTaxTableGUID)
					.HasConstraintName("FK_InvoiceLineWithholdingTaxTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.InvoiceLineCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceLineCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.InvoiceLineBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceLineBusinessUnit");
			});
			modelBuilder.Entity<InvoiceNumberSeqSetup>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BranchGUID);
				entity.HasIndex(e => e.InvoiceTypeGUID);
				entity.HasIndex(e => e.NumberSeqTableGUID);

				entity.Property(e => e.InvoiceNumberSeqSetupGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Branch)
					.WithMany(p => p.InvoiceNumberSeqSetupBranch)
					.HasForeignKey(d => d.BranchGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceNumberSeqSetupBranch");

				entity.HasOne(d => d.InvoiceType)
					.WithMany(p => p.InvoiceNumberSeqSetupInvoiceType)
					.HasForeignKey(d => d.InvoiceTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceNumberSeqSetupInvoiceType");

				entity.HasOne(d => d.NumberSeqTable)
					.WithMany(p => p.InvoiceNumberSeqSetupNumberSeqTable)
					.HasForeignKey(d => d.NumberSeqTableGUID)
					.HasConstraintName("FK_InvoiceNumberSeqSetupNumberSeqTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.InvoiceNumberSeqSetupCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceNumberSeqSetupCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.InvoiceNumberSeqSetupBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceNumberSeqSetupBusinessUnit");
			});
			modelBuilder.Entity<InvoiceRevenueType>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.FeeTaxGUID);
				entity.HasIndex(e => e.FeeWHTGUID);
				entity.HasIndex(e => e.IntercompanyTableGUID);
				entity.HasIndex(e => e.InvoiceRevenueTypeGUID);
				entity.HasIndex(e => e.ServiceFeeRevenueTypeGUID);

				entity.Property(e => e.InvoiceRevenueTypeGUID).ValueGeneratedNever();

				entity.HasOne(d => d.FeeTax)
					.WithMany(p => p.InvoiceRevenueTypeFeeTax)
					.HasForeignKey(d => d.FeeTaxGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceRevenueTypeTaxTable");

				entity.HasOne(d => d.FeeWHT)
					.WithMany(p => p.InvoiceRevenueTypeFeeWHT)
					.HasForeignKey(d => d.FeeWHTGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceRevenueTypeWithholdingTaxTable");

				entity.HasOne(d => d.Intercompany)
					.WithMany(p => p.InvoiceRevenueTypeIntercompany)
					.HasForeignKey(d => d.IntercompanyTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceRevenueTypeIntercompany");

				entity.HasOne(d => d.ServiceFeeRevenueType)
					.WithMany(p => p.InvoiceRevenueTypeServiceFeeRevenueType)
					.HasForeignKey(d => d.ServiceFeeRevenueTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceRevenueTypeInvoiceRevenueType");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.InvoiceRevenueTypeCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceRevenueTypeCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.InvoiceRevenueTypeBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceRevenueTypeBusinessUnit");
			});
			modelBuilder.Entity<InvoiceSettlementDetail>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.AssignmentAgreementTableGUID);
				entity.HasIndex(e => e.BuyerAgreementTableGUID);
				entity.HasIndex(e => e.InvoiceSettlementDetailGUID);
				entity.HasIndex(e => e.InvoiceTableGUID);
				entity.HasIndex(e => e.InvoiceTypeGUID);
				entity.HasIndex(e => e.RefGUID);
				entity.HasIndex(e => e.RefReceiptTempPaymDetailGUID);

				entity.Property(e => e.InvoiceSettlementDetailGUID).ValueGeneratedNever();

				entity.HasOne(d => d.AssignmentAgreementTable)
					.WithMany(p => p.InvoiceSettlementDetailAssignmentAgreementTable)
					.HasForeignKey(d => d.AssignmentAgreementTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceSettlementDetailAssignmentAgreementTable");

				entity.HasOne(d => d.BuyerAgreementTable)
					.WithMany(p => p.InvoiceSettlementDetailBuyerAgreementTable)
					.HasForeignKey(d => d.BuyerAgreementTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceSettlementDetailBuyerAgreementTable");

				entity.HasOne(d => d.InvoiceTable)
					.WithMany(p => p.InvoiceSettlementDetailInvoiceTable)
					.HasForeignKey(d => d.InvoiceTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceSettlementDetailInvoiceTable");

				entity.HasOne(d => d.InvoiceType)
					.WithMany(p => p.InvoiceSettlementDetailInvoiceType)
					.HasForeignKey(d => d.InvoiceTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceSettlementDetailInvoiceType");

				entity.HasOne(d => d.RefReceiptTempPaymDetail)
					.WithMany(p => p.InvoiceSettlementDetailRefReceiptTempPaymDetail)
					.HasForeignKey(d => d.RefReceiptTempPaymDetailGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceSettlementDetailRefReceiptTempPaymDetail");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.InvoiceSettlementDetailCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceSettlementDetailCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.InvoiceSettlementDetailBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceSettlementDetailBusinessUnit");
			});
			modelBuilder.Entity<InvoiceTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BranchGUID);
				entity.HasIndex(e => e.BuyerAgreementTableGUID);
				entity.HasIndex(e => e.BuyerInvoiceTableGUID);
				entity.HasIndex(e => e.BuyerTableGUID);
				entity.HasIndex(e => e.CNReasonGUID);
				entity.HasIndex(e => e.CreditAppTableGUID);
				entity.HasIndex(e => e.CurrencyGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.Dimension1GUID);
				entity.HasIndex(e => e.Dimension2GUID);
				entity.HasIndex(e => e.Dimension3GUID);
				entity.HasIndex(e => e.Dimension4GUID);
				entity.HasIndex(e => e.Dimension5GUID);
				entity.HasIndex(e => e.DocumentStatusGUID);
				entity.HasIndex(e => e.InvoiceTableGUID);
				entity.HasIndex(e => e.InvoiceTypeGUID);
				entity.HasIndex(e => e.MethodOfPaymentGUID);
				entity.HasIndex(e => e.ReceiptTempTableGUID);
				entity.HasIndex(e => e.RefGUID);
				entity.HasIndex(e => e.RefInvoiceGUID);
				entity.HasIndex(e => e.RefTaxInvoiceGUID);

				entity.Property(e => e.InvoiceTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Branch)
					.WithMany(p => p.InvoiceTableBranch)
					.HasForeignKey(d => d.BranchGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceTableBranch");

				entity.HasOne(d => d.BuyerAgreementTable)
					.WithMany(p => p.InvoiceTableBuyerAgreementTable)
					.HasForeignKey(d => d.BuyerAgreementTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceTableBuyerAgreementTable");

				entity.HasOne(d => d.BuyerInvoiceTable)
					.WithMany(p => p.InvoiceTableBuyerInvoiceTable)
					.HasForeignKey(d => d.BuyerInvoiceTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceTableBuyerInvoiceTable");

				entity.HasOne(d => d.BuyerTable)
					.WithMany(p => p.InvoiceTableBuyerTable)
					.HasForeignKey(d => d.BuyerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceTableBuyerTable");

				entity.HasOne(d => d.CNReason)
					.WithMany(p => p.InvoiceTableCNReason)
					.HasForeignKey(d => d.CNReasonGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceTableDocumentReason");

				entity.HasOne(d => d.CreditAppTable)
					.WithMany(p => p.InvoiceTableCreditAppTable)
					.HasForeignKey(d => d.CreditAppTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceTableCreditAppTable");

				entity.HasOne(d => d.Currency)
					.WithMany(p => p.InvoiceTableCurrency)
					.HasForeignKey(d => d.CurrencyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceTableCurrency");

				entity.HasOne(d => d.LedgerDimension1)
					.WithMany(p => p.InvoiceTableDimension1)
					.HasForeignKey(d => d.Dimension1GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceTableLedgerDimension1");

				entity.HasOne(d => d.LedgerDimension2)
					.WithMany(p => p.InvoiceTableDimension2)
					.HasForeignKey(d => d.Dimension2GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceTableLedgerDimension2");

				entity.HasOne(d => d.LedgerDimension3)
					.WithMany(p => p.InvoiceTableDimension3)
					.HasForeignKey(d => d.Dimension3GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceTableLedgerDimension3");

				entity.HasOne(d => d.LedgerDimension4)
					.WithMany(p => p.InvoiceTableDimension4)
					.HasForeignKey(d => d.Dimension4GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceTableLedgerDimension4");

				entity.HasOne(d => d.LedgerDimension5)
					.WithMany(p => p.InvoiceTableDimension5)
					.HasForeignKey(d => d.Dimension5GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceTableLedgerDimension5");

				entity.HasOne(d => d.DocumentStatus)
					.WithMany(p => p.InvoiceTableDocumentStatus)
					.HasForeignKey(d => d.DocumentStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceTableDocumentStatus");

				entity.HasOne(d => d.InvoiceType)
					.WithMany(p => p.InvoiceTableInvoiceType)
					.HasForeignKey(d => d.InvoiceTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceTableInvoiceType");

				entity.HasOne(d => d.MethodOfPayment)
					.WithMany(p => p.InvoiceTableMethodOfPayment)
					.HasForeignKey(d => d.MethodOfPaymentGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceTableMethodOfPayment");

				entity.HasOne(d => d.ReceiptTempTable)
					.WithMany(p => p.InvoiceTableReceiptTempTable)
					.HasForeignKey(d => d.ReceiptTempTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceTableReceiptTempTable");

				entity.HasOne(d => d.RefInvoice)
					.WithMany(p => p.InvoiceTableRefInvoice)
					.HasForeignKey(d => d.RefInvoiceGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceTableRefInvoice");

				entity.HasOne(d => d.RefTaxInvoice)
					.WithMany(p => p.InvoiceTableRefTaxInvoice)
					.HasForeignKey(d => d.RefTaxInvoiceGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceTableTaxInvoiceTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.InvoiceTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.InvoiceTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceTableBusinessUnit");
			});
			modelBuilder.Entity<InvoiceType>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);

				entity.Property(e => e.InvoiceTypeGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.InvoiceTypeCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceTypeCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.InvoiceTypeBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_InvoiceTypeBusinessUnit");
			});
			modelBuilder.Entity<JobCheque>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.ChequeBankGroupGUID);
				entity.HasIndex(e => e.ChequeTableGUID);
				entity.HasIndex(e => e.CollectionFollowUpGUID);
				entity.HasIndex(e => e.JobChequeGUID);
				entity.HasIndex(e => e.MessengerJobTableGUID);

				entity.Property(e => e.JobChequeGUID).ValueGeneratedNever();

				entity.HasOne(d => d.ChequeBankGroup)
					.WithMany(p => p.JobChequeChequeBankGroup)
					.HasForeignKey(d => d.ChequeBankGroupGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_JobChequeBankGroup");

				entity.HasOne(d => d.ChequeTable)
					.WithMany(p => p.JobChequeChequeTable)
					.HasForeignKey(d => d.ChequeTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_JobChequeChequeTable");

				entity.HasOne(d => d.CollectionFollowUp)
					.WithMany(p => p.JobChequeCollectionFollowUp)
					.HasForeignKey(d => d.CollectionFollowUpGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_JobChequeCollectionFollowUp");

				entity.HasOne(d => d.MessengerJobTable)
					.WithMany(p => p.JobChequeMessengerJobTable)
					.HasForeignKey(d => d.MessengerJobTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_JobChequeMessengerJobTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.JobChequeCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_JobChequeCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.JobChequeBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_JobChequeBusinessUnit");
			});
			modelBuilder.Entity<JobType>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.JobTypeGUID);

				entity.Property(e => e.JobTypeGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.JobTypeCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_JobTypeCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.JobTypeBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_JobTypeBusinessUnit");
			});
			modelBuilder.Entity<JointVentureTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.AuthorizedPersonTypeGUID);
				entity.HasIndex(e => e.JointVentureTransGUID);
				entity.HasIndex(e => e.RefGUID);

				entity.Property(e => e.JointVentureTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.AuthorizedPersonType)
					.WithMany(p => p.JointVentureTransAuthorizedPersonType)
					.HasForeignKey(d => d.AuthorizedPersonTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_JointVentureTransAuthorizedPersonType");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.JointVentureTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_JointVentureTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.JointVentureTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_JointVentureTransBusinessUnit");
			});
			modelBuilder.Entity<KYCSetup>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.KYCSetupGUID);

				entity.Property(e => e.KYCSetupGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.KYCSetupCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_KYCSetupCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.KYCSetupBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_KYCSetupBusinessUnit");
			});
			modelBuilder.Entity<Language>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.LanguageGUID);

				entity.Property(e => e.LanguageGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.LanguageCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_LanguageCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.LanguageBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_LanguageBusinessUnit");
			});
			modelBuilder.Entity<LeaseType>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.IncomeNPLToNormalRateGUID);
				entity.HasIndex(e => e.IncomeNormalToNPLRateGUID);
				entity.HasIndex(e => e.PaymentStructureTableGUID);
				entity.HasIndex(e => e.PenaltySetupGUID);
				entity.HasIndex(e => e.TaxCostGUID);
				entity.HasIndex(e => e.TaxInstallmentGUID);
				entity.HasIndex(e => e.TaxNormalToNPLRateGUID);
				entity.HasIndex(e => e.TaxWriteOffGUID);
				entity.HasIndex(e => e.WithholdingTaxTableGUID);

				entity.Property(e => e.LeaseTypeGUID).ValueGeneratedNever();

				entity.HasOne(d => d.TaxCost)
					.WithMany(p => p.LeaseTypeTaxCost)
					.HasForeignKey(d => d.TaxCostGUID)
					.HasConstraintName("FK_LeaseTypeTaxCost");

				entity.HasOne(d => d.TaxInstallment)
					.WithMany(p => p.LeaseTypeTaxInstallment)
					.HasForeignKey(d => d.TaxInstallmentGUID)
					.HasConstraintName("FK_LeaseTypeTaxInstallment");

				entity.HasOne(d => d.TaxWriteOff)
					.WithMany(p => p.LeaseTypeTaxWriteOff)
					.HasForeignKey(d => d.TaxWriteOffGUID)
					.HasConstraintName("FK_LeaseTypeTaxWriteOff");

				entity.HasOne(d => d.WithholdingTaxTable)
					.WithMany(p => p.LeaseTypeWithholdingTaxTable)
					.HasForeignKey(d => d.WithholdingTaxTableGUID)
					.HasConstraintName("FK_LeaseTypeWithholdingTaxTable");
			});
			modelBuilder.Entity<LedgerDimension>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.LedgerDimensionGUID);

				entity.Property(e => e.LedgerDimensionGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.LedgerDimensionCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_LedgerDimensionCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.LedgerDimensionBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_LedgerDimensionBusinessUnit");
			});
			modelBuilder.Entity<LedgerFiscalPeriod>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.LedgerFiscalPeriodGUID);

				entity.Property(e => e.LedgerFiscalPeriodGUID).ValueGeneratedNever();

				entity.HasOne(d => d.LedgerFiscalYear)
					.WithMany(p => p.LedgerFiscalPeriodLedgerFiscalYear)
					.HasForeignKey(d => d.LedgerFiscalYearGUID)
					.HasConstraintName("FK_LedgerFiscalPeriodLedgerFiscalYear");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.LedgerFiscalPeriodCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_LedgerFiscalPeriodCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.LedgerFiscalPeriodBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_LedgerFiscalPeriodBusinessUnit");
			});
			modelBuilder.Entity<LedgerFiscalYear>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.LedgerFiscalYearGUID);

				entity.Property(e => e.LedgerFiscalYearGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.LedgerFiscalYearCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_LedgerFiscalYearCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.LedgerFiscalYearBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_LedgerFiscalYearBusinessUnit");
			});
			modelBuilder.Entity<LineOfBusiness>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.LineOfBusinessGUID);

				entity.Property(e => e.LineOfBusinessGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.LineOfBusinessCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_LineOfBusinessCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.LineOfBusinessBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_LineOfBusinessBusinessUnit");
			});
			modelBuilder.Entity<MainAgreementTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BuyerTableGUID);
				entity.HasIndex(e => e.ConsortiumTableGUID);
				entity.HasIndex(e => e.CreditAppRequestTableGUID);
				entity.HasIndex(e => e.CreditAppTableGUID);
				entity.HasIndex(e => e.CreditLimitTypeGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.DocumentReasonGUID);
				entity.HasIndex(e => e.DocumentStatusGUID);
				entity.HasIndex(e => e.MainAgreementTableGUID);
				entity.HasIndex(e => e.RefMainAgreementTableGUID);
				entity.HasIndex(e => e.WithdrawalTableGUID);

				entity.Property(e => e.MainAgreementTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.BuyerTable)
					.WithMany(p => p.MainAgreementTableBuyerTable)
					.HasForeignKey(d => d.BuyerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MainAgreementTableBuyerTable");

				entity.HasOne(d => d.ConsortiumTable)
					.WithMany(p => p.MainAgreementTableConsortiumTable)
					.HasForeignKey(d => d.ConsortiumTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MainAgreementTableConsortiumTable");

				entity.HasOne(d => d.CreditAppRequestTable)
					.WithMany(p => p.MainAgreementTableCreditAppRequestTable)
					.HasForeignKey(d => d.CreditAppRequestTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MainAgreementTableCreditAppRequestTable");

				entity.HasOne(d => d.CreditAppTable)
					.WithMany(p => p.MainAgreementTableCreditAppTable)
					.HasForeignKey(d => d.CreditAppTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MainAgreementTableCreditAppTable");

				entity.HasOne(d => d.CreditLimitType)
					.WithMany(p => p.MainAgreementTableCreditLimitType)
					.HasForeignKey(d => d.CreditLimitTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MainAgreementTableCreditLimitType");

				entity.HasOne(d => d.DocumentReason)
					.WithMany(p => p.MainAgreementTableDocumentReason)
					.HasForeignKey(d => d.DocumentReasonGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MainAgreementTableDocumentReason");

				entity.HasOne(d => d.DocumentStatus)
					.WithMany(p => p.MainAgreementTableDocumentStatus)
					.HasForeignKey(d => d.DocumentStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MainAgreementTableDocumentStatus");

				entity.HasOne(d => d.RefMainAgreementTable)
					.WithMany(p => p.MainAgreementTableRefMainAgreementTable)
					.HasForeignKey(d => d.RefMainAgreementTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MainAgreementTableRefMainAgreementTable");

				entity.HasOne(d => d.WithdrawalTable)
					.WithMany(p => p.MainAgreementTableWithdrawalTable)
					.HasForeignKey(d => d.WithdrawalTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MainAgreementTableWithdrawalTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.MainAgreementTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MainAgreementTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.MainAgreementTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MainAgreementTableBusinessUnit");
			});
			modelBuilder.Entity<MaritalStatus>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.MaritalStatusGUID);

				entity.Property(e => e.MaritalStatusGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.MaritalStatusCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MaritalStatusCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.MaritalStatusBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MaritalStatusBusinessUnit");
			});
			modelBuilder.Entity<MemoTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.MemoTransGUID);
				entity.HasIndex(e => e.RefGUID);

				entity.Property(e => e.MemoTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.MemoTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MemoTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.MemoTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MemoTransBusinessUnit");
			});
			modelBuilder.Entity<MessengerJobTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BuyerTableGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.DocumentStatusGUID);
				entity.HasIndex(e => e.JobTypeGUID);
				entity.HasIndex(e => e.MessengerJobTableGUID);
				entity.HasIndex(e => e.MessengerTableGUID);
				entity.HasIndex(e => e.RefCreditAppLineGUID);
				entity.HasIndex(e => e.RefGUID);
				entity.HasIndex(e => e.RefMessengerJobTableGUID);
				entity.HasIndex(e => e.RequestorGUID);

				entity.Property(e => e.MessengerJobTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.AssignmentAgreementTable)
					.WithMany(p => p.MessengerJobTableAssignmentAgreementTable)
					.HasForeignKey(d => d.AssignmentAgreementTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MessengerJobTableAssignmentAgreementTable");

				entity.HasOne(d => d.BuyerTable)
					.WithMany(p => p.MessengerJobTableBuyerTable)
					.HasForeignKey(d => d.BuyerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MessengerJobTableBuyerTable");

				entity.HasOne(d => d.CustomerTable)
					.WithMany(p => p.MessengerJobTableCustomerTable)
					.HasForeignKey(d => d.CustomerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MessengerJobTableCustomerTable");

				entity.HasOne(d => d.DocumentStatus)
					.WithMany(p => p.MessengerJobTableDocumentStatus)
					.HasForeignKey(d => d.DocumentStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MessengerJobTableDocumentStatus");

				entity.HasOne(d => d.JobType)
					.WithMany(p => p.MessengerJobTableJobType)
					.HasForeignKey(d => d.JobTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MessengerJobTableJobType");

				entity.HasOne(d => d.MessengerTable)
					.WithMany(p => p.MessengerJobTableMessengerTable)
					.HasForeignKey(d => d.MessengerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MessengerJobTableMessengerTable");

				entity.HasOne(d => d.RefCreditAppLine)
					.WithMany(p => p.MessengerJobTableRefCreditAppLine)
					.HasForeignKey(d => d.RefCreditAppLineGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MessengerJobTableRefCreditAppLine");

				entity.HasOne(d => d.RefMessengerJobTable)
					.WithMany(p => p.MessengerJobTableRefMessengerJobTable)
					.HasForeignKey(d => d.RefMessengerJobTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MessengerJobTableRefMessengerJobTable");

				entity.HasOne(d => d.Requestor)
					.WithMany(p => p.MessengerJobTableRequestor)
					.HasForeignKey(d => d.RequestorGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MessengerJobTableEmployeeTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.MessengerJobTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MessengerJobTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.MessengerJobTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MessengerJobTableBusinessUnit");
			});
			modelBuilder.Entity<MessengerTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);

				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.MessengerTableGUID);
				entity.HasIndex(e => e.VendorTableGUID);

				entity.Property(e => e.MessengerTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.VendorTable)
					.WithMany(p => p.MessengerTableVendorTable)
					.HasForeignKey(d => d.VendorTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MessengerTableVendorTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.MessengerTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MessengerTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.MessengerTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MessengerTableBusinessUnit");
			});
			modelBuilder.Entity<MethodOfPayment>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.CompanyBankGUID);
				entity.HasIndex(e => e.MethodOfPaymentGUID);

				entity.Property(e => e.MethodOfPaymentGUID).ValueGeneratedNever();

				entity.HasOne(d => d.CompanyBank)
					.WithMany(p => p.MethodOfPaymentCompanyBank)
					.HasForeignKey(d => d.CompanyBankGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MethodOfPaymentCompanyBank");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.MethodOfPaymentCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MethodOfPaymentCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.MethodOfPaymentBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_MethodOfPaymentBusinessUnit");
			});
			modelBuilder.Entity<MigrationTable>(entity =>
			{
				entity.Property(e => e.MigrationTableGUID).ValueGeneratedNever();
			});
			modelBuilder.Entity<Nationality>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.NationalityGUID);

				entity.Property(e => e.NationalityGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.NationalityCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_NationalityCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.NationalityBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_NationalityBusinessUnit");
			});
			modelBuilder.Entity<NCBAccountStatus>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.NCBAccountStatusGUID);

				entity.Property(e => e.NCBAccountStatusGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.NCBAccountStatusCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_NCBAccountStatusCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.NCBAccountStatusBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_NCBAccountStatusBusinessUnit");
			});
			modelBuilder.Entity<NCBTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BankGroupGUID);
				entity.HasIndex(e => e.CreditTypeGUID);
				entity.HasIndex(e => e.NCBAccountStatusGUID);
				entity.HasIndex(e => e.NCBTransGUID);
				entity.HasIndex(e => e.RefGUID);

				entity.Property(e => e.NCBTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.BankGroup)
					.WithMany(p => p.NCBTransBankGroup)
					.HasForeignKey(d => d.BankGroupGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_NCBTransBankGroup");

				entity.HasOne(d => d.CreditType)
					.WithMany(p => p.NCBTransCreditType)
					.HasForeignKey(d => d.CreditTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_NCBTransCreditType");

				entity.HasOne(d => d.NCBAccountStatus)
					.WithMany(p => p.NCBTransNCBAccountStatus)
					.HasForeignKey(d => d.NCBAccountStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_NCBTransNCBAccountStatus");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.NCBTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_NCBTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.NCBTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_NCBTransBusinessUnit");
			});
			modelBuilder.Entity<NumberSeqParameter>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.NumberSeqTableGUID);

				entity.Property(e => e.NumberSeqParameterGUID).ValueGeneratedNever();

				entity.HasOne(d => d.NumberSeqTable)
					.WithMany(p => p.NumberSeqParameterNumberSeqTable)
					.HasForeignKey(d => d.NumberSeqTableGUID)
					.HasConstraintName("FK_NumberSeqParameterNumberSeqTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.NumberSeqParameterCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_NumberSeqParameterCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.NumberSeqParameterBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_NumberSeqParameterBusinessUnit");
			});
			modelBuilder.Entity<NumberSeqSegment>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.NumberSeqTableGUID);

				entity.Property(e => e.NumberSeqSegmentGUID).ValueGeneratedNever();

				entity.HasOne(d => d.NumberSeqTable)
					.WithMany(p => p.NumberSeqSegmentNumberSeqTable)
					.HasForeignKey(d => d.NumberSeqTableGUID)
					.HasConstraintName("FK_NumberSeqSegmentNumberSeqTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.NumberSeqSegmentCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_NumberSeqSegmentCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.NumberSeqSegmentBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_NumberSeqSegmentBusinessUnit");
			});
			modelBuilder.Entity<NumberSeqSetupByProductType>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.CreditAppNumberSeqGUID);
				entity.HasIndex(e => e.CreditAppRequestNumberSeqGUID);
				entity.HasIndex(e => e.GuarantorAgreementNumberSeqGUID);
				entity.HasIndex(e => e.InternalGuarantorAgreementNumberSeqGUID);
				entity.HasIndex(e => e.InternalMainAgreementNumberSeqGUID);
				entity.HasIndex(e => e.MainAgreementNumberSeqGUID);
				entity.HasIndex(e => e.NumberSeqSetupByProductTypeGUID);

				entity.Property(e => e.NumberSeqSetupByProductTypeGUID).ValueGeneratedNever();

				entity.HasOne(d => d.CreditAppNumberSeq)
					.WithMany(p => p.NumberSeqSetupByProductTypeCreditAppNumberSeq)
					.HasForeignKey(d => d.CreditAppNumberSeqGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_NumberSeqSetupByProductTypeCreditAppNumberSeq");

				entity.HasOne(d => d.CreditAppRequestNumberSeq)
					.WithMany(p => p.NumberSeqSetupByProductTypeCreditAppRequestNumberSeq)
					.HasForeignKey(d => d.CreditAppRequestNumberSeqGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_NumberSeqSetupByProductTypeCreditAppRequestNumberSeq");

				entity.HasOne(d => d.GuarantorAgreementNumberSeq)
					.WithMany(p => p.NumberSeqSetupByProductTypeGuarantorAgreementNumberSeq)
					.HasForeignKey(d => d.GuarantorAgreementNumberSeqGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_NumberSeqSetupByProductTypeGuarantorAgreementNumberSeq");

				entity.HasOne(d => d.InternalGuarantorAgreementNumberSeq)
					.WithMany(p => p.NumberSeqSetupByProductTypeInternalGuarantorAgreementNumberSeq)
					.HasForeignKey(d => d.InternalGuarantorAgreementNumberSeqGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_NumberSeqSetupByProductTypeInternalGuarantorAgreementNumberSeq");

				entity.HasOne(d => d.InternalMainAgreementNumberSeq)
					.WithMany(p => p.NumberSeqSetupByProductTypeInternalMainAgreementNumberSeq)
					.HasForeignKey(d => d.InternalMainAgreementNumberSeqGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_NumberSeqSetupByProductTypeInternalMainAgreementNumberSeq");

				entity.HasOne(d => d.MainAgreementNumberSeq)
					.WithMany(p => p.NumberSeqSetupByProductTypeMainAgreementNumberSeq)
					.HasForeignKey(d => d.MainAgreementNumberSeqGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_NumberSeqSetupByProductTypeMainAgreementNumberSeq");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.NumberSeqSetupByProductTypeCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_NumberSeqSetupByProductTypeCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.NumberSeqSetupByProductTypeBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_NumberSeqSetupByProductTypeBusinessUnit");
			});
			modelBuilder.Entity<NumberSeqTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);

				entity.Property(e => e.NumberSeqTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.NumberSeqTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_NumberSeqTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.NumberSeqTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_NumberSeqTableBusinessUnit");
			});
			modelBuilder.Entity<Occupation>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.OccupationGUID);

				entity.Property(e => e.OccupationGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.OccupationCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_OccupationCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.OccupationBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_OccupationBusinessUnit");
			});
			modelBuilder.Entity<Ownership>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.OwnershipGUID);

				entity.Property(e => e.OwnershipGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.OwnershipCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_OwnershipCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.OwnershipBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_OwnershipBusinessUnit");
			});
			modelBuilder.Entity<OwnerTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.OwnerTransGUID);
				entity.HasIndex(e => e.RefGUID);
				entity.HasIndex(e => e.RelatedPersonTableGUID);

				entity.Property(e => e.OwnerTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.RelatedPersonTable)
					.WithMany(p => p.OwnerTransRelatedPersonTable)
					.HasForeignKey(d => d.RelatedPersonTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_OwnerTransRelatedPersonTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.OwnerTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_OwnerTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.OwnerTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_OwnerTransBusinessUnit");
			});
			modelBuilder.Entity<ParentCompany>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.ParentCompanyGUID);

				entity.Property(e => e.ParentCompanyGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.ParentCompanyCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ParentCompanyCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.ParentCompanyBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ParentCompanyBusinessUnit");
			});
			modelBuilder.Entity<PaymentDetail>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.InvoiceTypeGUID);
				entity.HasIndex(e => e.PaymentDetailGUID);
				entity.HasIndex(e => e.RefGUID);
				entity.HasIndex(e => e.VendorTableGUID);

				entity.Property(e => e.PaymentDetailGUID).ValueGeneratedNever();

				entity.HasOne(d => d.CustomerTable)
					.WithMany(p => p.PaymentDetailCustomerTable)
					.HasForeignKey(d => d.CustomerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PaymentDetailCustomerTable");

				entity.HasOne(d => d.InvoiceType)
					.WithMany(p => p.PaymentDetailInvoiceType)
					.HasForeignKey(d => d.InvoiceTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PaymentDetailInvoiceType");

				entity.HasOne(d => d.VendorTable)
					.WithMany(p => p.PaymentDetailVendorTable)
					.HasForeignKey(d => d.VendorTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PaymentDetailVendorTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.PaymentDetailCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PaymentDetailCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.PaymentDetailBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PaymentDetailBusinessUnit");
			});
			modelBuilder.Entity<PaymentFrequency>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.PaymentFrequencyGUID);

				entity.Property(e => e.PaymentFrequencyGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.PaymentFrequencyCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PaymentFrequencyCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.PaymentFrequencyBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PaymentFrequencyBusinessUnit");
			});
			modelBuilder.Entity<PaymentHistory>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BranchGUID);
				entity.HasIndex(e => e.CreditAppTableGUID);
				entity.HasIndex(e => e.CurrencyGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.CustTransGUID);
				entity.HasIndex(e => e.InvoiceTableGUID);
				entity.HasIndex(e => e.PaymentHistoryGUID);
				entity.HasIndex(e => e.ReceiptTableGUID);
				entity.HasIndex(e => e.RefGUID);
				entity.HasIndex(e => e.WithholdingTaxTableGUID);

				entity.Property(e => e.PaymentHistoryGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Branch)
					.WithMany(p => p.PaymentHistoryBranch)
					.HasForeignKey(d => d.BranchGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PaymentHistoryBranch");

				entity.HasOne(d => d.CreditAppTable)
					.WithMany(p => p.PaymentHistoryCreditAppTable)
					.HasForeignKey(d => d.CreditAppTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PaymentHistoryCreditAppTable");

				entity.HasOne(d => d.Currency)
					.WithMany(p => p.PaymentHistoryCurrency)
					.HasForeignKey(d => d.CurrencyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PaymentHistoryCurrency");

				entity.HasOne(d => d.CustomerTable)
					.WithMany(p => p.PaymentHistoryCustomerTable)
					.HasForeignKey(d => d.CustomerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PaymentHistoryCustomerTable");

				entity.HasOne(d => d.InvoiceTable)
					.WithMany(p => p.PaymentHistoryInvoiceTable)
					.HasForeignKey(d => d.InvoiceTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PaymentHistoryInvoiceTable");

				entity.HasOne(d => d.ReceiptTable)
					.WithMany(p => p.PaymentHistoryReceiptTable)
					.HasForeignKey(d => d.ReceiptTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PaymentHistoryReceiptTable");

				entity.HasOne(d => d.WithholdingTaxTable)
					.WithMany(p => p.PaymentHistoryWithholdingTaxTable)
					.HasForeignKey(d => d.WithholdingTaxTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PaymentHistoryWithholdingTaxTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.PaymentHistoryCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PaymentHistoryCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.PaymentHistoryBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PaymentHistoryBusinessUnit");
			});
			modelBuilder.Entity<ProcessTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.CreditAppTableGUID);
				entity.HasIndex(e => e.CurrencyGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.DocumentReasonGUID);
				entity.HasIndex(e => e.OrigTaxTableGUID);
				entity.HasIndex(e => e.PaymentDetailGUID);
				entity.HasIndex(e => e.PaymentHistoryGUID);
				entity.HasIndex(e => e.ProcessTransGUID);
				entity.HasIndex(e => e.RefGUID);
				entity.HasIndex(e => e.RefProcessTransGUID);
				entity.HasIndex(e => e.RefTaxInvoiceGUID);
				entity.HasIndex(e => e.TaxTableGUID);

				entity.Property(e => e.ProcessTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.CreditAppTable)
					.WithMany(p => p.ProcessTransCreditAppTable)
					.HasForeignKey(d => d.CreditAppTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ProcessTransCreditAppTable");

				entity.HasOne(d => d.Currency)
					.WithMany(p => p.ProcessTransCurrency)
					.HasForeignKey(d => d.CurrencyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ProcessTransCurrency");

				entity.HasOne(d => d.DocumentReason)
					.WithMany(p => p.ProcessTransDocumentReason)
					.HasForeignKey(d => d.DocumentReasonGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ProcessTransDocumentReason");

				entity.HasOne(d => d.OrigTaxTable)
					.WithMany(p => p.ProcessTransOrigTaxTable)
					.HasForeignKey(d => d.OrigTaxTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ProcessTransOrigTaxTable");

				entity.HasOne(d => d.PaymentDetail)
					.WithMany(p => p.ProcessTransPaymentDetail)
					.HasForeignKey(d => d.PaymentDetailGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ProcessTransPaymentDetail");

				entity.HasOne(d => d.PaymentHistory)
					.WithMany(p => p.ProcessTransPaymentHistory)
					.HasForeignKey(d => d.PaymentHistoryGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ProcessTransPaymentHistory");

				entity.HasOne(d => d.RefProcessTrans)
					.WithMany(p => p.ProcessTransRefProcessTrans)
					.HasForeignKey(d => d.RefProcessTransGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ProcessTransRefProcessTrans");

				entity.HasOne(d => d.RefTaxInvoice)
					.WithMany(p => p.ProcessTransRefTaxInvoice)
					.HasForeignKey(d => d.RefTaxInvoiceGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ProcessTransRefTaxInvoice");

				entity.HasOne(d => d.TaxTable)
					.WithMany(p => p.ProcessTransTaxTable)
					.HasForeignKey(d => d.TaxTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ProcessTransTaxTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.ProcessTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ProcessTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.ProcessTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ProcessTransBusinessUnit");
			});
			modelBuilder.Entity<ProductSettledTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.InvoiceSettlementDetailGUID);
				entity.HasIndex(e => e.OriginalRefGUID);
				entity.HasIndex(e => e.ProductSettledTransGUID);
				entity.HasIndex(e => e.RefGUID);

				entity.Property(e => e.ProductSettledTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.InvoiceSettlementDetail)
					.WithMany(p => p.ProductSettledTransInvoiceSettlementDetail)
					.HasForeignKey(d => d.InvoiceSettlementDetailGUID)
					.HasConstraintName("FK_ProductSettledTransInvoiceSettlementDetail");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.ProductSettledTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ProductSettledTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.ProductSettledTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ProductSettledTransBusinessUnit");
			});
			modelBuilder.Entity<ProductSubType>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.ProductSubTypeGUID);

				entity.Property(e => e.ProductSubTypeGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.ProductSubTypeCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ProductSubTypeCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.ProductSubTypeBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ProductSubTypeBusinessUnit");
			});
			modelBuilder.Entity<ProdUnit>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.ProdUnitGUID);

				entity.Property(e => e.ProdUnitGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.ProdUnitCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ProdUnitCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.ProdUnitBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ProdUnitBusinessUnit");
			});
			modelBuilder.Entity<ProjectProgressTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.ProjectProgressTableGUID);
				entity.HasIndex(e => e.WithdrawalTableGUID);

				entity.Property(e => e.ProjectProgressTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.WithdrawalTable)
					.WithMany(p => p.ProjectProgressTableWithdrawalTable)
					.HasForeignKey(d => d.WithdrawalTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ProjectProgressTableWithdrawalTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.ProjectProgressTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ProjectProgressTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.ProjectProgressTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ProjectProgressTableBusinessUnit");
			});
			modelBuilder.Entity<ProjectReferenceTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.ProjectReferenceTransGUID);
				entity.HasIndex(e => e.RefGUID);

				entity.Property(e => e.ProjectReferenceTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.ProjectReferenceTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ProjectReferenceTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.ProjectReferenceTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ProjectReferenceTransBusinessUnit");
			});
			modelBuilder.Entity<PropertyType>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.PropertyTypeGUID);

				entity.Property(e => e.PropertyTypeGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.PropertyTypeCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PropertyTypeCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.PropertyTypeBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PropertyTypeBusinessUnit");
			});
			modelBuilder.Entity<PurchaseLine>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.AssignmentAgreementTableGUID);
				entity.HasIndex(e => e.BuyerAgreementTableGUID);
				entity.HasIndex(e => e.BuyerInvoiceTableGUID);
				entity.HasIndex(e => e.BuyerPDCTableGUID);
				entity.HasIndex(e => e.BuyerTableGUID);
				entity.HasIndex(e => e.CreditAppLineGUID);
				entity.HasIndex(e => e.CustomerPDCTableGUID);
				entity.HasIndex(e => e.Dimension1GUID);
				entity.HasIndex(e => e.Dimension2GUID);
				entity.HasIndex(e => e.Dimension3GUID);
				entity.HasIndex(e => e.Dimension4GUID);
				entity.HasIndex(e => e.Dimension5GUID);
				entity.HasIndex(e => e.MethodOfPaymentGUID);
				entity.HasIndex(e => e.OriginalPurchaseLineGUID);
				entity.HasIndex(e => e.PurchaseLineGUID);
				entity.HasIndex(e => e.PurchaseLineInvoiceTableGUID);
				entity.HasIndex(e => e.PurchaseTableGUID);
				entity.HasIndex(e => e.RefPurchaseLineGUID);
				entity.HasIndex(e => e.RollbillPurchaseLineGUID);

				entity.Property(e => e.PurchaseLineGUID).ValueGeneratedNever();

				entity.HasOne(d => d.AssignmentAgreementTable)
					.WithMany(p => p.PurchaseLineAssignmentAgreementTable)
					.HasForeignKey(d => d.AssignmentAgreementTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseLineAssignmentAgreementTable");

				entity.HasOne(d => d.BuyerAgreementTable)
					.WithMany(p => p.PurchaseLineBuyerAgreementTable)
					.HasForeignKey(d => d.BuyerAgreementTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseLineBuyerAgreementTable");

				entity.HasOne(d => d.BuyerPDCTable)
					.WithMany(p => p.PurchaseLineBuyerPDCTable)
					.HasForeignKey(d => d.BuyerPDCTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseLineBuyerPDCTable");

				entity.HasOne(d => d.BuyerTable)
					.WithMany(p => p.PurchaseLineBuyerTable)
					.HasForeignKey(d => d.BuyerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseLineBuyerTable");

				entity.HasOne(d => d.CreditAppLine)
					.WithMany(p => p.PurchaseLineCreditAppLine)
					.HasForeignKey(d => d.CreditAppLineGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseLineCreditAppLine");

				entity.HasOne(d => d.CustomerPDCTable)
					.WithMany(p => p.PurchaseLineCustomerPDCTable)
					.HasForeignKey(d => d.CustomerPDCTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseLineCustomerPDCTable");

				entity.HasOne(d => d.LedgerDimension1)
					.WithMany(p => p.PurchaseLineDimension1)
					.HasForeignKey(d => d.Dimension1GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseLineLedgerDimension1");

				entity.HasOne(d => d.LedgerDimension2)
					.WithMany(p => p.PurchaseLineDimension2)
					.HasForeignKey(d => d.Dimension2GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseLineLedgerDimension2");

				entity.HasOne(d => d.LedgerDimension3)
					.WithMany(p => p.PurchaseLineDimension3)
					.HasForeignKey(d => d.Dimension3GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseLineLedgerDimension3");

				entity.HasOne(d => d.LedgerDimension4)
					.WithMany(p => p.PurchaseLineDimension4)
					.HasForeignKey(d => d.Dimension4GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseLineLedgerDimension4");

				entity.HasOne(d => d.LedgerDimension5)
					.WithMany(p => p.PurchaseLineDimension5)
					.HasForeignKey(d => d.Dimension5GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseLineLedgerDimension5");

				entity.HasOne(d => d.MethodOfPayment)
					.WithMany(p => p.PurchaseLineMethodOfPayment)
					.HasForeignKey(d => d.MethodOfPaymentGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseLineMethodOfPayment");

				entity.HasOne(d => d.OriginalPurchaseLine)
					.WithMany(p => p.PurchaseLineOriginalPurchaseLine)
					.HasForeignKey(d => d.OriginalPurchaseLineGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseLineOriginalPurchaseLine");

				entity.HasOne(d => d.InvoiceTable)
					.WithMany(p => p.PurchaseLineInvoiceTable)
					.HasForeignKey(d => d.PurchaseLineInvoiceTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseLineInvoiceTable");

				entity.HasOne(d => d.PurchaseTable)
					.WithMany(p => p.PurchaseLinePurchaseTable)
					.HasForeignKey(d => d.PurchaseTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseLinePurchaseTable");

				entity.HasOne(d => d.RefPurchaseLine)
					.WithMany(p => p.PurchaseLineRefPurchaseLine)
					.HasForeignKey(d => d.RefPurchaseLineGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseLineRefPurchaseLine");

				entity.HasOne(d => d.RollbillPurchaseLine)
					.WithMany(p => p.PurchaseLineRollbillPurchaseLine)
					.HasForeignKey(d => d.RollbillPurchaseLineGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseLineRollbillPurchaseLine");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.PurchaseLineCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseLineCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.PurchaseLineBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseLineBusinessUnit");
			});
			modelBuilder.Entity<PurchaseTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.CreditAppTableGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.Dimension1GUID);
				entity.HasIndex(e => e.Dimension2GUID);
				entity.HasIndex(e => e.Dimension3GUID);
				entity.HasIndex(e => e.Dimension4GUID);
				entity.HasIndex(e => e.Dimension5GUID);
				entity.HasIndex(e => e.DocumentStatusGUID);
				entity.HasIndex(e => e.DocumentReasonGUID);
				entity.HasIndex(e => e.PurchaseTableGUID);
				entity.HasIndex(e => e.ReceiptTempTableGUID);
				entity.HasIndex(e => e.OperReportSignatureGUID);

				entity.Property(e => e.PurchaseTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.CreditAppTable)
					.WithMany(p => p.PurchaseTableCreditAppTable)
					.HasForeignKey(d => d.CreditAppTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseTableCreditAppTable");

				entity.HasOne(d => d.CustomerTable)
					.WithMany(p => p.PurchaseTableCustomerTable)
					.HasForeignKey(d => d.CustomerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseTableCustomerTable");

				entity.HasOne(d => d.LedgerDimension1)
					.WithMany(p => p.PurchaseTableDimension1)
					.HasForeignKey(d => d.Dimension1GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseTableLedgerDimension1");

				entity.HasOne(d => d.LedgerDimension2)
					.WithMany(p => p.PurchaseTableDimension2)
					.HasForeignKey(d => d.Dimension2GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseTableLedgerDimension2");

				entity.HasOne(d => d.LedgerDimension3)
					.WithMany(p => p.PurchaseTableDimension3)
					.HasForeignKey(d => d.Dimension3GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseTableLedgerDimension3");

				entity.HasOne(d => d.LedgerDimension4)
					.WithMany(p => p.PurchaseTableDimension4)
					.HasForeignKey(d => d.Dimension4GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseTableLedgerDimension4");

				entity.HasOne(d => d.LedgerDimension5)
					.WithMany(p => p.PurchaseTableDimension5)
					.HasForeignKey(d => d.Dimension5GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseTableLedgerDimension5");

				entity.HasOne(d => d.DocumentStatus)
					.WithMany(p => p.PurchaseTableDocumentStatus)
					.HasForeignKey(d => d.DocumentStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseTableDocumentStatus");

				entity.HasOne(d => d.DocumentReason)
					.WithMany(p => p.PurchaseTableDocumentReason)
					.HasForeignKey(d => d.DocumentReasonGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseTableDocumentReason");

				entity.HasOne(d => d.ReceiptTempTable)
					.WithMany(p => p.PurchaseTableReceiptTempTable)
					.HasForeignKey(d => d.ReceiptTempTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseTableReceiptTempTable");

				entity.HasOne(d => d.EmployeeTable)
					.WithMany(p => p.PurchaseTableEmployeeTable)
					.HasForeignKey(d => d.OperReportSignatureGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseTableEmployeeTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.PurchaseTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.PurchaseTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_PurchaseTableBusinessUnit");
			});
			modelBuilder.Entity<Race>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.RaceGUID);

				entity.Property(e => e.RaceGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.RaceCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_RaceCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.RaceBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_RaceBusinessUnit");
			});
			modelBuilder.Entity<ReceiptLine>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BranchGUID);
				entity.HasIndex(e => e.InvoiceTableGUID);
				entity.HasIndex(e => e.ReceiptLineGUID);
				entity.HasIndex(e => e.ReceiptTableGUID);
				entity.HasIndex(e => e.TaxTableGUID);
				entity.HasIndex(e => e.WithholdingTaxTableGUID);

				entity.Property(e => e.ReceiptLineGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Branch)
					.WithMany(p => p.ReceiptLineBranch)
					.HasForeignKey(d => d.BranchGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptLineBranch");

				entity.HasOne(d => d.InvoiceTable)
					.WithMany(p => p.ReceiptLineInvoiceTable)
					.HasForeignKey(d => d.InvoiceTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptLineInvoiceTable");

				entity.HasOne(d => d.ReceiptTable)
					.WithMany(p => p.ReceiptLineReceiptTable)
					.HasForeignKey(d => d.ReceiptTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptLineReceiptTable");

				entity.HasOne(d => d.TaxTable)
					.WithMany(p => p.ReceiptLineTaxTable)
					.HasForeignKey(d => d.TaxTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptLineTaxTable");

				entity.HasOne(d => d.WithholdingTaxTable)
					.WithMany(p => p.ReceiptLineWithholdingTaxTable)
					.HasForeignKey(d => d.WithholdingTaxTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptLineWithholdingTaxTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.ReceiptLineCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptLineCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.ReceiptLineBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptLineBusinessUnit");
			});
			modelBuilder.Entity<ReceiptTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BranchGUID);
				entity.HasIndex(e => e.ChequeBankGroupGUID);
				entity.HasIndex(e => e.CurrencyGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.DocumentStatusGUID);
				entity.HasIndex(e => e.InvoiceSettlementDetailGUID);
				entity.HasIndex(e => e.MethodOfPaymentGUID);
				entity.HasIndex(e => e.ReceiptTableGUID);
				entity.HasIndex(e => e.RefGUID);

				entity.Property(e => e.ReceiptTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Branch)
					.WithMany(p => p.ReceiptTableBranch)
					.HasForeignKey(d => d.BranchGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTableBranch");

				entity.HasOne(d => d.ChequeBankGroup)
					.WithMany(p => p.ReceiptTableChequeBankGroup)
					.HasForeignKey(d => d.ChequeBankGroupGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTableBankGroup");

				entity.HasOne(d => d.Currency)
					.WithMany(p => p.ReceiptTableCurrency)
					.HasForeignKey(d => d.CurrencyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTableCurrency");

				entity.HasOne(d => d.CustomerTable)
					.WithMany(p => p.ReceiptTableCustomerTable)
					.HasForeignKey(d => d.CustomerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTableCustomerTable");

				entity.HasOne(d => d.DocumentStatus)
					.WithMany(p => p.ReceiptTableDocumentStatus)
					.HasForeignKey(d => d.DocumentStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTableDocumentStatus");

				entity.HasOne(d => d.InvoiceSettlementDetail)
					.WithMany(p => p.ReceiptTableInvoiceSettlementDetail)
					.HasForeignKey(d => d.InvoiceSettlementDetailGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTableInvoiceSettlementDetail");

				entity.HasOne(d => d.MethodOfPayment)
					.WithMany(p => p.ReceiptTableMethodOfPayment)
					.HasForeignKey(d => d.MethodOfPaymentGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTableMethodOfPayment");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.ReceiptTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.ReceiptTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTableBusinessUnit");
			});
			modelBuilder.Entity<ReceiptTempPaymDetail>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.ChequeBankGroupGUID);
				entity.HasIndex(e => e.ChequeTableGUID);
				entity.HasIndex(e => e.MethodOfPaymentGUID);
				entity.HasIndex(e => e.ReceiptTempPaymDetailGUID);
				entity.HasIndex(e => e.ReceiptTempTableGUID);

				entity.Property(e => e.ReceiptTempPaymDetailGUID).ValueGeneratedNever();

				entity.HasOne(d => d.ChequeBankGroup)
					.WithMany(p => p.ReceiptTempPaymDetailChequeBankGroup)
					.HasForeignKey(d => d.ChequeBankGroupGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTempPaymDetailBankGroup");

				entity.HasOne(d => d.ChequeTable)
					.WithMany(p => p.ReceiptTempPaymDetailChequeTable)
					.HasForeignKey(d => d.ChequeTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTempPaymDetailChequeTable");

				entity.HasOne(d => d.MethodOfPayment)
					.WithMany(p => p.ReceiptTempPaymDetailMethodOfPayment)
					.HasForeignKey(d => d.MethodOfPaymentGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTempPaymDetailMethodOfPayment");

				entity.HasOne(d => d.ReceiptTempTable)
					.WithMany(p => p.ReceiptTempPaymDetailReceiptTempTable)
					.HasForeignKey(d => d.ReceiptTempTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTempPaymDetailReceiptTempTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.ReceiptTempPaymDetailCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTempPaymDetailCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.ReceiptTempPaymDetailBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTempPaymDetailBusinessUnit");
			});
			modelBuilder.Entity<ReceiptTempTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BuyerReceiptTableGUID);
				entity.HasIndex(e => e.BuyerTableGUID);
				entity.HasIndex(e => e.CreditAppTableGUID);
				entity.HasIndex(e => e.CurrencyGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.Dimension1GUID);
				entity.HasIndex(e => e.Dimension2GUID);
				entity.HasIndex(e => e.Dimension3GUID);
				entity.HasIndex(e => e.Dimension4GUID);
				entity.HasIndex(e => e.Dimension5GUID);
				entity.HasIndex(e => e.DocumentReasonGUID);
				entity.HasIndex(e => e.DocumentStatusGUID);
				entity.HasIndex(e => e.MainReceiptTempGUID);
				entity.HasIndex(e => e.ReceiptTempTableGUID);
				entity.HasIndex(e => e.RefReceiptTempGUID);
				entity.HasIndex(e => e.SuspenseInvoiceTypeGUID);

				entity.Property(e => e.ReceiptTempTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.BuyerReceiptTable)
					.WithMany(p => p.ReceiptTempTableBuyerReceiptTable)
					.HasForeignKey(d => d.BuyerReceiptTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTempTableBuyerReceiptTable");

				entity.HasOne(d => d.BuyerTable)
					.WithMany(p => p.ReceiptTempTableBuyerTable)
					.HasForeignKey(d => d.BuyerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTempTableBuyerTable");

				entity.HasOne(d => d.CreditAppTable)
					.WithMany(p => p.ReceiptTempTableCreditAppTable)
					.HasForeignKey(d => d.CreditAppTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTempTableCreditAppTable");

				entity.HasOne(d => d.Currency)
					.WithMany(p => p.ReceiptTempTableCurrency)
					.HasForeignKey(d => d.CurrencyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTempTableCurrency");

				entity.HasOne(d => d.LedgerDimension1)
					.WithMany(p => p.ReceiptTempTableDimension1)
					.HasForeignKey(d => d.Dimension1GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTempTableLedgerDimension1");

				entity.HasOne(d => d.LedgerDimension2)
					.WithMany(p => p.ReceiptTempTableDimension2)
					.HasForeignKey(d => d.Dimension2GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTempTableLedgerDimension2");

				entity.HasOne(d => d.LedgerDimension3)
					.WithMany(p => p.ReceiptTempTableDimension3)
					.HasForeignKey(d => d.Dimension3GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTempTableLedgerDimension3");

				entity.HasOne(d => d.LedgerDimension4)
					.WithMany(p => p.ReceiptTempTableDimension4)
					.HasForeignKey(d => d.Dimension4GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTempTableLedgerDimension4");

				entity.HasOne(d => d.LedgerDimension5)
					.WithMany(p => p.ReceiptTempTableDimension5)
					.HasForeignKey(d => d.Dimension5GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTempTableLedgerDimension5");

				entity.HasOne(d => d.DocumentReason)
					.WithMany(p => p.ReceiptTempTableDocumentReason)
					.HasForeignKey(d => d.DocumentReasonGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTempTableDocumentReason");

				entity.HasOne(d => d.DocumentStatus)
					.WithMany(p => p.ReceiptTempTableDocumentStatus)
					.HasForeignKey(d => d.DocumentStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTempTableDocumentStatus");

				entity.HasOne(d => d.MainReceiptTemp)
					.WithMany(p => p.ReceiptTempTableMainReceiptTemp)
					.HasForeignKey(d => d.MainReceiptTempGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTempTableMainReceiptTemp");

				entity.HasOne(d => d.RefReceiptTemp)
					.WithMany(p => p.ReceiptTempTableRefReceiptTemp)
					.HasForeignKey(d => d.RefReceiptTempGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTempTableReceiptTempTable");

				entity.HasOne(d => d.SuspenseInvoiceType)
					.WithMany(p => p.ReceiptTempTableSuspenseInvoiceType)
					.HasForeignKey(d => d.SuspenseInvoiceTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTempTableInvoiceType");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.ReceiptTempTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTempTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.ReceiptTempTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ReceiptTempTableBusinessUnit");
			});
			modelBuilder.Entity<RegistrationType>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.RegistrationTypeGUID);

				entity.Property(e => e.RegistrationTypeGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.RegistrationTypeCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_RegistrationTypeCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.RegistrationTypeBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_RegistrationTypeBusinessUnit");
			});
			modelBuilder.Entity<RelatedPersonTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.GenderGUID);
				entity.HasIndex(e => e.MaritalStatusGUID);
				entity.HasIndex(e => e.NationalityGUID);
				entity.HasIndex(e => e.OccupationGUID);
				entity.HasIndex(e => e.RaceGUID);
				entity.HasIndex(e => e.RegistrationTypeGUID);
				entity.HasIndex(e => e.RelatedPersonTableGUID);

				entity.Property(e => e.RelatedPersonTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Gender)
					.WithMany(p => p.RelatedPersonTableGender)
					.HasForeignKey(d => d.GenderGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_RelatedPersonTableGender");

				entity.HasOne(d => d.MaritalStatus)
					.WithMany(p => p.RelatedPersonTableMaritalStatus)
					.HasForeignKey(d => d.MaritalStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_RelatedPersonTableMaritalStatus");

				entity.HasOne(d => d.Nationality)
					.WithMany(p => p.RelatedPersonTableNationality)
					.HasForeignKey(d => d.NationalityGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_RelatedPersonTableNationality");

				entity.HasOne(d => d.Occupation)
					.WithMany(p => p.RelatedPersonTableOccupation)
					.HasForeignKey(d => d.OccupationGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_RelatedPersonTableOccupation");

				entity.HasOne(d => d.Race)
					.WithMany(p => p.RelatedPersonTableRace)
					.HasForeignKey(d => d.RaceGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_RelatedPersonTableRace");

				entity.HasOne(d => d.RegistrationType)
					.WithMany(p => p.RelatedPersonTableRegistrationType)
					.HasForeignKey(d => d.RegistrationTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_RelatedPersonTableRegistrationType");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.RelatedPersonTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_RelatedPersonTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.RelatedPersonTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_RelatedPersonTableBusinessUnit");
			});
			modelBuilder.Entity<RetentionConditionSetup>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.RetentionConditionSetupGUID);

				entity.Property(e => e.RetentionConditionSetupGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.RetentionConditionSetupCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_RetentionConditionSetupCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.RetentionConditionSetupBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_RetentionConditionSetupBusinessUnit");
			});
			modelBuilder.Entity<RetentionConditionTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.RefGUID);
				entity.HasIndex(e => e.RetentionConditionTransGUID);

				entity.Property(e => e.RetentionConditionTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.RetentionConditionTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_RetentionConditionTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.RetentionConditionTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_RetentionConditionTransBusinessUnit");
			});
			modelBuilder.Entity<RetentionTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BuyerAgreementTableGUID);
				entity.HasIndex(e => e.BuyerTableGUID);
				entity.HasIndex(e => e.CreditAppTableGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.RefGUID);
				entity.HasIndex(e => e.RetentionTransGUID);

				entity.Property(e => e.RetentionTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.BuyerAgreementTable)
					.WithMany(p => p.RetentionTransBuyerAgreementTable)
					.HasForeignKey(d => d.BuyerAgreementTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_RetentionTransBuyerAgreementTable");

				entity.HasOne(d => d.BuyerTable)
					.WithMany(p => p.RetentionTransBuyerTable)
					.HasForeignKey(d => d.BuyerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_RetentionTransBuyerTable");

				entity.HasOne(d => d.CreditAppTable)
					.WithMany(p => p.RetentionTransCreditAppTable)
					.HasForeignKey(d => d.CreditAppTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_RetentionTransCreditAppTable");

				entity.HasOne(d => d.CustomerTable)
					.WithMany(p => p.RetentionTransCustomerTable)
					.HasForeignKey(d => d.CustomerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_RetentionTransCustomerTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.RetentionTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_RetentionTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.RetentionTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_RetentionTransBusinessUnit");
			});
			modelBuilder.Entity<ServiceFeeConditionTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.CreditAppRequestTableGUID);
				entity.HasIndex(e => e.InvoiceRevenueTypeGUID);
				entity.HasIndex(e => e.RefGUID);
				entity.HasIndex(e => e.RefServiceFeeConditionTransGUID);
				entity.HasIndex(e => e.ServiceFeeConditionTransGUID);

				entity.Property(e => e.ServiceFeeConditionTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.InvoiceRevenueType)
					.WithMany(p => p.ServiceFeeConditionTransInvoiceRevenueType)
					.HasForeignKey(d => d.InvoiceRevenueTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ServiceFeeConditionTransInvoiceRevenueType");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.ServiceFeeConditionTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ServiceFeeConditionTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.ServiceFeeConditionTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ServiceFeeConditionTransBusinessUnit");
			});
			modelBuilder.Entity<ServiceFeeCondTemplateLine>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.InvoiceRevenueTypeGUID);
				entity.HasIndex(e => e.ServiceFeeCondTemplateLineGUID);
				entity.HasIndex(e => e.ServiceFeeCondTemplateTableGUID);

				entity.Property(e => e.ServiceFeeCondTemplateLineGUID).ValueGeneratedNever();

				entity.HasOne(d => d.InvoiceRevenueType)
					.WithMany(p => p.ServiceFeeCondTemplateLineInvoiceRevenueType)
					.HasForeignKey(d => d.InvoiceRevenueTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ServiceFeeCondTemplateLineInvoiceRevenueType");

				entity.HasOne(d => d.ServiceFeeCondTemplateTable)
					.WithMany(p => p.ServiceFeeCondTemplateLineServiceFeeCondTemplateTable)
					.HasForeignKey(d => d.ServiceFeeCondTemplateTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ServiceFeeCondTemplateLineServiceFeeCondTemplateTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.ServiceFeeCondTemplateLineCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ServiceFeeCondTemplateLineCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.ServiceFeeCondTemplateLineBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ServiceFeeCondTemplateLineBusinessUnit");
			});
			modelBuilder.Entity<ServiceFeeCondTemplateTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.ServiceFeeCondTemplateTableGUID);

				entity.Property(e => e.ServiceFeeCondTemplateTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.ServiceFeeCondTemplateTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ServiceFeeCondTemplateTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.ServiceFeeCondTemplateTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ServiceFeeCondTemplateTableBusinessUnit");
			});
			modelBuilder.Entity<ServiceFeeTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.Dimension1GUID);
				entity.HasIndex(e => e.Dimension2GUID);
				entity.HasIndex(e => e.Dimension3GUID);
				entity.HasIndex(e => e.Dimension4GUID);
				entity.HasIndex(e => e.Dimension5GUID);
				entity.HasIndex(e => e.InvoiceRevenueTypeGUID);
				entity.HasIndex(e => e.RefGUID);
				entity.HasIndex(e => e.ServiceFeeTransGUID);
				entity.HasIndex(e => e.TaxTableGUID);
				entity.HasIndex(e => e.WithholdingTaxTableGUID);

				entity.Property(e => e.ServiceFeeTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.DocumentReason)
					.WithMany(p => p.ServiceFeeTransDocumentReason)
					.HasForeignKey(d => d.CNReasonGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ServiceFeeTransDocumentReason");

				entity.HasOne(d => d.LedgerDimension1)
					.WithMany(p => p.ServiceFeeTransDimension1)
					.HasForeignKey(d => d.Dimension1GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ServiceFeeTransLedgerDimension1");

				entity.HasOne(d => d.LedgerDimension2)
					.WithMany(p => p.ServiceFeeTransDimension2)
					.HasForeignKey(d => d.Dimension2GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ServiceFeeTransLedgerDimension2");

				entity.HasOne(d => d.LedgerDimension3)
					.WithMany(p => p.ServiceFeeTransDimension3)
					.HasForeignKey(d => d.Dimension3GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ServiceFeeTransLedgerDimension3");

				entity.HasOne(d => d.LedgerDimension4)
					.WithMany(p => p.ServiceFeeTransDimension4)
					.HasForeignKey(d => d.Dimension4GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ServiceFeeTransLedgerDimension4");

				entity.HasOne(d => d.LedgerDimension5)
					.WithMany(p => p.ServiceFeeTransDimension5)
					.HasForeignKey(d => d.Dimension5GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ServiceFeeTransLedgerDimension5");

				entity.HasOne(d => d.InvoiceRevenueType)
					.WithMany(p => p.ServiceFeeTransInvoiceRevenueType)
					.HasForeignKey(d => d.InvoiceRevenueTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ServiceFeeTransInvoiceRevenueType");

				entity.HasOne(d => d.TaxTable)
					.WithMany(p => p.ServiceFeeTransTaxTable)
					.HasForeignKey(d => d.TaxTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ServiceFeeTransTaxTable");

				entity.HasOne(d => d.WithholdingTaxTable)
					.WithMany(p => p.ServiceFeeTransWithholdingTaxTable)
					.HasForeignKey(d => d.WithholdingTaxTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ServiceFeeTransWithholdingTaxTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.ServiceFeeTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ServiceFeeTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.ServiceFeeTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_ServiceFeeTransBusinessUnit");
			});
			modelBuilder.Entity<TaxInvoiceLine>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BranchGUID);
				entity.HasIndex(e => e.Dimension1GUID);
				entity.HasIndex(e => e.Dimension2GUID);
				entity.HasIndex(e => e.Dimension3GUID);
				entity.HasIndex(e => e.Dimension4GUID);
				entity.HasIndex(e => e.Dimension5GUID);
				entity.HasIndex(e => e.InvoiceRevenueTypeGUID);
				entity.HasIndex(e => e.ProdUnitGUID);
				entity.HasIndex(e => e.TaxInvoiceLineGUID);
				entity.HasIndex(e => e.TaxInvoiceTableGUID);
				entity.HasIndex(e => e.TaxTableGUID);

				entity.Property(e => e.TaxInvoiceLineGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Branch)
					.WithMany(p => p.TaxInvoiceLineBranch)
					.HasForeignKey(d => d.BranchGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxInvoiceLineBranch");

				entity.HasOne(d => d.LedgerDimension1)
					.WithMany(p => p.TaxInvoiceLineDimension1)
					.HasForeignKey(d => d.Dimension1GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxInvoiceLineLedgerDimension1");

				entity.HasOne(d => d.LedgerDimension2)
					.WithMany(p => p.TaxInvoiceLineDimension2)
					.HasForeignKey(d => d.Dimension2GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxInvoiceLineLedgerDimension2");

				entity.HasOne(d => d.LedgerDimension3)
					.WithMany(p => p.TaxInvoiceLineDimension3)
					.HasForeignKey(d => d.Dimension3GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxInvoiceLineLedgerDimension3");

				entity.HasOne(d => d.LedgerDimension4)
					.WithMany(p => p.TaxInvoiceLineDimension4)
					.HasForeignKey(d => d.Dimension4GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxInvoiceLineLedgerDimension4");

				entity.HasOne(d => d.LedgerDimension5)
					.WithMany(p => p.TaxInvoiceLineDimension5)
					.HasForeignKey(d => d.Dimension5GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxInvoiceLineLedgerDimension5");

				entity.HasOne(d => d.InvoiceRevenueType)
					.WithMany(p => p.TaxInvoiceLineInvoiceRevenueType)
					.HasForeignKey(d => d.InvoiceRevenueTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxInvoiceLineInvoiceRevenueType");

				entity.HasOne(d => d.ProdUnit)
					.WithMany(p => p.TaxInvoiceLineProdUnit)
					.HasForeignKey(d => d.ProdUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxInvoiceLineProdUnit");

				entity.HasOne(d => d.TaxInvoiceTable)
					.WithMany(p => p.TaxInvoiceLineTaxInvoiceTable)
					.HasForeignKey(d => d.TaxInvoiceTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxInvoiceLineTaxInvoiceTable");

				entity.HasOne(d => d.TaxTable)
					.WithMany(p => p.TaxInvoiceLineTaxTable)
					.HasForeignKey(d => d.TaxTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxInvoiceLineTaxTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.TaxInvoiceLineCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxInvoiceLineCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.TaxInvoiceLineBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxInvoiceLineBusinessUnit");
			});
			modelBuilder.Entity<TaxInvoiceTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BranchGUID);
				entity.HasIndex(e => e.CNReasonGUID);
				entity.HasIndex(e => e.CurrencyGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.Dimension1GUID);
				entity.HasIndex(e => e.Dimension2GUID);
				entity.HasIndex(e => e.Dimension3GUID);
				entity.HasIndex(e => e.Dimension4GUID);
				entity.HasIndex(e => e.Dimension5GUID);
				entity.HasIndex(e => e.DocumentStatusGUID);
				entity.HasIndex(e => e.InvoiceTableGUID);
				entity.HasIndex(e => e.InvoiceTypeGUID);
				entity.HasIndex(e => e.MethodOfPaymentGUID);
				entity.HasIndex(e => e.RefTaxInvoiceGUID);
				entity.HasIndex(e => e.TaxInvoiceRefGUID);
				entity.HasIndex(e => e.TaxInvoiceTableGUID);

				entity.Property(e => e.TaxInvoiceTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Branch)
					.WithMany(p => p.TaxInvoiceTableBranch)
					.HasForeignKey(d => d.BranchGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxInvoiceTableBranch");

				entity.HasOne(d => d.CNReason)
					.WithMany(p => p.TaxInvoiceTableCNReason)
					.HasForeignKey(d => d.CNReasonGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxInvoiceTableDocumentReason");

				entity.HasOne(d => d.Currency)
					.WithMany(p => p.TaxInvoiceTableCurrency)
					.HasForeignKey(d => d.CurrencyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxInvoiceTableCurrency");

				entity.HasOne(d => d.CustomerTable)
					.WithMany(p => p.TaxInvoiceTableCustomerTable)
					.HasForeignKey(d => d.CustomerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxInvoiceTableCustomerTable");

				entity.HasOne(d => d.LedgerDimension1)
					.WithMany(p => p.TaxInvoiceTableDimension1)
					.HasForeignKey(d => d.Dimension1GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxInvoiceTableLedgerDimension1");

				entity.HasOne(d => d.LedgerDimension2)
					.WithMany(p => p.TaxInvoiceTableDimension2)
					.HasForeignKey(d => d.Dimension2GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxInvoiceTableLedgerDimension2");

				entity.HasOne(d => d.LedgerDimension3)
					.WithMany(p => p.TaxInvoiceTableDimension3)
					.HasForeignKey(d => d.Dimension3GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxInvoiceTableLedgerDimension3");

				entity.HasOne(d => d.LedgerDimension4)
					.WithMany(p => p.TaxInvoiceTableDimension4)
					.HasForeignKey(d => d.Dimension4GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxInvoiceTableLedgerDimension4");

				entity.HasOne(d => d.LedgerDimension5)
					.WithMany(p => p.TaxInvoiceTableDimension5)
					.HasForeignKey(d => d.Dimension5GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxInvoiceTableLedgerDimension5");

				entity.HasOne(d => d.DocumentStatus)
					.WithMany(p => p.TaxInvoiceTableDocumentStatus)
					.HasForeignKey(d => d.DocumentStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxInvoiceTableDocumentStatus");

				entity.HasOne(d => d.InvoiceTable)
					.WithMany(p => p.TaxInvoiceTableInvoiceTable)
					.HasForeignKey(d => d.InvoiceTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxInvoiceTableInvoiceTable");

				entity.HasOne(d => d.InvoiceType)
					.WithMany(p => p.TaxInvoiceTableInvoiceType)
					.HasForeignKey(d => d.InvoiceTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxInvoiceTableInvoiceType");

				entity.HasOne(d => d.MethodOfPayment)
					.WithMany(p => p.TaxInvoiceTableMethodOfPayment)
					.HasForeignKey(d => d.MethodOfPaymentGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxInvoiceTableMethodOfPayment");

				entity.HasOne(d => d.RefTaxInvoice)
					.WithMany(p => p.TaxInvoiceTableRefTaxInvoice)
					.HasForeignKey(d => d.RefTaxInvoiceGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxInvoiceTableTaxInvoiceTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.TaxInvoiceTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxInvoiceTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.TaxInvoiceTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxInvoiceTableBusinessUnit");
			});
			modelBuilder.Entity<TaxReportTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.RefGUID);
				entity.HasIndex(e => e.TaxReportTransGUID);

				entity.Property(e => e.TaxReportTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.TaxReportTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxReportTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.TaxReportTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxReportTransBusinessUnit");
			});
			modelBuilder.Entity<TaxTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.TaxTableGUID);

				entity.Property(e => e.TaxTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.PaymentTaxTable)
					.WithMany(p => p.TaxTablePaymentTaxTable)
					.HasForeignKey(d => d.PaymentTaxTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxTablePaymentTaxTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.TaxTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.TaxTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxTableBusinessUnit");
			});
			modelBuilder.Entity<TaxValue>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.TaxValueGUID);

				entity.HasIndex(e => e.TaxTableGUID);

				entity.Property(e => e.TaxValueGUID).ValueGeneratedNever();

				entity.HasOne(d => d.TaxTable)
					.WithMany(p => p.TaxValueTaxTable)
					.HasForeignKey(d => d.TaxTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxValueTaxTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.TaxValueCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxValueCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.TaxValueBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TaxValueBusinessUnit");
			});
			modelBuilder.Entity<Territory>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.TerritoryGUID);

				entity.Property(e => e.TerritoryGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.TerritoryCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TerritoryCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.TerritoryBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_TerritoryBusinessUnit");
			});
			modelBuilder.Entity<VendBank>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.VendBankGUID);

				entity.Property(e => e.VendBankGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.VendBankCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VendBankCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.VendBankBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VendBankBusinessUnit");
			});
			modelBuilder.Entity<VendGroup>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.VendGroupGUID);

				entity.Property(e => e.VendGroupGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.VendGroupCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VendGroupCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.VendGroupBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VendGroupBusinessUnit");
			});
			modelBuilder.Entity<VendorPaymentTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.CreditAppTableGUID);
				entity.HasIndex(e => e.DocumentStatusGUID);
				entity.HasIndex(e => e.RefGUID);
				entity.HasIndex(e => e.TaxTableGUID);
				entity.HasIndex(e => e.VendorPaymentTransGUID);
				entity.HasIndex(e => e.VendorTableGUID);

				entity.Property(e => e.VendorPaymentTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.CreditAppTable)
					.WithMany(p => p.VendorPaymentTransCreditAppTable)
					.HasForeignKey(d => d.CreditAppTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VendorPaymentTransCreditAppTable");

				entity.HasOne(d => d.LedgerDimension1)
					.WithMany(p => p.VendorPaymentTransDimension1)
					.HasForeignKey(d => d.Dimension1)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VendorPaymentTransLedgerDimension1");

				entity.HasOne(d => d.LedgerDimension2)
					.WithMany(p => p.VendorPaymentTransDimension2)
					.HasForeignKey(d => d.Dimension2)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VendorPaymentTransLedgerDimension2");

				entity.HasOne(d => d.LedgerDimension3)
					.WithMany(p => p.VendorPaymentTransDimension3)
					.HasForeignKey(d => d.Dimension3)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VendorPaymentTransLedgerDimension3");

				entity.HasOne(d => d.LedgerDimension4)
					.WithMany(p => p.VendorPaymentTransDimension4)
					.HasForeignKey(d => d.Dimension4)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VendorPaymentTransLedgerDimension4");

				entity.HasOne(d => d.LedgerDimension5)
					.WithMany(p => p.VendorPaymentTransDimension5)
					.HasForeignKey(d => d.Dimension5)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VendorPaymentTransLedgerDimension5");

				entity.HasOne(d => d.DocumentStatus)
					.WithMany(p => p.VendorPaymentTransDocumentStatus)
					.HasForeignKey(d => d.DocumentStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VendorPaymentTransDocumentStatus");

				entity.HasOne(d => d.TaxTable)
					.WithMany(p => p.VendorPaymentTransTaxTable)
					.HasForeignKey(d => d.TaxTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VendorPaymentTransTaxTable");

				entity.HasOne(d => d.VendorTable)
					.WithMany(p => p.VendorPaymentTransVendorTable)
					.HasForeignKey(d => d.VendorTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VendorPaymentTransVendorTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.VendorPaymentTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VendorPaymentTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.VendorPaymentTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VendorPaymentTransBusinessUnit");
			});
			modelBuilder.Entity<VendorTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.VendorTableGUID);
				entity.HasIndex(e => e.CurrencyGUID);
				entity.HasIndex(e => e.VendGroupGUID);

				entity.Property(e => e.VendorTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Currency)
					.WithMany(p => p.VendorTableCurrency)
					.HasForeignKey(d => d.CurrencyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VendorTableCurrency");

				entity.HasOne(d => d.VendGroup)
					.WithMany(p => p.VendorTableVendGroup)
					.HasForeignKey(d => d.VendGroupGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VendorTableVendGroup");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.VendorTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VendorTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.VendorTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VendorTableBusinessUnit");
			});
			modelBuilder.Entity<VerificationLine>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.VendorTableGUID);
				entity.HasIndex(e => e.VerificationLineGUID);
				entity.HasIndex(e => e.VerificationTableGUID);
				entity.HasIndex(e => e.VerificationTypeGUID);

				entity.Property(e => e.VerificationLineGUID).ValueGeneratedNever();

				entity.HasOne(d => d.VendorTable)
					.WithMany(p => p.VerificationLineVendorTable)
					.HasForeignKey(d => d.VendorTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VerificationLineVendorTable");

				entity.HasOne(d => d.VerificationTable)
					.WithMany(p => p.VerificationLineVerificationTable)
					.HasForeignKey(d => d.VerificationTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VerificationLineVerificationTable");

				entity.HasOne(d => d.VerificationType)
					.WithMany(p => p.VerificationLineVerificationType)
					.HasForeignKey(d => d.VerificationTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VerificationLineVerificationType");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.VerificationLineCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VerificationLineCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.VerificationLineBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VerificationLineBusinessUnit");
			});
			modelBuilder.Entity<VerificationTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BuyerTableGUID);
				entity.HasIndex(e => e.CreditAppTableGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.DocumentStatusGUID);
				entity.HasIndex(e => e.VerificationTableGUID);

				entity.Property(e => e.VerificationTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.BuyerTable)
					.WithMany(p => p.VerificationTableBuyerTable)
					.HasForeignKey(d => d.BuyerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VerificationTableBuyerTable");

				entity.HasOne(d => d.CreditAppTable)
					.WithMany(p => p.VerificationTableCreditAppTable)
					.HasForeignKey(d => d.CreditAppTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VerificationTableCreditAppTable");

				entity.HasOne(d => d.CustomerTable)
					.WithMany(p => p.VerificationTableCustomerTable)
					.HasForeignKey(d => d.CustomerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VerificationTableCustomerTable");

				entity.HasOne(d => d.DocumentStatus)
					.WithMany(p => p.VerificationTableDocumentStatus)
					.HasForeignKey(d => d.DocumentStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VerificationTableDocumentStatus");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.VerificationTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VerificationTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.VerificationTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VerificationTableBusinessUnit");
			});
			modelBuilder.Entity<VerificationTrans>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.RefGUID);
				entity.HasIndex(e => e.VerificationTableGUID);
				entity.HasIndex(e => e.VerificationTransGUID);

				entity.Property(e => e.VerificationTransGUID).ValueGeneratedNever();

				entity.HasOne(d => d.VerificationTable)
					.WithMany(p => p.VerificationTransVerificationTable)
					.HasForeignKey(d => d.VerificationTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VerificationTransVerificationTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.VerificationTransCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VerificationTransCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.VerificationTransBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VerificationTransBusinessUnit");
			});
			modelBuilder.Entity<VerificationType>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.VerificationTypeGUID);

				entity.Property(e => e.VerificationTypeGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.VerificationTypeCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VerificationTypeCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.VerificationTypeBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_VerificationTypeBusinessUnit");
			});
			modelBuilder.Entity<WithdrawalLine>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BuyerPDCTableGUID);
				entity.HasIndex(e => e.CustomerPDCTableGUID);
				entity.HasIndex(e => e.Dimension1GUID);
				entity.HasIndex(e => e.Dimension2GUID);
				entity.HasIndex(e => e.Dimension3GUID);
				entity.HasIndex(e => e.Dimension4GUID);
				entity.HasIndex(e => e.Dimension5GUID);
				entity.HasIndex(e => e.WithdrawalLineGUID);
				entity.HasIndex(e => e.WithdrawalLineInvoiceTableGUID);
				entity.HasIndex(e => e.WithdrawalTableGUID);

				entity.Property(e => e.WithdrawalLineGUID).ValueGeneratedNever();

				entity.HasOne(d => d.BuyerPDCTable)
					.WithMany(p => p.WithdrawalLineBuyerPDCTable)
					.HasForeignKey(d => d.BuyerPDCTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalLineBuyerPDCTable");

				entity.HasOne(d => d.CustomerPDCTable)
					.WithMany(p => p.WithdrawalLineCustomerPDCTable)
					.HasForeignKey(d => d.CustomerPDCTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalLineCustomerPDCTable");

				entity.HasOne(d => d.LedgerDimension1)
					.WithMany(p => p.WithdrawalLineDimension1)
					.HasForeignKey(d => d.Dimension1GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalLineLedgerDimension1");

				entity.HasOne(d => d.LedgerDimension2)
					.WithMany(p => p.WithdrawalLineDimension2)
					.HasForeignKey(d => d.Dimension2GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalLineLedgerDimension2");

				entity.HasOne(d => d.LedgerDimension3)
					.WithMany(p => p.WithdrawalLineDimension3)
					.HasForeignKey(d => d.Dimension3GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalLineLedgerDimension3");

				entity.HasOne(d => d.LedgerDimension4)
					.WithMany(p => p.WithdrawalLineDimension4)
					.HasForeignKey(d => d.Dimension4GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalLineLedgerDimension4");

				entity.HasOne(d => d.LedgerDimension5)
					.WithMany(p => p.WithdrawalLineDimension5)
					.HasForeignKey(d => d.Dimension5GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalLineLedgerDimension5");

				entity.HasOne(d => d.WithdrawalLineInvoiceTable)
					.WithMany(p => p.WithdrawalLineWithdrawalLineInvoiceTable)
					.HasForeignKey(d => d.WithdrawalLineInvoiceTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalLineInvoiceTable");

				entity.HasOne(d => d.WithdrawalTable)
					.WithMany(p => p.WithdrawalLineWithdrawalTable)
					.HasForeignKey(d => d.WithdrawalTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalLineWithdrawalTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.WithdrawalLineCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalLineCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.WithdrawalLineBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalLineBusinessUnit");
			});
			modelBuilder.Entity<WithdrawalTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.AssignmentAgreementTableGUID);
				entity.HasIndex(e => e.BuyerAgreementTableGUID);
				entity.HasIndex(e => e.BuyerTableGUID);
				entity.HasIndex(e => e.CreditAppLineGUID);
				entity.HasIndex(e => e.CreditAppTableGUID);
				entity.HasIndex(e => e.CreditTermGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.Dimension1GUID);
				entity.HasIndex(e => e.Dimension2GUID);
				entity.HasIndex(e => e.Dimension3GUID);
				entity.HasIndex(e => e.Dimension4GUID);
				entity.HasIndex(e => e.Dimension5GUID);
				entity.HasIndex(e => e.DocumentReasonGUID);
				entity.HasIndex(e => e.DocumentStatusGUID);
				entity.HasIndex(e => e.ExtendWithdrawalTableGUID);
				entity.HasIndex(e => e.MethodOfPaymentGUID);
				entity.HasIndex(e => e.OriginalWithdrawalTableGUID);
				entity.HasIndex(e => e.ReceiptTempTableGUID);
				entity.HasIndex(e => e.TermExtensionInvoiceRevenueTypeGUID);
				entity.HasIndex(e => e.WithdrawalTableGUID);
				entity.HasIndex(e => e.OperReportSignatureGUID);

				entity.Property(e => e.WithdrawalTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.AssignmentAgreementTable)
					.WithMany(p => p.WithdrawalTableAssignmentAgreementTable)
					.HasForeignKey(d => d.AssignmentAgreementTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalTableAssignmentAgreementTable");

				entity.HasOne(d => d.BuyerAgreementTable)
					.WithMany(p => p.WithdrawalTableBuyerAgreementTable)
					.HasForeignKey(d => d.BuyerAgreementTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalTableBuyerAgreementTable");

				entity.HasOne(d => d.BuyerTable)
					.WithMany(p => p.WithdrawalTableBuyerTable)
					.HasForeignKey(d => d.BuyerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalTableBuyerTable");

				entity.HasOne(d => d.CreditAppLine)
					.WithMany(p => p.WithdrawalTableCreditAppLine)
					.HasForeignKey(d => d.CreditAppLineGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalTableCreditAppLine");

				entity.HasOne(d => d.CreditAppTable)
					.WithMany(p => p.WithdrawalTableCreditAppTable)
					.HasForeignKey(d => d.CreditAppTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalTableCreditAppTable");

				entity.HasOne(d => d.CreditTerm)
					.WithMany(p => p.WithdrawalTableCreditTerm)
					.HasForeignKey(d => d.CreditTermGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalTableCreditTerm");

				entity.HasOne(d => d.CustomerTable)
					.WithMany(p => p.WithdrawalTableCustomerTable)
					.HasForeignKey(d => d.CustomerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalTableCustomerTable");

				entity.HasOne(d => d.LedgerDimension1)
					.WithMany(p => p.WithdrawalTableDimension1)
					.HasForeignKey(d => d.Dimension1GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalTableLedgerDimension1");

				entity.HasOne(d => d.LedgerDimension2)
					.WithMany(p => p.WithdrawalTableDimension2)
					.HasForeignKey(d => d.Dimension2GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalTableLedgerDimension2");

				entity.HasOne(d => d.LedgerDimension3)
					.WithMany(p => p.WithdrawalTableDimension3)
					.HasForeignKey(d => d.Dimension3GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalTableLedgerDimension3");

				entity.HasOne(d => d.LedgerDimension4)
					.WithMany(p => p.WithdrawalTableDimension4)
					.HasForeignKey(d => d.Dimension4GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalTableLedgerDimension4");

				entity.HasOne(d => d.LedgerDimension5)
					.WithMany(p => p.WithdrawalTableDimension5)
					.HasForeignKey(d => d.Dimension5GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalTableLedgerDimension5");

				entity.HasOne(d => d.DocumentReason)
					.WithMany(p => p.WithdrawalTableDocumentReason)
					.HasForeignKey(d => d.DocumentReasonGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalTableDocumentReason");

				entity.HasOne(d => d.DocumentStatus)
					.WithMany(p => p.WithdrawalTableDocumentStatus)
					.HasForeignKey(d => d.DocumentStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalTableDocumentStatus");

				entity.HasOne(d => d.ExtendWithdrawalTable)
					.WithMany(p => p.WithdrawalTableExtendWithdrawalTable)
					.HasForeignKey(d => d.ExtendWithdrawalTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalTableExtendWithdrawalTable");

				entity.HasOne(d => d.MethodOfPayment)
					.WithMany(p => p.WithdrawalTableMethodOfPayment)
					.HasForeignKey(d => d.MethodOfPaymentGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalTableMethodOfPayment");

				entity.HasOne(d => d.OriginalWithdrawalTable)
					.WithMany(p => p.WithdrawalTableOriginalWithdrawalTable)
					.HasForeignKey(d => d.OriginalWithdrawalTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalTableOriginalWithdrawalTable");

				entity.HasOne(d => d.ReceiptTempTable)
					.WithMany(p => p.WithdrawalTableReceiptTempTable)
					.HasForeignKey(d => d.ReceiptTempTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalTableReceiptTempTable");

				entity.HasOne(d => d.TermExtensionInvoiceRevenueType)
					.WithMany(p => p.WithdrawalTableTermExtensionInvoiceRevenueType)
					.HasForeignKey(d => d.TermExtensionInvoiceRevenueTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalTableInvoiceRevenueType");

				entity.HasOne(d => d.EmployeeTable)
					.WithMany(p => p.WithdrawalTableEmployeeTable)
					.HasForeignKey(d => d.OperReportSignatureGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalTableEmployeeTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.WithdrawalTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.WithdrawalTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithdrawalTableBusinessUnit");
			});
			modelBuilder.Entity<WithholdingTaxGroup>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.WithholdingTaxGroupGUID);

				entity.Property(e => e.WithholdingTaxGroupGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.WithholdingTaxGroupCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithholdingTaxGroupCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.WithholdingTaxGroupBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithholdingTaxGroupBusinessUnit");
			});
			modelBuilder.Entity<WithholdingTaxTable>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.WithholdingTaxTableGUID);

				entity.Property(e => e.WithholdingTaxTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.WithholdingTaxTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithholdingTaxTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.WithholdingTaxTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithholdingTaxTableBusinessUnit");
			});
			modelBuilder.Entity<WithholdingTaxValue>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.WithholdingTaxValueGUID);

				entity.HasIndex(e => e.WithholdingTaxTableGUID);

				entity.Property(e => e.WithholdingTaxValueGUID).ValueGeneratedNever();

				entity.HasOne(d => d.WithholdingTaxTable)
					.WithMany(p => p.WithholdingTaxValueWithholdingTaxTable)
					.HasForeignKey(d => d.WithholdingTaxTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithholdingTaxValueWithholdingTaxTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.WithholdingTaxValueCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithholdingTaxValueCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.WithholdingTaxValueBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_WithholdingTaxValueBusinessUnit");
			});

			modelBuilder.Entity<Staging_MainAgreementTable>(entity =>
			{ entity.HasKey(e => new { e.MainAgreementTableGUID, e.InternalMainAgreementId, e.CompanyGUID }); });

			modelBuilder.Entity<Staging_CustomerTable>(entity =>
			{
				entity.HasKey(p => new { p.CustomerTableGUID, p.CustomerId, p.CompanyGUID });
			});
			modelBuilder.Entity<Staging_AgreementTableInfo>(entity =>
			{
				entity.Property(p => p.AgreementTableInfoGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});
			modelBuilder.Entity<Staging_CreditAppRequestTable>(entity => { entity.HasKey(p => new { p.CreditAppRequestTableGUID, p.CreditAppRequestId, p.CompanyGUID }); });
			modelBuilder.Entity<Staging_CreditAppRequestLine>(entity => { entity.HasKey(p => new { p.CreditAppRequestLineGUID, p.CreditAppRequestTableGUID, p.BuyerTableGUID, p.CompanyGUID }); });
			modelBuilder.Entity<Staging_CreditAppReqBusinessCollateral>(entity =>
			{
				entity.Property(e => e.CreditAppReqBusinessCollateralGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});
			modelBuilder.Entity<Staging_ServiceFeeConditionTrans>(entity => { entity.HasKey(p => new { p.ServiceFeeConditionTransGUID, p.RefGUID, p.Ordering, p.CompanyGUID }); });
			modelBuilder.Entity<Staging_NCBTrans>(entity =>
			{
				entity.Property(e => e.NCBTransGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});
			modelBuilder.Entity<Staging_CustBusinessCollateral>(entity => { entity.HasKey(p => new { p.CustBusinessCollateralGUID, p.CustBusinessCollateralId, p.CompanyGUID }); });
			modelBuilder.Entity<Staging_AssignmentAgreementTable>(entity =>
			{ entity.HasKey(e => new { e.AssignmentAgreementTableGUID, e.InternalAssignmentAgreementId, e.CompanyGUID }); });
			modelBuilder.Entity<Staging_AssignmentAgreementLine>(entity =>
			{ entity.HasKey(e => new { e.AssignmentAgreementLineGUID, e.AssignmentAgreementTableGUID, e.LineNum, e.CompanyGUID }); });
			modelBuilder.Entity<Staging_AssignmentAgreementSettle>(entity =>
			{ entity.Property(e => e.AssignmentAgreementSettleGUID).HasDefaultValueSql("convert(nvarchar(50),newid())"); });

			modelBuilder.Entity<Staging_CreditAppTable>(entity => { entity.HasKey(p => new { p.CreditAppTableGUID, p.CreditAppId, p.CompanyGUID }); });
			modelBuilder.Entity<Staging_BusinessCollateralAgmTable>(entity =>
			{ entity.HasKey(e => new { e.BusinessCollateralAgmTableGUID, e.InternalBusinessCollateralAgmId, e.CompanyGUID }); });
			modelBuilder.Entity<Staging_BusinessCollateralAgmLine>(entity =>
			{ entity.HasKey(e => new { e.BusinessCollateralAgmLineGUID, e.BusinessCollateralAgmTableGUID, e.LineNum, e.CompanyGUID }); });
			modelBuilder.Entity<Staging_RetentionConditionTrans>(entity => { entity.HasKey(p => new { p.RetentionConditionTransGUID, p.ProductType, p.RetentionDeductionMethod, p.RefGUID, p.CompanyGUID }); });
			modelBuilder.Entity<Staging_FinancialCreditTrans>(entity =>
			{
				entity.Property(e => e.FinancialCreditTransGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});
			modelBuilder.Entity<Staging_CustVisitingTrans>(entity => {
				entity.Property(e => e.CustVisitingTransGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});
			modelBuilder.Entity<Staging_CreditAppLine>(entity => { entity.HasKey(p => new { p.CreditAppLineGUID, p.CreditAppTableGUID, p.LineNum, p.CompanyGUID }); });
			modelBuilder.Entity<Staging_CreditAppTrans>(entity =>
			{
				entity.Property(e => e.CreditAppTransGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});
			modelBuilder.Entity<Staging_VerificationTable>(entity =>
			{
				entity.HasKey(p => new { p.VerificationTableGUID, p.VerificationId, p.CompanyGUID });
			});
			modelBuilder.Entity<Staging_VerificationLine>(entity =>
			{
				entity.Property(e => e.VerificationLineGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});
			modelBuilder.Entity<Staging_GuarantorAgreementTable>(entity =>
			{ 
				entity.HasKey(e => new { e.GuarantorAgreementTableGUID, e.InternalGuarantorAgreementId, e.CompanyGUID });
				entity.Property(e => e.GuarantorAgreementTableGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});
			modelBuilder.Entity<Staging_BuyerInvoiceTable>(entity =>
			{
				entity.Property(e => e.BuyerInvoiceTableGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});
			modelBuilder.Entity<Staging_PurchaseTable>(entity =>
			{
				entity.HasKey(p => new { p.PurchaseTableGUID, p.PurchaseId, p.CompanyGUID });
			});
			modelBuilder.Entity<Staging_GuarantorAgreementLine>(entity =>
			{
				entity.HasKey(p => new { p.GuarantorAgreementLineGUID, p.GuarantorAgreementTableGUID, p.Ordering, p.CompanyGUID });
			});
			modelBuilder.Entity<Staging_GuarantorAgreementLineAffiliate>(entity =>
			{
				entity.HasKey(p => new { p.GuarantorAgreementLineAffiliateGUID, p.GuarantorAgreementLineGUID, p.Ordering, p.CompanyGUID });
			});
			modelBuilder.Entity<Staging_InvoiceTable>(entity =>
			{
				entity.HasKey(p => new { p.InvoiceTableGUID, p.InvoiceId, p.CompanyGUID });
			});
			modelBuilder.Entity<Staging_PurchaseLine>(entity =>
			{
				entity.HasKey(p => new { p.PurchaseLineGUID, p.PurchaseTableGUID, p.LineNum, p.CompanyGUID });
			});
			modelBuilder.Entity<Staging_InvoiceSettlementDetail>(entity =>
			{
				entity.Property(e => e.InvoiceSettlementDetailGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});
			modelBuilder.Entity<Staging_PaymentDetail>(entity =>
			{
				entity.Property(e => e.PaymentDetailGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});
			modelBuilder.Entity<Staging_VerificationTrans>(entity =>
			{
				entity.Property(e => e.VerificationTransGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});
			modelBuilder.Entity<Staging_ProductSettledTrans>(entity =>
			{
				entity.Property(e => e.ProductSettledTransGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});
			modelBuilder.Entity<Staging_VerificationTrans>(entity =>
			{
				entity.HasKey(p => new { p.VerificationTransGUID, p.VerificationTableGUID, p.RefGUID, p.CompanyGUID });
			});
			modelBuilder.Entity<Staging_InvoiceLine>(entity =>
			{
				entity.HasKey(p => new { p.InvoiceLineGUID, p.InvoiceTableGUID, p.LineNum, p.CompanyGUID });
			});
			modelBuilder.Entity<Staging_ServiceFeeTrans>(entity => { entity.HasKey(p => new { p.ServiceFeeTransGUID, p.RefGUID, p.Ordering, p.CompanyGUID }); });
			modelBuilder.Entity<Staging_WithdrawalTable>(entity =>
			{
				entity.HasKey(p => new { p.WithdrawalTableGUID, p.WithdrawalId, p.CompanyGUID });
			});
			modelBuilder.Entity<Staging_BuyerTable>(entity =>
			{
				entity.HasKey(p => new { p.BuyerTableGUID, p.BuyerId, p.CompanyGUID });
			});
			modelBuilder.Entity<Staging_CustomerCreditLimitByProduct>(entity =>
			{
				entity.Property(e => e.CustomerCreditLimitByProductGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});
			modelBuilder.Entity<Staging_FinancialStatementTrans>(entity =>
			{
				entity.HasKey(p => new { p.FinancialStatementTransGUID, p.RefGUID, p.Year, p.Ordering, p.CompanyGUID });
			});
			modelBuilder.Entity<Staging_JointVentureTrans>(entity =>
			{
				entity.HasKey(p => new { p.JointVentureTransGUID, p.RefGUID, p.Ordering, p.CompanyGUID });
			});
			modelBuilder.Entity<Staging_ConsortiumTable>(entity =>
			{
				entity.HasKey(p => new { p.ConsortiumTableGUID, p.ConsortiumId, p.CompanyGUID });
			});
			modelBuilder.Entity<Staging_OwnerTrans>(entity =>
			{
				entity.HasKey(p => new { p.OwnerTransGUID, p.RefGUID, p.Ordering, p.CompanyGUID });
			});
			modelBuilder.Entity<Staging_TaxReportTrans>(entity =>
			{
				entity.HasKey(p => new { p.TaxReportTransGUID, p.RefGUID, p.Year, p.Month, p.CompanyGUID });
			});
			modelBuilder.Entity<Staging_BuyerCreditLimitByProduct>(entity =>
			{
				entity.Property(e => e.BuyerCreditLimitByProductGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});
			modelBuilder.Entity<Staging_BuyerAgreementLine>(entity =>
			{
				entity.Property(e => e.BuyerAgreementLineGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});
			modelBuilder.Entity<Staging_BuyerAgreementTable>(entity =>
			{
				entity.HasKey(p => new { p.BuyerAgreementTableGUID, p.BuyerAgreementId, p.CompanyGUID });
			});
			modelBuilder.Entity<Staging_ConsortiumLine>(entity =>
			{
				entity.HasKey(p => new { p.ConsortiumLineGUID, p.Ordering, p.CompanyGUID, p.ConsortiumTableGUID });
			});
			modelBuilder.Entity<Staging_EmployeeTable>(entity =>
			{
				entity.HasKey(p => new { p.EmployeeTableGUID, p.EmployeeId, p.CompanyGUID });
			});
			modelBuilder.Entity<Staging_AddressTrans>(entity =>
			{
				entity.Property(e => e.AddressTransGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});
            modelBuilder.Entity<Staging_RelatedPersonTable>(entity =>
            {
				entity.Property(e => e.RelatedPersonTableGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});
			modelBuilder.Entity<Staging_ContactPersonTrans>(entity =>
			{
				entity.Property(e => e.ContactPersonTransGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});
			modelBuilder.Entity<Staging_ContactTrans>(entity =>
			{
				entity.Property(e => e.ContactTransGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});

			modelBuilder.Entity<StagingTable>(entity => 
			{ 
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);

				entity.Property(e => e.StagingTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.ProcessTrans)
					.WithMany(p => p.StagingTableProcessTrans)
					.HasForeignKey(d => d.ProcessTransGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_StagingTableProcessTrans");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.StagingTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_StagingTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.StagingTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_StagingTableBusinessUnit");
			});
            modelBuilder.Entity<DocumentReturnMethod>(entity => 
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);

				entity.Property(e => e.DocumentReturnMethodGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.DocumentReturnMethodCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentReturnMethodCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.DocumentReturnMethodBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentReturnMethodBusinessUnit");
			});
			modelBuilder.Entity<StagingTableVendorInfo>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.ProcessTransGUID);
				entity.HasIndex(e => e.VendorPaymentTransGUID);

				entity.Property(e => e.StagingTableVendorInfoGUID).ValueGeneratedNever();

				entity.HasOne(d => d.ProcessTrans)
					.WithMany(p => p.StagingTableVendorInfoProcessTrans)
					.HasForeignKey(d => d.ProcessTransGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_StagingTableVendorInfoProcessTrans");

				entity.HasOne(d => d.VendorPaymentTrans)
					.WithMany(p => p.StagingTableVendorInfoVendorPaymentTrans)
					.HasForeignKey(d => d.VendorPaymentTransGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_StagingTableVendorInfoVendorPaymentTrans");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.StagingTableVendorInfoCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_StagingTableVendorInfoCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.StagingTableVendorInfoBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_StagingTableVendorInfoBusinessUnit");
			});
			modelBuilder.Entity<StagingTransText>(entity =>
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);

				entity.Property(e => e.StagingTransTextGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.StagingTransTextCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_StagingTransTextCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.StagingTransTextBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_StagingTransTextBusinessUnit");
			});
			modelBuilder.Entity<Staging_VendorTable>(entity =>
			{
				entity.HasKey(p => new { p.VendorTableGUID, p.VendorId, p.CompanyGUID });
			});
			modelBuilder.Entity<CAReqBuyerCreditOutstanding>(entity => 
			{ 
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.AssignmentMethodGUID);
				entity.HasIndex(e => e.BillingResponsibleByGUID);
				entity.HasIndex(e => e.BuyerTableGUID);
				entity.HasIndex(e => e.CreditAppLineGUID);
				entity.HasIndex(e => e.CreditAppRequestTableGUID);
				entity.HasIndex(e => e.CreditAppTableGUID);

				entity.Property(e => e.CAReqBuyerCreditOutstandingGUID).ValueGeneratedNever();

				entity.HasOne(d => d.AssignmentMethod)
					.WithMany(p => p.CAReqBuyerCreditOutstandingAssignmentMethod)
					.HasForeignKey(d => d.AssignmentMethodGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CAReqBuyerCreditOutstandingAssignmentMethod");

				entity.HasOne(d => d.BillingResponsibleBy)
					.WithMany(p => p.CAReqBuyerCreditOutstandingBillingResponsibleBy)
					.HasForeignKey(d => d.BillingResponsibleByGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CAReqBuyerCreditOutstandingBillingResponsibleBy");

				entity.HasOne(d => d.BuyerTable)
					.WithMany(p => p.CAReqBuyerCreditOutstandingBuyerTable)
					.HasForeignKey(d => d.BuyerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CAReqBuyerCreditOutstandingBuyerTable");

				entity.HasOne(d => d.CreditAppLine)
					.WithMany(p => p.CAReqBuyerCreditOutstandingCreditAppLine)
					.HasForeignKey(d => d.CreditAppLineGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CAReqBuyerCreditOutstandingCreditAppLine");

				entity.HasOne(d => d.CreditAppRequestTable)
					.WithMany(p => p.CAReqBuyerCreditOutstandingCreditAppRequestTable)
					.HasForeignKey(d => d.CreditAppRequestTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CAReqBuyerCreditOutstandingCreditAppRequestTable");

				entity.HasOne(d => d.CreditAppTable)
					.WithMany(p => p.CAReqBuyerCreditOutstandingCreditAppTable)
					.HasForeignKey(d => d.CreditAppTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CAReqBuyerCreditOutstandingCreditAppTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.CAReqBuyerCreditOutstandingCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CAReqBuyerCreditOutstandingCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.CAReqBuyerCreditOutstandingBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_CAReqBuyerCreditOutstandingBusinessUnit");
			});
			modelBuilder.Entity<DocumentReturnLine>(entity => 
			{ 
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BuyerTableGUID);
				entity.HasIndex(e => e.DocumentReturnTableGUID);
				entity.HasIndex(e => e.DocumentTypeGUID);

				entity.Property(e => e.DocumentReturnLineGUID).ValueGeneratedNever();

				entity.HasOne(d => d.BuyerTable)
					.WithMany(p => p.DocumentReturnLineBuyerTable)
					.HasForeignKey(d => d.BuyerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentReturnLineBuyerTable");

				entity.HasOne(d => d.DocumentReturnTable)
					.WithMany(p => p.DocumentReturnLineDocumentReturnTable)
					.HasForeignKey(d => d.DocumentReturnTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentReturnLineDocumentReturnTable");

				entity.HasOne(d => d.DocumentType)
					.WithMany(p => p.DocumentReturnLineDocumentType)
					.HasForeignKey(d => d.DocumentTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentReturnLineDocumentType");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.DocumentReturnLineCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentReturnLineCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.DocumentReturnLineBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentReturnLineBusinessUnit");
			});
			modelBuilder.Entity<DocumentReturnTable>(entity => 
			{ 
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.BuyerTableGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.DocumentReturnMethodGUID);
				entity.HasIndex(e => e.DocumentStatusGUID);
				entity.HasIndex(e => e.RequestorGUID);
				entity.HasIndex(e => e.ReturnByGUID);

				entity.Property(e => e.DocumentReturnTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.BuyerTable)
					.WithMany(p => p.DocumentReturnTableBuyerTable)
					.HasForeignKey(d => d.BuyerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentReturnTableBuyerTable");

				entity.HasOne(d => d.CustomerTable)
					.WithMany(p => p.DocumentReturnTableCustomerTable)
					.HasForeignKey(d => d.CustomerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentReturnTableCustomerTable");

				entity.HasOne(d => d.DocumentReturnMethod)
					.WithMany(p => p.DocumentReturnTableDocumentReturnMethod)
					.HasForeignKey(d => d.DocumentReturnMethodGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentReturnTableDocumentReturnMethod");

				entity.HasOne(d => d.DocumentStatus)
					.WithMany(p => p.DocumentReturnTableDocumentStatus)
					.HasForeignKey(d => d.DocumentStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentReturnTableDocumentStatus");

				entity.HasOne(d => d.Requestor)
					.WithMany(p => p.DocumentReturnTableRequestor)
					.HasForeignKey(d => d.RequestorGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentReturnTableRequestor");

				entity.HasOne(d => d.ReturnBy)
					.WithMany(p => p.DocumentReturnTableReturnBy)
					.HasForeignKey(d => d.ReturnByGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentReturnTableReturnBy");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.DocumentReturnTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentReturnTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.DocumentReturnTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_DocumentReturnTableBusinessUnit");
			});
			modelBuilder.Entity<Staging_AuthorizedPersonTrans>(entity =>
			{
				entity.Property(e => e.AuthorizedPersonTransGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});
			modelBuilder.Entity<Staging_WithdrawalLine>(entity =>
			{
				entity.HasKey(p => new { p.WithdrawalLineGUID, p.WithdrawalTableGUID,p.LineNum, p.CompanyGUID });
			});
			modelBuilder.Entity<Staging_ProjectReferenceTrans>(entity =>
			{
				entity.Property(e => e.ProjectReferenceTransGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});
			modelBuilder.Entity<Staging_CustBank>(entity =>
			{
				entity.Property(e => e.CustBankGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});
			modelBuilder.Entity<Staging_VendBank>(entity =>
			{
				entity.Property(e => e.VendBankGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});
			modelBuilder.Entity<Staging_ChequeTable>(entity =>
			{
				entity.Property(e => e.ChequeTableGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});
			modelBuilder.Entity<Staging_ProjectProgressTable>(entity =>
			{
				entity.HasKey(p => new { p.ProjectProgressTableGUID, p.ProjectProgressId,p.CompanyGUID });
			});
			modelBuilder.Entity<IntercompanyInvoiceTable>(entity => 
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.CNReasonGUID);
				entity.HasIndex(e => e.CreditAppTableGUID);
				entity.HasIndex(e => e.CurrencyGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.Dimension1GUID);
				entity.HasIndex(e => e.Dimension2GUID);
				entity.HasIndex(e => e.Dimension3GUID);
				entity.HasIndex(e => e.Dimension4GUID);
				entity.HasIndex(e => e.Dimension5GUID);
				entity.HasIndex(e => e.DocumentStatusGUID);
				entity.HasIndex(e => e.IntercompanyGUID);
				entity.HasIndex(e => e.InvoiceRevenueTypeGUID);
				entity.HasIndex(e => e.InvoiceTypeGUID);
				entity.HasIndex(e => e.TaxTableGUID);
				entity.HasIndex(e => e.WithholdingTaxTableGUID);

				entity.Property(e => e.IntercompanyInvoiceTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.CNReason)
					.WithMany(p => p.IntercompanyInvoiceTableCNReason)
					.HasForeignKey(d => d.CNReasonGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntercompanyInvoiceTableCNReason");

				entity.HasOne(d => d.CreditAppTable)
					.WithMany(p => p.IntercompanyInvoiceTableCreditAppTable)
					.HasForeignKey(d => d.CreditAppTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntercompanyInvoiceTableCreditAppTable");

				entity.HasOne(d => d.Currency)
					.WithMany(p => p.IntercompanyInvoiceTableCurrency)
					.HasForeignKey(d => d.CurrencyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntercompanyInvoiceTableCurrency");

				entity.HasOne(d => d.LedgerDimension1)
					.WithMany(p => p.IntercompanyInvoiceTableDimension1)
					.HasForeignKey(d => d.Dimension1GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntercompanyInvoiceTableLedgerDimension1");

				entity.HasOne(d => d.LedgerDimension2)
					.WithMany(p => p.IntercompanyInvoiceTableDimension2)
					.HasForeignKey(d => d.Dimension2GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntercompanyInvoiceTableLedgerDimension2");

				entity.HasOne(d => d.LedgerDimension3)
					.WithMany(p => p.IntercompanyInvoiceTableDimension3)
					.HasForeignKey(d => d.Dimension3GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntercompanyInvoiceTableLedgerDimension3");

				entity.HasOne(d => d.LedgerDimension4)
					.WithMany(p => p.IntercompanyInvoiceTableDimension4)
					.HasForeignKey(d => d.Dimension4GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntercompanyInvoiceTableLedgerDimension4");

				entity.HasOne(d => d.LedgerDimension5)
					.WithMany(p => p.IntercompanyInvoiceTableDimension5)
					.HasForeignKey(d => d.Dimension5GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntercompanyInvoiceTableLedgerDimension5");

				entity.HasOne(d => d.DocumentStatus)
					.WithMany(p => p.IntercompanyInvoiceTableDocumentStatus)
					.HasForeignKey(d => d.DocumentStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntercompanyInvoiceTableDocumentStatus");

				entity.HasOne(d => d.Intercompany)
					.WithMany(p => p.IntercompanyInvoiceTableIntercompany)
					.HasForeignKey(d => d.IntercompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntercompanyInvoiceTableIntercompany");

				entity.HasOne(d => d.InvoiceRevenueType)
					.WithMany(p => p.IntercompanyInvoiceTableInvoiceRevenueType)
					.HasForeignKey(d => d.InvoiceRevenueTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntercompanyInvoiceTableInvoiceRevenueType");

				entity.HasOne(d => d.InvoiceType)
					.WithMany(p => p.IntercompanyInvoiceTableInvoiceType)
					.HasForeignKey(d => d.InvoiceTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntercompanyInvoiceTableInvoiceType");

				entity.HasOne(d => d.TaxTable)
					.WithMany(p => p.IntercompanyInvoiceTableTaxTable)
					.HasForeignKey(d => d.TaxTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntercompanyInvoiceTableTaxTable");

				entity.HasOne(d => d.WithholdingTaxTable)
					.WithMany(p => p.IntercompanyInvoiceTableWithholdingTaxTable)
					.HasForeignKey(d => d.WithholdingTaxTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntercompanyInvoiceTableWithholdingTaxTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.IntercompanyInvoiceTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntercompanyInvoiceTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.IntercompanyInvoiceTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntercompanyInvoiceTableBusinessUnit");
			});
			modelBuilder.Entity<FreeTextInvoiceTable>(entity => 
			{ 
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.CNReasonGUID);
				entity.HasIndex(e => e.CreditAppTableGUID);
				entity.HasIndex(e => e.CurrencyGUID);
				entity.HasIndex(e => e.CustomerTableGUID);
				entity.HasIndex(e => e.Dimension1GUID);
				entity.HasIndex(e => e.Dimension2GUID);
				entity.HasIndex(e => e.Dimension3GUID);
				entity.HasIndex(e => e.Dimension4GUID);
				entity.HasIndex(e => e.Dimension5GUID);
				entity.HasIndex(e => e.DocumentStatusGUID);
				entity.HasIndex(e => e.InvoiceAddressGUID);
				entity.HasIndex(e => e.InvoiceRevenueTypeGUID);
				entity.HasIndex(e => e.InvoiceTableGUID);
				entity.HasIndex(e => e.InvoiceTypeGUID);
				entity.HasIndex(e => e.MailingInvoiceAddressGUID);
				entity.HasIndex(e => e.TaxTableGUID);
				entity.HasIndex(e => e.WithholdingTaxTableGUID);

				entity.Property(e => e.FreeTextInvoiceTableGUID).ValueGeneratedNever();

				entity.HasOne(d => d.CNReason)
					.WithMany(p => p.FreeTextInvoiceTableCNReason)
					.HasForeignKey(d => d.CNReasonGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_FreeTextInvoiceTableCNReason");

				entity.HasOne(d => d.CreditAppTable)
					.WithMany(p => p.FreeTextInvoiceTableCreditAppTable)
					.HasForeignKey(d => d.CreditAppTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_FreeTextInvoiceTableCreditAppTable");

				entity.HasOne(d => d.Currency)
					.WithMany(p => p.FreeTextInvoiceTableCurrency)
					.HasForeignKey(d => d.CurrencyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_FreeTextInvoiceTableCurrency");

				entity.HasOne(d => d.CustomerTable)
					.WithMany(p => p.FreeTextInvoiceTableCustomerTable)
					.HasForeignKey(d => d.CustomerTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_FreeTextInvoiceTableCustomerTable");

				entity.HasOne(d => d.LedgerDimension1)
					.WithMany(p => p.FreeTextInvoiceTableDimension1)
					.HasForeignKey(d => d.Dimension1GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_FreeTextInvoiceTableLedgerDimension1");

				entity.HasOne(d => d.LedgerDimension2)
					.WithMany(p => p.FreeTextInvoiceTableDimension2)
					.HasForeignKey(d => d.Dimension2GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_FreeTextInvoiceTableLedgerDimension2");

				entity.HasOne(d => d.LedgerDimension3)
					.WithMany(p => p.FreeTextInvoiceTableDimension3)
					.HasForeignKey(d => d.Dimension3GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_FreeTextInvoiceTableLedgerDimension3");

				entity.HasOne(d => d.LedgerDimension4)
					.WithMany(p => p.FreeTextInvoiceTableDimension4)
					.HasForeignKey(d => d.Dimension4GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_FreeTextInvoiceTableLedgerDimension4");

				entity.HasOne(d => d.LedgerDimension5)
					.WithMany(p => p.FreeTextInvoiceTableDimension5)
					.HasForeignKey(d => d.Dimension5GUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_FreeTextInvoiceTableLedgerDimension5");

				entity.HasOne(d => d.DocumentStatus)
					.WithMany(p => p.FreeTextInvoiceTableDocumentStatus)
					.HasForeignKey(d => d.DocumentStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_FreeTextInvoiceTableDocumentStatus");

				entity.HasOne(d => d.InvoiceAddress)
					.WithMany(p => p.FreeTextInvoiceTableInvoiceAddress)
					.HasForeignKey(d => d.InvoiceAddressGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_FreeTextInvoiceTableInvoiceAddress");

				entity.HasOne(d => d.InvoiceRevenueType)
					.WithMany(p => p.FreeTextInvoiceTableInvoiceRevenueType)
					.HasForeignKey(d => d.InvoiceRevenueTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_FreeTextInvoiceTableInvoiceRevenueType");

				entity.HasOne(d => d.InvoiceTable)
					.WithMany(p => p.FreeTextInvoiceTableInvoiceTable)
					.HasForeignKey(d => d.InvoiceTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_FreeTextInvoiceTableInvoiceTable");

				entity.HasOne(d => d.InvoiceType)
					.WithMany(p => p.FreeTextInvoiceTableInvoiceType)
					.HasForeignKey(d => d.InvoiceTypeGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_FreeTextInvoiceTableInvoiceType");

				entity.HasOne(d => d.MailingInvoiceAddress)
					.WithMany(p => p.FreeTextInvoiceTableMailingInvoiceAddress)
					.HasForeignKey(d => d.MailingInvoiceAddressGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_FreeTextInvoiceTableMailingInvoiceAddress");

				entity.HasOne(d => d.TaxTable)
					.WithMany(p => p.FreeTextInvoiceTableTaxTable)
					.HasForeignKey(d => d.TaxTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_FreeTextInvoiceTableTaxTable");

				entity.HasOne(d => d.WithholdingTaxTable)
					.WithMany(p => p.FreeTextInvoiceTableWithholdingTaxTable)
					.HasForeignKey(d => d.WithholdingTaxTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_FreeTextInvoiceTableWithholdingTaxTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.FreeTextInvoiceTableCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_FreeTextInvoiceTableCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.FreeTextInvoiceTableBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_FreeTextInvoiceTableBusinessUnit");
			});
			modelBuilder.Entity<IntercompanyInvoiceSettlement>(entity => 
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.DocumentStatusGUID);
				entity.HasIndex(e => e.CNReasonGUID);
				entity.HasIndex(e => e.IntercompanyInvoiceTableGUID);
				entity.HasIndex(e => e.MethodOfPaymentGUID);

				entity.Property(e => e.IntercompanyInvoiceSettlementGUID).ValueGeneratedNever();

				entity.HasOne(d => d.DocumentStatus)
					.WithMany(p => p.IntercompanyInvoiceSettlementDocumentStatus)
					.HasForeignKey(d => d.DocumentStatusGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntercompanyInvoiceSettlementDocumentStatus");

				entity.HasOne(d => d.CNReason)
					.WithMany(p => p.IntercompanyInvoiceSettlementCNReason)
					.HasForeignKey(d => d.CNReasonGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntercompanyInvoiceSettlementCNReason");

				entity.HasOne(d => d.IntercompanyInvoiceTable)
					.WithMany(p => p.IntercompanyInvoiceSettlementIntercompanyInvoiceTable)
					.HasForeignKey(d => d.IntercompanyInvoiceTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntercompanyInvoiceSettlementIntercompanyInvoiceTable");

				entity.HasOne(d => d.MethodOfPayment)
					.WithMany(p => p.IntercompanyInvoiceSettlementMethodOfPayment)
					.HasForeignKey(d => d.MethodOfPaymentGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntercompanyInvoiceSettlementMethodOfPayment");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.IntercompanyInvoiceSettlementCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntercompanyInvoiceSettlementCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.IntercompanyInvoiceSettlementBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntercompanyInvoiceSettlementBusinessUnit");
			});
			modelBuilder.Entity<Staging_BuyerAgreementTrans>(entity =>
			{
				entity.Property(e => e.BuyerAgreementTransGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});
			modelBuilder.Entity<IntercompanyInvoiceAdjustment>(entity => 
			{ 
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.DocumentReasonGUID);
				entity.HasIndex(e => e.IntercompanyInvoiceTableGUID);

				entity.Property(e => e.IntercompanyInvoiceAdjustmentGUID).ValueGeneratedNever();

				entity.HasOne(d => d.DocumentReason)
					.WithMany(p => p.IntercompanyInvoiceAdjustmentDocumentReason)
					.HasForeignKey(d => d.DocumentReasonGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntercompanyInvoiceAdjustmentDocumentReason");

				entity.HasOne(d => d.IntercompanyInvoiceTable)
					.WithMany(p => p.IntercompanyInvoiceAdjustmentIntercompanyInvoiceTable)
					.HasForeignKey(d => d.IntercompanyInvoiceTableGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntercompanyInvoiceAdjustmentIntercompanyInvoiceTable");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.IntercompanyInvoiceAdjustmentCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntercompanyInvoiceAdjustmentCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.IntercompanyInvoiceAdjustmentBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_IntercompanyInvoiceAdjustmentBusinessUnit");
			});
			modelBuilder.Entity<StagingTableIntercoInvSettle>(entity => 
			{ 
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.HasIndex(e => e.IntercompanyInvoiceSettlementGUID);

				entity.Property(e => e.StagingTableIntercoInvSettleGUID).ValueGeneratedNever();

				entity.HasOne(d => d.IntercompanyInvoiceSettlement)
					.WithMany(p => p.StagingTableIntercoInvSettleIntercompanyInvoiceSettlement)
					.HasForeignKey(d => d.IntercompanyInvoiceSettlementGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_StagingTableIntercoInvSettleIntercompanyInvoiceSettlement");

				entity.HasOne(d => d.Company)
					.WithMany(p => p.StagingTableIntercoInvSettleCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_StagingTableIntercoInvSettleCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.StagingTableIntercoInvSettleBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_StagingTableIntercoInvSettleBusinessUnit");
			});
			modelBuilder.Entity<Staging_IntercompanyInvoiceTable>(entity =>
			{
				entity.HasKey(p => new { p.IntercompanyInvoiceId, p.CompanyGUID });
			});
			modelBuilder.Entity<Staging_InterestRealizedTrans>(entity =>
			{
				entity.HasKey(p => new { p.InterestRealizedTransGUID, p.RefGUID, p.LineNum, p.CompanyGUID });
			});
			modelBuilder.Entity<Staging_GuarantorTrans>(entity =>
			{
				entity.Property(e => e.GuarantorTransGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});
			modelBuilder.Entity<Staging_DocumentConditionTrans>(entity =>
			{
				entity.Property(e => e.DocumentConditionTransGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});
			modelBuilder.Entity<Staging_ConsortiumTrans>(entity =>
			{
				entity.Property(e => e.ConsortiumTransGUID).HasDefaultValueSql("convert(nvarchar(50),newid())");
			});
			modelBuilder.Entity<AgingReportSetup>(entity => 
			{
				entity.HasIndex(e => e.CompanyGUID);
				entity.HasIndex(e => e.OwnerBusinessUnitGUID);
				entity.Property(e => e.AgingReportSetupGUID).ValueGeneratedNever();

				entity.HasOne(d => d.Company)
					.WithMany(p => p.AgingReportSetupCompany)
					.HasForeignKey(d => d.CompanyGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AgingReportSetupCompany");

				entity.HasOne(d => d.BusinessUnit)
					.WithMany(p => p.AgingReportSetupBusinessUnit)
					.HasForeignKey(d => d.OwnerBusinessUnitGUID)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_AgingReportSetupBusinessUnit");
			});
		}
		#endregion on model creating
	}
}

