﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.Data.BatchProcessing
{
    public class BatchEnum
    {
        public enum BatchJobStatus {
            Waiting = 0,
            Executing = 1,
            Ended = 2,
            OnHold = 3,
            Cancelled = 4
        }
        public enum BatchInstanceState {
            Executing = 0,
            Completed = 1,
            Failed = 2,
            Retried = 3,
            Cancelled = 4,
            Retrying = 5,
            RetryCompleted = 6,
            RetryFailed = 7,
            FailedServerRestarted = 8,
            RetryFailedServerRestarted = 9
        }
        public enum BatchResultStatus {
            Fail = 0,
            Success = 1
        }
        public enum BatchIntervalType {
            Seconds = 0,
            Minutes = 1,
            Hours = 2,
            Days = 3,
            Months = 4,
            Years = 5,
            DayOfWeek = 6
        }
        public enum BatchEndDateOption
        {
            NoEndDate = 0,
            Recurring = 1,
            EndBy = 2
        }
    }
}
