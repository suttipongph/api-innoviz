﻿using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.WebApi.BatchProcessing
{
    [NotMapped]
    public class BatchObject
    {
        public BatchJob BatchJob { get; set; }
        public BatchTrigger BatchTrigger { get; set; }
    }
}
