﻿using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.BatchProcessing.ViewModelHandler.Models
{
    public class BatchHistoryViewMap : IViewEntityMap
    {
        public Guid InstanceHistoryId { get; set; }
        public Guid JobId { get; set; }
        public Guid? TriggerId { get; set; }
        public DateTime? ScheduledDateTime { get; set; }
        public DateTime? ActualStartDateTime { get; set; }
        public int? InstanceState { get; set; }
        public string ParamValues { get; set; }
        public string ControllerUrl { get; set; }
        public DateTime? FinishedDateTime { get; set; }

        public bool AnyFailedLogStatus { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDateTime { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
