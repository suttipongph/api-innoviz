﻿using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.BatchProcessing.ViewModelHandler.Models
{
    public class BatchViewMap : IViewEntityMap, IViewCompanyEntityMap
    {
        public Guid JobId { get; set; }
        public Guid TriggerId { get; set; }

        public DateTime StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }
        public DateTime? NextScheduledDateTime { get; set; }
        public DateTime? LastExecutedDateTime { get; set; }

        public bool IsFinalized { get; set; }
        public int? IntervalType { get; set; }
        public string IntervalCount { get; set; }
        public int? RecurringCount { get; set; }

        public bool IsEndOfMonth { get; set; }

        public string ControllerUrl { get; set; }
        public string Description { get; set; }
        public int? JobStatus { get; set; }
        public string ParamValues { get; set; }

        public int? LastExecutedInstanceState { get; set; }

        public Guid CompanyGUID { get; set; }
        public string CompanyId { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDateTime { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
