﻿using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.BatchProcessing.ViewModelHandler.Models
{
    public class BatchInstanceLogViewMap : IViewEntityMap
    {
        public int Id { get; set; }
        public int Status { get; set; }
        public DateTime TimeStamp { get; set; }
        public string Message { get; set; }
        public string Values { get; set; }
        public string StackTrace { get; set; }
        public Guid? InstanceHistoryId { get; set; }
        public string Reference { get; set; }

        public DateTime CreatedDateTime { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedDateTime { get; set; }
        public string ModifiedBy { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
