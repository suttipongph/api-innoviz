﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.WebApi.BatchProcessing.ViewModel;
using System;
using System.Collections.Generic;
using static Innoviz.SmartApp.Data.BatchProcessing.BatchEnum;

namespace Innoviz.SmartApp.WebApi.BatchProcessing.ViewModelHandler
{
    public static class BatchObjectHandler
    {
        
        public static BatchObject ToBatchObject(this BatchObjectView batchObjectView) {
            try {
                BatchObject batchObject = new BatchObject();
                batchObject.BatchJob = new BatchJob();
                batchObject.BatchTrigger = new BatchTrigger();
                batchObject.BatchJob.Description = batchObjectView.TaskDescription;
                batchObject.BatchJob.ControllerUrl = batchObjectView.BatchApiFunctionMappingKey;
                batchObject.BatchJob.JobData = batchObjectView.Parameters;
                batchObject.BatchTrigger.IntervalType = batchObjectView.RecurringPatternModel.IntervalType;
                batchObject.BatchTrigger.IntervalCount = batchObjectView.RecurringPatternModel.IntervalCount;

                batchObject.BatchTrigger.CompanyGUID = batchObjectView.CompanyGUID.StringToGuid();

                // batch trigger startDateTime
                string inputTime = batchObjectView.StartDateTime.TimeInput;
                string inputDate = batchObjectView.StartDateTime.DateInput;
                DateTime time = DateTime.ParseExact(inputTime, "HH:mm:ss", null);
                DateTime date = inputDate.StringToDate();
                batchObject.BatchTrigger.StartDateTime = date.Date.Add(time.TimeOfDay);

                // batch trigger endDateTime
                BatchEndDateOption endDateOption = (BatchEndDateOption)batchObjectView.EndDateModel.EndDateOption;
                switch (endDateOption) {
                    case BatchEndDateOption.NoEndDate: // No end date
                    batchObject.BatchTrigger.RecurringCount = null;
                    batchObject.BatchTrigger.EndDateTime = null;
                    break;

                    case BatchEndDateOption.Recurring: // Recurring
                    batchObject.BatchTrigger.RecurringCount = batchObjectView.EndDateModel.RecurringCount;
                    break;

                    case BatchEndDateOption.EndBy: // End by
                    batchObject.BatchTrigger.RecurringCount = null;
                    string endDateTime = batchObjectView.EndDateModel.EndBy;
                    batchObject.BatchTrigger.EndDateTime = endDateTime.StringNullToDateNull();
                    break;
                }
                return batchObject;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
            
        }
        public static BatchObject ToBatchObject( this BatchView batchView) {
            try {
                BatchObject batchObject = new BatchObject();
                batchObject.BatchJob = new BatchJob();
                batchObject.BatchTrigger = new BatchTrigger();

                string jobId = batchView.JobId;
                batchObject.BatchJob.JobId = (jobId == null) ? Guid.Empty : new Guid(jobId);
                string triggerId = batchView.TriggerId;
                if(triggerId == null) {
                    batchObject.BatchTrigger = null;
                }

                batchObject.BatchTrigger.CompanyGUID = batchView.CompanyGUID.StringToGuid();
                return batchObject;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        public static BatchInstanceLogView ToBatchInstanceLogView(this BatchInstanceLog batchInstanceLog) {
            try {
                BatchInstanceLogView batchInstanceLogView = new BatchInstanceLogView();
                batchInstanceLogView.Id = batchInstanceLog.Id;
                batchInstanceLogView.Message = batchInstanceLog.Message;
                batchInstanceLogView.StackTrace = batchInstanceLog.StackTrace;
                batchInstanceLogView.Status = batchInstanceLog.ResultStatus;
                batchInstanceLogView.TimeStamp = batchInstanceLog.TimeStamp.DateTimeToString();
                batchInstanceLogView.Values = batchInstanceLog.ItemValues;
                batchInstanceLogView.Reference = batchInstanceLog.Reference;
                batchInstanceLogView.InstanceHistoryId = batchInstanceLog.InstanceHistoryId.GuidNullToString();
                
                return batchInstanceLogView;
            }
            catch (Exception e) {
                SmartAppException ex = new SmartAppException("Error parsing model.", SmartAppUtil.AddStackTrace(e));
                throw SmartAppUtil.AddStackTrace(ex);
            }

        }
        public static BatchHistoryView ToBatchHistoryView(this BatchInstanceHistory batchInstanceHistory) {
            try {
                BatchHistoryView batchHistoryView = new BatchHistoryView();
                batchHistoryView.JobId = batchInstanceHistory.JobId.GuidNullToString();
                batchHistoryView.TriggerId = batchInstanceHistory.TriggerId.GuidNullToString();
                batchHistoryView.InstanceHistoryId = batchInstanceHistory.InstanceHistoryId.GuidNullToString();
                batchHistoryView.InstanceState = batchInstanceHistory.InstanceState;
                batchHistoryView.ControllerUrl = batchInstanceHistory.ControllerUrl;
                batchHistoryView.ParamValues = batchInstanceHistory.ParamValues;

                batchHistoryView.CreatedBy = batchInstanceHistory.CreatedBy;
                batchHistoryView.CreatedDateTime = batchInstanceHistory.CreatedDateTime.DateTimeToString();
                batchHistoryView.ModifiedBy = batchInstanceHistory.ModifiedBy;
                batchHistoryView.ModifiedDateTime = batchInstanceHistory.ModifiedDateTime.DateTimeToString();

                if (batchInstanceHistory.ScheduledTime != null) {
                    batchHistoryView.ScheduledDateTime = batchInstanceHistory.ScheduledTime.DateTimeNullToString();
                }
                if(batchInstanceHistory.ActualStartTime != null ) {
                    batchHistoryView.ActualStartDateTime = batchInstanceHistory.ActualStartTime.DateTimeNullToString();
                }
                if (batchInstanceHistory.FinishedDateTime != null) {
                    batchHistoryView.FinishedDateTime = batchInstanceHistory.FinishedDateTime.DateTimeNullToString();
                }

                return batchHistoryView;
            }
            catch (Exception e) {
                SmartAppException ex = new SmartAppException("Error parsing model.", SmartAppUtil.AddStackTrace(e));
                throw SmartAppUtil.AddStackTrace(ex);
            }

        }

        public static BatchView ToBatchView(this BatchObject batchObject) {
            BatchView batchView = new BatchView();

            batchView.TriggerId = batchObject.BatchTrigger.TriggerId.GuidNullToString();
            batchView.JobId = batchObject.BatchJob.JobId.GuidNullToString();
            batchView.StartDateTime = batchObject.BatchTrigger.StartDateTime.DateTimeToString();
            batchView.EndDateTime = batchObject.BatchTrigger.EndDateTime.DateTimeNullToString();
            batchView.NextScheduledDateTime = batchObject.BatchTrigger.NextScheduledTime.DateTimeNullToString();
            batchView.LastExecutedDateTime = batchObject.BatchTrigger.LastExecuted.DateTimeNullToString();

            batchView.IsFinalized = batchObject.BatchTrigger.IsFinalized;
            batchView.IntervalType = batchObject.BatchTrigger.IntervalType;
            batchView.IntervalCount = batchObject.BatchTrigger.IntervalCount;
            batchView.RecurringCount = batchObject.BatchTrigger.RecurringCount;

            batchView.IsEndOfMonth =
                (batchObject.BatchTrigger.CRONExpression != null) ?
                    batchObject.BatchTrigger.CRONExpression.Contains("L") : false;

            batchView.ControllerUrl = batchObject.BatchJob.ControllerUrl;
            batchView.Description = batchObject.BatchJob.Description;
            batchView.JobStatus = batchObject.BatchJob.JobStatus;
            batchView.ParamValues = batchObject.BatchJob.JobData;

            batchView.CreatedBy = batchObject.BatchTrigger.CreatedBy;
            batchView.CreatedDateTime = batchObject.BatchTrigger.CreatedDateTime.DateTimeToString();
            batchView.ModifiedBy = batchObject.BatchTrigger.ModifiedBy;
            batchView.ModifiedDateTime = batchObject.BatchTrigger.ModifiedDateTime.DateTimeToString();

            batchView.CompanyGUID = batchObject.BatchTrigger.CompanyGUID.GuidNullToString();

            return batchView;
        }
        private static int GetRowAuthorize(this BatchView batchView)
        {
            var jobStatus = batchView.JobStatus;
            int ended = Convert.ToInt32(BatchJobStatus.Ended);
            int cancelled = Convert.ToInt32(BatchJobStatus.Cancelled);
            if (jobStatus != ended && jobStatus != cancelled)
            {
                return AccessMode.Viewer.GetAttrValue();
            }
            else
            {
                return AccessMode.Full.GetAttrValue();
            }
        }
        public static List<BatchView> GetBatchViewValidation(this List<BatchView> batchViews)
        {
            var result = new List<BatchView>();
            try
            {
                foreach (BatchView batchView in batchViews)
                {
                    result.Add(batchView.GetBatchViewValidation());
                }
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static BatchView GetBatchViewValidation(this BatchView batchView)
        {
            try
            {
                batchView.RowAuthorize = batchView.GetRowAuthorize();
                return batchView;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
    }
}
