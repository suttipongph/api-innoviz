using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.WebApi.BatchProcessing.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;

namespace Innoviz.SmartApp.WebApi.BatchProcessing.Repository
{
    public interface IBatchRepo
    {
        BatchView GetBatchViewItemvw(BatchView model);
    }
    public class BatchRepo: IBatchRepo
    {
        private SmartAppDbContext db;

        public BatchRepo(SmartAppDbContext context) {
            db = context;
        }
        public BatchRepo() : base() { }

        public BatchView GetBatchViewItemvw(BatchView model) {
            try {
                List<BatchView> list = new List<BatchView>() { model };
                var item =
                    (from batchView in list.AsQueryable()
                     join company in db.Set<Company>() on batchView.CompanyGUID.ToLower() equals company.CompanyGUID.ToString().ToLower()

                     select new BatchView
                     {
                         TriggerId = batchView.TriggerId,
                         JobId = batchView.JobId,
                         StartDateTime = batchView.StartDateTime,
                         EndDateTime = batchView.EndDateTime,
                         NextScheduledDateTime = batchView.NextScheduledDateTime,
                         LastExecutedDateTime = batchView.LastExecutedDateTime,

                         IsFinalized = batchView.IsFinalized,
                         IntervalType = batchView.IntervalType,
                         IntervalCount = batchView.IntervalCount,
                         RecurringCount = batchView.RecurringCount,

                         IsEndOfMonth = batchView.IsEndOfMonth,

                         ControllerUrl = batchView.ControllerUrl,
                         Description = batchView.Description,
                         JobStatus = batchView.JobStatus,
                         ParamValues = batchView.ParamValues,

                         CompanyGUID = company.CompanyGUID.GuidNullToString(),
                         CompanyId = company.CompanyId,

                         CreatedBy = batchView.CreatedBy,
                         CreatedDateTime = batchView.CreatedDateTime,
                         ModifiedBy = batchView.ModifiedBy,
                         ModifiedDateTime = batchView.ModifiedDateTime
                     })
                     .FirstOrDefault();
                return item;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
    }
}