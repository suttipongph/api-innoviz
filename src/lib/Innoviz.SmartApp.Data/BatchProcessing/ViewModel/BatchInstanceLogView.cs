﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.WebApi.BatchProcessing.ViewModel
{
    public class BatchInstanceLogView : IViewEntity
    {
        public int Id { get; set; }
        public int Status { get; set; }
        public string TimeStamp { get; set; }
        public string Message { get; set; }
        public string Values { get; set; }
        public string StackTrace { get; set; }
        public string InstanceHistoryId { get; set; }
        public string Reference { get; set; }
        public int RowAuthorize { get; set; } = AccessMode.Viewer.GetAttrValue();
        // unused
        public string CreatedDateTime { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedDateTime { get; set; }
        public string ModifiedBy { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
