﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.WebApi.BatchProcessing.ViewModel
{
    public class CleanUpBatchHistoryView
    {
        public string JobId { get; set; }
        public string FromCreatedDate { get; set; }
        public string ToCreatedDate { get; set; }
        public int[] InstanceState { get; set; }
    }
}
