﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.ViewModels;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.WebApi.BatchProcessing.ViewModel
{
    [NotMapped]
    public class BatchHistoryView : IViewEntity
    {
        public string JobId { get; set; }
        public string TriggerId { get; set; }
        public string InstanceHistoryId { get; set; }
        public string ScheduledDateTime { get; set; }
        public string ActualStartDateTime { get; set; }
        public int? InstanceState { get; set; }
        public string ParamValues { get; set; }
        public string ControllerUrl { get; set; }
        public string FinishedDateTime { get; set; }

        public bool AnyFailedLogStatus { get; set; }

        public string CreatedBy { get; set; }
        public string CreatedDateTime { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDateTime { get; set; }

        public int RowAuthorize { get; set; } = AccessMode.Viewer.GetAttrValue();
        public byte[] RowVersion { get; set; }
    }
}
