﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.WebApi.BatchProcessing.ViewModel {
    [NotMapped]
    public class DateTimeInput {
        public string DateInput { get; set; }
        public string TimeInput { get; set; }
    }
    [NotMapped]
    public class EndDateModel {
        public int EndDateOption { get; set; }
        public int RecurringCount { get; set; }
        public string EndBy { get; set; }
    }
    [NotMapped]
    public class RecurringPatternModel {
        public int IntervalType { get; set; }
        public string IntervalCount { get; set; }
    }
    [NotMapped]
    public class BatchObjectView
    {
        public string TaskDescription { get; set; }
        public string BatchApiFunctionMappingKey { get; set; }
        public string Parameters { get; set; }
        public DateTimeInput StartDateTime { get; set; }
        public EndDateModel EndDateModel { get; set; }
        public RecurringPatternModel RecurringPatternModel { get; set; }

        public bool IsEndOfMonth { get; set; }

        public string CompanyGUID { get; set; }
        public string CreatedBy { get; set; }
        public string Owner { get; set; }
        public string OwnerBusinessUnitGUID { get; set; }
    }
}
