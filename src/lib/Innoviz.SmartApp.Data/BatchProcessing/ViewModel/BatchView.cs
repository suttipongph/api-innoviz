﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.WebApi.BatchProcessing.ViewModel
{
    [NotMapped]
    public class BatchView : IViewEntity, IViewCompanyEntity
    {
        public string JobId { get; set; }
        public string TriggerId { get; set; }

        public string StartDateTime { get; set; }
        public string EndDateTime { get; set; }
        public string NextScheduledDateTime { get; set; }
        public string LastExecutedDateTime { get; set; }

        public bool IsFinalized { get; set; }
        public int? IntervalType { get; set; }
        public string IntervalCount { get; set; }
        public int? RecurringCount { get; set; }

        public bool IsEndOfMonth { get; set; }

        public string ControllerUrl { get; set; }
        public string Description { get; set; }
        public int? JobStatus { get; set; }
        public string ParamValues { get; set; }

        public int? LastExecutedInstanceState { get; set; }

        public string CompanyGUID { get; set; }
        public string CompanyId { get; set; }
       
        public string CreatedBy { get; set; }
        public string CreatedDateTime { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDateTime { get; set; }
        public string Owner { get; set; }
        public string OwnerBusinessUnitGUID { get; set; }
        public int RowAuthorize { get; set; } = AccessMode.Full.GetAttrValue();
        public byte[] RowVersion { get; set; }
    }
}
