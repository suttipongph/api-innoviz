﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.WebApi.BatchProcessing.Repository;
using Innoviz.SmartApp.WebApi.BatchProcessing.ViewModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.WebApi.BatchProcessing.Service
{
    public interface IBatchService {
        BatchView GetBatchViewByJobId(BatchView model);
        Task<dynamic> CreateScheduledJob(BatchObjectView batchObject);
    }
    public class BatchService : SmartAppService, IBatchService
    {
        public BatchService(SmartAppDbContext context) : base(context) {

        }
        public BatchService() : base() { }

        public BatchView GetBatchViewByJobId(BatchView model) {
            try {
                IBatchRepo batchRepo = new BatchRepo(db);
                return batchRepo.GetBatchViewItemvw(model);
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public async Task<dynamic> CreateScheduledJob(BatchObjectView batchObject)
        {

            try
            {
                batchObject.CreatedBy = SmartAppUtil.GetUserNameWithoutDomain(db.GetUserName());
                using (var httpClient = new HttpClient())
                {
                    var batchUrl = "BatchApi:BaseUrl".GetConfigurationValue();
                    string url = batchUrl + "Quartz/scheduleJob";

                    //string token = await GetTokenFromRequest();
                    string token = GetBearerToken();
                    string companyHeader = GetCurrentCompany();
                    string requestId = GetRequestId();

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    httpClient.DefaultRequestHeaders.Add("CompanyHeader", companyHeader);

                    if (requestId != null)
                    {
                        httpClient.DefaultRequestHeaders.Add("RequestHeader", requestId);
                    }


                    var jsonObject = JsonConvert.SerializeObject(batchObject, new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    });

                    var response = await httpClient.PostAsync(url, new StringContent(jsonObject,
                                                                Encoding.UTF8, "application/json"));

                    dynamic content = await response.Content.ReadAsAsync<dynamic>();

                    if (!response.IsSuccessStatusCode)
                    {
                        int responseStatusCode = (int)response.StatusCode;
                        if (responseStatusCode == 400 || responseStatusCode == 500)
                        {
                            // failed 
                            var errResp = ((JObject)content).ToObject<WebApiErrorResponseMessage>();
                            SmartAppException ex = new SmartAppException("Error Scheduling job.", errResp);
                            throw ex;
                        }
                        else
                        {
                            // status != 400 or 500
                            string errorMsg = "Request responded with Status: " + responseStatusCode + "(" + response.ReasonPhrase + ").";
                            SmartAppException ex = new SmartAppException("Error Scheduling job.: \n" + errorMsg);
                            throw ex;
                        }
                    }
                    else
                    {
                        // success
                        return content;
                    }


                }
            }
            catch (Exception e)
            {
                // error occurred

                throw SmartAppUtil.AddStackTrace(e);
            }

        }
    }
}
