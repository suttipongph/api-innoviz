﻿using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.WebApi.BatchProcessing
{
    [NotMapped]
    public class BatchInstanceObject
    {
        public BatchInstanceHistory BatchInstanceHistory { get; set; }
        public BatchTrigger BatchTrigger { get; set; }
        public BatchJob BatchJob { get; set; }
        public DateTime? NextScheduledTime { get; set; }
        public DateTime? LastExecuted { get; set; }
    }
}
