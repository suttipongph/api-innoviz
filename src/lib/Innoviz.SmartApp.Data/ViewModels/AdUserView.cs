﻿namespace Innoviz.SmartApp.Data.ViewModels
{
    public class AdUserView
    {
        public string DisplayName { get; set; }
        public string GivenName { get; set; }
        public string MiddleName { get; set; }
        public string Surname { get; set; }
        public string SamAccountName { get; set; }
        public bool? Enabled { get; set; }
        public string EmailAddress { get; set; }
        public string VoiceTelephoneNumber { get; set; }
    }
}
