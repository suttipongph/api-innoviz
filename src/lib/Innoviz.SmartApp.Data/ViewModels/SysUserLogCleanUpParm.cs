﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModels
{
    public class SysUserLogCleanUpParm
    {
        public int HistoryLimitDays { get; set; }
        public string[] UserNames { get; set; }
    }
}
