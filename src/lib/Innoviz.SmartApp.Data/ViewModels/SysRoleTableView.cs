﻿using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System.Collections.Generic;

namespace Innoviz.SmartApp.Data.ViewModels
{
    public class SysRoleTableView : ViewCompanyBaseEntity
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string NormalizedName { get; set; }
        public string DisplayName { get; set; }

        public int SiteLoginType { get; set; }
        public string Company_Name { get; set; }

        public IEnumerable<SysFeatureGroupRoleView> SysFeatureRoleViewList { get; set; }
    }
}
