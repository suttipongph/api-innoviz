﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModels
{
    public class SysControllerEntityMappingView: ViewBaseEntity
    {
        public string SysControllerEntityMappingGUID { get; set; }
        public string SysControllerTableGUID { get; set; }
        public string ModelName { get; set; }
        public string FeatureType { get; set; } // primary, related-info
        public string SysControllerTable_RouteAttribute { get; set; }
        public string SysControllerTable_ControllerName { get; set; }
    }
}
