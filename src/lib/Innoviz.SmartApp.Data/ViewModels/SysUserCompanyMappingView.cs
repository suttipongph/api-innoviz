﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModels
{
    public class SysUserCompanyMappingView : ViewBaseEntity
    {
        public string CompanyGUID { get; set; }
        public string BranchGUID { get; set; }
        public string UserGUID { get; set; }

        public string Company_Name { get; set; }
        public string Company_CompanyId { get; set; }
        public string Branch_Name { get; set; }
        public string Branch_BranchId { get; set; }
        public string SysUserTable_UserName { get; set; }
        public string SysUserTable_Name { get; set; }
        public string Branch_Values { get; set; }
        public string Company_Values { get; set; }
    }
}
