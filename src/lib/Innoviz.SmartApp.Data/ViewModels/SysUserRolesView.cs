﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModels
{
    public class SysUserRolesView {
        public string UserId { get; set; }
        public string RoleId { get;set;}
        public string SysUserTable_UserName { get; set; }
        public string SysRoleTable_Name { get; set; }
        public string SysRoleTable_DisplayName { get; set; }
        public string SysRoleTable_CompanyGUID { get; set; }
        public string SysRoleTable_Company_CompanyId { get; set; }
        public string SysRoleTable_Company_Name { get; set; }

        public int SysRoleTable_SiteLoginType { get; set; }
    }
}
