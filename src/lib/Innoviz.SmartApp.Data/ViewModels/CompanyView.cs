﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModels
{
    public class CompanyView : ViewBaseEntity
    {
        public string CompanyId { get; set; }
        public string CompanyGUID { get; set; }
        public string Name { get; set; }
        public string CompanyLogo { get; set; }
        public string SecondName { get; set; }

    }
}
