﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModels
{
    public class TreeView<T>: ITree<T>
    {
        public T Data { get; set; }
        public ITree<T> Parent { get; set; }
        public virtual ICollection<ITree<T>> Children { get; set; }
        public bool Leaf => Children.Count == 0;
        public bool Expanded { get; set; } = false;

        private TreeView(T data)
        {
            Data = data;
            Children = new LinkedList<ITree<T>>();
        }
        public static TreeView<T> FromLookup(ILookup<T, T> lookup)
        {
            var rootData = lookup.Count == 1 ? lookup.First().Key : default(T);
            var root = new TreeView<T>(rootData);
            root.LoadChildren(lookup);
            return root;
        }
        private void LoadChildren(ILookup<T, T> lookup)
        {
            foreach (var data in lookup[Data])
            {
                var child = new TreeView<T>(data) { Parent = this };
                Children.Add(child);
                child.LoadChildren(lookup);
            }
        }
        
    }
}
