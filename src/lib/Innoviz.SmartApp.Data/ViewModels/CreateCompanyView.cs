﻿using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModels
{
    public class CreateCompanyParamView: ViewBaseEntity
    {
        // company
        public string CompanyGUID { get; set; }
        public string CompanyId { get; set; }
        public string Company_Name { get; set; }
        public string SecondName { get; set; }

        // branch
        public string BranchGUID { get; set; }
        public string BranchId { get; set; }
        public string Branch_Name { get; set; }

        // user
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string SysUserTable_Name { get; set; }

        public string EmployeeTable_EmployeeId { get; set; }
        public string BusinessUnit_BusinessUnitId { get; set; }
    }
    public class CreateCompanyResultView: ResultBaseEntity
    {

    }
}
namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class CreateCompanyViewMap
    {
        // security
        public Company Company { get; set; }
        public Branch Branch { get; set; }
        public BusinessUnit BusinessUnit { get; set; }
        public EmployeeTable EmployeeTable { get; set; }
        public List<SysRoleTable> SysRoleTable { get; set; }
        public List<SysControllerRole> SysControllerRole { get; set; }
        public List<SysFeatureGroupRole> SysFeatureGroupRole { get; set; }
        public List<SysScopeRole> SysScopeRole { get; set; }
        public List<SysUserRoles> SysUserRoles { get; set; }
        public List<SysUserCompanyMapping> SysUserCompanyMapping { get; set; }

        // business
        public CompanyParameter CompanyParameter { get; set; }
        public Currency Currency { get; set; }
        public ExchangeRate ExchangeRate { get; set; }
        public List<NumberSeqParameter> NumberSeqParameter { get; set; }
        public CalendarGroup CalendarGroup { get; set; }
        public List<CreditLimitType> CreditLimitType { get; set; }
        public List<RetentionConditionSetup> RetentionConditionSetup { get; set; }
    }
}
