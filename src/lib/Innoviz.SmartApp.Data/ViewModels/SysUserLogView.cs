﻿using System;

namespace Innoviz.SmartApp.Data.ViewModels
{
    public class SysUserLogView
    {
        public string CreatedDateTime { get; set; }

        public string ComputerName { get; set; }
        public string SessionId { get; set; }
        public string UserName { get; set; }

        public string LogOffTime { get; set; }

        public DateTime CreatedDateTimeSort { get; set; }
        public DateTime? LogOffTimeSort { get; set; }

        public string IPAddress { get; set; }
    }
}
