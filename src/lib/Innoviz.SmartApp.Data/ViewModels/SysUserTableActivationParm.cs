﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModels
{
    public class SysUserTableActivationParm
    {
        public string SysUserTableGUID { get; set; }
        public bool InActive { get; set; }
    }
}
