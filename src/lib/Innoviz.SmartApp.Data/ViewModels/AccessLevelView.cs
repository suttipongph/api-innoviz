﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModels
{
    public class AccessLevelView
    {

    }

    public class AccessLevelParm
    {
        public string Owner { get; set; }
        public Guid? OwnerBusinessUnit { get; set; }
        public int AccessLevel { get; set; }
    }
}
