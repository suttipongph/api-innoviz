﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModels
{
    public class SysFeatureRoleView : ViewCompanyBaseEntity
    {
        public int AccessRight { get; set; }
        public string SysFeatureRoleGUID { get; set; }
        public string SysFeatureTableGUID { get; set; }
        public string RoleGUID { get; set; }

        public string SysFeatureTable_Path { get; set; }
        public string SysFeatureTable_FeatureId { get; set; }
        public string SysFeatureTable_ParentFeatureId { get; set; }

        public string Company_Name { get; set; }
        
    }
    public class SysFeatureGroupExportView
    {
        public string PathMenu { get; set; }
        public int Action { get; set; }
        public int Read { get; set; }
        public int Create { get; set; }
        public int Update { get; set; }
        public int Delete { get; set; }
        public string Reference { get; set; }
    }
}
