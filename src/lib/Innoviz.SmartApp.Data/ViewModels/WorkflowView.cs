﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class Workflow
    {
        public string Name { get; set; }
        public string FullName { get; set; }
    }
    public class TaskList
    {
        public List<TaskItem> Items { get; set; }
        public int Total { get; set; }
    }
    public class TaskItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Folio { get; set; }
        public string SerialNumber { get; set; }
        public string Originator { get; set; }
        public string StartDate { get; set; }
        public string Viewflow { get; set; }
        public string Status { get; set; }
        public int ActInstDestId { get; set; }
        public string ActInstDestName { get; set; }
        public string ActInstDestDescription { get; set; }
        public string ActInstDestStartDate { get; set; }
        public List<DataField> ActInstDestDataFields { get; set; }
        public string EventName { get; set; }
        public string EventDescription { get; set; }

        public List<Action> Actions { get; set; }
        public List<Comment> Comments { get; set; }
        public List<DataField> DataFields { get; set; }
        public string FormURL { get; set; }
        public string ActInstDestDisplayName { get; set; }
        public string AllocatedUser { get; set; }
        public int WorklistItemId { get; set; }
    }

    public class Action
    {
        public bool Batchable { get; set; }
        public string Metadata { get; set; }
        public string Name { get; set; }
    }

    public class DataField
    {
        public string Category { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class Comment
    {
        public string User { get; set; }
        public string Message { get; set; }
        public string CreatedDate { get; set; }
        public string CretedBy { get; set; }
    }

    public class WorkflowInstance
    {
        public int Id { get; set; }
        public string SerialNumber { get; set; }
        public string Name { get; set; }
        public string Folio { get; set; }
        public string Action { get; set; }
        public string ImpersonateUser { get; set; }
        public List<Comment> Comments { get; set; }
        public List<DataField> DataFields { get; set; }
        public string AllocatedUser { get; set; }
        public string ActInstDestName { get; set; }
        public string ActInstDestDisplayName { get; set; }
        public int WorklistItemId { get; set; }
    }

    public class TaskListParm
    {
        public SearchParameter Search { get; set; }
        public WorkflowInstance Workflow { get; set; }
    }

    public class WorkflowResultView : ResultBaseEntity
    {
        public int ProcessInstanceId { get; set; }
    }
}
