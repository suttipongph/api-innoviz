﻿using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class SysUserSettingsItemView : IViewEntity
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        
        public string PhoneNumber { get; set; }
        public string Name { get; set; }
        public string DateFormatId { get; set; }
        public string TimeFormatId { get; set; }
        public string NumberFormatId { get; set; }
        public bool InActive { get; set; }
        public string LanguageId { get; set; }

        public bool ShowSystemLog { get; set; }

        public string DefaultCompanyGUID { get; set; }
        public string DefaultBranchGUID { get; set; }

        public string Company_CompanyId { get; set; }
        public string Branch_BranchId { get; set; }

        public string UserImage { get; set; }

        public string CreatedBy { get; set; }
        public string CreatedDateTime { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDateTime { get; set; }
        public byte[] RowVersion { get; set; }
        public List<AccessRightView> AccessRights { get; set; }
        public List<SysUserCompanyMappingView> SysUserCompanyMappings { get; set; }

    }
}
