﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModels
{
    public class BranchView : ViewCompanyBaseEntity
    {
        public string BranchId { get; set; }
        public string Name { get; set; }
        public string BranchGUID { get; set; }
        public string ApplicationNumberSeqGUID { get; set; }
        public string TaxBranchGUID { get; set; }

        public string ApplicationNumberSeq_NumberSeqCode { get; set; }
        public string ApplicationNumberSeq_Description { get; set; }
        public string TaxBranch_BranchId { get; set; }
    }
}
