﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModels
{
    public class CheckAccessLevelResult
    {
        public bool CanAccessController { get; set; }
        public int AccessLevel { get; set; }
        public bool IsCreateCompany { get; set; } = false;
    }
}
