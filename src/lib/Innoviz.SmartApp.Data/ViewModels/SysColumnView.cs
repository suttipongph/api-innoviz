﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModels
{
    public class SysColumnView<T>
    {
        public string Label { get; set; }
        public string TextKey { get; set; }
        public int ColumnType { get; set; }
        public bool Visibility { get; set; }
        public int Sorting { get; set; }
        public IEnumerable<T> MasterList { get; set; }
    }
}
