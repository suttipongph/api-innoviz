﻿using System.Collections.Generic;
using Innoviz.SmartApp.Data.ViewModelsV2;

namespace Innoviz.SmartApp.Data.ViewModels
{
    public class AccessRightView
    {
        //public int AccessRight { get; set; }
        public string CompanyGUID { get; set; }
        //public string RoleGUID { get; set; }
        public string SysFeatureTableGUID { get; set; }
        public string SysFeatureTable_FeatureId { get; set; }
        public string SysFeatureTable_ParentFeatureId { get; set; }
        public string SysFeatureTable_Path { get; set; }
        public int SysRoleTable_SiteLoginType { get; set; }
        public AccessLevelModel AccessLevels { get; set; }
    }
    public class UserLogonView
    {
        public SystemInitDataCheck InitDataCheck { get; set; }
        public SysUserSettingsItemView User { get; set; }
        public CompanyBranchView CompanyBranch { get; set; }
    }
    public class SystemInitDataCheck
    {
        public bool HasAnyCompany { get; set; }
        public bool HasAnySysUserTable { get; set; }
    }
    public class CompanyBranchView
    {
        public string CompanyGUID { get; set; }
        public string CompanyId { get; set; }
        public string BranchGUID { get; set; }
        public string BranchId { get; set; }
    }
    public class AccessRightWithCompanyBranchView
    {
        public IEnumerable<AccessRightView> AccessRight { get; set; }
        public CompanyBranchView CompanyBranch { get; set; }
    }

    public class AccessLevelModel
    {
        public int Action { get; set; }
        public int Read { get; set; }
        public int Update { get; set; }
        public int Create { get; set; }
        public int Delete { get; set; }
    }
    public class AccessRightBU
    {
        public IEnumerable<AccessRightView> AccessRight { get; set; }
        public IEnumerable<string> ParentChildBU { get; set; }
        public string BusinessUnitGUID { get; set; }
        public string BusinessUnitId { get; set; }
    }
}
