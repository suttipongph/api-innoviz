﻿using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModels
{
    public class SysFeatureGroupStructureView
    {
        public int NodeId { get; set; }
        public int NodeParentId { get; set; }
        public string SysFeatureGroup_GroupId { get; set; }
        public int FeatureType { get; set; }
        public string SysFeatureGroupGUID { get; set; }
        public bool HasSysFeatureTable { get; set; }
        public string NodeLabel { get; set; }
        public int SiteLoginType { get; set; }
        public int Action { get; set; }
        public int Read { get; set; }
        public int Create { get; set; }
        public int Update { get; set; }
        public int Delete { get; set; }
    }
    public class SysFeatureGroupMenuMapping
    {
        public string Label { get; set; }
        public string GroupId { get; set; }
        public List<SysFeatureGroupMenuMapping> Items { get; set; }
    }
}
