﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using static Innoviz.SmartApp.Data.BatchProcessing.BatchEnum;

namespace Innoviz.SmartApp.Data.BatchProcessing
{
    public class BatchLogService: IBatchLogService
    {
        private ILogger<BatchLogService> logger;
        public BatchLogService(ILogger<BatchLogService> logger) {
            this.logger = logger;
        }

        // batchInstanceLog.Message => success message
        public void LogSuccessResult(BatchInstanceLog item) {
            logger.LogInformation("{InstanceHistoryId} {Reference} {ResultStatus} {Message} {ItemValues} {StackTrace}",
                                   item.InstanceHistoryId, item.Reference,
                                       item.ResultStatus, item.Message, item.ItemValues, item.StackTrace);
        }

        public BatchInstanceLog GenBatchInstanceLogSuccess(string instanceHistoryId,
                                                        string reference,
                                                        string[] reference_parm,
                                                        string message,
                                                        string[] message_parm,
                                                        string itemValue,
                                                        string[] itemValue_parm)
        {
            BatchInstanceLog item = new BatchInstanceLog();
            if (instanceHistoryId != null)
            {
                item.InstanceHistoryId = new Guid(instanceHistoryId);
                item.Reference = SystemStaticData.GetTranslatedMessage(reference, reference_parm);
                item.ResultStatus = Convert.ToInt16(BatchResultStatus.Success);
                item.Message = SystemStaticData.GetTranslatedMessage(message, message_parm);
                item.ItemValues = SystemStaticData.GetTranslatedMessage(itemValue, itemValue_parm);
            }

            return item;
        }
        public void LogErrorResult(BatchInstanceLog item)
        {
            if (item != null)
            {
                logger.LogError("{InstanceHistoryId} {Reference} {ResultStatus} {Message} {ItemValues} {StackTrace}",
                                       item.InstanceHistoryId, item.Reference,
                                       item.ResultStatus, item.Message, item.ItemValues, item.StackTrace);
            }
        }

        public void LogErrorResult(List<BatchInstanceLog> list)
        {
            foreach (BatchInstanceLog item in list)
            {
                LogErrorResult(item);
            }
        }

        public BatchInstanceLog GenBatchInstanceLogError(string instanceHistoryId,
                                                        string reference,
                                                        string[] reference_parm,
                                                        string message,
                                                        string[] message_parm,
                                                        string itemValue,
                                                        string[] itemValue_parm,
                                                        string stackTrace)
        {
            if (instanceHistoryId != null)
            {
                BatchInstanceLog item = new BatchInstanceLog();
                item.InstanceHistoryId = new Guid(instanceHistoryId);
                item.Reference = SystemStaticData.GetTranslatedMessage(reference, reference_parm);
                item.ResultStatus = Convert.ToInt16(BatchResultStatus.Fail);
                item.Message = SystemStaticData.GetTranslatedMessage(message, message_parm);
                item.ItemValues = SystemStaticData.GetTranslatedMessage(itemValue, itemValue_parm);
                item.StackTrace = stackTrace;
                return item;
            }

            return null;
        }

        public BatchInstanceLog GenBatchInstanceLogErrorWithPropValues(string instanceHistoryId,
                                                        string reference,
                                                        string[] reference_parm,
                                                        string message,
                                                        string[] message_parm,
                                                        string[] itemValue_fieldNames,
                                                        string[] itemValue_values,
                                                        string stackTrace)
        {
            if (instanceHistoryId != null)
            {
                BatchInstanceLog item = new BatchInstanceLog();
                item.InstanceHistoryId = new Guid(instanceHistoryId);
                item.Reference = SystemStaticData.GetTranslatedMessage(reference, reference_parm);
                item.ResultStatus = Convert.ToInt16(BatchResultStatus.Fail);
                item.Message = SystemStaticData.GetTranslatedMessage(message, message_parm);
                item.ItemValues = FormatValues(itemValue_fieldNames, itemValue_values);
                item.StackTrace = stackTrace;
                return item;
            }

            return null;
        }

        // format to string > field1: value1, field2: value2, ...
        private string FormatValues(string[] itemValue_filedNames, string[] itemValue_values)
        {
            if(itemValue_filedNames != null && itemValue_values != null &&
                itemValue_filedNames.Length != 0 && itemValue_values.Length !=0 &&
                itemValue_filedNames.Length == itemValue_values.Length)
            {
                string result = "";
                for(int i=0; i<itemValue_filedNames.Length; i++)
                {
                    result += String.Format("{0}: {1}", itemValue_filedNames[i], itemValue_values[i]);
                    if(i < itemValue_filedNames.Length -1)
                    {
                        result += ", ";
                    }
                }
                return result;
            }
            else
            {
                return null;
            }
        }

        public void LogBatchError(string instanceHistoryId, string message, string reference, string itemValues, string stackTrace)
        {
            BatchInstanceLog item = new BatchInstanceLog
            {
                InstanceHistoryId = instanceHistoryId.StringToGuid(),
                ResultStatus = (int)BatchResultStatus.Fail,
                Message = message,
                Reference = reference,
                ItemValues = itemValues,
                StackTrace = stackTrace
            };
            LogErrorResult(item);
        }
        public void LogBatchError(string instanceHistoryId,
                                string reference,
                                string[] reference_parm,
                                string message,
                                string[] message_parm,
                                string itemValue,
                                string[] itemValue_parm,
                                string stackTrace)
        {
            BatchInstanceLog item = GenBatchInstanceLogError(instanceHistoryId,
                                                             reference,
                                                             reference_parm,
                                                             message,
                                                             message_parm,
                                                             itemValue,
                                                             itemValue_parm,
                                                             stackTrace);
            LogErrorResult(item);
        }
        public void LogBatchSuccess(string instanceHistoryId, string message, string reference, string itemValues)
        {
            BatchInstanceLog item = new BatchInstanceLog
            {
                InstanceHistoryId = instanceHistoryId.StringToGuid(),
                ResultStatus = (int)BatchResultStatus.Success,
                Message = message,
                Reference = reference,
                ItemValues = itemValues,
            };
            LogSuccessResult(item);
        }
        public void LogBatchSuccess(string instanceHistoryId,
                                string reference,
                                string[] reference_parm,
                                string message,
                                string[] message_parm,
                                string itemValue,
                                string[] itemValue_parm)
        {
            BatchInstanceLog item = GenBatchInstanceLogSuccess(instanceHistoryId,
                                                             reference,
                                                             reference_parm,
                                                             message,
                                                             message_parm,
                                                             itemValue,
                                                             itemValue_parm);
            LogSuccessResult(item);
        }
        public void LogBatchErrors(Exception e, string instanceHistoryId)
        {
            var batchLogRefData = (IDictionary)e.Data[ExceptionDataKey.BatchLogReference];
            var msgList = (List<string>)e.Data[ExceptionDataKey.MessageList];
            List<string> msgWRefs = new List<string>();

            if (batchLogRefData != null && batchLogRefData.Count > 0)
            {
                foreach (var key in batchLogRefData.Keys)
                {
                    var val = (SmartAppExceptionData)batchLogRefData[key];

                    if(val != null)
                    {
                        var errorMessage = SystemStaticData.GetTranslatedMessage(val.ErrorCode, val.Parameters);
                        msgWRefs.Add(errorMessage);
                        LogBatchError(instanceHistoryId, errorMessage, val.BatchLogReference?.Reference, val.BatchLogReference?.ItemValues, null);
                    }
                }
            }
            if(msgList != null && msgList.Count > 0)
            {
                var msgWORefs = msgList.Where(w => !msgWRefs.Any(a => a == w) && w != "Error" && w != "ERROR.ERROR");
                //for(int i=msgWORefs.Count() -1; i>=0; i--)
                //{

                //}
                for(int i=0; i < msgWORefs.Count(); i++)
                {
                    if(i == msgWORefs.Count() -1)
                    {
                        LogBatchError(instanceHistoryId, msgWORefs.ElementAt(i), null, null, e.StackTrace);
                    }
                    else
                    {
                        LogBatchError(instanceHistoryId, msgWORefs.ElementAt(i), null, null, null);
                    }
                }
            }
        }
    }
}
