﻿using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelHandler;
using Innoviz.SmartApp.Data.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Data.Repositories
{
    public interface ISysFeatureGroupStructureRepo
    {
        List<ITree<SysFeatureGroupStructureView>> GetSysFeatureGroupStructureTree();
    }
    public class SysFeatureGroupStructureRepo : BaseRepository<SysFeatureGroupStructure>, ISysFeatureGroupStructureRepo
    {
        public SysFeatureGroupStructureRepo(SmartAppDbContext context) : base(context) { }
        public SysFeatureGroupStructureRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }

        public List<ITree<SysFeatureGroupStructureView>> GetSysFeatureGroupStructureTree()
        {
            try
            {
                int site = SysParm.SiteLogin;
                var sysFeatureGroupStructures = GetQuery().Where(w => w.SiteLoginType == site).OrderBy(o => o.NodeId).AsNoTracking().ToList();
                
                var tree = sysFeatureGroupStructures.ToTree((parent, child) => child.NodeParentId == parent.NodeId);
                return tree.Children.ToList();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private IQueryable<SysFeatureGroupStructureView> GetQuery()
        {
            try
            {
                var result =
                    (from fgs in Entity
                     join fg in db.Set<SysFeatureGroup>()
                     on fgs.SysFeatureGroupGUID equals fg.SysFeatureGroupGUID into ljSysFeatureGroup

                     from fg in ljSysFeatureGroup.DefaultIfEmpty()
                     select new SysFeatureGroupStructureView
                     {
                         FeatureType = fgs.FeatureType,
                         NodeId = fgs.NodeId,
                         NodeLabel = fgs.NodeLabel,
                         NodeParentId = fgs.NodeParentId,
                         SysFeatureGroupGUID = fgs.SysFeatureGroupGUID.GuidNullToString(),
                         SysFeatureGroup_GroupId = fg != null ? fg.GroupId : null,
                         SiteLoginType = fgs.SiteLoginType,
                         HasSysFeatureTable = fgs.SysFeatureGroupGUID.HasValue
                     });
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
