﻿using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface ISysFeatureGroupRepo
    {
        List<SysFeatureGroup> GetListSysFeatureGroupNoTracking();
        List<SysFeatureGroup> GetSysFeatureGroupByGroupIdNoTracking(IEnumerable<string> groupIds);
    }
    public class SysFeatureGroupRepo : BaseRepository<SysFeatureGroup>, ISysFeatureGroupRepo
    {
        public SysFeatureGroupRepo(SmartAppDbContext context) : base(context) { }
        public SysFeatureGroupRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }

        public List<SysFeatureGroup> GetListSysFeatureGroupNoTracking()
        {
            try
            {
                var result = Entity
                                .AsNoTracking()
                                .ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<SysFeatureGroup> GetSysFeatureGroupByGroupIdNoTracking(IEnumerable<string> groupIds)
        {
            try
            {
                var result = Entity.Where(w => groupIds.Contains(w.GroupId))
                                    .AsNoTracking()
                                    .ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}


