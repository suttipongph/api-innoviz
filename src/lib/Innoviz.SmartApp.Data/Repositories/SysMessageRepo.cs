﻿using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.Data.Repositories
{
    public interface ISysMessageRepo {
        IEnumerable<SysMessage> GetList();
    }
    public class SysMessageRepo : BaseRepository<SysMessage>, ISysMessageRepo 
    {
        public SysMessageRepo(SmartAppDbContext context) : base(context) {

        }
    }
}
