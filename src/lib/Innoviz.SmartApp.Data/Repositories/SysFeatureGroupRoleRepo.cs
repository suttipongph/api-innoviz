﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.ViewModels;
using Innoviz.SmartApp.Core.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelsV2;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface ISysFeatureGroupRoleRepo {

        IEnumerable<AccessRightView> GetAccessRightModelByUserSiteCompany(Guid userId, int site, Guid companyGUID);
        IEnumerable<SysFeatureGroupRoleView> GetSysFeatureGroupRoleViewByRoleId(Guid roleId);
        // for role UI step 2
        IEnumerable<SysFeatureGroupRoleView> GetSysFeatureGroupRoleViewByCompanySiteAdminRole(int site, Guid companyGUID);

        //IEnumerable<SysFeatureGroupRole> CreateSysFeatureGroupRoles(IEnumerable<SysFeatureGroupRole> list);
        //IEnumerable<SysFeatureGroupRole> UpdateSysFeatureGroupRoles(string roleId, IEnumerable<SysFeatureGroupRole> list);

        bool RoleExistsInFeatureRole(Guid roleId);

        bool AddRange(IEnumerable<SysFeatureGroupRole> list);

        List<SysFeatureGroupRole> GetSysFeatureGroupRoleByRoleGUIDNoTracking(Guid roleId);
        List<SysFeatureGroupExportView> GetExportRoleData(Guid roleGUID);
    }

    public class SysFeatureGroupRoleRepo: BaseRepository<SysFeatureGroupRole>, ISysFeatureGroupRoleRepo
    {
        public SysFeatureGroupRoleRepo(SmartAppDbContext context) : base(context) 
        {

        }
        public override void ValidateAdd(SysFeatureGroupRole item) 
        {
            base.ValidateAdd(item);
        }
        public override void ValidateUpdate(SysFeatureGroupRole item) 
        {
            base.ValidateUpdate(item);
        }
        public override void ValidateRemove(SysFeatureGroupRole item) {

        }
        
        private IQueryable<SysFeatureGroupRoleViewMap> GetQuery()
        {
            var result =
                (from featureGroupRole in Entity
                 join featureGroup in db.Set<SysFeatureGroup>()
                 on featureGroupRole.SysFeatureGroupGUID equals featureGroup.SysFeatureGroupGUID

                 join role in db.Set<SysRoleTable>()
                 on featureGroupRole.RoleGUID equals role.Id

                 select new SysFeatureGroupRoleViewMap
                 {
                     SysFeatureGroupRoleGUID = featureGroupRole.SysFeatureGroupRoleGUID,
                     SysFeatureGroupGUID = featureGroupRole.SysFeatureGroupGUID,
                     RoleGUID = featureGroupRole.RoleGUID,
                     GroupId = featureGroup.GroupId,
                     Action = featureGroupRole.Action,
                     Create = featureGroupRole.Create,
                     Read = featureGroupRole.Read,
                     Update = featureGroupRole.Update,
                     Delete = featureGroupRole.Delete,
                     SysRoleTable_CompanyGUID = role.CompanyGUID,
                     SysFeatureGroup_FeatureType = featureGroup.FeatureType,
                     SysRoleTable_SiteLoginType = role.SiteLoginType,
                     SysRoleTable_Displayname = role.DisplayName,
                     CreatedBy = featureGroupRole.CreatedBy,
                     CreatedDateTime = featureGroupRole.CreatedDateTime,
                     ModifiedBy = featureGroupRole.ModifiedBy,
                     ModifiedDateTime = featureGroupRole.ModifiedDateTime
                 });
            return result;
        }
        public IEnumerable<AccessRightView> GetAccessRightModelByUserSiteCompany(Guid userId, int site, Guid companyGUID)
        {
            try
            {
                Guid userID = userId;
                IQueryable<SysRoleTable> roleQuery = db.Set<SysRoleTable>();
                //if(site != null)
                //{
                //int siteLogin = GetLoginSiteValue(site);
                int siteLogin = site;
                roleQuery = roleQuery.Where(item => item.SiteLoginType == siteLogin);
                //}
                if(companyGUID != null)
                {
                    Guid companyGuid = companyGUID;//new Guid(companyGUID);
                    roleQuery = roleQuery.Where(item => item.CompanyGUID == companyGuid);
                }

                var roles = (from userrole in db.Set<SysUserRoles>().Where(item => item.UserId == userID)
                                                    join role in roleQuery
                                                    on userrole.RoleId equals role.Id
                                                    select role.Id);
                if(roles.Count() == 0)
                {
                    return null;
                }

                var sysFeatureRolesQ1 = GetQuery().Where(w => roles.Contains(w.RoleGUID));
                    //(from featureRole in Entity
                    // join role in roles
                    // on featureRole.RoleGUID equals role.Id
                    // //on new { RoleGUID = featureRole.RoleGUID, CompanyGUID = featureRole.CompanyGUID }
                    // //   equals new { RoleGUID = role.Id, CompanyGUID = role.CompanyGUID }
                    // //select featureRole);
                    // select new SysFeatureGroupRoleViewMap
                    // {
                    //     SysFeatureGroupRoleGUID = featureRole.SysFeatureGroupRoleGUID,
                    //     Action = featureRole.Action,
                    //     Read = featureRole.Read,
                    //     Update = featureRole.Update,
                    //     Create = featureRole.Create,
                    //     Delete = featureRole.Delete,
                    //     SysFeatureGroupGUID = featureRole.SysFeatureGroupGUID,
                    //     SysRoleTable_CompanyGUID = role.CompanyGUID,
                    // });
            var groupedFeatureRoles = 
                        (from featureRole in sysFeatureRolesQ1
                         group featureRole by
                         new
                         {
                             featureRole.SysRoleTable_CompanyGUID,
                             featureRole.SysFeatureGroupGUID,
                         }
                         into g
                         select new SysFeatureGroupRoleViewMap
                         {
                             //AccessRight = g.Max(item => item.AccessRight),
                             Action = g.Max(item => item.Action),
                             Read = g.Max(item => item.Read),
                             Update = g.Max(item => item.Update),
                             Create = g.Max(item => item.Create),
                             Delete = g.Max(item => item.Delete),
                             SysFeatureGroupGUID = g.Key.SysFeatureGroupGUID,
                             SysRoleTable_CompanyGUID = g.Key.SysRoleTable_CompanyGUID,
                         })
                         .ToList();

                //var sysFeatureRolesQ2 = (from featureRole in sysFeatureRolesQ1
                //         join fr in groupedFeatureRoles
                //         on new
                //         {
                //             AccessRight = featureRole.AccessRight,
                //             SysFeatureTableGUID = featureRole.SysFeatureTableGUID,
                //             CompanyGUID = featureRole.CompanyGUID
                //         }
                //         equals new
                //         {
                //             AccessRight = fr.AccessRight,
                //             SysFeatureTableGUID = fr.SysFeatureTableGUID,
                //             CompanyGUID = fr.CompanyGUID
                //         }
                //         select featureRole)
                //         .ToList();

                var list = (from featureRole in groupedFeatureRoles
                            join featureGroupMapping in db.Set<SysFeatureGroupMapping>()
                            on featureRole.SysFeatureGroupGUID equals featureGroupMapping.SysFeatureGroupGUID
                            join feature in db.Set<SysFeatureTable>()
                            on featureGroupMapping.SysFeatureTableGUID equals feature.SysFeatureTableGUID

                            select new AccessRightView
                            {
                                //AccessRight = featureRole.AccessRight,
                                //RoleGUID = featureRole.RoleGUID.GuidNullToString(),
                                AccessLevels = new AccessLevelModel
                                {
                                    Action = featureRole.Action,
                                    Read = featureRole.Read,
                                    Update = featureRole.Update,
                                    Delete = featureRole.Delete,
                                    Create = featureRole.Create
                                },
                                SysRoleTable_SiteLoginType = site,//(site != null) ? GetLoginSiteValue(site) : -1,

                                SysFeatureTable_FeatureId = feature.FeatureId,
                                SysFeatureTable_ParentFeatureId = feature.ParentFeatureId,
                                SysFeatureTable_Path = feature.Path,

                                CompanyGUID = featureRole.SysRoleTable_CompanyGUID.GuidNullToString(),


                                SysFeatureTableGUID = feature.SysFeatureTableGUID.GuidNullToString(),

                            })
                         .ToList();

                return list;
            }
            
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        

        public IEnumerable<SysFeatureGroupRoleView> GetSysFeatureGroupRoleViewByRoleId(Guid roleID)
        {
            try
            {
                var searchParam = new SearchParameter();
                searchParam.Conditions.Add(SearchConditionService.GetSysFeatureGroupRoleByRoleIdCondition(roleID));
                var predicate = searchParam.GetSearchPredicate(typeof(SysFeatureGroupRoleViewMap));
                var list = GetQuery()
                            .Where(predicate.Predicates, predicate.Values)
                            .AsNoTracking()
                            .ToMaps<SysFeatureGroupRoleViewMap, SysFeatureGroupRoleView>();

                    //(from featureRole in Entity.Where(item => item.RoleGUID == roleID)
                    // join featureGroup in db.Set<SysFeatureGroup>() 
                    // on featureRole.SysFeatureGroupGUID equals featureGroup.SysFeatureGroupGUID

                    // join role in db.Set<SysRoleTable>()
                    // on featureRole.RoleGUID equals role.Id

                    // select new SysFeatureGroupRoleView
                    // {
                    //     SysFeatureGroupRoleGUID = featureRole.SysFeatureGroupRoleGUID.GuidNullToString(),
                    //     RoleGUID = featureRole.RoleGUID.GuidNullToString(),
                    //     Action = featureRole.Action,
                    //     Create = featureRole.Create,
                    //     Delete = featureRole.Delete,
                    //     Read = featureRole.Read,
                    //     Update = featureRole.Update,
                    //     GroupId = featureGroup.GroupId,
                    //     SysFeatureGroupGUID = featureRole.SysFeatureGroupGUID.GuidNullToString(),
                    //     SysRoleTable_CompanyGUID = role.CompanyGUID.GuidNullToString(),

                    //     CreatedBy = featureRole.CreatedBy,
                    //     ModifiedBy = featureRole.ModifiedBy,
                    //     CreatedDateTime = featureRole.CreatedDateTime.DateTimeToString(),
                    //     ModifiedDateTime = featureRole.ModifiedDateTime.DateTimeToString()
                    // })
                    // .ToList();
                return list;
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SysFeatureGroupRoleView> GetSysFeatureGroupRoleViewByCompanySiteAdminRole(int siteLogin, Guid companyGuid)
        {
            try
            {
                //int siteLogin = (site != null) ? GetLoginSiteValue(site): -1;
                // should be only one ADMIN role for each site/company
                //IQueryable<SysRoleTable> roleQuery = db.Set<SysRoleTable>().Where(item => item.SiteLoginType == siteLogin &&
                //                                                                        item.CompanyGUID == companyGuid &&
                //                                                                        item.DisplayName == TextConstants.ADMIN);
                var searchParam = new SearchParameter();
                searchParam.Conditions.AddRange(SearchConditionService.GetSysFeatureGroupRoleByAdminRoleCondition(companyGuid, siteLogin));
                var predicate = searchParam.GetSearchPredicate(typeof(SysFeatureGroupRoleViewMap));
                var list = GetQuery()
                            .Where(predicate.Predicates, predicate.Values)
                            .AsNoTracking()
                            .ToMaps<SysFeatureGroupRoleViewMap, SysFeatureGroupRoleView>();

                    //(from featureRole in Entity.Where(item=> roleQuery.Any(item2=>item2.Id == item.RoleGUID))
                    // join featureGroup in db.Set<SysFeatureGroup>()
                    // on featureRole.SysFeatureGroupGUID equals featureGroup.SysFeatureGroupGUID

                    // join role in db.Set<SysRoleTable>()
                    // on featureRole.RoleGUID equals role.Id

                    // join company in db.Set<Company>()
                    // on role.CompanyGUID equals company.CompanyGUID

                    // select new SysFeatureGroupRoleView
                    // {
                    //     SysFeatureGroupRoleGUID = featureRole.SysFeatureGroupRoleGUID.GuidNullToString(),
                    //     RoleGUID = featureRole.RoleGUID.GuidNullToString(),
                    //     Action = featureRole.
                    //     SysRoleTable_CompanyGUID = company.CompanyGUID.GuidNullToString(),

                    //     CreatedBy = featureRole.CreatedBy,
                    //     ModifiedBy = featureRole.ModifiedBy,
                    //     CreatedDateTime = featureRole.CreatedDateTime.DateTimeToString(),
                    //     ModifiedDateTime = featureRole.ModifiedDateTime.DateTimeToString()
                    // })
                    // .ToList();
                return list.Select(s =>
                {
                    s.Action = 0;
                    s.Read = 0;
                    s.Create = 0;
                    s.Update = 0;
                    s.Delete = 0;
                    return s;
                });
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        
        public bool RoleExistsInFeatureRole(Guid roleId) {
            try {
                bool result = Entity.Any(item => item.RoleGUID == roleId);
                return result;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool AddRange(IEnumerable<SysFeatureGroupRole> list) {
            try {
                Entity.AddRange(list);
                return true;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<SysFeatureGroupRole> GetSysFeatureGroupRoleByRoleGUIDNoTracking(Guid roleId)
        {
            try
            {
                var result = Entity.Where(w => w.RoleGUID == roleId)
                                    .AsNoTracking()
                                    .ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<SysFeatureGroupExportView> GetExportRoleData(Guid roleGUID)
        {
            try
            {
                int site = SiteLoginValue.Back;
                var result =
                    (from featureGroup in (from fg in db.Set<SysFeatureGroup>()
                                           join fgs in db.Set<SysFeatureGroupStructure>()
                                           on fg.SysFeatureGroupGUID equals fgs.SysFeatureGroupGUID
                                           select fg)
                     join featureGroupRole in (from fgr in Entity
                                               join role in db.Set<SysRoleTable>().Where(w => w.Id == roleGUID && w.SiteLoginType == site)
                                               on fgr.RoleGUID equals role.Id
                                               select fgr)
                     on featureGroup.SysFeatureGroupGUID equals featureGroupRole.SysFeatureGroupGUID into ljSysFeatureGroup

                     from featureGroupRole in ljSysFeatureGroup.DefaultIfEmpty()
                    select new SysFeatureGroupExportView
                    {
                        PathMenu = featureGroup.PathMenu,
                        Reference = featureGroup.GroupId,
                        Action = (featureGroupRole != null) ? featureGroupRole.Action : 0,
                        Read = (featureGroupRole != null) ? featureGroupRole.Read : 0,
                        Create = (featureGroupRole != null) ? featureGroupRole.Create : 0,
                        Update = (featureGroupRole != null) ? featureGroupRole.Update : 0,
                        Delete = (featureGroupRole != null) ? featureGroupRole.Delete : 0
                    }).AsNoTracking().OrderBy(o => o.PathMenu).ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
