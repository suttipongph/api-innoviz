﻿using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Innoviz.SmartApp.Data.ViewModelsV2;

namespace Innoviz.SmartApp.Data.Repositories
{
    public interface ISysUserCompanyMappingRepo {
        IEnumerable<SysUserCompanyMappingView> GetListByUserNamevw(string username);

        IEnumerable<CompanyItemView> GetCompanyItemViewList();
        IEnumerable<BranchView> GetBranchViewList();
        IEnumerable<BranchView> GetBranchByCompanyGUID(string companyGUID);

        IEnumerable<SysUserCompanyMapping> CreateSysUserCompanyMapping(
                    IEnumerable<SysUserCompanyMapping> list, string userId);

        IEnumerable<SysUserCompanyMapping> UpdateSysUserCompanyMapping(
            IEnumerable<SysUserCompanyMapping> list, string userId);

        IQueryable<Branch> GetBranchByUserAndCompany(string userId, string companyGUID);
    }
    public class SysUserCompanyMappingRepo : BaseRepository<SysUserCompanyMapping>, ISysUserCompanyMappingRepo {
        public SysUserCompanyMappingRepo(SmartAppDbContext context) : base(context) {

        }
        public override void ValidateAdd(SysUserCompanyMapping item) {
            
        }
        public override void ValidateUpdate(SysUserCompanyMapping item) {
            
        }
        public override void ValidateRemove(SysUserCompanyMapping item) {

        }
        public IEnumerable<SysUserCompanyMappingView> GetListByUserNamevw(string username) 
        {
            try 
            {
                var queryUser = db.Set<SysUserTable>().Where(w => w.UserName == username);
                var result =
                    (from ucm in Entity
                     join user in queryUser
                     on ucm.UserGUID equals user.Id
                     join branch in db.Set<Branch>()
                     on ucm.BranchGUID equals branch.BranchGUID
                     join company in db.Set<Company>()
                     on ucm.CompanyGUID equals company.CompanyGUID
                     select new SysUserCompanyMappingView
                     {
                         UserGUID = user.Id.GuidNullToString(),
                         BranchGUID = branch.BranchGUID.GuidNullToString(),
                         Branch_BranchId = branch.BranchId,
                         CompanyGUID = company.CompanyGUID.GuidNullToString(),
                         Company_CompanyId = company.CompanyId,
                         Branch_Name = branch.Name,
                         Company_Name = company.Name,
                         SysUserTable_Name = user.Name,
                         SysUserTable_UserName = user.UserName,
                         Branch_Values = SmartAppUtil.GetDropDownLabel(branch.BranchId, branch.Name),
                         Company_Values = SmartAppUtil.GetDropDownLabel(company.CompanyId, company.Name),

                         CreatedBy = ucm.CreatedBy,
                         CreatedDateTime = ucm.CreatedDateTime.DateTimeToString(),
                         ModifiedBy = ucm.ModifiedBy,
                         ModifiedDateTime = ucm.ModifiedDateTime.DateTimeToString(),
                     });
                return result;
            }
            catch (Exception e) 
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }

        public IEnumerable<BranchView> GetBranchByCompanyGUID(string companyGUID) {
            try {
                var list =
                    (from branch in db.Set<Branch>().Where(b => b.CompanyGUID.ToString().ToLower() == companyGUID)
                     join company in db.Set<Company>() on branch.CompanyGUID equals company.CompanyGUID

                     select new BranchView
                     {
                         BranchGUID = branch.BranchGUID.GuidNullToString(),
                         BranchId = branch.BranchId,
                         Name = branch.Name,
                         CompanyGUID = branch.CompanyGUID.GuidNullToString(),
                         CompanyId = company.CompanyId,
                     })
                     .ToList();
                return list;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<BranchView> GetBranchViewList() {
            try {
                var list =
                    (from branch in db.Set<Branch>()
                     join company in db.Set<Company>() on branch.CompanyGUID equals company.CompanyGUID

                     select new BranchView
                     {
                         BranchGUID = branch.BranchGUID.GuidNullToString(),
                         BranchId = branch.BranchId,
                         Name = branch.Name,
                         CompanyGUID = branch.CompanyGUID.GuidNullToString(),
                         CompanyId = company.CompanyId,
                     })
                     .ToList();
                return list;
            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SysUserCompanyMapping> CreateSysUserCompanyMapping(
                                                IEnumerable<SysUserCompanyMapping> list,
                                                string userId) {
            try {
                Guid userID = new Guid(userId);
                foreach(var item in list) {
                    item.UserGUID = userID;
                }
                base.Add(list);
                return list;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SysUserCompanyMapping> UpdateSysUserCompanyMapping(IEnumerable<SysUserCompanyMapping> list, string userId) {
            try {
                Guid id = new Guid(userId);
                IEnumerable<SysUserCompanyMapping> oldList =
                    Entity.Where(item => item.UserGUID == id).ToList();

                if (oldList != null && oldList.Count() != 0) {
                    base.Remove(oldList);
                }
                if (list != null && list.Count() != 0) {
                    base.Add(list);
                }

                return list;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<CompanyItemView> GetCompanyItemViewList() {
            try {
                var list =
                    db.Set<Company>().ToCompanyItemView();
                return list.OrderBy(item=>item.CompanyId);
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IQueryable<Branch> GetBranchByUserAndCompany(string userId, string companyGUID) {
            try {
                Guid userID = new Guid(userId);
                Guid companyguid = new Guid(companyGUID);
                var result = (from mapping in Entity.Where(item => item.UserGUID == userID &&
                                                                    item.CompanyGUID == companyguid)
                              join branch in db.Set<Branch>()
                              on mapping.BranchGUID equals branch.BranchGUID

                              select new Branch
                              {
                                  BranchGUID = branch.BranchGUID,
                                  BranchId = branch.BranchId,
                                  CompanyGUID = branch.CompanyGUID,
                                  CompanyId = branch.CompanyId,
                                  Name = branch.Name,

                                  CreatedBy = branch.CreatedBy,
                                  CreatedDateTime = branch.CreatedDateTime,
                                  ModifiedBy = branch.ModifiedBy,
                                  ModifiedDateTime = branch.ModifiedDateTime
                              });
                return result;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
