﻿using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Data.Repositories
{
    public interface ISysDataInitializationHistoryRepo
    {
        List<SysDataInitializationHistory> GetAllDataInitializationHistory();
        bool CheckVersionExists(SysDataInitializationHistory version);
    }
    public class SysDataInitializationHistoryRepo: BaseRepository<SysDataInitializationHistory>, ISysDataInitializationHistoryRepo
    {
        public SysDataInitializationHistoryRepo(SmartAppDbContext context) : base(context) { }
        public SysDataInitializationHistoryRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }

        public List<SysDataInitializationHistory> GetAllDataInitializationHistory()
        {
            try
            {
                var result = Entity.AsNoTracking().ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool CheckVersionExists(SysDataInitializationHistory version)
        {
            try
            {
                if(version != null && !string.IsNullOrWhiteSpace(version.Version))
                {
                    return Entity.Any(a => a.Version == version.Version && a.DataInitializationType == version.DataInitializationType);
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
