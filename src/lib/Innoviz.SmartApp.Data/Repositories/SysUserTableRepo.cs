﻿using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandler;
using Innoviz.SmartApp.Data.ViewModelHandler.Models;
using Innoviz.SmartApp.Data.ViewModels;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;

namespace Innoviz.SmartApp.Data.Repositories
{
    public interface ISysUserTableRepo
    {
        SysUserSettingsItemView GetUserSettingsById(string userId);
        SysUserTable CreateSysUserTable(SysUserTable model, string id);

        // update all properties
        void Update(SysUserTable item);

        SysUserTable UpdateBySysUserTableView(SysUserTable item);
        SysUserTable UpsdateBySysUserSettingsView(SysUserTable item);

        SysUserTable UpdateInActiveUser(SysUserTable item);

        bool HasAnyUser();
        bool Exist(Expression<Func<SysUserTable, bool>> predicate);

        IEnumerable<SelectItem<AdUserView>> GetDropDownAdUserForCreateUser(IEnumerable<AdUserView> adUsers);
        Guid? GetUserBusinessUnitGUIDByUserName(string userName, Guid companyGUID);
        List<SysUserTable> GetSysUserTableByUserNameNoTracking(List<string> usernames);
        SysUserTable GetSysUserTableByUserNameNoTracking(string username);
        //SysUserTable GetSysUserTableByIdNoTracking(Guid id);
        #region DropDown
        IEnumerable<SelectItem<SysUserTableItemView>> GetDropDownItem(SearchParameter search);
        IEnumerable<SelectItem<SysUserTableItemView>> GetDropDownItemUserNameValue(SearchParameter search);
        #endregion DropDown
        SearchResult<SysUserTableListView> GetListvw(SearchParameter search);
        SysUserTableItemView GetByIdvw(Guid id);
        SysUserTable Find(params object[] keyValues);
        SysUserTable GetSysUserTableByIdNoTracking(Guid guid);
        SysUserTable CreateSysUserTable(SysUserTable sysUserTable);
        void CreateSysUserTableVoid(SysUserTable sysUserTable);
        SysUserTable UpdateSysUserTable(SysUserTable dbSysUserTable, SysUserTable inputSysUserTable, List<string> skipUpdateFields = null);
        void UpdateSysUserTableVoid(SysUserTable dbSysUserTable, SysUserTable inputSysUserTable, List<string> skipUpdateFields = null);
        void Remove(SysUserTable item);
    }
    public class SysUserTableRepo : BaseRepository<SysUserTable>, ISysUserTableRepo
    {
        public SysUserTableRepo(SmartAppDbContext context) : base(context)
        {

        }
        
        public override void ValidateAdd(SysUserTable item)
        {
            base.ValidateAdd(item);
        }
        public override void ValidateUpdate(SysUserTable item)
        {
            base.ValidateUpdate(item);
        }
        public override void ValidateRemove(SysUserTable item)
        {

        }
        public SysUserTable CreateSysUserTable(SysUserTable model, string id)
        {
            try
            {
                model.Id = new Guid(id);
                base.Add(model);
                return model;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public SysUserTable UpdateBySysUserTableView(SysUserTable item)
        {
            try
            {
                item.ModifiedBy = db.GetUserName();
                item.ModifiedDateTime = DateTime.Now;

                var dbModelProps = typeof(SysUserTable).GetProperties().Where(p=>p.Name != "Id");
                var vwModelProps = typeof(SysUserTableView).GetProperties();
                var propsToUpdate = vwModelProps.Where(vwProps => 
                                        dbModelProps.Any(dbProps => dbProps.Name == vwProps.Name));

                if(propsToUpdate.Count() != 0)
                {

                    db.Attach(item);
                    var dbEntityEntry = db.Entry(item);
                    foreach (var props in propsToUpdate)
                    {
                        dbEntityEntry.Property(props.Name).IsModified = true;
                    }

                    dbEntityEntry.Property("ModifiedBy").IsModified = true;
                    dbEntityEntry.Property("ModifiedDateTime").IsModified = true;
                }
                return item;
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SysUserTable UpsdateBySysUserSettingsView(SysUserTable item)
        {
            try
            {
                item.ModifiedBy = db.GetUserName();
                item.ModifiedDateTime = DateTime.Now;

                var dbModelProps = typeof(SysUserTable).GetProperties().Where(p => p.Name != "Id");
                var vwModelProps = typeof(SysUserSettingsItemView).GetProperties();
                var propsToUpdate = vwModelProps.Where(vwProps =>
                                        dbModelProps.Any(dbProps => dbProps.Name == vwProps.Name));


                if (propsToUpdate.Count() != 0)
                {
                    db.Attach(item);
                    var dbEntityEntry = db.Entry(item);
                    foreach (var props in propsToUpdate)
                    {
                        dbEntityEntry.Property(props.Name).IsModified = true;
                    }

                    dbEntityEntry.Property("ModifiedBy").IsModified = true;
                    dbEntityEntry.Property("ModifiedDateTime").IsModified = true;
                }
                return item;
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SysUserTable UpdateInActiveUser(SysUserTable item)
        {
            try
            {
                item.ModifiedBy = db.GetUserName();
                item.ModifiedDateTime = DateTime.Now;

                string[] propsToUpdate = new string[] { "ModifiedBy", "ModifiedDateTime", "InActive" };

                db.Attach(item);
                var dbEntityEntry = db.Entry(item);

                foreach (var prop in propsToUpdate)
                {
                    dbEntityEntry.Property(prop).IsModified = true;
                }
                return item;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        public bool HasAnyUser()
        {
            try
            {
                return Entity.Any();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public IEnumerable<SelectItem<AdUserView>> GetDropDownAdUserForCreateUser(IEnumerable<AdUserView> adUsers)
        {
            try
            {
                var result = adUsers.Where(aduser => !Entity.Any(item => item.UserName == aduser.SamAccountName) &&
                                                        Convert.ToBoolean(aduser.Enabled))
                                .Select(s => new SelectItem<AdUserView>
                                {
                                    Label = s.SamAccountName,
                                    Value = s.SamAccountName,
                                    RowData = s
                                })
                                .OrderBy(o => o.Label);
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public Guid? GetUserBusinessUnitGUIDByUserName(string userName, Guid companyGUID)
        {
            try
            {
                var result =
                    (from user in Entity.Where(u => u.UserName == userName)
                     join employee in db.Set<EmployeeTable>().Where(e => e.CompanyGUID == companyGUID)
                     on user.Id equals employee.UserId
                     select employee)
                     .AsNoTracking()
                     .FirstOrDefault();
                return result?.BusinessUnitGUID;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<SysUserTable> GetSysUserTableByUserNameNoTracking(List<string> usernames)
        {
            try
            {
                if(usernames == null)
                {
                    return null;
                }
                var result = Entity.Where(item => usernames.Contains(item.UserName)).AsNoTracking().ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SysUserTable GetSysUserTableByUserNameNoTracking(string username)
        {
            try
            {
                List<string> user = new List<string>() { username };
                return GetSysUserTableByUserNameNoTracking(user).FirstOrDefault();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SysUserTable GetSysUserTableByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.Id == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        private IQueryable<SysUserTableItemViewMap> GetDropDownQuery()
        {
            return (from sysUserTable in Entity
                    select new SysUserTableItemViewMap
                    {
                        Id = sysUserTable.Id,
                        UserName = sysUserTable.UserName,
                        Name = sysUserTable.Name,
                        InActive = sysUserTable.InActive
                    });
        }
        public IEnumerable<SelectItem<SysUserTableItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<SysUserTable>(search);
                var sysUserTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values).ToMaps<SysUserTableItemViewMap, SysUserTableItemView>().ToDropDownItem();
                return sysUserTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<SysUserTableItemView>> GetDropDownItemUserNameValue(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<SysUserTable>(search);
                var sysUserTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values).ToMaps<SysUserTableItemViewMap, SysUserTableItemView>().ToDropDownItem(true);
                return sysUserTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #region GetListvw
        private IQueryable<SysUserTableListViewMap> GetListQuery()
        {
            return (from sysUserTable in Entity
                    select new SysUserTableListViewMap
                    {
                        Id = sysUserTable.Id,
                        UserName = sysUserTable.UserName,
                        Name = sysUserTable.Name,
                        Email = sysUserTable.Email,
                        PhoneNumber = sysUserTable.PhoneNumber,
                        InActive = sysUserTable.InActive,
                        LanguageId = sysUserTable.LanguageId,
                    });
        }
        public SearchResult<SysUserTableListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<SysUserTableListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<SysUserTable>(search);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<SysUserTableListViewMap, SysUserTableListView>();
                result = list.SetSearchResult<SysUserTableListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<SysUserTableItemViewMap> GetItemQuery()
        {
            return (from sysUserTable in Entity
                    select new SysUserTableItemViewMap
                    {
                        CreatedBy = sysUserTable.CreatedBy,
                        CreatedDateTime = sysUserTable.CreatedDateTime,
                        ModifiedBy = sysUserTable.ModifiedBy,
                        ModifiedDateTime = sysUserTable.ModifiedDateTime,
                        Id = sysUserTable.Id,
                        UserName = sysUserTable.UserName,
                        NormalizedUserName = sysUserTable.NormalizedUserName,
                        Email = sysUserTable.Email,
                        NormalizedEmail = sysUserTable.NormalizedEmail,
                        InActive = sysUserTable.InActive,
                        LanguageId = sysUserTable.LanguageId,
                        PhoneNumber = sysUserTable.PhoneNumber,
                        DateFormatId = sysUserTable.DateFormatId,
                        TimeFormatId = sysUserTable.TimeFormatId,
                        NumberFormatId = sysUserTable.NumberFormatId,
                        Name = sysUserTable.Name,

                        RowVersion = sysUserTable.RowVersion,
                    });
        }
        public SysUserTableItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<SysUserTableItemViewMap, SysUserTableItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        private IQueryable<SysUserTableViewMap> GetSysUserSettingsQuery()
        {
            try
            {
                return
                (from sysUserTable in Entity
                 join company in db.Set<Company>()
                 on sysUserTable.DefaultCompanyGUID equals company.CompanyGUID into lj1

                 from company in lj1.DefaultIfEmpty()
                 join branch in db.Set<Branch>()
                 on sysUserTable.DefaultBranchGUID equals branch.BranchGUID into lj2

                 from branch in lj2.DefaultIfEmpty()

                 select new SysUserTableViewMap
                 {
                     Id = sysUserTable.Id,
                     UserName = sysUserTable.UserName,
                     Email = sysUserTable.Email,

                     PhoneNumber = sysUserTable.PhoneNumber,
                     Name = sysUserTable.Name,
                     DateFormatId = sysUserTable.DateFormatId,
                     TimeFormatId = sysUserTable.TimeFormatId,
                     NumberFormatId = sysUserTable.NumberFormatId,
                     InActive = sysUserTable.InActive,
                     LanguageId = sysUserTable.LanguageId,

                     DefaultBranchGUID = sysUserTable.DefaultBranchGUID,
                     DefaultCompanyGUID = sysUserTable.DefaultCompanyGUID,

                     Company_CompanyId = (company != null) ? company.CompanyId : null,

                     Branch_BranchId = (branch != null) ? branch.BranchId : null,

                     ShowSystemLog = sysUserTable.ShowSystemLog,
                     UserImage = sysUserTable.UserImage,

                     CreatedBy = sysUserTable.CreatedBy,
                     CreatedDateTime = sysUserTable.CreatedDateTime,
                     ModifiedBy = sysUserTable.ModifiedBy,
                     ModifiedDateTime = sysUserTable.ModifiedDateTime,

                     RowVersion = sysUserTable.RowVersion,
                 });
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SysUserSettingsItemView GetUserSettingsById(string userId)
        {
            try
            {
                Guid userID = new Guid(userId);
                var result = GetSysUserSettingsQuery()
                            .Where(item => item.Id == userID)
                            .AsNoTracking()
                            .FirstOrDefault()
                            .ToMap<SysUserTableViewMap, SysUserSettingsItemView>();
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public SysUserTable CreateSysUserTable(SysUserTable sysUserTable)
        {
            try
            {
                //sysUserTable.Id = Guid.NewGuid();
                base.Add(sysUserTable);
                return sysUserTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateSysUserTableVoid(SysUserTable sysUserTable)
        {
            try
            {
                CreateSysUserTable(sysUserTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SysUserTable UpdateSysUserTable(SysUserTable dbSysUserTable, SysUserTable inputSysUserTable, List<string> skipUpdateFields = null)
        {
            try
            {
                dbSysUserTable = dbSysUserTable.MapUpdateValues<SysUserTable>(inputSysUserTable, skipUpdateFields);
                base.Update(dbSysUserTable);
                return dbSysUserTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateSysUserTableVoid(SysUserTable dbSysUserTable, SysUserTable inputSysUserTable, List<string> skipUpdateFields = null)
        {
            try
            {
                dbSysUserTable = dbSysUserTable.MapUpdateValues<SysUserTable>(inputSysUserTable, skipUpdateFields);
                base.Update(dbSysUserTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Create, Update, Delete
    }
}
