﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.ViewModels;
using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Data.ViewModelHandler.Models;
using Microsoft.EntityFrameworkCore;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.Data.ViewMaps;

namespace Innoviz.SmartApp.Data.Repositories
{
    public interface ISysRoleTableRepo
    {
        bool AddRange(IEnumerable<SysRoleTable> items);



        //SysRoleTable CreateSysRoleTable(SysRoleTable item);
        SysRoleTable UpdateSysRoleTable(SysRoleTable item);

        bool DeleteSysRoleTable(string roleId);

        SysRoleTableView GetOBSiteRole();
        List<SysRoleTable> GetAdminRoleByCompanies(List<Guid> companies);

        #region DropDown
        IEnumerable<SelectItem<SysRoleTableItemView>> GetDropDownItem(SearchParameter search);
        #endregion DropDown
        SearchResult<SysRoleTableListView> GetListvw(SearchParameter search);
        SysRoleTableItemView GetByIdvw(Guid id);
        SysRoleTable Find(params object[] keyValues);
        SysRoleTable GetSysRoleTableByIdNoTracking(Guid guid);
        SysRoleTable CreateSysRoleTable(SysRoleTable sysRoleTable);
        void CreateSysRoleTableVoid(SysRoleTable sysRoleTable);
        SysRoleTable UpdateSysRoleTable(SysRoleTable dbSysRoleTable, SysRoleTable inputSysRoleTable, List<string> skipUpdateFields = null);
        void UpdateSysRoleTableVoid(SysRoleTable dbSysRoleTable, SysRoleTable inputSysRoleTable, List<string> skipUpdateFields = null);
        void Remove(SysRoleTable item);
        List<SysRoleTable> GetAdminRoleBySiteNoTracking(int site);
        List<SysRoleTable> GetNotAdminRoleBySiteNoTracking(int site);
        List<SysRoleTable> GetAdminRolesByCompanyNoTracking(Guid companyGUID);
    }
    public class SysRoleTableRepo : CompanyBaseRepository<SysRoleTable>, ISysRoleTableRepo
    {

        public SysRoleTableRepo(SmartAppDbContext context) : base(context) { }
        public SysRoleTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public override void ValidateAdd(SysRoleTable model)
        {
            try
            {
                base.ValidateAdd(model);
                if (model.DisplayName == null || model.DisplayName.Trim() == "")
                {
                    SmartAppException ex = new SmartAppException("ERROR.ERROR");
                    ex.AddData("ERROR.00073");
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public override void ValidateUpdate(SysRoleTable model)
        {
            try
            {
                base.ValidateUpdate(model);
                if (model.DisplayName == null || model.DisplayName.Trim() == "")
                {
                    SmartAppException ex = new SmartAppException("ERROR.ERROR");
                    ex.AddData("ERROR.00073");
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public override void ValidateRemove(SysRoleTable item)
        {

        }
        public bool AddRange(IEnumerable<SysRoleTable> items)
        {
            try
            {
                Entity.AddRange(items);
                return true;
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        //public SysRoleTable CreateSysRoleTable(SysRoleTable item)
        //{
        //    try
        //    {
        //        Guid id = Guid.NewGuid();
        //        item.Id = id;
        //        item.Name = id.GuidNullToString();
        //        item.NormalizedName = id.GuidNullToString().ToUpper();
                
        //        item.CreatedBy = db.GetUserName();
        //        item.ModifiedBy = db.GetUserName();
        //        item.CreatedDateTime = DateTime.Now;
        //        item.ModifiedDateTime = DateTime.Now;

        //        Entity.Add(item);
        //        return item;
        //    }
        //    catch(Exception e)
        //    {
        //        throw SmartAppUtil.AddStackTrace(e);
        //    }
        //}
        public SysRoleTable UpdateSysRoleTable(SysRoleTable item)
        {
            try
            {
                item.ModifiedBy = db.GetUserName();
                item.ModifiedDateTime = DateTime.Now;

                Entity.Update(item);
                return item;
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteSysRoleTable(string roleId)
        {
            try
            {
                Guid roleID = new Guid(roleId);
                SysRoleTable item = new SysRoleTable();
                item.Id = roleID;
                base.Remove(item);
                return true;
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private IQueryable<SysRoleTableViewMap> GetSysRoleTableQuery()
        {
            try
            {
                var result = (from role in Entity
                              join company in db.Set<Company>()
                              on role.CompanyGUID equals company.CompanyGUID

                              select new SysRoleTableViewMap
                              {
                                  Id = role.Id,
                                  Name = role.Name,
                                  NormalizedName = role.NormalizedName,
                                  DisplayName = role.DisplayName,

                                  SiteLoginType = role.SiteLoginType,

                                  CompanyId = company.CompanyId,
                                  CompanyGUID = company.CompanyGUID,
                                  Company_Name = company.Name,

                                  CreatedBy = role.CreatedBy,
                                  CreatedDateTime = role.CreatedDateTime,
                                  ModifiedBy = role.ModifiedBy,
                                  ModifiedDateTime = role.ModifiedDateTime
                              });
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        
        public SysRoleTableView GetOBSiteRole()
        {
            try
            {
                var obSite = SysAccessLevelHelper.GetLoginSiteValue(SiteLoginType.OB);
                var currentCompany = SysParm.CompanyGUID.StringToGuid();
                var result = GetSysRoleTableQuery()
                                .Where(item => item.SiteLoginType == obSite && item.CompanyGUID == currentCompany)
                                .AsNoTracking()
                                .FirstOrDefault()
                                .ToMap<SysRoleTableViewMap, SysRoleTableView>();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        public List<SysRoleTable> GetAdminRoleByCompanies(List<Guid> companies)
        {
            try
            {
                var result = Entity.Where(w => companies.Contains(w.CompanyGUID) && w.DisplayName == TextConstants.ADMIN)
                                    .AsNoTracking()
                                    .ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SysRoleTable GetSysRoleTableByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.Id == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        private IQueryable<SysRoleTableItemViewMap> GetDropDownQuery()
        {
            return (from sysRoleTable in Entity
                    select new SysRoleTableItemViewMap
                    {
                        CompanyGUID = sysRoleTable.CompanyGUID,
                        Owner = sysRoleTable.Owner,
                        OwnerBusinessUnitGUID = sysRoleTable.OwnerBusinessUnitGUID,
                        Id = sysRoleTable.Id,
                        DisplayName = sysRoleTable.DisplayName,
                        SiteLoginType = sysRoleTable.SiteLoginType
                    });
        }
        public IEnumerable<SelectItem<SysRoleTableItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                search.Conditions.Add(SearchConditionService.GetSiteLoginCondition(SysParm.SiteLogin));
                //var predicate = base.GetFilterLevelPredicate<SysRoleTable>(search, SysParm.CompanyGUID);
                var predicate = search.GetSearchPredicate(typeof(SysRoleTableItemViewMap));
                var sysRoleTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values).ToMaps<SysRoleTableItemViewMap, SysRoleTableItemView>().ToDropDownItem();
                return sysRoleTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #region GetListvw
        private IQueryable<SysRoleTableListViewMap> GetListQuery()
        {
            return (from sysRoleTable in Entity
                    join company in db.Set<Company>()
                    on sysRoleTable.CompanyGUID equals company.CompanyGUID
                    select new SysRoleTableListViewMap
                    {
                        CompanyGUID = sysRoleTable.CompanyGUID,
                        Owner = sysRoleTable.Owner,
                        OwnerBusinessUnitGUID = sysRoleTable.OwnerBusinessUnitGUID,
                        Id = sysRoleTable.Id,
                        DisplayName = sysRoleTable.DisplayName,
                        CompanyId = company.CompanyId,
                        SiteLoginType = sysRoleTable.SiteLoginType
                    });
        }
        public SearchResult<SysRoleTableListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<SysRoleTableListView>();
            try
            {
                search.Conditions.Add(SearchConditionService.GetSiteLoginCondition(SysParm.SiteLogin));
                var predicate = base.GetFilterLevelPredicate<SysRoleTable>(search, SysParm.CompanyGUID);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<SysRoleTableListViewMap, SysRoleTableListView>();
                result = list.SetSearchResult<SysRoleTableListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<SysRoleTableItemViewMap> GetItemQuery()
        {
            return (from sysRoleTable in Entity
                    join company in db.Set<Company>()
                    on sysRoleTable.CompanyGUID equals company.CompanyGUID
                    join ownerBU in db.Set<BusinessUnit>()
                    on sysRoleTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljSysRoleTableOwnerBU
                    from ownerBU in ljSysRoleTableOwnerBU.DefaultIfEmpty()
                    select new SysRoleTableItemViewMap
                    {
                        CompanyGUID = sysRoleTable.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = sysRoleTable.Owner,
                        OwnerBusinessUnitGUID = sysRoleTable.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        CreatedBy = sysRoleTable.CreatedBy,
                        CreatedDateTime = sysRoleTable.CreatedDateTime,
                        ModifiedBy = sysRoleTable.ModifiedBy,
                        ModifiedDateTime = sysRoleTable.ModifiedDateTime,
                        Id = sysRoleTable.Id,
                        DisplayName = sysRoleTable.DisplayName,
                        SiteLoginType = sysRoleTable.SiteLoginType,
                        Name = sysRoleTable.Name,
                        NormalizedName = sysRoleTable.NormalizedName,

                        RowVersion = sysRoleTable.RowVersion,
                    });
        }
        public SysRoleTableItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<SysRoleTableItemViewMap, SysRoleTableItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public SysRoleTable CreateSysRoleTable(SysRoleTable sysRoleTable)
        {
            try
            {
                Guid id = Guid.NewGuid();
                sysRoleTable.Id = id;
                sysRoleTable.Name = id.GuidNullToString();
                sysRoleTable.NormalizedName = id.GuidNullToString().ToUpper();
                base.Add(sysRoleTable);
                return sysRoleTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateSysRoleTableVoid(SysRoleTable sysRoleTable)
        {
            try
            {
                CreateSysRoleTable(sysRoleTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SysRoleTable UpdateSysRoleTable(SysRoleTable dbSysRoleTable, SysRoleTable inputSysRoleTable, List<string> skipUpdateFields = null)
        {
            try
            {
                dbSysRoleTable = dbSysRoleTable.MapUpdateValues<SysRoleTable>(inputSysRoleTable);
                base.Update(dbSysRoleTable);
                return dbSysRoleTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateSysRoleTableVoid(SysRoleTable dbSysRoleTable, SysRoleTable inputSysRoleTable, List<string> skipUpdateFields = null)
        {
            try
            {
                dbSysRoleTable = dbSysRoleTable.MapUpdateValues<SysRoleTable>(inputSysRoleTable, skipUpdateFields);
                base.Update(dbSysRoleTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Create, Update, Delete

        public List<SysRoleTable> GetAdminRoleBySiteNoTracking(int site)
        {
            try
            {
                Guid companyGUID = SysParm.CompanyGUID.StringToGuid();
                var result = Entity.Where(w => w.DisplayName == TextConstants.ADMIN && w.SiteLoginType == site &&
                                                w.CompanyGUID == companyGUID)
                                    .AsNoTracking()
                                    .ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<SysRoleTable> GetNotAdminRoleBySiteNoTracking(int site)
        {
            try
            {
                Guid companyGUID = SysParm.CompanyGUID.StringToGuid();
                var result = Entity.Where(w => w.DisplayName != TextConstants.ADMIN && w.SiteLoginType == site &&
                                                w.CompanyGUID == companyGUID)
                                    .AsNoTracking()
                                    .ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<SysRoleTable> GetAdminRolesByCompanyNoTracking(Guid companyGUID)
        {
            try
            {
                var result = Entity.Where(w => w.DisplayName == TextConstants.ADMIN && w.CompanyGUID == companyGUID)
                                .AsNoTracking()
                                .ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
