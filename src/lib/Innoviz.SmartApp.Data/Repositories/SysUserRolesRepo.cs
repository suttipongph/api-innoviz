﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Data.Repositories
{
    public interface ISysUserRolesRepo
    {
        bool AddRange(IEnumerable<SysUserRoles> items);
        IEnumerable<SysUserRolesView> GetUserRolesViewByUserId(string userId, int site);

        IEnumerable<SysUserRoles> CreateSysUserRoles(IEnumerable<SysUserRoles> list, string userId);
        IEnumerable<SysUserRoles> UpdateSysUserRoles(IEnumerable<SysUserRoles> list, string userId, int site);

        bool RoleExistsInUserRole(string roleId);
    }
    public class SysUserRolesRepo : BaseRepository<SysUserRoles>, ISysUserRolesRepo
    {
        public SysUserRolesRepo(SmartAppDbContext context) : base(context)
        {

        }
        public override void ValidateAdd(SysUserRoles item)
        {
            base.ValidateAdd(item);
        }
        public override void ValidateUpdate(SysUserRoles item)
        {
            base.ValidateUpdate(item);
        }
        public override void ValidateRemove(SysUserRoles item)
        {

        }
        public bool RoleExistsInUserRole(string roleId)
        {
            try
            {
                var result = Entity.Any(item => item.RoleId.ToString().ToLower() == roleId);
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool AddRange(IEnumerable<SysUserRoles> items)
        {
            try
            {
                Entity.AddRange(items);
                return true;
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SysUserRolesView> GetUserRolesViewByUserId(string userId, int site)
        {
            try
            {
                IQueryable<SysRoleTable> roleQuery = db.Set<SysRoleTable>();
                //if(site != null)
                //{
                    //int siteLogin = GetLoginSiteValue(site);
                    int siteLogin = site;
                    roleQuery = roleQuery.Where(item => item.SiteLoginType == siteLogin);
                //}

                
                Guid userGUID = new Guid(userId);
                var list = (from userrole in Entity.Where(item=> item.UserId == userGUID)
                            join user in db.Set<SysUserTable>().Where(item=>item.Id == userGUID)
                            on userrole.UserId equals user.Id into lj1

                            from user in lj1
                            join role in roleQuery 
                            on userrole.RoleId equals role.Id into lj2

                            from role in lj2.DefaultIfEmpty()
                            join company in db.Set<Company>() 
                            on role.CompanyGUID equals company.CompanyGUID into lj3

                            from company in lj3

                            select new SysUserRolesView
                            {
                                UserId = userrole.UserId.GuidNullToString(),
                                RoleId = userrole.RoleId.GuidNullToString(),

                                SysUserTable_UserName = user.UserName,
                                SysRoleTable_Name = role.Name,
                                SysRoleTable_DisplayName = role.DisplayName,
                                SysRoleTable_CompanyGUID = role.CompanyGUID.GuidNullToString(),
                                SysRoleTable_Company_CompanyId = (company != null) ? company.CompanyId : null,
                                SysRoleTable_Company_Name = (company != null) ? company.Name : null,
                                SysRoleTable_SiteLoginType = role.SiteLoginType
                            }).ToList();

                return list;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SysUserRoles> CreateSysUserRoles(
                                                IEnumerable<SysUserRoles> list,
                                                string userId)
        {
            try
            {
                Guid userID = new Guid(userId);
                foreach (var item in list)
                {
                    item.UserId = userID;
                }
                Entity.AddRange(list);
                return list;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SysUserRoles> UpdateSysUserRoles(IEnumerable<SysUserRoles> list, 
                                                            string userId, int site)
        {
            try
            {
                Guid id = new Guid(userId);
                //int siteLoginType = GetLoginSiteValue(site);
                int siteLoginType = site;
                IEnumerable<SysUserRoles> oldList = 
                    (from userrole in Entity.Where(item => item.UserId == id)
                    join role in db.Set<SysRoleTable>().Where(item => item.SiteLoginType == siteLoginType)
                    on userrole.RoleId equals role.Id
                    select userrole)
                    .ToList();

                if (oldList != null && oldList.Count() != 0)
                {
                    base.Remove(oldList);
                }
                if (list != null && list.Count() != 0)
                {
                    Entity.AddRange(list);
                }

                return list;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private int GetLoginSiteValue(string site)
        {
            try
            {
                switch (site)
                {
                    case SiteLoginType.FRONT: return 0;
                    case SiteLoginType.BACK: return 1;
                    case SiteLoginType.CASHIER: return 2;
                    case SiteLoginType.OB: return 3;

                    default: return 0;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
