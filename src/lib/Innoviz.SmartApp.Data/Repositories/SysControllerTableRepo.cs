﻿using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface ISysControllerTableRepo
    {
        List<SysControllerTable> GetListSysControllerTableNoTracking();
    }
    public class SysControllerTableRepo : BaseRepository<SysControllerTable>, ISysControllerTableRepo
    {
        public SysControllerTableRepo(SmartAppDbContext context) : base(context) { }
        public SysControllerTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        
        public List<SysControllerTable> GetListSysControllerTableNoTracking()
        {
            try
            {
                var result = Entity
                                .AsNoTracking()
                                .ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}

