﻿using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Data.Repositories
{
    public interface ISysScopeRoleRepo
    {
        List<SysScopeRole> GetSysScopeRoleByRoleGUIDNoTracking(IEnumerable<Guid> roleGuids);
    }
    public class SysScopeRoleRepo: BaseRepository<SysScopeRole>, ISysScopeRoleRepo
    {
        public SysScopeRoleRepo(SmartAppDbContext context) : base(context)
        {

        }
        public override void ValidateAdd(SysScopeRole item)
        {
            base.ValidateAdd(item);
        }
        public override void ValidateUpdate(SysScopeRole item)
        {
            base.ValidateUpdate(item);
        }
        public override void ValidateRemove(SysScopeRole item)
        {
            
        }
        public List<SysScopeRole> GetSysScopeRoleByRoleGUIDNoTracking(IEnumerable<Guid> roleGuids)
        {
            try
            {
                var result = Entity.Where(w => roleGuids.Contains(w.RoleGUID))
                                    .AsNoTracking()
                                    .ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
