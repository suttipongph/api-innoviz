﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface ISysControllerRoleRepo 
    {
        bool RoleExistsInControllerRole(string roleId);
        List<SysControllerRole> PrepareSysControllerRoleForCreateOrUpdate(IEnumerable<SysFeatureGroupRole> featureRoles);
        List<SysControllerRole> GetSysControllerRoleByRoleGUIDNoTracking(Guid roleId);
    }
    public class SysControllerRoleRepo : CompanyBaseRepository<SysControllerRole>, ISysControllerRoleRepo
    {
        public SysControllerRoleRepo(SmartAppDbContext context) : base(context) { }
        public SysControllerRoleRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public bool RoleExistsInControllerRole(string roleId) 
        {
            try 
            {
                var result = Entity.Any(item => item.RoleGUID.ToString().ToLower() == roleId);
                return result;
            }
            catch(Exception e) 
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<SysControllerRole> PrepareSysControllerRoleForCreateOrUpdate(IEnumerable<SysFeatureGroupRole> featureRoles) 
        {
            try 
            {
                //var list =
                //    (from featureRole in featureRoles.AsQueryable()
                //                //join featureController in db.Set<SysFeatureGroupController>() on
                //            join featureController in db.Set<SysFeatureGroupController>() on
                //            new { fGUID = featureRole.SysFeatureGroupGUID, ar = featureRole.AccessRight } equals
                //            new { fGUID = featureController.SysFeatureTableGUID, ar = featureController.AccessRight }
                //            //new { fGUID = featureController.SysFeatureGroupGUID, ar = featureController.AccessRight }

                //            select new SysControllerRole
                //            {
                //                RoleGUID = featureRole.RoleGUID,
                //                SysControllerTableGUID = featureController.SysControllerTableGUID,

                //            }).ToList();

                //if(list.Count() != 0) {
                //    foreach (var item in list) {
                //        item.SysControllerRoleGUID = Guid.NewGuid();
                //    }
                //}
                //base.Add(list);
                if(featureRoles == null || featureRoles.Count() == 0)
                {
                    return new List<SysControllerRole>();
                }
                Guid roleId = featureRoles.FirstOrDefault().RoleGUID;
                List<Guid> featureGroups = featureRoles.Select(s => s.SysFeatureGroupGUID).ToList();
                var featureGroupControllers = db.Set<SysFeatureGroupController>().Where(w => featureGroups.Contains(w.SysFeatureGroupGUID)).ToList();
                var featureGroupAccessRight =
                    featureRoles.Select(s => new
                    {
                        FeatureGroups = new []
                        {
                            new 
                            {
                                SysFeatureGroupGUID = s.SysFeatureGroupGUID,
                                AccessRight = (s.Read > 0) ? (int)AccessRight.Read: -1,
                                AccessLevel = s.Read
                            },
                            new 
                            {
                                SysFeatureGroupGUID = s.SysFeatureGroupGUID,
                                AccessRight = (s.Update > 0) ? (int)AccessRight.Update: -1,
                                AccessLevel = s.Update
                            },
                            new 
                            {
                                SysFeatureGroupGUID = s.SysFeatureGroupGUID,
                                AccessRight = (s.Create > 0) ? (int)AccessRight.Create: -1,
                                AccessLevel = s.Create
                            },
                            new 
                            {
                                SysFeatureGroupGUID = s.SysFeatureGroupGUID,
                                AccessRight = (s.Delete > 0) ? (int)AccessRight.Delete: -1,
                                AccessLevel = s.Delete
                            },
                            new 
                            {
                                SysFeatureGroupGUID = s.SysFeatureGroupGUID,
                                AccessRight = (s.Action > 0) ? (int)AccessRight.Action: -1,
                                AccessLevel = s.Action
                            },
                        }
                    })
                    .SelectMany(sm => sm.FeatureGroups.Where(w => w.AccessRight > 0)).ToList();
                var list =
                    (from featureGroup in featureGroupAccessRight
                     join fgc in featureGroupControllers
                     on new { SysFeatureGroupGUID = featureGroup.SysFeatureGroupGUID, AccessRight = featureGroup.AccessRight }
                     equals new { SysFeatureGroupGUID = fgc.SysFeatureGroupGUID, AccessRight = fgc.AccessRight }
                     select new SysControllerRole
                     {
                         SysControllerRoleGUID = Guid.NewGuid(),
                         AccessLevel = featureGroup.AccessLevel,
                         RoleGUID = roleId,
                         SysControllerTableGUID = fgc.SysControllerTableGUID,
                         CreatedBy = SysParm.UserName,
                         CreatedDateTime = DateTime.Now,
                         ModifiedBy = SysParm.UserName,
                         ModifiedDateTime = DateTime.Now
                     }).ToList();
                return list;
            }
            catch(Exception e) 
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<SysControllerRole> GetSysControllerRoleByRoleGUIDNoTracking(Guid roleId)
        {
            try
            {
                var result = Entity.Where(w => w.RoleGUID == roleId)
                                    .AsNoTracking()
                                    .ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
