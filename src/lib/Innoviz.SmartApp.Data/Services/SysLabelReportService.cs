﻿using EFCore.BulkExtensions;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Data.Services
{
    public interface ISysLabelReportService
    {
        bool InitializeSysLabelReport(List<SysLabelReport> list, List<SysLabelReport_CUSTOM> listCustom);
    }

    public class SysLabelReportService : SmartAppService, ISysLabelReportService
    {
        public SysLabelReportService(SmartAppDbContext context): base(context)
        {

        }
        public SysLabelReportService() { }

        public bool InitializeSysLabelReport(List<SysLabelReport> list, List<SysLabelReport_CUSTOM> listCustom)
        {
            try
            {
                SmartAppUtil.LogMessage(Serilog.Events.LogEventLevel.Information, "Initializing SysLabelReport...");
                list = list.Select(s =>
                {
                    s.SysLabelReportGUID = Guid.NewGuid();
                    return s;
                })
                .ToList();
                listCustom = listCustom.Select(s =>
                {
                    s.SysLabelReport_CUSTOMGUID = Guid.NewGuid();
                    return s;
                })
                .ToList();
                if(list.Count > 0 || listCustom.Count > 0)
                {
                    // use UseTempDB when CREATE TABLE permission denied
                    var bulkConfig = new BulkConfig { UseTempDB = true };

                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        List<SysLabelReport> oldData = db.Set<SysLabelReport>().ToList();
                        db.BulkDelete(oldData);
                        if(list.Count > 0) db.BulkInsert(list);

                        List<SysLabelReport_CUSTOM> oldDataCustom = db.Set<SysLabelReport_CUSTOM>().ToList();
                        db.BulkDelete(oldDataCustom);
                        if (listCustom.Count > 0) db.BulkInsert(listCustom);

                        UnitOfWork.Commit(transaction);
                    }
                }
                
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
    }
}
