﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.Repositories;
using Innoviz.SmartApp.Data.ViewModelHandler;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.ViewModels;
using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Innoviz.SmartApp.Core;
using Microsoft.EntityFrameworkCore;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using Innoviz.SmartApp.Data.ServicesV2;
using IdentityModel.Client;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewMaps;

namespace Innoviz.SmartApp.Data.ServicesV2 
{
    public interface ISysUserService {

        SysUserTable CreateFirstSysUserTable(string userId, string username);

        SysUserTable CreateUserWithEmpRoleCompanyMapping(SysUserTable user,
            IEnumerable<EmployeeTable> empList,
            IEnumerable<SysUserCompanyMapping> list,
            IEnumerable<SysUserRoles> userRoles,
            string userId);

        SysUserTable UpdateUserWithEmpRoleCompanyMapping(SysUserTable user,
            IEnumerable<EmployeeTable> empList,
            IEnumerable<SysUserCompanyMapping> list,
            IEnumerable<SysUserRoles> userRoles,
            int site);

        SysUserTable UpdateInActiveUser(SysUserTableActivationParm model);

        SysUserSettingsItemView UpdateUserSettings(SysUserSettingsItemView model);
        
        bool ValidateUserEmployeeMapping(UserEmployeeMappingView model);
        IEnumerable<UserEmployeeMappingView> ValidateUserEmployeeMappings(IEnumerable<UserEmployeeMappingView> model);

        bool IsInactiveUser(string userId);
        bool HasAnyUser();

        bool CheckExistingUser(SysUserTableParm parm);
        //Task<bool> InitializeServiceUserNames(string[] usernames);
        SysUserTable GetSysUserTableByUserNameNoTracking(string username);
        Task<IEnumerable<SelectItem<AdUserView>>> GetAdUserDropDown(string userApiEndpoint, string token, string requestId);
        SysUserTableItemView GetSysUserTableById(string id);
        SysUserTableItemView GetSysUserTableByIdWithEmpRolesBranchCompany(string id);
        SysUserTableItemView CreateSysUserTable(SysUserTableItemView sysUserTableView);
        SysUserTableItemView UpdateSysUserTable(SysUserTableItemView sysUserTableView);
        bool DeleteSysUserTable(string item);
        Task<SysUserTableItemView> CreateSysUserTable(SysUserTableItemView model, string token, string requestId);
        SysUserTableItemView CreateSysUserTable(SysUserTableItemView model, string authUserId);
        SysUserTableItemView UpdateSysUserTable(SysUserTableItemView model, int site);

        //IEnumerable<SysUserCompanyMappingView> GetUserSettingMappedBranchCompanyByUserName(string username);
        SysUserSettingsItemView GetUserSettingsById(string userId);
        IEnumerable<SelectItem<SysUserTableItemView>> GetDropDownItemUserNameValue(SearchParameter search);
        Task<SearchResult<SysUserLogView>> GetSysUserLogs(string userApiEndpoint, string token, string requestId, SearchParameter search);
        Task<bool> CleanUpUserLog(string userApiEndpoint, string token, string requestId, SysUserLogCleanUpParm model);
    }
    public class SysUserService : SmartAppService, ISysUserService
    {
        public SysUserService(SmartAppDbContext context) : base(context) { }
        public SysUserService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public SysUserService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public SysUserService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public SysUserService() : base() { }

        public SysUserTable CreateFirstSysUserTable(string userId, string username)
        {
            try
            {
                ISysUserTableRepo userRepo = new SysUserTableRepo(db);
                SysUserTable user = new SysUserTable();
                user.UserName = username;
                user.Name = username;
                userRepo.CreateSysUserTable(user, userId);
                UnitOfWork.Commit();
                return user;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public SysUserTable CreateUserWithEmpRoleCompanyMapping(SysUserTable user,
                                                                IEnumerable<EmployeeTable> empList,
                                                                IEnumerable<SysUserCompanyMapping> list,
                                                                IEnumerable<SysUserRoles> userRoles,
                                                                string userId)
        {
            try
            {
                if(db != null)
                {
                    ISysUserTableRepo userRepo = new SysUserTableRepo(db);
                    ISysUserCompanyMappingRepo companyMappingRepo = new SysUserCompanyMappingRepo(db);
                    ISysUserRolesRepo userRolesRepo = new SysUserRolesRepo(db);
                    IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);

                    userRepo.CreateSysUserTable(user, userId);
                    companyMappingRepo.CreateSysUserCompanyMapping(list, userId);
                    userRolesRepo.CreateSysUserRoles(userRoles, userId);

                    employeeTableRepo.UpdateUserMapping(empList, userId);
                    
                    UnitOfWork.Commit();
                }
                user.SysUserCompanyMapping = list.ToList();
                return user;
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SysUserTable UpdateUserWithEmpRoleCompanyMapping(SysUserTable user,
                                                            IEnumerable<EmployeeTable> empList,
                                                            IEnumerable<SysUserCompanyMapping> list,
                                                            IEnumerable<SysUserRoles> userRoles,
                                                            int site)
        {
            try
            {
                if(db != null)
                {
                    ISysUserTableRepo userRepo = new SysUserTableRepo(db);
                    ISysUserCompanyMappingRepo companyMappingRepo = new SysUserCompanyMappingRepo(db);
                    ISysUserRolesRepo userRolesRepo = new SysUserRolesRepo(db);
                    IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);

                    companyMappingRepo.UpdateSysUserCompanyMapping(list, user.Id.GuidNullToString());
                    userRolesRepo.UpdateSysUserRoles(userRoles, user.Id.GuidNullToString(), site);
                    userRepo.UpdateBySysUserTableView(user);

                    employeeTableRepo.UpdateUserMapping(empList, user.Id.GuidNullToString());

                    UnitOfWork.Commit();
                }
                user.SysUserCompanyMapping = list.ToList();
                return user;
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        
        public SysUserTable UpdateInActiveUser(SysUserTableActivationParm model)
        {
            try
            {
                ISysUserTableRepo userRepo = new SysUserTableRepo(db);

                SysUserTable user = new SysUserTable();
                user.Id = new Guid(model.SysUserTableGUID);
                user.InActive = model.InActive;

                userRepo.UpdateInActiveUser(user);
                UnitOfWork.Commit();
                return user;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SysUserSettingsItemView UpdateUserSettings(SysUserSettingsItemView item)
        {
            try
            {
                ValidateCaseAccessUserSettings(item.Id);
                SysUserTable user = item.ToSysUserTable();
                ISysUserTableRepo userRepo = new SysUserTableRepo(db);
                user = userRepo.UpsdateBySysUserSettingsView(user);
                UnitOfWork.Commit();

                return user.ToSysUserSettingsView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        
        public bool ValidateUserEmployeeMapping(UserEmployeeMappingView model)
        {
            try
            {
                Guid employeeGuid = new Guid(model.EmployeeTableGUID);
                if(model.UserId == null)
                {
                    var result = db.Set<EmployeeTable>()
                                .Where(item => item.EmployeeTableGUID == employeeGuid)
                                .FirstOrDefault()
                                .UserId == null;
                    return result;
                }
                else
                {
                    var emp = db.Set<EmployeeTable>()
                                .Where(item => item.EmployeeTableGUID == employeeGuid)
                                .FirstOrDefault();
                    Guid? userId = new Guid(model.UserId);
                    bool result = (emp.UserId == null || emp.UserId == userId);

                    return result;
                }
                        
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<UserEmployeeMappingView> ValidateUserEmployeeMappings(IEnumerable<UserEmployeeMappingView> inputList)
        {
            try
            {
                List<UserEmployeeMappingView> result = new List<UserEmployeeMappingView>();
                if(inputList != null && inputList.Count() > 0)
                {
                    foreach (var item in inputList)
                    {
                        if(!ValidateUserEmployeeMapping(item))
                        {
                            result.Add(item);
                        }
                    }
                    return result;
                }
                return null;
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool IsInactiveUser(string userId)
        {
            try
            {
                ISysUserTableRepo userRepo = new SysUserTableRepo(db);
                var user = userRepo.GetSysUserTableByIdNoTracking(userId.StringToGuid());
                if(user != null)
                {
                    return user.InActive;
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.ERROR");
                    ex.AddData("Cannot find user.");
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool HasAnyUser()
        {
            try
            {
                ISysUserTableRepo userRepo = new SysUserTableRepo(db);
                return userRepo.HasAnyUser();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public bool CheckExistingUser(SysUserTableParm parm)
        {
            try
            {
                ISysUserTableRepo userTableRepo = new SysUserTableRepo(db);
                #region LIT
                //// check for case no existing user
                //if (!userTableRepo.HasAnyUser())
                //{
                //    return true;
                //}
                //else
                //{
                return userTableRepo.HasAnyUser() && userTableRepo.Exist(item => item.UserName == parm.UserName);
                ////}
                ///
                #endregion LIT
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        //public async Task<bool> InitializeServiceUserNames(string[] usernames)
        //{
        //    try
        //    {
        //        ICompanyRepo companyRepo = new CompanyRepo(db);
        //        IBranchRepo branchRepo = new BranchRepo(db);
        //        ISysUserTableRepo userRepo = new SysUserTableRepo(db);
        //        ISysRoleTableRepo roleRepo = new SysRoleTableRepo(db);
        //        if (!companyRepo.HasAnyCompany())
        //        {
        //            throw new Exception("Must have at least 1 Company.");
        //        }
        //        else
        //        {
        //            var userParam = usernames.Select(item => new SysUserTableView { UserName = item }).ToList();
        //            List<UserEmployeeMappingView> employeesToCreate = new List<UserEmployeeMappingView>();
        //            List<SysUserCompanyMappingView> companyMappingToCreate = new List<SysUserCompanyMappingView>();
        //            List<SysUserRolesView> userRoleToCreate = new List<SysUserRolesView>();
        
        //            List<Guid> companies = companyRepo.GetListCompanyNoTracking().Select(s => s.CompanyGUID).ToList();
        //            List<Branch> branches = branchRepo.GetFirstBranchOfCompanyNoTracking(companies);
        //            List<SysRoleTable> adminRoles = roleRepo.GetAdminRoleByCompanies(companies);
        //            var existingUsers = userRepo.GetSysUserTableByUserNameNoTracking(usernames.ToList());
        //            var newUsers = userParam.Where(w => !existingUsers.Any(existing => w.UserName == existing.UserName));
        //            #region prepare employees to create
        //            IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
        //            IEmployeeTableService employeeTableService = new EmployeeTableService(db);

        //            var mappedEmployees = employeeTableRepo.GetMappedEmployeesByUserNoTracking(existingUsers.ToList());
        //            var users = newUsers.Concat(existingUsers.ToSysUserTableView());
        //            if (mappedEmployees != null && mappedEmployees.Count() > 0)
        //            {
        //                var employeeCheck = mappedEmployees.GroupBy(g => g.UserId)
        //                                        .Select(s => new
        //                                        {
        //                                            UserId = s.Key.GuidNullToString(),
        //                                            Count = s.Count(),
        //                                            EmployeeId = s.FirstOrDefault().EmployeeId,
        //                                            Companies = s.Select(ss => ss.CompanyGUID)
        //                                        });
        //                var employees =
        //                        (from user in users
        //                         join empl in employeeCheck
        //                         on user.Id equals empl.UserId into lj

        //                         from empl in lj.DefaultIfEmpty()
        //                         select new
        //                         {
        //                             UserId = user.Id,
        //                             UserName = user.UserName,
        //                             EmployeeId = (empl != null) ? (empl.Count < companies.Count() ? empl.EmployeeId : null) : user.UserName,
        //                             Count = (empl != null) ? companies.Count() - empl.Count : companies.Count(),
        //                             Companies = (empl != null) ? companies.Where(w => !empl.Companies.Contains(w)) : companies
        //                         });
        //                employeesToCreate =
        //                    (from empl in employees.Where(w => w.EmployeeId != null)
        //                     select new
        //                     {
        //                         Employee =
        //                         (from company in empl.Companies
        //                          select new UserEmployeeMappingView
        //                          {
        //                              UserId = empl.UserId,
        //                              UserName = empl.UserName,
        //                              EmployeeId = empl.EmployeeId,
        //                              CompanyGUID = company.GuidNullToString()
        //                          })
        //                     })
        //                     .SelectMany(ss => ss.Employee)
        //                     .ToList();
        //            }
        //            else
        //            {
        //                employeesToCreate = (from user in users
        //                                     from company in companies
        //                                     select new UserEmployeeMappingView
        //                                     {
        //                                         EmployeeId = user.UserName,
        //                                         UserName = user.UserName,
        //                                         CompanyGUID = company.GuidNullToString(),
        //                                         UserId = user.Id
        //                                     })
        //                                     .ToList();
        //            }
        //            #endregion
        //            #region prepare company mapping to create
        //            ISysUserCompanyMappingRepo companyMappingRepo = new SysUserCompanyMappingRepo(db);

        //            companyMappingToCreate =
        //                (from user in newUsers
        //                 from branch in branches
        //                 select new SysUserCompanyMappingView
        //                 {
        //                     SysUserTable_UserName = user.UserName,
        //                     BranchGUID = branch.BranchGUID.GuidNullToString(),
        //                     CompanyGUID = branch.CompanyGUID.GuidNullToString()
        //                 }).ToList();
        //            #endregion
        //            #region prepare role mapping to create
        //            ISysUserRolesRepo userRolesRepo = new SysUserRolesRepo(db);

        //            userRoleToCreate = 
        //                (from user in newUsers
        //                 from role in adminRoles
        //                 select new SysUserRolesView
        //                 {
        //                     SysUserTable_UserName = user.UserName,
        //                     SysRoleTable_SiteLoginType = 1 , //back
        //                     RoleId = role.Id.GuidNullToString(),
        //                     SysRoleTable_CompanyGUID = role.CompanyGUID.GuidNullToString(),
                             
        //                 }).ToList();
        //            #endregion
        //            userParam = userParam.Select(s =>
        //            {
        //                s.Id = employeesToCreate.Where(empl => empl.UserName == s.UserName).FirstOrDefault().UserId;
        //                s.EmployeeMappingList = employeesToCreate.Where(empl => empl.UserName == s.UserName).ToList();
        //                s.SysUserCompanyMappingList = companyMappingToCreate.Where(mapping => mapping.SysUserTable_UserName == s.UserName).ToList();
        //                s.SysUserRolesViewList = userRoleToCreate.Where(userrole => userrole.SysUserTable_UserName == s.UserName).ToList();
        //                return s;
        //            }).ToList();
        
        //            await CreateMultipleSysUserTables(userParam);
        //            return true;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        throw SmartAppUtil.AddStackTrace(e);
        //    }
        //}
        //public async Task<List<SysUserTableView>> CreateMultipleSysUserTables(List<SysUserTableView> newUsers)
        //{
        //    try
        //    {
        //        ICompanyRepo companyRepo = new CompanyRepo(db);
        //        ISysUserTableRepo userRepo = new SysUserTableRepo(db);
        //        HttpHelper helper = new HttpHelper(GetSystemParameter());
        //        if (companyRepo.HasAnyCompany() && newUsers != null && newUsers.Count() > 0)
        //        {
        //            List<SysUserTable> usersToCreate = new List<SysUserTable>();
        //            List<EmployeeTable> employeesToCreate = new List<EmployeeTable>();
        //            string token = GetBearerToken() == null ? await helper.GetClientCredentialsToken("JWT:Authority".GetConfigurationValue(),
        //                                                                    "BatchApi:Client".GetConfigurationValue(),
        //                                                                    "BatchApi:ClientSecret".GetConfigurationValue(),
        //                                                                    "UserApi:Resource".GetConfigurationValue()) : 
        //                                                      GetBearerToken();

                    
        //            if(newUsers != null && newUsers.Count() > 0)
        //            {
        //                foreach (var newUser in newUsers)
        //                {
        //                    SysUserTable user = newUser.ToSysUserTable();

        //                    var fromAuth = await GetOrCreateIdentityUser(user.ToSysUserTableView(), token, GetRequestId());
        //                    user.Id = fromAuth.Id.StringToGuid();

        //                    usersToCreate.Add(user);
        //                }
        //            }


        //        }
        //        return null;

        //    }
        //    catch (Exception e)
        //    {
        //        throw SmartAppUtil.AddStackTrace(e);
        //    }
        //}
        
        public SysUserTable GetSysUserTableByUserNameNoTracking(string username)
        {
            try
            {
                ISysUserTableRepo userRepo = new SysUserTableRepo(db);
                return userRepo.GetSysUserTableByUserNameNoTracking(username);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region AdUser
        public async Task<IEnumerable<SelectItem<AdUserView>>> GetAdUserDropDown(string userApiEndpoint, string token, string requestId)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var url = userApiEndpoint + "/GetAdUsersForDropDown";

                    if (!string.IsNullOrWhiteSpace(token))
                    {
                        httpClient.SetBearerToken(token);
                    }
                    if (requestId != null)
                    {
                        httpClient.DefaultRequestHeaders.Add("RequestHeader", requestId);
                    }

                    var response = await httpClient.GetAsync(url);
                    dynamic content = await response.Content.ReadAsAsync<dynamic>();
                    if (!response.IsSuccessStatusCode)
                    {
                        int responseStatusCode = (int)response.StatusCode;
                        if (responseStatusCode == 400 || responseStatusCode == 500)
                        {
                            // failed 
                            var errResp = ((JObject)content).ToObject<WebApiErrorResponseMessage>();
                            SmartAppException ex = new SmartAppException("ERROR.ERROR", errResp);
                            throw ex;
                        }
                        else
                        {
                            // status != 400 or 500
                            string errorMsg = "Request responded with Status: " + responseStatusCode + "(" + response.ReasonPhrase + ").";
                            SmartAppException ex = new SmartAppException(errorMsg);
                            throw ex;
                        }
                    }
                    else
                    {
                        // success
                        string jsonContent = JsonConvert.SerializeObject(content);
                        IEnumerable<AdUserView> result =
                            JsonConvert.DeserializeObject<IEnumerable<AdUserView>>(jsonContent);

                        ISysUserTableRepo userRepo = new SysUserTableRepo(db);
                        return userRepo.GetDropDownAdUserForCreateUser(result);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AdUser
        #region V2
        public SysUserTableItemView GetSysUserTableById(string id)
        {
            try
            {
                ISysUserTableRepo sysUserTableRepo = new SysUserTableRepo(db);
                return sysUserTableRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SysUserTableItemView GetSysUserTableByIdWithEmpRolesBranchCompany(string id)
        {
            try
            {
                ISysUserTableRepo userRepo = new SysUserTableRepo(db);
                SysUserTableItemView item = userRepo.GetByIdvw(id.StringToGuid());

                ISysUserCompanyMappingRepo usercompanyMappingRepo = new SysUserCompanyMappingRepo(db);
                item.SysUserCompanyMappingList = usercompanyMappingRepo.GetListByUserNamevw(item.UserName).ToList();

                ISysUserRolesRepo userRolesRepo = new SysUserRolesRepo(db);
                item.SysUserRolesViewList = userRolesRepo.GetUserRolesViewByUserId(id, GetSiteLogin());

                item.EmployeeMappingList = GetMappedEmployeeTableItemViewByUserId(id);

                return item;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SysUserTableItemView CreateSysUserTable(SysUserTableItemView sysUserTableView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                sysUserTableView = accessLevelService.AssignOwnerBU(sysUserTableView);
                ISysUserTableRepo sysUserTableRepo = new SysUserTableRepo(db);
                SysUserTable sysUserTable = sysUserTableView.ToSysUserTable();
                sysUserTable = sysUserTableRepo.CreateSysUserTable(sysUserTable);
                base.LogTransactionCreate<SysUserTable>(sysUserTable);
                UnitOfWork.Commit();
                return sysUserTable.ToSysUserTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SysUserTableItemView UpdateSysUserTable(SysUserTableItemView sysUserTableView)
        {
            try
            {
                ISysUserTableRepo sysUserTableRepo = new SysUserTableRepo(db);
                SysUserTable inputSysUserTable = sysUserTableView.ToSysUserTable();
                SysUserTable dbSysUserTable = sysUserTableRepo.Find(inputSysUserTable.Id);
                dbSysUserTable = sysUserTableRepo.UpdateSysUserTable(dbSysUserTable, inputSysUserTable, new List<string>() { "UserImage" });
                base.LogTransactionUpdate<SysUserTable>(GetOriginalValues<SysUserTable>(dbSysUserTable), dbSysUserTable);
                UnitOfWork.Commit();
                return dbSysUserTable.ToSysUserTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteSysUserTable(string item)
        {
            try
            {
                ISysUserTableRepo sysUserTableRepo = new SysUserTableRepo(db);
                Guid sysUserTableGUID = new Guid(item);
                SysUserTable sysUserTable = sysUserTableRepo.Find(sysUserTableGUID);
                sysUserTableRepo.Remove(sysUserTable);
                base.LogTransactionDelete<SysUserTable>(sysUserTable);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<EmployeeTableItemView> GetMappedEmployeeTableItemViewByUserId(string id)
        {
            try
            {
                IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
                var mappedEmployees = employeeTableRepo.GetMappedEmployeesByUserNoTracking(new List<Guid>() { id.StringToGuid() })
                                                        .Select(s => new EmployeeTableItemView
                                                        {
                                                            EmployeeTableGUID = s.EmployeeTableGUID.GuidNullToString(),
                                                            CompanyGUID = s.CompanyGUID.GuidNullToString(),
                                                            UserId = s.UserId.GuidNullToString(),
                                                            EmployeeId = s.EmployeeId,
                                                            Name = s.Name
                                                        });
                return mappedEmployees;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public async Task<SysUserTableItemView> CreateSysUserTable(SysUserTableItemView model, string token, string requestId)
        {
            try
            {
                // prepare sysusertable
                SysUserTable user = PrepareSysUserTableForCreate(model);

                // call auth to check if username exists, if not, create new one
                SysUserTableItemView fromAuth = await GetOrCreateIdentityUser(model, token, requestId);
                if (fromAuth == null)
                {
                    SmartAppException ex = new SmartAppException("ERROR.00063");
                    ex.AddData("Failed creating Auth user '{{0}}'. Please contact system administrator.", model.UserName);
                    throw ex;
                }
                var result = CreateSysUserTable(model, fromAuth.Id);
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SysUserTableItemView CreateSysUserTable(SysUserTableItemView model, string authUserId)
        {
            try
            {
                // prepare sysusertable
                SysUserTable user = PrepareSysUserTableForCreate(model);
                IEnumerable<SysUserCompanyMapping> list = PrepareSysUserCompanyMappingForCreate(model);

                // prepare sysuserroles
                IEnumerable<SysUserRoles> userRoles = PrepareSysUserRolesForCreate(model);

                // prepare employee mapping
                IEnumerable<EmployeeTable> employeeMapping = PrepareEmployeeMappingForCreate(model);

                var result = CreateUserWithEmpRoleCompanyMapping(user, employeeMapping,
                                                    list, userRoles, authUserId);
                return result.ToSysUserTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SysUserTableItemView UpdateSysUserTable(SysUserTableItemView model, int site)
        {
            try
            {
                // prepare sysusertable
                SysUserTable user = PrepareSysUserTableForUpdate(model);
                IEnumerable<SysUserCompanyMapping> list = PrepareSysUserCompanyMappingForUpdate(model);

                // prepare sysuserroles
                IEnumerable<SysUserRoles> userRoles = PrepareSysUserRolesForUpdate(model);

                //prepare employee mappings
                IEnumerable<EmployeeTable> employeeMapping = PrepareEmployeeMappingForUpdate(model);

                var result = UpdateUserWithEmpRoleCompanyMapping(user, employeeMapping,
                                                                        list, userRoles, site);
                return result.ToSysUserTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SysUserTable PrepareSysUserTableForCreate(SysUserTableItemView model)
        {
            try
            {

                SysUserTable user = model.ToSysUserTable();
                return user;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SysUserTable PrepareSysUserTableForUpdate(SysUserTableItemView model)
        {
            try
            {

                SysUserTable user = model.ToSysUserTable();
                return user;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SysUserRoles> PrepareSysUserRolesForCreate(SysUserTableItemView model)
        {
            try
            {
                var userRolesList = model.SysUserRolesViewList;
                IEnumerable<SysUserRoles> sysUserRolesList = userRolesList.ToSysUserRoles();
                return sysUserRolesList;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SysUserRoles> PrepareSysUserRolesForUpdate(SysUserTableItemView model)
        {
            try
            {
                var userRolesList = model.SysUserRolesViewList;
                IEnumerable<SysUserRoles> sysUserRolesList = userRolesList.ToSysUserRoles();
                return sysUserRolesList;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SysUserCompanyMapping> PrepareSysUserCompanyMappingForCreate(SysUserTableItemView model)
        {
            try
            {
                var companyMappingList = model.SysUserCompanyMappingList.ToList();

                IEnumerable<SysUserCompanyMapping> usercompanymappinglist =
                    companyMappingList.ToSysUserCompanyMapping();

                return usercompanymappinglist;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SysUserCompanyMapping> PrepareSysUserCompanyMappingForUpdate(SysUserTableItemView model)
        {
            try
            {

                var companyMappingList = model.SysUserCompanyMappingList.ToList();
                IEnumerable<SysUserCompanyMapping> usercompanymappinglist =
                    companyMappingList.ToSysUserCompanyMapping();

                return usercompanymappinglist;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<EmployeeTable> PrepareEmployeeMappingForCreate(SysUserTableItemView model)
        {
            try
            {
                if (model.EmployeeMappingList != null &&
                    model.EmployeeMappingList.Count() != 0)
                {
                    IEnumerable<EmployeeTable> result = model.EmployeeMappingList.ToEmployeeTable();
                    return result;
                }
                return null;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<EmployeeTable> PrepareEmployeeMappingForUpdate(SysUserTableItemView model)
        {
            try
            {
                if (model.EmployeeMappingList != null &&
                    model.EmployeeMappingList.Count() != 0)
                {
                    IEnumerable<EmployeeTable> result = model.EmployeeMappingList.ToEmployeeTable();
                    return result;
                }
                return null;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private async Task<SysUserTableItemView> GetOrCreateIdentityUser(SysUserTableItemView model, string token, string requestId)
        {
            try
            {
                string baseUrl = "UserApi:BaseUrl".GetConfigurationValue();
                string userApi = "UserApi:User".GetConfigurationValue();
                using (var httpClient = new HttpClient())
                {
                    var url = baseUrl + userApi + "/GetOrCreateIdentityUser";

                    if (!string.IsNullOrWhiteSpace(token))
                    {
                        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    }
                    if (requestId != null)
                    {
                        httpClient.DefaultRequestHeaders.Add("RequestHeader", requestId);
                    }

                    var jsonObject = JsonConvert.SerializeObject(model, new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    });

                    var response = await httpClient.PostAsync(url, new StringContent(jsonObject,
                                                                Encoding.UTF8, "application/json"));

                    dynamic content = await response.Content.ReadAsAsync<dynamic>();
                    if (!response.IsSuccessStatusCode)
                    {
                        int responseStatusCode = (int)response.StatusCode;
                        if (responseStatusCode == 400 || responseStatusCode == 500)
                        {
                            // failed 
                            var errResp = ((JObject)content).ToObject<WebApiErrorResponseMessage>();
                            SmartAppException ex = new SmartAppException("ERROR.00063", errResp);
                            throw ex;
                        }
                        else
                        {
                            // status != 400 or 500
                            string errorMsg = "Request responded with Status: " + responseStatusCode + "(" + response.ReasonPhrase + ").";
                            SmartAppException ex = new SmartAppException(errorMsg);
                            ex.AddData("ERROR.00063");
                            throw ex;
                        }
                    }
                    else
                    {
                        // success
                        string jsonContent = JsonConvert.SerializeObject(content);
                        SysUserTableItemView result =
                            JsonConvert.DeserializeObject<SysUserTableItemView>(jsonContent);
                        return result;
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        //public IEnumerable<SysUserCompanyMappingView> GetUserSettingMappedBranchCompanyByUserName(string username)
        //{
        //    try
        //    {
        //        ValidateCaseAccessUserSettings(username);
        //        ISysUserCompanyMappingRepo ucmRepo = new SysUserCompanyMappingRepo(db);
        //        return ucmRepo.GetListByUserNamevw(username).ToList();
        //    }
        //    catch (Exception e)
        //    {
        //        throw SmartAppUtil.AddStackTrace(e);
        //    }
        //}
        private bool ValidateCaseAccessUserSettings(string inputUserId)
        {
            try
            {
                string currentUser = GetUserId();
                if (currentUser != inputUserId)
                {
                    SmartAppException ex = new SmartAppException("ERROR.ERROR");
                    ex.AddData("Invalid operation: Current UserName does not match requested UserName.");
                    throw ex;
                }
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SysUserSettingsItemView GetUserSettingsById(string userId)
        {
            try
            {
                ValidateCaseAccessUserSettings(userId);
                ISysUserTableRepo userRepo = new SysUserTableRepo(db);
                SysUserSettingsItemView result = userRepo.GetUserSettingsById(userId);

                ISysUserCompanyMappingRepo userCompanyMappingRepo = new SysUserCompanyMappingRepo(db);
                string username = result.UserName;
                result.SysUserCompanyMappings = userCompanyMappingRepo.GetListByUserNamevw(username).ToList();

                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<SysUserTableItemView>> GetDropDownItemUserNameValue(SearchParameter search)
        {
            try
            {
                ISysUserTableRepo sysUserTableRepo = new SysUserTableRepo(db);
                return sysUserTableRepo.GetDropDownItemUserNameValue(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region SysUserLog
        public async Task<SearchResult<SysUserLogView>> GetSysUserLogs(string userApiEndpoint, string token, string requestId, SearchParameter search)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {

                    string url = userApiEndpoint + "/GetUserLogs";


                    if (token != null && token != "")
                    {
                        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    }
                    if (requestId != null)
                    {
                        httpClient.DefaultRequestHeaders.Add("RequestHeader", requestId);
                    }

                    var jsonObject = JsonConvert.SerializeObject(search, new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    });

                    var response = await httpClient.PostAsync(url, new StringContent(jsonObject,
                                                                Encoding.UTF8, "application/json"));


                    dynamic content = await response.Content.ReadAsAsync<dynamic>();
                    if (!response.IsSuccessStatusCode)
                    {
                        int responseStatusCode = (int)response.StatusCode;
                        if (responseStatusCode == 400 || responseStatusCode == 500)
                        {
                            // failed 
                            var errResp = ((JObject)content).ToObject<WebApiErrorResponseMessage>();
                            SmartAppException ex = new SmartAppException("Error getting user logs.", errResp);
                            throw ex;
                        }
                        else
                        {
                            // status != 400 or 500
                            string errorMsg = "Request responded with Status: " + responseStatusCode + "(" + response.ReasonPhrase + ").";
                            SmartAppException ex = new SmartAppException("Error getting user logs.: \n" + errorMsg);
                            throw ex;
                        }
                    }
                    else
                    {
                        // success
                        string jsonContent = JsonConvert.SerializeObject(content);
                        SearchResult<SysUserLogView> result =
                            JsonConvert.DeserializeObject<SearchResult<SysUserLogView>>(jsonContent);
                        return result;
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public async Task<bool> CleanUpUserLog(string userApiEndpoint, string token, string requestId, SysUserLogCleanUpParm model)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {

                    string url = userApiEndpoint + "/CleanUpUserLog";

                    if (token != null && token != "")
                    {
                        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    }
                    if (requestId != null)
                    {
                        httpClient.DefaultRequestHeaders.Add("RequestHeader", requestId);
                    }
                    var jsonObject = JsonConvert.SerializeObject(model, new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    });

                    var response = await httpClient.PostAsync(url, new StringContent(jsonObject,
                                                                Encoding.UTF8, "application/json"));

                    dynamic content = await response.Content.ReadAsAsync<dynamic>();
                    if (!response.IsSuccessStatusCode)
                    {
                        int responseStatusCode = (int)response.StatusCode;
                        if (responseStatusCode == 400 || responseStatusCode == 500)
                        {
                            // failed 
                            var errResp = ((JObject)content).ToObject<WebApiErrorResponseMessage>();
                            SmartAppException ex = new SmartAppException("Error cleaning up user logs.", errResp);
                            throw ex;
                        }
                        else
                        {
                            // status != 400 or 500
                            string errorMsg = "Request responded with Status: " + responseStatusCode + "(" + response.ReasonPhrase + ").";
                            SmartAppException ex = new SmartAppException("Error cleaning up user logs.: \n" + errorMsg);
                            throw ex;
                        }
                    }
                    else
                    {
                        // success
                        string jsonContent = JsonConvert.SerializeObject(content);
                        bool result =
                            JsonConvert.DeserializeObject<bool>(jsonContent);
                        return result;
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }
}
