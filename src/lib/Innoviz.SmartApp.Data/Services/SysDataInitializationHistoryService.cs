﻿using EFCore.BulkExtensions;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.Services
{
    public interface ISysDataInitializationHistoryService
    {
        bool CheckVersionExists(SysDataInitType initType, string version);
        bool CheckVersionExists(SysDataInitializationHistory version);
        void AddNewVersionHistories(List<SysDataInitializationHistory> newVersions);
    }
    public class SysDataInitializationHistoryService: SmartAppService, ISysDataInitializationHistoryService
    {
        public SysDataInitializationHistoryService(SmartAppDbContext context) : base(context) { }
        public SysDataInitializationHistoryService() : base() { }

        public bool CheckVersionExists(SysDataInitializationHistory version)
        {
            try
            {
                ISysDataInitializationHistoryRepo sysDataInitRepo = new SysDataInitializationHistoryRepo(db);
                return sysDataInitRepo.CheckVersionExists(version);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool CheckVersionExists(SysDataInitType initType, string version)
        {
            try
            {
                return CheckVersionExists(new SysDataInitializationHistory { DataInitializationType = (int)initType, Version = version });
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void AddNewVersionHistories(List<SysDataInitializationHistory> newVersions)
        {
            try
            {
                if(newVersions != null && newVersions.Count > 0)
                {
                    // use UseTempDB when CREATE TABLE permission denied
                    var bulkConfig = new BulkConfig { UseTempDB = true };

                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        db.BulkInsert(newVersions);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
