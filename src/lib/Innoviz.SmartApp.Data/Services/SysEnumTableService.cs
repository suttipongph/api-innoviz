﻿using EFCore.BulkExtensions;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;

namespace Innoviz.SmartApp.Data.Services
{
    public interface ISysEnumTableService
    {
        bool InitializeSysEnumTable(List<SysEnumTable> list);
        bool InitializeSysEnumTable(string jsonContent, string jsonContent_CUSTOM, string username);
    }

    public class SysEnumTableService : SmartAppService, ISysEnumTableService
    {
        public SysEnumTableService(SmartAppDbContext context) : base(context)
        {

        }
        public SysEnumTableService() { }
        public bool InitializeSysEnumTable(string jsonContent, string jsonContent_CUSTOM, string username)
        {
            try
            {
                SmartAppUtil.LogMessage(Serilog.Events.LogEventLevel.Information, "Initializing SysEnumTable...");
                List<SysEnumTable> sysEnumTables = JsonConvert.DeserializeObject<List<SysEnumTable>>(jsonContent);
                List<SysEnumTable> sysEnumTables_CUSTOM = JsonConvert.DeserializeObject<List<SysEnumTable>>(jsonContent_CUSTOM);

                sysEnumTables = sysEnumTables.Select(s =>
                {
                    s.AttributeName = "0" + s.AttributeName;
                    s.AttributeDescription = "0" + s.AttributeDescription;
                    return s;
                })
                .ToList();
                sysEnumTables_CUSTOM = sysEnumTables_CUSTOM.Select(s =>
                {
                    s.AttributeName = "1" + s.AttributeName;
                    s.AttributeDescription = "1" + s.AttributeDescription;
                    return s;
                })
                .ToList();
                sysEnumTables = sysEnumTables
                        .Concat(sysEnumTables_CUSTOM)
                        .ToList().GroupBy(g => new { g.EnumName, g.AttributeValue })
                        .Select(g => new SysEnumTable
                        {
                            EnumName = g.Key.EnumName,
                            AttributeValue = g.Key.AttributeValue,
                            AttributeName = g.Max(p => p.AttributeName),
                            AttributeDescription = g.Max(p => p.AttributeDescription)
                        }).ToList();
                sysEnumTables = sysEnumTables.Select(s =>
                {
                    s.AttributeName = s.AttributeName.Substring(1);
                    s.AttributeDescription = s.AttributeDescription.Substring(1);
                    s.CreatedBy = username;
                    s.CreatedDateTime = DateTime.Now;
                    s.ModifiedBy = username;
                    s.ModifiedDateTime = DateTime.Now;
                    return s;
                })
                .ToList();

                return InitializeSysEnumTable(sysEnumTables);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool InitializeSysEnumTable(List<SysEnumTable> list)
        {
            try
            {
                // use UseTempDB when CREATE TABLE permission denied
                var bulkConfig = new BulkConfig { UseTempDB = true };

                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    List<SysEnumTable> oldData = db.Set<SysEnumTable>().ToList();
                    db.BulkDelete(oldData);
                    db.BulkInsert(list);
                    UnitOfWork.Commit(transaction);
                }
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
