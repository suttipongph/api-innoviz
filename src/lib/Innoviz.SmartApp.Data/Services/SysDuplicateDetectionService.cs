﻿using EFCore.BulkExtensions;
using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;

namespace Innoviz.SmartApp.Data.Services
{
    public interface ISysDuplicateDetectionService
    {
        bool InitializeSysDuplicateDetection(string jsonContent, string username);
        List<SysDuplicateDetection> GetSysDuplicateDetectionNoTracking();
    }
    public class SysDuplicateDetectionService : SmartAppService, ISysDuplicateDetectionService
    {
        public SysDuplicateDetectionService(SmartAppDbContext context) : base(context) { }
        public SysDuplicateDetectionService() : base() { }

        public bool InitializeSysDuplicateDetection(string jsonContent, string username)
        {
            try
            {
                SmartAppUtil.LogMessage(Serilog.Events.LogEventLevel.Information, "Initializing SysDuplicateDetection...");

                List<SysDuplicateDetection> duplicateDetectionList = JsonConvert.DeserializeObject<List<SysDuplicateDetection>>(jsonContent);

                // validate
                var groupDuplicate = duplicateDetectionList.GroupBy(g => new { g.ModelName, g.PropertyName, g.LabelName, g.RuleNum })
                                                            .Select(s => new { s.Key.ModelName, s.Key.PropertyName, s.Key.LabelName, s.Key.RuleNum, Group = s });
                if (groupDuplicate.Any(a => a.Group.Count() > 1))
                {
                    var groups = groupDuplicate.Where(w => w.Group.Count() > 1).Select(s => JObject.FromObject(new { s.ModelName, s.PropertyName, s.LabelName, s.RuleNum }));
                    SmartAppException ex = new SmartAppException("Error startup SysDuplicate");
                    ex.AddData("Duplicate object: \n" + string.Join(",\n", groups.Select(s => s.ToString())));
                    throw ex;
                }

                duplicateDetectionList = duplicateDetectionList.Select(s =>
                {
                    s.CreatedDateTime = DateTime.Now;
                    s.ModifiedDateTime = DateTime.Now;
                    s.CreatedBy = username;
                    s.ModifiedBy = username;
                    return s;
                }).ToList();

                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    List<SysDuplicateDetection> oldData = db.Set<SysDuplicateDetection>().ToList();
                    db.BulkDelete(oldData);
                    db.BulkInsert(duplicateDetectionList);
                    UnitOfWork.Commit(transaction);
                }
                return true;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<SysDuplicateDetection> GetSysDuplicateDetectionNoTracking()
        {
            try
            {
                return db.Set<SysDuplicateDetection>().AsNoTracking().ToList();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
