﻿using EFCore.BulkExtensions;
using IdentityModel.Client;
using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.Repositories;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandler;
using Innoviz.SmartApp.Data.ViewModels;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface ISysAccessRightService 
    {
        
        SysRoleTableItemView GetSysRoleTableById(string id);
        SysRoleTableItemView CreateSysRoleTable(SysRoleTableItemView sysRoleTableView);
        SysRoleTableItemView UpdateSysRoleTable(SysRoleTableItemView sysRoleTableView);
        bool DeleteSysRoleTable(string id);
        IEnumerable<SysFeatureGroupRole> CreateSysFeatureGroupRolesForRole(Guid roleId, Guid companyGUID,
                                                                        IEnumerable<SysFeatureGroupRoleView> featureRoles);
        IEnumerable<SysFeatureGroupRole> UpdateSysFeatureGroupRolesForRole(Guid roleId, Guid companyGUID, 
                                                                        IEnumerable<SysFeatureGroupRoleView> featureRoles);
        bool ValidateDeleteSysRoleTable(string roleId);

        // get SysFeatureRoles from ADMIN roles or role in edit mode (for step 2)
        IEnumerable<SysFeatureGroupRoleView> GetSysFeatureGroupRoleViewByCompanySiteAdminRole(int site, string companyHeader);
        SysUserSettingsItemView GetUserDefaultDataAccessRight(string userId, int site);
        SysUserSettingsItemView GetUserDefaultDataCompanyMapping(string userId);

        #region App login
        UserLogonView GetUserDefaultDataAndInitDataCheck(string userId, string username, int site);
        UserLogonView GetUserDataAccessRightCashierAndInitDataCheck(string userId, string username,
                                                                            int site, string companyGUID);
        SystemInitDataCheck GetSystemInitDataFlags();
        // get access right when logged in to App
        IEnumerable<AccessRightView> GetAccessRightModelByUserSiteCompany(string userId, int site, string companyGUID);
        AccessRightBU GetAccessRightBUByUserSiteCompany(string userId, int site, string companyGUID);

        // use with login sequence: case K2
        AccessRightWithCompanyBranchView GetUserAccessRightByCompany(string userId, string username, string companyGUID, int site);
        UserLogonView GetUserLogonViewByCompany(string userId, string username, int site, string companyGUID);

        // for ob framework
        AccessRightWithCompanyBranchView GetUserAccessRightForOB(string userId, string username);
        #endregion App login
        #region can access controller
        CheckAccessLevelResult CanUserAccessController(
                                            string userId,
                                            string siteHeader,
                                            string companyHeader,
                                            string controllerName,
                                            string routeAttr,
                                            string httpMethod);
        CheckAccessLevelResult CanClientScopeAccessController(string clientId,
                                                   IEnumerable<string> scopes,
                                                   string companyHeader,
                                                   string controllerName,
                                                   string routeAttr,
                                                   string httpMethod);
        #endregion can access controller

        #region Initialize data
        bool InitializeDevelopmentSysRoleTable(string username);
        bool InitializeDevelopmentSysScopeRole(string createdBy);
        bool InitializeSysFeatureGroupControllerMapping(IEnumerable<SysFeatureGroupControllerMappingView> mappings,
                                                        List<string> siteOBGroupIds, string sysFeatureGroupMenujson, 
                                                        List<SysControllerEntityMappingView> controllerEntityMappings,
                                                        string username, bool isDevelopment);
        List<string> GetControllerRouteAttributeFromAppRoute(string appRoute, string groupId, bool isDevelopment);
        List<SysControllerRole> GetSysControllerRolesForAdminRole(List<SysControllerTable> controllerTables,
                                                                        List<SysRoleTable> adminRoles, string username);
        List<SysFeatureGroupRole> GetSysFeatureGroupRolesForAdminRole(List<SysFeatureGroup> featureGroups,
                                                                                List<string> siteOBGroupIds,
                                                                                List<SysRoleTable> adminRoles, string username);
        CreateCompanyViewMap InitAdminRolesForNewCompany(CreateCompanyViewMap param, CreateCompanyParamView createCompanyView);
        IEnumerable<string> GetTableNamesForControllerEntityMapping();
        #endregion Initialize data
        //Task<string> GetClientCredentialsToken();
        IEnumerable<SelectItem<SysRoleTableItemView>> GetSysRoleTableDropDownNotFiltered(SearchParameter search);
        IEnumerable<SysFeatureGroupExportView> GetExportRoleData(string id);
        IEnumerable<ITree<SysFeatureGroupStructureView>> GetRoleStructure();
        ImportAccessRightResultView ImportAccessRight(ImportAccessRightView importAccessRightView);
    }
    public class SysAccessRightService: SmartAppService, ISysAccessRightService
    {
        public SysAccessRightService(SmartAppDbContext context) : base(context) { }
        public SysAccessRightService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public SysAccessRightService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public SysAccessRightService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public SysAccessRightService() : base() { }

        public SysRoleTableItemView GetSysRoleTableById(string id)
        {
            try
            {
                ISysRoleTableRepo roleRepo = new SysRoleTableRepo(db);
                SysRoleTableItemView role =  roleRepo.GetByIdvw(id.StringToGuid());

                ISysFeatureGroupRoleRepo featureRoleRepo = new SysFeatureGroupRoleRepo(db);
                role.SysFeatureGroupRoleViewList = featureRoleRepo.GetSysFeatureGroupRoleViewByRoleId(id.StringToGuid());

                return role;
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        public IEnumerable<SysFeatureGroupRoleView> GetSysFeatureGroupRoleViewByCompanySiteAdminRole(int site, string companyGUID)
        {
            try
            {
                ISysFeatureGroupRoleRepo featureRoleRepo = new SysFeatureGroupRoleRepo(db);
                return featureRoleRepo.GetSysFeatureGroupRoleViewByCompanySiteAdminRole(site, companyGUID.StringToGuid());
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        public IEnumerable<ViewModels.AccessRightView> GetAccessRightModelByUserSiteCompany(string userId, int site, string companyGUID)
        {
            try
            {
                ISysFeatureGroupRoleRepo featureRoleRepo = new SysFeatureGroupRoleRepo(db);
                return featureRoleRepo.GetAccessRightModelByUserSiteCompany(userId.StringToGuid(), site, companyGUID.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        public void ValidateAddRole(SysRoleTableItemView model)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(model.DisplayName))
                {
                    SmartAppException ex = new SmartAppException("ERROR.ERROR");
                    ex.AddData("ERROR.00073");
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                if (ExitsByDisplayName(model.DisplayName, model.CompanyGUID, model.SiteLoginType))
                {
                    SmartAppException ex = new SmartAppException("ERROR.ERROR");
                    ex.AddData("ERROR.DUPLICATE", "LABEL.NAME");
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SysRoleTableItemView CreateSysRoleTable(SysRoleTableItemView model)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                model = accessLevelService.AssignOwnerBU(model);
                ValidateAddRole(model);
                SysRoleTable sysRoleTable = model.ToSysRoleTable();
                ISysRoleTableRepo roleRepo = new SysRoleTableRepo(db);
                sysRoleTable = roleRepo.CreateSysRoleTable(sysRoleTable);
                base.LogTransactionCreate<SysRoleTable>(sysRoleTable);

                ISysFeatureGroupRoleRepo sysFeatureGroupRoleRepo = new SysFeatureGroupRoleRepo(db);
                ISysControllerRoleRepo sysControllerRoleRepo = new SysControllerRoleRepo(db);
                var sysFeatureGroupRole = PrepareSysFeatureGroupRoleForCreateOrUpdate(sysRoleTable.Id, sysRoleTable.CompanyGUID, model.SysFeatureGroupRoleViewList);
                var sysControllerRole = sysControllerRoleRepo.PrepareSysControllerRoleForCreateOrUpdate(sysFeatureGroupRole);
                var sysEntityRole = PrepareSysEntityRoleForCreateOrUpdate(sysRoleTable.Id, sysControllerRole);

                var bulkConfig = new BulkConfig { UseTempDB = true };
                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    this.BulkInsert(sysFeatureGroupRole, false, true, bulkConfig);
                    this.BulkInsert(sysControllerRole, false, true, bulkConfig);
                    this.BulkInsert(sysEntityRole, false, true, bulkConfig);
                    UnitOfWork.Commit(transaction);
                }

                return sysRoleTable.ToSysRoleTableItemView();
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SysFeatureGroupRole> CreateSysFeatureGroupRolesForRole(Guid roleId, Guid companyGUID,
                                                                        IEnumerable<SysFeatureGroupRoleView> featureRoles)
        {
            try
            {
                List<SysFeatureGroupRole> list = PrepareSysFeatureGroupRoleForCreateOrUpdate(roleId, companyGUID, featureRoles);

                var bulkConfig = new BulkConfig { UseTempDB = true };
                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    this.BulkInsert(list, false, true, bulkConfig);
                    UnitOfWork.Commit(transaction);
                }
                return list;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void ValidateUpdateRole(SysRoleTableItemView model)
        {
            try
            {
                if (model.DisplayName == null || model.DisplayName.Trim() == "")
                {
                    SmartAppException ex = new SmartAppException("ERROR.ERROR");
                    ex.AddData("ERROR.00073");
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                if (ExitsByDisplayName(model.Id, model.DisplayName, model.CompanyGUID, model.SiteLoginType))
                {
                    SmartAppException ex = new SmartAppException("ERROR.ERROR");
                    ex.AddData("ERROR.DUPLICATE", "LABEL.NAME");
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        public SysRoleTableItemView UpdateSysRoleTable(SysRoleTableItemView model)
        {
            try
            {
                ValidateUpdateRole(model);
                SysRoleTable inputSysRoleTable = model.ToSysRoleTable();
                ISysRoleTableRepo sysRoleTableRepo = new SysRoleTableRepo(db);
                SysRoleTable dbSysRoleTable = sysRoleTableRepo.Find(inputSysRoleTable.Id);
                dbSysRoleTable = sysRoleTableRepo.UpdateSysRoleTable(dbSysRoleTable, inputSysRoleTable);
                base.LogTransactionUpdate<SysRoleTable>(GetOriginalValues<SysRoleTable>(dbSysRoleTable), dbSysRoleTable);

                ISysFeatureGroupRoleRepo sysFeatureGroupRoleRepo = new SysFeatureGroupRoleRepo(db);
                ISysControllerRoleRepo sysControllerRoleRepo = new SysControllerRoleRepo(db);
                var sysFeatureGroupRole = PrepareSysFeatureGroupRoleForCreateOrUpdate(dbSysRoleTable.Id, dbSysRoleTable.CompanyGUID, model.SysFeatureGroupRoleViewList);
                var sysControllerRole = sysControllerRoleRepo.PrepareSysControllerRoleForCreateOrUpdate(sysFeatureGroupRole);
                var sysEntityRole = PrepareSysEntityRoleForCreateOrUpdate(dbSysRoleTable.Id, sysControllerRole);

                var bulkConfig = new BulkConfig { UseTempDB = true };
                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    List<SysFeatureGroupRole> oldSysFeatureGroupRole = sysFeatureGroupRoleRepo.GetSysFeatureGroupRoleByRoleGUIDNoTracking(dbSysRoleTable.Id);
                    List<SysControllerRole> oldSysControllerRole = sysControllerRoleRepo.GetSysControllerRoleByRoleGUIDNoTracking(dbSysRoleTable.Id);
                    List<SysEntityRole> oldSysEntityRole = db.Set<SysEntityRole>().Where(w => w.RoleGUID == dbSysRoleTable.Id).AsNoTracking().ToList();
                    this.BulkDelete(oldSysFeatureGroupRole);
                    this.BulkDelete(oldSysControllerRole);
                    this.BulkDelete(oldSysEntityRole);

                    this.BulkInsert(sysFeatureGroupRole, false, true, bulkConfig);
                    this.BulkInsert(sysControllerRole, false, true, bulkConfig);
                    this.BulkInsert(sysEntityRole, false, true, bulkConfig);
                    UnitOfWork.Commit(transaction);
                }

                return dbSysRoleTable.ToSysRoleTableItemView();
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SysFeatureGroupRole> UpdateSysFeatureGroupRolesForRole(Guid roleId, Guid companyGUID,
                                                                        IEnumerable<SysFeatureGroupRoleView> featureRoles)
        {
            try
            {
                List<SysFeatureGroupRole> list = PrepareSysFeatureGroupRoleForCreateOrUpdate(roleId, companyGUID, featureRoles);

                var bulkConfig = new BulkConfig { UseTempDB = true };
                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    List<SysFeatureGroupRole> oldData = db.Set<SysFeatureGroupRole>()
                                                    .Where(item => item.RoleGUID == roleId)
                                                    .ToList();
                    this.BulkDelete(oldData);
                    this.BulkInsert(list, false, true, bulkConfig);
                    UnitOfWork.Commit(transaction);
                }
                return list;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool ValidateDeleteSysRoleTable(string roleId) {
            try {
                ISysUserRolesRepo userRolesRepo = new SysUserRolesRepo(db);
                ISysFeatureGroupRoleRepo featureRoleRepo = new SysFeatureGroupRoleRepo(db);
                ISysControllerRoleRepo controllerRoleRepo = new SysControllerRoleRepo(db);
                bool roleExistsInFeatureRole = featureRoleRepo.RoleExistsInFeatureRole(roleId.StringToGuid());
                bool roleExistsInControllerRole = controllerRoleRepo.RoleExistsInControllerRole(roleId);
                bool roleExistsInUserRole = userRolesRepo.RoleExistsInUserRole(roleId);

                if(roleExistsInUserRole)
                {
                    SmartAppException ex = new SmartAppException("ERROR.00068");
                    ex.AddData("ERROR.00070");
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                if (roleExistsInControllerRole || roleExistsInFeatureRole) {
                    SmartAppException ex = new SmartAppException("ERROR.00068");
                    ex.AddData("ERROR.00072");
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else {
                    return true;
                }
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteSysRoleTable(string roleId)
        {
            try
            {
                ValidateDeleteSysRoleTable(roleId);
                ISysRoleTableRepo sysRoleTableRepo = new SysRoleTableRepo(db);
                Guid sysRoleTableGUID = new Guid(roleId);
                SysRoleTable sysRoleTable = sysRoleTableRepo.Find(sysRoleTableGUID);
                sysRoleTableRepo.Remove(sysRoleTable);
                base.LogTransactionDelete<SysRoleTable>(sysRoleTable);
                UnitOfWork.Commit();
                return true;
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool ExitsByDisplayName(string displayName, string companyGUID, int site)
        {
            try
            {
                Guid companyguid = new Guid(companyGUID);
                if (db.Set<SysRoleTable>().Where(a => a.CompanyGUID == companyguid &&
                                            a.DisplayName.ToLower() == displayName.ToLower() &&
                                            a.SiteLoginType == site)
                                            .Count() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        public bool ExitsByDisplayName(string id, string displayName, string companyGUID, int site)
        {
            try
            {
                Guid companyguid = new Guid(companyGUID);
                Guid roleId = new Guid(id);
                if (db.Set<SysRoleTable>().Where(a => a.CompanyGUID == companyguid &&
                                            a.Id != roleId &&
                                            a.DisplayName.ToLower() == displayName.ToLower() &&
                                            a.SiteLoginType == site).Count() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private List<SysFeatureGroupRole> PrepareSysFeatureGroupRoleForCreateOrUpdate(Guid roleGUID, Guid companyGUID,
                                                                            IEnumerable<SysFeatureGroupRoleView> sysFeatureRoleViews)
        {
            try
            {
                if (sysFeatureRoleViews != null && sysFeatureRoleViews.Count() > 0)
                {
                    string username = GetUserName();
                    List<SysFeatureGroupRole> list = new List<SysFeatureGroupRole>();
                    foreach (var fgrView in sysFeatureRoleViews)
                    {
                        int type = fgrView.SysFeatureGroup_FeatureType;

                        int action = SysAccessLevelHelper.GetCRUDActionByFeatureType(CRUDActionConst.ACTION, fgrView.Action, type);
                        int read = SysAccessLevelHelper.GetCRUDActionByFeatureType(CRUDActionConst.READ, fgrView.Read, type);
                        int update = SysAccessLevelHelper.GetCRUDActionByFeatureType(CRUDActionConst.UPDATE, fgrView.Update, type);
                        int create = SysAccessLevelHelper.GetCRUDActionByFeatureType(CRUDActionConst.CREATE, fgrView.Create, type);
                        int delete = SysAccessLevelHelper.GetCRUDActionByFeatureType(CRUDActionConst.DELETE, fgrView.Delete, type);

                        if(action > 0 || read > 0 || update > 0 || create > 0 || delete > 0)
                        {
                            SysFeatureGroupRole fgr = new SysFeatureGroupRole
                            {
                                SysFeatureGroupRoleGUID = Guid.NewGuid(),
                                RoleGUID = roleGUID,
                                SysFeatureGroupGUID = fgrView.SysFeatureGroupGUID.StringToGuid(),
                                Action = action,
                                Create = create,
                                Delete = delete,
                                Read = read,
                                Update = update,
                                CreatedBy = username,
                                CreatedDateTime = DateTime.Now,
                                ModifiedBy = username,
                                ModifiedDateTime = DateTime.Now,
                            };
                            list.Add(fgr);
                        }
                        
                    }

                    return list;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private List<SysEntityRole> PrepareSysEntityRoleForCreateOrUpdate(Guid roleGUID, List<SysControllerRole> sysControllerRole)
        {
            try
            {
                string username = GetUserName();
                var sysControllerEntityMappings = db.Set<SysControllerEntityMapping>().AsNoTracking().ToList();
                var list =
                    (from cem in sysControllerEntityMappings
                     join cr in sysControllerRole
                     on cem.SysControllerTableGUID equals cr.SysControllerTableGUID
                     select new { cem, cr })
                     .GroupBy(g => g.cem.ModelName)
                     .Select(s => new SysEntityRole
                     {
                         CreatedBy = username,
                         CreatedDateTime = DateTime.Now,
                         ModifiedBy = username,
                         ModifiedDateTime = DateTime.Now,
                         SysEntityRoleGUID = Guid.NewGuid(),
                         RoleGUID = roleGUID,
                         ModelName = s.Key,
                         AccessLevel = s.Max(m => m.cr.AccessLevel)
                     }).ToList();
                return list;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region App login
        public UserLogonView GetUserDefaultDataAndInitDataCheck(string userId, string username, int site)
        {
            try
            {
                return GetUserDataAccessRightAndInitDataCheck(userId, username, site);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public UserLogonView GetUserDataAccessRightCashierAndInitDataCheck(string userId, string username,
                                                                            int site, string companyGUID)
        {
            try
            {
                return GetUserDataAccessRightAndInitDataCheck(userId, username, site, companyGUID);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public UserLogonView GetUserLogonViewByCompany(string userId, string username,
                                                int site, string companyGUID)
        {
            try
            {
                UserLogonView result = GetUserDataAccessRightAndInitDataCheck(userId, username, site, companyGUID);
                companyGUID = companyGUID?.ToLower();
                if(result.InitDataCheck.HasAnyCompany && 
                    result.User != null &&
                    result.User.AccessRights != null)
                {
                    ISysUserCompanyMappingRepo userCompanyMappingRepo = new SysUserCompanyMappingRepo(db);
                    result.User.SysUserCompanyMappings = 
                        userCompanyMappingRepo.GetListByUserNamevw(result.User.UserName).ToList();

                    var userCompanyBranch = result.User.SysUserCompanyMappings.Where(item => item.CompanyGUID == companyGUID)
                                                        .FirstOrDefault();
                    CompanyBranchView companyBranchView = new CompanyBranchView
                    {
                        CompanyGUID = userCompanyBranch.CompanyGUID,
                        CompanyId = userCompanyBranch.Company_CompanyId,
                        BranchGUID = userCompanyBranch.BranchGUID,
                        BranchId = userCompanyBranch.Branch_BranchId
                    };

                    result.CompanyBranch = companyBranchView;
                }
                else
                {
                    result.CompanyBranch = null;
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private UserLogonView GetUserDataAccessRightAndInitDataCheck(string userId, string username, 
                                                                        int site, string companyGUID = null)
        {
            try
            {
                UserLogonView result = new UserLogonView();
                result.InitDataCheck = GetSystemInitDataFlags();
                if (!result.InitDataCheck.HasAnySysUserTable)
                {
                    ISysUserService userService = new SysUserService(db);
                    userService.CreateFirstSysUserTable(userId, username);

                    result.InitDataCheck.HasAnySysUserTable = true;
                }
                if(companyGUID == null)
                {
                    result.User = GetUserDefaultDataAccessRight(userId, site);
                }
                else
                {
                    result.User = GetUserDataAccessRightByCompany(userId, site, companyGUID);
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SystemInitDataCheck GetSystemInitDataFlags()
        {
            try
            {
                SystemInitDataCheck initDataCheck = new SystemInitDataCheck();
                initDataCheck.HasAnyCompany = (db.Set<Company>().Count() > 0);
                initDataCheck.HasAnySysUserTable = (db.Set<SysUserTable>().Count() > 0);
                return initDataCheck;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public SysUserSettingsItemView GetUserDefaultDataAccessRight(string userId, int site)
        {
            try
            {
                Guid userID = new Guid(userId);
                ISysUserTableRepo userRepo = new SysUserTableRepo(db);
                SysUserSettingsItemView result = userRepo.GetUserSettingsById(userId);

                if (result.DefaultCompanyGUID != null /*&& site != null*/)
                {
                    var accessRights = GetAccessRightModelByUserSiteCompany(userId, site, result.DefaultCompanyGUID);

                    result.AccessRights = accessRights != null ? accessRights.ToList() : null;
                }
                return result;
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        public SysUserSettingsItemView GetUserDataAccessRightByCompany(string userId, int site, string companyGUID)
        {
            try
            {
                Guid userID = new Guid(userId);
                ISysUserTableRepo userRepo = new SysUserTableRepo(db);
                SysUserSettingsItemView result = userRepo.GetUserSettingsById(userId);
                
                if (companyGUID != null/* && site != null*/)
                {
                    var accessRights = GetAccessRightModelByUserSiteCompany(userId, site, companyGUID);

                    result.AccessRights = accessRights != null ? accessRights.ToList() : null;
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SysUserSettingsItemView GetUserDefaultDataCompanyMapping(string userId)
        {
            try
            {
                ISysUserTableRepo userRepo = new SysUserTableRepo(db);
                SysUserSettingsItemView result = userRepo.GetUserSettingsById(userId);

                ISysUserCompanyMappingRepo userCompanyMappingRepo = new SysUserCompanyMappingRepo(db);
                string username = result.UserName;
                result.SysUserCompanyMappings = userCompanyMappingRepo.GetListByUserNamevw(username).ToList();

                return result;
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AccessRightWithCompanyBranchView GetUserAccessRightByCompany(string userId, string username,
                                                                            string companyGUID, int site)
        {
            try
            {
                AccessRightWithCompanyBranchView result = new AccessRightWithCompanyBranchView();
                companyGUID = companyGUID?.ToLower();
                //if(site != null)
                //{
                    result.AccessRight = GetAccessRightModelByUserSiteCompany(userId, site, companyGUID);
                //}
                if (result.AccessRight != null)
                {
                    ISysUserCompanyMappingRepo userCompanyMappingRepo = new SysUserCompanyMappingRepo(db);

                    var sysUserCompanyMappings =
                        userCompanyMappingRepo.GetListByUserNamevw(username).ToList();

                    var userCompanyBranch = sysUserCompanyMappings.Where(item => item.CompanyGUID == companyGUID)
                                                        .FirstOrDefault();
                    CompanyBranchView companyBranchView = new CompanyBranchView
                    {
                        CompanyGUID = userCompanyBranch.CompanyGUID,
                        CompanyId = userCompanyBranch.Company_CompanyId,
                        BranchGUID = userCompanyBranch.BranchGUID,
                        BranchId = userCompanyBranch.Branch_BranchId
                    };

                    result.CompanyBranch = companyBranchView;

                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AccessRightWithCompanyBranchView GetUserAccessRightForOB(string userId, string username)
        {
            try
            {
                AccessRightWithCompanyBranchView result = new AccessRightWithCompanyBranchView();

                ISysRoleTableRepo roleRepo = new SysRoleTableRepo(db);
                SysRoleTableView obRole = roleRepo.GetOBSiteRole();

                ISysUserCompanyMappingRepo userCompanyMappingRepo = new SysUserCompanyMappingRepo(db);
                var sysUserCompanyMappings =
                    userCompanyMappingRepo.GetListByUserNamevw(username).ToList();

                var userCompanyBranch = sysUserCompanyMappings.Where(item => item.CompanyGUID == obRole.CompanyGUID)
                                                    .FirstOrDefault();

                if(userCompanyBranch == null)
                {
                    result.AccessRight = null;
                    return result;
                }
                else
                {
                    result.AccessRight = GetAccessRightModelByUserSiteCompany(userId, SysAccessLevelHelper.GetLoginSiteValue(SiteLoginType.OB), obRole.CompanyGUID);
                    CompanyBranchView companyBranchView = new CompanyBranchView
                    {
                        CompanyGUID = userCompanyBranch.CompanyGUID,
                        CompanyId = userCompanyBranch.Company_CompanyId,
                        BranchGUID = userCompanyBranch.BranchGUID,
                        BranchId = userCompanyBranch.Branch_BranchId
                    };

                    result.CompanyBranch = companyBranchView;
                }
                
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion App login
        #region can access controller
        public CheckAccessLevelResult CanUserAccessController(
                                            string userId,
                                            string siteHeader,
                                            string companyHeader,
                                            string controllerName,
                                            string routeAttr,
                                            string httpMethod)
        {
            try
            {
                CheckAccessLevelResult result = new CheckAccessLevelResult();
                // white-listed: Get user's access right, get tasklist
                if ((controllerName == "SysAccessRight" &&
                    routeAttr == "UserDefaultBranchCompanyAccessRight/{userId}" &&
                    httpMethod == "GET") ||
                    (controllerName == "SysAccessRight" &&
                    routeAttr == "UserAccessRightCashier/{userId}" &&
                    httpMethod == "GET") ||
                    (controllerName == "SysAccessRight" &&
                    routeAttr == "SysFeatureRoleView/GetByUser/{userId}" &&
                    httpMethod == "GET") ||
                    (controllerName == "SysAccessRight" &&
                    routeAttr == "UserAccessRightForOB/{userId}" &&
                    httpMethod == "GET") ||
                    (controllerName == "SysAccessRight" &&
                    routeAttr == "UserAccessRightByCompany/{userId}/{companyGUID}" &&
                    httpMethod == "GET") ||
                    (controllerName == "SysAccessRight" &&
                    routeAttr == "UserLogonViewByCompany/{userId}/{companyGUID}" &&
                    httpMethod == "GET") ||
                    (controllerName == "SysUser" &&
                    routeAttr == "CheckExistingUser" &&
                    httpMethod == "POST") ||
                    (controllerName == "SysAccessRight" &&
                    routeAttr == "UserDefaultBranchCompanyMappings/{userId}" &&
                    httpMethod == "GET") ||
                    (controllerName == "Home" &&
                    routeAttr == "GetTaskList" &&
                    httpMethod == "POST") ||
                #region usersettings
                    (controllerName == "SysUser" &&
                    routeAttr == "UserSettings/{userId}" &&
                    httpMethod == "GET") ||
                    (controllerName == "SysUser" &&
                    routeAttr == "UpdateSettings" &&
                    httpMethod == "POST") ||
                #endregion 
                    (controllerName == "Assembly" &&
                    routeAttr == "GetAssemblyVersion" &&
                    httpMethod == "GET")
                    )
                {
                    #region LIT
                    result.CanAccessController = true;
                    return result;
                    #endregion LIT
                }

                // white-listed: create first company
                if (db.Set<Company>().Count() == 0)
                {
                    if((controllerName == "SysUser" &&
                        routeAttr == "SysUserTableView/{id}" &&
                        httpMethod == "GET") || 
                        (controllerName == "Company" &&
                        routeAttr == "CreateCompany" &&
                        httpMethod == "POST"))
                    {
                        #region LIT
                        result.CanAccessController = true;
                        result.IsCreateCompany = true;
                        return result;
                        #endregion LIT
                    }
                }

                // TODO remove this when SysFeatureGroupController has data
                //if (!db.Set<SysFeatureGroupController>().Any())
                ////if (!db.Set<SysFeatureController>().Any())
                //{
                //    #region LIT
                //    result.CanAccessController = true;
                //    return result;
                //    #endregion LIT
                //}

                Guid userID = new Guid(userId);
                IQueryable<SysRoleTable> roleQuery = db.Set<SysRoleTable>();
                if(siteHeader != null)
                {
                    int siteLogin = SysAccessLevelHelper.GetLoginSiteValue(siteHeader);
                    roleQuery = roleQuery.Where(item => item.SiteLoginType == siteLogin);
                }
                if(companyHeader != null)
                {
                    Guid companyGUID = new Guid(companyHeader);
                    roleQuery = roleQuery.Where(item => item.CompanyGUID == companyGUID);
                }
                // find ADMIN role
                roleQuery = (from userrole in db.Set<SysUserRoles>().Where(item => item.UserId == userID)
                             join role in roleQuery
                             on userrole.RoleId equals role.Id
                             select role);

                //if(roleQuery.Any(item=>item.DisplayName == TextConstants.ADMIN))
                //{
                //    #region LIT
                //    result.CanAccessController = true;
                //    result.AccessLevel = 4;
                //    return result;
                //    #endregion LIT
                //}
                //else
                //{
                #region LIT
                var getdropdownRegex = new Regex("(?<prefix>.*?/(?i)Get|(?i)Get)(?<tablename>(?i)Withdrawal.*?|(?i)Withholding.*?|.*?)(?i)(?:(dropdown)|(By.+?Dropdown)|(Filter.+?Dropdown)|(With.+?Dropdown))");
                if(getdropdownRegex.IsMatch(routeAttr))
                {
                    var match = getdropdownRegex.Match(routeAttr).Groups["tablename"].Value;
                    // system tables
                    if(match.StartsWith("Sys", StringComparison.InvariantCultureIgnoreCase) ||
                        match.StartsWith("Migration", StringComparison.InvariantCultureIgnoreCase) ||
                        match.StartsWith("Staging_", StringComparison.InvariantCultureIgnoreCase) ||
                        match.StartsWith("Batch", StringComparison.InvariantCultureIgnoreCase) ||
                        match.ToUpper() == "AdUser".ToUpper())
                    {
                        result.CanAccessController = true;
                        result.AccessLevel = 4;
                        return result;
                    }

                    var lookupTableNames = SystemStaticData.GetDropdownLookupData();
                    string tableName = null;
                    var lookup = lookupTableNames.TryGetValue(match, out tableName);
                    var cems = db.Set<SysControllerEntityMapping>().AsNoTracking();

                    if (lookup)
                    {
                        if (cems.Any(a => a.ModelName.ToUpper() == tableName.ToUpper()))
                        {
                            var resultQuery =
                                (from role in roleQuery
                                 join entityRole in db.Set<SysEntityRole>().Where(w => w.ModelName.ToUpper() == tableName.ToUpper())
                                 on role.Id equals entityRole.RoleGUID
                                 
                                 select entityRole
                                );
                            result.CanAccessController = true;
                            result.AccessLevel = resultQuery.Count() > 0 ? resultQuery.Max(m => m.AccessLevel) : 0;
                            return result;
                        }
                        else
                        {
                            // not found in mapping => default to AccessLevel.Company
                            result.CanAccessController = true;
                            result.AccessLevel = 4;
                            return result;
                        }
                    }
                    else
                    {
                        // route dropdown wrong format or not found in dropdown table name lookup
                        result.CanAccessController = true;
                        result.AccessLevel = 0;
                        return result;
                    }
                }
                else
                {
                    // normal case
                    var resultQuery =
                        (from role in roleQuery
                         join controllerrole in db.Set<SysControllerRole>()
                         on role.Id equals controllerrole.RoleGUID
                         join controller in db.Set<SysControllerTable>()
                                                        .Where(item => item.ControllerName == controllerName &&
                                                                    routeAttr.ToUpper().StartsWith(item.RouteAttribute.ToUpper()))
                         on controllerrole.SysControllerTableGUID equals controller.SysControllerTableGUID
                         select controllerrole
                        );
                    result.CanAccessController = resultQuery.Count() > 0;
                    result.AccessLevel = result.CanAccessController ? resultQuery.Max(m => m.AccessLevel) : 0;
                    return result;
                }
                
                    #endregion LIT
                //}
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CheckAccessLevelResult CanClientScopeAccessController(string clientId,
                                                   IEnumerable<string> scopes,
                                                   string companyHeader,
                                                   string controllerName,
                                                   string routeAttr,
                                                   string httpMethod)
        {
            try
            {
                var result = new CheckAccessLevelResult();
                if(clientId == null || scopes == null)
                {
                    result.CanAccessController = false;
                    return result;
                }

                // whitelisted: GetWebApiControllerDisco
                if((controllerName == "SysAccessRight" &&
                    routeAttr == "GetSysControllerDiscoveryList" &&
                    httpMethod == "GET"))
                {
                    result.CanAccessController = true;
                    return result;
                }

                IQueryable<SysScopeRole> scopeRoleQuery =
                    db.Set<SysScopeRole>().Where(item => item.ClientId == clientId &&
                                                        scopes.Any(s => s == item.ScopeName));
                
                Guid companyGUID = (companyHeader != null) ? new Guid(companyHeader) : Guid.Empty;
                IQueryable<SysRoleTable> roleQuery = db.Set<SysRoleTable>().Where(item=>item.CompanyGUID == companyGUID);
                roleQuery = (from scoperole in scopeRoleQuery
                             join role in roleQuery
                             on scoperole.RoleGUID equals role.Id
                             select role);
                //// find ADMIN role
                //if(roleQuery.Any(item=>item.DisplayName == TextConstants.ADMIN))
                //{
                //    return true;
                //}
                //else
                //{
                var resultQuery =
                    (from role in roleQuery
                         join controllerrole in db.Set<SysControllerRole>()
                         on role.Id equals controllerrole.RoleGUID
                         join controller in db.Set<SysControllerTable>()
                                                    .Where(item => item.ControllerName == controllerName &&
                                                                routeAttr.ToUpper().StartsWith(item.RouteAttribute.ToUpper()))
                         on controllerrole.SysControllerTableGUID equals controller.SysControllerTableGUID
                         select controllerrole
                         );
                result.CanAccessController = resultQuery.Count() > 0;
                result.AccessLevel = result.CanAccessController ? resultQuery.Max(m => m.AccessLevel) : 0;
                return result;
                //}
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion can access controller

        #region Initialize data
        public CreateCompanyViewMap InitAdminRolesForNewCompany(CreateCompanyViewMap param, CreateCompanyParamView createCompanyView)
        {
            try
            {
                ISysControllerTableRepo sysControllerTableRepo = new SysControllerTableRepo(db);
                ISysFeatureGroupRepo sysFeatureGroupRepo = new SysFeatureGroupRepo(db);

                var companyGuid = new List<Guid>() { param.Company.CompanyGUID };
                string username = createCompanyView.UserName;
                // set username
                List<SysRoleTable> rolesToAdd = GetNewAdminRoles(companyGuid, new List<BusinessUnit>() { param.BusinessUnit }, username);

                // map user to admin roles
                List<SysUserRoles> mappedUserRoles = MapUserToAdminRoleNewCompany(createCompanyView.UserId.StringToGuid(), rolesToAdd);

                // create SysScopeRole admin role
                List<SysScopeRole> sysScopeRole = GetAdminRoleSysScopeRole(rolesToAdd, username);

                // admin role SysControllerRoles
                List<SysControllerTable> controllerTables = sysControllerTableRepo.GetListSysControllerTableNoTracking();
                List<SysControllerRole> adminControllerRoles = GetSysControllerRolesForAdminRole(controllerTables, rolesToAdd, username);

                // admin role SysFeatureGroupRoles
                List<SysFeatureGroup> featureGroups = sysFeatureGroupRepo.GetListSysFeatureGroupNoTracking();
                var siteOBGroupIds = SystemStaticData.GetOBFeatureGroupIds();
                List<SysFeatureGroupRole> adminFeatureGroupRoles = GetSysFeatureGroupRolesForAdminRole(featureGroups, siteOBGroupIds, rolesToAdd, username);

                param.SysRoleTable = rolesToAdd;
                param.SysUserRoles = mappedUserRoles;
                param.SysScopeRole = sysScopeRole;
                param.SysControllerRole = adminControllerRoles;
                param.SysFeatureGroupRole = adminFeatureGroupRoles;

                return param;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private List<SysRoleTable> GetNewAdminRoles(IEnumerable<Guid> companies, List<BusinessUnit> rootBusinessUnits, string username)
        {
            try
            {
                if (companies.Count() == 0)
                {
                    return null;
                }
                ISysRoleTableRepo sysRoleTableRepo = new SysRoleTableRepo(db);
                List<SysRoleTable> adminRoles = sysRoleTableRepo.GetAdminRoleByCompanies(companies.ToList());
                List<SysRoleTable> rolesToAdd = new List<SysRoleTable>();

                foreach (var item in companies)
                {
                    var existingRoles = adminRoles.Where(r => r.CompanyGUID == item);
                    var businessUnit = rootBusinessUnits.Where(w => w.CompanyGUID == item).FirstOrDefault();
                    if (existingRoles.Count() < 2) // back, ob
                    {
                        if (existingRoles.Where(r => r.SiteLoginType == SiteLoginValue.Back).Count() < 1)
                        {
                            SysRoleTable role = GetNewRoleObject(item, TextConstants.ADMIN, SiteLoginValue.Back, username, businessUnit);
                            rolesToAdd.Add(role);
                        }
                        
                        if (existingRoles.Where(r => r.SiteLoginType == SiteLoginValue.OB).Count() < 1)
                        {
                            SysRoleTable role = GetNewRoleObject(item, TextConstants.ADMIN, SiteLoginValue.OB, username, businessUnit);
                            rolesToAdd.Add(role);
                        }
                    }
                }
                if (rolesToAdd.Count() == 0)
                {
                    return null;
                }
                else
                {
                    return rolesToAdd;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private SysRoleTable GetNewRoleObject(Guid companyGUID, string roleDisplayName, int site, string username, BusinessUnit businessUnit)
        {

            Guid guid = Guid.NewGuid();
            return new SysRoleTable()
            {
                Id = guid,
                Name = guid.GuidNullToString(),
                DisplayName = roleDisplayName,
                CompanyGUID = companyGUID,
                NormalizedName = guid.GuidNullToString().ToUpper(),
                SiteLoginType = site,

                CreatedDateTime = DateTime.Now,
                ModifiedDateTime = DateTime.Now,

                CreatedBy = username,
                ModifiedBy = username,

                Owner = username,
                OwnerBusinessUnitGUID = businessUnit != null ? (Guid?)businessUnit.BusinessUnitGUID : null
            };
        }
        private List<SysUserRoles> MapUserToAdminRoleNewCompany(Guid userGUID, List<SysRoleTable> rolesToMap)
        {
            try
            {
                List<SysUserRoles> userRoles = rolesToMap.Select(s => new SysUserRoles
                                                        {
                                                            UserId = userGUID,
                                                            RoleId = s.Id
                                                        })
                                                       .ToList();
                return userRoles;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public bool InitializeDevelopmentSysScopeRole(string createdBy)
        {
            try
            {
                SmartAppUtil.LogMessage(Serilog.Events.LogEventLevel.Information, "Initializing SysScopeRole...");

                ISysScopeRoleRepo scopeRoleRepo = new SysScopeRoleRepo(db);
                ISysRoleTableRepo sysRoleTableRepo = new SysRoleTableRepo(db);
                ICompanyRepo companyRepo = new CompanyRepo(db);

                var companyGuids = companyRepo.GetListCompanyNoTracking().Select(s => s.CompanyGUID).ToList();

                List<SysRoleTable> allAdminRoles = sysRoleTableRepo.GetAdminRoleByCompanies(companyGuids);
                
                List<SysScopeRole> scopeRolesToAdd = new List<SysScopeRole>();
                
                var existingScopeRoles = scopeRoleRepo.GetSysScopeRoleByRoleGUIDNoTracking(allAdminRoles.Select(s => s.Id));

                var allScopeRole = GetAdminRoleSysScopeRole(allAdminRoles, createdBy);

                var diffItems = allScopeRole.Where(item =>
                                !existingScopeRoles.Any(item2 => item2.ClientId == item.ClientId &&
                                                               item2.ScopeName == item.ScopeName &&
                                                               item2.RoleGUID == item.RoleGUID)).ToList();
                var diffDelete = existingScopeRoles.Where(w =>
                                !allScopeRole.Any(a => a.ClientId == w.ClientId &&
                                                        a.ScopeName == w.ScopeName &&
                                                        a.RoleGUID == w.RoleGUID)).ToList();
                if (diffItems.Count != 0)
                {
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        if (diffDelete.Count > 0)
                        {
                            db.BulkDelete(diffDelete);
                        }
                        db.BulkInsert(diffItems);
                        UnitOfWork.Commit(transaction);
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private List<SysScopeRole> GetAdminRoleSysScopeRole(List<SysRoleTable> allAdminRoles, string createdBy)
        {
            try
            {
                var obAdminRoles = allAdminRoles.Where(item => (item.SiteLoginType == SiteLoginValue.OB));
                var notObAdminRoles = allAdminRoles.Where(item => (item.SiteLoginType != SiteLoginValue.OB));

                #region all Client-ApiScope (fixed values)
                List<SysScopeRole> clientScopes = new List<SysScopeRole>()
                {
                    new SysScopeRole
                    {
                        ClientId = "oidc-client", ScopeName = "batch-user-api",
                        CreatedBy = createdBy, ModifiedBy = createdBy,
                        CreatedDateTime = DateTime.Now, ModifiedDateTime = DateTime.Now
                    },

                    new SysScopeRole
                    {
                        ClientId = "oidc-client-dev", ScopeName = "batch-user-api",
                        CreatedBy = createdBy, ModifiedBy = createdBy,
                        CreatedDateTime = DateTime.Now, ModifiedDateTime = DateTime.Now
                    },

                    new SysScopeRole
                    {
                        ClientId = "quartz-client", ScopeName = "batch-api",
                        CreatedBy = createdBy, ModifiedBy = createdBy,
                        CreatedDateTime = DateTime.Now, ModifiedDateTime = DateTime.Now
                    },
                    new SysScopeRole
                    {
                        ClientId = "quartz-client", ScopeName = "batch-user-api",
                        CreatedBy = createdBy, ModifiedBy = createdBy,
                        CreatedDateTime = DateTime.Now, ModifiedDateTime = DateTime.Now
                    },
                    new SysScopeRole
                    {
                        ClientId = "web-api-client", ScopeName = "batch-api",
                        CreatedBy = createdBy, ModifiedBy = createdBy,
                        CreatedDateTime = DateTime.Now, ModifiedDateTime = DateTime.Now
                    },
                    new SysScopeRole
                    {
                        ClientId = "web-api-client", ScopeName = "batch-user-api",
                        CreatedBy = createdBy, ModifiedBy = createdBy,
                        CreatedDateTime = DateTime.Now, ModifiedDateTime = DateTime.Now
                    },

                    new SysScopeRole
                    {
                        ClientId = "ob-framework-client", ScopeName = "web-api.dev",
                        CreatedBy = createdBy, ModifiedBy = createdBy,
                        CreatedDateTime = DateTime.Now, ModifiedDateTime = DateTime.Now
                    },
                    new SysScopeRole
                    {
                        ClientId = "ob-framework-client", ScopeName = "web-api.prod",
                        CreatedBy = createdBy, ModifiedBy = createdBy,
                        CreatedDateTime = DateTime.Now, ModifiedDateTime = DateTime.Now
                    },
                    new SysScopeRole
                    {
                        ClientId = "ob-framework-client", ScopeName = "connector-api",
                        CreatedBy = createdBy, ModifiedBy = createdBy,
                        CreatedDateTime = DateTime.Now, ModifiedDateTime = DateTime.Now
                    },
                    new SysScopeRole
                    {
                        ClientId = "api-unit-test-client", ScopeName = "web-api.dev",
                        CreatedBy = createdBy, ModifiedBy = createdBy,
                        CreatedDateTime = DateTime.Now, ModifiedDateTime = DateTime.Now
                    },
                    new SysScopeRole
                    {
                        ClientId = "api-unit-test-client", ScopeName = "web-api.prod",
                        CreatedBy = createdBy, ModifiedBy = createdBy,
                        CreatedDateTime = DateTime.Now, ModifiedDateTime = DateTime.Now
                    },
                    new SysScopeRole
                    {
                        ClientId = "api-unit-test-client", ScopeName = "connector-api",
                        CreatedBy = createdBy, ModifiedBy = createdBy,
                        CreatedDateTime = DateTime.Now, ModifiedDateTime = DateTime.Now
                    },
                };
                #endregion
                var allScopeRole = (from scope in clientScopes.Where(w => w.ClientId != "ob-framework-client")
                                    from role in notObAdminRoles
                                    select new SysScopeRole
                                    {
                                        ClientId = scope.ClientId,
                                        ScopeName = scope.ScopeName,
                                        RoleGUID = role.Id,
                                        CreatedBy = scope.CreatedBy,
                                        ModifiedBy = scope.ModifiedBy,
                                        CreatedDateTime = scope.CreatedDateTime,
                                        ModifiedDateTime = scope.ModifiedDateTime
                                    }).ToList();
                var obScopeRole = (from scope in clientScopes.Where(w => w.ClientId == "ob-framework-client")
                                   from role in obAdminRoles
                                   select new SysScopeRole
                                   {
                                       ClientId = scope.ClientId,
                                       ScopeName = scope.ScopeName,
                                       RoleGUID = role.Id,
                                       CreatedBy = scope.CreatedBy,
                                       ModifiedBy = scope.ModifiedBy,
                                       CreatedDateTime = scope.CreatedDateTime,
                                       ModifiedDateTime = scope.ModifiedDateTime,
                                   });
                allScopeRole.AddRange(obScopeRole);
                return allScopeRole.ToList();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool InitializeDevelopmentSysRoleTable(string username)
        {
            try
            {
                SmartAppUtil.LogMessage(Serilog.Events.LogEventLevel.Information, "Initializing SysRoleTable...");

                ICompanyRepo companyRepo = new CompanyRepo(db);
                IBusinessUnitRepo businessUnitRepo = new BusinessUnitRepo(db);
                
                var companies = companyRepo.GetListCompanyNoTracking().Select(s => s.CompanyGUID);
                List<BusinessUnit> rootBusinessUnits = businessUnitRepo.GetAllRootBusinessUnits();

                IEnumerable<SysRoleTable> newAdminRoles = GetNewAdminRoles(companies, rootBusinessUnits, username);

                if (newAdminRoles != null && newAdminRoles.Count() != 0)
                {
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        db.BulkInsert(newAdminRoles.ToList());
                        UnitOfWork.Commit(transaction);
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool InitializeSysFeatureGroupControllerMapping(IEnumerable<SysFeatureGroupControllerMappingView> mappings, 
                                                            List<string> siteOBGroupIds, string sysFeatureGroupMenujson,
                                                            List<SysControllerEntityMappingView> controllerEntityMappingViews, 
                                                            string username, bool isDevelopment)
        {
            try
            {
                SmartAppUtil.LogMessage(Serilog.Events.LogEventLevel.Information, "Initializing SysFeatureTableData...");
                if (mappings != null && mappings.Count() > 0)
                {
                    List<SysFeatureGroup> featureGroups = new List<SysFeatureGroup>();
                    List<SysFeatureGroupMapping> featureGroupMappings = new List<SysFeatureGroupMapping>();
                    List<SysFeatureTable> featureTables = new List<SysFeatureTable>();
                    List<SysFeatureGroupController> featureGroupControllers = new List<SysFeatureGroupController>();
                    List<SysControllerRole> controllerRoles = new List<SysControllerRole>();
                    List<SysFeatureGroupRole> featureGroupRoles = new List<SysFeatureGroupRole>();
                    List<SysControllerTable> controllerTables = new List<SysControllerTable>();
                    List<SysFeatureGroupStructure> featureGroupStructures = new List<SysFeatureGroupStructure>();
                    List<SysControllerEntityMapping> controllerEntityMappings = new List<SysControllerEntityMapping>();
                    List<SysEntityRole> entityRoles = new List<SysEntityRole>();

                    // menu mappings
                    List<SysFeatureGroupMenuMapping> menuStructures = JsonConvert.DeserializeObject<List<SysFeatureGroupMenuMapping>>(sysFeatureGroupMenujson);
                    var featureGroupMenus = GetSysFeatureGroupMenuMappings(menuStructures, new List<SysFeatureGroupMenuMapping>());

                    var all = mappings.ToList();

                    // validate GroupId
                    SmartAppException ex = new SmartAppException("Error SysFeatureTableData");
                    var groupByGroupId = all.GroupBy(g => g.GroupId).Select(s => new { s.Key, Group = s });
                    if(groupByGroupId.Any(a => a.Group.Count() > 1))
                    {
                        var groups = groupByGroupId.Where(w => w.Group.Count() > 1).Select(s => s.Key);
                        ex.AddData("Duplicate SysFeatureGroup.GroupId: " + string.Join(", ", groups));
                        throw ex;
                    }
                    if(groupByGroupId.Any(a => !a.Key.IsSnakeUpperCase()))
                    {
                        var groups = groupByGroupId.Where(w => !w.Key.IsSnakeUpperCase()).Select(s => s.Key);
                        ex.AddData("Wrong format SysFeatureGroup.GroupId: " + string.Join(", ", groups));
                        throw ex;
                    }

                    // validate menus
                    var notMatchedMenuGroup = featureGroupMenus.Where(w => !all.Any(a => a.GroupId == w.GroupId));
                    if (notMatchedMenuGroup.Count() > 0)
                    {
                        var groups = notMatchedMenuGroup.Select(s => s.GroupId);
                        ex.AddData("Menu-GroupId mapping not found in SysFeatureTableData: " + string.Join(", ", groups));
                        throw ex;
                    }


                    var fgg = all.GroupBy(g => g.ControllerName)
                                .Select(s => new
                                {
                                    ControllerName = s.Key,
                                    Features = s
                                });

                    List<string> notTranslated = new List<string>();
                    
                    foreach (var controller in fgg)
                    {
                        //List<SysFeatureGroupStructure> sysFeatureGroupStructureToMap = GetSysFeatureGroupStructureToMap(controller.ControllerName, controller.Features.ToList());
                        foreach (var item in controller.Features)
                        {
                            #region SysControllerTable
                            List<SysControllerTable> controllers = 
                                GetSysControllerTableFromMapping(item.ControllerName, item.GroupId, item.FeatureType, item.SysFeatureTables, true);
                            controllerTables.AddRange(controllers);
                            #endregion
                            #region SysFeatureGroup
                            string pathMenu = GetPathMenu(item.SysFeatureTables, item.GroupId, notTranslated);
                            SysFeatureGroup featureGroup = new SysFeatureGroup
                            {
                                CreatedBy = username,
                                CreatedDateTime = DateTime.Now,
                                ModifiedBy = username,
                                ModifiedDateTime = DateTime.Now,
                                GroupId = item.GroupId,
                                FeatureType = GetFeatureTypeValue(item.FeatureType),
                                SysFeatureGroupGUID = Guid.NewGuid(),
                                PathMenu = pathMenu
                            };
                            featureGroups.Add(featureGroup);
                            #endregion
                            #region SysFeatureTables
                            List<SysFeatureTable> features = item.SysFeatureTables.Select(s => new SysFeatureTable
                            {
                                CreatedBy = username,
                                CreatedDateTime = DateTime.Now,
                                ModifiedBy = username,
                                ModifiedDateTime = DateTime.Now,
                                SysFeatureTableGUID = Guid.NewGuid(),
                                FeatureId = s.FeatureId,
                                ParentFeatureId = s.ParentFeatureId,
                                Path = s.Path,

                            }).ToList();
                            featureTables.AddRange(features);
                            #endregion
                            #region SysFeatureGroupMappings
                            List<SysFeatureGroupMapping> fgMappings = features.Select(s => new SysFeatureGroupMapping
                            {
                                CreatedBy = username,
                                CreatedDateTime = DateTime.Now,
                                ModifiedBy = username,
                                ModifiedDateTime = DateTime.Now,
                                SysFeatureGroupGUID = featureGroup.SysFeatureGroupGUID,
                                SysFeatureTableGUID = s.SysFeatureTableGUID
                            }).ToList();
                            featureGroupMappings.AddRange(fgMappings);
                            #endregion
                            #region SysFeatureGroupControllers
                            if (item.FeatureType == FeatureType.PRIMARY || item.FeatureType == FeatureType.RELATEDINFO)
                            {
                                var fgcMappings = 
                                    controllers.Where(w => !w.RouteAttribute.Split("/").Any(a => a == "Function") &&
                                                            !w.RouteAttribute.Split("/").Any(a => a == "Report"))
                                                .Select(s => new SysFeatureGroupController
                                                {
                                                    CreatedBy = username,
                                                    CreatedDateTime = DateTime.Now,
                                                    ModifiedBy = username,
                                                    ModifiedDateTime = DateTime.Now,
                                                    SiteLoginType = 1, // back
                                                    SysFeatureGroupGUID = featureGroup.SysFeatureGroupGUID,
                                                    SysControllerTableGUID = s.SysControllerTableGUID,
                                                    SysFeatureGroupControllerGUID = Guid.NewGuid(),
                                                    AccessRight = s.RouteAttribute.EndsWith("Get") || s.RouteAttribute.EndsWith("Validate") ? 1 :
                                                        (s.RouteAttribute.EndsWith("Update") ? 2 : (s.RouteAttribute.EndsWith("Create") ? 3 :
                                                        (s.RouteAttribute.EndsWith("Delete") ? 4 : 0))),
                                                });
                                featureGroupControllers.AddRange(fgcMappings);
                            }
                            else if(item.FeatureType == FeatureType.WORKFLOW || item.FeatureType == FeatureType.WORKFLOW_ACTIONHISTORY)
                            {
                                var fgcMappings =
                                    controllers.Where(w => w.RouteAttribute.Split("/").Any(a => a == "Workflow"))
                                                .Select(s => new SysFeatureGroupController
                                                {
                                                    //CreatedBy = username,
                                                    CreatedBy = "System",
                                                    CreatedDateTime = DateTime.Now,
                                                    //ModifiedBy = username,
                                                    ModifiedBy = "System",
                                                    ModifiedDateTime = DateTime.Now,
                                                    SiteLoginType = 1, // back
                                                    SysFeatureGroupGUID = featureGroup.SysFeatureGroupGUID,
                                                    SysControllerTableGUID = s.SysControllerTableGUID,
                                                    SysFeatureGroupControllerGUID = Guid.NewGuid(),
                                                    AccessRight = s.RouteAttribute.Contains("Workflow/ActionHistory/Get") ? 1 :
                                                        (s.RouteAttribute.Contains("Workflow/Action") || s.RouteAttribute.Contains("Workflow/StartWorkflow") ? 5 : 0)
                                                });
                                featureGroupControllers.AddRange(fgcMappings);
                            }
                            else
                            {
                                var fgcMappings = 
                                    controllers.Where(w => w.RouteAttribute.Split("/").Any(a => a == "Function") ||
                                                            w.RouteAttribute.Split("/").Any(a => a == "Report"))
                                                .Select(s => new SysFeatureGroupController
                                                {
                                                    CreatedBy = username,
                                                    CreatedDateTime = DateTime.Now,
                                                    ModifiedBy = username,
                                                    ModifiedDateTime = DateTime.Now,
                                                    SiteLoginType = 1, // back
                                                    SysFeatureGroupGUID = featureGroup.SysFeatureGroupGUID,
                                                    SysControllerTableGUID = s.SysControllerTableGUID,
                                                    SysFeatureGroupControllerGUID = Guid.NewGuid(),
                                                    AccessRight = 5
                                                });
                                featureGroupControllers.AddRange(fgcMappings);
                            }
                            #endregion
                        }
                    }
                    #region SysFeatureGroupStructure
                    
                    var sysFeatureGroupStructures = GetSysFeatureGroupStructureFromMenu(menuStructures, new List<SysFeatureGroupStructure>(), featureGroups, 0, -1, username, notTranslated);

                    #endregion SysFeatureGroupStructure

                    // not translated label
                    if(notTranslated.Count > 0)
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine("Please add translation in SysMessage.json for: ");
                        foreach (var seg in notTranslated)
                        {
                            sb.AppendLine(seg);
                        }
                        ex.AddData(sb.ToString());
                        throw ex;
                    }

                    #region SysControllerTables for controller endpoint only
                    List<string> controllerEndpointOnly = new List<string>()
                    {
                        // ControllerNames
                        "K2", "Migration"
                    };
                    List<SysControllerTable> sysControllerTableEndpointOnly = GetSysControllerTableEndpointOnly(controllerTables, controllerEndpointOnly);
                    controllerTables.AddRange(sysControllerTableEndpointOnly);
                    #endregion

                    #region SysControllerEntityMapping
                    if(controllerEntityMappingViews != null && controllerEntityMappingViews.Count > 0)
                    {
                        // TODO: to be changed
                        controllerEntityMappingViews =
                            controllerEntityMappingViews.Where(w => !w.SysControllerTable_RouteAttribute
                                                                    .Contains("RelatedInfo", StringComparison.InvariantCultureIgnoreCase)).ToList();
                        foreach (var cemView in controllerEntityMappingViews)
                        {
                            var ct = controllerTables.Where(w => w.RouteAttribute.ToUpper() == cemView.SysControllerTable_RouteAttribute.ToUpper() &&
                                                                w.ControllerName.ToUpper() == cemView.SysControllerTable_ControllerName.ToUpper())
                                                    .FirstOrDefault();
                            //var featureType = featureGroupControllers
                            if(ct != null)
                            {
                                var cem = new SysControllerEntityMapping
                                {
                                    CreatedBy = username,
                                    CreatedDateTime = DateTime.Now,
                                    ModifiedBy = username,
                                    ModifiedDateTime = DateTime.Now,
                                    FeatureType = ct.RouteAttribute.Contains("RelatedInfo", StringComparison.InvariantCultureIgnoreCase) ?
                                    (int)SysFeatureType.RelatedInfo : (int)SysFeatureType.Primary,
                                    SysControllerTableGUID = ct.SysControllerTableGUID,
                                    ModelName = cemView.ModelName,
                                    SysControllerEntityMappingGUID = Guid.NewGuid()
                                };
                                controllerEntityMappings.Add(cem);
                            }
                            

                        }
                    }
                    #endregion SysControllerEntityMapping

                    var adminRoles = db.Set<SysRoleTable>().Where(item => item.DisplayName == TextConstants.ADMIN).ToList();

                    #region ADMIN SysControllerRole, SysFeatureGroupRole (includes OB)

                    var controllerRolesAdmin = GetSysControllerRolesForAdminRole(controllerTables, adminRoles, username);
                    controllerRoles.AddRange(controllerRolesAdmin);

                    var featureGroupRolesAdmin = GetSysFeatureGroupRolesForAdminRole(featureGroups, siteOBGroupIds, adminRoles, username);
                    featureGroupRoles.AddRange(featureGroupRolesAdmin);

                    #endregion ADMIN SysControllerRole, SysFeatureGroupRole

                    #region ADMIN SysEntityRole
                    var tableNames = controllerEntityMappings.Select(s => s.ModelName).Distinct();
                    var adminSysEntityRoles =
                        (from admin in adminRoles
                         from tableName in tableNames
                         select new SysEntityRole
                         {
                             CreatedBy = username,
                             CreatedDateTime = DateTime.Now,
                             ModifiedBy = username,
                             ModifiedDateTime = DateTime.Now,
                             ModelName = tableName,
                             RoleGUID = admin.Id,
                             AccessLevel = (int)AccessLevel.Company,
                             SysEntityRoleGUID = Guid.NewGuid()
                         });
                    entityRoles.AddRange(adminSysEntityRoles);
                    #endregion ADMIN SysEntityRole

                    #region non-ADMIN SysControllerRole, SysFeatureGroupRole
                    // re-map other roles
                    var otherRoles = db.Set<SysRoleTable>().Where(item => item.DisplayName != "ADMIN" && item.SiteLoginType == 1).ToList();
                    var newFeatureControllers =
                        (from fgc in featureGroupControllers
                         join ct in controllerTables
                         on fgc.SysControllerTableGUID equals ct.SysControllerTableGUID
                         join fg in featureGroups
                         on fgc.SysFeatureGroupGUID equals fg.SysFeatureGroupGUID
                         select new
                         {
                             ct.SysControllerTableGUID,
                             ct.ControllerName,
                             ct.RouteAttribute,
                             fg.GroupId
                         });
                    if(otherRoles.Count() > 0)
                    {
                        foreach (var role in otherRoles)
                        {
                            // non-ADMIN SysControllerRole
                            var notAdmincontrollerRoles =
                                (from controllerRole in db.Set<SysControllerRole>().Where(cr => cr.RoleGUID == role.Id)
                                 join controller in db.Set<SysControllerTable>()
                                 on controllerRole.SysControllerTableGUID equals controller.SysControllerTableGUID
                                 join fgc in db.Set<SysFeatureGroupController>()
                                 on controller.SysControllerTableGUID equals fgc.SysControllerTableGUID
                                 join fg in db.Set<SysFeatureGroup>()
                                 on fgc.SysFeatureGroupGUID equals fg.SysFeatureGroupGUID
                                 select new
                                 {
                                     RoleGUID = controllerRole.RoleGUID,
                                     AccessLevel = controllerRole.AccessLevel,
                                     ControllerName = controller.ControllerName,
                                     RouteAttribute = controller.RouteAttribute,
                                     GroupId = fg.GroupId
                                 }).AsNoTracking().ToList();
                            
                            var remapNotAdminControllerRole =
                                (from controllerRole in notAdmincontrollerRoles
                                 join controller in newFeatureControllers.Where(w => !w.ControllerName.StartsWith("Migration"))
                                 on new { controllerRole.ControllerName, controllerRole.RouteAttribute, controllerRole.GroupId }
                                 equals new { controller.ControllerName, controller.RouteAttribute, controller.GroupId }
                                 select new SysControllerRole
                                 {
                                     CreatedBy = username,
                                     CreatedDateTime = DateTime.Now,
                                     ModifiedBy = username,
                                     ModifiedDateTime = DateTime.Now,
                                     SysControllerRoleGUID = Guid.NewGuid(),
                                     SysControllerTableGUID = controller.SysControllerTableGUID,
                                     RoleGUID = controllerRole.RoleGUID,
                                     AccessLevel = controllerRole.AccessLevel,
                                 });
                            controllerRoles.AddRange(remapNotAdminControllerRole);

                            // non-ADMIN SysFeatureGroupRole
                            var notAdminFeatureGroupRole =
                                (from featureGroupRole in db.Set<SysFeatureGroupRole>().Where(fgr => fgr.RoleGUID == role.Id)
                                 join featureGroup in db.Set<SysFeatureGroup>()
                                 on featureGroupRole.SysFeatureGroupGUID equals featureGroup.SysFeatureGroupGUID
                                 select new
                                 {
                                     RoleGUID = featureGroupRole.RoleGUID,
                                     CompanyGUID = role.CompanyGUID,
                                     Action = featureGroupRole.Action,
                                     Read = featureGroupRole.Read,
                                     Update = featureGroupRole.Update,
                                     Create = featureGroupRole.Create,
                                     Delete = featureGroupRole.Delete,
                                     FeatureType = featureGroup.FeatureType,
                                     GroupId = featureGroup.GroupId
                                 }).ToList();
                            var remapNotAdminFeatureGroupRole =
                                (from featureGroupRole in notAdminFeatureGroupRole
                                 join featureGroup in featureGroups.Where(w => !siteOBGroupIds.Contains(w.GroupId))
                                 on new { featureGroupRole.GroupId, featureGroupRole.FeatureType }
                                 equals new { featureGroup.GroupId, featureGroup.FeatureType }
                                 select new SysFeatureGroupRole
                                 {
                                     CreatedBy = username,
                                     CreatedDateTime = DateTime.Now,
                                     ModifiedBy = username,
                                     ModifiedDateTime = DateTime.Now,
                                     SysFeatureGroupRoleGUID = Guid.NewGuid(),
                                     SysFeatureGroupGUID = featureGroup.SysFeatureGroupGUID,
                                     RoleGUID = featureGroupRole.RoleGUID,
                                     Action = featureGroupRole.Action,
                                     Read = featureGroupRole.Read,
                                     Update = featureGroupRole.Update,
                                     Create = featureGroupRole.Create,
                                     Delete = featureGroupRole.Delete,
                                 });
                            featureGroupRoles.AddRange(remapNotAdminFeatureGroupRole);
                        }
                    }
                    #endregion non-ADMIN SysControllerRole, SysFeatureGroupRole

                    #region non-ADMIN SysEntityRole (no remap)
                    var otherRoleIds = otherRoles.Select(s => s.Id);
                    var notAdminEntityRoles = db.Set<SysEntityRole>().Where(w => otherRoleIds.Contains(w.RoleGUID)).ToList();
                    entityRoles.AddRange(notAdminEntityRoles);
                    #endregion

                    #region bulk operations
                    var oldSysFeatureTable = db.Set<SysFeatureTable>().ToList();
                    var oldSysControllerTable = db.Set<SysControllerTable>().ToList();
                    var oldSysFeatureGroup = db.Set<SysFeatureGroup>().ToList();
                    var oldSysFeatureGroupMapping = db.Set<SysFeatureGroupMapping>().ToList();
                    var oldSysFeatureGroupRole = db.Set<SysFeatureGroupRole>().ToList();
                    var oldSysFeatureGroupController = db.Set<SysFeatureGroupController>().ToList();
                    var oldSysControllerRole = db.Set<SysControllerRole>().ToList();
                    var oldSysFeatureGroupStructure = db.Set<SysFeatureGroupStructure>().ToList();
                    var oldSysControllerEntityMapping = db.Set<SysControllerEntityMapping>().ToList();
                    var oldSysEntityRole = db.Set<SysEntityRole>().ToList();

                    // use UseTempDB when CREATE TABLE permission denied
                    var bulkConfig = new BulkConfig { UseTempDB = true };

                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        // SysEntityRole
                        if (oldSysEntityRole.Count() > 0)
                            db.BulkDelete(oldSysEntityRole);
                        if (entityRoles.Count() > 0)
                            db.BulkInsert(entityRoles);

                        // SysControllerEntityMapping
                        if (oldSysControllerEntityMapping.Count() > 0)
                            db.BulkDelete(oldSysControllerEntityMapping);
                        if (controllerEntityMappings.Count() > 0)
                            db.BulkInsert(controllerEntityMappings);

                        // SysFeatureGroupStructure
                        if (oldSysFeatureGroupStructure.Count() > 0)
                            db.BulkDelete(oldSysFeatureGroupStructure);
                        if (sysFeatureGroupStructures.Count() > 0)
                            db.BulkInsert(sysFeatureGroupStructures);

                        // SysFeatureGroupMapping
                        if (oldSysFeatureGroupMapping.Count() > 0) 
                            db.BulkDelete(oldSysFeatureGroupMapping);
                        if(featureGroupMappings.Count() > 0)
                            db.BulkInsert(featureGroupMappings);
                        // SysFeatureGroupRole
                        if (oldSysFeatureGroupRole.Count() > 0)
                            db.BulkDelete(oldSysFeatureGroupRole);
                        if (featureGroupRoles.Count() > 0)
                            db.BulkInsert(featureGroupRoles);
                        // SysControllerRole
                        if (oldSysControllerRole.Count() > 0)
                            db.BulkDelete(oldSysControllerRole);
                        if (controllerRoles.Count() > 0)
                            db.BulkInsert(controllerRoles);
                        // SysFeatureGroupController
                        if (oldSysFeatureGroupController.Count() > 0)
                            db.BulkDelete(oldSysFeatureGroupController);
                        if (featureGroupControllers.Count() > 0)
                            db.BulkInsert(featureGroupControllers);
                        // SysFeatureGroup
                        if (oldSysFeatureGroup.Count() > 0)
                            db.BulkDelete(oldSysFeatureGroup);
                        if (featureGroups.Count() > 0)
                            db.BulkInsert(featureGroups);
                        // SysFeatureTable
                        if (oldSysFeatureTable.Count() > 0)
                            db.BulkDelete(oldSysFeatureTable);
                        if (featureTables.Count() > 0)
                            db.BulkInsert(featureTables);
                        // SysControllerTable
                        if (oldSysControllerTable.Count() > 0)
                            db.BulkDelete(oldSysControllerTable);
                        if (controllerTables.Count() > 0)
                            db.BulkInsert(controllerTables);

                        UnitOfWork.Commit(transaction);
                    }
                    #endregion bulk operations
                    return true;
                }
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<SysControllerRole> GetSysControllerRolesForAdminRole(List<SysControllerTable> controllerTables, 
                                                                        List<SysRoleTable> adminRoles, string username)
        {
            try
            {
                List<SysControllerRole> result = new List<SysControllerRole>();

                var adminRolesBack = adminRoles.Where(item => item.SiteLoginType == SiteLoginValue.Back).ToList();
                var adminRolesOB = adminRoles.Where(item => item.SiteLoginType == SiteLoginValue.OB).ToList();

                var controllerRolesOB =
                        (from ct in controllerTables.Where(w => w.ControllerName.StartsWith("Migration") ||
                                                                w.ControllerName == "SysUser" ||
                                                                w.ControllerName == "SysAccessRight")
                         from role in adminRolesOB
                         select new SysControllerRole
                         {
                             CreatedBy = username,
                             CreatedDateTime = DateTime.Now,
                             ModifiedBy = username,
                             ModifiedDateTime = DateTime.Now,
                             SysControllerRoleGUID = Guid.NewGuid(),
                             SysControllerTableGUID = ct.SysControllerTableGUID,
                             RoleGUID = role.Id,
                             AccessLevel = 4,

                         }).ToList();
                result.AddRange(controllerRolesOB);

                var controllerRolesBack =
                        (from ct in controllerTables.Where(w => !w.ControllerName.StartsWith("Migration"))
                         from role in adminRolesBack
                         select new SysControllerRole
                         {
                             CreatedBy = username,
                             CreatedDateTime = DateTime.Now,
                             ModifiedBy = username,
                             ModifiedDateTime = DateTime.Now,
                             SysControllerRoleGUID = Guid.NewGuid(),
                             SysControllerTableGUID = ct.SysControllerTableGUID,
                             RoleGUID = role.Id,
                             AccessLevel = 4,

                         }).ToList();
                result.AddRange(controllerRolesBack);
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<SysFeatureGroupRole> GetSysFeatureGroupRolesForAdminRole(List<SysFeatureGroup> featureGroups, 
                                                                                List<string> siteOBGroupIds,
                                                                                List<SysRoleTable> adminRoles, string username)
        {
            try
            {
                List<SysFeatureGroupRole> result = new List<SysFeatureGroupRole>();

                var adminRolesBack = adminRoles.Where(item => item.SiteLoginType == SiteLoginValue.Back).ToList();
                var adminRolesOB = adminRoles.Where(item => item.SiteLoginType == SiteLoginValue.OB).ToList();

                var fgrMappingsOB =
                    (from map in featureGroups.Where(w => siteOBGroupIds.Contains(w.GroupId) ||
                                                            w.GroupId == "SYSUSERTABLE" ||
                                                            w.GroupId == "SYSROLETABLE")
                     from role in adminRolesOB
                     select new SysFeatureGroupRole
                     {
                         CreatedBy = username,
                         CreatedDateTime = DateTime.Now,
                         ModifiedBy = username,
                         ModifiedDateTime = DateTime.Now,
                         SysFeatureGroupRoleGUID = Guid.NewGuid(),
                         SysFeatureGroupGUID = map.SysFeatureGroupGUID,
                         RoleGUID = role.Id,
                         Action = SysAccessLevelHelper.GetCRUDActionByFeatureType(CRUDActionConst.ACTION, (int)AccessLevel.Company, map.FeatureType),
                         Read = SysAccessLevelHelper.GetCRUDActionByFeatureType(CRUDActionConst.READ, (int)AccessLevel.Company, map.FeatureType),
                         Update = SysAccessLevelHelper.GetCRUDActionByFeatureType(CRUDActionConst.UPDATE, (int)AccessLevel.Company, map.FeatureType),
                         Create = SysAccessLevelHelper.GetCRUDActionByFeatureType(CRUDActionConst.CREATE, (int)AccessLevel.Company, map.FeatureType),
                         Delete = SysAccessLevelHelper.GetCRUDActionByFeatureType(CRUDActionConst.DELETE, (int)AccessLevel.Company, map.FeatureType)
                     });
                result.AddRange(fgrMappingsOB);

                var fgrMappingsBack =
                    (from map in featureGroups.Where(w => !siteOBGroupIds.Contains(w.GroupId))
                     from role in adminRolesBack
                     select new SysFeatureGroupRole
                     {
                         CreatedBy = username,
                         CreatedDateTime = DateTime.Now,
                         ModifiedBy = username,
                         ModifiedDateTime = DateTime.Now,
                         SysFeatureGroupRoleGUID = Guid.NewGuid(),
                         SysFeatureGroupGUID = map.SysFeatureGroupGUID,
                         RoleGUID = role.Id,
                         Action = SysAccessLevelHelper.GetCRUDActionByFeatureType(CRUDActionConst.ACTION, (int)AccessLevel.Company, map.FeatureType),
                         Read = SysAccessLevelHelper.GetCRUDActionByFeatureType(CRUDActionConst.READ, (int)AccessLevel.Company, map.FeatureType),
                         Update = SysAccessLevelHelper.GetCRUDActionByFeatureType(CRUDActionConst.UPDATE, (int)AccessLevel.Company, map.FeatureType),
                         Create = SysAccessLevelHelper.GetCRUDActionByFeatureType(CRUDActionConst.CREATE, (int)AccessLevel.Company, map.FeatureType),
                         Delete = SysAccessLevelHelper.GetCRUDActionByFeatureType(CRUDActionConst.DELETE, (int)AccessLevel.Company, map.FeatureType)
                     });
                result.AddRange(fgrMappingsBack);
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private int GetFeatureTypeValue(string input)
        {
            try
            {
                switch(input)
                {
                    case FeatureType.PRIMARY:
                        return (int)SysFeatureType.Primary;
                    case FeatureType.RELATEDINFO:
                        return (int)SysFeatureType.RelatedInfo;
                    case FeatureType.FUNCTION:
                        return (int)SysFeatureType.Function;
                    case FeatureType.REPORT:
                        return (int)SysFeatureType.Report;
                    case FeatureType.WORKFLOW:
                        return (int)SysFeatureType.Workflow;
                    case FeatureType.WORKFLOW_ACTIONHISTORY:
                        return (int)SysFeatureType.WorkflowActionHistory;
                    default:
                        return -1;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AccessRightBU GetAccessRightBUByUserSiteCompany(string userId, int site, string companyGUID)
        {
            try
            {
                ISysAccessLevelDataService accessLevelService = new SysAccessLevelService(db);
                AccessRightBU result = new AccessRightBU();
                result.AccessRight = GetAccessRightModelByUserSiteCompany(userId, site, companyGUID);
                var parentChildBU = accessLevelService.GetParentChildBusinessUnitsByUserName(GetUserName(), companyGUID);
                result.BusinessUnitGUID = parentChildBU.Last().BusinessUnitGUID.GuidNullToString();
                result.BusinessUnitId = parentChildBU.Last().BusinessUnitId;
                result.ParentChildBU = parentChildBU.Select(s => s.BusinessUnitGUID.GuidNullToString());
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private List<SysControllerTable> GetSysControllerTableFromMapping(string controllerName, string groupId, string featureType, List<SysFeatureTableView> features, bool isDevelopment)
        {
            try
            {
                List<SysControllerTable> result = new List<SysControllerTable>();
                if(features.Count() > 0)
                {
                    string maxPath = features.Where(w => w.Path.Length == features.Max(m => m.Path.Length)).Select(s => s.Path).FirstOrDefault();
                    var controllerRoutes = GetControllerRouteAttributeFromAppRoute(maxPath, groupId, isDevelopment);
                    result = controllerRoutes.Select(s => new SysControllerTable
                    {
                        ControllerName = controllerName,
                        RouteAttribute = s,
                        SysControllerTableGUID = Guid.NewGuid(),
                        CreatedBy = "System",
                        ModifiedBy = "System",
                        CreatedDateTime = DateTime.Now,
                        ModifiedDateTime = DateTime.Now,
                    }).ToList();
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<string> GetControllerRouteAttributeFromAppRoute(string appRoute, string groupId, bool isDevelopment)
        {
            try
            {
                List<string> result = new List<string> { "Get", "Validate", "Create", "Update", "Delete" };

                StringBuilder sb = new StringBuilder();
                BuildRouteAttributeView brv = new BuildRouteAttributeView { StringBuilder = sb, GroupIdStartIndex = 0 };
                brv = GetRoutePrefix(brv, appRoute, groupId, isDevelopment);
                

                bool isFunction = appRoute.Contains("/function/");
                bool isRelated = appRoute.Contains("/relatedinfo/");
                bool isReport = appRoute.Contains("/report/");
                bool isWorkflow = appRoute.Contains("/workflow/");
                if (!(isFunction || isReport || isRelated || isWorkflow))
                {
                    string routeAttr = sb.ToString();
                    result = result.Select(s => routeAttr + s).ToList();
                    return result;
                }
                else
                {
                    if (isRelated)
                    {
                        var routeSplits = appRoute.Split("/relatedinfo/");
                        // more than 1 /relatedinfo/
                        if(routeSplits.Length > 2)
                        {
                            for(int i=1; i<routeSplits.Length; i++)
                            {
                                brv = GetRouteAttributeRelatedInfo(brv, routeSplits[i], groupId, isDevelopment);
                            }
                            if(isFunction)
                            {
                                brv = GetRouteAttributeFunctionOrReport(brv, routeSplits.Last(), groupId, "Function", isDevelopment);
                            }
                            else if(isReport)
                            {
                                brv = GetRouteAttributeFunctionOrReport(brv, routeSplits.Last(), groupId, "Report", isDevelopment);
                            }
                            else if(isWorkflow)
                            {
                                brv = GetRouteAttributeWorkflow(brv, routeSplits.Last(), groupId);
                            }
                        }
                        else
                        {
                            brv = GetRouteAttributeRelatedInfo(brv, routeSplits.Last(), groupId, isDevelopment);
                            if (isFunction)
                            {
                                brv = GetRouteAttributeFunctionOrReport(brv, routeSplits.Last(), groupId, "Function", isDevelopment);
                            }
                            else if (isReport)
                            {
                                brv = GetRouteAttributeFunctionOrReport(brv, routeSplits.Last(), groupId, "Report", isDevelopment);
                            }
                            else if (isWorkflow)
                            {
                                brv = GetRouteAttributeWorkflow(brv, routeSplits.Last(), groupId);
                            }
                        }
                    }
                    else
                    {
                        if(isFunction)
                        {
                            brv = GetRouteAttributeFunctionOrReport(brv, appRoute, groupId, "Function", isDevelopment);
                        }
                        else if (isReport)
                        {
                            brv = GetRouteAttributeFunctionOrReport(brv, appRoute, groupId, "Report", isDevelopment);
                        }
                        else if (isWorkflow)
                        {
                            brv = GetRouteAttributeWorkflow(brv, appRoute, groupId);
                        }
                    }
                    string routeAttr = brv.StringBuilder.ToString();
                    if(!(isFunction || isReport || isWorkflow))
                    {
                        result = result.Select(s => routeAttr + s).ToList();
                        return result;
                    }
                    else
                    {
                        return new List<string> { routeAttr };

                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private BuildRouteAttributeView GetRouteAttributeRelatedInfo(BuildRouteAttributeView brv, string routeSegment, string groupId, bool isDevelopment)
        {
            try
            {
                brv.StringBuilder = brv.StringBuilder.Append("RelatedInfo/");
                if(routeSegment.Contains("/function/"))
                {
                    routeSegment = routeSegment.Split("/function/")[0];
                }
                if(routeSegment.Contains("/report/"))
                {
                    routeSegment = routeSegment.Split("/report/")[0];
                }
                if(routeSegment.Contains("/workflow/"))
                {
                    routeSegment = routeSegment.Split("/workflow/")[0];
                }

                var related1Splits = routeSegment.Split("/");

                string groupIdCheck = groupId.SnakeCaseToPascal().Replace("-child", "-Child").Substring(brv.GroupIdStartIndex);
                bool containsChild = routeSegment.Contains("-child") && groupIdCheck.Contains("-Child");
                var relatedSplitsFilter = containsChild ?
                        related1Splits.Where(w => !string.IsNullOrWhiteSpace(w) && !w.Contains(":") &&
                                            !(w == "function" || w == "report" || w == "workflow")) :
                        related1Splits.Where(w => !string.IsNullOrWhiteSpace(w) && !w.Contains(":") && !w.Contains("-child") &&
                                            !(w == "function" || w == "report" || w == "workflow"));

                var pageSegmentIndex = relatedSplitsFilter
                                        .Select(s => new
                                        {
                                            Index = Array.IndexOf(related1Splits, s),
                                            Value = s
                                        })
                                        .Max(m => m.Index);
                var pageSegment = related1Splits[pageSegmentIndex];
                brv = GetPageNamePascalFromRoute(brv, pageSegment, groupId, isDevelopment);
                brv.StringBuilder = brv.StringBuilder.Append("/");
                return brv;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private BuildRouteAttributeView GetRouteAttributeFunctionOrReport(BuildRouteAttributeView brv, string routeSegment, string groupId, string type, bool isDevelopment)
        {
            try
            {
                string functionSegment = routeSegment.Split("/" + type.ToLower() + "/").Last();
                functionSegment = functionSegment.TrimEnd('/');
                brv.StringBuilder = brv.StringBuilder.Append(type + "/");
                brv = GetPageNamePascalFromRoute(brv, functionSegment, groupId, isDevelopment);
                return brv;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private BuildRouteAttributeView GetRouteAttributeWorkflow(BuildRouteAttributeView brv, string routeSegment, string groupId)
        {
            try
            {
                string workflowSegment = routeSegment.Split("/workflow/").Last();
                brv.StringBuilder = brv.StringBuilder.Append("Workflow/");
                if(workflowSegment.ToLower().StartsWith("actionhistory"))
                {
                    brv.StringBuilder = brv.StringBuilder.Append("ActionHistory/Get");
                }
                else  if(workflowSegment.ToLower().StartsWith("action"))
                {
                    brv.StringBuilder = brv.StringBuilder.Append("Action");
                }
                else  if( workflowSegment.ToLower().StartsWith("startworkflow"))
                {
                    brv.StringBuilder = brv.StringBuilder.Append("StartWorkflow");
                }
                return brv;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private BuildRouteAttributeView GetPageNamePascalFromRoute(BuildRouteAttributeView brv, string pageSegment, string groupId, bool isDevelopment)
        {
            try
            {
                if(isDevelopment) SmartAppUtil.LogMessage(Serilog.Events.LogEventLevel.Debug, "PageSegment: " + pageSegment + ", GroupId: " + groupId + "\n");
                
                string groupPascal = groupId.SnakeCaseToPascal().Replace("-child", "-Child");
                int pageNameIndex = groupPascal.IndexOf(pageSegment, StringComparison.InvariantCultureIgnoreCase);
                // validate ?
                string pascalGroupIdSegment = groupPascal.Substring(pageNameIndex, pageSegment.Length);
                brv.StringBuilder = brv.StringBuilder.Append(pascalGroupIdSegment);
                brv.GroupIdStartIndex = pageNameIndex + pascalGroupIdSegment.Length;
                return brv;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private BuildRouteAttributeView GetRoutePrefix(BuildRouteAttributeView brv, string appRoute, string groupId, bool isDevelopment)
        {
            try
            {
                var mainPage = appRoute.Contains("/relatedinfo/") ?
                                    appRoute.Split("/relatedinfo/").FirstOrDefault() :
                                    appRoute.Contains("/function/") ?
                                    appRoute.Split("/function/").FirstOrDefault() :
                                    appRoute.Contains("/report/") ?
                                    appRoute.Split("/report/").FirstOrDefault() : 
                                    appRoute.Contains("/workflow/") ?
                                    appRoute.Split("/workflow/").FirstOrDefault() : appRoute;
                bool containsChild = mainPage.Contains("-child") && groupId.Contains("-CHILD");
                var pageRoute = containsChild ? mainPage : mainPage.Split("/:id").FirstOrDefault();
                
                var mainPageSegments = pageRoute.Split("/").Where(w => !w.Contains(":") && !string.IsNullOrWhiteSpace(w));
                var prefix = mainPageSegments;
                if(containsChild)
                {
                    prefix = mainPageSegments.Take(mainPageSegments.Count() - 2).Append(mainPageSegments.Last());
                }
                else
                {
                    prefix = mainPageSegments.Take(mainPageSegments.Count() - 1);
                }
                
                string groupPascal = groupId.SnakeCaseToPascal();
                int prefixIndex = -1, childIndex = -1;
                if(containsChild)
                {
                    prefixIndex = groupPascal.IndexOf(string.Join("", prefix.Take(prefix.Count() - 1)), StringComparison.InvariantCultureIgnoreCase);
                    childIndex = groupPascal.IndexOf(string.Join("", prefix.Last()), StringComparison.InvariantCultureIgnoreCase);
                }
                else
                {
                    prefixIndex = groupPascal.IndexOf(string.Join("", prefix), StringComparison.InvariantCultureIgnoreCase);
                }
                
                if(prefixIndex == 0)
                {
                    if(containsChild)
                    {
                        foreach (var segment in prefix.Take(prefix.Count() -1))
                        {
                            if (!string.IsNullOrWhiteSpace(segment))
                            {
                                brv = GetPageNamePascalFromRoute(brv, segment, groupId, isDevelopment);
                                brv.StringBuilder = brv.StringBuilder.Append("/");
                            }
                        }
                        if(childIndex != -1)
                        {
                            string childSegment = prefix.Last();
                            if(!string.IsNullOrWhiteSpace(childSegment))
                            {
                                brv = GetPageNamePascalFromRoute(brv, childSegment, groupId, isDevelopment);
                                brv.StringBuilder = brv.StringBuilder.Append("/");
                            }
                        }
                    }
                    else
                    {
                        foreach (var segment in prefix)
                        {
                            if (!string.IsNullOrWhiteSpace(segment))
                            {
                                brv = GetPageNamePascalFromRoute(brv, segment, groupId, isDevelopment);
                                brv.StringBuilder = brv.StringBuilder.Append("/");
                            }
                        }
                    }
                }
                else
                {
                    if (childIndex != -1)
                    {
                        string childSegment = prefix.Last();
                        if (!string.IsNullOrWhiteSpace(childSegment))
                        {
                            brv = GetPageNamePascalFromRoute(brv, childSegment, groupId, isDevelopment);
                            brv.StringBuilder = brv.StringBuilder.Append("/");
                        }
                    }
                }
                return brv;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private List<SysControllerTable> GetSysControllerTableEndpointOnly(List<SysControllerTable> existing, List<string> controllerNames)
        {
            try
            {
                List<SysControllerTable> result = new List<SysControllerTable>();
                var controllerToAdd = controllerNames.Where(w => !existing.Any(a => a.ControllerName == w));
                if(controllerToAdd.Count() != 0)
                {
                    List<string> route = new List<string> { "Get", "Validate", "Create", "Update", "Delete", "Function" };
                    foreach (var c in controllerToAdd)
                    {
                        List<SysControllerTable> list = route.Select(s => new SysControllerTable
                        {
                            ControllerName = c,
                            RouteAttribute = s,
                            SysControllerTableGUID = Guid.NewGuid(),
                            CreatedBy = "System",
                            ModifiedBy = "System",
                            CreatedDateTime = DateTime.Now,
                            ModifiedDateTime = DateTime.Now,
                        }).ToList();
                        result.AddRange(list);
                    }
                }
                return result;
                
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private string GetPathMenu(List<SysFeatureTableView> features, string groupId, List<string> notTranslateds)
        {
            try
            {
                string groupPascal = groupId.SnakeCaseToPascal().Replace("-child", "-Child");
                string minPath = features.Where(w => w.Path.Length == features.Min(m => m.Path.Length)).Select(s => s.Path).FirstOrDefault();
                var splits = minPath.Split("/");
                var segments = splits.Where(w => !w.Contains(":") && !string.IsNullOrWhiteSpace(w));
                if(segments != null && segments.Count() > 0)
                {
                    List<string> translatedSegments = new List<string>();
                    List<string> notTranslatedSegs = new List<string>();
                    foreach (var segment in segments)
                    {
                        int pageNameIndex = groupPascal.IndexOf(segment, StringComparison.InvariantCultureIgnoreCase);
                        if(pageNameIndex > -1)
                        {
                            string pascalGroupIdSegment = groupPascal.Substring(pageNameIndex, segment.Length);
                            string labelSegment = pascalGroupIdSegment.PascalToSnakeCase();
                            string translated = SystemStaticData.GetTranslatedMessage($"{MessageGroup.LABEL}.{labelSegment}");
                            if(translated == $"{MessageGroup.LABEL}.{labelSegment}")
                            {
                                translated = SystemStaticData.GetTranslatedMessage($"{MessageGroup.ROLE_ROUTE_SEGMENT}.{labelSegment}");

                                if(translated == $"{MessageGroup.ROLE_ROUTE_SEGMENT}.{labelSegment}")
                                {
                                    notTranslatedSegs.Add(translated);
                                }
                            }
                            //using(StreamWriter file = new StreamWriter("labelsegment.txt", append: true))
                            //{
                            //    file.WriteLine("{\"Key\": \"" + labelSegment + "\", \"Value\": \"" + translated + "\"},");
                            //}
                            
                            translatedSegments.Add(translated);
                        }
                        else
                        {
                            //if (segment != "relatedinfo" && segment != "function" && segment != "report" && segment != "workflow")
                            //{
                            //using (StreamWriter file = new StreamWriter("nonexistsegment.txt", append: true))
                            //{
                            //    file.WriteLine("{\"Key\": \"" + segment.ToUpper() + "\", \"Value\": \"" + segment.ToUpper() + "\"},");
                            //}
                            string labelSegment = segment.ToUpper();
                            string translated = SystemStaticData.GetTranslatedMessage($"{MessageGroup.LABEL}.{labelSegment}");
                            if (translated == $"{MessageGroup.LABEL}.{labelSegment}")
                            {
                                translated = SystemStaticData.GetTranslatedMessage($"{MessageGroup.ROLE_ROUTE_SEGMENT}.{labelSegment}");

                                if (translated == $"{MessageGroup.ROLE_ROUTE_SEGMENT}.{labelSegment}")
                                {
                                    notTranslatedSegs.Add(translated);
                                }
                            }
                            translatedSegments.Add(translated);
                            //}
                            //else
                            //{
                                //string translated = SystemStaticData.GetTranslatedMessage(segment.ToUpperInvariant());
                                //translatedSegments.Add(translated);
                            //}
                        }
                        
                    };
                    StringBuilder sb = new StringBuilder();
                    sb.AppendJoin(" > ", translatedSegments);
                    string joinedPath = sb.ToString();
                    if(notTranslatedSegs.Count > 0)
                    {
                        notTranslateds.Add(string.Concat(string.Join(", ", notTranslatedSegs), " (", joinedPath, ")"));
                    }
                    return joinedPath;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private List<SysFeatureGroupMenuMapping> GetSysFeatureGroupMenuMappings(List<SysFeatureGroupMenuMapping> featureGroupMapping, List<SysFeatureGroupMenuMapping> result)
        {
            try
            {
                if (result == null) result = new List<SysFeatureGroupMenuMapping>();

                if(featureGroupMapping != null && featureGroupMapping.Count > 0)
                {
                    foreach (var item in featureGroupMapping)
                    {
                        if(!string.IsNullOrWhiteSpace(item.Label) && !string.IsNullOrWhiteSpace(item.GroupId))
                        {
                            result.Add(new SysFeatureGroupMenuMapping { GroupId = item.GroupId, Label = item.Label });
                        }

                        if(item.Items != null && item.Items.Count > 0)
                        {
                            GetSysFeatureGroupMenuMappings(item.Items, result);
                        }
                    }
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private List<SysFeatureGroupStructure> GetSysFeatureGroupStructureFromMenu(List<SysFeatureGroupMenuMapping> menuStructures,
                                                                                    List<SysFeatureGroupStructure> result,
                                                                                    IEnumerable<SysFeatureGroup> source, 
                                                                                    int count, int parentNodeId, string username,
                                                                                    List<string> notTranslated)
        {
            try
            {
                int site = SiteLoginValue.Back;
                if (result == null) result = new List<SysFeatureGroupStructure>();

                if (menuStructures != null && menuStructures.Count > 0)
                {
                    foreach (var item in menuStructures)
                    {
                        #region current node
                        var currentSysFeatureGroup = item.GroupId != null ? source.Where(w => w.GroupId == item.GroupId).FirstOrDefault() : null;
                        int featureType = currentSysFeatureGroup == null ? (int)SysFeatureType.Primary : currentSysFeatureGroup.FeatureType;
                        SysFeatureGroupStructure current = new SysFeatureGroupStructure
                        {
                            NodeId = count,
                            NodeParentId = parentNodeId,
                            SiteLoginType = site,
                            NodeLabel = !string.IsNullOrWhiteSpace(item.Label) ? SystemStaticData.GetTranslatedMessage(item.Label) : item.Label,
                            SysFeatureGroupGUID = currentSysFeatureGroup != null ?
                                (Guid?)currentSysFeatureGroup.SysFeatureGroupGUID : null,
                            FeatureType = featureType,
                            CreatedBy = username,
                            ModifiedBy = username,
                            CreatedDateTime = DateTime.Now,
                            ModifiedDateTime = DateTime.Now
                        };
                        result.Add(current);
                        count = result.Count;
                        #endregion

                        // not translated
                        if (item.Label == current.NodeLabel)
                        {
                            notTranslated.Add(item.Label);
                        }

                        #region nested SysFeatureGroup (HasSysFeatureGroup)
                        if(currentSysFeatureGroup != null)
                        {
                            var sourceChildren = source.Where(w => w.PathMenu.StartsWith(currentSysFeatureGroup.PathMenu));
                            result = GetNestedSysFeatureGroupStructure(current, sourceChildren, result,
                                                                        count, currentSysFeatureGroup.PathMenu, current.NodeId, username);
                            count = result.Count;
                        }
                        #endregion

                        #region nested menu structure (has Items)
                        if (item.Items != null && item.Items.Count > 0)
                        {
                            result = GetSysFeatureGroupStructureFromMenu(item.Items, result, source, count, current.NodeId, username, notTranslated);
                            count = result.Count;
                        }
                        #endregion
                    }
                }
                return result;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private List<SysFeatureGroupStructure> GetNestedSysFeatureGroupStructure(SysFeatureGroupStructure parent,
                                                                            IEnumerable<SysFeatureGroup> sourceChildren, 
                                                                            List<SysFeatureGroupStructure> result,
                                                                            int count, string parentPathMenu, int parentNodeId, string username)
        {
            try
            {
                int site = SiteLoginValue.Back;
                if (parent != null && sourceChildren != null && sourceChildren.Count() > 0)
                {
                    var parentSplit = parentPathMenu.Split(">").Where(w => !string.IsNullOrWhiteSpace(w)).Select(s => s.Trim()).ToArray();
                    #region group routes next token
                    var sourceChildrenSplits =
                        sourceChildren.Where(w => w.PathMenu.Trim() != parentPathMenu.Trim() && w.PathMenu.Trim().StartsWith(parentPathMenu)).Select(s => new
                        {
                            SysFeatureGroup = s,
                            PathMenuSplits = s.PathMenu.Split(">").Where(w => !string.IsNullOrWhiteSpace(w)).Select(s => s.Trim()).ToArray(),
                            NextToken = s.PathMenu.Split(">").Where(w => !string.IsNullOrWhiteSpace(w)).Select(s => s.Trim()).Count() > parentSplit.Length ?
                                        s.PathMenu.Split(">").Where(w => !string.IsNullOrWhiteSpace(w)).Select(s => s.Trim()).ElementAt(parentSplit.Length) : null
                        })
                        .Where(w => !string.IsNullOrWhiteSpace(w.NextToken) && w.PathMenuSplits[parentSplit.Length-1] == parentSplit.Last())
                        .GroupBy(g => g.NextToken).Select(s => new { NextToken = s.Key, Group = s });
                    #endregion
                    if (sourceChildrenSplits.Count() > 0)
                    {
                        #region loop grouped next token
                        foreach (var group in sourceChildrenSplits)
                        {
                            bool notLine_ChildNextToken = group.NextToken == RoleRouteConst.Function || group.NextToken == RoleRouteConst.Report ||
                                group.NextToken == RoleRouteConst.RelatedInfo || group.NextToken == RoleRouteConst.Workflow;
                            string nodeLabel;
                            if(notLine_ChildNextToken)
                            {
                                #region create group node for relatedinfo, function, workflow, report
                                nodeLabel = string.Concat(parentSplit.Last(), " - ", group.NextToken);
                                SysFeatureGroupStructure groupNode = new SysFeatureGroupStructure
                                {
                                    NodeId = count,
                                    NodeParentId = parentNodeId,
                                    SiteLoginType = site,
                                    NodeLabel = nodeLabel,
                                    SysFeatureGroupGUID = null,
                                    FeatureType = GetSysFeatureTypeValueFromGroupSegment(group.NextToken),
                                    CreatedBy = username,
                                    ModifiedBy = username,
                                    CreatedDateTime = DateTime.Now,
                                    ModifiedDateTime = DateTime.Now
                                };
                                result.Add(groupNode);
                                count = result.Count;
                                #endregion
                                #region create page nodes
                                // group page nodes (in each group type)
                                int nextPageIdx = parentSplit.Length + 1;
                                var nextPageGroup = group.Group.Where(w => w.PathMenuSplits.Length > nextPageIdx)
                                                    .Select(s => new
                                                    {
                                                        NextToken = s.PathMenuSplits.ElementAt(nextPageIdx),
                                                        SysFeatureGroup = s.SysFeatureGroup,
                                                        PathMenuSplits = s.PathMenuSplits
                                                    }).GroupBy(g => g.NextToken).Select(s => new { NextToken = s.Key, Group = s });
                                if(nextPageGroup.Count() > 0)
                                {
                                    foreach (var nextPage in nextPageGroup)
                                    {
                                        // find min path length in each group 
                                        var nextPageMinLength = nextPage.Group.Min(m => m.PathMenuSplits.Length);
                                        var nextPageMin = nextPage.Group.Where(w => w.PathMenuSplits.Length == nextPageMinLength);
                                        
                                        if(nextPageMin.Count() > 0)
                                        {
                                            // there should be only 1 page min length
                                            foreach (var page in nextPageMin)
                                            {
                                                nodeLabel = page.NextToken;
                                                SysFeatureGroupStructure pageNode = new SysFeatureGroupStructure
                                                {
                                                    NodeId = count++,
                                                    NodeParentId = groupNode.NodeId,
                                                    SiteLoginType = site,
                                                    NodeLabel = nodeLabel,
                                                    SysFeatureGroupGUID = page.SysFeatureGroup.SysFeatureGroupGUID,
                                                    FeatureType = page.SysFeatureGroup.FeatureType,
                                                    CreatedBy = username,
                                                    ModifiedBy = username,
                                                    CreatedDateTime = DateTime.Now,
                                                    ModifiedDateTime = DateTime.Now
                                                };
                                                result.Add(pageNode);
                                                count = result.Count;
                                                // recursive next page level
                                                var nextSourceChildren = nextPage.Group.Where(w => w.SysFeatureGroup.PathMenu.Trim() != page.SysFeatureGroup.PathMenu.Trim() &&
                                                                                                w.SysFeatureGroup.PathMenu.Trim().StartsWith(page.SysFeatureGroup.PathMenu.Trim()))
                                                                                        .Select(s => s.SysFeatureGroup);
                                                GetNestedSysFeatureGroupStructure(pageNode, nextSourceChildren, result, count, 
                                                                                    page.SysFeatureGroup.PathMenu, pageNode.NodeId, username);
                                                count = result.Count;
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                // group page -child by group type (relatedinfo, function, report, workflow)
                                int nextPageIdx = parentSplit.Length + 1;
                                var line_ChildNextToken = group.Group.Where(w => w.PathMenuSplits.Length > nextPageIdx)
                                                        .Select(s => new 
                                                        { 
                                                            NextToken = s.PathMenuSplits.ElementAt(nextPageIdx), 
                                                            SysFeatureGroup = s.SysFeatureGroup, 
                                                            PathMenuSplits = s.PathMenuSplits 
                                                        }).GroupBy(g => g.NextToken).Select(s => new { NextToken = s.Key, Group = s });
                                if(line_ChildNextToken.Count() > 0)
                                {
                                    foreach (var line_Child in line_ChildNextToken)
                                    {
                                        #region create group node for -child relatedinfo, function, report, workflow

                                        nodeLabel = string.Concat(group.NextToken, " - ", line_Child.NextToken);
                                        SysFeatureGroupStructure groupNode = new SysFeatureGroupStructure
                                        {
                                            NodeId = count++,
                                            NodeParentId = parentNodeId,
                                            SiteLoginType = site,
                                            NodeLabel = nodeLabel,
                                            SysFeatureGroupGUID = null,
                                            FeatureType = GetSysFeatureTypeValueFromGroupSegment(line_Child.NextToken),
                                            CreatedBy = username,
                                            ModifiedBy = username,
                                            CreatedDateTime = DateTime.Now,
                                            ModifiedDateTime = DateTime.Now
                                        };
                                        result.Add(groupNode);
                                        count = result.Count;
                                        #endregion
                                        #region create page nodes
                                        // group page nodes (in each group type)
                                        int nextPageLevelIdx = parentSplit.Length + 2;
                                        var nextPageGroup = group.Group.Where(w => w.PathMenuSplits.Length > nextPageLevelIdx)
                                                            .Select(s => new
                                                            {
                                                                NextToken = s.PathMenuSplits.ElementAt(nextPageLevelIdx),
                                                                SysFeatureGroup = s.SysFeatureGroup,
                                                                PathMenuSplits = s.PathMenuSplits
                                                            }).GroupBy(g => g.NextToken).Select(s => new { NextToken = s.Key, Group = s });
                                        if (nextPageGroup.Count() > 0)
                                        {
                                            foreach (var nextPage in nextPageGroup)
                                            {
                                                // find min path length in each group 
                                                var nextPageMinLength = nextPage.Group.Min(m => m.PathMenuSplits.Length);
                                                var nextPageMin = nextPage.Group.Where(w => w.PathMenuSplits.Length == nextPageMinLength);

                                                if (nextPageMin.Count() > 0)
                                                {
                                                    // there should be only 1 page min length
                                                    foreach (var page in nextPageMin)
                                                    {
                                                        nodeLabel = page.NextToken;
                                                        SysFeatureGroupStructure pageNode = new SysFeatureGroupStructure
                                                        {
                                                            NodeId = count++,
                                                            NodeParentId = groupNode.NodeId,
                                                            SiteLoginType = site,
                                                            NodeLabel = nodeLabel,
                                                            SysFeatureGroupGUID = page.SysFeatureGroup.SysFeatureGroupGUID,
                                                            FeatureType = page.SysFeatureGroup.FeatureType,
                                                            CreatedBy = username,
                                                            ModifiedBy = username,
                                                            CreatedDateTime = DateTime.Now,
                                                            ModifiedDateTime = DateTime.Now
                                                        };
                                                        result.Add(pageNode);
                                                        count = result.Count;

                                                        // recursive next page level
                                                        var nextSourceChildren = nextPage.Group.Where(w => w.SysFeatureGroup.PathMenu.Trim() != page.SysFeatureGroup.PathMenu.Trim() &&
                                                                                                        w.SysFeatureGroup.PathMenu.Trim().StartsWith(page.SysFeatureGroup.PathMenu.Trim()))
                                                                                                .Select(s => s.SysFeatureGroup);
                                                        GetNestedSysFeatureGroupStructure(pageNode, nextSourceChildren, result, count,
                                                                                            page.SysFeatureGroup.PathMenu, pageNode.NodeId, username);
                                                        count = result.Count;
                                                    }
                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                }
                            }

                        }
                        #endregion
                    }
                }

                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private int GetSysFeatureTypeValueFromGroupSegment(string group)
        {
            try
            {
                switch(group)
                {
                    case RoleRouteConst.Function: return (int)SysFeatureType.Function;
                    case RoleRouteConst.RelatedInfo: return (int)SysFeatureType.RelatedInfo;
                    case RoleRouteConst.Report: return (int)SysFeatureType.Report;
                    case RoleRouteConst.Workflow: return (int)SysFeatureType.Workflow;
                    default: throw new Exception("Invalid group value: " + group);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<string> GetTableNamesForControllerEntityMapping()
        {
            try
            {
                var tables = db.Model.GetEntityTypes().Select(s => s.ClrType.Name).Where(w => !w.StartsWith("Sys", StringComparison.InvariantCultureIgnoreCase) &&
                                                                                !w.StartsWith("Migration", StringComparison.InvariantCultureIgnoreCase) &&
                                                                                !w.StartsWith("Staging_", StringComparison.InvariantCultureIgnoreCase) &&
                                                                                !w.StartsWith("Batch", StringComparison.InvariantCultureIgnoreCase));
                return tables;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Initialize data

        public IEnumerable<SelectItem<SysRoleTableItemView>> GetSysRoleTableDropDownNotFiltered(SearchParameter search)
        {
            try
            {
                ISysRoleTableRepo sysRoleTableRepo = new SysRoleTableRepo(db);
                return sysRoleTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        //public List<SysFeatureGroupStructure> GetSysFeatureGroupStructureToMap(string controllerName, IEnumerable<SysFeatureGroupControllerMappingView> features)
        //{
        //    try
        //    {
        //        return null;
        //    }
        //    catch (Exception e)
        //    {
        //        throw SmartAppUtil.AddStackTrace(e);
        //    }
        //}
        public IEnumerable<SysFeatureGroupExportView> GetExportRoleData(string id)
        {
            try
            {
                ISysFeatureGroupRoleRepo sysFeatureGroupRoleRepo = new SysFeatureGroupRoleRepo(db);
                var result = sysFeatureGroupRoleRepo.GetExportRoleData(id.StringToGuid());
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<ITree<SysFeatureGroupStructureView>> GetRoleStructure()
        {
            try
            {
                ISysFeatureGroupStructureRepo sysFeatureGroupStructureRepo = new SysFeatureGroupStructureRepo(db);
                return sysFeatureGroupStructureRepo.GetSysFeatureGroupStructureTree();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ImportAccessRightResultView ImportAccessRight(ImportAccessRightView importAccessRightView)
        {
            try
            {
                ISysFeatureGroupRepo sysFeatureGroupRepo = new SysFeatureGroupRepo(db);
                ISysFeatureGroupRoleRepo sysFeatureGroupRoleRepo = new SysFeatureGroupRoleRepo(db);
                ISysControllerRoleRepo sysControllerRoleRepo = new SysControllerRoleRepo(db);
                FileHelper fileHelper = new FileHelper();

                Guid roleGUID = importAccessRightView.RoleGUID.StringToGuid();
                string username = GetUserName();

                FileUpload fileUpload = new FileUpload 
                { 
                    FileInfos = new List<FileInformation>() { importAccessRightView.FileInfo } 
                };

                var importAccessRightViews =
                    fileHelper.MapInputExcelFileToSingleEntityType<ImportAccessRightView>(fileUpload, ImportAccessRightView.GetMapper());

                if (importAccessRightViews.Any())
                {
                    #region create SysFeatureGroupRole from import
                    // group duplicate GroupIds
                    var groupImportAccessRightViews = importAccessRightViews.GroupBy(g => g.GroupId)
                                                                            .Select(s => new { s.Key, Group = s.FirstOrDefault() })
                                                                            .Where(w => !string.IsNullOrWhiteSpace(w.Key));
                    // get SysFeatureGroup by GroupId from imports
                    var sysFeatureGroups =
                        sysFeatureGroupRepo.GetSysFeatureGroupByGroupIdNoTracking(groupImportAccessRightViews.Select(s => s.Group.GroupId.Trim()));
                    
                    List<SysFeatureGroupRole> sysFeatureGroupRoles = new List<SysFeatureGroupRole>();
                    foreach (var item in groupImportAccessRightViews)
                    {
                        var fg = sysFeatureGroups.Where(w => w.GroupId == item.Group.GroupId.Trim()).FirstOrDefault();
                        if(fg != null)
                        {
                            int type = fg.FeatureType;

                            int action = SysAccessLevelHelper.GetCRUDActionByFeatureType(CRUDActionConst.ACTION, item.Group.Action, type);
                            int read = SysAccessLevelHelper.GetCRUDActionByFeatureType(CRUDActionConst.READ, item.Group.Read, type);
                            int update = SysAccessLevelHelper.GetCRUDActionByFeatureType(CRUDActionConst.UPDATE, item.Group.Update, type);
                            int create = SysAccessLevelHelper.GetCRUDActionByFeatureType(CRUDActionConst.CREATE, item.Group.Create, type);
                            int delete = SysAccessLevelHelper.GetCRUDActionByFeatureType(CRUDActionConst.DELETE, item.Group.Delete, type);

                            if (action > 0 || read > 0 || update > 0 || create > 0 || delete > 0)
                            {
                                SysFeatureGroupRole fgr = new SysFeatureGroupRole
                                {
                                    SysFeatureGroupRoleGUID = Guid.NewGuid(),
                                    RoleGUID = roleGUID,
                                    SysFeatureGroupGUID = fg.SysFeatureGroupGUID,
                                    Action = action,
                                    Create = create,
                                    Delete = delete,
                                    Read = read,
                                    Update = update,
                                    CreatedBy = username,
                                    CreatedDateTime = DateTime.Now,
                                    ModifiedBy = username,
                                    ModifiedDateTime = DateTime.Now,
                                };
                                sysFeatureGroupRoles.Add(fgr);
                            }
                        }
                        
                    }
                    #endregion

                    #region create SysControllerRole from SysFeatureGroupRole
                    var sysControllerRole = sysControllerRoleRepo.PrepareSysControllerRoleForCreateOrUpdate(sysFeatureGroupRoles);
                    #endregion
                    var sysEntityRole = PrepareSysEntityRoleForCreateOrUpdate(roleGUID, sysControllerRole);
                    #region bulk operations
                    List<SysFeatureGroupRole> oldSysFeatureGroupRole = sysFeatureGroupRoleRepo.GetSysFeatureGroupRoleByRoleGUIDNoTracking(roleGUID);
                    List<SysControllerRole> oldSysControllerRole = sysControllerRoleRepo.GetSysControllerRoleByRoleGUIDNoTracking(roleGUID);
                    List<SysEntityRole> oldSysEntityRole = db.Set<SysEntityRole>().Where(w => w.RoleGUID == roleGUID).AsNoTracking().ToList();

                    if (oldSysControllerRole.Count > 0 || oldSysFeatureGroupRole.Count > 0 || sysFeatureGroupRoles.Count > 0 || sysControllerRole.Count > 0)
                    {
                        var bulkConfig = new BulkConfig { UseTempDB = true };
                        using (var transaction = UnitOfWork.ContextTransaction())
                        {
                            this.BulkDelete(oldSysFeatureGroupRole);
                            this.BulkDelete(oldSysControllerRole);
                            this.BulkDelete(oldSysEntityRole);

                            this.BulkInsert(sysFeatureGroupRoles, false, true, bulkConfig);
                            this.BulkInsert(sysControllerRole, false, true, bulkConfig);
                            this.BulkInsert(sysEntityRole, false, true, bulkConfig);
                            UnitOfWork.Commit(transaction);
                        }
                    }
                    
                    #endregion
                }

                NotificationResponse success = new NotificationResponse();
                ImportAccessRightResultView importAccessRightResultView = new ImportAccessRightResultView();
                success.AddData("SUCCESS.90017", new string[] { "LABEL.ACCESS_RIGHT" });
                importAccessRightResultView.Notification = success;

                return importAccessRightResultView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
