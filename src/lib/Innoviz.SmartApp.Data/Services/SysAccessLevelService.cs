﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.Repositories;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ViewModelHandler;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Innoviz.SmartApp.Data.Services
{
    public interface ISysAccessLevelDataService
    {
        IEnumerable<BusinessUnit> GetParentChildBusinessUnitsByUserName(string userName, string companyGUID);
    }
    public class SysAccessLevelService : SmartAppService, ISysAccessLevelService, ISysAccessLevelDataService
    {
        public SysAccessLevelService(SmartAppDbContext context): base(context)
        {

        }
        public T AssignOwnerBU<T> (T item)
        {
            try
            {
                var HasCompany = typeof(T).BaseType.GetProperty(TextConstants.CompanyGUID) != null ? true : false;
                var HasOwner = typeof(T).BaseType.GetProperty(TextConstants.Owner) != null;
                var HasOwnerBusinessUnitGUID = typeof(T).BaseType.GetProperty(TextConstants.OwnerBusinessUnitGUID) != null;
                if (HasCompany && (HasOwner && HasOwnerBusinessUnitGUID))
                {
                    PropertyInfo ownerBUPropInfo = item.GetType().GetProperty(TextConstants.OwnerBusinessUnitGUID);
                    PropertyInfo ownerPropInfo = item.GetType().GetProperty(TextConstants.Owner);
                    var owner = ownerPropInfo.GetValue(item)?.ToString();
                    var ownerBU = ownerBUPropInfo.GetValue(item)?.ToString();

                    if (string.IsNullOrEmpty(owner) && string.IsNullOrEmpty(GetUserName()))
                    {
                        throw new Exception("Value: Owner must be specified.");
                    }
                    else
                    {
                        if(string.IsNullOrEmpty(owner))
                        {
                            owner = GetUserName();
                            ownerPropInfo.SetValue(item, owner);
                        }

                        if(string.IsNullOrEmpty(ownerBU))
                        {
                            var sysOwnerBU = GetOwnerBusinessUnitGUIDByUserName(owner, GetCurrentCompany());
                            if (ownerBUPropInfo.PropertyType == typeof(string))
                            {
                                ownerBUPropInfo.SetValue(item, sysOwnerBU.GuidNullToString());
                            }
                            else
                            {
                                ownerBUPropInfo.SetValue(item, sysOwnerBU);
                            }
                        }
                        
                        
                    }
                }
                return item;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public Guid? GetOwnerBusinessUnitGUIDByUserName(string userName, string companyGUID)
        {
            try
            {
                ISysUserTableRepo userRepo = new SysUserTableRepo(db);
                return userRepo.GetUserBusinessUnitGUIDByUserName(userName, companyGUID.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<BusinessUnit> GetParentChildBusinessUnitsByUserName(string userName, string companyGUID)
        {
            try
            {
                Guid? buGUID = GetOwnerBusinessUnitGUIDByUserName(userName, companyGUID);
                return GetParentChildBusinessUnitsByBusinessUnit(buGUID.GuidNullToString(), companyGUID);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<Guid?> GetParentChildBusinessUnitGUIDsByUserName(string userName, string companyGUID)
        {
            try
            {
                return GetParentChildBusinessUnitsByUserName(userName, companyGUID)
                            .Select(s => (Guid?)s.BusinessUnitGUID);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<BusinessUnit> GetParentChildBusinessUnitsByBusinessUnit(string businessUnitGuid, string companyGUID)
        {
            try
            {
                Guid companyGuid = companyGUID.StringToGuid();
                Guid? buGUID = businessUnitGuid.StringToGuidNull();
                IBusinessUnitRepo buRepo = new BusinessUnitRepo(db);
                if (buGUID.HasValue)
                {
                    return buRepo.GetParentChildBusinessUnitByBusinessUnitGUID(buGUID.Value, companyGuid);
                }
                else
                {
                    return new List<BusinessUnit>();
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<Guid?> GetParentChildBusinessUnitGUIDsByBusinessUnit(string businessUnitGuid, string companyGuid)
        {
            try
            {
                return GetParentChildBusinessUnitsByBusinessUnit(businessUnitGuid, companyGuid)
                            .Select(s => (Guid?)s.BusinessUnitGUID);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public Guid GetRootBusinessUnitGUIDByCompany(string companyGuid)
        {
            try
            {
                IBusinessUnitRepo businessUnitRepo = new BusinessUnitRepo(db);
                var result = businessUnitRepo.GetRootBusinessUnitByCompany(companyGuid.StringToGuid()).BusinessUnitGUID;
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
