﻿using EFCore.BulkExtensions;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Data.Services
{
    public interface ISysMessageService
    {
        bool InitializeSysMessage(List<SysMessage> list);
        bool InitializeSysMessage(string jsonContent);
        List<SysMessage> GetSysMessageNoTracking();
    }

    public class SysMessageService : SmartAppService, ISysMessageService
    {
        public SysMessageService(SmartAppDbContext context): base(context)
        {

        }
        public SysMessageService() { }

        public bool InitializeSysMessage(List<SysMessage> list)
        {
            try
            {
                // use UseTempDB when CREATE TABLE permission denied
                var bulkConfig = new BulkConfig { UseTempDB = true };
                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    List<SysMessage> oldData = db.Set<SysMessage>().ToList();
                    db.BulkDelete(oldData);
                    db.BulkInsert(list);
                    UnitOfWork.Commit(transaction);
                }
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool InitializeSysMessage(string jsonContent)
        {
            try
            {
                SmartAppUtil.LogMessage(Serilog.Events.LogEventLevel.Information, "Initializing SysMessage...");
                var msgList = BuildMessageListFromJsonContent(jsonContent);
                return InitializeSysMessage(msgList);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private List<SysMessage> BuildMessageListFromJsonContent(string jsonContent)
        {
            try
            {
                Dictionary<string, Dictionary<string, string>> msgs =
                    JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, string>>>(jsonContent);

                Dictionary<string, string> success = msgs[MessageGroup.SUCCESS];
                Dictionary<string, string> error = msgs[MessageGroup.ERROR];
                Dictionary<string, string> label = msgs[MessageGroup.LABEL];
                Dictionary<string, string> menu = msgs[MessageGroup.MENU];
                Dictionary<string, string> enums = msgs[MessageGroup.ENUM];
                Dictionary<string, string> role = msgs[MessageGroup.ROLE_CMPNT_GRP];
                Dictionary<string, string> routeSegment = msgs[MessageGroup.ROLE_ROUTE_SEGMENT];

                List<SysMessage> msgList = new List<SysMessage>();
                foreach (var key in label.Keys)
                {
                    string code = $"{MessageGroup.LABEL}.{key}";
                    string value = label[key];
                    SysMessage msgObj = new SysMessage();
                    msgObj.Code = code;
                    msgObj.Message = value;

                    msgList.Add(msgObj);
                }
                foreach (var key in success.Keys)
                {
                    string code = $"{MessageGroup.SUCCESS}.{key}";
                    string value = success[key];
                    SysMessage msgObj = new SysMessage();
                    msgObj.Code = code;
                    msgObj.Message = value;

                    msgList.Add(msgObj);
                }
                foreach (var key in error.Keys)
                {
                    string code = $"{MessageGroup.ERROR}.{key}";
                    string value = error[key];
                    SysMessage msgObj = new SysMessage();
                    msgObj.Code = code;
                    msgObj.Message = value;

                    msgList.Add(msgObj);
                }
                foreach (var key in menu.Keys)
                {
                    string code = $"{MessageGroup.MENU}.{key}";
                    string value = menu[key];
                    SysMessage msgObj = new SysMessage();
                    msgObj.Code = code;
                    msgObj.Message = value;

                    msgList.Add(msgObj);
                }
                foreach (var key in enums.Keys)
                {
                    string code = $"{MessageGroup.ENUM}.{key}";
                    string value = enums[key];
                    SysMessage msgObj = new SysMessage();
                    msgObj.Code = code;
                    msgObj.Message = value;

                    msgList.Add(msgObj);
                }
                foreach (var key in role.Keys)
                {
                    string code = $"{MessageGroup.ROLE_CMPNT_GRP}.{key}"; ;
                    string value = role[key];
                    SysMessage msgObj = new SysMessage();
                    msgObj.Code = code;
                    msgObj.Message = value;

                    msgList.Add(msgObj);
                }
                foreach (var key in routeSegment.Keys)
                {
                    string code = $"{MessageGroup.ROLE_ROUTE_SEGMENT}.{key}"; ;
                    string value = routeSegment[key];
                    SysMessage msgObj = new SysMessage();
                    msgObj.Code = code;
                    msgObj.Message = value;

                    msgList.Add(msgObj);
                }
                return msgList;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<SysMessage> GetSysMessageNoTracking()
        {
            try
            {
                return db.Set<SysMessage>().AsNoTracking().ToList();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
