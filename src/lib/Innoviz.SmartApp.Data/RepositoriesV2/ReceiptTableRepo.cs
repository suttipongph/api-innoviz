using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IReceiptTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<ReceiptTableItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<ReceiptTableListView> GetListvw(SearchParameter search);
		ReceiptTableItemView GetByIdvw(Guid id);
		ReceiptTable Find(params object[] keyValues);
		ReceiptTable GetReceiptTableByIdNoTracking(Guid guid);
		ReceiptTable CreateReceiptTable(ReceiptTable receiptTable);
		void CreateReceiptTableVoid(ReceiptTable receiptTable);
		ReceiptTable UpdateReceiptTable(ReceiptTable dbReceiptTable, ReceiptTable inputReceiptTable, List<string> skipUpdateFields = null);
		void UpdateReceiptTableVoid(ReceiptTable dbReceiptTable, ReceiptTable inputReceiptTable, List<string> skipUpdateFields = null);
		void Remove(ReceiptTable item);
		ReceiptTable GetReceiptTableByIdNoTrackingByAccessLevel(Guid guid);
	}
	public class ReceiptTableRepo : BranchCompanyBaseRepository<ReceiptTable>, IReceiptTableRepo
	{
		public ReceiptTableRepo(SmartAppDbContext context) : base(context) { }
		public ReceiptTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ReceiptTable GetReceiptTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ReceiptTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<ReceiptTableItemViewMap> GetDropDownQuery()
		{
			return (from receiptTable in Entity
					select new ReceiptTableItemViewMap
					{
						CompanyGUID = receiptTable.CompanyGUID,
						Owner = receiptTable.Owner,
						OwnerBusinessUnitGUID = receiptTable.OwnerBusinessUnitGUID,
						ReceiptTableGUID = receiptTable.ReceiptTableGUID,
						ReceiptId = receiptTable.ReceiptId,
						ReceiptDate = receiptTable.ReceiptDate
					});
		}
		public IEnumerable<SelectItem<ReceiptTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<ReceiptTable>(search, db.GetAvailableBranch(), SysParm.CompanyGUID, SysParm.BranchGUID);
				var receiptTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<ReceiptTableItemViewMap, ReceiptTableItemView>().ToDropDownItem(search.ExcludeRowData);
				return receiptTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<ReceiptTableListViewMap> GetListQuery()
		{
		
			return (from receiptTable in Entity
					join company in db.Set<Company>()
					on receiptTable.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on receiptTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljReceiptTableOwnerBU
					from ownerBU in ljReceiptTableOwnerBU.DefaultIfEmpty()

					join documentStatus in db.Set<DocumentStatus>()
					on receiptTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentstatus
					from documentStatus in ljdocumentstatus.DefaultIfEmpty()

					join currency_Value in db.Set<Currency>()
					on receiptTable.CurrencyGUID equals currency_Value.CurrencyGUID into ljCurrency
					from currency_Value in ljCurrency.DefaultIfEmpty()

					join customer_Value in db.Set<CustomerTable>()
					on receiptTable.CustomerTableGUID equals customer_Value.CustomerTableGUID into ljcustomer
					from customer_Value in ljcustomer.DefaultIfEmpty()
					
					join invoiceSettlement in db.Set<InvoiceSettlementDetail>()
					on receiptTable.InvoiceSettlementDetailGUID equals invoiceSettlement.InvoiceSettlementDetailGUID into ljinvoiceSettlement
					from invoiceSettlement in ljinvoiceSettlement.DefaultIfEmpty()

					join invoiceTable in db.Set<InvoiceTable>()
					on invoiceSettlement.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljinvoiceTable
					from invoiceTable in ljinvoiceTable.DefaultIfEmpty()
					select new ReceiptTableListViewMap
				{
						CompanyGUID = receiptTable.CompanyGUID,
						Owner = receiptTable.Owner,
						OwnerBusinessUnitGUID = receiptTable.OwnerBusinessUnitGUID,
						ReceiptTableGUID = receiptTable.ReceiptTableGUID,
						ReceiptId = receiptTable.ReceiptId,
						DocumentStatusGUID = receiptTable.DocumentStatusGUID,
						ReceiptDate = receiptTable.ReceiptDate,
						TransDate = receiptTable.TransDate,
						CustomerTableGUID = receiptTable.CustomerTableGUID,
						InvoiceSettlementDetailGUID = receiptTable.InvoiceSettlementDetailGUID,
						InvoiceSettlementDetail_Values = SmartAppUtil.GetDropDownLabel(invoiceTable.InvoiceId,invoiceTable.IssuedDate.ToString()),
						CurrencyGUID = receiptTable.CurrencyGUID,
						SettleBaseAmount = receiptTable.SettleBaseAmount,
						SettleTaxAmount = receiptTable.SettleTaxAmount,
						SettleAmount = receiptTable.SettleAmount,
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customer_Value.CustomerId,customer_Value.Name),
						Currency_Values = SmartAppUtil.GetDropDownLabel(currency_Value.CurrencyId,currency_Value.Name),
						DocumentStatus_Values =documentStatus.Description,
						RefGUID=receiptTable.RefGUID
				});
		}
		public SearchResult<ReceiptTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<ReceiptTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<ReceiptTable>(search, db.GetAvailableBranch(), SysParm.CompanyGUID, SysParm.BranchGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<ReceiptTableListViewMap, ReceiptTableListView>();
				result = list.SetSearchResult<ReceiptTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<ReceiptTableItemViewMap> GetItemQuery()
		{
			var res = (from receiptTable in Entity
                       join company in db.Set<Company>()
                       on receiptTable.CompanyGUID equals company.CompanyGUID
                       join branch in db.Set<Branch>()
                       on receiptTable.BranchGUID equals branch.BranchGUID
                       join ownerBU in db.Set<BusinessUnit>()
                       on receiptTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljReceiptTableOwnerBU
                       from ownerBU in ljReceiptTableOwnerBU.DefaultIfEmpty()

                       join customer_Value in db.Set<CustomerTable>()
                       on receiptTable.CustomerTableGUID equals customer_Value.CustomerTableGUID into ljCustomer
                       from customer_Value in ljCustomer.DefaultIfEmpty()

                       join receiptTemp in db.Set<ReceiptTempTable>()
                       on receiptTable.RefGUID equals receiptTemp.ReceiptTempTableGUID into ljreceiptTemp
                       from receiptTemp in ljreceiptTemp.DefaultIfEmpty()

                       join currency_Value in db.Set<Currency>()
                       on receiptTable.CurrencyGUID equals currency_Value.CurrencyGUID into ljCurrency
                       from currency_Value in ljCurrency.DefaultIfEmpty()

                       join methodOfPayment_value in db.Set<MethodOfPayment>()
                       on receiptTable.MethodOfPaymentGUID equals methodOfPayment_value.MethodOfPaymentGUID into ljmethodOfPayment
                       from methodOfPayment_value in ljmethodOfPayment.DefaultIfEmpty()

                       join documentStatus in db.Set<DocumentStatus>()
                       on receiptTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentstatus
                       from documentStatus in ljdocumentstatus.DefaultIfEmpty()

                       join purchaseLine in db.Set<PurchaseLine>()
                       on receiptTable.RefGUID equals purchaseLine.PurchaseLineGUID into ljPurchase
                       from purchaseLine in ljPurchase.DefaultIfEmpty()

                       join purchaseTable in db.Set<PurchaseTable>()
                       on purchaseLine.PurchaseTableGUID equals purchaseTable.PurchaseTableGUID into ljpurchaseTable
                       from purchaseTable in ljpurchaseTable.DefaultIfEmpty()

                       join withdrawalLine in db.Set<WithdrawalLine>()
                       on receiptTable.RefGUID equals withdrawalLine.WithdrawalLineGUID into ljWithdrawalLine
                       from withdrawalLine in ljWithdrawalLine.DefaultIfEmpty()

                       join servicefeeTrans in db.Set<ServiceFeeTrans>()
                       on receiptTable.RefGUID equals servicefeeTrans.ServiceFeeTransGUID into ljServicefeeTrans
                       from servicefeeTrans in ljServicefeeTrans.DefaultIfEmpty()

                       join invoiceRevenueType in db.Set<InvoiceRevenueType>()
                       on servicefeeTrans.InvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID into ljinvoiceRevenueType
                       from invoiceRevenueType in ljinvoiceRevenueType.DefaultIfEmpty()

					   join invoiceSettlementDetail in db.Set<InvoiceSettlementDetail>()
					   on receiptTable.InvoiceSettlementDetailGUID equals invoiceSettlementDetail.InvoiceSettlementDetailGUID into ljinvoiceSettlementDetail
					   from invoiceSettlementDetail in ljinvoiceSettlementDetail.DefaultIfEmpty()

					   join invoiceTable in db.Set<InvoiceTable>()
					   on invoiceSettlementDetail.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljinvoiceTable
					   from invoiceTable in ljinvoiceTable.DefaultIfEmpty()

					   join receiptTempTable in db.Set<ReceiptTempTable>()
					   on receiptTable.RefGUID equals receiptTempTable.ReceiptTempTableGUID into ljreceiptTempTable
					   from receiptTempTable in ljreceiptTempTable.DefaultIfEmpty()

					   join customerRefundTable in db.Set<CustomerRefundTable>()
					   on receiptTable.RefGUID equals customerRefundTable.CustomerRefundTableGUID into ljcustomerRefund
					   from customerRefundTable in ljcustomerRefund.DefaultIfEmpty()

					   
					   select new ReceiptTableItemViewMap
					   {
						   CompanyGUID = receiptTable.CompanyGUID,
                           CompanyId = company.CompanyId,
                           BranchGUID = receiptTable.BranchGUID,
                           BranchId = branch.BranchId,
                           Owner = receiptTable.Owner,
                           OwnerBusinessUnitGUID = receiptTable.OwnerBusinessUnitGUID,
                           OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                           CreatedBy = receiptTable.CreatedBy,
						   CreatedDateTime = receiptTable.CreatedDateTime,
						   ModifiedBy = receiptTable.ModifiedBy,
						   ModifiedDateTime = receiptTable.ModifiedDateTime,
						   ReceiptTableGUID = receiptTable.ReceiptTableGUID,
						   ChequeBankGroupGUID = receiptTable.ChequeBankGroupGUID,
						   ChequeBranch = receiptTable.ChequeBranch,
						   ChequeDate = receiptTable.ChequeDate,
						   ChequeNo = receiptTable.ChequeNo,
						   CurrencyGUID = receiptTable.CurrencyGUID,
						   CustomerTableGUID = receiptTable.CustomerTableGUID,
						   DocumentStatusGUID = receiptTable.DocumentStatusGUID,
						   ExchangeRate = receiptTable.ExchangeRate,
						   MethodOfPaymentGUID = receiptTable.MethodOfPaymentGUID,
						   OverUnderAmount = receiptTable.OverUnderAmount,
						   ReceiptAddress1 = receiptTable.ReceiptAddress1,
						   ReceiptAddress2 = receiptTable.ReceiptAddress2,
						   ReceiptAddress_Values = SmartAppUtil.GetAddressLabel(receiptTable.ReceiptAddress1, receiptTable.ReceiptAddress2),
						   ReceiptDate = receiptTable.ReceiptDate,
						   ReceiptId = receiptTable.ReceiptId,
                           RefGUID = receiptTable.RefGUID,
                           RefId = (receiptTemp != null && receiptTable.RefType == (int)RefType.ReceiptTemp) ? receiptTemp.ReceiptTempId :
                                   (purchaseLine != null && receiptTable.RefType == (int)RefType.PurchaseLine) ? purchaseTable.PurchaseId :
                                   (withdrawalLine != null && receiptTable.RefType == (int)RefType.WithdrawalLine) ? withdrawalLine.WithdrawalLineType.ToString() :
                                   (servicefeeTrans != null && receiptTable.RefType == (int)RefType.ServiceFeeTrans) ? invoiceRevenueType.RevenueTypeId :
                                   (receiptTempTable != null && receiptTable.RefType == (int)RefType.ReceiptTemp)?receiptTempTable.ReceiptTempId:
								   (customerRefundTable !=null && receiptTable.RefType==(int)RefType.CustomerRefund)?customerRefundTable.CustomerRefundId
								   :null,
                           RefType = receiptTable.RefType,
                           InvoiceSettlementDetailGUID = SmartAppUtil.GetDropDownLabel(invoiceTable.InvoiceId,invoiceTable.IssuedDate.ToString()),
						   Remark = receiptTable.Remark,
						   SettleAmount = receiptTable.SettleAmount,
						   SettleAmountMST = receiptTable.SettleAmountMST,
						   SettleBaseAmount = receiptTable.SettleBaseAmount,
						   SettleBaseAmountMST = receiptTable.SettleBaseAmountMST,
						   SettleTaxAmount = receiptTable.SettleTaxAmount,
						   SettleTaxAmountMST = receiptTable.SettleTaxAmountMST,
						   TransDate = receiptTable.TransDate,
                           CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customer_Value.CustomerId, customer_Value.Name),
                           DocumentStatus_Values = documentStatus.StatusId,
						   DocumentStatus_Description = documentStatus.Description,
						   ReceiptTempTable_Values = receiptTemp!=null? SmartAppUtil.GetDropDownLabel(receiptTemp.ReceiptTempId, receiptTemp.ReceiptDate.DateToString()): "",
                           Methodopayment_Values = methodOfPayment_value.MethodOfPaymentId,
                           Currency_Values = currency_Value.CurrencyId,
						   InvoiceSettlementDetail_Values = invoiceSettlementDetail!=null? SmartAppUtil.GetDropDownLabel(invoiceTable.InvoiceId, invoiceTable.IssuedDate.DateToString()) : "",

						   RowVersion = receiptTable.RowVersion,
					   });
			return res;
		}
		public ReceiptTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<ReceiptTableItemViewMap, ReceiptTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public ReceiptTable CreateReceiptTable(ReceiptTable receiptTable)
		{
			try
			{
				receiptTable.ReceiptTableGUID = Guid.NewGuid();
				base.Add(receiptTable);
				return receiptTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateReceiptTableVoid(ReceiptTable receiptTable)
		{
			try
			{
				CreateReceiptTable(receiptTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ReceiptTable UpdateReceiptTable(ReceiptTable dbReceiptTable, ReceiptTable inputReceiptTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbReceiptTable = dbReceiptTable.MapUpdateValues<ReceiptTable>(inputReceiptTable);
				base.Update(dbReceiptTable);
				return dbReceiptTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateReceiptTableVoid(ReceiptTable dbReceiptTable, ReceiptTable inputReceiptTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbReceiptTable = dbReceiptTable.MapUpdateValues<ReceiptTable>(inputReceiptTable, skipUpdateFields);
				base.Update(dbReceiptTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public ReceiptTable GetReceiptTableByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ReceiptTableGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}
