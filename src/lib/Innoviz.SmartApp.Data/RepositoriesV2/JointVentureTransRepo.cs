using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface IJointVentureTransRepo
    {
        #region DropDown
        IEnumerable<SelectItem<JointVentureTransItemView>> GetDropDownItem(SearchParameter search);
        #endregion DropDown
        SearchResult<JointVentureTransListView> GetListvw(SearchParameter search);
        JointVentureTransItemView GetByIdvw(Guid id);
        JointVentureTrans Find(params object[] keyValues);
        JointVentureTrans GetJointVentureTransByIdNoTracking(Guid guid);
        JointVentureTrans CreateJointVentureTrans(JointVentureTrans jointVentureTrans);
        void CreateJointVentureTransVoid(JointVentureTrans jointVentureTrans);
        JointVentureTrans UpdateJointVentureTrans(JointVentureTrans dbJointVentureTrans, JointVentureTrans inputJointVentureTrans, List<string> skipUpdateFields = null);
        void UpdateJointVentureTransVoid(JointVentureTrans dbJointVentureTrans, JointVentureTrans inputJointVentureTrans, List<string> skipUpdateFields = null);
        void Remove(JointVentureTrans item);
        IEnumerable<JointVentureTrans> GetJointVentureTransByReferenceTableNoTracking(Guid refGUID, int refType);
        void ValidateAdd(JointVentureTrans item);
        void ValidateAdd(IEnumerable<JointVentureTrans> items);
        #region Bookmark
        MainAgreementJVBookmarkView GetMainAgreementJVBookmarkValue(Guid refGuid);
        QJointVentureTrans1 GetQJointVentureTrans1(Guid refGUID);
        QJointVentureTrans2 GetQJointVentureTrans2(Guid refGUID);
        QJointVentureTrans3 GetQJointVentureTrans3(Guid refGUID);

        #endregion
    }
    public class JointVentureTransRepo : CompanyBaseRepository<JointVentureTrans>, IJointVentureTransRepo
    {
        public JointVentureTransRepo(SmartAppDbContext context) : base(context) { }
        public JointVentureTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public JointVentureTrans GetJointVentureTransByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.JointVentureTransGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        private IQueryable<JointVentureTransItemViewMap> GetDropDownQuery()
        {
            return (from jointVentureTrans in Entity
                    select new JointVentureTransItemViewMap
                    {
                        CompanyGUID = jointVentureTrans.CompanyGUID,
                        Owner = jointVentureTrans.Owner,
                        OwnerBusinessUnitGUID = jointVentureTrans.OwnerBusinessUnitGUID,
                        JointVentureTransGUID = jointVentureTrans.JointVentureTransGUID
                    });
        }
        public IEnumerable<SelectItem<JointVentureTransItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<JointVentureTrans>(search, SysParm.CompanyGUID);
                var jointVentureTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<JointVentureTransItemViewMap, JointVentureTransItemView>().ToDropDownItem(search.ExcludeRowData);


                return jointVentureTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #region GetListvw
        private IQueryable<JointVentureTransListViewMap> GetListQuery()
        {
            return (from jointVentureTrans in Entity
                    join authorizedPersonType in db.Set<AuthorizedPersonType>()
                    on jointVentureTrans.AuthorizedPersonTypeGUID equals authorizedPersonType.AuthorizedPersonTypeGUID into ljAuthorizedPersonType
                    from authorizedPersonType in ljAuthorizedPersonType.DefaultIfEmpty()

                    join businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
                    on jointVentureTrans.RefGUID equals businessCollateralAgmTable.BusinessCollateralAgmTableGUID into ljbusinessCollateralTable
                    from businessCollateralAgmTable in ljbusinessCollateralTable.DefaultIfEmpty()

                    join documentStatus in db.Set<DocumentStatus>()
                    on businessCollateralAgmTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocStatus
                    from documentStatus in ljdocStatus.DefaultIfEmpty()


                    select new JointVentureTransListViewMap
                    {
                        CompanyGUID = jointVentureTrans.CompanyGUID,
                        Owner = jointVentureTrans.Owner,
                        OwnerBusinessUnitGUID = jointVentureTrans.OwnerBusinessUnitGUID,
                        JointVentureTransGUID = jointVentureTrans.JointVentureTransGUID,
                        Ordering = jointVentureTrans.Ordering,
                        AuthorizedPersonTypeGUID = jointVentureTrans.AuthorizedPersonTypeGUID,
                        Name = jointVentureTrans.Name,
                        OperatedBy = jointVentureTrans.OperatedBy,
                        RefGUID = jointVentureTrans.RefGUID,
                        AuthorizedPersonType_AuthorizedPersonTypeId = authorizedPersonType.AuthorizedPersonTypeId,
                        AuthorizedPersonType_Values = SmartAppUtil.GetDropDownLabel(authorizedPersonType.AuthorizedPersonTypeId, authorizedPersonType.Description),
                        DocumentStatus_StatusCode = documentStatus.StatusId
                    });
        }
        public SearchResult<JointVentureTransListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<JointVentureTransListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<JointVentureTrans>(search, SysParm.CompanyGUID);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<JointVentureTransListViewMap, JointVentureTransListView>();
                result = list.SetSearchResult<JointVentureTransListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<JointVentureTransItemViewMap> GetItemQuery()
        {
            return (from jointVentureTrans in Entity
                    join company in db.Set<Company>()
                    on jointVentureTrans.CompanyGUID equals company.CompanyGUID
                    join ownerBU in db.Set<BusinessUnit>()
                    on jointVentureTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljJointVentureTransOwnerBU
                    from ownerBU in ljJointVentureTransOwnerBU.DefaultIfEmpty()

                    join customerTable in db.Set<CustomerTable>()
                    on jointVentureTrans.RefGUID equals customerTable.CustomerTableGUID into ljCustomerTable
                    from customerTable in ljCustomerTable.DefaultIfEmpty()

                    join mainAgreementTable in db.Set<MainAgreementTable>()
                    on jointVentureTrans.RefGUID equals mainAgreementTable.MainAgreementTableGUID into ljMainAgreementTable
                    from mainAgreementTable in ljMainAgreementTable.DefaultIfEmpty()

                    join businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
                    on jointVentureTrans.RefGUID equals businessCollateralAgmTable.BusinessCollateralAgmTableGUID into ljbusinessCollateralTable
                    from businessCollateralAgmTable in ljbusinessCollateralTable.DefaultIfEmpty()

                    join assignmentAgreementTable in db.Set<AssignmentAgreementTable>()
                    on jointVentureTrans.RefGUID equals assignmentAgreementTable.AssignmentAgreementTableGUID into ljAssignmentAgreementTable
                    from assignmentAgreementTable in ljAssignmentAgreementTable.DefaultIfEmpty()

                    join documentStatus in db.Set<DocumentStatus>()
                    on businessCollateralAgmTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocStatus
                    from documentStatus in ljdocStatus.DefaultIfEmpty()
                    select new JointVentureTransItemViewMap
                    {
                        CompanyGUID = jointVentureTrans.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = jointVentureTrans.Owner,
                        OwnerBusinessUnitGUID = jointVentureTrans.OwnerBusinessUnitGUID,
                        CreatedBy = jointVentureTrans.CreatedBy,
                        CreatedDateTime = jointVentureTrans.CreatedDateTime,
                        ModifiedBy = jointVentureTrans.ModifiedBy,
                        ModifiedDateTime = jointVentureTrans.ModifiedDateTime,
                        JointVentureTransGUID = jointVentureTrans.JointVentureTransGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        Address = jointVentureTrans.Address,
                        AuthorizedPersonTypeGUID = jointVentureTrans.AuthorizedPersonTypeGUID,
                        Name = jointVentureTrans.Name,
                        OperatedBy = jointVentureTrans.OperatedBy,
                        Ordering = jointVentureTrans.Ordering,
                        RefGUID = jointVentureTrans.RefGUID,
                        RefType = jointVentureTrans.RefType,
                        Remark = jointVentureTrans.Remark,
                        DocumentStatus_StatusCode = documentStatus.StatusId,
                        RefId = (customerTable != null && jointVentureTrans.RefType == (int)RefType.Customer) ? customerTable.CustomerId :
                                (mainAgreementTable != null && jointVentureTrans.RefType == (int)RefType.MainAgreement) ? mainAgreementTable.InternalMainAgreementId :
                                (businessCollateralAgmTable != null && jointVentureTrans.RefType == (int)RefType.BusinessCollateralAgreement) ? businessCollateralAgmTable.InternalBusinessCollateralAgmId :
                                (assignmentAgreementTable != null && jointVentureTrans.RefType == (int)RefType.AssignmentAgreement) ? assignmentAgreementTable.InternalAssignmentAgreementId :
                                null,
                    
                        RowVersion = jointVentureTrans.RowVersion,
                    });
        }
        public JointVentureTransItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<JointVentureTransItemViewMap, JointVentureTransItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public JointVentureTrans CreateJointVentureTrans(JointVentureTrans jointVentureTrans)
        {
            try
            {
                jointVentureTrans.JointVentureTransGUID = Guid.NewGuid();
                base.Add(jointVentureTrans);
                return jointVentureTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateJointVentureTransVoid(JointVentureTrans jointVentureTrans)
        {
            try
            {
                CreateJointVentureTrans(jointVentureTrans);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public JointVentureTrans UpdateJointVentureTrans(JointVentureTrans dbJointVentureTrans, JointVentureTrans inputJointVentureTrans, List<string> skipUpdateFields = null)
        {
            try
            {
                dbJointVentureTrans = dbJointVentureTrans.MapUpdateValues<JointVentureTrans>(inputJointVentureTrans);
                base.Update(dbJointVentureTrans);
                return dbJointVentureTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateJointVentureTransVoid(JointVentureTrans dbJointVentureTrans, JointVentureTrans inputJointVentureTrans, List<string> skipUpdateFields = null)
        {
            try
            {
                dbJointVentureTrans = dbJointVentureTrans.MapUpdateValues<JointVentureTrans>(inputJointVentureTrans, skipUpdateFields);
                base.Update(dbJointVentureTrans);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Create, Update, Delete
        public IEnumerable<JointVentureTrans> GetJointVentureTransByReferenceTableNoTracking(Guid refGUID, int refType)
        {
            try
            {
                var result = Entity.Where(item => item.RefGUID == refGUID && item.RefType == refType).AsNoTracking();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public override void ValidateAdd(IEnumerable<JointVentureTrans> items)
        {
            base.ValidateAdd(items);
        }
        public override void ValidateAdd(JointVentureTrans item)
        {
            base.ValidateAdd(item);
        }
        #region Bookmark
        public MainAgreementJVBookmarkView GetMainAgreementJVBookmarkValue(Guid refGuid)
        {
            try
            {
                List<MainAgreementJVBookmarkView> trans = (from jointVentureTrans in Entity
                                                     join authorizedPersonType in db.Set<AuthorizedPersonType>()
                                                     on jointVentureTrans.AuthorizedPersonTypeGUID equals authorizedPersonType.AuthorizedPersonTypeGUID
                                                     into ljAuth from authorizedPersonType in ljAuth.DefaultIfEmpty()
                                                     where jointVentureTrans.RefGUID == refGuid
                                                     orderby jointVentureTrans.Ordering ascending
                                                     select new MainAgreementJVBookmarkView
                                                     {
                                                         JointAddress = jointVentureTrans.Address,
                                                         JointName = jointVentureTrans.Name,
                                                         PositionJoint = authorizedPersonType.Description,
                                                         TextJVDetail1 = jointVentureTrans.Remark,
                                                         OperatedBy = jointVentureTrans.OperatedBy
                                                     }).AsNoTracking().Take(4).ToList();

                MainAgreementJVBookmarkView mainAgreementJVBookmarkView = new MainAgreementJVBookmarkView
                {
                    JointAddressFirst1 = trans.Count > 0 && trans[0] != null ? trans[0].JointAddress : "",
                    JointNameFirst1 = trans.Count > 0 && trans[0] != null ? trans[0].JointName : "",
                    JointAddressSecond1 = trans.Count > 1 && trans[1] != null ? trans[1].JointAddress : "",
                    JointNameSecond1 = trans.Count > 1 && trans[1] != null ? trans[1].JointName : "",
                    JointAddressThird1 = trans.Count > 2 && trans[2] != null ? trans[2].JointAddress : "",
                    JointNameThird1 = trans.Count > 2 && trans[2] != null ? trans[2].JointName : "",
                    JointAddressFourth1 = trans.Count > 3 && trans[3] != null ? trans[3].JointAddress : "",
                    JointNameFourth1 = trans.Count > 3 && trans[3] != null ? trans[3].JointName : "",
                    PositionJointFirst1 = trans.Count > 0 && trans[0] != null ? trans[0].PositionJoint : "",
                    PositionJointSecond1 = trans.Count > 1 && trans[1] != null ? trans[1].PositionJoint : "",
                    PositionJointThird1 = trans.Count > 2 && trans[2] != null ? trans[2].PositionJoint : "",
                    PositionJointFourth1 = trans.Count > 3 && trans[3] != null ? trans[3].PositionJoint : "",
                    JointAddressFirst2 = trans.Count > 0 && trans[0] != null ? trans[0].JointAddress : "",
                    JointNameFirst2 = trans.Count > 0 && trans[0] != null ? trans[0].JointName : "",
                    JointAddressSecond2 = trans.Count > 1 && trans[1] != null ? trans[1].JointAddress : "",
                    JointNameSecond2 = trans.Count > 1 && trans[1] != null ? trans[1].JointName : "",
                    JointAddressThird2 = trans.Count > 2 && trans[2] != null ? trans[2].JointAddress : "",
                    JointNameThird2 = trans.Count > 2 && trans[2] != null ? trans[2].JointName : "",
                    JointAddressFourth2 = trans.Count > 3 && trans[3] != null ? trans[3].JointAddress : "",
                    JointNameFourth2 = trans.Count > 3 && trans[3] != null ? trans[3].JointName : "",
                    PositionJointFirst2 = trans.Count > 0 && trans[0] != null ? trans[0].PositionJoint : "",
                    PositionJointSecond2 = trans.Count > 1 && trans[1] != null ? trans[1].PositionJoint : "",
                    PositionJointThird2 = trans.Count > 2 && trans[2] != null ? trans[2].PositionJoint : "",
                    PositionJointFourth2 = trans.Count > 3 && trans[3] != null ? trans[3].PositionJoint : "",
                    JointAddressFirst3 = trans.Count > 0 && trans[0] != null ? trans[0].JointAddress : "",
                    JointNameFirst3 = trans.Count > 0 && trans[0] != null ? trans[0].JointName : "",
                    JointAddressSecond3 = trans.Count > 1 && trans[1] != null ? trans[1].JointAddress : "",
                    JointNameSecond3 = trans.Count > 1 && trans[1] != null ? trans[1].JointName : "",
                    JointAddressThird3 = trans.Count > 2 && trans[2] != null ? trans[2].JointAddress : "",
                    JointNameThird3 = trans.Count > 2 && trans[2] != null ? trans[2].JointName : "",
                    JointAddressFourth3 = trans.Count > 3 && trans[3] != null ? trans[3].JointAddress : "",
                    JointNameFourth3 = trans.Count > 3 && trans[3] != null ? trans[3].JointName : "",
                    PositionJointFirst3 = trans.Count > 0 && trans[0] != null ? trans[0].PositionJoint : "",
                    PositionJointSecond3 = trans.Count > 1 && trans[1] != null ? trans[1].PositionJoint : "",
                    PositionJointThird3 = trans.Count > 2 && trans[2] != null ? trans[2].PositionJoint : "",
                    PositionJointFourth3 = trans.Count > 3 && trans[3] != null ? trans[3].PositionJoint : "",
                    JointAddressFirst4 = trans.Count > 0 && trans[0] != null ? trans[0].JointAddress : "",
                    JointNameFirst4 = trans.Count > 0 && trans[0] != null ? trans[0].JointName : "",
                    JointAddressSecond4 = trans.Count > 1 && trans[1] != null ? trans[1].JointAddress : "",
                    JointNameSecond4 = trans.Count > 1 && trans[1] != null ? trans[1].JointName : "",
                    JointAddressThird4 = trans.Count > 2 && trans[2] != null ? trans[2].JointAddress : "",
                    JointNameThird4 = trans.Count > 2 && trans[2] != null ? trans[2].JointName : "",
                    JointAddressFourth4 = trans.Count > 3 && trans[3] != null ? trans[3].JointAddress : "",
                    JointNameFourth4 = trans.Count > 3 && trans[3] != null ? trans[3].JointName : "",
                    PositionJointFirst4 = trans.Count > 0 && trans[0] != null ? trans[0].PositionJoint : "",
                    PositionJointSecond4 = trans.Count > 1 && trans[1] != null ? trans[1].PositionJoint : "",
                    PositionJointThird4 = trans.Count > 2 && trans[2] != null ? trans[2].PositionJoint : "",
                    PositionJointFourth4 = trans.Count > 3 && trans[3] != null ? trans[3].PositionJoint : "",
                    TextJVDetail1 = trans.Count > 0 && trans[0] != null ? trans[0].TextJVDetail1 : "",
                    OperatedbyFirst1 = trans.Count > 0 && trans[0] != null ? trans[0].OperatedBy : "",
                    OperatedbySecond1 = trans.Count > 1 && trans[1] != null ? trans[1].OperatedBy : "",
                    OperatedbyThird1 = trans.Count > 2 && trans[2] != null ? trans[2].OperatedBy : "",
                    OperatedbyFourth1 = trans.Count > 3 && trans[3] != null ? trans[3].OperatedBy : "",
                };                
                return mainAgreementJVBookmarkView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }

        public QJointVentureTrans1 GetQJointVentureTrans1(Guid refGUID)
        {
            try
            {
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                var g_result = guarantorAgreementTableRepo.GetGuarantorAgreementTableByIdNoTracking(refGUID);

                QJointVentureTrans1 result = (from jointVentureTrans in Entity

                                              where jointVentureTrans.RefGUID == g_result.MainAgreementTableGUID
                                              select new QJointVentureTrans1
                                              {
                                                  QJointVentureTrans1_GuarantorAgreement_Name = jointVentureTrans.Name,
                                                  QJointVentureTrans1_GuarantorAgreement_Address = jointVentureTrans.Address

                                              }).FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public QJointVentureTrans2 GetQJointVentureTrans2(Guid refGUID)
        {
            try
            {
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                var g_result = guarantorAgreementTableRepo.GetGuarantorAgreementTableByIdNoTracking(refGUID);

                QJointVentureTrans2 result = (from jointVentureTrans in Entity

                                              where jointVentureTrans.RefGUID == g_result.MainAgreementTableGUID
                                              select new QJointVentureTrans2
                                              {
                                                  QJointVentureTrans2_GuarantorAgreement_Name = jointVentureTrans.Name,
                                                  QJointVentureTrans2_GuarantorAgreement_Address = jointVentureTrans.Address

                                              }).FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public QJointVentureTrans3 GetQJointVentureTrans3(Guid refGUID)
        {
            try
            {
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                var g_result = guarantorAgreementTableRepo.GetGuarantorAgreementTableByIdNoTracking(refGUID);

                QJointVentureTrans3 result = (from jointVentureTrans in Entity

                                              where jointVentureTrans.RefGUID == g_result.MainAgreementTableGUID
                                              select new QJointVentureTrans3
                                              {
                                                  QJointVentureTrans3_GuarantorAgreement_Name = jointVentureTrans.Name,
                                                  QJointVentureTrans3_GuarantorAgreement_Address = jointVentureTrans.Address

                                              }).FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }
}

