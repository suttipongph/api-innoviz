using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ILedgerFiscalYearRepo
	{
		#region DropDown
		IEnumerable<SelectItem<LedgerFiscalYearItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<LedgerFiscalYearListView> GetListvw(SearchParameter search);
		LedgerFiscalYearItemView GetByIdvw(Guid id);
		LedgerFiscalYear Find(params object[] keyValues);
		LedgerFiscalYear GetLedgerFiscalYearByIdNoTracking(Guid guid);
		LedgerFiscalYear CreateLedgerFiscalYear(LedgerFiscalYear ledgerFiscalYear);
		void CreateLedgerFiscalYearVoid(LedgerFiscalYear ledgerFiscalYear);
		LedgerFiscalYear UpdateLedgerFiscalYear(LedgerFiscalYear dbLedgerFiscalYear, LedgerFiscalYear inputLedgerFiscalYear, List<string> skipUpdateFields = null);
		void UpdateLedgerFiscalYearVoid(LedgerFiscalYear dbLedgerFiscalYear, LedgerFiscalYear inputLedgerFiscalYear, List<string> skipUpdateFields = null);
		void Remove(LedgerFiscalYear item);
		LedgerFiscalYear GetLedgerFiscalYearByIdNoTrackingByAccessLevel(Guid guid);
		LedgerFiscalYear GetMaxEndDate(Guid companyGUID);
	}
	public class LedgerFiscalYearRepo : CompanyBaseRepository<LedgerFiscalYear>, ILedgerFiscalYearRepo
	{
		public LedgerFiscalYearRepo(SmartAppDbContext context) : base(context) { }
		public LedgerFiscalYearRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public LedgerFiscalYear GetLedgerFiscalYearByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.LedgerFiscalYearGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<LedgerFiscalYearItemViewMap> GetDropDownQuery()
		{
			return (from ledgerFiscalYear in Entity
					select new LedgerFiscalYearItemViewMap
					{
						CompanyGUID = ledgerFiscalYear.CompanyGUID,
						Owner = ledgerFiscalYear.Owner,
						OwnerBusinessUnitGUID = ledgerFiscalYear.OwnerBusinessUnitGUID,
						LedgerFiscalYearGUID = ledgerFiscalYear.LedgerFiscalYearGUID,
						FiscalYearId = ledgerFiscalYear.FiscalYearId,
						Description = ledgerFiscalYear.Description
					});
		}
		public IEnumerable<SelectItem<LedgerFiscalYearItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<LedgerFiscalYear>(search, SysParm.CompanyGUID);
				var ledgerFiscalYear = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<LedgerFiscalYearItemViewMap, LedgerFiscalYearItemView>().ToDropDownItem(search.ExcludeRowData);


				return ledgerFiscalYear;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<LedgerFiscalYearListViewMap> GetListQuery()
		{
			return (from ledgerFiscalYear in Entity
				select new LedgerFiscalYearListViewMap
				{
						LedgerFiscalYearGUID = ledgerFiscalYear.LedgerFiscalYearGUID,
						FiscalYearId = ledgerFiscalYear.FiscalYearId,
						Description = ledgerFiscalYear.Description,
						LengthOfPeriod = ledgerFiscalYear.LengthOfPeriod,
						PeriodUnit = ledgerFiscalYear.PeriodUnit,
						StartDate = ledgerFiscalYear.StartDate,
						EndDate = ledgerFiscalYear.EndDate
				});
		}
		public SearchResult<LedgerFiscalYearListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<LedgerFiscalYearListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<LedgerFiscalYear>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<LedgerFiscalYearListViewMap, LedgerFiscalYearListView>();
				result = list.SetSearchResult<LedgerFiscalYearListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<LedgerFiscalYearItemViewMap> GetItemQuery()
		{
			return (from ledgerFiscalYear in Entity
					join company in db.Set<Company>()
					on ledgerFiscalYear.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on ledgerFiscalYear.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljLedgerFiscalYearOwnerBU
					from ownerBU in ljLedgerFiscalYearOwnerBU.DefaultIfEmpty()
					select new LedgerFiscalYearItemViewMap
					{
						CompanyGUID = ledgerFiscalYear.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = ledgerFiscalYear.Owner,
						OwnerBusinessUnitGUID = ledgerFiscalYear.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = ledgerFiscalYear.CreatedBy,
						CreatedDateTime = ledgerFiscalYear.CreatedDateTime,
						ModifiedBy = ledgerFiscalYear.ModifiedBy,
						ModifiedDateTime = ledgerFiscalYear.ModifiedDateTime,
						LedgerFiscalYearGUID = ledgerFiscalYear.LedgerFiscalYearGUID,
						Description = ledgerFiscalYear.Description,
						EndDate = ledgerFiscalYear.EndDate,
						FiscalYearId = ledgerFiscalYear.FiscalYearId,
						LengthOfPeriod = ledgerFiscalYear.LengthOfPeriod,
						PeriodUnit = ledgerFiscalYear.PeriodUnit,
						StartDate = ledgerFiscalYear.StartDate,
					
						RowVersion = ledgerFiscalYear.RowVersion,
					});
		}
		public LedgerFiscalYearItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<LedgerFiscalYearItemViewMap, LedgerFiscalYearItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public LedgerFiscalYear CreateLedgerFiscalYear(LedgerFiscalYear ledgerFiscalYear)
		{
			try
			{
				ledgerFiscalYear.LedgerFiscalYearGUID = Guid.NewGuid();
				base.Add(ledgerFiscalYear);
				return ledgerFiscalYear;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateLedgerFiscalYearVoid(LedgerFiscalYear ledgerFiscalYear)
		{
			try
			{
				CreateLedgerFiscalYear(ledgerFiscalYear);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public LedgerFiscalYear UpdateLedgerFiscalYear(LedgerFiscalYear dbLedgerFiscalYear, LedgerFiscalYear inputLedgerFiscalYear, List<string> skipUpdateFields = null)
		{
			try
			{
				dbLedgerFiscalYear = dbLedgerFiscalYear.MapUpdateValues<LedgerFiscalYear>(inputLedgerFiscalYear);
				base.Update(dbLedgerFiscalYear);
				return dbLedgerFiscalYear;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateLedgerFiscalYearVoid(LedgerFiscalYear dbLedgerFiscalYear, LedgerFiscalYear inputLedgerFiscalYear, List<string> skipUpdateFields = null)
		{
			try
			{
				dbLedgerFiscalYear = dbLedgerFiscalYear.MapUpdateValues<LedgerFiscalYear>(inputLedgerFiscalYear, skipUpdateFields);
				base.Update(dbLedgerFiscalYear);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public override void ValidateRemove(LedgerFiscalYear item )
		{
			DateTime maxEndDate = GetMaxEndDate(item.CompanyGUID.ToString());
			ILedgerFiscalPeriodRepo ledgerFiscalPeriodRepo = new LedgerFiscalPeriodRepo(db);
			IEnumerable<LedgerFiscalPeriod> ledgerFiscalPeriods = ledgerFiscalPeriodRepo.GetLedgerFiscalPeriodByLedgerFiscalYearGUID(item.LedgerFiscalYearGUID);
			if (item.EndDate != maxEndDate  )
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				ex.AddData("ERROR.00278");
				throw SmartAppUtil.AddStackTrace(ex);
            }
			if (ledgerFiscalPeriods.Any()) {
				if (ledgerFiscalPeriods.Any(a => a.PeriodStatus != (int)LedgerFiscalPeriodStatus.Open)) 
				{
					SmartAppException ex = new SmartAppException("ERROR.ERROR");
					ex.AddData("ERROR.00278");
					throw SmartAppUtil.AddStackTrace(ex);
				}
				
			}
		}

		public DateTime GetMaxEndDate(string companyGUID)
        {
            try
            {
                Guid guid = new Guid(companyGUID);
                return Entity.Where(a => a.CompanyGUID == guid).DefaultIfEmpty().Max(m => m == null ? DateTime.MinValue : m.EndDate);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Create, Update, Delete
        public LedgerFiscalYear GetLedgerFiscalYearByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.LedgerFiscalYearGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public LedgerFiscalYear GetMaxEndDate(Guid companyGUID)
		{
			try
			{
				return Entity.Where(a => a.CompanyGUID == companyGUID).OrderByDescending(o => o.EndDate).FirstOrDefault();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
	}
}

