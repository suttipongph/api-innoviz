using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IProcessTransRepo
	{
		#region DropDown
		IEnumerable<SelectItem<ProcessTransItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<ProcessTransListView> GetListvw(SearchParameter search);
		ProcessTransItemView GetByIdvw(Guid id);
		ProcessTrans Find(params object[] keyValues);
		ProcessTrans GetProcessTransByIdNoTracking(Guid guid);
		ProcessTrans CreateProcessTrans(ProcessTrans processTrans);
		void CreateProcessTransVoid(ProcessTrans processTrans);
		ProcessTrans UpdateProcessTrans(ProcessTrans dbProcessTrans, ProcessTrans inputProcessTrans, List<string> skipUpdateFields = null);
		void UpdateProcessTransVoid(ProcessTrans dbProcessTrans, ProcessTrans inputProcessTrans, List<string> skipUpdateFields = null);
		void Remove(ProcessTrans item);
		ProcessTrans GetProcessTransByIdNoTrackingByAccessLevel(Guid guid);
		List<ProcessTrans> GetProcessTransVendorPaymentByVendorPaymentRefTypeRefGUID(int refType, Guid refGUID);
		int GetCountProcessTransVendorPaymentByVendorPaymentRefTypeRefGUID(int refType, Guid refGUID);
		List<ProcessTrans> GetProcessTransForGenerateStaging(List<int> processTransType, List<int> stagingBatchStatus, List<Guid> refGUID = null);
		List<ProcessTrans> GetProcessTransByIdNoTracking(List<Guid> guids);
	}
	public class ProcessTransRepo : CompanyBaseRepository<ProcessTrans>, IProcessTransRepo
	{
		public ProcessTransRepo(SmartAppDbContext context) : base(context) { }
		public ProcessTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ProcessTrans GetProcessTransByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ProcessTransGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<ProcessTransItemViewMap> GetDropDownQuery()
		{
			return (from processTrans in Entity
					select new ProcessTransItemViewMap
					{
						CompanyGUID = processTrans.CompanyGUID,
						Owner = processTrans.Owner,
						OwnerBusinessUnitGUID = processTrans.OwnerBusinessUnitGUID,
						ProcessTransGUID = processTrans.ProcessTransGUID,
						DocumentId = processTrans.DocumentId,
						ProcessTransType = processTrans.ProcessTransType,
					});
		}
		public IEnumerable<SelectItem<ProcessTransItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<ProcessTrans>(search, SysParm.CompanyGUID);
				var processTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
                    .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                    .Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
                    .ToMaps<ProcessTransItemViewMap, ProcessTransItemView>().ToDropDownItem(search.ExcludeRowData);

				return processTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<ProcessTransListViewMap> GetListQuery()
		{
			return (from processTrans in Entity
					join customer in db.Set<CustomerTable>()
					on processTrans.CustomerTableGUID equals customer.CustomerTableGUID into ljprocesstrans
					from customer in ljprocesstrans.DefaultIfEmpty()
					join creditapp in db.Set<CreditAppTable>()
					on processTrans.CreditAppTableGUID equals creditapp.CreditAppTableGUID into ljcreditapp
					from creditapp in ljcreditapp.DefaultIfEmpty()

					join invoiceTable in db.Set<InvoiceTable>()
					on processTrans.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljinvoiceTable
					from invoiceTable in ljinvoiceTable.DefaultIfEmpty()

					join withdrawalTable in db.Set<WithdrawalTable>()
					on processTrans.RefGUID equals withdrawalTable.WithdrawalTableGUID into ljwithdrawal
					from withdrawalTable in ljwithdrawal.DefaultIfEmpty()

					join customerrefundTable in db.Set<CustomerRefundTable>()
					on processTrans.RefGUID equals customerrefundTable.CustomerRefundTableGUID into customerrefund
					from customerrefundTable in customerrefund.DefaultIfEmpty()

					join mainagreementTable in db.Set<MainAgreementTable>()
					on processTrans.RefGUID equals mainagreementTable.MainAgreementTableGUID into ijmainagreement
					from mainagreementTable in ijmainagreement.DefaultIfEmpty()

					join businesscollateralagmTable in db.Set<BusinessCollateralAgmTable>()
					on processTrans.RefGUID equals businesscollateralagmTable.BusinessCollateralAgmTableGUID into ijbusinesscollateralagm
					from businesscollateralagmTable in ijbusinesscollateralagm.DefaultIfEmpty()

					join assignmentagreementTable in db.Set<AssignmentAgreementTable>()
					on processTrans.RefGUID equals assignmentagreementTable.AssignmentAgreementTableGUID into ijassignmentagreement
					from assignmentagreementTable in ijassignmentagreement.DefaultIfEmpty()

					join purchaseTable in db.Set<PurchaseTable>()
					on processTrans.RefGUID equals purchaseTable.PurchaseTableGUID into ijpurchase
					from purchaseTable in ijpurchase.DefaultIfEmpty()

					join messengerjopTable in db.Set<MessengerJobTable>()
					on processTrans.RefGUID equals messengerjopTable.MessengerJobTableGUID into ijmessengerjop
					from messengerjopTable in ijmessengerjop.DefaultIfEmpty()

					join receipttempTable in db.Set<ReceiptTempTable>()
					on processTrans.RefGUID equals receipttempTable.ReceiptTempTableGUID into ijreceipttemp
					from receipttempTable in ijreceipttemp.DefaultIfEmpty()

					join vendorPaymentTrans in db.Set<VendorPaymentTrans>()
					on processTrans.RefGUID equals vendorPaymentTrans.VendorPaymentTransGUID into ijvendorPaymentTrans
					from vendorPaymentTrans in ijvendorPaymentTrans.DefaultIfEmpty()
					join vendorTable in db.Set<VendorTable>()
					on vendorPaymentTrans.VendorTableGUID equals vendorTable.VendorTableGUID into ijvendorTable
					from vendorTable in ijvendorTable.DefaultIfEmpty()

					join interestRealizedTrans in db.Set<InterestRealizedTrans>()
					on processTrans.RefGUID equals interestRealizedTrans.InterestRealizedTransGUID into ijinterestRealizedTrans
					from interestRealizedTrans in ijinterestRealizedTrans.DefaultIfEmpty()

					select new ProcessTransListViewMap
				{
						CompanyGUID = processTrans.CompanyGUID,
						Owner = processTrans.Owner,
						OwnerBusinessUnitGUID = processTrans.OwnerBusinessUnitGUID,
						ProcessTransGUID = processTrans.ProcessTransGUID,
						CustomerTableGUID = processTrans.CustomerTableGUID,
						StagingBatchStatus = processTrans.StagingBatchStatus,
						CreditAppTableGUID = processTrans.CreditAppTableGUID,
						ProductType = processTrans.ProductType,
						TransDate = processTrans.TransDate,
						ProcessTransType = processTrans.ProcessTransType,
						Amount = processTrans.Amount,
						TaxAmount = processTrans.TaxAmount,
						Revert = processTrans.Revert,
						StagingBatchId = processTrans.StagingBatchId,
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customer.CustomerId,customer.Name),
						CreditAppTable_Values =SmartAppUtil.GetDropDownLabel(creditapp.CreditAppId,creditapp.Description),
						InvoiceTableGUID = processTrans.InvoiceTableGUID,
						InvoiceTable_Values =SmartAppUtil.GetDropDownLabel(invoiceTable.InvoiceId,invoiceTable.IssuedDate.DateToString()),
						RefId = (withdrawalTable != null && processTrans.RefType == (int)RefType.WithdrawalTable) ? withdrawalTable.WithdrawalId :
						(customerrefundTable != null && processTrans.RefType == (int)RefType.CustomerRefund) ? customerrefundTable.CustomerRefundId :
						(mainagreementTable != null && processTrans.RefType == (int)RefType.MainAgreement) ? mainagreementTable.InternalMainAgreementId :
						(businesscollateralagmTable != null && processTrans.RefType == (int)RefType.BusinessCollateralAgreement) ? businesscollateralagmTable.InternalBusinessCollateralAgmId :
						(assignmentagreementTable != null && processTrans.RefType == (int)RefType.AssignmentAgreement) ? assignmentagreementTable.InternalAssignmentAgreementId :
						(messengerjopTable != null && processTrans.RefType == (int)RefType.MessengerJob) ? messengerjopTable.JobId :
						(purchaseTable != null && processTrans.RefType == (int)RefType.PurchaseTable) ? purchaseTable.PurchaseId :
						(receipttempTable != null && processTrans.RefType == (int)RefType.ReceiptTemp) ? receipttempTable.ReceiptTempId :
						(vendorPaymentTrans != null && processTrans.RefType == (int)RefType.VendorPaymentTrans) ? (vendorTable != null ? vendorTable.VendorId : null) :
						(interestRealizedTrans != null && processTrans.RefType == (int)RefType.InterestRealizedTrans) ? interestRealizedTrans.DocumentId :
						null,
						RefType = processTrans.RefType.ToString(),
						InvoiceTable_InvoiceId = invoiceTable.InvoiceId,
						CreditAppTable_CreditappId = creditapp.CreditAppId,
						CustomerTable_CustomerId = customer.CustomerId,
						RefGUID = processTrans.RefGUID.ToString()
					});
		}
		public SearchResult<ProcessTransListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<ProcessTransListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<ProcessTrans>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<ProcessTransListViewMap, ProcessTransListView>();
				result = list.SetSearchResult<ProcessTransListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<ProcessTransItemViewMap> GetItemQuery()
		{

			return (from processTrans in Entity
					join company in db.Set<Company>()
					on processTrans.CompanyGUID equals company.CompanyGUID

					join ownerBU in db.Set<BusinessUnit>()
					on processTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljProcessTransOwnerBU
					from ownerBU in ljProcessTransOwnerBU.DefaultIfEmpty()

					join customer in db.Set<CustomerTable>()
					on processTrans.CustomerTableGUID equals customer.CustomerTableGUID into ljProcessTranscustomer
					from customer in ljProcessTranscustomer.DefaultIfEmpty()

					join currency in db.Set<Currency>()
					on processTrans.CurrencyGUID equals currency.CurrencyGUID into ljProcessTransCurrrency
					from currency in ljProcessTransCurrrency.DefaultIfEmpty()

					join creditapp in db.Set<CreditAppTable>()
					on processTrans.CreditAppTableGUID equals creditapp.CreditAppTableGUID into ljProcessTransCreditAppTable
					from creditapp in ljProcessTransCreditAppTable.DefaultIfEmpty()

					join reason in db.Set<DocumentReason>()
					on processTrans.DocumentReasonGUID equals reason.DocumentReasonGUID into ljProcesstransReason
					from reason in ljProcesstransReason.DefaultIfEmpty()

					join taxcode in db.Set<TaxTable>()
					on processTrans.TaxTableGUID equals taxcode.TaxTableGUID into ljProcesstranstaxcode
					from taxcode in ljProcesstranstaxcode.DefaultIfEmpty()

					join ori_taxcode in db.Set<TaxTable>()
					on processTrans.OrigTaxTableGUID equals ori_taxcode.TaxTableGUID into ljProcesstransOritaxcode
					from ori_taxcode in ljProcesstransOritaxcode.DefaultIfEmpty()

					join paymentHistory in db.Set<PaymentHistory>()
					on processTrans.PaymentHistoryGUID equals paymentHistory.PaymentHistoryGUID into ljpaymenthistory
					from paymentHistory in ljpaymenthistory.DefaultIfEmpty()

					join invoiceTable in db.Set<InvoiceTable>()
					on paymentHistory.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljinvoiceTable
					from invoiceTable in ljinvoiceTable.DefaultIfEmpty()

					join taxinvioce in db.Set<TaxInvoiceTable>()
					on processTrans.RefTaxInvoiceGUID equals taxinvioce.TaxInvoiceTableGUID into ljtaxinvoice
					from taxinvioce in ljtaxinvoice.DefaultIfEmpty()

					join invoiceTableRef in db.Set<InvoiceTable>()
					on processTrans.RefGUID equals invoiceTableRef.InvoiceTableGUID into ljinvoiceRefTabel
					from invoiceTableRef in ljinvoiceRefTabel.DefaultIfEmpty()

					join receipttemp in db.Set<ReceiptTempTable>()
					on processTrans.RefGUID equals receipttemp.ReceiptTempTableGUID into ljreceipttempTabel
					from receipttemp in ljreceipttempTabel.DefaultIfEmpty()

					join invoiceTable_Value in db.Set<InvoiceTable>()
					on processTrans.InvoiceTableGUID equals invoiceTable_Value.InvoiceTableGUID into ljinvoiceTable_Value
					from invoiceTable_Value in ljinvoiceTable_Value.DefaultIfEmpty()

					join withdrawalTable in db.Set<WithdrawalTable>()
					on processTrans.RefGUID equals withdrawalTable.WithdrawalTableGUID into ljwithdrawal
					from withdrawalTable in ljwithdrawal.DefaultIfEmpty()

					join customerrefundTable in db.Set<CustomerRefundTable>()
					on processTrans.RefGUID equals customerrefundTable.CustomerRefundTableGUID into customerrefund
					from customerrefundTable in customerrefund.DefaultIfEmpty()

					join mainagreementTable in db.Set<MainAgreementTable>()
					on processTrans.RefGUID equals mainagreementTable.MainAgreementTableGUID into ijmainagreement
					from mainagreementTable in ijmainagreement.DefaultIfEmpty()

					join businesscollateralagmTable in db.Set<BusinessCollateralAgmTable>()
					on processTrans.RefGUID equals businesscollateralagmTable.BusinessCollateralAgmTableGUID into ijbusinesscollateralagm
					from businesscollateralagmTable in ijbusinesscollateralagm.DefaultIfEmpty()

					join assignmentagreementTable in db.Set<AssignmentAgreementTable>()
					on processTrans.RefGUID equals assignmentagreementTable.AssignmentAgreementTableGUID into ijassignmentagreement
					from assignmentagreementTable in ijassignmentagreement.DefaultIfEmpty()

					join purchaseTable in db.Set<PurchaseTable>()
					on processTrans.RefGUID equals purchaseTable.PurchaseTableGUID into ijpurchase
					from purchaseTable in ijpurchase.DefaultIfEmpty()

					join messengerjopTable in db.Set<MessengerJobTable>()
					on processTrans.RefGUID equals messengerjopTable.MessengerJobTableGUID into ijmessengerjop
					from messengerjopTable in ijmessengerjop.DefaultIfEmpty()

					join receipttempTable in db.Set<ReceiptTempTable>()
					on processTrans.RefGUID equals receipttempTable.ReceiptTempTableGUID into ijreceipttemp
					from receipttempTable in ijreceipttemp.DefaultIfEmpty()

					join vendorPaymentTrans in db.Set<VendorPaymentTrans>()
					on processTrans.RefGUID equals vendorPaymentTrans.VendorPaymentTransGUID into ijvendorPaymentTrans
					from vendorPaymentTrans in ijvendorPaymentTrans.DefaultIfEmpty()
					join vendorTable in db.Set<VendorTable>()
					on vendorPaymentTrans.VendorTableGUID equals vendorTable.VendorTableGUID into ijvendorTable
					from vendorTable in ijvendorTable.DefaultIfEmpty()

					join interestRealizedTrans in db.Set<InterestRealizedTrans>()
					on processTrans.RefGUID equals interestRealizedTrans.InterestRealizedTransGUID into ijinterestRealizedTrans
					from interestRealizedTrans in ijinterestRealizedTrans.DefaultIfEmpty()

					join paymentDetail in db.Set<PaymentDetail>()
					on processTrans.PaymentDetailGUID equals paymentDetail.PaymentDetailGUID into ljpaymentDetail
					from paymentDetail in ljpaymentDetail.DefaultIfEmpty()

					join invioceType in db.Set<InvoiceType>()
					on paymentDetail.InvoiceTypeGUID equals invioceType.InvoiceTypeGUID into ljinvioceType
					from invioceType in ljinvioceType.DefaultIfEmpty()

					join refProcessTrans in db.Set<ProcessTrans>()
					on processTrans.RefProcessTransGUID equals refProcessTrans.ProcessTransGUID into ljrefProcessTrans
					from refProcessTrans in ljrefProcessTrans.DefaultIfEmpty()
					select new ProcessTransItemViewMap
					{
						CompanyGUID = processTrans.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = processTrans.Owner,
						OwnerBusinessUnitGUID = processTrans.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = processTrans.CreatedBy,
						CreatedDateTime = processTrans.CreatedDateTime,
						ModifiedBy = processTrans.ModifiedBy,
						ModifiedDateTime = processTrans.ModifiedDateTime,
						ProcessTransGUID = processTrans.ProcessTransGUID,
						Amount = processTrans.Amount,
						AmountMST = processTrans.AmountMST,
						ARLedgerAccount = processTrans.ARLedgerAccount,
						CreditAppTableGUID = processTrans.CreditAppTableGUID,
						CurrencyGUID = currency.CurrencyGUID,
						CustomerTableGUID = customer.CustomerTableGUID,
						DocumentId = processTrans.DocumentId,
						DocumentReasonGUID = processTrans.DocumentReasonGUID,
						ExchangeRate = processTrans.ExchangeRate,
						OrigTaxTableGUID = processTrans.OrigTaxTableGUID,
						OriginalTax_Values = SmartAppUtil.GetDropDownLabel(ori_taxcode.TaxCode, ori_taxcode.Description),
						PaymentHistoryGUID = processTrans.PaymentHistoryGUID,
						PaymentDetailGUID = processTrans.PaymentDetailGUID,
						ProcessTransType = processTrans.ProcessTransType,
						ProductType = processTrans.ProductType,
						SourceRefType = processTrans.SourceRefType,
						SourceRefId = processTrans.SourceRefId,
						RefId = (withdrawalTable != null && processTrans.RefType == (int)RefType.WithdrawalTable) ? withdrawalTable.WithdrawalId:
						(customerrefundTable != null && processTrans.RefType == (int)RefType.CustomerRefund) ? customerrefundTable.CustomerRefundId:
						(mainagreementTable != null && processTrans.RefType == (int)RefType.MainAgreement) ? mainagreementTable.InternalMainAgreementId:
						(businesscollateralagmTable != null && processTrans.RefType == (int)RefType.BusinessCollateralAgreement) ? businesscollateralagmTable.InternalBusinessCollateralAgmId:
						(assignmentagreementTable != null && processTrans.RefType == (int)RefType.AssignmentAgreement) ? assignmentagreementTable.InternalAssignmentAgreementId:
						(messengerjopTable != null && processTrans.RefType == (int)RefType.MessengerJob) ? messengerjopTable.JobId:
						(purchaseTable != null && processTrans.RefType == (int)RefType.PurchaseTable) ? purchaseTable.PurchaseId:
						(receipttempTable != null && processTrans.RefType == (int) RefType.ReceiptTemp) ? receipttempTable.ReceiptTempId:
						(vendorPaymentTrans != null && processTrans.RefType == (int)RefType.VendorPaymentTrans) ? (vendorTable != null ? vendorTable.VendorId : null) :
						(interestRealizedTrans != null && processTrans.RefType == (int)RefType.InterestRealizedTrans) ? interestRealizedTrans.DocumentId :
						null,
						RefGUID = processTrans.RefGUID,
						RefProcessTransGUID = processTrans.RefProcessTransGUID,
						RefProcessTrans_Values = ( refProcessTrans !=null)? SmartAppUtil.GetDropDownLabel(((ProcessTransType)refProcessTrans.ProcessTransType).ToString(), refProcessTrans.DocumentId):null,
						RefTaxInvoiceGUID = processTrans.RefTaxInvoiceGUID,
						RefTaxInvoice_Values = SmartAppUtil.GetDropDownLabel(taxinvioce.TaxInvoiceId, taxinvioce.IssuedDate.DateToString()),
						RefType = processTrans.RefType,
						Revert = processTrans.Revert,
						StagingBatchId = processTrans.StagingBatchId,
						StagingBatchStatus = processTrans.StagingBatchStatus,
						TaxAmount = processTrans.TaxAmount,
						TaxAmountMST = processTrans.TaxAmountMST,
						TaxTableGUID = processTrans.TaxTableGUID,
						TransDate = processTrans.TransDate,
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customer.CustomerId, customer.Name),
						CreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditapp.CreditAppId, creditapp.Description),
						CurrencyTable_Values = SmartAppUtil.GetDropDownLabel(currency.CurrencyId, currency.Name),
						ReasonTable_Values = SmartAppUtil.GetDropDownLabel(reason.ReasonId, reason.Description),
						TexCode_Values = SmartAppUtil.GetDropDownLabel(taxcode.TaxCode, taxcode.Description),
						History_Values = SmartAppUtil.GetDropDownLabel(invoiceTable.InvoiceId, paymentHistory.PaymentDate.DateToString()),
						InvoiceTableGUID = processTrans.InvoiceTableGUID,
						InvoiceTable_Values = SmartAppUtil.GetDropDownLabel(invoiceTable_Value.InvoiceId, invoiceTable_Value.IssuedDate.DateToString()),
						PaymentDetail_Values = SmartAppUtil.GetDropDownLabel(invioceType.InvoiceTypeId,paymentDetail.PaymentAmount.ToString()),

						RowVersion = processTrans.RowVersion,
					});
		}
		public ProcessTransItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<ProcessTransItemViewMap, ProcessTransItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public ProcessTrans CreateProcessTrans(ProcessTrans processTrans)
		{
			try
			{
				processTrans.ProcessTransGUID = processTrans.ProcessTransGUID == Guid.Empty? Guid.NewGuid(): processTrans.ProcessTransGUID;
				base.Add(processTrans);
				return processTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateProcessTransVoid(ProcessTrans processTrans)
		{
			try
			{
				CreateProcessTrans(processTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ProcessTrans UpdateProcessTrans(ProcessTrans dbProcessTrans, ProcessTrans inputProcessTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbProcessTrans = dbProcessTrans.MapUpdateValues<ProcessTrans>(inputProcessTrans);
				base.Update(dbProcessTrans);
				return dbProcessTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateProcessTransVoid(ProcessTrans dbProcessTrans, ProcessTrans inputProcessTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbProcessTrans = dbProcessTrans.MapUpdateValues<ProcessTrans>(inputProcessTrans, skipUpdateFields);
				base.Update(dbProcessTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #endregion Create, Update, Delete
        #region Function
        public ProcessTrans GetProcessTransByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ProcessTransGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		
        #endregion Function
		private IQueryable<ProcessTrans> GetQueryProcessTransVendorPaymentByVendorPaymentRefTypeRefGUID(int refType, Guid refGUID)
		{
			try
			{
				var result =
					(from processTrans in Entity.Where(w => w.ProcessTransType == (int)ProcessTransType.VendorPayment &&
															w.RefType == (int)RefType.VendorPaymentTrans)
					 join vendorPaymentTrans in db.Set<VendorPaymentTrans>().Where(w => w.RefType == refType && w.RefGUID == refGUID)
					 on processTrans.RefGUID equals vendorPaymentTrans.VendorPaymentTransGUID
					 select processTrans).AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public int GetCountProcessTransVendorPaymentByVendorPaymentRefTypeRefGUID(int refType, Guid refGUID)
		{
			try
			{
				var result = GetQueryProcessTransVendorPaymentByVendorPaymentRefTypeRefGUID(refType, refGUID).Count();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<ProcessTrans> GetProcessTransVendorPaymentByVendorPaymentRefTypeRefGUID(int refType, Guid refGUID)
		{
			try
			{
				var result = GetQueryProcessTransVendorPaymentByVendorPaymentRefTypeRefGUID(refType, refGUID).ToList();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<ProcessTrans> GetProcessTransForGenerateStaging(List<int> processTransType, List<int> stagingBatchStatus, List<Guid> refGUID = null)
		{
            try
            {
				List<ProcessTrans> processTrans = Entity.Where(w => processTransType.Contains(w.ProcessTransType) &&
																	stagingBatchStatus.Contains(w.StagingBatchStatus) &&
																	((refGUID != null && refGUID.Contains(w.RefGUID ?? Guid.Empty)) || refGUID == null || refGUID.Count() == 0))
														.AsNoTracking()
														.ToList();
				return processTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<ProcessTrans> GetProcessTransByIdNoTracking(List<Guid> guids)
		{
			try
			{
				List<ProcessTrans> processTrans = Entity.Where(w => guids.Contains(w.ProcessTransGUID)).AsNoTracking().ToList();
				return processTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}
