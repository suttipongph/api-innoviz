using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IMessengerJobTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<MessengerJobTableItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<MessengerJobTableListView> GetListvw(SearchParameter search);
		MessengerJobTableItemView GetByIdvw(Guid id);
		
		MessengerJobTableItemView GetByIdvwBy(Guid id);
		MessengerJobTable Find(params object[] keyValues);
		MessengerJobTable GetMessengerJobTableByIdNoTracking(Guid guid);
		MessengerJobTable CreateMessengerJobTable(MessengerJobTable messengerJobTable);
		void CreateMessengerJobTableVoid(MessengerJobTable messengerJobTable);
		MessengerJobTable UpdateMessengerJobTable(MessengerJobTable dbMessengerJobTable, MessengerJobTable inputMessengerJobTable, List<string> skipUpdateFields = null);
		void UpdateMessengerJobTableVoid(MessengerJobTable dbMessengerJobTable, MessengerJobTable inputMessengerJobTable, List<string> skipUpdateFields = null);
		void Remove(MessengerJobTable item);
		MessengerJobTable GetMessengerJobTableByIdNoTrackingByAccessLevel(Guid guid);
	}
	public class MessengerJobTableRepo : CompanyBaseRepository<MessengerJobTable>, IMessengerJobTableRepo
	{
		public MessengerJobTableRepo(SmartAppDbContext context) : base(context) { }
		public MessengerJobTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public MessengerJobTable GetMessengerJobTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.MessengerJobTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<MessengerJobTableItemViewMap> GetDropDownQuery()
		{
			return (from messengerJobTable in Entity
					select new MessengerJobTableItemViewMap
					{
						CompanyGUID = messengerJobTable.CompanyGUID,
						Owner = messengerJobTable.Owner,
						OwnerBusinessUnitGUID = messengerJobTable.OwnerBusinessUnitGUID,
						MessengerJobTableGUID = messengerJobTable.MessengerJobTableGUID,
						JobId = messengerJobTable.JobId,
						JobDate = messengerJobTable.JobDate,
						CustomerTableGUID = messengerJobTable.CustomerTableGUID,
						AssignmentAgreementTableGUID = messengerJobTable.AssignmentAgreementTableGUID,
						Result = messengerJobTable.Result
					});
		}
		public IEnumerable<SelectItem<MessengerJobTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<MessengerJobTable>(search, SysParm.CompanyGUID);
				var messengerJobTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<MessengerJobTableItemViewMap, MessengerJobTableItemView>().ToDropDownItem(search.ExcludeRowData);
				return messengerJobTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<MessengerJobTableListViewMap> GetListQuery()
		{
			return (from messengerJobTable in Entity

					join messengerTable in db.Set<MessengerTable>()
					on messengerJobTable.MessengerTableGUID equals messengerTable.MessengerTableGUID into ljMessengerTable
					from messengerTable in ljMessengerTable.DefaultIfEmpty()

					join employeeTable in db.Set<EmployeeTable>()
					on messengerJobTable.RequestorGUID equals employeeTable.EmployeeTableGUID into ljemlooyee
					from employeeTable in ljemlooyee.DefaultIfEmpty()

					join jobtype in db.Set<JobType>()
					on messengerJobTable.JobTypeGUID equals jobtype.JobTypeGUID into ljjobtype
					from jobtype in ljjobtype.DefaultIfEmpty()

					join customerTable in db.Set<CustomerTable>()
					on messengerJobTable.CustomerTableGUID equals customerTable.CustomerTableGUID into ljcustomerTable
					from customerTable in ljcustomerTable.DefaultIfEmpty()

					join buyerTable in db.Set<BuyerTable>()
					on messengerJobTable.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
					from buyerTable in ljbuyerTable.DefaultIfEmpty()

					join documentStatus in db.Set<DocumentStatus>()
					on messengerJobTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus
					from documentStatus in ljdocumentStatus.DefaultIfEmpty()
					select new MessengerJobTableListViewMap
					{
						CompanyGUID = messengerJobTable.CompanyGUID,
						Owner = messengerJobTable.Owner,
						OwnerBusinessUnitGUID = messengerJobTable.OwnerBusinessUnitGUID,
						MessengerJobTableGUID = messengerJobTable.MessengerJobTableGUID,
						JobId = messengerJobTable.JobId,
						BuyerTableGUID = messengerJobTable.BuyerTableGUID,
						JobEndTime = messengerJobTable.JobEndTime.DateTimeNullToTimeString(),
						ContactName = messengerJobTable.ContactName,
						DocumentStatusGUID = messengerJobTable.DocumentStatusGUID,
						JobDate = messengerJobTable.JobDate,
						JobStartTime = messengerJobTable.JobStartTime.DateTimeNullToTimeString(),
						JobTypeGUID = messengerJobTable.JobTypeGUID,
						MessengerTableGUID = messengerJobTable.MessengerTableGUID,
						Priority = messengerJobTable.Priority,
						Result = messengerJobTable.Result,
						RequestorGUID = messengerJobTable.RequestorGUID,
						CustomerTableGUID = messengerJobTable.CustomerTableGUID,
						CustomerTable_Values = (customerTable != null) ? SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name) : string.Empty,
						BuyerTable_Values = (buyerTable != null) ? SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName) : string.Empty,
						JobType_Values = SmartAppUtil.GetDropDownLabel(jobtype.JobTypeId, jobtype.Description),
						MessengerTable_Values = messengerTable.Name,
						DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
						EmployeeTable_Values = SmartAppUtil.GetDropDownLabel(employeeTable.EmployeeId, employeeTable.Name),
						RefType = messengerJobTable.RefType,
						RefGUID = messengerJobTable.RefGUID,
						CustomerTable_CustomerId = (customerTable != null) ? customerTable.CustomerId : string.Empty,
						BuyerTable_BuyerId = (buyerTable != null) ? buyerTable.BuyerId : string.Empty,
						EmployeeTable_EmployeeId = employeeTable.EmployeeId,
						MessengerTable_MessengerId = messengerTable.Name,
						DocumentStatus_StatusId = documentStatus.StatusId,
						JobType_JobTypeId = jobtype.JobTypeId
					});
		}
		public SearchResult<MessengerJobTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<MessengerJobTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<MessengerJobTable>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<MessengerJobTableListViewMap, MessengerJobTableListView>();
				result = list.SetSearchResult<MessengerJobTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<MessengerJobTableItemViewMap> GetItemQuery()
		{
			return (from messengerJobTable in Entity
					join company in db.Set<Company>()
					on messengerJobTable.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on messengerJobTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljMessengerJobTableOwnerBU
					from ownerBU in ljMessengerJobTableOwnerBU.DefaultIfEmpty()
					join documentStatus in db.Set<DocumentStatus>()
					on messengerJobTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
					join messengerJobTableRef in db.Set<MessengerJobTable>()
					on messengerJobTable.RefGUID equals messengerJobTableRef.MessengerJobTableGUID into ljmessengerJobTableRef
					from messengerJobTableRef in ljmessengerJobTableRef.DefaultIfEmpty()
					join purchaseLine in db.Set<PurchaseLine>()
					on messengerJobTable.RefGUID equals purchaseLine.PurchaseLineGUID into ljpurhaseLineTable
					from purchaseLine in ljpurhaseLineTable.DefaultIfEmpty()

					join purchaseTable in db.Set<PurchaseTable>()
					on purchaseLine.PurchaseTableGUID equals purchaseTable.PurchaseTableGUID into ljpurchaseTable
					from purchaseTable in ljpurchaseTable.DefaultIfEmpty()

					join customer_Table in db.Set<CustomerTable>()
					on messengerJobTable.CustomerTableGUID equals customer_Table.CustomerTableGUID into ljcustomerTable
					from customer_Table in ljcustomerTable.DefaultIfEmpty()

					join buyer_table in db.Set<BuyerTable>()
					on messengerJobTable.BuyerTableGUID equals buyer_table.BuyerTableGUID into ljbuyerTable
					from buyer_table in ljbuyerTable.DefaultIfEmpty()

					join withdrawalLine in db.Set<WithdrawalLine>()
					on messengerJobTable.RefGUID equals withdrawalLine.WithdrawalLineGUID into ljwithdrawalLine
					from withdrawalLine in ljwithdrawalLine.DefaultIfEmpty()

					join creditAppLineTable in db.Set<CreditAppLine>()
					on messengerJobTable.RefCreditAppLineGUID equals creditAppLineTable.CreditAppLineGUID into ljcreditAppLineTable
					from creditAppLineTable in ljcreditAppLineTable.DefaultIfEmpty()
					select new MessengerJobTableItemViewMap
					{
						CompanyGUID = messengerJobTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = messengerJobTable.Owner,
						OwnerBusinessUnitGUID = messengerJobTable.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = messengerJobTable.CreatedBy,
						CreatedDateTime = messengerJobTable.CreatedDateTime,
						ModifiedBy = messengerJobTable.ModifiedBy,
						ModifiedDateTime = messengerJobTable.ModifiedDateTime,
						MessengerJobTableGUID = messengerJobTable.MessengerJobTableGUID,
						Address1 = messengerJobTable.Address1,
						Address2 = messengerJobTable.Address2,
						BuyerTableGUID = messengerJobTable.BuyerTableGUID,
						ContactPersonExtension = messengerJobTable.ContactPersonExtension,
						ContactPersonMobile = messengerJobTable.ContactPersonMobile,
						ContactPersonName = messengerJobTable.ContactPersonName,
						ContactPersonPhone = messengerJobTable.ContactPersonPhone,
						ContactPersonPosition = messengerJobTable.ContactPersonPosition,
						ContactTo = messengerJobTable.ContactTo,
						CustomerTableGUID = messengerJobTable.CustomerTableGUID,
						DocumentStatusGUID = messengerJobTable.DocumentStatusGUID,
						FeeAmount = messengerJobTable.FeeAmount,
						JobDate = messengerJobTable.JobDate,
						JobDetail = messengerJobTable.JobDetail,
						JobId = messengerJobTable.JobId,
						JobStartTime = messengerJobTable.JobStartTime,
						JobEndTime = messengerJobTable.JobEndTime,
						JobTypeGUID = messengerJobTable.JobTypeGUID,
						Map = messengerJobTable.Map,
						MessengerTableGUID = messengerJobTable.MessengerTableGUID,
						Priority = messengerJobTable.Priority,
						ProductType = messengerJobTable.ProductType,
						RefCreditAppLineGUID = messengerJobTable.RefCreditAppLineGUID,
						RefCreditAppLine_Values  = SmartAppUtil.GetDropDownLabel(creditAppLineTable.LineNum.IntNullToString(), creditAppLineTable.ApprovedCreditLimitLine.IntNullToString()),
						RefGUID = messengerJobTable.RefGUID,
						RefMessengerJobTableGUID = messengerJobTable.RefMessengerJobTableGUID,
						RefType = messengerJobTable.RefType,
						RequestorGUID = messengerJobTable.RequestorGUID,
						Result = messengerJobTable.Result,
						ResultRemark = messengerJobTable.ResultRemark,
						DocumentStatus_StatusId = documentStatus.StatusId,
						RefID = messengerJobTableRef != null ? SmartAppUtil.GetDropDownLabel(messengerJobTableRef.JobId, messengerJobTableRef.JobDate.DateToString()) :
								purchaseLine != null && messengerJobTable.RefType == (int)RefType.PurchaseLine ? purchaseTable.PurchaseId :
								withdrawalLine != null && messengerJobTable.RefType == (int)RefType.WithdrawalLine ? withdrawalLine.WithdrawalLineType.ToString() : "",
						ContactName = messengerJobTable.ContactName,
						CustomerTable_Value = customer_Table.Name,
						BuyerTable_Value = buyer_table.BuyerName,
						AssignmentAgreementTableGUID = messengerJobTable.AssignmentAgreementTableGUID,
						ContactPersonDepartment = messengerJobTable.ContactPersonDepartment,
						ExpenseAmount = messengerJobTable.ExpenseAmount,
						RowVersion = messengerJobTable.RowVersion,

					});
			
		}
		public MessengerJobTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<MessengerJobTableItemViewMap, MessengerJobTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private IQueryable<MessengerJobTableItemViewMap> GetItemQueryBy()
		{
			var sumServiceFeeTrans = (from messengerJobTable in Entity
									  join servicefeetrans in db.Set<ServiceFeeTrans>()
									  on messengerJobTable.MessengerJobTableGUID equals servicefeetrans.RefGUID into ljServicefeetrans
									  from servicefeetrans in ljServicefeetrans.DefaultIfEmpty()
									  where servicefeetrans.RefType == (int)RefType.MessengerJob
									  group new { FeeAmount =  servicefeetrans.FeeAmount } by servicefeetrans.RefGUID into g
									  select new { MessengerJobTableGUID = g.Key, sumFeeAmount = g.Sum(s => s.FeeAmount) });
			return (from messengerJobTable in Entity
					join company in db.Set<Company>()
					on messengerJobTable.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on messengerJobTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljMessengerJobTableOwnerBU
					from ownerBU in ljMessengerJobTableOwnerBU.DefaultIfEmpty()
					join documentStatus in db.Set<DocumentStatus>()
					on messengerJobTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
					join messengerJobTableRef in db.Set<MessengerJobTable>()
					on messengerJobTable.RefGUID equals messengerJobTableRef.MessengerJobTableGUID into ljmessengerJobTableRef
					from messengerJobTableRef in ljmessengerJobTableRef.DefaultIfEmpty()
					join messengerTable in db.Set<MessengerTable>()
					on messengerJobTable.MessengerTableGUID equals messengerTable.MessengerTableGUID into ljmessengerTable
					from messengerTable in ljmessengerTable.DefaultIfEmpty()
					join vedorTable in db.Set<VendorTable>()
					on messengerTable.VendorTableGUID equals vedorTable.VendorTableGUID into ljvedorTable
					from vedorTable in ljvedorTable.DefaultIfEmpty()

					join jobType in db.Set<JobType>()
					on messengerJobTable.JobTypeGUID equals jobType.JobTypeGUID into ljjobType
					from jobType in ljjobType.DefaultIfEmpty()

					join groupServiceFeeTrans in sumServiceFeeTrans
					on messengerJobTable.MessengerJobTableGUID equals groupServiceFeeTrans.MessengerJobTableGUID into ljgroupServiceFeeTrans
					from groupServiceFeeTrans in ljgroupServiceFeeTrans.DefaultIfEmpty()

					select new MessengerJobTableItemViewMap
					{
						CompanyGUID = messengerJobTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = messengerJobTable.Owner,
						OwnerBusinessUnitGUID = messengerJobTable.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = messengerJobTable.CreatedBy,
						CreatedDateTime = messengerJobTable.CreatedDateTime,
						ModifiedBy = messengerJobTable.ModifiedBy,
						ModifiedDateTime = messengerJobTable.ModifiedDateTime,
						MessengerJobTableGUID = messengerJobTable.MessengerJobTableGUID,
						Address1 = messengerJobTable.Address1,
						Address2 = messengerJobTable.Address2,
						BuyerTableGUID = messengerJobTable.BuyerTableGUID,
						ContactPersonExtension = messengerJobTable.ContactPersonExtension,
						ContactPersonMobile = messengerJobTable.ContactPersonMobile,
						ContactPersonName = messengerJobTable.ContactPersonName,
						ContactPersonPhone = messengerJobTable.ContactPersonPhone,
						ContactPersonPosition = messengerJobTable.ContactPersonPosition,
						ContactTo = messengerJobTable.ContactTo,
						CustomerTableGUID = messengerJobTable.CustomerTableGUID,
						DocumentStatusGUID = messengerJobTable.DocumentStatusGUID,
						FeeAmount = messengerJobTable.FeeAmount,
						JobDate = messengerJobTable.JobDate,
						JobDetail = messengerJobTable.JobDetail,
						JobId = messengerJobTable.JobId,
						JobStartTime = messengerJobTable.JobStartTime,
						JobEndTime = messengerJobTable.JobEndTime,
						JobTypeGUID = messengerJobTable.JobTypeGUID,
						Map = messengerJobTable.Map,
						MessengerTableGUID = messengerJobTable.MessengerTableGUID,
						Priority = messengerJobTable.Priority,
						ProductType = messengerJobTable.ProductType,
						RefCreditAppLineGUID = messengerJobTable.RefCreditAppLineGUID,
						RefGUID = messengerJobTable.RefGUID,
						RefMessengerJobTableGUID = messengerJobTable.RefMessengerJobTableGUID,
						RefType = messengerJobTable.RefType,
						RequestorGUID = messengerJobTable.RequestorGUID,
						Result = messengerJobTable.Result,
						ResultRemark = messengerJobTable.ResultRemark,
						DocumentStatus_StatusId = documentStatus.StatusId,
						RefID = messengerJobTableRef != null ? SmartAppUtil.GetDropDownLabel(messengerJobTableRef.JobId, messengerJobTableRef.JobDate.DateNullToString()) : "",
						MessengerJob_Value = messengerJobTable != null ? SmartAppUtil.GetDropDownLabel(messengerJobTable.JobId, messengerJobTable.JobDate.DateNullToString()) : "",
						JobType_Value = SmartAppUtil.GetDropDownLabel(jobType.JobTypeId, jobType.Description),
						MessengerTable_Value = SmartAppUtil.GetDropDownLabel(messengerTable.Name, vedorTable.VendorId),
						DocumentStatus_Value = documentStatus.Description,
						ServiceFeeAmount = groupServiceFeeTrans != null ? groupServiceFeeTrans.sumFeeAmount : 0
					}) ;
		
		}
		public MessengerJobTableItemView GetByIdvwBy(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQueryBy()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<MessengerJobTableItemViewMap, MessengerJobTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public MessengerJobTable CreateMessengerJobTable(MessengerJobTable messengerJobTable)
		{
			try
			{
				messengerJobTable.MessengerJobTableGUID = Guid.NewGuid();
				base.Add(messengerJobTable);
				return messengerJobTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateMessengerJobTableVoid(MessengerJobTable messengerJobTable)
		{
			try
			{
				CreateMessengerJobTable(messengerJobTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public MessengerJobTable UpdateMessengerJobTable(MessengerJobTable dbMessengerJobTable, MessengerJobTable inputMessengerJobTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbMessengerJobTable = dbMessengerJobTable.MapUpdateValues<MessengerJobTable>(inputMessengerJobTable);
				base.Update(dbMessengerJobTable);
				return dbMessengerJobTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateMessengerJobTableVoid(MessengerJobTable dbMessengerJobTable, MessengerJobTable inputMessengerJobTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbMessengerJobTable = dbMessengerJobTable.MapUpdateValues<MessengerJobTable>(inputMessengerJobTable, skipUpdateFields);
				base.Update(dbMessengerJobTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public MessengerJobTable GetMessengerJobTableByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.MessengerJobTableGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}
