using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICompanyParameterRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CompanyParameterItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<CompanyParameterListView> GetListvw(SearchParameter search);
		CompanyParameterItemView GetByIdvw(Guid id);
		CompanyParameter Find(params object[] keyValues);
		CompanyParameter GetCompanyParameterByIdNoTracking(Guid guid);
		CompanyParameter CreateCompanyParameter(CompanyParameter companyParameter);
		void CreateCompanyParameterVoid(CompanyParameter companyParameter);
		CompanyParameter UpdateCompanyParameter(CompanyParameter dbCompanyParameter, CompanyParameter inputCompanyParameter, List<string> skipUpdateFields = null);
		void UpdateCompanyParameterVoid(CompanyParameter dbCompanyParameter, CompanyParameter inputCompanyParameter, List<string> skipUpdateFields = null);
		void Remove(CompanyParameter item);
		CompanyParameterItemView GetByCompanyIdvw(SearchParameter search);
		CompanyParameter GetCompanyParameterByCompanyNoTracking(Guid companyGUID);
		CompanyParameter GetCompanyParameterByCompanyNoTrackingByAccessLevel(Guid companyGUID);
	}
	public class CompanyParameterRepo : CompanyBaseRepository<CompanyParameter>, ICompanyParameterRepo
	{
		public CompanyParameterRepo(SmartAppDbContext context) : base(context) { }
		public CompanyParameterRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CompanyParameter GetCompanyParameterByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CompanyParameterGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CompanyParameterItemViewMap> GetDropDownQuery()
		{
			return (from companyParameter in Entity
					join currency in db.Set<Currency>()
					on companyParameter.HomeCurrencyGUID equals currency.CurrencyGUID into ljCompanyParametercurrency
					from currency in ljCompanyParametercurrency.DefaultIfEmpty()
					select new CompanyParameterItemViewMap
					{
						CompanyGUID = companyParameter.CompanyGUID,
						Owner = companyParameter.Owner,
						OwnerBusinessUnitGUID = companyParameter.OwnerBusinessUnitGUID,
						CompanyParameterGUID = companyParameter.CompanyParameterGUID,
						HomeCurrencyGUID = companyParameter.HomeCurrencyGUID,
					});
		}
		public IEnumerable<SelectItem<CompanyParameterItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CompanyParameter>(search, SysParm.CompanyGUID);
				var companyParameter = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CompanyParameterItemViewMap, CompanyParameterItemView>().ToDropDownItem(search.ExcludeRowData);


				return companyParameter;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CompanyParameterListViewMap> GetListQuery()
		{
			return (from companyParameter in Entity
					join currency in db.Set<Currency>()
					on companyParameter.HomeCurrencyGUID equals currency.CurrencyGUID into ljCompanyParametercurrency
					from currency in ljCompanyParametercurrency.DefaultIfEmpty()
					select new CompanyParameterListViewMap
				{
						CompanyGUID = companyParameter.CompanyGUID,
						Owner = companyParameter.Owner,
						OwnerBusinessUnitGUID = companyParameter.OwnerBusinessUnitGUID,
						CompanyParameterGUID = companyParameter.CompanyParameterGUID
				});
		}
		public SearchResult<CompanyParameterListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CompanyParameterListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CompanyParameter>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CompanyParameterListViewMap, CompanyParameterListView>();
				result = list.SetSearchResult<CompanyParameterListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CompanyParameterItemViewMap> GetItemQuery()
		{
			return (from companyParameter in Entity
					join company in db.Set<Company>()
					on companyParameter.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on companyParameter.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCompanyParameterOwnerBU
					from ownerBU in ljCompanyParameterOwnerBU.DefaultIfEmpty()
					select new CompanyParameterItemViewMap
					{
						CompanyGUID = companyParameter.CompanyGUID,
						Owner = companyParameter.Owner,
						OwnerBusinessUnitGUID = companyParameter.OwnerBusinessUnitGUID,
						CreatedBy = companyParameter.CreatedBy,
						CreatedDateTime = companyParameter.CreatedDateTime,
						ModifiedBy = companyParameter.ModifiedBy,
						ModifiedDateTime = companyParameter.ModifiedDateTime,
						CompanyParameterGUID = companyParameter.CompanyParameterGUID,
						AgreementType = companyParameter.AgreementType,
						AutoSettleToleranceLedgerAcc = companyParameter.AutoSettleToleranceLedgerAcc,
						BOTRealization = companyParameter.BOTRealization,
						CalcSheetIncAdditionalCost = companyParameter.CalcSheetIncAdditionalCost,
						CalcSheetIncTax = companyParameter.CalcSheetIncTax,
						CollectionFeeMethod = companyParameter.CollectionFeeMethod,
						CollectionFeeMinBalCal = companyParameter.CollectionFeeMinBalCal,
						CollectionInvTypeGUID = companyParameter.CollectionInvTypeGUID,
						CollectionOverdueDay = companyParameter.CollectionOverdueDay,
						CompulsoryInvTypeGUID = companyParameter.CompulsoryInvTypeGUID,
						CompulsoryTaxGUID = companyParameter.CompulsoryTaxGUID,
						DepositReturnInvTypeGUID = companyParameter.DepositReturnInvTypeGUID,
						DiscountMethodOfPaymentGUID = companyParameter.DiscountMethodOfPaymentGUID,
						EarlyPayOffToleranceInvTypeGUID = companyParameter.EarlyPayOffToleranceInvTypeGUID,
						EarlyToleranceMethodOfPaymentGUID = companyParameter.EarlyToleranceMethodOfPaymentGUID,
						EarlyToleranceOverAmount = companyParameter.EarlyToleranceOverAmount,
						EarlyToleranceOverLedgerAcc = companyParameter.EarlyToleranceOverLedgerAcc,
						EarlyToleranceUnderAmount = companyParameter.EarlyToleranceUnderAmount,
						EarlyToleranceUnderLedgerAcc = companyParameter.EarlyToleranceUnderLedgerAcc,
						HomeCurrencyGUID = companyParameter.HomeCurrencyGUID,
						InitInvAdvInvTypeGUID = companyParameter.InitInvAdvInvTypeGUID,
						InitInvDepInvTypeGUID = companyParameter.InitInvDepInvTypeGUID,
						InitInvDownInvTypeGUID = companyParameter.InitInvDownInvTypeGUID,
						InstallAdvInvTypeGUID = companyParameter.InstallAdvInvTypeGUID,
						InstallInvTypeGUID = companyParameter.InstallInvTypeGUID,
						InsuranceInvTypeGUID = companyParameter.InsuranceInvTypeGUID,
						InsuranceTaxGUID = companyParameter.InsuranceTaxGUID,
						InvoiceIssuing = companyParameter.InvoiceIssuing,
						InvoiceIssuingDay = companyParameter.InvoiceIssuingDay,
						LeaseSubType = companyParameter.LeaseSubType,
						LeaseType = companyParameter.LeaseType,
						MaintenanceTaxGUID = companyParameter.MaintenanceTaxGUID,
						MaximumPennyDifference = companyParameter.MaximumPennyDifference,
						NPLMethod = companyParameter.NPLMethod,
						NumberSeqVariable = companyParameter.NumberSeqVariable,
						OverdueType = companyParameter.OverdueType,
						PaymStructureRefNum = companyParameter.PaymStructureRefNum,
						PenaltyBase = companyParameter.PenaltyBase,
						PenaltyCalculationMethod = companyParameter.PenaltyCalculationMethod,
						PenaltyInvTypeGUID = companyParameter.PenaltyInvTypeGUID,
						PennyDifferenceLedgerAcc = companyParameter.PennyDifferenceLedgerAcc,
						Product = companyParameter.Product,
						ProductGroup = companyParameter.ProductGroup,
						ProvisionMethod = companyParameter.ProvisionMethod,
						ProvisionRateBy = companyParameter.ProvisionRateBy,
						RealizedGainExchRateLedgerAcc = companyParameter.RealizedGainExchRateLedgerAcc,
						RealizedLossExchRateLedgerAcc = companyParameter.RealizedLossExchRateLedgerAcc,
						ReceiptGenBy = companyParameter.ReceiptGenBy,
						RecordType = companyParameter.RecordType,
						SettleByPeriod = companyParameter.SettleByPeriod,
						SettlementGraceDay = companyParameter.SettlementGraceDay,
						VehicleTaxInvTypeGUID = companyParameter.VehicleTaxInvTypeGUID,
						VehicleTaxServiceFee = companyParameter.VehicleTaxServiceFee,
						VehicleTaxServiceFeeTaxGUID = companyParameter.VehicleTaxServiceFeeTaxGUID,
						WaiveMethodOfPaymentGUID = companyParameter.WaiveMethodOfPaymentGUID,
						CompanyId = company.CompanyId,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						BuyerReviewedDay = companyParameter.BuyerReviewedDay,
                        ChequeToleranceAmount = companyParameter.ChequeToleranceAmount,
                        CompanyCalendarGroupGUID = companyParameter.CompanyCalendarGroupGUID,
                        BOTCalendarGroupGUID = companyParameter.BOTCalendarGroupGUID,
						CreditReqInvRevenueTypeGUID =companyParameter.CreditReqInvRevenueTypeGUID,
                        DaysPerYear = companyParameter.DaysPerYear,
                        MaxCopyFinancialStatementNumOfYear = companyParameter.MaxCopyFinancialStatementNumOfYear,
                        MaxCopyTaxReportNumOfMonth = companyParameter.MaxCopyTaxReportNumOfMonth,
                        MaxInterestPct = companyParameter.MaxInterestPct,
                        MinUpcountryChequeFeeAmount =companyParameter.MinUpcountryChequeFeeAmount,
                        ServiceFeeInvTypeGUID = companyParameter.ServiceFeeInvTypeGUID,
                        SettlementMethodOfPaymentGUID = companyParameter.SettlementMethodOfPaymentGUID,
                        SuspenseMethodOfPaymentGUID = companyParameter.SuspenseMethodOfPaymentGUID,
                        UpcountryChequeFeePct = companyParameter.UpcountryChequeFeePct,
                        FTBillingFeeInvRevenueTypeGUID = companyParameter.FTBillingFeeInvRevenueTypeGUID,
                        FTInterestInvTypeGUID = companyParameter.FTInterestInvTypeGUID,
                        FTInterestRefundInvTypeGUID = companyParameter.FTInterestRefundInvTypeGUID,
                        FTMaxRollbillInterestDay = companyParameter.FTMaxRollbillInterestDay,
                        FTMinInterestDay = companyParameter.FTMinInterestDay,
                        FTFeeInvTypeGUID = companyParameter.FTFeeInvTypeGUID,
                        FTInvTypeGUID = companyParameter.FTInvTypeGUID,
                        FTReceiptFeeInvRevenueTypeGUID = companyParameter.FTReceiptFeeInvRevenueTypeGUID,
                        FTReserveInvRevenueTypeGUID = companyParameter.FTReserveInvRevenueTypeGUID,
                        FTReserveToBeRefundedInvTypeGUID = companyParameter.FTReserveToBeRefundedInvTypeGUID,
                        FTRetentionInvTypeGUID = companyParameter.FTRetentionInvTypeGUID,
                        FTRollbillInterestDay = companyParameter.FTRollbillInterestDay,
                        FTUnearnedInterestInvTypeGUID = companyParameter.FTUnearnedInterestInvTypeGUID,
                        PFInterestCutDay = companyParameter.PFInterestCutDay,
                        PFInvTypeGUID = companyParameter.PFInvTypeGUID,
                        PFUnearnedInterestInvTypeGUID = companyParameter.PFUnearnedInterestInvTypeGUID,
                        PFInterestInvTypeGUID = companyParameter.PFInterestInvTypeGUID,
                        PFInterestRefundInvTypeGUID = companyParameter.PFInterestRefundInvTypeGUID,
                        PFRetentionInvTypeGUID = companyParameter.PFRetentionInvTypeGUID,
                        PFMaxPostponeCollectionDay = companyParameter.PFMaxPostponeCollectionDay,
                        PFExtendCreditTermGUID = companyParameter.PFExtendCreditTermGUID,
                        PFTermExtensionInvRevenueTypeGUID = companyParameter.PFTermExtensionInvRevenueTypeGUID,
                        PFTermExtensionDaysPerFeeAmount = companyParameter.PFTermExtensionDaysPerFeeAmount,
                        BondRetentionInvTypeGUID = companyParameter.BondRetentionInvTypeGUID,
                        LCRetentionInvTypeGUID = companyParameter.LCRetentionInvTypeGUID,
						FTRollbillUnearnedInterestInvTypeGUID = companyParameter.FTRollbillUnearnedInterestInvTypeGUID,
						OperReportSignatureGUID = companyParameter.OperReportSignatureGUID,
						FTMinSettleInterestDayMaxPct = companyParameter.FTMinSettleInterestDayMaxPct,
						NormalAccruedIntDay = companyParameter.NormalAccruedIntDay,
						MaxAccruedIntDay = companyParameter.MaxAccruedIntDay,
					
						RowVersion = companyParameter.RowVersion,
					});
		}
		public CompanyParameterItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<CompanyParameterItemViewMap, CompanyParameterItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public CompanyParameter CreateCompanyParameter(CompanyParameter companyParameter)
		{
			try
			{
				companyParameter.CompanyParameterGUID = Guid.NewGuid();
				base.Add(companyParameter);
				return companyParameter;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCompanyParameterVoid(CompanyParameter companyParameter)
		{
			try
			{
				CreateCompanyParameter(companyParameter);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CompanyParameter UpdateCompanyParameter(CompanyParameter dbCompanyParameter, CompanyParameter inputCompanyParameter, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCompanyParameter = dbCompanyParameter.MapUpdateValues<CompanyParameter>(inputCompanyParameter);
				base.Update(dbCompanyParameter);
				return dbCompanyParameter;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCompanyParameterVoid(CompanyParameter dbCompanyParameter, CompanyParameter inputCompanyParameter, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCompanyParameter = dbCompanyParameter.MapUpdateValues<CompanyParameter>(inputCompanyParameter, skipUpdateFields);
				base.Update(dbCompanyParameter);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #endregion Create, Update, Delete

        public CompanyParameterItemView GetByCompanyIdvw(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<CompanyParameter>(search, SysParm.CompanyGUID);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .ToMap<CompanyParameterItemViewMap, CompanyParameterItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
		public CompanyParameter GetCompanyParameterByCompanyNoTracking(Guid companyGUID)
		{
			try
			{
				var result = Entity.Where(item => item.CompanyGUID == companyGUID).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CompanyParameter GetCompanyParameterByCompanyNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CompanyGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

