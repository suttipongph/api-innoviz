using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.Data.ServicesV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core;
using System.Globalization;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICreditAppTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CreditAppTableItemView>> GetDropDownItem(SearchParameter search);
		IEnumerable<SelectItem<CreditAppTableItemView>> GetDropDownItemByMainAgreementTable(SearchParameter search, Guid mainAgreementTableGUID);
		IEnumerable<SelectItem<CreditAppTableItemView>> GetDropDownItemByCreditAppRequestTable(SearchParameter search);
		IEnumerable<SelectItem<CreditAppTableItemView>> GetDropDownItemByProductType(SearchParameter search);
		IEnumerable<SelectItem<CreditAppTableItemView>> GetCreditAppTableByGuarantorLineIdDropDown(SearchParameter search);
		#endregion DropDown
		SearchResult<CreditAppTableListView> GetListvw(SearchParameter search);
		CreditAppTableItemView GetByIdvw(Guid id);
		CreditAppTable Find(params object[] keyValues);
		CreditAppTable GetCreditAppTableByLine(Guid? refGUID);
		CreditAppTable GetCreditAppTableByIdNoTracking(Guid guid);
		CreditAppTable GetCreditAppTableByIdNoTrackingByAccessLevel(Guid guid);
		CreditAppTable CreateCreditAppTable(CreditAppTable creditAppTable);
		void CreateCreditAppTableVoid(CreditAppTable creditAppTable);
		CreditAppTable UpdateCreditAppTable(CreditAppTable dbCreditAppTable, CreditAppTable inputCreditAppTable, List<string> skipUpdateFields = null);
		void UpdateCreditAppTableVoid(CreditAppTable dbCreditAppTable, CreditAppTable inputCreditAppTable, List<string> skipUpdateFields = null);
		void Remove(CreditAppTable item);
		IEnumerable<CreditAppTableItemView> GetCreditAppTableNotExpired(CreditAppRequestTable creditAppRequestTable);
		decimal GetCustomerCreditLimit(Guid customerTableGUID, int productType, DateTime RequestDate);
		IEnumerable<CreditAppTable> GetCreditAppTableByCompanyNoTracking(Guid guid);
		#region function
		ExtendExpiryDateParamView GetExtendExpiryDateById(Guid guid);
		UpdateExpectedSigningDateView GetUpdateExpectedSigningDateById(Guid guid);
		#endregion
		#region shared
		DateTime GetEndDateByCreditAppTableGUID(Guid guid);
		BookmarkDocumentCreditApplication GetBookmaskDocumentCreditApplication(Guid refGUID);
		#endregion
		List<CreditOutstandingViewMap> GetCreditOutstandingByCustomer(Guid customerTableGUID, DateTime asOfDate);
		SearchResult<CreditOutstandingView> GetCreditOutstandingByCustomervw(SearchParameter search);
		List<CreditAppTable> GetCreditAppTableByIdNoTracking(List<Guid> guids);
		void ValidateAdd(CreditAppTable item);
		void ValidateAdd(IEnumerable<CreditAppTable> items);
		BookmarkDocumentCreditApplicationConsideration GetBookmarkDocumentCreditApplicationConsideration(Guid refGUID);
	}
	public class CreditAppTableRepo : CompanyBaseRepository<CreditAppTable>, ICreditAppTableRepo
	{
		public CreditAppTableRepo(SmartAppDbContext context) : base(context) { }
		public CreditAppTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CreditAppTable GetCreditAppTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CreditAppTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<CreditAppTable> GetCreditAppTableByIdNoTracking(List<Guid> guids)
		{
			try
			{
				var results = Entity.Where(w => guids.Contains(w.CreditAppTableGUID)).AsNoTracking().ToList();
				return results;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditAppTable GetCreditAppTableByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CreditAppTableGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditAppTable GetCreditAppTableByLine(Guid? guid)
		{
			try
			{
				var result = (from creditAppLine in db.Set<CreditAppLine>()
							  join creditAppTable in db.Set<CreditAppTable>() 
							  on creditAppLine.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljcreditAppTable
							  from creditAppTable in ljcreditAppTable.DefaultIfEmpty()
							  where creditAppLine.CreditAppLineGUID == guid
							  select new CreditAppTable
							  {
								 CreditAppTableGUID = creditAppTable.CreditAppTableGUID
							  }).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<CreditAppTable> GetCreditAppTableByCompanyNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CompanyGUID == guid).AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		
			private IQueryable<CreditAppTableItemViewMap> GetDropDownQueryByProductType()
		{
			return (from creditAppTable in Entity
					select new CreditAppTableItemViewMap
					{
						CompanyGUID = creditAppTable.CompanyGUID,
						Owner = creditAppTable.Owner,
						OwnerBusinessUnitGUID = creditAppTable.OwnerBusinessUnitGUID,
						CreditAppTableGUID = creditAppTable.CreditAppTableGUID,
						CreditAppId = creditAppTable.CreditAppId,
						Description = creditAppTable.Description,
						CustomerTableGUID = creditAppTable.CustomerTableGUID,
						ProductType = creditAppTable.ProductType,
						CreditLimitTypeGUID = creditAppTable.CreditLimitTypeGUID,
						RefCreditAppRequestTableGUID = creditAppTable.RefCreditAppRequestTableGUID,
						ApprovedCreditLimit = creditAppTable.ApprovedCreditLimit,
						TotalInterestPct = creditAppTable.TotalInterestPct,
						ConsortiumTableGUID = creditAppTable.ConsortiumTableGUID
					});
		}
		private IQueryable<CreditAppTableItemViewMap> GetDropDownQuery()
		{
			return (from creditAppTable in Entity
					join creditLimitType in db.Set<CreditLimitType>() on creditAppTable.CreditLimitTypeGUID equals creditLimitType.CreditLimitTypeGUID

					join creditAppRequestTable in db.Set<CreditAppRequestTable>()
					on creditAppTable.RefCreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID

					join consortiumTable in db.Set<ConsortiumTable>()
					on creditAppTable.ConsortiumTableGUID equals consortiumTable.ConsortiumTableGUID into ljConsortiumTable
					from consortiumTable in ljConsortiumTable.DefaultIfEmpty()

					select new CreditAppTableItemViewMap
					{
						CompanyGUID = creditAppTable.CompanyGUID,
						Owner = creditAppTable.Owner,
						OwnerBusinessUnitGUID = creditAppTable.OwnerBusinessUnitGUID,
						CreditAppTableGUID = creditAppTable.CreditAppTableGUID,
						CreditAppId = creditAppTable.CreditAppId,
						Description = creditAppTable.Description,
						CustomerTableGUID = creditAppTable.CustomerTableGUID,
						ProductType = creditAppTable.ProductType,
						CreditLimitTypeGUID = creditAppTable.CreditLimitTypeGUID,
						RefCreditAppRequestTableGUID = creditAppTable.RefCreditAppRequestTableGUID,
						CreditLimitType_Revolving = creditLimitType.Revolving,
						ApprovedCreditLimit = creditAppTable.ApprovedCreditLimit,
						TotalInterestPct = creditAppTable.TotalInterestPct,
						ConsortiumTableGUID = creditAppTable.ConsortiumTableGUID,
						RefCreditAppRequestTable_Values = (creditAppRequestTable != null) ? SmartAppUtil.GetDropDownLabel(creditAppRequestTable.CreditAppRequestId, creditAppRequestTable.Description) : null,
						ConsortiumTable_Values = (consortiumTable != null) ? SmartAppUtil.GetDropDownLabel(consortiumTable.ConsortiumId, consortiumTable.Description) : null,
						CreditLimitType_Values = (creditLimitType != null) ? SmartAppUtil.GetDropDownLabel(creditLimitType.CreditLimitTypeId, creditLimitType.Description) : null,
						InactiveDate = creditAppTable.InactiveDate,
						ExpiryDate = creditAppTable.ExpiryDate,
						Dimension1GUID = creditAppTable.Dimension1GUID,
						Dimension2GUID = creditAppTable.Dimension2GUID,
						Dimension3GUID = creditAppTable.Dimension3GUID,
						Dimension4GUID = creditAppTable.Dimension4GUID,
						Dimension5GUID = creditAppTable.Dimension5GUID
					});
		}

		private IQueryable<CreditAppTableItemViewMap> GetDropDownQueryByMainAgreementTable()
		{
			return (from creditAppTable in Entity
					join creditLimitType in db.Set<CreditLimitType>() on creditAppTable.CreditLimitTypeGUID equals creditLimitType.CreditLimitTypeGUID

					join creditAppRequestTable in db.Set<CreditAppRequestTable>()
					on creditAppTable.RefCreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID

					join consortiumTable in db.Set<ConsortiumTable>()
					on creditAppTable.ConsortiumTableGUID equals consortiumTable.ConsortiumTableGUID into ljConsortiumTable
					from consortiumTable in ljConsortiumTable.DefaultIfEmpty()

					join creditAppLine in db.Set<CreditAppLine>().GroupBy(g => g.CreditAppTableGUID).Select(s => new { CreditAppTableGUID = s.Key, SumApprovedCreditLimitLine = s.Sum(t => t.ApprovedCreditLimitLine) })
					on creditAppTable.CreditAppTableGUID equals creditAppLine.CreditAppTableGUID into ljCreditAppLine
					from creditAppLine in ljCreditAppLine.DefaultIfEmpty()

					select new CreditAppTableItemViewMap
					{
						CompanyGUID = creditAppTable.CompanyGUID,
						Owner = creditAppTable.Owner,
						OwnerBusinessUnitGUID = creditAppTable.OwnerBusinessUnitGUID,
						CreditAppTableGUID = creditAppTable.CreditAppTableGUID,
						CreditAppId = creditAppTable.CreditAppId,
						Description = creditAppTable.Description,
						CustomerTableGUID = creditAppTable.CustomerTableGUID,
						ProductType = creditAppTable.ProductType,
						CreditLimitTypeGUID = creditAppTable.CreditLimitTypeGUID,
						RefCreditAppRequestTableGUID = creditAppTable.RefCreditAppRequestTableGUID,
						CreditLimitType_Revolving = creditLimitType.Revolving,
						ApprovedCreditLimit = creditAppTable.ApprovedCreditLimit,
						TotalInterestPct = creditAppTable.TotalInterestPct,
						ConsortiumTableGUID = creditAppTable.ConsortiumTableGUID,
						RefCreditAppRequestTable_Values = (creditAppRequestTable != null) ? SmartAppUtil.GetDropDownLabel(creditAppRequestTable.CreditAppRequestId, creditAppRequestTable.Description) : null,
						ConsortiumTable_Values = (consortiumTable != null) ? SmartAppUtil.GetDropDownLabel(consortiumTable.ConsortiumId, consortiumTable.Description) : null,
						CreditLimitType_Values = (creditLimitType != null) ? SmartAppUtil.GetDropDownLabel(creditLimitType.CreditLimitTypeId, creditLimitType.Description) : null,
						InactiveDate = creditAppTable.InactiveDate,
						SumApprovedCreditLimitLine = creditAppLine.SumApprovedCreditLimitLine,
					});
		}

		private IQueryable<CreditAppTableItemViewMap> GetFilterQueryByMainAgreementTable()
		{
			return (from creditAppTable in Entity
					join creditLimitType in db.Set<CreditLimitType>() on creditAppTable.CreditLimitTypeGUID equals creditLimitType.CreditLimitTypeGUID
					join mainAgreementTable in db.Set<MainAgreementTable>() on new { creditAppTable.CreditAppTableGUID, creditAppTable.CustomerTableGUID }
											equals new { mainAgreementTable.CreditAppTableGUID, mainAgreementTable.CustomerTableGUID }
					join documentStatus in db.Set<DocumentStatus>() on mainAgreementTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljDocumentStatus
					from documentStatus in ljDocumentStatus.DefaultIfEmpty()
					select new CreditAppTableItemViewMap
					{
						DocumentStatus_StatusId = (documentStatus != null) ? documentStatus.StatusId : null,
						ProductType = creditAppTable.ProductType,
						CustomerTableGUID = creditAppTable.CustomerTableGUID,
						CreditAppTableGUID = creditAppTable.CreditAppTableGUID,
						MainAgreementTable_MainAgreementTableGUID = mainAgreementTable.MainAgreementTableGUID,
						MainAgreementTable_AgreementDocType = mainAgreementTable.AgreementDocType,
						CreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
						CreditAppId = creditAppTable.CreditAppId,
						Description = creditAppTable.Description
					});
		}
		public IEnumerable<SelectItem<CreditAppTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditAppTable>(search, SysParm.CompanyGUID);
				var creditAppTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CreditAppTableItemViewMap, CreditAppTableItemView>().ToDropDownItem(search.ExcludeRowData);
				return creditAppTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<CreditAppTableItemView>> GetDropDownItemByMainAgreementTable(SearchParameter search, Guid mainAgreementTableGUID)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditAppTable>(search, SysParm.CompanyGUID);
				var creditAppTable = GetDropDownQueryByMainAgreementTable().Where(predicate.Predicates, predicate.Values).ToMaps<CreditAppTableItemViewMap, CreditAppTableItemView>();
				var filterList = GetFilterQueryByMainAgreementTable().Where(predicate.Predicates, predicate.Values).ToMaps<CreditAppTableItemViewMap, CreditAppTableItemView>()
																	 .Where(t => t.DocumentStatus_StatusId != null && t.DocumentStatus_StatusId != Convert.ToInt32(MainAgreementStatus.Cancelled).ToString()
																				 && t.MainAgreementTable_AgreementDocType == Convert.ToInt32(AgreementDocType.New)
																				 && t.MainAgreementTable_MainAgreementTableGUID != mainAgreementTableGUID.GuidNullToString()).ToList();
				var final = creditAppTable.Where(t => !filterList.Select(s => s.CreditAppTableGUID).Contains(t.CreditAppTableGUID));
				return final.ToDropDownItem(search.ExcludeRowData);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<CreditAppTableItemView>> GetDropDownItemByCreditAppRequestTable(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditAppTable>(search, SysParm.CompanyGUID);
				var creditAppTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CreditAppTableItemViewMap, CreditAppTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return creditAppTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		
				public IEnumerable<SelectItem<CreditAppTableItemView>> GetCreditAppTableByGuarantorLineIdDropDown(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditAppTable>(search, SysParm.CompanyGUID);
				var creditAppTable = GetDropDownQueryByProductType().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CreditAppTableItemViewMap, CreditAppTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return creditAppTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<CreditAppTableItemView>> GetDropDownItemByProductType(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditAppTable>(search, SysParm.CompanyGUID);
				var creditAppTable = GetDropDownQueryByProductType().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CreditAppTableItemViewMap, CreditAppTableItemView>().ToDropDownItem(search.ExcludeRowData);

				return creditAppTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CreditAppTableListViewMap> GetListQuery()
		{
			return (from creditAppTable in Entity
					join creditAppRequestTable in db.Set<CreditAppRequestTable>() on creditAppTable.RefCreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID into ljCreditAppRequestTableCreditAppRequestTable
					from creditAppRequestTable in ljCreditAppRequestTableCreditAppRequestTable.DefaultIfEmpty()
					join creditLimitType in db.Set<CreditLimitType>() on creditAppTable.CreditLimitTypeGUID equals creditLimitType.CreditLimitTypeGUID
					join customerTable in db.Set<CustomerTable>() on creditAppTable.CustomerTableGUID equals customerTable.CustomerTableGUID
					select new CreditAppTableListViewMap
					{
						CompanyGUID = creditAppTable.CompanyGUID,
						Owner = creditAppTable.Owner,
						OwnerBusinessUnitGUID = creditAppTable.OwnerBusinessUnitGUID,
						CreditAppTableGUID = creditAppTable.CreditAppTableGUID,
						CreditAppId = creditAppTable.CreditAppId,
						ExpiryDate = creditAppTable.ExpiryDate,
						CustomerTableGUID = creditAppTable.CustomerTableGUID,
						ProductType = creditAppTable.ProductType,
						CreditLimitTypeGUID = creditAppTable.CreditLimitTypeGUID,
						ApprovedDate = creditAppTable.ApprovedDate,
						RefCreditAppRequestTableGUID = creditAppTable.RefCreditAppRequestTableGUID,
						ApprovedCreditLimit = creditAppTable.ApprovedCreditLimit,
						InactiveDate = creditAppTable.InactiveDate,
						StartDate = creditAppTable.StartDate,
						CreditLimitType_Values = SmartAppUtil.GetDropDownLabel(creditLimitType.CreditLimitTypeId, creditLimitType.Description),
						RefCreditAppRequestTable_Values = SmartAppUtil.GetDropDownLabel(creditAppRequestTable.CreditAppRequestId, creditAppRequestTable.Description),
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
						CreditLimitType_CreditLimitTypeId = creditLimitType.CreditLimitTypeId,
						RefCreditAppRequestTable_CreditAppRequestId = creditAppRequestTable.CreditAppRequestId,
						CustomerTable_CustomerId = customerTable.CustomerId,
					});
		}
		public SearchResult<CreditAppTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CreditAppTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditAppTable>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CreditAppTableListViewMap, CreditAppTableListView>();
				result = list.SetSearchResult<CreditAppTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CreditAppTableItemViewMap> GetItemQuery()
		{
			var address = db.Set<AddressTrans>().Where(w => w.RefType == (int)RefType.Customer);
			var addressLabel = (from creditAppTable in Entity
								join billingAddress in address on creditAppTable.BillingAddressGUID equals billingAddress.AddressTransGUID into ljCreditAppRequestLineBillingAddress
								from billingAddress in ljCreditAppRequestLineBillingAddress.DefaultIfEmpty()
								join invoiceAddress in address on creditAppTable.InvoiceAddressGUID equals invoiceAddress.AddressTransGUID into ljCreditAppRequestLineInvoiceAddress
								from invoiceAddress in ljCreditAppRequestLineInvoiceAddress.DefaultIfEmpty()
								join mailingReceiptAddress in address on creditAppTable.MailingReceipAddressGUID equals mailingReceiptAddress.AddressTransGUID into ljCreditAppRequestLineMailingReceiptAddress
								from mailingReceiptAddress in ljCreditAppRequestLineMailingReceiptAddress.DefaultIfEmpty()
								join receiptAddress in address on creditAppTable.ReceiptAddressGUID equals receiptAddress.AddressTransGUID into ljCreditAppRequestLineReceiptAddressGUID
								from receiptAddress in ljCreditAppRequestLineReceiptAddressGUID.DefaultIfEmpty()
								join registeredAddress in address on creditAppTable.RegisteredAddressGUID equals registeredAddress.AddressTransGUID
								join creditAppLine in db.Set<CreditAppLine>().GroupBy(g => g.CreditAppTableGUID)
																						.Select(s => new { CreditAppTableGUID = s.Key, _count = s.Count() })
								on creditAppTable.CreditAppTableGUID equals creditAppLine.CreditAppTableGUID into ljCreditAppRequestTableCreditAppline
								from creditAppLine in ljCreditAppRequestTableCreditAppline.DefaultIfEmpty()
								select new CreditAppTableItemViewMap
								{
									CreditAppTableGUID = creditAppTable.CreditAppTableGUID,
									UnboundBillingAddress = (billingAddress != null) ? SmartAppUtil.GetAddressLabel(billingAddress.Address1, billingAddress.Address2) : null,
									UnboundInvoiceAddress = (invoiceAddress != null) ? SmartAppUtil.GetAddressLabel(invoiceAddress.Address1, invoiceAddress.Address2) : null,
									UnboundMailingReceiptAddress = (mailingReceiptAddress != null) ? SmartAppUtil.GetAddressLabel(mailingReceiptAddress.Address1, mailingReceiptAddress.Address2) : null,
									UnboundReceiptAddress = (receiptAddress != null) ? SmartAppUtil.GetAddressLabel(receiptAddress.Address1, receiptAddress.Address2) : null,
									UnboundRegisteredAddress = SmartAppUtil.GetAddressLabel(registeredAddress.Address1, registeredAddress.Address2),
									BillingAddress_Values = (billingAddress != null) ? SmartAppUtil.GetDropDownLabel(billingAddress.Name, billingAddress.Address1) : null,
									InvoiceAddress_Values = (invoiceAddress != null) ? SmartAppUtil.GetDropDownLabel(invoiceAddress.Name, invoiceAddress.Address1) : null,
									MailingReceipAddress_Values = (mailingReceiptAddress != null) ? SmartAppUtil.GetDropDownLabel(mailingReceiptAddress.Name, mailingReceiptAddress.Address1) : null,
									ReceiptAddress_Values = (receiptAddress != null) ? SmartAppUtil.GetDropDownLabel(receiptAddress.Name, receiptAddress.Address1) : null,
									RegisteredAddress_Values = (registeredAddress != null) ? SmartAppUtil.GetDropDownLabel(registeredAddress.Name, registeredAddress.Address1) : null,
									NumberOfRegisteredBuyer = (creditAppLine != null) ? creditAppLine._count : 0,
								});
			var ledgerLabel = (from creditAppTable in Entity
							   join ledgerDimension1 in db.Set<LedgerDimension>() on creditAppTable.Dimension1GUID equals ledgerDimension1.LedgerDimensionGUID into ljCreditAppTableLedgerDimension1
							   from ledgerDimension1 in ljCreditAppTableLedgerDimension1.DefaultIfEmpty()
							   join ledgerDimension2 in db.Set<LedgerDimension>() on creditAppTable.Dimension2GUID equals ledgerDimension2.LedgerDimensionGUID into ljCreditAppTableLedgerDimension2
							   from ledgerDimension2 in ljCreditAppTableLedgerDimension2.DefaultIfEmpty()
							   join ledgerDimension3 in db.Set<LedgerDimension>() on creditAppTable.Dimension3GUID equals ledgerDimension3.LedgerDimensionGUID into ljCreditAppTableLedgerDimension3
							   from ledgerDimension3 in ljCreditAppTableLedgerDimension3.DefaultIfEmpty()
							   join ledgerDimension4 in db.Set<LedgerDimension>() on creditAppTable.Dimension4GUID equals ledgerDimension4.LedgerDimensionGUID into ljCreditAppTableLedgerDimension4
							   from ledgerDimension4 in ljCreditAppTableLedgerDimension4.DefaultIfEmpty()
							   join ledgerDimension5 in db.Set<LedgerDimension>() on creditAppTable.Dimension5GUID equals ledgerDimension5.LedgerDimensionGUID into ljCreditAppTableLedgerDimension5
							   from ledgerDimension5 in ljCreditAppTableLedgerDimension5.DefaultIfEmpty()
							   select new CreditAppTableItemViewMap
							   {
								   CreditAppTableGUID = creditAppTable.CreditAppTableGUID,
								   Dimension1_Values = (ledgerDimension1 != null) ? SmartAppUtil.GetDropDownLabel(ledgerDimension1.DimensionCode, ledgerDimension1.Description) : null,
								   Dimension2_Values = (ledgerDimension2 != null) ? SmartAppUtil.GetDropDownLabel(ledgerDimension2.DimensionCode, ledgerDimension2.Description) : null,
								   Dimension3_Values = (ledgerDimension3 != null) ? SmartAppUtil.GetDropDownLabel(ledgerDimension3.DimensionCode, ledgerDimension3.Description) : null,
								   Dimension4_Values = (ledgerDimension4 != null) ? SmartAppUtil.GetDropDownLabel(ledgerDimension4.DimensionCode, ledgerDimension4.Description) : null,
								   Dimension5_Values = (ledgerDimension5 != null) ? SmartAppUtil.GetDropDownLabel(ledgerDimension5.DimensionCode, ledgerDimension5.Description) : null,
							   });
			var custBankLabel = (from creditAppTable in Entity
								 join bankAccountControl in db.Set<CustBank>() on creditAppTable.BankAccountControlGUID equals bankAccountControl.CustBankGUID into ljCreditAppTableBankAccountControl
								 from bankAccountControl in ljCreditAppTableBankAccountControl.DefaultIfEmpty()
								 join pdcBank in db.Set<CustBank>() on creditAppTable.PDCBankGUID equals pdcBank.CustBankGUID into ljCreditAppTablePDCBank
								 from pdcBank in ljCreditAppTablePDCBank.DefaultIfEmpty()
								 select new CreditAppTableItemViewMap
								 {
									 CreditAppTableGUID = creditAppTable.CreditAppTableGUID,
									 BankAccountControl_Values = (bankAccountControl != null) ? SmartAppUtil.GetDropDownLabel(bankAccountControl.BankAccountName) : null,
									 PDCBank_Values = (pdcBank != null) ? SmartAppUtil.GetDropDownLabel(pdcBank.BankAccountName) : null,
								 });

			var contactPersonLabel = (from creditAppTable in Entity
									  join billingContactPerson in db.Set<ContactPersonTrans>() on creditAppTable.BillingContactPersonGUID equals billingContactPerson.ContactPersonTransGUID into ljCreditAppTableBillingContactPerson
									  from billingContactPerson in ljCreditAppTableBillingContactPerson.DefaultIfEmpty()
									  join receiptContactPerson in db.Set<ContactPersonTrans>() on creditAppTable.ReceiptContactPersonGUID equals receiptContactPerson.ContactPersonTransGUID into ljCreditAppTableReceiptContactPerson
									  from receiptContactPerson in ljCreditAppTableReceiptContactPerson.DefaultIfEmpty()
									  join billingRelatedPersonTable in db.Set<RelatedPersonTable>() on billingContactPerson.RelatedPersonTableGUID equals billingRelatedPersonTable.RelatedPersonTableGUID into ljCreditAppTableReceiptBillingRelatedPersonTable
									  from billingRelatedPersonTable in ljCreditAppTableReceiptBillingRelatedPersonTable.DefaultIfEmpty()
									  join receiptRelatedPersonTable in db.Set<RelatedPersonTable>() on billingContactPerson.RelatedPersonTableGUID equals receiptRelatedPersonTable.RelatedPersonTableGUID into ljCreditAppTableReceiptReceiptRelatedPersonTable
									  from receiptRelatedPersonTable in ljCreditAppTableReceiptReceiptRelatedPersonTable.DefaultIfEmpty()
									  select new CreditAppTableItemViewMap
									  {
										  CreditAppTableGUID = creditAppTable.CreditAppTableGUID,
										  BillingContactPerson_Values = (billingRelatedPersonTable != null) ? SmartAppUtil.GetDropDownLabel(billingRelatedPersonTable.RelatedPersonId, billingRelatedPersonTable.Name) : null,
										  ReceiptContactPerson_Values = (receiptRelatedPersonTable != null) ? SmartAppUtil.GetDropDownLabel(receiptRelatedPersonTable.RelatedPersonId, receiptRelatedPersonTable.Name) : null,
									  });
			var otherLabel = (from creditAppTable in Entity
							  join applicationTable in db.Set<ApplicationTable>() on creditAppTable.ApplicationTableGUID equals applicationTable.ApplicationTableGUID into ljCreditAppRequestTableApplicationTable
							  from applicationTable in ljCreditAppRequestTableApplicationTable.DefaultIfEmpty()

							  join productSubType in db.Set<ProductSubType>() on creditAppTable.ProductSubTypeGUID equals productSubType.ProductSubTypeGUID into ljCreditAppRequestTableProductSubType
							  from productSubType in ljCreditAppRequestTableProductSubType.DefaultIfEmpty()

							  join creditLimitType in db.Set<CreditLimitType>() on creditAppTable.CreditLimitTypeGUID equals creditLimitType.CreditLimitTypeGUID

							  join documentReason in db.Set<DocumentReason>() on creditAppTable.DocumentReasonGUID equals documentReason.DocumentReasonGUID into ljCreditAppRequestTableDocumentReason
							  from documentReason in ljCreditAppRequestTableDocumentReason.DefaultIfEmpty()

							  join creditAppRequestTable in db.Set<CreditAppRequestTable>() on creditAppTable.RefCreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID

							  join interestType in db.Set<InterestType>() on creditAppTable.InterestTypeGUID equals interestType.InterestTypeGUID into ljCreditAppRequestTableInterestType
							  from interestType in ljCreditAppRequestTableInterestType.DefaultIfEmpty()

							  join creditTerm in db.Set<CreditTerm>() on creditAppTable.CreditTermGUID equals creditTerm.CreditTermGUID into ljCreditAppRequestTableCreditTerm
							  from creditTerm in ljCreditAppRequestTableCreditTerm.DefaultIfEmpty()

							  join serviceFeeCondTemplateTable in db.Set<ServiceFeeCondTemplateTable>() on creditAppTable.ExtensionServiceFeeCondTemplateGUID equals serviceFeeCondTemplateTable.ServiceFeeCondTemplateTableGUID into ljCreditAppRequestTableServiceFeeCondTemplateTable
							  from serviceFeeCondTemplateTable in ljCreditAppRequestTableServiceFeeCondTemplateTable.DefaultIfEmpty()

							  join consortiumTable in db.Set<ConsortiumTable>() on creditAppTable.ConsortiumTableGUID equals consortiumTable.ConsortiumTableGUID into ljCreditAppRequestTableConsortiumTable
							  from consortiumTable in ljCreditAppRequestTableConsortiumTable.DefaultIfEmpty()

							  join parentCreditLimitType in db.Set<CreditLimitType>() on creditLimitType.ParentCreditLimitTypeGUID equals parentCreditLimitType.CreditLimitTypeGUID into ljParentCreditLimitType
							  from parentCreditLimitType in ljParentCreditLimitType.DefaultIfEmpty()

							  join assignmentMethod in db.Set<AssignmentMethod>() on creditAppTable.AssignmentMethodGUID equals assignmentMethod.AssignmentMethodGUID into ljAssignmentMethod
							  from assignmentMethod in ljAssignmentMethod.DefaultIfEmpty()
							  select new CreditAppTableItemViewMap
							  {
								  CreditAppTableGUID = creditAppTable.CreditAppTableGUID,
								  ApplicationTable_Values = (applicationTable != null) ? SmartAppUtil.GetDropDownLabel(applicationTable.ApplicationId) : null,
								  ProductSubType_Values = (productSubType != null) ? SmartAppUtil.GetDropDownLabel(productSubType.ProductSubTypeId, productSubType.Description) : null,
								  CreditLimitType_Values = SmartAppUtil.GetDropDownLabel(creditLimitType.CreditLimitTypeId, creditLimitType.Description),
								  DocumentReason_Values = (documentReason != null) ? SmartAppUtil.GetDropDownLabel(documentReason.Description) : null,
								  RefCreditAppRequestTable_Values = SmartAppUtil.GetDropDownLabel(creditAppRequestTable.CreditAppRequestId, creditAppRequestTable.Description),
								  InterestType_Values = (interestType != null) ? SmartAppUtil.GetDropDownLabel(interestType.InterestTypeId, interestType.Description) : null,
								  CreditTerm_Values = (creditTerm != null) ? SmartAppUtil.GetDropDownLabel(creditTerm.CreditTermId, creditTerm.Description) : null,
								  ExtensionServiceFeeCondTemplate_Values = (serviceFeeCondTemplateTable != null) ? SmartAppUtil.GetDropDownLabel(serviceFeeCondTemplateTable.ServiceFeeCondTemplateId, serviceFeeCondTemplateTable.Description) : null,
								  ConsortiumTable_Values = (consortiumTable != null) ? SmartAppUtil.GetDropDownLabel(consortiumTable.ConsortiumId, consortiumTable.Description) : null,
								  CreditLimitType_BuyerMatchingAndLoanRequest = creditLimitType.BuyerMatchingAndLoanRequest,
								  CreditLimitType_ParentCreditLimitTypeGUID = (parentCreditLimitType != null) ? parentCreditLimitType.CreditLimitTypeGUID: (Guid?)null,
								  AssignmentMethod_Values = (assignmentMethod != null) ? SmartAppUtil.GetDropDownLabel(assignmentMethod.AssignmentMethodId, assignmentMethod.Description) : null,

							  });

			return (from creditAppTable in Entity
					join company in db.Set<Company>()
					on creditAppTable.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on creditAppTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCreditAppTableOwnerBU
					from ownerBU in ljCreditAppTableOwnerBU.DefaultIfEmpty()
					join customerTable in db.Set<CustomerTable>() on creditAppTable.CustomerTableGUID equals customerTable.CustomerTableGUID
					join addLabel in addressLabel on creditAppTable.CreditAppTableGUID equals addLabel.CreditAppTableGUID into ljCreditAppRequestTableAddLabel
					from addLabel in ljCreditAppRequestTableAddLabel.DefaultIfEmpty()
					join businessType in db.Set<BusinessType>() on customerTable.BusinessTypeGUID equals businessType.BusinessTypeGUID into ljCreditAppRequestLineBusinessTypes
					from businessType in ljCreditAppRequestLineBusinessTypes.DefaultIfEmpty()
					join lineOfBusiness in db.Set<LineOfBusiness>() on customerTable.LineOfBusinessGUID equals lineOfBusiness.LineOfBusinessGUID into ljCreditAppRequestLineLineOfBusiness
					from lineOfBusiness in ljCreditAppRequestLineLineOfBusiness.DefaultIfEmpty()
					join introducedBy in db.Set<IntroducedBy>() on customerTable.IntroducedByGUID equals introducedBy.IntroducedByGUID into ljCreditAppRequestLineIntroducedBy
					from introducedBy in ljCreditAppRequestLineIntroducedBy.DefaultIfEmpty()
					join ledLabel in ledgerLabel on creditAppTable.CreditAppTableGUID equals ledLabel.CreditAppTableGUID into ljCreditAppRequestTableLedgerLabel
					from ledLabel in ljCreditAppRequestTableLedgerLabel.DefaultIfEmpty()
					join bankLabel in custBankLabel on creditAppTable.CreditAppTableGUID equals bankLabel.CreditAppTableGUID into ljCreditAppRequestTableCustBankLabel
					from bankLabel in ljCreditAppRequestTableCustBankLabel.DefaultIfEmpty()
					join contactLabel in contactPersonLabel on creditAppTable.CreditAppTableGUID equals contactLabel.CreditAppTableGUID into ljCreditAppRequestTableContactPersonLabel
					from contactLabel in ljCreditAppRequestTableContactPersonLabel.DefaultIfEmpty()
					join _otherLabel in otherLabel on creditAppTable.CreditAppTableGUID equals _otherLabel.CreditAppTableGUID into ljCreditAppRequestTableOtherLabel
					from _otherLabel in ljCreditAppRequestTableOtherLabel.DefaultIfEmpty()
					select new CreditAppTableItemViewMap
					{
						CompanyGUID = creditAppTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = creditAppTable.Owner,
						OwnerBusinessUnitGUID = creditAppTable.OwnerBusinessUnitGUID,
						CreatedBy = creditAppTable.CreatedBy,
						CreatedDateTime = creditAppTable.CreatedDateTime,
						ModifiedBy = creditAppTable.ModifiedBy,
						ModifiedDateTime = creditAppTable.ModifiedDateTime,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreditAppTableGUID = creditAppTable.CreditAppTableGUID,
						ApplicationTableGUID = creditAppTable.ApplicationTableGUID,
						ApprovedCreditLimit = creditAppTable.ApprovedCreditLimit,
						ApprovedDate = creditAppTable.ApprovedDate,
						BankAccountControlGUID = creditAppTable.BankAccountControlGUID,
						BillingAddressGUID = creditAppTable.BillingAddressGUID,
						BillingContactPersonGUID = creditAppTable.BillingContactPersonGUID,
						CACondition = creditAppTable.CACondition,
						ConsortiumTableGUID = creditAppTable.ConsortiumTableGUID,
						CreditAppId = creditAppTable.CreditAppId,
						CreditLimitExpiration = creditAppTable.CreditLimitExpiration,
						CreditLimitRemark = creditAppTable.CreditLimitRemark,
						CreditLimitTypeGUID = creditAppTable.CreditLimitTypeGUID,
						CreditRequestFeePct = creditAppTable.CreditRequestFeePct,
						CreditTermGUID = creditAppTable.CreditTermGUID,
						CustomerTableGUID = creditAppTable.CustomerTableGUID,
						Description = creditAppTable.Description,
						Dimension1GUID = creditAppTable.Dimension1GUID,
						Dimension2GUID = creditAppTable.Dimension2GUID,
						Dimension3GUID = creditAppTable.Dimension3GUID,
						Dimension4GUID = creditAppTable.Dimension4GUID,
						Dimension5GUID = creditAppTable.Dimension5GUID,
						DocumentReasonGUID = creditAppTable.DocumentReasonGUID,
						ExpectedAgreementSigningDate = creditAppTable.ExpectedAgreementSigningDate,
						ExpiryDate = creditAppTable.ExpiryDate,
						ExtensionServiceFeeCondTemplateGUID = creditAppTable.ExtensionServiceFeeCondTemplateGUID,
						InactiveDate = creditAppTable.InactiveDate,
						InterestAdjustment = creditAppTable.InterestAdjustment,
						InterestTypeGUID = creditAppTable.InterestTypeGUID,
						InvoiceAddressGUID = creditAppTable.InvoiceAddressGUID,
						MailingReceipAddressGUID = creditAppTable.MailingReceipAddressGUID,
						MaxPurchasePct = creditAppTable.MaxPurchasePct,
						MaxRetentionAmount = creditAppTable.MaxRetentionAmount,
						MaxRetentionPct = creditAppTable.MaxRetentionPct,
						PDCBankGUID = creditAppTable.PDCBankGUID,
						ProductSubTypeGUID = creditAppTable.ProductSubTypeGUID,
						ProductType = creditAppTable.ProductType,
						PurchaseFeeCalculateBase = creditAppTable.PurchaseFeeCalculateBase,
						PurchaseFeePct = creditAppTable.PurchaseFeePct,
						ReceiptAddressGUID = creditAppTable.ReceiptAddressGUID,
						ReceiptContactPersonGUID = creditAppTable.ReceiptContactPersonGUID,
						RefCreditAppRequestTableGUID = creditAppTable.RefCreditAppRequestTableGUID,
						RegisteredAddressGUID = creditAppTable.RegisteredAddressGUID,
						Remark = creditAppTable.Remark,
						SalesAvgPerMonth = creditAppTable.SalesAvgPerMonth,
						StartDate = creditAppTable.StartDate,
						TotalInterestPct = creditAppTable.TotalInterestPct,

						UnboundBillingAddress = (addLabel != null) ? addLabel.UnboundBillingAddress : null,
						UnboundInvoiceAddress = (addLabel != null) ? addLabel.UnboundInvoiceAddress : null,
						UnboundMailingReceiptAddress = (addLabel != null) ? addLabel.UnboundMailingReceiptAddress : null,
						UnboundReceiptAddress = (addLabel != null) ? addLabel.UnboundReceiptAddress : null,
						UnboundRegisteredAddress = (addLabel != null) ? addLabel.UnboundRegisteredAddress : null,
						NumberOfRegisteredBuyer = (addLabel != null) ? addLabel.NumberOfRegisteredBuyer : 0,
						BillingAddress_Values = (addLabel != null) ? addLabel.BillingAddress_Values : null,
						InvoiceAddress_Values = (addLabel != null) ? addLabel.InvoiceAddress_Values : null,
						MailingReceipAddress_Values = (addLabel != null) ? addLabel.MailingReceipAddress_Values : null,
						ReceiptAddress_Values = (addLabel != null) ? addLabel.ReceiptAddress_Values : null,
						RegisteredAddress_Values = (addLabel != null) ? addLabel.RegisteredAddress_Values : null,
						TaxIdentificationId = (customerTable.IdentificationType == (int)IdentificationType.PassportId) ? customerTable.PassportID : customerTable.TaxID,
						IntroducedBy_Values = (introducedBy != null) ? SmartAppUtil.GetDropDownLabel(introducedBy.IntroducedById, introducedBy.Description) : null,
						BusinessType_Values = (businessType != null) ? SmartAppUtil.GetDropDownLabel(businessType.BusinessTypeId, businessType.Description) : null,
						LineOfBusiness_Values = (lineOfBusiness != null) ? SmartAppUtil.GetDropDownLabel(lineOfBusiness.LineOfBusinessId, lineOfBusiness.Description) : null,
						DateOfEstablish = (customerTable.RecordType == (int)RecordType.Person) ? customerTable.DateOfBirth : customerTable.DateOfEstablish,
						Dimension1_Values = (ledLabel != null) ? ledLabel.Dimension1_Values : null,
						Dimension2_Values = (ledLabel != null) ? ledLabel.Dimension2_Values : null,
						Dimension3_Values = (ledLabel != null) ? ledLabel.Dimension3_Values : null,
						Dimension4_Values = (ledLabel != null) ? ledLabel.Dimension4_Values : null,
						Dimension5_Values = (ledLabel != null) ? ledLabel.Dimension5_Values : null,
						BankAccountControl_Values = (bankLabel != null) ? bankLabel.BankAccountControl_Values : null,
						PDCBank_Values = (bankLabel != null) ? bankLabel.PDCBank_Values : null,
						BillingContactPerson_Values = (contactLabel != null) ? contactLabel.BillingContactPerson_Values : null,
						ReceiptContactPerson_Values = (contactLabel != null) ? contactLabel.ReceiptContactPerson_Values : null,
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
						ApplicationTable_Values = (_otherLabel != null) ? _otherLabel.ApplicationTable_Values : null,
						ProductSubType_Values = (_otherLabel != null) ? _otherLabel.ProductSubType_Values : null,
						CreditLimitType_Values = (_otherLabel != null) ? _otherLabel.CreditLimitType_Values : null,
						DocumentReason_Values = (_otherLabel != null) ? _otherLabel.DocumentReason_Values : null,
						RefCreditAppRequestTable_Values = (_otherLabel != null) ? _otherLabel.RefCreditAppRequestTable_Values : null,
						InterestType_Values = (_otherLabel != null) ? _otherLabel.InterestType_Values : null,
						CreditTerm_Values = (_otherLabel != null) ? _otherLabel.CreditTerm_Values : null,
						ExtensionServiceFeeCondTemplate_Values = (_otherLabel != null) ? _otherLabel.ExtensionServiceFeeCondTemplate_Values : null,
						ConsortiumTable_Values = (_otherLabel != null) ? _otherLabel.ConsortiumTable_Values : null,
						CreditLimitType_BuyerMatchingAndLoanRequest = (_otherLabel != null) ? _otherLabel.CreditLimitType_BuyerMatchingAndLoanRequest : false,
						CreditLimitType_ParentCreditLimitTypeGUID = (_otherLabel != null) ? _otherLabel.CreditLimitType_ParentCreditLimitTypeGUID : (Guid?)null,
						
						AssignmentMethodGUID = creditAppTable.AssignmentMethodGUID,
						PurchaseWithdrawalCondition = creditAppTable.PurchaseWithdrawalCondition,
						AssignmentMethod_Values = (_otherLabel != null) ? _otherLabel.AssignmentMethod_Values : null,


					
						RowVersion = creditAppTable.RowVersion,
					});
		}
		public CreditAppTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.ToMap<CreditAppTableItemViewMap, CreditAppTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}

		#endregion GetByIdvw
		#region Create, Update, Delete
		public CreditAppTable CreateCreditAppTable(CreditAppTable creditAppTable)
		{
			try
			{
				creditAppTable.CreditAppTableGUID = Guid.NewGuid();
				base.Add(creditAppTable);
				return creditAppTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCreditAppTableVoid(CreditAppTable creditAppTable)
		{
			try
			{
				CreateCreditAppTable(creditAppTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditAppTable UpdateCreditAppTable(CreditAppTable dbCreditAppTable, CreditAppTable inputCreditAppTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCreditAppTable = dbCreditAppTable.MapUpdateValues<CreditAppTable>(inputCreditAppTable);
				base.Update(dbCreditAppTable);
				return dbCreditAppTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCreditAppTableVoid(CreditAppTable dbCreditAppTable, CreditAppTable inputCreditAppTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCreditAppTable = dbCreditAppTable.MapUpdateValues<CreditAppTable>(inputCreditAppTable, skipUpdateFields);
				base.Update(dbCreditAppTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public IEnumerable<CreditAppTableItemView> GetCreditAppTableNotExpired(CreditAppRequestTable creditAppRequestTable)
		{
			try
			{ 
				var result = (from creditAppTable in Entity
							  join customerTable in db.Set<CustomerTable>() on creditAppTable.CustomerTableGUID equals customerTable.CustomerTableGUID
							  join creditLimitType in db.Set<CreditLimitType>() on creditAppTable.CreditLimitTypeGUID equals creditLimitType.CreditLimitTypeGUID
							  where creditAppTable.CreditLimitTypeGUID == creditAppRequestTable.CreditLimitTypeGUID
								 && creditAppTable.ExpiryDate > creditAppRequestTable.RequestDate
								 && creditAppTable.ConsortiumTableGUID == creditAppRequestTable.ConsortiumTableGUID
								 && creditAppTable.CustomerTableGUID == creditAppRequestTable.CustomerTableGUID
							  select new CreditAppTableItemView
							  {
								  CreditAppId = creditAppTable.CreditAppId,
								  CustomerTable_CustomerId = customerTable.CustomerId,
								  CreditLimitType_CreditLimitTypeId = creditLimitType.CreditLimitTypeId,
								  ApprovedCreditLimit = creditAppTable.ApprovedCreditLimit,
								  CreditLimitType_Values = SmartAppUtil.GetDropDownLabel(creditLimitType.CreditLimitTypeId, creditLimitType.Description),
								  CreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
							  }).AsNoTracking();
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public IEnumerable<CreditAppTableItemView> GetCreditAppTableByCreditAppRequestTable(CreditAppRequestTable creditAppRequestTable)
		{
			try
			{
				var result = (from creditAppTable in Entity
							  join customerTable in db.Set<CustomerTable>() on creditAppTable.CustomerTableGUID equals customerTable.CustomerTableGUID
							  join creditLimitType in db.Set<CreditLimitType>() on creditAppTable.CreditLimitTypeGUID equals creditLimitType.CreditLimitTypeGUID
							  where creditAppTable.ProductType == creditAppRequestTable.ProductType
								 && creditAppTable.ExpiryDate > creditAppRequestTable.RequestDate
								 && creditAppTable.ConsortiumTableGUID == creditAppRequestTable.ConsortiumTableGUID
							  select new CreditAppTableItemView
							  {
								  CustomerTable_CustomerId = customerTable.CustomerId,
								  CreditLimitType_CreditLimitTypeId = creditLimitType.CreditLimitTypeId,
								  ApprovedCreditLimit = creditAppTable.ApprovedCreditLimit
							  }).AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region function
		public ExtendExpiryDateParamView GetExtendExpiryDateById(Guid guid)
		{
			try
			{
				var result = (from creditAppTable in Entity
							  join customerTable in db.Set<CustomerTable>()
							  on creditAppTable.CustomerTableGUID equals customerTable.CustomerTableGUID into ljcreditappCustomer
							  from customerTable in ljcreditappCustomer.DefaultIfEmpty()
							  where creditAppTable.CreditAppTableGUID == guid
							  select new ExtendExpiryDateParamView
							  {
								  CreditAppTableGUID = creditAppTable.CreditAppTableGUID.ToString(),
								  CreditAppId = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
								  CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId.ToString(), customerTable.Name),
								  OriginalExpiryDate = creditAppTable.ExpiryDate.DateToString(),
								  CreditLimitExpiration = creditAppTable.CreditLimitExpiration
							  }).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public decimal GetCustomerCreditLimit(Guid customerTableGUID, int productType, DateTime requestDate)
		{
			try
			{
				IQueryable<CreditAppTable> queryable = Entity.Where(w => w.ProductType == productType && w.ExpiryDate >= requestDate && w.CustomerTableGUID == customerTableGUID);
				if (queryable.Any())
				{
					return queryable.Sum(su => su.ApprovedCreditLimit);
				}
				return 0m;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public UpdateExpectedSigningDateView GetUpdateExpectedSigningDateById(Guid guid)
		{
			try
			{
				var result = (from creditAppTable in Entity
							  join customerTable in db.Set<CustomerTable>()
							  on creditAppTable.CustomerTableGUID equals customerTable.CustomerTableGUID into ljcreditappCustomer
							  from customerTable in ljcreditappCustomer.DefaultIfEmpty()
							  where creditAppTable.CreditAppTableGUID == guid
							  select new UpdateExpectedSigningDateView
							  {
								  CreditAppTableGUID = creditAppTable.CreditAppTableGUID.ToString(),
								  CreditAppId = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
								  CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId.ToString(), customerTable.Name)
							  
							  }).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region shared
		public DateTime GetEndDateByCreditAppTableGUID(Guid guid)
		{
			try
			{
				var result = (from creditAppTable in Entity
							  join creditAppLine in db.Set<CreditAppLine>()
							  on creditAppTable.CreditAppTableGUID equals creditAppLine.CreditAppTableGUID
							  join buyerAgreementTrans in db.Set<BuyerAgreementTrans>()
							  on creditAppLine.CreditAppLineGUID equals buyerAgreementTrans.RefGUID into ljbuyerAgreementTrans
							  from buyerAgreementTrans in ljbuyerAgreementTrans.DefaultIfEmpty()
							  join buyerAgreementTable in db.Set<BuyerAgreementTable>()
							  on buyerAgreementTrans.BuyerAgreementTableGUID equals buyerAgreementTable.BuyerAgreementTableGUID
							  where buyerAgreementTrans.RefType == (int)RefType.CreditAppLine && creditAppTable.CreditAppTableGUID == guid
							  select new
							  {
								  EndDate = buyerAgreementTable.EndDate
							  }
							  ).OrderByDescending(o => o.EndDate).AsNoTracking().FirstOrDefault();
				return (result != null) ? result.EndDate: DateTime.MaxValue;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		private IQueryable<CreditOutstandingViewMap> GetCreditOutstandingByCustomerQuery(DateTime? asOfDate = null)
		{
			try
			{
				if (!asOfDate.HasValue)
				{
					asOfDate = DateTime.Now;
				}
				IQueryable<CreditOutstandingViewMap> calARBalance =
						(from invoice in db.Set<InvoiceTable>()
						 join custTrans in db.Set<CustTrans>().Where(w => w.CustTransStatus == (int)CustTransStatus.Open && w.CreditAppTableGUID.HasValue)
						 on invoice.InvoiceTableGUID equals custTrans.InvoiceTableGUID
						 join compaparameter in db.Set<CompanyParameter>() on invoice.CompanyGUID equals compaparameter.CompanyGUID
						 group new
						 {
							 custTrans,
							 invoice,
							 compaparameter
						 }
						 by new
						 {
							 custTrans.CreditAppTableGUID,
							 custTrans.InvoiceTypeGUID,
							 invoice.ProductInvoice,
							 compaparameter.FTReserveToBeRefundedInvTypeGUID
						 }
						 into g
						 select new CreditOutstandingViewMap
						 {
							 CreditAppTableGUID = g.Key.CreditAppTableGUID.Value,
							 ARBalance = (g.Key.ProductInvoice) ? g.Sum(su => su.custTrans.TransAmount - su.custTrans.SettleAmount) : 0,
							 ReserveToBeRefund = (g.Key.InvoiceTypeGUID == g.Key.FTReserveToBeRefundedInvTypeGUID)
												 ? g.Sum(su => su.custTrans.TransAmount - su.custTrans.SettleAmount) : 0
						 }).GroupBy(g => g.CreditAppTableGUID)
						 .Select(s => new CreditOutstandingViewMap
						 {
							 CreditAppTableGUID = s.Key,
							 ARBalance = s.Sum(su => su.ARBalance),
							 ReserveToBeRefund = s.Sum(su => su.ReserveToBeRefund),
						 });



				IQueryable<CreditOutstandingViewMap> result =
							 (from creditAppTable in Entity
							  join creditAppTrans in db.Set<CreditAppTrans>()
							  .GroupBy(g => g.CreditAppTableGUID)
							  .Select(s => new { CreditAppTableGUID = s.Key, TotalCreditDedutionAmount = s.Sum(su => su.CreditDeductAmount) })
							  on creditAppTable.CreditAppTableGUID equals creditAppTrans.CreditAppTableGUID into ljCreditAppTrans
							  from creditAppTrans in ljCreditAppTrans.DefaultIfEmpty()
							  join custTrans in calARBalance
							  on creditAppTable.CreditAppTableGUID equals custTrans.CreditAppTableGUID into ljCalARBalance
							  from custTrans in ljCalARBalance.DefaultIfEmpty()
							  join retentionTrans in db.Set<RetentionTrans>()
							  .GroupBy(g => g.CreditAppTableGUID)
							  .Select(s => new { CreditAppTableGUID = s.Key, AccumRetentionAmount = s.Sum(su => su.Amount) })
							  on creditAppTable.CreditAppTableGUID equals retentionTrans.CreditAppTableGUID into ljRetentionTrans
							  from retentionTrans in ljRetentionTrans.DefaultIfEmpty()
							  join creditimitType in db.Set<CreditLimitType>() on creditAppTable.CreditLimitTypeGUID equals creditimitType.CreditLimitTypeGUID
							  join customertable in db.Set<CustomerTable>() on creditAppTable.CustomerTableGUID equals customertable.CustomerTableGUID into ljcustomertable
							  from customertable in ljcustomertable.DefaultIfEmpty()
							  select new CreditOutstandingViewMap
							  {
								  CompanyGUID = creditAppTable.CompanyGUID,
								  Owner = creditAppTable.Owner,
								  OwnerBusinessUnitGUID = creditAppTable.OwnerBusinessUnitGUID,
								  CreditAppTableGUID = creditAppTable.CreditAppTableGUID,
								  ProductType = creditAppTable.ProductType,
								  CreditLimitTypeGUID = creditAppTable.CreditLimitTypeGUID,
								  ApprovedCreditLimit = creditAppTable.ApprovedCreditLimit,
								  CustomerTableGUID = creditAppTable.CustomerTableGUID,
								  ExpiryDate = creditAppTable.ExpiryDate,
								  AsOfDate = asOfDate.Value,
								  CreditLimitBalance = (creditAppTrans != null) ? creditAppTable.ApprovedCreditLimit - creditAppTrans.TotalCreditDedutionAmount: creditAppTable.ApprovedCreditLimit,
								  ARBalance = (custTrans != null) ? custTrans.ARBalance : 0,
								  ReserveToBeRefund = (custTrans != null)
													  ? (creditAppTable.ProductType == (int)ProductType.Factoring)
														? custTrans.ReserveToBeRefund : 0
													  : 0,
								  AccumRetentionAmount = (retentionTrans != null) ? retentionTrans.AccumRetentionAmount : 0,
								  ParentCreditLimitType = creditimitType.ParentCreditLimitTypeGUID,
								  CreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
								  CreditAppTable_CreditAppId = creditAppTable.CreditAppId,
								  CreditLimitType_Values = SmartAppUtil.GetDropDownLabel(creditimitType.CreditLimitTypeId, creditimitType.Description),
								  CreditLimitType_CreditLimitTypeId = creditimitType.CreditLimitTypeId,
								  CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customertable.CustomerId, customertable.Name),
								  CustomerTable_CustomerId = customertable.CustomerId,
							  }).AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<CreditOutstandingViewMap> GetCreditOutstandingByCustomer(Guid customerTableGUID, DateTime asOfDate)
		{
			try
			{
				List<CreditOutstandingViewMap> creditOustandingView =
					   GetCreditOutstandingByCustomerQuery(asOfDate)
					   .Where(w => w.CustomerTableGUID == customerTableGUID && w.ExpiryDate > asOfDate && w.ParentCreditLimitType == null)
					   .ToList();
				return creditOustandingView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public SearchResult<CreditOutstandingView> GetCreditOutstandingByCustomervw(SearchParameter search)
		{
			try
			{
				var result = new SearchResult<CreditOutstandingView>();

				var predicate = base.GetFilterLevelPredicate<CreditAppTable>(search, SysParm.CompanyGUID);
				var total = GetCreditOutstandingByCustomerQuery( DateTime.Now).Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetCreditOutstandingByCustomerQuery(DateTime.Now).Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CreditOutstandingViewMap, CreditOutstandingView>();
				result = list.SetSearchResult<CreditOutstandingView>(total, search);
				return result;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public override void ValidateAdd(IEnumerable<CreditAppTable> items)
		{
			base.ValidateAdd(items);
		}
		public override void ValidateAdd(CreditAppTable item)
		{
			base.ValidateAdd(item);
		}
		#region Bookmark document query 15CreditApplicationShared
		private QCustomer GetQCustomer(Guid CustomerTableGUID)
		{
			try
			{
				ICustomerTableRepo customerTable1 = new CustomerTableRepo(db);
				CustomerTable res_customerTable = customerTable1.GetCustomerTableByIdNoTrackingByAccessLevel(CustomerTableGUID);
				EmployeeTable res_employeeTable = (from employeeTable in db.Set<EmployeeTable>()
											   where employeeTable.EmployeeTableGUID == (Guid)res_customerTable.ResponsibleByGUID
											   select new EmployeeTable
											   {
												   Name = employeeTable.Name
											   }
											   ).FirstOrDefault();

				QCustomer result = (from customerTable in db.Set<CustomerTable>()
									where customerTable.CustomerTableGUID == CustomerTableGUID
									select new QCustomer
									{
										CustomerId = customerTable.CustomerId,
										CustomerName = customerTable.Name,
										IdentificationType = customerTable.IdentificationType,
										TaxID = customerTable.TaxID,
										PassportID = customerTable.PassportID,
										RecordType = customerTable.RecordType,
										DateOfEstablish = customerTable.DateOfEstablish.DateNullToString(),
										DateOfBirth = customerTable.DateOfBirth.DateNullToString(),
										IntroducedByGUID = customerTable.IntroducedByGUID.GuidNullToString(),
										BusinessTypeGUID = customerTable.BusinessTypeGUID.GuidNullToString(),
										ResponsibleByGUID = customerTable.ResponsibleByGUID.GuidNullToString(),
										ResponsibleBy = res_employeeTable.Name,
									}
									).AsNoTracking().FirstOrDefault();

				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private QPDCBank GetQPDCBank(Guid PDCBankGUID)
		{
			try
			{
				QPDCBank result = (from custBank in db.Set<CustBank>()
								   join bankGroup in db.Set<BankGroup>()
								   on custBank.BankGroupGUID equals bankGroup.BankGroupGUID into ljbankGroup
								   from bankGroup in ljbankGroup.DefaultIfEmpty()

								   join bankType in db.Set<BankType>()
								   on custBank.BankTypeGUID equals bankType.BankTypeGUID into ljbankType
								   from bankType in ljbankType.DefaultIfEmpty()

								   where custBank.CustBankGUID == PDCBankGUID
								   select new QPDCBank
								   {
									   BankGroupGUID = custBank.BankGroupGUID.GuidNullToString(),
									   CustPDCBankName = bankGroup.Description,
									   CustPDCBankBranch = custBank.BankBranch,
									   CustPDCBankType = bankType.Description,
									   CustPDCBankAccountName = custBank.AccountNumber
								   }
								   ).AsNoTracking().FirstOrDefault();
				return result;

			} catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		} 
		private List<QRetentionConditionTrans> GetQRetentionConditionTrans(Guid CreditAppRequestTableGUID)
		{
			try
			{
				int i = 1;
				int j = 0;
				List<QRetentionConditionTrans> result = new List<QRetentionConditionTrans>(new QRetentionConditionTrans[3]);
				List<QRetentionConditionTrans> tmp_result = (from retentionConditionTrans in db.Set<RetentionConditionTrans>()
														 where retentionConditionTrans.RefGUID == CreditAppRequestTableGUID
														 select new QRetentionConditionTrans
														 {
															 RetentionDeductionMethod = (RetentionDeductionMethod)retentionConditionTrans.RetentionDeductionMethod,
															 RetentionCalculateBase = (RetentionCalculateBase)retentionConditionTrans.RetentionCalculateBase,
															 RetentionPct = retentionConditionTrans.RetentionPct,
															 RetentionAmount = retentionConditionTrans.RetentionAmount,
														 }
												   ).Take(3).OrderBy(o => o.RetentionDeductionMethod).AsNoTracking().ToList();
				foreach (var item in tmp_result)
				{
					result[j] = item;
					item.Row = i;
					i++;
					j++;
				}

				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private QBankAccountControl GetQBankAccountControl(Guid BankAccountControlGUID)
		{
			try
			{
				QBankAccountControl result = (from custBank in db.Set<CustBank>()
											   join bankGroup in db.Set<BankGroup>()
											   on custBank.BankGroupGUID equals bankGroup.BankGroupGUID into ljbankGroup
											   from bankGroup in ljbankGroup.DefaultIfEmpty()

											   join bankType in db.Set<BankType>()
											   on custBank.BankTypeGUID equals bankType.BankTypeGUID into ljbankType
											   from bankType in ljbankType.DefaultIfEmpty()

											   where custBank.CustBankGUID == BankAccountControlGUID
											   select new QBankAccountControl
											   {
												   BankGroupGUID = custBank.BankGroupGUID.GuidNullToString(),
												   Instituetion = bankGroup.Description,
												   BankBranch = custBank.BankBranch,
												   BankTypeGUID = custBank.BankTypeGUID.GuidNullToString(),
												   AccountType = bankType.Description,
												   BankAccountName = custBank.BankAccountName,
												   AccountNumber = custBank.AccountNumber
												   
											   }
								   ).AsNoTracking().FirstOrDefault();
				return result;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QCreditAppReqBusinessCollateral> GetCreditAppReqBusinessCollateral(Guid CreditAppRequestTableGUID)
		{
			try
			{
				int i = 1;
				int j = 0;
				decimal sum = (from creditAppReqBusinessCollateral in db.Set<CreditAppReqBusinessCollateral>()
							   where creditAppReqBusinessCollateral.CreditAppRequestTableGUID == CreditAppRequestTableGUID
							   orderby creditAppReqBusinessCollateral.CreatedDateTime ascending
							   select creditAppReqBusinessCollateral.BusinessCollateralValue
							   ).Take(15).Sum();
				List<QCreditAppReqBusinessCollateral> result = new List<QCreditAppReqBusinessCollateral>(new QCreditAppReqBusinessCollateral[15]);
				List<QCreditAppReqBusinessCollateral> tmp_result = (from creditAppReqBusinessCollateral in db.Set<CreditAppReqBusinessCollateral>()
														  join businessCollateralType in db.Set<BusinessCollateralType>()
														  on creditAppReqBusinessCollateral.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID into ljbusinessCollateralType
														  from businessCollateralType in ljbusinessCollateralType.DefaultIfEmpty()
														  join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
														  on creditAppReqBusinessCollateral.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
														  from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()
														  where creditAppReqBusinessCollateral.CreditAppRequestTableGUID == CreditAppRequestTableGUID
														  //orderby creditAppReqBusinessCollateral.CreatedDateTime ascending
														  select new QCreditAppReqBusinessCollateral
														  {
															  IsNew = creditAppReqBusinessCollateral.IsNew,
															  BusinessCollateralTypeGUID = creditAppReqBusinessCollateral.BusinessCollateralTypeGUID.GuidNullToString(),
															  BusinessCollateralTypeId = businessCollateralType.Description,
															  BusinessCollateralSubTypeId = businessCollateralSubType.Description,
															  Description = creditAppReqBusinessCollateral.Description,
															  BusinessCollateralValue = creditAppReqBusinessCollateral.BusinessCollateralValue,
															  SumBusinessCollateralValue = sum,
															  CreatedDateTime = creditAppReqBusinessCollateral.CreatedDateTime,
														  }
														  ).OrderBy(o => o.CreatedDateTime).Take(15).AsNoTracking().ToList();
				foreach (var item in tmp_result)
				{
					result[j] = item;
					item.Row = i;
					i++;
					j++;
				}

				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private QSumCreditAppReqBusinessCollateral GetSumCreditAppReqBusinessCollateral(Guid CreditAppRequestTableGUID)
		{
			try
			{
				decimal sum = (from creditAppReqBusinessCollateral in db.Set<CreditAppReqBusinessCollateral>()
							   where creditAppReqBusinessCollateral.CreditAppRequestTableGUID == CreditAppRequestTableGUID
							   orderby creditAppReqBusinessCollateral.CreatedDateTime ascending
							   select creditAppReqBusinessCollateral.BusinessCollateralValue
							   ).Take(10).Sum();
				QSumCreditAppReqBusinessCollateral result = new QSumCreditAppReqBusinessCollateral
				{
					SumBusinessCollateralValue = sum
				};
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QServiceFeeConditionTrans> GetQServiceFeeConditionTrans(Guid CreditAppRequestTableGUID)
		{
			try
			{
				int j = 0;
				List<QServiceFeeConditionTrans> result = new List<QServiceFeeConditionTrans>(new QServiceFeeConditionTrans[10]);
				List<QServiceFeeConditionTrans> tmp_result = (from serviceFeeConditionTrans in db.Set<ServiceFeeConditionTrans>()
												   join invoiceRevenueType in db.Set<InvoiceRevenueType>()
												   on serviceFeeConditionTrans.InvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID into ljinvoiceRevenueType
												   from invoiceRevenueType in ljinvoiceRevenueType.DefaultIfEmpty()
												   where serviceFeeConditionTrans.RefGUID == CreditAppRequestTableGUID
												   orderby serviceFeeConditionTrans.Ordering ascending
															  select new QServiceFeeConditionTrans
												   {
													   Description = serviceFeeConditionTrans.Description,
													   AmountBeforeTax = serviceFeeConditionTrans.AmountBeforeTax,
													   InvoiceRevenueTypeGUID = serviceFeeConditionTrans.InvoiceRevenueTypeGUID.GuidNullToString(),
													   ServicefeeTypeId = invoiceRevenueType.RevenueTypeId,
													   ServicefeeTypeDesc = invoiceRevenueType.Description,
													   Row = serviceFeeConditionTrans.Ordering
															  }
												   ).Take(10).AsNoTracking().ToList();
				foreach (var item in tmp_result)
				{
					result[j] = item;
					j++;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private QSumServiceFeeConditionTrans GetSumServiceFeeConditionTrans(Guid CreditAppRequestTableGUID)
		{
			try
			{
				decimal sum = (from serviceFeeConditionTrans in db.Set<ServiceFeeConditionTrans>()
							   join invoiceRevenueType in db.Set<InvoiceRevenueType>()
							   on serviceFeeConditionTrans.InvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID into ljinvoiceRevenueType
							   from invoiceRevenueType in ljinvoiceRevenueType.DefaultIfEmpty()
							   where serviceFeeConditionTrans.RefGUID == CreditAppRequestTableGUID
							   select serviceFeeConditionTrans.AmountBeforeTax).Sum();
				QSumServiceFeeConditionTrans result = new QSumServiceFeeConditionTrans
				{
					ServiceFeeAmt = sum
				};
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QGuarantorTrans> GetQGuarantorTrans(Guid CreditAppRequestTableGUID)
		{
			try
			{
				int i = 1;
				int j = 0;
				List<QGuarantorTrans> result = new List<QGuarantorTrans>(new QGuarantorTrans[8]);
				List<QGuarantorTrans> tmp_result = (from guarantorTrans in db.Set< GuarantorTrans>()
										  join relatedPersonTable in db.Set<RelatedPersonTable>()
										  on guarantorTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID into ljrelatedPersonTable
										  from relatedPersonTable in ljrelatedPersonTable.DefaultIfEmpty()
										  join guarantorType in db.Set<GuarantorType>()
										  on guarantorTrans.GuarantorTypeGUID equals guarantorType.GuarantorTypeGUID into ljguarantorType
										  from guarantorType in ljguarantorType.DefaultIfEmpty()
										  where guarantorTrans.RefGUID == CreditAppRequestTableGUID && guarantorTrans.InActive == false
										  orderby guarantorTrans.Ordering ascending
													select new QGuarantorTrans
										  {
														RelatedPersonTableGUID = relatedPersonTable.RelatedPersonTableGUID.GuidNullToString(),
														RelatedPersonName = relatedPersonTable.Name,
														GuarantorType = guarantorType.Description,
													}

										  ).Take(8).AsNoTracking().ToList();
				foreach (var item in tmp_result)
				{
					result[j] = item;
					item.Row = i;
					i++;
					j++;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QAuthorizedPersonTrans> GetQAuthorizedPersonTrans(Guid CreditAppRequestTableGUID)
		{
			try
			{
				int i = 1;
				int j = 0;
				List<QAuthorizedPersonTrans> result = new List<QAuthorizedPersonTrans>( new QAuthorizedPersonTrans[8] );
				List<QAuthorizedPersonTrans>  tmp_result = (from authorizedPersonTrans in db.Set<AuthorizedPersonTrans>()
												 join relatedPersonTable in db.Set<RelatedPersonTable>()
												 on authorizedPersonTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID into ljrelatedPersonTable
												 from relatedPersonTable in ljrelatedPersonTable.DefaultIfEmpty()
												 where authorizedPersonTrans.RefGUID == CreditAppRequestTableGUID && authorizedPersonTrans.InActive == false
												 orderby authorizedPersonTrans.Ordering ascending
												 select new QAuthorizedPersonTrans
												 {
													 AuthorizedPersonName = relatedPersonTable.Name,
													 DateOfBirth = relatedPersonTable.DateOfBirth,
												 }
					).Take(8).ToList();
				foreach (var item in tmp_result)
				{
					result[j] = item;
					item.Row = i;
					i++;
					j++;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QOwnerTrans> GetQOwnerTrans(Guid CreditAppRequestTableGUID)
		{
			try
			{
				int i = 1;
				int j = 0;
				List<QOwnerTrans> result = new List<QOwnerTrans>(new QOwnerTrans[8]);
				List<QOwnerTrans> tmp_result = (from ownerTrans in db.Set<OwnerTrans>()
									 join relatedPersonTable in db.Set<RelatedPersonTable>()
									 on ownerTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID into ljrelatedPersonTable
									 from relatedPersonTable in ljrelatedPersonTable.DefaultIfEmpty()
									 where ownerTrans.RefGUID == CreditAppRequestTableGUID && ownerTrans.InActive == false
									 orderby ownerTrans.Ordering ascending
									 select new QOwnerTrans
									 {
										 RelatedPersonTableGUID = ownerTrans.RelatedPersonTable.GuidNullToString(),
										 ShareHolderPersonName = relatedPersonTable.Name,
										 PropotionOfShareholderPct = ownerTrans.PropotionOfShareholderPct,
									 }
					).Take(8).AsNoTracking().ToList();
				foreach (var item in tmp_result)
				{
					result[j] = item;
					item.Row = i;
					i++;
					j++;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QCreditAppReqLineFin> GetCreditAppReqLineFin(Guid CreditAppRequestTableGUID)
		{
			try
			{
				int i = 1;
				int j = 0;
				List<QCreditAppReqLineFin> result = new List<QCreditAppReqLineFin>(new QCreditAppReqLineFin[5]);
				List<QCreditAppReqLineFin> tmp_result = (from financialStatementTrans in db.Set<FinancialStatementTrans>()
														 where financialStatementTrans.RefGUID == CreditAppRequestTableGUID && financialStatementTrans.RefType == (int)RefType.CreditAppRequestTable
											   
														 select new QCreditAppReqLineFin { 
												   Year = financialStatementTrans.Year,
												   RegisteredCapital = (from financialStatementTrans1 in db.Set<FinancialStatementTrans>()
																		where financialStatementTrans1.RefGUID == CreditAppRequestTableGUID && financialStatementTrans1.RefType == (int)RefType.CreditAppRequestTable && financialStatementTrans1.Year == financialStatementTrans.Year && financialStatementTrans1.Ordering == 1
																		select financialStatementTrans1.Amount
																		).FirstOrDefault(),
												   PaidCapital = (from financialStatementTrans2 in db.Set<FinancialStatementTrans>()
																  where financialStatementTrans2.RefGUID == CreditAppRequestTableGUID && financialStatementTrans2.RefType == (int)RefType.CreditAppRequestTable && financialStatementTrans2.Year == financialStatementTrans.Year && financialStatementTrans2.Ordering == 2
																  select financialStatementTrans2.Amount).FirstOrDefault(),
												   TotalAsset = (from financialStatementTrans3 in db.Set<FinancialStatementTrans>()
																 where financialStatementTrans3.RefGUID == CreditAppRequestTableGUID && financialStatementTrans3.RefType == (int)RefType.CreditAppRequestTable && financialStatementTrans3.Year == financialStatementTrans.Year && financialStatementTrans3.Ordering == 3
																 select financialStatementTrans3.Amount).FirstOrDefault(),
												   TotalLiability = (from financialStatementTrans4 in db.Set<FinancialStatementTrans>()
																  where financialStatementTrans4.RefGUID == CreditAppRequestTableGUID && financialStatementTrans4.RefType == (int)RefType.CreditAppRequestTable && financialStatementTrans4.Year == financialStatementTrans.Year && financialStatementTrans4.Ordering == 4
																  select financialStatementTrans4.Amount).FirstOrDefault(),
												   TotalEquity = (from financialStatementTrans5 in db.Set<FinancialStatementTrans>()
																   where financialStatementTrans5.RefGUID == CreditAppRequestTableGUID && financialStatementTrans5.RefType == (int)RefType.CreditAppRequestTable && financialStatementTrans5.Year == financialStatementTrans.Year && financialStatementTrans5.Ordering == 5
																   select financialStatementTrans5.Amount).FirstOrDefault(),
												   TotalRevenue = (from financialStatementTrans6 in db.Set<FinancialStatementTrans>()
																where financialStatementTrans6.RefGUID == CreditAppRequestTableGUID && financialStatementTrans6.RefType == (int)RefType.CreditAppRequestTable && financialStatementTrans6.Year == financialStatementTrans.Year && financialStatementTrans6.Ordering == 6
																select financialStatementTrans6.Amount).FirstOrDefault(),
												   TotalCOGS = (from financialStatementTrans7 in db.Set<FinancialStatementTrans>()
																	   where financialStatementTrans7.RefGUID == CreditAppRequestTableGUID && financialStatementTrans7.RefType == (int)RefType.CreditAppRequestTable && financialStatementTrans7.Year == financialStatementTrans.Year && financialStatementTrans7.Ordering == 7
																	   select financialStatementTrans7.Amount).FirstOrDefault(),
												   TotalGrossProfit = (from financialStatementTrans8 in db.Set<FinancialStatementTrans>()
																		where financialStatementTrans8.RefGUID == CreditAppRequestTableGUID && financialStatementTrans8.RefType == (int)RefType.CreditAppRequestTable && financialStatementTrans8.Year == financialStatementTrans.Year && financialStatementTrans8.Ordering == 8
																		select financialStatementTrans8.Amount).FirstOrDefault(),
												   TotalOperExpFirst = (from financialStatementTrans9 in db.Set<FinancialStatementTrans>()
																		where financialStatementTrans9.RefGUID == CreditAppRequestTableGUID && financialStatementTrans9.RefType == (int)RefType.CreditAppRequestTable && financialStatementTrans9.Year == financialStatementTrans.Year && financialStatementTrans9.Ordering == 9
																		select financialStatementTrans9.Amount).FirstOrDefault(),
												   TotalNetProfitFirst = (from financialStatementTrans10 in db.Set<FinancialStatementTrans>()
																		  where financialStatementTrans10.RefGUID == CreditAppRequestTableGUID && financialStatementTrans10.RefType == (int)RefType.CreditAppRequestTable && financialStatementTrans10.Year == financialStatementTrans.Year && financialStatementTrans10.Ordering == 10
																		  select financialStatementTrans10.Amount).FirstOrDefault(),
												   NetProfitPercent = (from financialStatementTrans11 in db.Set<FinancialStatementTrans>()
																	   where financialStatementTrans11.RefGUID == CreditAppRequestTableGUID && financialStatementTrans11.RefType == (int)RefType.CreditAppRequestTable && financialStatementTrans11.Year == financialStatementTrans.Year && financialStatementTrans11.Ordering == 11
																	   select financialStatementTrans11.Amount).FirstOrDefault(),
												   lDE = (from financialStatementTrans12 in db.Set<FinancialStatementTrans>()
														  where financialStatementTrans12.RefGUID == CreditAppRequestTableGUID && financialStatementTrans12.RefType == (int)RefType.CreditAppRequestTable && financialStatementTrans12.Year == financialStatementTrans.Year && financialStatementTrans12.Ordering == 12
														  select financialStatementTrans12.Amount).FirstOrDefault(),
												   QuickRatio = (from financialStatementTrans13 in db.Set<FinancialStatementTrans>()
																 where financialStatementTrans13.RefGUID == CreditAppRequestTableGUID && financialStatementTrans13.RefType == (int)RefType.CreditAppRequestTable && financialStatementTrans13.Year == financialStatementTrans.Year && financialStatementTrans13.Ordering == 13
																 select financialStatementTrans13.Amount).FirstOrDefault(),
												   IntCoverageRatio = (from financialStatementTrans14 in db.Set<FinancialStatementTrans>()
																	   where financialStatementTrans14.RefGUID == CreditAppRequestTableGUID && financialStatementTrans14.RefType == (int)RefType.CreditAppRequestTable && financialStatementTrans14.Year == financialStatementTrans.Year && financialStatementTrans14.Ordering == 14
																	   select financialStatementTrans14.Amount).FirstOrDefault()
											   }).Distinct().OrderByDescending(f => f.Year).Take(5).ToList();
				foreach (var item in tmp_result)
				{
					result[j] = item;
					item.Row = i;
					i++;
					j++;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QTaxReportTrans> GetQTaxReportTrans(Guid CreditAppRequestTableGUID)
		{
			try
			{
				int i = 1;
				int j = 0;
				CultureInfo ci = new CultureInfo("en-EN");
				List<QTaxReportTrans> result = new List<QTaxReportTrans>(new QTaxReportTrans[12]);
				List<QTaxReportTrans> tmp_result = (from taxReportTrans in db.Set<TaxReportTrans>()
										  where taxReportTrans.RefGUID == CreditAppRequestTableGUID
										  orderby taxReportTrans.Year descending 
										  orderby taxReportTrans.Month ascending
										  select new QTaxReportTrans
										  {
											  TaxMonth = ci.DateTimeFormat.GetMonthName(taxReportTrans.Month) + "-" + taxReportTrans.Year.ToString(),
											  Amount = taxReportTrans.Amount,
										  }
										  ).Take(12).AsNoTracking().ToList();
				foreach (var item in tmp_result)
				{
					result[j] = item;
					item.Row = i;
					i++;
					j++;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private QTaxReportStart GetQTaxReportStart(Guid CreditAppRequestTableGUID)
		{
			try
			{
				CultureInfo ci = new CultureInfo("en-US");
				QTaxReportStart result =(from taxReportTrans in db.Set<TaxReportTrans>()
										 where taxReportTrans.RefGUID == CreditAppRequestTableGUID
										 orderby taxReportTrans.Year descending
										 orderby taxReportTrans.Month ascending
										 select new QTaxReportStart
										 {
											 TaxMonth = ci.DateTimeFormat.GetMonthName(taxReportTrans.Month) + " " + taxReportTrans.Year.ToString(),
										 }
										 ).Take(1).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private QTaxReportEnd GetQTaxReportEnd(Guid CreditAppRequestTableGUID)
		{
			try
			{
				CultureInfo ci = new CultureInfo("en-US");
				QTaxReportEnd result = (from taxReportTrans in db.Set<TaxReportTrans>()
										  where taxReportTrans.RefGUID == CreditAppRequestTableGUID
										  orderby taxReportTrans.Year descending
										  orderby taxReportTrans.Month descending
										  select new QTaxReportEnd
										  {
											  TaxMonth = ci.DateTimeFormat.GetMonthName(taxReportTrans.Month) + " " + taxReportTrans.Year.ToString(),
										  }
										 ).Take(1).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QTaxReportTransSum> GetQTaxReportTransSum(Guid CreditAppRequestTableGUID)
		{
			try
			{
				List<QTaxReportTransSum>  result = (from taxReportTrans in db.Set<TaxReportTrans>()
											  where taxReportTrans.RefGUID == CreditAppRequestTableGUID
											  group taxReportTrans by 1 into GtaxReportTrans
											  select new QTaxReportTransSum
											  {
												  SumTaxReport = GtaxReportTrans.Sum(x => x.Amount),
												  TaxReportCOUNT = GtaxReportTrans.Count()
											  }
										 ).Take(1).ToList();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QCAReqCreditOutStanding> GetCAReqCreditOutStanding(Guid CreditAppRequestTableGUID)
		{
			try
			{
				int i = 1;
				int j = 0;
				List<QCAReqCreditOutStanding> result = new List<QCAReqCreditOutStanding>(new QCAReqCreditOutStanding[12]);
				List<QCAReqCreditOutStanding> tmp_result = (from cAReqCreditOutStanding in db.Set<CAReqCreditOutStanding>()
															join creditLimitType in db.Set<CreditLimitType>()
															on cAReqCreditOutStanding.CreditLimitTypeGUID equals creditLimitType.CreditLimitTypeGUID into ljcreditLimitType
															from creditLimitType in ljcreditLimitType.DefaultIfEmpty()
															where cAReqCreditOutStanding.CreditAppRequestTableGUID == CreditAppRequestTableGUID
															//orderby creditLimitType.BookmarkOrdering ascending
															select new QCAReqCreditOutStanding
															{
																AsOfDate = cAReqCreditOutStanding.AsOfDate.DateNullToString(),
																ProductType = cAReqCreditOutStanding.ProductType.ToString(),
																BookmarkOrdering = creditLimitType != null ? creditLimitType.BookmarkOrdering : 0,
																CreditLimitTypeId = creditLimitType != null ? creditLimitType.CreditLimitTypeId : "",
																CreditLimitTypeDesc = creditLimitType != null ? creditLimitType.Description : "",
																ApprovedCreditLimit = cAReqCreditOutStanding.ApprovedCreditLimit ,
																ARBalance = cAReqCreditOutStanding.ARBalance,
																CreditLimitBalance = cAReqCreditOutStanding.CreditLimitBalance,
																ReserveToBeRefund = cAReqCreditOutStanding.ReserveToBeRefund,
																AccumRetentionAmount = cAReqCreditOutStanding.AccumRetentionAmount
															}
					).OrderBy(o => o.BookmarkOrdering).Take(12).AsNoTracking().ToList();
				foreach (var item in tmp_result)
				{
					result[j] = item;
					item.Row = i;
					i++;
					j++;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QCAReqCreditOutStanding> GetCAReqCreditOutStanding2(Guid CreditAppRequestTableGUID)
		{
			try
			{
				int i = 1;
				int j = 0;

				List<QCAReqCreditOutStanding> LjResult = new List<QCAReqCreditOutStanding>(new QCAReqCreditOutStanding[12]);
				List<QCAReqCreditOutStanding> LJ_tmp_result = (from cAReqCreditOutStanding in db.Set<CAReqCreditOutStanding>()
															where cAReqCreditOutStanding.CreditAppRequestTableGUID == CreditAppRequestTableGUID
															group cAReqCreditOutStanding by new
                                                            {
																//cAReqCreditOutStanding.AsOfDate,
																cAReqCreditOutStanding.CreditLimitTypeGUID,
																//cAReqCreditOutStanding.ApprovedCreditLimit,
																//cAReqCreditOutStanding.ARBalance,
																//cAReqCreditOutStanding.CreditLimitBalance,
																//cAReqCreditOutStanding.ReserveToBeRefund,
																//cAReqCreditOutStanding.AccumRetentionAmount
															} into g
															select new QCAReqCreditOutStanding
															{
																//AsOfDate = g.Key.AsOfDate.DateNullToString(),
																//ProductType = g.Key.ProductType.ToString(),
																//BookmarkOrdering = g.Key.BookmarkOrdering,
																//CreditLimitTypeDesc = g.Key.Description,
																CreditLimitTypeGUID = g.Key.CreditLimitTypeGUID,
																ApprovedCreditLimit = g.Sum(x => x.ApprovedCreditLimit),
																ARBalance = g.Sum(x => x.ARBalance),
																CreditLimitBalance = g.Sum(x => x.CreditLimitBalance),
																ReserveToBeRefund = g.Sum(x => x.ReserveToBeRefund),
																AccumRetentionAmount = g.Sum(x => x.AccumRetentionAmount)
															}
															).Take(12).ToList();

				List<QCAReqCreditOutStanding> result = new List<QCAReqCreditOutStanding>(new QCAReqCreditOutStanding[12]);
				List<QCAReqCreditOutStanding> tmp_result = (from creditLimitType  in db.Set< CreditLimitType  >()
															orderby creditLimitType.BookmarkOrdering ascending
															select new QCAReqCreditOutStanding
															{
																CreditLimitTypeGUID = creditLimitType.CreditLimitTypeGUID,
																CreditLimitTypeDesc = creditLimitType.Description,
															}
					).Take(12).ToList();
				foreach (var item in tmp_result)
				{
					foreach(var item2 in LJ_tmp_result)
                    {
						if(item.CreditLimitTypeGUID == item2.CreditLimitTypeGUID)
                        {
							item.AsOfDate = item2.AsOfDate;
							item.ApprovedCreditLimit = item2.ApprovedCreditLimit;
							item.ARBalance = item2.ARBalance;
							item.CreditLimitBalance = item2.CreditLimitBalance;
							item.ReserveToBeRefund = item2.ReserveToBeRefund;
							item.AccumRetentionAmount = item2.AccumRetentionAmount;
						}
                    }
					result[j] = item;
					item.Row = i;
					i++;
					j++;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private QCAReqCreditOutStandingSum GetReqCreditOutStandingSum(Guid CreditAppRequestTableGUID)
		{
			try
			{
				QCAReqCreditOutStandingSum result = (from cAReqCreditOutStanding in db.Set<CAReqCreditOutStanding>()
													 where cAReqCreditOutStanding.CreditAppRequestTableGUID == CreditAppRequestTableGUID
													 group cAReqCreditOutStanding by 1 into GcAReqCreditOutStanding
													 select new QCAReqCreditOutStandingSum
													 {
														 SumApprovedCreditLimit = GcAReqCreditOutStanding.Sum(a => a.ApprovedCreditLimit),
														 SumARBalance = GcAReqCreditOutStanding.Sum(b => b.ARBalance),
														 SumCreditLimitBalance = GcAReqCreditOutStanding.Sum(c => c.CreditLimitBalance),
														 SumAccumRetentionAmount = GcAReqCreditOutStanding.Sum(d => d.AccumRetentionAmount),
														 SumReserveToBeRefund = GcAReqCreditOutStanding.Sum(e => e.ReserveToBeRefund)

													 }).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QNCBTrans> GetQNCBTrans(Guid CreditAppRequestTableGUID)
		{
			try
			{
				int i = 1;
				int j = 0;
				List<QNCBTrans> result = new List<QNCBTrans>(new QNCBTrans[8]);
				List<QNCBTrans> tmp_result = (from nCBTrans in db.Set<NCBTrans>()
											  join bankGroup in db.Set<BankGroup>()
											  on nCBTrans.BankGroupGUID equals bankGroup.BankGroupGUID into ljbankGroup
											  from bankGroup in ljbankGroup.DefaultIfEmpty()

											  join creditType in db.Set<CreditType>()
											  on nCBTrans.CreditTypeGUID equals creditType.CreditTypeGUID into ljcreditType
											  from creditType in ljcreditType.DefaultIfEmpty()

											  join nCBAccountStatus in db.Set<NCBAccountStatus>()
											  on nCBTrans.NCBAccountStatusGUID equals nCBAccountStatus.NCBAccountStatusGUID into ljnCBAccountStatus
											  from nCBAccountStatus in ljnCBAccountStatus.DefaultIfEmpty()

											  where nCBTrans.RefGUID == CreditAppRequestTableGUID
											  //orderby nCBAccountStatus.NCBAccStatusId descending
											  //orderby creditType.CreditTypeId ascending
											  
											  select new QNCBTrans
												{
													BankGroupGUID = nCBTrans.BankGroupGUID.GuidNullToString(),
													Institution = bankGroup.Description,
													CreditTypeGUID = nCBTrans.CreditTypeGUID.GuidNullToString(),
													CreditTypeId = creditType.CreditTypeId,
													LoanType = creditType.Description,
													LoanLimit = nCBTrans.CreditLimit,
													NCBAccountStatusGUID = nCBAccountStatus.NCBAccountStatusGUID.ToString(),
													NCBAccountStatusId = nCBAccountStatus.NCBAccStatusId,
														  }
					).OrderBy(f => f.CreditTypeId).ThenByDescending(f => f.NCBAccountStatusId).Take(8).ToList();
				 //).Take(8).ToList();
				foreach (var item in tmp_result)
				{
					result[j] = item;
					item.Row = i;
					i++;
					j++;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QActionHistory> GetQActionHistory(Guid CreditAppRequestTableGUID)
		{
			try
			{
				int i = 1;
				int j = 0;
				List<QActionHistory> result = new List<QActionHistory>(new QActionHistory[5]);
				List<QActionHistory> tmp_result =(from actionHistory in db.Set<ActionHistory>()
									where (actionHistory.ActivityName == "Assist MD approve" || actionHistory.ActivityName == "Board 2 in 3 approve" || actionHistory.ActivityName == "Board 3 in 4 approve") && actionHistory.ActionName == "Approve" && actionHistory.RefGUID == CreditAppRequestTableGUID
										orderby actionHistory.CreatedDateTime ascending
										select new QActionHistory
										{
											Comment = actionHistory.Comment,
											CreatedDateTime = actionHistory.CreatedDateTime.DateToString(),
											ActionName = actionHistory.ActionName,
											ApproverName = (from employeeTable in db.Set<EmployeeTable>()
															where employeeTable.EmployeeId == actionHistory.CreatedBy
															select employeeTable.Name
															).AsNoTracking().FirstOrDefault(),

			}
					).Take(5).AsNoTracking().ToList();
				foreach (var item in tmp_result)
				{
					result[j] = item;
					item.Row = i;
					i++;
					j++;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QCreditAppReqAssignment> GetQCreditAppReqAssignment(Guid CreditAppRequestTableGUID)
		{
			try
			{
				int i = 1;
				int j = 0;
				List<QCreditAppReqAssignment> result = new List<QCreditAppReqAssignment>(new QCreditAppReqAssignment[10]);
				List<QCreditAppReqAssignment> tmp_result = (from creditAppReqAssignment in db.Set<CreditAppReqAssignment>()
												  join buyerTable in db.Set<BuyerTable>() 
												  on creditAppReqAssignment.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
												  from buyerTable in ljbuyerTable.DefaultIfEmpty()

												  join assignmentAgreementTable in db.Set<AssignmentAgreementTable>()
												  on creditAppReqAssignment.AssignmentAgreementTableGUID equals assignmentAgreementTable.AssignmentAgreementTableGUID into ljassignmentAgreementTable
												  from assignmentAgreementTable in ljassignmentAgreementTable.DefaultIfEmpty()

												  join buyerAgreementTable in db.Set<BuyerAgreementTable>()
												  on creditAppReqAssignment.BuyerAgreementTableGUID equals buyerAgreementTable.BuyerAgreementTableGUID into ljbuyerAgreementTable
												  from buyerAgreementTable in ljbuyerAgreementTable.DefaultIfEmpty()

												  where creditAppReqAssignment.CreditAppRequestTableGUID == CreditAppRequestTableGUID
												  select new QCreditAppReqAssignment
												  {
													  IsNew = creditAppReqAssignment.IsNew,
													  AssignmentBuyer = buyerTable.BuyerName + " (" + buyerTable.BuyerId+")",
													  BuyerAgreementTableGUID = creditAppReqAssignment.BuyerAgreementTableGUID.GuidNullToString(),
													  RefAgreementId = buyerAgreementTable != null ? buyerAgreementTable.BuyerAgreementId : "",
													  BuyerAgreementAmount = creditAppReqAssignment.BuyerAgreementAmount,
													  AssignmentAgreementAmount = creditAppReqAssignment.AssignmentAgreementAmount,
													  AssignmentAgreementDate = assignmentAgreementTable != null ? assignmentAgreementTable.AgreementDate.DateToString() : "",
													  AssignmentAgreementTableGUID = creditAppReqAssignment.AssignmentAgreementTableGUID.GuidNullToString(),
													  RemainingAmount = creditAppReqAssignment.RemainingAmount,
													  CreatedDateTime = creditAppReqAssignment.CreatedDateTime
												  }
					).OrderBy(f => f.CreatedDateTime).Take(10).ToList();
				foreach (var item in tmp_result)
				{
					result[j] = item;
					item.Row = i;
					i++;
					j++;
				}
				return result;
			 }
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private QSumCreditAppReqAssignment GetSumCreditAppReqAssignment(Guid CreditAppRequestTableGUID)
		{
			try
			{
				QSumCreditAppReqAssignment result = (from creditAppReqAssignment in db.Set<CreditAppReqAssignment>()
													 join buyerTable in db.Set<BuyerTable>()
													 on creditAppReqAssignment.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
													 from buyerTable in ljbuyerTable.DefaultIfEmpty()
													 join assignmentAgreementTable in db.Set<AssignmentAgreementTable>()
													 on creditAppReqAssignment.AssignmentAgreementTableGUID equals assignmentAgreementTable.AssignmentAgreementTableGUID into ljassignmentAgreementTable
													 from assignmentAgreementTable in ljassignmentAgreementTable.DefaultIfEmpty()
													 where creditAppReqAssignment.CreditAppRequestTableGUID == CreditAppRequestTableGUID
													 group creditAppReqAssignment by 1 into GcreditAppReqAssignment
													 select new QSumCreditAppReqAssignment
													 {
														 SumBuyerAgreementAmount = GcreditAppReqAssignment.Sum(x => x.BuyerAgreementAmount),
														 SumAssignmentAgreementAmount = GcreditAppReqAssignment.Sum(y => y.AssignmentAgreementAmount),
														 SumRemainingAmount = GcreditAppReqAssignment.Sum(z => z.RemainingAmount)
													 }
													 ).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BookmarkDocumentCreditApplication GetBookmaskDocumentCreditApplication(Guid refGUID)
		{
			try
			{
				ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
				CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTrackingByAccessLevel(refGUID);

				QCustomer qCustomer = GetQCustomer(creditAppRequestTable.CustomerTableGUID);
				QPDCBank qPDCBank = creditAppRequestTable.PDCBankGUID != null ? GetQPDCBank((Guid)creditAppRequestTable.PDCBankGUID) : null;
				List<QRetentionConditionTrans> qRetentionConditionTrans = GetQRetentionConditionTrans(creditAppRequestTable.CreditAppRequestTableGUID);
				QBankAccountControl qBankAccountControl = creditAppRequestTable.BankAccountControlGUID != null ? GetQBankAccountControl((Guid)creditAppRequestTable.BankAccountControlGUID) : null;
				List<QCreditAppReqBusinessCollateral> qCreditAppReqBusinessCollateral = GetCreditAppReqBusinessCollateral(creditAppRequestTable.CreditAppRequestTableGUID);
				QSumCreditAppReqBusinessCollateral qSumCreditAppReqBusinessCollateral = GetSumCreditAppReqBusinessCollateral(creditAppRequestTable.CreditAppRequestTableGUID);
				List<QServiceFeeConditionTrans> qServiceFeeConditionTrans = GetQServiceFeeConditionTrans(creditAppRequestTable.CreditAppRequestTableGUID);
				QSumServiceFeeConditionTrans qSumServiceFeeConditionTrans = GetSumServiceFeeConditionTrans(creditAppRequestTable.CreditAppRequestTableGUID);
				List<QGuarantorTrans> qGuarantorTrans = GetQGuarantorTrans(creditAppRequestTable.CreditAppRequestTableGUID);
				List<QAuthorizedPersonTrans> qAuthorizedPersonTrans = GetQAuthorizedPersonTrans(creditAppRequestTable.CreditAppRequestTableGUID);
				List<QOwnerTrans> qOwnerTrans = GetQOwnerTrans(creditAppRequestTable.CreditAppRequestTableGUID);
				List<QCreditAppReqLineFin> qCreditAppReqLineFin = GetCreditAppReqLineFin(creditAppRequestTable.CreditAppRequestTableGUID);
				List<QTaxReportTrans> qTaxReportTrans = GetQTaxReportTrans(creditAppRequestTable.CreditAppRequestTableGUID);
				QTaxReportStart qTaxReportStart = GetQTaxReportStart(creditAppRequestTable.CreditAppRequestTableGUID);
				QTaxReportEnd qTaxReportEnd = GetQTaxReportEnd(creditAppRequestTable.CreditAppRequestTableGUID);
				List<QTaxReportTransSum> qTaxReportTransSum = GetQTaxReportTransSum(creditAppRequestTable.CreditAppRequestTableGUID);
				List<QCAReqCreditOutStanding> qCAReqCreditOutStanding = GetCAReqCreditOutStanding2(creditAppRequestTable.CreditAppRequestTableGUID);
				QCAReqNCBTransSum qQCAReqNCBTransSum = GetCAReqNCBTransSum2(creditAppRequestTable.CreditAppRequestTableGUID);
				QCAReqCreditOutStandingSum qCAReqCreditOutStandingSum = GetReqCreditOutStandingSum(creditAppRequestTable.CreditAppRequestTableGUID);
				List<QNCBTrans> qNCBTrans = GetQNCBTrans(creditAppRequestTable.CreditAppRequestTableGUID);
				List<QActionHistory> qActionHistory = GetQActionHistory(creditAppRequestTable.CreditAppRequestTableGUID);
				List<QCreditAppReqAssignment> qCreditAppReqAssignment = GetQCreditAppReqAssignment(creditAppRequestTable.CreditAppRequestTableGUID);
				QSumCreditAppReqAssignment qSumCreditAppReqAssignment = GetSumCreditAppReqAssignment(creditAppRequestTable.CreditAppRequestTableGUID);
				QCAReqCreditOutStandingAsOfDate qCAReqCreditOutStandingAsOfDate = getQCAReqCreditOutStandingAsOfDate(creditAppRequestTable.CreditAppRequestTableGUID);

				#region QCreditAppRequestTable 
				QCreditAppRequestTable qCreditAppRequestTable = (from tmp_creditAppRequestTable in db.Set<CreditAppRequestTable>()
																 where tmp_creditAppRequestTable.CreditAppRequestTableGUID == refGUID
																 select new QCreditAppRequestTable
																 {
																	 CreditAppRequestId = tmp_creditAppRequestTable.CreditAppRequestId,
																	 RefCreditAppTableGUID = tmp_creditAppRequestTable.RefCreditAppTableGUID.GuidNullToString(),
																	 OriginalCreditAppTableId = (from creditAppTable in db.Set<CreditAppTable>()
																								 where tmp_creditAppRequestTable.RefCreditAppTableGUID == creditAppTable.CreditAppTableGUID
																								 select creditAppTable.CreditAppId
																								 ).AsNoTracking().FirstOrDefault(),
																	 OriginalPurchaseWithdrawalCondition = (from creditAppTable in db.Set<CreditAppTable>()
																											where tmp_creditAppRequestTable.RefCreditAppTableGUID == creditAppTable.CreditAppTableGUID
																											select creditAppTable.PurchaseWithdrawalCondition
																								 ).AsNoTracking().FirstOrDefault(),
																	 OriginalAssignmentMethodGUID = (from creditAppTable in db.Set<CreditAppTable>()
																							 where tmp_creditAppRequestTable.RefCreditAppTableGUID == creditAppTable.CreditAppTableGUID
																							 select creditAppTable.AssignmentMethodGUID
																								 ).FirstOrDefault().GuidNullToString(),
																	 OriginalAssignmentMethodTable = (from assignmentMethod in db.Set<AssignmentMethod>()
																									  where (from creditAppTable in db.Set<CreditAppTable>()
																											 where tmp_creditAppRequestTable.RefCreditAppTableGUID == creditAppTable.CreditAppTableGUID
																											 select creditAppTable.AssignmentMethodGUID
																								 ).FirstOrDefault() == assignmentMethod.AssignmentMethodGUID
																									  select assignmentMethod.Description
																								 ).AsNoTracking().FirstOrDefault(),
																	 RequestDate = tmp_creditAppRequestTable.RequestDate.DateToString(),
																	 CustomerTableGUID = tmp_creditAppRequestTable.CustomerTableGUID.GuidNullToString(),
																	 Description = tmp_creditAppRequestTable.Description,
																	 Remark = tmp_creditAppRequestTable.Remark,
																	 ProductType = tmp_creditAppRequestTable.ProductType,
																	 CreditLimitExpiration = tmp_creditAppRequestTable.CreditLimitExpiration,
																	 StartDate = tmp_creditAppRequestTable.StartDate.DateToString(),
																	 ExpiryDate = tmp_creditAppRequestTable.ExpiryDate.DateToString(),
																	 CreditLimitRequest = tmp_creditAppRequestTable.CreditLimitRequest,
																	 InterestAdjustment = tmp_creditAppRequestTable.InterestAdjustment,
																	 InterestValue = "",
																	 MaxPurchasePct = tmp_creditAppRequestTable.MaxPurchasePct,
																	 MaxRetentionPct = tmp_creditAppRequestTable.MaxRetentionPct,
																	 MaxRetentionAmount = tmp_creditAppRequestTable.MaxRetentionAmount,
																	 CACondition = tmp_creditAppRequestTable.CACondition,
																	 PDCBankGUID = tmp_creditAppRequestTable.PDCBankGUID.GuidNullToString(),
																	 CreditRequestFeePct = tmp_creditAppRequestTable.CreditRequestFeePct,
																	 CreditRequestFeeAmount = tmp_creditAppRequestTable.CreditRequestFeeAmount,
																	 PurchaseFeePct = tmp_creditAppRequestTable.PurchaseFeePct,
																	 PurchaseFeeCalculateBase = tmp_creditAppRequestTable.PurchaseFeeCalculateBase,
																	 BankAccountControlGUID = tmp_creditAppRequestTable.BankAccountControlGUID.GuidNullToString(),
																	 SalesAvgPerMonth = tmp_creditAppRequestTable.SalesAvgPerMonth,
																	 CustomerCreditLimit = tmp_creditAppRequestTable.CustomerCreditLimit,
																	 NCBCheckedDate = tmp_creditAppRequestTable.NCBCheckedDate.DateNullToString(),
																	 MarketingComment = tmp_creditAppRequestTable.MarketingComment,
																	 CreditComment = tmp_creditAppRequestTable.CreditComment,
																	 ApproverComment = tmp_creditAppRequestTable.ApproverComment,
																	 DocumentStatusGUID = tmp_creditAppRequestTable.DocumentStatusGUID.GuidNullToString(),
																	 Status = (from documentStatus in db.Set<DocumentStatus>()
																			   join documentProcess in db.Set<DocumentProcess>()
																			   on documentStatus.DocumentProcessGUID equals documentProcess.DocumentProcessGUID into ljdocumentProcess
																			   from documentProcess in ljdocumentProcess.DefaultIfEmpty()
																			   where tmp_creditAppRequestTable.DocumentStatusGUID == documentStatus.DocumentStatusGUID && documentProcess.ProcessId == ((int)DocumentProcessStatus.CreditAppRequestTable).ToString()
																			   select documentStatus.Description).AsNoTracking().FirstOrDefault(),
																	 KYC = (from kYCSetup in db.Set<KYCSetup>()
																			where kYCSetup.KYCSetupGUID == (Guid)tmp_creditAppRequestTable.KYCGUID
																			select kYCSetup.Description
																			).AsNoTracking().FirstOrDefault(),
																	 CreditScoring = (from creditScoring in db.Set<CreditScoring>()
																					  where creditScoring.CreditScoringGUID == tmp_creditAppRequestTable.CreditScoringGUID
																					  select creditScoring.Description
																			).AsNoTracking().FirstOrDefault(),
																	 Purpose = (from documentReason in db.Set<DocumentReason>()
																				where documentReason.DocumentReasonGUID == tmp_creditAppRequestTable.DocumentReasonGUID
																				select documentReason.Description
																			).AsNoTracking().FirstOrDefault(),
																	 ProductSubType = (from productSubType in db.Set<ProductSubType>()
																					   where productSubType.ProductSubTypeGUID == tmp_creditAppRequestTable.ProductSubTypeGUID
																					   select productSubType.Description
																			).AsNoTracking().FirstOrDefault(),
																	 CreditLimitTypeId = (from creditLimitType in db.Set<CreditLimitType>()
																						where creditLimitType.CreditLimitTypeGUID == tmp_creditAppRequestTable.CreditLimitTypeGUID
																						select creditLimitType.Description
																			).AsNoTracking().FirstOrDefault(),
																	 CreditLimitType = (from creditLimitType in db.Set<CreditLimitType>()
																						where creditLimitType.CreditLimitTypeGUID == tmp_creditAppRequestTable.CreditLimitTypeGUID
																						select creditLimitType.Description
																			).AsNoTracking().FirstOrDefault(),
																	 InterestType = (from interestType in db.Set<InterestType>()
																					 where interestType.InterestTypeGUID == tmp_creditAppRequestTable.InterestTypeGUID
																					 select interestType.Description
																			).AsNoTracking().FirstOrDefault(),
																	 InterestTypeGUID = tmp_creditAppRequestTable.InterestTypeGUID.ToString(),
																	 RegisteredAddressGUID = tmp_creditAppRequestTable.RegisteredAddressGUID.GuidNullToString(),
																	 RegisteredAddress = (from addressTrans in db.Set<AddressTrans>()
																						  where addressTrans.AddressTransGUID == tmp_creditAppRequestTable.RegisteredAddressGUID
																						  select addressTrans.Address1 +" " +addressTrans.Address2
																			).AsNoTracking().FirstOrDefault(),
																	 IntroducedByGUID = qCustomer.IntroducedByGUID,
																	 IntroducedBy = (from introducedBy in db.Set<IntroducedBy>()
																					 where introducedBy.IntroducedByGUID == qCustomer.IntroducedByGUID.StringToGuid()
																					 select introducedBy.Description
																			).AsNoTracking().FirstOrDefault(),
																	 BusinessTypeGUID = qCustomer.BusinessTypeGUID,
																	 BusinessType = (from businessType in db.Set<BusinessType>()
																					 where businessType.BusinessTypeGUID == qCustomer.BusinessTypeGUID.StringToGuid()
																					 select businessType.Description
																			).AsNoTracking().FirstOrDefault(),
																	 PurchaseWithdrawalCondition = tmp_creditAppRequestTable.PurchaseWithdrawalCondition,
																	 AssignmentMethodGUID = tmp_creditAppRequestTable.AssignmentMethodGUID.GuidNullToString(),
																	 AssignmentMethodTable = (from assignmentMethod in db.Set<AssignmentMethod>()
																							  where tmp_creditAppRequestTable.AssignmentMethodGUID == assignmentMethod.AssignmentMethodGUID
																							  select assignmentMethod.Description
																								 ).AsNoTracking().FirstOrDefault(),
																 }
					).AsNoTracking().FirstOrDefault();

				#endregion


				IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService(db);
				IInterestTypeValueRepo interestTypeValueRepo = new InterestTypeValueRepo(db);
				BookmarkDocumentCreditApplication_Variable variable = new BookmarkDocumentCreditApplication_Variable()
				{
					AuthorizedPersonTrans1Age = qAuthorizedPersonTrans[0] != null ? relatedPersonTableService.CalcAge(qAuthorizedPersonTrans[0].DateOfBirth).ToString() : "-",
					AuthorizedPersonTrans2Age = qAuthorizedPersonTrans[1] != null ? relatedPersonTableService.CalcAge(qAuthorizedPersonTrans[1].DateOfBirth).ToString() : "-",
					AuthorizedPersonTrans3Age = qAuthorizedPersonTrans[2] != null ? relatedPersonTableService.CalcAge(qAuthorizedPersonTrans[2].DateOfBirth).ToString() : "-",
					AuthorizedPersonTrans4Age = qAuthorizedPersonTrans[3] != null ? relatedPersonTableService.CalcAge(qAuthorizedPersonTrans[3].DateOfBirth).ToString() : "-",
					AuthorizedPersonTrans5Age = qAuthorizedPersonTrans[4] != null ? relatedPersonTableService.CalcAge(qAuthorizedPersonTrans[4].DateOfBirth).ToString() : "-",
					AuthorizedPersonTrans6Age = qAuthorizedPersonTrans[5] != null ? relatedPersonTableService.CalcAge(qAuthorizedPersonTrans[5].DateOfBirth).ToString() : "-",
					AuthorizedPersonTrans7Age = qAuthorizedPersonTrans[6] != null ? relatedPersonTableService.CalcAge(qAuthorizedPersonTrans[6].DateOfBirth).ToString() : "-",
					AuthorizedPersonTrans8Age = qAuthorizedPersonTrans[7] != null ? relatedPersonTableService.CalcAge(qAuthorizedPersonTrans[7].DateOfBirth).ToString() : "-",

					CustCompanyRegistrationID = qCustomer.IdentificationType == (decimal)IdentificationType.TaxId ? qCustomer.TaxID : qCustomer.IdentificationType == (decimal)IdentificationType.PassportId ? qCustomer.PassportID : "-",
					CustComEstablishedDate = qCustomer.RecordType == (decimal)RecordType.Person ? qCustomer.DateOfBirth : qCustomer.RecordType == (decimal)RecordType.Organization ? qCustomer.DateOfEstablish : "-",
					AverageOutputVATPerMonth = qTaxReportTransSum[0] != null ? (qTaxReportTransSum[0].TaxReportCOUNT != 0 ? decimal.Round(qTaxReportTransSum[0].SumTaxReport / qTaxReportTransSum[0].TaxReportCOUNT ,2) : 0) : 0,

					BusinessCollateralIsNew1 = qCreditAppReqBusinessCollateral[0] != null ? qCreditAppReqBusinessCollateral[0].IsNew == true ? "Yes" : "No" : "-",
					BusinessCollateralIsNew2 = qCreditAppReqBusinessCollateral[1] != null ? qCreditAppReqBusinessCollateral[1].IsNew == true ? "Yes" : "No" : "-",
					BusinessCollateralIsNew3 = qCreditAppReqBusinessCollateral[2] != null ? qCreditAppReqBusinessCollateral[2].IsNew == true ? "Yes" : "No" : "-",
					BusinessCollateralIsNew4 = qCreditAppReqBusinessCollateral[3] != null ? qCreditAppReqBusinessCollateral[3].IsNew == true ? "Yes" : "No" : "-",
					BusinessCollateralIsNew5 = qCreditAppReqBusinessCollateral[4] != null ? qCreditAppReqBusinessCollateral[4].IsNew == true ? "Yes" : "No" : "-",
					BusinessCollateralIsNew6 = qCreditAppReqBusinessCollateral[5] != null ? qCreditAppReqBusinessCollateral[5].IsNew == true ? "Yes" : "No" : "-",
					BusinessCollateralIsNew7 = qCreditAppReqBusinessCollateral[6] != null ? qCreditAppReqBusinessCollateral[6].IsNew == true ? "Yes" : "No" : "-",
					BusinessCollateralIsNew8 = qCreditAppReqBusinessCollateral[7] != null ? qCreditAppReqBusinessCollateral[7].IsNew == true ? "Yes" : "No" : "-",
					BusinessCollateralIsNew9 = qCreditAppReqBusinessCollateral[8] != null ? qCreditAppReqBusinessCollateral[8].IsNew == true ? "Yes" : "No" : "-",
					BusinessCollateralIsNew10 = qCreditAppReqBusinessCollateral[9] != null ? qCreditAppReqBusinessCollateral[9].IsNew == true ? "Yes" : "No" : "-",
					BusinessCollateralIsNew11 = qCreditAppReqBusinessCollateral[10] != null ? qCreditAppReqBusinessCollateral[10].IsNew == true ? "Yes" : "No" : "-",
					BusinessCollateralIsNew12 = qCreditAppReqBusinessCollateral[11] != null ? qCreditAppReqBusinessCollateral[11].IsNew == true ? "Yes" : "No" : "-",
					BusinessCollateralIsNew13 = qCreditAppReqBusinessCollateral[12] != null ? qCreditAppReqBusinessCollateral[12].IsNew == true ? "Yes" : "No" : "-",
					BusinessCollateralIsNew14 = qCreditAppReqBusinessCollateral[13] != null ? qCreditAppReqBusinessCollateral[13].IsNew == true ? "Yes" : "No" : "-",
					BusinessCollateralIsNew15 = qCreditAppReqBusinessCollateral[14] != null ? qCreditAppReqBusinessCollateral[14].IsNew == true ? "Yes" : "No" : "-",
					InterestValue = interestTypeValueRepo.GetByInterestTypeDateNoTracking(qCreditAppRequestTable.InterestTypeGUID.StringToGuid(), qCreditAppRequestTable.RequestDate.StringToDate(), false).Value,
					NewInterestRate = qCreditAppRequestTable.InterestType == null ? qCreditAppRequestTable.InterestAdjustment : interestTypeValueRepo.GetByInterestTypeDateNoTracking(qCreditAppRequestTable.InterestTypeGUID.StringToGuid(), qCreditAppRequestTable.RequestDate.StringToDate(), false).Value + qCreditAppRequestTable.InterestAdjustment,
				};

				BookmarkDocumentCreditApplication bookmarkDocumentCreditApplication = new BookmarkDocumentCreditApplication() {
					QCreditAppRequestTable_CreditAppRequestId = qCreditAppRequestTable.CreditAppRequestId,
					QCreditAppRequestTable_OriginalCreditAppTableId = qCreditAppRequestTable.OriginalCreditAppTableId,
					QCreditAppRequestTable_Status = qCreditAppRequestTable.Status,
					QCreditAppRequestTable_RequestDate = qCreditAppRequestTable.RequestDate,
					QCreditAppRequestTable_CustomerName = qCustomer.CustomerName,
					QCreditAppRequestTable_CustomerId = qCustomer.CustomerId,
					QCreditAppRequestTable_ResponsibleBy = qCustomer.ResponsibleBy,
					QCreditAppRequestTable_KYC = qCreditAppRequestTable.KYC,
					QCreditAppRequestTable_CreditScoring = qCreditAppRequestTable.CreditScoring,
					QCreditAppRequestTable_Description = qCreditAppRequestTable.Description,
					QCreditAppRequestTable_Purpose = qCreditAppRequestTable.Purpose,
					QCreditAppRequestTable_CARemark = qCreditAppRequestTable.Remark,
					QCreditAppRequestTable_ProductType = SystemStaticData.GetTranslatedMessage(((ProductType)qCreditAppRequestTable.ProductType).GetAttrCode()),
					QCreditAppRequestTable_ProductSubType = qCreditAppRequestTable.ProductSubType,
					QCreditAppRequestTable_CreditLimitTypeId = qCreditAppRequestTable.CreditLimitTypeId,
					QCreditAppRequestTable_CreditLimitExpiration = SystemStaticData.GetTranslatedMessage(((CreditLimitExpiration)qCreditAppRequestTable.CreditLimitExpiration).GetAttrCode()),
					QCreditAppRequestTable_StartDate = qCreditAppRequestTable.StartDate,
					QCreditAppRequestTable_ExpiryDate = qCreditAppRequestTable.ExpiryDate,
					QCreditAppRequestTable_CreditLimitRequest = qCreditAppRequestTable.CreditLimitRequest,
					QCreditAppRequestTable_CustomerCreditLimit = qCreditAppRequestTable.CustomerCreditLimit,
					QCreditAppRequestTable_InterestType = qCreditAppRequestTable.InterestType,
					QCreditAppRequestTable_InterestAdjustment = qCreditAppRequestTable.InterestAdjustment,
					Variable_NewInterestRate = variable.NewInterestRate,
					QCreditAppRequestTable_MaxPurchasePct = qCreditAppRequestTable != null ? qCreditAppRequestTable.MaxPurchasePct : 0,
					QCreditAppRequestTable_MaxRetentionPct = qCreditAppRequestTable != null ? qCreditAppRequestTable.MaxRetentionPct : 0,
					QCreditAppRequestTable_MaxRetentionAmount = qCreditAppRequestTable != null ? qCreditAppRequestTable.MaxRetentionAmount : 0,
					QCreditAppRequestTable_CustPDCBankName = qPDCBank != null ? qPDCBank.CustPDCBankName : "-",
					QCreditAppRequestTable_CustPDCBankBranch = qPDCBank != null ? qPDCBank.CustPDCBankBranch : "-",
					QCreditAppRequestTable_CustPDCBankType = qPDCBank != null ? qPDCBank.CustPDCBankType : "-",
					QCreditAppRequestTable_CustPDCBankAccountName = qPDCBank != null ? qPDCBank.CustPDCBankAccountName : "-",
					QCreditAppRequestTable_CreditRequestFeePct = qCreditAppRequestTable != null ? qCreditAppRequestTable.CreditRequestFeePct : 0,
					QCreditAppRequestTable_CreditRequestFeeAmount = qCreditAppRequestTable != null ? qCreditAppRequestTable.CreditRequestFeeAmount : 0,
					QCreditAppRequestTable_PurchaseFeePct = qCreditAppRequestTable != null ? qCreditAppRequestTable.PurchaseFeePct : 0,
					QCreditAppRequestTable_PurchaseFeeCalculateBase = SystemStaticData.GetTranslatedMessage(((PurchaseFeeCalculateBase)qCreditAppRequestTable.PurchaseFeeCalculateBase).GetAttrCode()),
					QCreditAppRequestTable_CACondition = qCreditAppRequestTable != null ? qCreditAppRequestTable.CACondition : "-",
					QRetentionConditionTrans1_RetentionDeductionMethod = qRetentionConditionTrans[0] != null ? SystemStaticData.GetTranslatedMessage(((RetentionDeductionMethod)qRetentionConditionTrans[0].RetentionDeductionMethod).GetAttrCode()) : "-",
					QRetentionConditionTrans1_RetentionCalculateBase = qRetentionConditionTrans[0] != null ? SystemStaticData.GetTranslatedMessage(((RetentionCalculateBase)qRetentionConditionTrans[0].RetentionCalculateBase).GetAttrCode()) : "-",
					QRetentionConditionTrans1_RetentionPct = qRetentionConditionTrans[0] != null ? qRetentionConditionTrans[0].RetentionPct : 0,
					tentionConditionTrans1_RetentionAmount = qRetentionConditionTrans[0] != null ? qRetentionConditionTrans[0].RetentionAmount : 0,
					QRetentionConditionTrans2_RetentionDeductionMethod = qRetentionConditionTrans[1] != null ? SystemStaticData.GetTranslatedMessage(((RetentionDeductionMethod)qRetentionConditionTrans[1].RetentionDeductionMethod).GetAttrCode()) : "-",
					QRetentionConditionTrans2_RetentionCalculateBase = qRetentionConditionTrans[1] != null ? SystemStaticData.GetTranslatedMessage(((RetentionCalculateBase)qRetentionConditionTrans[1].RetentionCalculateBase).GetAttrCode()) : "-",
					QRetentionConditionTrans2_RetentionPct = qRetentionConditionTrans[1] != null ? qRetentionConditionTrans[1].RetentionPct : 0,
					tentionConditionTrans2_RetentionAmount = qRetentionConditionTrans[1] != null ? qRetentionConditionTrans[1].RetentionAmount : 0,
					QRetentionConditionTrans3_RetentionDeductionMethod = qRetentionConditionTrans[2] != null ? SystemStaticData.GetTranslatedMessage(((RetentionDeductionMethod)qRetentionConditionTrans[2].RetentionDeductionMethod).GetAttrCode()) : "-",
					QRetentionConditionTrans3_RetentionCalculateBase = qRetentionConditionTrans[2] != null ? SystemStaticData.GetTranslatedMessage(((RetentionCalculateBase)qRetentionConditionTrans[2].RetentionCalculateBase).GetAttrCode()) : "-",
					QRetentionConditionTrans3_RetentionPct = qRetentionConditionTrans[2] != null ? qRetentionConditionTrans[2].RetentionPct : 0,
					tentionConditionTrans3_RetentionAmount = qRetentionConditionTrans[2] != null ? qRetentionConditionTrans[2].RetentionAmount : 0,
					QServiceFeeConditionTrans1_ServicefeeTypeId = qServiceFeeConditionTrans[0] != null ? qServiceFeeConditionTrans[0].ServicefeeTypeDesc : "-",
					QServiceFeeConditionTrans1_AmountBeforeTax = qServiceFeeConditionTrans[0] != null ? qServiceFeeConditionTrans[0].AmountBeforeTax : 0,
					QServiceFeeConditionTrans1_Description = qServiceFeeConditionTrans[0] != null ? qServiceFeeConditionTrans[0].Description : "-",
					QServiceFeeConditionTrans2_ServicefeeTypeId = qServiceFeeConditionTrans[1] != null ? qServiceFeeConditionTrans[1].ServicefeeTypeDesc : "-",
					QServiceFeeConditionTrans2_AmountBeforeTax = qServiceFeeConditionTrans[1] != null ? qServiceFeeConditionTrans[1].AmountBeforeTax : 0,
					QServiceFeeConditionTrans2_Description = qServiceFeeConditionTrans[1] != null ? qServiceFeeConditionTrans[1].Description : "-",
					QServiceFeeConditionTrans3_ServicefeeTypeId = qServiceFeeConditionTrans[2] != null ? qServiceFeeConditionTrans[2].ServicefeeTypeDesc : "-",
					QServiceFeeConditionTrans3_AmountBeforeTax = qServiceFeeConditionTrans[2] != null ? qServiceFeeConditionTrans[2].AmountBeforeTax : 0,
					QServiceFeeConditionTrans3_Description = qServiceFeeConditionTrans[2] != null ? qServiceFeeConditionTrans[2].Description : "-",
					QServiceFeeConditionTrans4_ServicefeeTypeId = qServiceFeeConditionTrans[3] != null ? qServiceFeeConditionTrans[3].ServicefeeTypeDesc : "-",
					QServiceFeeConditionTrans4_AmountBeforeTax = qServiceFeeConditionTrans[3] != null ? qServiceFeeConditionTrans[3].AmountBeforeTax : 0,
					QServiceFeeConditionTrans4_Description = qServiceFeeConditionTrans[3] != null ? qServiceFeeConditionTrans[3].Description : "-",
					QServiceFeeConditionTrans5_ServicefeeTypeId = qServiceFeeConditionTrans[4] != null ? qServiceFeeConditionTrans[4].ServicefeeTypeDesc : "-",
					QServiceFeeConditionTrans5_AmountBeforeTax = qServiceFeeConditionTrans[4] != null ? qServiceFeeConditionTrans[4].AmountBeforeTax : 0,
					QServiceFeeConditionTrans5_Description = qServiceFeeConditionTrans[4] != null ? qServiceFeeConditionTrans[4].Description : "-",
					QServiceFeeConditionTrans6_ServicefeeTypeId = qServiceFeeConditionTrans[5] != null ? qServiceFeeConditionTrans[5].ServicefeeTypeDesc : "-",
					QServiceFeeConditionTrans6_AmountBeforeTax = qServiceFeeConditionTrans[5] != null ? qServiceFeeConditionTrans[5].AmountBeforeTax : 0,
					QServiceFeeConditionTrans6_Description = qServiceFeeConditionTrans[5] != null ? qServiceFeeConditionTrans[5].Description : "-",
					QServiceFeeConditionTrans7_ServicefeeTypeId = qServiceFeeConditionTrans[6] != null ? qServiceFeeConditionTrans[6].ServicefeeTypeDesc : "-",
					QServiceFeeConditionTrans7_AmountBeforeTax = qServiceFeeConditionTrans[6] != null ? qServiceFeeConditionTrans[6].AmountBeforeTax : 0,
					QServiceFeeConditionTrans7_Description = qServiceFeeConditionTrans[6] != null ? qServiceFeeConditionTrans[6].Description : "-",
					QServiceFeeConditionTrans8_ServicefeeTypeId = qServiceFeeConditionTrans[7] != null ? qServiceFeeConditionTrans[7].ServicefeeTypeDesc : "-",
					QServiceFeeConditionTrans8_AmountBeforeTax = qServiceFeeConditionTrans[7] != null ? qServiceFeeConditionTrans[7].AmountBeforeTax : 0,
					QServiceFeeConditionTrans8_Description = qServiceFeeConditionTrans[7] != null ? qServiceFeeConditionTrans[7].Description : "-",
					QServiceFeeConditionTrans9_ServicefeeTypeId = qServiceFeeConditionTrans[8] != null ? qServiceFeeConditionTrans[8].ServicefeeTypeDesc : "-",
					QServiceFeeConditionTrans9_AmountBeforeTax = qServiceFeeConditionTrans[8] != null ? qServiceFeeConditionTrans[8].AmountBeforeTax : 0,
					QServiceFeeConditionTrans9_Description = qServiceFeeConditionTrans[8] != null ? qServiceFeeConditionTrans[8].Description : "-",
					QServiceFeeConditionTrans10_ServicefeeTypeId = qServiceFeeConditionTrans[9] != null ? qServiceFeeConditionTrans[9].ServicefeeTypeDesc : "-",
					QServiceFeeConditionTrans10_AmountBeforeTax = qServiceFeeConditionTrans[9] != null ? qServiceFeeConditionTrans[9].AmountBeforeTax : 0,
					QServiceFeeConditionTrans10_Description = qServiceFeeConditionTrans[9] != null ? qServiceFeeConditionTrans[9].Description : "-",
					QSumServiceFeeConditionTrans_ServiceFeeAmt = qSumServiceFeeConditionTrans != null ? qSumServiceFeeConditionTrans.ServiceFeeAmt : 0,
					QGuarantorTrans1_RelatedPersonName = qGuarantorTrans[0] != null ? qGuarantorTrans[0].RelatedPersonName : "-",
					QGuarantorTrans1_GuarantorType = qGuarantorTrans[0] != null ? qGuarantorTrans[0].GuarantorType : "-",
					QGuarantorTrans2_RelatedPersonName = qGuarantorTrans[1] != null ? qGuarantorTrans[1].RelatedPersonName : "-",
					QGuarantorTrans2_GuarantorType = qGuarantorTrans[1] != null ? qGuarantorTrans[1].GuarantorType : "-",
					QGuarantorTrans3_RelatedPersonName = qGuarantorTrans[2] != null ? qGuarantorTrans[2].RelatedPersonName : "-",
					QGuarantorTrans3_GuarantorType = qGuarantorTrans[2] != null ? qGuarantorTrans[2].GuarantorType : "-",
					QGuarantorTrans4_RelatedPersonName = qGuarantorTrans[3] != null ? qGuarantorTrans[3].RelatedPersonName : "-",
					QGuarantorTrans4_GuarantorType = qGuarantorTrans[3] != null ? qGuarantorTrans[3].GuarantorType : "-",
					QGuarantorTrans5_RelatedPersonName = qGuarantorTrans[4] != null ? qGuarantorTrans[4].RelatedPersonName : "-",
					QGuarantorTrans5_GuarantorType = qGuarantorTrans[4] != null ? qGuarantorTrans[4].GuarantorType : "-",
					QGuarantorTrans6_RelatedPersonName = qGuarantorTrans[5] != null ? qGuarantorTrans[5].RelatedPersonName : "-",
					QGuarantorTrans6_GuarantorType = qGuarantorTrans[5] != null ? qGuarantorTrans[5].GuarantorType : "-",
					QGuarantorTrans7_RelatedPersonName = qGuarantorTrans[6] != null ? qGuarantorTrans[6].RelatedPersonName : "-",
					QGuarantorTrans7_GuarantorType = qGuarantorTrans[6] != null ? qGuarantorTrans[6].GuarantorType : "-",
					QGuarantorTrans8_RelatedPersonName = qGuarantorTrans[7] != null ? qGuarantorTrans[7].RelatedPersonName : "-",
					QGuarantorTrans8_GuarantorType = qGuarantorTrans[7] != null ? qGuarantorTrans[7].GuarantorType : "-",
					Variable_IsNew1 = qCreditAppReqAssignment[0] != null ? qCreditAppReqAssignment[0].IsNew == true ? "Yes" : "No" : "-",
					QCreditAppReqAssignment1_AssignmentBuyer = qCreditAppReqAssignment[0] != null ? qCreditAppReqAssignment[0].AssignmentBuyer : "-",
					QCreditAppReqAssignment1_RefAgreementId = qCreditAppReqAssignment[0] != null ? qCreditAppReqAssignment[0].RefAgreementId : "-",
					QCreditAppReqAssignment1_BuyerAgreementAmount = qCreditAppReqAssignment[0] != null ? qCreditAppReqAssignment[0].BuyerAgreementAmount : 0,
					QCreditAppReqAssignment1_AssignmentAgreementAmount = qCreditAppReqAssignment[0] != null ? qCreditAppReqAssignment[0].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignment1_AssignmentAgreementDate = qCreditAppReqAssignment[0] != null ? qCreditAppReqAssignment[0].AssignmentAgreementDate : "-",
					QCreditAppReqAssignment1_RemainingAmount = qCreditAppReqAssignment[0] != null ? qCreditAppReqAssignment[0].RemainingAmount : 0,
					Variable_IsNew2 = qCreditAppReqAssignment[1] != null ? qCreditAppReqAssignment[1].IsNew == true ? "Yes" : "No" : "-",
					QCreditAppReqAssignment2_AssignmentBuyer = qCreditAppReqAssignment[1] != null ? qCreditAppReqAssignment[1].AssignmentBuyer: "-",
					QCreditAppReqAssignment2_RefAgreementId = qCreditAppReqAssignment[1] != null ? qCreditAppReqAssignment[1].RefAgreementId : "-",
					QCreditAppReqAssignment2_BuyerAgreementAmount = qCreditAppReqAssignment[1] != null ? qCreditAppReqAssignment[1].BuyerAgreementAmount : 0,
					QCreditAppReqAssignment2_AssignmentAgreementAmount = qCreditAppReqAssignment[1] != null ? qCreditAppReqAssignment[1].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignment2_AssignmentAgreementDate = qCreditAppReqAssignment[1] != null ? qCreditAppReqAssignment[1].AssignmentAgreementDate : "-",
					QCreditAppReqAssignment2_RemainingAmount = qCreditAppReqAssignment[1] != null ? qCreditAppReqAssignment[1].RemainingAmount : 0,
					Variable_IsNew3 = qCreditAppReqAssignment[2] != null ? qCreditAppReqAssignment[2].IsNew == true ? "Yes" : "No" : "-",
					QCreditAppReqAssignment3_AssignmentBuyer = qCreditAppReqAssignment[2] != null ? qCreditAppReqAssignment[2].AssignmentBuyer : "-",
					QCreditAppReqAssignment3_RefAgreementId = qCreditAppReqAssignment[2] != null ? qCreditAppReqAssignment[2].RefAgreementId : "-",
					QCreditAppReqAssignment3_BuyerAgreementAmount = qCreditAppReqAssignment[2] != null ? qCreditAppReqAssignment[2].BuyerAgreementAmount : 0,
					QCreditAppReqAssignment3_AssignmentAgreementAmount = qCreditAppReqAssignment[2] != null ? qCreditAppReqAssignment[2].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignment3_AssignmentAgreementDate = qCreditAppReqAssignment[2] != null ? qCreditAppReqAssignment[2].AssignmentAgreementDate : "-",
					QCreditAppReqAssignment3_RemainingAmount = qCreditAppReqAssignment[2] != null ? qCreditAppReqAssignment[2].RemainingAmount : 0,
					Variable_IsNew4 = qCreditAppReqAssignment[3] != null ? qCreditAppReqAssignment[3].IsNew == true ? "Yes" : "No" : "-",
					QCreditAppReqAssignment4_AssignmentBuyer = qCreditAppReqAssignment[3] != null ? qCreditAppReqAssignment[3].AssignmentBuyer : "-",
					QCreditAppReqAssignment4_RefAgreementId = qCreditAppReqAssignment[3] != null ? qCreditAppReqAssignment[3].RefAgreementId : "-",
					QCreditAppReqAssignment4_BuyerAgreementAmount = qCreditAppReqAssignment[3] != null ? qCreditAppReqAssignment[3].BuyerAgreementAmount : 0,
					QCreditAppReqAssignment4_AssignmentAgreementAmount = qCreditAppReqAssignment[3] != null ? qCreditAppReqAssignment[3].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignment4_AssignmentAgreementDate = qCreditAppReqAssignment[3] != null ? qCreditAppReqAssignment[3].AssignmentAgreementDate : "-",
					QCreditAppReqAssignment4_RemainingAmount = qCreditAppReqAssignment[3] != null ? qCreditAppReqAssignment[3].RemainingAmount : 0,
					Variable_IsNew5 = qCreditAppReqAssignment[4] != null ? qCreditAppReqAssignment[4].IsNew == true ? "Yes" : "No" : "-",
					QCreditAppReqAssignment5_AssignmentBuyer = qCreditAppReqAssignment[4] != null ? qCreditAppReqAssignment[4].AssignmentBuyer : "-",
					QCreditAppReqAssignment5_RefAgreementId = qCreditAppReqAssignment[4] != null ? qCreditAppReqAssignment[4].RefAgreementId : "-",
					QCreditAppReqAssignment5_BuyerAgreementAmount = qCreditAppReqAssignment[4] != null ? qCreditAppReqAssignment[4].BuyerAgreementAmount : 0,
					QCreditAppReqAssignment5_AssignmentAgreementAmount = qCreditAppReqAssignment[4] != null ? qCreditAppReqAssignment[4].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignment5_AssignmentAgreementDate = qCreditAppReqAssignment[4] != null ? qCreditAppReqAssignment[4].AssignmentAgreementDate : "-",
					QCreditAppReqAssignment5_RemainingAmount = qCreditAppReqAssignment[4] != null ? qCreditAppReqAssignment[4].RemainingAmount : 0,
					Variable_IsNew6 = qCreditAppReqAssignment[5] != null ? qCreditAppReqAssignment[5].IsNew == true ? "Yes" : "No" : "-",
					QCreditAppReqAssignment6_AssignmentBuyer = qCreditAppReqAssignment[5] != null ? qCreditAppReqAssignment[5].AssignmentBuyer : "-",
					QCreditAppReqAssignment6_RefAgreementId = qCreditAppReqAssignment[5] != null ? qCreditAppReqAssignment[5].RefAgreementId : "-",
					QCreditAppReqAssignment6_BuyerAgreementAmount = qCreditAppReqAssignment[5] != null ? qCreditAppReqAssignment[5].BuyerAgreementAmount : 0,
					QCreditAppReqAssignment6_AssignmentAgreementAmount = qCreditAppReqAssignment[5] != null ? qCreditAppReqAssignment[5].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignment6_AssignmentAgreementDate = qCreditAppReqAssignment[5] != null ? qCreditAppReqAssignment[5].AssignmentAgreementDate : "-",
					QCreditAppReqAssignment6_RemainingAmount = qCreditAppReqAssignment[5] != null ? qCreditAppReqAssignment[5].RemainingAmount : 0,
					Variable_IsNew7 = qCreditAppReqAssignment[6] != null ? qCreditAppReqAssignment[6].IsNew == true ? "Yes" : "No" : "-",
					QCreditAppReqAssignment7_AssignmentBuyer = qCreditAppReqAssignment[6] != null ? qCreditAppReqAssignment[6].AssignmentBuyer : "-",
					QCreditAppReqAssignment7_RefAgreementId = qCreditAppReqAssignment[6] != null ? qCreditAppReqAssignment[6].RefAgreementId : "-",
					QCreditAppReqAssignment7_BuyerAgreementAmount = qCreditAppReqAssignment[6] != null ? qCreditAppReqAssignment[6].BuyerAgreementAmount : 0,
					QCreditAppReqAssignment7_AssignmentAgreementAmount = qCreditAppReqAssignment[6] != null ? qCreditAppReqAssignment[6].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignment7_AssignmentAgreementDate = qCreditAppReqAssignment[6] != null ? qCreditAppReqAssignment[6].AssignmentAgreementDate : "-",
					QCreditAppReqAssignment7_RemainingAmount = qCreditAppReqAssignment[6] != null ? qCreditAppReqAssignment[6].RemainingAmount : 0,
					Variable_IsNew8 = qCreditAppReqAssignment[7] != null ? qCreditAppReqAssignment[7].IsNew == true ? "Yes" : "No" : "-",
					QCreditAppReqAssignment8_AssignmentBuyer = qCreditAppReqAssignment[7] != null ? qCreditAppReqAssignment[7].AssignmentBuyer : "-",
					QCreditAppReqAssignment8_RefAgreementId = qCreditAppReqAssignment[7] != null ? qCreditAppReqAssignment[7].RefAgreementId : "-",
					QCreditAppReqAssignment8_BuyerAgreementAmount = qCreditAppReqAssignment[7] != null ? qCreditAppReqAssignment[7].BuyerAgreementAmount : 0,
					QCreditAppReqAssignment8_AssignmentAgreementAmount = qCreditAppReqAssignment[7] != null ? qCreditAppReqAssignment[7].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignment8_AssignmentAgreementDate = qCreditAppReqAssignment[7] != null ? qCreditAppReqAssignment[7].AssignmentAgreementDate : "-",
					QCreditAppReqAssignment8_RemainingAmount = qCreditAppReqAssignment[7] != null ? qCreditAppReqAssignment[7].RemainingAmount : 0,
					Variable_IsNew9 = qCreditAppReqAssignment[8] != null ? qCreditAppReqAssignment[8].IsNew == true ? "Yes" : "No" : "-",
					QCreditAppReqAssignment9_AssignmentBuyer = qCreditAppReqAssignment[8] != null ? qCreditAppReqAssignment[8].AssignmentBuyer : "-",
					QCreditAppReqAssignment9_RefAgreementId = qCreditAppReqAssignment[8] != null ? qCreditAppReqAssignment[8].RefAgreementId : "-",
					QCreditAppReqAssignment9_BuyerAgreementAmount = qCreditAppReqAssignment[8] != null ? qCreditAppReqAssignment[8].BuyerAgreementAmount : 0,
					QCreditAppReqAssignment9_AssignmentAgreementAmount = qCreditAppReqAssignment[8] != null ? qCreditAppReqAssignment[8].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignment9_AssignmentAgreementDate = qCreditAppReqAssignment[8] != null ? qCreditAppReqAssignment[8].AssignmentAgreementDate : "-",
					QCreditAppReqAssignment9_RemainingAmount = qCreditAppReqAssignment[8] != null ? qCreditAppReqAssignment[8].RemainingAmount : 0,
					Variable_IsNew10 = qCreditAppReqAssignment[9] != null ? qCreditAppReqAssignment[9].IsNew== true ? "Yes" : "No" : "-",
					QCreditAppReqAssignment10_AssignmentBuyer = qCreditAppReqAssignment[9] != null ? qCreditAppReqAssignment[9].AssignmentBuyer : "-",
					QCreditAppReqAssignment10_RefAgreementId = qCreditAppReqAssignment[9] != null ? qCreditAppReqAssignment[9].RefAgreementId : "-",
					QCreditAppReqAssignment10_BuyerAgreementAmount = qCreditAppReqAssignment[9] != null ? qCreditAppReqAssignment[9].BuyerAgreementAmount : 0,
					QCreditAppReqAssignment10_AssignmentAgreementAmount = qCreditAppReqAssignment[9] != null ? qCreditAppReqAssignment[9].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignment10_AssignmentAgreementDate = qCreditAppReqAssignment[9] != null ? qCreditAppReqAssignment[9].AssignmentAgreementDate : "-",
					QCreditAppReqAssignment10_RemainingAmount = qCreditAppReqAssignment[9] != null ? qCreditAppReqAssignment[9].RemainingAmount : 0,
					//---------------------------------------------------------------------------------------------
					QSumCreditAppReqAssignment_SumBuyerAgreementAmount = qSumCreditAppReqAssignment != null ? qSumCreditAppReqAssignment.SumBuyerAgreementAmount : 0,
					QSumCreditAppReqAssignment_SumAssignmentAgreementAmount = qSumCreditAppReqAssignment != null ? qSumCreditAppReqAssignment.SumAssignmentAgreementAmount : 0,
					QSumCreditAppReqAssignment_SumRemainingAmount = qSumCreditAppReqAssignment != null ? qSumCreditAppReqAssignment.SumRemainingAmount : 0,
					QCreditAppReqBusinessCollateral1_IsNew = qCreditAppReqBusinessCollateral[0] != null ? variable.BusinessCollateralIsNew1 : "-",
					QCreditAppReqBusinessCollateral1_BusinessCollateralType = qCreditAppReqBusinessCollateral[0] != null ? qCreditAppReqBusinessCollateral[0].BusinessCollateralTypeId : "-",
					QCreditAppReqBusinessCollateral1_BusinessCollateralSubType = qCreditAppReqBusinessCollateral[0] != null ? qCreditAppReqBusinessCollateral[0].BusinessCollateralSubTypeId : "-",
					QCreditAppReqBusinessCollateral1_Description = qCreditAppReqBusinessCollateral[0] != null ? qCreditAppReqBusinessCollateral[0].Description : "-",
					QCreditAppReqBusinessCollateral1_BusinessCollateralValue = qCreditAppReqBusinessCollateral[0] != null ? qCreditAppReqBusinessCollateral[0].BusinessCollateralValue : 0,
					QCreditAppReqBusinessCollateral2_IsNew = qCreditAppReqBusinessCollateral[1] != null ? variable.BusinessCollateralIsNew2: "-",
					QCreditAppReqBusinessCollateral2_BusinessCollateralType = qCreditAppReqBusinessCollateral[1] != null ? qCreditAppReqBusinessCollateral[1].BusinessCollateralTypeId : "-",
					QCreditAppReqBusinessCollateral2_BusinessCollateralSubType = qCreditAppReqBusinessCollateral[1] != null ? qCreditAppReqBusinessCollateral[1].BusinessCollateralSubTypeId : "-",
					QCreditAppReqBusinessCollateral2_Description = qCreditAppReqBusinessCollateral[1] != null ? qCreditAppReqBusinessCollateral[1].Description : "-",
					QCreditAppReqBusinessCollateral2_BusinessCollateralValue = qCreditAppReqBusinessCollateral[1] != null ? qCreditAppReqBusinessCollateral[1].BusinessCollateralValue : 0,
					QCreditAppReqBusinessCollateral3_IsNew = qCreditAppReqBusinessCollateral[2] != null ? variable.BusinessCollateralIsNew3 : "-",
					QCreditAppReqBusinessCollateral3_BusinessCollateralType = qCreditAppReqBusinessCollateral[2] != null ? qCreditAppReqBusinessCollateral[2].BusinessCollateralTypeId : "-",
					QCreditAppReqBusinessCollateral3_BusinessCollateralSubType = qCreditAppReqBusinessCollateral[2] != null ? qCreditAppReqBusinessCollateral[2].BusinessCollateralSubTypeId : "-",
					QCreditAppReqBusinessCollateral3_Description = qCreditAppReqBusinessCollateral[2] != null ? qCreditAppReqBusinessCollateral[2].Description : "-",
					QCreditAppReqBusinessCollateral3_BusinessCollateralValue = qCreditAppReqBusinessCollateral[2] != null ? qCreditAppReqBusinessCollateral[2].BusinessCollateralValue : 0,
					QCreditAppReqBusinessCollateral4_IsNew = qCreditAppReqBusinessCollateral[3] != null ? variable.BusinessCollateralIsNew4 : "-",
					QCreditAppReqBusinessCollateral4_BusinessCollateralType = qCreditAppReqBusinessCollateral[3] != null ? qCreditAppReqBusinessCollateral[3].BusinessCollateralTypeId : "-",
					QCreditAppReqBusinessCollateral4_BusinessCollateralSubType = qCreditAppReqBusinessCollateral[3] != null ? qCreditAppReqBusinessCollateral[3].BusinessCollateralSubTypeId : "-",
					QCreditAppReqBusinessCollateral4_Description = qCreditAppReqBusinessCollateral[3] != null ? qCreditAppReqBusinessCollateral[3].Description : "-",
					QCreditAppReqBusinessCollateral4_BusinessCollateralValue = qCreditAppReqBusinessCollateral[3] != null ? qCreditAppReqBusinessCollateral[3].BusinessCollateralValue : 0,
					QCreditAppReqBusinessCollateral5_IsNew = qCreditAppReqBusinessCollateral[4] != null ? variable.BusinessCollateralIsNew5 : "-",
					QCreditAppReqBusinessCollateral5_BusinessCollateralType = qCreditAppReqBusinessCollateral[4] != null ? qCreditAppReqBusinessCollateral[4].BusinessCollateralTypeId : "-",
					QCreditAppReqBusinessCollateral5_BusinessCollateralSubType = qCreditAppReqBusinessCollateral[4] != null ? qCreditAppReqBusinessCollateral[4].BusinessCollateralSubTypeId : "-",
					QCreditAppReqBusinessCollateral5_Description = qCreditAppReqBusinessCollateral[4] != null ? qCreditAppReqBusinessCollateral[4].Description : "-",
					QCreditAppReqBusinessCollateral5_BusinessCollateralValue = qCreditAppReqBusinessCollateral[4] != null ? qCreditAppReqBusinessCollateral[4].BusinessCollateralValue : 0,
					QCreditAppReqBusinessCollateral6_IsNew = qCreditAppReqBusinessCollateral[5] != null ? variable.BusinessCollateralIsNew6 : "-",
					QCreditAppReqBusinessCollateral6_BusinessCollateralType = qCreditAppReqBusinessCollateral[5] != null ? qCreditAppReqBusinessCollateral[5].BusinessCollateralTypeId : "-",
					QCreditAppReqBusinessCollateral6_BusinessCollateralSubType = qCreditAppReqBusinessCollateral[5] != null ? qCreditAppReqBusinessCollateral[5].BusinessCollateralSubTypeId : "-",
					QCreditAppReqBusinessCollateral6_Description = qCreditAppReqBusinessCollateral[5] != null ? qCreditAppReqBusinessCollateral[5].Description : "-",
					QCreditAppReqBusinessCollateral6_BusinessCollateralValue = qCreditAppReqBusinessCollateral[5] != null ? qCreditAppReqBusinessCollateral[5].BusinessCollateralValue : 0,
					QCreditAppReqBusinessCollateral7_IsNew = qCreditAppReqBusinessCollateral[6] != null ? variable.BusinessCollateralIsNew7 : "-",
					QCreditAppReqBusinessCollateral7_BusinessCollateralType = qCreditAppReqBusinessCollateral[6] != null ? qCreditAppReqBusinessCollateral[6].BusinessCollateralTypeId : "-",
					QCreditAppReqBusinessCollateral7_BusinessCollateralSubType = qCreditAppReqBusinessCollateral[6] != null ? qCreditAppReqBusinessCollateral[6].BusinessCollateralSubTypeId : "-",
					QCreditAppReqBusinessCollateral7_Description = qCreditAppReqBusinessCollateral[6] != null ? qCreditAppReqBusinessCollateral[6].Description : "-",
					QCreditAppReqBusinessCollateral7_BusinessCollateralValue = qCreditAppReqBusinessCollateral[6] != null ? qCreditAppReqBusinessCollateral[6].BusinessCollateralValue : 0,
					QCreditAppReqBusinessCollateral8_IsNew = qCreditAppReqBusinessCollateral[7] != null ? variable.BusinessCollateralIsNew8 : "-",
					QCreditAppReqBusinessCollateral8_BusinessCollateralType = qCreditAppReqBusinessCollateral[7] != null ? qCreditAppReqBusinessCollateral[7].BusinessCollateralTypeId : "-",
					QCreditAppReqBusinessCollateral8_BusinessCollateralSubType = qCreditAppReqBusinessCollateral[7] != null ? qCreditAppReqBusinessCollateral[7].BusinessCollateralSubTypeId : "-",
					QCreditAppReqBusinessCollateral8_Description = qCreditAppReqBusinessCollateral[7] != null ? qCreditAppReqBusinessCollateral[7].Description : "-",
					QCreditAppReqBusinessCollateral8_BusinessCollateralValue = qCreditAppReqBusinessCollateral[7] != null ? qCreditAppReqBusinessCollateral[7].BusinessCollateralValue : 0,
					QCreditAppReqBusinessCollateral9_IsNew = qCreditAppReqBusinessCollateral[8] != null ? variable.BusinessCollateralIsNew9 : "-",
					QCreditAppReqBusinessCollateral9_BusinessCollateralType = qCreditAppReqBusinessCollateral[8] != null ? qCreditAppReqBusinessCollateral[8].BusinessCollateralTypeId : "-",
					QCreditAppReqBusinessCollateral9_BusinessCollateralSubType = qCreditAppReqBusinessCollateral[8] != null ? qCreditAppReqBusinessCollateral[8].BusinessCollateralSubTypeId : "-",
					QCreditAppReqBusinessCollateral9_Description = qCreditAppReqBusinessCollateral[8] != null ? qCreditAppReqBusinessCollateral[8].Description : "-",
					QCreditAppReqBusinessCollateral9_BusinessCollateralValue = qCreditAppReqBusinessCollateral[8] != null ? qCreditAppReqBusinessCollateral[8].BusinessCollateralValue : 0,
					QCreditAppReqBusinessCollateral10_IsNew = qCreditAppReqBusinessCollateral[9] != null ? variable.BusinessCollateralIsNew10 : "-",
					QCreditAppReqBusinessCollateral10_BusinessCollateralType = qCreditAppReqBusinessCollateral[9] != null ? qCreditAppReqBusinessCollateral[9].BusinessCollateralTypeId : "-",
					QCreditAppReqBusinessCollateral10_BusinessCollateralSubType = qCreditAppReqBusinessCollateral[9] != null ? qCreditAppReqBusinessCollateral[9].BusinessCollateralSubTypeId : "-",
					QCreditAppReqBusinessCollateral10_Description = qCreditAppReqBusinessCollateral[9] != null ? qCreditAppReqBusinessCollateral[9].Description : "-",
					QCreditAppReqBusinessCollateral10_BusinessCollateralValue = qCreditAppReqBusinessCollateral[9] != null ? qCreditAppReqBusinessCollateral[9].BusinessCollateralValue : 0,
					QCreditAppReqBusinessCollateral11_IsNew = qCreditAppReqBusinessCollateral[10] != null ? variable.BusinessCollateralIsNew11 : "-",
					QCreditAppReqBusinessCollateral11_BusinessCollateralType = qCreditAppReqBusinessCollateral[10] != null ? qCreditAppReqBusinessCollateral[10].BusinessCollateralTypeId : "-",
					QCreditAppReqBusinessCollateral11_BusinessCollateralSubType = qCreditAppReqBusinessCollateral[10] != null ? qCreditAppReqBusinessCollateral[10].BusinessCollateralSubTypeId : "-",
					QCreditAppReqBusinessCollateral11_Description = qCreditAppReqBusinessCollateral[10] != null ? qCreditAppReqBusinessCollateral[10].Description : "-",
					QCreditAppReqBusinessCollateral11_BusinessCollateralValue = qCreditAppReqBusinessCollateral[10] != null ? qCreditAppReqBusinessCollateral[10].BusinessCollateralValue : 0,
					QCreditAppReqBusinessCollateral12_IsNew = qCreditAppReqBusinessCollateral[11] != null ? variable.BusinessCollateralIsNew12 : "-",
					QCreditAppReqBusinessCollateral12_BusinessCollateralType = qCreditAppReqBusinessCollateral[11] != null ? qCreditAppReqBusinessCollateral[11].BusinessCollateralTypeId : "-",
					QCreditAppReqBusinessCollateral12_BusinessCollateralSubType = qCreditAppReqBusinessCollateral[11] != null ? qCreditAppReqBusinessCollateral[11].BusinessCollateralSubTypeId : "-",
					QCreditAppReqBusinessCollateral12_Description = qCreditAppReqBusinessCollateral[11] != null ? qCreditAppReqBusinessCollateral[11].Description : "-",
					QCreditAppReqBusinessCollateral12_BusinessCollateralValue = qCreditAppReqBusinessCollateral[11] != null ? qCreditAppReqBusinessCollateral[11].BusinessCollateralValue : 0,
					QCreditAppReqBusinessCollateral13_IsNew = qCreditAppReqBusinessCollateral[12] != null ? variable.BusinessCollateralIsNew13 : "-",
					QCreditAppReqBusinessCollateral13_BusinessCollateralType = qCreditAppReqBusinessCollateral[12] != null ? qCreditAppReqBusinessCollateral[12].BusinessCollateralTypeId : "-",
					QCreditAppReqBusinessCollateral13_BusinessCollateralSubType = qCreditAppReqBusinessCollateral[12] != null ? qCreditAppReqBusinessCollateral[12].BusinessCollateralSubTypeId : "-",
					QCreditAppReqBusinessCollateral13_Description = qCreditAppReqBusinessCollateral[12] != null ? qCreditAppReqBusinessCollateral[12].Description : "-",
					QCreditAppReqBusinessCollateral13_BusinessCollateralValue = qCreditAppReqBusinessCollateral[12] != null ? qCreditAppReqBusinessCollateral[12].BusinessCollateralValue : 0,
					QCreditAppReqBusinessCollateral14_IsNew = qCreditAppReqBusinessCollateral[13] != null ? variable.BusinessCollateralIsNew14 : "-",
					QCreditAppReqBusinessCollateral14_BusinessCollateralType = qCreditAppReqBusinessCollateral[13] != null ? qCreditAppReqBusinessCollateral[13].BusinessCollateralTypeId : "-",
					QCreditAppReqBusinessCollateral14_BusinessCollateralSubType = qCreditAppReqBusinessCollateral[13] != null ? qCreditAppReqBusinessCollateral[13].BusinessCollateralSubTypeId : "-",
					QCreditAppReqBusinessCollateral14_Description = qCreditAppReqBusinessCollateral[13] != null ? qCreditAppReqBusinessCollateral[13].Description : "-",
					QCreditAppReqBusinessCollateral14_BusinessCollateralValue = qCreditAppReqBusinessCollateral[13] != null ? qCreditAppReqBusinessCollateral[13].BusinessCollateralValue : 0,
					QCreditAppReqBusinessCollateral15_IsNew = qCreditAppReqBusinessCollateral[14] != null ? variable.BusinessCollateralIsNew14 : "-",
					QCreditAppReqBusinessCollateral15_BusinessCollateralType = qCreditAppReqBusinessCollateral[14] != null ? qCreditAppReqBusinessCollateral[14].BusinessCollateralTypeId : "-",
					QCreditAppReqBusinessCollateral15_BusinessCollateralSubType = qCreditAppReqBusinessCollateral[14] != null ? qCreditAppReqBusinessCollateral[14].BusinessCollateralSubTypeId : "-",
					QCreditAppReqBusinessCollateral15_Description = qCreditAppReqBusinessCollateral[14] != null ? qCreditAppReqBusinessCollateral[14].Description : "-",
					QCreditAppReqBusinessCollateral15_BusinessCollateralValue = qCreditAppReqBusinessCollateral[14] != null ? qCreditAppReqBusinessCollateral[14].BusinessCollateralValue : 0,
					//-------------------------------------------------------------------------------------------------------------------------------
					QSumCreditAppReqBusinessCollateral_SumBusinessCollateralValue = qSumCreditAppReqBusinessCollateral!= null ? qSumCreditAppReqBusinessCollateral.SumBusinessCollateralValue : 0,
					Variable_CustCompanyRegistrationID = variable.CustCompanyRegistrationID,
					QCreditAppRequestTable_RegisteredAddress = qCreditAppRequestTable.RegisteredAddress,
					Variable_CustComEstablishedDate = variable.CustComEstablishedDate,
					QCreditAppRequestTable_BusinessType = qCreditAppRequestTable.BusinessType,
					QCreditAppRequestTable_IntroducedBy = qCreditAppRequestTable.IntroducedBy,
					QCreditAppRequestTable_Instituetion = qBankAccountControl != null ? qBankAccountControl.Instituetion : " ",
					QCreditAppRequestTable_BankBranch = qBankAccountControl != null ? qBankAccountControl.BankBranch : " ",
					QCreditAppRequestTable_BankType = qBankAccountControl != null ? qBankAccountControl.AccountType : " ",
					QCreditAppRequestTable_BankAccountName = qBankAccountControl != null ? qBankAccountControl.AccountNumber: " ",
					QAuthorizedPersonTrans1_AuthorizedPersonName = qAuthorizedPersonTrans[0] != null ? qAuthorizedPersonTrans[0].AuthorizedPersonName : "",
					Variable_AuthorizedPersonTrans1Age = qAuthorizedPersonTrans[0] != null ? variable.AuthorizedPersonTrans1Age : "-",
					QAuthorizedPersonTrans2_AuthorizedPersonName = qAuthorizedPersonTrans[1] != null ? qAuthorizedPersonTrans[1].AuthorizedPersonName : "",
					Variable_AuthorizedPersonTrans2Age = qAuthorizedPersonTrans[1] != null ? variable.AuthorizedPersonTrans2Age : "-",
					QAuthorizedPersonTrans3_AuthorizedPersonName = qAuthorizedPersonTrans[2] != null ? qAuthorizedPersonTrans[2].AuthorizedPersonName : "",
					Variable_AuthorizedPersonTrans3Age = qAuthorizedPersonTrans[2] != null ? variable.AuthorizedPersonTrans3Age : "-",
					QAuthorizedPersonTrans4_AuthorizedPersonName = qAuthorizedPersonTrans[3] != null ? qAuthorizedPersonTrans[3].AuthorizedPersonName : "",
					Variable_AuthorizedPersonTrans4Age = qAuthorizedPersonTrans[3] != null ? variable.AuthorizedPersonTrans4Age : "-",
					QAuthorizedPersonTrans5_AuthorizedPersonName = qAuthorizedPersonTrans[4] != null ? qAuthorizedPersonTrans[4].AuthorizedPersonName : "",
					Variable_AuthorizedPersonTrans5Age = qAuthorizedPersonTrans[4] != null ? variable.AuthorizedPersonTrans5Age : "-",
					QAuthorizedPersonTrans6_AuthorizedPersonName = qAuthorizedPersonTrans[5] != null ? qAuthorizedPersonTrans[5].AuthorizedPersonName : "",
					Variable_AuthorizedPersonTrans6Age = qAuthorizedPersonTrans[5] != null ? variable.AuthorizedPersonTrans6Age : "-",
					QAuthorizedPersonTrans7_AuthorizedPersonName = qAuthorizedPersonTrans[6] != null ? qAuthorizedPersonTrans[6].AuthorizedPersonName : "",
					Variable_AuthorizedPersonTrans7Age = qAuthorizedPersonTrans[6] != null ? variable.AuthorizedPersonTrans7Age : "-",
					QAuthorizedPersonTrans8_AuthorizedPersonName = qAuthorizedPersonTrans[7] != null ? qAuthorizedPersonTrans[7].AuthorizedPersonName : "",
					Variable_AuthorizedPersonTrans8Age = qAuthorizedPersonTrans[7] != null ? variable.AuthorizedPersonTrans8Age : "-",
					QOwnerTrans1_ShareHolderPersonName = qOwnerTrans[0] != null ? qOwnerTrans[0].ShareHolderPersonName : "",
					QOwnerTrans1_PropotionOfShareholderPct = qOwnerTrans[0] != null ? qOwnerTrans[0].PropotionOfShareholderPct : 0,
					QOwnerTrans2_ShareHolderPersonName = qOwnerTrans[1] != null ? qOwnerTrans[1].ShareHolderPersonName : "",
					QOwnerTrans2_PropotionOfShareholderPct = qOwnerTrans[1] != null ? qOwnerTrans[1].PropotionOfShareholderPct : 0,
					QOwnerTrans3_ShareHolderPersonName = qOwnerTrans[2] != null ? qOwnerTrans[2].ShareHolderPersonName : "",
					QOwnerTrans3_PropotionOfShareholderPct = qOwnerTrans[2] != null ? qOwnerTrans[2].PropotionOfShareholderPct : 0,
					QOwnerTrans4_ShareHolderPersonName = qOwnerTrans[3] != null ? qOwnerTrans[3].ShareHolderPersonName : "",
					QOwnerTrans4_PropotionOfShareholderPct = qOwnerTrans[3] != null ? qOwnerTrans[3].PropotionOfShareholderPct : 0,
					QOwnerTrans5_ShareHolderPersonName = qOwnerTrans[4] != null ? qOwnerTrans[4].ShareHolderPersonName : "",
					QOwnerTrans5_PropotionOfShareholderPct = qOwnerTrans[4] != null ? qOwnerTrans[4].PropotionOfShareholderPct : 0,
					QOwnerTrans6_ShareHolderPersonName = qOwnerTrans[5] != null ? qOwnerTrans[5].ShareHolderPersonName : "",
					QOwnerTrans6_PropotionOfShareholderPct = qOwnerTrans[5] != null ? qOwnerTrans[5].PropotionOfShareholderPct : 0,
					QOwnerTrans7_ShareHolderPersonName = qOwnerTrans[6] != null ? qOwnerTrans[6].ShareHolderPersonName : "",
					QOwnerTrans7_PropotionOfShareholderPct = qOwnerTrans[6] != null ? qOwnerTrans[6].PropotionOfShareholderPct : 0,
					QOwnerTrans8_ShareHolderPersonName = qOwnerTrans[7] != null ? qOwnerTrans[7].ShareHolderPersonName : "",
					QOwnerTrans8_PropotionOfShareholderPct = qOwnerTrans[7] != null ? qOwnerTrans[7].PropotionOfShareholderPct : 0,
					QCreditAppReqLineFin1_Year = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].Year : 0,
					QCreditAppReqLineFin1_RegisteredCapital = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].RegisteredCapital : 0,
					QCreditAppReqLineFin1_PaidCapital = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].PaidCapital : 0,
					QCreditAppReqLineFin1_TotalAsset = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].TotalAsset : 0,
					QCreditAppReqLineFin1_TotalLiability = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].TotalLiability : 0,
					QCreditAppReqLineFin1_TotalEquity = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].TotalEquity : 0,
					QCreditAppReqLineFin1_TotalRevenue = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].TotalRevenue : 0,
					QCreditAppReqLineFin1_TotalCOGS = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].TotalCOGS : 0,
					QCreditAppReqLineFin1_TotalGrossProfit = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].TotalGrossProfit : 0,
					QCreditAppReqLineFin1_IntCoverageRatio = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].IntCoverageRatio : 0,
					QCreditAppReqLineFin1_TotalOperExpFirst = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].TotalOperExpFirst : 0,
					QCreditAppReqLineFin1_TotalNetProfitFirst = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].TotalNetProfitFirst : 0,
					QCreditAppReqLineFin2_Year = qCreditAppReqLineFin[1] != null ? qCreditAppReqLineFin[1].Year : 0,
					QCreditAppReqLineFin2_RegisteredCapital = qCreditAppReqLineFin[1] != null ? qCreditAppReqLineFin[1].RegisteredCapital : 0,
					QCreditAppReqLineFin2_PaidCapital = qCreditAppReqLineFin[1] != null ?  qCreditAppReqLineFin[1].PaidCapital : 0,
					QCreditAppReqLineFin2_TotalAsset = qCreditAppReqLineFin[1] != null ? qCreditAppReqLineFin[1].TotalAsset : 0,
					QCreditAppReqLineFin2_TotalLiability = qCreditAppReqLineFin[1] != null ? qCreditAppReqLineFin[1].TotalLiability : 0,
					QCreditAppReqLineFin2_TotalEquity = qCreditAppReqLineFin[1] != null ? qCreditAppReqLineFin[1].TotalEquity : 0,
					QCreditAppReqLineFin2_TotalRevenue = qCreditAppReqLineFin[1] != null ? qCreditAppReqLineFin[1].TotalRevenue : 0,
					QCreditAppReqLineFin2_TotalCOGS = qCreditAppReqLineFin[1] != null ? qCreditAppReqLineFin[1].TotalCOGS : 0,
					QCreditAppReqLineFin2_TotalGrossProfit = qCreditAppReqLineFin[1] != null ? qCreditAppReqLineFin[1].TotalGrossProfit : 0,
					QCreditAppReqLineFin2_TotalOperExpSecond = qCreditAppReqLineFin[1] != null ?  qCreditAppReqLineFin[1].TotalOperExpFirst : 0,
					QCreditAppReqLineFin2_TotalNetProfitSecond = qCreditAppReqLineFin[1] != null ? qCreditAppReqLineFin[1].TotalNetProfitFirst : 0,
					QCreditAppReqLineFin3_Year = qCreditAppReqLineFin[2] != null ? qCreditAppReqLineFin[2].Year : 0,
					QCreditAppReqLineFin3_RegisteredCapital = qCreditAppReqLineFin[2] != null ? qCreditAppReqLineFin[2].RegisteredCapital : 0,
					QCreditAppReqLineFin3_PaidCapital = qCreditAppReqLineFin[2] != null ? qCreditAppReqLineFin[2].PaidCapital : 0,
					QCreditAppReqLineFin3_TotalAsset = qCreditAppReqLineFin[2] != null ? qCreditAppReqLineFin[2].TotalAsset : 0,
					QCreditAppReqLineFin3_TotalLiability = qCreditAppReqLineFin[2] != null ? qCreditAppReqLineFin[2].TotalLiability : 0,
					QCreditAppReqLineFin3_TotalEquity = qCreditAppReqLineFin[2] != null ? qCreditAppReqLineFin[2].TotalEquity : 0,
					QCreditAppReqLineFin3_TotalRevenue = qCreditAppReqLineFin[2] != null ? qCreditAppReqLineFin[2].TotalRevenue : 0,
					QCreditAppReqLineFin3_TotalCOGS = qCreditAppReqLineFin[2] != null ? qCreditAppReqLineFin[2].TotalCOGS : 0,
					QCreditAppReqLineFin3_TotalGrossProfit = qCreditAppReqLineFin[2] != null ? qCreditAppReqLineFin[2].TotalGrossProfit : 0,
					QCreditAppReqLineFin3_TotalOperExpThird = qCreditAppReqLineFin[2] != null ? qCreditAppReqLineFin[2].TotalOperExpFirst : 0,
					QCreditAppReqLineFin3_TotalNetProfitThird = qCreditAppReqLineFin[2] != null ? qCreditAppReqLineFin[2].TotalNetProfitFirst : 0,
					QCreditAppReqLineFin4_Year = qCreditAppReqLineFin[3] != null ? qCreditAppReqLineFin[3].Year : 0,
					QCreditAppReqLineFin4_RegisteredCapital = qCreditAppReqLineFin[3] != null ? qCreditAppReqLineFin[3].RegisteredCapital : 0,
					QCreditAppReqLineFin4_PaidCapital = qCreditAppReqLineFin[3] != null ? qCreditAppReqLineFin[3].PaidCapital : 0,
					QCreditAppReqLineFin4_TotalAsset = qCreditAppReqLineFin[3] != null ? qCreditAppReqLineFin[3].TotalLiability : 0,
					QCreditAppReqLineFin4_TotalLiability = qCreditAppReqLineFin[3] != null ? qCreditAppReqLineFin[3].TotalEquity : 0,
					QCreditAppReqLineFin4_TotalEquity = qCreditAppReqLineFin[3] != null ? qCreditAppReqLineFin[3].TotalRevenue : 0,
					QCreditAppReqLineFin4_TotalRevenue = qCreditAppReqLineFin[3] != null ? qCreditAppReqLineFin[3].TotalRevenue : 0,
					QCreditAppReqLineFin4_TotalCOGS = qCreditAppReqLineFin[3] != null ? qCreditAppReqLineFin[3].TotalCOGS : 0,
					QCreditAppReqLineFin4_TotalGrossProfit = qCreditAppReqLineFin[3] != null ? qCreditAppReqLineFin[3].TotalGrossProfit : 0,
					QCreditAppReqLineFin4_TotalOperExpFourth = qCreditAppReqLineFin[3] != null ? qCreditAppReqLineFin[3].TotalOperExpFirst : 0,
					QCreditAppReqLineFin4_TotalNetProfitFourth = qCreditAppReqLineFin[3] != null ? qCreditAppReqLineFin[3].TotalNetProfitFirst : 0,
					QCreditAppReqLineFin5_Year = qCreditAppReqLineFin[4] != null ? qCreditAppReqLineFin[4].Year : 0,
					QCreditAppReqLineFin5_RegisteredCapital = qCreditAppReqLineFin[4] != null ? qCreditAppReqLineFin[4].RegisteredCapital : 0,
					QCreditAppReqLineFin5_PaidCapital = qCreditAppReqLineFin[4] != null ? qCreditAppReqLineFin[4].PaidCapital : 0,
					QCreditAppReqLineFin5_TotalAsset = qCreditAppReqLineFin[4] != null ? qCreditAppReqLineFin[4].TotalAsset : 0,
					QCreditAppReqLineFin5_TotalLiability = qCreditAppReqLineFin[4] != null ? qCreditAppReqLineFin[4].TotalLiability : 0,
					QCreditAppReqLineFin5_TotalEquity = qCreditAppReqLineFin[4] != null ? qCreditAppReqLineFin[4].TotalEquity : 0,
					QCreditAppReqLineFin5_TotalRevenue = qCreditAppReqLineFin[4] != null ? qCreditAppReqLineFin[4].TotalRevenue : 0,
					QCreditAppReqLineFin5_TotalCOGS = qCreditAppReqLineFin[4] != null ? qCreditAppReqLineFin[4].TotalCOGS : 0,
					QCreditAppReqLineFin5_TotalGrossProfit = qCreditAppReqLineFin[4] != null ? qCreditAppReqLineFin[4].TotalGrossProfit : 0,
					QCreditAppReqLineFin5_TotalOperExpFifth = qCreditAppReqLineFin[4] != null ? qCreditAppReqLineFin[4].TotalOperExpFirst : 0,
					QCreditAppReqLineFin5_TotalNetProfitFifth = qCreditAppReqLineFin[4] != null ? qCreditAppReqLineFin[4].TotalNetProfitFirst : 0,
					//-----------------------------------------------------------------------------------------------------
					QCreditAppReqLineFin1_NetProfitPercent = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].NetProfitPercent : 0,
					QCreditAppReqLineFin1_lDE = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].lDE : 0,
					QCreditAppReqLineFin1_QuickRatio = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].QuickRatio : 0,
					QCreditAppRequestTable_SalesAvgPerMonth = qCreditAppRequestTable != null ? qCreditAppRequestTable.SalesAvgPerMonth : 0,
					QTaxReportStart_TaxMonth = qTaxReportStart != null ? qTaxReportStart.TaxMonth : "",
					QTaxReportEnd_TaxMonth = qTaxReportEnd != null ? qTaxReportEnd.TaxMonth: "",
					QTaxReportTrans1_TaxMonth = qTaxReportTrans[0] != null ? qTaxReportTrans[0].TaxMonth : "",
					QTaxReportTrans1_Amount = qTaxReportTrans[0] != null ? qTaxReportTrans[0].Amount : 0,
					QTaxReportTrans2_TaxMonth = qTaxReportTrans[1] != null ? qTaxReportTrans[1].TaxMonth : "",
					QTaxReportTrans2_Amount = qTaxReportTrans[1] != null ? qTaxReportTrans[1].Amount : 0,
					QTaxReportTrans3_TaxMonth = qTaxReportTrans[2] != null ? qTaxReportTrans[2].TaxMonth : "",
					QTaxReportTrans3_Amount = qTaxReportTrans[2] != null ? qTaxReportTrans[2].Amount : 0,
					QTaxReportTrans4_TaxMonth = qTaxReportTrans[3] != null ? qTaxReportTrans[3].TaxMonth : "",
					QTaxReportTrans4_Amount = qTaxReportTrans[3] != null ? qTaxReportTrans[3].Amount : 0,
					QTaxReportTrans5_TaxMonth = qTaxReportTrans[4] != null ? qTaxReportTrans[4].TaxMonth : "",
					QTaxReportTrans5_Amount = qTaxReportTrans[4] != null ? qTaxReportTrans[4].Amount : 0,
					QTaxReportTrans6_TaxMonth = qTaxReportTrans[5] != null ? qTaxReportTrans[5].TaxMonth : "",
					QTaxReportTrans6_Amount = qTaxReportTrans[5] != null ? qTaxReportTrans[5].Amount : 0,
					QTaxReportTrans7_TaxMonth = qTaxReportTrans[6] != null ? qTaxReportTrans[6].TaxMonth : "",
					QTaxReportTrans7_Amount = qTaxReportTrans[6] != null ? qTaxReportTrans[6].Amount : 0,
					QTaxReportTrans8_TaxMonth = qTaxReportTrans[7] != null ? qTaxReportTrans[7].TaxMonth : "",
					QTaxReportTrans8_Amount = qTaxReportTrans[7] != null ? qTaxReportTrans[7].Amount : 0,
					QTaxReportTrans9_TaxMonth = qTaxReportTrans[8] != null ? qTaxReportTrans[8].TaxMonth : "",
					QTaxReportTrans9_Amount = qTaxReportTrans[8] != null ? qTaxReportTrans[8].Amount : 0,
					QTaxReportTrans10_TaxMonth = qTaxReportTrans[9] != null ? qTaxReportTrans[9].TaxMonth : "",
					QTaxReportTrans10_Amount = qTaxReportTrans[9] != null ? qTaxReportTrans[9].Amount : 0,
					QTaxReportTrans11_TaxMonth = qTaxReportTrans[10] != null ? qTaxReportTrans[10].TaxMonth : "",
					QTaxReportTrans11_Amount = qTaxReportTrans[10] != null ? qTaxReportTrans[10].Amount : 0,
					QTaxReportTrans12_TaxMonth = qTaxReportTrans[11] != null ? qTaxReportTrans[11].TaxMonth : "",
					QTaxReportTrans12_Amount = qTaxReportTrans[11] != null ? qTaxReportTrans[11].Amount : 0,
					QTaxReportTransSum_SumTaxReport = qTaxReportTransSum[0] != null ? qTaxReportTransSum[0].SumTaxReport : 0,
					Variable_AverageOutputVATPerMonth = variable != null ? variable.AverageOutputVATPerMonth : 0,
					QCreditAppRequestTable_AsOfDate = qCAReqCreditOutStandingAsOfDate != null ? qCAReqCreditOutStandingAsOfDate.AsOfDate.DateNullToString() : "",
					//-----------------------------------------------------------------------------------
					Variable_CrditLimitAmtFirst1 = qCAReqCreditOutStanding[2] != null ? qCAReqCreditOutStanding[2].ApprovedCreditLimit : 0,
					Variable_AROutstandingAmtFirst = qCAReqCreditOutStanding[2] != null ? qCAReqCreditOutStanding[2].ARBalance : 0,
					Variable_CreditLimitRemainingFirst1 = qCAReqCreditOutStanding[2] != null ? qCAReqCreditOutStanding[2].CreditLimitBalance : 0,
					Variable_ReserveOutstandingFirst1 = qCAReqCreditOutStanding[2] != null ? qCAReqCreditOutStanding[2].ReserveToBeRefund : 0,
					Variable_RetentionOutstandingFirst1 = qCAReqCreditOutStanding[2] != null ? qCAReqCreditOutStanding[2].AccumRetentionAmount : 0,
					Variable_CrditLimitAmtSecond1 = qCAReqCreditOutStanding[1] != null ? qCAReqCreditOutStanding[1].ApprovedCreditLimit :0,
					Variable_AROutstandingAmtSecond = qCAReqCreditOutStanding[1] != null ? qCAReqCreditOutStanding[1].ARBalance :0,
					Variable_CreditLimitRemainingSecond1 = qCAReqCreditOutStanding[1] != null ? qCAReqCreditOutStanding[1].CreditLimitBalance : 0,
					Variable_ReserveOutstandingSecond1 = qCAReqCreditOutStanding[1] != null ? qCAReqCreditOutStanding[1].ReserveToBeRefund : 0,
					Variable_RetentionOutstandingSecond1 = qCAReqCreditOutStanding[1] != null ? qCAReqCreditOutStanding[1].AccumRetentionAmount : 0,
					Variable_CrditLimitAmtThird1 = qCAReqCreditOutStanding[3] != null ? qCAReqCreditOutStanding[3].ApprovedCreditLimit : 0,
					Variable_AROutstandingAmtThird = qCAReqCreditOutStanding[3] != null ? qCAReqCreditOutStanding[3].ARBalance : 0,
					Variable_CreditLimitRemainingThird1 = qCAReqCreditOutStanding[3] != null ? qCAReqCreditOutStanding[3].CreditLimitBalance : 0,
					Variable_ReserveOutstandingThird1 = qCAReqCreditOutStanding[3] != null ? qCAReqCreditOutStanding[3].ReserveToBeRefund : 0,
					Variable_RetentionOutstandingThird1 = qCAReqCreditOutStanding[3] != null ? qCAReqCreditOutStanding[3].AccumRetentionAmount : 0,
					Variable_CrditLimitAmtFourth1 = qCAReqCreditOutStanding[4] != null ? qCAReqCreditOutStanding[4].AccumRetentionAmount : 0,
					Variable_AROutstandingAmtFourth = qCAReqCreditOutStanding[4] != null ? qCAReqCreditOutStanding[4].ARBalance : 0,
					Variable_CreditLimitRemainingFourth1 = qCAReqCreditOutStanding[4] != null ? qCAReqCreditOutStanding[4].CreditLimitBalance : 0,
					Variable_ReserveOutstandingFourth1 = qCAReqCreditOutStanding[4] != null ? qCAReqCreditOutStanding[4].ReserveToBeRefund : 0,
					Variable_RetentionOutstandingFourth1 = qCAReqCreditOutStanding[4] != null ? qCAReqCreditOutStanding[4].AccumRetentionAmount : 0,
					Variable_CrditLimitAmtFifth1 = qCAReqCreditOutStanding[5] != null ? qCAReqCreditOutStanding[5].AccumRetentionAmount : 0,
					Variable_AROutstandingAmtFifth = qCAReqCreditOutStanding[5] != null ? qCAReqCreditOutStanding[5].ARBalance : 0,
					Variable_CreditLimitRemainingFifth1 = qCAReqCreditOutStanding[5] != null ? qCAReqCreditOutStanding[5].CreditLimitBalance : 0,
					Variable_ReserveOutstandingFifth1 = qCAReqCreditOutStanding[5] != null ? qCAReqCreditOutStanding[5].ReserveToBeRefund : 0,
					Variable_RetentionOutstandingFifth1 = qCAReqCreditOutStanding[5] != null ? qCAReqCreditOutStanding[5].AccumRetentionAmount : 0,
					Variable_CrditLimitAmtSixth1 = qCAReqCreditOutStanding[6] != null ? qCAReqCreditOutStanding[6].AccumRetentionAmount : 0,
					Variable_AROutstandingAmtSixth = qCAReqCreditOutStanding[6] != null ? qCAReqCreditOutStanding[6].ARBalance : 0,
					Variable_CreditLimitRemainingSixth1 = qCAReqCreditOutStanding[6] != null ? qCAReqCreditOutStanding[6].CreditLimitBalance : 0,
					Variable_ReserveOutstandingSixth1 = qCAReqCreditOutStanding[6] != null ? qCAReqCreditOutStanding[6].ReserveToBeRefund : 0,
					Variable_RetentionOutstandingSixth1 = qCAReqCreditOutStanding[6] != null ? qCAReqCreditOutStanding[6].AccumRetentionAmount : 0,
					Variable_CrditLimitAmtSeventh1 = qCAReqCreditOutStanding[7] != null ? qCAReqCreditOutStanding[7].AccumRetentionAmount : 0,
					Variable_AROutstandingAmtSeventh = qCAReqCreditOutStanding[7] != null ? qCAReqCreditOutStanding[7].ARBalance : 0,
					Variable_CreditLimitRemainingSeventh1 = qCAReqCreditOutStanding[7] != null ? qCAReqCreditOutStanding[7].CreditLimitBalance : 0,
					Variable_ReserveOutstandingSeventh1 = qCAReqCreditOutStanding[7] != null ? qCAReqCreditOutStanding[7].ReserveToBeRefund : 0,
					Variable_RetentionOutstandingSeventh1 = qCAReqCreditOutStanding[7] != null ? qCAReqCreditOutStanding[7].AccumRetentionAmount : 0,
					Variable_CrditLimitAmtEighth1 = qCAReqCreditOutStanding[8] != null ? qCAReqCreditOutStanding[8].AccumRetentionAmount : 0,
					Variable_AROutstandingAmtEighth = qCAReqCreditOutStanding[8] != null ? qCAReqCreditOutStanding[8].ARBalance : 0,
					Variable_CreditLimitRemainingEighth1 = qCAReqCreditOutStanding[8] != null ? qCAReqCreditOutStanding[8].CreditLimitBalance : 0,
					Variable_ReserveOutstandingEighth1 = qCAReqCreditOutStanding[8] != null ? qCAReqCreditOutStanding[8].ReserveToBeRefund : 0,
					Variable_RetentionOutstandingEighth1 = qCAReqCreditOutStanding[8] != null ? qCAReqCreditOutStanding[8].AccumRetentionAmount : 0,
					Variable_CrditLimitAmtNineth1 = qCAReqCreditOutStanding[9] != null ? qCAReqCreditOutStanding[9].AccumRetentionAmount : 0,
					Variable_AROutstandingAmtNineth = qCAReqCreditOutStanding[9] != null ? qCAReqCreditOutStanding[9].ARBalance : 0,
					Variable_CreditLimitRemainingNineth1 = qCAReqCreditOutStanding[9] != null ? qCAReqCreditOutStanding[9].CreditLimitBalance : 0,
					Variable_ReserveOutstandingNineth1 = qCAReqCreditOutStanding[9] != null ? qCAReqCreditOutStanding[9].ReserveToBeRefund : 0,
					Variable_RetentionOutstandingNineth1 = qCAReqCreditOutStanding[9] != null ? qCAReqCreditOutStanding[9].AccumRetentionAmount : 0,
					Variable_CrditLimitAmtTenth1 = qCAReqCreditOutStanding[10] != null ? qCAReqCreditOutStanding[10].AccumRetentionAmount : 0,
					Variable_AROutstandingAmtTenth = qCAReqCreditOutStanding[10] != null ? qCAReqCreditOutStanding[10].ARBalance : 0,
					Variable_CreditLimitRemainingTenth1 = qCAReqCreditOutStanding[10] != null ? qCAReqCreditOutStanding[10].CreditLimitBalance : 0,
					Variable_ReserveOutstandingTenth1 = qCAReqCreditOutStanding[10] != null ? qCAReqCreditOutStanding[10].ReserveToBeRefund : 0,
					Variable_RetentionOutstandingTenth1 = qCAReqCreditOutStanding[10] != null ? qCAReqCreditOutStanding[10].AccumRetentionAmount : 0,
					//-----------------------------------------------------------------------------------
					QCAReqCreditOutStandingSum_SumApprovedCreditLimit = qCAReqCreditOutStandingSum != null ? qCAReqCreditOutStandingSum.SumApprovedCreditLimit : 0,
					QCAReqCreditOutStandingSum_SumARBalance = qCAReqCreditOutStandingSum != null ? qCAReqCreditOutStandingSum.SumARBalance: 0,
					QCAReqCreditOutStandingSum_SumCreditLimitBalance = qCAReqCreditOutStandingSum != null ? qCAReqCreditOutStandingSum.SumCreditLimitBalance: 0,
					QCAReqCreditOutStandingSum_SumReserveToBeRefund = qCAReqCreditOutStandingSum != null ? qCAReqCreditOutStandingSum.SumReserveToBeRefund: 0,
					QCAReqCreditOutStandingSum_SumAccumRetentionAmount = qCAReqCreditOutStandingSum != null ? qCAReqCreditOutStandingSum.SumAccumRetentionAmount: 0,
					QCreditAppRequestTable_NCBCheckedDate = qCreditAppRequestTable.NCBCheckedDate,
					QNCBTrans1_Institution = qNCBTrans[0] != null ? qNCBTrans[0].Institution : "-",
					QNCBTrans1_LoanType = qNCBTrans[0] != null ? qNCBTrans[0].LoanType : "-",
					QNCBTrans1_LoanLimit = qNCBTrans[0] != null ? qNCBTrans[0].LoanLimit : 0,
					QNCBTrans2_Institution = qNCBTrans[1] != null ? qNCBTrans[1].Institution : "-",
					QNCBTrans2_LoanType = qNCBTrans[1] != null ? qNCBTrans[1].LoanType : "-",
					QNCBTrans2_LoanLimit = qNCBTrans[1] != null ? qNCBTrans[1].LoanLimit : 0,
					QNCBTrans3_Institution = qNCBTrans[2] != null ? qNCBTrans[2].Institution : "-",
					QNCBTrans3_LoanType = qNCBTrans[2] != null ? qNCBTrans[2].LoanType : "-",
					QNCBTrans3_LoanLimit = qNCBTrans[2] != null ? qNCBTrans[2].LoanLimit : 0,
					QNCBTrans4_Institution = qNCBTrans[3] != null ? qNCBTrans[3].Institution : "-",
					QNCBTrans4_LoanType = qNCBTrans[3] != null ? qNCBTrans[3].LoanType : "-",
					QNCBTrans4_LoanLimit = qNCBTrans[3] != null ? qNCBTrans[3].LoanLimit : 0,
					QNCBTrans5_Institution = qNCBTrans[4] != null ? qNCBTrans[4].Institution : "-",
					QNCBTrans5_LoanType = qNCBTrans[4] != null ? qNCBTrans[4].LoanType : "-",
					QNCBTrans5_LoanLimit = qNCBTrans[4] != null ? qNCBTrans[4].LoanLimit : 0,
					QNCBTrans6_Institution = qNCBTrans[5] != null ? qNCBTrans[5].Institution : "-",
					QNCBTrans6_LoanType = qNCBTrans[5] != null ? qNCBTrans[5].LoanType : "-",
					QNCBTrans6_LoanLimit = qNCBTrans[5] != null ? qNCBTrans[5].LoanLimit : 0,
					QNCBTrans7_Institution = qNCBTrans[6] != null ? qNCBTrans[6].Institution : "-",
					QNCBTrans7_LoanType = qNCBTrans[6] != null ? qNCBTrans[6].LoanType : "-",
					QNCBTrans7_LoanLimit = qNCBTrans[6] != null ? qNCBTrans[6].LoanLimit : 0,
					QNCBTrans8_Institution = qNCBTrans[7] != null ? qNCBTrans[7].Institution : "-",
					QNCBTrans8_LoanType = qNCBTrans[7] != null ? qNCBTrans[7].LoanType : "-",
					QNCBTrans8_LoanLimit = qNCBTrans[7] != null ? qNCBTrans[7].LoanLimit : 0,
					//-------------------------------------------------------------------------------------
					QNCBTransSum_TotalLoanLimit = qQCAReqNCBTransSum.SumCreditLimit,// edit
					QCreditAppRequestTable_MarketingComment = qCreditAppRequestTable.MarketingComment,
					QCreditAppRequestTable_ApproverComment = qCreditAppRequestTable.ApproverComment,
					QCreditAppRequestTable_CreditComment = qCreditAppRequestTable.CreditComment,
					QActionHistory1_ApproverName = qActionHistory[0] != null ? qActionHistory[0].ApproverName : "",
					QActionHistory1_Comment = qActionHistory[0] != null ? qActionHistory[0].Comment : "",
					QActionHistory1_CreatedDateTime = qActionHistory[0] != null ? qActionHistory[0].CreatedDateTime : "",
					QActionHistory2_ApproverName = qActionHistory[1] != null ? qActionHistory[1].ApproverName : "",
					QActionHistory2_Comment = qActionHistory[1] != null ? qActionHistory[1].Comment : "",
					QActionHistory2_CreatedDateTime = qActionHistory[1] != null ? qActionHistory[1].CreatedDateTime : "",
					QActionHistory3_ApproverName = qActionHistory[2] != null ? qActionHistory[2].ApproverName : "",
					QActionHistory3_Comment = qActionHistory[2] != null ? qActionHistory[2].Comment : "",
					QActionHistory3_CreatedDateTime = qActionHistory[2] != null ? qActionHistory[2].CreatedDateTime : "",
					QActionHistory4_ApproverName = qActionHistory[3] != null ? qActionHistory[3].ApproverName : "",
					QActionHistory4_Comment = qActionHistory[3] != null ? qActionHistory[3].Comment : "",
					QActionHistory4_CreatedDateTime = qActionHistory[3] != null ? qActionHistory[3].CreatedDateTime : "",
					QActionHistory5_ApproverName = qActionHistory[4] != null ? qActionHistory[4].ApproverName : "",
					QActionHistory5_Comment = qActionHistory[4] != null ? qActionHistory[4].Comment : "",
					QActionHistory5_CreatedDateTime = qActionHistory[4] != null ? qActionHistory[4].CreatedDateTime : "",
					//-------------------------------------------------------------------------------------------------
					QCAReqCreditOutStanding1_ApprovedCreditLimit = qCAReqCreditOutStanding[0] != null ? qCAReqCreditOutStanding[0].ApprovedCreditLimit : 0,
					QCAReqCreditOutStanding1_ARBalance = qCAReqCreditOutStanding[0] != null ? qCAReqCreditOutStanding[0].ARBalance : 0,
					QCAReqCreditOutStanding1_CreditLimitBalance = qCAReqCreditOutStanding[0] != null ? qCAReqCreditOutStanding[0].CreditLimitBalance : 0,
					QCAReqCreditOutStanding1_ReserveToBeRefund = qCAReqCreditOutStanding[0] != null ? qCAReqCreditOutStanding[0].ReserveToBeRefund : 0,
					QCAReqCreditOutStanding1_AccumRetentionAmount = qCAReqCreditOutStanding[0] != null ? qCAReqCreditOutStanding[0].AccumRetentionAmount : 0,

					QCreditAppRequestTable_PurchaseWithdrawalCondition = qCreditAppRequestTable !=null ? qCreditAppRequestTable.PurchaseWithdrawalCondition : "",
					QCreditAppRequestTable_AssignmentMethod = qCreditAppRequestTable != null ? qCreditAppRequestTable.AssignmentMethodTable : "",


				};
				return bookmarkDocumentCreditApplication;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region Bookmark document query_18CreditApplicationConsideration
		private QQCustomer GetQQCustomer(Guid CustomerTableGUID)
		{
			try
			{
				ICustomerTableRepo customerTable1 = new CustomerTableRepo(db);
				CustomerTable res_customerTable = customerTable1.GetCustomerTableByIdNoTrackingByAccessLevel(CustomerTableGUID);
				EmployeeTable res_employeeTable = (from employeeTable in db.Set<EmployeeTable>()
												   where employeeTable.EmployeeTableGUID == (Guid)res_customerTable.ResponsibleByGUID
												   select new EmployeeTable
												   {
													   Name = employeeTable.Name
												   }
											   ).FirstOrDefault();

				QQCustomer result = (from customerTable in db.Set<CustomerTable>()
									where customerTable.CustomerTableGUID == CustomerTableGUID
									select new QQCustomer
									{
										CustomerId = customerTable.CustomerId,
										CustomerName = customerTable.Name,
										IdentificationType = customerTable.IdentificationType,
										TaxID = customerTable.TaxID,
										PassportID = customerTable.PassportID,
										RecordType = customerTable.RecordType,
										DateOfEstablish = customerTable.DateOfEstablish.DateNullToString(),
										DateOfBirth = customerTable.DateOfBirth.DateNullToString(),
										IntroducedByGUID = customerTable.IntroducedByGUID.GuidNullToString(),
										BusinessTypeGUID = customerTable.BusinessTypeGUID.GuidNullToString(),
										ResponsibleByGUID = customerTable.ResponsibleByGUID.GuidNullToString(),
										ResponsibleBy = res_employeeTable.Name,
									}
									).AsNoTracking().FirstOrDefault();

				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private QQBankAccountControl GetQQBankAccountControl(Guid BankAccountControlGUID)
		{
			try
			{
				QQBankAccountControl result = (from custBank in db.Set<CustBank>()
											  join bankGroup in db.Set<BankGroup>()
											  on custBank.BankGroupGUID equals bankGroup.BankGroupGUID into ljbankGroup
											  from bankGroup in ljbankGroup.DefaultIfEmpty()

											  join bankType in db.Set<BankType>()
											  on custBank.BankTypeGUID equals bankType.BankTypeGUID into ljbankType
											  from bankType in ljbankType.DefaultIfEmpty()

											  where custBank.CustBankGUID == BankAccountControlGUID
											  select new QQBankAccountControl
											  {
												  BankGroupGUID = bankGroup.BankGroupGUID,
												  Instituetion = bankGroup.Description,
												  BankBranch = custBank.BankBranch,
												  BankTypeGUID = custBank.BankTypeGUID,
												  AccountType = bankType.Description,
												  BankAccountName = custBank.BankAccountName,
												  AccountNumber = custBank.AccountNumber,
											  }
								   ).AsNoTracking().FirstOrDefault();
				return result;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private QQPDCBank GetQQPDCBank(Guid PDCBankGUID)
		{
			try
			{
				QQPDCBank result = (from custBank in db.Set<CustBank>()
								   join bankGroup in db.Set<BankGroup>()
								   on custBank.BankGroupGUID equals bankGroup.BankGroupGUID into ljbankGroup
								   from bankGroup in ljbankGroup.DefaultIfEmpty()

								   join bankType in db.Set<BankType>()
								   on custBank.BankTypeGUID equals bankType.BankTypeGUID into ljbankType
								   from bankType in ljbankType.DefaultIfEmpty()

								   where custBank.CustBankGUID == PDCBankGUID
								   select new QQPDCBank
								   {
									   BankTypeGUID = custBank.BankTypeGUID,
									   CustPDCBankName = bankGroup.Description,
									   CustPDCBankBranch = custBank.BankBranch,
									   BankGroupGUID = custBank.BankGroupGUID,
									   CustPDCBankType = bankType.Description,
									   CustPDCBankAccountName = custBank.AccountNumber
								   }
								   ).AsNoTracking().FirstOrDefault();
				return result;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QQAuthorizedPersonTrans> GetQQAuthorizedPersonTrans(Guid CreditAppRequestTableGUID ,int CreditAppRequestType)
		{
			try
			{
				int i = 1;
				int j = 0;
				Guid tmp_RefGuid;
				ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
				CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTrackingByAccessLevel(CreditAppRequestTableGUID);
				if(CreditAppRequestType == 0)
                {
					tmp_RefGuid = creditAppRequestTable.CreditAppRequestTableGUID;
				}
                else
                {
					tmp_RefGuid = (Guid)creditAppRequestTable.RefCreditAppTableGUID;

				}

				List<QQAuthorizedPersonTrans> result = new List<QQAuthorizedPersonTrans>(new QQAuthorizedPersonTrans[8]);
				List<QQAuthorizedPersonTrans> tmp_result = (from authorizedPersonTrans in db.Set<AuthorizedPersonTrans>()
														   join relatedPersonTable in db.Set<RelatedPersonTable>()
														   on authorizedPersonTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID into ljrelatedPersonTable
														   from relatedPersonTable in ljrelatedPersonTable.DefaultIfEmpty()
														   where authorizedPersonTrans.RefGUID == tmp_RefGuid && authorizedPersonTrans.InActive == false
														   //orderby authorizedPersonTrans.Ordering ascending
														   select new QQAuthorizedPersonTrans
														   {
															   AuthorizedPersonTransGUID = authorizedPersonTrans.AuthorizedPersonTransGUID.GuidNullToString(),
															   AuthorizedPersonName = relatedPersonTable.Name,
															   DateOfBirth = relatedPersonTable.DateOfBirth,
															   NCBCheckedDate = authorizedPersonTrans.NCBCheckedDate.DateNullToString(),
															   Ordering = authorizedPersonTrans.Ordering,
															   Row = i + 1
														   }
					).OrderBy(o => o.Ordering).Take(8).ToList();
				foreach (var item in tmp_result)
				{
					result[j] = item;
					j++;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QQAuthorizedNCB1> GetQQAuthorizedNCB1s(Guid AuthorizedPersonTransGUID)
		{
			try
			{
				int i = 1;
				int j = 0;
				List<QQAuthorizedNCB1> result = new List<QQAuthorizedNCB1>(new QQAuthorizedNCB1[10]);
				List<QQAuthorizedNCB1> tmp_result = (from ncbTrans in db.Set<NCBTrans>()
															where ncbTrans.RefGUID == AuthorizedPersonTransGUID 
													 //orderby ncbTrans.CreditType, ncbTrans.NCBAccountStatus ascending
															select new QQAuthorizedNCB1
															{
																NCBTransGUID = ncbTrans.NCBTransGUID,
																BankGroupGUID = ncbTrans.BankGroupGUID.GuidNullToString(),
																FinancialInstitution = (from bankGroup in db.Set<BankGroup>() where bankGroup.BankGroupGUID == ncbTrans.BankGroupGUID 
																select new BankGroup { 
																	Description = bankGroup.Description
																   }).AsNoTracking().FirstOrDefault().Description,
																CreditTypeGUID = ncbTrans.CreditTypeGUID.GuidNullToString(),
																CreditType = (from creditType in db.Set<CreditType>() where creditType.CreditTypeGUID == ncbTrans.CreditTypeGUID
																				  select new CreditType { 
																					  Description = creditType.Description
																				  }).AsNoTracking().FirstOrDefault().Description,
																CreditTypeId = (from creditType in db.Set<CreditType>()
																				where creditType.CreditTypeGUID == ncbTrans.CreditTypeGUID
																				select new CreditType
																				{
																					CreditTypeId = creditType.CreditTypeId
																				}).FirstOrDefault().CreditTypeId,
																CreditLimit = ncbTrans.CreditLimit,
																CreatedDateTime = ncbTrans.CreatedDateTime,
																OutstandingAR = ncbTrans.OutstandingAR,
																MonthlyRepayment = ncbTrans.MonthlyRepayment,
																EndDate = ncbTrans.EndDate.DateToString(),
																NCBAccountStatusGUID = ncbTrans.NCBAccountStatusGUID.GuidNullToString(),
																NCBAccountStatus = (from ncbAccountStatus in db.Set<NCBAccountStatus>() where ncbAccountStatus.NCBAccountStatusGUID == ncbTrans.NCBAccountStatusGUID
																					select new NCBAccountStatus { 
																						Description = ncbAccountStatus.Description
																					}).AsNoTracking().FirstOrDefault().Description,
																NCBAccountStatusId = (from ncbAccountStatus in db.Set<NCBAccountStatus>()
																					  where ncbAccountStatus.NCBAccountStatusGUID == ncbTrans.NCBAccountStatusGUID
																					  select new NCBAccountStatus
																					  {
																						  NCBAccStatusId = ncbAccountStatus.NCBAccStatusId
																					  }).AsNoTracking().FirstOrDefault().NCBAccStatusId,
																Row = i + 1
															}
					).OrderBy(o => o.CreditTypeId).ThenByDescending(f => f.NCBAccountStatusId).ThenByDescending(m => m.CreatedDateTime).Take(10).ToList();
				foreach (var item in tmp_result)
				{
					result[j] = item;
					j++;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private QQSumAuthorizedNCB1 GetQQSumAuthorizedNCB1(Guid AuthorizedPersonTransGUID)
		{
			try
			{
				QQSumAuthorizedNCB1 result = (from ncbTrans in db.Set<NCBTrans>()
									where ncbTrans.RefGUID == AuthorizedPersonTransGUID
									group ncbTrans by 1 into GncbTrans
									select new QQSumAuthorizedNCB1
									{
											SumCreditLimit = GncbTrans.Sum(x => x.CreditLimit),
											SumOutstandingAR = GncbTrans.Sum(x => x.OutstandingAR),
											SumMonthlyRepayment = GncbTrans.Sum(x => x.MonthlyRepayment),
									}
								   ).AsNoTracking().FirstOrDefault();
				return result;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QQAuthorizedNCB2> GetQQAuthorizedNCB2s(Guid AuthorizedPersonTransGUID)
		{
			try
			{
				int i = 1;
				int j = 0;
				List<QQAuthorizedNCB2> result = new List<QQAuthorizedNCB2>(new QQAuthorizedNCB2[10]);
				List<QQAuthorizedNCB2> tmp_result = (from ncbTrans in db.Set<NCBTrans>()
													 where ncbTrans.RefGUID == AuthorizedPersonTransGUID
													 orderby ncbTrans.CreditType, ncbTrans.NCBAccountStatus ascending
													 select new QQAuthorizedNCB2
													 {
														 NCBTransGUID = ncbTrans.NCBTransGUID,
														 BankGroupGUID = ncbTrans.BankGroupGUID.GuidNullToString(),
														 FinancialInstitution = (from bankGroup in db.Set<BankGroup>()
																				 where bankGroup.BankGroupGUID == ncbTrans.BankGroupGUID
																				 select new BankGroup
																				 {
																					 Description = bankGroup.Description
																				 }).AsNoTracking().FirstOrDefault().Description,
														 CreditTypeGUID = ncbTrans.CreditTypeGUID.GuidNullToString(),
														 CreditType = (from creditType in db.Set<CreditType>()
																	   where creditType.CreditTypeGUID == ncbTrans.CreditTypeGUID
																	   select new CreditType
																	   {
																		   Description = creditType.Description
																	   }).AsNoTracking().FirstOrDefault().Description,
														 CreditTypeId = (from creditType in db.Set<CreditType>()
																		 where creditType.CreditTypeGUID == ncbTrans.CreditTypeGUID
																		 select new CreditType
																		 {
																			 CreditTypeId = creditType.CreditTypeId
																		 }).AsNoTracking().FirstOrDefault().CreditTypeId,
														 CreditLimit = ncbTrans.CreditLimit,
														 CreatedDateTime = ncbTrans.CreatedDateTime,
														 OutstandingAR = ncbTrans.OutstandingAR,
														 MonthlyRepayment = ncbTrans.MonthlyRepayment,
														 EndDate = ncbTrans.EndDate.DateToString(),
														 NCBAccountStatusGUID = ncbTrans.NCBAccountStatusGUID.GuidNullToString(),
														 NCBAccountStatus = (from ncbAccountStatus in db.Set<NCBAccountStatus>()
																			 where ncbAccountStatus.NCBAccountStatusGUID == ncbTrans.NCBAccountStatusGUID
																			 select new NCBAccountStatus
																			 {
																				 Description = ncbAccountStatus.Description
																			 }).AsNoTracking().FirstOrDefault().Description,
														 NCBAccountStatusId = (from ncbAccountStatus in db.Set<NCBAccountStatus>()
																			   where ncbAccountStatus.NCBAccountStatusGUID == ncbTrans.NCBAccountStatusGUID
																			   select new NCBAccountStatus
																			   {
																				   NCBAccStatusId = ncbAccountStatus.NCBAccStatusId
																			   }).AsNoTracking().FirstOrDefault().NCBAccStatusId,
														 Row = i + 1
													 }
					).OrderBy(o => o.CreditTypeId).ThenByDescending(f => f.NCBAccountStatusId).ThenByDescending(m => m.CreatedDateTime).Take(10).ToList();
				foreach (var item in tmp_result)
				{
					result[j] = item;
					j++;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private QQSumAuthorizedNCB2 GetQQSumAuthorizedNCB2(Guid AuthorizedPersonTransGUID)
		{
			try
			{
				QQSumAuthorizedNCB2 result = (from ncbTrans in db.Set<NCBTrans>()

											  where ncbTrans.RefGUID == AuthorizedPersonTransGUID
											  group ncbTrans by 1 into GncbTrans
											  select new QQSumAuthorizedNCB2
											  {
												  SumCreditLimit = GncbTrans.Sum(x => x.CreditLimit),
												  SumOutstandingAR = GncbTrans.Sum(x => x.OutstandingAR),
												  SumMonthlyRepayment = GncbTrans.Sum(x => x.MonthlyRepayment),
											  }
								   ).AsNoTracking().FirstOrDefault();
				return result;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QQAuthorizedNCB3> GetQQAuthorizedNCB3s(Guid AuthorizedPersonTransGUID)
		{
			try
			{
				int i = 1;
				int j = 0;
				List<QQAuthorizedNCB3> result = new List<QQAuthorizedNCB3>(new QQAuthorizedNCB3[10]);
				List<QQAuthorizedNCB3> tmp_result = (from ncbTrans in db.Set<NCBTrans>()
													 where ncbTrans.RefGUID == AuthorizedPersonTransGUID
													 orderby ncbTrans.CreditType, ncbTrans.NCBAccountStatus ascending
													 select new QQAuthorizedNCB3
													 {
														 NCBTransGUID = ncbTrans.NCBTransGUID,
														 BankGroupGUID = ncbTrans.BankGroupGUID.GuidNullToString(),
														 FinancialInstitution = (from bankGroup in db.Set<BankGroup>()
																				 where bankGroup.BankGroupGUID == ncbTrans.BankGroupGUID
																				 select new BankGroup
																				 {
																					 Description = bankGroup.Description
																				 }).AsNoTracking().FirstOrDefault().Description,
														 CreditTypeGUID = ncbTrans.CreditTypeGUID.GuidNullToString(),
														 CreditType = (from creditType in db.Set<CreditType>()
																	   where creditType.CreditTypeGUID == ncbTrans.CreditTypeGUID
																	   select new CreditType
																	   {
																		   Description = creditType.Description
																	   }).AsNoTracking().FirstOrDefault().Description,
														 CreditTypeId = (from creditType in db.Set<CreditType>()
																		 where creditType.CreditTypeGUID == ncbTrans.CreditTypeGUID
																		 select new CreditType
																		 {
																			 CreditTypeId = creditType.CreditTypeId
																		 }).AsNoTracking().FirstOrDefault().CreditTypeId,
														 CreditLimit = ncbTrans.CreditLimit,
														 CreatedDateTime = ncbTrans.CreatedDateTime,
														 OutstandingAR = ncbTrans.OutstandingAR,
														 MonthlyRepayment = ncbTrans.MonthlyRepayment,
														 EndDate = ncbTrans.EndDate.DateToString(),
														 NCBAccountStatusGUID = ncbTrans.NCBAccountStatusGUID.GuidNullToString(),
														 NCBAccountStatus = (from ncbAccountStatus in db.Set<NCBAccountStatus>()
																			 where ncbAccountStatus.NCBAccountStatusGUID == ncbTrans.NCBAccountStatusGUID
																			 select new NCBAccountStatus
																			 {
																				 Description = ncbAccountStatus.Description
																			 }).AsNoTracking().FirstOrDefault().Description,
														 NCBAccountStatusId = (from ncbAccountStatus in db.Set<NCBAccountStatus>()
																			   where ncbAccountStatus.NCBAccountStatusGUID == ncbTrans.NCBAccountStatusGUID
																			   select new NCBAccountStatus
																			   {
																				   NCBAccStatusId = ncbAccountStatus.NCBAccStatusId
																			   }).AsNoTracking().FirstOrDefault().NCBAccStatusId,
														 Row = i + 1
													 }
					).OrderBy(o => o.CreditTypeId).ThenByDescending(f => f.NCBAccountStatusId).ThenByDescending(m => m.CreatedDateTime).Take(10).ToList();
				foreach (var item in tmp_result)
				{
					result[j] = item;
					j++;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private QQSumAuthorizedNCB3 GetQQSumAuthorizedNCB3(Guid AuthorizedPersonTransGUID)
		{
			try
			{
				QQSumAuthorizedNCB3 result = (from ncbTrans in db.Set<NCBTrans>()

											  where ncbTrans.RefGUID == AuthorizedPersonTransGUID
											  group ncbTrans by 1 into GncbTrans
											  select new QQSumAuthorizedNCB3
											  {
												  SumCreditLimit = GncbTrans.Sum(x => x.CreditLimit),
												  SumOutstandingAR = GncbTrans.Sum(x => x.OutstandingAR),
												  SumMonthlyRepayment = GncbTrans.Sum(x => x.MonthlyRepayment),
											  }
								   ).AsNoTracking().FirstOrDefault();
				return result;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QQOwnerTrans> GetQQOwnerTrans(Guid CreditAppRequestTableGUID, int CreditAppRequestType)
		{
			try
			{
				int i = 1;
				int j = 0;
				Guid tmp_RefGuid;
				ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
				CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTrackingByAccessLevel(CreditAppRequestTableGUID);
				if (CreditAppRequestType == 0)
				{
					tmp_RefGuid = creditAppRequestTable.CreditAppRequestTableGUID;
				}
				else
				{
					tmp_RefGuid = (Guid)creditAppRequestTable.RefCreditAppTableGUID;

				}

				List<QQOwnerTrans> result = new List<QQOwnerTrans>(new QQOwnerTrans[8]);
				List<QQOwnerTrans> tmp_result = (from ownerTrans in db.Set<OwnerTrans>()
													join relatedPersonTable in db.Set<RelatedPersonTable>() on ownerTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID into ljrelatedPersonTable
													from relatedPersonTable in ljrelatedPersonTable.DefaultIfEmpty()
												 orderby ownerTrans.Ordering ascending
												 where ownerTrans.RefGUID == tmp_RefGuid
												 select new QQOwnerTrans
												   {
													   ShareHolderPersonName = relatedPersonTable.Name,
													   PropotionOfShareholderPct = ownerTrans.PropotionOfShareholderPct,
													   Row = i + 1
												   }
					).Take(8).AsNoTracking().ToList();
				foreach (var item in tmp_result)
				{
					result[j] = item;
					j++;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QQCreditAppReqLineFin> GetQQCreditAppReqLineFins(Guid RefGuid)
		{
			try
			{
				int i = 1;
				int j = 0;
				List<QQCreditAppReqLineFin> result = new List<QQCreditAppReqLineFin>(new QQCreditAppReqLineFin[5]);
				List<QQCreditAppReqLineFin> tmp_result = (from financialStatementTrans in db.Set<FinancialStatementTrans>()
														 where financialStatementTrans.RefGUID == RefGuid && financialStatementTrans.RefType == (int)RefType.CreditAppRequestTable
														 select new QQCreditAppReqLineFin
														 {
															 Year = financialStatementTrans.Year,
															 Row = i + 1,
															 RegisteredCapital = (from financialStatementTrans1 in db.Set<FinancialStatementTrans>()
																				  where financialStatementTrans1.RefGUID == RefGuid && financialStatementTrans1.RefType == (int)RefType.CreditAppRequestTable && financialStatementTrans1.Year == financialStatementTrans.Year && financialStatementTrans1.Ordering == 1
																				  select financialStatementTrans1.Amount
																				  ).FirstOrDefault(),
															 PaidCapital = (from financialStatementTrans2 in db.Set<FinancialStatementTrans>()
																			where financialStatementTrans2.RefGUID == RefGuid && financialStatementTrans2.RefType == (int)RefType.CreditAppRequestTable && financialStatementTrans2.Year == financialStatementTrans.Year && financialStatementTrans2.Ordering == 2
																			select financialStatementTrans2.Amount).FirstOrDefault(),
															 TotalAsset = (from financialStatementTrans3 in db.Set<FinancialStatementTrans>()
																		   where financialStatementTrans3.RefGUID == RefGuid && financialStatementTrans3.RefType == (int)RefType.CreditAppRequestTable && financialStatementTrans3.Year == financialStatementTrans.Year && financialStatementTrans3.Ordering == 3
																		   select financialStatementTrans3.Amount).FirstOrDefault(),
															 TotalLiability = (from financialStatementTrans4 in db.Set<FinancialStatementTrans>()
																			   where financialStatementTrans4.RefGUID == RefGuid && financialStatementTrans4.RefType == (int)RefType.CreditAppRequestTable && financialStatementTrans4.Year == financialStatementTrans.Year && financialStatementTrans4.Ordering == 4
																			   select financialStatementTrans4.Amount).FirstOrDefault(),
															 TotalEquity = (from financialStatementTrans5 in db.Set<FinancialStatementTrans>()
																			where financialStatementTrans5.RefGUID == RefGuid && financialStatementTrans5.RefType == (int)RefType.CreditAppRequestTable && financialStatementTrans5.Year == financialStatementTrans.Year && financialStatementTrans5.Ordering == 5
																			select financialStatementTrans5.Amount).FirstOrDefault(),
															 TotalRevenue = (from financialStatementTrans6 in db.Set<FinancialStatementTrans>()
																			 where financialStatementTrans6.RefGUID == RefGuid && financialStatementTrans6.RefType == (int)RefType.CreditAppRequestTable && financialStatementTrans6.Year == financialStatementTrans.Year && financialStatementTrans6.Ordering == 6
																			 select financialStatementTrans6.Amount).FirstOrDefault(),
															 TotalCOGS = (from financialStatementTrans7 in db.Set<FinancialStatementTrans>()
																		  where financialStatementTrans7.RefGUID == RefGuid && financialStatementTrans7.RefType == (int)RefType.CreditAppRequestTable && financialStatementTrans7.Year == financialStatementTrans.Year && financialStatementTrans7.Ordering == 7
																		  select financialStatementTrans7.Amount).FirstOrDefault(),
															 TotalGrossProfit = (from financialStatementTrans8 in db.Set<FinancialStatementTrans>()
																				 where financialStatementTrans8.RefGUID == RefGuid && financialStatementTrans8.RefType == (int)RefType.CreditAppRequestTable && financialStatementTrans8.Year == financialStatementTrans.Year && financialStatementTrans8.Ordering == 8
																				 select financialStatementTrans8.Amount).FirstOrDefault(),
															 TotalOperExpFirst = (from financialStatementTrans9 in db.Set<FinancialStatementTrans>()
																				  where financialStatementTrans9.RefGUID == RefGuid && financialStatementTrans9.RefType == (int)RefType.CreditAppRequestTable && financialStatementTrans9.Year == financialStatementTrans.Year && financialStatementTrans9.Ordering == 9
																				  select financialStatementTrans9.Amount).FirstOrDefault(),
															 TotalNetProfitFirst = (from financialStatementTrans10 in db.Set<FinancialStatementTrans>()
																					where financialStatementTrans10.RefGUID == RefGuid && financialStatementTrans10.RefType == (int)RefType.CreditAppRequestTable && financialStatementTrans10.Year == financialStatementTrans.Year && financialStatementTrans10.Ordering == 10
																					select financialStatementTrans10.Amount).FirstOrDefault(),
															 NetProfitPercent = (from financialStatementTrans11 in db.Set<FinancialStatementTrans>()
																				 where financialStatementTrans11.RefGUID == RefGuid && financialStatementTrans11.RefType == (int)RefType.CreditAppRequestTable && financialStatementTrans11.Year == financialStatementTrans.Year && financialStatementTrans11.Ordering == 11
																				 select financialStatementTrans11.Amount).FirstOrDefault(),
															 lDE = (from financialStatementTrans12 in db.Set<FinancialStatementTrans>()
																	where financialStatementTrans12.RefGUID == RefGuid && financialStatementTrans12.RefType == (int)RefType.CreditAppRequestTable && financialStatementTrans12.Year == financialStatementTrans.Year && financialStatementTrans12.Ordering == 12
																	select financialStatementTrans12.Amount).FirstOrDefault(),
															 QuickRatio = (from financialStatementTrans13 in db.Set<FinancialStatementTrans>()
																		   where financialStatementTrans13.RefGUID == RefGuid && financialStatementTrans13.RefType == (int)RefType.CreditAppRequestTable && financialStatementTrans13.Year == financialStatementTrans.Year && financialStatementTrans13.Ordering == 13
																		   select financialStatementTrans13.Amount).FirstOrDefault(),
															 IntCoverageRatio = (from financialStatementTrans14 in db.Set<FinancialStatementTrans>()
																				 where financialStatementTrans14.RefGUID == RefGuid && financialStatementTrans14.RefType == (int)RefType.CreditAppRequestTable && financialStatementTrans14.Year == financialStatementTrans.Year && financialStatementTrans14.Ordering == 14
																				 select financialStatementTrans14.Amount).FirstOrDefault()
														 }).Distinct().OrderByDescending(x => x.Year).Take(5).AsNoTracking().ToList();
				foreach (var item in tmp_result)
				{
					result[j] = item;
					j++;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QQCAReqNCBTrans> GetQQCAReqNCBTrans(Guid CreditAppRequestTableGUID)
		{
			try
			{
				int i = 1;
				int j = 0;
				List<QQCAReqNCBTrans> result = new List<QQCAReqNCBTrans>(new QQCAReqNCBTrans[30]);
				List<QQCAReqNCBTrans> tmp_result = (from ncbTrans in db.Set<NCBTrans>()
												 join bankGroup in db.Set<BankGroup>() on ncbTrans.BankGroupGUID equals bankGroup.BankGroupGUID into ljbankGroup
												 from bankGroup in ljbankGroup.DefaultIfEmpty()

												 join creditType in db.Set<CreditType>() on ncbTrans.CreditTypeGUID equals creditType.CreditTypeGUID into ljcreditType
												 from creditType in ljcreditType.DefaultIfEmpty()

												 join nCBAccountStatus in db.Set<NCBAccountStatus>() on ncbTrans.NCBAccountStatusGUID equals nCBAccountStatus.NCBAccountStatusGUID into ljnCBAccountStatus
												 from nCBAccountStatus in ljnCBAccountStatus.DefaultIfEmpty()
												 //orderby ncbTrans.CreatedDateTime ascending
												 where ncbTrans.RefGUID == CreditAppRequestTableGUID
												 select new QQCAReqNCBTrans
												 {
													 NCBTransGUID = ncbTrans.NCBTransGUID,
													 BankGroupGUID = ncbTrans.BankGroupGUID,
													 FinancialInstitution = bankGroup.Description,
													 CreditTypeGUID = ncbTrans.CreditTypeGUID,
													 CreditTypeId = creditType.CreditTypeId,
													 CreditType = creditType.Description,
													 NCBAccountStatusGUID = ncbTrans.NCBAccountStatusGUID,
													 NCBAccountStatusId = nCBAccountStatus.NCBAccStatusId,
													 NCBAccountStatus = nCBAccountStatus.Description,
													 CreditLimit = ncbTrans.CreditLimit,
													 OutstandingAR = ncbTrans.OutstandingAR,
													 MonthlyRepayment = ncbTrans.MonthlyRepayment,
													 EndDate = ncbTrans.EndDate,
													 CreatedDateTime = ncbTrans.CreatedDateTime,
												 }
					).OrderBy(o => o.CreditTypeId).ThenByDescending(f => f.NCBAccountStatusId).ThenByDescending(m => m.CreatedDateTime).Take(30).ToList();
				foreach (var item in tmp_result)
				{
					result[j] = item;
					result[j].Row = j;
					j++;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private QQCAReqNCBTransSum GetCAReqNCBTransSum(Guid CreditAppRequestTableGUID)
		{
			try
			{
				QQCAReqNCBTransSum result = (from ncbTrans in db.Set<NCBTrans>()
											  where ncbTrans.RefGUID == CreditAppRequestTableGUID
											 group ncbTrans by 1 into GncbTrans
											 select new QQCAReqNCBTransSum
											  {
												 TotalLoanLimit = GncbTrans.Sum(x => x.CreditLimit)
											  }
								   ).FirstOrDefault();
				return result;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private QCAReqNCBTransSum GetCAReqNCBTransSum2(Guid CreditAppRequestTableGUID)
		{
			try
			{
				QCAReqNCBTransSum result = (from ncbTrans in db.Set<NCBTrans>()
											 where ncbTrans.RefGUID == CreditAppRequestTableGUID
											 group ncbTrans by 1 into GncbTrans
											 select new QCAReqNCBTransSum
											 {
												 SumCreditLimit = GncbTrans.Sum(x => x.CreditLimit),
												 SumOutstandingAR = GncbTrans.Sum(x => x.OutstandingAR),
												 SumMonthlyRepayment = GncbTrans.Sum(x => x.MonthlyRepayment),
											 }
								   ).FirstOrDefault();
				return result;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QQCAReqBuyerCreditOutstandingPrivate> GetQCAReqBuyerCreditOutstandingPrivates(Guid CreditAppRequestTableGUID)
		{
			try
			{
				int i = 1;
				int j = 0;
				List<QQCAReqBuyerCreditOutstandingPrivate> result = new List<QQCAReqBuyerCreditOutstandingPrivate>(new QQCAReqBuyerCreditOutstandingPrivate[10]);
				List<QQCAReqBuyerCreditOutstandingPrivate> tmp_result = (from cAReqBuyerCreditOutstanding in db.Set<CAReqBuyerCreditOutstanding>()
													join buyerTable in db.Set<BuyerTable>() on cAReqBuyerCreditOutstanding.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
																		 from buyerTable in ljbuyerTable.DefaultIfEmpty()
													join businessSegment in db.Set<BusinessSegment>() on buyerTable.BusinessSegmentGUID equals businessSegment.BusinessSegmentGUID into ljbusinessSegment from businessSegment in ljbusinessSegment.DefaultIfEmpty()
																		 orderby cAReqBuyerCreditOutstanding.LineNum ascending
													where cAReqBuyerCreditOutstanding.CreditAppRequestTableGUID == CreditAppRequestTableGUID && businessSegment.BusinessSegmentType == 1
																		 select new QQCAReqBuyerCreditOutstandingPrivate
													{
														BuyerTableGUID = cAReqBuyerCreditOutstanding.BuyerTableGUID.GuidNullToString(),
														BuyerCreditOutstandingBuyerName = buyerTable.BuyerName,
														BusinessSegmentGUID = buyerTable.BusinessSegmentGUID.GuidNullToString(),
														BusinessSegmentType = (from businessSegment in db.Set<BusinessSegment>()
																			   where businessSegment.BusinessSegmentGUID == buyerTable.BusinessSegmentGUID
																			   select businessSegment.BusinessSegmentType
																				  ).FirstOrDefault(),
														Status = cAReqBuyerCreditOutstanding.Status,
														ApprovedCreditLimitLine = cAReqBuyerCreditOutstanding.ApprovedCreditLimitLine,
														ARBalance = cAReqBuyerCreditOutstanding.ARBalance,
														MaxPurchasePct = cAReqBuyerCreditOutstanding.MaxPurchasePct,
														AssignmentMethodGUID = cAReqBuyerCreditOutstanding.AssignmentMethodGUID.GuidNullToString(),
																			 AssignmentMethod = (from creditAppLine in db.Set<CreditAppLine>() where creditAppLine.CreditAppLineGUID == cAReqBuyerCreditOutstanding.CreditAppLineGUID select creditAppLine.AssignmentMethodRemark).FirstOrDefault(),
																			 BillingResponsibleByGUID = cAReqBuyerCreditOutstanding.BillingResponsibleByGUID.GuidNullToString(),
														BillingResponsibleBy = (from billingResponsibleBy in db.Set<BillingResponsibleBy>() where billingResponsibleBy.BillingResponsibleByGUID == cAReqBuyerCreditOutstanding.BillingResponsibleByGUID select billingResponsibleBy.Description).FirstOrDefault(),
														MethodOfPaymentGUID = cAReqBuyerCreditOutstanding.MethodOfPaymentGUID.GuidNullToString(),
														MethodOfPayment = (from methodOfPayment in db.Set<MethodOfPayment>() where cAReqBuyerCreditOutstanding.MethodOfPaymentGUID == methodOfPayment.MethodOfPaymentGUID select methodOfPayment.Description).FirstOrDefault(),
														Row = i+1,

													}
					).Take(10).AsNoTracking().ToList();
				foreach (var item in tmp_result)
				{
					result[j] = item;
					j++;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private QCAReqBuyerCreditOutstandingPrivateCOUNT getQCAReqBuyerCreditOutstandingPrivateCOUNT(Guid CreditAppRequestTableGUID)
        {
            try
            {
				QCAReqBuyerCreditOutstandingPrivateCOUNT result = (from cAReqBuyerCreditOutstanding in db.Set<CAReqBuyerCreditOutstanding>()
																   join buyerTable in db.Set<BuyerTable>() on cAReqBuyerCreditOutstanding.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
																   from buyerTable in ljbuyerTable.DefaultIfEmpty()
																   join businessSegment in db.Set<BusinessSegment>() on buyerTable.BusinessSegmentGUID equals businessSegment.BusinessSegmentGUID into ljbusinessSegment
																   from businessSegment in ljbusinessSegment.DefaultIfEmpty()
																   orderby cAReqBuyerCreditOutstanding.LineNum ascending
																   where cAReqBuyerCreditOutstanding.CreditAppRequestTableGUID == CreditAppRequestTableGUID && businessSegment.BusinessSegmentType == 1
																	group cAReqBuyerCreditOutstanding by 1 into G
																   select new QCAReqBuyerCreditOutstandingPrivateCOUNT
                                                                   {
																	   CountCAbuyerCreditOutstandingPrivate = G.Count()

                                                                   }
																   ).FirstOrDefault();

				return result;
			}
			catch (Exception e)
            {
				throw SmartAppUtil.AddStackTrace(e);
			}
        }
		private List<QQCAReqBuyerCreditOutstandingGov> GetQQCAReqBuyerCreditOutstandingGovs(Guid CreditAppRequestTableGUID)
		{
			try
			{
				int i = 1;
				int j = 0;
				List<QQCAReqBuyerCreditOutstandingGov> result = new List<QQCAReqBuyerCreditOutstandingGov>(new QQCAReqBuyerCreditOutstandingGov[10]);
				List<QQCAReqBuyerCreditOutstandingGov> tmp_result = (from cAReqBuyerCreditOutstanding in db.Set<CAReqBuyerCreditOutstanding>()
													join buyerTable in db.Set<BuyerTable>() on  cAReqBuyerCreditOutstanding.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
																	 from buyerTable in ljbuyerTable.DefaultIfEmpty()
													join businessSegment in db.Set<BusinessSegment>() on buyerTable.BusinessSegmentGUID equals businessSegment.BusinessSegmentGUID into ljbusinessSegment
																	 from businessSegment in ljbusinessSegment.DefaultIfEmpty()
													//orderby cAReqBuyerCreditOutstanding.LineNum ascending
													where cAReqBuyerCreditOutstanding.CreditAppRequestTableGUID == CreditAppRequestTableGUID && businessSegment.BusinessSegmentType == 2
																	 select new QQCAReqBuyerCreditOutstandingGov
																	 {
																		 BuyerCreditOutstandingBuyerName = buyerTable.BuyerName,
																		 BusinessSegmentGUID = buyerTable.BusinessSegmentGUID.GuidNullToString(),
																		 BusinessSegmentType = (from businessSegment in db.Set<BusinessSegment>()
																								where businessSegment.BusinessSegmentGUID == buyerTable.BusinessSegmentGUID
																								select businessSegment.BusinessSegmentType
																				  ).FirstOrDefault(),
																		 Status = cAReqBuyerCreditOutstanding.Status,
																		 ApprovedCreditLimitLine = cAReqBuyerCreditOutstanding.ApprovedCreditLimitLine,
																		 ARBalance = cAReqBuyerCreditOutstanding.ARBalance,
																		 MaxPurchasePct = cAReqBuyerCreditOutstanding.MaxPurchasePct,
																		 AssignmentMethodGUID = cAReqBuyerCreditOutstanding.AssignmentMethodGUID.GuidNullToString(),
																		 AssignmentMethod = (from creditAppLine in db.Set<CreditAppLine>() where creditAppLine.CreditAppLineGUID == cAReqBuyerCreditOutstanding.CreditAppLineGUID select creditAppLine.AssignmentMethodRemark).FirstOrDefault(),
																		 BillingResponsibleByGUID = cAReqBuyerCreditOutstanding.BillingResponsibleByGUID.GuidNullToString(),
																		 BillingResponsibleBy = (from billingResponsibleBy in db.Set<BillingResponsibleBy>() where billingResponsibleBy.BillingResponsibleByGUID == cAReqBuyerCreditOutstanding.BillingResponsibleByGUID select billingResponsibleBy.Description).FirstOrDefault(),
																		 MethodOfPaymentGUID = cAReqBuyerCreditOutstanding.MethodOfPaymentGUID.GuidNullToString(),
																		 MethodOfPayment = (from methodOfPayment in db.Set<MethodOfPayment>() where cAReqBuyerCreditOutstanding.MethodOfPaymentGUID == (Guid?)methodOfPayment.MethodOfPaymentGUID select methodOfPayment.Description).FirstOrDefault(),
																		 LineNum = cAReqBuyerCreditOutstanding.LineNum,
																		 Row = i + 1
													}
					).OrderBy(o => o.LineNum).Take(10).AsNoTracking().ToList();
				foreach (var item in tmp_result)
				{
					result[j] = item;
					j++;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private QCAReqBuyerCreditOutstandingGovCOUNT getQCAReqBuyerCreditOutstandingGovCOUNT(Guid CreditAppRequestTableGUID)
		{
			try
			{
				QCAReqBuyerCreditOutstandingGovCOUNT result = (from cAReqBuyerCreditOutstanding in db.Set<CAReqBuyerCreditOutstanding>()
																   join buyerTable in db.Set<BuyerTable>() on cAReqBuyerCreditOutstanding.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
																   from buyerTable in ljbuyerTable.DefaultIfEmpty()
																   join businessSegment in db.Set<BusinessSegment>() on buyerTable.BusinessSegmentGUID equals businessSegment.BusinessSegmentGUID into ljbusinessSegment
																   from businessSegment in ljbusinessSegment.DefaultIfEmpty()
																   orderby cAReqBuyerCreditOutstanding.LineNum ascending
																   where cAReqBuyerCreditOutstanding.CreditAppRequestTableGUID == CreditAppRequestTableGUID && businessSegment.BusinessSegmentType == 2
															   group cAReqBuyerCreditOutstanding by 1 into G
															   select new QCAReqBuyerCreditOutstandingGovCOUNT
																   {
																	   CountCAbuyerCreditOutstandingPrivate = G.Count()

																   }
																   ).FirstOrDefault();

				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QQCreditAppReqAssignment> GetQQCreditAppReqAssignments(Guid CreditAppRequestTableGUID)
		{
			try
			{
				int i = 1;
				int j = 0;
				List<QQCreditAppReqAssignment> result = new List<QQCreditAppReqAssignment>(new QQCreditAppReqAssignment[30]);
				List<QQCreditAppReqAssignment> tmp_result = (from creditAppReqAssignment in db.Set<CreditAppReqAssignment>()
																	 join assignmentAgreementTable in db.Set<AssignmentAgreementTable>() on creditAppReqAssignment.AssignmentAgreementTableGUID equals assignmentAgreementTable.AssignmentAgreementTableGUID into ljassignmentAgreementTable
															 from assignmentAgreementTable in ljassignmentAgreementTable.DefaultIfEmpty()
															 join buyerTable in db.Set<BuyerTable>() on creditAppReqAssignment.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
															 from buyerTable in ljbuyerTable.DefaultIfEmpty()
															 orderby assignmentAgreementTable.InternalAssignmentAgreementId ascending
																	 where creditAppReqAssignment.CreditAppRequestTableGUID == CreditAppRequestTableGUID
																	 select new QQCreditAppReqAssignment
																	 {
																		 BuyerTableGUID = creditAppReqAssignment.CreditAppRequestTableGUID.GuidNullToString(),
																		 AssignmentBuyer = buyerTable.BuyerName + " (" + buyerTable.BuyerId +")",
																		 BuyerAgreementTableGUID = creditAppReqAssignment.BuyerAgreementTableGUID.GuidNullToString(),
																		 AssignmentRefAgreementId = (from buyerAgreementTable in db.Set<BuyerAgreementTable>() where creditAppReqAssignment.BuyerAgreementTableGUID == buyerAgreementTable.BuyerAgreementTableGUID select buyerAgreementTable.BuyerAgreementId).FirstOrDefault(),
																		 AssignmentBuyerAgreementAmount = creditAppReqAssignment.BuyerAgreementAmount,
																		 AssignmentAgreementAmount = creditAppReqAssignment.AssignmentAgreementAmount,
																		 AssignmentAgreementTableGUID = creditAppReqAssignment.AssignmentAgreementTableGUID.GuidNullToString(),
																		 InternalAssignmentAgreementId = assignmentAgreementTable.InternalAssignmentAgreementId,
																		 AssignmentAgreementDate = assignmentAgreementTable.AgreementDate.DateToString(),
																		 RemainingAmount = creditAppReqAssignment.RemainingAmount,
																		 Description = creditAppReqAssignment.Description,
																		 Row = i+1,
																	 }
					).Take(30).AsNoTracking().ToList();
				foreach (var item in tmp_result)
				{
					result[j] = item;
					j++;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QCreditAppReqAssignmentOutstanding> GetQCreditAppReqAssignmentOutstandings(Guid CreditAppRequestTableGUID)
        {
			try
			{
				int i = 1;
				int j = 0;
				ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);

				List <QCreditAppReqAssignmentOutstanding> result = new List<QCreditAppReqAssignmentOutstanding>(new QCreditAppReqAssignmentOutstanding[30]);
				List<QCreditAppReqAssignmentOutstanding> tmp_result = (from cAReqAssignmentOutstanding in db.Set<CAReqAssignmentOutstanding>()
															 join assignmentAgreementTable in db.Set<AssignmentAgreementTable>() on cAReqAssignmentOutstanding.AssignmentAgreementTableGUID equals assignmentAgreementTable.AssignmentAgreementTableGUID into ljassignmentAgreementTable
															 from assignmentAgreementTable in ljassignmentAgreementTable.DefaultIfEmpty()
															 join buyerTable in db.Set<BuyerTable>() on cAReqAssignmentOutstanding.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
															 from buyerTable in ljbuyerTable.DefaultIfEmpty()
															 orderby assignmentAgreementTable.InternalAssignmentAgreementId ascending
															 where cAReqAssignmentOutstanding.CreditAppRequestTableGUID == CreditAppRequestTableGUID
															 select new QCreditAppReqAssignmentOutstanding
															 {
																 BuyerTableGUID = (Guid)cAReqAssignmentOutstanding.BuyerTableGUID,
																 AssignmentBuyer = buyerTable.BuyerName + " (" + buyerTable.BuyerId + ")",
																 AssignmentRefAgreementId = (from assignmentAgreementLine in db.Set<AssignmentAgreementLine>()
																							 join buyerAgreementTable in db.Set< BuyerAgreementTable >() on assignmentAgreementLine.BuyerAgreementTableGUID equals buyerAgreementTable.BuyerAgreementTableGUID into ljbuyerAgreementTable
																							 from buyerAgreementTable in ljbuyerAgreementTable.DefaultIfEmpty()
																							 orderby assignmentAgreementLine.LineNum ascending
																							 where assignmentAgreementLine.AssignmentAgreementTableGUID == cAReqAssignmentOutstanding.AssignmentAgreementTableGUID select buyerAgreementTable.ReferenceAgreementID).Take(1).FirstOrDefault(),
                                                                 AssignmentBuyerAgreementAmount = (from assignmentAgreementLine in db.Set<AssignmentAgreementLine>()
																								   join buyerAgreementTable in db.Set<BuyerAgreementTable>() on assignmentAgreementLine.BuyerAgreementTableGUID equals buyerAgreementTable.BuyerAgreementTableGUID into ljbuyerAgreementTable
																								   from buyerAgreementTable in ljbuyerAgreementTable.DefaultIfEmpty()
																								   join buyerAgreementLine in db.Set<BuyerAgreementLine>() on buyerAgreementTable.BuyerAgreementTableGUID equals buyerAgreementLine.BuyerAgreementTableGUID into ljbuyerAgreementLine
																								   from buyerAgreementLine in ljbuyerAgreementLine.DefaultIfEmpty()
																								   orderby assignmentAgreementLine.LineNum ascending
																								   where assignmentAgreementLine.AssignmentAgreementTableGUID == cAReqAssignmentOutstanding.AssignmentAgreementTableGUID
																								   select buyerAgreementLine.Amount
																								   ).Sum(),
                                                                 AssignmentAgreementAmount = cAReqAssignmentOutstanding.AssignmentAgreementAmount,
																InternalAssignmentAgreementId = assignmentAgreementTable.InternalAssignmentAgreementId != null ? assignmentAgreementTable.InternalAssignmentAgreementId : "-",
																AssignmentAgreementDate = assignmentAgreementTable.AgreementDate.DateToString(),
																Description = assignmentAgreementTable.Description,
																RemainingAmount = cAReqAssignmentOutstanding.RemainingAmount
															 }
					).Take(30).AsNoTracking().ToList();
				foreach (var item in tmp_result)
				{
					result[j] = item;
					j++;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private QSumCreditAppReqAssignmentOutstanding getQSumCreditAppReqAssignmentOutstanding(Guid CreditAppRequestTableGUID)
		{
			try
			{

				QSumCreditAppReqAssignmentOutstanding result = (from cAReqAssignmentOutstanding in db.Set<CAReqAssignmentOutstanding>()

																join assignmentAgreementLine in db.Set< AssignmentAgreementLine >() on cAReqAssignmentOutstanding.AssignmentAgreementTableGUID equals assignmentAgreementLine.AssignmentAgreementTableGUID into ljassignmentAgreementLine
																from assignmentAgreementLine in ljassignmentAgreementLine.DefaultIfEmpty()

                                                                join buyerAgreementTable in db.Set<BuyerAgreementTable>() on assignmentAgreementLine.BuyerAgreementTableGUID equals buyerAgreementTable.BuyerAgreementTableGUID into ljbuyerAgreementTable
                                                                from buyerAgreementTable in ljbuyerAgreementTable.DefaultIfEmpty()

                                                                join buyerAgreementLine in db.Set<BuyerAgreementLine>() on buyerAgreementTable.BuyerAgreementTableGUID equals buyerAgreementLine.BuyerAgreementTableGUID into ljbuyerAgreementLine
                                                                from buyerAgreementLine in ljbuyerAgreementLine.DefaultIfEmpty()

                                                                orderby assignmentAgreementLine.LineNum ascending
																where cAReqAssignmentOutstanding.CreditAppRequestTableGUID == CreditAppRequestTableGUID
																group new { cAReqAssignmentOutstanding , buyerAgreementLine } by 1 into G
													  select new QSumCreditAppReqAssignmentOutstanding
													  {
															SumAssignmentBuyerAgreementAmount = G.Sum(o => o.buyerAgreementLine.Amount),
															SumAssignmentAgreementAmount = G.Sum(o => o.cAReqAssignmentOutstanding.AssignmentAgreementAmount),
															SumRemainingAmount = G.Sum(o => o.cAReqAssignmentOutstanding.RemainingAmount),
													  }
								   ).FirstOrDefault();
				result.SumAssignmentAgreementAmount = (from cAReqAssignmentOutstanding in db.Set<CAReqAssignmentOutstanding>()

						  join assignmentAgreementLine in db.Set<AssignmentAgreementLine>() on cAReqAssignmentOutstanding.AssignmentAgreementTableGUID equals assignmentAgreementLine.AssignmentAgreementTableGUID into ljassignmentAgreementLine
						  from assignmentAgreementLine in ljassignmentAgreementLine.DefaultIfEmpty()

						  orderby assignmentAgreementLine.LineNum ascending
						  where cAReqAssignmentOutstanding.CreditAppRequestTableGUID == CreditAppRequestTableGUID
						  select cAReqAssignmentOutstanding.AssignmentAgreementAmount).Sum();
				return result;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QQCustBusinessCollateral> GetQCustBusinessCollaterals(Guid CustomerTableGUID)
		{
			try
			{
				int i = 1;
				int j = 0;
				List<QQCustBusinessCollateral> result = new List<QQCustBusinessCollateral>(new QQCustBusinessCollateral[30]);
				List<QQCustBusinessCollateral> tmp_result = (from custBusinessCollateral in db.Set<CustBusinessCollateral>()
													orderby custBusinessCollateral.CustBusinessCollateralId ascending
													where custBusinessCollateral.CustomerTableGUID == CustomerTableGUID && custBusinessCollateral.Cancelled == false
															 select new QQCustBusinessCollateral
													{
																 BusinessCollateralTypeGUID = custBusinessCollateral.BusinessCollateralTypeGUID.GuidNullToString(),
																 BusinessCollateralType = (from businessCollateralType in db.Set< BusinessCollateralType>() where custBusinessCollateral.BusinessCollateralTypeGUID == businessCollateralType.BusinessCollateralTypeGUID select businessCollateralType.Description).FirstOrDefault(),
																 BusinessCollateralSubTypeGUID = custBusinessCollateral.BusinessCollateralTypeGUID.GuidNullToString(),
																 BusinessCollateralSubType = (from businessCollateralSubType in db.Set< BusinessCollateralSubType>() where custBusinessCollateral.BusinessCollateralSubTypeGUID == businessCollateralSubType.BusinessCollateralSubTypeGUID select businessCollateralSubType.Description).FirstOrDefault(),
																 Description = custBusinessCollateral.Description,
																 BusinessCollateralValue = custBusinessCollateral.BusinessCollateralValue,
																 Cancelled = custBusinessCollateral.Cancelled,
																 BusinessCollateralStatus = (from businessCollateralStatus in db.Set<BusinessCollateralStatus>() where businessCollateralStatus.BusinessCollateralStatusGUID == custBusinessCollateral.BusinessCollateralStatusGUID select businessCollateralStatus.Description).FirstOrDefault(),
																 Row = i+1
															 }
					).Take(30).AsNoTracking().ToList();
				foreach (var item in tmp_result)
				{
					result[j] = item;
					j++;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private QQSumCustBusinessCollateral GetQQSumCustBusinessCollateral(Guid CustomerTableGUID)
		{
			try
			{
				QQSumCustBusinessCollateral result = (from custBusinessCollateral in db.Set<CustBusinessCollateral>()

											  where custBusinessCollateral.CustomerTableGUID == CustomerTableGUID && custBusinessCollateral.Cancelled == false
													  group custBusinessCollateral by 1 into GcustBusinessCollateral
													  select new QQSumCustBusinessCollateral
											  {
														  SumBusinessCollateralValue = GcustBusinessCollateral.Sum(x => x.BusinessCollateralValue)
													  }
								   ).AsNoTracking().FirstOrDefault();
				return result;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QQNCBTrans> GetQQNCBTrans(Guid CustomerTableGUID)
		{
			try
			{
				int i = 1;
				int j = 0;
				List<QQNCBTrans> result = new List<QQNCBTrans>(new QQNCBTrans[30]);
				List<QQNCBTrans> tmp_result = (from ncbTrans in db.Set<NCBTrans>()
															 where ncbTrans.RefGUID == CustomerTableGUID
											   select new QQNCBTrans
															 {
																BankGroupGUID = ncbTrans.BankGroupGUID.GuidNullToString(),
																FinancialInstitution = (from bankGroup in db.Set<BankGroup>() where ncbTrans.BankGroupGUID == bankGroup.BankGroupGUID select bankGroup.Description).FirstOrDefault(),
																CreditType = (from creditType in db.Set<CreditType>() where ncbTrans.CreditTypeGUID == creditType.CreditTypeGUID select creditType.Description).FirstOrDefault(),
																CreditTypeGUID = ncbTrans.CreditTypeGUID.GuidNullToString(),
																OutstandingAR = ncbTrans.OutstandingAR,
																MonthlyRepayment = ncbTrans.MonthlyRepayment,
																EndDate = ncbTrans.EndDate.DateToString(),
																NCBAccountStatusGUID = ncbTrans.NCBAccountStatusGUID.GuidNullToString(),
																NCBAccountStatus = (from ncbAccountStatus in db.Set< NCBAccountStatus>() where ncbTrans.NCBAccountStatusGUID == ncbAccountStatus.NCBAccountStatusGUID select ncbAccountStatus.Description).FirstOrDefault()
											   }
					).Take(30).AsNoTracking().ToList();
				foreach (var item in tmp_result)
				{
					result[j] = item;
					j++;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private QQSumNCBTrans GetQQSumNCBTrans(Guid CustomerTableGUID)
		{
			try
			{
				QQSumNCBTrans result = (from ncbTrans in db.Set<NCBTrans>()

													  where ncbTrans.RefGUID == CustomerTableGUID
													  group ncbTrans by 1 into GncbTrans
										select new QQSumNCBTrans
													  {
															SumCreditLimit = GncbTrans.Sum(x => x.CreditLimit),
															SumOutstandingAR = GncbTrans.Sum(y => y.OutstandingAR),
															MonthlyRepayment = GncbTrans.Sum(z => z.MonthlyRepayment)
													  }
								   ).AsNoTracking().FirstOrDefault();
				return result;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QQProjectReferenceTrans> GetQQProjectReferenceTrans(Guid CustomerTableGUID)
		{
			try
			{
				int i = 1;
				int j = 0;
				List<QQProjectReferenceTrans> result = new List<QQProjectReferenceTrans>(new QQProjectReferenceTrans[10]);
				List<QQProjectReferenceTrans> tmp_result = (from projectReferenceTrans in db.Set<ProjectReferenceTrans>()
									where projectReferenceTrans.RefGUID == CustomerTableGUID
												  select new QQProjectReferenceTrans
									{
										  ProjectCompanyName = projectReferenceTrans.ProjectCompanyName,
										  ProjectName = projectReferenceTrans.ProjectName,
										  Remark = projectReferenceTrans.Remark,
										  ProjectStatus = projectReferenceTrans.ProjectStatus,
										  ProjectValue = projectReferenceTrans.ProjectValue,
										  ProjectCompletion = projectReferenceTrans.ProjectCompletion,
										  Row = i+1
												  }
								   ).OrderBy(x => x. ProjectCompanyName).Take(10).AsNoTracking().ToList();
				foreach (var item in tmp_result)
				{
					result[j] = item;
					j++;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private QCAReqBuyerCreditOutstandingPrivateSum GetCAReqBuyerCreditOutstandingPrivateSum(Guid? CreditAppRequestTableGUID)
		{
			try
			{
				int i = 1;
				int j = 0;
				QCAReqBuyerCreditOutstandingPrivateSum result = (from cAReqBuyerCreditOutstanding in db.Set<CAReqBuyerCreditOutstanding>()
																	   join buyerTable in db.Set<BuyerTable>() on cAReqBuyerCreditOutstanding.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
																	   from buyerTable in ljbuyerTable.DefaultIfEmpty()
																	   join businessSegment in db.Set<BusinessSegment>() on buyerTable.BusinessSegmentGUID equals businessSegment.BusinessSegmentGUID into ljbusinessSegment
																	   from businessSegment in ljbusinessSegment.DefaultIfEmpty()
																	   where cAReqBuyerCreditOutstanding.CreditAppRequestTableGUID == CreditAppRequestTableGUID && businessSegment.BusinessSegmentType == 1
																	group cAReqBuyerCreditOutstanding  by 1 into g
																	   select new QCAReqBuyerCreditOutstandingPrivateSum
																	   {
																		   SumARBalance = g.Sum(x => x.ARBalance),
																	   }
					).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QCAReqBuyerCreditOutstandingGovSum> GetCAReqBuyerCreditOutstandingGovSum(Guid? CreditAppRequestTableGUID)
		{
			try
			{
				int i = 1;
				int j = 0;
				List<QCAReqBuyerCreditOutstandingGovSum> result = new List<QCAReqBuyerCreditOutstandingGovSum>(new QCAReqBuyerCreditOutstandingGovSum[30]);
				List<QCAReqBuyerCreditOutstandingGovSum> tmp_result = (from cAReqBuyerCreditOutstanding in db.Set<CAReqBuyerCreditOutstanding>()
																	   join buyerTable in db.Set<BuyerTable>() on cAReqBuyerCreditOutstanding.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
																	   from buyerTable in ljbuyerTable.DefaultIfEmpty()
																	   join businessSegment in db.Set<BusinessSegment>() on buyerTable.BusinessSegmentGUID equals businessSegment.BusinessSegmentGUID into ljbusinessSegment
																	   from businessSegment in ljbusinessSegment.DefaultIfEmpty()
																	   where cAReqBuyerCreditOutstanding.CreditAppRequestTableGUID == CreditAppRequestTableGUID && businessSegment.BusinessSegmentType == 2
																	   group new
																	   {
																		   cAReqBuyerCreditOutstanding,
																		   buyerTable,
																		   businessSegment
																	   } 
																	   by new
																	   {
																		   cAReqBuyerCreditOutstanding.CAReqBuyerCreditOutstandingGUID,
																		   cAReqBuyerCreditOutstanding.ARBalance,
																		   buyerTable.BuyerTableGUID,
																		   buyerTable.BusinessSegmentGUID,
																		   businessSegment.Description,
																	   } 
																		into g
																	   select new QCAReqBuyerCreditOutstandingGovSum
																	   {
																		   SumARBalance = g.Sum(x => x.cAReqBuyerCreditOutstanding.ARBalance),
																		   BuyerTableGUID = g.Key.BuyerTableGUID.GuidNullOrEmptyToString(),
																		   BusinessSegmentGUID = g.Key.BusinessSegmentGUID.GuidNullOrEmptyToString(),
																		   BusinessSegmentType = g.Key.Description
																	   }
					).Take(10).AsNoTracking().ToList();
				foreach (var item in tmp_result)
				{
					result[j] = item;
					j++;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QAuthorizedPersonTransForNCB> GetQAuthorizedPersonTransForNCBs(Guid? CreditAppRequestTableGUID)
		{
			try
			{
				int i = 1;
				int j = 0;
				List<QAuthorizedPersonTransForNCB> result = new List<QAuthorizedPersonTransForNCB>(new QAuthorizedPersonTransForNCB[3]);
				List<QAuthorizedPersonTransForNCB> tmp_result = (from authorizedPersonTrans in db.Set<AuthorizedPersonTrans>()
																 join relatedPersonTable in db.Set<RelatedPersonTable>() on authorizedPersonTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID into ljrelatedPersonTable
																 from relatedPersonTable in ljrelatedPersonTable.DefaultIfEmpty()
																 where authorizedPersonTrans.RefGUID == CreditAppRequestTableGUID && authorizedPersonTrans.InActive == false
																 orderby authorizedPersonTrans.Ordering ascending
																 select new QAuthorizedPersonTransForNCB{
																	   AuthorizedPersonTransGUID = authorizedPersonTrans.AuthorizedPersonTransGUID.GuidNullToString(),
																	   AuthorizedPersonName = relatedPersonTable.Name,
																	   NCBCheckedDate = authorizedPersonTrans.NCBCheckedDate.DateNullToString(),
																}).Take(3).AsNoTracking().ToList();
				foreach (var item in tmp_result)
				{
					item.Row = j + 1;
					result[j] = item;
					j++;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QFinancialCreditTrans> GetQFinancialCreditTrans(Guid? CreditAppRequestTableGUID)
        {
			try
			{
				int i = 1;
				int j = 0;
				List<QFinancialCreditTrans> result = new List<QFinancialCreditTrans>(new QFinancialCreditTrans[8]);
				List<QFinancialCreditTrans> tmp_result = (from financialCreditTrans in db.Set<FinancialCreditTrans>()
														  join bankGroup in db.Set<BankGroup>() on financialCreditTrans.BankGroupGUID equals bankGroup.BankGroupGUID into ljbankGroup
														  from bankGroup in ljbankGroup.DefaultIfEmpty()

														  join creditType in db.Set<CreditType>() on financialCreditTrans.CreditTypeGUID equals creditType.CreditTypeGUID into ljcreditType
														  from creditType in ljcreditType.DefaultIfEmpty()
														  where financialCreditTrans.RefGUID == CreditAppRequestTableGUID
														  orderby financialCreditTrans.CreatedDateTime ascending
														  select new QFinancialCreditTrans
                                                          {
															  BankGroupGUID = financialCreditTrans.BankGroupGUID,
															  FinancialInstitutionDesc = bankGroup.Description,
															  CreditTypeGUID = creditType.CreditTypeGUID,
															  LoanType = creditType.Description,
															  Amount = financialCreditTrans.Amount,
															  Row = i+1
														  }
																 ).Take(8).AsNoTracking().ToList();
				foreach (var item in tmp_result)
				{
					item.Row = j + 1;
					result[j] = item;
					j++;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private QFinancialCreditTransSum GetQFinancialCreditTransSum(Guid CreditAppRequestTableGUID)
		{
			try
			{
				QFinancialCreditTransSum result = (from financialCreditTrans in db.Set<FinancialCreditTrans>()

										where financialCreditTrans.RefGUID == CreditAppRequestTableGUID
												   group financialCreditTrans by 1 into GfinancialCreditTrans
												   select new QFinancialCreditTransSum
										{
											TotalLoanLimit = GfinancialCreditTrans.Sum(o => o.Amount)
												   }
								   ).AsNoTracking().FirstOrDefault();
				return result;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private QCAReqCreditOutStandingAsOfDate getQCAReqCreditOutStandingAsOfDate(Guid CreditAppRequestTableGUID)
        {
			try
			{
				QCAReqCreditOutStandingAsOfDate result = (from cAReqCreditOutStanding in db.Set<CAReqCreditOutStanding>()

												   where cAReqCreditOutStanding.CreditAppRequestTableGUID == CreditAppRequestTableGUID
												   select new QCAReqCreditOutStandingAsOfDate
												   {
													   AsOfDate = cAReqCreditOutStanding.AsOfDate
												   }
								   ).AsNoTracking().FirstOrDefault();
				return result;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BookmarkDocumentCreditApplicationConsideration GetBookmarkDocumentCreditApplicationConsideration(Guid refGUID)
		{
			try
			{
				ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
				CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTrackingByAccessLevel(refGUID);
				
				QQCustomer qQCustomer = GetQQCustomer(creditAppRequestTable.CustomerTableGUID);
				QQBankAccountControl qQBankAccountControl = creditAppRequestTable.BankAccountControlGUID != null ? GetQQBankAccountControl((Guid)creditAppRequestTable.BankAccountControlGUID) : null;
				QQPDCBank qQPDCBank = creditAppRequestTable.PDCBankGUID != null ? GetQQPDCBank((Guid)creditAppRequestTable.PDCBankGUID) : null;
				//----------------------------------------------------------------------------------------------------------------
				List<QQAuthorizedPersonTrans> qQAuthorizedPersonTrans = GetQQAuthorizedPersonTrans(creditAppRequestTable.CreditAppRequestTableGUID , creditAppRequestTable.CreditAppRequestType);
				List<QQOwnerTrans> qQOwnerTrans = GetQQOwnerTrans(creditAppRequestTable.CreditAppRequestTableGUID, creditAppRequestTable.CreditAppRequestType);
				//----------------------------------------------------------------------------------------------------------------
				List<QAuthorizedPersonTransForNCB> qAuthorizedPersonTransForNCBs = GetQAuthorizedPersonTransForNCBs(creditAppRequestTable.CreditAppRequestTableGUID);
				List<QQAuthorizedNCB1> qQAuthorizedNCB1s = qQAuthorizedPersonTrans[0] != null ? GetQQAuthorizedNCB1s(qQAuthorizedPersonTrans[0].AuthorizedPersonTransGUID.StringToGuid()) : null;
				QQSumAuthorizedNCB1 qQSumAuthorizedNCB1 = qAuthorizedPersonTransForNCBs[0] != null ? GetQQSumAuthorizedNCB1(qAuthorizedPersonTransForNCBs[0].AuthorizedPersonTransGUID.StringToGuid()) : null;
				List<QQAuthorizedNCB2> qQAuthorizedNCB2s = qQAuthorizedPersonTrans[1] != null ? GetQQAuthorizedNCB2s(qQAuthorizedPersonTrans[1].AuthorizedPersonTransGUID.StringToGuid()) : null;
				QQSumAuthorizedNCB2 qQSumAuthorizedNCB2 = qAuthorizedPersonTransForNCBs[1] != null ? GetQQSumAuthorizedNCB2(qAuthorizedPersonTransForNCBs[1].AuthorizedPersonTransGUID.StringToGuid()) : null;
				List<QQAuthorizedNCB3> qQAuthorizedNCB3s = qQAuthorizedPersonTrans[2] != null ? GetQQAuthorizedNCB3s(qQAuthorizedPersonTrans[2].AuthorizedPersonTransGUID.StringToGuid()) : null;
				QQSumAuthorizedNCB3 qQSumAuthorizedNCB3 = qAuthorizedPersonTransForNCBs[2] != null ? GetQQSumAuthorizedNCB3(qAuthorizedPersonTransForNCBs[2].AuthorizedPersonTransGUID.StringToGuid()) : null;
				
				List<QQCreditAppReqLineFin> qQCreditAppReqLineFins = GetQQCreditAppReqLineFins(refGUID);
				List<QTaxReportTrans> qTaxReportTrans = GetQTaxReportTrans(creditAppRequestTable.CreditAppRequestTableGUID);
				QTaxReportStart qTaxReportStart = GetQTaxReportStart(creditAppRequestTable.CreditAppRequestTableGUID);
				QTaxReportEnd qTaxReportEnd = GetQTaxReportEnd(creditAppRequestTable.CreditAppRequestTableGUID);
				List<QTaxReportTransSum> qTaxReportTransSum = GetQTaxReportTransSum(creditAppRequestTable.CreditAppRequestTableGUID);
				List<QCAReqCreditOutStanding> qCAReqCreditOutStanding = GetCAReqCreditOutStanding2(creditAppRequestTable.CreditAppRequestTableGUID);
				QCAReqCreditOutStandingSum qCAReqCreditOutStandingSum = GetReqCreditOutStandingSum(creditAppRequestTable.CreditAppRequestTableGUID);
				List<QQCAReqNCBTrans> qQCAReqNCBTrans = GetQQCAReqNCBTrans(creditAppRequestTable.CreditAppRequestTableGUID);
				QCAReqNCBTransSum qQCAReqNCBTransSum = GetCAReqNCBTransSum2(creditAppRequestTable.CreditAppRequestTableGUID);
				List<QQCAReqBuyerCreditOutstandingPrivate> qQCAReqBuyerCreditOutstandingGovs = GetQCAReqBuyerCreditOutstandingPrivates(creditAppRequestTable.CreditAppRequestTableGUID);
				List<QQCAReqBuyerCreditOutstandingGov> qQCAReqBuyerCreditOutstandingGovs1 = GetQQCAReqBuyerCreditOutstandingGovs(creditAppRequestTable.CreditAppRequestTableGUID);
				List<QQCreditAppReqAssignment> qQCreditAppReqAssignments = GetQQCreditAppReqAssignments(creditAppRequestTable.CustomerTableGUID);
				List<QQCustBusinessCollateral> qQCustBusinessCollaterals = GetQCustBusinessCollaterals(creditAppRequestTable.CustomerTableGUID);
				QQSumCustBusinessCollateral qQSumCustBusinessCollateral = GetQQSumCustBusinessCollateral(creditAppRequestTable.CustomerTableGUID);
				List<QQNCBTrans> qQNCBTrans = GetQQNCBTrans(creditAppRequestTable.CustomerTableGUID);
				QQSumNCBTrans qQSumNCBTrans = GetQQSumNCBTrans(creditAppRequestTable.CustomerTableGUID);
				List<QQProjectReferenceTrans> qQProjectReferenceTrans = GetQQProjectReferenceTrans(creditAppRequestTable.CustomerTableGUID);
				QSumCreditAppReqAssignment qSumCreditAppReqAssignment = GetSumCreditAppReqAssignment(creditAppRequestTable.CreditAppRequestTableGUID);
				QCAReqBuyerCreditOutstandingPrivateSum qCAReqBuyerCreditOutstandingPrivateSum = GetCAReqBuyerCreditOutstandingPrivateSum(creditAppRequestTable.CreditAppRequestTableGUID);
				List<QCAReqBuyerCreditOutstandingGovSum> qCAReqBuyerCreditOutstandingGovSum = GetCAReqBuyerCreditOutstandingGovSum(creditAppRequestTable.CreditAppRequestTableGUID);
				List<QFinancialCreditTrans> qFinancialCreditTrans = GetQFinancialCreditTrans(creditAppRequestTable.CreditAppRequestTableGUID);
				QFinancialCreditTransSum qFinancialCreditTransSum = GetQFinancialCreditTransSum(creditAppRequestTable.CreditAppRequestTableGUID);
				List<QCreditAppReqAssignmentOutstanding> qCreditAppReqAssignmentOutstanding = GetQCreditAppReqAssignmentOutstandings(creditAppRequestTable.CreditAppRequestTableGUID);
				QSumCreditAppReqAssignmentOutstanding qSumCreditAppReqAssignmentOutstanding = getQSumCreditAppReqAssignmentOutstanding(creditAppRequestTable.CreditAppRequestTableGUID);
				QCAReqBuyerCreditOutstandingPrivateCOUNT qCAReqBuyerCreditOutstandingPrivateCOUNT = getQCAReqBuyerCreditOutstandingPrivateCOUNT(creditAppRequestTable.CreditAppRequestTableGUID);
				QCAReqBuyerCreditOutstandingGovCOUNT qCAReqBuyerCreditOutstandingGovCOUNT =  getQCAReqBuyerCreditOutstandingGovCOUNT(creditAppRequestTable.CreditAppRequestTableGUID);
				QCAReqCreditOutStandingAsOfDate qCAReqCreditOutStandingAsOfDate = getQCAReqCreditOutStandingAsOfDate(creditAppRequestTable.CreditAppRequestTableGUID);

				#region QCreditAppRequestTable 
				IInterestTypeValueRepo interestTypeValueRepo = new InterestTypeValueRepo(db);
				QQCreditAppRequestTable qQCreditAppRequestTable = (from tmp_creditAppRequestTable in db.Set<CreditAppRequestTable>()
																   where tmp_creditAppRequestTable.CreditAppRequestTableGUID == refGUID
																   select new QQCreditAppRequestTable()
																   {
																	   CreditAppRequestId = tmp_creditAppRequestTable.CreditAppRequestId,
																	   RefCreditAppTableGUID = tmp_creditAppRequestTable.RefCreditAppTableGUID.GuidNullToString(),
																	   OriginalCreditAppTableId = (from creditAppTable in db.Set<CreditAppTable>()
																								   where tmp_creditAppRequestTable.RefCreditAppTableGUID == creditAppTable.CreditAppTableGUID
																								   select creditAppTable.CreditAppId
																								 ).AsNoTracking().FirstOrDefault(),
																	   OriginalPurchaseWithdrawalCondition = (from creditAppTable in db.Set<CreditAppTable>()
																											  where tmp_creditAppRequestTable.RefCreditAppTableGUID == creditAppTable.CreditAppTableGUID
																											  select creditAppTable.PurchaseWithdrawalCondition
																								 ).AsNoTracking().FirstOrDefault(),
																	   OriginalAssignmentMethodGUID = (from creditAppTable in db.Set<CreditAppTable>()
																									   where tmp_creditAppRequestTable.RefCreditAppTableGUID == creditAppTable.CreditAppTableGUID
																									   select creditAppTable.AssignmentMethodGUID
																								 ).FirstOrDefault().GuidNullToString(),
																	   OriginalAssignmentMethodTable = (from assignmentMethod in db.Set<AssignmentMethod>()
																										where (from creditAppTable in db.Set<CreditAppTable>()
																											   where tmp_creditAppRequestTable.RefCreditAppTableGUID == creditAppTable.CreditAppTableGUID
																											   select creditAppTable.AssignmentMethodGUID
																								   ).FirstOrDefault() == assignmentMethod.AssignmentMethodGUID
																										select assignmentMethod.Description
																								 ).AsNoTracking().FirstOrDefault(),
																	   DocumentStatusGUID = tmp_creditAppRequestTable.DocumentStatusGUID.GuidNullToString(),
																	   Status = (from documentStatus in db.Set<DocumentStatus>()
																				 where tmp_creditAppRequestTable.DocumentStatusGUID == documentStatus.DocumentStatusGUID
																				 select documentStatus.Description).AsNoTracking().FirstOrDefault(),
																	   RequestDate = tmp_creditAppRequestTable.RequestDate.DateToString(),
																	   KYCSetupGUID = tmp_creditAppRequestTable.KYCGUID.GuidNullToString(),
																	   KYC = (from kYCSetup in db.Set<KYCSetup>()
																			  where kYCSetup.KYCSetupGUID == (Guid)tmp_creditAppRequestTable.KYCGUID
																			  select kYCSetup.Description
																			).AsNoTracking().FirstOrDefault(),
																	   CreditScoringGUID = tmp_creditAppRequestTable.CreditScoringGUID.GuidNullToString(),
																	   CreditScoring = (from creditScoring in db.Set<CreditScoring>()
																						where creditScoring.CreditScoringGUID == tmp_creditAppRequestTable.CreditScoringGUID
																						select creditScoring.Description
																			).AsNoTracking().FirstOrDefault(),
																	   Description = tmp_creditAppRequestTable.Description,
																	   DocumentReasonGUID = tmp_creditAppRequestTable.DocumentReasonGUID.GuidNullToString(),
																	   Purpose = (from documentReason in db.Set<DocumentReason>()
																				  where documentReason.DocumentReasonGUID == tmp_creditAppRequestTable.DocumentReasonGUID
																				  select documentReason.Description
																			).AsNoTracking().FirstOrDefault(),
																	   Remark = tmp_creditAppRequestTable.Remark,
																	   RegisteredAddressGUID = tmp_creditAppRequestTable.RegisteredAddressGUID.GuidNullToString(),
																	   RegisteredAddress = (from addressTrans in db.Set<AddressTrans>()
																							where addressTrans.AddressTransGUID == tmp_creditAppRequestTable.RegisteredAddressGUID
																							select addressTrans.Address1 +" "+ addressTrans.Address2
																			).AsNoTracking().FirstOrDefault(),
																	   BusinessTypeGUID = qQCustomer.BusinessTypeGUID,
																	   BusinessType = (from businessType in db.Set<BusinessType>()
																					   where businessType.BusinessTypeGUID == qQCustomer.BusinessTypeGUID.StringToGuid()
																					   select businessType.Description
																			).AsNoTracking().FirstOrDefault(),
																	   IntroducedByGUID = qQCustomer.IntroducedByGUID,
																	   IntroducedBy = (from introducedBy in db.Set<IntroducedBy>()
																					   where introducedBy.IntroducedByGUID == qQCustomer.IntroducedByGUID.StringToGuid()
																					   select introducedBy.Description
																			).AsNoTracking().FirstOrDefault(),
																	   //ProductType = SystemStaticData.GetTranslatedMessage(((ProductType)tmp_creditAppRequestTable.ProductType).GetAttrCode()),
																	   ProductType = tmp_creditAppRequestTable.ProductType,
																	   ProductSubType = (from productSubType in db.Set<ProductSubType>()
																						 where productSubType.ProductSubTypeGUID == tmp_creditAppRequestTable.ProductSubTypeGUID
																						 select productSubType.Description
																			).AsNoTracking().FirstOrDefault(),
																	   CreditLimitTypeGUID = tmp_creditAppRequestTable.CreditLimitTypeGUID.GuidNullToString(),
																	   CreditLimitType = (from creditLimitType in db.Set<CreditLimitType>()
																						  where creditLimitType.CreditLimitTypeGUID == tmp_creditAppRequestTable.CreditLimitTypeGUID
																						  select creditLimitType.Description
																			).AsNoTracking().FirstOrDefault(),
																	   //CreditLimitExpiration = SystemStaticData.GetTranslatedMessage(((CreditLimitExpiration)tmp_creditAppRequestTable.CreditLimitExpiration).GetAttrCode()),
																	   CreditLimitExpiration = tmp_creditAppRequestTable.CreditLimitExpiration,
																	   StartDate = tmp_creditAppRequestTable.StartDate.DateNullToString(),
																	   ExpiryDate = tmp_creditAppRequestTable.ExpiryDate.DateNullToString(),
																	   CreditLimitRequest = tmp_creditAppRequestTable.CreditLimitRequest,
																	   CustomerCreditLimit = tmp_creditAppRequestTable.CustomerCreditLimit,
																	   InterestTypeGUID = tmp_creditAppRequestTable.InterestTypeGUID.GuidNullToString(),
																	   InterestType = (from interestType in db.Set<InterestType>()
																					   where interestType.InterestTypeGUID == tmp_creditAppRequestTable.InterestTypeGUID
																					   select interestType.Description
																			).AsNoTracking().FirstOrDefault(),
																	   InterestAdjustment = tmp_creditAppRequestTable.InterestAdjustment,
																	   CreditRequestFeePct = tmp_creditAppRequestTable.CreditRequestFeePct,
																	   CreditRequestFeeAmount = tmp_creditAppRequestTable.CreditRequestFeeAmount,
																	   PurchaseFeePct = tmp_creditAppRequestTable.PurchaseFeePct,
																	   //PurchaseFeeCalculateBase = SystemStaticData.GetTranslatedMessage(((PurchaseFeeCalculateBase)tmp_creditAppRequestTable.PurchaseFeeCalculateBase).GetAttrCode()),
																	   PurchaseFeeCalculateBase = tmp_creditAppRequestTable.PurchaseFeeCalculateBase,
																	   NCBCheckedDate = tmp_creditAppRequestTable.NCBCheckedDate.DateNullToString(),
																	   MaxPurchasePct = tmp_creditAppRequestTable.MaxPurchasePct,
																	   MaxRetentionPct = tmp_creditAppRequestTable.MaxRetentionPct,
																	   MaxRetentionAmount = tmp_creditAppRequestTable.MaxRetentionAmount,
																	   InterestValue = "",
																	   FinancialCreditCheckedDate = tmp_creditAppRequestTable.FinancialCreditCheckedDate.DateNullToString(),
																	   SalesAvgPerMonth = tmp_creditAppRequestTable.SalesAvgPerMonth,
																	   PurchaseWithdrawalCondition = tmp_creditAppRequestTable.PurchaseWithdrawalCondition,
																	   AssignmentMethodGUID = tmp_creditAppRequestTable.AssignmentMethodGUID.GuidNullToString(),
																	   AssignmentMethodTable = (from assignmentMethod in db.Set<AssignmentMethod>()
																								where tmp_creditAppRequestTable.AssignmentMethodGUID == assignmentMethod.AssignmentMethodGUID
																								select assignmentMethod.Description
																								 ).AsNoTracking().FirstOrDefault(),
																   }
																   ).AsNoTracking().FirstOrDefault();
				#endregion
				IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService(db);
				BookmarkDocumentCreditApplicationConsideration_Variable variable = new BookmarkDocumentCreditApplicationConsideration_Variable()
				{
					CustCompanyRegistrationID = qQCustomer.IdentificationType == (decimal)IdentificationType.TaxId ? qQCustomer.TaxID : qQCustomer.IdentificationType == (decimal)IdentificationType.PassportId ? qQCustomer.PassportID : "-",
					CustComEstablishedDate = qQCustomer.RecordType == (decimal)RecordType.Person ? qQCustomer.DateOfBirth : qQCustomer.RecordType == (decimal)RecordType.Organization ? qQCustomer.DateOfEstablish : "-",

					AuthorizedPersonTrans1Age = qQAuthorizedPersonTrans[0] != null ? relatedPersonTableService.CalcAge(qQAuthorizedPersonTrans[0].DateOfBirth).ToString() : "0",
					AuthorizedPersonTrans2Age = qQAuthorizedPersonTrans[1] != null ? relatedPersonTableService.CalcAge(qQAuthorizedPersonTrans[1].DateOfBirth).ToString() : "0",
					AuthorizedPersonTrans3Age = qQAuthorizedPersonTrans[2] != null ? relatedPersonTableService.CalcAge(qQAuthorizedPersonTrans[2].DateOfBirth).ToString() : "0",
					AuthorizedPersonTrans4Age = qQAuthorizedPersonTrans[3] != null ? relatedPersonTableService.CalcAge(qQAuthorizedPersonTrans[3].DateOfBirth).ToString() : "0",
					AuthorizedPersonTrans5Age = qQAuthorizedPersonTrans[4] != null ? relatedPersonTableService.CalcAge(qQAuthorizedPersonTrans[4].DateOfBirth).ToString() : "0",
					AuthorizedPersonTrans6Age = qQAuthorizedPersonTrans[5] != null ? relatedPersonTableService.CalcAge(qQAuthorizedPersonTrans[5].DateOfBirth).ToString() : "0",
					AuthorizedPersonTrans7Age = qQAuthorizedPersonTrans[6] != null ? relatedPersonTableService.CalcAge(qQAuthorizedPersonTrans[6].DateOfBirth).ToString() : "0",
					AuthorizedPersonTrans8Age = qQAuthorizedPersonTrans[7] != null ? relatedPersonTableService.CalcAge(qQAuthorizedPersonTrans[7].DateOfBirth).ToString() : "0",
					//-----------------------------------------------------------------------------------------------------------------------------------------------------------
					InterestValue = interestTypeValueRepo.GetByInterestTypeDateNoTracking(qQCreditAppRequestTable.InterestTypeGUID.StringToGuid(), qQCreditAppRequestTable.RequestDate.StringToDate(), false).Value,
					NewInterestRate = qQCreditAppRequestTable == null ? qQCreditAppRequestTable.InterestAdjustment : interestTypeValueRepo.GetByInterestTypeDateNoTracking(qQCreditAppRequestTable.InterestTypeGUID.StringToGuid(), qQCreditAppRequestTable.RequestDate.StringToDate(), false).Value + qQCreditAppRequestTable.InterestAdjustment,
					//NewInterestRate = qQCreditAppRequestTable.InterestType == null ? qQCreditAppRequestTable.InterestAdjustment : interestTypeValueRepo.GetByInterestTypeDateNoTracking(qQCreditAppRequestTable.InterestTypeGUID.StringToGuid(), qQCreditAppRequestTable.RequestDate.StringToDate(), false).Value + qQCreditAppRequestTable.InterestAdjustment,
					AverageOutputVATPerMonth = qTaxReportTransSum != null ? (qTaxReportTransSum[0].TaxReportCOUNT != 0 ? qTaxReportTransSum[0].SumTaxReport / qTaxReportTransSum[0].TaxReportCOUNT : 0) : 0,
					NewCreditLimitAmt = qQCreditAppRequestTable != null ? qQCreditAppRequestTable.CreditLimitRequest : 0, //QOriginalCreditAppRequest ไม่มี query
					FactoringPurchaseCondition = qQCreditAppRequestTable != null ? ( qQCreditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.MainCreditLimit || qQCreditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.BuyerMatching || qQCreditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.LoanRequest ) ? qQCreditAppRequestTable.PurchaseWithdrawalCondition : qQCreditAppRequestTable.OriginalPurchaseWithdrawalCondition : "",
					AssignmentCondition = qQCreditAppRequestTable != null ? (qQCreditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.MainCreditLimit || qQCreditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.BuyerMatching || qQCreditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.LoanRequest) ? qQCreditAppRequestTable.AssignmentMethodTable : qQCreditAppRequestTable.OriginalAssignmentMethodTable : "",
					////-------------------------------------------------------------------------------------------------------------------------------------------------------------
				};

				BookmarkDocumentCreditApplicationConsideration bookmarkDocumentCreditApplicationConsideration = new BookmarkDocumentCreditApplicationConsideration()
				{
					QCreditAppRequestTable_CreditAppRequestId = qQCreditAppRequestTable != null ? qQCreditAppRequestTable.CreditAppRequestId : "",
					QCreditAppRequestTable_OriginalCreditAppTableId = qQCreditAppRequestTable != null ? (qQCreditAppRequestTable.OriginalCreditAppTableId == null ? "" : qQCreditAppRequestTable.OriginalCreditAppTableId) : "",
					QCreditAppRequestTable_Status = qQCreditAppRequestTable != null ? qQCreditAppRequestTable.Status : "",
					QCreditAppRequestTable_RequestDate = qQCreditAppRequestTable != null ? qQCreditAppRequestTable.RequestDate : "",
					QCreditAppRequestTable_CustomerName = qQCustomer != null ? qQCustomer.CustomerName : "",
					QCreditAppRequestTable_CustomerId = qQCustomer != null ? qQCustomer.CustomerId : "",
					QCreditAppRequestTable_ResponsibleBy = qQCustomer != null ? qQCustomer.ResponsibleBy : "",
					QCreditAppRequestTable_KYC = qQCreditAppRequestTable != null ? qQCreditAppRequestTable.KYC : "",
					QCreditAppRequestTable_CreditScoring = qQCreditAppRequestTable != null ? qQCreditAppRequestTable.CreditScoring : "",
					QCreditAppRequestTable_Description = qQCreditAppRequestTable != null ? qQCreditAppRequestTable.Description : "",
					QCreditAppRequestTable_Purpose = qQCreditAppRequestTable != null ? qQCreditAppRequestTable.Purpose : "",
					QCreditAppRequestTable_Remark = qQCreditAppRequestTable != null ? qQCreditAppRequestTable.Remark : "",
					Variable_CustCompanyRegistrationID = variable.CustCompanyRegistrationID != null ? variable.CustCompanyRegistrationID : "",
					QCreditAppRequestTable_RegisteredAddress = qQCreditAppRequestTable != null ? qQCreditAppRequestTable.RegisteredAddress : "",
					Variable_CustComEstablishedDate = variable.CustComEstablishedDate != null ? variable.CustComEstablishedDate : "",
					QCreditAppRequestTable_BusinessType = qQCreditAppRequestTable != null ? qQCreditAppRequestTable.BusinessType : "",
					QCreditAppRequestTable_IntroducedBy = qQCreditAppRequestTable != null ? qQCreditAppRequestTable.IntroducedBy : "",
					QCreditAppRequestTable_Instituetion = qQBankAccountControl != null ? qQBankAccountControl.Instituetion : "",
					QCreditAppRequestTable_BankBranch = qQBankAccountControl != null ? qQBankAccountControl.BankBranch : "",
					QCreditAppRequestTable_BankType = qQBankAccountControl != null ? qQBankAccountControl.AccountType : "",
					QCreditAppRequestTable_BankAccountName = qQBankAccountControl != null ? qQBankAccountControl.AccountNumber : "",

					Variable_AuthorizedPersonTrans1Age = variable.AuthorizedPersonTrans1Age != null ? variable.AuthorizedPersonTrans1Age : "",
					Variable_AuthorizedPersonTrans2Age = variable.AuthorizedPersonTrans2Age != null ? variable.AuthorizedPersonTrans2Age : "",
					AuthorizedPersonTrans3Age = variable.AuthorizedPersonTrans3Age != null ? variable.AuthorizedPersonTrans3Age : "",
					AuthorizedPersonTrans4Age = variable.AuthorizedPersonTrans4Age != null ? variable.AuthorizedPersonTrans4Age : "",
					AuthorizedPersonTrans5Age = variable.AuthorizedPersonTrans5Age != null ? variable.AuthorizedPersonTrans5Age : "",
					AuthorizedPersonTrans6Age = variable.AuthorizedPersonTrans6Age != null ? variable.AuthorizedPersonTrans6Age : "",
					AuthorizedPersonTrans7Age = variable.AuthorizedPersonTrans7Age != null ? variable.AuthorizedPersonTrans7Age : "",
					AuthorizedPersonTrans8Age = variable.AuthorizedPersonTrans8Age != null ? variable.AuthorizedPersonTrans8Age : "",

					QAuthorizedPersonTrans1_AuthorizedPersonName = qQAuthorizedPersonTrans[0] != null ? qQAuthorizedPersonTrans[0].AuthorizedPersonName : "",
					QAuthorizedPersonTrans2_AuthorizedPersonName = qQAuthorizedPersonTrans[1] != null ? qQAuthorizedPersonTrans[1].AuthorizedPersonName : "",
					QAuthorizedPersonTrans3_AuthorizedPersonName = qQAuthorizedPersonTrans[2] != null ? qQAuthorizedPersonTrans[2].AuthorizedPersonName : "",
					QAuthorizedPersonTrans4_AuthorizedPersonName = qQAuthorizedPersonTrans[3] != null ? qQAuthorizedPersonTrans[3].AuthorizedPersonName : "",
					QAuthorizedPersonTrans5_AuthorizedPersonName = qQAuthorizedPersonTrans[4] != null ? qQAuthorizedPersonTrans[4].AuthorizedPersonName : "",
					QAuthorizedPersonTrans6_AuthorizedPersonName = qQAuthorizedPersonTrans[5] != null ? qQAuthorizedPersonTrans[5].AuthorizedPersonName : "",
					QAuthorizedPersonTrans7_AuthorizedPersonName = qQAuthorizedPersonTrans[6] != null ? qQAuthorizedPersonTrans[6].AuthorizedPersonName : "",
					QAuthorizedPersonTrans8_AuthorizedPersonName = qQAuthorizedPersonTrans[7] != null ? qQAuthorizedPersonTrans[7].AuthorizedPersonName : "",
					QOwnerTrans1_ShareHolderPersonName = qQOwnerTrans[0] != null ? qQOwnerTrans[0].ShareHolderPersonName : "",
					QOwnerTrans1_PropotionOfShareholderPct = qQOwnerTrans[0] != null ? qQOwnerTrans[0].PropotionOfShareholderPct.ToString() : "0",
					QOwnerTrans2_ShareHolderPersonName = qQOwnerTrans[1] != null ? qQOwnerTrans[1].ShareHolderPersonName : "",
					QOwnerTrans2_PropotionOfShareholderPct = qQOwnerTrans[1] != null ? qQOwnerTrans[1].PropotionOfShareholderPct.ToString() : "0",
					QOwnerTrans3_ShareHolderPersonName = qQOwnerTrans[2] != null ? qQOwnerTrans[2].ShareHolderPersonName : "",
					QOwnerTrans3_PropotionOfShareholderPct = qQOwnerTrans[2] != null ? qQOwnerTrans[2].PropotionOfShareholderPct.ToString() : "0",
					QOwnerTrans4_ShareHolderPersonName = qQOwnerTrans[3] != null ? qQOwnerTrans[3].ShareHolderPersonName : "",
					QOwnerTrans4_PropotionOfShareholderPct = qQOwnerTrans[3] != null ? qQOwnerTrans[3].PropotionOfShareholderPct.ToString() : "0",
					QOwnerTrans5_ShareHolderPersonName = qQOwnerTrans[4] != null ? qQOwnerTrans[4].ShareHolderPersonName : "",
					QOwnerTrans5_PropotionOfShareholderPct = qQOwnerTrans[4] != null ? qQOwnerTrans[4].PropotionOfShareholderPct.ToString() : "0",
					QOwnerTrans6_ShareHolderPersonName = qQOwnerTrans[5] != null ? qQOwnerTrans[5].ShareHolderPersonName : "",
					QOwnerTrans6_PropotionOfShareholderPct = qQOwnerTrans[5] != null ? qQOwnerTrans[5].PropotionOfShareholderPct.ToString() : "0",
					QOwnerTrans7_ShareHolderPersonName = qQOwnerTrans[6] != null ? qQOwnerTrans[6].ShareHolderPersonName : "",
					QOwnerTrans7_PropotionOfShareholderPct = qQOwnerTrans[6] != null ? qQOwnerTrans[6].PropotionOfShareholderPct.ToString() : "0",
					QOwnerTrans8_ShareHolderPersonName = qQOwnerTrans[7] != null ? qQOwnerTrans[7].ShareHolderPersonName : "",
					QOwnerTrans8_PropotionOfShareholderPct = qQOwnerTrans[7] != null ? qQOwnerTrans[7].PropotionOfShareholderPct.ToString() : "0",
					QCreditAppRequestTable_ProductType = qQCreditAppRequestTable != null ? SystemStaticData.GetTranslatedMessage(((ProductType)qQCreditAppRequestTable.ProductType).GetAttrCode()) : "",
					QCreditAppRequestTable_ProductSubType = qQCreditAppRequestTable != null ? qQCreditAppRequestTable.ProductSubType : "",
					QCreditAppRequestTable_CreditLimitType = qQCreditAppRequestTable != null ? qQCreditAppRequestTable.CreditLimitType : "",
					QCreditAppRequestTable_CreditLimitExpiration = qQCreditAppRequestTable != null ? SystemStaticData.GetTranslatedMessage(((CreditLimitExpiration)qQCreditAppRequestTable.CreditLimitExpiration).GetAttrCode()) : "",
					QCreditAppRequestTable_StartDate = qQCreditAppRequestTable.StartDate != null ? qQCreditAppRequestTable.StartDate : "",
					QCreditAppRequestTable_ExpiryDate = qQCreditAppRequestTable.ExpiryDate != null ? qQCreditAppRequestTable.ExpiryDate : "",
					QCreditAppRequestTable_CreditLimitRequest = qQCreditAppRequestTable != null ? qQCreditAppRequestTable.CreditLimitRequest : 0,
					QCreditAppRequestTable_CustomerCreditLimit = qQCreditAppRequestTable != null ? qQCreditAppRequestTable.CustomerCreditLimit : 0,
					QCreditAppRequestTable_InterestType = qQCreditAppRequestTable != null ? qQCreditAppRequestTable.InterestType : "",
					QCreditAppRequestTable_InterestAdjustment = qQCreditAppRequestTable != null ? qQCreditAppRequestTable.InterestAdjustment : 0,
					NewInterestRate = variable.NewInterestRate,
					QCreditAppRequestTable_MaxPurchasePct = qQCreditAppRequestTable != null ? qQCreditAppRequestTable.MaxPurchasePct : 0,
					QCreditAppRequestTable_MaxRetentionPct = qQCreditAppRequestTable != null ? qQCreditAppRequestTable.MaxRetentionPct : 0,
					QCreditAppRequestTable_MaxRetentionAmount = qQCreditAppRequestTable != null ? qQCreditAppRequestTable.MaxRetentionAmount : 0,
					QCreditAppRequestTable_CustPDCBankName = qQPDCBank != null ? qQPDCBank.CustPDCBankName : "",
					QCreditAppRequestTable_CustPDCBankBranch = qQPDCBank != null ? qQPDCBank.CustPDCBankBranch : "",
					QCreditAppRequestTable_CustPDCBankType = qQPDCBank != null ? qQPDCBank.CustPDCBankType : "",
					QCreditAppRequestTable_CustPDCBankAccountName = qQPDCBank != null ? qQPDCBank.CustPDCBankAccountName : "",
					QCreditAppRequestTable_CreditRequestFeePct = qQCreditAppRequestTable != null ? qQCreditAppRequestTable.CreditRequestFeePct : 0,
					QCreditAppRequestTable_CreditRequestFeeAmount = qQCreditAppRequestTable != null ? qQCreditAppRequestTable.CreditRequestFeeAmount : 0,
					QCreditAppRequestTable_PurchaseFeePct = qQCreditAppRequestTable != null ? qQCreditAppRequestTable.PurchaseFeePct : 0,
					QCreditAppRequestTable_PurchaseFeeCalculateBase = qQCreditAppRequestTable != null ? SystemStaticData.GetTranslatedMessage(((PurchaseFeeCalculateBase)qQCreditAppRequestTable.PurchaseFeeCalculateBase).GetAttrCode()) : "-",
					QCreditAppReqLineFin1_Year = qQCreditAppReqLineFins[0] != null ? qQCreditAppReqLineFins[0].Year : 0,
					QCreditAppReqLineFin1_RegisteredCapital = qQCreditAppReqLineFins[0] != null ? qQCreditAppReqLineFins[0].RegisteredCapital : 0,
					QCreditAppReqLineFin1_PaidCapital = qQCreditAppReqLineFins[0] != null ? qQCreditAppReqLineFins[0].PaidCapital : 0,
					QCreditAppReqLineFin1_TotalAsset = qQCreditAppReqLineFins[0] != null ? qQCreditAppReqLineFins[0].TotalAsset : 0,
					QCreditAppReqLineFin1_TotalLiability = qQCreditAppReqLineFins[0] != null ? qQCreditAppReqLineFins[0].TotalLiability : 0,
					QCreditAppReqLineFin1_TotalEquity = qQCreditAppReqLineFins[0] != null ? qQCreditAppReqLineFins[0].TotalEquity : 0,
					AppReqLineFin1_Year = qQCreditAppReqLineFins[0] != null ? qQCreditAppReqLineFins[0].Year : 0,
					QCreditAppReqLineFin1_TotalRevenue = qQCreditAppReqLineFins[0] != null ? qQCreditAppReqLineFins[0].TotalRevenue : 0,
					QCreditAppReqLineFin1_TotalCOGS = qQCreditAppReqLineFins[0] != null ? qQCreditAppReqLineFins[0].TotalCOGS : 0,
					QCreditAppReqLineFin1_TotalGrossProfit = qQCreditAppReqLineFins[0] != null ? qQCreditAppReqLineFins[0].TotalGrossProfit : 0,
					QCreditAppReqLineFin1_TotalOperExpFirst = qQCreditAppReqLineFins[0] != null ? qQCreditAppReqLineFins[0].TotalOperExpFirst : 0,
					QCreditAppReqLineFin1_TotalNetProfitFirst = qQCreditAppReqLineFins[0] != null ? qQCreditAppReqLineFins[0].TotalNetProfitFirst : 0,
					QCreditAppReqLineFin2_Year = qQCreditAppReqLineFins[1] != null ? qQCreditAppReqLineFins[1].Year : 0,
					QCreditAppReqLineFin2_RegisteredCapital = qQCreditAppReqLineFins[1] != null ? qQCreditAppReqLineFins[1].RegisteredCapital : 0,
					QCreditAppReqLineFin2_PaidCapital = qQCreditAppReqLineFins[1] != null ? qQCreditAppReqLineFins[1].PaidCapital : 0,
					QCreditAppReqLineFin2_TotalAsset = qQCreditAppReqLineFins[1] != null ? qQCreditAppReqLineFins[1].TotalAsset : 0,
					QCreditAppReqLineFin2_TotalLiability = qQCreditAppReqLineFins[1] != null ? qQCreditAppReqLineFins[1].TotalLiability : 0,
					QCreditAppReqLineFin2_TotalEquity = qQCreditAppReqLineFins[1] != null ? qQCreditAppReqLineFins[1].TotalEquity : 0,
					AppReqLineFin2_Year = qQCreditAppReqLineFins[1] != null ? qQCreditAppReqLineFins[1].Year : 0,
					QCreditAppReqLineFin2_TotalRevenue = qQCreditAppReqLineFins[1] != null ? qQCreditAppReqLineFins[1].TotalRevenue : 0,
					QCreditAppReqLineFin2_TotalCOGS = qQCreditAppReqLineFins[1] != null ? qQCreditAppReqLineFins[1].TotalCOGS : 0,
					QCreditAppReqLineFin2_TotalGrossProfit = qQCreditAppReqLineFins[1] != null ? qQCreditAppReqLineFins[1].TotalGrossProfit : 0,
					QCreditAppReqLineFin2_TotalOperExpFirst = qQCreditAppReqLineFins[1] != null ? qQCreditAppReqLineFins[1].TotalOperExpFirst : 0,
					QCreditAppReqLineFin2_TotalNetProfitFirst = qQCreditAppReqLineFins[1] != null ? qQCreditAppReqLineFins[1].TotalNetProfitFirst : 0,
					QCreditAppReqLineFin3_Year = qQCreditAppReqLineFins[2] != null ? qQCreditAppReqLineFins[2].Year : 0,
					QCreditAppReqLineFin3_RegisteredCapital = qQCreditAppReqLineFins[2] != null ? qQCreditAppReqLineFins[2].RegisteredCapital : 0,
					QCreditAppReqLineFin3_PaidCapital = qQCreditAppReqLineFins[2] != null ? qQCreditAppReqLineFins[2].PaidCapital : 0,
					QCreditAppReqLineFin3_TotalAsset = qQCreditAppReqLineFins[2] != null ? qQCreditAppReqLineFins[2].TotalAsset : 0,
					QCreditAppReqLineFin3_TotalLiability = qQCreditAppReqLineFins[2] != null ? qQCreditAppReqLineFins[2].TotalLiability : 0,
					QCreditAppReqLineFin3_TotalEquity = qQCreditAppReqLineFins[2] != null ? qQCreditAppReqLineFins[2].TotalEquity : 0,
					AppReqLineFin3_Year = qQCreditAppReqLineFins[2] != null ? qQCreditAppReqLineFins[2].Year : 0,
					QCreditAppReqLineFin3_TotalRevenue = qQCreditAppReqLineFins[2] != null ? qQCreditAppReqLineFins[2].TotalRevenue : 0,
					QCreditAppReqLineFin3_TotalCOGS = qQCreditAppReqLineFins[2] != null ? qQCreditAppReqLineFins[2].TotalCOGS : 0,
					QCreditAppReqLineFin3_TotalGrossProfit = qQCreditAppReqLineFins[2] != null ? qQCreditAppReqLineFins[2].TotalGrossProfit : 0,
					QCreditAppReqLineFin3_TotalOperExpFirst = qQCreditAppReqLineFins[2] != null ? qQCreditAppReqLineFins[2].TotalOperExpFirst : 0,
					QCreditAppReqLineFin3_TotalNetProfitFirst = qQCreditAppReqLineFins[2] != null ? qQCreditAppReqLineFins[2].TotalNetProfitFirst : 0,
					QCreditAppReqLineFin4_Year = qQCreditAppReqLineFins[3] != null ? qQCreditAppReqLineFins[3].Year : 0,
					QCreditAppReqLineFin4_RegisteredCapital = qQCreditAppReqLineFins[3] != null ? qQCreditAppReqLineFins[3].RegisteredCapital : 0,
					QCreditAppReqLineFin4_PaidCapital = qQCreditAppReqLineFins[3] != null ? qQCreditAppReqLineFins[3].PaidCapital : 0,
					QCreditAppReqLineFin4_TotalAsset = qQCreditAppReqLineFins[3] != null ? qQCreditAppReqLineFins[3].TotalAsset : 0,
					QCreditAppReqLineFin4_TotalLiability = qQCreditAppReqLineFins[3] != null ? qQCreditAppReqLineFins[3].TotalLiability : 0,
					QCreditAppReqLineFin4_TotalEquity = qQCreditAppReqLineFins[3] != null ? qQCreditAppReqLineFins[3].TotalEquity : 0,
					AppReqLineFin4_Year = qQCreditAppReqLineFins[3] != null ? qQCreditAppReqLineFins[3].Year : 0,
					QCreditAppReqLineFin4_TotalRevenue = qQCreditAppReqLineFins[3] != null ? qQCreditAppReqLineFins[3].TotalRevenue : 0,
					QCreditAppReqLineFin4_TotalCOGS = qQCreditAppReqLineFins[3] != null ? qQCreditAppReqLineFins[3].TotalCOGS : 0,
					QCreditAppReqLineFin4_TotalGrossProfit = qQCreditAppReqLineFins[3] != null ? qQCreditAppReqLineFins[3].TotalGrossProfit : 0,
					QCreditAppReqLineFin4_TotalOperExpFirst = qQCreditAppReqLineFins[3] != null ? qQCreditAppReqLineFins[3].TotalOperExpFirst : 0,
					QCreditAppReqLineFin4_TotalNetProfitFirst = qQCreditAppReqLineFins[3] != null ? qQCreditAppReqLineFins[3].TotalNetProfitFirst : 0,
					QCreditAppReqLineFin5_Year = qQCreditAppReqLineFins[4] != null ? qQCreditAppReqLineFins[4].Year : 0,
					QCreditAppReqLineFin5_RegisteredCapital = qQCreditAppReqLineFins[4] != null ? qQCreditAppReqLineFins[4].RegisteredCapital : 0,
					QCreditAppReqLineFin5_PaidCapital = qQCreditAppReqLineFins[4] != null ? qQCreditAppReqLineFins[4].PaidCapital : 0,
					QCreditAppReqLineFin5_TotalAsset = qQCreditAppReqLineFins[4] != null ? qQCreditAppReqLineFins[4].TotalAsset : 0,
					QCreditAppReqLineFin5_TotalLiability = qQCreditAppReqLineFins[4] != null ? qQCreditAppReqLineFins[4].TotalLiability : 0,
					QCreditAppReqLineFin5_TotalEquity = qQCreditAppReqLineFins[4] != null ? qQCreditAppReqLineFins[4].TotalEquity : 0,
					AppReqLineFin5_Year = qQCreditAppReqLineFins[4] != null ? qQCreditAppReqLineFins[4].Year : 0,
					QCreditAppReqLineFin5_TotalRevenue = qQCreditAppReqLineFins[4] != null ? qQCreditAppReqLineFins[4].TotalRevenue : 0,
					QCreditAppReqLineFin5_TotalCOGS = qQCreditAppReqLineFins[4] != null ? qQCreditAppReqLineFins[4].TotalCOGS : 0,
					QCreditAppReqLineFin5_TotalGrossProfit = qQCreditAppReqLineFins[4] != null ? qQCreditAppReqLineFins[4].TotalGrossProfit : 0,
					QCreditAppReqLineFin5_TotalOperExpFirst = qQCreditAppReqLineFins[4] != null ? qQCreditAppReqLineFins[4].TotalOperExpFirst : 0,
					QCreditAppReqLineFin5_TotalNetProfitFirst = qQCreditAppReqLineFins[4] != null ? qQCreditAppReqLineFins[4].TotalNetProfitFirst : 0,

					QCreditAppReqLineFin1_NetProfitPercent = qQCreditAppReqLineFins[0] != null ? qQCreditAppReqLineFins[0].NetProfitPercent : 0,
					QCreditAppReqLineFin1_lDE = qQCreditAppReqLineFins[0] != null ? qQCreditAppReqLineFins[0].lDE : 0,
					QCreditAppReqLineFin1_QuickRatio = qQCreditAppReqLineFins[0] != null ? qQCreditAppReqLineFins[0].QuickRatio : 0,
					QCreditAppReqLineFin1_IntCoverageRatio = qQCreditAppReqLineFins[0] != null ? qQCreditAppReqLineFins[0].IntCoverageRatio : 0,

					QCreditAppRequestTable_SalesAvgPerMonth = qQCreditAppRequestTable.SalesAvgPerMonth,

					QTaxReportStart_TaxMonth = qTaxReportStart != null ? qTaxReportStart.TaxMonth : "",

					QTaxReportEnd_TaxMonth = qTaxReportEnd != null ? qTaxReportEnd.TaxMonth : "",
					QTaxReportTrans1_TaxMonth = qTaxReportTrans[0] != null ? qTaxReportTrans[0].TaxMonth : "",
					QTaxReportTrans1_Amount = qTaxReportTrans[0] != null ? qTaxReportTrans[0].Amount : 0,
					QTaxReportTrans2_TaxMonth = qTaxReportTrans[1] != null ? qTaxReportTrans[1].TaxMonth : "",
					QTaxReportTrans2_Amount = qTaxReportTrans[1] != null ? qTaxReportTrans[1].Amount : 0,
					QTaxReportTrans3_TaxMonth = qTaxReportTrans[2] != null ? qTaxReportTrans[2].TaxMonth : "",
					QTaxReportTrans3_Amount = qTaxReportTrans[2] != null ? qTaxReportTrans[2].Amount : 0,
					QTaxReportTrans4_TaxMonth = qTaxReportTrans[3] != null ? qTaxReportTrans[3].TaxMonth : "",
					QTaxReportTrans4_Amount = qTaxReportTrans[3] != null ? qTaxReportTrans[3].Amount : 0,
					QTaxReportTrans5_TaxMonth = qTaxReportTrans[4] != null ? qTaxReportTrans[4].TaxMonth : "",
					QTaxReportTrans5_Amount = qTaxReportTrans[4] != null ? qTaxReportTrans[4].Amount : 0,
					QTaxReportTrans6_TaxMonth = qTaxReportTrans[5] != null ? qTaxReportTrans[5].TaxMonth : "",
					QTaxReportTrans6_Amount = qTaxReportTrans[5] != null ? qTaxReportTrans[5].Amount : 0,
					QTaxReportTrans7_TaxMonth = qTaxReportTrans[6] != null ? qTaxReportTrans[6].TaxMonth : "",
					QTaxReportTrans7_Amount = qTaxReportTrans[6] != null ? qTaxReportTrans[6].Amount : 0,
					QTaxReportTrans8_TaxMonth = qTaxReportTrans[7] != null ? qTaxReportTrans[7].TaxMonth : "",
					QTaxReportTrans8_Amount = qTaxReportTrans[7] != null ? qTaxReportTrans[7].Amount : 0,
					QTaxReportTrans9_TaxMonth = qTaxReportTrans[8] != null ? qTaxReportTrans[8].TaxMonth : "",
					QTaxReportTrans9_Amount = qTaxReportTrans[8] != null ? qTaxReportTrans[8].Amount : 0,
					QTaxReportTrans10_TaxMonth = qTaxReportTrans[9] != null ? qTaxReportTrans[9].TaxMonth : "",
					QTaxReportTrans10_Amount = qTaxReportTrans[9] != null ? qTaxReportTrans[9].Amount : 0,
					QTaxReportTrans11_TaxMonth = qTaxReportTrans[10] != null ? qTaxReportTrans[10].TaxMonth : "",
					QTaxReportTrans11_Amount = qTaxReportTrans[10] != null ? qTaxReportTrans[10].Amount : 0,
					QTaxReportTrans12_TaxMonth = qTaxReportTrans[11] != null ? qTaxReportTrans[11].TaxMonth : "",
					QTaxReportTrans12_Amount = qTaxReportTrans[11] != null ? qTaxReportTrans[11].Amount : 0,
					QTaxReportTransSum_SumTaxReport = qTaxReportTransSum[0] != null ? qTaxReportTransSum[0].SumTaxReport : 0,

					Variable_AverageOutputVATPerMonth = Math.Round(variable.AverageOutputVATPerMonth, 2),

					QCreditAppRequestTable_AsOfDate = qCAReqCreditOutStandingAsOfDate != null ? qCAReqCreditOutStandingAsOfDate.AsOfDate.DateNullToString() : "",
					QCAReqCreditOutStanding3_ApprovedCreditLimit = qCAReqCreditOutStanding[2] != null ? qCAReqCreditOutStanding[2].ApprovedCreditLimit : 0,
					QCAReqCreditOutStanding3_ARBalance = qCAReqCreditOutStanding[2] != null ? qCAReqCreditOutStanding[2].ARBalance : 0,
					QCAReqCreditOutStanding3_CreditLimitBalance = qCAReqCreditOutStanding[2] != null ? qCAReqCreditOutStanding[2].CreditLimitBalance : 0,
					QCAReqCreditOutStanding3_ReserveToBeRefund = qCAReqCreditOutStanding[2] != null ? qCAReqCreditOutStanding[2].ReserveToBeRefund : 0,
					QCAReqCreditOutStanding3_AccumRetentionAmount = qCAReqCreditOutStanding[2] != null ? qCAReqCreditOutStanding[2].AccumRetentionAmount : 0,

					QCAReqCreditOutStanding2_ApprovedCreditLimit = qCAReqCreditOutStanding[1] != null ? qCAReqCreditOutStanding[1].ApprovedCreditLimit : 0,
					QCAReqCreditOutStanding2_ARBalance = qCAReqCreditOutStanding[1] != null ? qCAReqCreditOutStanding[1].ARBalance : 0,
					QCAReqCreditOutStanding2_CreditLimitBalance = qCAReqCreditOutStanding[1] != null ? qCAReqCreditOutStanding[1].CreditLimitBalance : 0,
					QCAReqCreditOutStanding2_ReserveToBeRefund = qCAReqCreditOutStanding[1] != null ? qCAReqCreditOutStanding[1].ReserveToBeRefund : 0,
					QCAReqCreditOutStanding2_AccumRetentionAmount = qCAReqCreditOutStanding[1] != null ? qCAReqCreditOutStanding[1].AccumRetentionAmount : 0,

					QCAReqCreditOutStanding4_ApprovedCreditLimit = qCAReqCreditOutStanding[3] != null ? qCAReqCreditOutStanding[3].ApprovedCreditLimit : 0,
					QCAReqCreditOutStanding4_ARBalance = qCAReqCreditOutStanding[3] != null ? qCAReqCreditOutStanding[3].ARBalance : 0,
					QCAReqCreditOutStanding4_CreditLimitBalance = qCAReqCreditOutStanding[3] != null ? qCAReqCreditOutStanding[3].CreditLimitBalance : 0,
					QCAReqCreditOutStanding4_ReserveToBeRefund = qCAReqCreditOutStanding[3] != null ? qCAReqCreditOutStanding[3].ReserveToBeRefund : 0,
					QCAReqCreditOutStanding4_AccumRetentionAmount = qCAReqCreditOutStanding[3] != null ? qCAReqCreditOutStanding[3].AccumRetentionAmount : 0,

					QCAReqCreditOutStanding5_ApprovedCreditLimit = qCAReqCreditOutStanding[4] != null ? qCAReqCreditOutStanding[4].ApprovedCreditLimit : 0,
					QCAReqCreditOutStanding5_ARBalance = qCAReqCreditOutStanding[4] != null ? qCAReqCreditOutStanding[4].ARBalance : 0,
					QCAReqCreditOutStanding5_CreditLimitBalance = qCAReqCreditOutStanding[4] != null ? qCAReqCreditOutStanding[4].CreditLimitBalance : 0,
					QCAReqCreditOutStanding5_ReserveToBeRefund = qCAReqCreditOutStanding[4] != null ? qCAReqCreditOutStanding[4].ReserveToBeRefund : 0,
					QCAReqCreditOutStanding5_AccumRetentionAmount = qCAReqCreditOutStanding[4] != null ? qCAReqCreditOutStanding[4].AccumRetentionAmount : 0,

					QCAReqCreditOutStanding6_ApprovedCreditLimit = qCAReqCreditOutStanding[5] != null ? qCAReqCreditOutStanding[5].ApprovedCreditLimit : 0,
					QCAReqCreditOutStanding6_ARBalance = qCAReqCreditOutStanding[5] != null ? qCAReqCreditOutStanding[5].ARBalance : 0,
					QCAReqCreditOutStanding6_CreditLimitBalance = qCAReqCreditOutStanding[5] != null ? qCAReqCreditOutStanding[5].CreditLimitBalance : 0,
					QCAReqCreditOutStanding6_ReserveToBeRefund = qCAReqCreditOutStanding[5] != null ? qCAReqCreditOutStanding[5].ReserveToBeRefund : 0,
					QCAReqCreditOutStanding6_AccumRetentionAmount = qCAReqCreditOutStanding[5] != null ? qCAReqCreditOutStanding[5].AccumRetentionAmount : 0,

					QCAReqCreditOutStanding7_ApprovedCreditLimit = qCAReqCreditOutStanding[6] != null ? qCAReqCreditOutStanding[6].ApprovedCreditLimit : 0,
					QCAReqCreditOutStanding7_ARBalance = qCAReqCreditOutStanding[6] != null ? qCAReqCreditOutStanding[6].ARBalance : 0,
					QCAReqCreditOutStanding7_CreditLimitBalance = qCAReqCreditOutStanding[6] != null ? qCAReqCreditOutStanding[6].CreditLimitBalance : 0,
					QCAReqCreditOutStanding7_ReserveToBeRefund = qCAReqCreditOutStanding[6] != null ? qCAReqCreditOutStanding[6].ReserveToBeRefund : 0,
					QCAReqCreditOutStanding7_AccumRetentionAmount = qCAReqCreditOutStanding[6] != null ? qCAReqCreditOutStanding[6].AccumRetentionAmount : 0,

					QCAReqCreditOutStanding8_ApprovedCreditLimit = qCAReqCreditOutStanding[7] != null ? qCAReqCreditOutStanding[7].ApprovedCreditLimit : 0,
					QCAReqCreditOutStanding8_ARBalance = qCAReqCreditOutStanding[7] != null ? qCAReqCreditOutStanding[7].ARBalance : 0,
					QCAReqCreditOutStanding8_CreditLimitBalance = qCAReqCreditOutStanding[7] != null ? qCAReqCreditOutStanding[7].CreditLimitBalance : 0,
					QCAReqCreditOutStanding8_ReserveToBeRefund = qCAReqCreditOutStanding[7] != null ? qCAReqCreditOutStanding[7].ReserveToBeRefund : 0,
					QCAReqCreditOutStanding8_AccumRetentionAmount = qCAReqCreditOutStanding[7] != null ? qCAReqCreditOutStanding[7].AccumRetentionAmount : 0,

					QCAReqCreditOutStanding9_ApprovedCreditLimit = qCAReqCreditOutStanding[8] != null ? qCAReqCreditOutStanding[8].ApprovedCreditLimit : 0,
					QCAReqCreditOutStanding9_ARBalance = qCAReqCreditOutStanding[8] != null ? qCAReqCreditOutStanding[8].ARBalance : 0,
					QCAReqCreditOutStanding9_CreditLimitBalance = qCAReqCreditOutStanding[8] != null ? qCAReqCreditOutStanding[8].CreditLimitBalance : 0,
					QCAReqCreditOutStanding9_ReserveToBeRefund = qCAReqCreditOutStanding[8] != null ? qCAReqCreditOutStanding[8].ReserveToBeRefund : 0,
					QCAReqCreditOutStanding9_AccumRetentionAmount = qCAReqCreditOutStanding[8] != null ? qCAReqCreditOutStanding[8].AccumRetentionAmount : 0,

					QCAReqCreditOutStanding10_ApprovedCreditLimit = qCAReqCreditOutStanding[9] != null ? qCAReqCreditOutStanding[9].ApprovedCreditLimit : 0,
					QCAReqCreditOutStanding10_ARBalance = qCAReqCreditOutStanding[9] != null ? qCAReqCreditOutStanding[9].ARBalance : 0,
					QCAReqCreditOutStanding10_CreditLimitBalance = qCAReqCreditOutStanding[9] != null ? qCAReqCreditOutStanding[9].CreditLimitBalance : 0,
					QCAReqCreditOutStanding10_ReserveToBeRefund = qCAReqCreditOutStanding[9] != null ? qCAReqCreditOutStanding[9].ReserveToBeRefund : 0,
					QCAReqCreditOutStanding10_AccumRetentionAmount = qCAReqCreditOutStanding[9] != null ? qCAReqCreditOutStanding[9].AccumRetentionAmount : 0,

					QCAReqCreditOutStanding11_ApprovedCreditLimit = qCAReqCreditOutStanding[10] != null ? qCAReqCreditOutStanding[10].ApprovedCreditLimit : 0,
					QCAReqCreditOutStanding11_ARBalance = qCAReqCreditOutStanding[10] != null ? qCAReqCreditOutStanding[10].ARBalance : 0,
					QCAReqCreditOutStanding11_CreditLimitBalance = qCAReqCreditOutStanding[10] != null ? qCAReqCreditOutStanding[10].CreditLimitBalance : 0,
					QCAReqCreditOutStanding11_ReserveToBeRefund = qCAReqCreditOutStanding[10] != null ? qCAReqCreditOutStanding[10].ReserveToBeRefund : 0,
					QCAReqCreditOutStanding11_AccumRetentionAmount = qCAReqCreditOutStanding[10] != null ? qCAReqCreditOutStanding[10].AccumRetentionAmount : 0,


					QCAReqCreditOutStandingSum_SumApprovedCreditLimit = qCAReqCreditOutStandingSum != null ? qCAReqCreditOutStandingSum.SumApprovedCreditLimit : 0,
					QCAReqCreditOutStandingSum_SumARBalance = qCAReqCreditOutStandingSum != null ? qCAReqCreditOutStandingSum.SumARBalance : 0,
					QCAReqCreditOutStandingSum_SumCreditLimitBalance = qCAReqCreditOutStandingSum != null ? qCAReqCreditOutStandingSum.SumCreditLimitBalance : 0,

					QCAReqCreditOutStandingSum_SumReserveToBeRefund = qCAReqCreditOutStandingSum != null ? qCAReqCreditOutStandingSum.SumReserveToBeRefund : 0,

					QCAReqCreditOutStandingSum_SumAccumRetentionAmount = qCAReqCreditOutStandingSum != null ? qCAReqCreditOutStandingSum.SumAccumRetentionAmount : 0,
					QCreditAppRequestTable_FinancialCreditCheckedDate = qQCreditAppRequestTable.FinancialCreditCheckedDate != null ? qQCreditAppRequestTable.FinancialCreditCheckedDate : "-",

					QFinancialCreditTrans1_FinancialInstitutionDesc = qFinancialCreditTrans[0] != null ? qFinancialCreditTrans[0].FinancialInstitutionDesc : "",
					QFinancialCreditTrans1_LoanType = qFinancialCreditTrans[0] != null ? qFinancialCreditTrans[0].LoanType : "",
					QFinancialCreditTrans1_Amount = qFinancialCreditTrans[0] != null ? qFinancialCreditTrans[0].Amount : 0,

					QFinancialCreditTrans2_FinancialInstitutionDesc = qFinancialCreditTrans[1] != null ? qFinancialCreditTrans[1].FinancialInstitutionDesc : "",
					QFinancialCreditTrans2_LoanType = qFinancialCreditTrans[1] != null ? qFinancialCreditTrans[1].LoanType : "",
					QFinancialCreditTrans2_Amount = qFinancialCreditTrans[1] != null ? qFinancialCreditTrans[1].Amount : 0,

					QFinancialCreditTrans3_FinancialInstitutionDesc = qFinancialCreditTrans[2] != null ? qFinancialCreditTrans[2].FinancialInstitutionDesc : "",
					QFinancialCreditTrans3_LoanType = qFinancialCreditTrans[2] != null ? qFinancialCreditTrans[2].LoanType : "",
					QFinancialCreditTrans3_Amount = qFinancialCreditTrans[2] != null ? qFinancialCreditTrans[2].Amount : 0,

					QFinancialCreditTrans4_FinancialInstitutionDesc = qFinancialCreditTrans[3] != null ? qFinancialCreditTrans[3].FinancialInstitutionDesc : "",
					QFinancialCreditTrans4_LoanType = qFinancialCreditTrans[3] != null ? qFinancialCreditTrans[3].LoanType : "",
					QFinancialCreditTrans4_Amount = qFinancialCreditTrans[3] != null ? qFinancialCreditTrans[3].Amount : 0,

					QFinancialCreditTrans5_FinancialInstitutionDesc = qFinancialCreditTrans[4] != null ? qFinancialCreditTrans[4].FinancialInstitutionDesc : "",
					QFinancialCreditTrans5_LoanType = qFinancialCreditTrans[4] != null ? qFinancialCreditTrans[4].LoanType : "",
					QFinancialCreditTrans5_Amount = qFinancialCreditTrans[4] != null ? qFinancialCreditTrans[4].Amount : 0,

					QFinancialCreditTrans6_FinancialInstitutionDesc = qFinancialCreditTrans[5] != null ? qFinancialCreditTrans[5].FinancialInstitutionDesc : "",
					QFinancialCreditTrans6_LoanType = qFinancialCreditTrans[5] != null ? qFinancialCreditTrans[5].LoanType : "",
					QFinancialCreditTrans6_Amount = qFinancialCreditTrans[5] != null ? qFinancialCreditTrans[5].Amount : 0,

					QFinancialCreditTrans7_FinancialInstitutionDesc = qFinancialCreditTrans[6] != null ? qFinancialCreditTrans[6].FinancialInstitutionDesc : "",
					QFinancialCreditTrans7_LoanType = qFinancialCreditTrans[6] != null ? qFinancialCreditTrans[6].LoanType : "",
					QFinancialCreditTrans7_Amount = qFinancialCreditTrans[6] != null ? qFinancialCreditTrans[6].Amount : 0,

					QFinancialCreditTrans8_FinancialInstitutionDesc = qFinancialCreditTrans[7] != null ? qFinancialCreditTrans[7].FinancialInstitutionDesc : "",
					QFinancialCreditTrans8_LoanType = qFinancialCreditTrans[7] != null ? qFinancialCreditTrans[7].LoanType : "",
					QFinancialCreditTrans8_Amount = qFinancialCreditTrans[7] != null ? qFinancialCreditTrans[7].Amount : 0,
					QFinancialCreditTransSum_TotalLoanLimit = qFinancialCreditTransSum != null ? qFinancialCreditTransSum.TotalLoanLimit : 0,

					QCAReqBuyerCreditOutstandingPrivate1_BuyerCreditOutstandingBuyerName = qQCAReqBuyerCreditOutstandingGovs[0] != null ? qQCAReqBuyerCreditOutstandingGovs[0].BuyerCreditOutstandingBuyerName : "",
					QCAReqBuyerCreditOutstandingPrivate1_Status = qQCAReqBuyerCreditOutstandingGovs[0] != null ? qQCAReqBuyerCreditOutstandingGovs[0].Status : "",
					QCAReqBuyerCreditOutstandingPrivate1_ApprovedCreditLimitLine = qQCAReqBuyerCreditOutstandingGovs[0] != null ? qQCAReqBuyerCreditOutstandingGovs[0].ApprovedCreditLimitLine : 0,
					QCAReqBuyerCreditOutstandingPrivate1_ARBalance = qQCAReqBuyerCreditOutstandingGovs[0] != null ? qQCAReqBuyerCreditOutstandingGovs[0].ARBalance : 0,
					QCAReqBuyerCreditOutstandingPrivate1_MaxPurchasePct = qQCAReqBuyerCreditOutstandingGovs[0] != null ? qQCAReqBuyerCreditOutstandingGovs[0].MaxPurchasePct : 0,
					QCAReqBuyerCreditOutstandingPrivate1_AssignmentMethod = qQCAReqBuyerCreditOutstandingGovs[0] != null ? qQCAReqBuyerCreditOutstandingGovs[0].AssignmentMethod : "",
					QCAReqBuyerCreditOutstandingPrivate1_BillingResponsibleBy = qQCAReqBuyerCreditOutstandingGovs[0] != null ? qQCAReqBuyerCreditOutstandingGovs[0].BillingResponsibleBy : "",
					QCAReqBuyerCreditOutstandingPrivate1_MethodOfPayment = qQCAReqBuyerCreditOutstandingGovs[0] != null ? qQCAReqBuyerCreditOutstandingGovs[0].MethodOfPayment : "",

					QCAReqBuyerCreditOutstandingPrivate2_BuyerCreditOutstandingBuyerName = qQCAReqBuyerCreditOutstandingGovs[1] != null ? qQCAReqBuyerCreditOutstandingGovs[1].BuyerCreditOutstandingBuyerName : "",
					QCAReqBuyerCreditOutstandingPrivate2_Status = qQCAReqBuyerCreditOutstandingGovs[1] != null ? qQCAReqBuyerCreditOutstandingGovs[1].Status : "",
					QCAReqBuyerCreditOutstandingPrivate2_ApprovedCreditLimitLine = qQCAReqBuyerCreditOutstandingGovs[1] != null ? qQCAReqBuyerCreditOutstandingGovs[1].ApprovedCreditLimitLine : 0,
					QCAReqBuyerCreditOutstandingPrivate2_ARBalance = qQCAReqBuyerCreditOutstandingGovs[1] != null ? qQCAReqBuyerCreditOutstandingGovs[1].ARBalance : 0,
					QCAReqBuyerCreditOutstandingPrivate2_MaxPurchasePct = qQCAReqBuyerCreditOutstandingGovs[1] != null ? qQCAReqBuyerCreditOutstandingGovs[1].MaxPurchasePct : 0,
					QCAReqBuyerCreditOutstandingPrivate2_AssignmentMethod = qQCAReqBuyerCreditOutstandingGovs[1] != null ? qQCAReqBuyerCreditOutstandingGovs[1].AssignmentMethod : "",
					QCAReqBuyerCreditOutstandingPrivate2_BillingResponsibleBy = qQCAReqBuyerCreditOutstandingGovs[1] != null ? qQCAReqBuyerCreditOutstandingGovs[1].BillingResponsibleBy : "",
					QCAReqBuyerCreditOutstandingPrivate2_MethodOfPayment = qQCAReqBuyerCreditOutstandingGovs[1] != null ? qQCAReqBuyerCreditOutstandingGovs[1].MethodOfPayment : "",

					QCAReqBuyerCreditOutstandingPrivate3_BuyerCreditOutstandingBuyerName = qQCAReqBuyerCreditOutstandingGovs[2] != null ? qQCAReqBuyerCreditOutstandingGovs[2].BuyerCreditOutstandingBuyerName : "",
					QCAReqBuyerCreditOutstandingPrivate3_Status = qQCAReqBuyerCreditOutstandingGovs[2] != null ? qQCAReqBuyerCreditOutstandingGovs[2].Status : "",
					QCAReqBuyerCreditOutstandingPrivate3_ApprovedCreditLimitLine = qQCAReqBuyerCreditOutstandingGovs[2] != null ? qQCAReqBuyerCreditOutstandingGovs[2].ApprovedCreditLimitLine : 0,
					QCAReqBuyerCreditOutstandingPrivate3_ARBalance = qQCAReqBuyerCreditOutstandingGovs[2] != null ? qQCAReqBuyerCreditOutstandingGovs[2].ARBalance : 0,
					QCAReqBuyerCreditOutstandingPrivate3_MaxPurchasePct = qQCAReqBuyerCreditOutstandingGovs[2] != null ? qQCAReqBuyerCreditOutstandingGovs[2].MaxPurchasePct : 0,
					QCAReqBuyerCreditOutstandingPrivate3_AssignmentMethod = qQCAReqBuyerCreditOutstandingGovs[2] != null ? qQCAReqBuyerCreditOutstandingGovs[2].AssignmentMethod : "",
					QCAReqBuyerCreditOutstandingPrivate3_BillingResponsibleBy = qQCAReqBuyerCreditOutstandingGovs[2] != null ? qQCAReqBuyerCreditOutstandingGovs[2].BillingResponsibleBy : "",
					QCAReqBuyerCreditOutstandingPrivate3_MethodOfPayment = qQCAReqBuyerCreditOutstandingGovs[2] != null ? qQCAReqBuyerCreditOutstandingGovs[2].MethodOfPayment : "",

					QCAReqBuyerCreditOutstandingPrivate4_BuyerCreditOutstandingBuyerName = qQCAReqBuyerCreditOutstandingGovs[3] != null ? qQCAReqBuyerCreditOutstandingGovs[3].BuyerCreditOutstandingBuyerName : "",
					QCAReqBuyerCreditOutstandingPrivate4_Status = qQCAReqBuyerCreditOutstandingGovs[3] != null ? qQCAReqBuyerCreditOutstandingGovs[3].Status : "",
					QCAReqBuyerCreditOutstandingPrivate4_ApprovedCreditLimitLine = qQCAReqBuyerCreditOutstandingGovs[3] != null ? qQCAReqBuyerCreditOutstandingGovs[3].ApprovedCreditLimitLine : 0,
					QCAReqBuyerCreditOutstandingPrivate4_ARBalance = qQCAReqBuyerCreditOutstandingGovs[3] != null ? qQCAReqBuyerCreditOutstandingGovs[3].ARBalance : 0,
					QCAReqBuyerCreditOutstandingPrivate4_MaxPurchasePct = qQCAReqBuyerCreditOutstandingGovs[3] != null ? qQCAReqBuyerCreditOutstandingGovs[3].MaxPurchasePct : 0,
					QCAReqBuyerCreditOutstandingPrivate4_AssignmentMethod = qQCAReqBuyerCreditOutstandingGovs[3] != null ? qQCAReqBuyerCreditOutstandingGovs[3].AssignmentMethod : "",
					QCAReqBuyerCreditOutstandingPrivate4_BillingResponsibleBy = qQCAReqBuyerCreditOutstandingGovs[3] != null ? qQCAReqBuyerCreditOutstandingGovs[3].BillingResponsibleBy : "",
					QCAReqBuyerCreditOutstandingPrivate4_MethodOfPayment = qQCAReqBuyerCreditOutstandingGovs[3] != null ? qQCAReqBuyerCreditOutstandingGovs[3].MethodOfPayment : "",

					QCAReqBuyerCreditOutstandingPrivate5_BuyerCreditOutstandingBuyerName = qQCAReqBuyerCreditOutstandingGovs[4] != null ? qQCAReqBuyerCreditOutstandingGovs[4].BuyerCreditOutstandingBuyerName : "",
					QCAReqBuyerCreditOutstandingPrivate5_Status = qQCAReqBuyerCreditOutstandingGovs[4] != null ? qQCAReqBuyerCreditOutstandingGovs[4].Status : "",
					QCAReqBuyerCreditOutstandingPrivate5_ApprovedCreditLimitLine = qQCAReqBuyerCreditOutstandingGovs[4] != null ? qQCAReqBuyerCreditOutstandingGovs[4].ApprovedCreditLimitLine : 0,
					QCAReqBuyerCreditOutstandingPrivate5_ARBalance = qQCAReqBuyerCreditOutstandingGovs[4] != null ? qQCAReqBuyerCreditOutstandingGovs[4].ARBalance : 0,
					QCAReqBuyerCreditOutstandingPrivate5_MaxPurchasePct = qQCAReqBuyerCreditOutstandingGovs[4] != null ? qQCAReqBuyerCreditOutstandingGovs[4].MaxPurchasePct : 0,
					QCAReqBuyerCreditOutstandingPrivate5_AssignmentMethod = qQCAReqBuyerCreditOutstandingGovs[4] != null ? qQCAReqBuyerCreditOutstandingGovs[4].AssignmentMethod : "",
					QCAReqBuyerCreditOutstandingPrivate5_BillingResponsibleBy = qQCAReqBuyerCreditOutstandingGovs[4] != null ? qQCAReqBuyerCreditOutstandingGovs[4].BillingResponsibleBy : "",
					QCAReqBuyerCreditOutstandingPrivate5_MethodOfPayment = qQCAReqBuyerCreditOutstandingGovs[4] != null ? qQCAReqBuyerCreditOutstandingGovs[4].MethodOfPayment : "",

					QCAReqBuyerCreditOutstandingPrivate6_BuyerCreditOutstandingBuyerName = qQCAReqBuyerCreditOutstandingGovs[5] != null ? qQCAReqBuyerCreditOutstandingGovs[5].BuyerCreditOutstandingBuyerName : "",
					QCAReqBuyerCreditOutstandingPrivate6_Status = qQCAReqBuyerCreditOutstandingGovs[5] != null ? qQCAReqBuyerCreditOutstandingGovs[5].Status : "",
					QCAReqBuyerCreditOutstandingPrivate6_ApprovedCreditLimitLine = qQCAReqBuyerCreditOutstandingGovs[5] != null ? qQCAReqBuyerCreditOutstandingGovs[5].ApprovedCreditLimitLine : 0,
					QCAReqBuyerCreditOutstandingPrivate6_ARBalance = qQCAReqBuyerCreditOutstandingGovs[5] != null ? qQCAReqBuyerCreditOutstandingGovs[5].ARBalance : 0,
					QCAReqBuyerCreditOutstandingPrivate6_MaxPurchasePct = qQCAReqBuyerCreditOutstandingGovs[5] != null ? qQCAReqBuyerCreditOutstandingGovs[5].MaxPurchasePct : 0,
					QCAReqBuyerCreditOutstandingPrivate6_AssignmentMethod = qQCAReqBuyerCreditOutstandingGovs[5] != null ? qQCAReqBuyerCreditOutstandingGovs[5].AssignmentMethod : "",
					QCAReqBuyerCreditOutstandingPrivate6_BillingResponsibleBy = qQCAReqBuyerCreditOutstandingGovs[5] != null ? qQCAReqBuyerCreditOutstandingGovs[5].BillingResponsibleBy : "",
					QCAReqBuyerCreditOutstandingPrivate6_MethodOfPayment = qQCAReqBuyerCreditOutstandingGovs[5] != null ? qQCAReqBuyerCreditOutstandingGovs[5].MethodOfPayment : "",

					QCAReqBuyerCreditOutstandingPrivate7_BuyerCreditOutstandingBuyerName = qQCAReqBuyerCreditOutstandingGovs[6] != null ? qQCAReqBuyerCreditOutstandingGovs[6].BuyerCreditOutstandingBuyerName : "",
					QCAReqBuyerCreditOutstandingPrivate7_Status = qQCAReqBuyerCreditOutstandingGovs[6] != null ? qQCAReqBuyerCreditOutstandingGovs[6].Status : "",
					QCAReqBuyerCreditOutstandingPrivate7_ApprovedCreditLimitLine = qQCAReqBuyerCreditOutstandingGovs[6] != null ? qQCAReqBuyerCreditOutstandingGovs[6].ApprovedCreditLimitLine : 0,
					QCAReqBuyerCreditOutstandingPrivate7_ARBalance = qQCAReqBuyerCreditOutstandingGovs[6] != null ? qQCAReqBuyerCreditOutstandingGovs[6].ARBalance : 0,
					QCAReqBuyerCreditOutstandingPrivate7_MaxPurchasePct = qQCAReqBuyerCreditOutstandingGovs[6] != null ? qQCAReqBuyerCreditOutstandingGovs[6].MaxPurchasePct : 0,
					QCAReqBuyerCreditOutstandingPrivate7_AssignmentMethod = qQCAReqBuyerCreditOutstandingGovs[6] != null ? qQCAReqBuyerCreditOutstandingGovs[6].AssignmentMethod : "",
					QCAReqBuyerCreditOutstandingPrivate7_BillingResponsibleBy = qQCAReqBuyerCreditOutstandingGovs[6] != null ? qQCAReqBuyerCreditOutstandingGovs[6].BillingResponsibleBy : "",
					QCAReqBuyerCreditOutstandingPrivate7_MethodOfPayment = qQCAReqBuyerCreditOutstandingGovs[6] != null ? qQCAReqBuyerCreditOutstandingGovs[6].MethodOfPayment : "",

					QCAReqBuyerCreditOutstandingPrivate8_BuyerCreditOutstandingBuyerName = qQCAReqBuyerCreditOutstandingGovs[7] != null ? qQCAReqBuyerCreditOutstandingGovs[7].BuyerCreditOutstandingBuyerName : "",
					QCAReqBuyerCreditOutstandingPrivate8_Status = qQCAReqBuyerCreditOutstandingGovs[7] != null ? qQCAReqBuyerCreditOutstandingGovs[7].Status : "",
					QCAReqBuyerCreditOutstandingPrivate8_ApprovedCreditLimitLine = qQCAReqBuyerCreditOutstandingGovs[7] != null ? qQCAReqBuyerCreditOutstandingGovs[7].ApprovedCreditLimitLine : 0,
					QCAReqBuyerCreditOutstandingPrivate8_ARBalance = qQCAReqBuyerCreditOutstandingGovs[7] != null ? qQCAReqBuyerCreditOutstandingGovs[7].ARBalance : 0,
					QCAReqBuyerCreditOutstandingPrivate8_MaxPurchasePct = qQCAReqBuyerCreditOutstandingGovs[7] != null ? qQCAReqBuyerCreditOutstandingGovs[7].MaxPurchasePct : 0,
					QCAReqBuyerCreditOutstandingPrivate8_AssignmentMethod = qQCAReqBuyerCreditOutstandingGovs[7] != null ? qQCAReqBuyerCreditOutstandingGovs[7].AssignmentMethod : "",
					QCAReqBuyerCreditOutstandingPrivate8_BillingResponsibleBy = qQCAReqBuyerCreditOutstandingGovs[7] != null ? qQCAReqBuyerCreditOutstandingGovs[7].BillingResponsibleBy : "",
					QCAReqBuyerCreditOutstandingPrivate8_MethodOfPayment = qQCAReqBuyerCreditOutstandingGovs[7] != null ? qQCAReqBuyerCreditOutstandingGovs[7].MethodOfPayment : "",

					QCAReqBuyerCreditOutstandingPrivate9_BuyerCreditOutstandingBuyerName = qQCAReqBuyerCreditOutstandingGovs[8] != null ? qQCAReqBuyerCreditOutstandingGovs[8].BuyerCreditOutstandingBuyerName : "",
					QCAReqBuyerCreditOutstandingPrivate9_Status = qQCAReqBuyerCreditOutstandingGovs[8] != null ? qQCAReqBuyerCreditOutstandingGovs[8].Status : "",
					QCAReqBuyerCreditOutstandingPrivate9_ApprovedCreditLimitLine = qQCAReqBuyerCreditOutstandingGovs[8] != null ? qQCAReqBuyerCreditOutstandingGovs[8].ApprovedCreditLimitLine : 0,
					QCAReqBuyerCreditOutstandingPrivate9_ARBalance = qQCAReqBuyerCreditOutstandingGovs[8] != null ? qQCAReqBuyerCreditOutstandingGovs[8].ARBalance : 0,
					QCAReqBuyerCreditOutstandingPrivate9_MaxPurchasePct = qQCAReqBuyerCreditOutstandingGovs[8] != null ? qQCAReqBuyerCreditOutstandingGovs[8].MaxPurchasePct : 0,
					QCAReqBuyerCreditOutstandingPrivate9_AssignmentMethod = qQCAReqBuyerCreditOutstandingGovs[8] != null ? qQCAReqBuyerCreditOutstandingGovs[8].AssignmentMethod : "",
					QCAReqBuyerCreditOutstandingPrivate9_BillingResponsibleBy = qQCAReqBuyerCreditOutstandingGovs[8] != null ? qQCAReqBuyerCreditOutstandingGovs[8].BillingResponsibleBy : "",
					QCAReqBuyerCreditOutstandingPrivate9_MethodOfPayment = qQCAReqBuyerCreditOutstandingGovs[8] != null ? qQCAReqBuyerCreditOutstandingGovs[8].MethodOfPayment : "",

					QCAReqBuyerCreditOutstandingPrivate10_BuyerCreditOutstandingBuyerName = qQCAReqBuyerCreditOutstandingGovs[9] != null ? qQCAReqBuyerCreditOutstandingGovs[9].BuyerCreditOutstandingBuyerName : "",
					QCAReqBuyerCreditOutstandingPrivate10_Status = qQCAReqBuyerCreditOutstandingGovs[9] != null ? qQCAReqBuyerCreditOutstandingGovs[9].Status : "",
					QCAReqBuyerCreditOutstandingPrivate10_ApprovedCreditLimitLine = qQCAReqBuyerCreditOutstandingGovs[9] != null ? qQCAReqBuyerCreditOutstandingGovs[9].ApprovedCreditLimitLine : 0,
					QCAReqBuyerCreditOutstandingPrivate10_ARBalance = qQCAReqBuyerCreditOutstandingGovs[9] != null ? qQCAReqBuyerCreditOutstandingGovs[9].ARBalance : 0,
					QCAReqBuyerCreditOutstandingPrivate10_MaxPurchasePct = qQCAReqBuyerCreditOutstandingGovs[9] != null ? qQCAReqBuyerCreditOutstandingGovs[9].MaxPurchasePct : 0,
					QCAReqBuyerCreditOutstandingPrivate10_AssignmentMethod = qQCAReqBuyerCreditOutstandingGovs[9] != null ? qQCAReqBuyerCreditOutstandingGovs[9].AssignmentMethod : "",
					QCAReqBuyerCreditOutstandingPrivate10_BillingResponsibleBy = qQCAReqBuyerCreditOutstandingGovs[9] != null ? qQCAReqBuyerCreditOutstandingGovs[9].BillingResponsibleBy : "",
					QCAReqBuyerCreditOutstandingPrivate10_MethodOfPayment = qQCAReqBuyerCreditOutstandingGovs[9] != null ? qQCAReqBuyerCreditOutstandingGovs[9].MethodOfPayment : "",

					QCAReqBuyerCreditOutstandingGov1_BuyerCreditOutstandingBuyerName = qQCAReqBuyerCreditOutstandingGovs1[0] != null ? qQCAReqBuyerCreditOutstandingGovs1[0].BuyerCreditOutstandingBuyerName : "",
					QCAReqBuyerCreditOutstandingGov1_Status = qQCAReqBuyerCreditOutstandingGovs1[0] != null ? qQCAReqBuyerCreditOutstandingGovs1[0].Status : "",
					QCAReqBuyerCreditOutstandingGov1_ApprovedCreditLimitLine = qQCAReqBuyerCreditOutstandingGovs1[0] != null ? qQCAReqBuyerCreditOutstandingGovs1[0].ApprovedCreditLimitLine : 0,
					QCAReqBuyerCreditOutstandingGov1_ARBalance = qQCAReqBuyerCreditOutstandingGovs1[0] != null ? qQCAReqBuyerCreditOutstandingGovs1[0].ARBalance : 0,
					QCAReqBuyerCreditOutstandingGov1_MaxPurchasePct = qQCAReqBuyerCreditOutstandingGovs1[0] != null ? qQCAReqBuyerCreditOutstandingGovs1[0].MaxPurchasePct : 0,
					QCAReqBuyerCreditOutstandingGov1_AssignmentMethod = qQCAReqBuyerCreditOutstandingGovs1[0] != null ? qQCAReqBuyerCreditOutstandingGovs1[0].AssignmentMethod : "",
					QCAReqBuyerCreditOutstandingGov1_BillingResponsibleBy = qQCAReqBuyerCreditOutstandingGovs1[0] != null ? qQCAReqBuyerCreditOutstandingGovs1[0].BillingResponsibleBy : "",
					QCAReqBuyerCreditOutstandingGov1_MethodOfPayment = qQCAReqBuyerCreditOutstandingGovs1[0] != null ? qQCAReqBuyerCreditOutstandingGovs1[0].MethodOfPayment : "",

                    QCAReqBuyerCreditOutstandingGov2_BuyerCreditOutstandingBuyerName = qQCAReqBuyerCreditOutstandingGovs1[1] != null ? qQCAReqBuyerCreditOutstandingGovs1[1].BuyerCreditOutstandingBuyerName : "",
                    QCAReqBuyerCreditOutstandingGov2_Status = qQCAReqBuyerCreditOutstandingGovs1[1] != null ? qQCAReqBuyerCreditOutstandingGovs1[1].Status : "",
                    QCAReqBuyerCreditOutstandingGov2_ApprovedCreditLimitLine = qQCAReqBuyerCreditOutstandingGovs1[1] != null ? qQCAReqBuyerCreditOutstandingGovs1[1].ApprovedCreditLimitLine : 0,
                    QCAReqBuyerCreditOutstandingGov2_ARBalance = qQCAReqBuyerCreditOutstandingGovs1[1] != null ? qQCAReqBuyerCreditOutstandingGovs1[1].ARBalance : 0,
                    QCAReqBuyerCreditOutstandingGov2_MaxPurchasePct = qQCAReqBuyerCreditOutstandingGovs1[1] != null ? qQCAReqBuyerCreditOutstandingGovs1[1].MaxPurchasePct : 0,
                    QCAReqBuyerCreditOutstandingGov2_AssignmentMethod = qQCAReqBuyerCreditOutstandingGovs1[1] != null ? qQCAReqBuyerCreditOutstandingGovs1[1].AssignmentMethod : "",
                    QCAReqBuyerCreditOutstandingGov2_BillingResponsibleBy = qQCAReqBuyerCreditOutstandingGovs1[1] != null ? qQCAReqBuyerCreditOutstandingGovs1[1].BillingResponsibleBy != null ? qQCAReqBuyerCreditOutstandingGovs1[1].BillingResponsibleBy : "" : "",
                    QCAReqBuyerCreditOutstandingGov2_MethodOfPayment = qQCAReqBuyerCreditOutstandingGovs1[1] != null ? qQCAReqBuyerCreditOutstandingGovs1[1].MethodOfPayment : "",

                    QCAReqBuyerCreditOutstandingGov3_BuyerCreditOutstandingBuyerName = qQCAReqBuyerCreditOutstandingGovs1[2] != null ? qQCAReqBuyerCreditOutstandingGovs1[2].BuyerCreditOutstandingBuyerName : "",
                    QCAReqBuyerCreditOutstandingGov3_Status = qQCAReqBuyerCreditOutstandingGovs1[2] != null ? qQCAReqBuyerCreditOutstandingGovs1[2].Status : "",
                    QCAReqBuyerCreditOutstandingGov3_ApprovedCreditLimitLine = qQCAReqBuyerCreditOutstandingGovs1[2] != null ? qQCAReqBuyerCreditOutstandingGovs1[2].ApprovedCreditLimitLine : 0,
                    QCAReqBuyerCreditOutstandingGov3_ARBalance = qQCAReqBuyerCreditOutstandingGovs1[2] != null ? qQCAReqBuyerCreditOutstandingGovs1[2].ARBalance : 0,
                    QCAReqBuyerCreditOutstandingGov3_MaxPurchasePct = qQCAReqBuyerCreditOutstandingGovs1[2] != null ? qQCAReqBuyerCreditOutstandingGovs1[2].MaxPurchasePct : 0,
                    QCAReqBuyerCreditOutstandingGov3_AssignmentMethod = qQCAReqBuyerCreditOutstandingGovs1[2] != null ? qQCAReqBuyerCreditOutstandingGovs1[2].AssignmentMethod : "",
                    QCAReqBuyerCreditOutstandingGov3_BillingResponsibleBy = qQCAReqBuyerCreditOutstandingGovs1[2] != null ? qQCAReqBuyerCreditOutstandingGovs1[2].BillingResponsibleBy != null ? qQCAReqBuyerCreditOutstandingGovs1[1].BillingResponsibleBy : "" : "",
                    QCAReqBuyerCreditOutstandingGov3_MethodOfPayment = qQCAReqBuyerCreditOutstandingGovs1[2] != null ? qQCAReqBuyerCreditOutstandingGovs1[2].MethodOfPayment : "",

                    QCAReqBuyerCreditOutstandingGov4_BuyerCreditOutstandingBuyerName = qQCAReqBuyerCreditOutstandingGovs1[3] != null ? qQCAReqBuyerCreditOutstandingGovs1[3].BuyerCreditOutstandingBuyerName : "",
                    QCAReqBuyerCreditOutstandingGov4_Status = qQCAReqBuyerCreditOutstandingGovs1[3] != null ? qQCAReqBuyerCreditOutstandingGovs1[3].Status : "",
                    QCAReqBuyerCreditOutstandingGov4_ApprovedCreditLimitLine = qQCAReqBuyerCreditOutstandingGovs1[3] != null ? qQCAReqBuyerCreditOutstandingGovs1[3].ApprovedCreditLimitLine : 0,
                    QCAReqBuyerCreditOutstandingGov4_ARBalance = qQCAReqBuyerCreditOutstandingGovs1[3] != null ? qQCAReqBuyerCreditOutstandingGovs1[3].ARBalance : 0,
                    QCAReqBuyerCreditOutstandingGov4_MaxPurchasePct = qQCAReqBuyerCreditOutstandingGovs1[3] != null ? qQCAReqBuyerCreditOutstandingGovs1[3].MaxPurchasePct : 0,
                    QCAReqBuyerCreditOutstandingGov4_AssignmentMethod = qQCAReqBuyerCreditOutstandingGovs1[3] != null ? qQCAReqBuyerCreditOutstandingGovs1[3].AssignmentMethod : "",
                    QCAReqBuyerCreditOutstandingGov4_BillingResponsibleBy = qQCAReqBuyerCreditOutstandingGovs1[3] != null ? qQCAReqBuyerCreditOutstandingGovs1[3].BillingResponsibleBy : "",
                    QCAReqBuyerCreditOutstandingGov4_MethodOfPayment = qQCAReqBuyerCreditOutstandingGovs1[3] != null ? qQCAReqBuyerCreditOutstandingGovs1[3].MethodOfPayment : "",

                    QCAReqBuyerCreditOutstandingGov5_BuyerCreditOutstandingBuyerName = qQCAReqBuyerCreditOutstandingGovs1[4] != null ? qQCAReqBuyerCreditOutstandingGovs1[4].BuyerCreditOutstandingBuyerName : "",
                    QCAReqBuyerCreditOutstandingGov5_Status = qQCAReqBuyerCreditOutstandingGovs1[4] != null ? qQCAReqBuyerCreditOutstandingGovs1[4].Status : "",
                    QCAReqBuyerCreditOutstandingGov5_ApprovedCreditLimitLine = qQCAReqBuyerCreditOutstandingGovs1[4] != null ? qQCAReqBuyerCreditOutstandingGovs1[4].ApprovedCreditLimitLine : 0,
                    QCAReqBuyerCreditOutstandingGov5_ARBalance = qQCAReqBuyerCreditOutstandingGovs1[4] != null ? qQCAReqBuyerCreditOutstandingGovs1[4].ARBalance : 0,
                    QCAReqBuyerCreditOutstandingGov5_MaxPurchasePct = qQCAReqBuyerCreditOutstandingGovs1[4] != null ? qQCAReqBuyerCreditOutstandingGovs1[4].MaxPurchasePct : 0,
                    QCAReqBuyerCreditOutstandingGov5_AssignmentMethod = qQCAReqBuyerCreditOutstandingGovs1[4] != null ? qQCAReqBuyerCreditOutstandingGovs1[4].AssignmentMethod : "",
                    QCAReqBuyerCreditOutstandingGov5_BillingResponsibleBy = qQCAReqBuyerCreditOutstandingGovs1[4] != null ? qQCAReqBuyerCreditOutstandingGovs1[4].BillingResponsibleBy : "",
                    QCAReqBuyerCreditOutstandingGov5_MethodOfPayment = qQCAReqBuyerCreditOutstandingGovs1[4] != null ? qQCAReqBuyerCreditOutstandingGovs1[4].MethodOfPayment : "",

                    QCAReqBuyerCreditOutstandingGov6_BuyerCreditOutstandingBuyerName = qQCAReqBuyerCreditOutstandingGovs1[5] != null ? qQCAReqBuyerCreditOutstandingGovs1[5].BuyerCreditOutstandingBuyerName : "",
                    QCAReqBuyerCreditOutstandingGov6_Status = qQCAReqBuyerCreditOutstandingGovs1[5] != null ? qQCAReqBuyerCreditOutstandingGovs1[5].Status : "",
                    QCAReqBuyerCreditOutstandingGov6_ApprovedCreditLimitLine = qQCAReqBuyerCreditOutstandingGovs1[5] != null ? qQCAReqBuyerCreditOutstandingGovs1[5].ApprovedCreditLimitLine : 0,
                    QCAReqBuyerCreditOutstandingGov6_ARBalance = qQCAReqBuyerCreditOutstandingGovs1[5] != null ? qQCAReqBuyerCreditOutstandingGovs1[5].ARBalance : 0,
                    QCAReqBuyerCreditOutstandingGov6_MaxPurchasePct = qQCAReqBuyerCreditOutstandingGovs1[5] != null ? qQCAReqBuyerCreditOutstandingGovs1[5].MaxPurchasePct : 0,
                    QCAReqBuyerCreditOutstandingGov6_AssignmentMethod = qQCAReqBuyerCreditOutstandingGovs1[5] != null ? qQCAReqBuyerCreditOutstandingGovs1[5].AssignmentMethod : "",
                    QCAReqBuyerCreditOutstandingGov6_BillingResponsibleBy = qQCAReqBuyerCreditOutstandingGovs1[5] != null ? qQCAReqBuyerCreditOutstandingGovs1[5].BillingResponsibleBy : "",
                    QCAReqBuyerCreditOutstandingGov6_MethodOfPayment = qQCAReqBuyerCreditOutstandingGovs1[5] != null ? qQCAReqBuyerCreditOutstandingGovs1[5].MethodOfPayment : "",

                    QCAReqBuyerCreditOutstandingGov7_BuyerCreditOutstandingBuyerName = qQCAReqBuyerCreditOutstandingGovs1[6] != null ? qQCAReqBuyerCreditOutstandingGovs1[6].BuyerCreditOutstandingBuyerName : "",
                    QCAReqBuyerCreditOutstandingGov7_Status = qQCAReqBuyerCreditOutstandingGovs1[6] != null ? qQCAReqBuyerCreditOutstandingGovs1[6].Status : "",
                    QCAReqBuyerCreditOutstandingGov7_ApprovedCreditLimitLine = qQCAReqBuyerCreditOutstandingGovs1[6] != null ? qQCAReqBuyerCreditOutstandingGovs1[6].ApprovedCreditLimitLine : 0,
                    QCAReqBuyerCreditOutstandingGov7_ARBalance = qQCAReqBuyerCreditOutstandingGovs1[6] != null ? qQCAReqBuyerCreditOutstandingGovs1[6].ARBalance : 0,
                    QCAReqBuyerCreditOutstandingGov7_MaxPurchasePct = qQCAReqBuyerCreditOutstandingGovs1[6] != null ? qQCAReqBuyerCreditOutstandingGovs1[6].MaxPurchasePct : 0,
                    QCAReqBuyerCreditOutstandingGov7_AssignmentMethod = qQCAReqBuyerCreditOutstandingGovs1[6] != null ? qQCAReqBuyerCreditOutstandingGovs1[6].AssignmentMethod : "",
                    QCAReqBuyerCreditOutstandingGov7_BillingResponsibleBy = qQCAReqBuyerCreditOutstandingGovs1[6] != null ? qQCAReqBuyerCreditOutstandingGovs1[6].BillingResponsibleBy : "",
                    QCAReqBuyerCreditOutstandingGov7_MethodOfPayment = qQCAReqBuyerCreditOutstandingGovs1[6] != null ? qQCAReqBuyerCreditOutstandingGovs1[6].MethodOfPayment : "",

                    QCAReqBuyerCreditOutstandingGov8_BuyerCreditOutstandingBuyerName = qQCAReqBuyerCreditOutstandingGovs1[7] != null ? qQCAReqBuyerCreditOutstandingGovs1[7].BuyerCreditOutstandingBuyerName : "",
                    QCAReqBuyerCreditOutstandingGov8_Status = qQCAReqBuyerCreditOutstandingGovs1[7] != null ? qQCAReqBuyerCreditOutstandingGovs1[7].Status : "",
                    QCAReqBuyerCreditOutstandingGov8_ApprovedCreditLimitLine = qQCAReqBuyerCreditOutstandingGovs1[7] != null ? qQCAReqBuyerCreditOutstandingGovs1[7].ApprovedCreditLimitLine : 0,
                    QCAReqBuyerCreditOutstandingGov8_ARBalance = qQCAReqBuyerCreditOutstandingGovs1[7] != null ? qQCAReqBuyerCreditOutstandingGovs1[7].ARBalance : 0,
                    QCAReqBuyerCreditOutstandingGov8_MaxPurchasePct = qQCAReqBuyerCreditOutstandingGovs1[7] != null ? qQCAReqBuyerCreditOutstandingGovs1[7].MaxPurchasePct : 0,
                    QCAReqBuyerCreditOutstandingGov8_AssignmentMethod = qQCAReqBuyerCreditOutstandingGovs1[7] != null ? qQCAReqBuyerCreditOutstandingGovs1[7].AssignmentMethod : "",
                    QCAReqBuyerCreditOutstandingGov8_BillingResponsibleBy = qQCAReqBuyerCreditOutstandingGovs1[7] != null ? qQCAReqBuyerCreditOutstandingGovs1[7].BillingResponsibleBy : "",
                    QCAReqBuyerCreditOutstandingGov8_MethodOfPayment = qQCAReqBuyerCreditOutstandingGovs1[7] != null ? qQCAReqBuyerCreditOutstandingGovs1[7].MethodOfPayment : "",

                    QCAReqBuyerCreditOutstandingGov9_BuyerCreditOutstandingBuyerName = qQCAReqBuyerCreditOutstandingGovs1[8] != null ? qQCAReqBuyerCreditOutstandingGovs1[8].BuyerCreditOutstandingBuyerName : "",
                    QCAReqBuyerCreditOutstandingGov9_Status = qQCAReqBuyerCreditOutstandingGovs1[8] != null ? qQCAReqBuyerCreditOutstandingGovs1[8].Status : "",
                    QCAReqBuyerCreditOutstandingGov9_ApprovedCreditLimitLine = qQCAReqBuyerCreditOutstandingGovs1[8] != null ? qQCAReqBuyerCreditOutstandingGovs1[8].ApprovedCreditLimitLine : 0,
                    QCAReqBuyerCreditOutstandingGov9_ARBalance = qQCAReqBuyerCreditOutstandingGovs1[8] != null ? qQCAReqBuyerCreditOutstandingGovs1[8].ARBalance : 0,
                    QCAReqBuyerCreditOutstandingGov9_MaxPurchasePct = qQCAReqBuyerCreditOutstandingGovs1[8] != null ? qQCAReqBuyerCreditOutstandingGovs1[8].MaxPurchasePct : 0,
                    QCAReqBuyerCreditOutstandingGov9_AssignmentMethod = qQCAReqBuyerCreditOutstandingGovs1[8] != null ? qQCAReqBuyerCreditOutstandingGovs1[8].AssignmentMethod : "",
                    QCAReqBuyerCreditOutstandingGov9_BillingResponsibleBy = qQCAReqBuyerCreditOutstandingGovs1[8] != null ? qQCAReqBuyerCreditOutstandingGovs1[8].BillingResponsibleBy : "",
                    QCAReqBuyerCreditOutstandingGov9_MethodOfPayment = qQCAReqBuyerCreditOutstandingGovs1[8] != null ? qQCAReqBuyerCreditOutstandingGovs1[8].MethodOfPayment : "",

                    QCAReqBuyerCreditOutstandingGov10_BuyerCreditOutstandingBuyerName = qQCAReqBuyerCreditOutstandingGovs1[9] != null ? qQCAReqBuyerCreditOutstandingGovs1[9].BuyerCreditOutstandingBuyerName : "",
                    QCAReqBuyerCreditOutstandingGov10_Status = qQCAReqBuyerCreditOutstandingGovs1[9] != null ? qQCAReqBuyerCreditOutstandingGovs1[9].Status : "",
                    QCAReqBuyerCreditOutstandingGov10_ApprovedCreditLimitLine = qQCAReqBuyerCreditOutstandingGovs1[9] != null ? qQCAReqBuyerCreditOutstandingGovs1[9].ApprovedCreditLimitLine : 0,
                    QCAReqBuyerCreditOutstandingGov10_ARBalance = qQCAReqBuyerCreditOutstandingGovs1[9] != null ? qQCAReqBuyerCreditOutstandingGovs1[9].ARBalance : 0,
                    QCAReqBuyerCreditOutstandingGov10_MaxPurchasePct = qQCAReqBuyerCreditOutstandingGovs1[9] != null ? qQCAReqBuyerCreditOutstandingGovs1[9].MaxPurchasePct : 0,
                    QCAReqBuyerCreditOutstandingGov10_AssignmentMethod = qQCAReqBuyerCreditOutstandingGovs1[9] != null ? qQCAReqBuyerCreditOutstandingGovs1[9].AssignmentMethod : "",
                    QCAReqBuyerCreditOutstandingGov10_BillingResponsibleBy = qQCAReqBuyerCreditOutstandingGovs1[9] != null ? qQCAReqBuyerCreditOutstandingGovs1[9].BillingResponsibleBy : "",
                    QCAReqBuyerCreditOutstandingGov10_MethodOfPayment = qQCAReqBuyerCreditOutstandingGovs1[9] != null ? qQCAReqBuyerCreditOutstandingGovs1[9].MethodOfPayment : "",

                    QCreditAppReqAssignmentOutstanding1_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[0] != null ? qCreditAppReqAssignmentOutstanding[0].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding1_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[0] != null ? (qCreditAppReqAssignmentOutstanding[0].AssignmentRefAgreementId != null ? qCreditAppReqAssignmentOutstanding[0].AssignmentRefAgreementId : "") : "",
					QCreditAppReqAssignmentOutstanding1_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[0] != null ? qCreditAppReqAssignmentOutstanding[0].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding1_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[0] != null ? qCreditAppReqAssignmentOutstanding[0].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding1_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[0] != null ? qCreditAppReqAssignmentOutstanding[0].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding1_RemainingAmount = qCreditAppReqAssignmentOutstanding[0] != null ? qCreditAppReqAssignmentOutstanding[0].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding1_Description = qCreditAppReqAssignmentOutstanding[0] != null ? qCreditAppReqAssignmentOutstanding[0].Description : "",

					QCreditAppReqAssignmentOutstanding2_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[1] != null ? qCreditAppReqAssignmentOutstanding[1].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding2_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[1] != null ? (qCreditAppReqAssignmentOutstanding[1].AssignmentRefAgreementId != null ? qCreditAppReqAssignmentOutstanding[1].AssignmentRefAgreementId : "") : "",
					QCreditAppReqAssignmentOutstanding2_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[1] != null ? qCreditAppReqAssignmentOutstanding[1].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding2_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[1] != null ? qCreditAppReqAssignmentOutstanding[1].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding2_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[1] != null ? qCreditAppReqAssignmentOutstanding[1].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding2_RemainingAmount = qCreditAppReqAssignmentOutstanding[1] != null ? qCreditAppReqAssignmentOutstanding[1].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding2_Description = qCreditAppReqAssignmentOutstanding[1] != null ? qCreditAppReqAssignmentOutstanding[1].Description : "",

					QCreditAppReqAssignmentOutstanding3_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[2] != null ? qCreditAppReqAssignmentOutstanding[2].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding3_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[2] != null ? (qCreditAppReqAssignmentOutstanding[2].AssignmentRefAgreementId != null ? qCreditAppReqAssignmentOutstanding[2].AssignmentRefAgreementId : "") : "",
					QCreditAppReqAssignmentOutstanding3_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[2] != null ? qCreditAppReqAssignmentOutstanding[2].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding3_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[2] != null ? qCreditAppReqAssignmentOutstanding[2].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding3_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[2] != null ? qCreditAppReqAssignmentOutstanding[2].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding3_RemainingAmount = qCreditAppReqAssignmentOutstanding[2] != null ? qCreditAppReqAssignmentOutstanding[2].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding3_Description = qCreditAppReqAssignmentOutstanding[2] != null ? qCreditAppReqAssignmentOutstanding[2].Description : "",

					QCreditAppReqAssignmentOutstanding4_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[3] != null ? qCreditAppReqAssignmentOutstanding[3].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding4_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[3] != null ? (qCreditAppReqAssignmentOutstanding[3].AssignmentRefAgreementId != null ? qCreditAppReqAssignmentOutstanding[3].AssignmentRefAgreementId : "") : "",
					QCreditAppReqAssignmentOutstanding4_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[3] != null ? qCreditAppReqAssignmentOutstanding[3].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding4_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[3] != null ? qCreditAppReqAssignmentOutstanding[3].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding4_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[3] != null ? qCreditAppReqAssignmentOutstanding[3].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding4_RemainingAmount = qCreditAppReqAssignmentOutstanding[3] != null ? qCreditAppReqAssignmentOutstanding[3].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding4_Description = qCreditAppReqAssignmentOutstanding[3] != null ? qCreditAppReqAssignmentOutstanding[3].Description : "",

					QCreditAppReqAssignmentOutstanding5_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[4] != null ? qCreditAppReqAssignmentOutstanding[4].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding5_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[4] != null ? (qCreditAppReqAssignmentOutstanding[4].AssignmentRefAgreementId != null ? qCreditAppReqAssignmentOutstanding[4].AssignmentRefAgreementId : "") : "",
					QCreditAppReqAssignmentOutstanding5_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[4] != null ? qCreditAppReqAssignmentOutstanding[4].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding5_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[4] != null ? qCreditAppReqAssignmentOutstanding[4].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding5_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[4] != null ? qCreditAppReqAssignmentOutstanding[4].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding5_RemainingAmount = qCreditAppReqAssignmentOutstanding[4] != null ? qCreditAppReqAssignmentOutstanding[4].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding5_Description = qCreditAppReqAssignmentOutstanding[4] != null ? qCreditAppReqAssignmentOutstanding[4].Description : "",

					QCreditAppReqAssignmentOutstanding6_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[5] != null ? qCreditAppReqAssignmentOutstanding[5].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding6_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[5] != null ? (qCreditAppReqAssignmentOutstanding[5].AssignmentRefAgreementId != null ? qCreditAppReqAssignmentOutstanding[5].AssignmentRefAgreementId : "") : "",
					QCreditAppReqAssignmentOutstanding6_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[5] != null ? qCreditAppReqAssignmentOutstanding[5].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding6_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[5] != null ? qCreditAppReqAssignmentOutstanding[5].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding6_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[5] != null ? qCreditAppReqAssignmentOutstanding[5].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding6_RemainingAmount = qCreditAppReqAssignmentOutstanding[5] != null ? qCreditAppReqAssignmentOutstanding[5].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding6_Description = qCreditAppReqAssignmentOutstanding[5] != null ? qCreditAppReqAssignmentOutstanding[5].Description : "",

					QCreditAppReqAssignmentOutstanding7_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[6] != null ? qCreditAppReqAssignmentOutstanding[6].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding7_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[6] != null ? (qCreditAppReqAssignmentOutstanding[6].AssignmentRefAgreementId != null ? qCreditAppReqAssignmentOutstanding[6].AssignmentRefAgreementId : "") : "",
					QCreditAppReqAssignmentOutstanding7_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[6] != null ? qCreditAppReqAssignmentOutstanding[6].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding7_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[6] != null ? qCreditAppReqAssignmentOutstanding[6].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding7_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[6] != null ? qCreditAppReqAssignmentOutstanding[6].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding7_RemainingAmount = qCreditAppReqAssignmentOutstanding[6] != null ? qCreditAppReqAssignmentOutstanding[6].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding7_Description = qCreditAppReqAssignmentOutstanding[6] != null ? qCreditAppReqAssignmentOutstanding[6].Description : "",

					QCreditAppReqAssignmentOutstanding8_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[7] != null ? qCreditAppReqAssignmentOutstanding[7].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding8_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[7] != null ? (qCreditAppReqAssignmentOutstanding[7].AssignmentRefAgreementId != null ? qCreditAppReqAssignmentOutstanding[7].AssignmentRefAgreementId : "") : "",
					QCreditAppReqAssignmentOutstanding8_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[7] != null ? qCreditAppReqAssignmentOutstanding[7].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding8_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[7] != null ? qCreditAppReqAssignmentOutstanding[7].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding8_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[7] != null ? qCreditAppReqAssignmentOutstanding[7].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding8_RemainingAmount = qCreditAppReqAssignmentOutstanding[7] != null ? qCreditAppReqAssignmentOutstanding[7].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding8_Description = qCreditAppReqAssignmentOutstanding[7] != null ? qCreditAppReqAssignmentOutstanding[7].Description : "",

					QCreditAppReqAssignmentOutstanding9_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[8] != null ? qCreditAppReqAssignmentOutstanding[8].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding9_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[8] != null ? qCreditAppReqAssignmentOutstanding[8].AssignmentRefAgreementId : "",
					QCreditAppReqAssignmentOutstanding9_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[8] != null ? qCreditAppReqAssignmentOutstanding[8].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding9_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[8] != null ? qCreditAppReqAssignmentOutstanding[8].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding9_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[8] != null ? qCreditAppReqAssignmentOutstanding[8].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding9_RemainingAmount = qCreditAppReqAssignmentOutstanding[8] != null ? qCreditAppReqAssignmentOutstanding[8].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding9_Description = qCreditAppReqAssignmentOutstanding[8] != null ? qCreditAppReqAssignmentOutstanding[8].Description : "",

					QCreditAppReqAssignmentOutstanding10_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[9] != null ? qCreditAppReqAssignmentOutstanding[9].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding10_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[9] != null ? qCreditAppReqAssignmentOutstanding[9].AssignmentRefAgreementId : "",
					QCreditAppReqAssignmentOutstanding10_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[9] != null ? qCreditAppReqAssignmentOutstanding[9].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding10_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[9] != null ? qCreditAppReqAssignmentOutstanding[9].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding10_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[9] != null ? qCreditAppReqAssignmentOutstanding[9].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding10_RemainingAmount = qCreditAppReqAssignmentOutstanding[9] != null ? qCreditAppReqAssignmentOutstanding[9].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding10_Description = qCreditAppReqAssignmentOutstanding[9] != null ? qCreditAppReqAssignmentOutstanding[9].Description : "",

					QCreditAppReqAssignmentOutstanding11_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[10] != null ? qCreditAppReqAssignmentOutstanding[10].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding11_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[10] != null ? qCreditAppReqAssignmentOutstanding[10].AssignmentRefAgreementId : "",
					QCreditAppReqAssignmentOutstanding11_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[10] != null ? qCreditAppReqAssignmentOutstanding[10].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding11_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[10] != null ? qCreditAppReqAssignmentOutstanding[10].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding11_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[10] != null ? qCreditAppReqAssignmentOutstanding[10].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding11_RemainingAmount = qCreditAppReqAssignmentOutstanding[10] != null ? qCreditAppReqAssignmentOutstanding[10].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding11_Description = qCreditAppReqAssignmentOutstanding[10] != null ? qCreditAppReqAssignmentOutstanding[10].Description : "",

					QCreditAppReqAssignmentOutstanding12_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[11] != null ? qCreditAppReqAssignmentOutstanding[11].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding12_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[11] != null ? qCreditAppReqAssignmentOutstanding[11].AssignmentRefAgreementId : "",
					QCreditAppReqAssignmentOutstanding12_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[11] != null ? qCreditAppReqAssignmentOutstanding[11].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding12_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[11] != null ? qCreditAppReqAssignmentOutstanding[11].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding12_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[11] != null ? qCreditAppReqAssignmentOutstanding[11].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding12_RemainingAmount = qCreditAppReqAssignmentOutstanding[11] != null ? qCreditAppReqAssignmentOutstanding[11].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding12_Description = qCreditAppReqAssignmentOutstanding[11] != null ? qCreditAppReqAssignmentOutstanding[11].Description : "",

					QCreditAppReqAssignmentOutstanding13_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[12] != null ? qCreditAppReqAssignmentOutstanding[12].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding13_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[12] != null ? qCreditAppReqAssignmentOutstanding[12].AssignmentRefAgreementId : "",
					QCreditAppReqAssignmentOutstanding13_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[12] != null ? qCreditAppReqAssignmentOutstanding[12].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding13_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[12] != null ? qCreditAppReqAssignmentOutstanding[12].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding13_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[12] != null ? qCreditAppReqAssignmentOutstanding[12].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding13_RemainingAmount = qCreditAppReqAssignmentOutstanding[12] != null ? qCreditAppReqAssignmentOutstanding[12].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding13_Description = qCreditAppReqAssignmentOutstanding[12] != null ? qCreditAppReqAssignmentOutstanding[12].Description : "",

					QCreditAppReqAssignmentOutstanding14_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[13] != null ? qCreditAppReqAssignmentOutstanding[13].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding14_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[13] != null ? qCreditAppReqAssignmentOutstanding[13].AssignmentRefAgreementId : "",
					QCreditAppReqAssignmentOutstanding14_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[13] != null ? qCreditAppReqAssignmentOutstanding[13].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding14_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[13] != null ? qCreditAppReqAssignmentOutstanding[13].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding14_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[13] != null ? qCreditAppReqAssignmentOutstanding[13].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding14_RemainingAmount = qCreditAppReqAssignmentOutstanding[13] != null ? qCreditAppReqAssignmentOutstanding[13].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding14_Description = qCreditAppReqAssignmentOutstanding[13] != null ? qCreditAppReqAssignmentOutstanding[13].Description : "",

					QCreditAppReqAssignmentOutstanding15_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[14] != null ? qCreditAppReqAssignmentOutstanding[14].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding15_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[14] != null ? qCreditAppReqAssignmentOutstanding[14].AssignmentRefAgreementId : "",
					QCreditAppReqAssignmentOutstanding15_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[14] != null ? qCreditAppReqAssignmentOutstanding[14].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding15_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[14] != null ? qCreditAppReqAssignmentOutstanding[14].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding15_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[14] != null ? qCreditAppReqAssignmentOutstanding[14].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding15_RemainingAmount = qCreditAppReqAssignmentOutstanding[14] != null ? qCreditAppReqAssignmentOutstanding[14].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding15_Description = qCreditAppReqAssignmentOutstanding[14] != null ? qCreditAppReqAssignmentOutstanding[14].Description : "",

					QCreditAppReqAssignmentOutstanding16_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[15] != null ? qCreditAppReqAssignmentOutstanding[15].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding16_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[15] != null ? qCreditAppReqAssignmentOutstanding[15].AssignmentRefAgreementId : "",
					QCreditAppReqAssignmentOutstanding16_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[15] != null ? qCreditAppReqAssignmentOutstanding[15].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding16_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[15] != null ? qCreditAppReqAssignmentOutstanding[15].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding16_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[15] != null ? qCreditAppReqAssignmentOutstanding[15].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding16_RemainingAmount = qCreditAppReqAssignmentOutstanding[15] != null ? qCreditAppReqAssignmentOutstanding[15].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding16_Description = qCreditAppReqAssignmentOutstanding[15] != null ? qCreditAppReqAssignmentOutstanding[15].Description : "",

					QCreditAppReqAssignmentOutstanding17_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[16] != null ? qCreditAppReqAssignmentOutstanding[16].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding17_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[16] != null ? qCreditAppReqAssignmentOutstanding[16].AssignmentRefAgreementId : "",
					QCreditAppReqAssignmentOutstanding17_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[16] != null ? qCreditAppReqAssignmentOutstanding[16].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding17_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[16] != null ? qCreditAppReqAssignmentOutstanding[16].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding17_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[16] != null ? qCreditAppReqAssignmentOutstanding[16].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding17_RemainingAmount = qCreditAppReqAssignmentOutstanding[16] != null ? qCreditAppReqAssignmentOutstanding[16].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding17_Description = qCreditAppReqAssignmentOutstanding[16] != null ? qCreditAppReqAssignmentOutstanding[16].Description : "",

					QCreditAppReqAssignmentOutstanding18_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[17] != null ? qCreditAppReqAssignmentOutstanding[17].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding18_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[17] != null ? qCreditAppReqAssignmentOutstanding[17].AssignmentRefAgreementId : "",
					QCreditAppReqAssignmentOutstanding18_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[17] != null ? qCreditAppReqAssignmentOutstanding[17].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding18_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[17] != null ? qCreditAppReqAssignmentOutstanding[17].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding18_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[17] != null ? qCreditAppReqAssignmentOutstanding[17].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding18_RemainingAmount = qCreditAppReqAssignmentOutstanding[17] != null ? qCreditAppReqAssignmentOutstanding[17].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding18_Description = qCreditAppReqAssignmentOutstanding[17] != null ? qCreditAppReqAssignmentOutstanding[17].Description : "",

					QCreditAppReqAssignmentOutstanding19_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[18] != null ? qCreditAppReqAssignmentOutstanding[18].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding19_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[18] != null ? qCreditAppReqAssignmentOutstanding[18].AssignmentRefAgreementId : "",
					QCreditAppReqAssignmentOutstanding19_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[18] != null ? qCreditAppReqAssignmentOutstanding[18].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding19_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[18] != null ? qCreditAppReqAssignmentOutstanding[18].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding19_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[18] != null ? qCreditAppReqAssignmentOutstanding[18].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding19_RemainingAmount = qCreditAppReqAssignmentOutstanding[18] != null ? qCreditAppReqAssignmentOutstanding[18].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding19_Description = qCreditAppReqAssignmentOutstanding[18] != null ? qCreditAppReqAssignmentOutstanding[18].Description : "",

					QCreditAppReqAssignmentOutstanding20_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[19] != null ? qCreditAppReqAssignmentOutstanding[19].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding20_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[19] != null ? qCreditAppReqAssignmentOutstanding[19].AssignmentRefAgreementId : "",
					QCreditAppReqAssignmentOutstanding20_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[19] != null ? qCreditAppReqAssignmentOutstanding[19].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding20_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[19] != null ? qCreditAppReqAssignmentOutstanding[19].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding20_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[19] != null ? qCreditAppReqAssignmentOutstanding[19].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding20_RemainingAmount = qCreditAppReqAssignmentOutstanding[19] != null ? qCreditAppReqAssignmentOutstanding[19].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding20_Description = qCreditAppReqAssignmentOutstanding[19] != null ? qCreditAppReqAssignmentOutstanding[19].Description : "",

					QCreditAppReqAssignmentOutstanding21_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[20] != null ? qCreditAppReqAssignmentOutstanding[20].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding21_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[20] != null ? qCreditAppReqAssignmentOutstanding[20].AssignmentRefAgreementId : "",
					QCreditAppReqAssignmentOutstanding21_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[20] != null ? qCreditAppReqAssignmentOutstanding[20].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding21_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[20] != null ? qCreditAppReqAssignmentOutstanding[20].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding21_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[20] != null ? qCreditAppReqAssignmentOutstanding[20].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding21_RemainingAmount = qCreditAppReqAssignmentOutstanding[20] != null ? qCreditAppReqAssignmentOutstanding[20].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding21_Description = qCreditAppReqAssignmentOutstanding[20] != null ? qCreditAppReqAssignmentOutstanding[20].Description : "",

					QCreditAppReqAssignmentOutstanding22_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[21] != null ? qCreditAppReqAssignmentOutstanding[21].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding22_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[21] != null ? qCreditAppReqAssignmentOutstanding[21].AssignmentRefAgreementId : "",
					QCreditAppReqAssignmentOutstanding22_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[21] != null ? qCreditAppReqAssignmentOutstanding[21].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding22_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[21] != null ? qCreditAppReqAssignmentOutstanding[21].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding22_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[21] != null ? qCreditAppReqAssignmentOutstanding[21].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding22_RemainingAmount = qCreditAppReqAssignmentOutstanding[21] != null ? qCreditAppReqAssignmentOutstanding[21].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding22_Description = qCreditAppReqAssignmentOutstanding[21] != null ? qCreditAppReqAssignmentOutstanding[21].Description : "",

					QCreditAppReqAssignmentOutstanding23_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[22] != null ? qCreditAppReqAssignmentOutstanding[22].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding23_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[22] != null ? qCreditAppReqAssignmentOutstanding[22].AssignmentRefAgreementId : "",
					QCreditAppReqAssignmentOutstanding23_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[22] != null ? qCreditAppReqAssignmentOutstanding[22].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding23_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[22] != null ? qCreditAppReqAssignmentOutstanding[22].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding23_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[22] != null ? qCreditAppReqAssignmentOutstanding[22].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding23_RemainingAmount = qCreditAppReqAssignmentOutstanding[22] != null ? qCreditAppReqAssignmentOutstanding[22].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding23_Description = qCreditAppReqAssignmentOutstanding[22] != null ? qCreditAppReqAssignmentOutstanding[22].Description : "",

					QCreditAppReqAssignmentOutstanding24_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[23] != null ? qCreditAppReqAssignmentOutstanding[23].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding24_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[23] != null ? qCreditAppReqAssignmentOutstanding[23].AssignmentRefAgreementId : "",
					QCreditAppReqAssignmentOutstanding24_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[23] != null ? qCreditAppReqAssignmentOutstanding[23].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding24_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[23] != null ? qCreditAppReqAssignmentOutstanding[23].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding24_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[23] != null ? qCreditAppReqAssignmentOutstanding[23].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding24_RemainingAmount = qCreditAppReqAssignmentOutstanding[23] != null ? qCreditAppReqAssignmentOutstanding[23].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding24_Description = qCreditAppReqAssignmentOutstanding[23] != null ? qCreditAppReqAssignmentOutstanding[23].Description : "",

					QCreditAppReqAssignmentOutstanding25_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[24] != null ? qCreditAppReqAssignmentOutstanding[24].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding25_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[24] != null ? qCreditAppReqAssignmentOutstanding[24].AssignmentRefAgreementId : "",
					QCreditAppReqAssignmentOutstanding25_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[24] != null ? qCreditAppReqAssignmentOutstanding[24].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding25_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[24] != null ? qCreditAppReqAssignmentOutstanding[24].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding25_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[24] != null ? qCreditAppReqAssignmentOutstanding[24].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding25_RemainingAmount = qCreditAppReqAssignmentOutstanding[24] != null ? qCreditAppReqAssignmentOutstanding[24].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding25_Description = qCreditAppReqAssignmentOutstanding[24] != null ? qCreditAppReqAssignmentOutstanding[24].Description : "",

					QCreditAppReqAssignmentOutstanding26_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[25] != null ? qCreditAppReqAssignmentOutstanding[25].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding26_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[25] != null ? qCreditAppReqAssignmentOutstanding[25].AssignmentRefAgreementId : "",
					QCreditAppReqAssignmentOutstanding26_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[25] != null ? qCreditAppReqAssignmentOutstanding[25].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding26_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[25] != null ? qCreditAppReqAssignmentOutstanding[25].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding26_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[25] != null ? qCreditAppReqAssignmentOutstanding[25].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding26_RemainingAmount = qCreditAppReqAssignmentOutstanding[25] != null ? qCreditAppReqAssignmentOutstanding[25].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding26_Description = qCreditAppReqAssignmentOutstanding[25] != null ? qCreditAppReqAssignmentOutstanding[25].Description : "",

					QCreditAppReqAssignmentOutstanding27_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[26] != null ? qCreditAppReqAssignmentOutstanding[26].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding27_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[26] != null ? qCreditAppReqAssignmentOutstanding[26].AssignmentRefAgreementId : "",
					QCreditAppReqAssignmentOutstanding27_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[26] != null ? qCreditAppReqAssignmentOutstanding[26].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding27_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[26] != null ? qCreditAppReqAssignmentOutstanding[26].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding27_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[26] != null ? qCreditAppReqAssignmentOutstanding[26].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding27_RemainingAmount = qCreditAppReqAssignmentOutstanding[26] != null ? qCreditAppReqAssignmentOutstanding[26].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding27_Description = qCreditAppReqAssignmentOutstanding[26] != null ? qCreditAppReqAssignmentOutstanding[26].Description : "",

					QCreditAppReqAssignmentOutstanding28_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[27] != null ? qCreditAppReqAssignmentOutstanding[27].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding28_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[27] != null ? qCreditAppReqAssignmentOutstanding[27].AssignmentRefAgreementId : "",
					QCreditAppReqAssignmentOutstanding28_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[27] != null ? qCreditAppReqAssignmentOutstanding[27].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding28_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[27] != null ? qCreditAppReqAssignmentOutstanding[27].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding28_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[27] != null ? qCreditAppReqAssignmentOutstanding[27].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding28_RemainingAmount = qCreditAppReqAssignmentOutstanding[27] != null ? qCreditAppReqAssignmentOutstanding[27].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding28_Description = qCreditAppReqAssignmentOutstanding[27] != null ? qCreditAppReqAssignmentOutstanding[27].Description : "",

					QCreditAppReqAssignmentOutstanding29_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[28] != null ? qCreditAppReqAssignmentOutstanding[28].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding29_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[28] != null ? qCreditAppReqAssignmentOutstanding[28].AssignmentRefAgreementId : "",
					QCreditAppReqAssignmentOutstanding29_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[28] != null ? qCreditAppReqAssignmentOutstanding[28].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding29_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[28] != null ? qCreditAppReqAssignmentOutstanding[28].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding29_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[28] != null ? qCreditAppReqAssignmentOutstanding[28].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding29_RemainingAmount = qCreditAppReqAssignmentOutstanding[28] != null ? qCreditAppReqAssignmentOutstanding[28].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding29_Description = qCreditAppReqAssignmentOutstanding[28] != null ? qCreditAppReqAssignmentOutstanding[28].Description : "",

					QCreditAppReqAssignmentOutstanding30_AssignmentBuyer = qCreditAppReqAssignmentOutstanding[29] != null ? qCreditAppReqAssignmentOutstanding[29].AssignmentBuyer : "",
					QCreditAppReqAssignmentOutstanding30_AssignmentRefAgreementId = qCreditAppReqAssignmentOutstanding[29] != null ? qCreditAppReqAssignmentOutstanding[29].AssignmentRefAgreementId : "",
					QCreditAppReqAssignmentOutstanding30_AssignmentBuyerAgreementAmount = qCreditAppReqAssignmentOutstanding[29] != null ? qCreditAppReqAssignmentOutstanding[29].AssignmentBuyerAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding30_AssignmentAgreementAmount = qCreditAppReqAssignmentOutstanding[29] != null ? qCreditAppReqAssignmentOutstanding[29].AssignmentAgreementAmount : 0,
					QCreditAppReqAssignmentOutstanding30_AssignmentAgreementDate = qCreditAppReqAssignmentOutstanding[29] != null ? qCreditAppReqAssignmentOutstanding[29].AssignmentAgreementDate : "",
					QCreditAppReqAssignmentOutstanding30_RemainingAmount = qCreditAppReqAssignmentOutstanding[29] != null ? qCreditAppReqAssignmentOutstanding[29].RemainingAmount : 0,
					QCreditAppReqAssignmentOutstanding30_Description = qCreditAppReqAssignmentOutstanding[29] != null ? qCreditAppReqAssignmentOutstanding[29].Description : "",

					QSumCreditAppReqAssignment_SumAssignmentBuyerAgreementAmount = qSumCreditAppReqAssignmentOutstanding != null ? qSumCreditAppReqAssignmentOutstanding.SumAssignmentBuyerAgreementAmount : 0,
					QSumCreditAppReqAssignment_SumAssignmentAgreementAmount = qSumCreditAppReqAssignmentOutstanding != null ? qSumCreditAppReqAssignmentOutstanding.SumAssignmentAgreementAmount : 0,
					QSumCreditAppReqAssignment_SumAssignmentRemainingAmount = qSumCreditAppReqAssignmentOutstanding != null ? qSumCreditAppReqAssignmentOutstanding.SumRemainingAmount : 0,

					QCustBusinessCollateral1_BusinessCollateralType = qQCustBusinessCollaterals[0] != null ? qQCustBusinessCollaterals[0].BusinessCollateralType : "",
					QCustBusinessCollateral1_BusinessCollateralSubType = qQCustBusinessCollaterals[0] != null ? qQCustBusinessCollaterals[0].BusinessCollateralSubType : "",
					QCustBusinessCollateral1_Description = qQCustBusinessCollaterals[0] != null ? qQCustBusinessCollaterals[0].Description : "",
					QCustBusinessCollateral1_BusinessCollateralValue = qQCustBusinessCollaterals[0] != null ? qQCustBusinessCollaterals[0].BusinessCollateralValue : 0,
					QCustBusinessCollateral1_BusinessCollateralStatus = qQCustBusinessCollaterals[0] != null ? qQCustBusinessCollaterals[0].BusinessCollateralStatus : "",

					QCustBusinessCollateral2_BusinessCollateralType = qQCustBusinessCollaterals[1] != null ? qQCustBusinessCollaterals[1].BusinessCollateralType : "",
					QCustBusinessCollateral2_BusinessCollateralSubType = qQCustBusinessCollaterals[1] != null ? qQCustBusinessCollaterals[1].BusinessCollateralSubType : "",
					QCustBusinessCollateral2_Description = qQCustBusinessCollaterals[1] != null ? qQCustBusinessCollaterals[1].Description : "",
					QCustBusinessCollateral2_BusinessCollateralValue = qQCustBusinessCollaterals[1] != null ? qQCustBusinessCollaterals[1].BusinessCollateralValue : 0,
					QCustBusinessCollateral2_BusinessCollateralStatus = qQCustBusinessCollaterals[1] != null ? qQCustBusinessCollaterals[1].BusinessCollateralStatus : "",

					QCustBusinessCollateral3_BusinessCollateralType = qQCustBusinessCollaterals[2] != null ? qQCustBusinessCollaterals[2].BusinessCollateralType : "",
					QCustBusinessCollateral3_BusinessCollateralSubType = qQCustBusinessCollaterals[2] != null ? qQCustBusinessCollaterals[2].BusinessCollateralSubType : "",
					QCustBusinessCollateral3_Description = qQCustBusinessCollaterals[2] != null ? qQCustBusinessCollaterals[2].Description : "",
					QCustBusinessCollateral3_BusinessCollateralValue = qQCustBusinessCollaterals[2] != null ? qQCustBusinessCollaterals[2].BusinessCollateralValue : 0,
					QCustBusinessCollateral3_BusinessCollateralStatus = qQCustBusinessCollaterals[2] != null ? qQCustBusinessCollaterals[2].BusinessCollateralStatus : "",

					QCustBusinessCollateral4_BusinessCollateralType = qQCustBusinessCollaterals[3] != null ? qQCustBusinessCollaterals[3].BusinessCollateralType : "",
					QCustBusinessCollateral4_BusinessCollateralSubType = qQCustBusinessCollaterals[3] != null ? qQCustBusinessCollaterals[3].BusinessCollateralSubType : "",
					QCustBusinessCollateral4_Description = qQCustBusinessCollaterals[3] != null ? qQCustBusinessCollaterals[3].Description : "",
					QCustBusinessCollateral4_BusinessCollateralValue = qQCustBusinessCollaterals[3] != null ? qQCustBusinessCollaterals[3].BusinessCollateralValue : 0,
					QCustBusinessCollateral4_BusinessCollateralStatus = qQCustBusinessCollaterals[3] != null ? qQCustBusinessCollaterals[3].BusinessCollateralStatus : "",

					QCustBusinessCollateral5_BusinessCollateralType = qQCustBusinessCollaterals[4] != null ? qQCustBusinessCollaterals[4].BusinessCollateralType : "",
					QCustBusinessCollateral5_BusinessCollateralSubType = qQCustBusinessCollaterals[4] != null ? qQCustBusinessCollaterals[4].BusinessCollateralSubType : "",
					QCustBusinessCollateral5_Description = qQCustBusinessCollaterals[4] != null ? qQCustBusinessCollaterals[4].Description : "",
					QCustBusinessCollateral5_BusinessCollateralValue = qQCustBusinessCollaterals[4] != null ? qQCustBusinessCollaterals[4].BusinessCollateralValue : 0,
					QCustBusinessCollateral5_BusinessCollateralStatus = qQCustBusinessCollaterals[4] != null ? qQCustBusinessCollaterals[4].BusinessCollateralStatus : "",

					QCustBusinessCollateral6_BusinessCollateralType = qQCustBusinessCollaterals[5] != null ? qQCustBusinessCollaterals[5].BusinessCollateralType : "",
					QCustBusinessCollateral6_BusinessCollateralSubType = qQCustBusinessCollaterals[5] != null ? qQCustBusinessCollaterals[5].BusinessCollateralSubType : "",
					QCustBusinessCollateral6_Description = qQCustBusinessCollaterals[5] != null ? qQCustBusinessCollaterals[5].Description : "",
					QCustBusinessCollateral6_BusinessCollateralValue = qQCustBusinessCollaterals[5] != null ? qQCustBusinessCollaterals[5].BusinessCollateralValue : 0,
					QCustBusinessCollateral6_BusinessCollateralStatus = qQCustBusinessCollaterals[5] != null ? qQCustBusinessCollaterals[5].BusinessCollateralStatus : "",

					QCustBusinessCollateral7_BusinessCollateralType = qQCustBusinessCollaterals[6] != null ? qQCustBusinessCollaterals[6].BusinessCollateralType : "",
					QCustBusinessCollateral7_BusinessCollateralSubType = qQCustBusinessCollaterals[6] != null ? qQCustBusinessCollaterals[6].BusinessCollateralSubType : "",
					QCustBusinessCollateral7_Description = qQCustBusinessCollaterals[6] != null ? qQCustBusinessCollaterals[6].Description : "",
					QCustBusinessCollateral7_BusinessCollateralValue = qQCustBusinessCollaterals[6] != null ? qQCustBusinessCollaterals[6].BusinessCollateralValue : 0,
					QCustBusinessCollateral7_BusinessCollateralStatus = qQCustBusinessCollaterals[6] != null ? qQCustBusinessCollaterals[6].BusinessCollateralStatus : "",

					QCustBusinessCollateral8_BusinessCollateralType = qQCustBusinessCollaterals[7] != null ? qQCustBusinessCollaterals[7].BusinessCollateralType : "",
					QCustBusinessCollateral8_BusinessCollateralSubType = qQCustBusinessCollaterals[7] != null ? qQCustBusinessCollaterals[7].BusinessCollateralSubType : "",
					QCustBusinessCollateral8_Description = qQCustBusinessCollaterals[7] != null ? qQCustBusinessCollaterals[7].Description : "",
					QCustBusinessCollateral8_BusinessCollateralValue = qQCustBusinessCollaterals[7] != null ? qQCustBusinessCollaterals[7].BusinessCollateralValue : 0,
					QCustBusinessCollateral8_BusinessCollateralStatus = qQCustBusinessCollaterals[7] != null ? qQCustBusinessCollaterals[7].BusinessCollateralStatus : "",

					QCustBusinessCollateral9_BusinessCollateralType = qQCustBusinessCollaterals[8] != null ? qQCustBusinessCollaterals[8].BusinessCollateralType : "",
					QCustBusinessCollateral9_BusinessCollateralSubType = qQCustBusinessCollaterals[8] != null ? qQCustBusinessCollaterals[8].BusinessCollateralSubType : "",
					QCustBusinessCollateral9_Description = qQCustBusinessCollaterals[8] != null ? qQCustBusinessCollaterals[8].Description : "",
					QCustBusinessCollateral9_BusinessCollateralValue = qQCustBusinessCollaterals[8] != null ? qQCustBusinessCollaterals[8].BusinessCollateralValue : 0,
					QCustBusinessCollateral9_BusinessCollateralStatus = qQCustBusinessCollaterals[8] != null ? qQCustBusinessCollaterals[8].BusinessCollateralStatus : "",

					QCustBusinessCollateral10_BusinessCollateralType = qQCustBusinessCollaterals[9] != null ? qQCustBusinessCollaterals[9].BusinessCollateralType : "",
					QCustBusinessCollateral10_BusinessCollateralSubType = qQCustBusinessCollaterals[9] != null ? qQCustBusinessCollaterals[9].BusinessCollateralSubType : "",
					QCustBusinessCollateral10_Description = qQCustBusinessCollaterals[9] != null ? qQCustBusinessCollaterals[9].Description : "",
					QCustBusinessCollateral10_BusinessCollateralValue = qQCustBusinessCollaterals[9] != null ? qQCustBusinessCollaterals[9].BusinessCollateralValue : 0,
					QCustBusinessCollateral10_BusinessCollateralStatus = qQCustBusinessCollaterals[9] != null ? qQCustBusinessCollaterals[9].BusinessCollateralStatus : "",

					QCustBusinessCollateral11_BusinessCollateralType = qQCustBusinessCollaterals[10] != null ? qQCustBusinessCollaterals[10].BusinessCollateralType : "",
					QCustBusinessCollateral11_BusinessCollateralSubType = qQCustBusinessCollaterals[10] != null ? qQCustBusinessCollaterals[10].BusinessCollateralSubType : "",
					QCustBusinessCollateral11_Description = qQCustBusinessCollaterals[10] != null ? qQCustBusinessCollaterals[10].Description : "",
					QCustBusinessCollateral11_BusinessCollateralValue = qQCustBusinessCollaterals[10] != null ? qQCustBusinessCollaterals[10].BusinessCollateralValue : 0,
					QCustBusinessCollateral11_BusinessCollateralStatus = qQCustBusinessCollaterals[10] != null ? qQCustBusinessCollaterals[10].BusinessCollateralStatus : "",

					QCustBusinessCollateral12_BusinessCollateralType = qQCustBusinessCollaterals[11] != null ? qQCustBusinessCollaterals[11].BusinessCollateralType : "",
					QCustBusinessCollateral12_BusinessCollateralSubType = qQCustBusinessCollaterals[11] != null ? qQCustBusinessCollaterals[11].BusinessCollateralSubType : "",
					QCustBusinessCollateral12_Description = qQCustBusinessCollaterals[11] != null ? qQCustBusinessCollaterals[11].Description : "",
					QCustBusinessCollateral12_BusinessCollateralValue = qQCustBusinessCollaterals[11] != null ? qQCustBusinessCollaterals[11].BusinessCollateralValue : 0,
					QCustBusinessCollateral12_BusinessCollateralStatus = qQCustBusinessCollaterals[12] != null ? qQCustBusinessCollaterals[11].BusinessCollateralStatus : "",

					QCustBusinessCollateral13_BusinessCollateralType = qQCustBusinessCollaterals[12] != null ? qQCustBusinessCollaterals[12].BusinessCollateralType : "",
					QCustBusinessCollateral13_BusinessCollateralSubType = qQCustBusinessCollaterals[12] != null ? qQCustBusinessCollaterals[12].BusinessCollateralSubType : "",
					QCustBusinessCollateral13_Description = qQCustBusinessCollaterals[12] != null ? qQCustBusinessCollaterals[12].Description : "",
					QCustBusinessCollateral13_BusinessCollateralValue = qQCustBusinessCollaterals[12] != null ? qQCustBusinessCollaterals[12].BusinessCollateralValue : 0,
					QCustBusinessCollateral13_BusinessCollateralStatus = qQCustBusinessCollaterals[12] != null ? qQCustBusinessCollaterals[12].BusinessCollateralStatus : "",

					QCustBusinessCollateral14_BusinessCollateralType = qQCustBusinessCollaterals[13] != null ? qQCustBusinessCollaterals[13].BusinessCollateralType : "",
					QCustBusinessCollateral14_BusinessCollateralSubType = qQCustBusinessCollaterals[13] != null ? qQCustBusinessCollaterals[13].BusinessCollateralSubType : "",
					QCustBusinessCollateral14_Description = qQCustBusinessCollaterals[13] != null ? qQCustBusinessCollaterals[13].Description : "",
					QCustBusinessCollateral14_BusinessCollateralValue = qQCustBusinessCollaterals[13] != null ? qQCustBusinessCollaterals[13].BusinessCollateralValue : 0,
					QCustBusinessCollateral14_BusinessCollateralStatus = qQCustBusinessCollaterals[13] != null ? qQCustBusinessCollaterals[13].BusinessCollateralStatus : "",

					QCustBusinessCollateral15_BusinessCollateralType = qQCustBusinessCollaterals[14] != null ? qQCustBusinessCollaterals[14].BusinessCollateralType : "",
					QCustBusinessCollateral15_BusinessCollateralSubType = qQCustBusinessCollaterals[14] != null ? qQCustBusinessCollaterals[14].BusinessCollateralSubType : "",
					QCustBusinessCollateral15_Description = qQCustBusinessCollaterals[14] != null ? qQCustBusinessCollaterals[14].Description : "",
					QCustBusinessCollateral15_BusinessCollateralValue = qQCustBusinessCollaterals[14] != null ? qQCustBusinessCollaterals[14].BusinessCollateralValue : 0,
					QCustBusinessCollateral15_BusinessCollateralStatus = qQCustBusinessCollaterals[14] != null ? qQCustBusinessCollaterals[14].BusinessCollateralStatus : "",

					QCustBusinessCollateral16_BusinessCollateralType = qQCustBusinessCollaterals[15] != null ? qQCustBusinessCollaterals[15].BusinessCollateralType : "",
					QCustBusinessCollateral16_BusinessCollateralSubType = qQCustBusinessCollaterals[15] != null ? qQCustBusinessCollaterals[15].BusinessCollateralSubType : "",
					QCustBusinessCollateral16_Description = qQCustBusinessCollaterals[15] != null ? qQCustBusinessCollaterals[15].Description : "",
					QCustBusinessCollateral16_BusinessCollateralValue = qQCustBusinessCollaterals[15] != null ? qQCustBusinessCollaterals[15].BusinessCollateralValue : 0,
					QCustBusinessCollateral16_BusinessCollateralStatus = qQCustBusinessCollaterals[15] != null ? qQCustBusinessCollaterals[15].BusinessCollateralStatus : "",

					QCustBusinessCollateral17_BusinessCollateralType = qQCustBusinessCollaterals[16] != null ? qQCustBusinessCollaterals[16].BusinessCollateralType : "",
					QCustBusinessCollateral17_BusinessCollateralSubType = qQCustBusinessCollaterals[16] != null ? qQCustBusinessCollaterals[16].BusinessCollateralSubType : "",
					QCustBusinessCollateral17_Description = qQCustBusinessCollaterals[16] != null ? qQCustBusinessCollaterals[16].Description : "",
					QCustBusinessCollateral17_BusinessCollateralValue = qQCustBusinessCollaterals[16] != null ? qQCustBusinessCollaterals[16].BusinessCollateralValue : 0,
					QCustBusinessCollateral17_BusinessCollateralStatus = qQCustBusinessCollaterals[16] != null ? qQCustBusinessCollaterals[16].BusinessCollateralStatus : "",

					QCustBusinessCollateral18_BusinessCollateralType = qQCustBusinessCollaterals[17] != null ? qQCustBusinessCollaterals[17].BusinessCollateralType : "",
					QCustBusinessCollateral18_BusinessCollateralSubType = qQCustBusinessCollaterals[17] != null ? qQCustBusinessCollaterals[17].BusinessCollateralSubType : "",
					QCustBusinessCollateral18_Description = qQCustBusinessCollaterals[17] != null ? qQCustBusinessCollaterals[17].Description : "",
					QCustBusinessCollateral18_BusinessCollateralValue = qQCustBusinessCollaterals[17] != null ? qQCustBusinessCollaterals[17].BusinessCollateralValue : 0,
					QCustBusinessCollateral18_BusinessCollateralStatus = qQCustBusinessCollaterals[17] != null ? qQCustBusinessCollaterals[17].BusinessCollateralStatus : "",

					QCustBusinessCollateral19_BusinessCollateralType = qQCustBusinessCollaterals[18] != null ? qQCustBusinessCollaterals[18].BusinessCollateralType : "",
					QCustBusinessCollateral19_BusinessCollateralSubType = qQCustBusinessCollaterals[18] != null ? qQCustBusinessCollaterals[18].BusinessCollateralSubType : "",
					QCustBusinessCollateral19_Description = qQCustBusinessCollaterals[18] != null ? qQCustBusinessCollaterals[18].Description : "",
					QCustBusinessCollateral19_BusinessCollateralValue = qQCustBusinessCollaterals[18] != null ? qQCustBusinessCollaterals[18].BusinessCollateralValue : 0,
					QCustBusinessCollateral19_BusinessCollateralStatus = qQCustBusinessCollaterals[18] != null ? qQCustBusinessCollaterals[18].BusinessCollateralStatus : "",

					QCustBusinessCollateral20_BusinessCollateralType = qQCustBusinessCollaterals[19] != null ? qQCustBusinessCollaterals[19].BusinessCollateralType : "",
					QCustBusinessCollateral20_BusinessCollateralSubType = qQCustBusinessCollaterals[19] != null ? qQCustBusinessCollaterals[19].BusinessCollateralSubType : "",
					QCustBusinessCollateral20_Description = qQCustBusinessCollaterals[19] != null ? qQCustBusinessCollaterals[19].Description : "",
					QCustBusinessCollateral20_BusinessCollateralValue = qQCustBusinessCollaterals[19] != null ? qQCustBusinessCollaterals[19].BusinessCollateralValue : 0,
					QCustBusinessCollateral20_BusinessCollateralStatus = qQCustBusinessCollaterals[19] != null ? qQCustBusinessCollaterals[19].BusinessCollateralStatus : "",

					QCustBusinessCollateral21_BusinessCollateralType = qQCustBusinessCollaterals[20] != null ? qQCustBusinessCollaterals[20].BusinessCollateralType : "",
					QCustBusinessCollateral21_BusinessCollateralSubType = qQCustBusinessCollaterals[20] != null ? qQCustBusinessCollaterals[20].BusinessCollateralSubType : "",
					QCustBusinessCollateral21_Description = qQCustBusinessCollaterals[20] != null ? qQCustBusinessCollaterals[20].Description : "",
					QCustBusinessCollateral21_BusinessCollateralValue = qQCustBusinessCollaterals[20] != null ? qQCustBusinessCollaterals[20].BusinessCollateralValue : 0,
					QCustBusinessCollateral21_BusinessCollateralStatus = qQCustBusinessCollaterals[20] != null ? qQCustBusinessCollaterals[20].BusinessCollateralStatus : "",

					QCustBusinessCollateral22_BusinessCollateralType = qQCustBusinessCollaterals[21] != null ? qQCustBusinessCollaterals[21].BusinessCollateralType : "",
					QCustBusinessCollateral22_BusinessCollateralSubType = qQCustBusinessCollaterals[21] != null ? qQCustBusinessCollaterals[21].BusinessCollateralSubType : "",
					QCustBusinessCollateral22_Description = qQCustBusinessCollaterals[21] != null ? qQCustBusinessCollaterals[21].Description : "",
					QCustBusinessCollateral22_BusinessCollateralValue = qQCustBusinessCollaterals[21] != null ? qQCustBusinessCollaterals[21].BusinessCollateralValue : 0,
					QCustBusinessCollateral22_BusinessCollateralStatus = qQCustBusinessCollaterals[21] != null ? qQCustBusinessCollaterals[21].BusinessCollateralStatus : "",

					QCustBusinessCollateral23_BusinessCollateralType = qQCustBusinessCollaterals[22] != null ? qQCustBusinessCollaterals[22].BusinessCollateralType : "",
					QCustBusinessCollateral23_BusinessCollateralSubType = qQCustBusinessCollaterals[22] != null ? qQCustBusinessCollaterals[22].BusinessCollateralSubType : "",
					QCustBusinessCollateral23_Description = qQCustBusinessCollaterals[22] != null ? qQCustBusinessCollaterals[22].Description : "",
					QCustBusinessCollateral23_BusinessCollateralValue = qQCustBusinessCollaterals[22] != null ? qQCustBusinessCollaterals[22].BusinessCollateralValue : 0,
					QCustBusinessCollateral23_BusinessCollateralStatus = qQCustBusinessCollaterals[22] != null ? qQCustBusinessCollaterals[22].BusinessCollateralStatus : "",

					QCustBusinessCollateral24_BusinessCollateralType = qQCustBusinessCollaterals[23] != null ? qQCustBusinessCollaterals[23].BusinessCollateralType : "",
					QCustBusinessCollateral24_BusinessCollateralSubType = qQCustBusinessCollaterals[23] != null ? qQCustBusinessCollaterals[23].BusinessCollateralSubType : "",
					QCustBusinessCollateral24_Description = qQCustBusinessCollaterals[23] != null ? qQCustBusinessCollaterals[23].Description : "",
					QCustBusinessCollateral24_BusinessCollateralValue = qQCustBusinessCollaterals[23] != null ? qQCustBusinessCollaterals[23].BusinessCollateralValue : 0,
					QCustBusinessCollateral24_BusinessCollateralStatus = qQCustBusinessCollaterals[23] != null ? qQCustBusinessCollaterals[23].BusinessCollateralStatus : "",

					QCustBusinessCollateral25_BusinessCollateralType = qQCustBusinessCollaterals[24] != null ? qQCustBusinessCollaterals[24].BusinessCollateralType : "",
					QCustBusinessCollateral25_BusinessCollateralSubType = qQCustBusinessCollaterals[24] != null ? qQCustBusinessCollaterals[24].BusinessCollateralSubType : "",
					QCustBusinessCollateral25_Description = qQCustBusinessCollaterals[24] != null ? qQCustBusinessCollaterals[24].Description : "",
					QCustBusinessCollateral25_BusinessCollateralValue = qQCustBusinessCollaterals[24] != null ? qQCustBusinessCollaterals[24].BusinessCollateralValue : 0,
					QCustBusinessCollateral25_BusinessCollateralStatus = qQCustBusinessCollaterals[24] != null ? qQCustBusinessCollaterals[24].BusinessCollateralStatus : "",

					QCustBusinessCollateral26_BusinessCollateralType = qQCustBusinessCollaterals[25] != null ? qQCustBusinessCollaterals[25].BusinessCollateralType : "",
					QCustBusinessCollateral26_BusinessCollateralSubType = qQCustBusinessCollaterals[25] != null ? qQCustBusinessCollaterals[25].BusinessCollateralSubType : "",
					QCustBusinessCollateral26_Description = qQCustBusinessCollaterals[25] != null ? qQCustBusinessCollaterals[25].Description : "",
					QCustBusinessCollateral26_BusinessCollateralValue = qQCustBusinessCollaterals[25] != null ? qQCustBusinessCollaterals[25].BusinessCollateralValue : 0,
					QCustBusinessCollateral26_BusinessCollateralStatus = qQCustBusinessCollaterals[25] != null ? qQCustBusinessCollaterals[25].BusinessCollateralStatus : "",

					QCustBusinessCollateral27_BusinessCollateralType = qQCustBusinessCollaterals[26] != null ? qQCustBusinessCollaterals[26].BusinessCollateralType : "",
					QCustBusinessCollateral27_BusinessCollateralSubType = qQCustBusinessCollaterals[26] != null ? qQCustBusinessCollaterals[26].BusinessCollateralSubType : "",
					QCustBusinessCollateral27_Description = qQCustBusinessCollaterals[26] != null ? qQCustBusinessCollaterals[26].Description : "",
					QCustBusinessCollateral27_BusinessCollateralValue = qQCustBusinessCollaterals[26] != null ? qQCustBusinessCollaterals[26].BusinessCollateralValue : 0,
					QCustBusinessCollateral27_BusinessCollateralStatus = qQCustBusinessCollaterals[26] != null ? qQCustBusinessCollaterals[26].BusinessCollateralStatus : "",

					QCustBusinessCollateral28_BusinessCollateralType = qQCustBusinessCollaterals[27] != null ? qQCustBusinessCollaterals[27].BusinessCollateralType : "",
					QCustBusinessCollateral28_BusinessCollateralSubType = qQCustBusinessCollaterals[27] != null ? qQCustBusinessCollaterals[27].BusinessCollateralSubType : "",
					QCustBusinessCollateral28_Description = qQCustBusinessCollaterals[27] != null ? qQCustBusinessCollaterals[27].Description : "",
					QCustBusinessCollateral28_BusinessCollateralValue = qQCustBusinessCollaterals[27] != null ? qQCustBusinessCollaterals[27].BusinessCollateralValue : 0,
					QCustBusinessCollateral28_BusinessCollateralStatus = qQCustBusinessCollaterals[27] != null ? qQCustBusinessCollaterals[27].BusinessCollateralStatus : "",

					QCustBusinessCollateral29_BusinessCollateralType = qQCustBusinessCollaterals[28] != null ? qQCustBusinessCollaterals[28].BusinessCollateralType : "",
					QCustBusinessCollateral29_BusinessCollateralSubType = qQCustBusinessCollaterals[28] != null ? qQCustBusinessCollaterals[28].BusinessCollateralSubType : "",
					QCustBusinessCollateral29_Description = qQCustBusinessCollaterals[28] != null ? qQCustBusinessCollaterals[28].Description : "",
					QCustBusinessCollateral29_BusinessCollateralValue = qQCustBusinessCollaterals[28] != null ? qQCustBusinessCollaterals[28].BusinessCollateralValue : 0,
					QCustBusinessCollateral29_BusinessCollateralStatus = qQCustBusinessCollaterals[28] != null ? qQCustBusinessCollaterals[28].BusinessCollateralStatus : "",

					QCustBusinessCollateral30_BusinessCollateralType = qQCustBusinessCollaterals[29] != null ? qQCustBusinessCollaterals[29].BusinessCollateralType : "",
					QCustBusinessCollateral30_BusinessCollateralSubType = qQCustBusinessCollaterals[29] != null ? qQCustBusinessCollaterals[29].BusinessCollateralSubType : "",
					QCustBusinessCollateral30_Description = qQCustBusinessCollaterals[29] != null ? qQCustBusinessCollaterals[29].Description : "",
					QCustBusinessCollateral30_BusinessCollateralValue = qQCustBusinessCollaterals[29] != null ? qQCustBusinessCollaterals[29].BusinessCollateralValue : 0,
					QCustBusinessCollateral30_BusinessCollateralStatus = qQCustBusinessCollaterals[29] != null ? qQCustBusinessCollaterals[29].BusinessCollateralStatus : "",

					QSumCustBusinessCollateral_SumBusinessCollateralValue = qQSumCustBusinessCollateral != null ? qQSumCustBusinessCollateral.SumBusinessCollateralValue : 0,
					AppRequestTable_NCBCheckedDate = qQCreditAppRequestTable != null ? qQCreditAppRequestTable.NCBCheckedDate : "",

					//-----------------------------------------------------------------------------------------------------------------------------------------------------

					QCAReqNCBTrans1_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[0] != null ? qQCAReqNCBTrans[0].FinancialInstitution : "") : "",
					QCAReqNCBTrans1_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[0] != null ? qQCAReqNCBTrans[0].CreditType : "") : "",
					QCAReqNCBTrans1_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[0] != null ? qQCAReqNCBTrans[0].CreditLimit : 0) : 0,
					QCAReqNCBTrans1_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[0] != null ? qQCAReqNCBTrans[0].OutstandingAR : 0) : 0,
					QCAReqNCBTrans1_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[0] != null ? qQCAReqNCBTrans[0].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans1_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[0] != null ? qQCAReqNCBTrans[0].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans1_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[0] != null ? qQCAReqNCBTrans[0].NCBAccountStatus : "") : "",

					QCAReqNCBTrans2_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[1] != null ? qQCAReqNCBTrans[1].FinancialInstitution : "") : "",
					QCAReqNCBTrans2_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[1] != null ? qQCAReqNCBTrans[1].CreditType : "") : "",
					QCAReqNCBTrans2_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[1] != null ? qQCAReqNCBTrans[1].CreditLimit : 0) : 0,
					QCAReqNCBTrans2_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[1] != null ? qQCAReqNCBTrans[1].OutstandingAR : 0) : 0,
					QCAReqNCBTrans2_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[1] != null ? qQCAReqNCBTrans[1].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans2_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[1] != null ? qQCAReqNCBTrans[1].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans2_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[1] != null ? qQCAReqNCBTrans[1].NCBAccountStatus : "") : "",

					QCAReqNCBTrans3_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[2] != null ? qQCAReqNCBTrans[2].FinancialInstitution : "") : "",
					QCAReqNCBTrans3_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[2] != null ? qQCAReqNCBTrans[2].CreditType : "") : "",
					QCAReqNCBTrans3_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[2] != null ? qQCAReqNCBTrans[2].CreditLimit : 0) : 0,
					QCAReqNCBTrans3_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[2] != null ? qQCAReqNCBTrans[2].OutstandingAR : 0) : 0,
					QCAReqNCBTrans3_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[2] != null ? qQCAReqNCBTrans[2].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans3_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[2] != null ? qQCAReqNCBTrans[2].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans3_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[2] != null ? qQCAReqNCBTrans[2].NCBAccountStatus : "") : "",

					QCAReqNCBTrans4_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[3] != null ? qQCAReqNCBTrans[3].FinancialInstitution : "") : "",
					QCAReqNCBTrans4_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[3] != null ? qQCAReqNCBTrans[3].CreditType : "") : "",
					QCAReqNCBTrans4_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[3] != null ? qQCAReqNCBTrans[3].CreditLimit : 0) : 0,
					QCAReqNCBTrans4_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[3] != null ? qQCAReqNCBTrans[3].OutstandingAR : 0) : 0,
					QCAReqNCBTrans4_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[3] != null ? qQCAReqNCBTrans[3].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans4_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[3] != null ? qQCAReqNCBTrans[3].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans4_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[3] != null ? qQCAReqNCBTrans[3].NCBAccountStatus : "") : "",

					QCAReqNCBTrans5_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[4] != null ? qQCAReqNCBTrans[4].FinancialInstitution : "") : "",
					QCAReqNCBTrans5_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[4] != null ? qQCAReqNCBTrans[4].CreditType : "") : "",
					QCAReqNCBTrans5_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[4] != null ? qQCAReqNCBTrans[4].CreditLimit : 0) : 0,
					QCAReqNCBTrans5_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[4] != null ? qQCAReqNCBTrans[4].OutstandingAR : 0) : 0,
					QCAReqNCBTrans5_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[4] != null ? qQCAReqNCBTrans[4].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans5_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[4] != null ? qQCAReqNCBTrans[4].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans5_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[4] != null ? qQCAReqNCBTrans[4].NCBAccountStatus : "") : "",

					QCAReqNCBTrans6_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[5] != null ? qQCAReqNCBTrans[5].FinancialInstitution : "") : "",
					QCAReqNCBTrans6_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[5] != null ? qQCAReqNCBTrans[5].CreditType : "") : "",
					QCAReqNCBTrans6_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[5] != null ? qQCAReqNCBTrans[5].CreditLimit : 0) : 0,
					QCAReqNCBTrans6_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[5] != null ? qQCAReqNCBTrans[5].OutstandingAR : 0) : 0,
					QCAReqNCBTrans6_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[5] != null ? qQCAReqNCBTrans[5].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans6_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[5] != null ? qQCAReqNCBTrans[5].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans6_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[5] != null ? qQCAReqNCBTrans[5].NCBAccountStatus : "") : "",

					QCAReqNCBTrans7_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[6] != null ? qQCAReqNCBTrans[6].FinancialInstitution : "") : "",
					QCAReqNCBTrans7_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[6] != null ? qQCAReqNCBTrans[6].CreditType : "") : "",
					QCAReqNCBTrans7_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[6] != null ? qQCAReqNCBTrans[6].CreditLimit : 0) : 0,
					QCAReqNCBTrans7_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[6] != null ? qQCAReqNCBTrans[6].OutstandingAR : 0) : 0,
					QCAReqNCBTrans7_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[6] != null ? qQCAReqNCBTrans[6].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans7_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[6] != null ? qQCAReqNCBTrans[6].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans7_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[6] != null ? qQCAReqNCBTrans[6].NCBAccountStatus : "") : "",

					QCAReqNCBTrans8_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[7] != null ? qQCAReqNCBTrans[7].FinancialInstitution : "") : "",
					QCAReqNCBTrans8_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[7] != null ? qQCAReqNCBTrans[7].CreditType : "") : "",
					QCAReqNCBTrans8_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[7] != null ? qQCAReqNCBTrans[7].CreditLimit : 0) : 0,
					QCAReqNCBTrans8_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[7] != null ? qQCAReqNCBTrans[7].OutstandingAR : 0) : 0,
					QCAReqNCBTrans8_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[7] != null ? qQCAReqNCBTrans[7].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans8_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[7] != null ? qQCAReqNCBTrans[7].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans8_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[7] != null ? qQCAReqNCBTrans[7].NCBAccountStatus : "") : "",

					QCAReqNCBTrans9_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[8] != null ? qQCAReqNCBTrans[8].FinancialInstitution : "") : "",
					QCAReqNCBTrans9_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[8] != null ? qQCAReqNCBTrans[8].CreditType : "") : "",
					QCAReqNCBTrans9_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[8] != null ? qQCAReqNCBTrans[8].CreditLimit : 0) : 0,
					QCAReqNCBTrans9_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[8] != null ? qQCAReqNCBTrans[8].OutstandingAR : 0) : 0,
					QCAReqNCBTrans9_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[8] != null ? qQCAReqNCBTrans[8].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans9_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[8] != null ? qQCAReqNCBTrans[8].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans9_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[8] != null ? qQCAReqNCBTrans[8].NCBAccountStatus : "") : "",

					QCAReqNCBTrans10_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[9] != null ? qQCAReqNCBTrans[9].FinancialInstitution : "") : "",
					QCAReqNCBTrans10_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[9] != null ? qQCAReqNCBTrans[9].CreditType : "") : "",
					QCAReqNCBTrans10_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[9] != null ? qQCAReqNCBTrans[9].CreditLimit : 0) : 0,
					QCAReqNCBTrans10_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[9] != null ? qQCAReqNCBTrans[9].OutstandingAR : 0) : 0,
					QCAReqNCBTrans10_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[9] != null ? qQCAReqNCBTrans[9].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans10_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[9] != null ? qQCAReqNCBTrans[9].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans10_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[9] != null ? qQCAReqNCBTrans[9].NCBAccountStatus : "") : "",

					QCAReqNCBTrans11_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[10] != null ? qQCAReqNCBTrans[10].FinancialInstitution : "") : "",
					QCAReqNCBTrans11_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[10] != null ? qQCAReqNCBTrans[10].CreditType : "") : "",
					QCAReqNCBTrans11_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[10] != null ? qQCAReqNCBTrans[10].CreditLimit : 0) : 0,
					QCAReqNCBTrans11_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[10] != null ? qQCAReqNCBTrans[10].OutstandingAR : 0) : 0,
					QCAReqNCBTrans11_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[10] != null ? qQCAReqNCBTrans[10].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans11_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[10] != null ? qQCAReqNCBTrans[10].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans11_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[10] != null ? qQCAReqNCBTrans[10].NCBAccountStatus : "") : "",

					QCAReqNCBTrans12_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[11] != null ? qQCAReqNCBTrans[11].FinancialInstitution : "") : "",
					QCAReqNCBTrans12_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[11] != null ? qQCAReqNCBTrans[11].CreditType : "") : "",
					QCAReqNCBTrans12_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[11] != null ? qQCAReqNCBTrans[11].CreditLimit : 0) : 0,
					QCAReqNCBTrans12_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[11] != null ? qQCAReqNCBTrans[11].OutstandingAR : 0) : 0,
					QCAReqNCBTrans12_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[11] != null ? qQCAReqNCBTrans[11].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans12_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[11] != null ? qQCAReqNCBTrans[11].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans12_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[11] != null ? qQCAReqNCBTrans[11].NCBAccountStatus : "") : "",

					QCAReqNCBTrans13_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[12] != null ? qQCAReqNCBTrans[12].FinancialInstitution : "") : "",
					QCAReqNCBTrans13_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[12] != null ? qQCAReqNCBTrans[12].CreditType : "") : "",
					QCAReqNCBTrans13_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[12] != null ? qQCAReqNCBTrans[12].CreditLimit : 0) : 0,
					QCAReqNCBTrans13_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[12] != null ? qQCAReqNCBTrans[12].OutstandingAR : 0) : 0,
					QCAReqNCBTrans13_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[12] != null ? qQCAReqNCBTrans[12].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans13_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[12] != null ? qQCAReqNCBTrans[12].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans13_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[12] != null ? qQCAReqNCBTrans[12].NCBAccountStatus : "") : "",

					QCAReqNCBTrans14_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[13] != null ? qQCAReqNCBTrans[13].FinancialInstitution : "") : "",
					QCAReqNCBTrans14_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[13] != null ? qQCAReqNCBTrans[13].CreditType : "") : "",
					QCAReqNCBTrans14_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[13] != null ? qQCAReqNCBTrans[13].CreditLimit : 0) : 0,
					QCAReqNCBTrans14_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[13] != null ? qQCAReqNCBTrans[13].OutstandingAR : 0) : 0,
					QCAReqNCBTrans14_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[13] != null ? qQCAReqNCBTrans[13].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans14_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[13] != null ? qQCAReqNCBTrans[13].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans14_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[13] != null ? qQCAReqNCBTrans[13].NCBAccountStatus : "") : "",

					QCAReqNCBTrans15_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[14] != null ? qQCAReqNCBTrans[14].FinancialInstitution : "") : "",
					QCAReqNCBTrans15_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[14] != null ? qQCAReqNCBTrans[14].CreditType : "") : "",
					QCAReqNCBTrans15_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[14] != null ? qQCAReqNCBTrans[14].CreditLimit : 0) : 0,
					QCAReqNCBTrans15_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[14] != null ? qQCAReqNCBTrans[14].OutstandingAR : 0) : 0,
					QCAReqNCBTrans15_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[14] != null ? qQCAReqNCBTrans[14].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans15_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[14] != null ? qQCAReqNCBTrans[14].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans15_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[14] != null ? qQCAReqNCBTrans[14].NCBAccountStatus : "") : "",

					QCAReqNCBTrans16_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[15] != null ? qQCAReqNCBTrans[15].FinancialInstitution : "") : "",
					QCAReqNCBTrans16_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[15] != null ? qQCAReqNCBTrans[15].CreditType : "") : "",
					QCAReqNCBTrans16_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[15] != null ? qQCAReqNCBTrans[15].CreditLimit : 0) : 0,
					QCAReqNCBTrans16_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[15] != null ? qQCAReqNCBTrans[15].OutstandingAR : 0) : 0,
					QCAReqNCBTrans16_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[15] != null ? qQCAReqNCBTrans[15].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans16_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[15] != null ? qQCAReqNCBTrans[15].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans16_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[15] != null ? qQCAReqNCBTrans[15].NCBAccountStatus : "") : "",

					QCAReqNCBTrans17_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[16] != null ? qQCAReqNCBTrans[16].FinancialInstitution : "") : "",
					QCAReqNCBTrans17_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[16] != null ? qQCAReqNCBTrans[16].CreditType : "") : "",
					QCAReqNCBTrans17_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[16] != null ? qQCAReqNCBTrans[16].CreditLimit : 0) : 0,
					QCAReqNCBTrans17_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[16] != null ? qQCAReqNCBTrans[16].OutstandingAR : 0) : 0,
					QCAReqNCBTrans17_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[16] != null ? qQCAReqNCBTrans[16].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans17_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[16] != null ? qQCAReqNCBTrans[16].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans17_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[16] != null ? qQCAReqNCBTrans[16].NCBAccountStatus : "") : "",

					QCAReqNCBTrans18_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[17] != null ? qQCAReqNCBTrans[17].FinancialInstitution : "") : "",
					QCAReqNCBTrans18_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[17] != null ? qQCAReqNCBTrans[17].CreditType : "") : "",
					QCAReqNCBTrans18_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[17] != null ? qQCAReqNCBTrans[17].CreditLimit : 0) : 0,
					QCAReqNCBTrans18_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[17] != null ? qQCAReqNCBTrans[17].OutstandingAR : 0) : 0,
					QCAReqNCBTrans18_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[17] != null ? qQCAReqNCBTrans[17].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans18_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[17] != null ? qQCAReqNCBTrans[17].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans18_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[17] != null ? qQCAReqNCBTrans[17].NCBAccountStatus : "") : "",

					QCAReqNCBTrans19_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[18] != null ? qQCAReqNCBTrans[18].FinancialInstitution : "") : "",
					QCAReqNCBTrans19_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[18] != null ? qQCAReqNCBTrans[18].CreditType : "") : "",
					QCAReqNCBTrans19_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[18] != null ? qQCAReqNCBTrans[18].CreditLimit : 0) : 0,
					QCAReqNCBTrans19_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[18] != null ? qQCAReqNCBTrans[18].OutstandingAR : 0) : 0,
					QCAReqNCBTrans19_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[18] != null ? qQCAReqNCBTrans[18].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans19_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[18] != null ? qQCAReqNCBTrans[18].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans19_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[18] != null ? qQCAReqNCBTrans[18].NCBAccountStatus : "") : "",

					QCAReqNCBTrans20_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[19] != null ? qQCAReqNCBTrans[19].FinancialInstitution : "") : "",
					QCAReqNCBTrans20_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[19] != null ? qQCAReqNCBTrans[19].CreditType : "") : "",
					QCAReqNCBTrans20_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[19] != null ? qQCAReqNCBTrans[19].CreditLimit : 0) : 0,
					QCAReqNCBTrans20_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[19] != null ? qQCAReqNCBTrans[19].OutstandingAR : 0) : 0,
					QCAReqNCBTrans20_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[19] != null ? qQCAReqNCBTrans[19].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans20_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[19] != null ? qQCAReqNCBTrans[19].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans20_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[19] != null ? qQCAReqNCBTrans[19].NCBAccountStatus : "") : "",

					QCAReqNCBTrans21_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[20] != null ? qQCAReqNCBTrans[20].FinancialInstitution : "") : "",
					QCAReqNCBTrans21_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[20] != null ? qQCAReqNCBTrans[20].CreditType : "") : "",
					QCAReqNCBTrans21_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[20] != null ? qQCAReqNCBTrans[20].CreditLimit : 0) : 0,
					QCAReqNCBTrans21_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[20] != null ? qQCAReqNCBTrans[20].OutstandingAR : 0) : 0,
					QCAReqNCBTrans21_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[20] != null ? qQCAReqNCBTrans[20].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans21_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[20] != null ? qQCAReqNCBTrans[20].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans21_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[20] != null ? qQCAReqNCBTrans[20].NCBAccountStatus : "") : "",

					QCAReqNCBTrans22_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[21] != null ? qQCAReqNCBTrans[21].FinancialInstitution : "") : "",
					QCAReqNCBTrans22_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[21] != null ? qQCAReqNCBTrans[21].CreditType : "") : "",
					QCAReqNCBTrans22_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[21] != null ? qQCAReqNCBTrans[21].CreditLimit : 0) : 0,
					QCAReqNCBTrans22_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[21] != null ? qQCAReqNCBTrans[21].OutstandingAR : 0) : 0,
					QCAReqNCBTrans22_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[21] != null ? qQCAReqNCBTrans[21].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans22_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[21] != null ? qQCAReqNCBTrans[21].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans22_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[21] != null ? qQCAReqNCBTrans[21].NCBAccountStatus : "") : "",

					QCAReqNCBTrans23_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[22] != null ? qQCAReqNCBTrans[22].FinancialInstitution : "") : "",
					QCAReqNCBTrans23_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[22] != null ? qQCAReqNCBTrans[22].CreditType : "") : "",
					QCAReqNCBTrans23_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[22] != null ? qQCAReqNCBTrans[22].CreditLimit : 0) : 0,
					QCAReqNCBTrans23_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[22] != null ? qQCAReqNCBTrans[22].OutstandingAR : 0) : 0,
					QCAReqNCBTrans23_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[22] != null ? qQCAReqNCBTrans[22].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans23_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[22] != null ? qQCAReqNCBTrans[22].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans23_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[22] != null ? qQCAReqNCBTrans[22].NCBAccountStatus : "") : "",

					QCAReqNCBTrans24_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[23] != null ? qQCAReqNCBTrans[23].FinancialInstitution : "") : "",
					QCAReqNCBTrans24_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[23] != null ? qQCAReqNCBTrans[23].CreditType : "") : "",
					QCAReqNCBTrans24_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[23] != null ? qQCAReqNCBTrans[23].CreditLimit : 0) : 0,
					QCAReqNCBTrans24_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[23] != null ? qQCAReqNCBTrans[23].OutstandingAR : 0) : 0,
					QCAReqNCBTrans24_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[23] != null ? qQCAReqNCBTrans[23].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans24_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[23] != null ? qQCAReqNCBTrans[23].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans24_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[23] != null ? qQCAReqNCBTrans[23].NCBAccountStatus : "") : "",

					QCAReqNCBTrans25_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[24] != null ? qQCAReqNCBTrans[24].FinancialInstitution : "") : "",
					QCAReqNCBTrans25_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[24] != null ? qQCAReqNCBTrans[24].CreditType : "") : "",
					QCAReqNCBTrans25_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[24] != null ? qQCAReqNCBTrans[24].CreditLimit : 0) : 0,
					QCAReqNCBTrans25_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[24] != null ? qQCAReqNCBTrans[24].OutstandingAR : 0) : 0,
					QCAReqNCBTrans25_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[24] != null ? qQCAReqNCBTrans[24].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans25_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[24] != null ? qQCAReqNCBTrans[24].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans25_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[24] != null ? qQCAReqNCBTrans[24].NCBAccountStatus : "") : "",

					QCAReqNCBTrans26_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[25] != null ? qQCAReqNCBTrans[25].FinancialInstitution : "") : "",
					QCAReqNCBTrans26_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[25] != null ? qQCAReqNCBTrans[25].CreditType : "") : "",
					QCAReqNCBTrans26_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[25] != null ? qQCAReqNCBTrans[25].CreditLimit : 0) : 0,
					QCAReqNCBTrans26_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[25] != null ? qQCAReqNCBTrans[25].OutstandingAR : 0) : 0,
					QCAReqNCBTrans26_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[25] != null ? qQCAReqNCBTrans[25].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans26_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[25] != null ? qQCAReqNCBTrans[25].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans26_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[25] != null ? qQCAReqNCBTrans[25].NCBAccountStatus : "") : "",

					QCAReqNCBTrans27_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[26] != null ? qQCAReqNCBTrans[26].FinancialInstitution : "") : "",
					QCAReqNCBTrans27_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[26] != null ? qQCAReqNCBTrans[26].CreditType : "") : "",
					QCAReqNCBTrans27_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[26] != null ? qQCAReqNCBTrans[26].CreditLimit : 0) : 0,
					QCAReqNCBTrans27_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[26] != null ? qQCAReqNCBTrans[26].OutstandingAR : 0) : 0,
					QCAReqNCBTrans27_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[26] != null ? qQCAReqNCBTrans[26].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans27_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[26] != null ? qQCAReqNCBTrans[26].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans27_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[26] != null ? qQCAReqNCBTrans[26].NCBAccountStatus : "") : "",

					QCAReqNCBTrans28_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[27] != null ? qQCAReqNCBTrans[27].FinancialInstitution : "") : "",
					QCAReqNCBTrans28_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[27] != null ? qQCAReqNCBTrans[27].CreditType : "") : "",
					QCAReqNCBTrans28_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[27] != null ? qQCAReqNCBTrans[27].CreditLimit : 0) : 0,
					QCAReqNCBTrans28_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[27] != null ? qQCAReqNCBTrans[27].OutstandingAR : 0) : 0,
					QCAReqNCBTrans28_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[27] != null ? qQCAReqNCBTrans[27].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans28_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[27] != null ? qQCAReqNCBTrans[27].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans28_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[27] != null ? qQCAReqNCBTrans[27].NCBAccountStatus : "") : "",

					QCAReqNCBTrans29_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[28] != null ? qQCAReqNCBTrans[28].FinancialInstitution : "") : "",
					QCAReqNCBTrans29_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[28] != null ? qQCAReqNCBTrans[28].CreditType : "") : "",
					QCAReqNCBTrans29_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[28] != null ? qQCAReqNCBTrans[28].CreditLimit : 0) : 0,
					QCAReqNCBTrans29_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[28] != null ? qQCAReqNCBTrans[28].OutstandingAR : 0) : 0,
					QCAReqNCBTrans29_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[28] != null ? qQCAReqNCBTrans[28].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans29_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[28] != null ? qQCAReqNCBTrans[28].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans29_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[28] != null ? qQCAReqNCBTrans[28].NCBAccountStatus : "") : "",

					QCAReqNCBTrans30_FinancialInstitution = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[29] != null ? qQCAReqNCBTrans[29].FinancialInstitution : "") : "",
					QCAReqNCBTrans30_CreditType = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[29] != null ? qQCAReqNCBTrans[29].CreditType : "") : "",
					QCAReqNCBTrans30_CreditLimit = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[29] != null ? qQCAReqNCBTrans[29].CreditLimit : 0) : 0,
					QCAReqNCBTrans30_OutstandingAR = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[29] != null ? qQCAReqNCBTrans[29].OutstandingAR : 0) : 0,
					QCAReqNCBTrans30_MonthlyRepayment = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[29] != null ? qQCAReqNCBTrans[29].MonthlyRepayment : 0) : 0,
					QCAReqNCBTrans30_EndDate = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[29] != null ? qQCAReqNCBTrans[29].EndDate.DateToString() : "") : "",
					QCAReqNCBTrans30_NCBAccountStatus = qQCAReqNCBTrans != null ? (qQCAReqNCBTrans[29] != null ? qQCAReqNCBTrans[29].NCBAccountStatus : "") : "",

					//-----------------------------------------------------------------------------------------------------------------------------------------------------

					QCAReqNCBTransSum_SumCreditLimit = qQCAReqNCBTransSum != null ? qQCAReqNCBTransSum.SumCreditLimit : 0,
					QCAReqNCBTransSum_SumOutstandingAR = qQCAReqNCBTransSum != null ? qQCAReqNCBTransSum.SumOutstandingAR : 0,
					QCAReqNCBTransSum_MonthlyRepayment = qQCAReqNCBTransSum != null ? qQCAReqNCBTransSum.SumMonthlyRepayment : 0,

					QAuthorizedPersonTransForNCB1_AuthorizedPersonName = qAuthorizedPersonTransForNCBs[0] != null ? qAuthorizedPersonTransForNCBs[0].AuthorizedPersonName : "",
					QAuthorizedPersonTransForNCB1_NCBCheckedDate = qAuthorizedPersonTransForNCBs[0] != null ? qAuthorizedPersonTransForNCBs[0].NCBCheckedDate : "",

					QAuthorizedNCB11_FinancialInstitution = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[0] != null ? qQAuthorizedNCB1s[0].FinancialInstitution : "") : "",
					QAuthorizedNCB11_CreditType = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[0] != null ? qQAuthorizedNCB1s[0].CreditType : "") : "",
					QAuthorizedNCB11_CreditLimit = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[0] != null ? qQAuthorizedNCB1s[0].CreditLimit : 0) : 0,
					QAuthorizedNCB11_OutstandingAR = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[0] != null ? qQAuthorizedNCB1s[0].OutstandingAR : 0) : 0,
					QAuthorizedNCB11_MonthlyRepayment = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[0] != null ? qQAuthorizedNCB1s[0].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB11_EndDate = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[0] != null ? qQAuthorizedNCB1s[0].EndDate : "") : "",
					QAuthorizedNCB11_NCBAccountStatus = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[0] != null ? qQAuthorizedNCB1s[0].NCBAccountStatus : "") : "",

					QAuthorizedNCB12_FinancialInstitution = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[1] != null ? qQAuthorizedNCB1s[1].FinancialInstitution : "") : "",
					QAuthorizedNCB12_CreditType = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[1] != null ? qQAuthorizedNCB1s[1].CreditType : "") : "",
					QAuthorizedNCB12_CreditLimit = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[1] != null ? qQAuthorizedNCB1s[1].CreditLimit : 0) : 0,
					QAuthorizedNCB12_OutstandingAR = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[1] != null ? qQAuthorizedNCB1s[1].OutstandingAR : 0) : 0,
					QAuthorizedNCB12_MonthlyRepayment = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[1] != null ? qQAuthorizedNCB1s[1].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB12_EndDate = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[1] != null ? qQAuthorizedNCB1s[1].EndDate : "") : "",
					QAuthorizedNCB12_NCBAccountStatus = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[1] != null ? qQAuthorizedNCB1s[1].NCBAccountStatus : "") : "",

					QAuthorizedNCB13_FinancialInstitution = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[2] != null ? qQAuthorizedNCB1s[2].FinancialInstitution : "") : "",
					QAuthorizedNCB13_CreditType = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[2] != null ? qQAuthorizedNCB1s[2].CreditType : "") : "",
					QAuthorizedNCB13_CreditLimit = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[2] != null ? qQAuthorizedNCB1s[2].CreditLimit : 0) : 0,
					QAuthorizedNCB13_OutstandingAR = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[2] != null ? qQAuthorizedNCB1s[2].OutstandingAR : 0) : 0,
					QAuthorizedNCB13_MonthlyRepayment = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[2] != null ? qQAuthorizedNCB1s[2].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB13_EndDate = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[2] != null ? qQAuthorizedNCB1s[2].EndDate : "") : "",
					QAuthorizedNCB13_NCBAccountStatus = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[2] != null ? qQAuthorizedNCB1s[2].NCBAccountStatus : "") : "",

					QAuthorizedNCB14_FinancialInstitution = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[3] != null ? qQAuthorizedNCB1s[3].FinancialInstitution : "") : "",
					QAuthorizedNCB14_CreditType = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[3] != null ? qQAuthorizedNCB1s[3].CreditType : "") : "",
					QAuthorizedNCB14_CreditLimit = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[3] != null ? qQAuthorizedNCB1s[3].CreditLimit : 0) : 0,
					QAuthorizedNCB14_OutstandingAR = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[3] != null ? qQAuthorizedNCB1s[3].OutstandingAR : 0) : 0,
					QAuthorizedNCB14_MonthlyRepayment = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[3] != null ? qQAuthorizedNCB1s[3].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB14_EndDate = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[3] != null ? qQAuthorizedNCB1s[1].EndDate : "") : "",
					QAuthorizedNCB14_NCBAccountStatus = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[3] != null ? qQAuthorizedNCB1s[3].NCBAccountStatus : "") : "",

					QAuthorizedNCB15_FinancialInstitution = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[4] != null ? qQAuthorizedNCB1s[4].FinancialInstitution : "") : "",
					QAuthorizedNCB15_CreditType = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[4] != null ? qQAuthorizedNCB1s[4].CreditType : "") : "",
					QAuthorizedNCB15_CreditLimit = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[4] != null ? qQAuthorizedNCB1s[4].CreditLimit : 0) : 0,
					QAuthorizedNCB15_OutstandingAR = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[4] != null ? qQAuthorizedNCB1s[4].OutstandingAR : 0) : 0,
					QAuthorizedNCB15_MonthlyRepayment = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[4] != null ? qQAuthorizedNCB1s[4].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB15_EndDate = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[4] != null ? qQAuthorizedNCB1s[4].EndDate : "") : "",
					QAuthorizedNCB15_NCBAccountStatus = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[4] != null ? qQAuthorizedNCB1s[4].NCBAccountStatus : "") : "",

					QAuthorizedNCB16_FinancialInstitution = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[5] != null ? qQAuthorizedNCB1s[5].FinancialInstitution : "") : "",
					QAuthorizedNCB16_CreditType = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[5] != null ? qQAuthorizedNCB1s[5].CreditType : "") : "",
					QAuthorizedNCB16_CreditLimit = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[5] != null ? qQAuthorizedNCB1s[5].CreditLimit : 0) : 0,
					QAuthorizedNCB16_OutstandingAR = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[5] != null ? qQAuthorizedNCB1s[5].OutstandingAR : 0) : 0,
					QAuthorizedNCB16_MonthlyRepayment = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[5] != null ? qQAuthorizedNCB1s[5].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB16_EndDate = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[5] != null ? qQAuthorizedNCB1s[5].EndDate : "") : "",
					QAuthorizedNCB16_NCBAccountStatus = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[5] != null ? qQAuthorizedNCB1s[5].NCBAccountStatus : "") : "",

					QAuthorizedNCB17_FinancialInstitution = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[6] != null ? qQAuthorizedNCB1s[6].FinancialInstitution : "") : "",
					QAuthorizedNCB17_CreditType = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[6] != null ? qQAuthorizedNCB1s[6].CreditType : "") : "",
					QAuthorizedNCB17_CreditLimit = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[6] != null ? qQAuthorizedNCB1s[6].CreditLimit : 0) : 0,
					QAuthorizedNCB17_OutstandingAR = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[6] != null ? qQAuthorizedNCB1s[6].OutstandingAR : 0) : 0,
					QAuthorizedNCB17_MonthlyRepayment = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[6] != null ? qQAuthorizedNCB1s[6].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB17_EndDate = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[6] != null ? qQAuthorizedNCB1s[6].EndDate : "") : "",
					QAuthorizedNCB17_NCBAccountStatus = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[6] != null ? qQAuthorizedNCB1s[6].NCBAccountStatus : "") : "",

					QAuthorizedNCB18_FinancialInstitution = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[7] != null ? qQAuthorizedNCB1s[7].FinancialInstitution : "") : "",
					QAuthorizedNCB18_CreditType = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[7] != null ? qQAuthorizedNCB1s[7].CreditType : "") : "",
					QAuthorizedNCB18_CreditLimit = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[7] != null ? qQAuthorizedNCB1s[7].CreditLimit : 0) : 0,
					QAuthorizedNCB18_OutstandingAR = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[7] != null ? qQAuthorizedNCB1s[7].OutstandingAR : 0) : 0,
					QAuthorizedNCB18_MonthlyRepayment = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[7] != null ? qQAuthorizedNCB1s[7].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB18_EndDate = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[7] != null ? qQAuthorizedNCB1s[7].EndDate : "") : "",
					QAuthorizedNCB18_NCBAccountStatus = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[7] != null ? qQAuthorizedNCB1s[7].NCBAccountStatus : "") : "",

					QAuthorizedNCB19_FinancialInstitution = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[8] != null ? qQAuthorizedNCB1s[8].FinancialInstitution : "") : "",
					QAuthorizedNCB19_CreditType = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[8] != null ? qQAuthorizedNCB1s[8].CreditType : "") : "",
					QAuthorizedNCB19_CreditLimit = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[8] != null ? qQAuthorizedNCB1s[8].CreditLimit : 0) : 0,
					QAuthorizedNCB19_OutstandingAR = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[8] != null ? qQAuthorizedNCB1s[8].OutstandingAR : 0) : 0,
					QAuthorizedNCB19_MonthlyRepayment = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[8] != null ? qQAuthorizedNCB1s[8].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB19_EndDate = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[8] != null ? qQAuthorizedNCB1s[8].EndDate : "") : "",
					QAuthorizedNCB19_NCBAccountStatus = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[8] != null ? qQAuthorizedNCB1s[8].NCBAccountStatus : "") : "",

					QAuthorizedNCB110_FinancialInstitution = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[9] != null ? qQAuthorizedNCB1s[9].FinancialInstitution : "") : "",
					QAuthorizedNCB110_CreditType = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[9] != null ? qQAuthorizedNCB1s[9].CreditType : "") : "",
					QAuthorizedNCB110_CreditLimit = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[9] != null ? qQAuthorizedNCB1s[9].CreditLimit : 0) : 0,
					QAuthorizedNCB110_OutstandingAR = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[9] != null ? qQAuthorizedNCB1s[9].OutstandingAR : 0) : 0,
					QAuthorizedNCB110_MonthlyRepayment = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[9] != null ? qQAuthorizedNCB1s[9].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB110_EndDate = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[9] != null ? qQAuthorizedNCB1s[9].EndDate : "") : "",
					QAuthorizedNCB110_NCBAccountStatus = qQAuthorizedNCB1s != null ? (qQAuthorizedNCB1s[9] != null ? qQAuthorizedNCB1s[9].NCBAccountStatus : "") : "",

					QSumAuthorizedNCB1_SumCreditLimit = qQSumAuthorizedNCB1 != null ? qQSumAuthorizedNCB1.SumCreditLimit : 0,
					QSumAuthorizedNCB1_SumOutstandingAR = qQSumAuthorizedNCB1 != null ? qQSumAuthorizedNCB1.SumOutstandingAR : 0,
					QSumAuthorizedNCB1_SumMonthlyRepayment = qQSumAuthorizedNCB1 != null ? qQSumAuthorizedNCB1.SumMonthlyRepayment : 0,

					QAuthorizedPersonTransForNCB2_AuthorizedPersonName = qAuthorizedPersonTransForNCBs[1] != null ? qAuthorizedPersonTransForNCBs[1].AuthorizedPersonName : "",
					QAuthorizedPersonTransForNCB2_NCBCheckedDate = qAuthorizedPersonTransForNCBs[1] != null ? qAuthorizedPersonTransForNCBs[1].NCBCheckedDate : "",

					QAuthorizedNCB21_FinancialInstitution = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[0] != null ? qQAuthorizedNCB2s[0].FinancialInstitution : "") : "",
					QAuthorizedNCB21_CreditType = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[0] != null ? qQAuthorizedNCB2s[0].CreditType : "") : "",
					QAuthorizedNCB21_CreditLimit = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[0] != null ? qQAuthorizedNCB2s[0].CreditLimit : 0) : 0,
					QAuthorizedNCB21_OutstandingAR = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[0] != null ? qQAuthorizedNCB2s[0].OutstandingAR : 0) : 0,
					QAuthorizedNCB21_MonthlyRepayment = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[0] != null ? qQAuthorizedNCB2s[0].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB21_EndDate = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[0] != null ? qQAuthorizedNCB2s[0].EndDate : "") : "",
					QAuthorizedNCB21_NCBAccountStatus = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[0] != null ? qQAuthorizedNCB2s[0].NCBAccountStatus : "") : "",

					QAuthorizedNCB22_FinancialInstitution = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[1] != null ? qQAuthorizedNCB2s[1].FinancialInstitution : "") : "",
					QAuthorizedNCB22_CreditType = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[1] != null ? qQAuthorizedNCB2s[1].CreditType : "") : "",
					QAuthorizedNCB22_CreditLimit = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[1] != null ? qQAuthorizedNCB2s[1].CreditLimit : 0) : 0,
					QAuthorizedNCB22_OutstandingAR = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[1] != null ? qQAuthorizedNCB2s[1].OutstandingAR : 0) : 0,
					QAuthorizedNCB22_MonthlyRepayment = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[1] != null ? qQAuthorizedNCB2s[1].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB22_EndDate = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[1] != null ? qQAuthorizedNCB2s[1].EndDate : "") : "",
					QAuthorizedNCB22_NCBAccountStatus = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[1] != null ? qQAuthorizedNCB2s[1].NCBAccountStatus : "") : "",

					QAuthorizedNCB23_FinancialInstitution = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[2] != null ? qQAuthorizedNCB2s[2].FinancialInstitution : "") : "",
					QAuthorizedNCB23_CreditType = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[2] != null ? qQAuthorizedNCB2s[2].CreditType : "") : "",
					QAuthorizedNCB23_CreditLimit = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[2] != null ? qQAuthorizedNCB2s[2].CreditLimit : 0) : 0,
					QAuthorizedNCB23_OutstandingAR = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[2] != null ? qQAuthorizedNCB2s[2].OutstandingAR : 0) : 0,
					QAuthorizedNCB23_MonthlyRepayment = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[2] != null ? qQAuthorizedNCB2s[2].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB23_EndDate = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[2] != null ? qQAuthorizedNCB2s[2].EndDate : "") : "",
					QAuthorizedNCB23_NCBAccountStatus = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[2] != null ? qQAuthorizedNCB2s[2].NCBAccountStatus : "") : "",

					QAuthorizedNCB24_FinancialInstitution = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[3] != null ? qQAuthorizedNCB2s[3].FinancialInstitution : "") : "",
					QAuthorizedNCB24_CreditType = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[3] != null ? qQAuthorizedNCB2s[3].CreditType : "") : "",
					QAuthorizedNCB24_CreditLimit = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[3] != null ? qQAuthorizedNCB2s[3].CreditLimit : 0) : 0,
					QAuthorizedNCB24_OutstandingAR = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[3] != null ? qQAuthorizedNCB2s[3].OutstandingAR : 0) : 0,
					QAuthorizedNCB24_MonthlyRepayment = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[3] != null ? qQAuthorizedNCB2s[3].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB24_EndDate = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[3] != null ? qQAuthorizedNCB2s[3].EndDate : "") : "",
					QAuthorizedNCB24_NCBAccountStatus = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[3] != null ? qQAuthorizedNCB2s[3].NCBAccountStatus : "") : "",

					QAuthorizedNCB25_FinancialInstitution = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[4] != null ? qQAuthorizedNCB2s[4].FinancialInstitution : "") : "",
					QAuthorizedNCB25_CreditType = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[4] != null ? qQAuthorizedNCB2s[4].CreditType : "") : "",
					QAuthorizedNCB25_CreditLimit = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[4] != null ? qQAuthorizedNCB2s[4].CreditLimit : 0) : 0,
					QAuthorizedNCB25_OutstandingAR = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[4] != null ? qQAuthorizedNCB2s[4].OutstandingAR : 0) : 0,
					QAuthorizedNCB25_MonthlyRepayment = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[4] != null ? qQAuthorizedNCB2s[4].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB25_EndDate = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[4] != null ? qQAuthorizedNCB2s[4].EndDate : "") : "",
					QAuthorizedNCB25_NCBAccountStatus = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[4] != null ? qQAuthorizedNCB2s[4].NCBAccountStatus : "") : "",

					QAuthorizedNCB26_FinancialInstitution = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[5] != null ? qQAuthorizedNCB2s[5].FinancialInstitution : "") : "",
					QAuthorizedNCB26_CreditType = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[5] != null ? qQAuthorizedNCB2s[5].CreditType : "") : "",
					QAuthorizedNCB26_CreditLimit = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[5] != null ? qQAuthorizedNCB2s[5].CreditLimit : 0) : 0,
					QAuthorizedNCB26_OutstandingAR = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[5] != null ? qQAuthorizedNCB2s[5].OutstandingAR : 0) : 0,
					QAuthorizedNCB26_MonthlyRepayment = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[5] != null ? qQAuthorizedNCB2s[5].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB26_EndDate = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[5] != null ? qQAuthorizedNCB2s[5].EndDate : "") : "",
					QAuthorizedNCB26_NCBAccountStatus = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[5] != null ? qQAuthorizedNCB2s[5].NCBAccountStatus : "") : "",

					QAuthorizedNCB27_FinancialInstitution = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[6] != null ? qQAuthorizedNCB2s[6].FinancialInstitution : "") : "",
					QAuthorizedNCB27_CreditType = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[6] != null ? qQAuthorizedNCB2s[6].CreditType : "") : "",
					QAuthorizedNCB27_CreditLimit = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[6] != null ? qQAuthorizedNCB2s[6].CreditLimit : 0) : 0,
					QAuthorizedNCB27_OutstandingAR = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[6] != null ? qQAuthorizedNCB2s[6].OutstandingAR : 0) : 0,
					QAuthorizedNCB27_MonthlyRepayment = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[6] != null ? qQAuthorizedNCB2s[6].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB27_EndDate = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[6] != null ? qQAuthorizedNCB2s[6].EndDate : "") : "",
					QAuthorizedNCB27_NCBAccountStatus = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[6] != null ? qQAuthorizedNCB2s[6].NCBAccountStatus : "") : "",

					QAuthorizedNCB28_FinancialInstitution = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[7] != null ? qQAuthorizedNCB2s[7].FinancialInstitution : "") : "",
					QAuthorizedNCB28_CreditType = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[7] != null ? qQAuthorizedNCB2s[7].CreditType : "") : "",
					QAuthorizedNCB28_CreditLimit = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[7] != null ? qQAuthorizedNCB2s[7].CreditLimit : 0) : 0,
					QAuthorizedNCB28_OutstandingAR = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[7] != null ? qQAuthorizedNCB2s[7].OutstandingAR : 0) : 0,
					QAuthorizedNCB28_MonthlyRepayment = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[7] != null ? qQAuthorizedNCB2s[7].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB28_EndDate = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[7] != null ? qQAuthorizedNCB2s[7].EndDate : "") : "",
					QAuthorizedNCB28_NCBAccountStatus = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[7] != null ? qQAuthorizedNCB2s[7].NCBAccountStatus : "") : "",

					QAuthorizedNCB29_FinancialInstitution = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[8] != null ? qQAuthorizedNCB2s[8].FinancialInstitution : "") : "",
					QAuthorizedNCB29_CreditType = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[8] != null ? qQAuthorizedNCB2s[8].CreditType : "") : "",
					QAuthorizedNCB29_CreditLimit = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[8] != null ? qQAuthorizedNCB2s[8].CreditLimit : 0) : 0,
					QAuthorizedNCB29_OutstandingAR = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[8] != null ? qQAuthorizedNCB2s[8].OutstandingAR : 0) : 0,
					QAuthorizedNCB29_MonthlyRepayment = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[8] != null ? qQAuthorizedNCB2s[8].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB29_EndDate = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[8] != null ? qQAuthorizedNCB2s[8].EndDate : "") : "",
					QAuthorizedNCB29_NCBAccountStatus = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[8] != null ? qQAuthorizedNCB2s[8].NCBAccountStatus : "") : "",

					QAuthorizedNCB210_FinancialInstitution = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[9] != null ? qQAuthorizedNCB2s[9].FinancialInstitution : "") : "",
					QAuthorizedNCB210_CreditType = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[9] != null ? qQAuthorizedNCB2s[9].CreditType : "") : "",
					QAuthorizedNCB210_CreditLimit = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[9] != null ? qQAuthorizedNCB2s[9].CreditLimit : 0) : 0,
					QAuthorizedNCB210_OutstandingAR = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[9] != null ? qQAuthorizedNCB2s[9].OutstandingAR : 0) : 0,
					QAuthorizedNCB210_MonthlyRepayment = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[9] != null ? qQAuthorizedNCB2s[9].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB210_EndDate = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[9] != null ? qQAuthorizedNCB2s[9].EndDate : "") : "",
					QAuthorizedNCB210_NCBAccountStatus = qQAuthorizedNCB2s != null ? (qQAuthorizedNCB2s[9] != null ? qQAuthorizedNCB2s[9].NCBAccountStatus : "") : "",

					QSumAuthorizedNCB2_SumCreditLimit = qQSumAuthorizedNCB2 != null ? qQSumAuthorizedNCB2.SumCreditLimit : 0,
					QSumAuthorizedNCB2_SumOutstandingAR = qQSumAuthorizedNCB2 != null ? qQSumAuthorizedNCB2.SumOutstandingAR : 0,
					QSumAuthorizedNCB2_SumMonthlyRepayment = qQSumAuthorizedNCB2 != null ? qQSumAuthorizedNCB2.SumMonthlyRepayment : 0,

					QAuthorizedNCB31_FinancialInstitution = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[0] != null ? qQAuthorizedNCB3s[0].FinancialInstitution : "") : "",
					QAuthorizedNCB31_CreditType = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[0] != null ? qQAuthorizedNCB3s[0].CreditType : "") : "",
					QAuthorizedNCB31_CreditLimit = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[0] != null ? qQAuthorizedNCB3s[0].CreditLimit : 0) : 0,
					QAuthorizedNCB31_OutstandingAR = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[0] != null ? qQAuthorizedNCB3s[0].OutstandingAR : 0) : 0,
					QAuthorizedNCB31_MonthlyRepayment = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[0] != null ? qQAuthorizedNCB3s[0].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB31_EndDate = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[0] != null ? qQAuthorizedNCB3s[0].EndDate : "") : "",
					QAuthorizedNCB31_NCBAccountStatus = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[0] != null ? qQAuthorizedNCB3s[0].NCBAccountStatus : "") : "",

					QAuthorizedNCB32_FinancialInstitution = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[1] != null ? qQAuthorizedNCB3s[1].FinancialInstitution : "") : "",
					QAuthorizedNCB32_CreditType = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[1] != null ? qQAuthorizedNCB3s[1].CreditType : "") : "",
					QAuthorizedNCB32_CreditLimit = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[1] != null ? qQAuthorizedNCB3s[1].CreditLimit : 0) : 0,
					QAuthorizedNCB32_OutstandingAR = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[1] != null ? qQAuthorizedNCB3s[1].OutstandingAR : 0) : 0,
					QAuthorizedNCB32_MonthlyRepayment = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[1] != null ? qQAuthorizedNCB3s[1].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB32_EndDate = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[1] != null ? qQAuthorizedNCB3s[1].EndDate : "") : "",
					QAuthorizedNCB32_NCBAccountStatus = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[1] != null ? qQAuthorizedNCB3s[1].NCBAccountStatus : "") : "",

					QAuthorizedNCB33_FinancialInstitution = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[2] != null ? qQAuthorizedNCB3s[2].FinancialInstitution : "") : "",
					QAuthorizedNCB33_CreditType = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[2] != null ? qQAuthorizedNCB3s[2].CreditType : "") : "",
					QAuthorizedNCB33_CreditLimit = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[2] != null ? qQAuthorizedNCB3s[2].CreditLimit : 0) : 0,
					QAuthorizedNCB33_OutstandingAR = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[2] != null ? qQAuthorizedNCB3s[2].OutstandingAR : 0) : 0,
					QAuthorizedNCB33_MonthlyRepayment = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[2] != null ? qQAuthorizedNCB3s[2].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB33_EndDate = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[2] != null ? qQAuthorizedNCB3s[2].EndDate : "") : "",
					QAuthorizedNCB33_NCBAccountStatus = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[2] != null ? qQAuthorizedNCB3s[2].NCBAccountStatus : "") : "",

					QAuthorizedNCB34_FinancialInstitution = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[3] != null ? qQAuthorizedNCB3s[3].FinancialInstitution : "") : "",
					QAuthorizedNCB34_CreditType = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[3] != null ? qQAuthorizedNCB3s[3].CreditType : "") : "",
					QAuthorizedNCB34_CreditLimit = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[3] != null ? qQAuthorizedNCB3s[3].CreditLimit : 0) : 0,
					QAuthorizedNCB34_OutstandingAR = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[3] != null ? qQAuthorizedNCB3s[3].OutstandingAR : 0) : 0,
					QAuthorizedNCB34_MonthlyRepayment = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[3] != null ? qQAuthorizedNCB3s[3].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB34_EndDate = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[3] != null ? qQAuthorizedNCB3s[3].EndDate : "") : "",
					QAuthorizedNCB34_NCBAccountStatus = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[3] != null ? qQAuthorizedNCB3s[3].NCBAccountStatus : "") : "",

					QAuthorizedNCB35_FinancialInstitution = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[4] != null ? qQAuthorizedNCB3s[4].FinancialInstitution : "") : "",
					QAuthorizedNCB35_CreditType = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[4] != null ? qQAuthorizedNCB3s[4].CreditType : "") : "",
					QAuthorizedNCB35_CreditLimit = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[4] != null ? qQAuthorizedNCB3s[4].CreditLimit : 0) : 0,
					QAuthorizedNCB35_OutstandingAR = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[4] != null ? qQAuthorizedNCB3s[4].OutstandingAR : 0) : 0,
					QAuthorizedNCB35_MonthlyRepayment = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[4] != null ? qQAuthorizedNCB3s[4].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB35_EndDate = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[4] != null ? qQAuthorizedNCB3s[4].EndDate : "") : "",
					QAuthorizedNCB35_NCBAccountStatus = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[4] != null ? qQAuthorizedNCB3s[4].NCBAccountStatus : "") : "",

					QAuthorizedNCB36_FinancialInstitution = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[5] != null ? qQAuthorizedNCB3s[5].FinancialInstitution : "") : "",
					QAuthorizedNCB36_CreditType = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[5] != null ? qQAuthorizedNCB3s[5].CreditType : "") : "",
					QAuthorizedNCB36_CreditLimit = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[5] != null ? qQAuthorizedNCB3s[5].CreditLimit : 0) : 0,
					QAuthorizedNCB36_OutstandingAR = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[5] != null ? qQAuthorizedNCB3s[5].OutstandingAR : 0) : 0,
					QAuthorizedNCB36_MonthlyRepayment = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[5] != null ? qQAuthorizedNCB3s[5].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB36_EndDate = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[5] != null ? qQAuthorizedNCB3s[5].EndDate : "") : "",
					QAuthorizedNCB36_NCBAccountStatus = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[5] != null ? qQAuthorizedNCB3s[5].NCBAccountStatus : "") : "",

					QAuthorizedNCB37_FinancialInstitution = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[6] != null ? qQAuthorizedNCB3s[6].FinancialInstitution : "") : "",
					QAuthorizedNCB37_CreditType = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[6] != null ? qQAuthorizedNCB3s[6].CreditType : "") : "",
					QAuthorizedNCB37_CreditLimit = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[6] != null ? qQAuthorizedNCB3s[6].CreditLimit : 0) : 0,
					QAuthorizedNCB37_OutstandingAR = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[6] != null ? qQAuthorizedNCB3s[6].OutstandingAR : 0) : 0,
					QAuthorizedNCB37_MonthlyRepayment = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[6] != null ? qQAuthorizedNCB3s[6].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB37_EndDate = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[6] != null ? qQAuthorizedNCB3s[6].EndDate : "") : "",
					QAuthorizedNCB37_NCBAccountStatus = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[6] != null ? qQAuthorizedNCB3s[6].NCBAccountStatus : "") : "",

					QAuthorizedNCB38_FinancialInstitution = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[7] != null ? qQAuthorizedNCB3s[7].FinancialInstitution : "") : "",
					QAuthorizedNCB38_CreditType = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[7] != null ? qQAuthorizedNCB3s[7].CreditType : "") : "",
					QAuthorizedNCB38_CreditLimit = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[7] != null ? qQAuthorizedNCB3s[7].CreditLimit : 0) : 0,
					QAuthorizedNCB38_OutstandingAR = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[7] != null ? qQAuthorizedNCB3s[7].OutstandingAR : 0) : 0,
					QAuthorizedNCB38_MonthlyRepayment = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[7] != null ? qQAuthorizedNCB3s[7].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB38_EndDate = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[7] != null ? qQAuthorizedNCB3s[7].EndDate : "") : "",
					QAuthorizedNCB38_NCBAccountStatus = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[7] != null ? qQAuthorizedNCB3s[7].NCBAccountStatus : "") : "",

					QAuthorizedNCB39_FinancialInstitution = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[8] != null ? qQAuthorizedNCB3s[8].FinancialInstitution : "") : "",
					QAuthorizedNCB39_CreditType = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[8] != null ? qQAuthorizedNCB3s[8].CreditType : "") : "",
					QAuthorizedNCB39_CreditLimit = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[8] != null ? qQAuthorizedNCB3s[8].CreditLimit : 0) : 0,
					QAuthorizedNCB39_OutstandingAR = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[8] != null ? qQAuthorizedNCB3s[8].OutstandingAR : 0) : 0,
					QAuthorizedNCB39_MonthlyRepayment = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[8] != null ? qQAuthorizedNCB3s[8].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB39_EndDate = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[8] != null ? qQAuthorizedNCB3s[8].EndDate : "") : "",
					QAuthorizedNCB39_NCBAccountStatus = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[8] != null ? qQAuthorizedNCB3s[8].NCBAccountStatus : "") : "",

					QAuthorizedNCB310_FinancialInstitution = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[9] != null ? qQAuthorizedNCB3s[9].FinancialInstitution : "") : "",
					QAuthorizedNCB310_CreditType = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[9] != null ? qQAuthorizedNCB3s[9].CreditType : "") : "",
					QAuthorizedNCB310_CreditLimit = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[9] != null ? qQAuthorizedNCB3s[9].CreditLimit : 0) : 0,
					QAuthorizedNCB310_OutstandingAR = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[9] != null ? qQAuthorizedNCB3s[9].OutstandingAR : 0) : 0,
					QAuthorizedNCB310_MonthlyRepayment = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[9] != null ? qQAuthorizedNCB3s[9].MonthlyRepayment : 0) : 0,
					QAuthorizedNCB310_EndDate = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[9] != null ? qQAuthorizedNCB3s[9].EndDate : "") : "",
					QAuthorizedNCB310_NCBAccountStatus = qQAuthorizedNCB3s != null ? (qQAuthorizedNCB3s[9] != null ? qQAuthorizedNCB3s[9].NCBAccountStatus : "") : "",

					QAuthorizedPersonTransForNCB3_AuthorizedPersonName = qAuthorizedPersonTransForNCBs[2] != null ? qAuthorizedPersonTransForNCBs[2].AuthorizedPersonName : "",
					QAuthorizedPersonTransForNCB3_NCBCheckedDate = qAuthorizedPersonTransForNCBs[2] != null ? qAuthorizedPersonTransForNCBs[2].NCBCheckedDate : "",

					QSumAuthorizedNCB3_SumCreditLimit = qQSumAuthorizedNCB3 != null ? qQSumAuthorizedNCB3.SumCreditLimit : 0,
					QSumAuthorizedNCB3_SumOutstandingAR = qQSumAuthorizedNCB3 != null ? qQSumAuthorizedNCB3.SumOutstandingAR : 0,
					QSumAuthorizedNCB3_SumMonthlyRepayment = qQSumAuthorizedNCB3 != null ? qQSumAuthorizedNCB3.SumMonthlyRepayment : 0,

					QProjectReferenceTrans1_ProjectCompanyName = qQProjectReferenceTrans[0] != null ? qQProjectReferenceTrans[0].ProjectCompanyName : "",
					QProjectReferenceTrans1_ProjectName = qQProjectReferenceTrans[0] != null ? qQProjectReferenceTrans[0].ProjectName : "",
					QProjectReferenceTrans1_Remark = qQProjectReferenceTrans[0] != null ? qQProjectReferenceTrans[0].Remark : "",
					QProjectReferenceTrans1_ProjectStatus = qQProjectReferenceTrans[0] != null ? qQProjectReferenceTrans[0].ProjectStatus : "",
					QProjectReferenceTrans1_ProjectValue = qQProjectReferenceTrans[0] != null ? qQProjectReferenceTrans[0].ProjectValue : 0,
					QProjectReferenceTrans1_ProjectCompletion = qQProjectReferenceTrans[0] != null ? qQProjectReferenceTrans[0].ProjectCompletion.ToString() : "",

					QProjectReferenceTrans2_ProjectCompanyName = qQProjectReferenceTrans[1] != null ? qQProjectReferenceTrans[1].ProjectCompanyName : "",
					QProjectReferenceTrans2_ProjectName = qQProjectReferenceTrans[1] != null ? qQProjectReferenceTrans[1].ProjectName : "",
					QProjectReferenceTrans2_Remark = qQProjectReferenceTrans[1] != null ? qQProjectReferenceTrans[1].Remark : "",
					QProjectReferenceTrans2_ProjectStatus = qQProjectReferenceTrans[1] != null ? qQProjectReferenceTrans[1].ProjectStatus : "",
					QProjectReferenceTrans2_ProjectValue = qQProjectReferenceTrans[1] != null ? qQProjectReferenceTrans[1].ProjectValue : 0,
					QProjectReferenceTrans2_ProjectCompletion = qQProjectReferenceTrans[1] != null ? qQProjectReferenceTrans[1].ProjectCompletion.ToString() : "",

					QProjectReferenceTrans3_ProjectCompanyName = qQProjectReferenceTrans[2] != null ? qQProjectReferenceTrans[2].ProjectCompanyName : "",
					QProjectReferenceTrans3_ProjectName = qQProjectReferenceTrans[2] != null ? qQProjectReferenceTrans[2].ProjectName : "",
					QProjectReferenceTrans3_Remark = qQProjectReferenceTrans[2] != null ? qQProjectReferenceTrans[2].Remark : "",
					QProjectReferenceTrans3_ProjectStatus = qQProjectReferenceTrans[2] != null ? qQProjectReferenceTrans[2].ProjectStatus : "",
					QProjectReferenceTrans3_ProjectValue = qQProjectReferenceTrans[2] != null ? qQProjectReferenceTrans[2].ProjectValue : 0,
					QProjectReferenceTrans3_ProjectCompletion = qQProjectReferenceTrans[2] != null ? qQProjectReferenceTrans[2].ProjectCompletion.ToString() : "",

					QProjectReferenceTrans4_ProjectCompanyName = qQProjectReferenceTrans[3] != null ? qQProjectReferenceTrans[3].ProjectCompanyName : "",
					QProjectReferenceTrans4_ProjectName = qQProjectReferenceTrans[3] != null ? qQProjectReferenceTrans[3].ProjectName : "",
					QProjectReferenceTrans4_Remark = qQProjectReferenceTrans[3] != null ? qQProjectReferenceTrans[3].Remark : "",
					QProjectReferenceTrans4_ProjectStatus = qQProjectReferenceTrans[3] != null ? qQProjectReferenceTrans[3].ProjectStatus : "",
					QProjectReferenceTrans4_ProjectValue = qQProjectReferenceTrans[3] != null ? qQProjectReferenceTrans[3].ProjectValue : 0,
					QProjectReferenceTrans4_ProjectCompletion = qQProjectReferenceTrans[3] != null ? qQProjectReferenceTrans[3].ProjectCompletion.ToString() : "",

					QProjectReferenceTrans5_ProjectCompanyName = qQProjectReferenceTrans[4] != null ? qQProjectReferenceTrans[4].ProjectCompanyName : "",
					QProjectReferenceTrans5_ProjectName = qQProjectReferenceTrans[4] != null ? qQProjectReferenceTrans[4].ProjectName : "",
					QProjectReferenceTrans5_Remark = qQProjectReferenceTrans[4] != null ? qQProjectReferenceTrans[4].Remark : "",
					QProjectReferenceTrans5_ProjectStatus = qQProjectReferenceTrans[4] != null ? qQProjectReferenceTrans[4].ProjectStatus : "",
					QProjectReferenceTrans5_ProjectValue = qQProjectReferenceTrans[4] != null ? qQProjectReferenceTrans[4].ProjectValue : 0,
					QProjectReferenceTrans5_ProjectCompletion = qQProjectReferenceTrans[4] != null ? qQProjectReferenceTrans[4].ProjectCompletion.ToString() : "",

					QProjectReferenceTrans6_ProjectCompanyName = qQProjectReferenceTrans[5] != null ? qQProjectReferenceTrans[5].ProjectCompanyName : "",
					QProjectReferenceTrans6_ProjectName = qQProjectReferenceTrans[5] != null ? qQProjectReferenceTrans[5].ProjectName : "",
					QProjectReferenceTrans6_Remark = qQProjectReferenceTrans[5] != null ? qQProjectReferenceTrans[5].Remark : "",
					QProjectReferenceTrans6_ProjectStatus = qQProjectReferenceTrans[5] != null ? qQProjectReferenceTrans[5].ProjectStatus : "",
					QProjectReferenceTrans6_ProjectValue = qQProjectReferenceTrans[5] != null ? qQProjectReferenceTrans[5].ProjectValue : 0,
					QProjectReferenceTrans6_ProjectCompletion = qQProjectReferenceTrans[5] != null ? qQProjectReferenceTrans[5].ProjectCompletion.ToString() : "",

					QProjectReferenceTrans7_ProjectCompanyName = qQProjectReferenceTrans[6] != null ? qQProjectReferenceTrans[6].ProjectCompanyName : "",
					QProjectReferenceTrans7_ProjectName = qQProjectReferenceTrans[6] != null ? qQProjectReferenceTrans[6].ProjectName : "",
					QProjectReferenceTrans7_Remark = qQProjectReferenceTrans[6] != null ? qQProjectReferenceTrans[6].Remark : "",
					QProjectReferenceTrans7_ProjectStatus = qQProjectReferenceTrans[6] != null ? qQProjectReferenceTrans[6].ProjectStatus : "",
					QProjectReferenceTrans7_ProjectValue = qQProjectReferenceTrans[6] != null ? qQProjectReferenceTrans[6].ProjectValue : 0,
					QProjectReferenceTrans7_ProjectCompletion = qQProjectReferenceTrans[6] != null ? qQProjectReferenceTrans[6].ProjectCompletion.ToString() : "",

					QProjectReferenceTrans8_ProjectCompanyName = qQProjectReferenceTrans[7] != null ? qQProjectReferenceTrans[7].ProjectCompanyName : "",
					QProjectReferenceTrans8_ProjectName = qQProjectReferenceTrans[7] != null ? qQProjectReferenceTrans[7].ProjectName : "",
					QProjectReferenceTrans8_Remark = qQProjectReferenceTrans[7] != null ? qQProjectReferenceTrans[7].Remark : "",
					QProjectReferenceTrans8_ProjectStatus = qQProjectReferenceTrans[7] != null ? qQProjectReferenceTrans[7].ProjectStatus : "",
					QProjectReferenceTrans8_ProjectValue = qQProjectReferenceTrans[7] != null ? qQProjectReferenceTrans[7].ProjectValue : 0,
					QProjectReferenceTrans8_ProjectCompletion = qQProjectReferenceTrans[7] != null ? qQProjectReferenceTrans[7].ProjectCompletion.ToString() : "",

					QProjectReferenceTrans9_ProjectCompanyName = qQProjectReferenceTrans[8] != null ? qQProjectReferenceTrans[8].ProjectCompanyName : "",
					QProjectReferenceTrans9_ProjectName = qQProjectReferenceTrans[8] != null ? qQProjectReferenceTrans[8].ProjectName : "",
					QProjectReferenceTrans9_Remark = qQProjectReferenceTrans[8] != null ? qQProjectReferenceTrans[8].Remark : "",
					QProjectReferenceTrans9_ProjectStatus = qQProjectReferenceTrans[8] != null ? qQProjectReferenceTrans[8].ProjectStatus : "",
					QProjectReferenceTrans9_ProjectValue = qQProjectReferenceTrans[8] != null ? qQProjectReferenceTrans[8].ProjectValue : 0,
					QProjectReferenceTrans9_ProjectCompletion = qQProjectReferenceTrans[8] != null ? qQProjectReferenceTrans[8].ProjectCompletion.ToString() : "",

					QProjectReferenceTrans10_ProjectCompanyName = qQProjectReferenceTrans[9] != null ? qQProjectReferenceTrans[9].ProjectCompanyName : "",
					QProjectReferenceTrans10_ProjectName = qQProjectReferenceTrans[9] != null ? qQProjectReferenceTrans[9].ProjectName : "",
					QProjectReferenceTrans10_Remark = qQProjectReferenceTrans[9] != null ? qQProjectReferenceTrans[9].Remark : "",
					QProjectReferenceTrans10_ProjectStatus = qQProjectReferenceTrans[9] != null ? qQProjectReferenceTrans[9].ProjectStatus : "",
					QProjectReferenceTrans10_ProjectValue = qQProjectReferenceTrans[9] != null ? qQProjectReferenceTrans[9].ProjectValue : 0,
					QProjectReferenceTrans10_ProjectCompletion = qQProjectReferenceTrans[9] != null ? qQProjectReferenceTrans[9].ProjectCompletion.ToString() : "",

					QCAReqBuyerCreditOutstandingPrivateSum_SumARBalance = qCAReqBuyerCreditOutstandingPrivateSum != null ? qCAReqBuyerCreditOutstandingPrivateSum.SumARBalance : 0,
					QCAReqBuyerCreditOutstandingGovSum_SumARBalance = qCAReqBuyerCreditOutstandingGovSum[0] != null ? qCAReqBuyerCreditOutstandingGovSum[0].SumARBalance : 0,

					//-------------------------------------------------------------------------------------------------
					QCAReqCreditOutStanding1_ApprovedCreditLimit = qCAReqCreditOutStanding[0] != null ? qCAReqCreditOutStanding[0].ApprovedCreditLimit : 0,
					QCAReqCreditOutStanding1_ARBalance = qCAReqCreditOutStanding[0] != null ? qCAReqCreditOutStanding[0].ARBalance : 0,
					QCAReqCreditOutStanding1_CreditLimitBalance = qCAReqCreditOutStanding[0] != null ? qCAReqCreditOutStanding[0].CreditLimitBalance : 0,
					QCAReqCreditOutStanding1_ReserveToBeRefund = qCAReqCreditOutStanding[0] != null ? qCAReqCreditOutStanding[0].ReserveToBeRefund : 0,
					QCAReqCreditOutStanding1_AccumRetentionAmount = qCAReqCreditOutStanding[0] != null ? qCAReqCreditOutStanding[0].AccumRetentionAmount : 0,
					QCAReqBuyerCreditOutstandingPrivateCOUNT_CountCAbuyerCreditOutstandingPrivate = qCAReqBuyerCreditOutstandingPrivateCOUNT.CountCAbuyerCreditOutstandingPrivate,
					QCAReqBuyerCreditOutstandingGovCOUNT_CountCAbuyerCreditOutstandingGovernment = qCAReqBuyerCreditOutstandingGovCOUNT.CountCAbuyerCreditOutstandingPrivate,

					Variable_FactoringPurchaseCondition = variable.FactoringPurchaseCondition,
					Variable_AssignmentCondition = variable.AssignmentCondition,

				};
				return bookmarkDocumentCreditApplicationConsideration;
			}                  
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}

