﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.Repositories;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Core.Service.ConditionService;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public partial interface ICreditAppRequestTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CreditAppRequestTableItemView>> GetDropDownItem(SearchParameter search);
		IEnumerable<SelectItem<CreditAppRequestTableItemView>> CreditAppRequestDropdownByDocumentStatus(SearchParameter search);

		#endregion DropDown
		SearchResult<CreditAppRequestTableListView> GetListvw(SearchParameter search);
		SearchResult<CreditAppRequestTableListView> GetListReview(SearchParameter search);
		CreditAppRequestTableItemView GetByIdvw(Guid id);
		CreditAppRequestTableItemView GetByIdvwByCreditAppRequestLine(Guid id);
		CreditAppRequestTable Find(params object[] keyValues);
		CreditAppRequestTable GetCreditAppRequestTableByIdNoTracking(Guid guid);
		IQueryable<CreditAppRequestTable> GetCreditAppRequestTableByRefCreditAppIdNoTrackingAccessLavel(Guid guid);
		IQueryable<CreditAppRequestTable> GetCreditAppRequestTableByRefCreditAppId(Guid guid);
		CreditAppRequestTable CreateCreditAppRequestTable(CreditAppRequestTable creditAppRequestTable);
		void CreateCreditAppRequestTableVoid(CreditAppRequestTable creditAppRequestTable);
		CreditAppRequestTable UpdateCreditAppRequestTable(CreditAppRequestTable dbCreditAppRequestTable, CreditAppRequestTable inputCreditAppRequestTable, List<string> skipUpdateFields = null);
		void UpdateCreditAppRequestTableVoid(CreditAppRequestTable dbCreditAppRequestTable, CreditAppRequestTable inputCreditAppRequestTable, List<string> skipUpdateFields = null);
		void Remove(CreditAppRequestTable item);
		CreditAppRequestTable GetCreditAppRequestTableByIdNoTrackingByAccessLevel(Guid guid);
		#region function
		CancelCreditAppRequestResultView GetCancelCreditApplicationRequestByAppReqGUID(Guid guid);
		IEnumerable<SelectItem<CreditAppRequestTableItemView>> GetDropDownItemForAddendumBusiness(string refGuid);
		#endregion
		#region Amend CA
		IQueryable<CreditAppRequestTable> GetExistByCAReqAmend(CreditAppRequestTableAmend item, CreditAppRequestTable caRequest);
		CreditAppRequestTable CopyByCreditAppRequestTableAmend(Guid creditAppTableGUID);
		CreditAppRequestTable GetCreditAppRequestTableByAmendCANoTrackingByAccessLevel(Guid guid);
		#endregion Amend CA
		#region Amend CA Line
		CreditAppRequestTable CopyCreditAppRequestForCreateCreditAppRequestLineAmend(CreditAppRequestLineAmendItemView creditAppRequestLineAmendView);
		CreditAppRequestTable GetCreditAppRequestTableByRefCreditAppLineAndCreditAppRequestType(Guid refCreditAppLineGUID, int creditAppRequestType);
		#endregion  Amend CA Line
		CreditAppRequestTable GetCreditAppRequestTableByCreditAppRequestLine(Guid guid);
		IEnumerable<SelectItem<CreditAppRequestTableItemView>> GetDropDownItemForFunctionAddendum(string refGuid);
		IEnumerable<SelectItem<CreditAppRequestTableItemView>> GetDropDownItemForFunctionLoanRequest(string refGuid);
		GenMainAgmAddendumView GetDatabyCreditAppRequestID(string id);
		GenBusCollateralAgmAddendumView GetDatabyCreditAppRequestIDforBusiness(string id);
		#region Loan Request
		CreditAppRequestTableItemView GetLoanRequestOrBuyerMatchingCreditAppRequestTableInitialData(Guid creditApId, int creditAppRequestType);
		#endregion Loan Request
		#region WorkFlow
		CreditAppRequestTableItemView GetByIdwf(Guid guid);
		#endregion WorkFlow
		#region Review
		CreditAppRequestTableItemView GetReviewByIdvw(Guid guid);
		CreditAppRequestTableItemView GetReviewCreditAppRequestInitialData(string id);
		#endregion Review
		#region CloseCustomerCreditLimit
		public CreditAppRequestTableItemView GetCloseCustomerCreditLimitInitialData(string id);
		SearchResult<CreditAppRequestTableListView> GetListClosing(SearchParameter search);
		CreditAppRequestTableItemView GetClosingByIdvw(Guid guid);
		#endregion CloseCustomerCreditLimit
		void ValidateAdd(CreditAppRequestTable item);
		void ValidateAdd(IEnumerable<CreditAppRequestTable> items);

		#region K2 Function
		GetCreditApplicationRequestEmailContentK2ResultView GetCreditApplicationRequestEmailContentK2(Guid id, string userName);
		#endregion  K2 Function

		BookmarkDocumentQueryCreditApplicationPF GetBookmarkDocumentQueryCreditApplicationPFValues(Guid refGUID, Guid bookmarkDocumentTransGUID);
		BookmarkDocumentQueryCusotmerAmendInformation GetBookmarkDocumentQueryCustomerAmendInformation(Guid refGUID, Guid bookmarkDocumentTransGUID);
		BookmarkDocumentQueryBuyerAmendInformation GetBookmarkDocumentQueryBuyerAmendInformation(Guid refGUID, Guid bookmarkDocumentTransGUID);
		CreditLimitClosingBookmarkView GetCreditLimitClosingBookmarkValue(Guid refGuid, Guid bookmarkDocumentTransGuid);
	}
	public partial class CreditAppRequestTableRepo : CompanyBaseRepository<CreditAppRequestTable>, ICreditAppRequestTableRepo
	{
		public CreditAppRequestTableRepo(SmartAppDbContext context) : base(context) { }
		public CreditAppRequestTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CreditAppRequestTable GetCreditAppRequestTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CreditAppRequestTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IQueryable<CreditAppRequestTable> GetCreditAppRequestTableByRefCreditAppIdNoTrackingAccessLavel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.RefCreditAppTableGUID == guid).AsNoTracking().FilterByAccessLevel(SysParm.AccessLevel);
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IQueryable<CreditAppRequestTable> GetCreditAppRequestTableByRefCreditAppId(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.RefCreditAppTableGUID == guid).AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CreditAppRequestTableItemViewMap> GetDropDownQuery()
		{
			return (from creditAppRequestTable in Entity
					join creditLimitType in db.Set<CreditLimitType>() on creditAppRequestTable.CreditLimitTypeGUID equals creditLimitType.CreditLimitTypeGUID
					join documentStatus in db.Set<DocumentStatus>() on creditAppRequestTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID

					join creditAppTable in db.Set<CreditAppTable>()
					on creditAppRequestTable.RefCreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljCreditAppRequestTableCreditAppTable
					from creditAppTable in ljCreditAppRequestTableCreditAppTable.DefaultIfEmpty()

					join consortiumTable in db.Set<ConsortiumTable>()
					on creditAppRequestTable.ConsortiumTableGUID equals consortiumTable.ConsortiumTableGUID into ljConsortiumTable
					from consortiumTable in ljConsortiumTable.DefaultIfEmpty()

					join refCreditAppTable in db.Set<CreditAppTable>()
					on creditAppRequestTable.CreditAppRequestTableGUID equals refCreditAppTable.RefCreditAppRequestTableGUID into ljRefCreditAppTable
					from refCreditAppTable in ljRefCreditAppTable.DefaultIfEmpty()

					select new CreditAppRequestTableItemViewMap
					{
						CompanyGUID = creditAppRequestTable.CompanyGUID,
						Owner = creditAppRequestTable.Owner,
						OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID,
						CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
						CreditAppRequestId = creditAppRequestTable.CreditAppRequestId,
						Description = creditAppRequestTable.Description,
						CustomerTableGUID = creditAppRequestTable.CustomerTableGUID,
						DocumentStatus_StatusId = documentStatus.StatusId,
						RefCreditAppTableGUID = (creditAppRequestTable.RefCreditAppTableGUID == null && refCreditAppTable != null) ? refCreditAppTable.CreditAppTableGUID : creditAppRequestTable.RefCreditAppTableGUID,
						CreditLimitTypeGUID = creditAppRequestTable.CreditLimitTypeGUID,
						ConsortiumTableGUID = creditAppRequestTable.ConsortiumTableGUID,
						CreditLimitType_CreditLimitTypeId = creditLimitType.CreditLimitTypeId,
						CreditAppTable_CreditAppId = (creditAppTable != null) ? creditAppTable.CreditAppId : null,
						ConsortiumTable_ConsortiumId = (consortiumTable != null) ? consortiumTable.ConsortiumId : null,
						ProductType = creditAppRequestTable.ProductType,
						CreditLimitType_Values = SmartAppUtil.GetDropDownLabel(creditLimitType.CreditLimitTypeId, creditLimitType.Description),
						CreditAppTable_Values = (creditAppTable != null) ? SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description) :
												(refCreditAppTable != null) ? SmartAppUtil.GetDropDownLabel(refCreditAppTable.CreditAppId, refCreditAppTable.Description) : null,
						ConsortiumTable_Values = (consortiumTable != null) ? SmartAppUtil.GetDropDownLabel(consortiumTable.ConsortiumId, consortiumTable.Description) : null,
					});
		}
		private IQueryable<CreditAppRequestTableItemViewMap> GetDropDownQueryForAddendum()
		{
			return (from creditAppRequestTable in Entity
					join creditLimitType in db.Set<CreditLimitType>() on creditAppRequestTable.CreditLimitTypeGUID equals creditLimitType.CreditLimitTypeGUID
					join documentStatus in db.Set<DocumentStatus>() on creditAppRequestTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID

					join creditAppTable in db.Set<CreditAppTable>()
					on creditAppRequestTable.RefCreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljCreditAppRequestTableCreditAppTable
					from creditAppTable in ljCreditAppRequestTableCreditAppTable.DefaultIfEmpty()

					join consortiumTable in db.Set<ConsortiumTable>()
					on creditAppRequestTable.ConsortiumTableGUID equals consortiumTable.ConsortiumTableGUID into ljConsortiumTable
					from consortiumTable in ljConsortiumTable.DefaultIfEmpty()

					select new CreditAppRequestTableItemViewMap
					{
						CompanyGUID = creditAppRequestTable.CompanyGUID,
						Owner = creditAppRequestTable.Owner,
						OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID,
						CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
						CreditAppRequestId = creditAppRequestTable.CreditAppRequestId,
						Description = creditAppRequestTable.Description,
						CustomerTableGUID = creditAppRequestTable.CustomerTableGUID,
						DocumentStatus_StatusId = documentStatus.StatusId,
						RefCreditAppTableGUID = creditAppRequestTable.RefCreditAppTableGUID,
						CreditLimitTypeGUID = creditAppRequestTable.CreditLimitTypeGUID,
						CreditLimitType_CreditLimitTypeId = creditLimitType.CreditLimitTypeId,
						CreditAppTable_CreditAppId = (creditAppTable != null) ? creditAppTable.CreditAppId : null,
						ConsortiumTableGUID = creditAppRequestTable.ConsortiumTableGUID,
						ConsortiumTable_ConsortiumId = (consortiumTable != null) ? consortiumTable.ConsortiumId : null

					});
		}
		public IEnumerable<SelectItem<CreditAppRequestTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditAppRequestTable>(search, SysParm.CompanyGUID);
				var creditAppRequestTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CreditAppRequestTableItemViewMap, CreditAppRequestTableItemView>().ToDropDownItem(search.ExcludeRowData);
				return creditAppRequestTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<CreditAppRequestTableItemView>> GetDropDownItemForAddendum(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditAppRequestTable>(search, SysParm.CompanyGUID);
				var creditAppRequestTable = GetDropDownQueryForAddendum().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CreditAppRequestTableItemViewMap, CreditAppRequestTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return creditAppRequestTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public GenMainAgmAddendumView GetDatabyCreditAppRequestID(string id)
		{
			try
			{
				var existCreditAppRequest = (from creditAppRequestTable in db.Set<CreditAppRequestTable>()
											 join creditLimitType in db.Set<CreditLimitType>() on creditAppRequestTable.CreditLimitTypeGUID equals creditLimitType.CreditLimitTypeGUID
											 where creditAppRequestTable.CreditAppRequestTableGUID == id.StringToGuid()
											 select new GenMainAgmAddendumView
											 {
												 CreditAppRequestType = creditAppRequestTable.CreditAppRequestType,
												 CreditAppRequestTable_Values = SmartAppUtil.GetDropDownLabel(creditAppRequestTable.CreditAppRequestId, creditAppRequestTable.Description),
												 CreditLimitType_Values = SmartAppUtil.GetDropDownLabel(creditLimitType.CreditLimitTypeId, creditLimitType.Description),
												 RequestDate = creditAppRequestTable.RequestDate.DateToString(),
												 CreditAppRequestDescription = creditAppRequestTable.Description,
											 }).FirstOrDefault();

				return existCreditAppRequest;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public GenBusCollateralAgmAddendumView GetDatabyCreditAppRequestIDforBusiness(string id)
		{
			try
			{
				var existCreditAppRequest = (from creditAppRequestTable in db.Set<CreditAppRequestTable>()
											 join creditLimitType in db.Set<CreditLimitType>() on creditAppRequestTable.CreditLimitTypeGUID equals creditLimitType.CreditLimitTypeGUID
											 where creditAppRequestTable.CreditAppRequestTableGUID == id.StringToGuid()
											 select new GenBusCollateralAgmAddendumView
											 {
												 CreditAppRequestType = creditAppRequestTable.CreditAppRequestType,
												 CreditAppRequestTable_Values = SmartAppUtil.GetDropDownLabel(creditAppRequestTable.CreditAppRequestId, creditAppRequestTable.Description),
												 CreditLimitType_Values = SmartAppUtil.GetDropDownLabel(creditLimitType.CreditLimitTypeId, creditLimitType.Description),
												 RequestDate = creditAppRequestTable.RequestDate.DateToString(),
												 CreditAppRequestDescription = creditAppRequestTable.Description,
											 }).FirstOrDefault();

				return existCreditAppRequest;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public IEnumerable<SelectItem<CreditAppRequestTableItemView>> GetDropDownItemForFunctionAddendum(string refGuid)
		{
			try
			{
				var exitMainAgreement = (from mainAgreementTable in db.Set<MainAgreementTable>()
										 where mainAgreementTable.MainAgreementTableGUID == refGuid.StringToGuid()
										 select mainAgreementTable
										 ).FirstOrDefault();

				var existCreditAppRequest = (from creditAppRequestTable in db.Set<CreditAppRequestTable>()
											 join documentStatus in db.Set<DocumentStatus>() on creditAppRequestTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
											 where creditAppRequestTable.RefCreditAppTableGUID == exitMainAgreement.CreditAppTableGUID
													&& Convert.ToInt32(documentStatus.StatusId) == (int)CreditAppRequestStatus.Approved
													&& (creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.ActiveAmendCustomerCreditLimit
													|| creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.AmendCustomerInfo
													|| creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.AmendCustomerBuyerCreditLimit
													|| creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.AmendBuyerInfo)

											 select new CreditAppRequestTableItemViewMap
											 {
												 CompanyGUID = creditAppRequestTable.CompanyGUID,
												 Owner = creditAppRequestTable.Owner,
												 OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID,
												 CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
												 CreditAppRequestId = creditAppRequestTable.CreditAppRequestId,
												 Description = creditAppRequestTable.Description,
												 CustomerTableGUID = creditAppRequestTable.CustomerTableGUID,
												 DocumentStatus_StatusId = documentStatus.StatusId,
												 RefCreditAppTableGUID = creditAppRequestTable.RefCreditAppTableGUID,
												 CreditLimitTypeGUID = creditAppRequestTable.CreditLimitTypeGUID,
											 }).ToMaps<CreditAppRequestTableItemViewMap, CreditAppRequestTableItemView>().ToDropDownItem();

				return existCreditAppRequest;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<CreditAppRequestTableItemView>> GetDropDownItemForFunctionLoanRequest(string refGuid)
		{
			try
			{
				var exitMainAgreement = (from mainAgreementTable in db.Set<MainAgreementTable>()
										 where mainAgreementTable.MainAgreementTableGUID == refGuid.StringToGuid()
										 select mainAgreementTable
						 ).FirstOrDefault();

				var existCreditAppRequest = (from creditAppRequestTable in db.Set<CreditAppRequestTable>()
											 join documentStatus in db.Set<DocumentStatus>()
											 on creditAppRequestTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus
											 from documentStatus in ljdocumentStatus.DefaultIfEmpty()

											 where creditAppRequestTable.RefCreditAppTableGUID == exitMainAgreement.CreditAppTableGUID
													&& Convert.ToInt32(documentStatus.StatusId) == (int)CreditAppRequestStatus.Approved
													&& creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.LoanRequest

											 select new CreditAppRequestTableItemViewMap
											 {
												 CompanyGUID = creditAppRequestTable.CompanyGUID,
												 Owner = creditAppRequestTable.Owner,
												 OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID,
												 CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
												 CreditAppRequestId = creditAppRequestTable.CreditAppRequestId,
												 Description = creditAppRequestTable.Description,
												 CustomerTableGUID = creditAppRequestTable.CustomerTableGUID,
												 DocumentStatus_StatusId = documentStatus.StatusId,
												 RefCreditAppTableGUID = creditAppRequestTable.RefCreditAppTableGUID,
												 CreditLimitTypeGUID = creditAppRequestTable.CreditLimitTypeGUID,
											 }).ToMaps<CreditAppRequestTableItemViewMap, CreditAppRequestTableItemView>().ToDropDownItem();

				return existCreditAppRequest;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<CreditAppRequestTableItemView>> GetDropDownItemForAddendumBusiness(string refGuid)
		{
			try
			{
				var exitBusinessCollateralAGM = (from businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
												 where businessCollateralAgmTable.BusinessCollateralAgmTableGUID == refGuid.StringToGuid()
												 select businessCollateralAgmTable
										 ).FirstOrDefault();

				var existCreditAppRequest = (from creditAppRequestTable in db.Set<CreditAppRequestTable>()
											 join documentStatus in db.Set<DocumentStatus>() on creditAppRequestTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
											 join creditLimitType in db.Set<CreditLimitType>() on creditAppRequestTable.CreditLimitTypeGUID equals creditLimitType.CreditLimitTypeGUID into ljcreditLimitType
											 from creditLimitType in ljcreditLimitType.DefaultIfEmpty()

											 where creditAppRequestTable.RefCreditAppTableGUID == exitBusinessCollateralAGM.CreditAppTableGUID
													&& Convert.ToInt32(documentStatus.StatusId) == (int)CreditAppRequestStatus.Approved
													&& (creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.BuyerMatching
													|| creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.LoanRequest
													|| creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.ActiveAmendCustomerCreditLimit
													|| creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.AmendCustomerInfo
													|| creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.AmendCustomerBuyerCreditLimit
													|| creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.AmendBuyerInfo)

											 select new CreditAppRequestTableItemViewMap
											 {
												 CompanyGUID = creditAppRequestTable.CompanyGUID,
												 Owner = creditAppRequestTable.Owner,
												 OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID,
												 CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
												 CreditAppRequestId = creditAppRequestTable.CreditAppRequestId,
												 Description = creditAppRequestTable.Description,
												 CustomerTableGUID = creditAppRequestTable.CustomerTableGUID,
												 DocumentStatus_StatusId = documentStatus.StatusId,
												 RefCreditAppTableGUID = creditAppRequestTable.RefCreditAppTableGUID,
												 CreditLimitTypeGUID = creditAppRequestTable.CreditLimitTypeGUID,
												 CreditLimitType_Values = SmartAppUtil.GetDropDownLabel(creditLimitType.CreditLimitTypeId, creditLimitType.Description),
												 RequestDate = creditAppRequestTable.RequestDate,
												 CreditAppRequestType = creditAppRequestTable.CreditAppRequestType,
												 CreditAppRequestTable_Values = SmartAppUtil.GetDropDownLabel(creditAppRequestTable.CreditAppRequestId, creditAppRequestTable.Description),
											 }).ToMaps<CreditAppRequestTableItemViewMap, CreditAppRequestTableItemView>().ToDropDownItem();

				return existCreditAppRequest;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private IQueryable<CreditAppRequestTableItemViewMap> GetDropDownQueryByDocumentStatus()
		{
			var result = (from creditAppRequestTable in db.Set<CreditAppRequestTable>()
						  join documentStatus in db.Set<DocumentStatus>()
						  on creditAppRequestTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus
						  from documentStatus in ljdocumentStatus.DefaultIfEmpty()
						  select new CreditAppRequestTableItemViewMap
						  {
							  CompanyGUID = creditAppRequestTable.CompanyGUID,
							  Owner = creditAppRequestTable.Owner,
							  OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID,
							  CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
							  CreditAppRequestId = creditAppRequestTable.CreditAppRequestId,
							  Description = creditAppRequestTable.Description,
							  CustomerTableGUID = creditAppRequestTable.CustomerTableGUID,
							  RefCreditAppTableGUID = creditAppRequestTable.RefCreditAppTableGUID,
							  CreditLimitTypeGUID = creditAppRequestTable.CreditLimitTypeGUID,
							  ConsortiumTableGUID = creditAppRequestTable.ConsortiumTableGUID,
							  ProductType = creditAppRequestTable.ProductType,
							  DocumentStatus_StatusId = documentStatus.StatusId
						  });
			return result;
		}
		public IEnumerable<SelectItem<CreditAppRequestTableItemView>> CreditAppRequestDropdownByDocumentStatus(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditAppRequestTable>(search, SysParm.CompanyGUID);
				var creditAppRequestTable = GetDropDownQueryByDocumentStatus().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CreditAppRequestTableItemViewMap, CreditAppRequestTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return creditAppRequestTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CreditAppRequestTableListViewMap> GetListQuery()
		{
			var result = (from creditAppRequestTable in Entity
						  join customerTable in db.Set<CustomerTable>() on creditAppRequestTable.CustomerTableGUID equals customerTable.CustomerTableGUID
						  join creditLimitType in db.Set<CreditLimitType>() on creditAppRequestTable.CreditLimitTypeGUID equals creditLimitType.CreditLimitTypeGUID
						  join documentStatus in db.Set<DocumentStatus>() on creditAppRequestTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID

						  select new CreditAppRequestTableListViewMap
						  {
							  CompanyGUID = creditAppRequestTable.CompanyGUID,
							  Owner = creditAppRequestTable.Owner,
							  OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID,
							  CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
							  CreditAppRequestId = creditAppRequestTable.CreditAppRequestId,
							  DocumentStatusGUID = creditAppRequestTable.DocumentStatusGUID,
							  CustomerTableGUID = creditAppRequestTable.CustomerTableGUID,
							  ProductType = creditAppRequestTable.ProductType,
							  CreditAppRequestType = creditAppRequestTable.CreditAppRequestType,
							  CreditLimitTypeGUID = creditAppRequestTable.CreditLimitTypeGUID,
							  RequestDate = creditAppRequestTable.RequestDate,
							  CreditLimitRequest = creditAppRequestTable.CreditLimitRequest,
							  ApprovedCreditLimitRequest = creditAppRequestTable.ApprovedCreditLimitRequest,
							  CreditLimitType_Values = SmartAppUtil.GetDropDownLabel(creditLimitType.CreditLimitTypeId, creditLimitType.Description),
							  CreditLimitType_CreditLimitTypeId = creditLimitType.CreditLimitTypeId,
							  DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
							  DocumentStatus_StatusId = documentStatus.StatusId,
							  CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
							  CustomerTable_CustomerId = customerTable.CustomerId,
							  RefCreditAppTableGUID = creditAppRequestTable.RefCreditAppTableGUID,
						  });
			return result;
		}
		public SearchResult<CreditAppRequestTableListView> GetListReview(SearchParameter search)
		{
			var result = new SearchResult<CreditAppRequestTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditAppRequestTable>(search, SysParm.CompanyGUID);
				var total = GetListReviewQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListReviewQuery().Where(predicate.Predicates, predicate.Values)
												.OrderBy(predicate.Sorting)
												.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
												.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
												.ToMaps<CreditAppRequestTableListViewMap, CreditAppRequestTableListView>();
				result = list.SetSearchResult<CreditAppRequestTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private IQueryable<CreditAppRequestTableListViewMap> GetListReviewQuery()
		{
			var result = (from creditAppLine in db.Set<CreditAppLine>()
						  join creditAppRequestLine in db.Set<CreditAppRequestLine>()
						  on creditAppLine.CreditAppLineGUID equals creditAppRequestLine.RefCreditAppLineGUID into ljCreditAppRequestLine
						  from creditAppRequestLine in ljCreditAppRequestLine.DefaultIfEmpty()
						  join creditAppRequestTable in Entity
						  on creditAppRequestLine.CreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID
						  join customerTable in db.Set<CustomerTable>() on creditAppRequestTable.CustomerTableGUID equals customerTable.CustomerTableGUID
						  join creditLimitType in db.Set<CreditLimitType>() on creditAppRequestTable.CreditLimitTypeGUID equals creditLimitType.CreditLimitTypeGUID
						  join documentStatus in db.Set<DocumentStatus>() on creditAppRequestTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
						  join buyerTable in db.Set<BuyerTable>() on creditAppRequestLine.BuyerTableGUID equals buyerTable.BuyerTableGUID
						  join creditAppTable in db.Set<CreditAppTable>() on creditAppRequestTable.RefCreditAppTableGUID equals creditAppTable.CreditAppTableGUID
						  select new CreditAppRequestTableListViewMap
						  {
							  CompanyGUID = creditAppRequestTable.CompanyGUID,
							  Owner = creditAppRequestTable.Owner,
							  OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID,
							  CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
							  CreditAppRequestId = creditAppRequestTable.CreditAppRequestId,
							  DocumentStatusGUID = creditAppRequestTable.DocumentStatusGUID,
							  CustomerTableGUID = creditAppRequestTable.CustomerTableGUID,
							  ProductType = creditAppRequestTable.ProductType,
							  CreditAppRequestType = creditAppRequestTable.CreditAppRequestType,
							  CreditLimitTypeGUID = creditAppRequestTable.CreditLimitTypeGUID,
							  RequestDate = creditAppRequestTable.RequestDate,
							  CreditLimitRequest = creditAppRequestTable.CreditLimitRequest,
							  ApprovedCreditLimitRequest = creditAppRequestTable.ApprovedCreditLimitRequest,
							  CreditLimitType_Values = SmartAppUtil.GetDropDownLabel(creditLimitType.CreditLimitTypeId, creditLimitType.Description),
							  CreditLimitType_CreditLimitTypeId = creditLimitType.CreditLimitTypeId,
							  DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
							  DocumentStatus_StatusId = documentStatus.StatusId,
							  CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
							  CustomerTable_CustomerId = customerTable.CustomerId,
							  CreditAppLineGUID = creditAppLine.CreditAppLineGUID,
							  BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
							  CreditAppRequestLine_BuyerId = buyerTable.BuyerId,
							  CreditAppRequestLine_BuyerTableGUID = buyerTable.BuyerTableGUID,
							  RefCreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
							  RefCreditAppTable_CreditAppId = creditAppTable.CreditAppId,
							  RefCreditAppTableGUID = creditAppRequestTable.RefCreditAppTableGUID,

							  BuyerCreditLimit = creditAppRequestLine.BuyerCreditLimit,
							  CreditLimitLineRequest = creditAppRequestLine.CreditLimitLineRequest,
						  });
			return result;
		}
		public SearchResult<CreditAppRequestTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CreditAppRequestTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditAppRequestTable>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
												.OrderBy(predicate.Sorting)
												.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
												.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
												.ToMaps<CreditAppRequestTableListViewMap, CreditAppRequestTableListView>();
				result = list.SetSearchResult<CreditAppRequestTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}

		public SearchResult<CreditAppRequestTableListView> GetListClosing(SearchParameter search)
		{
			var result = new SearchResult<CreditAppRequestTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditAppRequestTable>(search, SysParm.CompanyGUID);
				var total = GetListClosingQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListClosingQuery().Where(predicate.Predicates, predicate.Values)
												.OrderBy(predicate.Sorting)
												.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
												.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
												.ToMaps<CreditAppRequestTableListViewMap, CreditAppRequestTableListView>();
				result = list.SetSearchResult<CreditAppRequestTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private IQueryable<CreditAppRequestTableListViewMap> GetListClosingQuery()
		{
			return (from creditAppTable in db.Set<CreditAppTable>()
					join creditAppRequestTable in Entity
					on creditAppTable.CreditAppTableGUID equals creditAppRequestTable.RefCreditAppTableGUID
					join customerTable in db.Set<CustomerTable>() on creditAppRequestTable.CustomerTableGUID equals customerTable.CustomerTableGUID
					join creditLimitType in db.Set<CreditLimitType>() on creditAppRequestTable.CreditLimitTypeGUID equals creditLimitType.CreditLimitTypeGUID
					join documentStatus in db.Set<DocumentStatus>() on creditAppRequestTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
					select new CreditAppRequestTableListViewMap
					{
						CompanyGUID = creditAppRequestTable.CompanyGUID,
						Owner = creditAppRequestTable.Owner,
						OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID,
						CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
						CreditAppRequestId = creditAppRequestTable.CreditAppRequestId,
						DocumentStatusGUID = creditAppRequestTable.DocumentStatusGUID,
						CustomerTableGUID = creditAppRequestTable.CustomerTableGUID,
						ProductType = creditAppRequestTable.ProductType,
						CreditAppRequestType = creditAppRequestTable.CreditAppRequestType,
						CreditLimitTypeGUID = creditAppRequestTable.CreditLimitTypeGUID,
						RequestDate = creditAppRequestTable.RequestDate,
						CreditLimitRequest = creditAppRequestTable.CreditLimitRequest,
						ApprovedCreditLimitRequest = creditAppRequestTable.ApprovedCreditLimitRequest,
						CreditLimitType_Values = SmartAppUtil.GetDropDownLabel(creditLimitType.CreditLimitTypeId, creditLimitType.Description),
						CreditLimitType_CreditLimitTypeId = creditLimitType.CreditLimitTypeId,
						DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
						DocumentStatus_StatusId = documentStatus.StatusId,
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
						CustomerTable_CustomerId = customerTable.CustomerId,
						RefCreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
						RefCreditAppTable_CreditAppId = creditAppTable.CreditAppId,
						RefCreditAppTableGUID = creditAppRequestTable.RefCreditAppTableGUID,
					});
		}
		#endregion GetListvw
		#region GetByIdvw

		private IQueryable<CreditAppRequestTableItemViewMap> GetItemQueryByCreditAppRequestLine()
		{
			var result = (from creditAppRequestTable in Entity
						  join creditAppRequestLine in db.Set<CreditAppRequestLine>()
						  on creditAppRequestTable.CreditAppRequestTableGUID equals creditAppRequestLine.CreditAppRequestTableGUID into ljcreditAppRequestLine
						  from creditAppRequestLine in ljcreditAppRequestLine.DefaultIfEmpty()
						  select new CreditAppRequestTableItemViewMap
						  {
							  CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
							  CreditAppRequestLine_CreditAppRequestLineGuid = creditAppRequestLine.CreditAppRequestLineGUID,
							  CreditAppRequestLine_LineNum = creditAppRequestLine.LineNum,
							  RowVersion = creditAppRequestTable.RowVersion
						  }
								);
			return result;


		}
		private IQueryable<CreditAppRequestTableItemViewMap> GetItemQuery()
		{
			var address = db.Set<AddressTrans>().Where(w => w.RefType == (int)RefType.Customer);
			var addressLabel = (from creditAppRequestTable in Entity
								join billingAddress in address on creditAppRequestTable.BillingAddressGUID equals billingAddress.AddressTransGUID into ljCreditAppRequestLineBillingAddress
								from billingAddress in ljCreditAppRequestLineBillingAddress.DefaultIfEmpty()
								join invoiceAddress in address on creditAppRequestTable.InvoiceAddressGUID equals invoiceAddress.AddressTransGUID into ljCreditAppRequestLineInvoiceAddress
								from invoiceAddress in ljCreditAppRequestLineInvoiceAddress.DefaultIfEmpty()
								join mailingReceiptAddress in address on creditAppRequestTable.MailingReceiptAddressGUID equals mailingReceiptAddress.AddressTransGUID into ljCreditAppRequestLineMailingReceiptAddress
								from mailingReceiptAddress in ljCreditAppRequestLineMailingReceiptAddress.DefaultIfEmpty()
								join receiptAddress in address on creditAppRequestTable.ReceiptAddressGUID equals receiptAddress.AddressTransGUID into ljCreditAppRequestLineReceiptAddressGUID
								from receiptAddress in ljCreditAppRequestLineReceiptAddressGUID.DefaultIfEmpty()
								join registeredAddress in address on creditAppRequestTable.RegisteredAddressGUID equals registeredAddress.AddressTransGUID
								join creditAppRequestline in db.Set<CreditAppRequestLine>().GroupBy(g => g.CreditAppRequestTableGUID)
																						.Select(s => new { CreditAppRequestTableGUID = s.Key, _count = s.Count() })
								on creditAppRequestTable.CreditAppRequestTableGUID equals creditAppRequestline.CreditAppRequestTableGUID into ljCreditAppRequestTableCreditAppRequestline
								from creditAppRequestline in ljCreditAppRequestTableCreditAppRequestline.DefaultIfEmpty()
								select new CreditAppRequestTableItemViewMap
								{
									CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
									UnboundBillingAddress = (billingAddress != null) ? SmartAppUtil.GetAddressLabel(billingAddress.Address1, billingAddress.Address2) : null,
									UnboundInvoiceAddress = (invoiceAddress != null) ? SmartAppUtil.GetAddressLabel(invoiceAddress.Address1, invoiceAddress.Address2) : null,
									UnboundMailingReceiptAddress = (mailingReceiptAddress != null) ? SmartAppUtil.GetAddressLabel(mailingReceiptAddress.Address1, mailingReceiptAddress.Address2) : null,
									UnboundReceiptAddress = (receiptAddress != null) ? SmartAppUtil.GetAddressLabel(receiptAddress.Address1, receiptAddress.Address2) : null,
									UnboundRegisteredAddress = SmartAppUtil.GetAddressLabel(registeredAddress.Address1, registeredAddress.Address2),
									NumberOfRegisteredBuyer = (creditAppRequestline != null) ? creditAppRequestline._count : 0,
								});
			var totalInterestPct = (from creditAppRequestTable in Entity
									join interestTypeValue in db.Set<InterestTypeValue>() on creditAppRequestTable.InterestTypeGUID equals interestTypeValue.InterestTypeGUID
									where interestTypeValue.EffectiveFrom <= creditAppRequestTable.RequestDate
									&& (interestTypeValue.EffectiveTo >= creditAppRequestTable.RequestDate || interestTypeValue.EffectiveTo == (DateTime?)null)
									select new CreditAppRequestTableItemViewMap
									{
										CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
										TotalInterestPct = creditAppRequestTable.InterestAdjustment + interestTypeValue.Value,
									});



			return (from creditAppRequestTable in Entity
					join company in db.Set<Company>()
					on creditAppRequestTable.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on creditAppRequestTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCreditAppRequestTableOwnerBU
					from ownerBU in ljCreditAppRequestTableOwnerBU.DefaultIfEmpty()
					join documentStatus in db.Set<DocumentStatus>() on creditAppRequestTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
					join addLabel in addressLabel on creditAppRequestTable.CreditAppRequestTableGUID equals addLabel.CreditAppRequestTableGUID into ljCreditAppRequestTableAddLabel
					from addLabel in ljCreditAppRequestTableAddLabel.DefaultIfEmpty()
					join cusomerTable in db.Set<CustomerTable>() on creditAppRequestTable.CustomerTableGUID equals cusomerTable.CustomerTableGUID
					join _totalInterestPct in totalInterestPct on creditAppRequestTable.CreditAppRequestTableGUID equals _totalInterestPct.CreditAppRequestTableGUID into ljCreditAppRequestTableTotalInterestPct
					from _totalInterestPct in ljCreditAppRequestTableTotalInterestPct.DefaultIfEmpty()
					join lineOfBusiness in db.Set<LineOfBusiness>() on cusomerTable.LineOfBusinessGUID equals lineOfBusiness.LineOfBusinessGUID into ljLineOfBusiness
					from lineOfBusiness in ljLineOfBusiness.DefaultIfEmpty()
					join businessType in db.Set<BusinessType>() on cusomerTable.BusinessTypeGUID equals businessType.BusinessTypeGUID into ljBusinessType
					from businessType in ljBusinessType.DefaultIfEmpty()
					join introducedBy in db.Set<IntroducedBy>() on cusomerTable.IntroducedByGUID equals introducedBy.IntroducedByGUID into ljIntroducedBy
					from introducedBy in ljIntroducedBy.DefaultIfEmpty()
					join creditlimitType in db.Set<CreditLimitType>() on creditAppRequestTable.CreditLimitTypeGUID equals creditlimitType.CreditLimitTypeGUID into ljcreditlimitType
					from creditlimitType in ljcreditlimitType.DefaultIfEmpty()
					join creditAppTable in db.Set<CreditAppTable>() on creditAppRequestTable.RefCreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljCreditAppTable
					from creditAppTable in ljCreditAppTable.DefaultIfEmpty()
					join documentReason in db.Set<DocumentReason>() on creditAppRequestTable.DocumentReasonGUID equals documentReason.DocumentReasonGUID into ljDocumentReason
					from documentReason in ljDocumentReason.DefaultIfEmpty()
					select new CreditAppRequestTableItemViewMap
					{
						CompanyGUID = creditAppRequestTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = creditAppRequestTable.Owner,
						OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = creditAppRequestTable.CreatedBy,
						CreatedDateTime = creditAppRequestTable.CreatedDateTime,
						ModifiedBy = creditAppRequestTable.ModifiedBy,
						ModifiedDateTime = creditAppRequestTable.ModifiedDateTime,
						CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
						ApplicationTableGUID = creditAppRequestTable.ApplicationTableGUID,
						ApprovedCreditLimitRequest = creditAppRequestTable.ApprovedCreditLimitRequest,
						ApproverComment = creditAppRequestTable.ApproverComment,
						BankAccountControlGUID = creditAppRequestTable.BankAccountControlGUID,
						BillingAddressGUID = creditAppRequestTable.BillingAddressGUID,
						BillingContactPersonGUID = creditAppRequestTable.BillingContactPersonGUID,
						BlacklistStatusGUID = creditAppRequestTable.BlacklistStatusGUID,
						CACondition = creditAppRequestTable.CACondition,
						ConsortiumTableGUID = creditAppRequestTable.ConsortiumTableGUID,
						CreditAppRequestId = creditAppRequestTable.CreditAppRequestId,
						CreditAppRequestType = creditAppRequestTable.CreditAppRequestType,
						CreditComment = creditAppRequestTable.CreditComment,
						CreditLimitExpiration = creditAppRequestTable.CreditLimitExpiration,
						CreditLimitRemark = creditAppRequestTable.CreditLimitRemark,
						CreditLimitRequest = creditAppRequestTable.CreditLimitRequest,
						CreditLimitTypeGUID = creditAppRequestTable.CreditLimitTypeGUID,
						CreditRequestFeeAmount = creditAppRequestTable.CreditRequestFeeAmount,
						CreditRequestFeePct = creditAppRequestTable.CreditRequestFeePct,
						CreditScoringGUID = creditAppRequestTable.CreditScoringGUID,
						CreditTermGUID = creditAppRequestTable.CreditTermGUID,
						CustomerCreditLimit = creditAppRequestTable.CustomerCreditLimit,
						CustomerTableGUID = creditAppRequestTable.CustomerTableGUID,
						Description = creditAppRequestTable.Description,
						Dimension1GUID = creditAppRequestTable.Dimension1GUID,
						Dimension2GUID = creditAppRequestTable.Dimension2GUID,
						Dimension3GUID = creditAppRequestTable.Dimension3GUID,
						Dimension4GUID = creditAppRequestTable.Dimension4GUID,
						Dimension5GUID = creditAppRequestTable.Dimension5GUID,
						DocumentReasonGUID = creditAppRequestTable.DocumentReasonGUID,
						DocumentStatusGUID = creditAppRequestTable.DocumentStatusGUID,
						ExpiryDate = creditAppRequestTable.ExpiryDate,
						ExtensionServiceFeeCondTemplateGUID = creditAppRequestTable.ExtensionServiceFeeCondTemplateGUID,
						FinancialCheckedBy = creditAppRequestTable.FinancialCheckedBy,
						FinancialCreditCheckedDate = creditAppRequestTable.FinancialCreditCheckedDate,
						InterestAdjustment = creditAppRequestTable.InterestAdjustment,
						InterestTypeGUID = creditAppRequestTable.InterestTypeGUID,
						InvoiceAddressGUID = creditAppRequestTable.InvoiceAddressGUID,
						KYCGUID = creditAppRequestTable.KYCGUID,
						MailingReceiptAddressGUID = creditAppRequestTable.MailingReceiptAddressGUID,
						MarketingComment = creditAppRequestTable.MarketingComment,
						MaxPurchasePct = creditAppRequestTable.MaxPurchasePct,
						MaxRetentionAmount = creditAppRequestTable.MaxRetentionAmount,
						MaxRetentionPct = creditAppRequestTable.MaxRetentionPct,
						NCBCheckedBy = creditAppRequestTable.NCBCheckedBy,
						NCBCheckedDate = creditAppRequestTable.NCBCheckedDate,
						PDCBankGUID = creditAppRequestTable.PDCBankGUID,
						ProductSubTypeGUID = creditAppRequestTable.ProductSubTypeGUID,
						ProductType = creditAppRequestTable.ProductType,
						PurchaseFeeCalculateBase = creditAppRequestTable.PurchaseFeeCalculateBase,
						PurchaseFeePct = creditAppRequestTable.PurchaseFeePct,
						ReceiptAddressGUID = creditAppRequestTable.ReceiptAddressGUID,
						ReceiptContactPersonGUID = creditAppRequestTable.ReceiptContactPersonGUID,
						RefCreditAppTableGUID = creditAppRequestTable.RefCreditAppTableGUID,
						RegisteredAddressGUID = creditAppRequestTable.RegisteredAddressGUID,
						Remark = creditAppRequestTable.Remark,
						RequestDate = creditAppRequestTable.RequestDate,
						SalesAvgPerMonth = creditAppRequestTable.SalesAvgPerMonth,
						SigningCondition = creditAppRequestTable.SigningCondition,
						StartDate = creditAppRequestTable.StartDate,
						DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
						DocumentStatus_StatusId = documentStatus.StatusId,
						UnboundBillingAddress = (addLabel != null) ? addLabel.UnboundBillingAddress : null,
						UnboundInvoiceAddress = (addLabel != null) ? addLabel.UnboundInvoiceAddress : null,
						UnboundMailingReceiptAddress = (addLabel != null) ? addLabel.UnboundMailingReceiptAddress : null,
						UnboundReceiptAddress = (addLabel != null) ? addLabel.UnboundReceiptAddress : null,
						UnboundRegisteredAddress = (addLabel != null) ? addLabel.UnboundRegisteredAddress : null,
						CustomerTable_IdentificationType = cusomerTable.IdentificationType,
						NumberOfRegisteredBuyer = (addLabel != null) ? addLabel.NumberOfRegisteredBuyer : 0,
						TotalInterestPct = (_totalInterestPct != null) ? _totalInterestPct.TotalInterestPct : 0,
						TaxIdentificationId = (cusomerTable.IdentificationType == (int)IdentificationType.PassportId) ? cusomerTable.PassportID : cusomerTable.TaxID,
						LineOfBusinessId = (lineOfBusiness != null) ? SmartAppUtil.GetDropDownLabel(lineOfBusiness.LineOfBusinessId, lineOfBusiness.Description) : string.Empty,
						BusinessTypeId = (businessType != null) ? SmartAppUtil.GetDropDownLabel(businessType.BusinessTypeId, businessType.Description) : string.Empty,
						IntroducedById = (introducedBy != null) ? SmartAppUtil.GetDropDownLabel(introducedBy.IntroducedById, introducedBy.Description) : string.Empty,
						DateOfEstablish = (cusomerTable.RecordType == (int)RecordType.Person) ? cusomerTable.DateOfBirth : cusomerTable.DateOfEstablish,
						CustomerAllBuyerOutstanding = creditAppRequestTable.CustomerAllBuyerOutstanding,
						ProcessInstanceId = creditAppRequestTable.ProcessInstanceId,
						ApprovedDate = creditAppRequestTable.ApprovedDate,
						CreditlimitType_ParentCreditLimitTypeGUID = creditlimitType.ParentCreditLimitTypeGUID,
						CreditLimitType_CreditLimitTypeId = creditlimitType.CreditLimitTypeId,
						CreditLimitType_Values = SmartAppUtil.GetDropDownLabel(creditlimitType.CreditLimitTypeId, creditlimitType.Description),

						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(cusomerTable.CustomerId, cusomerTable.Name),
						RefCreditAppTable_Values = (creditAppTable != null) ? SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description) : string.Empty,
						DocumentReason_Values = (documentReason != null) ? SmartAppUtil.GetDropDownLabel(documentReason.ReasonId, documentReason.Description) : null,

						AssignmentMethodGUID = creditAppRequestTable.AssignmentMethodGUID,
						PurchaseWithdrawalCondition = creditAppRequestTable.PurchaseWithdrawalCondition,
						RowVersion = creditAppRequestTable.RowVersion
					});
		}
		public CreditAppRequestTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.ToMap<CreditAppRequestTableItemViewMap, CreditAppRequestTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}

		public CreditAppRequestTableItemView GetByIdvwByCreditAppRequestLine(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQueryByCreditAppRequestLine()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.ToMap<CreditAppRequestTableItemViewMap, CreditAppRequestTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public CreditAppRequestTable CreateCreditAppRequestTable(CreditAppRequestTable creditAppRequestTable)
		{
			try
			{
				creditAppRequestTable.CreditAppRequestTableGUID = Guid.NewGuid();
				base.Add(creditAppRequestTable);
				return creditAppRequestTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCreditAppRequestTableVoid(CreditAppRequestTable creditAppRequestTable)
		{
			try
			{
				CreateCreditAppRequestTable(creditAppRequestTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditAppRequestTable UpdateCreditAppRequestTable(CreditAppRequestTable dbCreditAppRequestTable, CreditAppRequestTable inputCreditAppRequestTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCreditAppRequestTable = dbCreditAppRequestTable.MapUpdateValues<CreditAppRequestTable>(inputCreditAppRequestTable);
				base.Update(dbCreditAppRequestTable);
				return dbCreditAppRequestTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCreditAppRequestTableVoid(CreditAppRequestTable dbCreditAppRequestTable, CreditAppRequestTable inputCreditAppRequestTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCreditAppRequestTable = dbCreditAppRequestTable.MapUpdateValues<CreditAppRequestTable>(inputCreditAppRequestTable, skipUpdateFields);
				base.Update(dbCreditAppRequestTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete

		public CreditAppRequestTable GetCreditAppRequestTableByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CreditAppRequestTableGUID == guid)
									.FilterByAccessLevel(SysParm.AccessLevel)
									.AsNoTracking()
									.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region function
		public CancelCreditAppRequestResultView GetCancelCreditApplicationRequestByAppReqGUID(Guid guid)
		{
			try
			{
				var result = (from creditAppReqTable in Entity
							  join customer in db.Set<CustomerTable>()
							  on creditAppReqTable.CustomerTableGUID equals customer.CustomerTableGUID into ljCreditAppReqTableCustomer
							  from customer in ljCreditAppReqTableCustomer.DefaultIfEmpty()
							  join documentStatus in db.Set<DocumentStatus>() on creditAppReqTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID

							  where creditAppReqTable.CreditAppRequestTableGUID == guid
							  select new CancelCreditAppRequestResultView
							  {
								  CreditAppRequestGUID = creditAppReqTable.CreditAppRequestTableGUID.ToString(),
								  CreditAppRequestId = creditAppReqTable.CreditAppRequestId,
								  Description = creditAppReqTable.Description,
								  CustomerId = customer.CustomerId,
								  CustomerName = customer.Name,
								  ReasonRemark = creditAppReqTable.Remark,
								  DocumentReasonGUID = creditAppReqTable.DocumentReasonGUID.ToString(),
								  DocumentStatus_Description = documentStatus.Description,
							  }).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region Amend CA
		public IQueryable<CreditAppRequestTable> GetExistByCAReqAmend(CreditAppRequestTableAmend item, CreditAppRequestTable caRequest)
		{
			try
			{
				var existCreditAppRequest = (from creditAppRequestTable in db.Set<CreditAppRequestTable>()
											 join documentStatus in db.Set<DocumentStatus>() on creditAppRequestTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
											 where creditAppRequestTable.RefCreditAppTableGUID == caRequest.RefCreditAppTableGUID
												   && creditAppRequestTable.CreditAppRequestType == caRequest.CreditAppRequestType
												   && Convert.ToInt32(documentStatus.StatusId) < (int)CreditAppRequestStatus.Approved
												   && creditAppRequestTable.CreditAppRequestTableGUID != caRequest.CreditAppRequestTableGUID
												   && creditAppRequestTable.CompanyGUID == caRequest.CompanyGUID
											 select creditAppRequestTable);
				return existCreditAppRequest;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditAppRequestTable CopyByCreditAppRequestTableAmend(Guid creditAppTableGUID)
		{
			try
			{
				CreditAppRequestTable creditAppRequestTable = (from creditAppTable in db.Set<CreditAppTable>()
															   join CARequest in db.Set<CreditAppRequestTable>() on creditAppTable.RefCreditAppRequestTableGUID equals CARequest.CreditAppRequestTableGUID
															   join customerTable in db.Set<CustomerTable>() on creditAppTable.CustomerTableGUID equals customerTable.CustomerTableGUID
															   where creditAppTable.CreditAppTableGUID == creditAppTableGUID
															   select new CreditAppRequestTable
															   {
																   CreditRequestFeePct = CARequest.CreditRequestFeePct,
																   CreditRequestFeeAmount = CARequest.CreditRequestFeeAmount,

																   BlacklistStatusGUID = customerTable.BlacklistStatusGUID,
																   KYCGUID = customerTable.KYCSetupGUID,
																   CreditScoringGUID = customerTable.CreditScoringGUID,

																   ProductType = creditAppTable.ProductType,
																   ProductSubTypeGUID = creditAppTable.ProductSubTypeGUID,
																   CreditLimitTypeGUID = creditAppTable.CreditLimitTypeGUID,
																   RefCreditAppTableGUID = creditAppTable.CreditAppTableGUID,
																   CustomerTableGUID = creditAppTable.CustomerTableGUID,
																   ConsortiumTableGUID = creditAppTable.ConsortiumTableGUID,
																   RegisteredAddressGUID = creditAppTable.RegisteredAddressGUID.Value,
																   SalesAvgPerMonth = creditAppTable.SalesAvgPerMonth,
																   BankAccountControlGUID = creditAppTable.BankAccountControlGUID,
																   PDCBankGUID = creditAppTable.PDCBankGUID,
																   CreditLimitExpiration = creditAppTable.CreditLimitExpiration,
																   StartDate = creditAppTable.StartDate,
																   ExpiryDate = creditAppTable.ExpiryDate,
																   CreditLimitRemark = creditAppTable.CreditLimitRemark,
																   CACondition = creditAppTable.CACondition,
																   BillingContactPersonGUID = creditAppTable.BillingContactPersonGUID,
																   BillingAddressGUID = creditAppTable.BillingAddressGUID,
																   ReceiptContactPersonGUID = creditAppTable.ReceiptContactPersonGUID,
																   ReceiptAddressGUID = creditAppTable.ReceiptAddressGUID,
																   InvoiceAddressGUID = creditAppTable.InvoiceAddressGUID,
																   MailingReceiptAddressGUID = creditAppTable.MailingReceipAddressGUID,
																   CreditLimitRequest = creditAppTable.ApprovedCreditLimit,
																   InterestTypeGUID = creditAppTable.InterestTypeGUID,
																   InterestAdjustment = creditAppTable.InterestAdjustment,
																   MaxRetentionPct = creditAppTable.MaxRetentionPct,
																   MaxRetentionAmount = creditAppTable.MaxRetentionAmount,
																   PurchaseFeePct = creditAppTable.PurchaseFeePct,
																   PurchaseFeeCalculateBase = creditAppTable.PurchaseFeeCalculateBase,
																   CreditTermGUID = creditAppTable.CreditTermGUID,
																   ExtensionServiceFeeCondTemplateGUID = creditAppTable.ExtensionServiceFeeCondTemplateGUID,
																   ApplicationTableGUID = creditAppTable.ApplicationTableGUID,
																   Dimension1GUID = creditAppTable.Dimension1GUID,
																   Dimension2GUID = creditAppTable.Dimension2GUID,
																   Dimension3GUID = creditAppTable.Dimension3GUID,
																   Dimension4GUID = creditAppTable.Dimension4GUID,
																   Dimension5GUID = creditAppTable.Dimension5GUID,
																   CompanyGUID = creditAppTable.CompanyGUID,
															   }).FirstOrDefault();
				return creditAppRequestTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditAppRequestTable GetCreditAppRequestTableByAmendCANoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = (from creditAppTableRequest in Entity
							  join creditAppRequestTableAmend in db.Set<CreditAppRequestTableAmend>() on creditAppTableRequest.CreditAppRequestTableGUID equals creditAppRequestTableAmend.CreditAppRequestTableGUID
							  where creditAppRequestTableAmend.CreditAppRequestTableAmendGUID == guid
							  select creditAppTableRequest)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion  Amend CA
		#region Amend CA Line
		public CreditAppRequestTable CopyCreditAppRequestForCreateCreditAppRequestLineAmend(CreditAppRequestLineAmendItemView creditAppRequestLineAmendView)
		{
			try
			{
				Guid refCreditAppTableGUID = creditAppRequestLineAmendView.RefCreditAppTableGUID.StringToGuid();
				var result =
						(from creditAppTable in db.Set<CreditAppTable>()
						 join customerTable in db.Set<CustomerTable>() on creditAppTable.CustomerTableGUID equals customerTable.CustomerTableGUID
						 join creditAppRequestTable in db.Set<CreditAppRequestTable>() on creditAppTable.RefCreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID
						 where creditAppTable.CreditAppTableGUID == refCreditAppTableGUID
						 select new CreditAppRequestTable
						 {
							 CreditAppRequestTableGUID = Guid.NewGuid(),
							 DocumentStatusGUID = creditAppRequestLineAmendView.CreditAppRequestTable_DocumentStatusGUID.StringToGuid(),
							 Description = creditAppRequestLineAmendView.CreditAppRequestTable_Description,
							 CreditAppRequestType = creditAppRequestLineAmendView.CreditAppRequestTable_CreditAppRequestType,
							 RequestDate = creditAppRequestLineAmendView.CreditAppRequestTable_RequestDate.StringToDate(),
							 Remark = creditAppRequestLineAmendView.CreditAppRequestTable_Remark,
							 CustomerCreditLimit = creditAppRequestLineAmendView.CreditAppRequestTable_CustomerCreditLimit,
							 ProductType = creditAppTable.ProductType,
							 ProductSubTypeGUID = creditAppTable.ProductSubTypeGUID,
							 CreditLimitTypeGUID = creditAppTable.CreditLimitTypeGUID,
							 RefCreditAppTableGUID = creditAppTable.CreditAppTableGUID,
							 CustomerTableGUID = creditAppTable.CustomerTableGUID,
							 ConsortiumTableGUID = creditAppTable.ConsortiumTableGUID,
							 SalesAvgPerMonth = creditAppTable.SalesAvgPerMonth,
							 BankAccountControlGUID = creditAppTable.BankAccountControlGUID,
							 PDCBankGUID = creditAppTable.PDCBankGUID,
							 CreditLimitExpiration = creditAppTable.CreditLimitExpiration,
							 StartDate = creditAppTable.StartDate,
							 ExpiryDate = creditAppTable.ExpiryDate,
							 CreditLimitRemark = creditAppTable.CreditLimitRemark,
							 BillingContactPersonGUID = creditAppTable.BillingContactPersonGUID,
							 BillingAddressGUID = creditAppTable.BillingAddressGUID,
							 ReceiptContactPersonGUID = creditAppTable.ReceiptContactPersonGUID,
							 ReceiptAddressGUID = creditAppTable.ReceiptAddressGUID,
							 InvoiceAddressGUID = creditAppTable.InvoiceAddressGUID,
							 MailingReceiptAddressGUID = creditAppTable.MailingReceipAddressGUID,
							 CreditLimitRequest = creditAppTable.ApprovedCreditLimit,
							 InterestTypeGUID = creditAppTable.InterestTypeGUID,
							 InterestAdjustment = creditAppTable.InterestAdjustment,
							 MaxPurchasePct = creditAppTable.MaxPurchasePct,
							 MaxRetentionAmount = creditAppTable.MaxRetentionAmount,
							 PurchaseFeePct = creditAppTable.PurchaseFeePct,
							 PurchaseFeeCalculateBase = creditAppTable.PurchaseFeeCalculateBase,
							 CreditTermGUID = creditAppTable.CreditTermGUID,
							 ExtensionServiceFeeCondTemplateGUID = creditAppTable.ExtensionServiceFeeCondTemplateGUID,
							 ApplicationTableGUID = creditAppTable.ApplicationTableGUID,
							 Dimension1GUID = creditAppTable.Dimension1GUID,
							 Dimension2GUID = creditAppTable.Dimension2GUID,
							 Dimension3GUID = creditAppTable.Dimension3GUID,
							 Dimension4GUID = creditAppTable.Dimension4GUID,
							 Dimension5GUID = creditAppTable.Dimension5GUID,
							 CreditRequestFeePct = creditAppRequestTable.CreditRequestFeePct,
							 CreditRequestFeeAmount = creditAppRequestTable.CreditRequestFeeAmount,
							 BlacklistStatusGUID = customerTable.BlacklistStatusGUID,
							 KYCGUID = customerTable.KYCSetupGUID,
							 CreditScoringGUID = customerTable.CreditScoringGUID,
							 RegisteredAddressGUID = creditAppTable.RegisteredAddressGUID.Value,
							 CompanyGUID = creditAppTable.CompanyGUID,
						 }).FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditAppRequestTable GetCreditAppRequestTableByRefCreditAppLineAndCreditAppRequestType(Guid refCreditAppLineGUID, int creditAppRequestType)
		{
			try
			{
				return (from creditAppRequestTable in Entity
						join creditAppRequestLine in db.Set<CreditAppRequestLine>()
						on creditAppRequestTable.CreditAppRequestTableGUID equals creditAppRequestLine.CreditAppRequestTableGUID
						join documentStatus in db.Set<DocumentStatus>() on creditAppRequestTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
						where creditAppRequestLine.RefCreditAppLineGUID == refCreditAppLineGUID &&
						creditAppRequestTable.CreditAppRequestType == creditAppRequestType &&
						Convert.ToInt32(documentStatus.StatusId) < (int)CreditAppRequestStatus.Approved
						select creditAppRequestTable).AsNoTracking().FirstOrDefault();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Amend CA Line
		public CreditAppRequestTable GetCreditAppRequestTableByCreditAppRequestLine(Guid guid)
		{
			try
			{
				var result = (from creditAppRequestTable in Entity
							  join creditAppRequestLine in db.Set<CreditAppRequestLine>()
							  on creditAppRequestTable.CreditAppRequestTableGUID equals creditAppRequestLine.CreditAppRequestTableGUID
							  where creditAppRequestLine.CreditAppRequestLineGUID == guid
							  select creditAppRequestTable)
							  .AsNoTracking()
							  .FilterByAccessLevel(SysParm.AccessLevel)
							  .FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Loan Request
		public CreditAppRequestTableItemView GetLoanRequestOrBuyerMatchingCreditAppRequestTableInitialData(Guid creditApId, int creditAppRequestType)
		{
			try
			{
				var draft = ((int)CreditAppRequestStatus.Draft).ToString();
				var address = db.Set<AddressTrans>();
				string creditAppRequestTableGUID = new Guid().GuidNullToString();
				var addressLabel = (from creditAppTable in db.Set<CreditAppTable>()
									join billingAddress in address on creditAppTable.BillingAddressGUID equals billingAddress.AddressTransGUID into ljCreditAppRequestLineBillingAddress
									from billingAddress in ljCreditAppRequestLineBillingAddress.DefaultIfEmpty()
									join invoiceAddress in address on creditAppTable.InvoiceAddressGUID equals invoiceAddress.AddressTransGUID into ljCreditAppRequestLineInvoiceAddress
									from invoiceAddress in ljCreditAppRequestLineInvoiceAddress.DefaultIfEmpty()
									join mailingReceiptAddress in address on creditAppTable.MailingReceipAddressGUID equals mailingReceiptAddress.AddressTransGUID into ljCreditAppRequestLineMailingReceiptAddress
									from mailingReceiptAddress in ljCreditAppRequestLineMailingReceiptAddress.DefaultIfEmpty()
									join receiptAddress in address on creditAppTable.ReceiptAddressGUID equals receiptAddress.AddressTransGUID into ljCreditAppRequestLineReceiptAddressGUID
									from receiptAddress in ljCreditAppRequestLineReceiptAddressGUID.DefaultIfEmpty()
									join registeredAddress in address on creditAppTable.RegisteredAddressGUID equals registeredAddress.AddressTransGUID
									where creditAppTable.CreditAppTableGUID == creditApId
									select new CreditAppRequestTableItemView
									{
										CreditAppRequestTableGUID = creditAppRequestTableGUID,
										UnboundBillingAddress = (billingAddress != null) ? SmartAppUtil.GetAddressLabel(billingAddress.Address1, billingAddress.Address2) : null,
										UnboundInvoiceAddress = (invoiceAddress != null) ? SmartAppUtil.GetAddressLabel(invoiceAddress.Address1, invoiceAddress.Address2) : null,
										UnboundMailingReceiptAddress = (mailingReceiptAddress != null) ? SmartAppUtil.GetAddressLabel(mailingReceiptAddress.Address1, mailingReceiptAddress.Address2) : null,
										UnboundReceiptAddress = (receiptAddress != null) ? SmartAppUtil.GetAddressLabel(receiptAddress.Address1, receiptAddress.Address2) : null,
										UnboundRegisteredAddress = SmartAppUtil.GetAddressLabel(registeredAddress.Address1, registeredAddress.Address2),
									});

				var result = (from creditAppTable in db.Set<CreditAppTable>()
							  join creditAppRequestTable in db.Set<CreditAppRequestTable>()
							  on creditAppTable.RefCreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID
							  join documentStatus in db.Set<DocumentStatus>() on draft equals documentStatus.StatusId
							  join addLabel in addressLabel on creditAppRequestTableGUID equals addLabel.CreditAppRequestTableGUID into ljCreditAppRequestTableAddLabel
							  from addLabel in ljCreditAppRequestTableAddLabel.DefaultIfEmpty()
							  join customerTable in db.Set<CustomerTable>() on creditAppTable.CustomerTableGUID equals customerTable.CustomerTableGUID
							  join lineOfBusiness in db.Set<LineOfBusiness>() on customerTable.LineOfBusinessGUID equals lineOfBusiness.LineOfBusinessGUID into ljLineOfBusiness
							  from lineOfBusiness in ljLineOfBusiness.DefaultIfEmpty()
							  join businessType in db.Set<BusinessType>() on customerTable.BusinessTypeGUID equals businessType.BusinessTypeGUID into ljBusinessType
							  from businessType in ljBusinessType.DefaultIfEmpty()
							  join introducedBy in db.Set<IntroducedBy>() on customerTable.IntroducedByGUID equals introducedBy.IntroducedByGUID into ljIntroducedBy
							  from introducedBy in ljIntroducedBy.DefaultIfEmpty()
							  where creditAppTable.CreditAppTableGUID == creditApId
							  select new CreditAppRequestTableItemView
							  {
								  CreditAppRequestTableGUID = creditAppRequestTableGUID,
								  CreditAppRequestType = creditAppRequestType,
								  RequestDate = DateTime.Now.DateToString(),
								  DocumentStatusGUID = documentStatus.DocumentStatusGUID.GuidNullToString(),
								  DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
								  DocumentStatus_StatusId = documentStatus.StatusId,
								  RefCreditAppTableGUID = creditAppTable.CreditAppTableGUID.GuidNullToString(),
								  ProductType = creditAppTable.ProductType,
								  ProductSubTypeGUID = creditAppTable.ProductSubTypeGUID.GuidNullToString(),
								  CreditLimitTypeGUID = creditAppTable.CreditLimitTypeGUID.GuidNullToString(),
								  CustomerTableGUID = creditAppTable.CustomerTableGUID.GuidNullToString(),
								  ConsortiumTableGUID = creditAppTable.ConsortiumTableGUID.GuidNullToString(),
								  RegisteredAddressGUID = creditAppTable.RegisteredAddressGUID.GuidNullToString(),
								  SalesAvgPerMonth = creditAppTable.SalesAvgPerMonth,
								  BankAccountControlGUID = creditAppTable.BankAccountControlGUID.GuidNullToString(),
								  PDCBankGUID = creditAppTable.PDCBankGUID.GuidNullToString(),
								  CreditLimitRequest = creditAppTable.ApprovedCreditLimit,
								  CreditLimitExpiration = creditAppTable.CreditLimitExpiration,
								  StartDate = creditAppTable.StartDate.DateToString(),
								  ExpiryDate = creditAppTable.ExpiryDate.DateToString(),
								  CreditLimitRemark = creditAppTable.CreditLimitRemark,
								  CreditRequestFeePct = creditAppTable.CreditRequestFeePct,
								  CreditRequestFeeAmount = 0,
								  InterestTypeGUID = creditAppTable.InterestTypeGUID.GuidNullToString(),
								  InterestAdjustment = creditAppTable.InterestAdjustment,
								  CACondition = creditAppTable.CACondition,
								  BillingContactPersonGUID = creditAppTable.BillingContactPersonGUID.GuidNullToString(),
								  BillingAddressGUID = creditAppTable.BillingAddressGUID.GuidNullToString(),
								  ReceiptContactPersonGUID = creditAppTable.ReceiptContactPersonGUID.GuidNullToString(),
								  ReceiptAddressGUID = creditAppTable.ReceiptAddressGUID.GuidNullToString(),
								  InvoiceAddressGUID = creditAppTable.InvoiceAddressGUID.GuidNullToString(),
								  MailingReceiptAddressGUID = creditAppTable.MailingReceipAddressGUID.GuidNullToString(),
								  MaxRetentionPct = creditAppTable.MaxRetentionPct,
								  MaxRetentionAmount = creditAppTable.MaxRetentionAmount,
								  MaxPurchasePct = creditAppTable.MaxPurchasePct,
								  PurchaseFeePct = creditAppTable.PurchaseFeePct,
								  PurchaseFeeCalculateBase = creditAppTable.PurchaseFeeCalculateBase,
								  CreditTermGUID = creditAppTable.CreditTermGUID.GuidNullToString(),
								  ExtensionServiceFeeCondTemplateGUID = creditAppTable.ExtensionServiceFeeCondTemplateGUID.GuidNullToString(),
								  ApprovedCreditLimitRequest = creditAppTable.ApprovedCreditLimit,
								  Dimension1GUID = creditAppTable.Dimension1GUID.GuidNullToString(),
								  Dimension2GUID = creditAppTable.Dimension2GUID.GuidNullToString(),
								  Dimension3GUID = creditAppTable.Dimension3GUID.GuidNullToString(),
								  Dimension4GUID = creditAppTable.Dimension4GUID.GuidNullToString(),
								  Dimension5GUID = creditAppTable.Dimension5GUID.GuidNullToString(),

								  NCBCheckedDate = creditAppRequestTable.NCBCheckedDate.DateNullToString(),
								  NCBCheckedBy = creditAppRequestTable.NCBCheckedBy,
								  BlacklistStatusGUID = creditAppRequestTable.BlacklistStatusGUID.GuidNullToString(),
								  KYCGUID = creditAppRequestTable.KYCGUID.GuidNullToString(),
								  CreditScoringGUID = creditAppRequestTable.CreditScoringGUID.GuidNullToString(),
								  CustomerCreditLimit = creditAppRequestTable.CustomerCreditLimit,
								  CustomerAllBuyerOutstanding = creditAppRequestTable.CustomerAllBuyerOutstanding,
								  FinancialCheckedBy = creditAppRequestTable.FinancialCheckedBy,
								  FinancialCreditCheckedDate = creditAppRequestTable.FinancialCreditCheckedDate.DateNullToString(),
								  SigningCondition = creditAppRequestTable.SigningCondition,
								  TaxIdentificationId = (customerTable.IdentificationType == (int)IdentificationType.TaxId) ? customerTable.TaxID : customerTable.PassportID,
								  DateOfEstablish = (customerTable.RecordType == (int)RecordType.Person) ? customerTable.DateOfBirth.DateNullToString() : customerTable.DateOfEstablish.DateNullToString(),
								  LineOfBusinessId = (lineOfBusiness != null) ? SmartAppUtil.GetDropDownLabel(lineOfBusiness.LineOfBusinessId, lineOfBusiness.Description) : string.Empty,
								  BusinessTypeId = (businessType != null) ? SmartAppUtil.GetDropDownLabel(businessType.BusinessTypeId, businessType.Description) : string.Empty,
								  IntroducedById = (introducedBy != null) ? SmartAppUtil.GetDropDownLabel(introducedBy.IntroducedById, introducedBy.Description) : string.Empty,

								  UnboundBillingAddress = (addLabel != null) ? addLabel.UnboundBillingAddress : null,
								  UnboundInvoiceAddress = (addLabel != null) ? addLabel.UnboundInvoiceAddress : null,
								  UnboundMailingReceiptAddress = (addLabel != null) ? addLabel.UnboundMailingReceiptAddress : null,
								  UnboundReceiptAddress = (addLabel != null) ? addLabel.UnboundReceiptAddress : null,
								  UnboundRegisteredAddress = (addLabel != null) ? addLabel.UnboundRegisteredAddress : null,

								  AssignmentMethodGUID = creditAppTable.AssignmentMethodGUID.GuidNullToString(),
								  PurchaseWithdrawalCondition = creditAppTable.PurchaseWithdrawalCondition,

							  }).FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Loan Request
		#region WorkFlow
		private IQueryable<CreditAppRequestTableItemViewMap> GetItemQuerywf()
		{
			return (from creditAppRequestTable in Entity
					join company in db.Set<Company>()
					on creditAppRequestTable.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on creditAppRequestTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCreditAppRequestTableOwnerBU
					from ownerBU in ljCreditAppRequestTableOwnerBU.DefaultIfEmpty()
					join documentStatus in db.Set<DocumentStatus>() on creditAppRequestTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
					join customerTable in db.Set<CustomerTable>() on creditAppRequestTable.CustomerTableGUID equals customerTable.CustomerTableGUID
					join employeeTable in db.Set<EmployeeTable>() on customerTable.ResponsibleByGUID equals employeeTable.EmployeeTableGUID into ljEmployeeTable
					from employeeTable in ljEmployeeTable.DefaultIfEmpty()
					join contactTrans in db.Set<ContactTrans>().Where(w => w.RefType == (int)RefType.Customer
																		&& w.PrimaryContact == true
																		&& w.ContactType == (int)ContactType.Email)
					on creditAppRequestTable.CustomerTableGUID equals contactTrans.RefGUID into ljContactTrans
					from contactTrans in ljContactTrans.DefaultIfEmpty()
					select new CreditAppRequestTableItemViewMap
					{
						CompanyGUID = creditAppRequestTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = creditAppRequestTable.Owner,
						OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = creditAppRequestTable.CreatedBy,
						CreatedDateTime = creditAppRequestTable.CreatedDateTime,
						ModifiedBy = creditAppRequestTable.ModifiedBy,
						ModifiedDateTime = creditAppRequestTable.ModifiedDateTime,
						CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
						CreditAppRequestId = creditAppRequestTable.CreditAppRequestId,
						Description = creditAppRequestTable.Description,
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
						DocumentStatus_StatusId = documentStatus.StatusId,
						CreditAppRequestType = creditAppRequestTable.CreditAppRequestType,
						ProductType = creditAppRequestTable.ProductType,
						RefCreditAppTableGUID = creditAppRequestTable.RefCreditAppTableGUID,
						ProcessInstanceId = creditAppRequestTable.ProcessInstanceId,
						// for ParmFolio
						UnboundInvoiceAddress = "Credit application request : [" + creditAppRequestTable.CreditAppRequestId + "] : "
												+ customerTable.Name + " : "
												// GetTranslatedMessage for find and repalce
												+ "GetTranslatedMessage" + " : "
												+ ((employeeTable != null) ? employeeTable.Name : string.Empty),
						CustomerEmail = (contactTrans != null) ? contactTrans.ContactValue : string.Empty,
						RowVersion = creditAppRequestTable.RowVersion,
					});
		}
		public CreditAppRequestTableItemView GetByIdwf(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuerywf()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.ToMap<CreditAppRequestTableItemViewMap, CreditAppRequestTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion WorkFlow
		#region Review CreditAppRequest
		private IQueryable<CreditAppRequestTableItemViewMap> GetReviewItemQuery()
		{
			return (from creditAppRequestTable in db.Set<CreditAppRequestTable>()
					join company in db.Set<Company>()
					on creditAppRequestTable.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on creditAppRequestTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCreditAppRequestTableOwnerBU
					from ownerBU in ljCreditAppRequestTableOwnerBU.DefaultIfEmpty()
					join creditAppRequestLine in db.Set<CreditAppRequestLine>()
					on creditAppRequestTable.CreditAppRequestTableGUID equals creditAppRequestLine.CreditAppRequestTableGUID
					join creditAppLine in db.Set<CreditAppLine>() on creditAppRequestLine.RefCreditAppLineGUID equals creditAppLine.CreditAppLineGUID
					join creditAppTable in db.Set<CreditAppTable>() on creditAppLine.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID
					join customerTable in db.Set<CustomerTable>() on creditAppTable.CustomerTableGUID equals customerTable.CustomerTableGUID
					join buyerTable in db.Set<BuyerTable>() on creditAppLine.BuyerTableGUID equals buyerTable.BuyerTableGUID
					join documentStatus in db.Set<DocumentStatus>() on creditAppRequestTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
					join documentReason in db.Set<DocumentReason>()
					on creditAppRequestTable.DocumentReasonGUID equals documentReason.DocumentReasonGUID into ljDocumentReason
					from documentReason in ljDocumentReason.DefaultIfEmpty()
					select new CreditAppRequestTableItemViewMap
					{
						CompanyGUID = creditAppRequestTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = creditAppRequestTable.Owner,
						OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = creditAppRequestTable.CreatedBy,
						CreatedDateTime = creditAppRequestTable.CreatedDateTime,
						ModifiedBy = creditAppRequestTable.ModifiedBy,
						ModifiedDateTime = creditAppRequestTable.ModifiedDateTime,
						CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
						CreditAppRequestId = creditAppRequestTable.CreditAppRequestId,
						CreditAppRequestType = creditAppRequestTable.CreditAppRequestType,
						RequestDate = creditAppRequestTable.RequestDate,
						DocumentStatusGUID = creditAppRequestTable.DocumentStatusGUID,
						DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
						DocumentStatus_StatusId = documentStatus.StatusId,
						ProductType = creditAppRequestTable.ProductType,
						CustomerTableGUID = creditAppTable.CustomerTableGUID,
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
						BuyerTableGUID = creditAppRequestLine.BuyerTableGUID,
						BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
						RefCreditAppTableGUID = creditAppTable.CreditAppTableGUID,
						RefCreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
						RefCreditAppLineGUID = creditAppRequestLine.RefCreditAppLineGUID,
						RefCreditAppLine_Values = SmartAppUtil.GetDropDownLabel(creditAppLine.LineNum.ToString(), creditAppLine.ApprovedCreditLimitLine.ToString()),
						DocumentReason_Values = (documentReason != null) ? SmartAppUtil.GetDropDownLabel(documentReason.ReasonId, documentReason.Description) : string.Empty,
						ApprovedDate = creditAppRequestTable.ApprovedDate,
						Remark = creditAppRequestTable.Remark,
						Description = creditAppRequestTable.Description,
						CreditLimitTypeGUID = creditAppRequestTable.CreditLimitTypeGUID,

						BuyerCreditLimit = creditAppRequestLine.BuyerCreditLimit,
						CreditLimitLineRequest = creditAppRequestLine.CreditLimitLineRequest,
						CustomerBuyerOutstanding = creditAppRequestLine.CustomerBuyerOutstanding,
						CustomerAllBuyerOutstanding = creditAppRequestLine.AllCustomerBuyerOutstanding,

						MarketingComment = creditAppRequestLine.MarketingComment,
						ApproverComment = creditAppRequestLine.ApproverComment,
						CreditComment = creditAppRequestLine.CreditComment,
						CreditAppRequestLine_CreditAppRequestLineGuid = creditAppRequestLine.CreditAppRequestLineGUID,

						ApplicationTableGUID = creditAppRequestTable.ApplicationTableGUID,
						ApprovedCreditLimitRequest = creditAppRequestTable.ApprovedCreditLimitRequest,
						BankAccountControlGUID = creditAppRequestTable.BankAccountControlGUID,
						BillingAddressGUID = creditAppRequestTable.BillingAddressGUID,
						BillingContactPersonGUID = creditAppRequestTable.BillingContactPersonGUID,
						BlacklistStatusGUID = creditAppRequestTable.BlacklistStatusGUID,
						CACondition = creditAppRequestTable.CACondition,
						ConsortiumTableGUID = creditAppRequestTable.ConsortiumTableGUID,

						CreditLimitExpiration = creditAppRequestTable.CreditLimitExpiration,
						CreditLimitRemark = creditAppRequestTable.CreditLimitRemark,
						CreditLimitRequest = creditAppRequestTable.CreditLimitRequest,
						CreditRequestFeeAmount = creditAppRequestTable.CreditRequestFeeAmount,
						CreditRequestFeePct = creditAppRequestTable.CreditRequestFeePct,
						CreditScoringGUID = creditAppRequestTable.CreditScoringGUID,
						CreditTermGUID = creditAppRequestTable.CreditTermGUID,
						CustomerCreditLimit = creditAppRequestTable.CustomerCreditLimit,
						Dimension1GUID = creditAppRequestTable.Dimension1GUID,
						Dimension2GUID = creditAppRequestTable.Dimension2GUID,
						Dimension3GUID = creditAppRequestTable.Dimension3GUID,
						Dimension4GUID = creditAppRequestTable.Dimension4GUID,
						Dimension5GUID = creditAppRequestTable.Dimension5GUID,
						DocumentReasonGUID = creditAppRequestTable.DocumentReasonGUID,
						ExpiryDate = creditAppRequestTable.ExpiryDate,
						ExtensionServiceFeeCondTemplateGUID = creditAppRequestTable.ExtensionServiceFeeCondTemplateGUID,
						FinancialCheckedBy = creditAppRequestTable.FinancialCheckedBy,
						FinancialCreditCheckedDate = creditAppRequestTable.FinancialCreditCheckedDate,
						InterestAdjustment = creditAppRequestTable.InterestAdjustment,
						InterestTypeGUID = creditAppRequestTable.InterestTypeGUID,
						InvoiceAddressGUID = creditAppRequestTable.InvoiceAddressGUID,
						KYCGUID = creditAppRequestTable.KYCGUID,
						MailingReceiptAddressGUID = creditAppRequestTable.MailingReceiptAddressGUID,
						MaxPurchasePct = creditAppRequestTable.MaxPurchasePct,
						MaxRetentionAmount = creditAppRequestTable.MaxRetentionAmount,
						MaxRetentionPct = creditAppRequestTable.MaxRetentionPct,
						NCBCheckedBy = creditAppRequestTable.NCBCheckedBy,
						NCBCheckedDate = creditAppRequestTable.NCBCheckedDate,
						PDCBankGUID = creditAppRequestTable.PDCBankGUID,
						ProductSubTypeGUID = creditAppRequestTable.ProductSubTypeGUID,
						PurchaseFeeCalculateBase = creditAppRequestTable.PurchaseFeeCalculateBase,
						PurchaseFeePct = creditAppRequestTable.PurchaseFeePct,
						ReceiptAddressGUID = creditAppRequestTable.ReceiptAddressGUID,
						ReceiptContactPersonGUID = creditAppRequestTable.ReceiptContactPersonGUID,
						RegisteredAddressGUID = creditAppRequestTable.RegisteredAddressGUID,
						SalesAvgPerMonth = creditAppRequestTable.SalesAvgPerMonth,
						SigningCondition = creditAppRequestTable.SigningCondition,
						StartDate = creditAppRequestTable.StartDate,
						ProcessInstanceId = creditAppRequestTable.ProcessInstanceId,
						RowVersion = creditAppRequestTable.RowVersion
					});
		}
		public CreditAppRequestTableItemView GetReviewByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetReviewItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.ToMap<CreditAppRequestTableItemViewMap, CreditAppRequestTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public CreditAppRequestTableItemView GetReviewCreditAppRequestInitialData(string id)
		{
			try
			{
				string draft = ((int)CreditAppRequestStatus.Draft).ToString();
				return (from creditAppLine in db.Set<CreditAppLine>()
						join creditAppTable in db.Set<CreditAppTable>() on creditAppLine.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID
						join customerTable in db.Set<CustomerTable>() on creditAppTable.CustomerTableGUID equals customerTable.CustomerTableGUID
						join buyerTable in db.Set<BuyerTable>() on creditAppLine.BuyerTableGUID equals buyerTable.BuyerTableGUID
						join buyerCreditLimitByProduct in db.Set<BuyerCreditLimitByProduct>().Where(w => w.ProductType == (int)ProductType.Factoring)
						on buyerTable.BuyerTableGUID equals buyerCreditLimitByProduct.BuyerTableGUID into ljBuyerCreditLimitByProduct
						from buyerCreditLimitByProduct in ljBuyerCreditLimitByProduct.DefaultIfEmpty()
						join documentStatus in db.Set<DocumentStatus>() on draft equals documentStatus.StatusId
						where creditAppLine.CreditAppLineGUID == id.StringToGuid()
						select new CreditAppRequestTableItemView
						{
							CreditAppRequestTableGUID = new Guid().GuidNullToString(),
							CreditAppRequestType = (int)CreditAppRequestType.ReviewBuyerCreditLimit,
							RequestDate = DateTime.Now.DateToString(),
							DocumentStatusGUID = documentStatus.DocumentStatusGUID.GuidNullToString(),
							DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
							DocumentStatus_StatusId = documentStatus.StatusId,
							ProductType = (int)ProductType.Factoring,
							CustomerTableGUID = creditAppTable.CustomerTableGUID.GuidNullToString(),
							CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
							BuyerTableGUID = creditAppLine.BuyerTableGUID.GuidNullToString(),
							BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
							RefCreditAppTableGUID = creditAppTable.CreditAppTableGUID.GuidNullToString(),
							RefCreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
							CreditLimitLineRequest = creditAppLine.ApprovedCreditLimitLine,
							RefCreditAppLineGUID = creditAppLine.CreditAppLineGUID.GuidNullToString(),
							RefCreditAppLine_Values = SmartAppUtil.GetDropDownLabel(creditAppLine.LineNum.ToString(), creditAppLine.ApprovedCreditLimitLine.ToString()),
							BuyerCreditLimit = (buyerCreditLimitByProduct != null) ? buyerCreditLimitByProduct.CreditLimit : 0,

							ProductSubTypeGUID = creditAppTable.ProductSubTypeGUID.GuidNullToString(),
							CreditLimitTypeGUID = creditAppTable.CreditLimitTypeGUID.GuidNullToString(),
							RegisteredAddressGUID = creditAppTable.RegisteredAddressGUID.GuidNullToString(),
							ApprovedCreditLimitRequest = creditAppTable.ApprovedCreditLimit,
							CreditLimitRequest = creditAppTable.ApprovedCreditLimit,
							InterestTypeGUID = creditAppTable.InterestTypeGUID.GuidNullToString(),
						}).FirstOrDefault();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Review CreditAppRequest
		#region CloseCustomerCreditLimit
		public CreditAppRequestTableItemView GetCloseCustomerCreditLimitInitialData(string id)
		{
			try
			{
				string draft = ((int)CreditAppRequestStatus.Draft).ToString();
				return (from creditAppTable in db.Set<CreditAppTable>()
						join customerTable in db.Set<CustomerTable>() on creditAppTable.CustomerTableGUID equals customerTable.CustomerTableGUID
						join documentStatus in db.Set<DocumentStatus>() on draft equals documentStatus.StatusId
						where creditAppTable.CreditAppTableGUID == id.StringToGuid()
						select new CreditAppRequestTableItemView
						{
							CreditAppRequestTableGUID = new Guid().GuidNullToString(),
							CreditAppRequestType = (int)CreditAppRequestType.CloseCustomerCreditLimit,
							RequestDate = DateTime.Now.DateToString(),
							DocumentStatusGUID = documentStatus.DocumentStatusGUID.GuidNullToString(),
							DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
							DocumentStatus_StatusId = documentStatus.StatusId,
							ProductType = creditAppTable.ProductType,
							CustomerTableGUID = creditAppTable.CustomerTableGUID.GuidNullToString(),
							CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
							RefCreditAppTableGUID = creditAppTable.CreditAppTableGUID.GuidNullToString(),
							RefCreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
							ProductSubTypeGUID = creditAppTable.ProductSubTypeGUID.GuidNullToString(),
							CreditLimitTypeGUID = creditAppTable.CreditLimitTypeGUID.GuidNullToString(),
							RegisteredAddressGUID = creditAppTable.RegisteredAddressGUID.GuidNullToString(),
							ApprovedCreditLimitRequest = creditAppTable.ApprovedCreditLimit,
							CreditLimitRequest = creditAppTable.ApprovedCreditLimit,
							InterestTypeGUID = creditAppTable.InterestTypeGUID.GuidNullToString(),
							KYCGUID = customerTable.KYCSetupGUID.GuidNullToString(),
							CreditScoringGUID = customerTable.CreditScoringGUID.GuidNullToString(),
						}).FirstOrDefault();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private IQueryable<CreditAppRequestTableItemViewMap> GetClosingItemQuery()
		{
			return (from creditAppRequestTable in db.Set<CreditAppRequestTable>()
					join company in db.Set<Company>()
					on creditAppRequestTable.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on creditAppRequestTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCreditAppRequestTableOwnerBU
					from ownerBU in ljCreditAppRequestTableOwnerBU.DefaultIfEmpty()
					join creditAppTable in db.Set<CreditAppTable>() on creditAppRequestTable.RefCreditAppTableGUID equals creditAppTable.CreditAppTableGUID
					join customerTable in db.Set<CustomerTable>() on creditAppTable.CustomerTableGUID equals customerTable.CustomerTableGUID
					join documentStatus in db.Set<DocumentStatus>() on creditAppRequestTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
					join documentReason in db.Set<DocumentReason>()
					on creditAppRequestTable.DocumentReasonGUID equals documentReason.DocumentReasonGUID into ljDocumentReason
					from documentReason in ljDocumentReason.DefaultIfEmpty()
					select new CreditAppRequestTableItemViewMap
					{
						CompanyGUID = creditAppRequestTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = creditAppRequestTable.Owner,
						OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = creditAppRequestTable.CreatedBy,
						CreatedDateTime = creditAppRequestTable.CreatedDateTime,
						ModifiedBy = creditAppRequestTable.ModifiedBy,
						ModifiedDateTime = creditAppRequestTable.ModifiedDateTime,
						CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
						CreditAppRequestId = creditAppRequestTable.CreditAppRequestId,
						CreditAppRequestType = creditAppRequestTable.CreditAppRequestType,
						RequestDate = creditAppRequestTable.RequestDate,
						DocumentStatusGUID = creditAppRequestTable.DocumentStatusGUID,
						DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
						DocumentStatus_StatusId = documentStatus.StatusId,
						ProductType = creditAppRequestTable.ProductType,
						CustomerTableGUID = creditAppTable.CustomerTableGUID,
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
						RefCreditAppTableGUID = creditAppTable.CreditAppTableGUID,
						RefCreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
						DocumentReason_Values = (documentReason != null) ? SmartAppUtil.GetDropDownLabel(documentReason.ReasonId, documentReason.Description) : string.Empty,
						ApprovedDate = creditAppRequestTable.ApprovedDate,
						Remark = creditAppRequestTable.Remark,
						Description = creditAppRequestTable.Description,
						MarketingComment = creditAppRequestTable.MarketingComment,
						ApproverComment = creditAppRequestTable.ApproverComment,
						CreditComment = creditAppRequestTable.CreditComment,
						CreditLimitRequest = creditAppTable.ApprovedCreditLimit,
						CustomerAllBuyerOutstanding = creditAppRequestTable.CustomerAllBuyerOutstanding,
						CreditLimitTypeGUID = creditAppRequestTable.CreditLimitTypeGUID,
						KYCGUID = creditAppRequestTable.KYCGUID,
						CreditScoringGUID = creditAppRequestTable.CreditScoringGUID,
						InterestTypeGUID = creditAppRequestTable.InterestTypeGUID,
						ProductSubTypeGUID = creditAppRequestTable.ProductSubTypeGUID,
						RowVersion = creditAppRequestTable.RowVersion,
					});
		}
		public CreditAppRequestTableItemView GetClosingByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.ToMap<CreditAppRequestTableItemViewMap, CreditAppRequestTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CloseCustomerCreditLimit
		public override void ValidateAdd(IEnumerable<CreditAppRequestTable> items)
		{
			base.ValidateAdd(items);
		}
		public override void ValidateAdd(CreditAppRequestTable item)
		{
			base.ValidateAdd(item);
		}
		#region GetCreditLimitClosingBookmarkView
		public CreditLimitClosingBookmarkView GetCreditLimitClosingBookmarkValue(Guid refGuid, Guid bookmarkDocumentTransGuid)
		{
			try
			{
				ISharedService sharedService = new SharedService(db);
				return (from creditAppRequestTable in Entity.Where(w => w.CreditAppRequestTableGUID == refGuid)
						join creditAppTable in db.Set<CreditAppTable>()
						on creditAppRequestTable.RefCreditAppTableGUID equals creditAppTable.CreditAppTableGUID
						into ljMainCreditAppRequestTable
						from creditAppTable in ljMainCreditAppRequestTable.DefaultIfEmpty()

						join documentStatus in db.Set<DocumentStatus>()
						on creditAppRequestTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus
						from documentStatus in ljdocumentStatus.DefaultIfEmpty()

						join customerTable in db.Set<CustomerTable>()
						on creditAppRequestTable.CustomerTableGUID equals customerTable.CustomerTableGUID into ljcustomerTable
						from customerTable in ljcustomerTable.DefaultIfEmpty()

						join employeeTable in db.Set<EmployeeTable>()
						on customerTable.ResponsibleByGUID equals employeeTable.EmployeeTableGUID into ljemployeeTable
						from employeeTable in ljemployeeTable.DefaultIfEmpty()

						join kycSetup in db.Set<KYCSetup>()
						on creditAppRequestTable.KYCGUID equals kycSetup.KYCSetupGUID into ljkycSetup
						from kycSetup in ljkycSetup.DefaultIfEmpty()

						join creditScoring in db.Set<CreditScoring>()
						on creditAppRequestTable.CreditScoringGUID equals creditScoring.CreditScoringGUID into ljcreditScoring
						from creditScoring in ljcreditScoring.DefaultIfEmpty()

						join customerCreditScoring in db.Set<CreditScoring>()
						on customerTable.CreditScoringGUID equals customerCreditScoring.CreditScoringGUID
						into ljCustomerCreditScoring
						from customerCreditScoring in ljCustomerCreditScoring.DefaultIfEmpty()

						join addressTrans in db.Set<AddressTrans>()
						on creditAppRequestTable.RegisteredAddressGUID equals addressTrans.AddressTransGUID into ljaddressTrans
						from addressTrans in ljaddressTrans.DefaultIfEmpty()

						join blacklistStatus in db.Set<BlacklistStatus>()
						on customerTable.BlacklistStatusGUID equals blacklistStatus.BlacklistStatusGUID into ljblacklistStatus
						from blacklistStatus in ljblacklistStatus.DefaultIfEmpty()

						join businessType in db.Set<BusinessType>()
						on customerTable.BusinessTypeGUID equals businessType.BusinessTypeGUID into ljbusinessType
						from businessType in ljbusinessType.DefaultIfEmpty()

						join documentReason in db.Set<DocumentReason>()
						on creditAppRequestTable.DocumentReasonGUID equals documentReason.DocumentReasonGUID into ljdocreason
						from documentReason in ljdocreason.DefaultIfEmpty()

						select new CreditLimitClosingBookmarkView
						{
							CANumber = creditAppRequestTable.CreditAppRequestId,
							RefCANumber = creditAppTable == null ? null : creditAppTable.CreditAppId,
							CAStatus = documentStatus.Description,
							CACreate = creditAppRequestTable.RequestDate,
							CustomerName1 = customerTable.Name,
							CustomerName2 = customerTable.Name,
							CustomerCode = customerTable.CustomerId,
							SalesResp = employeeTable != null ? employeeTable.Name : "",
							KYCHeader = kycSetup != null ? kycSetup.Description : "",

							CreditScoring1 = creditScoring == null ? null : creditScoring.Description,
							CreditScoring2 = customerCreditScoring == null ? null : customerCreditScoring.Description,

							CADesp = creditAppRequestTable.Description,
							CustCompanyRegistrationID = customerTable.IdentificationType == (int)IdentificationType.TaxId ? customerTable.TaxID : customerTable.PassportID,
							CustComEstablished = customerTable.RecordType == (int)RecordType.Person ? customerTable.DateOfBirth : customerTable.DateOfEstablish,
							CustomerAddress = addressTrans.Address1 + " " + addressTrans.Address2,
							CustBusinessType = businessType != null ? businessType.Description : "",
							CustBlackListStatus = blacklistStatus != null ? blacklistStatus.Description : "",
							TotalApprovedCreditLimit = creditAppRequestTable.ApprovedCreditLimitRequest,
							TotalAROutstanding = creditAppRequestTable.CustomerAllBuyerOutstanding,
							MarketingComment = creditAppRequestTable.MarketingComment,
							CreditComment = creditAppRequestTable.CreditComment,
							ApproverComment = creditAppRequestTable.ApproverComment,
							CARemark = creditAppRequestTable.Remark,
							ReasonRemark = documentReason == null ? null : documentReason.Description,
						}).AsNoTracking().FirstOrDefault();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region K2 Function
		public GetCreditApplicationRequestEmailContentK2ResultView GetCreditApplicationRequestEmailContentK2(Guid id, string userName)
		{
			try
			{
				#region 1.Get data into variable
				#region Global Variable
				string up = "เพิ่ม";
				string down = "ลด";
				string changed = "เปลี่ยนแปลง";
				string unchanged = "คงเดิม";
				#endregion Global Variable
				#region 1.1 General data(Query all condition)
				var productTypeList = ConditionService.EnumToList<ProductType>(true);
				ISysUserTableRepo sysUserTableRepo = new SysUserTableRepo(db);
				SysUserTable sysUserTable = sysUserTableRepo.GetSysUserTableByUserNameNoTracking(userName.FirstToList()).FirstOrDefault();
				GetCreditApplicationRequestEmailContentK2ResultView
					result = (from creditAppRequestTable in Entity
							  join customerTable in db.Set<CustomerTable>() on creditAppRequestTable.CustomerTableGUID equals customerTable.CustomerTableGUID
							  join employeeTable in db.Set<EmployeeTable>().Where(w => w.UserId == sysUserTable.Id)
							  on creditAppRequestTable.CompanyGUID equals employeeTable.CompanyGUID into ljEmployeeTable
							  from employeeTable in ljEmployeeTable.DefaultIfEmpty()
							  join contactTrans in db.Set<ContactTrans>().Where(w => w.RefType == (int)RefType.Employee
																			&& w.ContactType == (int)ContactType.Phone
																			&& w.PrimaryContact == true)
							  on employeeTable.EmployeeTableGUID equals contactTrans.RefGUID into ljContactTrans
							  from contactTrans in ljContactTrans.DefaultIfEmpty()
							  where creditAppRequestTable.CreditAppRequestTableGUID == id
							  select new GetCreditApplicationRequestEmailContentK2ResultView
							  {
								  CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
								  CreditAppRequestType = creditAppRequestTable.CreditAppRequestType,
								  InterestTypeGUID = creditAppRequestTable.InterestTypeGUID,
								  RequestDate = creditAppRequestTable.RequestDate,
								  ParmCustomerName = customerTable.Name,
								  ParmCreditRequestFeeAmount = creditAppRequestTable.CreditRequestFeeAmount.DecimalToString(),
								  ParmMaxRetentionAmount = creditAppRequestTable.MaxRetentionAmount.DecimalToString(),
								  ParmProductTypeDescription = creditAppRequestTable.ProductType.ToString(),
								  ParmApprovedCreditLimitRequest = creditAppRequestTable.ApprovedCreditLimitRequest.DecimalToString(),
								  ParmMarketingEmpName = (employeeTable != null) ? employeeTable.Name : string.Empty,
								  ParmMarketingEmpPosition = (employeeTable != null) ? employeeTable.Position : string.Empty,
								  ParmMarketingEmpPhone = (contactTrans != null) ? contactTrans.ContactValue : string.Empty,
								  RefCreditAppTableGUID = creditAppRequestTable.RefCreditAppTableGUID,
								  InterestAdjustmentPct = creditAppRequestTable.InterestAdjustment,
							  }).FirstOrDefault();
				result.ParmProductTypeDescription = SystemStaticData.GetTranslatedMessage((((ProductType)Convert.ToInt32(result.ParmProductTypeDescription)).GetAttrCode()));

				if ((result.CreditAppRequestType == (int)CreditAppRequestType.BuyerMatching ||
				   result.CreditAppRequestType == (int)CreditAppRequestType.LoanRequest ||
				   result.CreditAppRequestType == (int)CreditAppRequestType.ActiveAmendCustomerCreditLimit ||
				   result.CreditAppRequestType == (int)CreditAppRequestType.AmendCustomerBuyerCreditLimit))
				{


					var authorizedPerson = (from authorizedPersonTrans in db.Set<AuthorizedPersonTrans>().Where(w => w.RefType == (int)RefType.CreditAppTable
																						 && w.InActive == false
																						 && w.RefGUID == result.RefCreditAppTableGUID)
											join relatedPersonTable in db.Set<RelatedPersonTable>()
											on authorizedPersonTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID into ljRelatedPersonTable
											from relatedPersonTable in ljRelatedPersonTable.DefaultIfEmpty()
											select new GetCreditApplicationRequestEmailContentK2ResultView
											{
												ParmAuthorizedPersonName = (relatedPersonTable != null) ? relatedPersonTable.Name : string.Empty,
												OrderingAuthorizedPersonName = (authorizedPersonTrans != null) ? authorizedPersonTrans.Ordering : 0,
											});

					if (authorizedPerson.Any())
					{
						result.ParmAuthorizedPersonFirst = authorizedPerson.OrderBy(s => s.OrderingAuthorizedPersonName).Select(s => s.ParmAuthorizedPersonName).First();
					}
				}
				if (result == null)
				{
					return null;
				}
				#endregion 1.1 General data(Query all condition)
				#region Method042_GetInterestTypeValue For 1.2, 1.5
				IInterestTypeValueRepo interestTypeValueRepo = new InterestTypeValueRepo(db);
				decimal parmTotalInterestPct = 0;
				if (result.CreditAppRequestType == (int)CreditAppRequestType.MainCreditLimit || result.CreditAppRequestType == (int)CreditAppRequestType.AmendCustomerInfo)
				{
					InterestTypeValue interestTypeValue = interestTypeValueRepo.GetByInterestTypeDateNoTracking(result.InterestTypeGUID, result.RequestDate.Value, true);
					if (interestTypeValue != null)
					{
						parmTotalInterestPct = result.InterestAdjustmentPct + interestTypeValue.Value;
					}
				}
				#endregion Method042_GetInterestTypeValue For 1.2, 1.5
				#region 1.2	IF CAReqTable.CreditAppRequestType = CreditAppRequestType.MainCreditLimit (0)
				if (result.CreditAppRequestType == (int)CreditAppRequestType.MainCreditLimit)
				{
					var authorizedPerson = (from authorizedPersonTrans in db.Set<AuthorizedPersonTrans>().Where(w => w.RefType == (int)RefType.CreditAppRequestTable
																										 && w.InActive == false
																										 && w.RefGUID == id)
											join relatedPersonTable in db.Set<RelatedPersonTable>()
											on authorizedPersonTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID into ljRelatedPersonTable
											from relatedPersonTable in ljRelatedPersonTable.DefaultIfEmpty()
											select new GetCreditApplicationRequestEmailContentK2ResultView
											{
												ParmAuthorizedPersonName = (relatedPersonTable != null) ? relatedPersonTable.Name : string.Empty,
												OrderingAuthorizedPersonName = (authorizedPersonTrans != null) ? authorizedPersonTrans.Ordering : 0,
											});
					var guarantor = (from guarantorTrans in db.Set<GuarantorTrans>().Where(w => w.RefType == (int)RefType.CreditAppRequestTable
																	   && w.InActive == false
																	   && w.RefGUID == id)

									 join guarantorRelatedPersonTable in db.Set<RelatedPersonTable>()
									 on guarantorTrans.RelatedPersonTableGUID equals guarantorRelatedPersonTable.RelatedPersonTableGUID into ljGuarantorRelatedPersonTable
									 from guarantorRelatedPersonTable in ljGuarantorRelatedPersonTable.DefaultIfEmpty()
									 select new GetCreditApplicationRequestEmailContentK2ResultView
									 {
										 ParmGuarantorName = (guarantorRelatedPersonTable != null) ? guarantorRelatedPersonTable.Name : string.Empty,
										 OrderingGuarantorName = (guarantorTrans != null) ? guarantorTrans.Ordering : 0,
									 });

					if (authorizedPerson.Any())
					{
						result.ParmAuthorizedPersonName = string.Join(", ", authorizedPerson.OrderBy(o => o.OrderingAuthorizedPersonName)
																							.Select(s => s.ParmAuthorizedPersonName));
						// 1.1
						result.ParmAuthorizedPersonFirst = authorizedPerson.OrderBy(s => s.OrderingAuthorizedPersonName).Select(s => s.ParmAuthorizedPersonName).First();

					}
					if (guarantor.Any())
					{
						result.ParmGuarantorName = string.Join(", ", guarantor.OrderBy(o => o.OrderingGuarantorName)
																					 .Select(s => s.ParmGuarantorName));
					}
				}
				#endregion 1.2	IF CAReqTable.CreditAppRequestType = CreditAppRequestType.MainCreditLimit (0)
				#region 1.3	IF CAReqTable.CreditAppRequestType IN (MainCreditLimit (0), BuyerMatching (1), LoanRequest (2)) 
				if (result.CreditAppRequestType == (int)CreditAppRequestType.MainCreditLimit ||
				   result.CreditAppRequestType == (int)CreditAppRequestType.BuyerMatching ||
				   result.CreditAppRequestType == (int)CreditAppRequestType.LoanRequest)
				{
					var buyerTableName = (from creditAppRequestTable in Entity
										  join creditAppRequestLine in db.Set<CreditAppRequestLine>()
										  on creditAppRequestTable.CreditAppRequestTableGUID equals creditAppRequestLine.CreditAppRequestTableGUID
										  join buyerTable in db.Set<BuyerTable>() on creditAppRequestLine.BuyerTableGUID equals buyerTable.BuyerTableGUID
										  where creditAppRequestLine.ApprovalDecision == (int)ApprovalDecision.Approved &&
												creditAppRequestTable.CreditAppRequestTableGUID == id
										  select new GetCreditApplicationRequestEmailContentK2ResultView
										  {
											  ParmBuyerNameCreditLimit = $"{buyerTable.BuyerName} วงเงินลูกหนี้ {creditAppRequestLine.ApprovedCreditLimitLineRequest.DecimalToString()} บาท",
											  LineNumCreditAppRequestLine = creditAppRequestLine.LineNum,
										  }).ToList();
					if (buyerTableName.Any())
					{
						result.ParmNumberOfCreditAppRequestLine = buyerTableName.Count();
						result.ParmBuyerNameCreditLimit = string.Join(", ", buyerTableName.OrderBy(o => o.LineNumCreditAppRequestLine).Select(s => s.ParmBuyerNameCreditLimit));
					}
				}
				#endregion 1.3	IF CAReqTable.CreditAppRequestType IN (MainCreditLimit (0), BuyerMatching (1), LoanRequest (2)) 
				#region 1.4	IF CAReqTable.CreditAppRequestType = CreditAppRequestType.ActiveAmendCustomerCreditLimit (3)
				if (result.CreditAppRequestType == (int)CreditAppRequestType.ActiveAmendCustomerCreditLimit)
				{
					var amendOriginalCreditLimit = (from creditAppRequestTableAmend in db.Set<CreditAppRequestTableAmend>()
													where creditAppRequestTableAmend.CreditAppRequestTableGUID == id
													select new GetCreditApplicationRequestEmailContentK2ResultView
													{
														OriginalCreditLimit = creditAppRequestTableAmend.OriginalCreditLimit,
													}).FirstOrDefault();
					decimal parmApprovedCreditLimitRequest = Convert.ToDecimal(result.ParmApprovedCreditLimitRequest);
					decimal originalCreditLimit = (amendOriginalCreditLimit != null) ? amendOriginalCreditLimit.OriginalCreditLimit : 0;
					decimal diff = (parmApprovedCreditLimitRequest - originalCreditLimit);
					decimal parmCreditRequestFeeAmount = Convert.ToDecimal(result.ParmCreditRequestFeeAmount);
					result.ParmOriginalCreditLimit = originalCreditLimit.DecimalToString();
					result.ParmCreditLimitChange = diff > 0 ? up : diff < 0 ? down : unchanged;
					result.ParmCreditRequestFeeAmountChange = parmCreditRequestFeeAmount > 0 ? up : parmCreditRequestFeeAmount < 0 ? down : unchanged;
				}
				#endregion 1.4	IF CAReqTable.CreditAppRequestType = CreditAppRequestType.ActiveAmendCustomerCreditLimit (3)
				#region 1.5	IF CAReqTable.CreditAppRequestType = CreditAppRequestType.AmendCustomerInfo (4)
				if (result.CreditAppRequestType == (int)CreditAppRequestType.AmendCustomerInfo)
				{
					var authorizedPerson = (from authorizedPersonTrans in db.Set<AuthorizedPersonTrans>().Where(w => w.RefType == (int)RefType.CreditAppRequestTable
																					 && w.InActive == false
																					 && w.RefGUID == id)
											join relatedPersonTable in db.Set<RelatedPersonTable>()
											on authorizedPersonTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID into ljRelatedPersonTable
											from relatedPersonTable in ljRelatedPersonTable.DefaultIfEmpty()
											select new GetCreditApplicationRequestEmailContentK2ResultView
											{
												ParmAuthorizedPersonName = (relatedPersonTable != null) ? relatedPersonTable.Name : string.Empty,
												OrderingAuthorizedPersonName = (authorizedPersonTrans != null) ? authorizedPersonTrans.Ordering : 0,
											});
					var guarantor = (from guarantorTrans in db.Set<GuarantorTrans>().Where(w => w.RefType == (int)RefType.CreditAppRequestTable
																	   && w.InActive == false
																	   && w.RefGUID == id)

									 join guarantorRelatedPersonTable in db.Set<RelatedPersonTable>()
									 on guarantorTrans.RelatedPersonTableGUID equals guarantorRelatedPersonTable.RelatedPersonTableGUID into ljGuarantorRelatedPersonTable
									 from guarantorRelatedPersonTable in ljGuarantorRelatedPersonTable.DefaultIfEmpty()
									 select new GetCreditApplicationRequestEmailContentK2ResultView
									 {
										 ParmGuarantorName = (guarantorRelatedPersonTable != null) ? guarantorRelatedPersonTable.Name : string.Empty,
										 OrderingGuarantorName = (guarantorTrans != null) ? guarantorTrans.Ordering : 0,
									 });
					var originAuthorizedPerson = (from originalAuthorizedPersonTrans in db.Set<AuthorizedPersonTrans>().Where(w => w.RefType == (int)RefType.CreditAppTable
																																&& w.RefGUID == result.RefCreditAppTableGUID)
												  join originalRelatedPersonTable in db.Set<RelatedPersonTable>()
												  on originalAuthorizedPersonTrans.RelatedPersonTableGUID equals originalRelatedPersonTable.RelatedPersonTableGUID into ljOriginalRelatedPersonTable
												  from originalRelatedPersonTable in ljOriginalRelatedPersonTable.DefaultIfEmpty()
												  select new GetCreditApplicationRequestEmailContentK2ResultView
												  {
													  ParmOriginalAuthorizedPersonName = (originalRelatedPersonTable != null) ? originalRelatedPersonTable.Name : string.Empty,
													  OrderingOriginalAuthorizedPersonName = (originalAuthorizedPersonTrans != null) ? originalAuthorizedPersonTrans.Ordering : 0,
												  });
					var originGuarantor = (from originalGuarantorTrans in db.Set<GuarantorTrans>().Where(w => w.RefType == (int)RefType.CreditAppTable
																										   && w.RefGUID == result.RefCreditAppTableGUID)
										   join originalGuarantorRelatedPersonTable in db.Set<RelatedPersonTable>()
										   on originalGuarantorTrans.RelatedPersonTableGUID equals originalGuarantorRelatedPersonTable.RelatedPersonTableGUID into ljOriginalGuarantorRelatedPersonTable
										   from originalGuarantorRelatedPersonTable in ljOriginalGuarantorRelatedPersonTable.DefaultIfEmpty()
										   select new GetCreditApplicationRequestEmailContentK2ResultView
										   {
											   ParmOriginalGuarantorName = (originalGuarantorRelatedPersonTable != null) ? originalGuarantorRelatedPersonTable.Name : string.Empty,
											   OrderingOriginalGuarantorName = (originalGuarantorTrans != null) ? originalGuarantorTrans.Ordering : 0,
										   });
					var amendOriginalCreditLimit = (from creditAppRequestTable in Entity
													join creditAppRequestTableAmend in db.Set<CreditAppRequestTableAmend>()
													on creditAppRequestTable.CreditAppRequestTableGUID equals creditAppRequestTableAmend.CreditAppRequestTableGUID

													where creditAppRequestTableAmend.CreditAppRequestTableGUID == id
													select new GetCreditApplicationRequestEmailContentK2ResultView
													{
														ParmAmendRate = creditAppRequestTableAmend.AmendRate.ToString(),
														ParmAmendAuthorizedPerson = creditAppRequestTableAmend.AmendAuthorizedPerson.ToString(),
														ParmAmendGuarantor = creditAppRequestTableAmend.AmendGuarantor.ToString(),
														ParmOriginalMaxRetentionAmount = creditAppRequestTableAmend.OriginalMaxRetentionAmount.DecimalToString(),
														ParmOriginalTotalInterestPct = creditAppRequestTableAmend.OriginalTotalInterestPct.DecimalToString(),
														ParmOriginalCreditRequestFeeAmount = creditAppRequestTableAmend.OriginalCreditLimitRequestFeeAmount.DecimalToString(),
													}).ToList();
					bool amendRate = false;
					bool amendAuthorizedPerson = false;
					bool amendGuarantor = false;
					if (amendOriginalCreditLimit.Any())
					{
						amendRate = bool.Parse(amendOriginalCreditLimit.First().ParmAmendRate);
						amendAuthorizedPerson = bool.Parse(amendOriginalCreditLimit.First().ParmAmendAuthorizedPerson);
						amendGuarantor = bool.Parse(amendOriginalCreditLimit.First().ParmAmendGuarantor);
						result.ParmAmendRate = amendRate ? changed : unchanged;
						result.ParmAmendAuthorizedPerson = amendAuthorizedPerson ? changed : unchanged;
						result.ParmAmendGuarantor = amendGuarantor ? changed : unchanged;
					}
					if (originAuthorizedPerson.Any())
					{
						result.ParmOriginalAuthorizedPersonName = string.Join(", ", originAuthorizedPerson.OrderBy(s => s.OrderingOriginalAuthorizedPersonName)
																										 .Select(s => s.ParmOriginalAuthorizedPersonName));
					}
					if (originGuarantor.Any())
					{
						result.ParmOriginalGuarantorName = string.Join(", ", originGuarantor.OrderBy(o => o.OrderingOriginalGuarantorName)
																				   .Select(s => s.ParmOriginalGuarantorName));
					}
					if (authorizedPerson.Any())
					{
						result.ParmAuthorizedPersonName = (amendAuthorizedPerson) ? string.Join(", ", authorizedPerson.OrderBy(s => s.OrderingAuthorizedPersonName)
																															 .Select(s => s.ParmAuthorizedPersonName))
																				  : result.ParmOriginalAuthorizedPersonName;
						result.ParmAuthorizedPersonFirst = (amendAuthorizedPerson) ? authorizedPerson.OrderBy(s => s.OrderingAuthorizedPersonName).Select(s => s.ParmAuthorizedPersonName).First()
																				   : (originAuthorizedPerson.Any())
																				   ? originAuthorizedPerson.OrderBy(s => s.OrderingOriginalAuthorizedPersonName).Select(s => s.ParmOriginalAuthorizedPersonName).First()
																				   : string.Empty;


					}
					if (guarantor.Any())
					{
						result.ParmGuarantorName = (amendGuarantor) ? string.Join(", ", guarantor.OrderBy(s => s.OrderingGuarantorName)
																								.Select(s => s.ParmGuarantorName))
																	: result.ParmOriginalGuarantorName;
					}
					decimal parmMaxRetentionAmount = Convert.ToDecimal(result.ParmMaxRetentionAmount);
					decimal totalInterestPct = Convert.ToDecimal(result.ParmTotalInterestPct);
					decimal parmCreditRequestFeeAmount = Convert.ToDecimal(result.ParmCreditRequestFeeAmount);
					decimal parmOriginalMaxRetentionAmount = Convert.ToDecimal(result.ParmOriginalMaxRetentionAmount);
					decimal parmOriginalTotalInterestPct = Convert.ToDecimal(result.ParmOriginalTotalInterestPct);
					decimal diffParmMaxRetentionAmountChange = (parmMaxRetentionAmount - parmOriginalMaxRetentionAmount);
					result.ParmMaxRetentionAmountChange = diffParmMaxRetentionAmountChange > 0 ? up : diffParmMaxRetentionAmountChange < 0 ? down : unchanged;
					decimal diffParmTotalIntestPctChange = (totalInterestPct - parmOriginalTotalInterestPct);
					result.ParmTotalIntestPctChange = diffParmTotalIntestPctChange > 0 ? up : diffParmTotalIntestPctChange < 0 ? down : unchanged;
					result.ParmCreditRequestFeeAmountChange = parmCreditRequestFeeAmount > 0 ? up : parmCreditRequestFeeAmount < 0 ? down : unchanged;

				}
				#endregion 1.5	IF CAReqTable.CreditAppRequestType = CreditAppRequestType.AmendCustomerInfo (4)
				#region 1.6	IF CAReqTable.CreditAppRequestType = CreditAppRequestType.AmendCustomerBuyerCreditLimit (5)
				if (result.CreditAppRequestType == (int)CreditAppRequestType.AmendCustomerBuyerCreditLimit)
				{
					IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
					DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByStatusId(Convert.ToInt32(MainAgreementStatus.Signed).ToString());
					var amendLine = (from creditAppRequestTable in Entity
									 join creditAppRequestLine in db.Set<CreditAppRequestLine>()
									 on creditAppRequestTable.CreditAppRequestTableGUID equals creditAppRequestLine.CreditAppRequestTableGUID
									 join creditAppRequestLineAmend in db.Set<CreditAppRequestLineAmend>()
									 on creditAppRequestLine.CreditAppRequestLineGUID equals creditAppRequestLineAmend.CreditAppRequestLineGUID
									 join mainAgreement in db.Set<MainAgreementTable>().Where(w => w.AgreementDocType == (int)AgreementDocType.New &&
																								   w.DocumentStatusGUID == documentStatus.DocumentStatusGUID)

									 on creditAppRequestTable.RefCreditAppTableGUID equals mainAgreement.CreditAppTableGUID into ljMainAgreementTable
									 from mainAgreement in ljMainAgreementTable.DefaultIfEmpty()

									 join assignmentMethod in db.Set<AssignmentMethod>()
									 on creditAppRequestLine.AssignmentMethodGUID equals assignmentMethod.AssignmentMethodGUID into ljAssignmentMethod
									 from assignmentMethod in ljAssignmentMethod.DefaultIfEmpty()

									 join billingResponsibleBy in db.Set<BillingResponsibleBy>()
									 on creditAppRequestLine.BillingResponsibleByGUID equals billingResponsibleBy.BillingResponsibleByGUID into ljBillingResponsibleBy
									 from billingResponsibleBy in ljBillingResponsibleBy.DefaultIfEmpty()

									 join methodOfPayment in db.Set<MethodOfPayment>()
									 on creditAppRequestLine.MethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID into ljMethodOfPayment
									 from methodOfPayment in ljMethodOfPayment.DefaultIfEmpty()

									 join buyerTable in db.Set<BuyerTable>() on creditAppRequestLine.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljBuyerTable
									 from buyerTable in ljBuyerTable.DefaultIfEmpty()
									 where creditAppRequestTable.CreditAppRequestTableGUID == id
									 select new GetCreditApplicationRequestEmailContentK2ResultView
									 {
										 ParmCreditLimitChange = creditAppRequestLineAmend.OriginalCreditLimitLineRequest.DecimalToString(),
										 ParmMaxPurchasePct = (creditAppRequestTable.ProductType == (int)ProductType.Factoring) ? $"{creditAppRequestLine.MaxPurchasePct.DecimalToString()}%" : "ไม่มี",
										 ParmAssignmentMethod = (assignmentMethod != null) ? assignmentMethod.Description : string.Empty,
										 ParmBillingResponsibleBy = (billingResponsibleBy != null) ? billingResponsibleBy.Description : string.Empty,
										 ParmMethodOfPayment = (methodOfPayment != null) ? methodOfPayment.Description : string.Empty,
										 ParmBuyerName = (buyerTable != null) ? buyerTable.BuyerName : string.Empty,
										 ParmMainAgreementId = (mainAgreement != null) ? mainAgreement.MainAgreementId : string.Empty,
										 MainAgreementDate = (mainAgreement != null) ? mainAgreement.AgreementDate : (DateTime?)null,
										 ParmApprovedCreditLimitRequest = creditAppRequestLine.ApprovedCreditLimitLineRequest.DecimalToString(),
									 }).FirstOrDefault();

					if (amendLine != null)
					{

						decimal parmApprovedCreditLimitRequest = Convert.ToDecimal(amendLine.ParmApprovedCreditLimitRequest);
						decimal originalCreditLimit = Convert.ToDecimal(amendLine.ParmCreditLimitChange);
						decimal diff = (parmApprovedCreditLimitRequest - originalCreditLimit);
						result.ParmCreditLimitChange = diff > 0 ? up : diff < 0 ? down : unchanged;
						result.ParmMaxPurchasePct = amendLine.ParmMaxPurchasePct;
						result.ParmAssignmentMethod = amendLine.ParmAssignmentMethod;
						result.ParmBillingResponsibleBy = amendLine.ParmBillingResponsibleBy;
						result.ParmMethodOfPayment = amendLine.ParmMethodOfPayment;
						result.ParmBuyerName = amendLine.ParmBuyerName;
						result.ParmMainAgreementId = amendLine.ParmMainAgreementId;
						result.ParmApprovedCreditLimitRequest = amendLine.ParmApprovedCreditLimitRequest;
						ISharedService sharedService = new SharedService(db);
						result.ParmMainAgreementDate = (amendLine.MainAgreementDate.HasValue) ? sharedService.DateToWord(amendLine.MainAgreementDate.Value, TextConstants.TH, TextConstants.DMY) : string.Empty;
					}
				}
				#endregion 1.6	IF CAReqTable.CreditAppRequestType = CreditAppRequestType.AmendCustomerBuyerCreditLimit (5)
				decimal defaultDecimal = 0m;
				string defaultDecimalString = defaultDecimal.DecimalToString();
				result.ParmCreditRequestFeeAmount = (string.IsNullOrEmpty(result.ParmCreditRequestFeeAmount) || result.ParmCreditRequestFeeAmount == "0") ? defaultDecimalString : result.ParmCreditRequestFeeAmount;
				result.ParmMaxRetentionAmount = (string.IsNullOrEmpty(result.ParmMaxRetentionAmount) || result.ParmMaxRetentionAmount == "0") ? defaultDecimalString : result.ParmMaxRetentionAmount;
				result.ParmApprovedCreditLimitRequest = (string.IsNullOrEmpty(result.ParmApprovedCreditLimitRequest) || result.ParmApprovedCreditLimitRequest == "0") ? defaultDecimalString : result.ParmApprovedCreditLimitRequest;
				result.ParmOriginalCreditLimit = (string.IsNullOrEmpty(result.ParmOriginalCreditLimit) || result.ParmOriginalCreditLimit == "0") ? defaultDecimalString : result.ParmOriginalCreditLimit;
				result.ParmOriginalCreditRequestFeeAmount = (string.IsNullOrEmpty(result.ParmOriginalCreditRequestFeeAmount) || result.ParmOriginalCreditRequestFeeAmount == "0") ? defaultDecimalString : result.ParmOriginalCreditRequestFeeAmount;
				result.ParmOriginalMaxRetentionAmount = (string.IsNullOrEmpty(result.ParmOriginalMaxRetentionAmount) || result.ParmOriginalMaxRetentionAmount == "0") ? defaultDecimalString : result.ParmOriginalMaxRetentionAmount;
				result.ParmOriginalTotalInterestPct = (string.IsNullOrEmpty(result.ParmOriginalTotalInterestPct) || result.ParmOriginalTotalInterestPct == "0") ? defaultDecimalString : result.ParmOriginalTotalInterestPct;
				result.ParmApprovedCreditLimitRequest = (string.IsNullOrEmpty(result.ParmApprovedCreditLimitRequest) || result.ParmApprovedCreditLimitRequest == "0") ? defaultDecimalString : result.ParmApprovedCreditLimitRequest;
				result.ParmTotalInterestPct = parmTotalInterestPct.DecimalToString();
				#endregion 1.Get data into variable
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}


		#endregion K2 Function
		#region BookmarkDocumentQueryCreditApplicationPF
		public BookmarkDocumentQueryCreditApplicationPF GetBookmarkDocumentQueryCreditApplicationPFValues(Guid refGUID, Guid bookmarkDocumentTransGUID)
		{
			try
			{
				BookmarkDocumentQueryCreditApplicationPF model = new BookmarkDocumentQueryCreditApplicationPF();
				QCreditAppRequestTableCAPF qCreditAppRequestTableCAPF = GetQCreditAppRequestTableCAPF(refGUID);
				SetmodelByQCreditAppRequestTableCAPF(model, qCreditAppRequestTableCAPF);

				QCreditAppRequestLineCAPF qCreditAppRequestLineCAPF = GetQCreditAppRequestLineCAPF(refGUID);
				SetmodelQCreditAppRequestLineCAPF(model, qCreditAppRequestLineCAPF);

				QBuyerTableCAPF qBuyerTableCAPF = GetQBuyerTableCAPF(qCreditAppRequestLineCAPF.QCreditAppRequestLineCAPF_BuyerTableGUID);
				SetModelByQBuyerTableCAPF(model, qBuyerTableCAPF);

				QBuyerAgreementCAPF qBuyerAgreementCAPF = GetQBuyerAgreementCAPF(qCreditAppRequestLineCAPF.QCreditAppRequestLineCAPF_CreditAppRequestLineGUID);
				SetModelByQBuyerAgreementCAPF(model, qBuyerAgreementCAPF);

				List<QBuyerAgreementLineCAPF> qBuyerAgreementLineCAPF = GetQBuyerAgreementLineCAPF(qBuyerAgreementCAPF.QBuyerAgreementCAPF_BuyerAgreementTableGUID);
				QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_1 = GetQBuyerAgreementLineCAPFEachRow(qBuyerAgreementLineCAPF, 1);
				SetModelByQBuyerAgreementLineCAPF_1(model, qBuyerAgreementLineCAPF_1);
				QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_2 = GetQBuyerAgreementLineCAPFEachRow(qBuyerAgreementLineCAPF, 2);
				SetModelByQBuyerAgreementLineCAPF_2(model, qBuyerAgreementLineCAPF_2);
				QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_3 = GetQBuyerAgreementLineCAPFEachRow(qBuyerAgreementLineCAPF, 3);
				SetModelByQBuyerAgreementLineCAPF_3(model, qBuyerAgreementLineCAPF_3);
				QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_4 = GetQBuyerAgreementLineCAPFEachRow(qBuyerAgreementLineCAPF, 4);
				SetModelByQBuyerAgreementLineCAPF_4(model, qBuyerAgreementLineCAPF_4);
				QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_5 = GetQBuyerAgreementLineCAPFEachRow(qBuyerAgreementLineCAPF, 5);
				SetModelByQBuyerAgreementLineCAPF_5(model, qBuyerAgreementLineCAPF_5);
				QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_6 = GetQBuyerAgreementLineCAPFEachRow(qBuyerAgreementLineCAPF, 6);
				SetModelByQBuyerAgreementLineCAPF_6(model, qBuyerAgreementLineCAPF_6);
				QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_7 = GetQBuyerAgreementLineCAPFEachRow(qBuyerAgreementLineCAPF, 7);
				SetModelByQBuyerAgreementLineCAPF_7(model, qBuyerAgreementLineCAPF_7);
				QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_8 = GetQBuyerAgreementLineCAPFEachRow(qBuyerAgreementLineCAPF, 8);
				SetModelByQBuyerAgreementLineCAPF_8(model, qBuyerAgreementLineCAPF_8);
				QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_9 = GetQBuyerAgreementLineCAPFEachRow(qBuyerAgreementLineCAPF, 9);
				SetModelByQBuyerAgreementLineCAPF_9(model, qBuyerAgreementLineCAPF_9);
				QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_10 = GetQBuyerAgreementLineCAPFEachRow(qBuyerAgreementLineCAPF, 10);
				SetModelByQBuyerAgreementLineCAPF_10(model, qBuyerAgreementLineCAPF_10);
				QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_11 = GetQBuyerAgreementLineCAPFEachRow(qBuyerAgreementLineCAPF, 11);
				SetModelByQBuyerAgreementLineCAPF_11(model, qBuyerAgreementLineCAPF_11);
				QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_12 = GetQBuyerAgreementLineCAPFEachRow(qBuyerAgreementLineCAPF, 12);
				SetModelByQBuyerAgreementLineCAPF_12(model, qBuyerAgreementLineCAPF_12);
				QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_13 = GetQBuyerAgreementLineCAPFEachRow(qBuyerAgreementLineCAPF, 13);
				SetModelByQBuyerAgreementLineCAPF_13(model, qBuyerAgreementLineCAPF_13);
				QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_14 = GetQBuyerAgreementLineCAPFEachRow(qBuyerAgreementLineCAPF, 14);
				SetModelByQBuyerAgreementLineCAPF_14(model, qBuyerAgreementLineCAPF_14);
				QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_15 = GetQBuyerAgreementLineCAPFEachRow(qBuyerAgreementLineCAPF, 15);
				SetModelByQBuyerAgreementLineCAPF_15(model, qBuyerAgreementLineCAPF_15);
				QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_16 = GetQBuyerAgreementLineCAPFEachRow(qBuyerAgreementLineCAPF, 16);
				SetModelByQBuyerAgreementLineCAPF_16(model, qBuyerAgreementLineCAPF_16);
				QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_17 = GetQBuyerAgreementLineCAPFEachRow(qBuyerAgreementLineCAPF, 17);
				SetModelByQBuyerAgreementLineCAPF_17(model, qBuyerAgreementLineCAPF_17);
				QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_18 = GetQBuyerAgreementLineCAPFEachRow(qBuyerAgreementLineCAPF, 18);
				SetModelByQBuyerAgreementLineCAPF_18(model, qBuyerAgreementLineCAPF_18);
				QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_19 = GetQBuyerAgreementLineCAPFEachRow(qBuyerAgreementLineCAPF, 19);
				SetModelByQBuyerAgreementLineCAPF_19(model, qBuyerAgreementLineCAPF_19);
				QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_20 = GetQBuyerAgreementLineCAPFEachRow(qBuyerAgreementLineCAPF, 20);
				SetModelByQBuyerAgreementLineCAPF_20(model, qBuyerAgreementLineCAPF_20);
				decimal qSumBuyerAgreementLineCAPF = GetQSumBuyerAgreementLineCAPF(qBuyerAgreementCAPF.QBuyerAgreementCAPF_BuyerAgreementTableGUID);
				SetModelByQSumBuyerAgreementLineCAPF(model, qSumBuyerAgreementLineCAPF);
				decimal qSumCreditAppRequestLineCAPF = GetQSumCreditAppRequestLineCAPF(refGUID);
				SetModelByGetQSumCreditAppRequestLineCAPF(model, qSumCreditAppRequestLineCAPF);
				
				List<QCreditAppReqLineFin> qCreditAppReqLineFin = GetQCreditAppReqLineFinCAPF(qCreditAppRequestLineCAPF.QCreditAppRequestLineCAPF_CreditAppRequestLineGUID);
				//-----------------------------------------------------------------------------------------------------------------------
				model.QSum10BuyerAgreementLine_SumBuyerAgreementAmount = GetQSum10BuyerAgreementLineCAPF(qBuyerAgreementCAPF.QBuyerAgreementCAPF_BuyerAgreementTableGUID);
				model.QCreditAppRequestLine_BusinessSegmentDesc = qBuyerTableCAPF.QBuyerTableCAPF_BusinessSegmentDesc != null ? qBuyerTableCAPF.QBuyerTableCAPF_BusinessSegmentDesc : "-";
				model.QCreditAppRequestLine_BuyerName = qBuyerTableCAPF.QBuyerTableCAPF_BuyerName != null ? qBuyerTableCAPF.QBuyerTableCAPF_BuyerName : "-" ;
				model.QCreditAppRequestLine_InvoiceAddress = qCreditAppRequestLineCAPF.QCreditAppRequestLineCAPF_InvoiceAddress != null ? qCreditAppRequestLineCAPF.QCreditAppRequestLineCAPF_InvoiceAddress : "-";
				model.QCreditAppRequestLine_LineOfBusinessDesc = qBuyerTableCAPF.QBuyerTableCAPF_LineOfBusinessDesc != null ? qBuyerTableCAPF.QBuyerTableCAPF_LineOfBusinessDesc  : "-" ;
				model.QCreditAppRequestLine_BlacklistStatusDesc = qCreditAppRequestLineCAPF.QCreditAppRequestLineCAPF_BlacklistStatus != null ? qCreditAppRequestLineCAPF.QCreditAppRequestLineCAPF_BlacklistStatus : "-";
				model.QCreditAppRequestLine_TaxId = qBuyerTableCAPF.QBuyerTableCAPF_TaxId != null ? qBuyerTableCAPF.QBuyerTableCAPF_TaxId : "-";
				model.QCreditAppRequestLine_DateOfEstablish = qBuyerTableCAPF.QCreditAppRequestLineCAPF_DateOfEstablish.DateToString() != null ? qBuyerTableCAPF.QCreditAppRequestLineCAPF_DateOfEstablish.DateToString() : "-";
				model.QCreditAppRequestLine_CreditScoringDesc = qCreditAppRequestLineCAPF.QCreditAppRequestLineCAPF_CreditScoring ;
				//-----------------------------------------------------------------
				model.QCreditAppReqLineFin1_Year = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].Year : 0;
				model.QCreditAppReqLineFin1_RegisteredCapital = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].RegisteredCapital : 0;
				model.QCreditAppReqLineFin1_PaidCapital = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].PaidCapital : 0;
				model.QCreditAppReqLineFin1_TotalAsset = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].TotalAsset : 0;
				model.QCreditAppReqLineFin1_TotalLiability = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].TotalLiability : 0;
				model.QCreditAppReqLineFin1_TotalEquity = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].TotalEquity : 0;
				model.QCreditAppReqLineFin1_TotalRevenue = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].TotalRevenue : 0;
				model.QCreditAppReqLineFin1_TotalCOGS = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].TotalCOGS : 0;
				model.QCreditAppReqLineFin1_TotalGrossProfit = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].TotalGrossProfit : 0;
				model.QCreditAppReqLineFin1_TotalOperExpFirst = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].TotalOperExpFirst : 0;
				model.QCreditAppReqLineFin1_TotalNetProfitFirst = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].TotalNetProfitFirst : 0;
				//-----------------------------------------------------------------
				model.QCreditAppReqLineFin2_Year = qCreditAppReqLineFin[1] != null ? qCreditAppReqLineFin[1].Year : 0;
				model.QCreditAppReqLineFin2_RegisteredCapital = qCreditAppReqLineFin[1] != null ? qCreditAppReqLineFin[1].RegisteredCapital : 0;
				model.QCreditAppReqLineFin2_PaidCapital = qCreditAppReqLineFin[1] != null ? qCreditAppReqLineFin[1].PaidCapital : 0;
				model.QCreditAppReqLineFin2_TotalAsset = qCreditAppReqLineFin[1] != null ? qCreditAppReqLineFin[1].TotalAsset : 0;
				model.QCreditAppReqLineFin2_TotalLiability = qCreditAppReqLineFin[1] != null ? qCreditAppReqLineFin[1].TotalLiability : 0;
				model.QCreditAppReqLineFin2_TotalEquity = qCreditAppReqLineFin[1] != null ? qCreditAppReqLineFin[1].TotalEquity : 0;
				model.QCreditAppReqLineFin2_TotalRevenue = qCreditAppReqLineFin[1] != null ? qCreditAppReqLineFin[1].TotalRevenue : 0;
				model.QCreditAppReqLineFin2_TotalCOGS = qCreditAppReqLineFin[1] != null ? qCreditAppReqLineFin[1].TotalCOGS : 0;
				model.QCreditAppReqLineFin2_TotalGrossProfit = qCreditAppReqLineFin[1] != null ? qCreditAppReqLineFin[1].TotalGrossProfit : 0;
				model.QCreditAppReqLineFin2_TotalOperExpFirst = qCreditAppReqLineFin[1] != null ? qCreditAppReqLineFin[1].TotalOperExpFirst : 0;
				model.QCreditAppReqLineFin2_TotalNetProfitFirst = qCreditAppReqLineFin[1] != null ? qCreditAppReqLineFin[1].TotalNetProfitFirst : 0;
				//-----------------------------------------------------------------
				model.QCreditAppReqLineFin3_Year = qCreditAppReqLineFin[2] != null ? qCreditAppReqLineFin[2].Year : 0;
				model.QCreditAppReqLineFin3_RegisteredCapital = qCreditAppReqLineFin[2] != null ? qCreditAppReqLineFin[2].RegisteredCapital : 0;
				model.QCreditAppReqLineFin3_PaidCapital = qCreditAppReqLineFin[2] != null ? qCreditAppReqLineFin[2].PaidCapital : 0;
				model.QCreditAppReqLineFin3_TotalAsset = qCreditAppReqLineFin[2] != null ? qCreditAppReqLineFin[2].TotalAsset : 0;
				model.QCreditAppReqLineFin3_TotalLiability = qCreditAppReqLineFin[2] != null ? qCreditAppReqLineFin[2].TotalLiability : 0;
				model.QCreditAppReqLineFin3_TotalEquity = qCreditAppReqLineFin[2] != null ? qCreditAppReqLineFin[2].TotalEquity : 0;
				model.QCreditAppReqLineFin3_TotalRevenue = qCreditAppReqLineFin[2] != null ? qCreditAppReqLineFin[2].TotalRevenue : 0;
				model.QCreditAppReqLineFin3_TotalCOGS = qCreditAppReqLineFin[2] != null ? qCreditAppReqLineFin[2].TotalCOGS : 0;
				model.QCreditAppReqLineFin3_TotalGrossProfit = qCreditAppReqLineFin[2] != null ? qCreditAppReqLineFin[2].TotalGrossProfit : 0;
				model.QCreditAppReqLineFin3_TotalOperExpFirst = qCreditAppReqLineFin[2] != null ? qCreditAppReqLineFin[2].TotalOperExpFirst : 0;
				model.QCreditAppReqLineFin3_TotalNetProfitFirst = qCreditAppReqLineFin[2] != null ? qCreditAppReqLineFin[2].TotalNetProfitFirst : 0;
				model.QSumCreditAppRequestLine_SumCreditLimitLineRequest = qSumCreditAppRequestLineCAPF;
				model.QCreditAppReqLineFin1_NetProfitPercent = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].NetProfitPercent : 0;
				model.QCreditAppReqLineFin1_lDE = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].lDE : 0;
				model.QCreditAppReqLineFin1_QuickRatio = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].QuickRatio : 0;
				model.QCreditAppReqLineFin1_IntCoverageRatio = qCreditAppReqLineFin[0] != null ? qCreditAppReqLineFin[0].IntCoverageRatio : 0;
				model.QCreditAppRequestTableCAPF_NumberOfDays = qCreditAppRequestTableCAPF.QCreditAppRequestTableCAPF_NumberOfDays;
				return model;
			}

			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}


		private void SetModelByGetQSumCreditAppRequestLineCAPF(BookmarkDocumentQueryCreditApplicationPF model, decimal qSumCreditAppRequestLineCAPF)
		{
			try
			{
				model.QSumCreditAppRequestLine_SumCreditLimitLineRequest = qSumCreditAppRequestLineCAPF;
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQSumBuyerAgreementLineCAPF(BookmarkDocumentQueryCreditApplicationPF model, decimal qSumBuyerAgreementLineCAPF)
		{
			try
			{
				model.QSumBuyerAgreementLineCAPF_SumBuyerAgreementAmount = qSumBuyerAgreementLineCAPF;

			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetModelByQBuyerAgreementLineCAPF_20(BookmarkDocumentQueryCreditApplicationPF model, QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_20)
		{
			try
			{
				model.QBuyerAgreementLineCAPF_Period_20 = qBuyerAgreementLineCAPF_20.QBuyerAgreementLineCAPF_Period;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_20 = qBuyerAgreementLineCAPF_20.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_20 = qBuyerAgreementLineCAPF_20.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_20 = qBuyerAgreementLineCAPF_20.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate;
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQBuyerAgreementLineCAPF_19(BookmarkDocumentQueryCreditApplicationPF model, QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_19)
		{
			try
			{
				model.QBuyerAgreementLineCAPF_Period_19 = qBuyerAgreementLineCAPF_19.QBuyerAgreementLineCAPF_Period;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_19 = qBuyerAgreementLineCAPF_19.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_19 = qBuyerAgreementLineCAPF_19.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_19 = qBuyerAgreementLineCAPF_19.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate;
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQBuyerAgreementLineCAPF_18(BookmarkDocumentQueryCreditApplicationPF model, QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_18)
		{
			try
			{
				model.QBuyerAgreementLineCAPF_Period_18 = qBuyerAgreementLineCAPF_18.QBuyerAgreementLineCAPF_Period;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_18 = qBuyerAgreementLineCAPF_18.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_18 = qBuyerAgreementLineCAPF_18.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_18 = qBuyerAgreementLineCAPF_18.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate;
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQBuyerAgreementLineCAPF_17(BookmarkDocumentQueryCreditApplicationPF model, QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_17)
		{
			try
			{
				model.QBuyerAgreementLineCAPF_Period_17 = qBuyerAgreementLineCAPF_17.QBuyerAgreementLineCAPF_Period;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_17 = qBuyerAgreementLineCAPF_17.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_17 = qBuyerAgreementLineCAPF_17.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_17 = qBuyerAgreementLineCAPF_17.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate;
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQBuyerAgreementLineCAPF_16(BookmarkDocumentQueryCreditApplicationPF model, QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_16)
		{
			try
			{
				model.QBuyerAgreementLineCAPF_Period_16 = qBuyerAgreementLineCAPF_16.QBuyerAgreementLineCAPF_Period;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_16 = qBuyerAgreementLineCAPF_16.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_16 = qBuyerAgreementLineCAPF_16.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_16 = qBuyerAgreementLineCAPF_16.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate;
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQBuyerAgreementLineCAPF_15(BookmarkDocumentQueryCreditApplicationPF model, QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_15)
		{
			try
			{
				model.QBuyerAgreementLineCAPF_Period_15 = qBuyerAgreementLineCAPF_15.QBuyerAgreementLineCAPF_Period;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_15 = qBuyerAgreementLineCAPF_15.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_15 = qBuyerAgreementLineCAPF_15.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_15 = qBuyerAgreementLineCAPF_15.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate;
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQBuyerAgreementLineCAPF_14(BookmarkDocumentQueryCreditApplicationPF model, QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_14)
		{
			try
			{
				model.QBuyerAgreementLineCAPF_Period_14 = qBuyerAgreementLineCAPF_14.QBuyerAgreementLineCAPF_Period;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_14 = qBuyerAgreementLineCAPF_14.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_14 = qBuyerAgreementLineCAPF_14.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_14 = qBuyerAgreementLineCAPF_14.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate;
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQBuyerAgreementLineCAPF_13(BookmarkDocumentQueryCreditApplicationPF model, QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_13)
		{
			try
			{
				model.QBuyerAgreementLineCAPF_Period_13 = qBuyerAgreementLineCAPF_13.QBuyerAgreementLineCAPF_Period;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_13 = qBuyerAgreementLineCAPF_13.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_13 = qBuyerAgreementLineCAPF_13.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_13 = qBuyerAgreementLineCAPF_13.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate;
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQBuyerAgreementLineCAPF_12(BookmarkDocumentQueryCreditApplicationPF model, QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_12)
		{
			try
			{
				model.QBuyerAgreementLineCAPF_Period_12 = qBuyerAgreementLineCAPF_12.QBuyerAgreementLineCAPF_Period;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_12 = qBuyerAgreementLineCAPF_12.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_12 = qBuyerAgreementLineCAPF_12.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_12 = qBuyerAgreementLineCAPF_12.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate;
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQBuyerAgreementLineCAPF_11(BookmarkDocumentQueryCreditApplicationPF model, QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_11)
		{
			try
			{
				model.QBuyerAgreementLineCAPF_Period_11 = qBuyerAgreementLineCAPF_11.QBuyerAgreementLineCAPF_Period;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_11 = qBuyerAgreementLineCAPF_11.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_11 = qBuyerAgreementLineCAPF_11.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_11 = qBuyerAgreementLineCAPF_11.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate;
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQBuyerAgreementLineCAPF_10(BookmarkDocumentQueryCreditApplicationPF model, QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_10)
		{
			try
			{
				model.QBuyerAgreementLineCAPF_Period_10 = qBuyerAgreementLineCAPF_10.QBuyerAgreementLineCAPF_Period;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_10 = qBuyerAgreementLineCAPF_10.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_10 = qBuyerAgreementLineCAPF_10.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_10 = qBuyerAgreementLineCAPF_10.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate;
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQBuyerAgreementLineCAPF_9(BookmarkDocumentQueryCreditApplicationPF model, QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_9)
		{
			try
			{
				model.QBuyerAgreementLineCAPF_Period_9 = qBuyerAgreementLineCAPF_9.QBuyerAgreementLineCAPF_Period;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_9 = qBuyerAgreementLineCAPF_9.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_9 = qBuyerAgreementLineCAPF_9.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_9 = qBuyerAgreementLineCAPF_9.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate;
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQBuyerAgreementLineCAPF_8(BookmarkDocumentQueryCreditApplicationPF model, QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_8)
		{
			try
			{
				model.QBuyerAgreementLineCAPF_Period_8 = qBuyerAgreementLineCAPF_8.QBuyerAgreementLineCAPF_Period;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_8 = qBuyerAgreementLineCAPF_8.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_8 = qBuyerAgreementLineCAPF_8.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_8 = qBuyerAgreementLineCAPF_8.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate;
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQBuyerAgreementLineCAPF_7(BookmarkDocumentQueryCreditApplicationPF model, QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_7)
		{
			try
			{
				model.QBuyerAgreementLineCAPF_Period_7 = qBuyerAgreementLineCAPF_7.QBuyerAgreementLineCAPF_Period;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_7 = qBuyerAgreementLineCAPF_7.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_7 = qBuyerAgreementLineCAPF_7.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_7 = qBuyerAgreementLineCAPF_7.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate;
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQBuyerAgreementLineCAPF_6(BookmarkDocumentQueryCreditApplicationPF model, QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_6)
		{
			try
			{
				model.QBuyerAgreementLineCAPF_Period_6 = qBuyerAgreementLineCAPF_6.QBuyerAgreementLineCAPF_Period;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_6 = qBuyerAgreementLineCAPF_6.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_6 = qBuyerAgreementLineCAPF_6.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_6 = qBuyerAgreementLineCAPF_6.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate;
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQBuyerAgreementLineCAPF_5(BookmarkDocumentQueryCreditApplicationPF model, QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_5)
		{
			try
			{
				model.QBuyerAgreementLineCAPF_Period_5 = qBuyerAgreementLineCAPF_5.QBuyerAgreementLineCAPF_Period;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_5 = qBuyerAgreementLineCAPF_5.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_5 = qBuyerAgreementLineCAPF_5.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_5 = qBuyerAgreementLineCAPF_5.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate;
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQBuyerAgreementLineCAPF_4(BookmarkDocumentQueryCreditApplicationPF model, QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_4)
		{
			try
			{
				model.QBuyerAgreementLineCAPF_Period_4 = qBuyerAgreementLineCAPF_4.QBuyerAgreementLineCAPF_Period;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_4 = qBuyerAgreementLineCAPF_4.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_4 = qBuyerAgreementLineCAPF_4.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_4 = qBuyerAgreementLineCAPF_4.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate;
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQBuyerAgreementLineCAPF_3(BookmarkDocumentQueryCreditApplicationPF model, QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_3)
		{
			try
			{
				model.QBuyerAgreementLineCAPF_Period_3 = qBuyerAgreementLineCAPF_3.QBuyerAgreementLineCAPF_Period;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_3 = qBuyerAgreementLineCAPF_3.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_3 = qBuyerAgreementLineCAPF_3.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_3 = qBuyerAgreementLineCAPF_3.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate;
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQBuyerAgreementLineCAPF_2(BookmarkDocumentQueryCreditApplicationPF model, QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_2)
		{
			try
			{
				model.QBuyerAgreementLineCAPF_Period_2 = qBuyerAgreementLineCAPF_2.QBuyerAgreementLineCAPF_Period;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_2 = qBuyerAgreementLineCAPF_2.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_2 = qBuyerAgreementLineCAPF_2.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_2 = qBuyerAgreementLineCAPF_2.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate;
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetModelByQBuyerAgreementLineCAPF_1(BookmarkDocumentQueryCreditApplicationPF model, QBuyerAgreementLineCAPF qBuyerAgreementLineCAPF_1)
		{
			try
			{
				model.QBuyerAgreementLineCAPF_Period_1 = qBuyerAgreementLineCAPF_1.QBuyerAgreementLineCAPF_Period;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_1 = qBuyerAgreementLineCAPF_1.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_1 = qBuyerAgreementLineCAPF_1.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount;
				model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_1 = qBuyerAgreementLineCAPF_1.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate;
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetModelByQBuyerAgreementCAPF(BookmarkDocumentQueryCreditApplicationPF model, QBuyerAgreementCAPF qBuyerAgreementCAPF)
		{
			try
			{
				model.QBuyerAgreementCAPF_BuyerAgreementTableGUID = qBuyerAgreementCAPF.QBuyerAgreementCAPF_BuyerAgreementTableGUID;
				model.QBuyerAgreementCAPF_BuyerAgreementId = qBuyerAgreementCAPF.QBuyerAgreementCAPF_BuyerAgreementId;
				model.QBuyerAgreementCAPF_BuyerAgreement = qBuyerAgreementCAPF.QBuyerAgreementCAPF_BuyerAgreement;
				model.QBuyerAgreementCAPF_StartDate = qBuyerAgreementCAPF.QBuyerAgreementCAPF_StartDate;
				model.QBuyerAgreementCAPF_EndDate = qBuyerAgreementCAPF.QBuyerAgreementCAPF_EndDate;
				model.QBuyerAgreementCAPF_Remark = qBuyerAgreementCAPF.QBuyerAgreementCAPF_Remark;
				model.QBuyerAgreementCAPF_Penalty = qBuyerAgreementCAPF.QBuyerAgreementCAPF_Penalty;
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetModelByQBuyerTableCAPF(BookmarkDocumentQueryCreditApplicationPF model, QBuyerTableCAPF qBuyerTableCAPF)
		{
			try
			{
				model.QBuyerTableCAPF_BuyerName = qBuyerTableCAPF.QBuyerTableCAPF_BuyerName;
				model.QBuyerTableCAPF_BuyerId = qBuyerTableCAPF.QBuyerTableCAPF_BuyerId;
				model.QBuyerTableCAPF_TaxId = qBuyerTableCAPF.QBuyerTableCAPF_TaxId;
				model.QCreditAppRequestLineCAPF_DateOfEstablish = qBuyerTableCAPF.QCreditAppRequestLineCAPF_DateOfEstablish;
				model.QBuyerTableCAPF_BusinessSegmentDesc = qBuyerTableCAPF.QBuyerTableCAPF_BusinessSegmentDesc;
				model.QBuyerTableCAPF_LineOfBusinessDesc = qBuyerTableCAPF.QBuyerTableCAPF_LineOfBusinessDesc;

			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetmodelQCreditAppRequestLineCAPF(BookmarkDocumentQueryCreditApplicationPF model, QCreditAppRequestLineCAPF qCreditAppRequestLineCAPF)
		{
			try
			{
				model.QCreditAppRequestLineCAPF_CreditAppRequestLineGUID = qCreditAppRequestLineCAPF.QCreditAppRequestLineCAPF_CreditAppRequestLineGUID;
				model.QCreditAppRequestLineCAPF_BuyerTableGUID = qCreditAppRequestLineCAPF.QCreditAppRequestLineCAPF_BuyerTableGUID;
				model.QCreditAppRequestLineCAPF_InvoiceAddressGUID = qCreditAppRequestLineCAPF.QCreditAppRequestLineCAPF_InvoiceAddressGUID;
				model.QCreditAppRequestLineCAPF_CreditLimitLineRequest = qCreditAppRequestLineCAPF.QCreditAppRequestLineCAPF_CreditLimitLineRequest;
				model.QCreditAppRequestLineCAPF_InvoiceAddress = qCreditAppRequestLineCAPF.QCreditAppRequestLineCAPF_InvoiceAddress;
				model.QCreditAppRequestLineCAPF_CreditScoring = qCreditAppRequestLineCAPF.QCreditAppRequestLineCAPF_CreditScoring;
				model.QCreditAppRequestLineCAPF_BlacklistStatus = qCreditAppRequestLineCAPF.QCreditAppRequestLineCAPF_BlacklistStatus;

			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetmodelByQCreditAppRequestTableCAPF(BookmarkDocumentQueryCreditApplicationPF model, QCreditAppRequestTableCAPF qCreditAppRequestTableCAPF)
		{
			try
			{
				model.QCreditAppRequestTableCAPF_CreditTermId = qCreditAppRequestTableCAPF.QCreditAppRequestTableCAPF_CreditTermId;
				model.QCreditAppRequestTableCAPF_CreditAppRequestId = qCreditAppRequestTableCAPF.QCreditAppRequestTableCAPF_CreditAppRequestId;
				model.QCreditAppRequestTableCAPF_SalesAvgPerMonth = qCreditAppRequestTableCAPF.QCreditAppRequestTableCAPF_SalesAvgPerMonth;
				model.QCreditAppRequestTableCAPF_CreditLimitRequest = qCreditAppRequestTableCAPF.QCreditAppRequestTableCAPF_CreditLimitRequest;
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private QBuyerAgreementLineCAPF GetQBuyerAgreementLineCAPFEachRow(List<QBuyerAgreementLineCAPF> qBuyerAgreementLineCAPF, int row_number)
		{
			try
			{
				var result = qBuyerAgreementLineCAPF.Where(w => w.QBuyerAgreementLineCAPF_Row == row_number).FirstOrDefault();
				if (result != null)
				{
					return result;
				}
				else
				{
					return new QBuyerAgreementLineCAPF();

				}
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private QCreditAppRequestTableCAPF GetQCreditAppRequestTableCAPF(Guid refGUID)
		{
			try
			{
				var result = (from creditAppRequestTable in db.Set<CreditAppRequestTable>()
							  join creditTerm in db.Set<CreditTerm>()
							  on creditAppRequestTable.CreditTermGUID equals creditTerm.CreditTermGUID into ljcreditTerm
							  from creditTerm in ljcreditTerm.DefaultIfEmpty()
							  where creditAppRequestTable.CreditAppRequestTableGUID == refGUID
							  select new QCreditAppRequestTableCAPF
							  {
								  QCreditAppRequestTableCAPF_CreditTermId = creditTerm.CreditTermId,
								  QCreditAppRequestTableCAPF_CreditAppRequestId = creditAppRequestTable.CreditAppRequestId,
								  QCreditAppRequestTableCAPF_SalesAvgPerMonth = creditAppRequestTable.SalesAvgPerMonth,
								  QCreditAppRequestTableCAPF_CreditLimitRequest = creditAppRequestTable.CreditLimitRequest,
								  QCreditAppRequestTableCAPF_NumberOfDays = creditTerm.NumberOfDays.ToString(),
							  }

							  ).FirstOrDefault();
				if (result != null)
				{
					return result;

				}
				else
				{
					return new QCreditAppRequestTableCAPF();

				}

			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private QCreditAppRequestLineCAPF GetQCreditAppRequestLineCAPF(Guid refGUID)
		{
			try
			{
				var result = (from creditAppRequestLine in db.Set<CreditAppRequestLine>()

							  join addressTrans in db.Set<AddressTrans>()
							  on creditAppRequestLine.InvoiceAddressGUID equals addressTrans.AddressTransGUID into ljaddressTrans
							  from addressTrans in ljaddressTrans.DefaultIfEmpty()

							  join creditScoring in db.Set<CreditScoring>()
							  on creditAppRequestLine.CreditScoringGUID equals creditScoring.CreditScoringGUID into ljcreditScoring
							  from creditScoring in ljcreditScoring.DefaultIfEmpty()

                              join blacklistStatus in db.Set<BlacklistStatus>()
                              on creditAppRequestLine.BlacklistStatusGUID equals blacklistStatus.BlacklistStatusGUID into ljblacklistStatus
                              from blacklistStatus in ljblacklistStatus.DefaultIfEmpty()

                              join assignmentMethod in db.Set<AssignmentMethod>()
							  on creditAppRequestLine.AssignmentMethodGUID equals assignmentMethod.AssignmentMethodGUID into ljassignmentMethod
							  from assignmentMethod in ljassignmentMethod.DefaultIfEmpty()

							  where creditAppRequestLine.CreditAppRequestTableGUID == refGUID 
							  orderby creditAppRequestLine.LineNum ascending

							  select new QCreditAppRequestLineCAPF
							  {
								  QCreditAppRequestLineCAPF_CreditAppRequestLineGUID = creditAppRequestLine.CreditAppRequestLineGUID,
								  QCreditAppRequestLineCAPF_BuyerTableGUID = creditAppRequestLine.BuyerTableGUID,
								  QCreditAppRequestLineCAPF_InvoiceAddressGUID = creditAppRequestLine.InvoiceAddressGUID,
								  QCreditAppRequestLineCAPF_CreditLimitLineRequest = creditAppRequestLine.CreditLimitLineRequest,
								  QCreditAppRequestLineCAPF_PaymentCondition = creditAppRequestLine.PaymentCondition,
								  QCreditAppRequestLineCAPF_AssignmentMethodGUID = creditAppRequestLine.AssignmentMethodGUID,
								  QCreditAppRequestLineCAPF_AssignmentMethodDesc = assignmentMethod.Description,
								  QCreditAppRequestLineCAPF_InvoiceAddress = String.Concat(addressTrans.Address1, ' ', addressTrans.Address2),
								  QCreditAppRequestLineCAPF_CreditScoringGUID = creditScoring.CreditScoringGUID,
								  QCreditAppRequestLineCAPF_CreditScoring = creditScoring.Description,
								  QCreditAppRequestLineCAPF_BlacklistStatusGUID = blacklistStatus.BlacklistStatusGUID,
								  QCreditAppRequestLineCAPF_BlacklistStatus = blacklistStatus.Description,
								 
							  }
					).FirstOrDefault();
				if (result != null)
				{
					return result;

				}
				else
				{
					return new QCreditAppRequestLineCAPF();

				}
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private QBuyerTableCAPF GetQBuyerTableCAPF(Guid? qCreditAppRequestLineCAPF_BuyerTableGUID)
		{
			try
			{
				var result = (from buyerTable in db.Set<BuyerTable>()
							  join businessSegment in db.Set<BusinessSegment>()
							  on buyerTable.BusinessSegmentGUID equals businessSegment.BusinessSegmentGUID into ljbusinessSegment
							  from businessSegment in ljbusinessSegment.DefaultIfEmpty()

							  join lineOfBusiness in db.Set<LineOfBusiness>()
							  on buyerTable.LineOfBusinessGUID equals lineOfBusiness.LineOfBusinessGUID into ljlineOfBusiness
							  from lineOfBusiness in ljlineOfBusiness.DefaultIfEmpty()
							  where buyerTable.BuyerTableGUID == qCreditAppRequestLineCAPF_BuyerTableGUID
							  select new QBuyerTableCAPF
							  {
								  QBuyerTableCAPF_BuyerName = buyerTable.BuyerName,
								  QBuyerTableCAPF_BuyerId = buyerTable.BuyerId,
								  QBuyerTableCAPF_TaxId = buyerTable.TaxId,
								  QCreditAppRequestLineCAPF_DateOfEstablish = buyerTable.DateOfEstablish,
								  QBuyerTableCAPF_BusinessSegmentDesc = businessSegment.Description,
								  QBuyerTableCAPF_LineOfBusinessDesc = lineOfBusiness.Description
							  }
							 ).FirstOrDefault();
				if (result != null)
				{
					return result;
				}
				else
				{
					return new QBuyerTableCAPF();

				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private QBuyerAgreementCAPF GetQBuyerAgreementCAPF(Guid? qCreditAppRequestLineCAPF_CreditAppRequestLineGUID)
		{
			try
			{
				var result = (from buyerAgreementTrans in db.Set<BuyerAgreementTrans>()
							  join buyerAgreementTable in db.Set<BuyerAgreementTable>()
							  on buyerAgreementTrans.BuyerAgreementTableGUID equals buyerAgreementTable.BuyerAgreementTableGUID into ljbuyerAgreementTable
							  from buyerAgreementTable in ljbuyerAgreementTable.DefaultIfEmpty()
							  where buyerAgreementTrans.RefGUID == qCreditAppRequestLineCAPF_CreditAppRequestLineGUID
							  select new QBuyerAgreementCAPF
							  {

								  QBuyerAgreementCAPF_BuyerAgreementTableGUID = buyerAgreementTrans.BuyerAgreementTableGUID,
								  QBuyerAgreementCAPF_BuyerAgreementId = buyerAgreementTable.BuyerAgreementId,
								  QBuyerAgreementCAPF_BuyerAgreement = buyerAgreementTable.Description,
								  QBuyerAgreementCAPF_StartDate = buyerAgreementTable.StartDate,
								  QBuyerAgreementCAPF_EndDate = buyerAgreementTable.EndDate,
								  QBuyerAgreementCAPF_Remark = buyerAgreementTable.Remark,
								  QBuyerAgreementCAPF_Penalty = buyerAgreementTable.Penalty

							  }).FirstOrDefault();
				if (result != null)
				{
					return result;
				}
				else
				{
					return new QBuyerAgreementCAPF();

				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QBuyerAgreementLineCAPF> GetQBuyerAgreementLineCAPF(Guid? qBuyerAgreementCAPF_BuyerAgreementTableGUID)
		{

			try
			{
				var result = (from buyerAgreementLine in db.Set<BuyerAgreementLine>()
							  where buyerAgreementLine.BuyerAgreementTableGUID == qBuyerAgreementCAPF_BuyerAgreementTableGUID
							  orderby buyerAgreementLine.Period ascending
							  select new QBuyerAgreementLineCAPF
							  {
								  QBuyerAgreementLineCAPF_Period = buyerAgreementLine.Period,
								  QBuyerAgreementLineCAPF_BuyerAgreementLineDesc = buyerAgreementLine.Description,
								  QBuyerAgreementLineCAPF_BuyerAgreementLineAmount = buyerAgreementLine.Amount,
								  QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate = buyerAgreementLine.DueDate,
								  QBuyerAgreementLineCAPF_Row = 0
							  }).Take(20).ToList();
				var row_number = 0;
				foreach (var item in result)
				{
					item.QBuyerAgreementLineCAPF_Row = row_number + 1;
					row_number = row_number + 1;
				}
				if (result != null)
				{
					return result;
				}
				else
				{
					return new List<QBuyerAgreementLineCAPF>();

				}
			}

			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private decimal GetQSumBuyerAgreementLineCAPF(Guid? qBuyerAgreementCAPF_BuyerAgreementTableGUID)
		{
			try
			{
				var result = (from sumBuyerAgreementLine in db.Set<BuyerAgreementLine>()
							  where sumBuyerAgreementLine.BuyerAgreementTableGUID == qBuyerAgreementCAPF_BuyerAgreementTableGUID
							  select new QSumBuyerAgreementLineCAPF
							  {
								  QSumBuyerAgreementLineCAPF_SumBuyerAgreementAmount = sumBuyerAgreementLine.Amount
							  }).Sum(su => su.QSumBuyerAgreementLineCAPF_SumBuyerAgreementAmount);

				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private decimal GetQSum10BuyerAgreementLineCAPF(Guid? qBuyerAgreementCAPF_BuyerAgreementTableGUID)
		{
			try
			{
				var result = (from sumBuyerAgreementLine in db.Set<BuyerAgreementLine>()
							  where sumBuyerAgreementLine.BuyerAgreementTableGUID == qBuyerAgreementCAPF_BuyerAgreementTableGUID
							  select new QSumBuyerAgreementLineCAPF
							  {
								  QSumBuyerAgreementLineCAPF_SumBuyerAgreementAmount = sumBuyerAgreementLine.Amount
							  }).Take(10).Sum(su => su.QSumBuyerAgreementLineCAPF_SumBuyerAgreementAmount);

				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private decimal GetQSumCreditAppRequestLineCAPF(Guid? refGUID)
		{
			try
			{
				var result = (from sumCreditAppRequestLine in db.Set<CreditAppRequestLine>()
							  where sumCreditAppRequestLine.CreditAppRequestTableGUID == refGUID
							  select new QSumCreditAppRequestLineCAPF
							  {
								  QSumCreditAppRequestLine_SumCreditLimitLineRequest = sumCreditAppRequestLine.CreditLimitLineRequest
							  }).Sum(su => su.QSumCreditAppRequestLine_SumCreditLimitLineRequest);

				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<QCreditAppReqLineFin> GetQCreditAppReqLineFinCAPF(Guid? CreditAppRequestLineGUID)
        {
			try
			{
				int i = 1;
				int j = 0;
				List<QCreditAppReqLineFin> result = new List<QCreditAppReqLineFin>(new QCreditAppReqLineFin[3]);
				List<QCreditAppReqLineFin> tmp_result = (from financialStatementTrans in db.Set<FinancialStatementTrans>()
														 where financialStatementTrans.RefGUID == CreditAppRequestLineGUID && financialStatementTrans.RefType == (int)RefType.CreditAppRequestLine

														 select new QCreditAppReqLineFin
														 {
															 Year = financialStatementTrans.Year,
															 RegisteredCapital = (from financialStatementTrans1 in db.Set<FinancialStatementTrans>()
																				  where financialStatementTrans1.RefGUID == CreditAppRequestLineGUID && financialStatementTrans1.RefType == (int)RefType.CreditAppRequestLine && financialStatementTrans1.Year == financialStatementTrans.Year && financialStatementTrans1.Ordering == 1
																				  select financialStatementTrans1.Amount
																		).FirstOrDefault(),
															 PaidCapital = (from financialStatementTrans2 in db.Set<FinancialStatementTrans>()
																			where financialStatementTrans2.RefGUID == CreditAppRequestLineGUID && financialStatementTrans2.RefType == (int)RefType.CreditAppRequestLine && financialStatementTrans2.Year == financialStatementTrans.Year && financialStatementTrans2.Ordering == 2
																			select financialStatementTrans2.Amount).FirstOrDefault(),
															 TotalAsset = (from financialStatementTrans3 in db.Set<FinancialStatementTrans>()
																		   where financialStatementTrans3.RefGUID == CreditAppRequestLineGUID && financialStatementTrans3.RefType == (int)RefType.CreditAppRequestLine && financialStatementTrans3.Year == financialStatementTrans.Year && financialStatementTrans3.Ordering == 3
																		   select financialStatementTrans3.Amount).FirstOrDefault(),
															 TotalLiability = (from financialStatementTrans4 in db.Set<FinancialStatementTrans>()
																			   where financialStatementTrans4.RefGUID == CreditAppRequestLineGUID && financialStatementTrans4.RefType == (int)RefType.CreditAppRequestLine && financialStatementTrans4.Year == financialStatementTrans.Year && financialStatementTrans4.Ordering == 4
																			   select financialStatementTrans4.Amount).FirstOrDefault(),
															 TotalEquity = (from financialStatementTrans5 in db.Set<FinancialStatementTrans>()
																			where financialStatementTrans5.RefGUID == CreditAppRequestLineGUID && financialStatementTrans5.RefType == (int)RefType.CreditAppRequestLine && financialStatementTrans5.Year == financialStatementTrans.Year && financialStatementTrans5.Ordering == 5
																			select financialStatementTrans5.Amount).FirstOrDefault(),
															 TotalRevenue = (from financialStatementTrans6 in db.Set<FinancialStatementTrans>()
																			 where financialStatementTrans6.RefGUID == CreditAppRequestLineGUID && financialStatementTrans6.RefType == (int)RefType.CreditAppRequestLine && financialStatementTrans6.Year == financialStatementTrans.Year && financialStatementTrans6.Ordering == 6
																			 select financialStatementTrans6.Amount).FirstOrDefault(),
															 TotalCOGS = (from financialStatementTrans7 in db.Set<FinancialStatementTrans>()
																		  where financialStatementTrans7.RefGUID == CreditAppRequestLineGUID && financialStatementTrans7.RefType == (int)RefType.CreditAppRequestLine && financialStatementTrans7.Year == financialStatementTrans.Year && financialStatementTrans7.Ordering == 7
																		  select financialStatementTrans7.Amount).FirstOrDefault(),
															 TotalGrossProfit = (from financialStatementTrans8 in db.Set<FinancialStatementTrans>()
																				 where financialStatementTrans8.RefGUID == CreditAppRequestLineGUID && financialStatementTrans8.RefType == (int)RefType.CreditAppRequestLine && financialStatementTrans8.Year == financialStatementTrans.Year && financialStatementTrans8.Ordering == 8
																				 select financialStatementTrans8.Amount).FirstOrDefault(),
															 TotalOperExpFirst = (from financialStatementTrans9 in db.Set<FinancialStatementTrans>()
																				  where financialStatementTrans9.RefGUID == CreditAppRequestLineGUID && financialStatementTrans9.RefType == (int)RefType.CreditAppRequestLine && financialStatementTrans9.Year == financialStatementTrans.Year && financialStatementTrans9.Ordering == 9
																				  select financialStatementTrans9.Amount).FirstOrDefault(),
															 TotalNetProfitFirst = (from financialStatementTrans10 in db.Set<FinancialStatementTrans>()
																					where financialStatementTrans10.RefGUID == CreditAppRequestLineGUID && financialStatementTrans10.RefType == (int)RefType.CreditAppRequestLine && financialStatementTrans10.Year == financialStatementTrans.Year && financialStatementTrans10.Ordering == 10
																					select financialStatementTrans10.Amount).FirstOrDefault(),
															 NetProfitPercent = (from financialStatementTrans11 in db.Set<FinancialStatementTrans>()
																				 where financialStatementTrans11.RefGUID == CreditAppRequestLineGUID && financialStatementTrans11.RefType == (int)RefType.CreditAppRequestLine && financialStatementTrans11.Year == financialStatementTrans.Year && financialStatementTrans11.Ordering == 11
																				 select financialStatementTrans11.Amount).FirstOrDefault(),
															 lDE = (from financialStatementTrans12 in db.Set<FinancialStatementTrans>()
																	where financialStatementTrans12.RefGUID == CreditAppRequestLineGUID && financialStatementTrans12.RefType == (int)RefType.CreditAppRequestLine && financialStatementTrans12.Year == financialStatementTrans.Year && financialStatementTrans12.Ordering == 12
																	select financialStatementTrans12.Amount).FirstOrDefault(),
															 QuickRatio = (from financialStatementTrans13 in db.Set<FinancialStatementTrans>()
																		   where financialStatementTrans13.RefGUID == CreditAppRequestLineGUID && financialStatementTrans13.RefType == (int)RefType.CreditAppRequestLine && financialStatementTrans13.Year == financialStatementTrans.Year && financialStatementTrans13.Ordering == 13
																		   select financialStatementTrans13.Amount).FirstOrDefault(),
															 IntCoverageRatio = (from financialStatementTrans14 in db.Set<FinancialStatementTrans>()
																				 where financialStatementTrans14.RefGUID == CreditAppRequestLineGUID && financialStatementTrans14.RefType == (int)RefType.CreditAppRequestLine && financialStatementTrans14.Year == financialStatementTrans.Year && financialStatementTrans14.Ordering == 14
																				 select financialStatementTrans14.Amount).FirstOrDefault()
														 }).Distinct().Take(3).OrderByDescending(f => f.Year).AsNoTracking().ToList();
				foreach (var item in tmp_result)
				{
					result[j] = item;
					item.Row = i;
					i++;
					j++;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region QueryCustomerAmendInformation
		public BookmarkDocumentQueryCusotmerAmendInformation GetBookmarkDocumentQueryCustomerAmendInformation(Guid refGUID, Guid bookmarkDocumentTransGUID)
		{
			try
			{
				BookmarkDocumentQueryCusotmerAmendInformation model = new BookmarkDocumentQueryCusotmerAmendInformation();
				QCreditAppRequestTableCusAmnedInfo qCreditAppRequestTableCusAmnedInfo = GetQCreditAppRequestTableCusAmnedInfo(refGUID);
				SetmodelByQCreditAppRequestTableCusAmnedInfo(model, qCreditAppRequestTableCusAmnedInfo);
				QOriginalCreditAppTableCusAmendInfo qOriginalCreditAppTableCusAmendInfo = GetQOriginalCreditAppTableCusAmendInfo(qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_RefCreditAppTableGUID);
				SetmodelByQOriginalCreditAppTableCusAmendInfo(model, qOriginalCreditAppTableCusAmendInfo);
				QCustomerCusAmendInfo qCustomerCusAmendInfo = GetQCustomerCusAmendInfo(qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_CustomerTableGUID);
				SetmodelByQCustomerCusAmendInfo(model, qCustomerCusAmendInfo);
				QBankAccountControlCusAmendInfo qBankAccountControlCusAmendInfo = GetQBankAccountControlCusAmendInfo(qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_BankAccountControlGUID);
				SetmodelByQBankAccountControlCusAmendInfo(model, qBankAccountControlCusAmendInfo);
				QCreditAppRequestTableAmendCusAmendInfo qCreditAppRequestTableAmendCusAmendInfo = GetQCreditAppRequestTableAmendCusAmendInfo(refGUID);
				SetmodelByQCreditAppRequestTableAmendCusAmendInfo(model, qCreditAppRequestTableAmendCusAmendInfo);

				List<QRetentionConditionTransCusAmendInfo> qRetentionConditionTransCusAmendInfo = GetQRetentionConditionTransCusAmendInfo(qOriginalCreditAppTableCusAmendInfo.QOriginalCreditAppTableCusAmendInfo_CreditAppTableGUID);
				QRetentionConditionTransCusAmendInfo qRetentionConditionTransCusAmendInfo_1 = GetQRetentionConditionTransCusAmendInfoEachRow(qRetentionConditionTransCusAmendInfo, 1);
				SetModelByQRetentionConditionTransCusAmendInfo_1(model, qRetentionConditionTransCusAmendInfo_1);
				QRetentionConditionTransCusAmendInfo qRetentionConditionTransCusAmendInfo_2 = GetQRetentionConditionTransCusAmendInfoEachRow(qRetentionConditionTransCusAmendInfo, 2);
				SetModelByQRetentionConditionTransCusAmendInfo_2(model, qRetentionConditionTransCusAmendInfo_2);
				QRetentionConditionTransCusAmendInfo qRetentionConditionTransCusAmendInfo_3 = GetQRetentionConditionTransCusAmendInfoEachRow(qRetentionConditionTransCusAmendInfo, 3);
				SetModelByQRetentionConditionTransCusAmendInfo_3(model, qRetentionConditionTransCusAmendInfo_3);

				List<QAuthorizedCustCusAmendInfo> qAuthorizedCustCusAmendInfo = GetQAuthorizedCustCusAmendInfo(refGUID);
				QAuthorizedCustCusAmendInfo qAuthorizedCustCusAmendInfo_1 = GetQAuthorizedCustCusAmendInfoEachRow(qAuthorizedCustCusAmendInfo, 1);
				SetModelByQAuthorizedCustCusAmendInfo_1(model, qAuthorizedCustCusAmendInfo_1);
				QAuthorizedCustCusAmendInfo qAuthorizedCustCusAmendInfo_2 = GetQAuthorizedCustCusAmendInfoEachRow(qAuthorizedCustCusAmendInfo, 2);
				SetModelByQAuthorizedCustCusAmendInfo_2(model, qAuthorizedCustCusAmendInfo_2);
				QAuthorizedCustCusAmendInfo qAuthorizedCustCusAmendInfo_3 = GetQAuthorizedCustCusAmendInfoEachRow(qAuthorizedCustCusAmendInfo, 3);
				SetModelByQAuthorizedCustCusAmendInfo_3(model, qAuthorizedCustCusAmendInfo_3);
				QAuthorizedCustCusAmendInfo qAuthorizedCustCusAmendInfo_4 = GetQAuthorizedCustCusAmendInfoEachRow(qAuthorizedCustCusAmendInfo, 4);
				SetModelByQAuthorizedCustCusAmendInfo_4(model, qAuthorizedCustCusAmendInfo_4);
				QAuthorizedCustCusAmendInfo qAuthorizedCustCusAmendInfo_5 = GetQAuthorizedCustCusAmendInfoEachRow(qAuthorizedCustCusAmendInfo, 5);
				SetModelByQAuthorizedCustCusAmendInfo_5(model, qAuthorizedCustCusAmendInfo_5);
				QAuthorizedCustCusAmendInfo qAuthorizedCustCusAmendInfo_6 = GetQAuthorizedCustCusAmendInfoEachRow(qAuthorizedCustCusAmendInfo, 6);
				SetModelByQAuthorizedCustCusAmendInfo_6(model, qAuthorizedCustCusAmendInfo_6);

				// R02
				List<QAuthorizedCustOldCusAmendInfo> qAuthorizedCustOldCusAmendInfo = GetQAuthorizedCustOldCusAmendInfo(refGUID);
				QAuthorizedCustOldCusAmendInfo qAuthorizedCustOldCusAmendInfo_1 = GetQAuthorizedCustOldCusAmendInfoEachRow(qAuthorizedCustOldCusAmendInfo, 1);
				SetModelByQAuthorizedCustOldCusAmendInfo_1(model, qAuthorizedCustOldCusAmendInfo_1);
				QAuthorizedCustOldCusAmendInfo qAuthorizedCustOldCusAmendInfo_2 = GetQAuthorizedCustOldCusAmendInfoEachRow(qAuthorizedCustOldCusAmendInfo, 2);
				SetModelByQAuthorizedCustOldCusAmendInfo_2(model, qAuthorizedCustOldCusAmendInfo_2);
				QAuthorizedCustOldCusAmendInfo qAuthorizedCustOldCusAmendInfo_3 = GetQAuthorizedCustOldCusAmendInfoEachRow(qAuthorizedCustOldCusAmendInfo, 3);
				SetModelByQAuthorizedCustOldCusAmendInfo_3(model, qAuthorizedCustOldCusAmendInfo_3);
				QAuthorizedCustOldCusAmendInfo qAuthorizedCustOldCusAmendInfo_4 = GetQAuthorizedCustOldCusAmendInfoEachRow(qAuthorizedCustOldCusAmendInfo, 4);
				SetModelByQAuthorizedCustOldCusAmendInfo_4(model, qAuthorizedCustOldCusAmendInfo_4);
				QAuthorizedCustOldCusAmendInfo qAuthorizedCustOldCusAmendInfo_5 = GetQAuthorizedCustOldCusAmendInfoEachRow(qAuthorizedCustOldCusAmendInfo, 5);
				SetModelByQAuthorizedCustOldCusAmendInfo_5(model, qAuthorizedCustOldCusAmendInfo_5);
				QAuthorizedCustOldCusAmendInfo qAuthorizedCustOldCusAmendInfo_6 = GetQAuthorizedCustOldCusAmendInfoEachRow(qAuthorizedCustOldCusAmendInfo, 6);
				SetModelByQAuthorizedCustOldCusAmendInfo_6(model, qAuthorizedCustOldCusAmendInfo_6);

				List<QGuarantorNewCusAmendInfo> qGuarantorNewCusAmendInfo = GetQGuarantorNewCusAmendInfo(refGUID);
				QGuarantorNewCusAmendInfo qGuarantorNewCusAmendInfo_1 = GetQGuarantorNewCusAmendInfoEachRow(qGuarantorNewCusAmendInfo, 1);
				SetModelByQGuarantorNewCusAmendInfo_1(model, qGuarantorNewCusAmendInfo_1);
				QGuarantorNewCusAmendInfo qGuarantorNewCusAmendInfo_2 = GetQGuarantorNewCusAmendInfoEachRow(qGuarantorNewCusAmendInfo, 2);
				SetModelByQGuarantorNewCusAmendInfo_2(model, qGuarantorNewCusAmendInfo_2);
				QGuarantorNewCusAmendInfo qGuarantorNewCusAmendInfo_3 = GetQGuarantorNewCusAmendInfoEachRow(qGuarantorNewCusAmendInfo, 3);
				SetModelByQGuarantorNewCusAmendInfo_3(model, qGuarantorNewCusAmendInfo_3);
				QGuarantorNewCusAmendInfo qGuarantorNewCusAmendInfo_4 = GetQGuarantorNewCusAmendInfoEachRow(qGuarantorNewCusAmendInfo, 4);
				SetModelByQGuarantorNewCusAmendInfo_4(model, qGuarantorNewCusAmendInfo_4);
				QGuarantorNewCusAmendInfo qGuarantorNewCusAmendInfo_5 = GetQGuarantorNewCusAmendInfoEachRow(qGuarantorNewCusAmendInfo, 5);
				SetModelByQGuarantorNewCusAmendInfo_5(model, qGuarantorNewCusAmendInfo_5);
				QGuarantorNewCusAmendInfo qGuarantorNewCusAmendInfo_6 = GetQGuarantorNewCusAmendInfoEachRow(qGuarantorNewCusAmendInfo, 6);
				SetModelByQGuarantorNewCusAmendInfo_6(model, qGuarantorNewCusAmendInfo_6);

				List<QGuarantorOldCusAmendInfo> qGuarantorOldCusAmendInfo = GetQGuarantorOldCusAmendInfo(refGUID);
				QGuarantorOldCusAmendInfo qGuarantorOldCusAmendInfo_1 = GetQGuarantorOldCusAmendInfoEachRow(qGuarantorOldCusAmendInfo, 1);
				SetModelByQGuarantorOldCusAmendInfo_1(model, qGuarantorOldCusAmendInfo_1);
				QGuarantorOldCusAmendInfo qGuarantorOldCusAmendInfo_2 = GetQGuarantorOldCusAmendInfoEachRow(qGuarantorOldCusAmendInfo, 2);
				SetModelByQGuarantorOldCusAmendInfo_2(model, qGuarantorOldCusAmendInfo_2);
				QGuarantorOldCusAmendInfo qGuarantorOldCusAmendInfo_3 = GetQGuarantorOldCusAmendInfoEachRow(qGuarantorOldCusAmendInfo, 3);
				SetModelByQGuarantorOldCusAmendInfo_3(model, qGuarantorOldCusAmendInfo_3);
				QGuarantorOldCusAmendInfo qGuarantorOldCusAmendInfo_4 = GetQGuarantorOldCusAmendInfoEachRow(qGuarantorOldCusAmendInfo, 4);
				SetModelByQGuarantorOldCusAmendInfo_4(model, qGuarantorOldCusAmendInfo_4);
				QGuarantorOldCusAmendInfo qGuarantorOldCusAmendInfo_5 = GetQGuarantorOldCusAmendInfoEachRow(qGuarantorOldCusAmendInfo, 5);
				SetModelByQGuarantorOldCusAmendInfo_5(model, qGuarantorOldCusAmendInfo_5);
				QGuarantorOldCusAmendInfo qGuarantorOldCusAmendInfo_6 = GetQGuarantorOldCusAmendInfoEachRow(qGuarantorOldCusAmendInfo, 6);
				SetModelByQGuarantorOldCusAmendInfo_6(model, qGuarantorOldCusAmendInfo_6);

				List<QActionHistoryCusAmnedInfo> qActionHistoryCusAmnedInfo = GetQActionHistoryCusAmnedInfo(refGUID);
				QActionHistoryCusAmnedInfo qActionHistoryCusAmnedInfo_1 = GetQActionHistoryCusAmnedInfoEachRow(qActionHistoryCusAmnedInfo, 1);
				SetModelByQActionHistoryCusAmnedInfo_1(model, qActionHistoryCusAmnedInfo_1);
				QActionHistoryCusAmnedInfo qActionHistoryCusAmnedInfo_2 = GetQActionHistoryCusAmnedInfoEachRow(qActionHistoryCusAmnedInfo, 2);
				SetModelByQActionHistoryCusAmnedInfo_2(model, qActionHistoryCusAmnedInfo_2);
				QActionHistoryCusAmnedInfo qActionHistoryCusAmnedInfo_3 = GetQActionHistoryCusAmnedInfoEachRow(qActionHistoryCusAmnedInfo, 3);
				SetModelByQActionHistoryCusAmnedInfo_3(model, qActionHistoryCusAmnedInfo_3);
				QActionHistoryCusAmnedInfo qActionHistoryCusAmnedInfo_4 = GetQActionHistoryCusAmnedInfoEachRow(qActionHistoryCusAmnedInfo, 4);
				SetModelByQActionHistoryCusAmnedInfo_4(model, qActionHistoryCusAmnedInfo_4);
				QActionHistoryCusAmnedInfo qActionHistoryCusAmnedInfo_5 = GetQActionHistoryCusAmnedInfoEachRow(qActionHistoryCusAmnedInfo, 5);
				SetModelByQActionHistoryCusAmnedInfo_5(model, qActionHistoryCusAmnedInfo_5);
				SetModelByVariableCustCompanyRegistrationID(model, qCustomerCusAmendInfo);
				SetModelByVariableCustComEstablishedDate(model, qCustomerCusAmendInfo);
				// R02
				SetModelByVariableInterestValue(model, qCreditAppRequestTableCusAmnedInfo);
				SetModelByVariableNewInterestRate(model, qCreditAppRequestTableCusAmnedInfo);

				return model;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetModelByVariableNewInterestRate(BookmarkDocumentQueryCusotmerAmendInformation model, QCreditAppRequestTableCusAmnedInfo qCreditAppRequestTableCusAmnedInfo)
		{
			try
			{
				model.BookmarkDocumentQueryCusotmerAmendInformation_NewInterestRate = model.QCreditAppRequestTableCusAmnedInfo_InterestValue + qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableAmendCusAmendInfo_InterestAdjustment;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetModelByVariableCustComEstablishedDate(BookmarkDocumentQueryCusotmerAmendInformation model, QCustomerCusAmendInfo qCustomerCusAmendInfo)
		{
			try
			{
				if (qCustomerCusAmendInfo.QCustomerCusAmendInfo_RecordType == (int)RecordType.Person)
				{
					model.BookmarkDocumentQueryCusotmerAmendInformation_CustComEstablishedDate = qCustomerCusAmendInfo.QCustomerCusAmendInfo_DateOfBirth;
				}
				else if (qCustomerCusAmendInfo.QCustomerCusAmendInfo_RecordType == (int)RecordType.Organization)
				{
					model.BookmarkDocumentQueryCusotmerAmendInformation_CustComEstablishedDate = qCustomerCusAmendInfo.QCustomerCusAmendInfo_DateOfEstablish;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetModelByVariableCustCompanyRegistrationID(BookmarkDocumentQueryCusotmerAmendInformation model, QCustomerCusAmendInfo qCustomerCusAmendInfo)
		{

			try
			{

				if (qCustomerCusAmendInfo.QCustomerCusAmendInfo_IdentificationType == (int)IdentificationType.TaxId)
				{
					model.BookmarkDocumentQueryCusotmerAmendInformation_CustCompanyRegistrationID = qCustomerCusAmendInfo.QCustomerCusAmendInfo_TaxID;
				}
				else if (qCustomerCusAmendInfo.QCustomerCusAmendInfo_IdentificationType == (int)IdentificationType.PassportId)
				{
					model.BookmarkDocumentQueryCusotmerAmendInformation_CustCompanyRegistrationID = qCustomerCusAmendInfo.QCustomerCusAmendInfo_PassportID;

				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetModelByVariableInterestValue(BookmarkDocumentQueryCusotmerAmendInformation model, QCreditAppRequestTableCusAmnedInfo qCreditAppRequestTable)
		{

			try
			{
				IInterestTypeService interestTypeService = new InterestTypeService(db);
				decimal interestValue = 0;
				if(qCreditAppRequestTable.QCreditAppRequestTableCusAmnedInfo_InterestTypeGUID != null && qCreditAppRequestTable.QCreditAppRequestTableCusAmnedInfo_RequestDate != null)
                {
					interestValue = interestTypeService.GetInterestTypeValue(qCreditAppRequestTable.QCreditAppRequestTableCusAmnedInfo_InterestTypeGUID.Value, qCreditAppRequestTable.QCreditAppRequestTableCusAmnedInfo_RequestDate.Value);
				}

				model.QCreditAppRequestTableCusAmnedInfo_InterestValue = interestValue;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetModelByQActionHistoryCusAmnedInfo_5(BookmarkDocumentQueryCusotmerAmendInformation model, QActionHistoryCusAmnedInfo qActionHistoryCusAmnedInfo_5)
		{
			try
			{
				if(qActionHistoryCusAmnedInfo_5.QActionHistoryCusAmnedInfo_Row != 0)
                {
					model.QActionHistoryCusAmnedInfo_Comment_5 = qActionHistoryCusAmnedInfo_5.QActionHistoryCusAmnedInfo_Comment;
					model.QActionHistoryCusAmnedInfo_CreatedDateTime_5 = qActionHistoryCusAmnedInfo_5.QActionHistoryCusAmnedInfo_CreatedDateTime.DateToString();
					model.QActionHistoryCusAmnedInfo_ActionName_5 = qActionHistoryCusAmnedInfo_5.QActionHistoryCusAmnedInfo_ActionName;
					model.QActionHistoryCusAmnedInfo_ApproverName_5 = qActionHistoryCusAmnedInfo_5.QActionHistoryCusAmnedInfo_ApproverName;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQActionHistoryCusAmnedInfo_4(BookmarkDocumentQueryCusotmerAmendInformation model, QActionHistoryCusAmnedInfo qActionHistoryCusAmnedInfo_4)
		{
			try
			{
				if (qActionHistoryCusAmnedInfo_4.QActionHistoryCusAmnedInfo_Row != 0)
                {
					model.QActionHistoryCusAmnedInfo_Comment_4 = qActionHistoryCusAmnedInfo_4.QActionHistoryCusAmnedInfo_Comment;
					model.QActionHistoryCusAmnedInfo_CreatedDateTime_4 = qActionHistoryCusAmnedInfo_4.QActionHistoryCusAmnedInfo_CreatedDateTime.DateToString();
					model.QActionHistoryCusAmnedInfo_ActionName_4 = qActionHistoryCusAmnedInfo_4.QActionHistoryCusAmnedInfo_ActionName;
					model.QActionHistoryCusAmnedInfo_ApproverName_4 = qActionHistoryCusAmnedInfo_4.QActionHistoryCusAmnedInfo_ApproverName;
				}

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQActionHistoryCusAmnedInfo_3(BookmarkDocumentQueryCusotmerAmendInformation model, QActionHistoryCusAmnedInfo qActionHistoryCusAmnedInfo_3)
		{
			try
			{
				if(qActionHistoryCusAmnedInfo_3.QActionHistoryCusAmnedInfo_Row != 0)
                {
					model.QActionHistoryCusAmnedInfo_Comment_3 = qActionHistoryCusAmnedInfo_3.QActionHistoryCusAmnedInfo_Comment;
					model.QActionHistoryCusAmnedInfo_CreatedDateTime_3 = qActionHistoryCusAmnedInfo_3.QActionHistoryCusAmnedInfo_CreatedDateTime.DateToString();
					model.QActionHistoryCusAmnedInfo_ActionName_3 = qActionHistoryCusAmnedInfo_3.QActionHistoryCusAmnedInfo_ActionName;
					model.QActionHistoryCusAmnedInfo_ApproverName_3 = qActionHistoryCusAmnedInfo_3.QActionHistoryCusAmnedInfo_ApproverName;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQActionHistoryCusAmnedInfo_2(BookmarkDocumentQueryCusotmerAmendInformation model, QActionHistoryCusAmnedInfo qActionHistoryCusAmnedInfo_2)
		{
			try
			{
				if (qActionHistoryCusAmnedInfo_2.QActionHistoryCusAmnedInfo_Row != 0)
				{
					model.QActionHistoryCusAmnedInfo_Comment_2 = qActionHistoryCusAmnedInfo_2.QActionHistoryCusAmnedInfo_Comment;
					model.QActionHistoryCusAmnedInfo_CreatedDateTime_2 = qActionHistoryCusAmnedInfo_2.QActionHistoryCusAmnedInfo_CreatedDateTime.DateToString();
					model.QActionHistoryCusAmnedInfo_ActionName_2 = qActionHistoryCusAmnedInfo_2.QActionHistoryCusAmnedInfo_ActionName;
					model.QActionHistoryCusAmnedInfo_ApproverName_2 = qActionHistoryCusAmnedInfo_2.QActionHistoryCusAmnedInfo_ApproverName;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQActionHistoryCusAmnedInfo_1(BookmarkDocumentQueryCusotmerAmendInformation model, QActionHistoryCusAmnedInfo qActionHistoryCusAmnedInfo_1)
		{
			try
			{
				if(qActionHistoryCusAmnedInfo_1.QActionHistoryCusAmnedInfo_Row != 0)
                {
					model.QActionHistoryCusAmnedInfo_Comment_1 = qActionHistoryCusAmnedInfo_1.QActionHistoryCusAmnedInfo_Comment;
					model.QActionHistoryCusAmnedInfo_CreatedDateTime_1 = qActionHistoryCusAmnedInfo_1.QActionHistoryCusAmnedInfo_CreatedDateTime.DateToString();
					model.QActionHistoryCusAmnedInfo_ActionName_1 = qActionHistoryCusAmnedInfo_1.QActionHistoryCusAmnedInfo_ActionName;
					model.QActionHistoryCusAmnedInfo_ApproverName_1 = qActionHistoryCusAmnedInfo_1.QActionHistoryCusAmnedInfo_ApproverName;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private QActionHistoryCusAmnedInfo GetQActionHistoryCusAmnedInfoEachRow(List<QActionHistoryCusAmnedInfo> qActionHistoryCusAmnedInfo, int row_number)
		{
			try
			{
				var result = qActionHistoryCusAmnedInfo.Where(w => w.QActionHistoryCusAmnedInfo_Row == row_number).FirstOrDefault();
				if (result != null)
				{
					return result;
				}
				else
				{
					return new QActionHistoryCusAmnedInfo();

				}
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQGuarantorOldCusAmendInfo_6(BookmarkDocumentQueryCusotmerAmendInformation model, QGuarantorOldCusAmendInfo qGuarantorOldCusAmendInfo_6)
		{
			try
			{
				model.QGuarantorOldCusAmendInfo_GuarantorName_6 = qGuarantorOldCusAmendInfo_6.QGuarantorOldCusAmendInfo_GuarantorName;
				model.QGuarantorOldCusAmendInfo_GuarantorPositionNew_6 = qGuarantorOldCusAmendInfo_6.QGuarantorOldCusAmendInfo_GuarantorPositionNew;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQGuarantorOldCusAmendInfo_5(BookmarkDocumentQueryCusotmerAmendInformation model, QGuarantorOldCusAmendInfo qGuarantorOldCusAmendInfo_5)
		{
			try
			{
				model.QGuarantorOldCusAmendInfo_GuarantorName_5 = qGuarantorOldCusAmendInfo_5.QGuarantorOldCusAmendInfo_GuarantorName;
				model.QGuarantorOldCusAmendInfo_GuarantorPositionNew_5 = qGuarantorOldCusAmendInfo_5.QGuarantorOldCusAmendInfo_GuarantorPositionNew;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQGuarantorOldCusAmendInfo_4(BookmarkDocumentQueryCusotmerAmendInformation model, QGuarantorOldCusAmendInfo qGuarantorOldCusAmendInfo_4)
		{
			try
			{
				model.QGuarantorOldCusAmendInfo_GuarantorName_4 = qGuarantorOldCusAmendInfo_4.QGuarantorOldCusAmendInfo_GuarantorName;
				model.QGuarantorOldCusAmendInfo_GuarantorPositionNew_4 = qGuarantorOldCusAmendInfo_4.QGuarantorOldCusAmendInfo_GuarantorPositionNew;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQGuarantorOldCusAmendInfo_3(BookmarkDocumentQueryCusotmerAmendInformation model, QGuarantorOldCusAmendInfo qGuarantorOldCusAmendInfo_3)
		{
			try
			{
				model.QGuarantorOldCusAmendInfo_GuarantorName_3 = qGuarantorOldCusAmendInfo_3.QGuarantorOldCusAmendInfo_GuarantorName;
				model.QGuarantorOldCusAmendInfo_GuarantorPositionNew_3 = qGuarantorOldCusAmendInfo_3.QGuarantorOldCusAmendInfo_GuarantorPositionNew;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQGuarantorOldCusAmendInfo_2(BookmarkDocumentQueryCusotmerAmendInformation model, QGuarantorOldCusAmendInfo qGuarantorOldCusAmendInfo_2)
		{
			try
			{
				model.QGuarantorOldCusAmendInfo_GuarantorName_2 = qGuarantorOldCusAmendInfo_2.QGuarantorOldCusAmendInfo_GuarantorName;
				model.QGuarantorOldCusAmendInfo_GuarantorPositionNew_2 = qGuarantorOldCusAmendInfo_2.QGuarantorOldCusAmendInfo_GuarantorPositionNew;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQGuarantorOldCusAmendInfo_1(BookmarkDocumentQueryCusotmerAmendInformation model, QGuarantorOldCusAmendInfo qGuarantorOldCusAmendInfo_1)
		{
			try
			{
				model.QGuarantorOldCusAmendInfo_GuarantorName_1 = qGuarantorOldCusAmendInfo_1.QGuarantorOldCusAmendInfo_GuarantorName;
				model.QGuarantorOldCusAmendInfo_GuarantorPositionNew_1 = qGuarantorOldCusAmendInfo_1.QGuarantorOldCusAmendInfo_GuarantorPositionNew;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private QGuarantorOldCusAmendInfo GetQGuarantorOldCusAmendInfoEachRow(List<QGuarantorOldCusAmendInfo> qGuarantorOldCusAmendInfo, int row_number)
		{
			try
			{
				var result = qGuarantorOldCusAmendInfo.Where(w => w.QGuarantorOldCusAmendInfo_Row == row_number).FirstOrDefault();
				if (result != null)
				{
					return result;
				}
				else
				{
					return new QGuarantorOldCusAmendInfo();

				}
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQGuarantorNewCusAmendInfo_6(BookmarkDocumentQueryCusotmerAmendInformation model, QGuarantorNewCusAmendInfo qGuarantorNewCusAmendInfo_6)
		{
			try
			{
				model.QGuarantorNewCusAmendInfo_GuarantorName_6 = qGuarantorNewCusAmendInfo_6.QGuarantorNewCusAmendInfo_GuarantorName;
				model.QGuarantorNewCusAmendInfo_GuarantorPositionNew_6 = qGuarantorNewCusAmendInfo_6.QGuarantorNewCusAmendInfo_GuarantorPositionNew;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetModelByQGuarantorNewCusAmendInfo_5(BookmarkDocumentQueryCusotmerAmendInformation model, QGuarantorNewCusAmendInfo qGuarantorNewCusAmendInfo_5)
		{
			try
			{
				model.QGuarantorNewCusAmendInfo_GuarantorName_5 = qGuarantorNewCusAmendInfo_5.QGuarantorNewCusAmendInfo_GuarantorName;
				model.QGuarantorNewCusAmendInfo_GuarantorPositionNew_5 = qGuarantorNewCusAmendInfo_5.QGuarantorNewCusAmendInfo_GuarantorPositionNew;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetModelByQGuarantorNewCusAmendInfo_4(BookmarkDocumentQueryCusotmerAmendInformation model, QGuarantorNewCusAmendInfo qGuarantorNewCusAmendInfo_4)
		{
			try
			{
				model.QGuarantorNewCusAmendInfo_GuarantorName_4 = qGuarantorNewCusAmendInfo_4.QGuarantorNewCusAmendInfo_GuarantorName;
				model.QGuarantorNewCusAmendInfo_GuarantorPositionNew_4 = qGuarantorNewCusAmendInfo_4.QGuarantorNewCusAmendInfo_GuarantorPositionNew;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetModelByQGuarantorNewCusAmendInfo_3(BookmarkDocumentQueryCusotmerAmendInformation model, QGuarantorNewCusAmendInfo qGuarantorNewCusAmendInfo_3)
		{
			try
			{
				model.QGuarantorNewCusAmendInfo_GuarantorName_3 = qGuarantorNewCusAmendInfo_3.QGuarantorNewCusAmendInfo_GuarantorName;
				model.QGuarantorNewCusAmendInfo_GuarantorPositionNew_3 = qGuarantorNewCusAmendInfo_3.QGuarantorNewCusAmendInfo_GuarantorPositionNew;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetModelByQGuarantorNewCusAmendInfo_2(BookmarkDocumentQueryCusotmerAmendInformation model, QGuarantorNewCusAmendInfo qGuarantorNewCusAmendInfo_2)
		{
			try
			{
				model.QGuarantorNewCusAmendInfo_GuarantorName_2 = qGuarantorNewCusAmendInfo_2.QGuarantorNewCusAmendInfo_GuarantorName;
				model.QGuarantorNewCusAmendInfo_GuarantorPositionNew_2 = qGuarantorNewCusAmendInfo_2.QGuarantorNewCusAmendInfo_GuarantorPositionNew;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}


		private void SetModelByQGuarantorNewCusAmendInfo_1(BookmarkDocumentQueryCusotmerAmendInformation model, QGuarantorNewCusAmendInfo qGuarantorNewCusAmendInfo_1)
		{
			try
			{
				model.QGuarantorNewCusAmendInfo_GuarantorName_1 = qGuarantorNewCusAmendInfo_1.QGuarantorNewCusAmendInfo_GuarantorName;
				model.QGuarantorNewCusAmendInfo_GuarantorPositionNew_1 = qGuarantorNewCusAmendInfo_1.QGuarantorNewCusAmendInfo_GuarantorPositionNew;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private QGuarantorNewCusAmendInfo GetQGuarantorNewCusAmendInfoEachRow(List<QGuarantorNewCusAmendInfo> qGuarantorNewCusAmendInfo, int row_number)
		{
			try
			{
				var result = qGuarantorNewCusAmendInfo.Where(w => w.QGuarantorNewCusAmendInfo_Row == row_number).FirstOrDefault();
				if (result != null)
				{
					return result;
				}
				else
				{
					return new QGuarantorNewCusAmendInfo();

				}
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetModelByQAuthorizedCustCusAmendInfo_6(BookmarkDocumentQueryCusotmerAmendInformation model, QAuthorizedCustCusAmendInfo qAuthorizedCustCusAmendInfo_6)
		{
			try
			{
				model.QAuthorizedCustCusAmendInfo_AuthorizedCustNew_6 = qAuthorizedCustCusAmendInfo_6.QAuthorizedCustCusAmendInfo_AuthorizedCustNew;
				model.QAuthorizedCustCusAmendInfo_AuthorizedPersonTypeGUID_6 = qAuthorizedCustCusAmendInfo_6.QAuthorizedCustCusAmendInfo_AuthorizedPersonTypeGUID;
				model.QAuthorizedCustCusAmendInfo_AuthorizedPositionCustNew_6 = qAuthorizedCustCusAmendInfo_6.QAuthorizedCustCusAmendInfo_AuthorizedPositionCustNew;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQAuthorizedCustCusAmendInfo_5(BookmarkDocumentQueryCusotmerAmendInformation model, QAuthorizedCustCusAmendInfo qAuthorizedCustCusAmendInfo_5)
		{
			try
			{
				model.QAuthorizedCustCusAmendInfo_AuthorizedCustNew_5 = qAuthorizedCustCusAmendInfo_5.QAuthorizedCustCusAmendInfo_AuthorizedCustNew;
				model.QAuthorizedCustCusAmendInfo_AuthorizedPersonTypeGUID_5 = qAuthorizedCustCusAmendInfo_5.QAuthorizedCustCusAmendInfo_AuthorizedPersonTypeGUID;
				model.QAuthorizedCustCusAmendInfo_AuthorizedPositionCustNew_5 = qAuthorizedCustCusAmendInfo_5.QAuthorizedCustCusAmendInfo_AuthorizedPositionCustNew;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQAuthorizedCustCusAmendInfo_4(BookmarkDocumentQueryCusotmerAmendInformation model, QAuthorizedCustCusAmendInfo qAuthorizedCustCusAmendInfo_4)
		{
			try
			{
				model.QAuthorizedCustCusAmendInfo_AuthorizedCustNew_4 = qAuthorizedCustCusAmendInfo_4.QAuthorizedCustCusAmendInfo_AuthorizedCustNew;
				model.QAuthorizedCustCusAmendInfo_AuthorizedPersonTypeGUID_4 = qAuthorizedCustCusAmendInfo_4.QAuthorizedCustCusAmendInfo_AuthorizedPersonTypeGUID;
				model.QAuthorizedCustCusAmendInfo_AuthorizedPositionCustNew_4 = qAuthorizedCustCusAmendInfo_4.QAuthorizedCustCusAmendInfo_AuthorizedPositionCustNew;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQAuthorizedCustCusAmendInfo_3(BookmarkDocumentQueryCusotmerAmendInformation model, QAuthorizedCustCusAmendInfo qAuthorizedCustCusAmendInfo_3)
		{
			try
			{
				model.QAuthorizedCustCusAmendInfo_AuthorizedCustNew_3 = qAuthorizedCustCusAmendInfo_3.QAuthorizedCustCusAmendInfo_AuthorizedCustNew;
				model.QAuthorizedCustCusAmendInfo_AuthorizedPersonTypeGUID_3 = qAuthorizedCustCusAmendInfo_3.QAuthorizedCustCusAmendInfo_AuthorizedPersonTypeGUID;
				model.QAuthorizedCustCusAmendInfo_AuthorizedPositionCustNew_3 = qAuthorizedCustCusAmendInfo_3.QAuthorizedCustCusAmendInfo_AuthorizedPositionCustNew;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQAuthorizedCustCusAmendInfo_2(BookmarkDocumentQueryCusotmerAmendInformation model, QAuthorizedCustCusAmendInfo qAuthorizedCustCusAmendInfo_2)
		{
			try
			{
				model.QAuthorizedCustCusAmendInfo_AuthorizedCustNew_2 = qAuthorizedCustCusAmendInfo_2.QAuthorizedCustCusAmendInfo_AuthorizedCustNew;
				model.QAuthorizedCustCusAmendInfo_AuthorizedPersonTypeGUID_2 = qAuthorizedCustCusAmendInfo_2.QAuthorizedCustCusAmendInfo_AuthorizedPersonTypeGUID;
				model.QAuthorizedCustCusAmendInfo_AuthorizedPositionCustNew_2 = qAuthorizedCustCusAmendInfo_2.QAuthorizedCustCusAmendInfo_AuthorizedPositionCustNew;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQAuthorizedCustCusAmendInfo_1(BookmarkDocumentQueryCusotmerAmendInformation model, QAuthorizedCustCusAmendInfo qAuthorizedCustCusAmendInfo_1)
		{
			try
			{
				model.QAuthorizedCustCusAmendInfo_AuthorizedCustNew_1 = qAuthorizedCustCusAmendInfo_1.QAuthorizedCustCusAmendInfo_AuthorizedCustNew;
				model.QAuthorizedCustCusAmendInfo_AuthorizedPersonTypeGUID_1 = qAuthorizedCustCusAmendInfo_1.QAuthorizedCustCusAmendInfo_AuthorizedPersonTypeGUID;
				model.QAuthorizedCustCusAmendInfo_AuthorizedPositionCustNew_1 = qAuthorizedCustCusAmendInfo_1.QAuthorizedCustCusAmendInfo_AuthorizedPositionCustNew;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}


		#region R02 AuthCustOld
		private void SetModelByQAuthorizedCustOldCusAmendInfo_6(BookmarkDocumentQueryCusotmerAmendInformation model, QAuthorizedCustOldCusAmendInfo qAuthorizedCustOldCusAmendInfo_6)
		{
			try
			{
				model.QAuthorizedCustOldCusAmendInfo_AuthorizedCustOld_6 = qAuthorizedCustOldCusAmendInfo_6.QAuthorizedCustOldCusAmendInfo_AuthorizedCustOld;
				model.QAuthorizedCustOldCusAmendInfo_AuthorizedPersonTypeGUID_6 = qAuthorizedCustOldCusAmendInfo_6.QAuthorizedCustOldCusAmendInfo_AuthorizedPersonTypeGUID;
				model.QAuthorizedCustOldCusAmendInfo_AuthorizedPositionCustOld_6 = qAuthorizedCustOldCusAmendInfo_6.QAuthorizedCustOldCusAmendInfo_AuthorizedPositionCustNew;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQAuthorizedCustOldCusAmendInfo_5(BookmarkDocumentQueryCusotmerAmendInformation model, QAuthorizedCustOldCusAmendInfo qAuthorizedCustOldCusAmendInfo_5)
		{
			try
			{
				model.QAuthorizedCustOldCusAmendInfo_AuthorizedCustOld_5 = qAuthorizedCustOldCusAmendInfo_5.QAuthorizedCustOldCusAmendInfo_AuthorizedCustOld;
				model.QAuthorizedCustOldCusAmendInfo_AuthorizedPersonTypeGUID_5 = qAuthorizedCustOldCusAmendInfo_5.QAuthorizedCustOldCusAmendInfo_AuthorizedPersonTypeGUID;
				model.QAuthorizedCustOldCusAmendInfo_AuthorizedPositionCustOld_5 = qAuthorizedCustOldCusAmendInfo_5.QAuthorizedCustOldCusAmendInfo_AuthorizedPositionCustNew;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQAuthorizedCustOldCusAmendInfo_4(BookmarkDocumentQueryCusotmerAmendInformation model, QAuthorizedCustOldCusAmendInfo qAuthorizedCustOldCusAmendInfo_4)
		{
			try
			{
				model.QAuthorizedCustOldCusAmendInfo_AuthorizedCustOld_4 = qAuthorizedCustOldCusAmendInfo_4.QAuthorizedCustOldCusAmendInfo_AuthorizedCustOld;
				model.QAuthorizedCustOldCusAmendInfo_AuthorizedPersonTypeGUID_4 = qAuthorizedCustOldCusAmendInfo_4.QAuthorizedCustOldCusAmendInfo_AuthorizedPersonTypeGUID;
				model.QAuthorizedCustOldCusAmendInfo_AuthorizedPositionCustOld_4 = qAuthorizedCustOldCusAmendInfo_4.QAuthorizedCustOldCusAmendInfo_AuthorizedPositionCustNew;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQAuthorizedCustOldCusAmendInfo_3(BookmarkDocumentQueryCusotmerAmendInformation model, QAuthorizedCustOldCusAmendInfo qAuthorizedCustOldCusAmendInfo_3)
		{
			try
			{
				model.QAuthorizedCustOldCusAmendInfo_AuthorizedCustOld_3 = qAuthorizedCustOldCusAmendInfo_3.QAuthorizedCustOldCusAmendInfo_AuthorizedCustOld;
				model.QAuthorizedCustOldCusAmendInfo_AuthorizedPersonTypeGUID_3 = qAuthorizedCustOldCusAmendInfo_3.QAuthorizedCustOldCusAmendInfo_AuthorizedPersonTypeGUID;
				model.QAuthorizedCustOldCusAmendInfo_AuthorizedPositionCustOld_3 = qAuthorizedCustOldCusAmendInfo_3.QAuthorizedCustOldCusAmendInfo_AuthorizedPositionCustNew;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQAuthorizedCustOldCusAmendInfo_2(BookmarkDocumentQueryCusotmerAmendInformation model, QAuthorizedCustOldCusAmendInfo qAuthorizedCustOldCusAmendInfo_2)
		{
			try
			{
				model.QAuthorizedCustOldCusAmendInfo_AuthorizedCustOld_2 = qAuthorizedCustOldCusAmendInfo_2.QAuthorizedCustOldCusAmendInfo_AuthorizedCustOld;
				model.QAuthorizedCustOldCusAmendInfo_AuthorizedPersonTypeGUID_2 = qAuthorizedCustOldCusAmendInfo_2.QAuthorizedCustOldCusAmendInfo_AuthorizedPersonTypeGUID;
				model.QAuthorizedCustOldCusAmendInfo_AuthorizedPositionCustOld_2 = qAuthorizedCustOldCusAmendInfo_2.QAuthorizedCustOldCusAmendInfo_AuthorizedPositionCustNew;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQAuthorizedCustOldCusAmendInfo_1(BookmarkDocumentQueryCusotmerAmendInformation model, QAuthorizedCustOldCusAmendInfo qAuthorizedCustOldCusAmendInfo_1)
		{
			try
			{
				model.QAuthorizedCustOldCusAmendInfo_AuthorizedCustOld_1 = qAuthorizedCustOldCusAmendInfo_1.QAuthorizedCustOldCusAmendInfo_AuthorizedCustOld;
				model.QAuthorizedCustOldCusAmendInfo_AuthorizedPersonTypeGUID_1 = qAuthorizedCustOldCusAmendInfo_1.QAuthorizedCustOldCusAmendInfo_AuthorizedPersonTypeGUID;
				model.QAuthorizedCustOldCusAmendInfo_AuthorizedPositionCustOld_1 = qAuthorizedCustOldCusAmendInfo_1.QAuthorizedCustOldCusAmendInfo_AuthorizedPositionCustNew;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion

		private QAuthorizedCustCusAmendInfo GetQAuthorizedCustCusAmendInfoEachRow(List<QAuthorizedCustCusAmendInfo> qAuthorizedCustCusAmendInfo, int row_number)
		{
			try
			{
				var result = qAuthorizedCustCusAmendInfo.Where(w => w.QAuthorizedCustCusAmendInfo_Row == row_number).FirstOrDefault();
				if (result != null)
				{
					return result;
				}
				else
				{
					return new QAuthorizedCustCusAmendInfo();

				}
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private QAuthorizedCustOldCusAmendInfo GetQAuthorizedCustOldCusAmendInfoEachRow(List<QAuthorizedCustOldCusAmendInfo> qAuthorizedCustOldCusAmendInfo, int row_number)
		{
			try
			{
				var result = qAuthorizedCustOldCusAmendInfo.Where(w => w.QAuthorizedCustOldCusAmendInfo_Row == row_number).FirstOrDefault();
				if (result != null)
				{
					return result;
				}
				else
				{
					return new QAuthorizedCustOldCusAmendInfo();

				}
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetModelByQRetentionConditionTransCusAmendInfo_3(BookmarkDocumentQueryCusotmerAmendInformation model, QRetentionConditionTransCusAmendInfo qRetentionConditionTransCusAmendInfo_3)
		{
			try
			{
				if (qRetentionConditionTransCusAmendInfo_3.QRetentionConditionTransCusAmendInfo_Row != 0)
				{
					model.QRetentionConditionTransCusAmendInfo_RetentionDeductionMethod_3 = SystemStaticData.GetTranslatedMessage((((RetentionDeductionMethod)Convert.ToInt32(qRetentionConditionTransCusAmendInfo_3.QRetentionConditionTransCusAmendInfo_RetentionDeductionMethod)).GetAttrCode()));
					model.QRetentionConditionTransCusAmendInfo_RetentionCalculateBase_3 = SystemStaticData.GetTranslatedMessage((((RetentionCalculateBase)Convert.ToInt32(qRetentionConditionTransCusAmendInfo_3.QRetentionConditionTransCusAmendInfo_RetentionCalculateBase)).GetAttrCode()));
					model.QRetentionConditionTransCusAmendInfo_RetentionPct_3 = qRetentionConditionTransCusAmendInfo_3.QRetentionConditionTransCusAmendInfo_RetentionPct;
					model.QRetentionConditionTransCusAmendInfo_RetentionAmount_3 = qRetentionConditionTransCusAmendInfo_3.QRetentionConditionTransCusAmendInfo_RetentionAmount;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetModelByQRetentionConditionTransCusAmendInfo_2(BookmarkDocumentQueryCusotmerAmendInformation model, QRetentionConditionTransCusAmendInfo qRetentionConditionTransCusAmendInfo_2)
		{
			try
			{
				if (qRetentionConditionTransCusAmendInfo_2.QRetentionConditionTransCusAmendInfo_Row != 0)
                {
					model.QRetentionConditionTransCusAmendInfo_RetentionDeductionMethod_2 = SystemStaticData.GetTranslatedMessage((((RetentionDeductionMethod)Convert.ToInt32(qRetentionConditionTransCusAmendInfo_2.QRetentionConditionTransCusAmendInfo_RetentionDeductionMethod)).GetAttrCode()));
					model.QRetentionConditionTransCusAmendInfo_RetentionCalculateBase_2 = SystemStaticData.GetTranslatedMessage((((RetentionCalculateBase)Convert.ToInt32(qRetentionConditionTransCusAmendInfo_2.QRetentionConditionTransCusAmendInfo_RetentionCalculateBase)).GetAttrCode()));
					model.QRetentionConditionTransCusAmendInfo_RetentionPct_2 = qRetentionConditionTransCusAmendInfo_2.QRetentionConditionTransCusAmendInfo_RetentionPct;
					model.QRetentionConditionTransCusAmendInfo_RetentionAmount_2 = qRetentionConditionTransCusAmendInfo_2.QRetentionConditionTransCusAmendInfo_RetentionAmount;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetModelByQRetentionConditionTransCusAmendInfo_1(BookmarkDocumentQueryCusotmerAmendInformation model, QRetentionConditionTransCusAmendInfo qRetentionConditionTransCusAmendInfo_1)
		{

			try
			{
				if(qRetentionConditionTransCusAmendInfo_1.QRetentionConditionTransCusAmendInfo_Row != 0)
                {
					model.QRetentionConditionTransCusAmendInfo_RetentionDeductionMethod_1 = SystemStaticData.GetTranslatedMessage((((RetentionDeductionMethod)Convert.ToInt32(qRetentionConditionTransCusAmendInfo_1.QRetentionConditionTransCusAmendInfo_RetentionDeductionMethod)).GetAttrCode()));
					model.QRetentionConditionTransCusAmendInfo_RetentionCalculateBase_1 = SystemStaticData.GetTranslatedMessage((((RetentionCalculateBase)Convert.ToInt32(qRetentionConditionTransCusAmendInfo_1.QRetentionConditionTransCusAmendInfo_RetentionCalculateBase)).GetAttrCode()));
					model.QRetentionConditionTransCusAmendInfo_RetentionPct_1 = qRetentionConditionTransCusAmendInfo_1.QRetentionConditionTransCusAmendInfo_RetentionPct;
					model.QRetentionConditionTransCusAmendInfo_RetentionAmount_1 = qRetentionConditionTransCusAmendInfo_1.QRetentionConditionTransCusAmendInfo_RetentionAmount;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private QRetentionConditionTransCusAmendInfo GetQRetentionConditionTransCusAmendInfoEachRow(List<QRetentionConditionTransCusAmendInfo> qRetentionConditionTransCusAmendInfo, int row_number)
		{
			try
			{
				var result = qRetentionConditionTransCusAmendInfo.Where(w => w.QRetentionConditionTransCusAmendInfo_Row == row_number).FirstOrDefault();
				if (result != null)
				{
					return result;
				}
				else
				{
					return new QRetentionConditionTransCusAmendInfo();

				}
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		//private void SetmodelByQCreditAppTableCusAmendInfo(BookmarkDocumentQueryCusotmerAmendInformation model, QCreditAppTableCusAmendInfo qCreditAppTableCusAmendInfo)
		//{
		//	try
		//	{
		//		model.QCreditAppTableCusAmendInfo_CreditAppTableGUID = qCreditAppTableCusAmendInfo.QCreditAppTableCusAmendInfo_CreditAppTableGUID;
		//		model.QCreditAppTableCusAmendInfo_MaxRetentionPct = qCreditAppTableCusAmendInfo.QCreditAppTableCusAmendInfo_MaxRetentionPct;
		//		model.QCreditAppTableCusAmendInfo_MaxRetentionAmount = qCreditAppTableCusAmendInfo.QCreditAppTableCusAmendInfo_MaxRetentionAmount;
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}

		private void SetmodelByQCreditAppRequestTableAmendCusAmendInfo(BookmarkDocumentQueryCusotmerAmendInformation model, QCreditAppRequestTableAmendCusAmendInfo qCreditAppRequestTableAmendCusAmendInfo)
		{
			try
			{
				model.QCreditAppRequestTableAmendCusAmendInfo_OriginalMaxRetentionPct = qCreditAppRequestTableAmendCusAmendInfo.QCreditAppRequestTableAmendCusAmendInfo_OriginalMaxRetentionPct;
				model.QCreditAppRequestTableAmendCusAmendInfo_OriginalMaxRetentionAmount = qCreditAppRequestTableAmendCusAmendInfo.QCreditAppRequestTableAmendCusAmendInfo_OriginalMaxRetentionAmount;
				model.QCreditAppRequestTableAmendCusAmendInfo_OriginalCreditLimitRequestFeeAmount = qCreditAppRequestTableAmendCusAmendInfo.QCreditAppRequestTableAmendCusAmendInfo_OriginalCreditLimitRequestFeeAmount;
				model.QCreditAppRequestTableAmendCusAmendInfo_OriginalPurchaseFeePct = qCreditAppRequestTableAmendCusAmendInfo.QCreditAppRequestTableAmendCusAmendInfo_OriginalPurchaseFeePct;
				model.QCreditAppRequestTableAmendCusAmendInfo_OriginalPurchaseFeeCalculateBase = SystemStaticData.GetTranslatedMessage((((PurchaseFeeCalculateBase)Convert.ToInt32(qCreditAppRequestTableAmendCusAmendInfo.QCreditAppRequestTableAmendCusAmendInfo_OriginalPurchaseFeeCalculateBase)).GetAttrCode()));
				model.QCreditAppRequestTableAmendCusAmendInfo_OriginalIntersetType = qCreditAppRequestTableAmendCusAmendInfo.QCreditAppRequestTableAmendCusAmendInfo_OriginalIntersetType;
				model.QCreditAppRequestTableAmendCusAmendInfo_OriginalTotalLnterestPct = qCreditAppRequestTableAmendCusAmendInfo.QCreditAppRequestTableAmendCusAmendInfo_OriginalTotalLnterestPct;
				// R02
				model.QCreditAppRequestTableAmendCusAmendInfo_OriginalInterestAdjustmentPct = qCreditAppRequestTableAmendCusAmendInfo.QCreditAppRequestTableAmendCusAmendInfo_OriginalInterestAdjustmentPct;
				model.QCreditAppRequestTableAmendCusAmendInfo_OriginalMaxPurchasePct = qCreditAppRequestTableAmendCusAmendInfo.QCreditAppRequestTableAmendCusAmendInfo_OriginalMaxPurchasePct;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetmodelByQBankAccountControlCusAmendInfo(BookmarkDocumentQueryCusotmerAmendInformation model, QBankAccountControlCusAmendInfo qBankAccountControlCusAmendInfo)
		{
			try
			{
				model.QBankAccountControlCusAmendInfo_CustBankName = qBankAccountControlCusAmendInfo.QBankAccountControlCusAmendInfo_CustBankName;
				model.QBankAccountControlCusAmendInfo_CustBankBranch = qBankAccountControlCusAmendInfo.QBankAccountControlCusAmendInfo_CustBankBranch;
				model.QBankAccountControlCusAmendInfo_CustBankType = qBankAccountControlCusAmendInfo.QBankAccountControlCusAmendInfo_CustBankType;
				model.QBankAccountControlCusAmendInfo_CustBankAccountName = qBankAccountControlCusAmendInfo.QBankAccountControlCusAmendInfo_CustBankAccountName;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetmodelByQCustomerCusAmendInfo(BookmarkDocumentQueryCusotmerAmendInformation model, QCustomerCusAmendInfo qCustomerCusAmendInfo)
		{
			try
			{
				model.QCustomerCusAmendInfo_CustomerId = qCustomerCusAmendInfo.QCustomerCusAmendInfo_CustomerId;
				model.QCustomerCusAmendInfo_CustomerName = qCustomerCusAmendInfo.QCustomerCusAmendInfo_CustomerName;
				model.QCustomerCusAmendInfo_IdentificationType = qCustomerCusAmendInfo.QCustomerCusAmendInfo_IdentificationType;
				model.QCustomerCusAmendInfo_TaxID = qCustomerCusAmendInfo.QCustomerCusAmendInfo_TaxID;
				model.QCustomerCusAmendInfo_PassportID = qCustomerCusAmendInfo.QCustomerCusAmendInfo_PassportID;
				model.QCustomerCusAmendInfo_RecordType = qCustomerCusAmendInfo.QCustomerCusAmendInfo_RecordType;
				model.QCustomerCusAmendInfo_DateOfEstablish = qCustomerCusAmendInfo.QCustomerCusAmendInfo_DateOfEstablish;
				model.QCustomerCusAmendInfo_DateOfBirth = qCustomerCusAmendInfo.QCustomerCusAmendInfo_DateOfBirth;
				model.QCustomerCusAmendInfo_IntroducedByGUID = qCustomerCusAmendInfo.QCustomerCusAmendInfo_IntroducedByGUID;
				model.QCustomerCusAmendInfo_BusinessTypeGUID = qCustomerCusAmendInfo.QCustomerCusAmendInfo_BusinessTypeGUID;
				model.QCustomerCusAmendInfo_ResponsibleByGUID = qCustomerCusAmendInfo.QCustomerCusAmendInfo_ResponsibleByGUID;
				model.QCustomerCusAmendInfo_ResponsibleBy = qCustomerCusAmendInfo.QCustomerCusAmendInfo_ResponsibleBy;
				model.QCustomerCusAmendInfo_BusinessType = qCustomerCusAmendInfo.QCustomerCusAmendInfo_BusinessType;
				model.QCustomerCusAmendInfo_IntroducedBy = qCustomerCusAmendInfo.QCustomerCusAmendInfo_IntroducedBy;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetmodelByQOriginalCreditAppTableCusAmendInfo(BookmarkDocumentQueryCusotmerAmendInformation model, QOriginalCreditAppTableCusAmendInfo originalCreditAppTableCusAmendInfo)
		{
			try
			{
				model.QOriginalCreditAppTableCusAmendInfo_OriginalCreditAppTableId = originalCreditAppTableCusAmendInfo.QOriginalCreditAppTableCusAmendInfo_OriginalCreditAppTableId;
				model.QCreditAppTableCusAmendInfo_MaxRetentionAmount = originalCreditAppTableCusAmendInfo.QOriginalCreditAppTableCusAmendInfo_MaxRetentionAmount;
				model.QCreditAppTableCusAmendInfo_MaxRetentionPct = originalCreditAppTableCusAmendInfo.QOriginalCreditAppTableCusAmendInfo_MaxRetentionPct;
				model.QCreditAppTableCusAmendInfo_CreditAppTableGUID = originalCreditAppTableCusAmendInfo.QOriginalCreditAppTableCusAmendInfo_CreditAppTableGUID;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetmodelByQCreditAppRequestTableCusAmnedInfo(BookmarkDocumentQueryCusotmerAmendInformation model, QCreditAppRequestTableCusAmnedInfo qCreditAppRequestTableCusAmnedInfo)
		{
			try
			{
				model.QCreditAppRequestTableCusAmnedInfo_CreditAppRequestId = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_CreditAppRequestId;
				model.QCreditAppRequestTableCusAmnedInfo_RefCreditAppTableGUID = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_RefCreditAppTableGUID;
				model.QCreditAppRequestTableCusAmnedInfo_OriginalCreditAppTableId = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_OriginalCreditAppTableId;
				model.QCreditAppRequestTableCusAmnedInfo_RequestDate = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_RequestDate;
				model.QCreditAppRequestTableCusAmnedInfo_CustomerTableGUID = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_CustomerTableGUID;
				model.QCreditAppRequestTableCusAmnedInfo_Description = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_Description;
				model.QCreditAppRequestTableCusAmnedInfo_Remark = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_Remark;
				model.QCreditAppRequestTableCusAmnedInfo_DocumentStatusGUID = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_DocumentStatusGUID;
				model.QCreditAppRequestTableCusAmnedInfo_Status = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_Status;
				model.QCreditAppRequestTableCusAmnedInfo_KYCSetupGUID = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_KYCSetupGUID;
				model.QCreditAppRequestTableCusAmnedInfo_KYC = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_KYC;
				model.QCreditAppRequestTableCusAmnedInfo_CreditScoringGUID = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_CreditScoringGUID;
				model.QCreditAppRequestTableCusAmnedInfo_CreditScoring = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_CreditScoring;
				model.QCreditAppRequestTableCusAmnedInfo_DocumentReasonGUID = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_DocumentReasonGUID;
				model.QCreditAppRequestTableCusAmnedInfo_RegisteredAddressGUID = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_RegisteredAddressGUID;
				model.QCreditAppRequestTableCusAmnedInfo_BankAccountControlGUID = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_BankAccountControlGUID;
				model.QCreditAppRequestTableCusAmnedInfo_RegisteredAddress = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_RegisteredAddress;
				model.QCreditAppRequestTableCusAmnedInfo_MarketingComment = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_MarketingComment;
				model.QCreditAppRequestTableCusAmnedInfo_CreditComment = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_CreditComment;
				model.QCreditAppRequestTableCusAmnedInfo_ApproverComment = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_ApproverComment;
				model.QCreditAppRequestTableCusAmnedInfo_InterestTypeGUID = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_InterestTypeGUID;
				model.QCreditAppRequestTableCusAmnedInfo_InterestType = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_InterestType;
				model.QCreditAppRequestTableCusAmnedInfo_TotalInterestPct = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_TotalInterestPct;
				model.QCreditAppRequestTableCusAmnedInfo_MaxRetentionPct = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_MaxRetentionPct;
				model.QCreditAppRequestTableCusAmnedInfo_MaxRetentionAmount = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_MaxRetentionAmount;
				model.QCreditAppRequestTableCusAmnedInfo_CreditLimitRequest = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_CreditLimitRequest;
				model.QCreditAppRequestTableCusAmnedInfo_PurchaseFeePct = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_PurchaseFeePct;
				model.QCreditAppRequestTableCusAmnedInfo_PurchaseFeeCalculateBase = SystemStaticData.GetTranslatedMessage((((PurchaseFeeCalculateBase)Convert.ToInt32(qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_PurchaseFeeCalculateBase)).GetAttrCode()));
				model.QCreditAppRequestTableCusAmnedInfo_InterestId = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableCusAmnedInfo_InterestId;
				// R02
				model.QCreditAppRequestTableCusAmendInfo_InterestAdjustment = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableAmendCusAmendInfo_InterestAdjustment;
				model.QCreditAppRequestTableCusAmendInfo_MaximumPurchasePct = qCreditAppRequestTableCusAmnedInfo.QCreditAppRequestTableAmendCusAmendInfo_MaximumPurchasePct;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private List<QActionHistoryCusAmnedInfo> GetQActionHistoryCusAmnedInfo(Guid refGUID)
		{
			try
			{
				var result = (from actionhistory in db.Set<ActionHistory>()
							  where (actionhistory.ActivityName == "Assist MD approve" 
										|| actionhistory.ActivityName == "Board 2 in 3 apporve" 
										|| actionhistory.ActivityName == "Board 3 in 4 apporve"
										|| actionhistory.ActivityName == "Credit Staff Review and Approve"
										|| actionhistory.ActivityName == "Credit Head Review and Approve")
								&& actionhistory.ActionName == "Approve" && actionhistory.RefGUID == refGUID
							  select new QActionHistoryCusAmnedInfo
							  {
								  QActionHistoryCusAmnedInfo_Comment = actionhistory.Comment,
								  QActionHistoryCusAmnedInfo_CreatedDateTime = actionhistory.CreatedDateTime,
								  QActionHistoryCusAmnedInfo_ActionName = actionhistory.ActivityName,
								  QActionHistoryCusAmnedInfo_ApproverName = actionhistory.CreatedBy,
								  QActionHistoryCusAmnedInfo_Row = 0
							  }
								).Take(6).ToList();
				var row_number = 0;
				foreach (var item in result)
				{
					item.QActionHistoryCusAmnedInfo_Row = row_number + 1;
					row_number = row_number + 1;
				}
				if (result != null)
				{
					return result;
				}
				else
				{
					return new List<QActionHistoryCusAmnedInfo>();

				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private List<QGuarantorOldCusAmendInfo> GetQGuarantorOldCusAmendInfo(Guid refGUID)
		{
			try
			{
				var result = (from guarantorTrans in db.Set<GuarantorTrans>()
							  join relatedPersonTable in db.Set<RelatedPersonTable>()
							   on guarantorTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID into ljrelatedPersonTable
							  from relatedPersonTable in ljrelatedPersonTable.DefaultIfEmpty()

							  join guarantorType in db.Set<GuarantorType>()
							  on guarantorTrans.GuarantorTypeGUID equals guarantorType.GuarantorTypeGUID into ljguarantorType
							  from guarantorType in ljguarantorType.DefaultIfEmpty()
							  where guarantorTrans.RefGUID == refGUID && guarantorTrans.RefType == (int)RefType.CreditAppRequestTable
							  && guarantorTrans.RefGuarantorTransGUID == null
							  orderby guarantorTrans.Ordering ascending
							  select new QGuarantorOldCusAmendInfo
							  {
								  QGuarantorOldCusAmendInfo_GuarantorName = relatedPersonTable.Name,
								  QGuarantorOldCusAmendInfo_GuarantorPositionNew = guarantorType.Description,
								  QGuarantorOldCusAmendInfo_Row = 0

							  }
					 ).Take(6).ToList();
				var row_number = 0;
				foreach (var item in result)
				{
					item.QGuarantorOldCusAmendInfo_Row = row_number + 1;
					row_number = row_number + 1;
				}
				if (result != null)
				{
					return result;
				}
				else
				{
					return new List<QGuarantorOldCusAmendInfo>();

				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private List<QGuarantorNewCusAmendInfo> GetQGuarantorNewCusAmendInfo(Guid refGUID)
		{
			try
			{
				var result = (from guarantorTrans in db.Set<GuarantorTrans>()
							  join relatedPersonTable in db.Set<RelatedPersonTable>()
							  on guarantorTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID into ljrelatedPersonTable
							  from relatedPersonTable in ljrelatedPersonTable.DefaultIfEmpty()

							  join guarantorType in db.Set<GuarantorType>()
							  on guarantorTrans.GuarantorTypeGUID equals guarantorType.GuarantorTypeGUID into ljguarantorType
							  from guarantorType in ljguarantorType.DefaultIfEmpty()
							  where guarantorTrans.RefGUID == refGUID && guarantorTrans.RefType == (int)RefType.CreditAppRequestTable
							  && guarantorTrans.InActive == false
							  orderby guarantorTrans.Ordering ascending
							  select new QGuarantorNewCusAmendInfo
							  {
								  QGuarantorNewCusAmendInfo_GuarantorName = relatedPersonTable.Name,
								  QGuarantorNewCusAmendInfo_GuarantorPositionNew = guarantorType.Description,
								  QGuarantorNewCusAmendInfo_Row = 0
							  }
					).Take(6).ToList();
				var row_number = 0;
				foreach (var item in result)
				{
					item.QGuarantorNewCusAmendInfo_Row = row_number + 1;
					row_number = row_number + 1;
				}
				if (result != null)
				{
					return result;
				}
				else
				{
					return new List<QGuarantorNewCusAmendInfo>();

				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private List<QAuthorizedCustCusAmendInfo> GetQAuthorizedCustCusAmendInfo(Guid refGUID)
		{
			try
			{
				var result = (from authorizedPersonTrans in db.Set<AuthorizedPersonTrans>()

							  join relatedPersonTable in db.Set<RelatedPersonTable>()
							  on authorizedPersonTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID into ljrelatedPersonTable
							  from relatedPersonTable in ljrelatedPersonTable.DefaultIfEmpty()

							  join authorizedPersonType in db.Set<AuthorizedPersonType>()
							  on authorizedPersonTrans.AuthorizedPersonTypeGUID equals authorizedPersonType.AuthorizedPersonTypeGUID into ljauthorizedPersonType
							  from authorizedPersonType in ljauthorizedPersonType.DefaultIfEmpty()

							  where authorizedPersonTrans.RefGUID == refGUID && authorizedPersonTrans.RefType == (int)RefType.CreditAppRequestTable
							  && authorizedPersonTrans.InActive == false 
							  orderby authorizedPersonTrans.Ordering ascending
							  select new QAuthorizedCustCusAmendInfo
							  {
								  QAuthorizedCustCusAmendInfo_AuthorizedCustNew = relatedPersonTable.Name,
								  QAuthorizedCustCusAmendInfo_AuthorizedPersonTypeGUID = authorizedPersonType.AuthorizedPersonTypeGUID,
								  QAuthorizedCustCusAmendInfo_AuthorizedPositionCustNew = authorizedPersonType.Description,
								  QAuthorizedCustCusAmendInfo_Row = 0
							  }

					).Take(6).ToList();
				var row_number = 0;
				foreach (var item in result)
				{
					item.QAuthorizedCustCusAmendInfo_Row = row_number + 1;
					row_number = row_number + 1;
				}
				if (result != null)
				{
					return result;
				}
				else
				{
					return new List<QAuthorizedCustCusAmendInfo>();

				}


			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		// R02
		private List<QAuthorizedCustOldCusAmendInfo> GetQAuthorizedCustOldCusAmendInfo(Guid refGUID)
		{
			try
			{
				var result = (from authorizedPersonTrans in db.Set<AuthorizedPersonTrans>()

							  join relatedPersonTable in db.Set<RelatedPersonTable>()
							  on authorizedPersonTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID into ljrelatedPersonTable
							  from relatedPersonTable in ljrelatedPersonTable.DefaultIfEmpty()

							  join authorizedPersonType in db.Set<AuthorizedPersonType>()
							  on authorizedPersonTrans.AuthorizedPersonTypeGUID equals authorizedPersonType.AuthorizedPersonTypeGUID into ljauthorizedPersonType
							  from authorizedPersonType in ljauthorizedPersonType.DefaultIfEmpty()

							  where authorizedPersonTrans.RefGUID == refGUID && authorizedPersonTrans.RefType == (int)RefType.CreditAppRequestTable
							  && authorizedPersonTrans.RefAuthorizedPersonTransGUID != null
							  orderby authorizedPersonTrans.Ordering ascending
							  select new QAuthorizedCustOldCusAmendInfo
							  {
								  QAuthorizedCustOldCusAmendInfo_AuthorizedCustOld = relatedPersonTable.Name,
								  QAuthorizedCustOldCusAmendInfo_AuthorizedPersonTypeGUID = authorizedPersonType.AuthorizedPersonTypeGUID,
								  QAuthorizedCustOldCusAmendInfo_AuthorizedPositionCustNew = authorizedPersonType.Description,
								  QAuthorizedCustOldCusAmendInfo_Row = 0
							  }

					).Take(6).ToList();
				var row_number = 0;
				foreach (var item in result)
				{
					item.QAuthorizedCustOldCusAmendInfo_Row = row_number + 1;
					row_number = row_number + 1;
				}
				if (result != null)
				{
					return result;
				}
				else
				{
					return new List<QAuthorizedCustOldCusAmendInfo>();

				}


			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private List<QRetentionConditionTransCusAmendInfo> GetQRetentionConditionTransCusAmendInfo(Guid? qCreditAppTableCusAmendInfo_CreditAppTableGUID)
		{
			try
			{
				var result = (from retentionConditionTrans in db.Set<RetentionConditionTrans>()
							  where retentionConditionTrans.RefGUID == qCreditAppTableCusAmendInfo_CreditAppTableGUID
							  && retentionConditionTrans.RefType == (int)RefType.CreditAppTable
							  orderby retentionConditionTrans.RetentionDeductionMethod ascending
							  select new QRetentionConditionTransCusAmendInfo
							  {
								  QRetentionConditionTransCusAmendInfo_RetentionDeductionMethod = retentionConditionTrans.RetentionDeductionMethod,
								  QRetentionConditionTransCusAmendInfo_RetentionCalculateBase = retentionConditionTrans.RetentionCalculateBase,
								  QRetentionConditionTransCusAmendInfo_RetentionPct = retentionConditionTrans.RetentionPct,
								  QRetentionConditionTransCusAmendInfo_RetentionAmount = retentionConditionTrans.RetentionAmount,
								  QRetentionConditionTransCusAmendInfo_Row = 0
							  }
					).Take(3).ToList();
				var row_number = 0;
				foreach (var item in result)
				{
					item.QRetentionConditionTransCusAmendInfo_Row = row_number + 1;
					row_number = row_number + 1;
				}
				if (result != null)
				{
					return result;
				}
				else
				{
					return new List<QRetentionConditionTransCusAmendInfo>();

				}

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		//private QCreditAppTableCusAmendInfo GetQCreditAppTableCusAmendInfo(Guid refGUID)
		//{
		//	try
		//	{
		//		var result = (from crediappTable in db.Set<CreditAppTable>()
		//					  where crediappTable.RefCreditAppRequestTableGUID == refGUID
		//					  select new QCreditAppTableCusAmendInfo
		//					  {
		//						  QCreditAppTableCusAmendInfo_CreditAppTableGUID = crediappTable.CreditAppTableGUID,
		//						  QCreditAppTableCusAmendInfo_MaxRetentionPct = crediappTable.MaxRetentionPct,
		//						  QCreditAppTableCusAmendInfo_MaxRetentionAmount = crediappTable.MaxRetentionAmount,

		//					  }
		//			).FirstOrDefault();
		//		if (result != null)
		//		{
		//			return result;

		//		}
		//		else
		//		{
		//			return new QCreditAppTableCusAmendInfo();

		//		}
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}

		private QCreditAppRequestTableAmendCusAmendInfo GetQCreditAppRequestTableAmendCusAmendInfo(Guid refGUID)
		{
			try
			{
				var result = (from creditAppRequestTableAmend in db.Set<CreditAppRequestTableAmend>()
							  join interrestType in db.Set<InterestType>()
							  on creditAppRequestTableAmend.OriginalInterestTypeGUID equals interrestType.InterestTypeGUID into ljInterestType
							  from interrestType in ljInterestType.DefaultIfEmpty()
							  where creditAppRequestTableAmend.CreditAppRequestTableGUID == refGUID
							  select new QCreditAppRequestTableAmendCusAmendInfo
							  {
								  QCreditAppRequestTableAmendCusAmendInfo_OriginalMaxRetentionPct = creditAppRequestTableAmend.OriginalMaxRetentionPct,
								  QCreditAppRequestTableAmendCusAmendInfo_OriginalMaxRetentionAmount = creditAppRequestTableAmend.OriginalMaxRetentionAmount,
								  QCreditAppRequestTableAmendCusAmendInfo_OriginalPurchaseFeePct = creditAppRequestTableAmend.OriginalPurchaseFeePct,
								  QCreditAppRequestTableAmendCusAmendInfo_OriginalPurchaseFeeCalculateBase = creditAppRequestTableAmend.OriginalPurchaseFeeCalculateBase,
							      QCreditAppRequestTableAmendCusAmendInfo_OriginalIntersetType = interrestType.Description,
								  QCreditAppRequestTableAmendCusAmendInfo_OriginalTotalLnterestPct = creditAppRequestTableAmend.OriginalTotalInterestPct,
								  // R02
								  QCreditAppRequestTableAmendCusAmendInfo_OriginalInterestAdjustmentPct = creditAppRequestTableAmend.OriginalInterestAdjustmentPct,
								  QCreditAppRequestTableAmendCusAmendInfo_OriginalCreditLimitRequestFeeAmount = creditAppRequestTableAmend.OriginalCreditLimitRequestFeeAmount,
								  QCreditAppRequestTableAmendCusAmendInfo_OriginalMaxPurchasePct = creditAppRequestTableAmend.OriginalMaxPurchasePct,
							  }
							  ).FirstOrDefault();
				if (result != null)
				{
					return result;

				}
				else
				{
					return new QCreditAppRequestTableAmendCusAmendInfo();

				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private QBankAccountControlCusAmendInfo GetQBankAccountControlCusAmendInfo(Guid? qCreditAppRequestTableCusAmnedInfo_BankAccountControlGUID)
		{
			try
			{
				var result = (from custBank in db.Set<CustBank>()
							  join bankGroup in db.Set<BankGroup>()
							  on custBank.BankGroupGUID equals bankGroup.BankGroupGUID into ljbankGroup
							  from bankGroup in ljbankGroup.DefaultIfEmpty()

							  join bankType in db.Set<BankType>()
							  on custBank.BankTypeGUID equals bankType.BankTypeGUID into ljbankType
							  from bankType in ljbankType.DefaultIfEmpty()
							  where custBank.CustBankGUID == qCreditAppRequestTableCusAmnedInfo_BankAccountControlGUID
							  select new QBankAccountControlCusAmendInfo
							  {
								  QBankAccountControlCusAmendInfo_CustBankName = bankGroup.Description,
								  QBankAccountControlCusAmendInfo_CustBankBranch = custBank.BankBranch,
								  QBankAccountControlCusAmendInfo_CustBankType = bankType.Description,
								  QBankAccountControlCusAmendInfo_CustBankAccountName = custBank.AccountNumber
							  }).FirstOrDefault();
				if (result != null)
				{
					return result;

				}
				else
				{
					return new QBankAccountControlCusAmendInfo();

				}

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private QCustomerCusAmendInfo GetQCustomerCusAmendInfo(Guid? qCreditAppRequestTableCusAmnedInfo_CustomerTableGUID)
		{
			try
			{
				var result = (from customerTable in db.Set<CustomerTable>()
							  join employeeTable in db.Set<EmployeeTable>()
							  on customerTable.ResponsibleByGUID equals employeeTable.EmployeeTableGUID into ljemployeeTable
							  from employeeTable in ljemployeeTable.DefaultIfEmpty()

							  join businessType in db.Set<BusinessType>()
							  on customerTable.BusinessTypeGUID equals businessType.BusinessTypeGUID into ljbusinessType
							  from businessType in ljbusinessType.DefaultIfEmpty()

							  join introduced in db.Set<IntroducedBy>()
							  on customerTable.IntroducedByGUID equals introduced.IntroducedByGUID into ljintroduced
							  from introduced in ljintroduced.DefaultIfEmpty()
							  where customerTable.CustomerTableGUID == qCreditAppRequestTableCusAmnedInfo_CustomerTableGUID
							  select new QCustomerCusAmendInfo
							  {
								  QCustomerCusAmendInfo_CustomerId = customerTable.CustomerId,
								  QCustomerCusAmendInfo_CustomerName = customerTable.Name,
								  QCustomerCusAmendInfo_IdentificationType = customerTable.IdentificationType,
								  QCustomerCusAmendInfo_TaxID = customerTable.TaxID,
								  QCustomerCusAmendInfo_PassportID = customerTable.PassportID,
								  QCustomerCusAmendInfo_RecordType = customerTable.RecordType,
								  QCustomerCusAmendInfo_DateOfEstablish = customerTable.DateOfEstablish,
								  QCustomerCusAmendInfo_DateOfBirth = customerTable.DateOfBirth,
								  QCustomerCusAmendInfo_IntroducedByGUID = customerTable.IntroducedByGUID,
								  QCustomerCusAmendInfo_BusinessTypeGUID = customerTable.BusinessTypeGUID,
								  QCustomerCusAmendInfo_ResponsibleByGUID = customerTable.ResponsibleByGUID,
								  QCustomerCusAmendInfo_ResponsibleBy = employeeTable.Name,
								  QCustomerCusAmendInfo_BusinessType = businessType.Description,
								  QCustomerCusAmendInfo_IntroducedBy = introduced.Description
							  }

							  ).FirstOrDefault();
				if (result != null)
				{
					return result;

				}
				else
				{
					return new QCustomerCusAmendInfo();

				}

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private QOriginalCreditAppTableCusAmendInfo GetQOriginalCreditAppTableCusAmendInfo(Guid? qCreditAppRequestTableCusAmnedInfo_RefCreditAppTableGUID)
		{

			try
			{
				var result = (from creditAppTable in db.Set<CreditAppTable>()
							  where creditAppTable.CreditAppTableGUID == qCreditAppRequestTableCusAmnedInfo_RefCreditAppTableGUID
							  select new QOriginalCreditAppTableCusAmendInfo
							  {
								  QOriginalCreditAppTableCusAmendInfo_OriginalCreditAppTableId = creditAppTable.CreditAppId,
								  QOriginalCreditAppTableCusAmendInfo_CreditAppTableGUID = creditAppTable.CreditAppTableGUID,
								  QOriginalCreditAppTableCusAmendInfo_MaxRetentionAmount = creditAppTable.MaxRetentionAmount,
								  QOriginalCreditAppTableCusAmendInfo_MaxRetentionPct = creditAppTable.MaxRetentionPct,
							  }
							  ).FirstOrDefault();
				if (result != null)
				{
					return result;
				}
				else
				{
					return new QOriginalCreditAppTableCusAmendInfo();

				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private QCreditAppRequestTableCusAmnedInfo GetQCreditAppRequestTableCusAmnedInfo(Guid refGUID)
		{
			try
			{
				var result = (from creditAppRequestTable in db.Set<CreditAppRequestTable>()
							  join creditappTable in db.Set<CreditAppTable>()
							  on creditAppRequestTable.RefCreditAppTableGUID equals creditappTable.CreditAppTableGUID into ljcreditappTable
							  from creditappTable in ljcreditappTable.DefaultIfEmpty()

							  join kYCSetup in db.Set<KYCSetup>()
							  on creditAppRequestTable.KYCGUID equals kYCSetup.KYCSetupGUID into ljkYCSetup
							  from kYCSetup in ljkYCSetup.DefaultIfEmpty()

							  join creditScoring in db.Set<CreditScoring>()
							  on creditAppRequestTable.CreditScoringGUID equals creditScoring.CreditScoringGUID into ljcreditScoring
							  from creditScoring in ljcreditScoring.DefaultIfEmpty()

							  join addressTrans in db.Set<AddressTrans>()
							  on creditAppRequestTable.RegisteredAddressGUID equals addressTrans.AddressTransGUID into ljaddressTrans
							  from addressTrans in ljaddressTrans.DefaultIfEmpty()

							  join interestType in db.Set<InterestType>()
							  on creditAppRequestTable.InterestTypeGUID equals interestType.InterestTypeGUID into ljinterType
							  from interestType in ljinterType.DefaultIfEmpty()

							  join interestValue in db.Set<InterestTypeValue>()
							  on creditAppRequestTable.InterestTypeGUID equals interestValue.InterestTypeGUID into ljinterestValue
							  from interestValue in ljinterestValue.DefaultIfEmpty()

							  join documentReson in db.Set<DocumentReason>()
							 on creditAppRequestTable.DocumentReasonGUID equals documentReson.DocumentReasonGUID into ljdocumentReson
							  from documentReson in ljdocumentReson.DefaultIfEmpty()

							  where creditAppRequestTable.CreditAppRequestTableGUID == refGUID

							  select new QCreditAppRequestTableCusAmnedInfo
							  {
								  QCreditAppRequestTableCusAmnedInfo_CreditAppRequestId = creditAppRequestTable.CreditAppRequestId,
								  QCreditAppRequestTableCusAmnedInfo_RefCreditAppTableGUID = creditAppRequestTable.RefCreditAppTableGUID,
								  QCreditAppRequestTableCusAmnedInfo_OriginalCreditAppTableId = creditappTable.CreditAppId,
								  QCreditAppRequestTableCusAmnedInfo_RequestDate = creditAppRequestTable.RequestDate,
								  QCreditAppRequestTableCusAmnedInfo_CustomerTableGUID = creditAppRequestTable.CustomerTableGUID,
								  QCreditAppRequestTableCusAmnedInfo_Description = creditAppRequestTable.Description,
								  QCreditAppRequestTableCusAmnedInfo_Remark = creditAppRequestTable.Remark,
								  QCreditAppRequestTableCusAmnedInfo_DocumentStatusGUID = creditAppRequestTable.DocumentStatusGUID,
								  QCreditAppRequestTableCusAmnedInfo_KYCSetupGUID = kYCSetup.KYCSetupGUID,
								  QCreditAppRequestTableCusAmnedInfo_KYC = kYCSetup.Description,
								  QCreditAppRequestTableCusAmnedInfo_CreditScoringGUID = creditScoring.CreditScoringGUID,
								  QCreditAppRequestTableCusAmnedInfo_CreditScoring = creditScoring.Description,
								  QCreditAppRequestTableCusAmnedInfo_DocumentReasonGUID = creditAppRequestTable.DocumentReasonGUID,
								  QCreditAppRequestTableCusAmnedInfo_RegisteredAddressGUID = creditAppRequestTable.RegisteredAddressGUID,
								  QCreditAppRequestTableCusAmnedInfo_RegisteredAddress = addressTrans.Address1 + " " + addressTrans.Address2,
								  QCreditAppRequestTableCusAmnedInfo_BankAccountControlGUID = creditAppRequestTable.BankAccountControlGUID,
								  QCreditAppRequestTableCusAmnedInfo_MarketingComment = creditAppRequestTable.MarketingComment,
								  QCreditAppRequestTableCusAmnedInfo_CreditComment = creditAppRequestTable.CreditComment,
								  QCreditAppRequestTableCusAmnedInfo_ApproverComment = creditAppRequestTable.ApproverComment,
								  QCreditAppRequestTableCusAmnedInfo_InterestTypeGUID = creditAppRequestTable.InterestTypeGUID,
								  QCreditAppRequestTableCusAmnedInfo_InterestType = interestType.Description,
								  QCreditAppRequestTableCusAmnedInfo_TotalInterestPct = creditappTable.TotalInterestPct,
								  QCreditAppRequestTableCusAmnedInfo_MaxRetentionPct = creditAppRequestTable.MaxRetentionPct,
								  QCreditAppRequestTableCusAmnedInfo_MaxRetentionAmount = creditAppRequestTable.MaxRetentionAmount,
								  QCreditAppRequestTableCusAmnedInfo_CreditLimitRequest = creditAppRequestTable.CreditRequestFeeAmount,
								  QCreditAppRequestTableCusAmnedInfo_PurchaseFeePct = creditAppRequestTable.PurchaseFeePct,
								  QCreditAppRequestTableCusAmnedInfo_PurchaseFeeCalculateBase = creditAppRequestTable.PurchaseFeeCalculateBase,
								  QCreditAppRequestTableCusAmnedInfo_InterestId = interestType.InterestTypeId,
								  QCreditAppRequestTableAmendCusAmendInfo_InterestAdjustment = creditAppRequestTable.InterestAdjustment,
								  QCreditAppRequestTableAmendCusAmendInfo_CreditRequestFeeAmount = creditAppRequestTable.CreditRequestFeeAmount,
								  QCreditAppRequestTableAmendCusAmendInfo_MaximumPurchasePct = creditAppRequestTable.MaxPurchasePct,
							  }
							  ).AsNoTracking().FirstOrDefault();
				if (result != null)
				{
					var statusResult = (from documentStatus in db.Set<DocumentStatus>()
										join documentProcess in db.Set<DocumentProcess>()
										on documentStatus.DocumentProcessGUID equals documentProcess.DocumentProcessGUID into ljdocumentProcess
										from documentProcess in ljdocumentProcess.DefaultIfEmpty()
										where documentStatus.DocumentStatusGUID == result.QCreditAppRequestTableCusAmnedInfo_DocumentStatusGUID
										&& documentProcess.ProcessId == ((int)DocumentProcessStatus.CreditAppRequestTable).ToString()
										select documentStatus.Description
										).AsNoTracking().FirstOrDefault();

					result.QCreditAppRequestTableCusAmnedInfo_Status = statusResult;
					return result;
				}
				else
				{

					return new QCreditAppRequestTableCusAmnedInfo();


				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#endregion
		#region QueryBuyerAmendInformation
		public BookmarkDocumentQueryBuyerAmendInformation GetBookmarkDocumentQueryBuyerAmendInformation(Guid refGUID, Guid bookmarkDocumentTransGUID)
		{
			try
			{
				BookmarkDocumentQueryBuyerAmendInformation model = new BookmarkDocumentQueryBuyerAmendInformation();
				QCreditAppRequestTableBuyerAmendInfo qCreditAppRequestTableBuyerAmendInfo = GetQCreditAppRequestTableBuyerAmendInfo(refGUID);
				SetModelByQCreditAppRequestTableBuyexrAmendInfo(model, qCreditAppRequestTableBuyerAmendInfo);

				QCustomerBuyerAmendInfo qCustomerBuyerAmendInfo = GetQCustomerBuyerAmendInfo(qCreditAppRequestTableBuyerAmendInfo);
				SetmodelByQCustomerBuyerAmendInfo(model, qCustomerBuyerAmendInfo);

				QCreditAppRequestLineBuyerAmendInfo qCreditAppRequestLineBuyerAmendInfo = GetQCreditAppRequestLineBuyerAmendInfo(refGUID);
				SetModelByQCreditAppRequestLineBuyerAmendInfo(model, qCreditAppRequestLineBuyerAmendInfo);

				QCreditAppRequestLineAmendBuyerAmendInfo qCreditAppRequestLineAmendBuyerAmendInfo = GetQCreditAppRequestLineAmendBuyerAmendInfo(qCreditAppRequestLineBuyerAmendInfo);
				SetModelByQCreditAppRequestLineAmendBuyerAmendInfo(model, qCreditAppRequestLineAmendBuyerAmendInfo);

				List<QBillingDocumentBuyerAmendInfo> qBillingDocumentBuyerAmendInfo = GetQBillingDocumentBuyerAmendInfo(qCreditAppRequestLineBuyerAmendInfo);
				QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_1 = GetQBillingDocumentBuyerAmendInfoEachRow(qBillingDocumentBuyerAmendInfo, 1);
				SetNodelByqBillingDocumentBuyerAmendInfo_1(model, qBillingDocumentBuyerAmendInfo_1);
				QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_2 = GetQBillingDocumentBuyerAmendInfoEachRow(qBillingDocumentBuyerAmendInfo, 2);
				SetNodelByqBillingDocumentBuyerAmendInfo_2(model, qBillingDocumentBuyerAmendInfo_2);
				QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_3 = GetQBillingDocumentBuyerAmendInfoEachRow(qBillingDocumentBuyerAmendInfo, 3);
				SetNodelByqBillingDocumentBuyerAmendInfo_3(model, qBillingDocumentBuyerAmendInfo_3);
				QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_4 = GetQBillingDocumentBuyerAmendInfoEachRow(qBillingDocumentBuyerAmendInfo, 4);
				SetNodelByqBillingDocumentBuyerAmendInfo_4(model, qBillingDocumentBuyerAmendInfo_4);
				QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_5 = GetQBillingDocumentBuyerAmendInfoEachRow(qBillingDocumentBuyerAmendInfo, 5);
				SetNodelByqBillingDocumentBuyerAmendInfo_5(model, qBillingDocumentBuyerAmendInfo_5);
				QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_6 = GetQBillingDocumentBuyerAmendInfoEachRow(qBillingDocumentBuyerAmendInfo, 6);
				SetNodelByqBillingDocumentBuyerAmendInfo_6(model, qBillingDocumentBuyerAmendInfo_6);
				QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_7 = GetQBillingDocumentBuyerAmendInfoEachRow(qBillingDocumentBuyerAmendInfo, 7);
				SetNodelByqBillingDocumentBuyerAmendInfo_7(model, qBillingDocumentBuyerAmendInfo_7);
				QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_8 = GetQBillingDocumentBuyerAmendInfoEachRow(qBillingDocumentBuyerAmendInfo, 8);
				SetNodelByqBillingDocumentBuyerAmendInfo_8(model, qBillingDocumentBuyerAmendInfo_8);
				QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_9 = GetQBillingDocumentBuyerAmendInfoEachRow(qBillingDocumentBuyerAmendInfo, 9);
				SetNodelByqBillingDocumentBuyerAmendInfo_9(model, qBillingDocumentBuyerAmendInfo_9);
				QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_10 = GetQBillingDocumentBuyerAmendInfoEachRow(qBillingDocumentBuyerAmendInfo, 10);
				SetNodelByqBillingDocumentBuyerAmendInfo_10(model, qBillingDocumentBuyerAmendInfo_10);

				List<QReceiptDocumentBuyerAmendInfo> qReceiptDocumentBuyerAmendInfo = GetQReceiptDocumentBuyerAmendInfo(qCreditAppRequestLineBuyerAmendInfo);
				QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_1 = GetQReceiptDocumentBuyerAmendInfoEachRow(qReceiptDocumentBuyerAmendInfo, 1);
				SetNodelByqReceiptDocumentBuyerAmendInfo_1(model, qReceiptDocumentBuyerAmendInfo_1);
				QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_2 = GetQReceiptDocumentBuyerAmendInfoEachRow(qReceiptDocumentBuyerAmendInfo, 2);
				SetNodelByqReceiptDocumentBuyerAmendInfo_2(model, qReceiptDocumentBuyerAmendInfo_2);
				QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_3 = GetQReceiptDocumentBuyerAmendInfoEachRow(qReceiptDocumentBuyerAmendInfo, 3);
				SetNodelByqReceiptDocumentBuyerAmendInfo_3(model, qReceiptDocumentBuyerAmendInfo_3);
				QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_4 = GetQReceiptDocumentBuyerAmendInfoEachRow(qReceiptDocumentBuyerAmendInfo, 4);
				SetNodelByqReceiptDocumentBuyerAmendInfo_4(model, qReceiptDocumentBuyerAmendInfo_4);
				QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_5 = GetQReceiptDocumentBuyerAmendInfoEachRow(qReceiptDocumentBuyerAmendInfo, 5);
				SetNodelByqReceiptDocumentBuyerAmendInfo_5(model, qReceiptDocumentBuyerAmendInfo_5);
				QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_6 = GetQReceiptDocumentBuyerAmendInfoEachRow(qReceiptDocumentBuyerAmendInfo, 6);
				SetNodelByqReceiptDocumentBuyerAmendInfo_6(model, qReceiptDocumentBuyerAmendInfo_6);
				QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_7 = GetQReceiptDocumentBuyerAmendInfoEachRow(qReceiptDocumentBuyerAmendInfo, 7);
				SetNodelByqReceiptDocumentBuyerAmendInfo_7(model, qReceiptDocumentBuyerAmendInfo_7);
				QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_8 = GetQReceiptDocumentBuyerAmendInfoEachRow(qReceiptDocumentBuyerAmendInfo, 8);
				SetNodelByqReceiptDocumentBuyerAmendInfo_8(model, qReceiptDocumentBuyerAmendInfo_8);
				QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_9 = GetQReceiptDocumentBuyerAmendInfoEachRow(qReceiptDocumentBuyerAmendInfo, 9);
				SetNodelByqReceiptDocumentBuyerAmendInfo_9(model, qReceiptDocumentBuyerAmendInfo_9);
				QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_10 = GetQReceiptDocumentBuyerAmendInfoEachRow(qReceiptDocumentBuyerAmendInfo, 10);
				SetNodelByqReceiptDocumentBuyerAmendInfo_10(model, qReceiptDocumentBuyerAmendInfo_10);

				List<QCreditAppReqLineFinBuyerAmendInfo> QCreditAppReqLineFinBuyerAmendInfo = mainQCreditAppReqLineFinBuyerAmendInfo(qCreditAppRequestLineBuyerAmendInfo);
				QCreditAppReqLineFinBuyerAmendInfo qCreditAppReqLineFinBuyerAmendInfo_1 = GetQCreditAppReqLineFinBuyerAmendInfoEachRow(1, QCreditAppReqLineFinBuyerAmendInfo, qCreditAppRequestLineBuyerAmendInfo);
				SetModelByqCreditAppReqLineFinBuyerAmendInfo_1(model, qCreditAppReqLineFinBuyerAmendInfo_1);
				QCreditAppReqLineFinBuyerAmendInfo qCreditAppReqLineFinBuyerAmendInfo_2 = GetQCreditAppReqLineFinBuyerAmendInfoEachRow(2, QCreditAppReqLineFinBuyerAmendInfo, qCreditAppRequestLineBuyerAmendInfo);
				SetModelByqCreditAppReqLineFinBuyerAmendInfo_2(model, qCreditAppReqLineFinBuyerAmendInfo_2);
				QCreditAppReqLineFinBuyerAmendInfo qCreditAppReqLineFinBuyerAmendInfo_3 = GetQCreditAppReqLineFinBuyerAmendInfoEachRow(3, QCreditAppReqLineFinBuyerAmendInfo, qCreditAppRequestLineBuyerAmendInfo);
				SetModelByqCreditAppReqLineFinBuyerAmendInfo_3(model, qCreditAppReqLineFinBuyerAmendInfo_3);
				SetModelByNewBillingDocConInactive1(model, qBillingDocumentBuyerAmendInfo_1);
				SetModelByNewBillingDocConInactive2(model, qBillingDocumentBuyerAmendInfo_2);
				SetModelByNewBillingDocConInactive3(model, qBillingDocumentBuyerAmendInfo_3);
				SetModelByNewBillingDocConInactive4(model, qBillingDocumentBuyerAmendInfo_4);
				SetModelByNewBillingDocConInactive5(model, qBillingDocumentBuyerAmendInfo_5);
				SetModelByNewBillingDocConInactive6(model, qBillingDocumentBuyerAmendInfo_6);
				SetModelByNewBillingDocConInactive7(model, qBillingDocumentBuyerAmendInfo_7);
				SetModelByNewBillingDocConInactive8(model, qBillingDocumentBuyerAmendInfo_8);
				SetModelByNewBillingDocConInactive9(model, qBillingDocumentBuyerAmendInfo_9);
				SetModelByNewBillingDocConInactive10(model, qBillingDocumentBuyerAmendInfo_10);
				SetModelByNewReceiptDocConInactive1(model, qReceiptDocumentBuyerAmendInfo_1);
				SetModelByNewReceiptDocConInactive2(model, qReceiptDocumentBuyerAmendInfo_2);
				SetModelByNewReceiptDocConInactive3(model, qReceiptDocumentBuyerAmendInfo_3);
				SetModelByNewReceiptDocConInactive4(model, qReceiptDocumentBuyerAmendInfo_4);
				SetModelByNewReceiptDocConInactive5(model, qReceiptDocumentBuyerAmendInfo_5);
				SetModelByNewReceiptDocConInactive6(model, qReceiptDocumentBuyerAmendInfo_6);
				SetModelByNewReceiptDocConInactive7(model, qReceiptDocumentBuyerAmendInfo_7);
				SetModelByNewReceiptDocConInactive8(model, qReceiptDocumentBuyerAmendInfo_8);
				SetModelByNewReceiptDocConInactive9(model, qReceiptDocumentBuyerAmendInfo_9);
				SetModelByNewReceiptDocConInactive10(model, qReceiptDocumentBuyerAmendInfo_10);

				List<QCreditAppReqAssignmentBuyerAmendInformation> qCreditAppReqAssignmentList = GetQCreditAppReqAssignmentBuyerAmendInformation(refGUID);
				QCreditAppReqAssignmentBuyerAmendInformation creditAppReqAssignment_1 = GetSingleQCreditAppReqAssignment(qCreditAppReqAssignmentList,0);
				QCreditAppReqAssignmentBuyerAmendInformation creditAppReqAssignment_2 = GetSingleQCreditAppReqAssignment(qCreditAppReqAssignmentList, 1);
				QCreditAppReqAssignmentBuyerAmendInformation creditAppReqAssignment_3 = GetSingleQCreditAppReqAssignment(qCreditAppReqAssignmentList, 2);
				QCreditAppReqAssignmentBuyerAmendInformation creditAppReqAssignment_4 = GetSingleQCreditAppReqAssignment(qCreditAppReqAssignmentList, 3);
				QCreditAppReqAssignmentBuyerAmendInformation creditAppReqAssignment_5 = GetSingleQCreditAppReqAssignment(qCreditAppReqAssignmentList, 4);
				QCreditAppReqAssignmentBuyerAmendInformation creditAppReqAssignment_6 = GetSingleQCreditAppReqAssignment(qCreditAppReqAssignmentList, 5);
				QCreditAppReqAssignmentBuyerAmendInformation creditAppReqAssignment_7 = GetSingleQCreditAppReqAssignment(qCreditAppReqAssignmentList, 6);
				QCreditAppReqAssignmentBuyerAmendInformation creditAppReqAssignment_8 = GetSingleQCreditAppReqAssignment(qCreditAppReqAssignmentList, 7);
				QCreditAppReqAssignmentBuyerAmendInformation creditAppReqAssignment_9 = GetSingleQCreditAppReqAssignment(qCreditAppReqAssignmentList, 8);
				QCreditAppReqAssignmentBuyerAmendInformation creditAppReqAssignment_10 = GetSingleQCreditAppReqAssignment(qCreditAppReqAssignmentList, 9);
				SetModelByQCreditAppReqAssignment1(model, creditAppReqAssignment_1);
				SetModelByQCreditAppReqAssignment2(model, creditAppReqAssignment_2);
				SetModelByQCreditAppReqAssignment3(model, creditAppReqAssignment_3);
				SetModelByQCreditAppReqAssignment4(model, creditAppReqAssignment_4);
				SetModelByQCreditAppReqAssignment5(model, creditAppReqAssignment_5);
				SetModelByQCreditAppReqAssignment6(model, creditAppReqAssignment_6);
				SetModelByQCreditAppReqAssignment7(model, creditAppReqAssignment_7);
				SetModelByQCreditAppReqAssignment8(model, creditAppReqAssignment_8);
				SetModelByQCreditAppReqAssignment9(model, creditAppReqAssignment_9);
				SetModelByQCreditAppReqAssignment10(model, creditAppReqAssignment_10);

				SetModelByConvertVariableQCreditAppReqAssignment(model
					, creditAppReqAssignment_1
					, creditAppReqAssignment_2
					, creditAppReqAssignment_3
					, creditAppReqAssignment_4
					, creditAppReqAssignment_5
					, creditAppReqAssignment_6
					, creditAppReqAssignment_7
					, creditAppReqAssignment_8
					, creditAppReqAssignment_9
					, creditAppReqAssignment_10
					);

				QSumCreditAppReqAssignmentBuyerAmendInformation qSumCreditAppReqAssignmentBuyerAmendInformation = GetQSumCreditAppReqAssignmentBuyerAmendInformation(refGUID);
				SetModelByQSumCreditAppReqAssignment(model, qSumCreditAppReqAssignmentBuyerAmendInformation);

				List<QCreditAppReqBusinessCollateralBuyerAmendInformation> qCreditAppReqBusinessCollateralList = GetQCreditAppReqBusinessCollateralBuyerAmendInformation(refGUID);
				QCreditAppReqBusinessCollateralBuyerAmendInformation qCreditAppReqBusinessCollateral_1 = GetSingleQCreditAppReqBusinessCollateral(qCreditAppReqBusinessCollateralList, 0);
				QCreditAppReqBusinessCollateralBuyerAmendInformation qCreditAppReqBusinessCollateral_2 = GetSingleQCreditAppReqBusinessCollateral(qCreditAppReqBusinessCollateralList, 1);
				QCreditAppReqBusinessCollateralBuyerAmendInformation qCreditAppReqBusinessCollateral_3 = GetSingleQCreditAppReqBusinessCollateral(qCreditAppReqBusinessCollateralList, 2);
				QCreditAppReqBusinessCollateralBuyerAmendInformation qCreditAppReqBusinessCollateral_4 = GetSingleQCreditAppReqBusinessCollateral(qCreditAppReqBusinessCollateralList, 3);
				QCreditAppReqBusinessCollateralBuyerAmendInformation qCreditAppReqBusinessCollateral_5 = GetSingleQCreditAppReqBusinessCollateral(qCreditAppReqBusinessCollateralList, 4);
				QCreditAppReqBusinessCollateralBuyerAmendInformation qCreditAppReqBusinessCollateral_6 = GetSingleQCreditAppReqBusinessCollateral(qCreditAppReqBusinessCollateralList, 5);
				QCreditAppReqBusinessCollateralBuyerAmendInformation qCreditAppReqBusinessCollateral_7 = GetSingleQCreditAppReqBusinessCollateral(qCreditAppReqBusinessCollateralList, 6);
				QCreditAppReqBusinessCollateralBuyerAmendInformation qCreditAppReqBusinessCollateral_8 = GetSingleQCreditAppReqBusinessCollateral(qCreditAppReqBusinessCollateralList, 7);
				QCreditAppReqBusinessCollateralBuyerAmendInformation qCreditAppReqBusinessCollateral_9 = GetSingleQCreditAppReqBusinessCollateral(qCreditAppReqBusinessCollateralList, 8);
				QCreditAppReqBusinessCollateralBuyerAmendInformation qCreditAppReqBusinessCollateral_10 = GetSingleQCreditAppReqBusinessCollateral(qCreditAppReqBusinessCollateralList, 9);
				SetModelByQCreditAppReqBusinessCollateral_1(model, qCreditAppReqBusinessCollateral_1);
				SetModelByQCreditAppReqBusinessCollateral_2(model, qCreditAppReqBusinessCollateral_2);
				SetModelByQCreditAppReqBusinessCollateral_3(model, qCreditAppReqBusinessCollateral_3);
				SetModelByQCreditAppReqBusinessCollateral_4(model, qCreditAppReqBusinessCollateral_4);
				SetModelByQCreditAppReqBusinessCollateral_5(model, qCreditAppReqBusinessCollateral_5);
				SetModelByQCreditAppReqBusinessCollateral_6(model, qCreditAppReqBusinessCollateral_6);
				SetModelByQCreditAppReqBusinessCollateral_7(model, qCreditAppReqBusinessCollateral_7);
				SetModelByQCreditAppReqBusinessCollateral_8(model, qCreditAppReqBusinessCollateral_8);
				SetModelByQCreditAppReqBusinessCollateral_9(model, qCreditAppReqBusinessCollateral_9);
				SetModelByQCreditAppReqBusinessCollateral_10(model, qCreditAppReqBusinessCollateral_10);

				SetModelByConvertVariableQCreditAppReqBusinessCollateral(model
					, qCreditAppReqBusinessCollateral_1
					, qCreditAppReqBusinessCollateral_2
					, qCreditAppReqBusinessCollateral_3
					, qCreditAppReqBusinessCollateral_4
					, qCreditAppReqBusinessCollateral_5
					, qCreditAppReqBusinessCollateral_6
					, qCreditAppReqBusinessCollateral_7
					, qCreditAppReqBusinessCollateral_8
					, qCreditAppReqBusinessCollateral_9
					, qCreditAppReqBusinessCollateral_10
					);

				QSumCreditAppReqBusinessCollateralBuyerAmendInformation qSumCreditAppReqBusinessCollateral = GetQSumCreditAppReqBusinessCollateral(refGUID);
				SetModelByQSumCreditAppReqBusinessCollateral(model, qSumCreditAppReqBusinessCollateral);

				List<QBillingDocumentOldBuyerAmendInformation> qBillingDocumentOldList = GetQBillingDocumentOldBuyerAmendInformation(qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID);
				QBillingDocumentOldBuyerAmendInformation qBillingDocumentOld_1 = GetSingleQBillingDocumentOld(qBillingDocumentOldList, 0);
				QBillingDocumentOldBuyerAmendInformation qBillingDocumentOld_2 = GetSingleQBillingDocumentOld(qBillingDocumentOldList, 1);
				QBillingDocumentOldBuyerAmendInformation qBillingDocumentOld_3 = GetSingleQBillingDocumentOld(qBillingDocumentOldList, 2);
				QBillingDocumentOldBuyerAmendInformation qBillingDocumentOld_4 = GetSingleQBillingDocumentOld(qBillingDocumentOldList, 3);
				QBillingDocumentOldBuyerAmendInformation qBillingDocumentOld_5 = GetSingleQBillingDocumentOld(qBillingDocumentOldList, 4);
				QBillingDocumentOldBuyerAmendInformation qBillingDocumentOld_6 = GetSingleQBillingDocumentOld(qBillingDocumentOldList, 5);
				QBillingDocumentOldBuyerAmendInformation qBillingDocumentOld_7 = GetSingleQBillingDocumentOld(qBillingDocumentOldList, 6);
				QBillingDocumentOldBuyerAmendInformation qBillingDocumentOld_8 = GetSingleQBillingDocumentOld(qBillingDocumentOldList, 7);
				QBillingDocumentOldBuyerAmendInformation qBillingDocumentOld_9 = GetSingleQBillingDocumentOld(qBillingDocumentOldList, 8);
				QBillingDocumentOldBuyerAmendInformation qBillingDocumentOld_10 = GetSingleQBillingDocumentOld(qBillingDocumentOldList, 9);
				model.QBillingDocumentOld_DocumentType_1 = GetQBillingDocumentOldValue(qBillingDocumentOld_1);
				model.QBillingDocumentOld_DocumentType_2 = GetQBillingDocumentOldValue(qBillingDocumentOld_2);
				model.QBillingDocumentOld_DocumentType_3 = GetQBillingDocumentOldValue(qBillingDocumentOld_3);
				model.QBillingDocumentOld_DocumentType_4 = GetQBillingDocumentOldValue(qBillingDocumentOld_4);
				model.QBillingDocumentOld_DocumentType_5 = GetQBillingDocumentOldValue(qBillingDocumentOld_5);
				model.QBillingDocumentOld_DocumentType_6 = GetQBillingDocumentOldValue(qBillingDocumentOld_6);
				model.QBillingDocumentOld_DocumentType_7 = GetQBillingDocumentOldValue(qBillingDocumentOld_7);
				model.QBillingDocumentOld_DocumentType_8 = GetQBillingDocumentOldValue(qBillingDocumentOld_8);
				model.QBillingDocumentOld_DocumentType_9 = GetQBillingDocumentOldValue(qBillingDocumentOld_9);
				model.QBillingDocumentOld_DocumentType_10 = GetQBillingDocumentOldValue(qBillingDocumentOld_10);


				List<QReceiptDocumentOldBuyerAmendInformation> qReceiptDocumentOldList = GetQReceiptDocumentOldBuyerAmendInformation(qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID);
				QReceiptDocumentOldBuyerAmendInformation qReceiptDocumentOld_1 = GetSingleQReceiptDocumentOld(qReceiptDocumentOldList, 0);
				QReceiptDocumentOldBuyerAmendInformation qReceiptDocumentOld_2 = GetSingleQReceiptDocumentOld(qReceiptDocumentOldList, 1);
				QReceiptDocumentOldBuyerAmendInformation qReceiptDocumentOld_3 = GetSingleQReceiptDocumentOld(qReceiptDocumentOldList, 2);
				QReceiptDocumentOldBuyerAmendInformation qReceiptDocumentOld_4 = GetSingleQReceiptDocumentOld(qReceiptDocumentOldList, 3);
				QReceiptDocumentOldBuyerAmendInformation qReceiptDocumentOld_5 = GetSingleQReceiptDocumentOld(qReceiptDocumentOldList, 4);
				QReceiptDocumentOldBuyerAmendInformation qReceiptDocumentOld_6 = GetSingleQReceiptDocumentOld(qReceiptDocumentOldList, 5);
				QReceiptDocumentOldBuyerAmendInformation qReceiptDocumentOld_7 = GetSingleQReceiptDocumentOld(qReceiptDocumentOldList, 6);
				QReceiptDocumentOldBuyerAmendInformation qReceiptDocumentOld_8 = GetSingleQReceiptDocumentOld(qReceiptDocumentOldList, 7);
				QReceiptDocumentOldBuyerAmendInformation qReceiptDocumentOld_9 = GetSingleQReceiptDocumentOld(qReceiptDocumentOldList, 8);
				QReceiptDocumentOldBuyerAmendInformation qReceiptDocumentOld_10 = GetSingleQReceiptDocumentOld(qReceiptDocumentOldList, 9);
				model.QReceiptDocumentOld_DocumentType_1 = GetQReceiptDocumentOldValue(qReceiptDocumentOld_1);
				model.QReceiptDocumentOld_DocumentType_2 = GetQReceiptDocumentOldValue(qReceiptDocumentOld_2);
				model.QReceiptDocumentOld_DocumentType_3 = GetQReceiptDocumentOldValue(qReceiptDocumentOld_3);
				model.QReceiptDocumentOld_DocumentType_4 = GetQReceiptDocumentOldValue(qReceiptDocumentOld_4);
				model.QReceiptDocumentOld_DocumentType_5 = GetQReceiptDocumentOldValue(qReceiptDocumentOld_5);
				model.QReceiptDocumentOld_DocumentType_6 = GetQReceiptDocumentOldValue(qReceiptDocumentOld_6);
				model.QReceiptDocumentOld_DocumentType_7 = GetQReceiptDocumentOldValue(qReceiptDocumentOld_7);
				model.QReceiptDocumentOld_DocumentType_8 = GetQReceiptDocumentOldValue(qReceiptDocumentOld_8);
				model.QReceiptDocumentOld_DocumentType_9 = GetQReceiptDocumentOldValue(qReceiptDocumentOld_9);
				model.QReceiptDocumentOld_DocumentType_10 = GetQReceiptDocumentOldValue(qReceiptDocumentOld_10);

				model.Variable_BuyerAverageSalesPerMonth = GetCalcBuyerAverageSalesPerMonth(qCreditAppRequestTableBuyerAmendInfo.QCreditAppRequestTableBuyerAmendInfo_RefCreditAppTableGUID);

				return model;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private decimal GetCalcBuyerAverageSalesPerMonth(Guid? refGUID)
        {
			
				try
			{
                if (refGUID == null)
                {
					return 0;
                }
                else
                {
					ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
					return creditAppLineRepo.GetCALineOutstandingByCA((int)RefType.CreditAppTable, (Guid)refGUID, false, null).Sum(su=>su.CreditLimitBalance);
				}
				
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

        }

        private void SetModelByConvertVariableQCreditAppReqBusinessCollateral(BookmarkDocumentQueryBuyerAmendInformation model, QCreditAppReqBusinessCollateralBuyerAmendInformation v1, QCreditAppReqBusinessCollateralBuyerAmendInformation v2, QCreditAppReqBusinessCollateralBuyerAmendInformation v3, QCreditAppReqBusinessCollateralBuyerAmendInformation v4, QCreditAppReqBusinessCollateralBuyerAmendInformation v5, QCreditAppReqBusinessCollateralBuyerAmendInformation v6, QCreditAppReqBusinessCollateralBuyerAmendInformation v7, QCreditAppReqBusinessCollateralBuyerAmendInformation v8, QCreditAppReqBusinessCollateralBuyerAmendInformation v9, QCreditAppReqBusinessCollateralBuyerAmendInformation v10)
        {
			try
			{
				model.Variable_BusinessCollateralIsNew1 = CompairIsNew(v1.QCreditAppReqBusinessCollateral_IsNew);
				model.Variable_BusinessCollateralIsNew2 = CompairIsNew(v2.QCreditAppReqBusinessCollateral_IsNew);
				model.Variable_BusinessCollateralIsNew3 = CompairIsNew(v3.QCreditAppReqBusinessCollateral_IsNew);
				model.Variable_BusinessCollateralIsNew4 = CompairIsNew(v4.QCreditAppReqBusinessCollateral_IsNew);
				model.Variable_BusinessCollateralIsNew5 = CompairIsNew(v5.QCreditAppReqBusinessCollateral_IsNew);
				model.Variable_BusinessCollateralIsNew6 = CompairIsNew(v6.QCreditAppReqBusinessCollateral_IsNew);
				model.Variable_BusinessCollateralIsNew7 = CompairIsNew(v7.QCreditAppReqBusinessCollateral_IsNew);
				model.Variable_BusinessCollateralIsNew8 = CompairIsNew(v8.QCreditAppReqBusinessCollateral_IsNew);
				model.Variable_BusinessCollateralIsNew9 = CompairIsNew(v9.QCreditAppReqBusinessCollateral_IsNew);
				model.Variable_BusinessCollateralIsNew10 = CompairIsNew(v10.QCreditAppReqBusinessCollateral_IsNew);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
			
		}

        private void SetModelByConvertVariableQCreditAppReqAssignment(BookmarkDocumentQueryBuyerAmendInformation model, QCreditAppReqAssignmentBuyerAmendInformation v1, QCreditAppReqAssignmentBuyerAmendInformation v2, QCreditAppReqAssignmentBuyerAmendInformation v3, QCreditAppReqAssignmentBuyerAmendInformation v4, QCreditAppReqAssignmentBuyerAmendInformation v5, QCreditAppReqAssignmentBuyerAmendInformation v6, QCreditAppReqAssignmentBuyerAmendInformation v7, QCreditAppReqAssignmentBuyerAmendInformation v8, QCreditAppReqAssignmentBuyerAmendInformation v9, QCreditAppReqAssignmentBuyerAmendInformation v10)
        {
			try
			{
				model.Variable_AssignmentNew1 = CompairIsNew(v1.QCreditAppReqAssignment_IsNew);
				model.Variable_AssignmentNew2 = CompairIsNew(v2.QCreditAppReqAssignment_IsNew);
				model.Variable_AssignmentNew3 = CompairIsNew(v3.QCreditAppReqAssignment_IsNew);
				model.Variable_AssignmentNew4 = CompairIsNew(v4.QCreditAppReqAssignment_IsNew);
				model.Variable_AssignmentNew5 = CompairIsNew(v5.QCreditAppReqAssignment_IsNew);
				model.Variable_AssignmentNew6 = CompairIsNew(v6.QCreditAppReqAssignment_IsNew);
				model.Variable_AssignmentNew7 = CompairIsNew(v7.QCreditAppReqAssignment_IsNew);
				model.Variable_AssignmentNew8 = CompairIsNew(v8.QCreditAppReqAssignment_IsNew);
				model.Variable_AssignmentNew9 = CompairIsNew(v9.QCreditAppReqAssignment_IsNew);
				model.Variable_AssignmentNew10 = CompairIsNew(v10.QCreditAppReqAssignment_IsNew);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
			
		}

        private string CompairIsNew(bool isNew)
        {
			try
			{
				return isNew ? "Yes" : "No";
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private void SetModelByQSumCreditAppReqAssignment(BookmarkDocumentQueryBuyerAmendInformation model, QSumCreditAppReqAssignmentBuyerAmendInformation qSumCreditAppReqAssignmentBuyerAmendInformation)
        {
			try
			{
				model.QSumCreditAppReqAssignment_SumBuyerAgreementAmount = qSumCreditAppReqAssignmentBuyerAmendInformation.QSumCreditAppReqAssignment_SumBuyerAgreementAmount;
				model.QSumCreditAppReqAssignment_SumAssignmentAgreementAmount = qSumCreditAppReqAssignmentBuyerAmendInformation.QSumCreditAppReqAssignment_SumAssignmentAgreementAmount;
				model.QSumCreditAppReqAssignment_SumRemainingAmount = qSumCreditAppReqAssignmentBuyerAmendInformation.QSumCreditAppReqAssignment_SumRemainingAmount;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private void SetModelByQSumCreditAppReqBusinessCollateral(BookmarkDocumentQueryBuyerAmendInformation model, QSumCreditAppReqBusinessCollateralBuyerAmendInformation qSumCreditAppReqBusinessCollateral)
        {
			try
			{

				model.QSumCreditAppReqBusinessCollateral_SumBusinessCollateralValue = qSumCreditAppReqBusinessCollateral.QSumCreditAppReqBusinessCollateral_SumBusinessCollateralValue;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #region SetModelByQCreditAppReqBusinessCollateral
        private void SetModelByQCreditAppReqBusinessCollateral_1(BookmarkDocumentQueryBuyerAmendInformation model, QCreditAppReqBusinessCollateralBuyerAmendInformation qCreditAppReqBusinessCollateral)
        {
			try
			{
				model.QCreditAppReqBusinessCollateral_BusinessCollateralType_1 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralType;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralSubType_1 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralSubType;
				model.QCreditAppReqBusinessCollateral_Description_1 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_Description;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralValue_1 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralValue;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQCreditAppReqBusinessCollateral_2(BookmarkDocumentQueryBuyerAmendInformation model, QCreditAppReqBusinessCollateralBuyerAmendInformation qCreditAppReqBusinessCollateral)
		{
			try
			{
				model.QCreditAppReqBusinessCollateral_BusinessCollateralType_2 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralType;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralSubType_2 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralSubType;
				model.QCreditAppReqBusinessCollateral_Description_2 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_Description;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralValue_2 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralValue;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByQCreditAppReqBusinessCollateral_3(BookmarkDocumentQueryBuyerAmendInformation model, QCreditAppReqBusinessCollateralBuyerAmendInformation qCreditAppReqBusinessCollateral)
		{
			try
			{
				model.QCreditAppReqBusinessCollateral_BusinessCollateralType_3 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralType;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralSubType_3 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralSubType;
				model.QCreditAppReqBusinessCollateral_Description_3 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_Description;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralValue_3 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralValue;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByQCreditAppReqBusinessCollateral_4(BookmarkDocumentQueryBuyerAmendInformation model, QCreditAppReqBusinessCollateralBuyerAmendInformation qCreditAppReqBusinessCollateral)
		{
			try
			{
				model.QCreditAppReqBusinessCollateral_BusinessCollateralType_4 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralType;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralSubType_4 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralSubType;
				model.QCreditAppReqBusinessCollateral_Description_4 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_Description;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralValue_4 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralValue;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByQCreditAppReqBusinessCollateral_5(BookmarkDocumentQueryBuyerAmendInformation model, QCreditAppReqBusinessCollateralBuyerAmendInformation qCreditAppReqBusinessCollateral)
		{
			try
			{
				model.QCreditAppReqBusinessCollateral_BusinessCollateralType_5 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralType;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralSubType_5 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralSubType;
				model.QCreditAppReqBusinessCollateral_Description_5 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_Description;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralValue_5 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralValue;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByQCreditAppReqBusinessCollateral_6(BookmarkDocumentQueryBuyerAmendInformation model, QCreditAppReqBusinessCollateralBuyerAmendInformation qCreditAppReqBusinessCollateral)
		{
			try
			{
				model.QCreditAppReqBusinessCollateral_BusinessCollateralType_6 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralType;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralSubType_6 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralSubType;
				model.QCreditAppReqBusinessCollateral_Description_6 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_Description;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralValue_6 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralValue;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByQCreditAppReqBusinessCollateral_7(BookmarkDocumentQueryBuyerAmendInformation model, QCreditAppReqBusinessCollateralBuyerAmendInformation qCreditAppReqBusinessCollateral)
		{
			try
			{
				model.QCreditAppReqBusinessCollateral_BusinessCollateralType_7 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralType;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralSubType_7 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralSubType;
				model.QCreditAppReqBusinessCollateral_Description_7 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_Description;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralValue_7 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralValue;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByQCreditAppReqBusinessCollateral_8(BookmarkDocumentQueryBuyerAmendInformation model, QCreditAppReqBusinessCollateralBuyerAmendInformation qCreditAppReqBusinessCollateral)
		{
			try
			{
				model.QCreditAppReqBusinessCollateral_BusinessCollateralType_8 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralType;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralSubType_8 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralSubType;
				model.QCreditAppReqBusinessCollateral_Description_8 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_Description;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralValue_8 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralValue;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByQCreditAppReqBusinessCollateral_9(BookmarkDocumentQueryBuyerAmendInformation model, QCreditAppReqBusinessCollateralBuyerAmendInformation qCreditAppReqBusinessCollateral)
		{
			try
			{
				model.QCreditAppReqBusinessCollateral_BusinessCollateralType_9 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralType;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralSubType_9 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralSubType;
				model.QCreditAppReqBusinessCollateral_Description_9 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_Description;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralValue_9 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralValue;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByQCreditAppReqBusinessCollateral_10(BookmarkDocumentQueryBuyerAmendInformation model, QCreditAppReqBusinessCollateralBuyerAmendInformation qCreditAppReqBusinessCollateral)
		{
			try
			{
				model.QCreditAppReqBusinessCollateral_BusinessCollateralType_10 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralType;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralSubType_10 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralSubType;
				model.QCreditAppReqBusinessCollateral_Description_10 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_Description;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralValue_10 = qCreditAppReqBusinessCollateral.QCreditAppReqBusinessCollateral_BusinessCollateralValue;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		#endregion SetModelByQCreditAppReqBusinessCollateral

		#region SetModelByQCreditAppReqAssignment
		private void SetModelByQCreditAppReqAssignment1(BookmarkDocumentQueryBuyerAmendInformation model, QCreditAppReqAssignmentBuyerAmendInformation creditAppReqAssignment)
        {
			try
			{
				model.QCreditAppReqAssignment_AssignmentBuyer_1 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentBuyer;
				model.QCreditAppReqAssignment_AssignmentRefAgreementId_1 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentRefAgreementId;
				model.QCreditAppReqAssignment_AssignmentBuyerAgreementAmount_1 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentBuyerAgreementAmount;
				model.QCreditAppReqAssignment_AssignmentAgreementAmount_1 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentAgreementAmount;
				model.QCreditAppReqAssignment_AssignmentAgreementDate_1 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentAgreementDate;
				model.QCreditAppReqAssignment_RemainingAmount_1 = creditAppReqAssignment.QCreditAppReqAssignment_RemainingAmount;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQCreditAppReqAssignment2(BookmarkDocumentQueryBuyerAmendInformation model, QCreditAppReqAssignmentBuyerAmendInformation creditAppReqAssignment)
		{
			try
			{
				model.QCreditAppReqAssignment_AssignmentBuyer_2 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentBuyer;
				model.QCreditAppReqAssignment_AssignmentRefAgreementId_2 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentRefAgreementId;
				model.QCreditAppReqAssignment_AssignmentBuyerAgreementAmount_2 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentBuyerAgreementAmount;
				model.QCreditAppReqAssignment_AssignmentAgreementAmount_2 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentAgreementAmount;
				model.QCreditAppReqAssignment_AssignmentAgreementDate_2 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentAgreementDate;
				model.QCreditAppReqAssignment_RemainingAmount_2 = creditAppReqAssignment.QCreditAppReqAssignment_RemainingAmount;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQCreditAppReqAssignment3(BookmarkDocumentQueryBuyerAmendInformation model, QCreditAppReqAssignmentBuyerAmendInformation creditAppReqAssignment)
		{
			try
			{
				model.QCreditAppReqAssignment_AssignmentBuyer_3 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentBuyer;
				model.QCreditAppReqAssignment_AssignmentRefAgreementId_3 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentRefAgreementId;
				model.QCreditAppReqAssignment_AssignmentBuyerAgreementAmount_3 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentBuyerAgreementAmount;
				model.QCreditAppReqAssignment_AssignmentAgreementAmount_3 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentAgreementAmount;
				model.QCreditAppReqAssignment_AssignmentAgreementDate_3 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentAgreementDate;
				model.QCreditAppReqAssignment_RemainingAmount_3 = creditAppReqAssignment.QCreditAppReqAssignment_RemainingAmount;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQCreditAppReqAssignment4(BookmarkDocumentQueryBuyerAmendInformation model, QCreditAppReqAssignmentBuyerAmendInformation creditAppReqAssignment)
		{
			try
			{
				model.QCreditAppReqAssignment_AssignmentBuyer_4 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentBuyer;
				model.QCreditAppReqAssignment_AssignmentRefAgreementId_4 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentRefAgreementId;
				model.QCreditAppReqAssignment_AssignmentBuyerAgreementAmount_4 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentBuyerAgreementAmount;
				model.QCreditAppReqAssignment_AssignmentAgreementAmount_4 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentAgreementAmount;
				model.QCreditAppReqAssignment_AssignmentAgreementDate_4 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentAgreementDate;
				model.QCreditAppReqAssignment_RemainingAmount_4 = creditAppReqAssignment.QCreditAppReqAssignment_RemainingAmount;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQCreditAppReqAssignment5(BookmarkDocumentQueryBuyerAmendInformation model, QCreditAppReqAssignmentBuyerAmendInformation creditAppReqAssignment)
		{
			try
			{
				model.QCreditAppReqAssignment_AssignmentBuyer_5 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentBuyer;
				model.QCreditAppReqAssignment_AssignmentRefAgreementId_5 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentRefAgreementId;
				model.QCreditAppReqAssignment_AssignmentBuyerAgreementAmount_5 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentBuyerAgreementAmount;
				model.QCreditAppReqAssignment_AssignmentAgreementAmount_5 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentAgreementAmount;
				model.QCreditAppReqAssignment_AssignmentAgreementDate_5 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentAgreementDate;
				model.QCreditAppReqAssignment_RemainingAmount_5 = creditAppReqAssignment.QCreditAppReqAssignment_RemainingAmount;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQCreditAppReqAssignment6(BookmarkDocumentQueryBuyerAmendInformation model, QCreditAppReqAssignmentBuyerAmendInformation creditAppReqAssignment)
		{
			try
			{
				model.QCreditAppReqAssignment_AssignmentBuyer_6 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentBuyer;
				model.QCreditAppReqAssignment_AssignmentRefAgreementId_6 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentRefAgreementId;
				model.QCreditAppReqAssignment_AssignmentBuyerAgreementAmount_6 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentBuyerAgreementAmount;
				model.QCreditAppReqAssignment_AssignmentAgreementAmount_6 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentAgreementAmount;
				model.QCreditAppReqAssignment_AssignmentAgreementDate_6 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentAgreementDate;
				model.QCreditAppReqAssignment_RemainingAmount_6 = creditAppReqAssignment.QCreditAppReqAssignment_RemainingAmount;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQCreditAppReqAssignment7(BookmarkDocumentQueryBuyerAmendInformation model, QCreditAppReqAssignmentBuyerAmendInformation creditAppReqAssignment)
		{
			try
			{
				model.QCreditAppReqAssignment_AssignmentBuyer_7 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentBuyer;
				model.QCreditAppReqAssignment_AssignmentRefAgreementId_7 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentRefAgreementId;
				model.QCreditAppReqAssignment_AssignmentBuyerAgreementAmount_7 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentBuyerAgreementAmount;
				model.QCreditAppReqAssignment_AssignmentAgreementAmount_7 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentAgreementAmount;
				model.QCreditAppReqAssignment_AssignmentAgreementDate_7 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentAgreementDate;
				model.QCreditAppReqAssignment_RemainingAmount_7 = creditAppReqAssignment.QCreditAppReqAssignment_RemainingAmount;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQCreditAppReqAssignment8(BookmarkDocumentQueryBuyerAmendInformation model, QCreditAppReqAssignmentBuyerAmendInformation creditAppReqAssignment)
		{
			try
			{
				model.QCreditAppReqAssignment_AssignmentBuyer_8 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentBuyer;
				model.QCreditAppReqAssignment_AssignmentRefAgreementId_8 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentRefAgreementId;
				model.QCreditAppReqAssignment_AssignmentBuyerAgreementAmount_8 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentBuyerAgreementAmount;
				model.QCreditAppReqAssignment_AssignmentAgreementAmount_8 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentAgreementAmount;
				model.QCreditAppReqAssignment_AssignmentAgreementDate_8 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentAgreementDate;
				model.QCreditAppReqAssignment_RemainingAmount_8 = creditAppReqAssignment.QCreditAppReqAssignment_RemainingAmount;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQCreditAppReqAssignment9(BookmarkDocumentQueryBuyerAmendInformation model, QCreditAppReqAssignmentBuyerAmendInformation creditAppReqAssignment)
		{
			try
			{
				model.QCreditAppReqAssignment_AssignmentBuyer_9 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentBuyer;
				model.QCreditAppReqAssignment_AssignmentRefAgreementId_9 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentRefAgreementId;
				model.QCreditAppReqAssignment_AssignmentBuyerAgreementAmount_9 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentBuyerAgreementAmount;
				model.QCreditAppReqAssignment_AssignmentAgreementAmount_9 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentAgreementAmount;
				model.QCreditAppReqAssignment_AssignmentAgreementDate_9 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentAgreementDate;
				model.QCreditAppReqAssignment_RemainingAmount_9 = creditAppReqAssignment.QCreditAppReqAssignment_RemainingAmount;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByQCreditAppReqAssignment10(BookmarkDocumentQueryBuyerAmendInformation model, QCreditAppReqAssignmentBuyerAmendInformation creditAppReqAssignment)
		{
			try
			{
				model.QCreditAppReqAssignment_AssignmentBuyer_10 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentBuyer;
				model.QCreditAppReqAssignment_AssignmentRefAgreementId_10 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentRefAgreementId;
				model.QCreditAppReqAssignment_AssignmentBuyerAgreementAmount_10 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentBuyerAgreementAmount;
				model.QCreditAppReqAssignment_AssignmentAgreementAmount_10 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentAgreementAmount;
				model.QCreditAppReqAssignment_AssignmentAgreementDate_10 = creditAppReqAssignment.QCreditAppReqAssignment_AssignmentAgreementDate;
				model.QCreditAppReqAssignment_RemainingAmount_10 = creditAppReqAssignment.QCreditAppReqAssignment_RemainingAmount;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion SetModelByQCreditAppReqAssignment
		private string GetQReceiptDocumentOldValue(QReceiptDocumentOldBuyerAmendInformation model)
        {
			try
			{
                if (model != null)
                {
					return model.QReceiptDocumentOld_DocumentType;
                }
                else
                {
					return null;
                }
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private string GetQBillingDocumentOldValue(QBillingDocumentOldBuyerAmendInformation model)
        {
			try
			{
				if (model != null)
				{
					return model.QBillingDocumentOld_DocumentType;
				}
				else
				{
					return null;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private QReceiptDocumentOldBuyerAmendInformation GetSingleQReceiptDocumentOld(List<QReceiptDocumentOldBuyerAmendInformation> qReceiptDocumentOldList, int index)
        {
			try
			{
				if (index < qReceiptDocumentOldList.Count())
				{
					return qReceiptDocumentOldList[index];

				}
				else
				{
					return new QReceiptDocumentOldBuyerAmendInformation();

				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private QBillingDocumentOldBuyerAmendInformation GetSingleQBillingDocumentOld(List<QBillingDocumentOldBuyerAmendInformation> qBillingDocumentOldList, int index)
        {
			try
			{
				if (index < qBillingDocumentOldList.Count())
				{
					return qBillingDocumentOldList[index];

				}
				else
				{
					return new QBillingDocumentOldBuyerAmendInformation();

				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private QCreditAppReqBusinessCollateralBuyerAmendInformation GetSingleQCreditAppReqBusinessCollateral(List<QCreditAppReqBusinessCollateralBuyerAmendInformation> qCreditAppReqBusinessCollateralList, int index)
        {
			try
			{
				if (index < qCreditAppReqBusinessCollateralList.Count())
				{
					return qCreditAppReqBusinessCollateralList[index];

				}
				else
				{
					return new QCreditAppReqBusinessCollateralBuyerAmendInformation();

				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private QCreditAppReqAssignmentBuyerAmendInformation GetSingleQCreditAppReqAssignment(List<QCreditAppReqAssignmentBuyerAmendInformation> qCreditAppReqAssignmentList, int index)
        {
			try
			{
				if(index < qCreditAppReqAssignmentList.Count())
                {
					return qCreditAppReqAssignmentList[index];

                }
                else
                {
					return new QCreditAppReqAssignmentBuyerAmendInformation();

				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private List<QReceiptDocumentOldBuyerAmendInformation> GetQReceiptDocumentOldBuyerAmendInformation(Guid? qCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID)
        {
			try
			{
				return (from documentConditionTrans in db.Set<DocumentConditionTrans>()
						join documentType in db.Set<DocumentType>()
						on documentConditionTrans.DocumentTypeGUID equals documentType.DocumentTypeGUID into ljdocumentType
						from documentType in ljdocumentType.DefaultIfEmpty()
						orderby documentType.Description
						where documentConditionTrans.RefGUID == qCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID
						&& documentConditionTrans.RefType == (int)RefType.CreditAppRequestLine
						&& documentConditionTrans.DocConVerifyType == (int)DocConVerifyType.Receipt
						&& documentConditionTrans.Inactive == true
						select new QReceiptDocumentOldBuyerAmendInformation
						{
							QReceiptDocumentOld_DocumentType = documentType.Description,
							QReceiptDocumentOld_Inactive = documentConditionTrans.Inactive
						}



						).Take(10).ToList();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private List<QBillingDocumentOldBuyerAmendInformation> GetQBillingDocumentOldBuyerAmendInformation(Guid? qCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID)
        {
			try
			{
				return (from documentConditionTrans in db.Set<DocumentConditionTrans>()
						join documentType in db.Set<DocumentType>()
						on documentConditionTrans.DocumentTypeGUID equals documentType.DocumentTypeGUID into ljdocumentType
						from documentType in ljdocumentType.DefaultIfEmpty()
						orderby documentType.Description
						where documentConditionTrans.RefGUID == qCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID
						
						&& documentConditionTrans.RefType == (int)RefType.CreditAppRequestLine
						&& documentConditionTrans.DocConVerifyType == (int)DocConVerifyType.Billing
						&& documentConditionTrans.Inactive == true
						select new QBillingDocumentOldBuyerAmendInformation
						{
							QBillingDocumentOld_DocumentType = documentType.Description,
							QBillingDocumentOld_Inactive = documentConditionTrans.Inactive
						}



						).Take(10).ToList();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

  //      private List<QOriginalRefCreditAppRequestLineBuyerAmendInformation> GetQOriginalRefCreditAppRequestLineBuyerAmendInformation(Guid qCreditAppRequestTableBuyerAmendInfo_OriginalRefCreditAppRequestTableGUID, Guid? qCreditAppRequestTableBuyerAmendInfo_BuyerTableGUID)
  //      {
		//	try
		//	{
		//		return (from creditAppReqTable in db.Set<CreditAppRequestTable>().Where(wh => wh.CreditAppRequestTableGUID == qCreditAppRequestTableBuyerAmendInfo_OriginalRefCreditAppRequestTableGUID)
		//					  join creditAppRequestLine in db.Set<CreditAppRequestLine>().Where(wh => wh.BuyerTableGUID == qCreditAppRequestTableBuyerAmendInfo_BuyerTableGUID)
		//					  on creditAppReqTable.CreditAppRequestTableGUID equals creditAppRequestLine.CreditAppRequestTableGUID into ljcreditAppRequestLine
		//					  from creditAppRequestLine in ljcreditAppRequestLine.DefaultIfEmpty()
		//					  select new QOriginalRefCreditAppRequestLineBuyerAmendInformation
		//					  {
		//						  QOriginalRefCreditAppRequestLine_CreditAppRequestLineGUID = creditAppRequestLine.CreditAppRequestLineGUID
		//					  }


		//					  ).ToList();
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}

        private QSumCreditAppReqBusinessCollateralBuyerAmendInformation GetQSumCreditAppReqBusinessCollateral(Guid refGUID)
        {
			try
			{
				var result =(from creditAppReqBusinessCollateral in db.Set<CreditAppReqBusinessCollateral>().Where(wh => wh.CreditAppRequestTableGUID == refGUID)
						select new
                        {
							creditAppReqBusinessCollateral.BusinessCollateralValue
                        }
						).Sum(su=>su.BusinessCollateralValue);
				return new QSumCreditAppReqBusinessCollateralBuyerAmendInformation { QSumCreditAppReqBusinessCollateral_SumBusinessCollateralValue = result };

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
			

		}

        private List<QCreditAppReqBusinessCollateralBuyerAmendInformation> GetQCreditAppReqBusinessCollateralBuyerAmendInformation(Guid refGUID)
        {
			try
			{
				return (from creditAppReqBusinessCollateral in db.Set<CreditAppReqBusinessCollateral>()

							  join businessColleteralType in db.Set<BusinessCollateralType>()
							  on creditAppReqBusinessCollateral.BusinessCollateralTypeGUID equals businessColleteralType.BusinessCollateralTypeGUID into ljbusinessColleteralType
							  from businessColleteralType in ljbusinessColleteralType.DefaultIfEmpty()

							  join businessColleteralSubtype in db.Set<BusinessCollateralSubType>()
							  on creditAppReqBusinessCollateral.BusinessCollateralSubTypeGUID equals businessColleteralSubtype.BusinessCollateralSubTypeGUID into ljbusinessColleteralSubtype
							  from businessColleteralSubtype in ljbusinessColleteralSubtype.DefaultIfEmpty()


						orderby creditAppReqBusinessCollateral.CreatedDateTime

						where creditAppReqBusinessCollateral.CreditAppRequestTableGUID == refGUID
							  select new QCreditAppReqBusinessCollateralBuyerAmendInformation
							  {
								  QCreditAppReqBusinessCollateral_IsNew = creditAppReqBusinessCollateral.IsNew,
								  QCreditAppReqBusinessCollateral_BusinessCollateralTypeGUID = creditAppReqBusinessCollateral.BusinessCollateralTypeGUID,
								  QCreditAppReqBusinessCollateral_BusinessCollateralType = businessColleteralType.Description,
								  QCreditAppReqBusinessCollateral_BusinessCollateralSubTypeGUID = creditAppReqBusinessCollateral.BusinessCollateralSubTypeGUID,
								  QCreditAppReqBusinessCollateral_BusinessCollateralSubType = businessColleteralSubtype.Description,
								  QCreditAppReqBusinessCollateral_Description = creditAppReqBusinessCollateral.Description,
								  QCreditAppReqBusinessCollateral_BusinessCollateralValue = creditAppReqBusinessCollateral.BusinessCollateralValue,
							  }
							  ).Take(10).ToList();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private QSumCreditAppReqAssignmentBuyerAmendInformation GetQSumCreditAppReqAssignmentBuyerAmendInformation(Guid refGUID)
        {
			try
			{
				return (from creditAppReqAssignment in db.Set<CreditAppReqAssignment>().Where(wh=>wh.CreditAppRequestTableGUID==refGUID)
							  group creditAppReqAssignment
							  by new
							  {
								  creditAppReqAssignment.CreditAppRequestTableGUID,
		
							  } into g
							  select new QSumCreditAppReqAssignmentBuyerAmendInformation
							  {

								  QSumCreditAppReqAssignment_SumBuyerAgreementAmount = g.Sum(su => su.BuyerAgreementAmount),
								  QSumCreditAppReqAssignment_SumAssignmentAgreementAmount = g.Sum(su => su.AssignmentAgreementAmount),
								  QSumCreditAppReqAssignment_SumRemainingAmount = g.Sum(su => su.RemainingAmount),
							  }
							  ).FirstOrDefault();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private List<QCreditAppReqAssignmentBuyerAmendInformation> GetQCreditAppReqAssignmentBuyerAmendInformation(Guid refGUID)
        {
			try
			{
				return (from creditAppReqAssignment in db.Set<CreditAppReqAssignment>()
							  join buyerTable in db.Set<BuyerTable>()
							  on creditAppReqAssignment.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
							  from buyerTable in ljbuyerTable.DefaultIfEmpty()

							  join buyerAgreementTable in db.Set<BuyerAgreementTable>()
							  on creditAppReqAssignment.BuyerAgreementTableGUID equals buyerAgreementTable.BuyerAgreementTableGUID into ljbuyerAgreementTable
							  from buyerAgreementTable in ljbuyerAgreementTable.DefaultIfEmpty()

							  join assignmentAgreementTable in db.Set<AssignmentAgreementTable>()
							  on creditAppReqAssignment.AssignmentAgreementTableGUID equals assignmentAgreementTable.AssignmentAgreementTableGUID into ljassignmentAgreementTable
							  from assignmentAgreementTable in ljassignmentAgreementTable.DefaultIfEmpty()

							  where creditAppReqAssignment.CreditAppRequestTableGUID == refGUID
							  orderby creditAppReqAssignment.CreatedDateTime
							  select new QCreditAppReqAssignmentBuyerAmendInformation
                              {
								 QCreditAppReqAssignment_IsNew = creditAppReqAssignment.IsNew,
								QCreditAppReqAssignment_BuyerTableGUID = creditAppReqAssignment.BuyerTableGUID,
								QCreditAppReqAssignment_AssignmentBuyer = String.Concat(buyerTable.BuyerName," (",buyerTable.BuyerId,")"),
								QCreditAppReqAssignment_BuyerAgreementTableGUID = creditAppReqAssignment.BuyerAgreementTableGUID,
								QCreditAppReqAssignment_AssignmentRefAgreementId = buyerAgreementTable.BuyerAgreementId,
								QCreditAppReqAssignment_AssignmentBuyerAgreementAmount = creditAppReqAssignment.BuyerAgreementAmount,
								QCreditAppReqAssignment_AssignmentAgreementAmount = creditAppReqAssignment.AssignmentAgreementAmount,
								QCreditAppReqAssignment_AssignmentAgreementTableGUID = creditAppReqAssignment.AssignmentAgreementTableGUID,
								QCreditAppReqAssignment_AssignmentAgreementDate =  assignmentAgreementTable.AgreementDate,
								  QCreditAppReqAssignment_RemainingAmount = creditAppReqAssignment.RemainingAmount,

								  QCreditAppReqAssignment_Row = 0
	}



							  ).Take(10).ToList();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private void SetModelByNewReceiptDocConInactive10(BookmarkDocumentQueryBuyerAmendInformation model, QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_10)
		{
			try
			{
				if (qReceiptDocumentBuyerAmendInfo_10.QReceiptDocumentBuyerAmendInfo_Inactive == false)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive10 = "NO";
				}
				else if (qReceiptDocumentBuyerAmendInfo_10.QReceiptDocumentBuyerAmendInfo_Inactive == true)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive10 = "YES";
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByNewReceiptDocConInactive9(BookmarkDocumentQueryBuyerAmendInformation model, QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_9)
		{
			try
			{
				if (qReceiptDocumentBuyerAmendInfo_9.QReceiptDocumentBuyerAmendInfo_Inactive == false)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive9 = "NO";
				}
				else if (qReceiptDocumentBuyerAmendInfo_9.QReceiptDocumentBuyerAmendInfo_Inactive == true)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive9 = "YES";
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByNewReceiptDocConInactive8(BookmarkDocumentQueryBuyerAmendInformation model, QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_8)
		{
			try
			{
				if (qReceiptDocumentBuyerAmendInfo_8.QReceiptDocumentBuyerAmendInfo_Inactive == false)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive8 = "NO";
				}
				else if (qReceiptDocumentBuyerAmendInfo_8.QReceiptDocumentBuyerAmendInfo_Inactive == true)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive8 = "YES";
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByNewReceiptDocConInactive7(BookmarkDocumentQueryBuyerAmendInformation model, QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_7)
		{
			try
			{
				if (qReceiptDocumentBuyerAmendInfo_7.QReceiptDocumentBuyerAmendInfo_Inactive == false)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive7 = "NO";
				}
				else if (qReceiptDocumentBuyerAmendInfo_7.QReceiptDocumentBuyerAmendInfo_Inactive == true)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive7 = "YES";
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByNewReceiptDocConInactive6(BookmarkDocumentQueryBuyerAmendInformation model, QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_6)
		{
			try
			{
				if (qReceiptDocumentBuyerAmendInfo_6.QReceiptDocumentBuyerAmendInfo_Inactive == false)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive6 = "NO";
				}
				else if (qReceiptDocumentBuyerAmendInfo_6.QReceiptDocumentBuyerAmendInfo_Inactive == true)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive6 = "YES";
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByNewReceiptDocConInactive5(BookmarkDocumentQueryBuyerAmendInformation model, QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_5)
		{
			try
			{
				if (qReceiptDocumentBuyerAmendInfo_5.QReceiptDocumentBuyerAmendInfo_Inactive == false)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive5 = "NO";
				}
				else if (qReceiptDocumentBuyerAmendInfo_5.QReceiptDocumentBuyerAmendInfo_Inactive == true)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive5 = "YES";
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByNewReceiptDocConInactive4(BookmarkDocumentQueryBuyerAmendInformation model, QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_4)
		{
			try
			{
				if (qReceiptDocumentBuyerAmendInfo_4.QReceiptDocumentBuyerAmendInfo_Inactive == false)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive4 = "NO";
				}
				else if (qReceiptDocumentBuyerAmendInfo_4.QReceiptDocumentBuyerAmendInfo_Inactive == true)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive4 = "YES";
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByNewReceiptDocConInactive3(BookmarkDocumentQueryBuyerAmendInformation model, QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_3)
		{
			try
			{
				if (qReceiptDocumentBuyerAmendInfo_3.QReceiptDocumentBuyerAmendInfo_Inactive == false)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive3 = "NO";
				}
				else if (qReceiptDocumentBuyerAmendInfo_3.QReceiptDocumentBuyerAmendInfo_Inactive == true)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive3 = "YES";
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByNewReceiptDocConInactive2(BookmarkDocumentQueryBuyerAmendInformation model, QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_2)
		{
			try
			{
				if (qReceiptDocumentBuyerAmendInfo_2.QReceiptDocumentBuyerAmendInfo_Inactive == false)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive2 = "NO";
				}
				else if (qReceiptDocumentBuyerAmendInfo_2.QReceiptDocumentBuyerAmendInfo_Inactive == true)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive2 = "YES";
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByNewReceiptDocConInactive1(BookmarkDocumentQueryBuyerAmendInformation model, QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_1)
		{
			try
			{
				if (qReceiptDocumentBuyerAmendInfo_1.QReceiptDocumentBuyerAmendInfo_Inactive == false)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive1 = "NO";
				}
				else if (qReceiptDocumentBuyerAmendInfo_1.QReceiptDocumentBuyerAmendInfo_Inactive == true)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive1 = "YES";
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByNewBillingDocConInactive10(BookmarkDocumentQueryBuyerAmendInformation model, QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_10)
		{
			try
			{
				if (qBillingDocumentBuyerAmendInfo_10.QBillingDocumentBuyerAmendInfo_Inactive == false)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive10 = "NO";
				}
				else if (qBillingDocumentBuyerAmendInfo_10.QBillingDocumentBuyerAmendInfo_Inactive == true)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive10 = "YES";
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByNewBillingDocConInactive9(BookmarkDocumentQueryBuyerAmendInformation model, QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_9)
		{
			try
			{
				if (qBillingDocumentBuyerAmendInfo_9.QBillingDocumentBuyerAmendInfo_Inactive == false)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive9 = "NO";
				}
				else if (qBillingDocumentBuyerAmendInfo_9.QBillingDocumentBuyerAmendInfo_Inactive == true)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive9 = "YES";
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByNewBillingDocConInactive8(BookmarkDocumentQueryBuyerAmendInformation model, QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_8)
		{
			try
			{
				if (qBillingDocumentBuyerAmendInfo_8.QBillingDocumentBuyerAmendInfo_Inactive == false)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive8 = "NO";
				}
				else if (qBillingDocumentBuyerAmendInfo_8.QBillingDocumentBuyerAmendInfo_Inactive == true)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive8 = "YES";
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByNewBillingDocConInactive7(BookmarkDocumentQueryBuyerAmendInformation model, QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_7)
		{
			try
			{
				if (qBillingDocumentBuyerAmendInfo_7.QBillingDocumentBuyerAmendInfo_Inactive == false)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive7 = "NO";
				}
				else if (qBillingDocumentBuyerAmendInfo_7.QBillingDocumentBuyerAmendInfo_Inactive == true)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive7 = "YES";
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByNewBillingDocConInactive6(BookmarkDocumentQueryBuyerAmendInformation model, QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_6)
		{
			try
			{
				if (qBillingDocumentBuyerAmendInfo_6.QBillingDocumentBuyerAmendInfo_Inactive == false)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive6 = "NO";
				}
				else if (qBillingDocumentBuyerAmendInfo_6.QBillingDocumentBuyerAmendInfo_Inactive == true)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive6 = "YES";
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByNewBillingDocConInactive5(BookmarkDocumentQueryBuyerAmendInformation model, QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_5)
		{
			try
			{
				if (qBillingDocumentBuyerAmendInfo_5.QBillingDocumentBuyerAmendInfo_Inactive == false)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive5 = "NO";
				}
				else if (qBillingDocumentBuyerAmendInfo_5.QBillingDocumentBuyerAmendInfo_Inactive == true)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive5 = "YES";
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByNewBillingDocConInactive4(BookmarkDocumentQueryBuyerAmendInformation model, QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_4)
		{
			try
			{
				if (qBillingDocumentBuyerAmendInfo_4.QBillingDocumentBuyerAmendInfo_Inactive == false)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive4 = "NO";
				}
				else if (qBillingDocumentBuyerAmendInfo_4.QBillingDocumentBuyerAmendInfo_Inactive == true)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive4 = "YES";
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByNewBillingDocConInactive3(BookmarkDocumentQueryBuyerAmendInformation model, QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_3)
		{
			try
			{
				if (qBillingDocumentBuyerAmendInfo_3.QBillingDocumentBuyerAmendInfo_Inactive == false)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive3 = "NO";
				}
				else if (qBillingDocumentBuyerAmendInfo_3.QBillingDocumentBuyerAmendInfo_Inactive == true)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive3 = "YES";
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByNewBillingDocConInactive2(BookmarkDocumentQueryBuyerAmendInformation model, QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_2)
		{
			try
			{
				if (qBillingDocumentBuyerAmendInfo_2.QBillingDocumentBuyerAmendInfo_Inactive == false)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive2 = "NO";
				}
				else if (qBillingDocumentBuyerAmendInfo_2.QBillingDocumentBuyerAmendInfo_Inactive == true)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive2 = "YES";
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByNewBillingDocConInactive1(BookmarkDocumentQueryBuyerAmendInformation model, QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_1)
		{
			try
			{
				if (qBillingDocumentBuyerAmendInfo_1.QBillingDocumentBuyerAmendInfo_Inactive == false)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive1 = "NO";
				}
				else if (qBillingDocumentBuyerAmendInfo_1.QBillingDocumentBuyerAmendInfo_Inactive == true)
				{
					model.BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive1 = "YES";
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetModelByqCreditAppReqLineFinBuyerAmendInfo_3(BookmarkDocumentQueryBuyerAmendInformation model, QCreditAppReqLineFinBuyerAmendInfo qCreditAppReqLineFinBuyerAmendInfo_3)
		{

			try
			{
				model.QCreditAppReqLineFinBuyerAmendInfo_Year_3 = qCreditAppReqLineFinBuyerAmendInfo_3.QCreditAppReqLineFinBuyerAmendInfo_Year;
				model.QCreditAppReqLineFinBuyerAmendInfo_RegisteredCapital_3 = qCreditAppReqLineFinBuyerAmendInfo_3.QCreditAppReqLineFinBuyerAmendInfo_RegisteredCapital;
				model.QCreditAppReqLineFinBuyerAmendInfo_PaidCapital_3 = qCreditAppReqLineFinBuyerAmendInfo_3.QCreditAppReqLineFinBuyerAmendInfo_PaidCapital;
				model.QCreditAppReqLineFinBuyerAmendInfo_TotalAsset_3 = qCreditAppReqLineFinBuyerAmendInfo_3.QCreditAppReqLineFinBuyerAmendInfo_TotalAsset;
				model.QCreditAppReqLineFinBuyerAmendInfo_TotalLiability_3 = qCreditAppReqLineFinBuyerAmendInfo_3.QCreditAppReqLineFinBuyerAmendInfo_TotalLiability;
				model.QCreditAppReqLineFinBuyerAmendInfo_TotalEquity_3 = qCreditAppReqLineFinBuyerAmendInfo_3.QCreditAppReqLineFinBuyerAmendInfo_TotalEquity;
				model.QCreditAppReqLineFinBuyerAmendInfo_TotalRevenue_3 = qCreditAppReqLineFinBuyerAmendInfo_3.QCreditAppReqLineFinBuyerAmendInfo_TotalRevenue;
				model.QCreditAppReqLineFinBuyerAmendInfo_TotalCOGS_3 = qCreditAppReqLineFinBuyerAmendInfo_3.QCreditAppReqLineFinBuyerAmendInfo_TotalCOGS;
				model.QCreditAppReqLineFinBuyerAmendInfo_TotalGrossProfit_3 = qCreditAppReqLineFinBuyerAmendInfo_3.QCreditAppReqLineFinBuyerAmendInfo_TotalGrossProfit;
				model.QCreditAppReqLineFinBuyerAmendInfo_TotalOperExpFirst_3 = qCreditAppReqLineFinBuyerAmendInfo_3.QCreditAppReqLineFinBuyerAmendInfo_TotalOperExpFirst;
				model.QCreditAppReqLineFinBuyerAmendInfo_TotalNetProfitFirst_3 = qCreditAppReqLineFinBuyerAmendInfo_3.QCreditAppReqLineFinBuyerAmendInfo_TotalNetProfitFirst;
				model.QCreditAppReqLineFinBuyerAmendInfo_NetProfitPercent_3 = qCreditAppReqLineFinBuyerAmendInfo_3.QCreditAppReqLineFinBuyerAmendInfo_NetProfitPercent;
				model.QCreditAppReqLineFinBuyerAmendInfo_lDE_3 = qCreditAppReqLineFinBuyerAmendInfo_3.QCreditAppReqLineFinBuyerAmendInfo_lDE;
				model.QCreditAppReqLineFinBuyerAmendInfo_QuickRatio_3 = qCreditAppReqLineFinBuyerAmendInfo_3.QCreditAppReqLineFinBuyerAmendInfo_QuickRatio;
				model.QCreditAppReqLineFinBuyerAmendInfo_IntCoverageRatio_3 = qCreditAppReqLineFinBuyerAmendInfo_3.QCreditAppReqLineFinBuyerAmendInfo_IntCoverageRatio;
				model.QCreditAppReqLineFinBuyerAmendInfo_Row_3 = qCreditAppReqLineFinBuyerAmendInfo_3.QCreditAppReqLineFinBuyerAmendInfo_Row;

			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByqCreditAppReqLineFinBuyerAmendInfo_2(BookmarkDocumentQueryBuyerAmendInformation model, QCreditAppReqLineFinBuyerAmendInfo qCreditAppReqLineFinBuyerAmendInfo_2)
		{

			try
			{
				model.QCreditAppReqLineFinBuyerAmendInfo_Year_2 = qCreditAppReqLineFinBuyerAmendInfo_2.QCreditAppReqLineFinBuyerAmendInfo_Year;
				model.QCreditAppReqLineFinBuyerAmendInfo_RegisteredCapital_2 = qCreditAppReqLineFinBuyerAmendInfo_2.QCreditAppReqLineFinBuyerAmendInfo_RegisteredCapital;
				model.QCreditAppReqLineFinBuyerAmendInfo_PaidCapital_2 = qCreditAppReqLineFinBuyerAmendInfo_2.QCreditAppReqLineFinBuyerAmendInfo_PaidCapital;
				model.QCreditAppReqLineFinBuyerAmendInfo_TotalAsset_2 = qCreditAppReqLineFinBuyerAmendInfo_2.QCreditAppReqLineFinBuyerAmendInfo_TotalAsset;
				model.QCreditAppReqLineFinBuyerAmendInfo_TotalLiability_2 = qCreditAppReqLineFinBuyerAmendInfo_2.QCreditAppReqLineFinBuyerAmendInfo_TotalLiability;
				model.QCreditAppReqLineFinBuyerAmendInfo_TotalEquity_2 = qCreditAppReqLineFinBuyerAmendInfo_2.QCreditAppReqLineFinBuyerAmendInfo_TotalEquity;
				model.QCreditAppReqLineFinBuyerAmendInfo_TotalRevenue_2 = qCreditAppReqLineFinBuyerAmendInfo_2.QCreditAppReqLineFinBuyerAmendInfo_TotalRevenue;
				model.QCreditAppReqLineFinBuyerAmendInfo_TotalCOGS_2 = qCreditAppReqLineFinBuyerAmendInfo_2.QCreditAppReqLineFinBuyerAmendInfo_TotalCOGS;
				model.QCreditAppReqLineFinBuyerAmendInfo_TotalGrossProfit_2 = qCreditAppReqLineFinBuyerAmendInfo_2.QCreditAppReqLineFinBuyerAmendInfo_TotalGrossProfit;
				model.QCreditAppReqLineFinBuyerAmendInfo_TotalOperExpFirst_2 = qCreditAppReqLineFinBuyerAmendInfo_2.QCreditAppReqLineFinBuyerAmendInfo_TotalOperExpFirst;
				model.QCreditAppReqLineFinBuyerAmendInfo_TotalNetProfitFirst_2 = qCreditAppReqLineFinBuyerAmendInfo_2.QCreditAppReqLineFinBuyerAmendInfo_TotalNetProfitFirst;
				model.QCreditAppReqLineFinBuyerAmendInfo_NetProfitPercent_2 = qCreditAppReqLineFinBuyerAmendInfo_2.QCreditAppReqLineFinBuyerAmendInfo_NetProfitPercent;
				model.QCreditAppReqLineFinBuyerAmendInfo_lDE_2 = qCreditAppReqLineFinBuyerAmendInfo_2.QCreditAppReqLineFinBuyerAmendInfo_lDE;
				model.QCreditAppReqLineFinBuyerAmendInfo_QuickRatio_2 = qCreditAppReqLineFinBuyerAmendInfo_2.QCreditAppReqLineFinBuyerAmendInfo_QuickRatio;
				model.QCreditAppReqLineFinBuyerAmendInfo_IntCoverageRatio_2 = qCreditAppReqLineFinBuyerAmendInfo_2.QCreditAppReqLineFinBuyerAmendInfo_IntCoverageRatio;
				model.QCreditAppReqLineFinBuyerAmendInfo_Row_2 = qCreditAppReqLineFinBuyerAmendInfo_2.QCreditAppReqLineFinBuyerAmendInfo_Row;

			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByqCreditAppReqLineFinBuyerAmendInfo_1(BookmarkDocumentQueryBuyerAmendInformation model, QCreditAppReqLineFinBuyerAmendInfo qCreditAppReqLineFinBuyerAmendInfo_1)
		{

			try
			{
				model.QCreditAppReqLineFinBuyerAmendInfo_Year_1 = qCreditAppReqLineFinBuyerAmendInfo_1.QCreditAppReqLineFinBuyerAmendInfo_Year;
				model.QCreditAppReqLineFinBuyerAmendInfo_RegisteredCapital_1 = qCreditAppReqLineFinBuyerAmendInfo_1.QCreditAppReqLineFinBuyerAmendInfo_RegisteredCapital;
				model.QCreditAppReqLineFinBuyerAmendInfo_PaidCapital_1 = qCreditAppReqLineFinBuyerAmendInfo_1.QCreditAppReqLineFinBuyerAmendInfo_PaidCapital;
				model.QCreditAppReqLineFinBuyerAmendInfo_TotalAsset_1 = qCreditAppReqLineFinBuyerAmendInfo_1.QCreditAppReqLineFinBuyerAmendInfo_TotalAsset;
				model.QCreditAppReqLineFinBuyerAmendInfo_TotalLiability_1 = qCreditAppReqLineFinBuyerAmendInfo_1.QCreditAppReqLineFinBuyerAmendInfo_TotalLiability;
				model.QCreditAppReqLineFinBuyerAmendInfo_TotalEquity_1 = qCreditAppReqLineFinBuyerAmendInfo_1.QCreditAppReqLineFinBuyerAmendInfo_TotalEquity;
				model.QCreditAppReqLineFinBuyerAmendInfo_TotalRevenue_1 = qCreditAppReqLineFinBuyerAmendInfo_1.QCreditAppReqLineFinBuyerAmendInfo_TotalRevenue;
				model.QCreditAppReqLineFinBuyerAmendInfo_TotalCOGS_1 = qCreditAppReqLineFinBuyerAmendInfo_1.QCreditAppReqLineFinBuyerAmendInfo_TotalCOGS;
				model.QCreditAppReqLineFinBuyerAmendInfo_TotalGrossProfit_1 = qCreditAppReqLineFinBuyerAmendInfo_1.QCreditAppReqLineFinBuyerAmendInfo_TotalGrossProfit;
				model.QCreditAppReqLineFinBuyerAmendInfo_TotalOperExpFirst_1 = qCreditAppReqLineFinBuyerAmendInfo_1.QCreditAppReqLineFinBuyerAmendInfo_TotalOperExpFirst;
				model.QCreditAppReqLineFinBuyerAmendInfo_TotalNetProfitFirst_1 = qCreditAppReqLineFinBuyerAmendInfo_1.QCreditAppReqLineFinBuyerAmendInfo_TotalNetProfitFirst;
				model.QCreditAppReqLineFinBuyerAmendInfo_NetProfitPercent_1 = qCreditAppReqLineFinBuyerAmendInfo_1.QCreditAppReqLineFinBuyerAmendInfo_NetProfitPercent;
				model.QCreditAppReqLineFinBuyerAmendInfo_lDE_1 = qCreditAppReqLineFinBuyerAmendInfo_1.QCreditAppReqLineFinBuyerAmendInfo_lDE;
				model.QCreditAppReqLineFinBuyerAmendInfo_QuickRatio_1 = qCreditAppReqLineFinBuyerAmendInfo_1.QCreditAppReqLineFinBuyerAmendInfo_QuickRatio;
				model.QCreditAppReqLineFinBuyerAmendInfo_IntCoverageRatio_1 = qCreditAppReqLineFinBuyerAmendInfo_1.QCreditAppReqLineFinBuyerAmendInfo_IntCoverageRatio;
				model.QCreditAppReqLineFinBuyerAmendInfo_Row_1 = qCreditAppReqLineFinBuyerAmendInfo_1.QCreditAppReqLineFinBuyerAmendInfo_Row;

			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetNodelByqReceiptDocumentBuyerAmendInfo_10(BookmarkDocumentQueryBuyerAmendInformation model, QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_10)
		{
			try
			{
				model.QReceiptDocumentBuyerAmendInfo_DocumentType_10 = qReceiptDocumentBuyerAmendInfo_10.QReceiptDocumentBuyerAmendInfo_DocumentType;
				model.QReceiptDocumentBuyerAmendInfo_Inactive_10 = qReceiptDocumentBuyerAmendInfo_10.QReceiptDocumentBuyerAmendInfo_Inactive;

			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetNodelByqReceiptDocumentBuyerAmendInfo_9(BookmarkDocumentQueryBuyerAmendInformation model, QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_9)
		{
			try
			{
				model.QReceiptDocumentBuyerAmendInfo_DocumentType_9 = qReceiptDocumentBuyerAmendInfo_9.QReceiptDocumentBuyerAmendInfo_DocumentType;
				model.QReceiptDocumentBuyerAmendInfo_Inactive_9 = qReceiptDocumentBuyerAmendInfo_9.QReceiptDocumentBuyerAmendInfo_Inactive;

			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetNodelByqReceiptDocumentBuyerAmendInfo_8(BookmarkDocumentQueryBuyerAmendInformation model, QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_8)
		{
			try
			{
				model.QReceiptDocumentBuyerAmendInfo_DocumentType_8 = qReceiptDocumentBuyerAmendInfo_8.QReceiptDocumentBuyerAmendInfo_DocumentType;
				model.QReceiptDocumentBuyerAmendInfo_Inactive_8 = qReceiptDocumentBuyerAmendInfo_8.QReceiptDocumentBuyerAmendInfo_Inactive;

			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetNodelByqReceiptDocumentBuyerAmendInfo_7(BookmarkDocumentQueryBuyerAmendInformation model, QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_7)
		{
			try
			{
				model.QReceiptDocumentBuyerAmendInfo_DocumentType_7 = qReceiptDocumentBuyerAmendInfo_7.QReceiptDocumentBuyerAmendInfo_DocumentType;
				model.QReceiptDocumentBuyerAmendInfo_Inactive_7 = qReceiptDocumentBuyerAmendInfo_7.QReceiptDocumentBuyerAmendInfo_Inactive;

			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetNodelByqReceiptDocumentBuyerAmendInfo_6(BookmarkDocumentQueryBuyerAmendInformation model, QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_6)
		{
			try
			{
				model.QReceiptDocumentBuyerAmendInfo_DocumentType_6 = qReceiptDocumentBuyerAmendInfo_6.QReceiptDocumentBuyerAmendInfo_DocumentType;
				model.QReceiptDocumentBuyerAmendInfo_Inactive_6 = qReceiptDocumentBuyerAmendInfo_6.QReceiptDocumentBuyerAmendInfo_Inactive;

			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetNodelByqReceiptDocumentBuyerAmendInfo_5(BookmarkDocumentQueryBuyerAmendInformation model, QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_5)
		{
			try
			{
				model.QReceiptDocumentBuyerAmendInfo_DocumentType_5 = qReceiptDocumentBuyerAmendInfo_5.QReceiptDocumentBuyerAmendInfo_DocumentType;
				model.QReceiptDocumentBuyerAmendInfo_Inactive_5 = qReceiptDocumentBuyerAmendInfo_5.QReceiptDocumentBuyerAmendInfo_Inactive;

			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetNodelByqReceiptDocumentBuyerAmendInfo_4(BookmarkDocumentQueryBuyerAmendInformation model, QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_4)
		{
			try
			{
				model.QReceiptDocumentBuyerAmendInfo_DocumentType_4 = qReceiptDocumentBuyerAmendInfo_4.QReceiptDocumentBuyerAmendInfo_DocumentType;
				model.QReceiptDocumentBuyerAmendInfo_Inactive_4 = qReceiptDocumentBuyerAmendInfo_4.QReceiptDocumentBuyerAmendInfo_Inactive;

			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetNodelByqReceiptDocumentBuyerAmendInfo_3(BookmarkDocumentQueryBuyerAmendInformation model, QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_3)
		{
			try
			{
				model.QReceiptDocumentBuyerAmendInfo_DocumentType_3 = qReceiptDocumentBuyerAmendInfo_3.QReceiptDocumentBuyerAmendInfo_DocumentType;
				model.QReceiptDocumentBuyerAmendInfo_Inactive_3 = qReceiptDocumentBuyerAmendInfo_3.QReceiptDocumentBuyerAmendInfo_Inactive;

			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetNodelByqReceiptDocumentBuyerAmendInfo_2(BookmarkDocumentQueryBuyerAmendInformation model, QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_2)
		{
			try
			{
				model.QReceiptDocumentBuyerAmendInfo_DocumentType_2 = qReceiptDocumentBuyerAmendInfo_2.QReceiptDocumentBuyerAmendInfo_DocumentType;
				model.QReceiptDocumentBuyerAmendInfo_Inactive_2 = qReceiptDocumentBuyerAmendInfo_2.QReceiptDocumentBuyerAmendInfo_Inactive;

			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetNodelByqReceiptDocumentBuyerAmendInfo_1(BookmarkDocumentQueryBuyerAmendInformation model, QReceiptDocumentBuyerAmendInfo qReceiptDocumentBuyerAmendInfo_1)
		{
			try
			{
				model.QReceiptDocumentBuyerAmendInfo_DocumentType_1 = qReceiptDocumentBuyerAmendInfo_1.QReceiptDocumentBuyerAmendInfo_DocumentType;
				model.QReceiptDocumentBuyerAmendInfo_Inactive_1 = qReceiptDocumentBuyerAmendInfo_1.QReceiptDocumentBuyerAmendInfo_Inactive;

			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetNodelByqBillingDocumentBuyerAmendInfo_10(BookmarkDocumentQueryBuyerAmendInformation model, QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_10)
		{
			try
			{
				model.QBillingDocumentBuyerAmendInfo_DocumentType_10 = qBillingDocumentBuyerAmendInfo_10.QBillingDocumentBuyerAmendInfo_DocumentType;
				model.QBillingDocumentBuyerAmendInfo_Inactive_10 = qBillingDocumentBuyerAmendInfo_10.QBillingDocumentBuyerAmendInfo_Inactive;

			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetNodelByqBillingDocumentBuyerAmendInfo_9(BookmarkDocumentQueryBuyerAmendInformation model, QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_9)
		{
			try
			{
				model.QBillingDocumentBuyerAmendInfo_DocumentType_9 = qBillingDocumentBuyerAmendInfo_9.QBillingDocumentBuyerAmendInfo_DocumentType;
				model.QBillingDocumentBuyerAmendInfo_Inactive_9 = qBillingDocumentBuyerAmendInfo_9.QBillingDocumentBuyerAmendInfo_Inactive;

			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetNodelByqBillingDocumentBuyerAmendInfo_8(BookmarkDocumentQueryBuyerAmendInformation model, QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_8)
		{
			try
			{
				model.QBillingDocumentBuyerAmendInfo_DocumentType_8 = qBillingDocumentBuyerAmendInfo_8.QBillingDocumentBuyerAmendInfo_DocumentType;
				model.QBillingDocumentBuyerAmendInfo_Inactive_8 = qBillingDocumentBuyerAmendInfo_8.QBillingDocumentBuyerAmendInfo_Inactive;

			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetNodelByqBillingDocumentBuyerAmendInfo_7(BookmarkDocumentQueryBuyerAmendInformation model, QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_7)
		{
			try
			{
				model.QBillingDocumentBuyerAmendInfo_DocumentType_7 = qBillingDocumentBuyerAmendInfo_7.QBillingDocumentBuyerAmendInfo_DocumentType;
				model.QBillingDocumentBuyerAmendInfo_Inactive_7 = qBillingDocumentBuyerAmendInfo_7.QBillingDocumentBuyerAmendInfo_Inactive;

			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetNodelByqBillingDocumentBuyerAmendInfo_6(BookmarkDocumentQueryBuyerAmendInformation model, QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_6)
		{
			try
			{
				model.QBillingDocumentBuyerAmendInfo_DocumentType_6 = qBillingDocumentBuyerAmendInfo_6.QBillingDocumentBuyerAmendInfo_DocumentType;
				model.QBillingDocumentBuyerAmendInfo_Inactive_6 = qBillingDocumentBuyerAmendInfo_6.QBillingDocumentBuyerAmendInfo_Inactive;

			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetNodelByqBillingDocumentBuyerAmendInfo_5(BookmarkDocumentQueryBuyerAmendInformation model, QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_5)
		{
			try
			{
				model.QBillingDocumentBuyerAmendInfo_DocumentType_5 = qBillingDocumentBuyerAmendInfo_5.QBillingDocumentBuyerAmendInfo_DocumentType;
				model.QBillingDocumentBuyerAmendInfo_Inactive_5 = qBillingDocumentBuyerAmendInfo_5.QBillingDocumentBuyerAmendInfo_Inactive;

			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetNodelByqBillingDocumentBuyerAmendInfo_4(BookmarkDocumentQueryBuyerAmendInformation model, QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_4)
		{
			try
			{
				model.QBillingDocumentBuyerAmendInfo_DocumentType_4 = qBillingDocumentBuyerAmendInfo_4.QBillingDocumentBuyerAmendInfo_DocumentType;
				model.QBillingDocumentBuyerAmendInfo_Inactive_4 = qBillingDocumentBuyerAmendInfo_4.QBillingDocumentBuyerAmendInfo_Inactive;

			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetNodelByqBillingDocumentBuyerAmendInfo_3(BookmarkDocumentQueryBuyerAmendInformation model, QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_3)
		{
			try
			{
				model.QBillingDocumentBuyerAmendInfo_DocumentType_3 = qBillingDocumentBuyerAmendInfo_3.QBillingDocumentBuyerAmendInfo_DocumentType;
				model.QBillingDocumentBuyerAmendInfo_Inactive_3 = qBillingDocumentBuyerAmendInfo_3.QBillingDocumentBuyerAmendInfo_Inactive;

			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetNodelByqBillingDocumentBuyerAmendInfo_2(BookmarkDocumentQueryBuyerAmendInformation model, QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_2)
		{
			try
			{
				model.QBillingDocumentBuyerAmendInfo_DocumentType_2 = qBillingDocumentBuyerAmendInfo_2.QBillingDocumentBuyerAmendInfo_DocumentType;
				model.QBillingDocumentBuyerAmendInfo_Inactive_2 = qBillingDocumentBuyerAmendInfo_2.QBillingDocumentBuyerAmendInfo_Inactive;

			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetNodelByqBillingDocumentBuyerAmendInfo_1(BookmarkDocumentQueryBuyerAmendInformation model, QBillingDocumentBuyerAmendInfo qBillingDocumentBuyerAmendInfo_1)
		{
			try
			{
				model.QBillingDocumentBuyerAmendInfo_DocumentType_1 = qBillingDocumentBuyerAmendInfo_1.QBillingDocumentBuyerAmendInfo_DocumentType;
				model.QBillingDocumentBuyerAmendInfo_Inactive_1 = qBillingDocumentBuyerAmendInfo_1.QBillingDocumentBuyerAmendInfo_Inactive;

			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private QReceiptDocumentBuyerAmendInfo GetQReceiptDocumentBuyerAmendInfoEachRow(List<QReceiptDocumentBuyerAmendInfo> qReceiptDocumentBuyerAmendInfo, int row_number)
		{
			try
			{
				var result = qReceiptDocumentBuyerAmendInfo.Where(w => w.QReceiptDocumentBuyerAmendInfo_Row == row_number).FirstOrDefault();
				if (result != null)
				{
					return result;
				}
				else
				{
					return new QReceiptDocumentBuyerAmendInfo();

				}
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private QBillingDocumentBuyerAmendInfo GetQBillingDocumentBuyerAmendInfoEachRow(List<QBillingDocumentBuyerAmendInfo> qBillingDocumentBuyerAmendInfo, int row_number)
		{
			try
			{
				var result = qBillingDocumentBuyerAmendInfo.Where(w => w.QBillingDocumentBuyerAmendInfo_Row == row_number).FirstOrDefault();
				if (result != null)
				{
					return result;
				}
				else
				{
					return new QBillingDocumentBuyerAmendInfo();

				}
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetModelByQCreditAppRequestLineAmendBuyerAmendInfo(BookmarkDocumentQueryBuyerAmendInformation model, QCreditAppRequestLineAmendBuyerAmendInfo qCreditAppRequestLineAmendBuyerAmendInfo)
		{
			try
			{
				model.QCreditAppRequestLineAmendBuyerAmendInfo_OriginalMaxPurchasePct = qCreditAppRequestLineAmendBuyerAmendInfo.QCreditAppRequestLineAmendBuyerAmendInfo_OriginalMaxPurchasePct;
				model.QCreditAppRequestLineAmendBuyerAmendInfo_OriginalPurchaseFeePct = qCreditAppRequestLineAmendBuyerAmendInfo.QCreditAppRequestLineAmendBuyerAmendInfo_OriginalPurchaseFeePct;
				model.QCreditAppRequestLineAmendBuyerAmendInfo_OriginalPurchaseFeeCalculateBase = SystemStaticData.GetTranslatedMessage(((PurchaseFeeCalculateBase)qCreditAppRequestLineAmendBuyerAmendInfo.QCreditAppRequestLineAmendBuyerAmendInfo_OriginalPurchaseFeeCalculateBase).GetAttrCode());
				model.QCreditAppRequestLineAmendBuyerAmendInfo_OriginalBillingAddressGUID = qCreditAppRequestLineAmendBuyerAmendInfo.QCreditAppRequestLineAmendBuyerAmendInfo_OriginalBillingAddressGUID;
				model.QCreditAppRequestLineAmendBuyerAmendInfo_OriginalBillingAddress = qCreditAppRequestLineAmendBuyerAmendInfo.QCreditAppRequestLineAmendBuyerAmendInfo_OriginalBillingAddress;
				model.QCreditAppRequestLineAmendBuyerAmendInfo_OriginalReceiptAddressGUID = qCreditAppRequestLineAmendBuyerAmendInfo.QCreditAppRequestLineAmendBuyerAmendInfo_OriginalReceiptAddressGUID;
				model.QCreditAppRequestLineAmendBuyerAmendInfo_OriginalReceiptAddress = qCreditAppRequestLineAmendBuyerAmendInfo.QCreditAppRequestLineAmendBuyerAmendInfo_OriginalReceiptAddress;
				model.QCreditAppRequestLineAmendBuyerAmendInfo_OriginalReceiptRemark = qCreditAppRequestLineAmendBuyerAmendInfo.QCreditAppRequestLineAmendBuyerAmendInfo_OriginalReceiptRemark;
				model.QCreditAppRequestLineAmendBuyerAmendInfo_OldAssignment = qCreditAppRequestLineAmendBuyerAmendInfo.QCreditAppRequestLineAmendBuyerAmendInfo_OldAssignment;
				model.QCreditAppRequestLineAmendBuyerAmendInfo_OriginalMethodOfPayment = qCreditAppRequestLineAmendBuyerAmendInfo.QCreditAppRequestLineAmendBuyerAmendInfo_OriginalMethodOfPayment;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetModelByQCreditAppRequestLineBuyerAmendInfo(BookmarkDocumentQueryBuyerAmendInformation model, QCreditAppRequestLineBuyerAmendInfo qCreditAppRequestLineBuyerAmendInfo)
		{
			try
			{

				model.QCreditAppRequestLineBuyerAmendInfo_BusinessSegmentGUID = qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_BusinessSegmentGUID;
				model.QCreditAppRequestLineBuyerAmendInfo_BusinessSegment = qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_BusinessSegment;
				model.QCreditAppRequestLineBuyerAmendInfo_BuyerName = qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_BuyerName;
				model.QCreditAppRequestLineBuyerAmendInfo_TaxId = qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_TaxId;
				model.QCreditAppRequestLineBuyerAmendInfo_InvoiceAddressGUID = qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_InvoiceAddressGUID;
				model.QCreditAppRequestLineBuyerAmendInfo_InvoiceAddress = qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_InvoiceAddress;
				model.QCreditAppRequestLineBuyerAmendInfo_DateOfEstablish = qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_DateOfEstablish;
				model.QCreditAppRequestLineBuyerAmendInfo_LineOfBusinessGUID = qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_LineOfBusinessGUID;
				model.QCreditAppRequestLineBuyerAmendInfo_LineOfBusiness = qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_LineOfBusiness;
				model.QCreditAppRequestLineBuyerAmendInfo_BlacklistStatusGUID = qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_BlacklistStatusGUID;
				model.QCreditAppRequestLineBuyerAmendInfo_BlacklistStatus = qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_BlacklistStatus;
				model.QCreditAppRequestLineBuyerAmendInfo_CreditScoringGUID = qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_CreditScoringGUID;
				model.QCreditAppRequestLineBuyerAmendInfo_CreditScoring = qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_CreditScoring;
				model.QCreditAppRequestLineBuyerAmendInfo_MaxPurchasePct = qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_MaxPurchasePct;
				model.QCreditAppRequestLineBuyerAmendInfo_PurchaseFeePct = qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_PurchaseFeePct;
				model.QCreditAppRequestLineBuyerAmendInfo_PurchaseFeeCalculateBase = SystemStaticData.GetTranslatedMessage(((PurchaseFeeCalculateBase)qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_PurchaseFeeCalculateBase).GetAttrCode());
				model.QCreditAppRequestLineBuyerAmendInfo_BillingAddressGUID = qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_BillingAddressGUID;
				model.QCreditAppRequestLineBuyerAmendInfo_BillingAddress = qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_BillingAddress;
				model.QCreditAppRequestLineBuyerAmendInfo_ReceiptRemark = qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_ReceiptRemark;
				model.QCreditAppRequestLineBuyerAmendInfo_MarketingComment = qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_MarketingComment;
				model.QCreditAppRequestLineBuyerAmendInfo_CreditComment = qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_CreditComment;
				model.QCreditAppRequestLineBuyerAmendInfo_ApproverComment = qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_ApproverComment;
				model.QCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID = qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID;
				model.QCreditAppRequestLineBuyerAmendInfo_ReceiptAddress = qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_ReceiptAddress;
				model.QCreditAppRequestLineBuyerAmendInfo_KYCHeaderAmend = qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_KYCHeaderAmend;
				model.QCreditAppRequestLineBuyerAmendInfo_NewAssignment = qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_NewAssignment;
				model.QCreditAppRequestLineBuyerAmendInfo_NewMethodOfPayment = qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_NewMethodOfPayment;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetmodelByQCustomerBuyerAmendInfo(BookmarkDocumentQueryBuyerAmendInformation model, QCustomerBuyerAmendInfo qCustomerBuyerAmendInfo)
		{
			try
			{
				model.QCustomerBuyerAmendInfo_CustomerId = qCustomerBuyerAmendInfo.QCustomerBuyerAmendInfo_CustomerId;
				model.QCustomerBuyerAmendInfo_CustomerName = qCustomerBuyerAmendInfo.QCustomerBuyerAmendInfo_CustomerName;
				model.QCustomerBuyerAmendInfo_ResponsibleByGUID = qCustomerBuyerAmendInfo.QCustomerBuyerAmendInfo_ResponsibleByGUID;
				model.QCustomerBuyerAmendInfo_ResponsibleBy = qCustomerBuyerAmendInfo.QCustomerBuyerAmendInfo_ResponsibleBy;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private void SetModelByQCreditAppRequestTableBuyexrAmendInfo(BookmarkDocumentQueryBuyerAmendInformation model, QCreditAppRequestTableBuyerAmendInfo qCreditAppRequestTableBuyerAmendInfo)
		{
			try
			{
				model.QCreditAppRequestTableBuyerAmendInfo_CreditAppRequestId = qCreditAppRequestTableBuyerAmendInfo.QCreditAppRequestTableBuyerAmendInfo_CreditAppRequestId;
				model.QCreditAppRequestTableBuyerAmendInfo_Description = qCreditAppRequestTableBuyerAmendInfo.QCreditAppRequestTableBuyerAmendInfo_Description;
				model.QCreditAppRequestTableBuyerAmendInfo_RefCreditAppTableGUID = qCreditAppRequestTableBuyerAmendInfo.QCreditAppRequestTableBuyerAmendInfo_RefCreditAppTableGUID;
				model.QCreditAppRequestTableBuyerAmendInfo_OriginalCreditAppTableId = qCreditAppRequestTableBuyerAmendInfo.QCreditAppRequestTableBuyerAmendInfo_OriginalCreditAppTableId;
				model.QCreditAppRequestTableBuyerAmendInfo_DocumentStatusGUID = qCreditAppRequestTableBuyerAmendInfo.QCreditAppRequestTableBuyerAmendInfo_DocumentStatusGUID;
				model.QCreditAppRequestTableBuyerAmendInfo_Status = qCreditAppRequestTableBuyerAmendInfo.QCreditAppRequestTableBuyerAmendInfo_Status;
				model.QCreditAppRequestTableBuyerAmendInfo_RequestDate = qCreditAppRequestTableBuyerAmendInfo.QCreditAppRequestTableBuyerAmendInfo_RequestDate;
				model.QCreditAppRequestTableBuyerAmendInfo_CustomerTableGUID = qCreditAppRequestTableBuyerAmendInfo.QCreditAppRequestTableBuyerAmendInfo_CustomerTableGUID;
				model.QCreditAppRequestTableBuyerAmendInfo_Remark = qCreditAppRequestTableBuyerAmendInfo.QCreditAppRequestTableBuyerAmendInfo_Remark;


			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private QCreditAppReqLineFinBuyerAmendInfo GetQCreditAppReqLineFinBuyerAmendInfoEachRow(int row, List<QCreditAppReqLineFinBuyerAmendInfo> qCreditAppReqLineFinBuyerAmendInfo, QCreditAppRequestLineBuyerAmendInfo qCreditAppRequestLineBuyerAmendInfo)
		{
			try
			{
				var data = qCreditAppReqLineFinBuyerAmendInfo.Where(wh => wh.QCreditAppReqLineFinBuyerAmendInfo_Row == row).FirstOrDefault();
				if (data != null)
				{
					var model = new QCreditAppReqLineFinBuyerAmendInfo();
					model.QCreditAppReqLineFinBuyerAmendInfo_Year = data.QCreditAppReqLineFinBuyerAmendInfo_Year;
					model.QCreditAppReqLineFinBuyerAmendInfo_RegisteredCapital = GetFinanceStatementAmount(1, data, qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID);
					model.QCreditAppReqLineFinBuyerAmendInfo_PaidCapital = GetFinanceStatementAmount(2, data, qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID);
					model.QCreditAppReqLineFinBuyerAmendInfo_TotalAsset = GetFinanceStatementAmount(3, data, qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID);
					model.QCreditAppReqLineFinBuyerAmendInfo_TotalLiability = GetFinanceStatementAmount(4, data, qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID);
					model.QCreditAppReqLineFinBuyerAmendInfo_TotalEquity = GetFinanceStatementAmount(5, data, qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID);
					model.QCreditAppReqLineFinBuyerAmendInfo_TotalRevenue = GetFinanceStatementAmount(6, data, qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID);
					model.QCreditAppReqLineFinBuyerAmendInfo_TotalCOGS = GetFinanceStatementAmount(7, data, qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID);
					model.QCreditAppReqLineFinBuyerAmendInfo_TotalGrossProfit = GetFinanceStatementAmount(8, data, qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID);
					model.QCreditAppReqLineFinBuyerAmendInfo_TotalOperExpFirst = GetFinanceStatementAmount(9, data, qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID);
					model.QCreditAppReqLineFinBuyerAmendInfo_TotalNetProfitFirst = GetFinanceStatementAmount(10, data, qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID);
					model.QCreditAppReqLineFinBuyerAmendInfo_NetProfitPercent = GetFinanceStatementAmount(11, data, qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID);
					model.QCreditAppReqLineFinBuyerAmendInfo_lDE = GetFinanceStatementAmount(12, data, qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID);
					model.QCreditAppReqLineFinBuyerAmendInfo_QuickRatio = GetFinanceStatementAmount(13, data, qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID);
					model.QCreditAppReqLineFinBuyerAmendInfo_IntCoverageRatio = GetFinanceStatementAmount(14, data, qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID);
					return model;

				}
				else
				{
					return new QCreditAppReqLineFinBuyerAmendInfo();


				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private decimal GetFinanceStatementAmount(int ordering, QCreditAppReqLineFinBuyerAmendInfo data, Guid? qCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID)
		{
			try
			{
				var result = (from financialStatementTrans in db.Set<FinancialStatementTrans>()
							  where financialStatementTrans.RefGUID == qCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID && financialStatementTrans.RefType == (int)RefType.CreditAppRequestLine
							  && financialStatementTrans.Year == data.QCreditAppReqLineFinBuyerAmendInfo_Year && financialStatementTrans.Ordering == ordering
							  select financialStatementTrans.Amount).FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private List<QCreditAppReqLineFinBuyerAmendInfo> mainQCreditAppReqLineFinBuyerAmendInfo(QCreditAppRequestLineBuyerAmendInfo qCreditAppRequestLineBuyerAmendInfo)
		{
			try
			{
				var result = (from financialStatementTrans in db.Set<FinancialStatementTrans>()

							  where financialStatementTrans.RefType == (int)RefType.CreditAppRequestLine

							  && financialStatementTrans.RefGUID == qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID

							  group new
							  {
								  financialStatementTrans
							  }
							  by new
							  {
								  financialStatementTrans.Year,
							  } into g
							  select new QCreditAppReqLineFinBuyerAmendInfo
							  {

								  QCreditAppReqLineFinBuyerAmendInfo_Year = g.Key.Year,
								  QCreditAppReqLineFinBuyerAmendInfo_Row = 0
							  }
							  ).OrderByDescending(o => o.QCreditAppReqLineFinBuyerAmendInfo_Year).Take(3).ToList();
				if (result != null)
				{
					var number = 0;
					foreach (var item in result)
					{
						item.QCreditAppReqLineFinBuyerAmendInfo_Row = number + 1;
						number = number + 1;
					}
					return result;
				}
				else
				{
					return new List<QCreditAppReqLineFinBuyerAmendInfo>();

				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private List<QReceiptDocumentBuyerAmendInfo> GetQReceiptDocumentBuyerAmendInfo(QCreditAppRequestLineBuyerAmendInfo qCreditAppRequestLineBuyerAmendInfo)
		{
			try
			{
				var result = (from documentConditionTrans in db.Set<DocumentConditionTrans>()
							  join documentType in db.Set<DocumentType>()
							  on documentConditionTrans.DocumentTypeGUID equals documentType.DocumentTypeGUID into ljdocumentType
							  from documentType in ljdocumentType.DefaultIfEmpty()
							  where documentConditionTrans.RefGUID == qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID
							  && documentConditionTrans.RefType == (int)RefType.CreditAppRequestLine 
							  && documentConditionTrans.DocConVerifyType == (int)DocConVerifyType.Receipt
							  && documentConditionTrans.Inactive == false
							  select new QReceiptDocumentBuyerAmendInfo
							  {
								  QReceiptDocumentBuyerAmendInfo_DocumentType = documentType.Description,
								  QReceiptDocumentBuyerAmendInfo_Inactive = documentConditionTrans.Inactive,
								  QReceiptDocumentBuyerAmendInfo_Row = 0
							  }).Take(10).ToList();
				var row_number = 0;
				foreach (var item in result)
				{
					item.QReceiptDocumentBuyerAmendInfo_Row = row_number + 1;
					row_number = row_number + 1;
				}
				if (result != null)
				{
					return result;
				}
				else
				{
					return new List<QReceiptDocumentBuyerAmendInfo>();

				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private List<QBillingDocumentBuyerAmendInfo> GetQBillingDocumentBuyerAmendInfo(QCreditAppRequestLineBuyerAmendInfo qCreditAppRequestLineBuyerAmendInfo)
		{
			try
			{
				var result = (from documentConditionTrans in db.Set<DocumentConditionTrans>()
							  join documentType in db.Set<DocumentType>()
							  on documentConditionTrans.DocumentTypeGUID equals documentType.DocumentTypeGUID into ljdocumentType
							  from documentType in ljdocumentType.DefaultIfEmpty()
							  where documentConditionTrans.RefGUID == qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID
							  && documentConditionTrans.RefType == (int)RefType.CreditAppRequestLine 
							  && documentConditionTrans.DocConVerifyType == (int)DocConVerifyType.Billing
							  && documentConditionTrans.Inactive == false
							  orderby documentType.Description
							  select new QBillingDocumentBuyerAmendInfo
							  {

								  QBillingDocumentBuyerAmendInfo_DocumentType = documentType.Description,
								  QBillingDocumentBuyerAmendInfo_Inactive = documentConditionTrans.Inactive,
								  QBillingDocumentBuyerAmendInfo_Row = 0
							  }
					).Take(10).ToList();
				var row_number = 0;
				foreach (var item in result)
				{
					item.QBillingDocumentBuyerAmendInfo_Row = row_number + 1;
					row_number = row_number + 1;
				}
				if (result != null)
				{
					return result;
				}
				else
				{
					return new List<QBillingDocumentBuyerAmendInfo>();

				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private QCreditAppRequestLineAmendBuyerAmendInfo GetQCreditAppRequestLineAmendBuyerAmendInfo(QCreditAppRequestLineBuyerAmendInfo qCreditAppRequestLineBuyerAmendInfo)
		{
			try
			{
				var result = (from caLineAmend in db.Set<CreditAppRequestLineAmend>()
							  join addressTransBill in db.Set<AddressTrans>()
							  on caLineAmend.OriginalBillingAddressGUID equals addressTransBill.AddressTransGUID into ljaddressTransBill
							  from addressTransBill in ljaddressTransBill.DefaultIfEmpty()

							  join addressTransReceipt in db.Set<AddressTrans>()
							  on caLineAmend.OriginalBillingAddressGUID equals addressTransReceipt.AddressTransGUID into ljaddressTransReceipt
							  from addressTransReceipt in ljaddressTransReceipt.DefaultIfEmpty()

							  join assignmentMethod in db.Set<AssignmentMethod>()
							  on caLineAmend.OriginalAssignmentMethodGUID equals assignmentMethod.AssignmentMethodGUID into ljassignmentMethod
							  from assignmentMethod in ljassignmentMethod.DefaultIfEmpty()

							  join methodOfPayment in db.Set<MethodOfPayment>()
							  on caLineAmend.OriginalMethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID into ljmethodOfPayment 
							  from methodOfPayment in ljmethodOfPayment.DefaultIfEmpty()

							  where caLineAmend.CreditAppRequestLineGUID == qCreditAppRequestLineBuyerAmendInfo.QCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID
							  select new QCreditAppRequestLineAmendBuyerAmendInfo
							  {
								  QCreditAppRequestLineAmendBuyerAmendInfo_OriginalMaxPurchasePct = caLineAmend.OriginalMaxPurchasePct,
								  QCreditAppRequestLineAmendBuyerAmendInfo_OriginalPurchaseFeePct = caLineAmend.OriginalPurchaseFeePct,
								  QCreditAppRequestLineAmendBuyerAmendInfo_OriginalPurchaseFeeCalculateBase = caLineAmend.OriginalPurchaseFeeCalculateBase,
								  QCreditAppRequestLineAmendBuyerAmendInfo_OriginalBillingAddressGUID = caLineAmend.OriginalBillingAddressGUID,
								  QCreditAppRequestLineAmendBuyerAmendInfo_OriginalBillingAddress = String.Concat(addressTransBill.Address1, addressTransBill.Address2),
								  QCreditAppRequestLineAmendBuyerAmendInfo_OriginalReceiptAddressGUID = caLineAmend.OriginalReceiptAddressGUID,
								  QCreditAppRequestLineAmendBuyerAmendInfo_OriginalReceiptAddress = string.Concat(addressTransReceipt.Address1, addressTransReceipt.Address2),
								  QCreditAppRequestLineAmendBuyerAmendInfo_OriginalReceiptRemark = caLineAmend.OriginalReceiptRemark,

								QCreditAppRequestLineAmendBuyerAmendInfo_OldAssignment = assignmentMethod.Description,
								QCreditAppRequestLineAmendBuyerAmendInfo_OriginalMethodOfPayment = methodOfPayment.Description
	}).FirstOrDefault();
				if (result != null)
				{
					return result;
				}
				else
				{
					return new QCreditAppRequestLineAmendBuyerAmendInfo();
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}

		private QCreditAppRequestLineBuyerAmendInfo GetQCreditAppRequestLineBuyerAmendInfo(Guid refGUID)
		{
			try
			{
				var result = (from creditApprRquestLine in db.Set<CreditAppRequestLine>()

							  join buyerTable in db.Set<BuyerTable>()
							  on creditApprRquestLine.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
							  from buyerTable in ljbuyerTable.DefaultIfEmpty()

							  join addressTrans in db.Set<AddressTrans>()
							  on creditApprRquestLine.InvoiceAddressGUID equals addressTrans.AddressTransGUID into ljaddressTrans
							  from addressTrans in ljaddressTrans.DefaultIfEmpty()

							  join businessSegment in db.Set<BusinessSegment>()
							  on buyerTable.BusinessSegmentGUID equals businessSegment.BusinessSegmentGUID into ljbusinessSegment
							  from businessSegment in ljbusinessSegment.DefaultIfEmpty()

							  join lineOfBusiness in db.Set<LineOfBusiness>()
							  on buyerTable.LineOfBusinessGUID equals lineOfBusiness.LineOfBusinessGUID into ljlineOfBusiness
							  from lineOfBusiness in ljlineOfBusiness.DefaultIfEmpty()

							  join blackListStatus in db.Set<BlacklistStatus>()
							  on creditApprRquestLine.BlacklistStatusGUID equals blackListStatus.BlacklistStatusGUID into ljblackListStatus
							  from blackListStatus in ljblackListStatus.DefaultIfEmpty()

							  join creditScoring in db.Set<CreditScoring>()
							  on creditApprRquestLine.CreditScoringGUID equals creditScoring.CreditScoringGUID into ljcreditScoring
							  from creditScoring in ljcreditScoring.DefaultIfEmpty()

							  join addressTransBill in db.Set<AddressTrans>()
							  on creditApprRquestLine.BillingAddressGUID equals addressTransBill.AddressTransGUID into ljaddressTransBill
							  from addressTransBill in ljaddressTransBill.DefaultIfEmpty()

							  join addresstransReceipt in db.Set<AddressTrans>()
							  on creditApprRquestLine.ReceiptAddressGUID equals addresstransReceipt.AddressTransGUID into ljaddresstransReceipt
							  from addresstransReceipt in ljaddresstransReceipt.DefaultIfEmpty()

							  join creditAppReqTable in db.Set<CreditAppRequestTable>()
							  on creditApprRquestLine.CreditAppRequestTableGUID equals creditAppReqTable.CreditAppRequestTableGUID into ljcreditAppReqTable
							  from creditAppReqTable in ljcreditAppReqTable.DefaultIfEmpty()

							  join kycSetup in db.Set<KYCSetup>()
							  on creditAppReqTable.KYCGUID equals kycSetup.KYCSetupGUID into ljkycSetup
							  from kycSetup in ljkycSetup.DefaultIfEmpty()

							  join assignmentMethod in db.Set<AssignmentMethod>()
							  on creditApprRquestLine.AssignmentMethodGUID equals assignmentMethod.AssignmentMethodGUID into ljassignmentMethod
							  from assignmentMethod in ljassignmentMethod.DefaultIfEmpty()

							  join methodOfPayment in db.Set<MethodOfPayment>()
							  on creditApprRquestLine.MethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID into ljmethodOfPayment
							  from methodOfPayment in ljmethodOfPayment.DefaultIfEmpty()

							  where creditApprRquestLine.CreditAppRequestTableGUID == refGUID
							  select new QCreditAppRequestLineBuyerAmendInfo
							  {
								  QCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID = creditApprRquestLine.CreditAppRequestLineGUID,
								  QCreditAppRequestLineBuyerAmendInfo_BusinessSegmentGUID = buyerTable.BusinessSegmentGUID,
								  QCreditAppRequestLineBuyerAmendInfo_BusinessSegment = businessSegment.Description,
								  QCreditAppRequestLineBuyerAmendInfo_BuyerName = buyerTable.BuyerName,
								  QCreditAppRequestTableBuyerAmendInfo_BuyerTableGUID = buyerTable.BuyerTableGUID,
								  QCreditAppRequestLineBuyerAmendInfo_TaxId = buyerTable.TaxId,
								  QCreditAppRequestLineBuyerAmendInfo_InvoiceAddressGUID = creditApprRquestLine.InvoiceAddressGUID,
								  QCreditAppRequestLineBuyerAmendInfo_InvoiceAddress = String.Concat(addressTrans.Address1, addressTrans.Address2),
								  QCreditAppRequestLineBuyerAmendInfo_DateOfEstablish = buyerTable.DateOfEstablish,
								  QCreditAppRequestLineBuyerAmendInfo_LineOfBusinessGUID = buyerTable.LineOfBusinessGUID,
								  QCreditAppRequestLineBuyerAmendInfo_LineOfBusiness = lineOfBusiness.Description,
								  QCreditAppRequestLineBuyerAmendInfo_BlacklistStatusGUID = creditApprRquestLine.BlacklistStatusGUID,
								  QCreditAppRequestLineBuyerAmendInfo_BlacklistStatus = blackListStatus.Description,
								  QCreditAppRequestLineBuyerAmendInfo_CreditScoringGUID = creditApprRquestLine.CreditScoringGUID,
								  QCreditAppRequestLineBuyerAmendInfo_CreditScoring = creditScoring.Description,
								  QCreditAppRequestLineBuyerAmendInfo_MaxPurchasePct = creditApprRquestLine.MaxPurchasePct,
								  QCreditAppRequestLineBuyerAmendInfo_PurchaseFeePct = creditApprRquestLine.PurchaseFeePct,
								  QCreditAppRequestLineBuyerAmendInfo_PurchaseFeeCalculateBase = creditApprRquestLine.PurchaseFeeCalculateBase,
								  QCreditAppRequestLineBuyerAmendInfo_BillingAddressGUID = creditApprRquestLine.BillingAddressGUID,
								  QCreditAppRequestLineBuyerAmendInfo_BillingAddress = String.Concat(addressTransBill.Address1, addressTransBill.Address2),
								  QCreditAppRequestLineBuyerAmendInfo_ReceiptRemark = creditApprRquestLine.ReceiptRemark,
								  QCreditAppRequestLineBuyerAmendInfo_MarketingComment = creditApprRquestLine.MarketingComment,
								  QCreditAppRequestLineBuyerAmendInfo_CreditComment = creditApprRquestLine.CreditComment,
								  QCreditAppRequestLineBuyerAmendInfo_ApproverComment = creditApprRquestLine.ApproverComment,
								  QCreditAppRequestLineBuyerAmendInfo_ReceiptAddress = string.Concat(addresstransReceipt.Address1, addresstransReceipt.Address2),
									QCreditAppRequestLineBuyerAmendInfo_KYCHeaderAmend = kycSetup.Description,
									QCreditAppRequestLineBuyerAmendInfo_NewAssignment = assignmentMethod.Description,
									QCreditAppRequestLineBuyerAmendInfo_NewMethodOfPayment = methodOfPayment.Description
	}).FirstOrDefault(); ;
				if (result != null)
				{
					return result;
				}
				else
				{
					return new QCreditAppRequestLineBuyerAmendInfo();
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private QCustomerBuyerAmendInfo GetQCustomerBuyerAmendInfo(QCreditAppRequestTableBuyerAmendInfo qCreditAppRequestTableBuyerAmendInfo)
		{
			try
			{
				var result = (from customerTable in db.Set<CustomerTable>()
							  join employeeTable in db.Set<EmployeeTable>()
							  on customerTable.ResponsibleByGUID equals employeeTable.EmployeeTableGUID into ljemployeeTable
							  from employeeTable in ljemployeeTable.DefaultIfEmpty()
							  where customerTable.CustomerTableGUID == qCreditAppRequestTableBuyerAmendInfo.QCreditAppRequestTableBuyerAmendInfo_CustomerTableGUID
							  select new QCustomerBuyerAmendInfo
							  {
								  QCustomerBuyerAmendInfo_CustomerId = customerTable.CustomerId,
								  QCustomerBuyerAmendInfo_CustomerName = customerTable.Name,
								  QCustomerBuyerAmendInfo_ResponsibleByGUID = customerTable.ResponsibleByGUID,
								  QCustomerBuyerAmendInfo_ResponsibleBy = employeeTable.Name
							  }).FirstOrDefault(); ;
				if (result != null)
				{
					return result;
				}
				else
				{
					return new QCustomerBuyerAmendInfo();
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private QCreditAppRequestTableBuyerAmendInfo GetQCreditAppRequestTableBuyerAmendInfo(Guid refGUID)
		{
			try
			{
				var result = (from creditAppRequestTable in db.Set<CreditAppRequestTable>()
							  join creditAppTable in db.Set<CreditAppTable>()
							  on creditAppRequestTable.RefCreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljcreditAppTable
							  from creditAppTable in ljcreditAppTable.DefaultIfEmpty()
							  where creditAppRequestTable.CreditAppRequestTableGUID == refGUID
							  select new QCreditAppRequestTableBuyerAmendInfo
							  {
								  QCreditAppRequestTableBuyerAmendInfo_CreditAppRequestId = creditAppRequestTable.CreditAppRequestId,
								  QCreditAppRequestTableBuyerAmendInfo_Description = creditAppRequestTable.Description,
								  QCreditAppRequestTableBuyerAmendInfo_RefCreditAppTableGUID = creditAppRequestTable.RefCreditAppTableGUID,
								  QCreditAppRequestTableBuyerAmendInfo_OriginalCreditAppTableId = creditAppTable.CreditAppId,
								  QCreditAppRequestTableBuyerAmendInfo_OriginalRefCreditAppRequestTableGUID = creditAppTable.RefCreditAppRequestTableGUID,
								  QCreditAppRequestTableBuyerAmendInfo_DocumentStatusGUID = creditAppRequestTable.DocumentStatusGUID,
								  QCreditAppRequestTableBuyerAmendInfo_RequestDate = creditAppRequestTable.RequestDate,
								  QCreditAppRequestTableBuyerAmendInfo_CustomerTableGUID = creditAppRequestTable.CustomerTableGUID,
								  QCreditAppRequestTableBuyerAmendInfo_Remark = creditAppRequestTable.Remark
							  }).AsNoTracking().FirstOrDefault();
				if (result != null)
				{
					var statusResult = (from documentStatus in db.Set<DocumentStatus>()
										join documentProcess in db.Set<DocumentProcess>()
										on documentStatus.DocumentProcessGUID equals documentProcess.DocumentProcessGUID into ljdocumentProcess
										from documentProcess in ljdocumentProcess.DefaultIfEmpty()
										where documentStatus.DocumentStatusGUID == result.QCreditAppRequestTableBuyerAmendInfo_DocumentStatusGUID
										&& documentProcess.ProcessId == ((int)DocumentProcessStatus.CreditAppRequestTable).ToString()
										select documentStatus.Description
										).AsNoTracking().FirstOrDefault();

					result.QCreditAppRequestTableBuyerAmendInfo_Status = statusResult;
					return result;
				}
				else
				{

					return new QCreditAppRequestTableBuyerAmendInfo();


				};

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}
