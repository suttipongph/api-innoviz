using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IServiceFeeConditionTransRepo
	{
		#region DropDown
		IEnumerable<SelectItem<ServiceFeeConditionTransItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<ServiceFeeConditionTransListView> GetListvw(SearchParameter search);
		ServiceFeeConditionTransItemView GetByIdvw(Guid id);
		ServiceFeeConditionTrans Find(params object[] keyValues);
		ServiceFeeConditionTrans GetServiceFeeConditionTransByIdNoTracking(Guid guid);
		ServiceFeeConditionTrans CreateServiceFeeConditionTrans(ServiceFeeConditionTrans serviceFeeConditionTrans);
		void CreateServiceFeeConditionTransVoid(ServiceFeeConditionTrans serviceFeeConditionTrans);
		ServiceFeeConditionTrans UpdateServiceFeeConditionTrans(ServiceFeeConditionTrans dbServiceFeeConditionTrans, ServiceFeeConditionTrans inputServiceFeeConditionTrans, List<string> skipUpdateFields = null);
		void UpdateServiceFeeConditionTransVoid(ServiceFeeConditionTrans dbServiceFeeConditionTrans, ServiceFeeConditionTrans inputServiceFeeConditionTrans, List<string> skipUpdateFields = null);
		void Remove(ServiceFeeConditionTrans item);
		ServiceFeeConditionTrans GetServiceFeeConditionTransByIdNoTrackingByAccessLevel(Guid guid);
		IQueryable<ServiceFeeConditionTrans> GetServiceFeeConditionTransByRefernceNoTracking(Guid refGUID, int refType);
		List<ServiceFeeConditionTransItemViewMap> GetServiceFeeConditionTransViewMapByRefGUIDAndTypeNoTracking(Guid refGUID, int refType);
		void ValidateAdd(ServiceFeeConditionTrans item);
		void ValidateAdd(IEnumerable<ServiceFeeConditionTrans> items);

		#region K2 Function
		ServiceFeeConditionTrans GenServiceFeeCondCreditReqFeeByCAReq(Guid creditAppRequestTableGUID, string owner = null, Guid? ownerBusinessUnitGUID = null);
		#endregion
		IEnumerable<QServiceFeeConditionLine_BuyerCreditApplication> GetServiceFeeConditionLineByBuyerCreditApplication(Guid creditAppRequestLineGUID);

		decimal GetSumServiceFeeConditionByCreditAppRequestLine(Guid creditAppRequestLineGUID);
	}
    public class ServiceFeeConditionTransRepo : CompanyBaseRepository<ServiceFeeConditionTrans>, IServiceFeeConditionTransRepo
	{
		public ServiceFeeConditionTransRepo(SmartAppDbContext context) : base(context) { }
		public ServiceFeeConditionTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ServiceFeeConditionTrans GetServiceFeeConditionTransByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ServiceFeeConditionTransGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<ServiceFeeConditionTransItemViewMap> GetDropDownQuery()
		{
			return (from serviceFeeConditionTrans in Entity
					select new ServiceFeeConditionTransItemViewMap
					{
						CompanyGUID = serviceFeeConditionTrans.CompanyGUID,
						Owner = serviceFeeConditionTrans.Owner,
						OwnerBusinessUnitGUID = serviceFeeConditionTrans.OwnerBusinessUnitGUID,
						ServiceFeeConditionTransGUID = serviceFeeConditionTrans.ServiceFeeConditionTransGUID
					});
		}
		public IEnumerable<SelectItem<ServiceFeeConditionTransItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<ServiceFeeConditionTrans>(search, SysParm.CompanyGUID);
				var serviceFeeConditionTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<ServiceFeeConditionTransItemViewMap, ServiceFeeConditionTransItemView>().ToDropDownItem(search.ExcludeRowData);


				return serviceFeeConditionTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<ServiceFeeConditionTransListViewMap> GetListQuery()
		{
			return (from serviceFeeConditionTrans in Entity
					join invoiceRevenueType in db.Set<InvoiceRevenueType>()
					on serviceFeeConditionTrans.InvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID

					select new ServiceFeeConditionTransListViewMap
				{
						CompanyGUID = serviceFeeConditionTrans.CompanyGUID,
						Owner = serviceFeeConditionTrans.Owner,
						OwnerBusinessUnitGUID = serviceFeeConditionTrans.OwnerBusinessUnitGUID,
						ServiceFeeConditionTransGUID = serviceFeeConditionTrans.ServiceFeeConditionTransGUID,
						Ordering = serviceFeeConditionTrans.Ordering,
						InvoiceRevenueTypeGUID = serviceFeeConditionTrans.InvoiceRevenueTypeGUID,
						Description = serviceFeeConditionTrans.Description,
						AmountBeforeTax = serviceFeeConditionTrans.AmountBeforeTax,
						Inactive = serviceFeeConditionTrans.Inactive,
						InvoiceRevenueType_Values = SmartAppUtil.GetDropDownLabel(invoiceRevenueType.RevenueTypeId, invoiceRevenueType.Description),
						InvoiceRevenueType_revenueTypeId = invoiceRevenueType.RevenueTypeId,
						RefGUID = serviceFeeConditionTrans.RefGUID,
						InvoiceRevenueType_ServiceFeeCategory = invoiceRevenueType.ServiceFeeCategory
					});
		}
		public SearchResult<ServiceFeeConditionTransListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<ServiceFeeConditionTransListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<ServiceFeeConditionTrans>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<ServiceFeeConditionTransListViewMap, ServiceFeeConditionTransListView>();
				result = list.SetSearchResult<ServiceFeeConditionTransListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<ServiceFeeConditionTransItemViewMap> GetItemQuery()
		{
			return (from serviceFeeConditionTrans in Entity
					join company in db.Set<Company>()
					on serviceFeeConditionTrans.CompanyGUID equals company.CompanyGUID

					join invoiceRevenueType in db.Set<InvoiceRevenueType>()
					on serviceFeeConditionTrans.InvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID

					join ownerBU in db.Set<BusinessUnit>()
					on serviceFeeConditionTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljServiceFeeConditionTransOwnerBU
					from ownerBU in ljServiceFeeConditionTransOwnerBU.DefaultIfEmpty()

					join creditAppRequestTable in db.Set<CreditAppRequestTable>()
					on serviceFeeConditionTrans.CreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID into ljCreditAppRequestTable
					from creditAppRequestTable in ljCreditAppRequestTable.DefaultIfEmpty()

					join creditAppRequestTableRef in db.Set<CreditAppRequestTable>()
					on serviceFeeConditionTrans.RefGUID equals creditAppRequestTableRef.CreditAppRequestTableGUID into ljCreditAppRequestTableRef
					from creditAppRequestTableRef in ljCreditAppRequestTableRef.DefaultIfEmpty()

					join creditAppRequestLine in db.Set<CreditAppRequestLine>()
					on serviceFeeConditionTrans.RefGUID equals creditAppRequestLine.CreditAppRequestLineGUID into ljCreditAppRequestLine
					from creditAppRequestLine in ljCreditAppRequestLine.DefaultIfEmpty()

					join creditAppTable in db.Set<CreditAppTable>()
					on serviceFeeConditionTrans.RefGUID equals creditAppTable.CreditAppTableGUID into ljCreditAppTable
					from creditAppTable in ljCreditAppTable.DefaultIfEmpty()

					join creditAppLine in db.Set<CreditAppLine>()
					on serviceFeeConditionTrans.RefGUID equals creditAppLine.CreditAppLineGUID into ljCreditAppLine
					from creditAppLine in ljCreditAppLine.DefaultIfEmpty()

					join lineCreditAppTable in db.Set<CreditAppTable>()
					on creditAppLine.CreditAppTableGUID equals lineCreditAppTable.CreditAppTableGUID into ljLineCreditAppTable
					from lineCreditAppTable in ljLineCreditAppTable.DefaultIfEmpty()

					select new ServiceFeeConditionTransItemViewMap
					{
						CompanyGUID = serviceFeeConditionTrans.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = serviceFeeConditionTrans.Owner,
						OwnerBusinessUnitGUID = serviceFeeConditionTrans.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = serviceFeeConditionTrans.CreatedBy,
						CreatedDateTime = serviceFeeConditionTrans.CreatedDateTime,
						ModifiedBy = serviceFeeConditionTrans.ModifiedBy,
						ModifiedDateTime = serviceFeeConditionTrans.ModifiedDateTime,
						ServiceFeeConditionTransGUID = serviceFeeConditionTrans.ServiceFeeConditionTransGUID,
						AmountBeforeTax = serviceFeeConditionTrans.AmountBeforeTax,
						CreditAppRequestTableGUID = serviceFeeConditionTrans.CreditAppRequestTableGUID,
						Description = serviceFeeConditionTrans.Description,
						Inactive = serviceFeeConditionTrans.Inactive,
						InvoiceRevenueTypeGUID = serviceFeeConditionTrans.InvoiceRevenueTypeGUID,
						Ordering = serviceFeeConditionTrans.Ordering,
						RefGUID = serviceFeeConditionTrans.RefGUID,
						InvoiceRevenueType_ServiceFeeCategory = invoiceRevenueType.ServiceFeeCategory,
						RefServiceFeeConditionTransGUID = serviceFeeConditionTrans.RefServiceFeeConditionTransGUID,
						CreditAppRequestTable_CreditAppRequestId = (creditAppRequestTable != null) ? creditAppRequestTable.CreditAppRequestId : null,
						RefType = serviceFeeConditionTrans.RefType,
						RefId = (creditAppRequestTableRef != null && serviceFeeConditionTrans.RefType == (int)RefType.CreditAppRequestTable) ? creditAppRequestTableRef.CreditAppRequestId :
								(creditAppRequestLine != null && serviceFeeConditionTrans.RefType == (int)RefType.CreditAppRequestLine) ? creditAppRequestLine.LineNum.ToString() :
								(creditAppTable != null && serviceFeeConditionTrans.RefType == (int)RefType.CreditAppTable) ? creditAppTable.CreditAppId :
								(creditAppLine != null && serviceFeeConditionTrans.RefType == (int)RefType.CreditAppLine) ? creditAppLine.LineNum.ToString() :
								null,
						InvoiceRevenueType_ProductType = (serviceFeeConditionTrans.RefType == (int)RefType.CreditAppRequestTable || serviceFeeConditionTrans.RefType == (int)RefType.CreditAppRequestLine) ? creditAppRequestTable.ProductType :
														 serviceFeeConditionTrans.RefType == (int)RefType.CreditAppTable ? creditAppTable.ProductType : serviceFeeConditionTrans.RefType == (int)RefType.CreditAppLine ? lineCreditAppTable.ProductType : 0,
					
						RowVersion = serviceFeeConditionTrans.RowVersion,
					});
		}
		public ServiceFeeConditionTransItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<ServiceFeeConditionTransItemViewMap, ServiceFeeConditionTransItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public ServiceFeeConditionTrans CreateServiceFeeConditionTrans(ServiceFeeConditionTrans serviceFeeConditionTrans)
		{
			try
			{
				serviceFeeConditionTrans.ServiceFeeConditionTransGUID = Guid.NewGuid();
				base.Add(serviceFeeConditionTrans);
				return serviceFeeConditionTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateServiceFeeConditionTransVoid(ServiceFeeConditionTrans serviceFeeConditionTrans)
		{
			try
			{
				CreateServiceFeeConditionTrans(serviceFeeConditionTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ServiceFeeConditionTrans UpdateServiceFeeConditionTrans(ServiceFeeConditionTrans dbServiceFeeConditionTrans, ServiceFeeConditionTrans inputServiceFeeConditionTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbServiceFeeConditionTrans = dbServiceFeeConditionTrans.MapUpdateValues<ServiceFeeConditionTrans>(inputServiceFeeConditionTrans);
				base.Update(dbServiceFeeConditionTrans);
				return dbServiceFeeConditionTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateServiceFeeConditionTransVoid(ServiceFeeConditionTrans dbServiceFeeConditionTrans, ServiceFeeConditionTrans inputServiceFeeConditionTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbServiceFeeConditionTrans = dbServiceFeeConditionTrans.MapUpdateValues<ServiceFeeConditionTrans>(inputServiceFeeConditionTrans, skipUpdateFields);
				base.Update(dbServiceFeeConditionTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public ServiceFeeConditionTrans GetServiceFeeConditionTransByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ServiceFeeConditionTransGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IQueryable<ServiceFeeConditionTrans> GetServiceFeeConditionTransByRefernceNoTracking(Guid refGUID, int refType)
		{
			try
			{
				var result = Entity.Where(item => item.RefGUID == refGUID && item.RefType == refType)
					.AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		
		public List<ServiceFeeConditionTransItemViewMap> GetServiceFeeConditionTransViewMapByRefGUIDAndTypeNoTracking(Guid refGUID, int refType)
		{
			try
			{
				var result = (from serviceFeeConditionTrans in Entity
						join invoiceRevenueType in db.Set<InvoiceRevenueType>()
						on serviceFeeConditionTrans.InvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID

						select new ServiceFeeConditionTransItemViewMap
						{
							CompanyGUID = serviceFeeConditionTrans.CompanyGUID,
							Owner = serviceFeeConditionTrans.Owner,
							OwnerBusinessUnitGUID = serviceFeeConditionTrans.OwnerBusinessUnitGUID,
							ServiceFeeConditionTransGUID = serviceFeeConditionTrans.ServiceFeeConditionTransGUID,
							Ordering = serviceFeeConditionTrans.Ordering,
							InvoiceRevenueTypeGUID = serviceFeeConditionTrans.InvoiceRevenueTypeGUID,
							Description = serviceFeeConditionTrans.Description,
							AmountBeforeTax = serviceFeeConditionTrans.AmountBeforeTax,
							Inactive = serviceFeeConditionTrans.Inactive,
							RefGUID = serviceFeeConditionTrans.RefGUID,
							InvoiceRevenueType_ServiceFeeCategory = invoiceRevenueType.ServiceFeeCategory,
							InvoiceRevenueType_FeeTaxGUID = invoiceRevenueType.FeeTaxGUID,
							InvoiceRevenueType_FeeWHTGUID = invoiceRevenueType.FeeWHTGUID,
							RefType = serviceFeeConditionTrans.RefType
						}).AsNoTracking();
				return result.Where(wh => wh.RefGUID == refGUID && wh.RefType == refType)
					.OrderBy(or => or.Ordering).ToList();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public override void ValidateAdd(IEnumerable<ServiceFeeConditionTrans> items)
		{
			base.ValidateAdd(items);
		}
		public override void ValidateAdd(ServiceFeeConditionTrans item)
		{
			base.ValidateAdd(item);
		}
		#region K2 Function
		public ServiceFeeConditionTrans GenServiceFeeCondCreditReqFeeByCAReq(Guid creditAppRequestTableGUID, string owner = null, Guid? ownerBusinessUnitGUID = null)
        {
			try
			{
				IQueryable<ServiceFeeConditionTrans> queryOfServiceFeeConditionTrans = GetServiceFeeConditionTransByRefernceNoTracking(creditAppRequestTableGUID, (int)RefType.CreditAppRequestTable);
				return (from creditAppRequestTable in db.Set<CreditAppRequestTable>()
						join companyParameter in db.Set<CompanyParameter>()
						on creditAppRequestTable.CompanyGUID equals companyParameter.CompanyGUID
						join invoiceRevenueType in db.Set<InvoiceRevenueType>()
						on companyParameter.CreditReqInvRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID into ljCompanyParameterInvoiceRevenueType
						from invoiceRevenueType in ljCompanyParameterInvoiceRevenueType.DefaultIfEmpty()
						where creditAppRequestTable.CreditAppRequestTableGUID == creditAppRequestTableGUID
						select new ServiceFeeConditionTrans
						{
							ServiceFeeConditionTransGUID = Guid.NewGuid(),
							Ordering = queryOfServiceFeeConditionTrans.Any() ? queryOfServiceFeeConditionTrans.Max(m => m.Ordering) + 1 : 1,
							InvoiceRevenueTypeGUID = companyParameter.CreditReqInvRevenueTypeGUID.Value,
							Description = (invoiceRevenueType != null) ? invoiceRevenueType.Description: "",
							AmountBeforeTax = creditAppRequestTable.CreditRequestFeeAmount,
							RefType = (int)RefType.CreditAppRequestTable,
							Inactive = false,
							RefServiceFeeConditionTransGUID = null,
							CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
							RefGUID = creditAppRequestTableGUID,
							CompanyGUID = creditAppRequestTable.CompanyGUID,
							Owner = owner,
							OwnerBusinessUnitGUID = ownerBusinessUnitGUID
						}).FirstOrDefault();
					}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #endregion

		#region  BuyerCreditApp
		 public IEnumerable<QServiceFeeConditionLine_BuyerCreditApplication> GetServiceFeeConditionLineByBuyerCreditApplication(Guid creditAppRequestLineGUID)
        {
            try
            {
                var result = (from serviceFeeConditionTrans in Entity
                              join invoiceRevenueType in db.Set<InvoiceRevenueType>()
                              on serviceFeeConditionTrans.InvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID
                              orderby serviceFeeConditionTrans.Ordering ascending
                              where serviceFeeConditionTrans.RefGUID == creditAppRequestLineGUID
                                && serviceFeeConditionTrans.Inactive == false
                                && serviceFeeConditionTrans.RefType == (int)RefType.CreditAppRequestLine
                              select new QServiceFeeConditionLine_BuyerCreditApplication
							  {
                                  CreditAppRequestLineGUID = creditAppRequestLineGUID,
                                  ServicefeeType = invoiceRevenueType.Description,
                                  AmountBeforeTax = serviceFeeConditionTrans.AmountBeforeTax,
                                  Description = serviceFeeConditionTrans.Description,
								  Ordering = serviceFeeConditionTrans.Ordering
							  }).AsNoTracking().Take(10).ToList();

				result = result.Select((o, i) =>
				{
					o.RowNumber = i;
					return o;
				}).ToList();

				return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public decimal GetSumServiceFeeConditionByCreditAppRequestLine(Guid creditAppRequestLineGUID)
        {
            try
            {
				var item = Entity.Where(w => w.RefGUID == creditAppRequestLineGUID
										&& w.Inactive == false
										&& w.RefType == (int)RefType.CreditAppRequestLine)
								.Select(s => s.AmountBeforeTax).Sum();

                return item;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

		#endregion
    }
}

