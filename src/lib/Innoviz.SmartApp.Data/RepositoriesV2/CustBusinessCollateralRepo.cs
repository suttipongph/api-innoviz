using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICustBusinessCollateralRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CustBusinessCollateralItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<CustBusinessCollateralListView> GetListvw(SearchParameter search);
		CustBusinessCollateralItemView GetByIdvw(Guid id);
		CustBusinessCollateral Find(params object[] keyValues);
		CustBusinessCollateral GetCustBusinessCollateralByIdNoTracking(Guid guid);
		CustBusinessCollateral CreateCustBusinessCollateral(CustBusinessCollateral custBusinessCollateral);
		void CreateCustBusinessCollateralVoid(CustBusinessCollateral custBusinessCollateral);
		CustBusinessCollateral UpdateCustBusinessCollateral(CustBusinessCollateral dbCustBusinessCollateral, CustBusinessCollateral inputCustBusinessCollateral, List<string> skipUpdateFields = null);
		void UpdateCustBusinessCollateralVoid(CustBusinessCollateral dbCustBusinessCollateral, CustBusinessCollateral inputCustBusinessCollateral, List<string> skipUpdateFields = null);
		void Remove(CustBusinessCollateral item);
		CustBusinessCollateral GetCustBusinessCollateralByIdNoTrackingByAccessLevel(Guid guid);
		IEnumerable<CustBusinessCollateral> GetListByBusinessCollateralAgmLines(IEnumerable<Guid> businessCollateralAgmLineGuids);
		void ValidateAdd(CustBusinessCollateral item);
		void ValidateAdd(IEnumerable<CustBusinessCollateral> items);
	}
	public class CustBusinessCollateralRepo : CompanyBaseRepository<CustBusinessCollateral>, ICustBusinessCollateralRepo
	{
		public CustBusinessCollateralRepo(SmartAppDbContext context) : base(context) { }
		public CustBusinessCollateralRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CustBusinessCollateral GetCustBusinessCollateralByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CustBusinessCollateralGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CustBusinessCollateralItemViewMap> GetDropDownQuery()
		{
			return (from custBusinessCollateral in Entity
					select new CustBusinessCollateralItemViewMap
					{
						CompanyGUID = custBusinessCollateral.CompanyGUID,
						Owner = custBusinessCollateral.Owner,
						OwnerBusinessUnitGUID = custBusinessCollateral.OwnerBusinessUnitGUID,
						CustBusinessCollateralGUID = custBusinessCollateral.CustBusinessCollateralGUID,
						CustBusinessCollateralId = custBusinessCollateral.CustBusinessCollateralId,
						Description = custBusinessCollateral.Description,
						CustomerTableGUID = custBusinessCollateral.CustomerTableGUID,
						Cancelled = custBusinessCollateral.Cancelled,
						BusinessCollateralTypeGUID = custBusinessCollateral.BusinessCollateralTypeGUID,
						BusinessCollateralSubTypeGUID = custBusinessCollateral.BusinessCollateralSubTypeGUID,
						BuyerTableGUID = custBusinessCollateral.BuyerTableGUID,
						BuyerName = custBusinessCollateral.BuyerName,
						BankGroupGUID = custBusinessCollateral.BankGroupGUID,
						BankTypeGUID = custBusinessCollateral.BankTypeGUID,
						AccountNumber = custBusinessCollateral.AccountNumber,
						RefAgreementId = custBusinessCollateral.RefAgreementId,
						RefAgreementDate = custBusinessCollateral.RefAgreementDate,
						PreferentialCreditorNumber = custBusinessCollateral.PreferentialCreditorNumber,
						Lessee = custBusinessCollateral.Lessee,
						Lessor = custBusinessCollateral.Lessor,
						RegistrationPlateNumber = custBusinessCollateral.RegistrationPlateNumber,
						MachineNumber = custBusinessCollateral.MachineNumber,
						MachineRegisteredStatus = custBusinessCollateral.MachineRegisteredStatus,
						ChassisNumber = custBusinessCollateral.ChassisNumber,
						RegisteredPlace = custBusinessCollateral.RegisteredPlace,
						GuaranteeAmount = custBusinessCollateral.GuaranteeAmount,
                        BusinessCollateralValue = custBusinessCollateral.BusinessCollateralValue,
                        Quantity = custBusinessCollateral.Quantity,
						Unit = custBusinessCollateral.Unit,
						ProjectName = custBusinessCollateral.ProjectName,
						TitleDeedNumber = custBusinessCollateral.TitleDeedNumber,
						TitleDeedSubDistrict = custBusinessCollateral.TitleDeedSubDistrict,
						TitleDeedDistrict = custBusinessCollateral.TitleDeedDistrict,
						TitleDeedProvince = custBusinessCollateral.TitleDeedProvince,
						DBDRegistrationId = custBusinessCollateral.DBDRegistrationId,
						DBDRegistrationDate = custBusinessCollateral.DBDRegistrationDate,
						DBDRegistrationAmount = custBusinessCollateral.DBDRegistrationAmount,
						DBDRegistrationDescription = custBusinessCollateral.DBDRegistrationDescription,
						CapitalValuation = custBusinessCollateral.CapitalValuation,
						ValuationCommittee = custBusinessCollateral.ValuationCommittee,
						DateOfValuation = custBusinessCollateral.DateOfValuation,
						Ownership = custBusinessCollateral.Ownership,
						BuyerTaxIdentificationId = custBusinessCollateral.BuyerTaxIdentificationId
					});
		}
		public IEnumerable<SelectItem<CustBusinessCollateralItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CustBusinessCollateral>(search, SysParm.CompanyGUID);
				var custBusinessCollateral = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CustBusinessCollateralItemViewMap, CustBusinessCollateralItemView>().ToDropDownItem(search.ExcludeRowData);


				return custBusinessCollateral;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CustBusinessCollateralListViewMap> GetListQuery()
		{
			return (from custBusinessCollateral in Entity
					join creditAppRequestTable in db.Set<CreditAppRequestTable>() on custBusinessCollateral.CreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID
					join businessCollateralSubType in db.Set<BusinessCollateralSubType>() on custBusinessCollateral.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID
					join businessCollateralType in db.Set<BusinessCollateralType>() on custBusinessCollateral.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID
					select new CustBusinessCollateralListViewMap
					{
						CompanyGUID = custBusinessCollateral.CompanyGUID,
						Owner = custBusinessCollateral.Owner,
						OwnerBusinessUnitGUID = custBusinessCollateral.OwnerBusinessUnitGUID,
						CustBusinessCollateralGUID = custBusinessCollateral.CustBusinessCollateralGUID,
						CustBusinessCollateralId = custBusinessCollateral.CustBusinessCollateralId,
						Description = custBusinessCollateral.Description,
						DBDRegistrationDate = custBusinessCollateral.DBDRegistrationDate,
						BusinessCollateralTypeGUID = custBusinessCollateral.BusinessCollateralTypeGUID,
						BusinessCollateralSubTypeGUID = custBusinessCollateral.BusinessCollateralSubTypeGUID,
						BuyerName = custBusinessCollateral.BuyerName,
						Cancelled = custBusinessCollateral.Cancelled,
						BusinessCollateralValue = custBusinessCollateral.BusinessCollateralValue,
						CreditAppRequestTableGUID = custBusinessCollateral.CreditAppRequestTableGUID,
						CustomerTableGUID = custBusinessCollateral.CustomerTableGUID,
						CreditAppRequestTable_Values = SmartAppUtil.GetDropDownLabel(creditAppRequestTable.CreditAppRequestId, creditAppRequestTable.Description),
						BusinessCollateralSubType_Values = SmartAppUtil.GetDropDownLabel(businessCollateralSubType.BusinessCollateralSubTypeId, businessCollateralSubType.Description),
						BusinessCollateralType_Values = SmartAppUtil.GetDropDownLabel(businessCollateralType.BusinessCollateralTypeId, businessCollateralType.Description),
						CreditAppRequestTable_CreditAppRequestId = creditAppRequestTable.CreditAppRequestId,
						BusinessCollateralType_BusinessCollateralTypeId = businessCollateralType.BusinessCollateralTypeId,
						BusinessCollateralSubType_BusinessCollateralSubTypeId = businessCollateralSubType.BusinessCollateralSubTypeId,
					});
		}
		public SearchResult<CustBusinessCollateralListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CustBusinessCollateralListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CustBusinessCollateral>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CustBusinessCollateralListViewMap, CustBusinessCollateralListView>();
				result = list.SetSearchResult<CustBusinessCollateralListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CustBusinessCollateralItemViewMap> GetItemQuery()
		{
			return (from custBusinessCollateral in Entity
					join company in db.Set<Company>()
					on custBusinessCollateral.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on custBusinessCollateral.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCustBusinessCollateralOwnerBU
                    from ownerBU in ljCustBusinessCollateralOwnerBU.DefaultIfEmpty()
                    join customerTable in db.Set<CustomerTable>() on custBusinessCollateral.CustomerTableGUID equals customerTable.CustomerTableGUID
                    join creditAppRequestTable in db.Set<CreditAppRequestTable>() on custBusinessCollateral.CreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID
                    join businessCollateralSubType in db.Set<BusinessCollateralSubType>() on custBusinessCollateral.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID
					join businessCollateralType in db.Set<BusinessCollateralType>() on custBusinessCollateral.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID
					join bankGroup in db.Set<BankGroup>() on custBusinessCollateral.BankGroupGUID equals bankGroup.BankGroupGUID into ljCustBusinessCollateralBankGroup
                    from bankGroup in ljCustBusinessCollateralBankGroup.DefaultIfEmpty()
                    join bankType in db.Set<BankType>() on custBusinessCollateral.BankTypeGUID equals bankType.BankTypeGUID into ljCustBusinessCollateralBankType
                    from bankType in ljCustBusinessCollateralBankType.DefaultIfEmpty()
                    join businessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>() on custBusinessCollateral.BusinessCollateralAgmLineGUID equals businessCollateralAgmLine.BusinessCollateralAgmLineGUID
					join businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>() on businessCollateralAgmLine.BusinessCollateralAgmTableGUID equals businessCollateralAgmTable.BusinessCollateralAgmTableGUID
					join buyerTable in db.Set<BuyerTable>() on custBusinessCollateral.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljCustBusinessCollateralBuyerTable
					from buyerTable in ljCustBusinessCollateralBuyerTable.DefaultIfEmpty()
					select new CustBusinessCollateralItemViewMap
					{
						CompanyGUID = custBusinessCollateral.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = custBusinessCollateral.Owner,
						OwnerBusinessUnitGUID = custBusinessCollateral.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = custBusinessCollateral.CreatedBy,
						CreatedDateTime = custBusinessCollateral.CreatedDateTime,
						ModifiedBy = custBusinessCollateral.ModifiedBy,
						ModifiedDateTime = custBusinessCollateral.ModifiedDateTime,
						CustBusinessCollateralGUID = custBusinessCollateral.CustBusinessCollateralGUID,
						AccountNumber = custBusinessCollateral.AccountNumber,
						BankGroupGUID = custBusinessCollateral.BankGroupGUID,
						BankTypeGUID = custBusinessCollateral.BankTypeGUID,
						BusinessCollateralAgmLineGUID = custBusinessCollateral.BusinessCollateralAgmLineGUID,
						BusinessCollateralStatusGUID = custBusinessCollateral.BusinessCollateralStatusGUID,
						BusinessCollateralSubTypeGUID = custBusinessCollateral.BusinessCollateralSubTypeGUID,
						BusinessCollateralTypeGUID = custBusinessCollateral.BusinessCollateralTypeGUID,
						BusinessCollateralValue = custBusinessCollateral.BusinessCollateralValue,
						BuyerName = custBusinessCollateral.BuyerName,
						BuyerTableGUID = custBusinessCollateral.BuyerTableGUID,
						BuyerTaxIdentificationId = custBusinessCollateral.BuyerTaxIdentificationId,
						Cancelled = custBusinessCollateral.Cancelled,
						CapitalValuation = custBusinessCollateral.CapitalValuation,
						ChassisNumber = custBusinessCollateral.ChassisNumber,
						CreditAppRequestTableGUID = custBusinessCollateral.CreditAppRequestTableGUID,
						CustBusinessCollateralId = custBusinessCollateral.CustBusinessCollateralId,
						CustomerTableGUID = custBusinessCollateral.CustomerTableGUID,
						DateOfValuation = custBusinessCollateral.DateOfValuation,
						DBDRegistrationAmount = custBusinessCollateral.DBDRegistrationAmount,
						DBDRegistrationDate = custBusinessCollateral.DBDRegistrationDate,
						DBDRegistrationDescription = custBusinessCollateral.DBDRegistrationDescription,
						DBDRegistrationId = custBusinessCollateral.DBDRegistrationId,
						Description = custBusinessCollateral.Description,
						GuaranteeAmount = custBusinessCollateral.GuaranteeAmount,
						Lessee = custBusinessCollateral.Lessee,
						Lessor = custBusinessCollateral.Lessor,
						MachineNumber = custBusinessCollateral.MachineNumber,
						MachineRegisteredStatus = custBusinessCollateral.MachineRegisteredStatus,
						Ownership = custBusinessCollateral.Ownership,
						PreferentialCreditorNumber = custBusinessCollateral.PreferentialCreditorNumber,
						ProjectName = custBusinessCollateral.ProjectName,
						Quantity = custBusinessCollateral.Quantity,
						RefAgreementDate = custBusinessCollateral.RefAgreementDate,
						RefAgreementId = custBusinessCollateral.RefAgreementId,
						RegisteredPlace = custBusinessCollateral.RegisteredPlace,
						RegistrationPlateNumber = custBusinessCollateral.RegistrationPlateNumber,
						Remark = custBusinessCollateral.Remark,
						TitleDeedDistrict = custBusinessCollateral.TitleDeedDistrict,
						TitleDeedNumber = custBusinessCollateral.TitleDeedNumber,
						TitleDeedProvince = custBusinessCollateral.TitleDeedProvince,
						TitleDeedSubDistrict = custBusinessCollateral.TitleDeedSubDistrict,
						Unit = custBusinessCollateral.Unit,
						ValuationCommittee = custBusinessCollateral.ValuationCommittee,
                        CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                        CreditAppRequestTable_Values = SmartAppUtil.GetDropDownLabel(creditAppRequestTable.CreditAppRequestId, creditAppRequestTable.Description),
                        BusinessCollateralAgmLine_Values = SmartAppUtil.GetDropDownLabel(businessCollateralAgmLine.LineNum.ToString()),
                        BusinessCollateralSubType_Values = SmartAppUtil.GetDropDownLabel(businessCollateralSubType.BusinessCollateralSubTypeId, businessCollateralSubType.Description),
						BusinessCollateralType_Values = SmartAppUtil.GetDropDownLabel(businessCollateralType.BusinessCollateralTypeId, businessCollateralType.Description),
						BankGroup_Values = (bankGroup != null) ? SmartAppUtil.GetDropDownLabel(bankGroup.BankGroupId, bankGroup.Description): null,
                        BankType_Values = (bankType != null) ? SmartAppUtil.GetDropDownLabel(bankType.BankTypeId, bankType.Description): null,
                        BusinessCollateralAgmId = businessCollateralAgmTable.BusinessCollateralAgmId,
						BuyerId = (buyerTable != null) ? buyerTable.BuyerId: null,
					
						RowVersion = custBusinessCollateral.RowVersion,
					});
		}
		public CustBusinessCollateralItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<CustBusinessCollateralItemViewMap, CustBusinessCollateralItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public CustBusinessCollateral CreateCustBusinessCollateral(CustBusinessCollateral custBusinessCollateral)
		{
			try
			{
				custBusinessCollateral.CustBusinessCollateralGUID = Guid.NewGuid();
				base.Add(custBusinessCollateral);
				return custBusinessCollateral;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCustBusinessCollateralVoid(CustBusinessCollateral custBusinessCollateral)
		{
			try
			{
				CreateCustBusinessCollateral(custBusinessCollateral);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CustBusinessCollateral UpdateCustBusinessCollateral(CustBusinessCollateral dbCustBusinessCollateral, CustBusinessCollateral inputCustBusinessCollateral, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCustBusinessCollateral = dbCustBusinessCollateral.MapUpdateValues<CustBusinessCollateral>(inputCustBusinessCollateral);
				base.Update(dbCustBusinessCollateral);
				return dbCustBusinessCollateral;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCustBusinessCollateralVoid(CustBusinessCollateral dbCustBusinessCollateral, CustBusinessCollateral inputCustBusinessCollateral, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCustBusinessCollateral = dbCustBusinessCollateral.MapUpdateValues<CustBusinessCollateral>(inputCustBusinessCollateral, skipUpdateFields);
				base.Update(dbCustBusinessCollateral);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public CustBusinessCollateral GetCustBusinessCollateralByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CustBusinessCollateralGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<CustBusinessCollateral> GetListByBusinessCollateralAgmLines(IEnumerable<Guid> businessCollateralAgmLineGuids)
		{
			try
			{
				var result = Entity.Where(item => businessCollateralAgmLineGuids.Contains(item.BusinessCollateralAgmLineGUID)).AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public override void ValidateAdd(IEnumerable<CustBusinessCollateral> items)
		{
			base.ValidateAdd(items);
		}
		public override void ValidateAdd(CustBusinessCollateral item)
		{
			base.ValidateAdd(item);
		}
	}
}

