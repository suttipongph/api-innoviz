using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IDocumentReasonRepo
	{
		#region DropDown
		IEnumerable<SelectItem<DocumentReasonItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<DocumentReasonListView> GetListvw(SearchParameter search);
		DocumentReasonItemView GetByIdvw(Guid id);
		DocumentReason Find(params object[] keyValues);
		DocumentReason GetDocumentReasonByIdNoTracking(Guid guid);
		DocumentReason CreateDocumentReason(DocumentReason documentReason);
		void CreateDocumentReasonVoid(DocumentReason documentReason);
		DocumentReason UpdateDocumentReason(DocumentReason dbDocumentReason, DocumentReason inputDocumentReason, List<string> skipUpdateFields = null);
		void UpdateDocumentReasonVoid(DocumentReason dbDocumentReason, DocumentReason inputDocumentReason, List<string> skipUpdateFields = null);
		void Remove(DocumentReason item);
	}
	public class DocumentReasonRepo : CompanyBaseRepository<DocumentReason>, IDocumentReasonRepo
	{
		public DocumentReasonRepo(SmartAppDbContext context) : base(context) { }
		public DocumentReasonRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public DocumentReason GetDocumentReasonByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.DocumentReasonGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<DocumentReasonItemViewMap> GetDropDownQuery()
		{
			return (from documentReason in Entity
					select new DocumentReasonItemViewMap
					{
						CompanyGUID = documentReason.CompanyGUID,
						Owner = documentReason.Owner,
						OwnerBusinessUnitGUID = documentReason.OwnerBusinessUnitGUID,
						DocumentReasonGUID = documentReason.DocumentReasonGUID,
						ReasonId = documentReason.ReasonId,
						Description = documentReason.Description,
						RefType = documentReason.RefType
					});
		}
		public IEnumerable<SelectItem<DocumentReasonItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				search = search.GetParentCondition(CreditAppRequestTableCondition.RefType);
				var predicate = base.GetFilterLevelPredicate<DocumentReason>(search, SysParm.CompanyGUID);
				var documentReason = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<DocumentReasonItemViewMap, DocumentReasonItemView>().ToDropDownItem(search.ExcludeRowData);


				return documentReason;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<DocumentReasonListViewMap> GetListQuery()
		{
			return (from documentReason in Entity
				select new DocumentReasonListViewMap
				{
						CompanyGUID = documentReason.CompanyGUID,
						Owner = documentReason.Owner,
						OwnerBusinessUnitGUID = documentReason.OwnerBusinessUnitGUID,
						DocumentReasonGUID = documentReason.DocumentReasonGUID,
						Description = documentReason.Description,
						RefType = documentReason.RefType,
						ReasonId = documentReason.ReasonId
				});
		}
		public SearchResult<DocumentReasonListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<DocumentReasonListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<DocumentReason>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<DocumentReasonListViewMap, DocumentReasonListView>();
				result = list.SetSearchResult<DocumentReasonListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<DocumentReasonItemViewMap> GetItemQuery()
		{
			return (from documentReason in Entity
					join company in db.Set<Company>()
					on documentReason.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on documentReason.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljDocumentReasonOwnerBU
					from ownerBU in ljDocumentReasonOwnerBU.DefaultIfEmpty()
					select new DocumentReasonItemViewMap
					{
						CompanyGUID = documentReason.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = documentReason.Owner,
						OwnerBusinessUnitGUID = documentReason.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = documentReason.CreatedBy,
						CreatedDateTime = documentReason.CreatedDateTime,
						ModifiedBy = documentReason.ModifiedBy,
						ModifiedDateTime = documentReason.ModifiedDateTime,
						DocumentReasonGUID = documentReason.DocumentReasonGUID,
						Description = documentReason.Description,
						ReasonId = documentReason.ReasonId,
						RefType = documentReason.RefType,
					
						RowVersion = documentReason.RowVersion,
					});
		}
		public DocumentReasonItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<DocumentReasonItemViewMap, DocumentReasonItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public DocumentReason CreateDocumentReason(DocumentReason documentReason)
		{
			try
			{
				documentReason.DocumentReasonGUID = Guid.NewGuid();
				base.Add(documentReason);
				return documentReason;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateDocumentReasonVoid(DocumentReason documentReason)
		{
			try
			{
				CreateDocumentReason(documentReason);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentReason UpdateDocumentReason(DocumentReason dbDocumentReason, DocumentReason inputDocumentReason, List<string> skipUpdateFields = null)
		{
			try
			{
				dbDocumentReason = dbDocumentReason.MapUpdateValues<DocumentReason>(inputDocumentReason);
				base.Update(dbDocumentReason);
				return dbDocumentReason;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateDocumentReasonVoid(DocumentReason dbDocumentReason, DocumentReason inputDocumentReason, List<string> skipUpdateFields = null)
		{
			try
			{
				dbDocumentReason = dbDocumentReason.MapUpdateValues<DocumentReason>(inputDocumentReason, skipUpdateFields);
				base.Update(dbDocumentReason);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

