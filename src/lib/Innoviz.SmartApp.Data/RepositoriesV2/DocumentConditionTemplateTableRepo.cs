using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IDocumentConditionTemplateTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<DocumentConditionTemplateTableItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<DocumentConditionTemplateTableListView> GetListvw(SearchParameter search);
		DocumentConditionTemplateTableItemView GetByIdvw(Guid id);
		DocumentConditionTemplateTable Find(params object[] keyValues);
		DocumentConditionTemplateTable GetDocumentConditionTemplateTableByIdNoTracking(Guid guid);
		DocumentConditionTemplateTable CreateDocumentConditionTemplateTable(DocumentConditionTemplateTable documentConditionTemplateTable);
		void CreateDocumentConditionTemplateTableVoid(DocumentConditionTemplateTable documentConditionTemplateTable);
		DocumentConditionTemplateTable UpdateDocumentConditionTemplateTable(DocumentConditionTemplateTable dbDocumentConditionTemplateTable, DocumentConditionTemplateTable inputDocumentConditionTemplateTable, List<string> skipUpdateFields = null);
		void UpdateDocumentConditionTemplateTableVoid(DocumentConditionTemplateTable dbDocumentConditionTemplateTable, DocumentConditionTemplateTable inputDocumentConditionTemplateTable, List<string> skipUpdateFields = null);
		void Remove(DocumentConditionTemplateTable item);
		public DocumentConditionTemplateTable GetDocumentConditionTemplateTableNoTrackingByAccessLevel(Guid guid);
	}
	public class DocumentConditionTemplateTableRepo : CompanyBaseRepository<DocumentConditionTemplateTable>, IDocumentConditionTemplateTableRepo
	{
		public DocumentConditionTemplateTableRepo(SmartAppDbContext context) : base(context) { }
		public DocumentConditionTemplateTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public DocumentConditionTemplateTable GetDocumentConditionTemplateTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.DocumentConditionTemplateTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<DocumentConditionTemplateTableItemViewMap> GetDropDownQuery()
		{
			return (from documentConditionTemplateTable in Entity
					select new DocumentConditionTemplateTableItemViewMap
					{
						CompanyGUID = documentConditionTemplateTable.CompanyGUID,
						Owner = documentConditionTemplateTable.Owner,
						OwnerBusinessUnitGUID = documentConditionTemplateTable.OwnerBusinessUnitGUID,
						DocumentConditionTemplateTableGUID = documentConditionTemplateTable.DocumentConditionTemplateTableGUID,
						DocumentConditionTemplateTableId = documentConditionTemplateTable.DocumentConditionTemplateTableId,
						Description = documentConditionTemplateTable.Description
					});
		}
		public IEnumerable<SelectItem<DocumentConditionTemplateTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<DocumentConditionTemplateTable>(search, SysParm.CompanyGUID);
				var documentConditionTemplateTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<DocumentConditionTemplateTableItemViewMap, DocumentConditionTemplateTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return documentConditionTemplateTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<DocumentConditionTemplateTableListViewMap> GetListQuery()
		{
			return (from documentConditionTemplateTable in Entity
				select new DocumentConditionTemplateTableListViewMap
				{
						CompanyGUID = documentConditionTemplateTable.CompanyGUID,
						Owner = documentConditionTemplateTable.Owner,
						OwnerBusinessUnitGUID = documentConditionTemplateTable.OwnerBusinessUnitGUID,
						DocumentConditionTemplateTableGUID = documentConditionTemplateTable.DocumentConditionTemplateTableGUID,
						DocumentConditionTemplateTableId = documentConditionTemplateTable.DocumentConditionTemplateTableId,
						Description = documentConditionTemplateTable.Description,
				});
		}
		public SearchResult<DocumentConditionTemplateTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<DocumentConditionTemplateTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<DocumentConditionTemplateTable>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<DocumentConditionTemplateTableListViewMap, DocumentConditionTemplateTableListView>();
				result = list.SetSearchResult<DocumentConditionTemplateTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<DocumentConditionTemplateTableItemViewMap> GetItemQuery()
		{
			return (from documentConditionTemplateTable in Entity
					join company in db.Set<Company>()
					on documentConditionTemplateTable.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on documentConditionTemplateTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljDocumentConditionTemplateTableOwnerBU
					from ownerBU in ljDocumentConditionTemplateTableOwnerBU.DefaultIfEmpty()
					select new DocumentConditionTemplateTableItemViewMap
					{
						CompanyGUID = documentConditionTemplateTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = documentConditionTemplateTable.Owner,
						OwnerBusinessUnitGUID = documentConditionTemplateTable.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = documentConditionTemplateTable.CreatedBy,
						CreatedDateTime = documentConditionTemplateTable.CreatedDateTime,
						ModifiedBy = documentConditionTemplateTable.ModifiedBy,
						ModifiedDateTime = documentConditionTemplateTable.ModifiedDateTime,
						DocumentConditionTemplateTableGUID = documentConditionTemplateTable.DocumentConditionTemplateTableGUID,
						Description = documentConditionTemplateTable.Description,
						DocumentConditionTemplateTableId = documentConditionTemplateTable.DocumentConditionTemplateTableId,
					
						RowVersion = documentConditionTemplateTable.RowVersion,
					});
		}
		public DocumentConditionTemplateTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.ToMap<DocumentConditionTemplateTableItemViewMap, DocumentConditionTemplateTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public DocumentConditionTemplateTable CreateDocumentConditionTemplateTable(DocumentConditionTemplateTable documentConditionTemplateTable)
		{
			try
			{
				
				documentConditionTemplateTable.DocumentConditionTemplateTableGUID = Guid.NewGuid();
				base.Add(documentConditionTemplateTable);
				return documentConditionTemplateTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateDocumentConditionTemplateTableVoid(DocumentConditionTemplateTable documentConditionTemplateTable)
		{
			try
			{
				CreateDocumentConditionTemplateTable(documentConditionTemplateTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentConditionTemplateTable UpdateDocumentConditionTemplateTable(DocumentConditionTemplateTable dbDocumentConditionTemplateTable, DocumentConditionTemplateTable inputDocumentConditionTemplateTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbDocumentConditionTemplateTable = dbDocumentConditionTemplateTable.MapUpdateValues<DocumentConditionTemplateTable>(inputDocumentConditionTemplateTable);
				base.Update(dbDocumentConditionTemplateTable);
				return dbDocumentConditionTemplateTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateDocumentConditionTemplateTableVoid(DocumentConditionTemplateTable dbDocumentConditionTemplateTable, DocumentConditionTemplateTable inputDocumentConditionTemplateTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbDocumentConditionTemplateTable = dbDocumentConditionTemplateTable.MapUpdateValues<DocumentConditionTemplateTable>(inputDocumentConditionTemplateTable, skipUpdateFields);
				base.Update(dbDocumentConditionTemplateTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public DocumentConditionTemplateTable GetDocumentConditionTemplateTableNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.DocumentConditionTemplateTableGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

