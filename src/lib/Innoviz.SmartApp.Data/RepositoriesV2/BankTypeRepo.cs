using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IBankTypeRepo
	{
		#region DropDown
		IEnumerable<SelectItem<BankTypeItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<BankTypeListView> GetListvw(SearchParameter search);
		BankTypeItemView GetByIdvw(Guid id);
		BankType Find(params object[] keyValues);
		BankType GetBankTypeByIdNoTracking(Guid guid);
		BankType CreateBankType(BankType bankType);
		void CreateBankTypeVoid(BankType bankType);
		BankType UpdateBankType(BankType dbBankType, BankType inputBankType, List<string> skipUpdateFields = null);
		void UpdateBankTypeVoid(BankType dbBankType, BankType inputBankType, List<string> skipUpdateFields = null);
		void Remove(BankType item);
	}
	public class BankTypeRepo : CompanyBaseRepository<BankType>, IBankTypeRepo
	{
		public BankTypeRepo(SmartAppDbContext context) : base(context) { }
		public BankTypeRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BankType GetBankTypeByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BankTypeGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<BankTypeItemViewMap> GetDropDownQuery()
		{
			return (from bankType in Entity
					select new BankTypeItemViewMap
					{
						CompanyGUID = bankType.CompanyGUID,
						Owner = bankType.Owner,
						OwnerBusinessUnitGUID = bankType.OwnerBusinessUnitGUID,
						BankTypeGUID = bankType.BankTypeGUID,
						BankTypeId = bankType.BankTypeId,
						Description = bankType.Description
					});
		}
		public IEnumerable<SelectItem<BankTypeItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<BankType>(search, SysParm.CompanyGUID);
				var bankType = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<BankTypeItemViewMap, BankTypeItemView>().ToDropDownItem(search.ExcludeRowData);


				return bankType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<BankTypeListViewMap> GetListQuery()
		{
			return (from bankType in Entity
				select new BankTypeListViewMap
				{
						CompanyGUID = bankType.CompanyGUID,
						Owner = bankType.Owner,
						OwnerBusinessUnitGUID = bankType.OwnerBusinessUnitGUID,
						BankTypeGUID = bankType.BankTypeGUID,
						BankTypeId = bankType.BankTypeId,
						Description = bankType.Description,
				});
		}
		public SearchResult<BankTypeListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<BankTypeListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<BankType>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<BankTypeListViewMap, BankTypeListView>();
				result = list.SetSearchResult<BankTypeListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<BankTypeItemViewMap> GetItemQuery()
		{
			return (from bankType in Entity
					join company in db.Set<Company>()
					on bankType.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on bankType.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljBankTypeOwnerBU
					from ownerBU in ljBankTypeOwnerBU.DefaultIfEmpty()
					select new BankTypeItemViewMap
					{
						CompanyGUID = bankType.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = bankType.Owner,
						OwnerBusinessUnitGUID = bankType.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = bankType.CreatedBy,
						CreatedDateTime = bankType.CreatedDateTime,
						ModifiedBy = bankType.ModifiedBy,
						ModifiedDateTime = bankType.ModifiedDateTime,
						BankTypeGUID = bankType.BankTypeGUID,
						BankTypeId = bankType.BankTypeId,
						Description = bankType.Description,
					
						RowVersion = bankType.RowVersion,
					});
		}
		public BankTypeItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<BankTypeItemViewMap, BankTypeItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public BankType CreateBankType(BankType bankType)
		{
			try
			{
				bankType.BankTypeGUID = Guid.NewGuid();
				base.Add(bankType);
				return bankType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateBankTypeVoid(BankType bankType)
		{
			try
			{
				CreateBankType(bankType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BankType UpdateBankType(BankType dbBankType, BankType inputBankType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBankType = dbBankType.MapUpdateValues<BankType>(inputBankType);
				base.Update(dbBankType);
				return dbBankType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateBankTypeVoid(BankType dbBankType, BankType inputBankType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBankType = dbBankType.MapUpdateValues<BankType>(inputBankType, skipUpdateFields);
				base.Update(dbBankType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

