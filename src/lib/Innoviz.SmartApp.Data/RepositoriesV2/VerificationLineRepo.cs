using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface IVerificationLineRepo
    {
        #region DropDown
        IEnumerable<SelectItem<VerificationLineItemView>> GetDropDownItem(SearchParameter search);
        #endregion DropDown
        SearchResult<VerificationLineListView> GetListvw(SearchParameter search);
        VerificationLineItemView GetByIdvw(Guid id);
        VerificationLine Find(params object[] keyValues);
        VerificationLine GetVerificationLineByIdNoTracking(Guid guid);
        VerificationLine CreateVerificationLine(VerificationLine verificationLine);
        void CreateVerificationLineVoid(VerificationLine verificationLine);
        VerificationLine UpdateVerificationLine(VerificationLine dbVerificationLine, VerificationLine inputVerificationLine, List<string> skipUpdateFields = null);
        void UpdateVerificationLineVoid(VerificationLine dbVerificationLine, VerificationLine inputVerificationLine, List<string> skipUpdateFields = null);
        void Remove(VerificationLine item);
        VerificationLine GetVerificationLineByIdNoTrackingByAccessLevel(Guid guid);

        List<VerificationLine> GetVerificationLineByVerificationTableNoTracking(Guid guid);
        bool GetVerificationLineCount(Guid guid);
        List<VerificationLineItemView> GetVwByVerificationTable(Guid guid);
        void ValidateAdd(VerificationLine item);
        void ValidateAdd(IEnumerable<VerificationLine> items);
        List<VerificationLine> GetVerificationLineByVerificationTable(Guid guid);
    }
    public class VerificationLineRepo : CompanyBaseRepository<VerificationLine>, IVerificationLineRepo
    {
        public VerificationLineRepo(SmartAppDbContext context) : base(context) { }
        public VerificationLineRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public VerificationLine GetVerificationLineByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.VerificationLineGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        private IQueryable<VerificationLineItemViewMap> GetDropDownQuery()
        {
            return (from verificationLine in Entity
                    select new VerificationLineItemViewMap
                    {
                        CompanyGUID = verificationLine.CompanyGUID,
                        Owner = verificationLine.Owner,
                        OwnerBusinessUnitGUID = verificationLine.OwnerBusinessUnitGUID,
                        VerificationLineGUID = verificationLine.VerificationLineGUID,
                        VerificationTableGUID = verificationLine.VerificationTableGUID,
                        VerificationTypeGUID = verificationLine.VerificationTypeGUID
                    });
        }
        public IEnumerable<SelectItem<VerificationLineItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<VerificationLine>(search, SysParm.CompanyGUID);
                var verificationLine = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<VerificationLineItemViewMap, VerificationLineItemView>().ToDropDownItem(search.ExcludeRowData);


                return verificationLine;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #region GetListvw
        public List<VerificationLineItemView> GetVwByVerificationTable(Guid guid)
        {
            var result = (from verificationLine in Entity
                          where verificationLine.VerificationTableGUID == guid
                          select new VerificationLineItemView
                          {
                              Owner = verificationLine.Owner,
                              VerificationTypeGUID = verificationLine.VerificationTypeGUID.ToString(),
                              VerificationTableGUID = verificationLine.VerificationTableGUID.ToString(),
                              Pass = false,
                              Remark = null
                          }).ToList();
            return result;
        }
        public bool GetVerificationLineCount(Guid guid)
        {
            var result = (from verificationLine in Entity
                        where verificationLine.VerificationTableGUID == guid
                    select new VerificationLineListViewMap
                    {
                        CompanyGUID = verificationLine.CompanyGUID,
                        Owner = verificationLine.Owner,
                        OwnerBusinessUnitGUID = verificationLine.OwnerBusinessUnitGUID,
                        VerificationLineGUID = verificationLine.VerificationLineGUID,
                        VerificationTypeGUID = verificationLine.VerificationTypeGUID,
                        Pass = verificationLine.Pass,
                        VerificationTableGUID = verificationLine.VerificationTableGUID,
                    });
            return result.Count() > 0;
        }

        
        private IQueryable<VerificationLineListViewMap> GetListQuery()
        {
            return (from verificationLine in Entity
                    join verificationType in db.Set<VerificationType>() on verificationLine.VerificationTypeGUID equals verificationType.VerificationTypeGUID
                    select new VerificationLineListViewMap
                    {
                        CompanyGUID = verificationLine.CompanyGUID,
                        Owner = verificationLine.Owner,
                        OwnerBusinessUnitGUID = verificationLine.OwnerBusinessUnitGUID,
                        VerificationLineGUID = verificationLine.VerificationLineGUID,
                        VerificationTypeGUID = verificationLine.VerificationTypeGUID,
                        Pass = verificationLine.Pass,
                        VerificationType_Values = verificationType.VerificationTypeId,
                        VerificationTableGUID = verificationLine.VerificationTableGUID,
                    });
        }
        public SearchResult<VerificationLineListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<VerificationLineListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<VerificationLine>(search, SysParm.CompanyGUID);
      
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<VerificationLineListViewMap, VerificationLineListView>();
                result = list.SetSearchResult<VerificationLineListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<VerificationLineItemViewMap> GetItemQuery()
        {
            return (from verificationLine in Entity
                    join company in db.Set<Company>()
                    on verificationLine.CompanyGUID equals company.CompanyGUID
                    join ownerBU in db.Set<BusinessUnit>()
                    on verificationLine.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljVerificationLineOwnerBU
                    from ownerBU in ljVerificationLineOwnerBU.DefaultIfEmpty()
                    join verificationTable in db.Set<VerificationTable>()
                    on verificationLine.VerificationTableGUID equals verificationTable.VerificationTableGUID into ljverificationTable
                    from verificationTable in ljverificationTable.DefaultIfEmpty()
                    join documentStatus in db.Set<DocumentStatus>()
                    on verificationTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
                    select new VerificationLineItemViewMap
                    {
                        CompanyGUID = verificationLine.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = verificationLine.Owner,
                        OwnerBusinessUnitGUID = verificationLine.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        CreatedBy = verificationLine.CreatedBy,
                        CreatedDateTime = verificationLine.CreatedDateTime,
                        ModifiedBy = verificationLine.ModifiedBy,
                        ModifiedDateTime = verificationLine.ModifiedDateTime,
                        VerificationLineGUID = verificationLine.VerificationLineGUID,
                        Pass = verificationLine.Pass,
                        Remark = verificationLine.Remark,
                        VendorTableGUID = verificationLine.VendorTableGUID,
                        VerificationTableGUID = verificationLine.VerificationTableGUID,
                        VerificationTypeGUID = verificationLine.VerificationTypeGUID,
                        DocumentStatus_Values = documentStatus.StatusId,
                        VerificationTable_Values = SmartAppUtil.GetDropDownLabel(verificationTable.VerificationId, verificationTable.Description),
                    
                        RowVersion = verificationLine.RowVersion,
                    });
        }
        public VerificationLineItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<VerificationLineItemViewMap, VerificationLineItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public VerificationLine CreateVerificationLine(VerificationLine verificationLine)
        {
            try
            {
                verificationLine.VerificationLineGUID = Guid.NewGuid();
                base.Add(verificationLine);
                return verificationLine;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateVerificationLineVoid(VerificationLine verificationLine)
        {
            try
            {
                CreateVerificationLine(verificationLine);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public VerificationLine UpdateVerificationLine(VerificationLine dbVerificationLine, VerificationLine inputVerificationLine, List<string> skipUpdateFields = null)
        {
            try
            {
                dbVerificationLine = dbVerificationLine.MapUpdateValues<VerificationLine>(inputVerificationLine);
                base.Update(dbVerificationLine);
                return dbVerificationLine;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateVerificationLineVoid(VerificationLine dbVerificationLine, VerificationLine inputVerificationLine, List<string> skipUpdateFields = null)
        {
            try
            {
                dbVerificationLine = dbVerificationLine.MapUpdateValues<VerificationLine>(inputVerificationLine, skipUpdateFields);
                base.Update(dbVerificationLine);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Create, Update, Delete
        #region Function
        public VerificationLine GetVerificationLineByIdNoTrackingByAccessLevel(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.VerificationLineGUID == guid)
                    .FilterByAccessLevel(SysParm.AccessLevel)
                    .AsNoTracking()
                    .FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<VerificationLine> GetVerificationLineByVerificationTableNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.VerificationTableGUID == guid).AsNoTracking().ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Function
        public override void ValidateAdd(IEnumerable<VerificationLine> items)
        {
            base.ValidateAdd(items);
        }
        public override void ValidateAdd(VerificationLine item)
        {
            base.ValidateAdd(item);
        }

        public List<VerificationLine> GetVerificationLineByVerificationTable(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.VerificationTableGUID == guid).AsNoTracking().ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}

