using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IInterestRealizedTransRepo
	{
		#region DropDown
		IEnumerable<SelectItem<InterestRealizedTransItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<InterestRealizedTransListView> GetListvw(SearchParameter search);
		InterestRealizedTransItemView GetByIdvw(Guid id);
		InterestRealizedTrans Find(params object[] keyValues);
		InterestRealizedTrans GetInterestRealizedTransByIdNoTracking(Guid guid);
		InterestRealizedTrans CreateInterestRealizedTrans(InterestRealizedTrans interestRealizedTrans);
		void CreateInterestRealizedTransVoid(InterestRealizedTrans interestRealizedTrans);
		InterestRealizedTrans UpdateInterestRealizedTrans(InterestRealizedTrans dbInterestRealizedTrans, InterestRealizedTrans inputInterestRealizedTrans, List<string> skipUpdateFields = null);
		void UpdateInterestRealizedTransVoid(InterestRealizedTrans dbInterestRealizedTrans, InterestRealizedTrans inputInterestRealizedTrans, List<string> skipUpdateFields = null);
		void Remove(InterestRealizedTrans item);
		InterestRealizedTrans GetInterestRealizedTransByIdNoTrackingByAccessLevel(Guid guid);
		List<InterestRealizedTrans> GetInterestRealizedTransByReferenceNoTracking(RefType refType, IEnumerable<Guid> interestRealizedTransGuids);
		List<InterestRealizedTrans> GetInterestRealizedTransForGenInterestRealizeProcessTrans(List<int> productType, DateTime asOfDate);
		List<InterestRealizedTrans> GetInterestRealizedTransByProcessTransNoTracking(List<Guid> processTransGUID);
		List<InterestRealizedTrans> GetInterestRealizedTransForPostReceiptTempNoTracking(ProductType productType, IEnumerable<Guid> refGuids);
		void ValidateAdd(InterestRealizedTrans item);
		void ValidateAdd(IEnumerable<InterestRealizedTrans> items);
	}
	public class InterestRealizedTransRepo : CompanyBaseRepository<InterestRealizedTrans>, IInterestRealizedTransRepo
	{
		public InterestRealizedTransRepo(SmartAppDbContext context) : base(context) { }
		public InterestRealizedTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public InterestRealizedTrans GetInterestRealizedTransByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.InterestRealizedTransGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<InterestRealizedTransItemViewMap> GetDropDownQuery()
		{
			return (from interestRealizedTrans in Entity
					select new InterestRealizedTransItemViewMap
					{
						CompanyGUID = interestRealizedTrans.CompanyGUID,
						Owner = interestRealizedTrans.Owner,
						OwnerBusinessUnitGUID = interestRealizedTrans.OwnerBusinessUnitGUID,
						InterestRealizedTransGUID = interestRealizedTrans.InterestRealizedTransGUID
					});
		}
		public IEnumerable<SelectItem<InterestRealizedTransItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<InterestRealizedTrans>(search, SysParm.CompanyGUID);
				var interestRealizedTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<InterestRealizedTransItemViewMap, InterestRealizedTransItemView>().ToDropDownItem(search.ExcludeRowData);


				return interestRealizedTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<InterestRealizedTransListViewMap> GetListQuery()
		{
			return (from interestRealizedTrans in Entity
					join processTrans in db.Set<ProcessTrans>()
					on interestRealizedTrans.RefProcessTransGUID equals processTrans.ProcessTransGUID into ljprocessTrans
					from processTrans in ljprocessTrans.DefaultIfEmpty()
				select new InterestRealizedTransListViewMap
				{
						CompanyGUID = interestRealizedTrans.CompanyGUID,
						Owner = interestRealizedTrans.Owner,
						OwnerBusinessUnitGUID = interestRealizedTrans.OwnerBusinessUnitGUID,
						InterestRealizedTransGUID = interestRealizedTrans.InterestRealizedTransGUID,
						LineNum = interestRealizedTrans.LineNum,
						StartDate = interestRealizedTrans.StartDate,
						EndDate = interestRealizedTrans.EndDate,
						AccountingDate = interestRealizedTrans.AccountingDate,
						InterestDay = interestRealizedTrans.InterestDay,
						AccInterestAmount = interestRealizedTrans.AccInterestAmount,
						DocumentId = interestRealizedTrans.DocumentId,
						RefGUID = interestRealizedTrans.RefGUID,
						Cancelled = interestRealizedTrans.Cancelled,
						RefProcessTransGUID = interestRealizedTrans.RefProcessTransGUID,
						ProcessTrans_Value = (processTrans != null) ? SmartAppUtil.GetDropDownLabel(((ProcessTransType)processTrans.ProcessTransType).ToString(), processTrans.DocumentId) : null,
				});
		}
		public SearchResult<InterestRealizedTransListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<InterestRealizedTransListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<InterestRealizedTrans>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<InterestRealizedTransListViewMap, InterestRealizedTransListView>();
				result = list.SetSearchResult<InterestRealizedTransListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<InterestRealizedTransItemViewMap> GetItemQuery()
		{
			return (from interestRealizedTrans in Entity
					join company in db.Set<Company>()
					on interestRealizedTrans.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on interestRealizedTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljInterestRealizedTransOwnerBU
					from ownerBU in ljInterestRealizedTransOwnerBU.DefaultIfEmpty()
					join purchaseLine in db.Set<PurchaseLine>() on interestRealizedTrans.RefGUID equals purchaseLine.PurchaseLineGUID into ljpurchaseLine
					from purchaseLine in ljpurchaseLine.DefaultIfEmpty()
					join purchaseTable in db.Set<PurchaseTable>() on purchaseLine.PurchaseTableGUID equals purchaseTable.PurchaseTableGUID into ljpurchaseTable
					from purchaseTable in ljpurchaseTable.DefaultIfEmpty()

					join withdrawaLine in db.Set<WithdrawalLine>()
					on interestRealizedTrans.RefGUID equals withdrawaLine.WithdrawalLineGUID into ljwithdrawal
					from withdrawaLine in ljwithdrawal.DefaultIfEmpty()
					join processTrans in db.Set<ProcessTrans>()
					on interestRealizedTrans.RefProcessTransGUID equals processTrans.ProcessTransGUID into ljprocessTrans
					from processTrans in ljprocessTrans.DefaultIfEmpty()

					join ledgerDimension1 in db.Set<LedgerDimension>()
					on interestRealizedTrans.Dimension1GUID equals ledgerDimension1.LedgerDimensionGUID into ljledgerDimension1
					from ledgerDimension1 in ljledgerDimension1.DefaultIfEmpty()
					join ledgerDimension2 in db.Set<LedgerDimension>()
					on interestRealizedTrans.Dimension2GUID equals ledgerDimension2.LedgerDimensionGUID into ljledgerDimension2
					from ledgerDimension2 in ljledgerDimension2.DefaultIfEmpty()
					join ledgerDimension3 in db.Set<LedgerDimension>()
					on interestRealizedTrans.Dimension3GUID equals ledgerDimension3.LedgerDimensionGUID into ljledgerDimension3
					from ledgerDimension3 in ljledgerDimension3.DefaultIfEmpty()
					join ledgerDimension4 in db.Set<LedgerDimension>()
					on interestRealizedTrans.Dimension4GUID equals ledgerDimension4.LedgerDimensionGUID into ljledgerDimension4
					from ledgerDimension4 in ljledgerDimension4.DefaultIfEmpty()
					join ledgerDimension5 in db.Set<LedgerDimension>()
					on interestRealizedTrans.Dimension5GUID equals ledgerDimension5.LedgerDimensionGUID into ljledgerDimension5
					from ledgerDimension5 in ljledgerDimension5.DefaultIfEmpty()

					select new InterestRealizedTransItemViewMap
					{
						CompanyGUID = interestRealizedTrans.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = interestRealizedTrans.Owner,
						OwnerBusinessUnitGUID = interestRealizedTrans.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = interestRealizedTrans.CreatedBy,
						CreatedDateTime = interestRealizedTrans.CreatedDateTime,
						ModifiedBy = interestRealizedTrans.ModifiedBy,
						ModifiedDateTime = interestRealizedTrans.ModifiedDateTime,
						InterestRealizedTransGUID = interestRealizedTrans.InterestRealizedTransGUID,
						AccInterestAmount = interestRealizedTrans.AccInterestAmount,
						AccountingDate = interestRealizedTrans.AccountingDate,
						Accrued = interestRealizedTrans.Accrued,
						DocumentId = interestRealizedTrans.DocumentId,
						EndDate = interestRealizedTrans.EndDate,
						InterestDay = interestRealizedTrans.InterestDay,
						InterestPerDay = interestRealizedTrans.InterestPerDay,
						LineNum = interestRealizedTrans.LineNum,
						ProductType = interestRealizedTrans.ProductType,
                        RefGUID = (withdrawaLine != null && interestRealizedTrans.RefGUID == withdrawaLine.WithdrawalLineGUID) ? withdrawaLine.WithdrawalLineGUID:
                                   ( purchaseLine != null && interestRealizedTrans.RefGUID == purchaseLine.PurchaseLineGUID) ? purchaseLine.PurchaseLineGUID:
                                    new Guid(),
						RefType = interestRealizedTrans.RefType,
						StartDate = interestRealizedTrans.StartDate,
						
						RefID = (withdrawaLine != null && interestRealizedTrans.RefType == (int)RefType.WithdrawalLine) ? interestRealizedTrans.LineNum.ToString():
								(purchaseLine !=null && interestRealizedTrans.RefType==(int)RefType.PurchaseLine)?purchaseLine.LineNum.ToString():
								null,
						ProcessTrans_Value = (processTrans != null ) ? SmartAppUtil.GetDropDownLabel(((ProcessTransType)processTrans.ProcessTransType).ToString(), processTrans.DocumentId) : null,
						ProcessTrans_TransDate = (processTrans != null) ? (DateTime?)processTrans.TransDate : null,
						Cancelled = interestRealizedTrans.Cancelled,
						Dimension1GUID = interestRealizedTrans.Dimension1GUID,
						Dimension2GUID = interestRealizedTrans.Dimension2GUID,
						Dimension3GUID = interestRealizedTrans.Dimension3GUID,
						Dimension4GUID = interestRealizedTrans.Dimension4GUID,
						Dimension5GUID = interestRealizedTrans.Dimension5GUID,
						Dimension1_Values = SmartAppUtil.GetDropDownLabel(ledgerDimension1.DimensionCode, ledgerDimension1.Description),
						Dimension2_Values = SmartAppUtil.GetDropDownLabel(ledgerDimension2.DimensionCode, ledgerDimension2.Description),
						Dimension3_Values = SmartAppUtil.GetDropDownLabel(ledgerDimension3.DimensionCode, ledgerDimension3.Description),
						Dimension4_Values = SmartAppUtil.GetDropDownLabel(ledgerDimension4.DimensionCode, ledgerDimension4.Description),
						Dimension5_Values = SmartAppUtil.GetDropDownLabel(ledgerDimension5.DimensionCode, ledgerDimension5.Description),
					
						RowVersion = interestRealizedTrans.RowVersion,
					});
		}
		public InterestRealizedTransItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<InterestRealizedTransItemViewMap, InterestRealizedTransItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public InterestRealizedTrans CreateInterestRealizedTrans(InterestRealizedTrans interestRealizedTrans)
		{
			try
			{
				interestRealizedTrans.InterestRealizedTransGUID = Guid.NewGuid();
				base.Add(interestRealizedTrans);
				return interestRealizedTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateInterestRealizedTransVoid(InterestRealizedTrans interestRealizedTrans)
		{
			try
			{
				CreateInterestRealizedTrans(interestRealizedTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public InterestRealizedTrans UpdateInterestRealizedTrans(InterestRealizedTrans dbInterestRealizedTrans, InterestRealizedTrans inputInterestRealizedTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbInterestRealizedTrans = dbInterestRealizedTrans.MapUpdateValues<InterestRealizedTrans>(inputInterestRealizedTrans);
				base.Update(dbInterestRealizedTrans);
				return dbInterestRealizedTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateInterestRealizedTransVoid(InterestRealizedTrans dbInterestRealizedTrans, InterestRealizedTrans inputInterestRealizedTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbInterestRealizedTrans = dbInterestRealizedTrans.MapUpdateValues<InterestRealizedTrans>(inputInterestRealizedTrans, skipUpdateFields);
				base.Update(dbInterestRealizedTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public InterestRealizedTrans GetInterestRealizedTransByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.InterestRealizedTransGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<InterestRealizedTrans> GetInterestRealizedTransByReferenceNoTracking(RefType refType, IEnumerable<Guid> interestRealizedTransGuids)
        {
            try
            {
				var result = Entity.Where(w => w.RefType == (int)refType && 
											interestRealizedTransGuids.Contains(w.InterestRealizedTransGUID))
									.AsNoTracking()
									.ToList();
				return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
		}
		public List<InterestRealizedTrans> GetInterestRealizedTransForGenInterestRealizeProcessTrans(List<int> productType,DateTime asOfDate)
		{
			try
			{
				var result = Entity.Where(w => productType.Contains(w.ProductType) &&
											   w.AccountingDate <= asOfDate &&
											   w.RefProcessTransGUID == null &&
											   w.Cancelled == false)
									.AsNoTracking()
									.ToList();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<InterestRealizedTrans> GetInterestRealizedTransByProcessTransNoTracking(List<Guid> processTransGUID)
        {
            try
            {
				List<InterestRealizedTrans> interestRealizedTrans = Entity.Where(w => w.RefProcessTransGUID.HasValue &&
																					processTransGUID.Contains(w.RefProcessTransGUID.Value))
																			.AsNoTracking().ToList();
				return interestRealizedTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<InterestRealizedTrans> GetInterestRealizedTransForPostReceiptTempNoTracking(ProductType productType, IEnumerable<Guid> refGuids)
        {
            try
            {
				var result = Entity.Where(w => refGuids.Contains(w.RefGUID) &&
												w.ProductType == (int)productType &&
												w.RefProcessTransGUID == null &&
												w.Cancelled == false)
									.AsNoTracking()
									.ToList();
				return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
	}
}

