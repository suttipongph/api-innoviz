using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface INumberSeqSetupByProductTypeRepo
	{
		#region DropDown
		IEnumerable<SelectItem<NumberSeqSetupByProductTypeItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<NumberSeqSetupByProductTypeListView> GetListvw(SearchParameter search);
		NumberSeqSetupByProductTypeItemView GetByIdvw(Guid id);
		NumberSeqSetupByProductType Find(params object[] keyValues);
		NumberSeqSetupByProductType GetNumberSeqSetupByProductTypeByIdNoTracking(Guid guid);
		NumberSeqSetupByProductType CreateNumberSeqSetupByProductType(NumberSeqSetupByProductType numberSeqSetupByProductType);
		void CreateNumberSeqSetupByProductTypeVoid(NumberSeqSetupByProductType numberSeqSetupByProductType);
		NumberSeqSetupByProductType UpdateNumberSeqSetupByProductType(NumberSeqSetupByProductType dbNumberSeqSetupByProductType, NumberSeqSetupByProductType inputNumberSeqSetupByProductType, List<string> skipUpdateFields = null);
		void UpdateNumberSeqSetupByProductTypeVoid(NumberSeqSetupByProductType dbNumberSeqSetupByProductType, NumberSeqSetupByProductType inputNumberSeqSetupByProductType, List<string> skipUpdateFields = null);
		void Remove(NumberSeqSetupByProductType item);
		NumberSeqSetupByProductType GetNumberSeqSetupByProductType(Guid companyGUID, int productType);
		Guid GetCreditAppRequestNumberSeqGUID(Guid companyGUID, int productType);
		
		Guid GetCreditAppNumberSeqGUID(Guid companyGUID, int productType);
		Guid GetMainAgreementNumberSeqGUID(Guid companyGUID, int productType);
		Guid GetInternalMainAgreementNumberSeqGUID(Guid companyGUID, int productType);
		Guid GetInternalGuarantorAgreementNumberSeqGUID(Guid companyGUID, int productType);
		Guid GetGuarantorAgreementNumberSeqGUID(Guid companyGUID, int productType);
		Guid GetTaxInvoiceNumberSeqGUID(Guid companyGUID, int productType);
		Guid GetTaxCreditNoteNumberSeqGUID(Guid companyGUID, int productType);
		Guid GetReceiptNumberSeqGUID(Guid companyGUID, int productType);
	}
	public class NumberSeqSetupByProductTypeRepo : CompanyBaseRepository<NumberSeqSetupByProductType>, INumberSeqSetupByProductTypeRepo
	{
		public NumberSeqSetupByProductTypeRepo(SmartAppDbContext context) : base(context) { }
		public NumberSeqSetupByProductTypeRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public NumberSeqSetupByProductType GetNumberSeqSetupByProductTypeByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.NumberSeqSetupByProductTypeGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<NumberSeqSetupByProductTypeItemViewMap> GetDropDownQuery()
		{
			return (from numberSeqSetupByProductType in Entity
					select new NumberSeqSetupByProductTypeItemViewMap
					{
						CompanyGUID = numberSeqSetupByProductType.CompanyGUID,
						Owner = numberSeqSetupByProductType.Owner,
						OwnerBusinessUnitGUID = numberSeqSetupByProductType.OwnerBusinessUnitGUID,
						NumberSeqSetupByProductTypeGUID = numberSeqSetupByProductType.NumberSeqSetupByProductTypeGUID,
						ProductType = numberSeqSetupByProductType.ProductType
					});
		}
		public IEnumerable<SelectItem<NumberSeqSetupByProductTypeItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<NumberSeqSetupByProductType>(search, SysParm.CompanyGUID);
				var numberSeqSetupByProductType = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<NumberSeqSetupByProductTypeItemViewMap, NumberSeqSetupByProductTypeItemView>().ToDropDownItem(search.ExcludeRowData);


				return numberSeqSetupByProductType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<NumberSeqSetupByProductTypeListViewMap> GetListQuery()
		{
			return (from numberSeqSetupByProductType in Entity
					join numberSeqtable in db.Set<NumberSeqTable>()
					on numberSeqSetupByProductType.CreditAppRequestNumberSeqGUID equals numberSeqtable.NumberSeqTableGUID into ljNumberSeqSetupByProductTypeCreditAppRequest
					from numberSeqtable in ljNumberSeqSetupByProductTypeCreditAppRequest.DefaultIfEmpty()
					join numberSeqtable1 in db.Set<NumberSeqTable>()
					on numberSeqSetupByProductType.CreditAppNumberSeqGUID equals numberSeqtable1.NumberSeqTableGUID into ljNumberSeqSetupByProductTypeCreditApp
					from numberSeqtable1 in ljNumberSeqSetupByProductTypeCreditApp.DefaultIfEmpty()
					join numberSeqtable2 in db.Set<NumberSeqTable>()
					on numberSeqSetupByProductType.InternalMainAgreementNumberSeqGUID equals numberSeqtable2.NumberSeqTableGUID into ljNumberSeqSetupByProductTypeInternalMainAgreement
					from numberSeqtable2 in ljNumberSeqSetupByProductTypeInternalMainAgreement.DefaultIfEmpty()
					join numberSeqtable3 in db.Set<NumberSeqTable>()
					on numberSeqSetupByProductType.MainAgreementNumberSeqGUID equals numberSeqtable3.NumberSeqTableGUID into ljNumberSeqSetupByProductTypeMainAgreementNumber
					from numberSeqtable3 in ljNumberSeqSetupByProductTypeMainAgreementNumber.DefaultIfEmpty()
					join numberSeqtable4 in db.Set<NumberSeqTable>()
					on numberSeqSetupByProductType.InternalGuarantorAgreementNumberSeqGUID equals numberSeqtable4.NumberSeqTableGUID into ljNumberSeqSetupByProductTypeInternalGuarantorAgreement
					from numberSeqtable4 in ljNumberSeqSetupByProductTypeInternalGuarantorAgreement.DefaultIfEmpty()
					join numberSeqtable5 in db.Set<NumberSeqTable>()
					on numberSeqSetupByProductType.GuarantorAgreementNumberSeqGUID equals numberSeqtable5.NumberSeqTableGUID into ljNumberSeqSetupByProductTypeGuarantorAgreementNumber
					from numberSeqtable5 in ljNumberSeqSetupByProductTypeGuarantorAgreementNumber.DefaultIfEmpty()
					join numberSeqtable6 in db.Set<NumberSeqTable>()
					on numberSeqSetupByProductType.TaxInvoiceNumberSeqGUID equals numberSeqtable6.NumberSeqTableGUID into ljNumberSeqSetupByProductTypeTaxInvoiceNumber
					from numberSeqtable6 in ljNumberSeqSetupByProductTypeTaxInvoiceNumber.DefaultIfEmpty()
					join numberSeqtable7 in db.Set<NumberSeqTable>()
					on numberSeqSetupByProductType.TaxCreditNoteNumberSeqGUID equals numberSeqtable7.NumberSeqTableGUID into ljNumberSeqSetupByProductTypeTaxCreditNoteNumber
					from numberSeqtable7 in ljNumberSeqSetupByProductTypeTaxCreditNoteNumber.DefaultIfEmpty()
					join numberSeqtable8 in db.Set<NumberSeqTable>()
					on numberSeqSetupByProductType.ReceiptNumberSeqGUID equals numberSeqtable8.NumberSeqTableGUID into ljNumberSeqSetupByProductTypeReceiptNumber
					from numberSeqtable8 in ljNumberSeqSetupByProductTypeReceiptNumber.DefaultIfEmpty()
					select new NumberSeqSetupByProductTypeListViewMap
				{
						CompanyGUID = numberSeqSetupByProductType.CompanyGUID,
						Owner = numberSeqSetupByProductType.Owner,
						OwnerBusinessUnitGUID = numberSeqSetupByProductType.OwnerBusinessUnitGUID,
						NumberSeqSetupByProductTypeGUID = numberSeqSetupByProductType.NumberSeqSetupByProductTypeGUID,
						ProductType = numberSeqSetupByProductType.ProductType,
						CreditAppRequestNumberSeqGUID = numberSeqSetupByProductType.CreditAppRequestNumberSeqGUID,
						CreditAppNumberSeqGUID = numberSeqSetupByProductType.CreditAppNumberSeqGUID,
						InternalMainAgreementNumberSeqGUID = numberSeqSetupByProductType.InternalMainAgreementNumberSeqGUID,
						MainAgreementNumberSeqGUID = numberSeqSetupByProductType.MainAgreementNumberSeqGUID,
						InternalGuarantorAgreementNumberSeqGUID = numberSeqSetupByProductType.InternalGuarantorAgreementNumberSeqGUID,
						GuarantorAgreementNumberSeqGUID = numberSeqSetupByProductType.GuarantorAgreementNumberSeqGUID,
						TaxInvoiceNumberSeqGUID = numberSeqSetupByProductType.TaxInvoiceNumberSeqGUID,
						TaxCreditNoteNumberSeqGUID = numberSeqSetupByProductType.TaxCreditNoteNumberSeqGUID,
						ReceiptNumberSeqGUID = numberSeqSetupByProductType.ReceiptNumberSeqGUID,
						NumberSeqTableGUID = numberSeqtable.NumberSeqTableGUID,
						NumberSeqCode = numberSeqtable.NumberSeqCode,

						CreditAppRequestNumber_Values = SmartAppUtil.GetDropDownLabel(numberSeqtable.NumberSeqCode,numberSeqtable.Description),
						CreditAppNumberSeq_Values = SmartAppUtil.GetDropDownLabel(numberSeqtable1.NumberSeqCode, numberSeqtable1.Description),
						InternalMainAgreementNumber_Values = SmartAppUtil.GetDropDownLabel(numberSeqtable2.NumberSeqCode, numberSeqtable2.Description),
						MainAgreementNumber_Values = SmartAppUtil.GetDropDownLabel(numberSeqtable3.NumberSeqCode, numberSeqtable3.Description),
						InternalGuarantorAgreementNumber_Values = SmartAppUtil.GetDropDownLabel(numberSeqtable4.NumberSeqCode, numberSeqtable4.Description),
						GuarantorAgreementNumber_Values = SmartAppUtil.GetDropDownLabel(numberSeqtable5.NumberSeqCode, numberSeqtable5.Description),
						TaxInvoiceNumberSeq_Values = SmartAppUtil.GetDropDownLabel(numberSeqtable6.NumberSeqCode, numberSeqtable6.Description),
						TaxCreditNoteNumberSeq_Values = SmartAppUtil.GetDropDownLabel(numberSeqtable7.NumberSeqCode, numberSeqtable7.Description),
						ReceiptNumberSeq_Values = SmartAppUtil.GetDropDownLabel(numberSeqtable8.NumberSeqCode, numberSeqtable8.Description),
					});
		}
		public SearchResult<NumberSeqSetupByProductTypeListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<NumberSeqSetupByProductTypeListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<NumberSeqSetupByProductType>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<NumberSeqSetupByProductTypeListViewMap, NumberSeqSetupByProductTypeListView>();
				result = list.SetSearchResult<NumberSeqSetupByProductTypeListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<NumberSeqSetupByProductTypeItemViewMap> GetItemQuery()
		{
			return (from numberSeqSetupByProductType in Entity
					join company in db.Set<Company>()
					on numberSeqSetupByProductType.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on numberSeqSetupByProductType.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljNumberSeqSetupByProductTypeOwnerBU
					from ownerBU in ljNumberSeqSetupByProductTypeOwnerBU.DefaultIfEmpty()
					select new NumberSeqSetupByProductTypeItemViewMap
					{
						CompanyGUID = numberSeqSetupByProductType.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = numberSeqSetupByProductType.Owner,
						OwnerBusinessUnitGUID = numberSeqSetupByProductType.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = numberSeqSetupByProductType.CreatedBy,
						CreatedDateTime = numberSeqSetupByProductType.CreatedDateTime,
						ModifiedBy = numberSeqSetupByProductType.ModifiedBy,
						ModifiedDateTime = numberSeqSetupByProductType.ModifiedDateTime,
						NumberSeqSetupByProductTypeGUID = numberSeqSetupByProductType.NumberSeqSetupByProductTypeGUID,
						CreditAppNumberSeqGUID = numberSeqSetupByProductType.CreditAppNumberSeqGUID,
						CreditAppRequestNumberSeqGUID = numberSeqSetupByProductType.CreditAppRequestNumberSeqGUID,
						GuarantorAgreementNumberSeqGUID = numberSeqSetupByProductType.GuarantorAgreementNumberSeqGUID,
						InternalGuarantorAgreementNumberSeqGUID = numberSeqSetupByProductType.InternalGuarantorAgreementNumberSeqGUID,
						InternalMainAgreementNumberSeqGUID = numberSeqSetupByProductType.InternalMainAgreementNumberSeqGUID,
						MainAgreementNumberSeqGUID = numberSeqSetupByProductType.MainAgreementNumberSeqGUID,
						TaxInvoiceNumberSeqGUID = numberSeqSetupByProductType.TaxInvoiceNumberSeqGUID,
						TaxCreditNoteNumberSeqGUID = numberSeqSetupByProductType.TaxCreditNoteNumberSeqGUID,
						ReceiptNumberSeqGUID = numberSeqSetupByProductType.ReceiptNumberSeqGUID,
						ProductType = numberSeqSetupByProductType.ProductType,
					
						RowVersion = numberSeqSetupByProductType.RowVersion,
					});
		}
		public NumberSeqSetupByProductTypeItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.ToMap<NumberSeqSetupByProductTypeItemViewMap, NumberSeqSetupByProductTypeItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public NumberSeqSetupByProductType CreateNumberSeqSetupByProductType(NumberSeqSetupByProductType numberSeqSetupByProductType)
		{
			try
			{
				numberSeqSetupByProductType.NumberSeqSetupByProductTypeGUID = Guid.NewGuid();
				base.Add(numberSeqSetupByProductType);
				return numberSeqSetupByProductType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateNumberSeqSetupByProductTypeVoid(NumberSeqSetupByProductType numberSeqSetupByProductType)
		{
			try
			{
				CreateNumberSeqSetupByProductType(numberSeqSetupByProductType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public NumberSeqSetupByProductType UpdateNumberSeqSetupByProductType(NumberSeqSetupByProductType dbNumberSeqSetupByProductType, NumberSeqSetupByProductType inputNumberSeqSetupByProductType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbNumberSeqSetupByProductType = dbNumberSeqSetupByProductType.MapUpdateValues<NumberSeqSetupByProductType>(inputNumberSeqSetupByProductType);
				base.Update(dbNumberSeqSetupByProductType);
				return dbNumberSeqSetupByProductType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateNumberSeqSetupByProductTypeVoid(NumberSeqSetupByProductType dbNumberSeqSetupByProductType, NumberSeqSetupByProductType inputNumberSeqSetupByProductType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbNumberSeqSetupByProductType = dbNumberSeqSetupByProductType.MapUpdateValues<NumberSeqSetupByProductType>(inputNumberSeqSetupByProductType, skipUpdateFields);
				base.Update(dbNumberSeqSetupByProductType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public NumberSeqSetupByProductType GetNumberSeqSetupByProductType(Guid companyGUID, int productType)
		{
			try
			{
				NumberSeqSetupByProductType numberSeqSetupByProductType = Entity.Where(w => w.CompanyGUID == companyGUID && w.ProductType == productType).AsNoTracking().FirstOrDefault();
				if (numberSeqSetupByProductType == null)
				{
					SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
					smartAppException.AddData("ERROR.90017"); 
					throw smartAppException;
				}
				return numberSeqSetupByProductType;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public Guid GetCreditAppRequestNumberSeqGUID(Guid companyGUID, int productType)
		{
			try
			{
				NumberSeqSetupByProductType numberSeqSetupByProductType = GetNumberSeqSetupByProductType(companyGUID, productType);
				if (numberSeqSetupByProductType.CreditAppRequestNumberSeqGUID == null)
                {
					SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
					smartAppException.AddData("ERROR.90017", new string[] { "LABEL.CREDIT_APPLICATION_REQUEST_NUMBER_SEQUENCE_CODE" });
					throw smartAppException;
				}
				return numberSeqSetupByProductType.CreditAppRequestNumberSeqGUID.Value;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		
		public Guid GetCreditAppNumberSeqGUID(Guid companyGUID, int productType)
		{
			try
			{
				NumberSeqSetupByProductType numberSeqSetupByProductType = GetNumberSeqSetupByProductType(companyGUID, productType);
				if (numberSeqSetupByProductType.CreditAppNumberSeqGUID == null)
				{
					SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
					smartAppException.AddData("ERROR.90017", new string[] { "LABEL.CREDIT_APPLICATION_NUMBER_SEQUENCE_CODE" });
					throw smartAppException;
				}
				return numberSeqSetupByProductType.CreditAppNumberSeqGUID.Value;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public Guid GetMainAgreementNumberSeqGUID(Guid companyGUID, int productType)
		{
			try
			{
				NumberSeqSetupByProductType numberSeqSetupByProductType = GetNumberSeqSetupByProductType(companyGUID, productType);
				if (numberSeqSetupByProductType.MainAgreementNumberSeqGUID == null)
				{
					SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
					smartAppException.AddData("ERROR.90018", new string[] { "LABEL.MAIN_AGREEMENT_NUMBER_SEQUENCE_CODE" });
					throw smartAppException;
				}
				return numberSeqSetupByProductType.MainAgreementNumberSeqGUID.Value;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public Guid GetInternalMainAgreementNumberSeqGUID(Guid companyGUID, int productType)
		{
			try
			{
				NumberSeqSetupByProductType numberSeqSetupByProductType = GetNumberSeqSetupByProductType(companyGUID, productType);
				if (numberSeqSetupByProductType.InternalMainAgreementNumberSeqGUID == null)
				{
					SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
					smartAppException.AddData("ERROR.90018", new string[] { "LABEL.INTERNAL_MAIN_AGREEMENT_NUMBER_SEQUENCE_CODE" });
					throw smartAppException;
				}
				return numberSeqSetupByProductType.InternalMainAgreementNumberSeqGUID.Value;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public Guid GetInternalGuarantorAgreementNumberSeqGUID(Guid companyGUID, int productType)
		{
			try
			{
				NumberSeqSetupByProductType numberSeqSetupByProductType = GetNumberSeqSetupByProductType(companyGUID, productType);
				if (numberSeqSetupByProductType.InternalGuarantorAgreementNumberSeqGUID == null)
				{
					SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
					smartAppException.AddData("ERROR.90018", new string[] { "LABEL.INTERNAL_GUARANTOR_AGREEMENT_NUMBER_SEQUENCE_CODE" });
					throw smartAppException;
				}
				return numberSeqSetupByProductType.InternalGuarantorAgreementNumberSeqGUID.Value;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public Guid GetGuarantorAgreementNumberSeqGUID(Guid companyGUID, int productType)
		{
			try
			{
				NumberSeqSetupByProductType numberSeqSetupByProductType = GetNumberSeqSetupByProductType(companyGUID, productType);
				if (numberSeqSetupByProductType.GuarantorAgreementNumberSeqGUID == null)
				{
					SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
					smartAppException.AddData("ERROR.90018", new string[] { "LABEL.GUARANTOR_AGREEMENT_NUMBER_SEQUENCE_CODE" });
					throw smartAppException;
				}
				return numberSeqSetupByProductType.GuarantorAgreementNumberSeqGUID.Value;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public Guid GetTaxInvoiceNumberSeqGUID(Guid companyGUID, int productType)
		{
			try
			{
				NumberSeqSetupByProductType numberSeqSetupByProductType = GetNumberSeqSetupByProductType(companyGUID, productType);
				if (numberSeqSetupByProductType.TaxInvoiceNumberSeqGUID == null)
				{
					SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
					smartAppException.AddData("ERROR.90018", new string[] { "LABEL.TAX_INVOICE_NUMBER_SEQUENCE_CODE" });
					throw smartAppException;
				}
				return numberSeqSetupByProductType.TaxInvoiceNumberSeqGUID.Value;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public Guid GetTaxCreditNoteNumberSeqGUID(Guid companyGUID, int productType)
		{
			try
			{
				NumberSeqSetupByProductType numberSeqSetupByProductType = GetNumberSeqSetupByProductType(companyGUID, productType);
				if (numberSeqSetupByProductType.TaxCreditNoteNumberSeqGUID == null)
				{
					SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
					smartAppException.AddData("ERROR.90018", new string[] { "LABEL.TAX_CREDIT_NOTE_NUMBER_SEQUENCE_CODE" });
					throw smartAppException;
				}
				return numberSeqSetupByProductType.TaxCreditNoteNumberSeqGUID.Value;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public Guid GetReceiptNumberSeqGUID(Guid companyGUID, int productType)
		{
			try
			{
				NumberSeqSetupByProductType numberSeqSetupByProductType = GetNumberSeqSetupByProductType(companyGUID, productType);
				if (numberSeqSetupByProductType.ReceiptNumberSeqGUID == null)
				{
					SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
					smartAppException.AddData("ERROR.90018", new string[] { "LABEL.RECEIPT_NUMBER_SEQUENCE_CODE" });
					throw smartAppException;
				}
				return numberSeqSetupByProductType.ReceiptNumberSeqGUID.Value;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
	}
}

