using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IMaritalStatusRepo
	{
		#region DropDown
		IEnumerable<SelectItem<MaritalStatusItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<MaritalStatusListView> GetListvw(SearchParameter search);
		MaritalStatusItemView GetByIdvw(Guid id);
		MaritalStatus Find(params object[] keyValues);
		MaritalStatus GetMaritalStatusByIdNoTracking(Guid guid);
		MaritalStatus CreateMaritalStatus(MaritalStatus maritalStatus);
		void CreateMaritalStatusVoid(MaritalStatus maritalStatus);
		MaritalStatus UpdateMaritalStatus(MaritalStatus dbMaritalStatus, MaritalStatus inputMaritalStatus, List<string> skipUpdateFields = null);
		void UpdateMaritalStatusVoid(MaritalStatus dbMaritalStatus, MaritalStatus inputMaritalStatus, List<string> skipUpdateFields = null);
		void Remove(MaritalStatus item);
	}
	public class MaritalStatusRepo : CompanyBaseRepository<MaritalStatus>, IMaritalStatusRepo
	{
		public MaritalStatusRepo(SmartAppDbContext context) : base(context) { }
		public MaritalStatusRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public MaritalStatus GetMaritalStatusByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.MaritalStatusGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<MaritalStatusItemViewMap> GetDropDownQuery()
		{
			return (from maritalStatus in Entity
					select new MaritalStatusItemViewMap
					{
						CompanyGUID = maritalStatus.CompanyGUID,
						Owner = maritalStatus.Owner,
						OwnerBusinessUnitGUID = maritalStatus.OwnerBusinessUnitGUID,
						MaritalStatusGUID = maritalStatus.MaritalStatusGUID,
						MaritalStatusId = maritalStatus.MaritalStatusId,
						Description = maritalStatus.Description
					});
		}
		public IEnumerable<SelectItem<MaritalStatusItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<MaritalStatus>(search, SysParm.CompanyGUID);
				var maritalStatus = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<MaritalStatusItemViewMap, MaritalStatusItemView>().ToDropDownItem(search.ExcludeRowData);


				return maritalStatus;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<MaritalStatusListViewMap> GetListQuery()
		{
			return (from maritalStatus in Entity
				select new MaritalStatusListViewMap
				{
						CompanyGUID = maritalStatus.CompanyGUID,
						Owner = maritalStatus.Owner,
						OwnerBusinessUnitGUID = maritalStatus.OwnerBusinessUnitGUID,
						MaritalStatusGUID = maritalStatus.MaritalStatusGUID,
						MaritalStatusId = maritalStatus.MaritalStatusId,
						Description = maritalStatus.Description
				});
		}
		public SearchResult<MaritalStatusListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<MaritalStatusListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<MaritalStatus>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<MaritalStatusListViewMap, MaritalStatusListView>();
				result = list.SetSearchResult<MaritalStatusListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<MaritalStatusItemViewMap> GetItemQuery()
		{
			return (from maritalStatus in Entity
					join company in db.Set<Company>()
					on maritalStatus.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on maritalStatus.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljMaritalStatusOwnerBU
					from ownerBU in ljMaritalStatusOwnerBU.DefaultIfEmpty()
					select new MaritalStatusItemViewMap
					{
						CompanyGUID = maritalStatus.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = maritalStatus.Owner,
						OwnerBusinessUnitGUID = maritalStatus.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = maritalStatus.CreatedBy,
						CreatedDateTime = maritalStatus.CreatedDateTime,
						ModifiedBy = maritalStatus.ModifiedBy,
						ModifiedDateTime = maritalStatus.ModifiedDateTime,
						MaritalStatusGUID = maritalStatus.MaritalStatusGUID,
						Description = maritalStatus.Description,
						MaritalStatusId = maritalStatus.MaritalStatusId,

					
						RowVersion = maritalStatus.RowVersion,
					});
		}
		public MaritalStatusItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<MaritalStatusItemViewMap, MaritalStatusItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public MaritalStatus CreateMaritalStatus(MaritalStatus maritalStatus)
		{
			try
			{
				maritalStatus.MaritalStatusGUID = Guid.NewGuid();
				base.Add(maritalStatus);
				return maritalStatus;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateMaritalStatusVoid(MaritalStatus maritalStatus)
		{
			try
			{
				CreateMaritalStatus(maritalStatus);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public MaritalStatus UpdateMaritalStatus(MaritalStatus dbMaritalStatus, MaritalStatus inputMaritalStatus, List<string> skipUpdateFields = null)
		{
			try
			{
				dbMaritalStatus = dbMaritalStatus.MapUpdateValues<MaritalStatus>(inputMaritalStatus);
				base.Update(dbMaritalStatus);
				return dbMaritalStatus;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateMaritalStatusVoid(MaritalStatus dbMaritalStatus, MaritalStatus inputMaritalStatus, List<string> skipUpdateFields = null)
		{
			try
			{
				dbMaritalStatus = dbMaritalStatus.MapUpdateValues<MaritalStatus>(inputMaritalStatus, skipUpdateFields);
				base.Update(dbMaritalStatus);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

