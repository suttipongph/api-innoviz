using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface IChequeTableRepo
    {
        #region DropDown
        IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItem(SearchParameter search);
        IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItemStatus(SearchParameter search);
        IEnumerable<SelectItem<ChequeTableItemView>>  GetDropDownItemBy(SearchParameter search);
        #endregion DropDown
        SearchResult<ChequeTableListView> GetListvw(SearchParameter search);
        SearchResult<ChequeTableListView> GetListvwByLine(SearchParameter search);
        ChequeTableItemView GetByIdvw(Guid id);
        ChequeTable Find(params object[] keyValues);
        ChequeTable GetChequeTableByIdNoTracking(Guid guid);
        ChequeTable CreateChequeTable(ChequeTable chequeTable);
        void CreateChequeTableVoid(ChequeTable chequeTable);
        ChequeTable UpdateChequeTable(ChequeTable dbChequeTable, ChequeTable inputChequeTable, List<string> skipUpdateFields = null);
        void UpdateChequeTableVoid(ChequeTable dbChequeTable, ChequeTable inputChequeTable, List<string> skipUpdateFields = null);
        void Remove(ChequeTable item);
        ChequeTable GetChequeTableByIdNoTrackingByAccessLevel(Guid guid);
        ChequeTableItemView GetChequeTableInitialData(Guid id);
        ChequeTableItemView GetChequeTableInitialDataByPurchase(Guid id);
        ChequeTableItemView GetChequeTableInitialDataByWithdrawal(Guid id);
        public IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownByStatusItem(SearchParameter search, string statusCode);
        IEnumerable<ChequeTable> GetChequeTableByPurchaseLineCustomerPDCTable(Guid companyGUID, Guid purchaseTableGUID);
        IEnumerable<ChequeTable> GetChequeTableByWithdrawalLineTypePrincipal(Guid companyGUID, Guid withdrawalTableGUID);
        IEnumerable<SelectItem<ChequeTableItemView>> GetChequeRefPDC(int reftype, Guid customerGuid, Guid buyerGuid);
        void ValidateAdd(ChequeTable item);
        void ValidateAdd(IEnumerable<ChequeTable> items);
    }
    public class ChequeTableRepo : CompanyBaseRepository<ChequeTable>, IChequeTableRepo
    {
        public ChequeTableRepo(SmartAppDbContext context) : base(context) { }
        public ChequeTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public ChequeTable GetChequeTableByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.ChequeTableGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        private IQueryable<ChequeTableItemViewMap> GetDropDownQuery()
        {
            return (from chequeTable in Entity
                    join documentStatus in db.Set<DocumentStatus>()
                    on chequeTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
                    select new ChequeTableItemViewMap
                    {
                        CompanyGUID = chequeTable.CompanyGUID,
                        Owner = chequeTable.Owner,
                        OwnerBusinessUnitGUID = chequeTable.OwnerBusinessUnitGUID,
                        ChequeTableGUID = chequeTable.ChequeTableGUID,
                        ChequeNo = chequeTable.ChequeNo,
                        ChequeDate = chequeTable.ChequeDate,
                        CustomerTableGUID = chequeTable.CustomerTableGUID,
                        BuyerTableGUID = chequeTable.BuyerTableGUID,
                        RefType = chequeTable.RefType,
                        DocumentStatusGUID = chequeTable.DocumentStatusGUID,
                        DocumentStatus_StatusId = documentStatus.StatusId,
                        RefGUID = chequeTable.RefGUID,
                        ReceivedFrom = chequeTable.ReceivedFrom,
                        ChequeBankGroupGUID = chequeTable.ChequeBankGroupGUID,
                        ChequeBranch = chequeTable.ChequeBranch,
                        Amount = chequeTable.Amount
                    });
        }

        public IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<ChequeTable>(search, SysParm.CompanyGUID);
                var chequeTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
                    .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                    .Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
                    .ToMaps<ChequeTableItemViewMap, ChequeTableItemView>().ToDropDownItem(search.ExcludeRowData);
                return chequeTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItemStatus(SearchParameter search)
        {
            var predicate = base.GetFilterLevelPredicate<ChequeTable>(search, SysParm.CompanyGUID);
            var chequeTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<ChequeTableItemViewMap, ChequeTableItemView>().ToDropDownItem(search.ExcludeRowData);


            return chequeTable;
        }
        #endregion DropDown
        #region DropDownByStatus
        private IQueryable<ChequeTableItemViewMap> GetDropDownByStatusQuery(string statusCode)
        {
            return (from chequeTable in Entity
                    join status in db.Set<DocumentStatus>()
                    on chequeTable.DocumentStatusGUID equals status.DocumentStatusGUID into ljchequeTable
                    from status in ljchequeTable.DefaultIfEmpty()
                    where status.StatusId == statusCode
                    select new ChequeTableItemViewMap
                    {
                        CompanyGUID = chequeTable.CompanyGUID,
                        Owner = chequeTable.Owner,
                        OwnerBusinessUnitGUID = chequeTable.OwnerBusinessUnitGUID,
                        ChequeTableGUID = chequeTable.ChequeTableGUID,
                        ChequeNo = chequeTable.ChequeNo,
                        ChequeDate = chequeTable.ChequeDate,
                        CustomerTableGUID = chequeTable.CustomerTableGUID,
                        BuyerTableGUID = chequeTable.BuyerTableGUID,
                        RefType = chequeTable.RefType,
                        DocumentStatus_StatusId = status.StatusId,
                        DocumentStatusGUID = status.DocumentStatusGUID,
                        DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(status.Description),
                    });
        }
        public IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownByStatusItem(SearchParameter search, string statusCode)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<ChequeTable>(search, SysParm.CompanyGUID);
                var chequeTable = GetDropDownByStatusQuery(statusCode).Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<ChequeTableItemViewMap, ChequeTableItemView>().ToDropDownItem(search.ExcludeRowData);


                return chequeTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #region DropDownBy
        private IQueryable<ChequeTableItemViewMap> GetDropDownByQuery()
        {
            return (from chequeTable in Entity
                    join status in db.Set<DocumentStatus>()
                    on chequeTable.DocumentStatusGUID equals status.DocumentStatusGUID into ljchequeTable
                    from status in ljchequeTable.DefaultIfEmpty()
                    select new ChequeTableItemViewMap
                    {
                        CompanyGUID = chequeTable.CompanyGUID,
                        Owner = chequeTable.Owner,
                        OwnerBusinessUnitGUID = chequeTable.OwnerBusinessUnitGUID,
                        ChequeTableGUID = chequeTable.ChequeTableGUID,
                        ChequeNo = chequeTable.ChequeNo,
                        ChequeDate = chequeTable.ChequeDate,
                        CustomerTableGUID = chequeTable.CustomerTableGUID,
                        BuyerTableGUID = chequeTable.BuyerTableGUID,
                        RefType = chequeTable.RefType,
                        DocumentStatus_StatusId = status.StatusId,
                        DocumentStatusGUID = status.DocumentStatusGUID,
                        DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(status.Description),
                        ChequeBankGroupGUID = chequeTable.ChequeBankGroupGUID,
                        ChequeBranch = chequeTable.ChequeBranch,
                        ChequeBankAccNo = chequeTable.ChequeBankAccNo,
                        RecipientName = chequeTable.RecipientName,
                        Amount = chequeTable.Amount
                    });
        }
        public IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItemBy(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<ChequeTable>(search, SysParm.CompanyGUID);
                var chequeTable = GetDropDownByQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<ChequeTableItemViewMap, ChequeTableItemView>().ToDropDownItem(search.ExcludeRowData);


                return chequeTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDownBy


        #region GetListvw
        private IQueryable<ChequeTableListViewMap> GetListQuery()
        {
            var x = (from chequeTable in Entity

                     join customerTable in db.Set<CustomerTable>()
                     on chequeTable.CustomerTableGUID equals customerTable.CustomerTableGUID into ljchequeTable
                     from customerTable in ljchequeTable.DefaultIfEmpty()

                     join buyerTable in db.Set<BuyerTable>()
                     on chequeTable.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
                     from buyerTable in ljbuyerTable.DefaultIfEmpty()

                     join documentStatus in db.Set<DocumentStatus>()
                     on chequeTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus
                     from documentStatus in ljdocumentStatus.DefaultIfEmpty()


                     select new ChequeTableListViewMap
                     {
                         CompanyGUID = chequeTable.CompanyGUID,
                         Owner = chequeTable.Owner,
                         OwnerBusinessUnitGUID = chequeTable.OwnerBusinessUnitGUID,
                         ChequeTableGUID = chequeTable.ChequeTableGUID,
                         CustomerTableGUID = chequeTable.CustomerTableGUID,
                         DocumentStatusGUID = chequeTable.DocumentStatusGUID,
                         BuyerTableGUID = chequeTable.BuyerTableGUID,
                         ChequeDate = chequeTable.ChequeDate,
                         ChequeNo = chequeTable.ChequeNo,
                         ChequeBankAccNo = chequeTable.ChequeBankAccNo,
                         PDC = chequeTable.PDC,
                         Amount = chequeTable.Amount,
                         IssuedName = chequeTable.IssuedName,
                         RecipientName = chequeTable.RecipientName,
                         CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                         BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
                         DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
                         DocumentStatus_StatusCode = documentStatus.StatusCode,
                         CustomerTable_CustomerId = customerTable.CustomerId,
                         RefGUID = chequeTable.RefGUID,
                         RefType = chequeTable.RefType,
                         DocumentStatus_StatusId = documentStatus.StatusId
                     });
            return x;
        }
        public SearchResult<ChequeTableListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<ChequeTableListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<ChequeTable>(search, SysParm.CompanyGUID);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<ChequeTableListViewMap, ChequeTableListView>();
                result = list.SetSearchResult<ChequeTableListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        private IQueryable<ChequeTableListViewMap> GetListQueryByLine()
        {
            var x= (from chequeTable in Entity

                    join customerTable in db.Set<CustomerTable>()
                    on chequeTable.CustomerTableGUID equals customerTable.CustomerTableGUID into ljchequeTable
                    from customerTable in ljchequeTable.DefaultIfEmpty()

                    join buyerTable in db.Set<BuyerTable>()
                    on chequeTable.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
                    from buyerTable in ljbuyerTable.DefaultIfEmpty()

                    join documentStatus in db.Set<DocumentStatus>()
                    on chequeTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus
                    from documentStatus in ljdocumentStatus.DefaultIfEmpty()

                    join purchaseLine_CustomerPDC in db.Set<PurchaseLine>()
                    on chequeTable.ChequeTableGUID equals purchaseLine_CustomerPDC.CustomerPDCTableGUID into ljcustomerPDC
                    from purchaseLine_CustomerPDC in ljcustomerPDC.DefaultIfEmpty()

                    join purchaseLine_BuyerPDC in db.Set<PurchaseLine>()
                   on chequeTable.ChequeTableGUID equals purchaseLine_BuyerPDC.BuyerPDCTableGUID into ljBuyerPDC
                    from purchaseLine_BuyerPDC in ljBuyerPDC.DefaultIfEmpty()

                    join withdrawalLine_CustomerPDC in db.Set<WithdrawalLine>()
                    on chequeTable.ChequeTableGUID equals withdrawalLine_CustomerPDC.CustomerPDCTableGUID into ljwithdrawalLineCustomerPDC
                    from withdrawalLine_CustomerPDC in ljwithdrawalLineCustomerPDC.DefaultIfEmpty()

                    join withdrawalLine_BuyerPDC in db.Set<WithdrawalLine>()
                    on chequeTable.ChequeTableGUID equals withdrawalLine_BuyerPDC.BuyerPDCTableGUID into ljwithdrawalLineBuyerPDC
                    from withdrawalLine_BuyerPDC in ljwithdrawalLineBuyerPDC.DefaultIfEmpty()

                    select new ChequeTableListViewMap
                    {
                        CompanyGUID = chequeTable.CompanyGUID,
                        Owner = chequeTable.Owner,
                        OwnerBusinessUnitGUID = chequeTable.OwnerBusinessUnitGUID,
                        ChequeTableGUID = chequeTable.ChequeTableGUID,
                        CustomerTableGUID = chequeTable.CustomerTableGUID,
                        DocumentStatusGUID = chequeTable.DocumentStatusGUID,
                        BuyerTableGUID = chequeTable.BuyerTableGUID,
                        ChequeDate = chequeTable.ChequeDate,
                        ChequeNo = chequeTable.ChequeNo,
                        ChequeBankAccNo = chequeTable.ChequeBankAccNo,
                        PDC = chequeTable.PDC,
                        Amount = chequeTable.Amount,
                        IssuedName = chequeTable.IssuedName,
                        RecipientName = chequeTable.RecipientName,
                        CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                        BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
                        DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
                        DocumentStatus_StatusCode = documentStatus.StatusCode,
                        CustomerTable_CustomerId = customerTable.CustomerId,
                        RefGUID = chequeTable.RefGUID,
                        RefType = chequeTable.RefType,
                        DocumentStatus_StatusId = documentStatus.StatusId,
                        PurchaseLine_Value =(purchaseLine_BuyerPDC!=null )?purchaseLine_BuyerPDC.PurchaseLineGUID:
                                             (purchaseLine_CustomerPDC!=null)?purchaseLine_CustomerPDC.PurchaseLineGUID :
                                             new Guid(),
                        WithdrawalLine_Value = (withdrawalLine_BuyerPDC != null) ? withdrawalLine_BuyerPDC.WithdrawalLineGUID :
                                             (withdrawalLine_CustomerPDC != null) ? withdrawalLine_CustomerPDC.WithdrawalLineGUID :
                                             new Guid()
                    });
            return x;
        }
        public SearchResult<ChequeTableListView> GetListvwByLine(SearchParameter search)
        {
            var result = new SearchResult<ChequeTableListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<ChequeTable>(search, SysParm.CompanyGUID);
                var total = GetListQueryByLine().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQueryByLine().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<ChequeTableListViewMap, ChequeTableListView>();
                result = list.SetSearchResult<ChequeTableListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<ChequeTableItemViewMap> GetItemQuery()
        {
            return (from chequeTable in Entity
                    join company in db.Set<Company>()
                    on chequeTable.CompanyGUID equals company.CompanyGUID
                    join ownerBU in db.Set<BusinessUnit>()
                    on chequeTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljChequeTableOwnerBU
                    from ownerBU in ljChequeTableOwnerBU
                    join documentStatus in db.Set<DocumentStatus>()
                    on chequeTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus
                    from documentStatus in ljdocumentStatus.DefaultIfEmpty()
                    join purchaseTable in db.Set<PurchaseTable>()
                    on chequeTable.RefGUID equals purchaseTable.PurchaseTableGUID into ljpurchaseTable
                    from purchaseTable in ljpurchaseTable.DefaultIfEmpty()
                    join chequeTable2 in db.Set<ChequeTable>()
                    on chequeTable.RefPDCGUID equals chequeTable2.ChequeTableGUID into ljchequeTable2
                    from chequeTable2 in ljchequeTable2.DefaultIfEmpty()
                    join withdrawalTable in db.Set<WithdrawalTable>()
                    on chequeTable.RefGUID equals withdrawalTable.WithdrawalTableGUID into ljwithdrawalTable
                    from withdrawalTable in ljwithdrawalTable.DefaultIfEmpty()

                    join customerTable in db.Set<CustomerTable>()
                    on chequeTable.CustomerTableGUID equals customerTable.CustomerTableGUID into ljcustomer 
                    from customerTable in ljcustomer.DefaultIfEmpty()

                    join buyerTable in db.Set<BuyerTable>()
                    on chequeTable.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
                    from buyerTable in ljbuyerTable.DefaultIfEmpty()

                    join chequeBankGroup in db.Set<BankGroup>()
                    on chequeTable.ChequeBankGroupGUID equals chequeBankGroup.BankGroupGUID into ljchequeBankGroup
                    from chequeBankGroup in ljchequeBankGroup.DefaultIfEmpty()
                    select new ChequeTableItemViewMap
                    {
                        CompanyGUID = chequeTable.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = chequeTable.Owner,
                        OwnerBusinessUnitGUID = chequeTable.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        CreatedBy = chequeTable.CreatedBy,
                        CreatedDateTime = chequeTable.CreatedDateTime,
                        ModifiedBy = chequeTable.ModifiedBy,
                        ModifiedDateTime = chequeTable.ModifiedDateTime,
                        ChequeTableGUID = chequeTable.ChequeTableGUID,
                        Amount = chequeTable.Amount,
                        BuyerTableGUID = chequeTable.BuyerTableGUID,
                        ChequeBankAccNo = chequeTable.ChequeBankAccNo,
                        ChequeBankGroupGUID = chequeTable.ChequeBankGroupGUID,
                        ChequeBranch = chequeTable.ChequeBranch,
                        ChequeDate = chequeTable.ChequeDate,
                        ChequeNo = chequeTable.ChequeNo,
                        CompletedDate = chequeTable.CompletedDate,
                        CustomerTableGUID = chequeTable.CustomerTableGUID,
                        DocumentStatusGUID = chequeTable.DocumentStatusGUID,
                        ExpectedDepositDate = chequeTable.ExpectedDepositDate,
                        IssuedName = chequeTable.IssuedName,
                        PDC = chequeTable.PDC,
                        ReceivedFrom = chequeTable.ReceivedFrom,
                        RecipientName = chequeTable.RecipientName,
                        RefGUID = chequeTable.RefGUID,
                        RefID = withdrawalTable != null? withdrawalTable.WithdrawalId:
                        (chequeTable!=null? SmartAppUtil.GetDropDownLabel(purchaseTable.PurchaseId,purchaseTable.Description):""),
                        RefPDCGUID = chequeTable.RefPDCGUID,
                        RefType = chequeTable.RefType,
                        Remark = chequeTable.Remark,
                        DocumentStatus_StatusId = documentStatus.StatusId,
                        DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
                        PurchaseTable_Value = SmartAppUtil.GetDropDownLabel(purchaseTable.PurchaseId, purchaseTable.Description),
                        RefPDC_Values = SmartAppUtil.GetDropDownLabel(chequeTable2.ChequeNo, chequeTable2.ChequeDate.ToString()),
                        CustomerTable_Value = SmartAppUtil.GetDropDownLabel(customerTable.Name),
                        BuyerTable_Value = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId) ,
                        ChequeBankGroup_Value = SmartAppUtil.GetDropDownLabel(chequeBankGroup.BankGroupId),
                    
                        RowVersion = chequeTable.RowVersion,
                    });
        }
        public ChequeTableItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<ChequeTableItemViewMap, ChequeTableItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public ChequeTable CreateChequeTable(ChequeTable chequeTable)
        {
            try
            {
                chequeTable.ChequeTableGUID = Guid.NewGuid();
                base.Add(chequeTable);
                return chequeTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateChequeTableVoid(ChequeTable chequeTable)
        {
            try
            {
                CreateChequeTable(chequeTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ChequeTable UpdateChequeTable(ChequeTable dbChequeTable, ChequeTable inputChequeTable, List<string> skipUpdateFields = null)
        {
            try
            {
                dbChequeTable = dbChequeTable.MapUpdateValues<ChequeTable>(inputChequeTable);
                base.Update(dbChequeTable);
                return dbChequeTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateChequeTableVoid(ChequeTable dbChequeTable, ChequeTable inputChequeTable, List<string> skipUpdateFields = null)
        {
            try
            {
                dbChequeTable = dbChequeTable.MapUpdateValues<ChequeTable>(inputChequeTable, skipUpdateFields);
                base.Update(dbChequeTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Create, Update, Delete
        public ChequeTable GetChequeTableByIdNoTrackingByAccessLevel(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.ChequeTableGUID == guid)
                    .FilterByAccessLevel(SysParm.AccessLevel)
                    .AsNoTracking()
                    .FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
            public ChequeTableItemView GetChequeTableInitialDataByWithdrawal(Guid id)
        {
            try
            {

                var result = (from withdrawalTable in db.Set<WithdrawalTable>()
                              where withdrawalTable.WithdrawalTableGUID == id
                              select new WithdrawalTable
                              {
                                  WithdrawalTableGUID = withdrawalTable.WithdrawalTableGUID,
                                  WithdrawalId = withdrawalTable.WithdrawalId,
                                  BuyerTableGUID = withdrawalTable.BuyerTableGUID,
                                  CustomerTableGUID = withdrawalTable.CustomerTableGUID
                              }
                              ).FirstOrDefault();
                var temp = (
                    from documentStatus in db.Set<DocumentStatus>()
                    where documentStatus.StatusId == ((int)ChequeDocumentStatus.Created).ToString()
                    select new ChequeTableItemView
                    {
                        DocumentStatusGUID = documentStatus.DocumentStatusGUID.ToString(),
                        document_StatusId = documentStatus.StatusId

                    }).FirstOrDefault();
                return new ChequeTableItemView
                {
                    DocumentStatusGUID = temp.DocumentStatusGUID,
                    document_StatusId = temp.document_StatusId,
                    RefGUID = id.ToString(),
                    RefID = result.WithdrawalId,
                    RefType = (int)RefType.WithdrawalTable,
                    BuyerTableGUID = result.BuyerTableGUID.GuidNullToString(),
                    CustomerTableGUID = result.CustomerTableGUID.GuidNullToString()
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ChequeTableItemView GetChequeTableInitialDataByPurchase(Guid id)
        {
            return (
                    from purchaseTable in db.Set<PurchaseTable>()
                    where purchaseTable.PurchaseTableGUID == id
                    select new ChequeTableItemView
                    {
                        RefType = (int)RefType.PurchaseTable,
                        PurchaseTable_Value = SmartAppUtil.GetDropDownLabel(purchaseTable.PurchaseId, purchaseTable.Description),
                        RefGUID = purchaseTable.PurchaseTableGUID.ToString(),
                        RefID = purchaseTable.PurchaseId

                    }).FirstOrDefault();
        }
       
        public ChequeTableItemView GetChequeTableInitialData(Guid id)
        {
            try
            {
                var temp = (
                    from documentStatus in db.Set<DocumentStatus>()
                    where documentStatus.StatusId == ((int)ChequeDocumentStatus.Created).ToString()
                    select new ChequeTableItemView
                    {
                        DocumentStatusGUID = documentStatus.DocumentStatusGUID.ToString(),
                        document_StatusId = documentStatus.StatusId

                    }).FirstOrDefault();
                return new ChequeTableItemView
                {
                    DocumentStatusGUID = temp.DocumentStatusGUID,
                    document_StatusId = temp.document_StatusId,
                    RefType = (int)RefType.Cheque
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }

        public IEnumerable<ChequeTable> GetChequeTableByPurchaseLineCustomerPDCTable(Guid companyGUID, Guid purchaseTableGUID)
        {
            try
            {
                return (from chequeTable in Entity
                        join purchaseLine in db.Set<PurchaseLine>()
                        on chequeTable.ChequeTableGUID equals purchaseLine.CustomerPDCTableGUID
                        where purchaseLine.PurchaseTableGUID == purchaseTableGUID && chequeTable.CompanyGUID == companyGUID
                        select chequeTable
                        ).AsNoTracking();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public IEnumerable<ChequeTable> GetChequeTableByWithdrawalLineTypePrincipal(Guid companyGUID, Guid withdrawalTableGUID)
        {
            try
            {
                return (from chequeTable in Entity
                        join withdrawalLine in db.Set<WithdrawalLine>()
                        on chequeTable.ChequeTableGUID equals withdrawalLine.CustomerPDCTableGUID
                        where withdrawalLine.WithdrawalTableGUID == withdrawalTableGUID && chequeTable.CompanyGUID == companyGUID && withdrawalLine.WithdrawalLineType == (int)WithdrawalLineType.Principal
                        select chequeTable
                        ).AsNoTracking();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<ChequeTableItemView>> GetChequeRefPDC(int reftype, Guid customerGuid, Guid buyerGuid)
        {
            try
            {
                IQueryable<ChequeTableItemViewMap> temp = (from chequeTable in Entity
                                                           join documentStatus in db.Set<DocumentStatus>()
                                                           on chequeTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus
                                                           from documentStatus in ljdocumentStatus.DefaultIfEmpty()
                                                           join customerTable in db.Set<CustomerTable>()
                                                           on chequeTable.CustomerTableGUID equals customerTable.CustomerTableGUID into ljcustomerTable
                                                           from customerTable in ljcustomerTable.DefaultIfEmpty()
                                                           join buyerTable in db.Set<BuyerTable>()
                                                           on chequeTable.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
                                                           from buyerTable in ljbuyerTable.DefaultIfEmpty()
                                                           where documentStatus.StatusId == ((int)ChequeDocumentStatus.Replaced).ToString()
                                                              && customerTable.CustomerTableGUID == customerGuid
                                                              && buyerTable.BuyerTableGUID == buyerGuid
                                                              && chequeTable.RefType == reftype
                                                           select new ChequeTableItemViewMap
                                                           {
                                                               CompanyGUID = chequeTable.CompanyGUID,
                                                               Owner = chequeTable.Owner,
                                                               OwnerBusinessUnitGUID = chequeTable.OwnerBusinessUnitGUID,
                                                               ChequeTableGUID = chequeTable.ChequeTableGUID,
                                                               ChequeNo = chequeTable.ChequeNo,
                                                               ChequeDate = chequeTable.ChequeDate,
                                                               CustomerTableGUID = chequeTable.CustomerTableGUID,
                                                               BuyerTableGUID = chequeTable.BuyerTableGUID,
                                                               RefType = chequeTable.RefType,
                                                               DocumentStatusGUID = chequeTable.DocumentStatusGUID,
                                                               DocumentStatus_StatusId = documentStatus.StatusId,
                                                               RefGUID = chequeTable.RefGUID,
                                                               ReceivedFrom = chequeTable.ReceivedFrom,
                                                               RefPDCGUID = chequeTable.RefPDCGUID,
                                                               RefPDC_Values = SmartAppUtil.GetDropDownLabel(chequeTable.ChequeNo, chequeTable.ChequeDate.ToString())
                                                           });
                var Temp_ChequePDC = temp.ToMaps<ChequeTableItemViewMap, ChequeTableItemView>();

                return Temp_ChequePDC.ToDropDownItem();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}

