using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICustomerRefundTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CustomerRefundTableItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<CustomerRefundTableListView> GetListvw(SearchParameter search);
		CustomerRefundTableItemView GetByIdvw(Guid id);
		CustomerRefundTable Find(params object[] keyValues);
		CustomerRefundTable GetCustomerRefundTableByIdNoTracking(Guid guid);
		CustomerRefundTable CreateCustomerRefundTable(CustomerRefundTable customerRefundTable);
		void CreateCustomerRefundTableVoid(CustomerRefundTable customerRefundTable);
		CustomerRefundTable UpdateCustomerRefundTable(CustomerRefundTable dbCustomerRefundTable, CustomerRefundTable inputCustomerRefundTable, List<string> skipUpdateFields = null);
		void UpdateCustomerRefundTableVoid(CustomerRefundTable dbCustomerRefundTable, CustomerRefundTable inputCustomerRefundTable, List<string> skipUpdateFields = null);
		void Remove(CustomerRefundTable item);
		CustomerRefundTable GetCustomerRefundTableByIdNoTrackingByAccessLevel(Guid guid);
		List<CustomerRefundTable> GetCustomerRefundTableByIdNoTracking(List<Guid> customerRefundTableGuids);
	}
	public class CustomerRefundTableRepo : CompanyBaseRepository<CustomerRefundTable>, ICustomerRefundTableRepo
	{
		public CustomerRefundTableRepo(SmartAppDbContext context) : base(context) { }
		public CustomerRefundTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CustomerRefundTable GetCustomerRefundTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CustomerRefundTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CustomerRefundTableItemViewMap> GetDropDownQuery()
		{
			return (from customerRefundTable in Entity
					select new CustomerRefundTableItemViewMap
					{
						CompanyGUID = customerRefundTable.CompanyGUID,
						Owner = customerRefundTable.Owner,
						OwnerBusinessUnitGUID = customerRefundTable.OwnerBusinessUnitGUID,
						CustomerRefundTableGUID = customerRefundTable.CustomerRefundTableGUID,
						CustomerRefundId = customerRefundTable.CustomerRefundId,
						Description = customerRefundTable.Description
					});
		}
		public IEnumerable<SelectItem<CustomerRefundTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CustomerRefundTable>(search, SysParm.CompanyGUID);
				var customerRefundTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CustomerRefundTableItemViewMap, CustomerRefundTableItemView>().ToDropDownItem(search.ExcludeRowData);
				return customerRefundTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CustomerRefundTableListViewMap> GetListQuery()
		{
			return (from customerRefundTable in Entity
					join documentStatus in db.Set<DocumentStatus>()
					on customerRefundTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus
					from documentStatus in ljdocumentStatus.DefaultIfEmpty()
					join currency in db.Set<Currency>()
					on customerRefundTable.CurrencyGUID equals currency.CurrencyGUID into ljcurrency
					from currency in ljcurrency.DefaultIfEmpty()
					select new CustomerRefundTableListViewMap
					{
						CompanyGUID = customerRefundTable.CompanyGUID,
						Owner = customerRefundTable.Owner,
						OwnerBusinessUnitGUID = customerRefundTable.OwnerBusinessUnitGUID,
						CustomerRefundTableGUID = customerRefundTable.CustomerRefundTableGUID,
						CustomerRefundId = customerRefundTable.CustomerRefundId,
						Description = customerRefundTable.Description,
						TransDate = customerRefundTable.TransDate,
						DocumentStatusGUID = customerRefundTable.DocumentStatusGUID,
						PaymentAmount = customerRefundTable.PaymentAmount,
						SuspenseAmount = customerRefundTable.SuspenseAmount,
						CurrencyGUID = customerRefundTable.CurrencyGUID,
						DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
						DocumentStatus_StatusId = documentStatus.StatusId,
						Currency_Values = SmartAppUtil.GetDropDownLabel(currency.CurrencyId, currency.Name),
						Currency_CurrencyId = currency.CurrencyId,
						SuspenseInvoiceType = customerRefundTable.SuspenseInvoiceType,
						NetAmount = customerRefundTable.SuspenseAmount - customerRefundTable.ServiceFeeAmount - customerRefundTable.SettleAmount,
					});
		}
		public SearchResult<CustomerRefundTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CustomerRefundTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CustomerRefundTable>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CustomerRefundTableListViewMap, CustomerRefundTableListView>();
				result = list.SetSearchResult<CustomerRefundTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CustomerRefundTableItemViewMap> GetItemQuery()
		{
			return (from customerRefundTable in Entity
					join company in db.Set<Company>()
					on customerRefundTable.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on customerRefundTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCustomerRefundTableOwnerBU
					from ownerBU in ljCustomerRefundTableOwnerBU.DefaultIfEmpty()
					join documentStatus in db.Set<DocumentStatus>() on customerRefundTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
					join documentReason in db.Set<DocumentReason>()
					on customerRefundTable.DocumentReasonGUID equals documentReason.DocumentReasonGUID into ljDocumentReason
					from documentReason in ljDocumentReason.DefaultIfEmpty()
					select new CustomerRefundTableItemViewMap
					{
						CompanyGUID = customerRefundTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = customerRefundTable.Owner,
						OwnerBusinessUnitGUID = customerRefundTable.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = customerRefundTable.CreatedBy,
						CreatedDateTime = customerRefundTable.CreatedDateTime,
						ModifiedBy = customerRefundTable.ModifiedBy,
						ModifiedDateTime = customerRefundTable.ModifiedDateTime,
						CustomerRefundTableGUID = customerRefundTable.CustomerRefundTableGUID,
						CreditAppTableGUID = customerRefundTable.CreditAppTableGUID,
						CurrencyGUID = customerRefundTable.CurrencyGUID,
						CustomerRefundId = customerRefundTable.CustomerRefundId,
						CustomerTableGUID = customerRefundTable.CustomerTableGUID,
						Description = customerRefundTable.Description,
						Dimension1GUID = customerRefundTable.Dimension1GUID,
						Dimension2GUID = customerRefundTable.Dimension2GUID,
						Dimension3GUID = customerRefundTable.Dimension3GUID,
						Dimension4GUID = customerRefundTable.Dimension4GUID,
						Dimension5GUID = customerRefundTable.Dimension5GUID,
						DocumentReasonGUID = customerRefundTable.DocumentReasonGUID,
						DocumentStatusGUID = customerRefundTable.DocumentStatusGUID,
						ExchangeRate = customerRefundTable.ExchangeRate,
						OperReportSignatureGUID = customerRefundTable.OperReportSignatureGUID,
						PaymentAmount = customerRefundTable.PaymentAmount,
						ProductType = customerRefundTable.ProductType,
						RetentionAmount = customerRefundTable.RetentionAmount,
						RetentionCalculateBase = customerRefundTable.RetentionCalculateBase,
						RetentionPct = customerRefundTable.RetentionPct,
						ServiceFeeAmount = customerRefundTable.ServiceFeeAmount,
						SettleAmount = customerRefundTable.SettleAmount,
						SuspenseAmount = customerRefundTable.SuspenseAmount,
						SuspenseInvoiceType = customerRefundTable.SuspenseInvoiceType,
						TransDate = customerRefundTable.TransDate,
						DocumentStatus_StatusId = documentStatus.StatusId,
						NetAmount = customerRefundTable.SuspenseAmount - customerRefundTable.ServiceFeeAmount - customerRefundTable.SettleAmount,
						DocumentReason_Values = (documentReason != null) ? SmartAppUtil.GetDropDownLabel(documentReason.ReasonId, documentReason.Description) : string.Empty,
						Remark = customerRefundTable.Remark,
					
						RowVersion = customerRefundTable.RowVersion,
					});
		}
		public CustomerRefundTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<CustomerRefundTableItemViewMap, CustomerRefundTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public CustomerRefundTable CreateCustomerRefundTable(CustomerRefundTable customerRefundTable)
		{
			try
			{
				customerRefundTable.CustomerRefundTableGUID = Guid.NewGuid();
				base.Add(customerRefundTable);
				return customerRefundTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCustomerRefundTableVoid(CustomerRefundTable customerRefundTable)
		{
			try
			{
				CreateCustomerRefundTable(customerRefundTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CustomerRefundTable UpdateCustomerRefundTable(CustomerRefundTable dbCustomerRefundTable, CustomerRefundTable inputCustomerRefundTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCustomerRefundTable = dbCustomerRefundTable.MapUpdateValues<CustomerRefundTable>(inputCustomerRefundTable);
				base.Update(dbCustomerRefundTable);
				return dbCustomerRefundTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCustomerRefundTableVoid(CustomerRefundTable dbCustomerRefundTable, CustomerRefundTable inputCustomerRefundTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCustomerRefundTable = dbCustomerRefundTable.MapUpdateValues<CustomerRefundTable>(inputCustomerRefundTable, skipUpdateFields);
				base.Update(dbCustomerRefundTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public CustomerRefundTable GetCustomerRefundTableByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CustomerRefundTableGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<CustomerRefundTable> GetCustomerRefundTableByIdNoTracking(List<Guid> customerRefundTableGuids)
		{
			try
			{
				var result = Entity.Where(item => customerRefundTableGuids.Contains(item.CustomerRefundTableGUID)).AsNoTracking().ToList();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

