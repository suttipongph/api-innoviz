using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IBuyerReceiptTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<BuyerReceiptTableItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<BuyerReceiptTableListView> GetListvw(SearchParameter search);
		BuyerReceiptTableItemView GetByIdvw(Guid id);
		BuyerReceiptTableItemView GetBuyerReceiptInitialPurchaseLineData(Guid refGUID, int refType);
		BuyerReceiptTableItemView GetBuyerReceiptInitialWithdrawalLineData(Guid refGUID, int refType);
		BuyerReceiptTable Find(params object[] keyValues);
		BuyerReceiptTable GetBuyerReceiptTableByIdNoTracking(Guid guid);
		BuyerReceiptTable CreateBuyerReceiptTable(BuyerReceiptTable buyerReceiptTable);
		void CreateBuyerReceiptTableVoid(BuyerReceiptTable buyerReceiptTable);
		BuyerReceiptTable UpdateBuyerReceiptTable(BuyerReceiptTable dbBuyerReceiptTable, BuyerReceiptTable inputBuyerReceiptTable, List<string> skipUpdateFields = null);
		void UpdateBuyerReceiptTableVoid(BuyerReceiptTable dbBuyerReceiptTable, BuyerReceiptTable inputBuyerReceiptTable, List<string> skipUpdateFields = null);
		void Remove(BuyerReceiptTable item);
		BuyerReceiptTable GetBuyerReceiptTableByIdNoTrackingByAccessLevel(Guid guid);
	}
	public class BuyerReceiptTableRepo : CompanyBaseRepository<BuyerReceiptTable>, IBuyerReceiptTableRepo
	{
		public BuyerReceiptTableRepo(SmartAppDbContext context) : base(context) { }
		public BuyerReceiptTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BuyerReceiptTable GetBuyerReceiptTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BuyerReceiptTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<BuyerReceiptTableItemViewMap> GetDropDownQuery()
		{
			return (from buyerReceiptTable in Entity
					select new BuyerReceiptTableItemViewMap
					{
						CompanyGUID = buyerReceiptTable.CompanyGUID,
						Owner = buyerReceiptTable.Owner,
						OwnerBusinessUnitGUID = buyerReceiptTable.OwnerBusinessUnitGUID,
						BuyerReceiptTableGUID = buyerReceiptTable.BuyerReceiptTableGUID,
						BuyerReceiptId = buyerReceiptTable.BuyerReceiptId,
						BuyerReceiptDate = buyerReceiptTable.BuyerReceiptDate,
						CustomerTableGUID = buyerReceiptTable.CustomerTableGUID,
						BuyerTableGUID = buyerReceiptTable.BuyerTableGUID,
						Cancel = buyerReceiptTable.Cancel
					});
		}
		public IEnumerable<SelectItem<BuyerReceiptTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<BuyerReceiptTable>(search, SysParm.CompanyGUID);
				var buyerReceiptTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<BuyerReceiptTableItemViewMap, BuyerReceiptTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return buyerReceiptTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<BuyerReceiptTableListViewMap> GetListQuery()
		{
			return (from buyerReceiptTable in Entity
					join buyerTable in db.Set<BuyerTable>()
					on buyerReceiptTable.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
					from buyerTable in ljbuyerTable.DefaultIfEmpty()

					join customerTable in db.Set<CustomerTable>()
					on buyerReceiptTable.CustomerTableGUID equals customerTable.CustomerTableGUID into ljcustomerTable
					from customerTable in ljcustomerTable.DefaultIfEmpty()
				select new BuyerReceiptTableListViewMap
				{
						CompanyGUID = buyerReceiptTable.CompanyGUID,
						Owner = buyerReceiptTable.Owner,
						OwnerBusinessUnitGUID = buyerReceiptTable.OwnerBusinessUnitGUID,
						BuyerReceiptTableGUID = buyerReceiptTable.BuyerReceiptTableGUID,
						BuyerReceiptId = buyerReceiptTable.BuyerReceiptId,
						BuyerReceiptDate = buyerReceiptTable.BuyerReceiptDate,
						BuyerTableGUID = buyerReceiptTable.BuyerTableGUID,
						CustomerTableGUID = buyerReceiptTable.CustomerTableGUID,
						BuyerReceiptAmount = buyerReceiptTable.BuyerReceiptAmount,
						Cancel = buyerReceiptTable.Cancel,
						BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId,buyerTable.BuyerName),
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId,customerTable.Name),
						RefGUID = buyerReceiptTable.RefGUID,
						RefType = buyerReceiptTable.RefType,
					BuyerTable_BuyerId = buyerTable.BuyerId,
					CustomerTable_CustomerId = customerTable.CustomerId
				});
		}
		public SearchResult<BuyerReceiptTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<BuyerReceiptTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<BuyerReceiptTable>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<BuyerReceiptTableListViewMap, BuyerReceiptTableListView>();
				result = list.SetSearchResult<BuyerReceiptTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<BuyerReceiptTableItemViewMap> GetItemQuery()
		{
			return (from buyerReceiptTable in Entity
					join company in db.Set<Company>()
					on buyerReceiptTable.CompanyGUID equals company.CompanyGUID 

					join ownerBU in db.Set<BusinessUnit>()
					on buyerReceiptTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljBuyerReceiptTableOwnerBU
					from ownerBU in ljBuyerReceiptTableOwnerBU.DefaultIfEmpty()

					join buyerTable in db.Set<BuyerTable>()
					on buyerReceiptTable.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
					from buyerTable in ljbuyerTable.DefaultIfEmpty()

					join customerTable in db.Set<CustomerTable>()
					on buyerReceiptTable.CustomerTableGUID equals customerTable.CustomerTableGUID into ljcustomerTable
					from customerTable in ljcustomerTable.DefaultIfEmpty()

					join assignmentAgreement in db.Set<AssignmentAgreementTable>()
					on buyerReceiptTable.AssignmentAgreementTableGUID equals assignmentAgreement.AssignmentAgreementTableGUID into ljAssignment
					from assignmentAgreement in ljAssignment.DefaultIfEmpty()

					join buyerAgreement in db.Set<BuyerAgreementTable>()
					on buyerReceiptTable.BuyerAgreementTableGUID equals buyerAgreement.BuyerAgreementTableGUID into ljBuyerAgreement
					from buyerAgreement in ljBuyerAgreement.DefaultIfEmpty()

					join methodOfPayment in db.Set<MethodOfPayment>()
					on buyerReceiptTable.MethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID into ljmethodOfPayment
					from methodOfPayment in ljmethodOfPayment.DefaultIfEmpty()

					join companyBank in db.Set<CompanyBank>()
					on methodOfPayment.CompanyBankGUID equals companyBank.CompanyBankGUID into ljcompanyBank
					from companyBank in ljcompanyBank.DefaultIfEmpty()

					join collectionFollowUp in db.Set<CollectionFollowUp>()
					on buyerReceiptTable.RefGUID equals collectionFollowUp.CollectionFollowUpGUID into ljcollectionFollowUp
					from collectionFollowUp in ljcollectionFollowUp.DefaultIfEmpty()

					select new BuyerReceiptTableItemViewMap
					{
						CompanyGUID = buyerReceiptTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = buyerReceiptTable.Owner,
						OwnerBusinessUnitGUID = buyerReceiptTable.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = buyerReceiptTable.CreatedBy,
						CreatedDateTime = buyerReceiptTable.CreatedDateTime,
						ModifiedBy = buyerReceiptTable.ModifiedBy,
						ModifiedDateTime = buyerReceiptTable.ModifiedDateTime,
						BuyerReceiptTableGUID = buyerReceiptTable.BuyerReceiptTableGUID,
						AssignmentAgreementTableGUID = buyerReceiptTable.AssignmentAgreementTableGUID,
						BuyerAgreementTableGUID = buyerReceiptTable.BuyerAgreementTableGUID,
						BuyerReceiptAddress = buyerReceiptTable.BuyerReceiptAddress,
						BuyerReceiptAmount = buyerReceiptTable.BuyerReceiptAmount,
						BuyerReceiptDate = buyerReceiptTable.BuyerReceiptDate,
						BuyerReceiptId = buyerReceiptTable.BuyerReceiptId,
						BuyerTableGUID = buyerReceiptTable.BuyerTableGUID,
						Cancel = buyerReceiptTable.Cancel,
						ChequeDate = buyerReceiptTable.ChequeDate,
						ChequeNo = buyerReceiptTable.ChequeNo,
						CustomerTableGUID = buyerReceiptTable.CustomerTableGUID,
						MethodOfPaymentGUID = buyerReceiptTable.MethodOfPaymentGUID,
						RefGUID = buyerReceiptTable.RefGUID,
						RefType = buyerReceiptTable.RefType,
						Remark = buyerReceiptTable.Remark,
						ShowRemarkOnly = buyerReceiptTable.ShowRemarkOnly,
						BuyerTable_Values =SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId,buyerTable.BuyerName),
						CustomerTable_Values=SmartAppUtil.GetDropDownLabel(customerTable.CustomerId,customerTable.Name),
						AssignmentAgreementTable_Values = SmartAppUtil.GetDropDownLabel(assignmentAgreement.InternalAssignmentAgreementId,assignmentAgreement.Description),
						BuyerAgreementTable_Values =SmartAppUtil.GetDropDownLabel(buyerAgreement.ReferenceAgreementID,buyerAgreement.Description),
						CompanyBankId = companyBank.BankAccountName,
						refId = SmartAppUtil.GetDropDownLabel(collectionFollowUp.CollectionDate.DateToString(), collectionFollowUp.Description),
						BuyerTable_BuyerId = buyerTable.BuyerId,
						CustomerTable_CustomerId = customerTable.CustomerId,
					
						RowVersion = buyerReceiptTable.RowVersion,
					});
		}
		public BuyerReceiptTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<BuyerReceiptTableItemViewMap, BuyerReceiptTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public BuyerReceiptTable CreateBuyerReceiptTable(BuyerReceiptTable buyerReceiptTable)
		{
			try
			{
				buyerReceiptTable.BuyerReceiptTableGUID = Guid.NewGuid();
				base.Add(buyerReceiptTable);
				return buyerReceiptTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateBuyerReceiptTableVoid(BuyerReceiptTable buyerReceiptTable)
		{
			try
			{
				CreateBuyerReceiptTable(buyerReceiptTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BuyerReceiptTable UpdateBuyerReceiptTable(BuyerReceiptTable dbBuyerReceiptTable, BuyerReceiptTable inputBuyerReceiptTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBuyerReceiptTable = dbBuyerReceiptTable.MapUpdateValues<BuyerReceiptTable>(inputBuyerReceiptTable);
				base.Update(dbBuyerReceiptTable);
				return dbBuyerReceiptTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateBuyerReceiptTableVoid(BuyerReceiptTable dbBuyerReceiptTable, BuyerReceiptTable inputBuyerReceiptTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBuyerReceiptTable = dbBuyerReceiptTable.MapUpdateValues<BuyerReceiptTable>(inputBuyerReceiptTable, skipUpdateFields);
				base.Update(dbBuyerReceiptTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public BuyerReceiptTable GetBuyerReceiptTableByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BuyerReceiptTableGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        public BuyerReceiptTableItemView GetBuyerReceiptInitialPurchaseLineData(Guid guid, int refType)
        {
			try
			{
				return (from collectionFollowUp in db.Set<CollectionFollowUp>()
							  join creditAppline in db.Set<CreditAppLine>()
							  on collectionFollowUp.CreditAppLineGUID equals creditAppline.CreditAppLineGUID into ljcreditAppline
							  from creditAppline in ljcreditAppline.DefaultIfEmpty()

							  join buyerTable in db.Set<BuyerTable>()
							  on collectionFollowUp.BuyerTableGUID equals buyerTable.BuyerTableGUID  into ljbuyerTable
							  from buyerTable in ljbuyerTable.DefaultIfEmpty()

							  join customerTable in db.Set<CustomerTable>()
							  on collectionFollowUp.CustomerTableGUID equals customerTable.CustomerTableGUID into ljcustomerTable
							  from customerTable in ljcustomerTable.DefaultIfEmpty()

							  join addressTrans in db.Set<AddressTrans>()
							  on creditAppline.ReceiptAddressGUID equals addressTrans.AddressTransGUID into ljaddressTrans
							  from addressTrans in ljaddressTrans.DefaultIfEmpty()

							  join purchaseLine in db.Set<PurchaseLine>()
							  on collectionFollowUp.RefGUID equals purchaseLine.PurchaseLineGUID into ljpurchaseLine
							  from purchaseLine in ljpurchaseLine.DefaultIfEmpty()

							  join assignmentAgreemantTable in db.Set<AssignmentAgreementTable>()
							  on purchaseLine.AssignmentAgreementTableGUID equals assignmentAgreemantTable.AssignmentAgreementTableGUID into ljassignmentAgreemantTable
							  from assignmentAgreemantTable in ljassignmentAgreemantTable.DefaultIfEmpty()

							  join buyerAgreementTable in db.Set<BuyerAgreementTable>()
							  on purchaseLine.BuyerAgreementTableGUID equals buyerAgreementTable.BuyerAgreementTableGUID into ljbuyerAgreementTable
							  from buyerAgreementTable in ljbuyerAgreementTable.DefaultIfEmpty()

							  join methodOfPayment in db.Set<MethodOfPayment>()
							  on purchaseLine.MethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID into ljmethodOfPayment
							  from methodOfPayment in ljmethodOfPayment.DefaultIfEmpty()

							  join companyBank in db.Set<CompanyBank>()
							  on methodOfPayment.CompanyBankGUID equals companyBank.CompanyBankGUID into ljcompanyBank
							  from companyBank in ljcompanyBank.DefaultIfEmpty()
							  where collectionFollowUp.CollectionFollowUpGUID == guid
							  select new BuyerReceiptTableItemView
							  {
								  BuyerReceiptDate = collectionFollowUp.CollectionDate.DateToString(),
								  RefType = refType,
								  RefGUID = collectionFollowUp.CollectionFollowUpGUID.ToString(),
								  BuyerTableGUID = collectionFollowUp.BuyerTableGUID.ToString(),
								  CustomerTableGUID = collectionFollowUp.CustomerTableGUID.ToString(),
								  BuyerReceiptAddress = addressTrans != null ? addressTrans.Address1 + " " + addressTrans.Address2 : null,
								  ChequeNo = collectionFollowUp.ChequeNo,
								  ChequeDate = collectionFollowUp.ChequeDate.DateNullToString(),
								  BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId,buyerTable.BuyerName),
								  CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId,customerTable.Name),
								  RefId = SmartAppUtil.GetDropDownLabel(collectionFollowUp.CollectionDate.DateToString(), collectionFollowUp.Description),
								  AssignmentAgreementTableGUID = purchaseLine != null ? purchaseLine.AssignmentAgreementTableGUID.GuidNullToString() : null,
								  MethodOfPaymentGUID = purchaseLine != null ? purchaseLine.MethodOfPaymentGUID.GuidNullToString() : null,
								  BuyerAgreementTableGUID = purchaseLine != null ? purchaseLine.BuyerAgreementTableGUID.GuidNullToString() : null,
								  AssignmentAgreementTable_Values = (assignmentAgreemantTable!=null) ? SmartAppUtil.GetDropDownLabel(assignmentAgreemantTable.InternalAssignmentAgreementId, assignmentAgreemantTable.Description) : null,
								  BuyerAgreementTable_Values = (buyerAgreementTable!=null) ? SmartAppUtil.GetDropDownLabel(buyerAgreementTable.BuyerAgreementId, buyerAgreementTable.Description):null,
								  MethodOfPayment_Values = (methodOfPayment !=null) ? SmartAppUtil.GetDropDownLabel(methodOfPayment.MethodOfPaymentId, methodOfPayment.Description) : null,
								  CompanyBankId = companyBank.BankAccountName,
							  }).AsNoTracking().FirstOrDefault();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BuyerReceiptTableItemView GetBuyerReceiptInitialWithdrawalLineData(Guid guid, int refType)
		{
			try
			{
				return (from collectionFollowUp in db.Set<CollectionFollowUp>()
						join creditAppline in db.Set<CreditAppLine>()
						on collectionFollowUp.CreditAppLineGUID equals creditAppline.CreditAppLineGUID into ljcreditAppline
						from creditAppline in ljcreditAppline.DefaultIfEmpty()

						join buyerTable in db.Set<BuyerTable>()
						on collectionFollowUp.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
						from buyerTable in ljbuyerTable.DefaultIfEmpty()

						join customerTable in db.Set<CustomerTable>()
						on collectionFollowUp.CustomerTableGUID equals customerTable.CustomerTableGUID into ljcustomerTable
						from customerTable in ljcustomerTable.DefaultIfEmpty()

						join addressTrans in db.Set<AddressTrans>()
						  on creditAppline.ReceiptAddressGUID equals addressTrans.AddressTransGUID into ljaddressTrans
						from addressTrans in ljaddressTrans.DefaultIfEmpty()

						join withdrawalLine in db.Set<WithdrawalLine>()
						on collectionFollowUp.RefGUID equals withdrawalLine.WithdrawalLineGUID into ljwithdrawalLine
						from withdrawalLine in ljwithdrawalLine.DefaultIfEmpty()

						join withdrawalTable in db.Set<WithdrawalTable>()
						on withdrawalLine.WithdrawalTableGUID equals withdrawalTable.WithdrawalTableGUID into ljwithdrawalTable
						from withdrawalTable in ljwithdrawalTable.DefaultIfEmpty()

						join assignmentAgreemantTable in db.Set<AssignmentAgreementTable>()
						on withdrawalTable.AssignmentAgreementTableGUID equals assignmentAgreemantTable.AssignmentAgreementTableGUID into ljassignmentAgreemantTable
						from assignmentAgreemantTable in ljassignmentAgreemantTable.DefaultIfEmpty()

						join buyerAgreementTable in db.Set<BuyerAgreementTable>()
						on withdrawalTable.BuyerAgreementTableGUID equals buyerAgreementTable.BuyerAgreementTableGUID into ljbuyerAgreementTable
						from buyerAgreementTable in ljbuyerAgreementTable.DefaultIfEmpty()

						join methodOfPayment in db.Set<MethodOfPayment>()
						on withdrawalTable.MethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID into ljmethodOfPayment
						from methodOfPayment in ljmethodOfPayment.DefaultIfEmpty()

						join companyBank in db.Set<CompanyBank>()
						on methodOfPayment.CompanyBankGUID equals companyBank.CompanyBankGUID into ljcompanyBank
						from companyBank in ljcompanyBank.DefaultIfEmpty()
						where collectionFollowUp.CollectionFollowUpGUID == guid
						select new BuyerReceiptTableItemView
						{
							BuyerReceiptDate = collectionFollowUp.CollectionDate.DateToString(),
							RefType = refType,
							RefGUID = collectionFollowUp.CollectionFollowUpGUID.ToString(),
							BuyerTableGUID = collectionFollowUp.BuyerTableGUID.ToString(),
							CustomerTableGUID = collectionFollowUp.CustomerTableGUID.ToString(),
							BuyerReceiptAddress = addressTrans != null ? addressTrans.Address1 + " " + addressTrans.Address2 : null,
							ChequeNo = collectionFollowUp.ChequeNo,
							ChequeDate = collectionFollowUp.ChequeDate.DateNullToString(),
							BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
							CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
							RefId = SmartAppUtil.GetDropDownLabel(collectionFollowUp.CollectionDate.DateToString(), collectionFollowUp.Description),
							AssignmentAgreementTableGUID = withdrawalTable != null ? withdrawalTable.AssignmentAgreementTableGUID.GuidNullToString() : "",
							MethodOfPaymentGUID = withdrawalTable != null ? withdrawalTable.MethodOfPaymentGUID.GuidNullToString() : "",
							BuyerAgreementTableGUID = withdrawalTable != null ? withdrawalTable.BuyerAgreementTableGUID.GuidNullToString() : "",
							AssignmentAgreementTable_Values = (assignmentAgreemantTable != null) ? SmartAppUtil.GetDropDownLabel(assignmentAgreemantTable.InternalAssignmentAgreementId, assignmentAgreemantTable.Description) : null,
							BuyerAgreementTable_Values = (buyerAgreementTable != null) ? SmartAppUtil.GetDropDownLabel(buyerAgreementTable.BuyerAgreementId, buyerAgreementTable.Description) : null,
							MethodOfPayment_Values = (methodOfPayment != null) ? SmartAppUtil.GetDropDownLabel(methodOfPayment.MethodOfPaymentId, methodOfPayment.Description) : null,
							CompanyBankId = companyBank.BankAccountName,
						}).AsNoTracking().FirstOrDefault();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

