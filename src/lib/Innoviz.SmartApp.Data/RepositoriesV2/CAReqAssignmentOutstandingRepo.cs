using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICAReqAssignmentOutstandingRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CAReqAssignmentOutstandingItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<CAReqAssignmentOutstandingListView> GetListvw(SearchParameter search);
		CAReqAssignmentOutstandingItemView GetByIdvw(Guid id);
		CAReqAssignmentOutstanding Find(params object[] keyValues);
		CAReqAssignmentOutstanding GetCAReqAssignmentOutstandingByIdNoTracking(Guid guid);
		CAReqAssignmentOutstanding CreateCAReqAssignmentOutstanding(CAReqAssignmentOutstanding caReqAssignmentOutstanding);
		void CreateCAReqAssignmentOutstandingVoid(CAReqAssignmentOutstanding caReqAssignmentOutstanding);
		CAReqAssignmentOutstanding UpdateCAReqAssignmentOutstanding(CAReqAssignmentOutstanding dbCAReqAssignmentOutstanding, CAReqAssignmentOutstanding inputCAReqAssignmentOutstanding, List<string> skipUpdateFields = null);
		void UpdateCAReqAssignmentOutstandingVoid(CAReqAssignmentOutstanding dbCAReqAssignmentOutstanding, CAReqAssignmentOutstanding inputCAReqAssignmentOutstanding, List<string> skipUpdateFields = null);
		void Remove(CAReqAssignmentOutstanding item);
		CAReqAssignmentOutstanding GetCAReqAssignmentOutstandingByIdNoTrackingByAccessLevel(Guid guid);
	}
	public class CAReqAssignmentOutstandingRepo : CompanyBaseRepository<CAReqAssignmentOutstanding>, ICAReqAssignmentOutstandingRepo
	{
		public CAReqAssignmentOutstandingRepo(SmartAppDbContext context) : base(context) { }
		public CAReqAssignmentOutstandingRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CAReqAssignmentOutstanding GetCAReqAssignmentOutstandingByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CAReqAssignmentOutstandingGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CAReqAssignmentOutstandingItemViewMap> GetDropDownQuery()
		{
			return (from caReqAssignmentOutstanding in Entity
					select new CAReqAssignmentOutstandingItemViewMap
					{
						CompanyGUID = caReqAssignmentOutstanding.CompanyGUID,
						Owner = caReqAssignmentOutstanding.Owner,
						OwnerBusinessUnitGUID = caReqAssignmentOutstanding.OwnerBusinessUnitGUID,
						CAReqAssignmentOutstandingGUID = caReqAssignmentOutstanding.CAReqAssignmentOutstandingGUID
					});
		}
		public IEnumerable<SelectItem<CAReqAssignmentOutstandingItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CAReqAssignmentOutstanding>(search, SysParm.CompanyGUID);
				var caReqAssignmentOutstanding = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CAReqAssignmentOutstandingItemViewMap, CAReqAssignmentOutstandingItemView>().ToDropDownItem(search.ExcludeRowData);


				return caReqAssignmentOutstanding;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CAReqAssignmentOutstandingListViewMap> GetListQuery()
		{
			return (from caReqAssignmentOutstanding in Entity
					join customerTable in db.Set<CustomerTable>() 
					on caReqAssignmentOutstanding.CustomerTableGUID equals customerTable.CustomerTableGUID into ljCustomerTable
					from customerTable in ljCustomerTable.DefaultIfEmpty()
					join buyerTable in db.Set<BuyerTable>()
					on caReqAssignmentOutstanding.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljBuyerTable
					from buyerTable in ljBuyerTable.DefaultIfEmpty()
					join assignmentAgreementTable in db.Set<AssignmentAgreementTable>()
					on caReqAssignmentOutstanding.AssignmentAgreementTableGUID equals assignmentAgreementTable.AssignmentAgreementTableGUID into ljAssignmentAgreementTable
					from assignmentAgreementTable in ljAssignmentAgreementTable.DefaultIfEmpty()
					select new CAReqAssignmentOutstandingListViewMap
					{
						CompanyGUID = caReqAssignmentOutstanding.CompanyGUID,
						Owner = caReqAssignmentOutstanding.Owner,
						OwnerBusinessUnitGUID = caReqAssignmentOutstanding.OwnerBusinessUnitGUID,
						CAReqAssignmentOutstandingGUID = caReqAssignmentOutstanding.CAReqAssignmentOutstandingGUID,
						CustomerTableGUID = caReqAssignmentOutstanding.CustomerTableGUID,
						AssignmentAgreementTableGUID = caReqAssignmentOutstanding.AssignmentAgreementTableGUID,
						BuyerTableGUID = caReqAssignmentOutstanding.BuyerTableGUID,
						AssignmentAgreementAmount = caReqAssignmentOutstanding.AssignmentAgreementAmount,
						SettleAmount = caReqAssignmentOutstanding.SettleAmount,
						RemainingAmount = caReqAssignmentOutstanding.RemainingAmount,
						CreditAppRequestTableGUID = caReqAssignmentOutstanding.CreditAppRequestTableGUID,
						CustomerTable_Values = (customerTable != null) ? SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name) : string.Empty,
						BuyerTable_Values = (buyerTable != null) ? SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName) : string.Empty,
						AssignmentAgreement_Values = (assignmentAgreementTable != null) ? SmartAppUtil.GetDropDownLabel(assignmentAgreementTable.InternalAssignmentAgreementId, assignmentAgreementTable.Description) : string.Empty,
						CustomerTable_CustomerId = (customerTable != null) ? customerTable.CustomerId : string.Empty,
						BuyerTable_BuyerId = (buyerTable != null) ? buyerTable.BuyerId : string.Empty,
						AssignmentAgreement_InternalAssignmentAgreementId = (assignmentAgreementTable != null) ? assignmentAgreementTable.InternalAssignmentAgreementId : string.Empty,
					});
		}
		public SearchResult<CAReqAssignmentOutstandingListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CAReqAssignmentOutstandingListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CAReqAssignmentOutstanding>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.DistinctBy(b => b.CAReqAssignmentOutstandingGUID)
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CAReqAssignmentOutstandingListViewMap, CAReqAssignmentOutstandingListView>();
				list = list.DistinctBy(b => b.CAReqAssignmentOutstandingGUID).ToList();
				result = list.SetSearchResult<CAReqAssignmentOutstandingListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CAReqAssignmentOutstandingItemViewMap> GetItemQuery()
		{
			return (from caReqAssignmentOutstanding in Entity
					join company in db.Set<Company>()
					on caReqAssignmentOutstanding.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on caReqAssignmentOutstanding.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCAReqAssignmentOutstandingOwnerBU
					from ownerBU in ljCAReqAssignmentOutstandingOwnerBU.DefaultIfEmpty()
					select new CAReqAssignmentOutstandingItemViewMap
					{
						CompanyGUID = caReqAssignmentOutstanding.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = caReqAssignmentOutstanding.Owner,
						OwnerBusinessUnitGUID = caReqAssignmentOutstanding.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = caReqAssignmentOutstanding.CreatedBy,
						CreatedDateTime = caReqAssignmentOutstanding.CreatedDateTime,
						ModifiedBy = caReqAssignmentOutstanding.ModifiedBy,
						ModifiedDateTime = caReqAssignmentOutstanding.ModifiedDateTime,
						CAReqAssignmentOutstandingGUID = caReqAssignmentOutstanding.CAReqAssignmentOutstandingGUID,
						AssignmentAgreementAmount = caReqAssignmentOutstanding.AssignmentAgreementAmount,
						AssignmentAgreementTableGUID = caReqAssignmentOutstanding.AssignmentAgreementTableGUID,
						BuyerTableGUID = caReqAssignmentOutstanding.BuyerTableGUID,
						CreditAppRequestTableGUID = caReqAssignmentOutstanding.CreditAppRequestTableGUID,
						CustomerTableGUID = caReqAssignmentOutstanding.CustomerTableGUID,
						RemainingAmount = caReqAssignmentOutstanding.RemainingAmount,
						SettleAmount = caReqAssignmentOutstanding.SettleAmount,
					
						RowVersion = caReqAssignmentOutstanding.RowVersion,
					});
		}
		public CAReqAssignmentOutstandingItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<CAReqAssignmentOutstandingItemViewMap, CAReqAssignmentOutstandingItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public CAReqAssignmentOutstanding CreateCAReqAssignmentOutstanding(CAReqAssignmentOutstanding caReqAssignmentOutstanding)
		{
			try
			{
				caReqAssignmentOutstanding.CAReqAssignmentOutstandingGUID = Guid.NewGuid();
				base.Add(caReqAssignmentOutstanding);
				return caReqAssignmentOutstanding;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCAReqAssignmentOutstandingVoid(CAReqAssignmentOutstanding caReqAssignmentOutstanding)
		{
			try
			{
				CreateCAReqAssignmentOutstanding(caReqAssignmentOutstanding);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CAReqAssignmentOutstanding UpdateCAReqAssignmentOutstanding(CAReqAssignmentOutstanding dbCAReqAssignmentOutstanding, CAReqAssignmentOutstanding inputCAReqAssignmentOutstanding, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCAReqAssignmentOutstanding = dbCAReqAssignmentOutstanding.MapUpdateValues<CAReqAssignmentOutstanding>(inputCAReqAssignmentOutstanding);
				base.Update(dbCAReqAssignmentOutstanding);
				return dbCAReqAssignmentOutstanding;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCAReqAssignmentOutstandingVoid(CAReqAssignmentOutstanding dbCAReqAssignmentOutstanding, CAReqAssignmentOutstanding inputCAReqAssignmentOutstanding, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCAReqAssignmentOutstanding = dbCAReqAssignmentOutstanding.MapUpdateValues<CAReqAssignmentOutstanding>(inputCAReqAssignmentOutstanding, skipUpdateFields);
				base.Update(dbCAReqAssignmentOutstanding);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public CAReqAssignmentOutstanding GetCAReqAssignmentOutstandingByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CAReqAssignmentOutstandingGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

