using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IAddressCountryRepo
	{
		#region DropDown
		IEnumerable<SelectItem<AddressCountryItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<AddressCountryListView> GetListvw(SearchParameter search);
		AddressCountryItemView GetByIdvw(Guid id);
		AddressCountry Find(params object[] keyValues);
		AddressCountry GetAddressCountryByIdNoTracking(Guid guid);
		AddressCountry CreateAddressCountry(AddressCountry addressCountry);
		void CreateAddressCountryVoid(AddressCountry addressCountry);
		AddressCountry UpdateAddressCountry(AddressCountry dbAddressCountry, AddressCountry inputAddressCountry, List<string> skipUpdateFields = null);
		void UpdateAddressCountryVoid(AddressCountry dbAddressCountry, AddressCountry inputAddressCountry, List<string> skipUpdateFields = null);
		void Remove(AddressCountry item);
		List<AddressCountry> GetAddressCountryByCompanyNoTracking(Guid companyGUID);
	}
	public class AddressCountryRepo : CompanyBaseRepository<AddressCountry>, IAddressCountryRepo
	{
		public AddressCountryRepo(SmartAppDbContext context) : base(context) { }
		public AddressCountryRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public AddressCountry GetAddressCountryByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.AddressCountryGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<AddressCountryItemViewMap> GetDropDownQuery()
		{
			return (from addressCountry in Entity
					select new AddressCountryItemViewMap
					{
						CompanyGUID = addressCountry.CompanyGUID,
						Owner = addressCountry.Owner,
						OwnerBusinessUnitGUID = addressCountry.OwnerBusinessUnitGUID,
						AddressCountryGUID = addressCountry.AddressCountryGUID,
						CountryId = addressCountry.CountryId,
						Name = addressCountry.Name
					});
		}
		public IEnumerable<SelectItem<AddressCountryItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<AddressCountry>(search, SysParm.CompanyGUID);
				var addressCountry = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<AddressCountryItemViewMap, AddressCountryItemView>().ToDropDownItem(search.ExcludeRowData);


				return addressCountry;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<AddressCountryListViewMap> GetListQuery()
		{
			return (from addressCountry in Entity
				select new AddressCountryListViewMap
				{
						CompanyGUID = addressCountry.CompanyGUID,
						Owner = addressCountry.Owner,
						OwnerBusinessUnitGUID = addressCountry.OwnerBusinessUnitGUID,
						AddressCountryGUID = addressCountry.AddressCountryGUID,
						CountryId = addressCountry.CountryId,
						Name = addressCountry.Name,
				});
		}
		public SearchResult<AddressCountryListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<AddressCountryListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<AddressCountry>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<AddressCountryListViewMap, AddressCountryListView>();
				result = list.SetSearchResult<AddressCountryListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<AddressCountryItemViewMap> GetItemQuery()
		{
			return (from addressCountry in Entity
					join company in db.Set<Company>()
					on addressCountry.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on addressCountry.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljAddressCountryOwnerBU
					from ownerBU in ljAddressCountryOwnerBU.DefaultIfEmpty()
					select new AddressCountryItemViewMap
					{
						CompanyGUID = addressCountry.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = addressCountry.Owner,
						OwnerBusinessUnitGUID = addressCountry.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = addressCountry.CreatedBy,
						CreatedDateTime = addressCountry.CreatedDateTime,
						ModifiedBy = addressCountry.ModifiedBy,
						ModifiedDateTime = addressCountry.ModifiedDateTime,
						AddressCountryGUID = addressCountry.AddressCountryGUID,
						CountryId = addressCountry.CountryId,
						Name = addressCountry.Name,
					
						RowVersion = addressCountry.RowVersion,
					});
		}
		public AddressCountryItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<AddressCountryItemViewMap, AddressCountryItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public AddressCountry CreateAddressCountry(AddressCountry addressCountry)
		{
			try
			{
				addressCountry.AddressCountryGUID = Guid.NewGuid();
				base.Add(addressCountry);
				return addressCountry;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateAddressCountryVoid(AddressCountry addressCountry)
		{
			try
			{
				CreateAddressCountry(addressCountry);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AddressCountry UpdateAddressCountry(AddressCountry dbAddressCountry, AddressCountry inputAddressCountry, List<string> skipUpdateFields = null)
		{
			try
			{
				dbAddressCountry = dbAddressCountry.MapUpdateValues<AddressCountry>(inputAddressCountry);
				base.Update(dbAddressCountry);
				return dbAddressCountry;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateAddressCountryVoid(AddressCountry dbAddressCountry, AddressCountry inputAddressCountry, List<string> skipUpdateFields = null)
		{
			try
			{
				dbAddressCountry = dbAddressCountry.MapUpdateValues<AddressCountry>(inputAddressCountry, skipUpdateFields);
				base.Update(dbAddressCountry);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public List<AddressCountry> GetAddressCountryByCompanyNoTracking(Guid companyGUID)
		{
			try
			{
				List<AddressCountry> addressCountries = Entity.Where(w => w.CompanyGUID == companyGUID).AsNoTracking().ToList();
				return addressCountries;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

