using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface ICompanyBankRepo
    {
        #region DropDown
        IEnumerable<SelectItem<CompanyBankItemView>> GetDropDownItem(SearchParameter search);
        #endregion DropDown
        SearchResult<CompanyBankListView> GetListvw(SearchParameter search);
        CompanyBankItemView GetByIdvw(Guid id);
        CompanyBank Find(params object[] keyValues);
        CompanyBank GetCompanyBankByIdNoTracking(Guid guid);
        CompanyBank CreateCompanyBank(CompanyBank companyBank);
        void CreateCompanyBankVoid(CompanyBank companyBank);
        CompanyBank UpdateCompanyBank(CompanyBank dbCompanyBank, CompanyBank inputCompanyBank, List<string> skipUpdateFields = null);
        void UpdateCompanyBankVoid(CompanyBank dbCompanyBank, CompanyBank inputCompanyBank, List<string> skipUpdateFields = null);
        void Remove(CompanyBank item);
        CompanyBank GetPrimaryCompanyBankByCompanyNoTracking(Guid guid);
    }
    public class CompanyBankRepo : CompanyBaseRepository<CompanyBank>, ICompanyBankRepo
    {
        public CompanyBankRepo(SmartAppDbContext context) : base(context) { }
        public CompanyBankRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public CompanyBank GetCompanyBankByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.CompanyBankGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        private IQueryable<CompanyBankItemViewMap> GetDropDownQuery()
        {
            return (from companyBank in Entity
                    join bankGroup in db.Set<BankGroup>()
                    on companyBank.BankGroupGUID equals bankGroup.BankGroupGUID
                    select new CompanyBankItemViewMap
                    {
                        CompanyGUID = companyBank.CompanyGUID,
                        Owner = companyBank.Owner,
                        OwnerBusinessUnitGUID = companyBank.OwnerBusinessUnitGUID,
                        CompanyBankGUID = companyBank.CompanyBankGUID,
                        BankGroupGUID = companyBank.BankGroupGUID,
                        AccountNumber = companyBank.AccountNumber,
                        BankAccountName = companyBank.BankAccountName,
                        BankGroup_BankGroupId = bankGroup.BankGroupId
                    });
        }
        public IEnumerable<SelectItem<CompanyBankItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<CompanyBank>(search, SysParm.CompanyGUID);
                var companyBank = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CompanyBankItemViewMap, CompanyBankItemView>().ToDropDownItem(search.ExcludeRowData);


                return companyBank;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #region GetListvw
        private IQueryable<CompanyBankListViewMap> GetListQuery()
        {
            return (from companyBank in Entity
                    join bankGroup in db.Set<BankGroup>()
                    on companyBank.BankGroupGUID equals bankGroup.BankGroupGUID into ljBankGroup
                    from bankGroup in ljBankGroup.DefaultIfEmpty()
                    join bankType in db.Set<BankType>()
                    on companyBank.BankTypeGUID equals bankType.BankTypeGUID into ljBankType
                    from bankType in ljBankType.DefaultIfEmpty()
                    select new CompanyBankListViewMap
                    {
                        CompanyGUID = companyBank.CompanyGUID,
                        Owner = companyBank.Owner,
                        OwnerBusinessUnitGUID = companyBank.OwnerBusinessUnitGUID,
                        CompanyBankGUID = companyBank.CompanyBankGUID,
                        BankGroupGUID = companyBank.BankGroupGUID,
                        BankTypeGUID = companyBank.BankTypeGUID,
                        AccountNumber = companyBank.AccountNumber,
                        BankAccountName = companyBank.BankAccountName,
                        Primary = companyBank.Primary,
                        InActive = companyBank.InActive,
                        BankGroup_BankGroupId = bankGroup.BankGroupId,
                        BankType_BankTypeId = bankGroup.BankGroupId,
                        BankGroup_Values = SmartAppUtil.GetDropDownLabel(bankGroup.BankGroupId, bankGroup.Description),
                        BankType_Values = SmartAppUtil.GetDropDownLabel(bankType.BankTypeId, bankType.Description)
                    });
        }
        public SearchResult<CompanyBankListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<CompanyBankListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<CompanyBank>(search, SysParm.CompanyGUID);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<CompanyBankListViewMap, CompanyBankListView>();
                result = list.SetSearchResult<CompanyBankListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<CompanyBankItemViewMap> GetItemQuery()
        {
            return (from companyBank in Entity
                    join company in db.Set<Company>()
                    on companyBank.CompanyGUID equals company.CompanyGUID
                    join ownerBU in db.Set<BusinessUnit>()
                    on companyBank.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCompanyBankOwnerBU
                    from ownerBU in ljCompanyBankOwnerBU.DefaultIfEmpty()
                    select new CompanyBankItemViewMap
                    {
                        CompanyGUID = companyBank.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = companyBank.Owner,
                        OwnerBusinessUnitGUID = companyBank.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        CreatedBy = companyBank.CreatedBy,
                        CreatedDateTime = companyBank.CreatedDateTime,
                        ModifiedBy = companyBank.ModifiedBy,
                        ModifiedDateTime = companyBank.ModifiedDateTime,
                        CompanyBankGUID = companyBank.CompanyBankGUID,
                        AccountNumber = companyBank.AccountNumber,
                        BankAccountName = companyBank.BankAccountName,
                        BankBranch = companyBank.BankBranch,
                        BankGroupGUID = companyBank.BankGroupGUID,
                        BankTypeGUID = companyBank.BankTypeGUID,
                        InActive = companyBank.InActive,
                        Primary = companyBank.Primary,
                    
                        RowVersion = companyBank.RowVersion,
                    });
        }
        public CompanyBankItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<CompanyBankItemViewMap, CompanyBankItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public CompanyBank CreateCompanyBank(CompanyBank companyBank)
        {
            try
            {
                companyBank.CompanyBankGUID = Guid.NewGuid();
                base.Add(companyBank);
                return companyBank;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateCompanyBankVoid(CompanyBank companyBank)
        {
            try
            {
                CreateCompanyBank(companyBank);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CompanyBank UpdateCompanyBank(CompanyBank dbCompanyBank, CompanyBank inputCompanyBank, List<string> skipUpdateFields = null)
        {
            try
            {
                dbCompanyBank = dbCompanyBank.MapUpdateValues<CompanyBank>(inputCompanyBank);
                base.Update(dbCompanyBank);
                return dbCompanyBank;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateCompanyBankVoid(CompanyBank dbCompanyBank, CompanyBank inputCompanyBank, List<string> skipUpdateFields = null)
        {
            try
            {
                dbCompanyBank = dbCompanyBank.MapUpdateValues<CompanyBank>(inputCompanyBank, skipUpdateFields);
                base.Update(dbCompanyBank);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Create, Update, Delete

        #region Validate
        public void CheckDuplicatePrimary(CompanyBank item)
        {
            try
            {
                SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
                bool isDupplicatePrimary = Entity.Any(a => (a.CompanyGUID == item.CompanyGUID)
                                                    && (a.Primary == true)
                                                    && (a.Primary == item.Primary)
                                                    && (a.CompanyBankGUID != item.CompanyBankGUID));
                if (isDupplicatePrimary)
                {
                    smartAppException.AddData("ERROR.DUPLICATE", new string[] { "LABEL.PRIMARY", "LABEL.COMPANY_BANK" });
                }

               bool isDupplicate = Entity.Any(a => (a.CompanyGUID == item.CompanyGUID)
                                    && (a.InActive == false)
                                    && (a.InActive == item.InActive)
                                    && (a.CompanyBankGUID != item.CompanyBankGUID)
                                    && (a.AccountNumber == item.AccountNumber)
                                    && (a.BankGroupGUID == item.BankGroupGUID)
                                    );
                if (isDupplicate)
                {
                    smartAppException.AddData("ERROR.DUPLICATE", new string[] { "LABEL.FINANCIAL_INSTITUTION_ID", "LABEL.ACCOUNT_NUMBER" });
                }

                if (smartAppException.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(smartAppException);
                }
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public override void ValidateAdd(CompanyBank item)
        {
            base.ValidateAdd(item);
            CheckDuplicatePrimary(item);
        }
        public override void ValidateUpdate(CompanyBank item)
        {
            base.ValidateUpdate(item);
            CheckDuplicatePrimary(item);
        }
        #endregion
        public CompanyBank GetPrimaryCompanyBankByCompanyNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.CompanyGUID == guid && item.Primary == true).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}

