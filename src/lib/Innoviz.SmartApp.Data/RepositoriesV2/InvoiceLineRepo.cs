using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface IInvoiceLineRepo
    {
        #region DropDown
        IEnumerable<SelectItem<InvoiceLineItemView>> GetDropDownItem(SearchParameter search);
        #endregion DropDown
        SearchResult<InvoiceLineListView> GetListvw(SearchParameter search);
        InvoiceLineItemView GetByIdvw(Guid id);
        InvoiceLine Find(params object[] keyValues);
        InvoiceLine GetInvoiceLineByIdNoTracking(Guid guid);
        InvoiceLine CreateInvoiceLine(InvoiceLine invoiceLine);
        void CreateInvoiceLineVoid(InvoiceLine invoiceLine);
        InvoiceLine UpdateInvoiceLine(InvoiceLine dbInvoiceLine, InvoiceLine inputInvoiceLine, List<string> skipUpdateFields = null);
        void UpdateInvoiceLineVoid(InvoiceLine dbInvoiceLine, InvoiceLine inputInvoiceLine, List<string> skipUpdateFields = null);
        void Remove(InvoiceLine item);
        InvoiceLine GetInvoiceLineByIdNoTrackingByAccessLevel(Guid guid);
        IEnumerable<InvoiceLine> GetInvoiceLinesByInvoiceTableGuid(Guid guid);
        List<InvoiceLine> GetInvoiceLinesByInvoiceTableGuid(IEnumerable<Guid> invoiceTableGuids);
        IEnumerable<InvoiceLine> GetInvoiceLineNotNullTaxTableByInvoiceTableGUIDNoTracking(List<Guid> invoiceTableGuids);
        #region shared
        IEnumerable<InvoiceLineItemView> GetInvoiceLineItemViewByInvoiceTableGuid(Guid invoiceTableGuid);
        #endregion
        IEnumerable<InvoiceLine> GetInvoiceLinesForGenTaxTable(IEnumerable<InvoiceLine> invoiceLines);
        void ValidateAdd(InvoiceLine item);
        void ValidateAdd(IEnumerable<InvoiceLine> items);
    }
    public class InvoiceLineRepo : BranchCompanyBaseRepository<InvoiceLine>, IInvoiceLineRepo
    {
        public InvoiceLineRepo(SmartAppDbContext context) : base(context) { }
        public InvoiceLineRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public InvoiceLine GetInvoiceLineByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.InvoiceLineGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        private IQueryable<InvoiceLineItemViewMap> GetDropDownQuery()
        {
            return (from invoiceLine in Entity
                    select new InvoiceLineItemViewMap
                    {
                        CompanyGUID = invoiceLine.CompanyGUID,
                        Owner = invoiceLine.Owner,
                        OwnerBusinessUnitGUID = invoiceLine.OwnerBusinessUnitGUID,
                        InvoiceLineGUID = invoiceLine.InvoiceLineGUID
                    });
        }
        public IEnumerable<SelectItem<InvoiceLineItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<InvoiceLine>(search, db.GetAvailableBranch(), SysParm.CompanyGUID, SysParm.BranchGUID);
                var invoiceLine = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<InvoiceLineItemViewMap, InvoiceLineItemView>().ToDropDownItem(search.ExcludeRowData);


                return invoiceLine;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #region GetListvw
        private IQueryable<InvoiceLineListViewMap> GetListQuery()
        {
            return (from invoiceLine in Entity
                    join invoiceRevenueType in db.Set<InvoiceRevenueType>() on invoiceLine.InvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID
                    join taxTable in db.Set<TaxTable>() on invoiceLine.TaxTableGUID equals taxTable.TaxTableGUID into ljTaxTable
                    from taxTable in ljTaxTable.DefaultIfEmpty()
                    select new InvoiceLineListViewMap
                    {
                        CompanyGUID = invoiceLine.CompanyGUID,
                        Owner = invoiceLine.Owner,
                        OwnerBusinessUnitGUID = invoiceLine.OwnerBusinessUnitGUID,
                        InvoiceLineGUID = invoiceLine.InvoiceLineGUID,
                        LineNum = invoiceLine.LineNum,
                        InvoiceRevenueTypeGUID = invoiceLine.InvoiceRevenueTypeGUID,
                        InvoiceText = invoiceLine.InvoiceText,
                        TotalAmountBeforeTax = invoiceLine.TotalAmountBeforeTax,
                        TaxAmount = invoiceLine.TaxAmount,
                        TotalAmount = invoiceLine.TotalAmount,
                        TaxTableGUID = invoiceLine.TaxTableGUID,
                        InvoiceRevenueType_Values = SmartAppUtil.GetDropDownLabel(invoiceRevenueType.RevenueTypeId, invoiceRevenueType.Description),
                        InvoiceRevenueType_RevenueTypeId = invoiceRevenueType.RevenueTypeId,
                        TaxTable_taxTabled = taxTable.TaxCode,
                        TaxTable_Values = SmartAppUtil.GetDropDownLabel(taxTable.TaxCode, taxTable.Description),
                        InvoiceTableGUID = invoiceLine.InvoiceTableGUID
                    });
        }
        public SearchResult<InvoiceLineListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<InvoiceLineListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<InvoiceLine>(search, db.GetAvailableBranch(), SysParm.CompanyGUID, SysParm.BranchGUID);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<InvoiceLineListViewMap, InvoiceLineListView>();
                result = list.SetSearchResult<InvoiceLineListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<InvoiceLineItemViewMap> GetItemQuery()
        {
            return (from invoiceLine in Entity
                    join company in db.Set<Company>()
                    on invoiceLine.CompanyGUID equals company.CompanyGUID
                    join branch in db.Set<Branch>()
                    on invoiceLine.BranchGUID equals branch.BranchGUID
                    join ownerBU in db.Set<BusinessUnit>()
                    on invoiceLine.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljInvoiceLineOwnerBU
                    from ownerBU in ljInvoiceLineOwnerBU.DefaultIfEmpty()
                    select new InvoiceLineItemViewMap
                    {
                        CompanyGUID = invoiceLine.CompanyGUID,
                        CompanyId = company.CompanyId,
                        BranchGUID = invoiceLine.BranchGUID,
                        BranchId = branch.BranchId,
                        Owner = invoiceLine.Owner,
                        OwnerBusinessUnitGUID = invoiceLine.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        CreatedBy = invoiceLine.CreatedBy,
                        CreatedDateTime = invoiceLine.CreatedDateTime,
                        ModifiedBy = invoiceLine.ModifiedBy,
                        ModifiedDateTime = invoiceLine.ModifiedDateTime,
                        InvoiceLineGUID = invoiceLine.InvoiceLineGUID,
                        Dimension1GUID = invoiceLine.Dimension1GUID,
                        Dimension2GUID = invoiceLine.Dimension2GUID,
                        Dimension3GUID = invoiceLine.Dimension3GUID,
                        Dimension4GUID = invoiceLine.Dimension4GUID,
                        Dimension5GUID = invoiceLine.Dimension5GUID,
                        InvoiceRevenueTypeGUID = invoiceLine.InvoiceRevenueTypeGUID,
                        InvoiceTableGUID = invoiceLine.InvoiceTableGUID,
                        InvoiceText = invoiceLine.InvoiceText,
                        LineNum = invoiceLine.LineNum,
                        ProdUnitGUID = invoiceLine.ProdUnitGUID,
                        Qty = invoiceLine.Qty,
                        TaxAmount = invoiceLine.TaxAmount,
                        TaxTableGUID = invoiceLine.TaxTableGUID,
                        TotalAmount = invoiceLine.TotalAmount,
                        TotalAmountBeforeTax = invoiceLine.TotalAmountBeforeTax,
                        UnitPrice = invoiceLine.UnitPrice,
                        WHTAmount = invoiceLine.WHTAmount,
                        WHTBaseAmount = invoiceLine.WHTBaseAmount,
                        WithholdingTaxTableGUID = invoiceLine.WithholdingTaxTableGUID,
                    
                        RowVersion = invoiceLine.RowVersion,
                    });
        }
        public InvoiceLineItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<InvoiceLineItemViewMap, InvoiceLineItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public InvoiceLine CreateInvoiceLine(InvoiceLine invoiceLine)
        {
            try
            {
                invoiceLine.InvoiceLineGUID = Guid.NewGuid();
                base.Add(invoiceLine);
                return invoiceLine;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateInvoiceLineVoid(InvoiceLine invoiceLine)
        {
            try
            {
                CreateInvoiceLine(invoiceLine);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public InvoiceLine UpdateInvoiceLine(InvoiceLine dbInvoiceLine, InvoiceLine inputInvoiceLine, List<string> skipUpdateFields = null)
        {
            try
            {
                dbInvoiceLine = dbInvoiceLine.MapUpdateValues<InvoiceLine>(inputInvoiceLine);
                base.Update(dbInvoiceLine);
                return dbInvoiceLine;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateInvoiceLineVoid(InvoiceLine dbInvoiceLine, InvoiceLine inputInvoiceLine, List<string> skipUpdateFields = null)
        {
            try
            {
                dbInvoiceLine = dbInvoiceLine.MapUpdateValues<InvoiceLine>(inputInvoiceLine, skipUpdateFields);
                base.Update(dbInvoiceLine);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Create, Update, Delete
        public InvoiceLine GetInvoiceLineByIdNoTrackingByAccessLevel(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.InvoiceLineGUID == guid)
                    .FilterByAccessLevel(SysParm.AccessLevel)
                    .AsNoTracking()
                    .FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<InvoiceLine> GetInvoiceLinesByInvoiceTableGuid(IEnumerable<Guid> invoiceTableGuids)
        {
            try
            {
                var result = Entity.Where(w => invoiceTableGuids.Contains(w.InvoiceTableGUID))
                                    .AsNoTracking()
                                    .ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region function
        public IEnumerable<InvoiceLine> GetInvoiceLinesByInvoiceTableGuid(Guid guid)
        {
            try
            {
                IEnumerable<InvoiceLine> invoiceLines = Entity.Where(w => w.InvoiceTableGUID == guid)
                    .AsNoTracking().ToList();
                return invoiceLines;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<InvoiceLine> GetInvoiceLineNotNullTaxTableByInvoiceTableGUIDNoTracking(List<Guid> invoiceTableGuids)
        {
            try
            {
                var result = Entity.Where(w => w.TaxTableGUID.HasValue && invoiceTableGuids.Contains(w.InvoiceTableGUID))
                                .AsNoTracking()
                                .ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region shared
        public IEnumerable<InvoiceLineItemView> GetInvoiceLineItemViewByInvoiceTableGuid(Guid invoiceTableGuid)
        {
            IEnumerable<InvoiceLineItemView> invoieLines = (from invoiceLine in Entity
                                                            join taxTable in db.Set<TaxTable>() on invoiceLine.TaxTableGUID equals taxTable.TaxTableGUID
                                                            where invoiceLine.InvoiceTableGUID == invoiceTableGuid
                                                            select new InvoiceLineItemView
                                                            {
                                                                InvoiceLineGUID = invoiceLine.InvoiceLineGUID.ToString(),
                                                                TaxTableGUID = invoiceLine.TaxTableGUID.ToString(),
                                                                TaxTable_PaymentTaxTableGUID = taxTable.PaymentTaxTableGUID.Value.ToString()
                                                            }).AsNoTracking().ToList();
            return invoieLines;
        }
        #endregion

        public IEnumerable<InvoiceLine> GetInvoiceLinesForGenTaxTable(IEnumerable<InvoiceLine> invoiceLines)
        {
            try
            {
                IEnumerable<InvoiceLine> invoiceLineList = (from invoiceLine in invoiceLines
                                                                        join taxtable in db.Set<TaxTable>()
                                                                        on invoiceLine.TaxTableGUID equals taxtable.TaxTableGUID
                                                                        where taxtable.PaymentTaxTableGUID == null
                                                                        && invoiceLine.TaxTableGUID != null
                                                                        select invoiceLine).ToList();

                return invoiceLineList;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public override void ValidateAdd(IEnumerable<InvoiceLine> items)
        {
            base.ValidateAdd(items);
        }
        public override void ValidateAdd(InvoiceLine item)
        {
            base.ValidateAdd(item);
        }
    }
}

