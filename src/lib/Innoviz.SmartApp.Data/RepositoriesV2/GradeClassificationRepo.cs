using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IGradeClassificationRepo
	{
		#region DropDown
		IEnumerable<SelectItem<GradeClassificationItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<GradeClassificationListView> GetListvw(SearchParameter search);
		GradeClassificationItemView GetByIdvw(Guid id);
		GradeClassification Find(params object[] keyValues);
		GradeClassification GetGradeClassificationByIdNoTracking(Guid guid);
		GradeClassification CreateGradeClassification(GradeClassification gradeClassification);
		void CreateGradeClassificationVoid(GradeClassification gradeClassification);
		GradeClassification UpdateGradeClassification(GradeClassification dbGradeClassification, GradeClassification inputGradeClassification, List<string> skipUpdateFields = null);
		void UpdateGradeClassificationVoid(GradeClassification dbGradeClassification, GradeClassification inputGradeClassification, List<string> skipUpdateFields = null);
		void Remove(GradeClassification item);
	}
	public class GradeClassificationRepo : CompanyBaseRepository<GradeClassification>, IGradeClassificationRepo
	{
		public GradeClassificationRepo(SmartAppDbContext context) : base(context) { }
		public GradeClassificationRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public GradeClassification GetGradeClassificationByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.GradeClassificationGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<GradeClassificationItemViewMap> GetDropDownQuery()
		{
			return (from gradeClassification in Entity
					select new GradeClassificationItemViewMap
					{
						CompanyGUID = gradeClassification.CompanyGUID,
						Owner = gradeClassification.Owner,
						OwnerBusinessUnitGUID = gradeClassification.OwnerBusinessUnitGUID,
						GradeClassificationGUID = gradeClassification.GradeClassificationGUID,
						GradeClassificationId = gradeClassification.GradeClassificationId,
						Description = gradeClassification.Description
					});
		}
		public IEnumerable<SelectItem<GradeClassificationItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<GradeClassification>(search, SysParm.CompanyGUID);
				var gradeClassification = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<GradeClassificationItemViewMap, GradeClassificationItemView>().ToDropDownItem(search.ExcludeRowData);


				return gradeClassification;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<GradeClassificationListViewMap> GetListQuery()
		{
			return (from gradeClassification in Entity
				select new GradeClassificationListViewMap
				{
						CompanyGUID = gradeClassification.CompanyGUID,
						Owner = gradeClassification.Owner,
						OwnerBusinessUnitGUID = gradeClassification.OwnerBusinessUnitGUID,
						GradeClassificationGUID = gradeClassification.GradeClassificationGUID,
						GradeClassificationId = gradeClassification.GradeClassificationId,
						Description = gradeClassification.Description,
				});
		}
		public SearchResult<GradeClassificationListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<GradeClassificationListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<GradeClassification>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<GradeClassificationListViewMap, GradeClassificationListView>();
				result = list.SetSearchResult<GradeClassificationListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<GradeClassificationItemViewMap> GetItemQuery()
		{
			return (from gradeClassification in Entity
					join ownerBU in db.Set<BusinessUnit>()
					on gradeClassification.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljGradeClassificationOwnerBU
					from ownerBU in ljGradeClassificationOwnerBU.DefaultIfEmpty()
					join company in db.Set<Company>() on gradeClassification.CompanyGUID equals company.CompanyGUID
					select new GradeClassificationItemViewMap
					{
						CompanyGUID = gradeClassification.CompanyGUID,
						Owner = gradeClassification.Owner,
						OwnerBusinessUnitGUID = gradeClassification.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CompanyId = company.CompanyId,
						CreatedBy = gradeClassification.CreatedBy,
						CreatedDateTime = gradeClassification.CreatedDateTime,
						ModifiedBy = gradeClassification.ModifiedBy,
						ModifiedDateTime = gradeClassification.ModifiedDateTime,
						GradeClassificationGUID = gradeClassification.GradeClassificationGUID,
						Description = gradeClassification.Description,
						GradeClassificationId = gradeClassification.GradeClassificationId,
					
						RowVersion = gradeClassification.RowVersion,
					});
		}
		public GradeClassificationItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<GradeClassificationItemViewMap, GradeClassificationItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public GradeClassification CreateGradeClassification(GradeClassification gradeClassification)
		{
			try
			{
				gradeClassification.GradeClassificationGUID = Guid.NewGuid();
				base.Add(gradeClassification);
				return gradeClassification;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateGradeClassificationVoid(GradeClassification gradeClassification)
		{
			try
			{
				CreateGradeClassification(gradeClassification);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public GradeClassification UpdateGradeClassification(GradeClassification dbGradeClassification, GradeClassification inputGradeClassification, List<string> skipUpdateFields = null)
		{
			try
			{
				dbGradeClassification = dbGradeClassification.MapUpdateValues<GradeClassification>(inputGradeClassification);
				base.Update(dbGradeClassification);
				return dbGradeClassification;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateGradeClassificationVoid(GradeClassification dbGradeClassification, GradeClassification inputGradeClassification, List<string> skipUpdateFields = null)
		{
			try
			{
				dbGradeClassification = dbGradeClassification.MapUpdateValues<GradeClassification>(inputGradeClassification, skipUpdateFields);
				base.Update(dbGradeClassification);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

