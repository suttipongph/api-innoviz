using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IGuarantorAgreementLineRepo
	{
		#region DropDown
		IEnumerable<SelectItem<GuarantorAgreementLineItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		void ValidateAdd(IEnumerable<GuarantorAgreementLine> items);

		SearchResult<GuarantorAgreementLineListView> GetListvw(SearchParameter search);
		GuarantorAgreementLineItemView GetByIdvwFormAffiliate(Guid id);
		GuarantorAgreementLineItemView GetByIdvw(Guid id);
		GuarantorAgreementLine Find(params object[] keyValues);
		GuarantorAgreementLine GetGuarantorAgreementLineByIdNoTracking(Guid guid);
		GuarantorAgreementLine CreateGuarantorAgreementLine(GuarantorAgreementLine guarantorAgreementLine);
		void CreateGuarantorAgreementLineVoid(GuarantorAgreementLine guarantorAgreementLine);
		GuarantorAgreementLine UpdateGuarantorAgreementLine(GuarantorAgreementLine dbGuarantorAgreementLine, GuarantorAgreementLine inputGuarantorAgreementLine, List<string> skipUpdateFields = null);
		void UpdateGuarantorAgreementLineVoid(GuarantorAgreementLine dbGuarantorAgreementLine, GuarantorAgreementLine inputGuarantorAgreementLine, List<string> skipUpdateFields = null);
		void Remove(GuarantorAgreementLine item);
		GuarantorAgreementLine GetGuarantorAgreementLineByIdNoTrackingByAccessLevel(Guid guid);
		GuarantorAgreementLineItemView GetGuarantorAgreementLineInitialData(Guid guid);
		int IsLineRelatedById(Guid id);
		List<GuarantorAgreementLine> GetLineListByGuarantorAgreement(Guid guid);
		List<GuarantorAgreementLineItemView> GetGuarantoLineListByCreditApp();
	}
	public class GuarantorAgreementLineRepo : CompanyBaseRepository<GuarantorAgreementLine>, IGuarantorAgreementLineRepo
	{
		public GuarantorAgreementLineRepo(SmartAppDbContext context) : base(context) { }
		public GuarantorAgreementLineRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public GuarantorAgreementLine GetGuarantorAgreementLineByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.GuarantorAgreementLineGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<GuarantorAgreementLineItemViewMap> GetDropDownQuery()
		{
			return (from guarantorAgreementLine in Entity
					select new GuarantorAgreementLineItemViewMap
					{
						CompanyGUID = guarantorAgreementLine.CompanyGUID,
						Owner = guarantorAgreementLine.Owner,
						OwnerBusinessUnitGUID = guarantorAgreementLine.OwnerBusinessUnitGUID,
						GuarantorAgreementLineGUID = guarantorAgreementLine.GuarantorAgreementLineGUID,
						GuarantorAgreementTableGUID = guarantorAgreementLine.GuarantorAgreementTableGUID,
						Name = guarantorAgreementLine.Name
					});
		}
		public IEnumerable<SelectItem<GuarantorAgreementLineItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<GuarantorAgreementLine>(search, SysParm.CompanyGUID);
				var guarantorAgreementLine = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<GuarantorAgreementLineItemViewMap, GuarantorAgreementLineItemView>().ToDropDownItem(search.ExcludeRowData);


				return guarantorAgreementLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<GuarantorAgreementLineListViewMap> GetListQuery()
		{
			return (from guarantorAgreementLine in Entity
					join guarantorTransTable in db.Set<GuarantorTrans>()
					on guarantorAgreementLine.GuarantorTransGUID equals guarantorTransTable.GuarantorTransGUID into ljguarantorTransTable
					from guarantorTransTable in ljguarantorTransTable.DefaultIfEmpty()
					join relatedPersonTable in db.Set<RelatedPersonTable>()
					on guarantorTransTable.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID into ljrelatedPersonTable
					from relatedPersonTable in ljrelatedPersonTable.DefaultIfEmpty()
					select new GuarantorAgreementLineListViewMap
					{
						CompanyGUID = guarantorAgreementLine.CompanyGUID,
						Owner = guarantorAgreementLine.Owner,
						OwnerBusinessUnitGUID = guarantorAgreementLine.OwnerBusinessUnitGUID,
						GuarantorAgreementLineGUID = guarantorAgreementLine.GuarantorAgreementLineGUID,
						Ordering = guarantorAgreementLine.Ordering,
						GuarantorTransGUID = guarantorAgreementLine.GuarantorTransGUID,
						TaxId = guarantorAgreementLine.TaxId,
						PassportId = guarantorAgreementLine.PassportId,
						Name = guarantorAgreementLine.Name,
						GuarantorTrans_Values = SmartAppUtil.GetDropDownLabel(relatedPersonTable.RelatedPersonId, relatedPersonTable.Name),
						GuarantorAgreementTableGUID = guarantorAgreementLine.GuarantorAgreementTableGUID.ToString(),
						OperatedBy = guarantorAgreementLine.OperatedBy
						

					}); ;
		}
		public SearchResult<GuarantorAgreementLineListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<GuarantorAgreementLineListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<GuarantorAgreementLine>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<GuarantorAgreementLineListViewMap, GuarantorAgreementLineListView>();
				result = list.SetSearchResult<GuarantorAgreementLineListView>(total, search);
                return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		
		public List<GuarantorAgreementLine> GetLineListByGuarantorAgreement(Guid guid)
		{
			try
			{
				Guid companyGUID = SysParm.CompanyGUID.StringToGuid();
				return Entity.Where(t => t.CompanyGUID == companyGUID && t.GuarantorAgreementTableGUID == guid ).AsNoTracking().ToList();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
			
		}
		private IQueryable<GuarantorAgreementLineItemViewMap> GetListQueryByCreditApp()
		{
			var result = (from guarantorAgreementLine in Entity
						  join guarantorTrans in db.Set<GuarantorTrans>()
						  on guarantorAgreementLine.GuarantorTransGUID equals guarantorTrans.GuarantorTransGUID into ljguarantorTrans

						  from guarantorTrans in ljguarantorTrans.DefaultIfEmpty()
						  join relatedPerson in db.Set<RelatedPersonTable>()
						  on guarantorTrans.RelatedPersonTableGUID equals relatedPerson.RelatedPersonTableGUID into ljrelatedPerson

						  from relatedPerson in ljrelatedPerson.DefaultIfEmpty()
						  join addressTrans in db.Set<AddressTrans>()
						  on relatedPerson.RelatedPersonTableGUID equals addressTrans.AddressTransGUID into ljaddressTrans

						  from addressTrans in ljaddressTrans.DefaultIfEmpty()
						  where guarantorTrans.InActive == false
						  select new GuarantorAgreementLineItemViewMap
						  {
							  Ordering = guarantorTrans.Ordering,
							  GuarantorTransGUID = guarantorTrans.GuarantorTransGUID,
							  Name = relatedPerson.Name,
							  TaxId = relatedPerson.TaxId,
							  PassportId = relatedPerson.PassportId,
							  WorkPermitId = relatedPerson.WorkPermitId,
							  DateOfIssue = relatedPerson.DateOfIssue,
							  DateOfBirth = relatedPerson.DateOfBirth,
							  NationalityGUID = relatedPerson.NationalityGUID,
							  RaceGUID = relatedPerson.RaceGUID,
							  PrimaryAddress1 = addressTrans.Address1,
							  PrimaryAddress2 = addressTrans.Address2,
							  GuarantorTransTable_InActive = guarantorTrans.InActive,
							  GuarantorTransTable_RefGUID = guarantorTrans.RefGUID.GuidNullOrEmptyToString(),
							  CompanyGUID = guarantorTrans.CompanyGUID,
							  Affiliate = guarantorTrans.Affiliate,
							  OperatedBy = guarantorAgreementLine.OperatedBy
						  });
			return result;
			
		}
		public List<GuarantorAgreementLineItemView> GetGuarantoLineListByCreditApp()
		{
			try
			{
				var guarantorAgreementLine = GetListQueryByCreditApp().ToMaps<GuarantorAgreementLineItemViewMap, GuarantorAgreementLineItemView>();
				return guarantorAgreementLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private IQueryable<GuarantorAgreementLineItemViewMap> GetItemQuery()
		{
			IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService();
			return (from guarantorAgreementLine in Entity
					join company in db.Set<Company>()
					on guarantorAgreementLine.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on guarantorAgreementLine.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljGuarantorAgreementLineOwnerBU
					from ownerBU in ljGuarantorAgreementLineOwnerBU.DefaultIfEmpty()
					join guarantorAgreementTable in db.Set<GuarantorAgreementTable>()
					on guarantorAgreementLine.GuarantorAgreementTableGUID equals guarantorAgreementTable.GuarantorAgreementTableGUID into ljguarantorAgreementTable
					from guarantorAgreementTable in ljguarantorAgreementTable.DefaultIfEmpty()
					join documentStatus in db.Set<DocumentStatus>()
					on guarantorAgreementTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus
					from documentStatus in ljdocumentStatus.DefaultIfEmpty()
					join mainAgreement in db.Set<MainAgreementTable>()
					on guarantorAgreementTable.MainAgreementTableGUID equals mainAgreement.MainAgreementTableGUID into ljmainAgreement
					from mainAgreement in ljmainAgreement.DefaultIfEmpty()
					select new GuarantorAgreementLineItemViewMap
					{
						CompanyGUID = guarantorAgreementLine.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = guarantorAgreementLine.Owner,
						OwnerBusinessUnitGUID = guarantorAgreementLine.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = guarantorAgreementLine.CreatedBy,
						CreatedDateTime = guarantorAgreementLine.CreatedDateTime,
						ModifiedBy = guarantorAgreementLine.ModifiedBy,
						ModifiedDateTime = guarantorAgreementLine.ModifiedDateTime,
						GuarantorAgreementLineGUID = guarantorAgreementLine.GuarantorAgreementLineGUID,
						Age = relatedPersonTableService.CalcAge(guarantorAgreementLine.DateOfBirth),
						DateOfBirth = guarantorAgreementLine.DateOfBirth,
						DateOfIssue = guarantorAgreementLine.DateOfIssue,
						GuarantorAgreementTableGUID = guarantorAgreementLine.GuarantorAgreementTableGUID,
						GuarantorTransGUID = guarantorAgreementLine.GuarantorTransGUID,
						Name = guarantorAgreementLine.Name,
						NationalityGUID = guarantorAgreementLine.NationalityGUID,
						Ordering = guarantorAgreementLine.Ordering,
						PassportId = guarantorAgreementLine.PassportId,
						PrimaryAddress1 = guarantorAgreementLine.PrimaryAddress1,
						PrimaryAddress2 = guarantorAgreementLine.PrimaryAddress2,
						RaceGUID = guarantorAgreementLine.RaceGUID,
						TaxId = guarantorAgreementLine.TaxId,
						WorkPermitId = guarantorAgreementLine.WorkPermitId,
						GuarantorAgreementTable_InternalGuarantorAgreementId = SmartAppUtil.GetDropDownLabel(guarantorAgreementTable.InternalGuarantorAgreementId, guarantorAgreementTable.Description),
						Affiliate = guarantorAgreementTable.Affiliate,
						CreditAppTableGUID = guarantorAgreementLine.CreditAppTableGUID,
						ApprovedCreditLimit = guarantorAgreementLine.ApprovedCreditLimit,
						CustomerTableGUID = guarantorAgreementLine.CustomerTableGUID,
						MaximumGuaranteeAmount= guarantorAgreementLine.MaximumGuaranteeAmount,
						DocumentStatus_StatusId = documentStatus.StatusId,
						MainAgreement_CreditAppTableGUID = mainAgreement.CreditAppTableGUID,
						GuaranteeLand = guarantorAgreementLine.GuaranteeLand,
						OperatedBy = guarantorAgreementLine.OperatedBy,


					
						RowVersion = guarantorAgreementLine.RowVersion,
					});
		}
		public GuarantorAgreementLineItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<GuarantorAgreementLineItemViewMap, GuarantorAgreementLineItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public GuarantorAgreementLineItemView GetByIdvwFormAffiliate(Guid guid)
		{
			var result = (from guarantorAgreementLine in Entity
					join guarantorAgreementTable in db.Set<GuarantorAgreementTable>()
					on guarantorAgreementLine.GuarantorAgreementTableGUID equals guarantorAgreementTable.GuarantorAgreementTableGUID into ljguarantorAgreementTable
					from guarantorAgreementTable in ljguarantorAgreementTable.DefaultIfEmpty()
					join mainAgreement in db.Set<MainAgreementTable>()
					on guarantorAgreementTable.MainAgreementTableGUID equals mainAgreement.MainAgreementTableGUID into ljmainAgreement
					from mainAgreement in ljmainAgreement.DefaultIfEmpty()
					where guarantorAgreementLine.GuarantorAgreementLineGUID == guid
					select new GuarantorAgreementLineItemView
					{
						Mainagreement_AgreementDoctype= mainAgreement.AgreementDocType,
						Affiliate = guarantorAgreementTable.Affiliate



					}).FirstOrDefault();
			return result;
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public GuarantorAgreementLine CreateGuarantorAgreementLine(GuarantorAgreementLine guarantorAgreementLine)
		{
			try
			{
				guarantorAgreementLine.GuarantorAgreementLineGUID = Guid.NewGuid();
				base.Add(guarantorAgreementLine);
				return guarantorAgreementLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateGuarantorAgreementLineVoid(GuarantorAgreementLine guarantorAgreementLine)
		{
			try
			{
				CreateGuarantorAgreementLine(guarantorAgreementLine);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public GuarantorAgreementLine UpdateGuarantorAgreementLine(GuarantorAgreementLine dbGuarantorAgreementLine, GuarantorAgreementLine inputGuarantorAgreementLine, List<string> skipUpdateFields = null)
		{
			try
			{
				dbGuarantorAgreementLine = dbGuarantorAgreementLine.MapUpdateValues<GuarantorAgreementLine>(inputGuarantorAgreementLine);
				base.Update(dbGuarantorAgreementLine);
				return dbGuarantorAgreementLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateGuarantorAgreementLineVoid(GuarantorAgreementLine dbGuarantorAgreementLine, GuarantorAgreementLine inputGuarantorAgreementLine, List<string> skipUpdateFields = null)
		{
			try
			{
				dbGuarantorAgreementLine = dbGuarantorAgreementLine.MapUpdateValues<GuarantorAgreementLine>(inputGuarantorAgreementLine, skipUpdateFields);
				base.Update(dbGuarantorAgreementLine);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public GuarantorAgreementLine GetGuarantorAgreementLineByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.GuarantorAgreementLineGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public GuarantorAgreementLineItemView GetGuarantorAgreementLineInitialData(Guid guid)
        {
			try
			{
				var count = 1;

				var lineCount = Entity.Where(item => item.GuarantorAgreementTableGUID == guid).OrderByDescending(or=>or.Ordering).FirstOrDefault();
                if (lineCount != null)
                {
					count = lineCount.Ordering +1;
                }
				return (from guarantorAgreement in db.Set<GuarantorAgreementTable>()
						join mainAgreement in db.Set<MainAgreementTable>()
						on guarantorAgreement.MainAgreementTableGUID equals mainAgreement.MainAgreementTableGUID into ljmainAgreement
						from mainAgreement in ljmainAgreement.DefaultIfEmpty()
						join documentStatus in db.Set<DocumentStatus>()
						on guarantorAgreement.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus
						from documentStatus in ljdocumentStatus.DefaultIfEmpty()
						where guarantorAgreement.GuarantorAgreementTableGUID == guid
						select new GuarantorAgreementLineItemView
						{
							MainAgreement_CreditAppTableGUID = mainAgreement.CreditAppTableGUID.ToString(),
							GuarantorAgreementLineGUID = Guid.NewGuid().ToString(),
							Age = 0,
							GuarantorAgreementTableGUID = guarantorAgreement.GuarantorAgreementTableGUID.ToString(),
							Ordering = count,
							Affiliate = guarantorAgreement.Affiliate,
							CreditAppTableGUID = mainAgreement.CreditAppTableGUID.ToString(),
							GuarantorAgreementTable_InternalGuarantorAgreementId = SmartAppUtil.GetDropDownLabel(guarantorAgreement.InternalGuarantorAgreementId, guarantorAgreement.Description),
							DocumentStatus_StatusId = documentStatus.StatusId
						}).FirstOrDefault();


					
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public int IsLineRelatedById(Guid id)
        {
			var result = (from guarantorLine in db.Set<GuarantorAgreementLine>()
						  join guarantorTable in db.Set<GuarantorAgreementTable>()
						  on guarantorLine.GuarantorAgreementTableGUID equals guarantorTable.GuarantorAgreementTableGUID into ljguarantorTable
						  from guarantorTable in ljguarantorTable.DefaultIfEmpty()
						  where guarantorTable.GuarantorAgreementTableGUID == id
						  select new GuarantorAgreementLine
						  {
							  GuarantorAgreementTableGUID = guarantorTable.GuarantorAgreementTableGUID
						  }
						  ).ToList();
			return result.Count();
        }
		public override void ValidateAdd(IEnumerable<GuarantorAgreementLine> items)
		{
			foreach (var item in items)
			{
				ValidateAdd(item);
			}
		}
		public override void ValidateAdd(GuarantorAgreementLine item)
		{
			//CheckDupplicate(item);
			base.ValidateAdd(item);
		}
	}
}

