using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface IExchangeRateRepo
    {
        #region DropDown
        IEnumerable<SelectItem<ExchangeRateItemView>> GetDropDownItem(SearchParameter search);
        #endregion DropDown
        SearchResult<ExchangeRateListView> GetListvw(SearchParameter search);
        ExchangeRateItemView GetByIdvw(Guid id);
        ExchangeRate Find(params object[] keyValues);
        ExchangeRate GetExchangeRateByIdNoTracking(Guid guid);
        ExchangeRate CreateExchangeRate(ExchangeRate exchangeRate);
        void CreateExchangeRateVoid(ExchangeRate exchangeRate);
        ExchangeRate UpdateExchangeRate(ExchangeRate dbExchangeRate, ExchangeRate inputExchangeRate, List<string> skipUpdateFields = null);
        void UpdateExchangeRateVoid(ExchangeRate dbExchangeRate, ExchangeRate inputExchangeRate, List<string> skipUpdateFields = null);
        void Remove(ExchangeRate item);
        #region shared
        ExchangeRate GetByCurrencyGUIDAndDateNoTracking(Guid currencyGuid, DateTime asOfDate);
        #endregion
    }
    public class ExchangeRateRepo : CompanyBaseRepository<ExchangeRate>, IExchangeRateRepo
    {
        public ExchangeRateRepo(SmartAppDbContext context) : base(context) { }
        public ExchangeRateRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public ExchangeRate GetExchangeRateByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.ExchangeRateGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        private IQueryable<ExchangeRateItemViewMap> GetDropDownQuery()
        {
            return (from exchangeRate in Entity
                    select new ExchangeRateItemViewMap
                    {
                        CompanyGUID = exchangeRate.CompanyGUID,
                        Owner = exchangeRate.Owner,
                        OwnerBusinessUnitGUID = exchangeRate.OwnerBusinessUnitGUID,
                        ExchangeRateGUID = exchangeRate.ExchangeRateGUID
                    });
        }
        public IEnumerable<SelectItem<ExchangeRateItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<ExchangeRate>(search, SysParm.CompanyGUID, DateTime.MinValue);
                var exchangeRate = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<ExchangeRateItemViewMap, ExchangeRateItemView>().ToDropDownItem(search.ExcludeRowData);


                return exchangeRate;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #region GetListvw
        private IQueryable<ExchangeRateListViewMap> GetListQuery()
        {
            return (from exchangeRate in Entity
                    join company in db.Set<Company>()
                    on exchangeRate.CompanyGUID equals company.CompanyGUID
                    join currency in db.Set<Currency>()
                    on exchangeRate.CurrencyGUID equals currency.CurrencyGUID
                    join homeCurrency in db.Set<Currency>()
                    on exchangeRate.HomeCurrencyGUID equals homeCurrency.CurrencyGUID into ljhomeCurrency
                    from homeCurrency in ljhomeCurrency.DefaultIfEmpty()
                    select new ExchangeRateListViewMap
                    {
                        CurrencyGUID = exchangeRate.CurrencyGUID,
                        CompanyGUID = exchangeRate.CompanyGUID,
                        ExchangeRateGUID = exchangeRate.ExchangeRateGUID,
                        Owner = exchangeRate.Owner,
                        CompanyId = company.CompanyId,
                        OwnerBusinessUnitGUID = exchangeRate.OwnerBusinessUnitGUID,
                        EffectiveFrom = exchangeRate.EffectiveFrom,
                        EffectiveTo = exchangeRate.EffectiveTo,
                        Rate = exchangeRate.Rate,
                        HomeCurrencyGUID = exchangeRate.HomeCurrencyGUID,
                        Currency_CurrencyId = currency.CurrencyId,
                        HomeCurrency_CurrencyId = homeCurrency.CurrencyId,
                    });
        }
        public SearchResult<ExchangeRateListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<ExchangeRateListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<ExchangeRate>(search, SysParm.CompanyGUID, DateTime.MinValue);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<ExchangeRateListViewMap, ExchangeRateListView>();
                result = list.SetSearchResult<ExchangeRateListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<ExchangeRateItemViewMap> GetItemQuery()
        {
            return (from exchangeRate in Entity
                    join company in db.Set<Company>()
                    on exchangeRate.CompanyGUID equals company.CompanyGUID
                    join ownerBU in db.Set<BusinessUnit>()
                    on exchangeRate.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljExchangeRateOwnerBU
                    from ownerBU in ljExchangeRateOwnerBU.DefaultIfEmpty()
                    join currency in db.Set<Currency>() on exchangeRate.CurrencyGUID equals currency.CurrencyGUID
                    join homeCurrency in db.Set<Currency>() on exchangeRate.HomeCurrencyGUID equals homeCurrency.CurrencyGUID
                    select new ExchangeRateItemViewMap
                    {
                        CompanyGUID = exchangeRate.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = exchangeRate.Owner,
                        OwnerBusinessUnitGUID = exchangeRate.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        CreatedBy = exchangeRate.CreatedBy,
                        CreatedDateTime = exchangeRate.CreatedDateTime,
                        ModifiedBy = exchangeRate.ModifiedBy,
                        ModifiedDateTime = exchangeRate.ModifiedDateTime,
                        ExchangeRateGUID = exchangeRate.ExchangeRateGUID,
                        CurrencyGUID = exchangeRate.CurrencyGUID,
                        EffectiveFrom = exchangeRate.EffectiveFrom,
                        EffectiveTo = exchangeRate.EffectiveTo,
                        HomeCurrencyGUID = exchangeRate.HomeCurrencyGUID,
                        Rate = exchangeRate.Rate,
                        Currency_Values = SmartAppUtil.GetDropDownLabel(currency.CurrencyId, currency.Name),
                        HomeCurrency_Values = (homeCurrency != null) ? SmartAppUtil.GetDropDownLabel(homeCurrency.CurrencyId, homeCurrency.Name) : null,
                    
                        RowVersion = exchangeRate.RowVersion,
                    });
        }
        public ExchangeRateItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<ExchangeRateItemViewMap, ExchangeRateItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public ExchangeRate CreateExchangeRate(ExchangeRate exchangeRate)
        {
            try
            {
                exchangeRate.ExchangeRateGUID = Guid.NewGuid();
                base.Add(exchangeRate);
                return exchangeRate;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateExchangeRateVoid(ExchangeRate exchangeRate)
        {
            try
            {
                CreateExchangeRate(exchangeRate);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ExchangeRate UpdateExchangeRate(ExchangeRate dbExchangeRate, ExchangeRate inputExchangeRate, List<string> skipUpdateFields = null)
        {
            try
            {
                dbExchangeRate = dbExchangeRate.MapUpdateValues<ExchangeRate>(inputExchangeRate);
                base.Update(dbExchangeRate);
                return dbExchangeRate;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateExchangeRateVoid(ExchangeRate dbExchangeRate, ExchangeRate inputExchangeRate, List<string> skipUpdateFields = null)
        {
            try
            {
                dbExchangeRate = dbExchangeRate.MapUpdateValues<ExchangeRate>(inputExchangeRate, skipUpdateFields);
                base.Update(dbExchangeRate);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public override void ValidateAdd(ExchangeRate item)
        {
            base.ValidateAdd(item);
            ValidateEffective(item);
        }
        public override void ValidateUpdate(ExchangeRate item)
        {
            base.ValidateUpdate(item);
            ValidateEffective(item);
        }
        private void ValidateEffective(ExchangeRate item)
        {
            try
            {
                DateTime? effectiveTo = (item.EffectiveTo != null) ? item.EffectiveTo : DateTime.MaxValue;

                if (!(item.EffectiveFrom <= effectiveTo))
                {
                    SmartAppException ex = new SmartAppException("ERROR.ERROR");
                    ex.AddData("ERROR.00037");
                    throw SmartAppUtil.AddStackTrace(ex);
                }

                if (BetweenEffectiveDate(item))
                {
                    SmartAppException ex = new SmartAppException("ERROR.ERROR");
                    ex.AddData("ERROR.00651", new string[] { item.EffectiveFrom.DateToString(), effectiveTo.DateToString() });
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool BetweenEffectiveDate(ExchangeRate item)
        {
            try
            {
                DateTime? maxEffectiveTo = MaxEffectiveTo(item.CurrencyGUID);
                DateTime? DBeffectiveTo = (maxEffectiveTo != null) ? maxEffectiveTo : DateTime.MaxValue.AddDays(-1);

                DateTime? effectiveTo = (item.EffectiveTo != null) ? item.EffectiveTo : DateTime.MaxValue;

                if (!Entity.Any(o => o.CurrencyGUID == item.CurrencyGUID))
                {
                    return false;
                }

                var notBetween = Entity.Any(o =>
                                (item.EffectiveFrom >= o.EffectiveFrom && item.EffectiveFrom <= o.EffectiveTo && item.ExchangeRateGUID != o.ExchangeRateGUID &&
                                o.CurrencyGUID == item.CurrencyGUID && o.CompanyGUID == item.CompanyGUID)

                            || (item.EffectiveFrom <= o.EffectiveFrom && item.EffectiveTo >= o.EffectiveTo && item.ExchangeRateGUID != o.ExchangeRateGUID &&
                                o.CurrencyGUID == item.CurrencyGUID && o.CompanyGUID == item.CompanyGUID));

                if (notBetween)
                {
                    return notBetween;
                }

                if (maxEffectiveTo == null && DBeffectiveTo != DateTime.MaxValue.AddDays(-1))
                {
                    return notBetween;
                }

                var maxRow = Entity.Where(o => o.EffectiveTo == maxEffectiveTo &&
                                               o.CurrencyGUID == item.CurrencyGUID &&
                                               o.CompanyGUID == item.CompanyGUID).AsNoTracking().FirstOrDefault();

                if (item.ExchangeRateGUID == maxRow.ExchangeRateGUID)
                {
                    return notBetween;
                }
                if (maxEffectiveTo == null && effectiveTo < DBeffectiveTo && effectiveTo >= maxRow.EffectiveFrom && item.ExchangeRateGUID != maxRow.ExchangeRateGUID)
                {
                    return true;
                }

                var maxDB = Entity.Any(o => o.CurrencyGUID == item.CurrencyGUID &&
                        o.CompanyGUID == item.CompanyGUID &&
                        (effectiveTo >= DBeffectiveTo && item.EffectiveFrom <= DBeffectiveTo)
                        && item.ExchangeRateGUID != o.ExchangeRateGUID
                       );

                if (maxDB)
                {
                    return maxDB;
                }
                var intervene = Entity.Any(o => item.EffectiveTo < o.EffectiveTo && item.EffectiveFrom < o.EffectiveFrom && item.EffectiveTo >= o.EffectiveFrom
                                             && item.ExchangeRateGUID != o.ExchangeRateGUID && o.CurrencyGUID == item.CurrencyGUID && o.CompanyGUID == item.CompanyGUID);

                return intervene;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public DateTime? MaxEffectiveTo(Guid? CurrencyGUID)
        {
            try
            {
                List<ExchangeRate> interestTypeValues = Entity.Where(o => o.CurrencyGUID == CurrencyGUID).AsNoTracking().ToList();
                if (interestTypeValues.Count() == 0)
                {
                    return null;
                }
                else if (interestTypeValues.Any(o => o.EffectiveTo == null))
                {
                    return null;
                }
                else
                {
                    return interestTypeValues.Max(o => o.EffectiveTo);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Create, Update, Delete

        #region shared
        public ExchangeRate GetByCurrencyGUIDAndDateNoTracking(Guid currencyGuid, DateTime asOfDate)
        {
            try
            {
                ExchangeRate exchangeRate = Entity.Where(w => w.CurrencyGUID == currencyGuid
                                                        && w.EffectiveFrom <= asOfDate
                                                        && (w.EffectiveTo >= asOfDate ||
                                                                w.EffectiveTo == null)).AsNoTracking().FirstOrDefault();
                return exchangeRate;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}

