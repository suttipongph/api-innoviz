using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface INationalityRepo
	{
		#region DropDown
		IEnumerable<SelectItem<NationalityItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<NationalityListView> GetListvw(SearchParameter search);
		NationalityItemView GetByIdvw(Guid id);
		Nationality Find(params object[] keyValues);
		Nationality GetNationalityByIdNoTracking(Guid guid);
		Nationality CreateNationality(Nationality nationality);
		void CreateNationalityVoid(Nationality nationality);
		Nationality UpdateNationality(Nationality dbNationality, Nationality inputNationality, List<string> skipUpdateFields = null);
		void UpdateNationalityVoid(Nationality dbNationality, Nationality inputNationality, List<string> skipUpdateFields = null);
		void Remove(Nationality item);
	}
	public class NationalityRepo : CompanyBaseRepository<Nationality>, INationalityRepo
	{
		public NationalityRepo(SmartAppDbContext context) : base(context) { }
		public NationalityRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public Nationality GetNationalityByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.NationalityGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<NationalityItemViewMap> GetDropDownQuery()
		{
			return (from nationality in Entity
					select new NationalityItemViewMap
					{
						CompanyGUID = nationality.CompanyGUID,
						Owner = nationality.Owner,
						OwnerBusinessUnitGUID = nationality.OwnerBusinessUnitGUID,
						NationalityGUID = nationality.NationalityGUID,
						NationalityId = nationality.NationalityId,
						Description = nationality.Description
					});
		}
		public IEnumerable<SelectItem<NationalityItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<Nationality>(search, SysParm.CompanyGUID);
				var nationality = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<NationalityItemViewMap, NationalityItemView>().ToDropDownItem(search.ExcludeRowData);


				return nationality;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<NationalityListViewMap> GetListQuery()
		{
			return (from nationality in Entity
				select new NationalityListViewMap
				{
						CompanyGUID = nationality.CompanyGUID,
						Owner = nationality.Owner,
						OwnerBusinessUnitGUID = nationality.OwnerBusinessUnitGUID,
						NationalityGUID = nationality.NationalityGUID,
						NationalityId = nationality.NationalityId,
						Description = nationality.Description,
				});
		}
		public SearchResult<NationalityListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<NationalityListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<Nationality>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<NationalityListViewMap, NationalityListView>();
				result = list.SetSearchResult<NationalityListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<NationalityItemViewMap> GetItemQuery()
		{
			return (from nationality in Entity
					join ownerBU in db.Set<BusinessUnit>()
					on nationality.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljNationalityOwnerBU
					from ownerBU in ljNationalityOwnerBU.DefaultIfEmpty()
					join company in db.Set<Company>() on nationality.CompanyGUID equals company.CompanyGUID
					select new NationalityItemViewMap
					{
						CompanyGUID = nationality.CompanyGUID,
						Owner = nationality.Owner,
						OwnerBusinessUnitGUID = nationality.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CompanyId = company.CompanyId,
						CreatedBy = nationality.CreatedBy,
						CreatedDateTime = nationality.CreatedDateTime,
						ModifiedBy = nationality.ModifiedBy,
						ModifiedDateTime = nationality.ModifiedDateTime,
						NationalityGUID = nationality.NationalityGUID,
						Description = nationality.Description,
						NationalityId = nationality.NationalityId,
					
						RowVersion = nationality.RowVersion,
					});
		}
		public NationalityItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<NationalityItemViewMap, NationalityItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public Nationality CreateNationality(Nationality nationality)
		{
			try
			{
				nationality.NationalityGUID = Guid.NewGuid();
				base.Add(nationality);
				return nationality;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateNationalityVoid(Nationality nationality)
		{
			try
			{
				CreateNationality(nationality);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public Nationality UpdateNationality(Nationality dbNationality, Nationality inputNationality, List<string> skipUpdateFields = null)
		{
			try
			{
				dbNationality = dbNationality.MapUpdateValues<Nationality>(inputNationality);
				base.Update(dbNationality);
				return dbNationality;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateNationalityVoid(Nationality dbNationality, Nationality inputNationality, List<string> skipUpdateFields = null)
		{
			try
			{
				dbNationality = dbNationality.MapUpdateValues<Nationality>(inputNationality, skipUpdateFields);
				base.Update(dbNationality);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

