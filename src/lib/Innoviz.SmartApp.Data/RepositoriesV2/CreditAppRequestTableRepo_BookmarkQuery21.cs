﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public partial interface ICreditAppRequestTableRepo
	{
		BuyerReviewCreditLimitBookmarkView GetBuyerReviewCreditLimitBookmarkValue(Guid refGUID, Guid bookmarkDocumentTransGUID);
	}
	public partial class CreditAppRequestTableRepo : CompanyBaseRepository<CreditAppRequestTable>, ICreditAppRequestTableRepo
	{
		#region BuyerReviewCreditLimitBookmark
		public BuyerReviewCreditLimitBookmarkView GetBuyerReviewCreditLimitBookmarkValue(Guid refGUID, Guid bookmarkDocumentTransGUID)
		{
			try
			{
				BuyerReviewCreditLimitBookmarkView buyerReviewCreditLimitBookmarkView = (from creditAppRequestTable in Entity
																						 join creditAppRequestLine in db.Set<CreditAppRequestLine>().Where(w => w.CreditAppRequestLineGUID == refGUID)
																						 on creditAppRequestTable.CreditAppRequestTableGUID equals creditAppRequestLine.CreditAppRequestTableGUID

																						 join documentStatus in db.Set<DocumentStatus>()
																						 on creditAppRequestTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID

																						 join creditAppTable in db.Set<CreditAppTable>()
																						 on creditAppRequestTable.RefCreditAppTableGUID equals creditAppTable.CreditAppTableGUID
																						 into ljCreditAppTable 
																						 from creditAppTable in ljCreditAppTable.DefaultIfEmpty()

																						 join customerTable in db.Set<CustomerTable>()
																						 on creditAppRequestTable.CustomerTableGUID equals customerTable.CustomerTableGUID

																						 join employeeTable in db.Set<EmployeeTable>()
																						 on customerTable.ResponsibleByGUID equals employeeTable.EmployeeTableGUID
																						 into ljEmployee
																						 from employeeTable in ljEmployee.DefaultIfEmpty()

																						 join creditScoring in db.Set<CreditScoring>()
																						 on creditAppRequestTable.CreditScoringGUID equals creditScoring.CreditScoringGUID
																						 into ljCreditScoring
																						 from creditScoring in ljCreditScoring.DefaultIfEmpty()

																						 join buyerTable in db.Set<BuyerTable>()
																						 on creditAppRequestLine.BuyerTableGUID equals buyerTable.BuyerTableGUID
																						 
																						 join businessSegment in db.Set<BusinessSegment>()
																						 on buyerTable.BusinessSegmentGUID equals businessSegment.BusinessSegmentGUID

																						 join kycSetup in db.Set<KYCSetup>()
																						 on buyerTable.KYCSetupGUID equals kycSetup.KYCSetupGUID
																						 into ljKycSetup
																						 from kycSetup in ljKycSetup.DefaultIfEmpty()

																						 join buyerCreditScoring in db.Set<CreditScoring>()
																						 on buyerTable.CreditScoringGUID equals buyerCreditScoring.CreditScoringGUID
																						 into ljBuyerCreditScoring
																						 from buyerCreditScoring in ljBuyerCreditScoring.DefaultIfEmpty()

																						 join blacklistStatus in db.Set<BlacklistStatus>()
																						 on buyerTable.BlacklistStatusGUID equals blacklistStatus.BlacklistStatusGUID
																						 into ljBlacklistStatus
																						 from blacklistStatus in ljBlacklistStatus.DefaultIfEmpty()

																						 join lineOfBusiness in db.Set<LineOfBusiness>()
																						 on buyerTable.LineOfBusinessGUID equals lineOfBusiness.LineOfBusinessGUID
																						 into ljLineOfBusiness
																						 from lineOfBusiness in ljLineOfBusiness.DefaultIfEmpty()

																						 join custBank in db.Set<CustBank>()
																						 on creditAppTable.PDCBankGUID equals custBank.CustBankGUID
																						 into ljCustBank
																						 from custBank in ljCustBank.DefaultIfEmpty()

																						 join bankGroup in db.Set<BankGroup>()
																						 on custBank.BankGroupGUID equals bankGroup.BankGroupGUID
																						 into ljBankGroup
																						 from bankGroup in ljBankGroup.DefaultIfEmpty()

																						 join bankType in db.Set<BankType>()
																						 on custBank.BankTypeGUID equals bankType.BankTypeGUID
																						 into ljBankType
																						 from bankType in ljBankType.DefaultIfEmpty()
																						 select new BuyerReviewCreditLimitBookmarkView
																						 {
																							 CANumber = creditAppRequestTable.CreditAppRequestId,
																							 RefCANumber = creditAppTable != null ? creditAppTable.CreditAppId : "",
																							 CAStatus = documentStatus.Description,
																							 CACreateDate = creditAppRequestTable.RequestDate,
																							 CustomerName1 = customerTable.Name,
																							 CustomerCode = customerTable.CustomerId,
																							 SalesResp = employeeTable != null ? employeeTable.Name : "",
																							 KYCHeader = kycSetup != null ? kycSetup.Description : "",
																							 CreditScoring = buyerCreditScoring != null ? buyerCreditScoring.Description : "",
																							 CADesp = creditAppRequestTable.Description,
																							 CARemark = creditAppRequestTable.Remark,
																							 MarketingComment1st = creditAppRequestLine.MarketingComment,
																							 CreditComment1st = creditAppRequestLine.CreditComment,
																							 ApproverComment1st = creditAppRequestLine.ApproverComment,
																							 BusinessSegment1st_2 = businessSegment != null ? businessSegment.Description : "",
																							 BuyerBlacklistStatus1st_1 = blacklistStatus != null ? blacklistStatus.Description : "",
																							 BuyerCreditLimitAllCustomer1st_2 = creditAppRequestLine.AllCustomerBuyerOutstanding,
																							 BuyerName1st_1 = buyerTable.BuyerName,
																							 BuyerTaxID1st_1 = buyerTable.TaxId,
																							 BuyerCompanyRegisteredDate1st_1 = buyerTable.DateOfEstablish,
																							 BuyerCreditLimitRequest1st_2 = creditAppRequestLine.CreditLimitLineRequest,
																							 BuyerCreditScoring1st_1 = buyerCreditScoring != null ? buyerCreditScoring.Description : "",
																							 BuyerFinRevenuePerMonth1_1st_1 = creditAppTable != null ? creditAppTable.SalesAvgPerMonth : 0,
																							 BuyerLineOfBusiness1st_1 = lineOfBusiness != null ? lineOfBusiness.Description : "",
																							 CustPDCBankAccountNumber1st_1 = custBank != null ? custBank.AccountNumber : "",
																							 CustPDCBankAccountType1st_1 = bankType != null ? bankType.Description : "",
																							 CustPDCBankBranch1st_1 = custBank != null ? custBank.BankBranch : "",
																							 CustPDCBankName1st_1 = bankGroup != null ? bankGroup.Description : "",
																							 RefCreditAppTableGUID = creditAppRequestTable.RefCreditAppTableGUID.HasValue ? creditAppRequestTable.RefCreditAppTableGUID.Value : Guid.Empty,
																							 RefCreditAppLineGUID = creditAppRequestLine.RefCreditAppLineGUID.HasValue ? creditAppRequestLine.RefCreditAppLineGUID.Value : Guid.Empty,
																							 CreditAppRequestLineGUID = creditAppRequestLine.CreditAppRequestLineGUID,
																							 AllCustBuyerOutsatanding = creditAppRequestLine.AllCustomerBuyerOutstanding,
																							 CustomerBuyerOutstanding = creditAppRequestLine.CustomerBuyerOutstanding
																						 }).AsNoTracking().FirstOrDefault();

				//R03
				QOriginalCreditAppLine originalCreditAppLine = (from creditAppLine in db.Set<CreditAppLine>().Where(w => w.CreditAppLineGUID == buyerReviewCreditLimitBookmarkView.RefCreditAppLineGUID)
																join invoiceAddressTrans in db.Set<AddressTrans>()
																on creditAppLine.InvoiceAddressGUID equals invoiceAddressTrans.AddressTransGUID
																into ljInvoiceAddressTrans from invoiceAddressTrans in ljInvoiceAddressTrans.DefaultIfEmpty()

																join billingResponsibleBy in db.Set<BillingResponsibleBy>()
																on creditAppLine.BillingResponsibleByGUID equals billingResponsibleBy.BillingResponsibleByGUID
																into ljBillResponsibleBy from billingResponsibleBy in ljBillResponsibleBy.DefaultIfEmpty()

																join billingAddressTrans in db.Set<AddressTrans>()
																on creditAppLine.BillingAddressGUID equals billingAddressTrans.AddressTransGUID
																into ljBillingAddressTrans from billingAddressTrans in ljBillingAddressTrans.DefaultIfEmpty()

																join methodOfPayment in db.Set<MethodOfPayment>()
																on creditAppLine.MethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID
																into ljMethodOfPayment from methodOfPayment in ljMethodOfPayment.DefaultIfEmpty()

																join receiptAddressTrans in db.Set<AddressTrans>()
																on creditAppLine.ReceiptAddressGUID equals receiptAddressTrans.AddressTransGUID
																into ljReceiptAddressTrans from receiptAddressTrans in ljReceiptAddressTrans.DefaultIfEmpty()

																join billingContactPersonTrans in db.Set<ContactPersonTrans>()
																on creditAppLine.BillingContactPersonGUID equals billingContactPersonTrans.ContactPersonTransGUID
																into ljBillingContactPersonTrans
																from billingContactPersonTrans in ljBillingContactPersonTrans.DefaultIfEmpty()

																join billingRelatedPersonTable in db.Set<RelatedPersonTable>()
																on billingContactPersonTrans.RelatedPersonTableGUID equals billingRelatedPersonTable.RelatedPersonTableGUID
																into ljBillingRelatedPersonTable
																from billingRelatedPersonTable in ljBillingRelatedPersonTable.DefaultIfEmpty()

																join receiptContactPersonTrans in db.Set<ContactPersonTrans>()
																on creditAppLine.ReceiptContactPersonGUID equals receiptContactPersonTrans.ContactPersonTransGUID
																into ljReceiptContactPersonTrans
																from receiptContactPersonTrans in ljReceiptContactPersonTrans.DefaultIfEmpty()

																join receiptRelatedPersonTable in db.Set<RelatedPersonTable>()
																on receiptContactPersonTrans.RelatedPersonTableGUID equals receiptRelatedPersonTable.RelatedPersonTableGUID
																into ljReceiptRelatedPerson
																from receiptRelatedPersonTable in ljReceiptRelatedPerson.DefaultIfEmpty()

																select new QOriginalCreditAppLine
																{
																	InvoiceAddress = invoiceAddressTrans != null ? (invoiceAddressTrans.Address1 + " " + invoiceAddressTrans.Address2) : "",
																	MaxPurchasePct = creditAppLine.MaxPurchasePct,
																	AssignmentMethodRemark = creditAppLine.AssignmentMethodRemark,
																	PurchaseFeePct = creditAppLine.PurchaseFeePct,
																	PurchaseFeeCalculateBase = creditAppLine.PurchaseFeeCalculateBase,
																	BillingResponsibleByDescription = billingResponsibleBy != null ? billingResponsibleBy.Description : "",
																	AcceptanceDocument = creditAppLine.AcceptanceDocument,
																	BillingDescription = creditAppLine.BillingDescription,
																	BillingAddress = billingAddressTrans != null ? (billingAddressTrans.Address1 + " " + billingAddressTrans.Address2) : "",
																	LineCondition = creditAppLine.LineCondition,
																	ReceiptDescription = creditAppLine.ReceiptDescription,
																	MethodOfPaymentDescription = methodOfPayment != null ? methodOfPayment.Description : "",
																	ReceiptAddress = receiptAddressTrans != null ? (receiptAddressTrans.Address1 + " " + receiptAddressTrans.Address2) : "",
																	ReceiptContactName = receiptRelatedPersonTable != null ? receiptRelatedPersonTable.Name : "",
																	ReceiptContactPhone = receiptRelatedPersonTable != null ? receiptRelatedPersonTable.Phone : "",
																	ReceiptContactPosition = receiptRelatedPersonTable != null ? receiptRelatedPersonTable.Position : "",
																	BillingContactName = billingRelatedPersonTable != null ? billingRelatedPersonTable.Name : "",
																	BillingContactPhone = billingRelatedPersonTable != null ? billingRelatedPersonTable.Phone : "",
																	BillingContactPosition = billingRelatedPersonTable != null ? billingRelatedPersonTable.Position : "",
																}).FirstOrDefault();
				if(originalCreditAppLine != null)
                {
					buyerReviewCreditLimitBookmarkView.BuyerAddress1st_1 = originalCreditAppLine.InvoiceAddress;
					buyerReviewCreditLimitBookmarkView.AcceptanceDocument1st_1 = originalCreditAppLine.AcceptanceDocument == true ? TextConstants.Yes : TextConstants.No;
					buyerReviewCreditLimitBookmarkView.AssignmentCondition1st_2 = originalCreditAppLine.AssignmentMethodRemark;
					buyerReviewCreditLimitBookmarkView.BillingDescriptiong1st_1 = originalCreditAppLine.BillingDescription;
					buyerReviewCreditLimitBookmarkView.BillingResponsible1st_2 = originalCreditAppLine.BillingResponsibleByDescription;
					buyerReviewCreditLimitBookmarkView.BuyerReceiptAddress1st_1 = originalCreditAppLine.ReceiptAddress;
					buyerReviewCreditLimitBookmarkView.BuyerBillingAddress1st_1 = originalCreditAppLine.BillingAddress;
					buyerReviewCreditLimitBookmarkView.FactoringPurchaseFee1st_1 = originalCreditAppLine.PurchaseFeePct;
					buyerReviewCreditLimitBookmarkView.FactoringPurchaseFeeCalBase1st_1 = SystemStaticData.GetTranslatedMessage(((PurchaseFeeCalculateBase)originalCreditAppLine.PurchaseFeeCalculateBase).GetAttrCode());
					buyerReviewCreditLimitBookmarkView.PaymentMethod1st_2 = originalCreditAppLine.MethodOfPaymentDescription;
					buyerReviewCreditLimitBookmarkView.PurchasePercent1st_2 = originalCreditAppLine.MaxPurchasePct;
					buyerReviewCreditLimitBookmarkView.ReceiptDescriptiong1st_1 = originalCreditAppLine.ReceiptDescription;
					buyerReviewCreditLimitBookmarkView.PurchaseConditionCust = originalCreditAppLine.LineCondition;
					buyerReviewCreditLimitBookmarkView.BuyerBillingContactPerson1st_1 = originalCreditAppLine.BillingContactName;
					buyerReviewCreditLimitBookmarkView.BuyerReceiptContactPerson1st_1 = originalCreditAppLine.ReceiptContactName;
					buyerReviewCreditLimitBookmarkView.BuyerBillingContactPersonPosition1st_1 = originalCreditAppLine.BillingContactPosition;
					buyerReviewCreditLimitBookmarkView.BuyerReceiptContactPersonPosition1st_1 = originalCreditAppLine.ReceiptContactPosition;
					buyerReviewCreditLimitBookmarkView.BuyerBillingContactPersonTel1st_1 = originalCreditAppLine.BillingContactPhone;
					buyerReviewCreditLimitBookmarkView.BuyerReceiptContactPersonTel1st_1 = originalCreditAppLine.ReceiptContactPhone;
				}
				//R03

				

				List<ServiceFeeBookmarkView> serviceFeeBookmarkView = (from serviceFeeConditionTrans in db.Set<ServiceFeeConditionTrans>()
																	  join invoiceRevenueType in db.Set<InvoiceRevenueType>()
																	  on serviceFeeConditionTrans.InvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID
																	  where serviceFeeConditionTrans.RefType == (int)RefType.CreditAppLine
																	  && serviceFeeConditionTrans.RefGUID == buyerReviewCreditLimitBookmarkView.RefCreditAppLineGUID
																	  && serviceFeeConditionTrans.Inactive == false
																	  orderby serviceFeeConditionTrans.Ordering
																	   select new ServiceFeeBookmarkView
																	  {
																		  ServiceFeeTypeDescription = invoiceRevenueType.Description,
																		  AmountBeforeTax = serviceFeeConditionTrans.AmountBeforeTax,
																		  Description = serviceFeeConditionTrans.Description
																	  }).Take(10).ToList();

				IFinancialStatementTransRepo financialStatmentTransRepo = new FinancialStatementTransRepo(db);
				List<FinancialStatementTrans> financialStatmentTrans = financialStatmentTransRepo.GetFinancialStatementTransByRefernece(buyerReviewCreditLimitBookmarkView.CreditAppRequestLineGUID, (int)RefType.CreditAppRequestLine)
																									 .OrderByDescending(o => o.Year).ThenBy(o => o.Ordering).ToList();
				var yearDistinctList = financialStatmentTrans.GroupBy(g => g.Year).Select(s => new DistinctYear
				{
					Year = s.Key
				}).Take(3);

				List<CreditAppReqLineFinBookmarkView> creditAppReqLineFin = new List<CreditAppReqLineFinBookmarkView>();
				foreach (var itemDistinctYear in yearDistinctList)
				{
					var QCreditAppReqLineFin = GetCreditAppReqLineFinBookmarkView(financialStatmentTrans, itemDistinctYear.Year);
					creditAppReqLineFin.Add(QCreditAppReqLineFin);
				}

				List<BillingDocumentBookmarkView> billingDocumentList = (from documentConditionTrans in db.Set<DocumentConditionTrans>()
																		 join documentType in db.Set<DocumentType>()
																		 on documentConditionTrans.DocumentTypeGUID equals documentType.DocumentTypeGUID
																		 where documentConditionTrans.RefType == (int)RefType.CreditAppLine
																		 && documentConditionTrans.RefGUID == buyerReviewCreditLimitBookmarkView.RefCreditAppLineGUID
																		 && documentConditionTrans.DocConVerifyType == 0
																		 && documentConditionTrans.Inactive == false
																		 orderby documentType.DocumentTypeId
																		 select new BillingDocumentBookmarkView
																		 {
																			 BillingDocument = documentType.Description
																		 }).Take(10).ToList();

				billingDocumentList = billingDocumentList.Select((o, i) =>
				{
					o.Row = i + 1;
					return o;
				}).ToList();

				List<ReceiptDocumentBookmarkView> receiptDocumentList = (from documentConditionTrans in db.Set<DocumentConditionTrans>()
																		 join documentType in db.Set<DocumentType>()
																		 on documentConditionTrans.DocumentTypeGUID equals documentType.DocumentTypeGUID
																		 where documentConditionTrans.RefType == (int)RefType.CreditAppLine
																		 && documentConditionTrans.RefGUID == buyerReviewCreditLimitBookmarkView.RefCreditAppLineGUID
																		 && documentConditionTrans.DocConVerifyType == 1
																		 && documentConditionTrans.Inactive == false
																		 orderby documentType.DocumentTypeId
																		 select new ReceiptDocumentBookmarkView
																		 {
																			 ReceiptDocument = documentType.Description
																		 }).Take(10).ToList();

				receiptDocumentList = receiptDocumentList.Select((o, i) =>
				{
					o.Row = i + 1;
					return o;
				}).ToList();

				buyerReviewCreditLimitBookmarkView.ServiceFeeAmt01_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(0) != null ? serviceFeeBookmarkView[0].AmountBeforeTax : 0;
				buyerReviewCreditLimitBookmarkView.ServiceFeeDesc01_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(0) != null ? serviceFeeBookmarkView[0].ServiceFeeTypeDescription : "";
				buyerReviewCreditLimitBookmarkView.ServiceFeeRemark01_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(0) != null ? serviceFeeBookmarkView[0].Description : "";

				buyerReviewCreditLimitBookmarkView.ServiceFeeAmt02_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(1) != null ? serviceFeeBookmarkView[1].AmountBeforeTax : 0;
				buyerReviewCreditLimitBookmarkView.ServiceFeeDesc02_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(1) != null ? serviceFeeBookmarkView[1].ServiceFeeTypeDescription : "";
				buyerReviewCreditLimitBookmarkView.ServiceFeeRemark02_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(1) != null ? serviceFeeBookmarkView[1].Description : "";

				buyerReviewCreditLimitBookmarkView.ServiceFeeAmt03_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(2) != null ? serviceFeeBookmarkView[2].AmountBeforeTax : 0;
				buyerReviewCreditLimitBookmarkView.ServiceFeeDesc03_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(2) != null ? serviceFeeBookmarkView[2].ServiceFeeTypeDescription : "";
				buyerReviewCreditLimitBookmarkView.ServiceFeeRemark03_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(2) != null ? serviceFeeBookmarkView[2].Description : "";

				buyerReviewCreditLimitBookmarkView.ServiceFeeAmt04_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(3) != null ? serviceFeeBookmarkView[3].AmountBeforeTax : 0;
				buyerReviewCreditLimitBookmarkView.ServiceFeeDesc04_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(3) != null ? serviceFeeBookmarkView[3].ServiceFeeTypeDescription : "";
				buyerReviewCreditLimitBookmarkView.ServiceFeeRemark04_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(3) != null ? serviceFeeBookmarkView[3].Description : "";

				buyerReviewCreditLimitBookmarkView.ServiceFeeAmt05_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(4) != null ? serviceFeeBookmarkView[4].AmountBeforeTax : 0;
				buyerReviewCreditLimitBookmarkView.ServiceFeeDesc05_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(4) != null ? serviceFeeBookmarkView[4].ServiceFeeTypeDescription : "";
				buyerReviewCreditLimitBookmarkView.ServiceFeeRemark05_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(4) != null ? serviceFeeBookmarkView[4].Description : "";

				buyerReviewCreditLimitBookmarkView.ServiceFeeAmt06_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(5) != null ? serviceFeeBookmarkView[5].AmountBeforeTax : 0;
				buyerReviewCreditLimitBookmarkView.ServiceFeeDesc06_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(5) != null ? serviceFeeBookmarkView[5].ServiceFeeTypeDescription : "";
				buyerReviewCreditLimitBookmarkView.ServiceFeeRemark06_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(5) != null ? serviceFeeBookmarkView[5].Description : "";

				buyerReviewCreditLimitBookmarkView.ServiceFeeAmt07_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(6) != null ? serviceFeeBookmarkView[6].AmountBeforeTax : 0;
				buyerReviewCreditLimitBookmarkView.ServiceFeeDesc07_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(6) != null ? serviceFeeBookmarkView[6].ServiceFeeTypeDescription : "";
				buyerReviewCreditLimitBookmarkView.ServiceFeeRemark07_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(6) != null ? serviceFeeBookmarkView[6].Description : "";

				buyerReviewCreditLimitBookmarkView.ServiceFeeAmt08_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(7) != null ? serviceFeeBookmarkView[7].AmountBeforeTax : 0;
				buyerReviewCreditLimitBookmarkView.ServiceFeeDesc08_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(7) != null ? serviceFeeBookmarkView[7].ServiceFeeTypeDescription : "";
				buyerReviewCreditLimitBookmarkView.ServiceFeeRemark08_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(7) != null ? serviceFeeBookmarkView[7].Description : "";

				buyerReviewCreditLimitBookmarkView.ServiceFeeAmt09_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(8) != null ? serviceFeeBookmarkView[8].AmountBeforeTax : 0;
				buyerReviewCreditLimitBookmarkView.ServiceFeeDesc09_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(8) != null ? serviceFeeBookmarkView[8].ServiceFeeTypeDescription : "";
				buyerReviewCreditLimitBookmarkView.ServiceFeeRemark09_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(8) != null ? serviceFeeBookmarkView[8].Description : "";

				buyerReviewCreditLimitBookmarkView.ServiceFeeAmt10_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(9) != null ? serviceFeeBookmarkView[9].AmountBeforeTax : 0;
				buyerReviewCreditLimitBookmarkView.ServiceFeeDesc10_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(9) != null ? serviceFeeBookmarkView[9].ServiceFeeTypeDescription : "";
				buyerReviewCreditLimitBookmarkView.ServiceFeeRemark10_1st_1 = serviceFeeBookmarkView.ElementAtOrDefault(9) != null ? serviceFeeBookmarkView[9].Description : "";

				buyerReviewCreditLimitBookmarkView.SumServiceFeeAmt_1st_1 = serviceFeeBookmarkView.Select(s => s.AmountBeforeTax).Sum();

				buyerReviewCreditLimitBookmarkView.BuyerFinYearFirst1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(0) != null ? creditAppReqLineFin[0].Year : 0;
				buyerReviewCreditLimitBookmarkView.BuyerFinYearFirst2_1st_1 = creditAppReqLineFin.ElementAtOrDefault(0) != null ? creditAppReqLineFin[0].Year : 0;

				buyerReviewCreditLimitBookmarkView.BuyerFinYearSecond1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(1) != null ? creditAppReqLineFin[1].Year : 0;
				buyerReviewCreditLimitBookmarkView.BuyerFinYearSecond2_1st_1 = creditAppReqLineFin.ElementAtOrDefault(1) != null ? creditAppReqLineFin[1].Year : 0;

				buyerReviewCreditLimitBookmarkView.BuyerFinYearThird1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(2) != null ? creditAppReqLineFin[2].Year : 0;
				buyerReviewCreditLimitBookmarkView.BuyerFinYearThird2_1st_1 = creditAppReqLineFin.ElementAtOrDefault(2) != null ? creditAppReqLineFin[2].Year : 0;

				buyerReviewCreditLimitBookmarkView.BuyerFinRegisteredCapitalFirst1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(0) != null ? creditAppReqLineFin[0].BuyerFinRegisteredCapital : 0;
				buyerReviewCreditLimitBookmarkView.BuyerFinRegisteredCapitalSecond1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(1) != null ? creditAppReqLineFin[1].BuyerFinRegisteredCapital : 0;
				buyerReviewCreditLimitBookmarkView.BuyerFinRegisteredCapitalThird1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(2) != null ? creditAppReqLineFin[2].BuyerFinRegisteredCapital : 0;

				buyerReviewCreditLimitBookmarkView.BuyerFinPaidCapitalFirst1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(0) != null ? creditAppReqLineFin[0].BuyerFinPaidCapital : 0;
				buyerReviewCreditLimitBookmarkView.BuyerFinPaidCapitalSecond1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(1) != null ? creditAppReqLineFin[1].BuyerFinPaidCapital : 0;
				buyerReviewCreditLimitBookmarkView.BuyerFinPaidCapitalThird1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(2) != null ? creditAppReqLineFin[2].BuyerFinPaidCapital : 0;

				buyerReviewCreditLimitBookmarkView.BuyerFinTotalAssetFirst1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(0) != null ? creditAppReqLineFin[0].BuyerFinTotalAsset : 0;
				buyerReviewCreditLimitBookmarkView.BuyerFinTotalAssetSecond1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(1) != null ? creditAppReqLineFin[1].BuyerFinTotalAsset : 0;
				buyerReviewCreditLimitBookmarkView.BuyerFinTotalAssetThird1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(2) != null ? creditAppReqLineFin[2].BuyerFinTotalAsset : 0;

				buyerReviewCreditLimitBookmarkView.BuyerFinTotalLiabilityFirst1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(0) != null ? creditAppReqLineFin[0].BuyerFinTotalLiability : 0;
				buyerReviewCreditLimitBookmarkView.BuyerFinTotalLiabilitySecond1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(1) != null ? creditAppReqLineFin[1].BuyerFinTotalLiability : 0;
				buyerReviewCreditLimitBookmarkView.BuyerFinTotalLiabilityThird1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(2) != null ? creditAppReqLineFin[2].BuyerFinTotalLiability : 0;

				buyerReviewCreditLimitBookmarkView.BuyerFinTotalEquityFirst1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(0) != null ? creditAppReqLineFin[0].BuyerFinTotalEquity : 0;
				buyerReviewCreditLimitBookmarkView.BuyerFinTotalEquitySecond1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(1) != null ? creditAppReqLineFin[1].BuyerFinTotalEquity : 0;
				buyerReviewCreditLimitBookmarkView.BuyerFinTotalEquityThird1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(2) != null ? creditAppReqLineFin[2].BuyerFinTotalEquity : 0;

				buyerReviewCreditLimitBookmarkView.BuyerFinTotalRevenueFirst1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(0) != null ? creditAppReqLineFin[0].BuyerFinTotalRevenue : 0;
				buyerReviewCreditLimitBookmarkView.BuyerFinTotalRevenueSecond1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(1) != null ? creditAppReqLineFin[1].BuyerFinTotalRevenue : 0;
				buyerReviewCreditLimitBookmarkView.BuyerFinTotalRevenueThird1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(2) != null ? creditAppReqLineFin[2].BuyerFinTotalRevenue : 0;

				buyerReviewCreditLimitBookmarkView.BuyerFinTotalCOGSFirst1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(0) != null ? creditAppReqLineFin[0].BuyerFinTotalCOGS : 0;
				buyerReviewCreditLimitBookmarkView.BuyerFinTotalCOGSSecond1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(1) != null ? creditAppReqLineFin[1].BuyerFinTotalCOGS : 0;
				buyerReviewCreditLimitBookmarkView.BuyerFinTotalCOGSThird1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(2) != null ? creditAppReqLineFin[2].BuyerFinTotalCOGS : 0;

				buyerReviewCreditLimitBookmarkView.BuyerFinTotalGrossProfitFirst1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(0) != null ? creditAppReqLineFin[0].BuyerFinTotalGrossProfit : 0;
				buyerReviewCreditLimitBookmarkView.BuyerFinTotalGrossProfitSecond1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(1) != null ? creditAppReqLineFin[1].BuyerFinTotalGrossProfit : 0;
				buyerReviewCreditLimitBookmarkView.BuyerFinTotalGrossProfitThird1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(2) != null ? creditAppReqLineFin[2].BuyerFinTotalGrossProfit : 0;

				buyerReviewCreditLimitBookmarkView.BuyerFinTotalOperExpFirst1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(0) != null ? creditAppReqLineFin[0].BuyerFinTotalOperExp : 0;
				buyerReviewCreditLimitBookmarkView.BuyerFinTotalOperExpSecond1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(1) != null ? creditAppReqLineFin[1].BuyerFinTotalOperExp : 0;
				buyerReviewCreditLimitBookmarkView.BuyerFinTotalOperExpThird1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(2) != null ? creditAppReqLineFin[2].BuyerFinTotalOperExp : 0;

				buyerReviewCreditLimitBookmarkView.BuyerFinTotalNetProfitFirst1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(0) != null ? creditAppReqLineFin[0].BuyerFinTotalNetProfit : 0;
				buyerReviewCreditLimitBookmarkView.BuyerFinTotalNetProfitSecond1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(1) != null ? creditAppReqLineFin[1].BuyerFinTotalNetProfit : 0;
				buyerReviewCreditLimitBookmarkView.BuyerFinTotalNetProfitThird1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(2) != null ? creditAppReqLineFin[2].BuyerFinTotalNetProfit : 0;

				buyerReviewCreditLimitBookmarkView.BuyerFinNetProfitPercent1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(0) != null ? creditAppReqLineFin[0].BuyerFinNetProfitPercent : 0;

				buyerReviewCreditLimitBookmarkView.BuyerFinlDE1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(0) != null ? creditAppReqLineFin[0].BuyerFinlDERatio : 0;

				buyerReviewCreditLimitBookmarkView.BuyerFinQuickRatio1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(0) != null ? creditAppReqLineFin[0].BuyerFinQuickRatio : 0;

				buyerReviewCreditLimitBookmarkView.BuyerFinIntCoverageRatio1_1st_1 = creditAppReqLineFin.ElementAtOrDefault(0) != null ? creditAppReqLineFin[0].BuyerFinIntCoverageRatio : 0;

				buyerReviewCreditLimitBookmarkView.BillingDocumentFirst1_1st_1 = billingDocumentList.Where(b => b.Row == 1).Select(b => b.BillingDocument).FirstOrDefault();
				buyerReviewCreditLimitBookmarkView.BillingDocumentSecond1_1st_1 = billingDocumentList.Where(b => b.Row == 2).Select(b => b.BillingDocument).FirstOrDefault();
				buyerReviewCreditLimitBookmarkView.BillingDocumentThird1_1st_1 = billingDocumentList.Where(b => b.Row == 3).Select(b => b.BillingDocument).FirstOrDefault();
				buyerReviewCreditLimitBookmarkView.BillingDocumentFourth_1st_1 = billingDocumentList.Where(b => b.Row == 4).Select(b => b.BillingDocument).FirstOrDefault();
				buyerReviewCreditLimitBookmarkView.BillingDocumentFifth_1st_1 = billingDocumentList.Where(b => b.Row == 5).Select(b => b.BillingDocument).FirstOrDefault();
				buyerReviewCreditLimitBookmarkView.BillingDocumentSixth1_1st_1 = billingDocumentList.Where(b => b.Row == 6).Select(b => b.BillingDocument).FirstOrDefault();
				buyerReviewCreditLimitBookmarkView.BillingDocumentSeventh_1st_1 = billingDocumentList.Where(b => b.Row == 7).Select(b => b.BillingDocument).FirstOrDefault();
				buyerReviewCreditLimitBookmarkView.BillingDocumentEigth1_1st_1 = billingDocumentList.Where(b => b.Row == 8).Select(b => b.BillingDocument).FirstOrDefault();
				buyerReviewCreditLimitBookmarkView.BillingDocumentNineth1_1st_1 = billingDocumentList.Where(b => b.Row == 9).Select(b => b.BillingDocument).FirstOrDefault();
				buyerReviewCreditLimitBookmarkView.BillingDocumentTenth_1st_1 = billingDocumentList.Where(b => b.Row == 10).Select(b => b.BillingDocument).FirstOrDefault();

				buyerReviewCreditLimitBookmarkView.ReceiptDocumentFirst1_1st_1 = receiptDocumentList.Where(b => b.Row == 1).Select(b => b.ReceiptDocument).FirstOrDefault();
				buyerReviewCreditLimitBookmarkView.ReceiptDocumentSecond1_1st_1 = receiptDocumentList.Where(b => b.Row == 2).Select(b => b.ReceiptDocument).FirstOrDefault();
				buyerReviewCreditLimitBookmarkView.ReceiptDocumentThird1_1st_1 = receiptDocumentList.Where(b => b.Row == 3).Select(b => b.ReceiptDocument).FirstOrDefault();
				buyerReviewCreditLimitBookmarkView.ReceiptDocumentFourth_1st_1 = receiptDocumentList.Where(b => b.Row == 4).Select(b => b.ReceiptDocument).FirstOrDefault();
				buyerReviewCreditLimitBookmarkView.ReceiptDocumentFifth_1st_1 = receiptDocumentList.Where(b => b.Row == 5).Select(b => b.ReceiptDocument).FirstOrDefault();
				buyerReviewCreditLimitBookmarkView.ReceiptDocumentSixth1_1st_1 = receiptDocumentList.Where(b => b.Row == 6).Select(b => b.ReceiptDocument).FirstOrDefault();
				buyerReviewCreditLimitBookmarkView.ReceiptDocumentSeventh_1st_1 = receiptDocumentList.Where(b => b.Row == 7).Select(b => b.ReceiptDocument).FirstOrDefault();
				buyerReviewCreditLimitBookmarkView.ReceiptDocumentEigth1_1st_1 = receiptDocumentList.Where(b => b.Row == 8).Select(b => b.ReceiptDocument).FirstOrDefault();
				buyerReviewCreditLimitBookmarkView.ReceiptDocumentNineth1_1st_1 = receiptDocumentList.Where(b => b.Row == 9).Select(b => b.ReceiptDocument).FirstOrDefault();
				buyerReviewCreditLimitBookmarkView.ReceiptDocumentTenth_1st_1 = receiptDocumentList.Where(b => b.Row == 10).Select(b => b.ReceiptDocument).FirstOrDefault();

				return buyerReviewCreditLimitBookmarkView;
			}

			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public CreditAppReqLineFinBookmarkView GetCreditAppReqLineFinBookmarkView(List<FinancialStatementTrans> financialStatementTrans, int year)
		{
			try
			{
				var financialStatementTransByYear = financialStatementTrans.Where(w => w.Year == year);
				CreditAppReqLineFinBookmarkView result = new CreditAppReqLineFinBookmarkView
				{
					Year = year,
					BuyerFinRegisteredCapital = financialStatementTransByYear.Where(w => w.Ordering == 1).Select(s => s.Amount).FirstOrDefault(),
					BuyerFinPaidCapital = financialStatementTransByYear.Where(w => w.Ordering == 2).Select(s => s.Amount).FirstOrDefault(),
					BuyerFinTotalAsset = financialStatementTransByYear.Where(w => w.Ordering == 3).Select(s => s.Amount).FirstOrDefault(),
					BuyerFinTotalLiability = financialStatementTransByYear.Where(w => w.Ordering == 4).Select(s => s.Amount).FirstOrDefault(),
					BuyerFinTotalEquity = financialStatementTransByYear.Where(w => w.Ordering == 5).Select(s => s.Amount).FirstOrDefault(),
					BuyerFinTotalRevenue = financialStatementTransByYear.Where(w => w.Ordering == 6).Select(s => s.Amount).FirstOrDefault(),
					BuyerFinTotalCOGS = financialStatementTransByYear.Where(w => w.Ordering == 7).Select(s => s.Amount).FirstOrDefault(),
					BuyerFinTotalGrossProfit = financialStatementTransByYear.Where(w => w.Ordering == 8).Select(s => s.Amount).FirstOrDefault(),
					BuyerFinTotalOperExp = financialStatementTransByYear.Where(w => w.Ordering == 9).Select(s => s.Amount).FirstOrDefault(),
					BuyerFinTotalNetProfit = financialStatementTransByYear.Where(w => w.Ordering == 10).Select(s => s.Amount).FirstOrDefault(),
					BuyerFinNetProfitPercent = financialStatementTransByYear.Where(w => w.Ordering == 11).Select(s => s.Amount).FirstOrDefault(),
					BuyerFinlDERatio = financialStatementTransByYear.Where(w => w.Ordering == 12).Select(s => s.Amount).FirstOrDefault(),
					BuyerFinQuickRatio = financialStatementTransByYear.Where(w => w.Ordering == 13).Select(s => s.Amount).FirstOrDefault(),
					BuyerFinIntCoverageRatio = financialStatementTransByYear.Where(w => w.Ordering == 14).Select(s => s.Amount).FirstOrDefault()
				};

				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}
