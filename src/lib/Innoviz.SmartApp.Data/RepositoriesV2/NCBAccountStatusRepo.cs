using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface INCBAccountStatusRepo
	{
		#region DropDown
		IEnumerable<SelectItem<NCBAccountStatusItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<NCBAccountStatusListView> GetListvw(SearchParameter search);
		NCBAccountStatusItemView GetByIdvw(Guid id);
		NCBAccountStatus Find(params object[] keyValues);
		NCBAccountStatus GetNCBAccountStatusByIdNoTracking(Guid guid);
		NCBAccountStatus CreateNCBAccountStatus(NCBAccountStatus ncbAccountStatus);
		void CreateNCBAccountStatusVoid(NCBAccountStatus ncbAccountStatus);
		NCBAccountStatus UpdateNCBAccountStatus(NCBAccountStatus dbNCBAccountStatus, NCBAccountStatus inputNCBAccountStatus, List<string> skipUpdateFields = null);
		void UpdateNCBAccountStatusVoid(NCBAccountStatus dbNCBAccountStatus, NCBAccountStatus inputNCBAccountStatus, List<string> skipUpdateFields = null);
		void Remove(NCBAccountStatus item);
	}
	public class NCBAccountStatusRepo : CompanyBaseRepository<NCBAccountStatus>, INCBAccountStatusRepo
	{
		public NCBAccountStatusRepo(SmartAppDbContext context) : base(context) { }
		public NCBAccountStatusRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public NCBAccountStatus GetNCBAccountStatusByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.NCBAccountStatusGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<NCBAccountStatusItemViewMap> GetDropDownQuery()
		{
			return (from ncbAccountStatus in Entity
					select new NCBAccountStatusItemViewMap
					{
						CompanyGUID = ncbAccountStatus.CompanyGUID,
						Owner = ncbAccountStatus.Owner,
						OwnerBusinessUnitGUID = ncbAccountStatus.OwnerBusinessUnitGUID,
						NCBAccountStatusGUID = ncbAccountStatus.NCBAccountStatusGUID,
						NCBAccStatusId = ncbAccountStatus.NCBAccStatusId,
						Description = ncbAccountStatus.Description
					});
		}
		public IEnumerable<SelectItem<NCBAccountStatusItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<NCBAccountStatus>(search, SysParm.CompanyGUID);
				var ncbAccountStatus = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<NCBAccountStatusItemViewMap, NCBAccountStatusItemView>().ToDropDownItem(search.ExcludeRowData);


				return ncbAccountStatus;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<NCBAccountStatusListViewMap> GetListQuery()
		{
			return (from ncbAccountStatus in Entity
				select new NCBAccountStatusListViewMap
				{
						CompanyGUID = ncbAccountStatus.CompanyGUID,
						Owner = ncbAccountStatus.Owner,
						OwnerBusinessUnitGUID = ncbAccountStatus.OwnerBusinessUnitGUID,
						NCBAccountStatusGUID = ncbAccountStatus.NCBAccountStatusGUID,
						NCBAccStatusId = ncbAccountStatus.NCBAccStatusId,
						Code = ncbAccountStatus.Code,
						Description = ncbAccountStatus.Description
						
				});
		}
		public SearchResult<NCBAccountStatusListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<NCBAccountStatusListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<NCBAccountStatus>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<NCBAccountStatusListViewMap, NCBAccountStatusListView>();
				result = list.SetSearchResult<NCBAccountStatusListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<NCBAccountStatusItemViewMap> GetItemQuery()
		{
			return (from ncbAccountStatus in Entity
					join company in db.Set<Company>()
					on ncbAccountStatus.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on ncbAccountStatus.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljNCBAccountStatusOwnerBU
					from ownerBU in ljNCBAccountStatusOwnerBU.DefaultIfEmpty()
					select new NCBAccountStatusItemViewMap
					{
						CompanyGUID = ncbAccountStatus.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = ncbAccountStatus.Owner,
						OwnerBusinessUnitGUID = ncbAccountStatus.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = ncbAccountStatus.CreatedBy,
						CreatedDateTime = ncbAccountStatus.CreatedDateTime,
						ModifiedBy = ncbAccountStatus.ModifiedBy,
						ModifiedDateTime = ncbAccountStatus.ModifiedDateTime,
						NCBAccountStatusGUID = ncbAccountStatus.NCBAccountStatusGUID,
						Code = ncbAccountStatus.Code,
						Description = ncbAccountStatus.Description,
						NCBAccStatusId = ncbAccountStatus.NCBAccStatusId,
					
						RowVersion = ncbAccountStatus.RowVersion,
					});
		}
		public NCBAccountStatusItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<NCBAccountStatusItemViewMap, NCBAccountStatusItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public NCBAccountStatus CreateNCBAccountStatus(NCBAccountStatus ncbAccountStatus)
		{
			try
			{
				ncbAccountStatus.NCBAccountStatusGUID = Guid.NewGuid();
				base.Add(ncbAccountStatus);
				return ncbAccountStatus;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateNCBAccountStatusVoid(NCBAccountStatus ncbAccountStatus)
		{
			try
			{
				CreateNCBAccountStatus(ncbAccountStatus);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public NCBAccountStatus UpdateNCBAccountStatus(NCBAccountStatus dbNCBAccountStatus, NCBAccountStatus inputNCBAccountStatus, List<string> skipUpdateFields = null)
		{
			try
			{
				dbNCBAccountStatus = dbNCBAccountStatus.MapUpdateValues<NCBAccountStatus>(inputNCBAccountStatus);
				base.Update(dbNCBAccountStatus);
				return dbNCBAccountStatus;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateNCBAccountStatusVoid(NCBAccountStatus dbNCBAccountStatus, NCBAccountStatus inputNCBAccountStatus, List<string> skipUpdateFields = null)
		{
			try
			{
				dbNCBAccountStatus = dbNCBAccountStatus.MapUpdateValues<NCBAccountStatus>(inputNCBAccountStatus, skipUpdateFields);
				base.Update(dbNCBAccountStatus);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

