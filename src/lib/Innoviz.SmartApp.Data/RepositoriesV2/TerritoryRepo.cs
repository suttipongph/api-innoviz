using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ITerritoryRepo
	{
		#region DropDown
		IEnumerable<SelectItem<TerritoryItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<TerritoryListView> GetListvw(SearchParameter search);
		TerritoryItemView GetByIdvw(Guid id);
		Territory Find(params object[] keyValues);
		Territory GetTerritoryByIdNoTracking(Guid guid);
		Territory CreateTerritory(Territory territory);
		void CreateTerritoryVoid(Territory territory);
		Territory UpdateTerritory(Territory dbTerritory, Territory inputTerritory, List<string> skipUpdateFields = null);
		void UpdateTerritoryVoid(Territory dbTerritory, Territory inputTerritory, List<string> skipUpdateFields = null);
		void Remove(Territory item);
	}
	public class TerritoryRepo : CompanyBaseRepository<Territory>, ITerritoryRepo
	{
		public TerritoryRepo(SmartAppDbContext context) : base(context) { }
		public TerritoryRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public Territory GetTerritoryByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.TerritoryGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<TerritoryItemViewMap> GetDropDownQuery()
		{
			return (from territory in Entity
					select new TerritoryItemViewMap
					{
						CompanyGUID = territory.CompanyGUID,
						Owner = territory.Owner,
						OwnerBusinessUnitGUID = territory.OwnerBusinessUnitGUID,
						TerritoryGUID = territory.TerritoryGUID,
						TerritoryId = territory.TerritoryId,
						Description = territory.Description
					});
		}
		public IEnumerable<SelectItem<TerritoryItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<Territory>(search, SysParm.CompanyGUID);
				var territory = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<TerritoryItemViewMap, TerritoryItemView>().ToDropDownItem(search.ExcludeRowData);


				return territory;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<TerritoryListViewMap> GetListQuery()
		{
			return (from territory in Entity
				select new TerritoryListViewMap
				{
						CompanyGUID = territory.CompanyGUID,
						Owner = territory.Owner,
						OwnerBusinessUnitGUID = territory.OwnerBusinessUnitGUID,
						TerritoryGUID = territory.TerritoryGUID,
						TerritoryId = territory.TerritoryId,
						Description = territory.Description
				});
		}
		public SearchResult<TerritoryListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<TerritoryListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<Territory>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<TerritoryListViewMap, TerritoryListView>();
				result = list.SetSearchResult<TerritoryListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<TerritoryItemViewMap> GetItemQuery()
		{
			return (from territory in Entity
					join company in db.Set<Company>()
					on territory.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on territory.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljTerritoryOwnerBU
					from ownerBU in ljTerritoryOwnerBU.DefaultIfEmpty()
					select new TerritoryItemViewMap
					{
						CompanyGUID = territory.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = territory.Owner,
						OwnerBusinessUnitGUID = territory.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = territory.CreatedBy,
						CreatedDateTime = territory.CreatedDateTime,
						ModifiedBy = territory.ModifiedBy,
						ModifiedDateTime = territory.ModifiedDateTime,
						TerritoryGUID = territory.TerritoryGUID,
						Description = territory.Description,
						TerritoryId = territory.TerritoryId,
					
						RowVersion = territory.RowVersion,
					});
		}
		public TerritoryItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<TerritoryItemViewMap, TerritoryItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public Territory CreateTerritory(Territory territory)
		{
			try
			{
				territory.TerritoryGUID = Guid.NewGuid();
				base.Add(territory);
				return territory;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateTerritoryVoid(Territory territory)
		{
			try
			{
				CreateTerritory(territory);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public Territory UpdateTerritory(Territory dbTerritory, Territory inputTerritory, List<string> skipUpdateFields = null)
		{
			try
			{
				dbTerritory = dbTerritory.MapUpdateValues<Territory>(inputTerritory);
				base.Update(dbTerritory);
				return dbTerritory;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateTerritoryVoid(Territory dbTerritory, Territory inputTerritory, List<string> skipUpdateFields = null)
		{
			try
			{
				dbTerritory = dbTerritory.MapUpdateValues<Territory>(inputTerritory, skipUpdateFields);
				base.Update(dbTerritory);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

