using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IExposureGroupRepo
	{
		#region DropDown
		IEnumerable<SelectItem<ExposureGroupItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<ExposureGroupListView> GetListvw(SearchParameter search);
		ExposureGroupItemView GetByIdvw(Guid id);
		ExposureGroup Find(params object[] keyValues);
		ExposureGroup GetExposureGroupByIdNoTracking(Guid guid);
		ExposureGroup CreateExposureGroup(ExposureGroup exposureGroup);
		void CreateExposureGroupVoid(ExposureGroup exposureGroup);
		ExposureGroup UpdateExposureGroup(ExposureGroup dbExposureGroup, ExposureGroup inputExposureGroup, List<string> skipUpdateFields = null);
		void UpdateExposureGroupVoid(ExposureGroup dbExposureGroup, ExposureGroup inputExposureGroup, List<string> skipUpdateFields = null);
		void Remove(ExposureGroup item);
		ExposureGroup GetExposureGroupByIdNoTrackingByAccessLevel(Guid guid);
	}
	public class ExposureGroupRepo : CompanyBaseRepository<ExposureGroup>, IExposureGroupRepo
	{
		public ExposureGroupRepo(SmartAppDbContext context) : base(context) { }
		public ExposureGroupRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ExposureGroup GetExposureGroupByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ExposureGroupGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<ExposureGroupItemViewMap> GetDropDownQuery()
		{
			return (from exposureGroup in Entity
					select new ExposureGroupItemViewMap
					{
						CompanyGUID = exposureGroup.CompanyGUID,
						Owner = exposureGroup.Owner,
						OwnerBusinessUnitGUID = exposureGroup.OwnerBusinessUnitGUID,
						ExposureGroupGUID = exposureGroup.ExposureGroupGUID,
						ExposureGroupId = exposureGroup.ExposureGroupId,
						Description = exposureGroup.Description
					});
		}
		public IEnumerable<SelectItem<ExposureGroupItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<ExposureGroup>(search, SysParm.CompanyGUID);
				var exposureGroup = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<ExposureGroupItemViewMap, ExposureGroupItemView>().ToDropDownItem(search.ExcludeRowData);


				return exposureGroup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<ExposureGroupListViewMap> GetListQuery()
		{
			return (from exposureGroup in Entity
				select new ExposureGroupListViewMap
				{
						CompanyGUID = exposureGroup.CompanyGUID,
						Owner = exposureGroup.Owner,
						OwnerBusinessUnitGUID = exposureGroup.OwnerBusinessUnitGUID,
						ExposureGroupGUID = exposureGroup.ExposureGroupGUID,
						ExposureGroupId = exposureGroup.ExposureGroupId,
						Description = exposureGroup.Description,
				});
		}
		public SearchResult<ExposureGroupListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<ExposureGroupListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<ExposureGroup>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<ExposureGroupListViewMap, ExposureGroupListView>();
				result = list.SetSearchResult<ExposureGroupListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<ExposureGroupItemViewMap> GetItemQuery()
		{
			return (from exposureGroup in Entity
					join company in db.Set<Company>()
					on exposureGroup.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on exposureGroup.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljExposureGroupOwnerBU
					from ownerBU in ljExposureGroupOwnerBU.DefaultIfEmpty()
					select new ExposureGroupItemViewMap
					{
						CompanyGUID = exposureGroup.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = exposureGroup.Owner,
						OwnerBusinessUnitGUID = exposureGroup.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = exposureGroup.CreatedBy,
						CreatedDateTime = exposureGroup.CreatedDateTime,
						ModifiedBy = exposureGroup.ModifiedBy,
						ModifiedDateTime = exposureGroup.ModifiedDateTime,
						ExposureGroupGUID = exposureGroup.ExposureGroupGUID,
						Description = exposureGroup.Description,
						ExposureGroupId = exposureGroup.ExposureGroupId,
					
						RowVersion = exposureGroup.RowVersion,
					});
		}
		public ExposureGroupItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<ExposureGroupItemViewMap, ExposureGroupItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public ExposureGroup CreateExposureGroup(ExposureGroup exposureGroup)
		{
			try
			{
				exposureGroup.ExposureGroupGUID = Guid.NewGuid();
				base.Add(exposureGroup);
				return exposureGroup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateExposureGroupVoid(ExposureGroup exposureGroup)
		{
			try
			{
				CreateExposureGroup(exposureGroup);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ExposureGroup UpdateExposureGroup(ExposureGroup dbExposureGroup, ExposureGroup inputExposureGroup, List<string> skipUpdateFields = null)
		{
			try
			{
				dbExposureGroup = dbExposureGroup.MapUpdateValues<ExposureGroup>(inputExposureGroup);
				base.Update(dbExposureGroup);
				return dbExposureGroup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateExposureGroupVoid(ExposureGroup dbExposureGroup, ExposureGroup inputExposureGroup, List<string> skipUpdateFields = null)
		{
			try
			{
				dbExposureGroup = dbExposureGroup.MapUpdateValues<ExposureGroup>(inputExposureGroup, skipUpdateFields);
				base.Update(dbExposureGroup);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public ExposureGroup GetExposureGroupByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ExposureGroupGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

