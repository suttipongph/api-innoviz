using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandler;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IBusinessUnitRepo
	{
		#region DropDown
		IEnumerable<SelectItem<BusinessUnitItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<BusinessUnitListView> GetListvw(SearchParameter search);
		BusinessUnitItemView GetByIdvw(Guid id);
		BusinessUnit Find(params object[] keyValues);
		BusinessUnit GetBusinessUnitByIdNoTracking(Guid guid);
		BusinessUnit GetBusinessUnitByIdNoTrackingByAccessLevel(Guid guid);
		BusinessUnit CreateBusinessUnit(BusinessUnit businessUnit);
		void CreateBusinessUnitVoid(BusinessUnit businessUnit);
		BusinessUnit UpdateBusinessUnit(BusinessUnit dbBusinessUnit, BusinessUnit inputBusinessUnit, List<string> skipUpdateFields = null);
		void UpdateBusinessUnitVoid(BusinessUnit dbBusinessUnit, BusinessUnit inputBusinessUnit, List<string> skipUpdateFields = null);
		void Remove(BusinessUnit item);
		IEnumerable<BusinessUnit> GetParentChildBusinessUnitByBusinessUnitGUID(Guid businessUnitGUID, Guid companyGUID);
		ITree<BusinessUnit> GetBUTreeByCompany(Guid? companyGUID);
		ITree<BusinessUnit> GetSubTreeByBusinessUnitGUID(ITree<BusinessUnit> treeNode, Guid buGuid);
		ITree<BusinessUnit> GetSubTreeByBusinessUnitGUID(Guid buGUID, Guid companyGUID);
		List<BusinessUnit> GetBusinessUnitByCompanyNoTracking(Guid companyGUID);
		BusinessUnit GetRootBusinessUnitByCompany(Guid companyGUID);
		List<BusinessUnit> GetAllRootBusinessUnits();
	}
	public class BusinessUnitRepo : CompanyBaseRepository<BusinessUnit>, IBusinessUnitRepo
	{
		public BusinessUnitRepo(SmartAppDbContext context) : base(context) { }
		public BusinessUnitRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public override void ValidateAdd(BusinessUnit item)
        {
            base.ValidateAdd(item);
			ValidateRootBusinessUnit(item);
        }
        public override void ValidateUpdate(BusinessUnit item)
        {
            base.ValidateUpdate(item);
			ValidateRootBusinessUnit(item);
			ValidateIsChildParentBusinessUnit(item);
        }
        public BusinessUnit GetBusinessUnitByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BusinessUnitGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BusinessUnit GetBusinessUnitByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BusinessUnitGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<BusinessUnitItemViewMap> GetDropDownQuery()
		{
			return (from businessUnit in Entity
					select new BusinessUnitItemViewMap
					{
						CompanyGUID = businessUnit.CompanyGUID,
						Owner = businessUnit.Owner,
						OwnerBusinessUnitGUID = businessUnit.OwnerBusinessUnitGUID,
						BusinessUnitGUID = businessUnit.BusinessUnitGUID,
						BusinessUnitId = businessUnit.BusinessUnitId,
						Description = businessUnit.Description
					});
		}
		public IEnumerable<SelectItem<BusinessUnitItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<BusinessUnit>(search, SysParm.CompanyGUID);
				var businessUnit = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<BusinessUnitItemViewMap, BusinessUnitItemView>().ToDropDownItem(search.ExcludeRowData);


				return businessUnit;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<BusinessUnitListViewMap> GetListQuery()
		{
			return (from businessUnit in Entity
					join parentBusiness in Entity
					on businessUnit.ParentBusinessUnitGUID equals parentBusiness.BusinessUnitGUID into ljParent
					from parentBusiness in ljParent.DefaultIfEmpty()

				select new BusinessUnitListViewMap
				{
						CompanyGUID = businessUnit.CompanyGUID,
						Owner = businessUnit.Owner,
						OwnerBusinessUnitGUID = businessUnit.OwnerBusinessUnitGUID,
						BusinessUnitGUID = businessUnit.BusinessUnitGUID,
						BusinessUnitId = businessUnit.BusinessUnitId,
						Description = businessUnit.Description,
						ParentBusinessUnitGUID = businessUnit.ParentBusinessUnitGUID,
						ParentBusinessUnit_Values = SmartAppUtil.GetDropDownLabel(parentBusiness.BusinessUnitId, parentBusiness.Description),
						ParentBusinessUnit_BusinessUnitId = parentBusiness.BusinessUnitId
				});
		}
		public SearchResult<BusinessUnitListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<BusinessUnitListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<BusinessUnit>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<BusinessUnitListViewMap, BusinessUnitListView>();
				result = list.SetSearchResult<BusinessUnitListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<BusinessUnitItemViewMap> GetItemQuery()
		{
			return (from businessUnit in Entity
					join company in db.Set<Company>()
					on businessUnit.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on businessUnit.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljBusinessUnitOwnerBU
					from ownerBU in ljBusinessUnitOwnerBU.DefaultIfEmpty()
					select new BusinessUnitItemViewMap
					{
						CompanyGUID = businessUnit.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = businessUnit.Owner,
						OwnerBusinessUnitGUID = businessUnit.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = businessUnit.CreatedBy,
						CreatedDateTime = businessUnit.CreatedDateTime,
						ModifiedBy = businessUnit.ModifiedBy,
						ModifiedDateTime = businessUnit.ModifiedDateTime,
						BusinessUnitGUID = businessUnit.BusinessUnitGUID,
						BusinessUnitId = businessUnit.BusinessUnitId,
						Description = businessUnit.Description,
						ParentBusinessUnitGUID = businessUnit.ParentBusinessUnitGUID,
					
						RowVersion = businessUnit.RowVersion,
					});
		}
		public BusinessUnitItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<BusinessUnitItemViewMap, BusinessUnitItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public BusinessUnit CreateBusinessUnit(BusinessUnit businessUnit)
		{
			try
			{
				businessUnit.BusinessUnitGUID = Guid.NewGuid();
				base.Add(businessUnit);
				return businessUnit;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateBusinessUnitVoid(BusinessUnit businessUnit)
		{
			try
			{
				CreateBusinessUnit(businessUnit);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BusinessUnit UpdateBusinessUnit(BusinessUnit dbBusinessUnit, BusinessUnit inputBusinessUnit, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBusinessUnit = dbBusinessUnit.MapUpdateValues<BusinessUnit>(inputBusinessUnit);
				base.Update(dbBusinessUnit);
				return dbBusinessUnit;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateBusinessUnitVoid(BusinessUnit dbBusinessUnit, BusinessUnit inputBusinessUnit, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBusinessUnit = dbBusinessUnit.MapUpdateValues<BusinessUnit>(inputBusinessUnit, skipUpdateFields);
				base.Update(dbBusinessUnit);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		
		public IEnumerable<BusinessUnit> GetParentChildBusinessUnitByBusinessUnitGUID(Guid businessUnitGUID, Guid companyGUID)
        {
            try
            {
				var subTree = GetSubTreeByBusinessUnitGUID(businessUnitGUID, companyGUID);
				if(subTree != null)
                {
					var treeNodes = new List<ITree<BusinessUnit>> { subTree };
					var flattened = treeNodes.Flatten(node => node.Children).ToList();
					return flattened.Select(s => s.Data);
                }
				else
                {
					return new List<BusinessUnit>();
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public ITree<BusinessUnit> GetBUTreeByCompany(Guid? companyGUID)
		{
			try
			{
				var query = Entity.Where(item => item.CompanyGUID == companyGUID)
								.Include(item => item.ParentBusinessUnit)
								.AsNoTracking()
								.ToList();

				return query.ToTree((parent, child) => child.ParentBusinessUnitGUID == parent.BusinessUnitGUID);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ITree<BusinessUnit> GetSubTreeByBusinessUnitGUID(Guid buGUID, Guid companyGUID)
        {
            try
            {
				var wholeTree = GetBUTreeByCompany(companyGUID);
				return GetSubTreeByBusinessUnitGUID(wholeTree, buGUID);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public ITree<BusinessUnit> GetSubTreeByBusinessUnitGUID(ITree<BusinessUnit> treeNode, Guid buGuid)
		{
			try
			{
				if (treeNode.Data != null && treeNode.Data.BusinessUnitGUID == buGuid)
				{
					return treeNode;
				}
				else
				{
					if (treeNode.Children.Count() > 0)
					{
						for (int i = 0; i < treeNode.Children.Count(); i++)
						{
							var fromChildNode = GetSubTreeByBusinessUnitGUID(treeNode.Children.ElementAt(i), buGuid);
							if (fromChildNode != null)
							{
								return fromChildNode;
							}
						}
						return null;
					}
					else
					{
						return null;
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool ValidateRootBusinessUnit(BusinessUnit item)
        {
            try
            {
				Guid companyGUID = SysParm.CompanyGUID.StringToGuid();
				var hasAnyRoot = Entity.Where(w => w.ParentBusinessUnitGUID == null &&
										w.BusinessUnitGUID != item.BusinessUnitGUID &&
										w.CompanyGUID == companyGUID)
								.AsNoTracking()
								.Count() > 0;
				if(hasAnyRoot && item.ParentBusinessUnitGUID == null)
                {
					SmartAppException ex = new SmartAppException("ERROR.ERROR");
					ex.AddData("ERROR.90032", "LABEL.BUSINESS_UNIT");
					throw ex;
                }
				else
                {
					return true;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public bool ValidateIsChildParentBusinessUnit(BusinessUnit item)
        {
            try
            {
				if(item.ParentBusinessUnitGUID != null)
                {
					Guid companyGUID = SysParm.CompanyGUID.StringToGuid();
					var parentChildBusinessUnits = GetParentChildBusinessUnitByBusinessUnitGUID(item.BusinessUnitGUID, companyGUID);
					bool isDescendant = parentChildBusinessUnits.Any(a => a.BusinessUnitGUID == item.ParentBusinessUnitGUID);
					if (isDescendant)
					{
						SmartAppException ex = new SmartAppException("ERROR.ERROR");
						ex.AddData("ERROR.90033", "LABEL.PARENT_BUSINESS_UNIT_ID");
						throw ex;
					}
					else
					{
						return true;
					}
				}
				else
                {
					return true;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public List<BusinessUnit> GetBusinessUnitByCompanyNoTracking(Guid companyGUID)
        {
            try
            {
				return Entity.Where(w => w.CompanyGUID == companyGUID)
							.AsNoTracking()
							.ToList();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public BusinessUnit GetRootBusinessUnitByCompany(Guid companyGUID)
        {
            try
            {
				var result = Entity.Where(w => w.CompanyGUID == companyGUID && !w.ParentBusinessUnitGUID.HasValue)
								.AsNoTracking()
								.FirstOrDefault();
				return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public List<BusinessUnit> GetAllRootBusinessUnits()
        {
            try
            {
				var result = Entity.Where(w => !w.ParentBusinessUnitGUID.HasValue)
									.AsNoTracking()
									.ToList();
				return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
	}
}

