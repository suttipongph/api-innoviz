using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IEmplTeamRepo
	{
		#region DropDown
		IEnumerable<SelectItem<EmplTeamItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<EmplTeamListView> GetListvw(SearchParameter search);
		EmplTeamItemView GetByIdvw(Guid id);
		EmplTeam Find(params object[] keyValues);
		EmplTeam GetEmplTeamByIdNoTracking(Guid guid);
		EmplTeam CreateEmplTeam(EmplTeam emplTeam);
		void CreateEmplTeamVoid(EmplTeam emplTeam);
		EmplTeam UpdateEmplTeam(EmplTeam dbEmplTeam, EmplTeam inputEmplTeam, List<string> skipUpdateFields = null);
		void UpdateEmplTeamVoid(EmplTeam dbEmplTeam, EmplTeam inputEmplTeam, List<string> skipUpdateFields = null);
		void Remove(EmplTeam item);
	}
	public class EmplTeamRepo : CompanyBaseRepository<EmplTeam>, IEmplTeamRepo
	{
		public EmplTeamRepo(SmartAppDbContext context) : base(context) { }
		public EmplTeamRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public EmplTeam GetEmplTeamByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.EmplTeamGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<EmplTeamItemViewMap> GetDropDownQuery()
		{
			return (from emplTeam in Entity
					select new EmplTeamItemViewMap
					{
						CompanyGUID = emplTeam.CompanyGUID,
						Owner = emplTeam.Owner,
						OwnerBusinessUnitGUID = emplTeam.OwnerBusinessUnitGUID,
						EmplTeamGUID = emplTeam.EmplTeamGUID,
						TeamId = emplTeam.TeamId,
						Name = emplTeam.Name
					});
		}
		public IEnumerable<SelectItem<EmplTeamItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<EmplTeam>(search, SysParm.CompanyGUID);
				var emplTeam = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<EmplTeamItemViewMap, EmplTeamItemView>().ToDropDownItem(search.ExcludeRowData);


				return emplTeam;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<EmplTeamListViewMap> GetListQuery()
		{
			return (from emplTeam in Entity
				select new EmplTeamListViewMap
				{
						CompanyGUID = emplTeam.CompanyGUID,
						Owner = emplTeam.Owner,
						OwnerBusinessUnitGUID = emplTeam.OwnerBusinessUnitGUID,
						EmplTeamGUID = emplTeam.EmplTeamGUID,
						TeamId = emplTeam.TeamId,
						Name = emplTeam.Name
				});
		}
		public SearchResult<EmplTeamListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<EmplTeamListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<EmplTeam>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<EmplTeamListViewMap, EmplTeamListView>();
				result = list.SetSearchResult<EmplTeamListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<EmplTeamItemViewMap> GetItemQuery()
		{
			return (from emplTeam in Entity
					join company in db.Set<Company>()
					on emplTeam.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on emplTeam.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljEmplTeamOwnerBU
					from ownerBU in ljEmplTeamOwnerBU.DefaultIfEmpty()
					select new EmplTeamItemViewMap
					{
						CompanyGUID = emplTeam.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = emplTeam.Owner,
						OwnerBusinessUnitGUID = emplTeam.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = emplTeam.CreatedBy,
						CreatedDateTime = emplTeam.CreatedDateTime,
						ModifiedBy = emplTeam.ModifiedBy,
						ModifiedDateTime = emplTeam.ModifiedDateTime,
						EmplTeamGUID = emplTeam.EmplTeamGUID,
						Name = emplTeam.Name,
						TeamId = emplTeam.TeamId,
					
						RowVersion = emplTeam.RowVersion,
					});
		}
		public EmplTeamItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<EmplTeamItemViewMap, EmplTeamItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public EmplTeam CreateEmplTeam(EmplTeam emplTeam)
		{
			try
			{
				emplTeam.EmplTeamGUID = Guid.NewGuid();
				base.Add(emplTeam);
				return emplTeam;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateEmplTeamVoid(EmplTeam emplTeam)
		{
			try
			{
				CreateEmplTeam(emplTeam);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public EmplTeam UpdateEmplTeam(EmplTeam dbEmplTeam, EmplTeam inputEmplTeam, List<string> skipUpdateFields = null)
		{
			try
			{
				dbEmplTeam = dbEmplTeam.MapUpdateValues<EmplTeam>(inputEmplTeam);
				base.Update(dbEmplTeam);
				return dbEmplTeam;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateEmplTeamVoid(EmplTeam dbEmplTeam, EmplTeam inputEmplTeam, List<string> skipUpdateFields = null)
		{
			try
			{
				dbEmplTeam = dbEmplTeam.MapUpdateValues<EmplTeam>(inputEmplTeam, skipUpdateFields);
				base.Update(dbEmplTeam);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

