using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IBankGroupRepo
	{
		#region DropDown
		IEnumerable<SelectItem<BankGroupItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<BankGroupListView> GetListvw(SearchParameter search);
		BankGroupItemView GetByIdvw(Guid id);
		BankGroup Find(params object[] keyValues);
		BankGroup GetBankGroupByIdNoTracking(Guid guid);
		BankGroup CreateBankGroup(BankGroup bankGroup);
		void CreateBankGroupVoid(BankGroup bankGroup);
		BankGroup UpdateBankGroup(BankGroup dbBankGroup, BankGroup inputBankGroup, List<string> skipUpdateFields = null);
		void UpdateBankGroupVoid(BankGroup dbBankGroup, BankGroup inputBankGroup, List<string> skipUpdateFields = null);
		void Remove(BankGroup item);
		List<BankGroup> GetBankGroupByCompanyNoTracking(Guid companyGUID);
	}
	public class BankGroupRepo : CompanyBaseRepository<BankGroup>, IBankGroupRepo
	{
		public BankGroupRepo(SmartAppDbContext context) : base(context) { }
		public BankGroupRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BankGroup GetBankGroupByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BankGroupGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<BankGroupItemViewMap> GetDropDownQuery()
		{
			return (from bankGroup in Entity
					select new BankGroupItemViewMap
					{
						CompanyGUID = bankGroup.CompanyGUID,
						Owner = bankGroup.Owner,
						OwnerBusinessUnitGUID = bankGroup.OwnerBusinessUnitGUID,
						BankGroupGUID = bankGroup.BankGroupGUID,
						BankGroupId = bankGroup.BankGroupId,
						Description = bankGroup.Description
					});
		}
		public IEnumerable<SelectItem<BankGroupItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<BankGroup>(search, SysParm.CompanyGUID);
				var bankGroup = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<BankGroupItemViewMap, BankGroupItemView>().ToDropDownItem(search.ExcludeRowData);


				return bankGroup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<BankGroupListViewMap> GetListQuery()
		{
			return (from bankGroup in Entity
				select new BankGroupListViewMap
				{
						CompanyGUID = bankGroup.CompanyGUID,
						Owner = bankGroup.Owner,
						OwnerBusinessUnitGUID = bankGroup.OwnerBusinessUnitGUID,
						BankGroupGUID = bankGroup.BankGroupGUID,
						BankGroupId = bankGroup.BankGroupId,
						Alias = bankGroup.Alias,
						Description = bankGroup.Description,
				});
		}
		public SearchResult<BankGroupListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<BankGroupListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<BankGroup>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<BankGroupListViewMap, BankGroupListView>();
				result = list.SetSearchResult<BankGroupListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<BankGroupItemViewMap> GetItemQuery()
		{
			return (from bankGroup in Entity
					join company in db.Set<Company>()
					on bankGroup.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on bankGroup.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljBankGroupOwnerBU
					from ownerBU in ljBankGroupOwnerBU.DefaultIfEmpty()
					select new BankGroupItemViewMap
					{
						CompanyGUID = bankGroup.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = bankGroup.Owner,
						OwnerBusinessUnitGUID = bankGroup.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = bankGroup.CreatedBy,
						CreatedDateTime = bankGroup.CreatedDateTime,
						ModifiedBy = bankGroup.ModifiedBy,
						ModifiedDateTime = bankGroup.ModifiedDateTime,
						BankGroupGUID = bankGroup.BankGroupGUID,
						Alias = bankGroup.Alias,
						BankGroupId = bankGroup.BankGroupId,
						Description = bankGroup.Description,
					
						RowVersion = bankGroup.RowVersion,
					});
		}
		public BankGroupItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<BankGroupItemViewMap, BankGroupItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public BankGroup CreateBankGroup(BankGroup bankGroup)
		{
			try
			{
				bankGroup.BankGroupGUID = Guid.NewGuid();
				base.Add(bankGroup);
				return bankGroup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateBankGroupVoid(BankGroup bankGroup)
		{
			try
			{
				CreateBankGroup(bankGroup);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BankGroup UpdateBankGroup(BankGroup dbBankGroup, BankGroup inputBankGroup, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBankGroup = dbBankGroup.MapUpdateValues<BankGroup>(inputBankGroup);
				base.Update(dbBankGroup);
				return dbBankGroup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateBankGroupVoid(BankGroup dbBankGroup, BankGroup inputBankGroup, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBankGroup = dbBankGroup.MapUpdateValues<BankGroup>(inputBankGroup, skipUpdateFields);
				base.Update(dbBankGroup);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public List<BankGroup> GetBankGroupByCompanyNoTracking(Guid companyGUID)
		{
			try
			{
				List<BankGroup> bankGroups = Entity.Where(w => w.CompanyGUID == companyGUID).AsNoTracking().ToList();
				return bankGroups;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

