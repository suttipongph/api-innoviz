using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IReceiptTempPaymDetailRepo
	{
		#region DropDown
		IEnumerable<SelectItem<ReceiptTempPaymDetailItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<ReceiptTempPaymDetailListView> GetListvw(SearchParameter search);
		ReceiptTempPaymDetailItemView GetByIdvw(Guid id);
		ReceiptTempPaymDetail Find(params object[] keyValues);
		ReceiptTempPaymDetail GetReceiptTempPaymDetailByIdNoTracking(Guid guid);
		ReceiptTempPaymDetail CreateReceiptTempPaymDetail(ReceiptTempPaymDetail receiptTempPaymDetail);
		void CreateReceiptTempPaymDetailVoid(ReceiptTempPaymDetail receiptTempPaymDetail);
		ReceiptTempPaymDetail UpdateReceiptTempPaymDetail(ReceiptTempPaymDetail dbReceiptTempPaymDetail, ReceiptTempPaymDetail inputReceiptTempPaymDetail, List<string> skipUpdateFields = null);
		void UpdateReceiptTempPaymDetailVoid(ReceiptTempPaymDetail dbReceiptTempPaymDetail, ReceiptTempPaymDetail inputReceiptTempPaymDetail, List<string> skipUpdateFields = null);
		void Remove(ReceiptTempPaymDetail item);
		ReceiptTempPaymDetail GetReceiptTempPaymDetailByIdNoTrackingByAccessLevel(Guid guid);
		IEnumerable<ReceiptTempPaymDetail> GetReceiptTempPaymDetailByReceiptTempNoTracking(Guid receiptTempTableGUID);
		List<ReceiptTempPaymDetail> GetReceiptTempPaymDetailByReceiptTempNoTracking(List<Guid> receiptTempTableGUID);
		IEnumerable<ReceiptTempPaymDetail> GetReceiptTempPaymDetailByMethodOfPaymentNoTracking(Guid methodOfPaymentGUID);
	}
	public class ReceiptTempPaymDetailRepo : CompanyBaseRepository<ReceiptTempPaymDetail>, IReceiptTempPaymDetailRepo
	{
		public ReceiptTempPaymDetailRepo(SmartAppDbContext context) : base(context) { }
		public ReceiptTempPaymDetailRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ReceiptTempPaymDetail GetReceiptTempPaymDetailByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ReceiptTempPaymDetailGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<ReceiptTempPaymDetailItemViewMap> GetDropDownQuery()
		{
			return (from receiptTempPaymDetail in Entity
					select new ReceiptTempPaymDetailItemViewMap
					{
						CompanyGUID = receiptTempPaymDetail.CompanyGUID,
						Owner = receiptTempPaymDetail.Owner,
						OwnerBusinessUnitGUID = receiptTempPaymDetail.OwnerBusinessUnitGUID,
						ReceiptTempPaymDetailGUID = receiptTempPaymDetail.ReceiptTempPaymDetailGUID,
						ReceiptTempTableGUID = receiptTempPaymDetail.ReceiptTempTableGUID,
						MethodOfPaymentGUID = receiptTempPaymDetail.MethodOfPaymentGUID
					});
		}
		public IEnumerable<SelectItem<ReceiptTempPaymDetailItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<ReceiptTempPaymDetail>(search, SysParm.CompanyGUID);
				var receiptTempPaymDetail = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<ReceiptTempPaymDetailItemViewMap, ReceiptTempPaymDetailItemView>().ToDropDownItem(search.ExcludeRowData);


				return receiptTempPaymDetail;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<ReceiptTempPaymDetailListViewMap> GetListQuery()
		{
			return (from receiptTempPaymDetail in Entity
					join methodOfPayment in db.Set<MethodOfPayment>()
					on receiptTempPaymDetail.MethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID
					
					join chequeTable in db.Set<ChequeTable>()
					on receiptTempPaymDetail.ChequeTableGUID equals chequeTable.ChequeTableGUID into ljChequeTable
					from chequeTable in ljChequeTable.DefaultIfEmpty()

					join invoiceSettlementDetail in db.Set<InvoiceSettlementDetail>().Where(w=>w.RefReceiptTempPaymDetailGUID.HasValue).GroupBy(g=> g.RefReceiptTempPaymDetailGUID).Select(s=> new { RefReceiptTempPaymDetailGUID = s.Key })
					on receiptTempPaymDetail.ReceiptTempPaymDetailGUID equals invoiceSettlementDetail.RefReceiptTempPaymDetailGUID into ljInvoiceSettlementDetail
					from invoiceSettlementDetail in ljInvoiceSettlementDetail.DefaultIfEmpty()
					select new ReceiptTempPaymDetailListViewMap
					{
						CompanyGUID = receiptTempPaymDetail.CompanyGUID,
						Owner = receiptTempPaymDetail.Owner,
						OwnerBusinessUnitGUID = receiptTempPaymDetail.OwnerBusinessUnitGUID,
						ReceiptTempPaymDetailGUID = receiptTempPaymDetail.ReceiptTempPaymDetailGUID,
						MethodOfPaymentGUID = receiptTempPaymDetail.MethodOfPaymentGUID,
						ReceiptAmount = receiptTempPaymDetail.ReceiptAmount,
						ChequeTableGUID = receiptTempPaymDetail.ChequeTableGUID,
						MethodOfPayment_MethodOfPaymentId = methodOfPayment.MethodOfPaymentId,
						ReceiptTempTableGUID = receiptTempPaymDetail.ReceiptTempTableGUID,
						MethodOfPayment_Values = SmartAppUtil.GetDropDownLabel(methodOfPayment.MethodOfPaymentId, methodOfPayment.Description),
						InvoiceSettlementDetail_RefReceiptTempPaymDetailGUID = (invoiceSettlementDetail != null) ? invoiceSettlementDetail.RefReceiptTempPaymDetailGUID: null,
						ChequeTable_Values = (chequeTable != null) ? SmartAppUtil.GetDropDownLabel(chequeTable.ChequeNo, chequeTable.ChequeDate.DateToString()) : null,
					});
		}
		public SearchResult<ReceiptTempPaymDetailListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<ReceiptTempPaymDetailListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<ReceiptTempPaymDetail>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<ReceiptTempPaymDetailListViewMap, ReceiptTempPaymDetailListView>();
				result = list.SetSearchResult<ReceiptTempPaymDetailListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw 
		private IQueryable<ReceiptTempPaymDetailItemViewMap> GetItemQuery()
		{
			return (from receiptTempPaymDetail in Entity
					join company in db.Set<Company>()
					on receiptTempPaymDetail.CompanyGUID equals company.CompanyGUID

					join methodOfPayment in db.Set<MethodOfPayment>()
					on receiptTempPaymDetail.MethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID

					join receiptTempTable in db.Set<ReceiptTempTable>()
					on receiptTempPaymDetail.ReceiptTempTableGUID equals receiptTempTable.ReceiptTempTableGUID

					join companyBank in db.Set<CompanyBank>()
					on methodOfPayment.CompanyBankGUID equals companyBank.CompanyBankGUID into ljCompanyBank
					from companyBank in ljCompanyBank.DefaultIfEmpty()

					join bankGroup in db.Set<BankGroup>()
					on receiptTempPaymDetail.ChequeBankGroupGUID equals bankGroup.BankGroupGUID into ljBankGroup
					from bankGroup in ljBankGroup.DefaultIfEmpty()

					join ownerBU in db.Set<BusinessUnit>()
					on receiptTempPaymDetail.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljReceiptTempPaymDetailOwnerBU
					from ownerBU in ljReceiptTempPaymDetailOwnerBU.DefaultIfEmpty()
					select new ReceiptTempPaymDetailItemViewMap
					{
						CompanyGUID = receiptTempPaymDetail.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = receiptTempPaymDetail.Owner,
						OwnerBusinessUnitGUID = receiptTempPaymDetail.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = receiptTempPaymDetail.CreatedBy,
						CreatedDateTime = receiptTempPaymDetail.CreatedDateTime,
						ModifiedBy = receiptTempPaymDetail.ModifiedBy,
						ModifiedDateTime = receiptTempPaymDetail.ModifiedDateTime,
						ReceiptTempPaymDetailGUID = receiptTempPaymDetail.ReceiptTempPaymDetailGUID,
						ChequeBankGroupGUID = receiptTempPaymDetail.ChequeBankGroupGUID,
						ChequeBranch = receiptTempPaymDetail.ChequeBranch,
						ChequeTableGUID = receiptTempPaymDetail.ChequeTableGUID,
						MethodOfPaymentGUID = receiptTempPaymDetail.MethodOfPaymentGUID,
						ReceiptAmount = receiptTempPaymDetail.ReceiptAmount,
						ReceiptTempTableGUID = receiptTempPaymDetail.ReceiptTempTableGUID,
						TransferReference = receiptTempPaymDetail.TransferReference,
						MethodOfPayment_PaymentType = methodOfPayment.PaymentType,
						CompanyBank_Values = SmartAppUtil.GetDropDownLabel(bankGroup.BankGroupId, companyBank.BankAccountName),
						ReceiptTempTable_ProductType = receiptTempTable.ProductType,
						ReceiptTempTable_BuyerTableGUID = receiptTempTable.BuyerTableGUID,
						ReceiptTempTable_CustomerTableGUID = receiptTempTable.CustomerTableGUID,
						ReceiptTempTable_ReceivedFrom = receiptTempTable.ReceivedFrom,
						ReceiptTempTable_ReceiptTempId = receiptTempTable.ReceiptTempId,
					
						RowVersion = receiptTempPaymDetail.RowVersion,
					});
		}
		public ReceiptTempPaymDetailItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<ReceiptTempPaymDetailItemViewMap, ReceiptTempPaymDetailItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public ReceiptTempPaymDetail CreateReceiptTempPaymDetail(ReceiptTempPaymDetail receiptTempPaymDetail)
		{
			try
			{
				receiptTempPaymDetail.ReceiptTempPaymDetailGUID = Guid.NewGuid();
				base.Add(receiptTempPaymDetail);
				return receiptTempPaymDetail;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateReceiptTempPaymDetailVoid(ReceiptTempPaymDetail receiptTempPaymDetail)
		{
			try
			{
				CreateReceiptTempPaymDetail(receiptTempPaymDetail);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ReceiptTempPaymDetail UpdateReceiptTempPaymDetail(ReceiptTempPaymDetail dbReceiptTempPaymDetail, ReceiptTempPaymDetail inputReceiptTempPaymDetail, List<string> skipUpdateFields = null)
		{
			try
			{
				dbReceiptTempPaymDetail = dbReceiptTempPaymDetail.MapUpdateValues<ReceiptTempPaymDetail>(inputReceiptTempPaymDetail);
				base.Update(dbReceiptTempPaymDetail);
				return dbReceiptTempPaymDetail;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateReceiptTempPaymDetailVoid(ReceiptTempPaymDetail dbReceiptTempPaymDetail, ReceiptTempPaymDetail inputReceiptTempPaymDetail, List<string> skipUpdateFields = null)
		{
			try
			{
				dbReceiptTempPaymDetail = dbReceiptTempPaymDetail.MapUpdateValues<ReceiptTempPaymDetail>(inputReceiptTempPaymDetail, skipUpdateFields);
				base.Update(dbReceiptTempPaymDetail);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public ReceiptTempPaymDetail GetReceiptTempPaymDetailByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ReceiptTempPaymDetailGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<ReceiptTempPaymDetail> GetReceiptTempPaymDetailByReceiptTempNoTracking(Guid receiptTempTableGUID)
		{
			try
			{
				var result = Entity.Where(item => item.ReceiptTempTableGUID == receiptTempTableGUID)
					.AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<ReceiptTempPaymDetail> GetReceiptTempPaymDetailByReceiptTempNoTracking(List<Guid> receiptTempTableGUID)
        {
            try
            {
				List<ReceiptTempPaymDetail> receiptTempPaymDetails = Entity.Where(w => receiptTempTableGUID.Contains(w.ReceiptTempTableGUID))
																			.AsNoTracking().ToList();
				return receiptTempPaymDetails;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<ReceiptTempPaymDetail> GetReceiptTempPaymDetailByMethodOfPaymentNoTracking(Guid methodOfPaymentGUID)
		{
			try
			{
				var result = Entity.Where(item => item.MethodOfPaymentGUID == methodOfPaymentGUID)
					.AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

