using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IStagingTransTextRepo
	{
		#region DropDown
		IEnumerable<SelectItem<StagingTransTextItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<StagingTransTextListView> GetListvw(SearchParameter search);
		StagingTransTextItemView GetByIdvw(Guid id);
		StagingTransText Find(params object[] keyValues);
		StagingTransText GetStagingTransTextByIdNoTracking(Guid guid);
		StagingTransText CreateStagingTransText(StagingTransText stagingTransText);
		void CreateStagingTransTextVoid(StagingTransText stagingTransText);
		StagingTransText UpdateStagingTransText(StagingTransText dbStagingTransText, StagingTransText inputStagingTransText, List<string> skipUpdateFields = null);
		void UpdateStagingTransTextVoid(StagingTransText dbStagingTransText, StagingTransText inputStagingTransText, List<string> skipUpdateFields = null);
		void Remove(StagingTransText item);
		StagingTransText GetStagingTransTextByIdNoTrackingByAccessLevel(Guid guid);
		List<StagingTransText> GetStagingTransTextByProcessTransTypeNoTracking(List<int> processTransType);
	}
	public class StagingTransTextRepo : CompanyBaseRepository<StagingTransText>, IStagingTransTextRepo
	{
		public StagingTransTextRepo(SmartAppDbContext context) : base(context) { }
		public StagingTransTextRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public StagingTransText GetStagingTransTextByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.StagingTransTextGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<StagingTransTextItemViewMap> GetDropDownQuery()
		{
			return (from stagingTransText in Entity
					select new StagingTransTextItemViewMap
					{
						CompanyGUID = stagingTransText.CompanyGUID,
						Owner = stagingTransText.Owner,
						OwnerBusinessUnitGUID = stagingTransText.OwnerBusinessUnitGUID,
						StagingTransTextGUID = stagingTransText.StagingTransTextGUID,
						ProcessTransType = stagingTransText.ProcessTransType,
						TransText = stagingTransText.TransText
					});
		}
		public IEnumerable<SelectItem<StagingTransTextItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<StagingTransText>(search, SysParm.CompanyGUID);
				var stagingTransText = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<StagingTransTextItemViewMap, StagingTransTextItemView>().ToDropDownItem(search.ExcludeRowData);


				return stagingTransText;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<StagingTransTextListViewMap> GetListQuery()
		{
			return (from stagingTransText in Entity
				select new StagingTransTextListViewMap
				{
						CompanyGUID = stagingTransText.CompanyGUID,
						Owner = stagingTransText.Owner,
						OwnerBusinessUnitGUID = stagingTransText.OwnerBusinessUnitGUID,
						StagingTransTextGUID = stagingTransText.StagingTransTextGUID,
						ProcessTransType = stagingTransText.ProcessTransType,
						TransText = stagingTransText.TransText,
				});
		}
		public SearchResult<StagingTransTextListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<StagingTransTextListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<StagingTransText>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<StagingTransTextListViewMap, StagingTransTextListView>();
				result = list.SetSearchResult<StagingTransTextListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<StagingTransTextItemViewMap> GetItemQuery()
		{
			return (from stagingTransText in Entity
					join company in db.Set<Company>()
					on stagingTransText.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on stagingTransText.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljStagingTransTextOwnerBU
					from ownerBU in ljStagingTransTextOwnerBU.DefaultIfEmpty()
					select new StagingTransTextItemViewMap
					{
						CompanyGUID = stagingTransText.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = stagingTransText.Owner,
						OwnerBusinessUnitGUID = stagingTransText.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = stagingTransText.CreatedBy,
						CreatedDateTime = stagingTransText.CreatedDateTime,
						ModifiedBy = stagingTransText.ModifiedBy,
						ModifiedDateTime = stagingTransText.ModifiedDateTime,
						StagingTransTextGUID = stagingTransText.StagingTransTextGUID,
						ProcessTransType = stagingTransText.ProcessTransType,
						TransText = stagingTransText.TransText,
					
						RowVersion = stagingTransText.RowVersion,
					});
		}
		public StagingTransTextItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<StagingTransTextItemViewMap, StagingTransTextItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public StagingTransText CreateStagingTransText(StagingTransText stagingTransText)
		{
			try
			{
				stagingTransText.StagingTransTextGUID = Guid.NewGuid();
				base.Add(stagingTransText);
				return stagingTransText;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateStagingTransTextVoid(StagingTransText stagingTransText)
		{
			try
			{
				CreateStagingTransText(stagingTransText);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public StagingTransText UpdateStagingTransText(StagingTransText dbStagingTransText, StagingTransText inputStagingTransText, List<string> skipUpdateFields = null)
		{
			try
			{
				dbStagingTransText = dbStagingTransText.MapUpdateValues<StagingTransText>(inputStagingTransText);
				base.Update(dbStagingTransText);
				return dbStagingTransText;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateStagingTransTextVoid(StagingTransText dbStagingTransText, StagingTransText inputStagingTransText, List<string> skipUpdateFields = null)
		{
			try
			{
				dbStagingTransText = dbStagingTransText.MapUpdateValues<StagingTransText>(inputStagingTransText, skipUpdateFields);
				base.Update(dbStagingTransText);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public StagingTransText GetStagingTransTextByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.StagingTransTextGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public List<StagingTransText> GetStagingTransTextByProcessTransTypeNoTracking(List<int> processTransType)
		{
			try
			{
				List<StagingTransText> stagingTransTexts = Entity.Where(w => processTransType.Contains(w.ProcessTransType)).AsNoTracking().ToList();
				return stagingTransTexts;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

