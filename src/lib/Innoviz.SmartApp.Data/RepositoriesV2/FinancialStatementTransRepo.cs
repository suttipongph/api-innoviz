using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface IFinancialStatementTransRepo
    {
        #region DropDown
        IEnumerable<SelectItem<FinancialStatementTransItemView>> GetDropDownItem(SearchParameter search);
        #endregion DropDown
        void ValidateUpdate(FinancialStatementTrans item);
        SearchResult<FinancialStatementTransListView> GetListvw(SearchParameter search);
        FinancialStatementTransItemView GetByIdvw(Guid id);
        FinancialStatementTrans Find(params object[] keyValues);
        FinancialStatementTrans GetFinancialStatementTransByIdNoTracking(Guid guid);
        FinancialStatementTrans CreateFinancialStatementTrans(FinancialStatementTrans financialStatementTrans);
        void CreateFinancialStatementTransVoid(FinancialStatementTrans financialStatementTrans);
        FinancialStatementTrans UpdateFinancialStatementTrans(FinancialStatementTrans dbFinancialStatementTrans, FinancialStatementTrans inputFinancialStatementTrans, List<string> skipUpdateFields = null);
        void UpdateFinancialStatementTransVoid(FinancialStatementTrans dbFinancialStatementTrans, FinancialStatementTrans inputFinancialStatementTrans, List<string> skipUpdateFields = null);
        void Remove(FinancialStatementTrans item);
        void ValidateAdd(FinancialStatementTrans item);
        void ValidateAdd(IEnumerable<FinancialStatementTrans> items);

        #region function
        IEnumerable<FinancialStatementTrans> GetCopyFinancialStatementTranByRefTypes(Guid refGUID, int refType);
        #endregion
        List<FinancialStatementTrans> GetFinancialStatementTransByRefernece(Guid refGUID, int refType);
    }
    public class FinancialStatementTransRepo : CompanyBaseRepository<FinancialStatementTrans>, IFinancialStatementTransRepo
    {
        public FinancialStatementTransRepo(SmartAppDbContext context) : base(context) { }
        public FinancialStatementTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public FinancialStatementTrans GetFinancialStatementTransByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.FinancialStatementTransGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        private IQueryable<FinancialStatementTransItemViewMap> GetDropDownQuery()
        {
            return (from financialStatementTrans in Entity
                    select new FinancialStatementTransItemViewMap
                    {
                        CompanyGUID = financialStatementTrans.CompanyGUID,
                        Owner = financialStatementTrans.Owner,
                        OwnerBusinessUnitGUID = financialStatementTrans.OwnerBusinessUnitGUID,
                        FinancialStatementTransGUID = financialStatementTrans.FinancialStatementTransGUID
                    });
        }
        public IEnumerable<SelectItem<FinancialStatementTransItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<FinancialStatementTrans>(search, SysParm.CompanyGUID);
                var financialStatementTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<FinancialStatementTransItemViewMap, FinancialStatementTransItemView>().ToDropDownItem(search.ExcludeRowData);


                return financialStatementTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #region GetListvw
        private IQueryable<FinancialStatementTransListViewMap> GetListQuery()
        {
            var resule = (from financialStatementTrans in Entity
                    select new FinancialStatementTransListViewMap
                    {
                        CompanyGUID = financialStatementTrans.CompanyGUID,
                        Owner = financialStatementTrans.Owner,
                        OwnerBusinessUnitGUID = financialStatementTrans.OwnerBusinessUnitGUID,
                        FinancialStatementTransGUID = financialStatementTrans.FinancialStatementTransGUID,
                        Year = financialStatementTrans.Year,
                        Ordering = financialStatementTrans.Ordering,
                        Description = financialStatementTrans.Description,
                        Amount = financialStatementTrans.Amount,
                        RefGUID = financialStatementTrans.RefGUID
                    });
            return resule;
        }
        public SearchResult<FinancialStatementTransListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<FinancialStatementTransListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<FinancialStatementTrans>(search, SysParm.CompanyGUID);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<FinancialStatementTransListViewMap, FinancialStatementTransListView>();
                result = list.SetSearchResult<FinancialStatementTransListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<FinancialStatementTransItemViewMap> GetItemQuery()
        {
            return (from financialStatementTrans in Entity
                    join company in db.Set<Company>()
                    on financialStatementTrans.CompanyGUID equals company.CompanyGUID
                    join ownerBU in db.Set<BusinessUnit>()
                    on financialStatementTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljFinancialStatementTransOwnerBU
                    from ownerBU in ljFinancialStatementTransOwnerBU.DefaultIfEmpty()
                    join customerTable in db.Set<CustomerTable>()
                    on financialStatementTrans.RefGUID equals customerTable.CustomerTableGUID into ljCustomerTable
                    from customerTable in ljCustomerTable.DefaultIfEmpty()
                    join buyerTable in db.Set<BuyerTable>()
                    on financialStatementTrans.RefGUID equals buyerTable.BuyerTableGUID into ljBuyerTable
                    from buyerTable in ljBuyerTable.DefaultIfEmpty()
                    join creditAppRequestTable in db.Set<CreditAppRequestTable>()
                    on financialStatementTrans.RefGUID equals creditAppRequestTable.CreditAppRequestTableGUID into ljCreditAppRequestTable
                    from creditAppRequestTable in ljCreditAppRequestTable.DefaultIfEmpty()
                    join creditAppRequestLine in db.Set<CreditAppRequestLine>()
                    on financialStatementTrans.RefGUID equals creditAppRequestLine.CreditAppRequestLineGUID into ljCreditAppRequestLine
                    from creditAppRequestLine in ljCreditAppRequestLine.DefaultIfEmpty()
                    select new FinancialStatementTransItemViewMap
                    {
                        CompanyGUID = financialStatementTrans.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = financialStatementTrans.Owner,
                        OwnerBusinessUnitGUID = financialStatementTrans.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        CreatedBy = financialStatementTrans.CreatedBy,
                        CreatedDateTime = financialStatementTrans.CreatedDateTime,
                        ModifiedBy = financialStatementTrans.ModifiedBy,
                        ModifiedDateTime = financialStatementTrans.ModifiedDateTime,
                        FinancialStatementTransGUID = financialStatementTrans.FinancialStatementTransGUID,
                        Amount = financialStatementTrans.Amount,
                        Description = financialStatementTrans.Description,
                        Ordering = financialStatementTrans.Ordering,
                        RefGUID = financialStatementTrans.RefGUID,
                        RefType = financialStatementTrans.RefType,
                        Year = financialStatementTrans.Year,
                        RefId = (customerTable != null && financialStatementTrans.RefType == (int)RefType.Customer) ? customerTable.CustomerId :
                                (buyerTable != null && financialStatementTrans.RefType == (int)RefType.Buyer) ? buyerTable.BuyerId :
                                (creditAppRequestTable != null && financialStatementTrans.RefType == (int)RefType.CreditAppRequestTable) ? creditAppRequestTable.CreditAppRequestId :
                                (creditAppRequestLine != null && financialStatementTrans.RefType == (int)RefType.CreditAppRequestLine) ? creditAppRequestLine.LineNum.ToString() :
                                null,
                    
                        RowVersion = financialStatementTrans.RowVersion,
                    });
        }
        public FinancialStatementTransItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<FinancialStatementTransItemViewMap, FinancialStatementTransItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public FinancialStatementTrans CreateFinancialStatementTrans(FinancialStatementTrans financialStatementTrans)
        {
            try
            {
                financialStatementTrans.FinancialStatementTransGUID = Guid.NewGuid();
                base.Add(financialStatementTrans);
                return financialStatementTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateFinancialStatementTransVoid(FinancialStatementTrans financialStatementTrans)
        {
            try
            {
                CreateFinancialStatementTrans(financialStatementTrans);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public FinancialStatementTrans UpdateFinancialStatementTrans(FinancialStatementTrans dbFinancialStatementTrans, FinancialStatementTrans inputFinancialStatementTrans, List<string> skipUpdateFields = null)
        {
            try
            {
                dbFinancialStatementTrans = dbFinancialStatementTrans.MapUpdateValues<FinancialStatementTrans>(inputFinancialStatementTrans);
                base.Update(dbFinancialStatementTrans);
                return dbFinancialStatementTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateFinancialStatementTransVoid(FinancialStatementTrans dbFinancialStatementTrans, FinancialStatementTrans inputFinancialStatementTrans, List<string> skipUpdateFields = null)
        {
            try
            {
                dbFinancialStatementTrans = dbFinancialStatementTrans.MapUpdateValues<FinancialStatementTrans>(inputFinancialStatementTrans, skipUpdateFields);
                base.Update(dbFinancialStatementTrans);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public override void ValidateAdd(IEnumerable<FinancialStatementTrans> items)
        {
            base.ValidateAdd(items);
        }
        public override void ValidateAdd(FinancialStatementTrans item)
        {
            base.ValidateAdd(item);
        }
        public override void ValidateUpdate(IEnumerable<FinancialStatementTrans> items)
        {
            ValidateUpdate(items);
        }
        public override void ValidateUpdate(FinancialStatementTrans item)
        {
            base.ValidateUpdate(item);
        }
        #endregion Create, Update, Delete
        #region function
        public IEnumerable<FinancialStatementTrans> GetCopyFinancialStatementTranByRefTypes(Guid refGUID, int refType)
        {
            try
            {
                IEnumerable<FinancialStatementTrans> list = Entity.Where(w => w.RefGUID == refGUID && w.RefType == refType)
                    .AsNoTracking().ToList();
                return list;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        public List<FinancialStatementTrans> GetFinancialStatementTransByRefernece(Guid refGUID, int refType)
        {
            try
            {
                List<FinancialStatementTrans> list = Entity.Where(w => w.RefGUID == refGUID && w.RefType == refType)
                    .AsNoTracking().ToList();
                return list;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}

