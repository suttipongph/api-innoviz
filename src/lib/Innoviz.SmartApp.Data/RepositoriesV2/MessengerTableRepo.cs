using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IMessengerTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<MessengerTableItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<MessengerTableListView> GetListvw(SearchParameter search);
		MessengerTableItemView GetByIdvw(Guid id);
		MessengerTable Find(params object[] keyValues);
		MessengerTable GetMessengerTableByIdNoTracking(Guid guid);
		MessengerTable CreateMessengerTable(MessengerTable messengerTable);
		void CreateMessengerTableVoid(MessengerTable messengerTable);
		MessengerTable UpdateMessengerTable(MessengerTable dbMessengerTable, MessengerTable inputMessengerTable, List<string> skipUpdateFields = null);
		void UpdateMessengerTableVoid(MessengerTable dbMessengerTable, MessengerTable inputMessengerTable, List<string> skipUpdateFields = null);
		void Remove(MessengerTable item);
		MessengerTable GetMessengerTableByIdNoTrackingByAccessLevel(Guid guid);
	}
	public class MessengerTableRepo : CompanyBaseRepository<MessengerTable>, IMessengerTableRepo
	{
		public MessengerTableRepo(SmartAppDbContext context) : base(context) { }
		public MessengerTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public MessengerTable GetMessengerTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.MessengerTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<MessengerTableItemViewMap> GetDropDownQuery()
		{
			return (from messengerTable in Entity
					join vendorTable in db.Set<VendorTable>()
					on messengerTable.VendorTableGUID equals vendorTable.VendorTableGUID into ljvendorTable
					from vendorTable in ljvendorTable.DefaultIfEmpty()
					select new MessengerTableItemViewMap
					{
						CompanyGUID = messengerTable.CompanyGUID,
						Owner = messengerTable.Owner,
						OwnerBusinessUnitGUID = messengerTable.OwnerBusinessUnitGUID,
						MessengerTableGUID = messengerTable.MessengerTableGUID,
						Name = messengerTable.Name,
						VendorTableGUID = messengerTable.VendorTableGUID,
						VendorTable_VendorId = SmartAppUtil.GetDropDownLabel(vendorTable.VendorId, vendorTable.Name)
					});
		}
		public IEnumerable<SelectItem<MessengerTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<MessengerTable>(search, SysParm.CompanyGUID);
				var messengerTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<MessengerTableItemViewMap, MessengerTableItemView>().ToDropDownItem(search.ExcludeRowData);
				return messengerTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<MessengerTableListViewMap> GetListQuery()
		{
			return (from messengerTable in Entity
					join vendorTable in db.Set<VendorTable>()
					on messengerTable.VendorTableGUID equals vendorTable.VendorTableGUID into ljvendorTable
					from vendorTable in ljvendorTable.DefaultIfEmpty()
				select new MessengerTableListViewMap
				{
						CompanyGUID = messengerTable.CompanyGUID,
						Owner = messengerTable.Owner,
						OwnerBusinessUnitGUID = messengerTable.OwnerBusinessUnitGUID,
						MessengerTableGUID = messengerTable.MessengerTableGUID,
						Name = messengerTable.Name,
						TaxId = messengerTable.TaxId,
						VendorTableGUID = messengerTable.VendorTableGUID,
						Phone = messengerTable.Phone,
						PlateNumber = messengerTable.PlateNumber,
						DriverLicenseId = messengerTable.DriverLicenseId,
						VendorTable_Values = SmartAppUtil.GetDropDownLabel(vendorTable.VendorId, vendorTable.Name),
						VendorTable_VendorId = vendorTable != null ? vendorTable.VendorId : null
				});
		}
		public SearchResult<MessengerTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<MessengerTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<MessengerTable>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<MessengerTableListViewMap, MessengerTableListView>();
				result = list.SetSearchResult<MessengerTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<MessengerTableItemViewMap> GetItemQuery()
		{
			return (from messengerTable in Entity
					join company in db.Set<Company>()
					on messengerTable.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on messengerTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljMessengerTableOwnerBU
					from ownerBU in ljMessengerTableOwnerBU.DefaultIfEmpty()
					select new MessengerTableItemViewMap
					{
						CompanyGUID = messengerTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = messengerTable.Owner,
						OwnerBusinessUnitGUID = messengerTable.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = messengerTable.CreatedBy,
						CreatedDateTime = messengerTable.CreatedDateTime,
						ModifiedBy = messengerTable.ModifiedBy,
						ModifiedDateTime = messengerTable.ModifiedDateTime,
						MessengerTableGUID = messengerTable.MessengerTableGUID,
						Address = messengerTable.Address,
						DriverLicenseId = messengerTable.DriverLicenseId,
						Name = messengerTable.Name,
						Phone = messengerTable.Phone,
						PlateNumber = messengerTable.PlateNumber,
						TaxId = messengerTable.TaxId,
						VendorTableGUID = messengerTable.VendorTableGUID,
					
						RowVersion = messengerTable.RowVersion,
					});
		}
		public MessengerTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<MessengerTableItemViewMap, MessengerTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public MessengerTable CreateMessengerTable(MessengerTable messengerTable)
		{
			try
			{
				messengerTable.MessengerTableGUID = Guid.NewGuid();
				base.Add(messengerTable);
				return messengerTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateMessengerTableVoid(MessengerTable messengerTable)
		{
			try
			{
				CreateMessengerTable(messengerTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public MessengerTable UpdateMessengerTable(MessengerTable dbMessengerTable, MessengerTable inputMessengerTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbMessengerTable = dbMessengerTable.MapUpdateValues<MessengerTable>(inputMessengerTable);
				base.Update(dbMessengerTable);
				return dbMessengerTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateMessengerTableVoid(MessengerTable dbMessengerTable, MessengerTable inputMessengerTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbMessengerTable = dbMessengerTable.MapUpdateValues<MessengerTable>(inputMessengerTable, skipUpdateFields);
				base.Update(dbMessengerTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public MessengerTable GetMessengerTableByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.MessengerTableGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

