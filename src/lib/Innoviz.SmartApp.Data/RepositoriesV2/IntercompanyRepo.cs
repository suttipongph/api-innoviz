using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IIntercompanyRepo
	{
		#region DropDown
		IEnumerable<SelectItem<IntercompanyItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<IntercompanyListView> GetListvw(SearchParameter search);
		IntercompanyItemView GetByIdvw(Guid id);
		Intercompany Find(params object[] keyValues);
		Intercompany GetIntercompanyByIdNoTracking(Guid guid);
		List<Intercompany> GetIntercompanyByIdNoTracking(IEnumerable<Guid> intercompanyGuids);
		Intercompany CreateIntercompany(Intercompany intercompany);
		void CreateIntercompanyVoid(Intercompany intercompany);
		Intercompany UpdateIntercompany(Intercompany dbIntercompany, Intercompany inputIntercompany, List<string> skipUpdateFields = null);
		void UpdateIntercompanyVoid(Intercompany dbIntercompany, Intercompany inputIntercompany, List<string> skipUpdateFields = null);
		void Remove(Intercompany item);
	}
	public class IntercompanyRepo : CompanyBaseRepository<Intercompany>, IIntercompanyRepo
	{
		public IntercompanyRepo(SmartAppDbContext context) : base(context) { }
		public IntercompanyRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public Intercompany GetIntercompanyByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.IntercompanyGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<Intercompany> GetIntercompanyByIdNoTracking(IEnumerable<Guid> intercompanyGuids)
		{
			try
			{
				var result = Entity.Where(w => intercompanyGuids.Contains(w.IntercompanyGUID))
								.AsNoTracking()
								.ToList();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<IntercompanyItemViewMap> GetDropDownQuery()
		{
			return (from intercompany in Entity
					select new IntercompanyItemViewMap
					{
						CompanyGUID = intercompany.CompanyGUID,
						Owner = intercompany.Owner,
						OwnerBusinessUnitGUID = intercompany.OwnerBusinessUnitGUID,
						IntercompanyGUID = intercompany.IntercompanyGUID,
						IntercompanyId = intercompany.IntercompanyId,
						Description = intercompany.Description
					});
		}
		public IEnumerable<SelectItem<IntercompanyItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<Intercompany>(search, SysParm.CompanyGUID);
				var intercompany = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<IntercompanyItemViewMap, IntercompanyItemView>().ToDropDownItem(search.ExcludeRowData);


				return intercompany;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<IntercompanyListViewMap> GetListQuery()
		{
			return (from intercompany in Entity
				select new IntercompanyListViewMap
				{
						CompanyGUID = intercompany.CompanyGUID,
						Owner = intercompany.Owner,
						OwnerBusinessUnitGUID = intercompany.OwnerBusinessUnitGUID,
						IntercompanyGUID = intercompany.IntercompanyGUID,
						IntercompanyId = intercompany.IntercompanyId,
						Description = intercompany.Description,
				});
		}
		public SearchResult<IntercompanyListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<IntercompanyListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<Intercompany>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<IntercompanyListViewMap, IntercompanyListView>();
				result = list.SetSearchResult<IntercompanyListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<IntercompanyItemViewMap> GetItemQuery()
		{
			return (from intercompany in Entity
					join ownerBU in db.Set<BusinessUnit>()
					on intercompany.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljIntercompanyOwnerBU
					from ownerBU in ljIntercompanyOwnerBU.DefaultIfEmpty()
					join company in db.Set<Company>() on intercompany.CompanyGUID equals company.CompanyGUID
					select new IntercompanyItemViewMap
					{
						CompanyGUID = intercompany.CompanyGUID,
						Owner = intercompany.Owner,
						OwnerBusinessUnitGUID = intercompany.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CompanyId = company.CompanyId,
						CreatedBy = intercompany.CreatedBy,
						CreatedDateTime = intercompany.CreatedDateTime,
						ModifiedBy = intercompany.ModifiedBy,
						ModifiedDateTime = intercompany.ModifiedDateTime,
						IntercompanyGUID = intercompany.IntercompanyGUID,
						Description = intercompany.Description,
						IntercompanyId = intercompany.IntercompanyId,
					
						RowVersion = intercompany.RowVersion,
					});
		}
		public IntercompanyItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<IntercompanyItemViewMap, IntercompanyItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public Intercompany CreateIntercompany(Intercompany intercompany)
		{
			try
			{
				intercompany.IntercompanyGUID = Guid.NewGuid();
				base.Add(intercompany);
				return intercompany;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateIntercompanyVoid(Intercompany intercompany)
		{
			try
			{
				CreateIntercompany(intercompany);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public Intercompany UpdateIntercompany(Intercompany dbIntercompany, Intercompany inputIntercompany, List<string> skipUpdateFields = null)
		{
			try
			{
				dbIntercompany = dbIntercompany.MapUpdateValues<Intercompany>(inputIntercompany);
				base.Update(dbIntercompany);
				return dbIntercompany;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateIntercompanyVoid(Intercompany dbIntercompany, Intercompany inputIntercompany, List<string> skipUpdateFields = null)
		{
			try
			{
				dbIntercompany = dbIntercompany.MapUpdateValues<Intercompany>(inputIntercompany, skipUpdateFields);
				base.Update(dbIntercompany);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

