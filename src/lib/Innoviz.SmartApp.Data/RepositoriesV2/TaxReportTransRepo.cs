using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ITaxReportTransRepo
	{
		#region DropDown
		IEnumerable<SelectItem<TaxReportTransItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<TaxReportTransListView> GetListvw(SearchParameter search);
		TaxReportTransItemView GetByIdvw(Guid id);
		TaxReportTrans Find(params object[] keyValues);
		TaxReportTrans GetTaxReportTransByIdNoTracking(Guid guid);
		TaxReportTrans CreateTaxReportTrans(TaxReportTrans taxReportTrans);
		void CreateTaxReportTransVoid(TaxReportTrans taxReportTrans);
		TaxReportTrans UpdateTaxReportTrans(TaxReportTrans dbTaxReportTrans, TaxReportTrans inputTaxReportTrans, List<string> skipUpdateFields = null);
		void UpdateTaxReportTransVoid(TaxReportTrans dbTaxReportTrans, TaxReportTrans inputTaxReportTrans, List<string> skipUpdateFields = null);
		void Remove(TaxReportTrans item);
		void ValidateAdd(TaxReportTrans item);
		void ValidateAdd(IEnumerable<TaxReportTrans> items);
		#region function
		IEnumerable<TaxReportTrans> GetCopyTaxReportTransByRefTypes(Guid refGUID, int refType);
		#endregion
	}
    public class TaxReportTransRepo : CompanyBaseRepository<TaxReportTrans>, ITaxReportTransRepo
	{
		public TaxReportTransRepo(SmartAppDbContext context) : base(context) { }
		public TaxReportTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public TaxReportTrans GetTaxReportTransByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.TaxReportTransGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<TaxReportTransItemViewMap> GetDropDownQuery()
		{
			return (from taxReportTrans in Entity
					select new TaxReportTransItemViewMap
					{
						CompanyGUID = taxReportTrans.CompanyGUID,
						Owner = taxReportTrans.Owner,
						OwnerBusinessUnitGUID = taxReportTrans.OwnerBusinessUnitGUID,
						TaxReportTransGUID = taxReportTrans.TaxReportTransGUID,
						Year = taxReportTrans.Year,
						Month = taxReportTrans.Month
					});
		}
		public IEnumerable<SelectItem<TaxReportTransItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<TaxReportTrans>(search, SysParm.CompanyGUID);
				var taxReportTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<TaxReportTransItemViewMap, TaxReportTransItemView>().ToDropDownItem(search.ExcludeRowData);


				return taxReportTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<TaxReportTransListViewMap> GetListQuery()
		{
			return (from taxReportTrans in Entity
				select new TaxReportTransListViewMap
				{
						CompanyGUID = taxReportTrans.CompanyGUID,
						Owner = taxReportTrans.Owner,
						OwnerBusinessUnitGUID = taxReportTrans.OwnerBusinessUnitGUID,
						TaxReportTransGUID = taxReportTrans.TaxReportTransGUID,
						Year = taxReportTrans.Year,
						Month = taxReportTrans.Month,
						Description = taxReportTrans.Description,
						Amount = taxReportTrans.Amount,
						RefGUID = taxReportTrans.RefGUID
				});
		}
		public SearchResult<TaxReportTransListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<TaxReportTransListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<TaxReportTrans>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<TaxReportTransListViewMap, TaxReportTransListView>();
				result = list.SetSearchResult<TaxReportTransListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<TaxReportTransItemViewMap> GetItemQuery()
		{
			return (from taxReportTrans in Entity
					join company in db.Set<Company>()
					on taxReportTrans.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on taxReportTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljTaxReportTransOwnerBU
					from ownerBU in ljTaxReportTransOwnerBU.DefaultIfEmpty()
					join customerTable in db.Set<CustomerTable>()
					on taxReportTrans.RefGUID equals customerTable.CustomerTableGUID into ljCustomerTable
					from customerTable in ljCustomerTable.DefaultIfEmpty()
					join creditAppRequestTable in db.Set<CreditAppRequestTable>()
					on taxReportTrans.RefGUID equals creditAppRequestTable.CreditAppRequestTableGUID into ljCreditAppRequestTable
					from creditAppRequestTable in ljCreditAppRequestTable.DefaultIfEmpty()
					select new TaxReportTransItemViewMap
					{
						CompanyGUID = taxReportTrans.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = taxReportTrans.Owner,
						OwnerBusinessUnitGUID = taxReportTrans.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = taxReportTrans.CreatedBy,
						CreatedDateTime = taxReportTrans.CreatedDateTime,
						ModifiedBy = taxReportTrans.ModifiedBy,
						ModifiedDateTime = taxReportTrans.ModifiedDateTime,
						TaxReportTransGUID = taxReportTrans.TaxReportTransGUID,
						Amount = taxReportTrans.Amount,
						Description = taxReportTrans.Description,
						Month = taxReportTrans.Month,
						RefGUID = taxReportTrans.RefGUID,
						RefType = taxReportTrans.RefType,
						Year = taxReportTrans.Year,
						RefId = (customerTable != null && taxReportTrans.RefType == (int)RefType.Customer) ? customerTable.CustomerId :
								(creditAppRequestTable != null && taxReportTrans.RefType == (int)RefType.CreditAppRequestTable) ? creditAppRequestTable.CreditAppRequestId : 
								null,
					
						RowVersion = taxReportTrans.RowVersion,
					});
		}
		public TaxReportTransItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<TaxReportTransItemViewMap, TaxReportTransItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public TaxReportTrans CreateTaxReportTrans(TaxReportTrans taxReportTrans)
		{
			try
			{
				taxReportTrans.TaxReportTransGUID = Guid.NewGuid();
				base.Add(taxReportTrans);
				return taxReportTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateTaxReportTransVoid(TaxReportTrans taxReportTrans)
		{
			try
			{
				CreateTaxReportTrans(taxReportTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public TaxReportTrans UpdateTaxReportTrans(TaxReportTrans dbTaxReportTrans, TaxReportTrans inputTaxReportTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbTaxReportTrans = dbTaxReportTrans.MapUpdateValues<TaxReportTrans>(inputTaxReportTrans);
				base.Update(dbTaxReportTrans);
				return dbTaxReportTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateTaxReportTransVoid(TaxReportTrans dbTaxReportTrans, TaxReportTrans inputTaxReportTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbTaxReportTrans = dbTaxReportTrans.MapUpdateValues<TaxReportTrans>(inputTaxReportTrans, skipUpdateFields);
				base.Update(dbTaxReportTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		#region function
		public IEnumerable<TaxReportTrans> GetCopyTaxReportTransByRefTypes(Guid refGUID, int refType)
		{
			try
			{
				IEnumerable<TaxReportTrans> list = Entity.Where(w => w.RefGUID == refGUID && w.RefType == refType)
					.AsNoTracking().ToList();
				return list;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		public override void ValidateAdd(IEnumerable<TaxReportTrans> items)
		{
			base.ValidateAdd(items);
		}
		public override void ValidateAdd(TaxReportTrans item)
		{
			base.ValidateAdd(item);
		}
	}
}

