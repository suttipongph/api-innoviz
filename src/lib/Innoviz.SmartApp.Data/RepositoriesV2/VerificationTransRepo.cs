using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IVerificationTransRepo
	{
		#region DropDown
		IEnumerable<SelectItem<VerificationTransItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		void ValidateAdd(VerificationTrans item);
		void ValidateUpdate(VerificationTrans item);
		SearchResult<VerificationTransListView> GetListvw(SearchParameter search);
		VerificationTransItemView GetByIdvw(Guid id);
		VerificationTrans Find(params object[] keyValues);
		VerificationTrans GetVerificationTransByIdNoTracking(Guid guid);
		VerificationTrans CreateVerificationTrans(VerificationTrans verificationTrans);
		void CreateVerificationTransVoid(VerificationTrans verificationTrans);
		VerificationTrans UpdateVerificationTrans(VerificationTrans dbVerificationTrans, VerificationTrans inputVerificationTrans, List<string> skipUpdateFields = null);
		void UpdateVerificationTransVoid(VerificationTrans dbVerificationTrans, VerificationTrans inputVerificationTrans, List<string> skipUpdateFields = null);
		void Remove(VerificationTrans item);
		VerificationTrans GetVerificationTransByIdNoTrackingByAccessLevel(Guid guid);
		IEnumerable<VerificationTrans> GetVerificationTransByCompanyNoTracking(Guid guid);
		IEnumerable<VerificationTrans> GetVerificationTransByReferanceNoTracking(int refType, Guid refGUID);
		IEnumerable<VerificationTrans> GetVerificationTransByReferenceAndVerificationStatusNoTracking(int refType, IEnumerable<Guid> refGUIDs, VerificationDocumentStatus verificationStatus);
		IEnumerable<VerificationTrans> GetVerificationTransByNotEqualDocumentIdNoTracking(Guid companyGUID, string documentId);
		void ValidateAdd(IEnumerable<VerificationTrans> items);
	}
	public class VerificationTransRepo : CompanyBaseRepository<VerificationTrans>, IVerificationTransRepo
	{
		public VerificationTransRepo(SmartAppDbContext context) : base(context) { }
		public VerificationTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public VerificationTrans GetVerificationTransByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.VerificationTransGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<VerificationTransItemViewMap> GetDropDownQuery()
		{
			return (from verificationTrans in Entity
					select new VerificationTransItemViewMap
					{
						CompanyGUID = verificationTrans.CompanyGUID,
						Owner = verificationTrans.Owner,
						OwnerBusinessUnitGUID = verificationTrans.OwnerBusinessUnitGUID,
						VerificationTransGUID = verificationTrans.VerificationTransGUID
					});
		}
		public IEnumerable<SelectItem<VerificationTransItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<VerificationTrans>(search, SysParm.CompanyGUID);
				var verificationTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<VerificationTransItemViewMap, VerificationTransItemView>().ToDropDownItem(search.ExcludeRowData);


				return verificationTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<VerificationTransListViewMap> GetListQuery()
		{
			return (from verificationTrans in Entity
					join verificationTable in db.Set<VerificationTable>()
					on verificationTrans.VerificationTableGUID equals verificationTable.VerificationTableGUID
					join buyerTable in db.Set<BuyerTable>()
					on verificationTable.BuyerTableGUID equals buyerTable.BuyerTableGUID
					select new VerificationTransListViewMap
				{
						CompanyGUID = verificationTrans.CompanyGUID,
						Owner = verificationTrans.Owner,
						OwnerBusinessUnitGUID = verificationTrans.OwnerBusinessUnitGUID,
						VerificationTransGUID = verificationTrans.VerificationTransGUID,
						VerificationTableGUID = verificationTrans.VerificationTableGUID,
						VerificationTable_VerificationDate = verificationTable.VerificationDate,
						BuyerId = buyerTable.BuyerId,
						VerificationTable_Values = SmartAppUtil.GetDropDownLabel(verificationTable.VerificationId, verificationTable.Description),
						VerificationTable_VerificationId = verificationTable.VerificationId,
						RefGUID = verificationTrans.RefGUID
					});
		}
		public SearchResult<VerificationTransListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<VerificationTransListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<VerificationTrans>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<VerificationTransListViewMap, VerificationTransListView>();
				result = list.SetSearchResult<VerificationTransListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<VerificationTransItemViewMap> GetItemQuery()
		{
			return (from verificationTrans in Entity
					join company in db.Set<Company>()
					on verificationTrans.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on verificationTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljVerificationTransOwnerBU
					from ownerBU in ljVerificationTransOwnerBU.DefaultIfEmpty()
					select new VerificationTransItemViewMap
					{
						CompanyGUID = verificationTrans.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = verificationTrans.Owner,
						OwnerBusinessUnitGUID = verificationTrans.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = verificationTrans.CreatedBy,
						CreatedDateTime = verificationTrans.CreatedDateTime,
						ModifiedBy = verificationTrans.ModifiedBy,
						ModifiedDateTime = verificationTrans.ModifiedDateTime,
						VerificationTransGUID = verificationTrans.VerificationTransGUID,
						DocumentId = verificationTrans.DocumentId,
						RefGUID = verificationTrans.RefGUID,
						RefType = verificationTrans.RefType,
						VerificationTableGUID = verificationTrans.VerificationTableGUID,
					
						RowVersion = verificationTrans.RowVersion,
					});
		}
		public VerificationTransItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<VerificationTransItemViewMap, VerificationTransItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public VerificationTrans CreateVerificationTrans(VerificationTrans verificationTrans)
		{
			try
			{
				verificationTrans.VerificationTransGUID = Guid.NewGuid();
				base.Add(verificationTrans);
				return verificationTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateVerificationTransVoid(VerificationTrans verificationTrans)
		{
			try
			{
				CreateVerificationTrans(verificationTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public VerificationTrans UpdateVerificationTrans(VerificationTrans dbVerificationTrans, VerificationTrans inputVerificationTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbVerificationTrans = dbVerificationTrans.MapUpdateValues<VerificationTrans>(inputVerificationTrans);
				base.Update(dbVerificationTrans);
				return dbVerificationTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateVerificationTransVoid(VerificationTrans dbVerificationTrans, VerificationTrans inputVerificationTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbVerificationTrans = dbVerificationTrans.MapUpdateValues<VerificationTrans>(inputVerificationTrans, skipUpdateFields);
				base.Update(dbVerificationTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public VerificationTrans GetVerificationTransByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.VerificationTransGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<VerificationTrans> GetVerificationTransByCompanyNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CompanyGUID == guid)
					.AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public override void ValidateAdd(VerificationTrans item)
		{
			CheckDupplicate(item);
			base.ValidateAdd(item);
		}
		public override void ValidateUpdate(VerificationTrans item)
		{
			CheckDupplicate(item);
			base.ValidateUpdate(item);
		}
		public void CheckDupplicate(VerificationTrans item)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				IVerificationTransRepo verificationTransRepo = new VerificationTransRepo(db);
				IEnumerable<VerificationTrans> verificationTrans = verificationTransRepo.GetVerificationTransByCompanyNoTracking(item.CompanyGUID);
				#region Validate By Shared
				IAttachmentService attachmentService = new AttachmentService(db);
				bool isDupplicateVerificationTableAndRefId = false;
				isDupplicateVerificationTableAndRefId = verificationTrans.Any(w => w.VerificationTransGUID != item.VerificationTransGUID
																					  && w.VerificationTableGUID == item.VerificationTableGUID
																					  && w.RefGUID == item.RefGUID);
				if (isDupplicateVerificationTableAndRefId)
				{
					RefIdParm refIdParm = new RefIdParm
					{
						RefGUID = item.RefGUID.GuidNullToString(),
						RefType = item.RefType
					};
					string refId = attachmentService.GetRefIdByRefTypeRefGUID(refIdParm);
					ex.AddData("ERROR.DUPLICATE", new string[] { "LABEL.VERIFICATION_ID", refId });
				}
				#endregion Validate By Shared

				#region Validate By RefType
				bool isDupplicateVerificationTable = false;
				bool isDupplicateVerificationTableByNotEqualDocumentId = false;
				switch (item.RefType)
				{
					case (int)RefType.PurchaseLine:
						IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
						PurchaseLine purchaseLine = purchaseLineRepo.GetPurchaseLineByIdNoTrackingByAccessLevel(item.RefGUID);
						IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
						PurchaseTable purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTrackingByAccessLevel(purchaseLine.PurchaseTableGUID);
						if (purchaseTable.Rollbill == true)
						{
							isDupplicateVerificationTable = verificationTrans.Any(w => w.VerificationTransGUID != item.VerificationTransGUID
																					  && w.VerificationTableGUID == item.VerificationTableGUID
																					  && w.RefGUID == item.RefGUID);
						}
						if (purchaseTable.Rollbill == false)
						{
							isDupplicateVerificationTableByNotEqualDocumentId = verificationTrans.Any(w => w.VerificationTransGUID != item.VerificationTransGUID
																					  && w.VerificationTableGUID == item.VerificationTableGUID
																					  && w.DocumentId != item.DocumentId);
						}
						if (isDupplicateVerificationTable)
						{
							ex.AddData("ERROR.DUPLICATE", "LABEL.VERIFICATION_ID");
						}
						if (isDupplicateVerificationTableByNotEqualDocumentId)
						{
							ex.AddData("ERROR.90147", "LABEL.VERIFICATION_ID");
						}
						break;
					case (int)RefType.WithdrawalTable:
						IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
						WithdrawalTable withdrawalTable = withdrawalTableRepo.GetWithdrawalTableByIdNoTracking(item.RefGUID);
						if (withdrawalTable.TermExtension == true)
						{
							isDupplicateVerificationTable = verificationTrans.Any(w => w.VerificationTransGUID != item.VerificationTransGUID
																					  && w.VerificationTableGUID == item.VerificationTableGUID
																					  && w.RefGUID == item.RefGUID);
						}
						if (withdrawalTable.TermExtension == false)
						{
							isDupplicateVerificationTable = verificationTrans.Any(w => w.VerificationTransGUID != item.VerificationTransGUID
																					  && w.VerificationTableGUID == item.VerificationTableGUID);
						}
						if (isDupplicateVerificationTable)
						{
							ex.AddData("ERROR.DUPLICATE", "LABEL.VERIFICATION_ID");
						}
						break;
				}
				#endregion Validate By RefType
				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}

		public IEnumerable<VerificationTrans> GetVerificationTransByReferanceNoTracking(int refType,Guid refGUID)
		{
			try
			{
				var result = Entity.Where(item => item.RefGUID == refGUID && item.RefType == refType)
					.AsNoTracking().ToList();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<VerificationTrans> GetVerificationTransByReferenceAndVerificationStatusNoTracking(int refType, IEnumerable<Guid> refGUIDs, VerificationDocumentStatus verificationStatus)
        {
            try
            {
				string statusId = ((int)verificationStatus).ToString();
				var result =
					(from verificationTrans in Entity.Where(w => w.RefType == refType && refGUIDs.Contains(w.RefGUID))
					 join verificationTable in db.Set<VerificationTable>()
					 on verificationTrans.VerificationTableGUID equals verificationTable.VerificationTableGUID
					 join documentStatus in db.Set<DocumentStatus>().Where(w => w.StatusId == statusId)
					 on verificationTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
					 select verificationTrans)
					 .AsNoTracking()
					 .ToList();
				return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
		}
		public IEnumerable<VerificationTrans> GetVerificationTransByNotEqualDocumentIdNoTracking(Guid companyGUID,string documentId)
		{
			try
			{
				var temp = (from verificationTrans in Entity
							where verificationTrans.DocumentId == documentId && verificationTrans.CompanyGUID == companyGUID
							select verificationTrans.VerificationTableGUID).ToList();
				var result = (from verificationTrans in Entity
							  where !temp.Contains(verificationTrans.VerificationTableGUID)
							  select verificationTrans).AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Validate
		public override void ValidateAdd(IEnumerable<VerificationTrans> items)
        {
            base.ValidateAdd(items);
        }
        #endregion Validate
    }
}

