using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICustomerTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CustomerTableItemView>> GetDropDownItem(SearchParameter search);
		IEnumerable<SelectItem<CustomerTableItemView>> GetDropDownItemBy(SearchParameter search);
		IEnumerable<SelectItem<CustomerTableItemView>> GetDropDownItemByGuarantor(SearchParameter search);
		IEnumerable<SelectItem<CustomerTableItemView>> GetDropDownItemByGuarantorAfi(SearchParameter search);


		#endregion DropDown
		void ValidateAdd(CustomerTable item);
		void ValidateAdd(IEnumerable<CustomerTable> items);
		void ValidateUpdate(CustomerTable item);
		void ValidateRemove(IEnumerable<CustomerTable> items);
		SearchResult<CustomerTableListView> GetListvw(SearchParameter search);
		CustomerTableItemView GetByIdvw(Guid id);
		CustomerTable Find(params object[] keyValues);
		CustomerTable GetCustomerTableByIdNoTracking(Guid guid);
		List<CustomerTable> GetCustomerTableByIdNoTracking(IEnumerable<Guid> customerTableGUIDs);
		CustomerTable GetCustomerTableByIdNoTrackingByAccessLevel(Guid guid);
		CustomerTable CreateCustomerTable(CustomerTable customerTable);
		void CreateCustomerTableVoid(CustomerTable customerTable);
		CustomerTable UpdateCustomerTable(CustomerTable dbCustomerTable, CustomerTable inputCustomerTable, List<string> skipUpdateFields = null);
		void UpdateCustomerTableVoid(CustomerTable dbCustomerTable, CustomerTable inputCustomerTable, List<string> skipUpdateFields = null);
		void Remove(CustomerTable item);
		CustomerTableItemView IsPassportIdDuplicate(string customerId, string passPortId);
		CustomerTableItemView IsTaxIdDuplicate(string customerId, string taxId);
		#region function
		UpdateCustomerTableStatusResultView GetCustomerStatusById(Guid guid);
		UpdateCustomerTableBlacklistStatusResultView GetCustomerBlacklistStatusById(Guid guid);
		#endregion
		BookmarkDocumentCustomerAmendCreditLimitView GetBookmarkDocumentCustomerAmendCreditLimitView(Guid RefGUID, Guid BookmarkDocumentTransGUID);
		List<CustomerTable> GetCustomerTableByIdNoTracking(List<Guid> customerTableGuids);
	}
    public class CustomerTableRepo : CompanyBaseRepository<CustomerTable>, ICustomerTableRepo
	{
		public CustomerTableRepo(SmartAppDbContext context) : base(context) { }
		public CustomerTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CustomerTable GetCustomerTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CustomerTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<CustomerTable> GetCustomerTableByIdNoTracking(IEnumerable<Guid> customerTableGUIDs)
		{
			try
			{
				var result = Entity.Where(w => customerTableGUIDs.Contains(w.CustomerTableGUID))
									.AsNoTracking()
									.ToList();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CustomerTableItemViewMap> GetDropDownQueryByGuarantorAfli()
		{
			var result = (from customerTable in Entity
						  select new CustomerTableItemViewMap
						  {
							  CompanyGUID = customerTable.CompanyGUID,
							  Owner = customerTable.Owner,
							  OwnerBusinessUnitGUID = customerTable.OwnerBusinessUnitGUID,
							  CustomerTableGUID = customerTable.CustomerTableGUID,
							  CustomerId = customerTable.CustomerId,
							  Name = customerTable.Name,
							  AltName = customerTable.AltName,
							  ParentCompanyGUID = customerTable.ParentCompanyGUID

						  });
			return result;
		}
		public IEnumerable<SelectItem<CustomerTableItemView>> GetDropDownItemByGuarantorAfi(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CustomerTable>(search, SysParm.CompanyGUID);
				var customerTable = GetDropDownQueryByGuarantorAfli().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CustomerTableItemViewMap, CustomerTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return customerTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private IQueryable<CustomerTableItemViewMap> GetDropDownQuery()
		{
			return (from customerTable in Entity
					select new CustomerTableItemViewMap
					{
						CompanyGUID = customerTable.CompanyGUID,
						Owner = customerTable.Owner,
						OwnerBusinessUnitGUID = customerTable.OwnerBusinessUnitGUID,
						CustomerTableGUID = customerTable.CustomerTableGUID,
						CustomerId = customerTable.CustomerId,
						Name = customerTable.Name,
						AltName = customerTable.AltName,
						VendorTableGUID = customerTable.VendorTableGUID,
						CurrencyGUID = customerTable.CurrencyGUID,
						Dimension1GUID = customerTable.Dimension1GUID,
						Dimension2GUID = customerTable.Dimension2GUID,
						Dimension3GUID = customerTable.Dimension3GUID,
						Dimension4GUID = customerTable.Dimension4GUID,
						Dimension5GUID = customerTable.Dimension5GUID
					});
		}
		public IEnumerable<SelectItem<CustomerTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CustomerTable>(search, SysParm.CompanyGUID);
				var customerTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CustomerTableItemViewMap, CustomerTableItemView>().ToDropDownItem(search.ExcludeRowData);
				return customerTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private IQueryable<CustomerTableItemViewMap> GetDropDownQueryBy()
		{
			return (from customerTable in Entity
					join lineOfBusiness in db.Set<LineOfBusiness>() on customerTable.LineOfBusinessGUID equals lineOfBusiness.LineOfBusinessGUID into ljCustomerTableLineOfBusiness
					from lineOfBusiness in ljCustomerTableLineOfBusiness.DefaultIfEmpty()
					join introducedBy in db.Set<IntroducedBy>() on customerTable.IntroducedByGUID equals introducedBy.IntroducedByGUID into ljCustomerTableIntroducedBy
					from introducedBy in ljCustomerTableIntroducedBy.DefaultIfEmpty()
					join businessType in db.Set<BusinessType>() on customerTable.BusinessTypeGUID equals businessType.BusinessTypeGUID into ljCustomerTableBusinessType
					from businessType in ljCustomerTableBusinessType.DefaultIfEmpty()
					select new CustomerTableItemViewMap
					{
						CompanyGUID = customerTable.CompanyGUID,
						Owner = customerTable.Owner,
						OwnerBusinessUnitGUID = customerTable.OwnerBusinessUnitGUID,
						CustomerTableGUID = customerTable.CustomerTableGUID,
						CustomerId = customerTable.CustomerId,
						Name = customerTable.Name,
						DateOfBirth = customerTable.DateOfBirth,
						DateOfEstablish = customerTable.DateOfEstablish,
						RecordType = customerTable.RecordType,
						SigningCondition = customerTable.SigningCondition,
						BlacklistStatusGUID = customerTable.BlacklistStatusGUID,
						KYCSetupGUID = customerTable.KYCSetupGUID,
						CreditScoringGUID = customerTable.CreditScoringGUID,
						Dimension1GUID = customerTable.Dimension1GUID,
						Dimension2GUID = customerTable.Dimension2GUID,
						Dimension3GUID = customerTable.Dimension3GUID,
						Dimension4GUID = customerTable.Dimension4GUID,
						Dimension5GUID = customerTable.Dimension5GUID,
						IdentificationType = customerTable.IdentificationType,
						TaxID = customerTable.TaxID,
						PassportID = customerTable.PassportID,
						LineOfBusiness_Values = (lineOfBusiness != null) ? SmartAppUtil.GetDropDownLabel(lineOfBusiness.LineOfBusinessId, lineOfBusiness.Description) : null,
						IntroducedBy_Values = (introducedBy != null) ? SmartAppUtil.GetDropDownLabel(introducedBy.IntroducedById, introducedBy.Description) : null,
						BusinessType_Values = (businessType != null) ? SmartAppUtil.GetDropDownLabel(businessType.BusinessTypeId, businessType.Description) : null,
					});
		}
		public IEnumerable<SelectItem<CustomerTableItemView>> GetDropDownItemBy(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CustomerTable>(search, SysParm.CompanyGUID);
				var customerTable = GetDropDownQueryBy().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CustomerTableItemViewMap, CustomerTableItemView>().ToDropDownItem(search.ExcludeRowData);
				return customerTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private IQueryable<CustomerTableItemViewMap> GetDropDownQueryByGuarantor()
		{
			var result =  (from customerTable in Entity
					join mainAgreement in db.Set<MainAgreementTable>()
					on customerTable.CustomerTableGUID equals mainAgreement.CustomerTableGUID into ljmainAgreement
					from mainAgreement in ljmainAgreement.DefaultIfEmpty()
					join guarantorAgreement in db.Set<GuarantorAgreementTable>()
					on mainAgreement.MainAgreementTableGUID equals guarantorAgreement.MainAgreementTableGUID into ljguarantorAgreement
					from guarantorAgreement in ljguarantorAgreement.DefaultIfEmpty()
					join parentCompany in db.Set<ParentCompany>()
					on guarantorAgreement.ParentCompanyGUID equals parentCompany.ParentCompanyGUID into ljparentCompany
					from parentCompany in ljparentCompany.DefaultIfEmpty()
					join documentStatus in db.Set<DocumentStatus>()
					on mainAgreement.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus
					from documentStatus in ljdocumentStatus.DefaultIfEmpty()
					where documentStatus.StatusId != MainAgreementStatus.Cancelled.ToString()
						   select new CustomerTableItemViewMap
					{
						CompanyGUID = customerTable.CompanyGUID,
						Owner = customerTable.Owner,
						OwnerBusinessUnitGUID = customerTable.OwnerBusinessUnitGUID,
						CustomerTableGUID = customerTable.CustomerTableGUID,
						CustomerId = customerTable.CustomerId,
						Name = customerTable.Name,
						AltName = customerTable.AltName,
						DocumentStatus_StatusId = documentStatus.StatusId,
						GuarantorAgreement_GuarantorAgreementGUID = guarantorAgreement.GuarantorAgreementTableGUID,
					
					});
			return result;
		}
		public IEnumerable<SelectItem<CustomerTableItemView>> GetDropDownItemByGuarantor(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CustomerTable>(search, SysParm.CompanyGUID);
				var customerTable = GetDropDownQueryByGuarantor().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CustomerTableItemViewMap, CustomerTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return customerTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CustomerTableListViewMap> GetListQuery()
		{
			return (from customerTable in Entity
					join documentStatus in db.Set<DocumentStatus>()
					on customerTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljDocumentStatus
					from documentStatus in ljDocumentStatus.DefaultIfEmpty()
					select new CustomerTableListViewMap
					{
						CompanyGUID = customerTable.CompanyGUID,
						Owner = customerTable.Owner,
						OwnerBusinessUnitGUID = customerTable.OwnerBusinessUnitGUID,
						CustomerTableGUID = customerTable.CustomerTableGUID,
						CustomerId = customerTable.CustomerId,
						TaxID = customerTable.TaxID,
						PassportID = customerTable.PassportID,
						Name = customerTable.Name,
						RecordType = customerTable.RecordType,
						DocumentStatusGUID = customerTable.DocumentStatusGUID,
						DocumentStatus_StatusId = documentStatus.StatusId,
						DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description)
		});
		}
		public SearchResult<CustomerTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CustomerTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CustomerTable>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CustomerTableListViewMap, CustomerTableListView>();
				result = list.SetSearchResult<CustomerTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CustomerTableItemViewMap> GetItemQuery()
		{
			IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService();
			return (from customerTable in Entity
					join company in db.Set<Company>()
					on customerTable.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on customerTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCustomerTableOwnerBU
					from ownerBU in ljCustomerTableOwnerBU.DefaultIfEmpty()
					select new CustomerTableItemViewMap
					{
						CompanyGUID = customerTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = customerTable.Owner,
						OwnerBusinessUnitGUID = customerTable.OwnerBusinessUnitGUID,
						CreatedBy = customerTable.CreatedBy,
						CreatedDateTime = customerTable.CreatedDateTime,
						ModifiedBy = customerTable.ModifiedBy,
						ModifiedDateTime = customerTable.ModifiedDateTime,
						CustomerTableGUID = customerTable.CustomerTableGUID,
						Age = relatedPersonTableService.CalcAge(customerTable.DateOfBirth),
						AltName = customerTable.AltName,
						BlacklistStatusGUID = customerTable.BlacklistStatusGUID,
						BOTRating = customerTable.BOTRating,
						BusinessSegmentGUID = customerTable.BusinessSegmentGUID,
						BusinessSizeGUID = customerTable.BusinessSizeGUID,
						BusinessTypeGUID = customerTable.BusinessTypeGUID,
						CompanyName = customerTable.CompanyName,
						CompanyWebsite = customerTable.CompanyWebsite,
						CreditScoringGUID = customerTable.CreditScoringGUID,
						CurrencyGUID = customerTable.CurrencyGUID,
						CustGroupGUID = customerTable.CustGroupGUID,
						CustomerId = customerTable.CustomerId,
						DateOfBirth = customerTable.DateOfBirth,
						DateOfEstablish = customerTable.DateOfEstablish,
						DateOfExpiry = customerTable.DateOfExpiry,
						DateOfIssue = customerTable.DateOfIssue,
						Dimension1GUID = customerTable.Dimension1GUID,
						Dimension2GUID = customerTable.Dimension2GUID,
						Dimension3GUID = customerTable.Dimension3GUID,
						Dimension4GUID = customerTable.Dimension4GUID,
						Dimension5GUID = customerTable.Dimension5GUID,
						DocumentStatusGUID = customerTable.DocumentStatusGUID,
						DueDay = customerTable.DueDay,
						ExposureGroupGUID = customerTable.ExposureGroupGUID,
						FixedAsset = customerTable.FixedAsset,
						GenderGUID = customerTable.GenderGUID,
						GradeClassificationGUID = customerTable.GradeClassificationGUID,
						IdentificationType = customerTable.IdentificationType,
						Income = customerTable.Income,
						IntroducedByGUID = customerTable.IntroducedByGUID,
						IntroducedByRemark = customerTable.IntroducedByRemark,
						InvoiceIssuingDay = customerTable.InvoiceIssuingDay,
						IssuedBy = customerTable.IssuedBy,
						KYCSetupGUID = customerTable.KYCSetupGUID,
						Labor = customerTable.Labor,
						LineOfBusinessGUID = customerTable.LineOfBusinessGUID,
						MaritalStatusGUID = customerTable.MaritalStatusGUID,
						MethodOfPaymentGUID = customerTable.MethodOfPaymentGUID,
						Name = customerTable.Name,
						NationalityGUID = customerTable.NationalityGUID,
						NCBAccountStatusGUID = customerTable.NCBAccountStatusGUID,
						OccupationGUID = customerTable.OccupationGUID,
						OtherIncome = customerTable.OtherIncome,
						OtherSourceIncome = customerTable.OtherSourceIncome,
						PaidUpCapital = customerTable.PaidUpCapital,
						ParentCompanyGUID = customerTable.ParentCompanyGUID,
						PassportID = customerTable.PassportID,
						Position = customerTable.Position,
						PrivateARPct = customerTable.PrivateARPct,
						PublicARPct = customerTable.PublicARPct,
						RaceGUID = customerTable.RaceGUID,
						RecordType = customerTable.RecordType,
						ReferenceId = customerTable.ReferenceId,
						RegisteredCapital = customerTable.RegisteredCapital,
						RegistrationTypeGUID = customerTable.RegistrationTypeGUID,
						ResponsibleByGUID = customerTable.ResponsibleByGUID,
						SigningCondition = customerTable.SigningCondition,
						SpouseName = customerTable.SpouseName,
						TaxID = customerTable.TaxID,
						TerritoryGUID = customerTable.TerritoryGUID,
						VendorTableGUID = customerTable.VendorTableGUID,
						WithholdingTaxGroupGUID = customerTable.WithholdingTaxGroupGUID,
						WorkExperienceMonth = customerTable.WorkExperienceMonth,
						WorkExperienceYear = customerTable.WorkExperienceYear,
						WorkPermitID = customerTable.WorkPermitID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
					
						RowVersion = customerTable.RowVersion,
					});
		}
		public CustomerTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<CustomerTableItemViewMap, CustomerTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public CustomerTable CreateCustomerTable(CustomerTable customerTable)
		{
			try
			{
				customerTable.CustomerTableGUID = Guid.NewGuid();
				base.Add(customerTable);
				return customerTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCustomerTableVoid(CustomerTable customerTable)
		{
			try
			{
				CreateCustomerTable(customerTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CustomerTable UpdateCustomerTable(CustomerTable dbCustomerTable, CustomerTable inputCustomerTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCustomerTable = dbCustomerTable.MapUpdateValues<CustomerTable>(inputCustomerTable);
				base.Update(dbCustomerTable);
				return dbCustomerTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCustomerTableVoid(CustomerTable dbCustomerTable, CustomerTable inputCustomerTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCustomerTable = dbCustomerTable.MapUpdateValues<CustomerTable>(inputCustomerTable, skipUpdateFields);
				base.Update(dbCustomerTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public override void ValidateAdd(IEnumerable<CustomerTable> items)
		{
			foreach (var item in items)
			{
				ValidateAdd(item);
			}
		}
		public override void ValidateAdd(CustomerTable item)
		{
			CheckDupplicate(item);
			base.ValidateAdd(item);
		}
		public override void ValidateUpdate(IEnumerable<CustomerTable> items)
		{
			ValidateUpdate(items);
		}
		public override void ValidateUpdate(CustomerTable item)
		{
			CheckDupplicate(item);
			base.ValidateUpdate(item);
		}
		public void CheckDupplicate(CustomerTable item)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				bool isTaxIdDupplicate = IsTaxIdDuplicate(item.CustomerTableGUID.GuidNullToString(), item.TaxID).IsTaxIdDuplicate;
				bool isPassportIdDuplicate = IsPassportIdDuplicate(item.CustomerTableGUID.GuidNullToString(), item.PassportID).IsPassportIdDuplicate;

				if (isTaxIdDupplicate && item.IdentificationType == (int)IdentificationType.TaxId)
				{
					ex.AddData("ERROR.DUPLICATE", new string[] { "LABEL.TAX_ID" });
				}
				if (isPassportIdDuplicate && item.IdentificationType == (int)IdentificationType.PassportId)
				{
					ex.AddData("ERROR.DUPLICATE", new string[] { "LABEL.PASSPORT_ID"});
				}
				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion Create, Update, Delete

		public CustomerTableItemView IsPassportIdDuplicate(string customerId,string passPortId)
		{
			try
			{
				IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
				CustomerTableItemView result = new CustomerTableItemView();
				CustomerTable customerTable = Entity.Where(item => item.PassportID != "" && item.PassportID == passPortId && item.CustomerTableGUID != customerId.StringToGuid()).AsNoTracking().FirstOrDefault();
				if (customerTable != null)
				{
					result.IsPassportIdDuplicate = true;
					if (customerTable.ResponsibleByGUID != null)
					{
						EmployeeTable employeeTable = employeeTableRepo.GetByEmployeeGUIDNoTracking(customerTable.ResponsibleByGUID);
						result.PassportDuplicateEmployeeName = SmartAppUtil.GetDropDownLabel(employeeTable.EmployeeId, employeeTable.Name);
					}
				}
				else
				{
					result.IsPassportIdDuplicate = false;
				}

				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CustomerTableItemView IsTaxIdDuplicate(string customerId, string taxId)
		{
			try
			{
				IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
				CustomerTableItemView result = new CustomerTableItemView();
				CustomerTable customerTable = Entity.Where(item => item.TaxID != "" && item.TaxID == taxId && item.CustomerTableGUID != customerId.StringToGuid()).AsNoTracking().FirstOrDefault();
				if (customerTable != null)
				{
					result.IsTaxIdDuplicate = true;
					if (customerTable.ResponsibleByGUID != null)
					{
						EmployeeTable employeeTable = employeeTableRepo.GetByEmployeeGUIDNoTracking(customerTable.ResponsibleByGUID);
						result.PassportDuplicateEmployeeName = SmartAppUtil.GetDropDownLabel(employeeTable.EmployeeId, employeeTable.Name);
					}
				}
				else
				{
					result.IsTaxIdDuplicate = false;
				}

				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CustomerTable GetCustomerTableByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CustomerTableGUID == guid)
									.FilterByAccessLevel(SysParm.AccessLevel)
									.AsNoTracking()
									.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region function
		public UpdateCustomerTableStatusResultView GetCustomerStatusById(Guid guid)
		{
			try
			{
				var result = (from customer in Entity
							  join documentStatus in db.Set<DocumentStatus>()
							  on customer.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljCustomerDocumentStatus
							  from documentStatus in ljCustomerDocumentStatus.DefaultIfEmpty()
							  where customer.CustomerTableGUID == guid
							  select new UpdateCustomerTableStatusResultView
							  {
								  CustomerTableGUID = customer.CustomerTableGUID.ToString(),
								  OriginalDocumentStatusGUID = customer.DocumentStatusGUID.ToString(),
								  OriginalDocumentStatus = documentStatus.Description,
								  CustomerTableId = SmartAppUtil.GetDropDownLabel(customer.CustomerId,customer.Name)
							  }).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public UpdateCustomerTableBlacklistStatusResultView GetCustomerBlacklistStatusById(Guid guid)
		{
			try
			{
				var result = (from customer in Entity
							  join blacklistStatus in db.Set<BlacklistStatus>()
							  on customer.BlacklistStatusGUID equals blacklistStatus.BlacklistStatusGUID into ljCustomerBlacklistStatus
							  from blacklistStatus in ljCustomerBlacklistStatus.DefaultIfEmpty()
							  where customer.CustomerTableGUID == guid
							  select new UpdateCustomerTableBlacklistStatusResultView
							  {
								  CustomerTableGUID = customer.CustomerTableGUID.GuidNullToString(),
								  OriginalBlacklistStatusGUID = blacklistStatus.BlacklistStatusGUID.GuidNullToString(),
								  OriginalBlacklistStatus = blacklistStatus.Description,
								  CustomerTableId = SmartAppUtil.GetDropDownLabel(customer.CustomerId, customer.Name)

							  }).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}


		#endregion
		#region GetBookmarkDocumentCustomerAmendCreditLimitView
		public BookmarkDocumentCustomerAmendCreditLimitView GetBookmarkDocumentCustomerAmendCreditLimitView(Guid RefGUID, Guid BookmarkDocumentTransGUID)
		{
			try
			{
				BookmarkDocumentCustomerAmendCreditLimitView model = new BookmarkDocumentCustomerAmendCreditLimitView();

				QCreditAppRequestTableCrediLimit qCreditAppRequestTable = GetCreditAppRequestQuery(RefGUID, ((int)RefType.CreditAppRequestTable).ToString());
				SetModelByCreditAppRequestTable(model, qCreditAppRequestTable);

				List< QRetentionConditionTransCreditAppReqAssignmentCrediLimit> qRetentionConditionTrans = GetQRetentionConditionTrans(qCreditAppRequestTable.QCreditAppRequestTable_RefCreditAppTableGUID);
				QRetentionConditionTransCreditAppReqAssignmentCrediLimit qRetentionConditionTrans_Row_1 = GetQRetentionConditionTransEachRow(0, qRetentionConditionTrans);
				QRetentionConditionTransCreditAppReqAssignmentCrediLimit qRetentionConditionTrans_Row_2 = GetQRetentionConditionTransEachRow(1, qRetentionConditionTrans);
				QRetentionConditionTransCreditAppReqAssignmentCrediLimit qRetentionConditionTrans_Row_3 = GetQRetentionConditionTransEachRow(2, qRetentionConditionTrans);
				SetModelByRetentionConditionTransRow1(model, qRetentionConditionTrans_Row_1);
				SetModelByRetentionConditionTransRow2(model, qRetentionConditionTrans_Row_2);
				SetModelByRetentionConditionTransRow3(model, qRetentionConditionTrans_Row_3);

				List<QServiceFeeConditionTransCrediLimit> qServiceFeeConditionTransList_10 = GetServiceFeeConditionTransList(RefGUID);
				QServiceFeeConditionTransCrediLimit qServiceFeeConditionTrans_Row_1 = GetServiceFeeConditionTransEachRow(1, qServiceFeeConditionTransList_10);
				QServiceFeeConditionTransCrediLimit qServiceFeeConditionTrans_Row_2 = GetServiceFeeConditionTransEachRow(2, qServiceFeeConditionTransList_10);
				QServiceFeeConditionTransCrediLimit qServiceFeeConditionTrans_Row_3 = GetServiceFeeConditionTransEachRow(3, qServiceFeeConditionTransList_10);
				QServiceFeeConditionTransCrediLimit qServiceFeeConditionTrans_Row_4 = GetServiceFeeConditionTransEachRow(4, qServiceFeeConditionTransList_10);
				QServiceFeeConditionTransCrediLimit qServiceFeeConditionTrans_Row_5 = GetServiceFeeConditionTransEachRow(5, qServiceFeeConditionTransList_10);
				QServiceFeeConditionTransCrediLimit qServiceFeeConditionTrans_Row_6 = GetServiceFeeConditionTransEachRow(6, qServiceFeeConditionTransList_10);
				QServiceFeeConditionTransCrediLimit qServiceFeeConditionTrans_Row_7 = GetServiceFeeConditionTransEachRow(7, qServiceFeeConditionTransList_10);
				QServiceFeeConditionTransCrediLimit qServiceFeeConditionTrans_Row_8 = GetServiceFeeConditionTransEachRow(8, qServiceFeeConditionTransList_10);
				QServiceFeeConditionTransCrediLimit qServiceFeeConditionTrans_Row_9 = GetServiceFeeConditionTransEachRow(9, qServiceFeeConditionTransList_10);
				QServiceFeeConditionTransCrediLimit qServiceFeeConditionTrans_Row_10 = GetServiceFeeConditionTransEachRow(10, qServiceFeeConditionTransList_10);
				SetModelByServiceFeeConditionTransRow1(model, qServiceFeeConditionTrans_Row_1);
				SetModelByServiceFeeConditionTransRow2(model, qServiceFeeConditionTrans_Row_2);
				SetModelByServiceFeeConditionTransRow3(model, qServiceFeeConditionTrans_Row_3);
				SetModelByServiceFeeConditionTransRow4(model, qServiceFeeConditionTrans_Row_4);
				SetModelByServiceFeeConditionTransRow5(model, qServiceFeeConditionTrans_Row_5);
				SetModelByServiceFeeConditionTransRow6(model, qServiceFeeConditionTrans_Row_6);
				SetModelByServiceFeeConditionTransRow7(model, qServiceFeeConditionTrans_Row_7);
				SetModelByServiceFeeConditionTransRow8(model, qServiceFeeConditionTrans_Row_8);
				SetModelByServiceFeeConditionTransRow9(model, qServiceFeeConditionTrans_Row_9);
				SetModelByServiceFeeConditionTransRow10(model, qServiceFeeConditionTrans_Row_10);

				decimal qSumServiceFeeConditionTrans = GetSumServiceFeeConditionTrans(RefGUID);
				SetModelBySumServiceFeeConditionTrans(model, qSumServiceFeeConditionTrans);

				List<QGuarantorTransCrediLimit> qGuarantorTransList_8 = GetGuarantorTransList(qCreditAppRequestTable.QCreditAppRequestTable_RefCreditAppTableGUID.Value);
				QGuarantorTransCrediLimit qGuarantorTrans_Row_1 = GetGuarantorTransEachRow(1, qGuarantorTransList_8);
				QGuarantorTransCrediLimit qGuarantorTrans_Row_2 = GetGuarantorTransEachRow(2, qGuarantorTransList_8);
				QGuarantorTransCrediLimit qGuarantorTrans_Row_3 = GetGuarantorTransEachRow(3, qGuarantorTransList_8);
				QGuarantorTransCrediLimit qGuarantorTrans_Row_4 = GetGuarantorTransEachRow(4, qGuarantorTransList_8);
				QGuarantorTransCrediLimit qGuarantorTrans_Row_5 = GetGuarantorTransEachRow(5, qGuarantorTransList_8);
				QGuarantorTransCrediLimit qGuarantorTrans_Row_6 = GetGuarantorTransEachRow(6, qGuarantorTransList_8);
				QGuarantorTransCrediLimit qGuarantorTrans_Row_7 = GetGuarantorTransEachRow(7, qGuarantorTransList_8);
				QGuarantorTransCrediLimit qGuarantorTrans_Row_8 = GetGuarantorTransEachRow(8, qGuarantorTransList_8);
				SetModelByGuarantorTransRow1(model, qGuarantorTrans_Row_1);
				SetModelByGuarantorTransRow2(model, qGuarantorTrans_Row_2);
				SetModelByGuarantorTransRow3(model, qGuarantorTrans_Row_3);
				SetModelByGuarantorTransRow4(model, qGuarantorTrans_Row_4);
				SetModelByGuarantorTransRow5(model, qGuarantorTrans_Row_5);
				SetModelByGuarantorTransRow6(model, qGuarantorTrans_Row_6);
				SetModelByGuarantorTransRow7(model, qGuarantorTrans_Row_7);
				SetModelByGuarantorTransRow8(model, qGuarantorTrans_Row_8);

				List<QCreditAppReqBusinessCollateralCrediLimit> qCreditAppReqBusinessCollateralList_15 = GetCreditAppReqBusinessCollateralList(RefGUID);
				QCreditAppReqBusinessCollateralCrediLimit qCreditAppReqBusinessCollateral_Row_1 = GetCreditAppReqBusinessCollateralEachRow(1, qCreditAppReqBusinessCollateralList_15);
				QCreditAppReqBusinessCollateralCrediLimit qCreditAppReqBusinessCollateral_Row_2 = GetCreditAppReqBusinessCollateralEachRow(2, qCreditAppReqBusinessCollateralList_15);
				QCreditAppReqBusinessCollateralCrediLimit qCreditAppReqBusinessCollateral_Row_3 = GetCreditAppReqBusinessCollateralEachRow(3, qCreditAppReqBusinessCollateralList_15);
				QCreditAppReqBusinessCollateralCrediLimit qCreditAppReqBusinessCollateral_Row_4 = GetCreditAppReqBusinessCollateralEachRow(4, qCreditAppReqBusinessCollateralList_15);
				QCreditAppReqBusinessCollateralCrediLimit qCreditAppReqBusinessCollateral_Row_5 = GetCreditAppReqBusinessCollateralEachRow(5, qCreditAppReqBusinessCollateralList_15);
				QCreditAppReqBusinessCollateralCrediLimit qCreditAppReqBusinessCollateral_Row_6 = GetCreditAppReqBusinessCollateralEachRow(6, qCreditAppReqBusinessCollateralList_15);
				QCreditAppReqBusinessCollateralCrediLimit qCreditAppReqBusinessCollateral_Row_7 = GetCreditAppReqBusinessCollateralEachRow(7, qCreditAppReqBusinessCollateralList_15);
				QCreditAppReqBusinessCollateralCrediLimit qCreditAppReqBusinessCollateral_Row_8 = GetCreditAppReqBusinessCollateralEachRow(8, qCreditAppReqBusinessCollateralList_15);
				QCreditAppReqBusinessCollateralCrediLimit qCreditAppReqBusinessCollateral_Row_9 = GetCreditAppReqBusinessCollateralEachRow(9, qCreditAppReqBusinessCollateralList_15);
				QCreditAppReqBusinessCollateralCrediLimit qCreditAppReqBusinessCollateral_Row_10 = GetCreditAppReqBusinessCollateralEachRow(10, qCreditAppReqBusinessCollateralList_15);
				QCreditAppReqBusinessCollateralCrediLimit qCreditAppReqBusinessCollateral_Row_11 = GetCreditAppReqBusinessCollateralEachRow(11, qCreditAppReqBusinessCollateralList_15);
				QCreditAppReqBusinessCollateralCrediLimit qCreditAppReqBusinessCollateral_Row_12 = GetCreditAppReqBusinessCollateralEachRow(12, qCreditAppReqBusinessCollateralList_15);
				QCreditAppReqBusinessCollateralCrediLimit qCreditAppReqBusinessCollateral_Row_13 = GetCreditAppReqBusinessCollateralEachRow(13, qCreditAppReqBusinessCollateralList_15);
				QCreditAppReqBusinessCollateralCrediLimit qCreditAppReqBusinessCollateral_Row_14 = GetCreditAppReqBusinessCollateralEachRow(14, qCreditAppReqBusinessCollateralList_15);
				QCreditAppReqBusinessCollateralCrediLimit qCreditAppReqBusinessCollateral_Row_15 = GetCreditAppReqBusinessCollateralEachRow(15, qCreditAppReqBusinessCollateralList_15);
				SetModelByCreditAppReqBusinessCollateralRow1(model, qCreditAppReqBusinessCollateral_Row_1);
				SetModelByCreditAppReqBusinessCollateralRow2(model, qCreditAppReqBusinessCollateral_Row_2);
				SetModelByCreditAppReqBusinessCollateralRow3(model, qCreditAppReqBusinessCollateral_Row_3);
				SetModelByCreditAppReqBusinessCollateralRow4(model, qCreditAppReqBusinessCollateral_Row_4);
				SetModelByCreditAppReqBusinessCollateralRow5(model, qCreditAppReqBusinessCollateral_Row_5);
				SetModelByCreditAppReqBusinessCollateralRow6(model, qCreditAppReqBusinessCollateral_Row_6);
				SetModelByCreditAppReqBusinessCollateralRow7(model, qCreditAppReqBusinessCollateral_Row_7);
				SetModelByCreditAppReqBusinessCollateralRow8(model, qCreditAppReqBusinessCollateral_Row_8);
				SetModelByCreditAppReqBusinessCollateralRow9(model, qCreditAppReqBusinessCollateral_Row_9);
				SetModelByCreditAppReqBusinessCollateralRow10(model, qCreditAppReqBusinessCollateral_Row_10);
				SetModelByCreditAppReqBusinessCollateralRow11(model, qCreditAppReqBusinessCollateral_Row_11);
				SetModelByCreditAppReqBusinessCollateralRow12(model, qCreditAppReqBusinessCollateral_Row_12);
				SetModelByCreditAppReqBusinessCollateralRow13(model, qCreditAppReqBusinessCollateral_Row_13);
				SetModelByCreditAppReqBusinessCollateralRow14(model, qCreditAppReqBusinessCollateral_Row_14);
				SetModelByCreditAppReqBusinessCollateralRow15(model, qCreditAppReqBusinessCollateral_Row_15);


				decimal sumCreditAppReqBusinessCollateral = GetSumCreditAppReqBusinessCollateral(RefGUID);
				List<QAuthorizedPersonTransCrediLimit> qAuthorizedPersonTrans_8 = GetAuthorizedPersonTransList(qCreditAppRequestTable.QCreditAppRequestTable_RefCreditAppTableGUID.Value);
				QAuthorizedPersonTransCrediLimit qAuthorizedPersonTrans_Row_1 = GetAuthorizedPersonTransEachRow(1, qAuthorizedPersonTrans_8);
				QAuthorizedPersonTransCrediLimit qAuthorizedPersonTrans_Row_2 = GetAuthorizedPersonTransEachRow(2, qAuthorizedPersonTrans_8);
				QAuthorizedPersonTransCrediLimit qAuthorizedPersonTrans_Row_3 = GetAuthorizedPersonTransEachRow(3, qAuthorizedPersonTrans_8);
				QAuthorizedPersonTransCrediLimit qAuthorizedPersonTrans_Row_4 = GetAuthorizedPersonTransEachRow(4, qAuthorizedPersonTrans_8);
				QAuthorizedPersonTransCrediLimit qAuthorizedPersonTrans_Row_5 = GetAuthorizedPersonTransEachRow(5, qAuthorizedPersonTrans_8);
				QAuthorizedPersonTransCrediLimit qAuthorizedPersonTrans_Row_6 = GetAuthorizedPersonTransEachRow(6, qAuthorizedPersonTrans_8);
				QAuthorizedPersonTransCrediLimit qAuthorizedPersonTrans_Row_7 = GetAuthorizedPersonTransEachRow(7, qAuthorizedPersonTrans_8);
				QAuthorizedPersonTransCrediLimit qAuthorizedPersonTrans_Row_8 = GetAuthorizedPersonTransEachRow(8, qAuthorizedPersonTrans_8);
				SetModelByAuthorizedPersonTransRow1(model, qAuthorizedPersonTrans_Row_1);
				SetModelByAuthorizedPersonTransRow2(model, qAuthorizedPersonTrans_Row_2);
				SetModelByAuthorizedPersonTransRow3(model, qAuthorizedPersonTrans_Row_3);
				SetModelByAuthorizedPersonTransRow4(model, qAuthorizedPersonTrans_Row_4);
				SetModelByAuthorizedPersonTransRow5(model, qAuthorizedPersonTrans_Row_5);
				SetModelByAuthorizedPersonTransRow6(model, qAuthorizedPersonTrans_Row_6);
				SetModelByAuthorizedPersonTransRow7(model, qAuthorizedPersonTrans_Row_7);
				SetModelByAuthorizedPersonTransRow8(model, qAuthorizedPersonTrans_Row_8);

				List<QOwnerTransCrediLimit> qOwnerTrans_8 = GetOwnerTransList(qCreditAppRequestTable.QCreditAppRequestTable_RefCreditAppTableGUID.Value);
				QOwnerTransCrediLimit qOwnerTrans_Row_1 = GetOwnerTransEachRow(1, qOwnerTrans_8);
				QOwnerTransCrediLimit qOwnerTrans_Row_2 = GetOwnerTransEachRow(2, qOwnerTrans_8);
				QOwnerTransCrediLimit qOwnerTrans_Row_3 = GetOwnerTransEachRow(3, qOwnerTrans_8);
				QOwnerTransCrediLimit qOwnerTrans_Row_4 = GetOwnerTransEachRow(4, qOwnerTrans_8);
				QOwnerTransCrediLimit qOwnerTrans_Row_5 = GetOwnerTransEachRow(5, qOwnerTrans_8);
				QOwnerTransCrediLimit qOwnerTrans_Row_6 = GetOwnerTransEachRow(6, qOwnerTrans_8);
				QOwnerTransCrediLimit qOwnerTrans_Row_7 = GetOwnerTransEachRow(7, qOwnerTrans_8);
				QOwnerTransCrediLimit qOwnerTrans_Row_8 = GetOwnerTransEachRow(8, qOwnerTrans_8);
				SetModelByOwnerTransRow1(model, qOwnerTrans_Row_1);
				SetModelByOwnerTransRow2(model, qOwnerTrans_Row_2);
				SetModelByOwnerTransRow3(model, qOwnerTrans_Row_3);
				SetModelByOwnerTransRow4(model, qOwnerTrans_Row_4);
				SetModelByOwnerTransRow5(model, qOwnerTrans_Row_5);
				SetModelByOwnerTransRow6(model, qOwnerTrans_Row_6);
				SetModelByOwnerTransRow7(model, qOwnerTrans_Row_7);
				SetModelByOwnerTransRow8(model, qOwnerTrans_Row_8);


				List<QCreditAppReqLineFinCrediLimit> mainCreditAppReqLineFin_5 = GetCreditAppReqLineFinList(RefGUID);
				QCreditAppReqLineFinCrediLimit qCreditAppReqLineFin_Row_1 = GetCreditAppReqLineFinEachRow(1, mainCreditAppReqLineFin_5, RefGUID);
				QCreditAppReqLineFinCrediLimit qCreditAppReqLineFin_Row_2 = GetCreditAppReqLineFinEachRow(2, mainCreditAppReqLineFin_5, RefGUID);
				QCreditAppReqLineFinCrediLimit qCreditAppReqLineFin_Row_3 = GetCreditAppReqLineFinEachRow(3, mainCreditAppReqLineFin_5, RefGUID);
				QCreditAppReqLineFinCrediLimit qCreditAppReqLineFin_Row_4 = GetCreditAppReqLineFinEachRow(4, mainCreditAppReqLineFin_5, RefGUID);
				QCreditAppReqLineFinCrediLimit qCreditAppReqLineFin_Row_5 = GetCreditAppReqLineFinEachRow(5, mainCreditAppReqLineFin_5, RefGUID);
				SetModelByCreditAppReqLineFinRow1(model, qCreditAppReqLineFin_Row_1);
				SetModelByCreditAppReqLineFinRow2(model, qCreditAppReqLineFin_Row_2);
				SetModelByCreditAppReqLineFinRow3(model, qCreditAppReqLineFin_Row_3);
				SetModelByCreditAppReqLineFinRow4(model, qCreditAppReqLineFin_Row_4);
				SetModelByCreditAppReqLineFinRow5(model, qCreditAppReqLineFin_Row_5);

				List<QTaxReportTransCrediLimit> qTaxReportTransList_12 = GetTaxReportTransList(RefGUID);
				QTaxReportTransCrediLimit qTaxReportTrans_Row_1 = GetTaxReportTransEachRow(1, qTaxReportTransList_12);
				QTaxReportTransCrediLimit qTaxReportTrans_Row_2 = GetTaxReportTransEachRow(2, qTaxReportTransList_12);
				QTaxReportTransCrediLimit qTaxReportTrans_Row_3 = GetTaxReportTransEachRow(3, qTaxReportTransList_12);
				QTaxReportTransCrediLimit qTaxReportTrans_Row_4 = GetTaxReportTransEachRow(4, qTaxReportTransList_12);
				QTaxReportTransCrediLimit qTaxReportTrans_Row_5 = GetTaxReportTransEachRow(5, qTaxReportTransList_12);
				QTaxReportTransCrediLimit qTaxReportTrans_Row_6 = GetTaxReportTransEachRow(6, qTaxReportTransList_12);
				QTaxReportTransCrediLimit qTaxReportTrans_Row_7 = GetTaxReportTransEachRow(7, qTaxReportTransList_12);
				QTaxReportTransCrediLimit qTaxReportTrans_Row_8 = GetTaxReportTransEachRow(8, qTaxReportTransList_12);
				QTaxReportTransCrediLimit qTaxReportTrans_Row_9 = GetTaxReportTransEachRow(9, qTaxReportTransList_12);
				QTaxReportTransCrediLimit qTaxReportTrans_Row_10 = GetTaxReportTransEachRow(10, qTaxReportTransList_12);
				QTaxReportTransCrediLimit qTaxReportTrans_Row_11 = GetTaxReportTransEachRow(11, qTaxReportTransList_12);
				QTaxReportTransCrediLimit qTaxReportTrans_Row_12 = GetTaxReportTransEachRow(12, qTaxReportTransList_12);
				SetModelByTaxReportTransRow1(model, qTaxReportTrans_Row_1);
				SetModelByTaxReportTransRow2(model, qTaxReportTrans_Row_2);
				SetModelByTaxReportTransRow3(model, qTaxReportTrans_Row_3);
				SetModelByTaxReportTransRow4(model, qTaxReportTrans_Row_4);
				SetModelByTaxReportTransRow5(model, qTaxReportTrans_Row_5);
				SetModelByTaxReportTransRow6(model, qTaxReportTrans_Row_6);
				SetModelByTaxReportTransRow7(model, qTaxReportTrans_Row_7);
				SetModelByTaxReportTransRow8(model, qTaxReportTrans_Row_8);
				SetModelByTaxReportTransRow9(model, qTaxReportTrans_Row_9);
				SetModelByTaxReportTransRow10(model, qTaxReportTrans_Row_10);
				SetModelByTaxReportTransRow11(model, qTaxReportTrans_Row_11);
				SetModelByTaxReportTransRow12(model, qTaxReportTrans_Row_12);

				QTaxReportStartCrediLimit qTaxReportStart = GetTaxReportStart(RefGUID);
				SetModelByTaxReportStart(model, qTaxReportStart);

				QTaxReportEndCrediLimit qTaxReportEnd = GetTaxReportEnd(RefGUID);
				SetModelByTaxReportEnd(model, qTaxReportEnd);

				QTaxReportTransSumCrediLimit qTaxReportTransSum = GetTaxReportTransSum(RefGUID);
				SetModelByTaxReportTransSum(model, qTaxReportTransSum);

				List <QNCBTransCrediLimit> qNCBTrans_8 = GetNCBTrans(RefGUID);
				QNCBTransCrediLimit qNCBTrans_Row_1 = GetNCBTransEachRow(1, qNCBTrans_8);
				QNCBTransCrediLimit qNCBTrans_Row_2 = GetNCBTransEachRow(2, qNCBTrans_8);
				QNCBTransCrediLimit qNCBTrans_Row_3 = GetNCBTransEachRow(3, qNCBTrans_8);
				QNCBTransCrediLimit qNCBTrans_Row_4 = GetNCBTransEachRow(4, qNCBTrans_8);
				QNCBTransCrediLimit qNCBTrans_Row_5 = GetNCBTransEachRow(5, qNCBTrans_8);
				QNCBTransCrediLimit qNCBTrans_Row_6 = GetNCBTransEachRow(6, qNCBTrans_8);
				QNCBTransCrediLimit qNCBTrans_Row_7 = GetNCBTransEachRow(7, qNCBTrans_8);
				QNCBTransCrediLimit qNCBTrans_Row_8 = GetNCBTransEachRow(8, qNCBTrans_8);
				SetModelByNCBTransRow1(model, qNCBTrans_Row_1);
				SetModelByNCBTransRow2(model, qNCBTrans_Row_2);
				SetModelByNCBTransRow3(model, qNCBTrans_Row_3);
				SetModelByNCBTransRow4(model, qNCBTrans_Row_4);
				SetModelByNCBTransRow5(model, qNCBTrans_Row_5);
				SetModelByNCBTransRow6(model, qNCBTrans_Row_6);
				SetModelByNCBTransRow7(model, qNCBTrans_Row_7);
				SetModelByNCBTransRow8(model, qNCBTrans_Row_8);

				QNCBTransSumCrediLimit qNCBTransSum = GetNCBTransSum(RefGUID);
				SetModelByNCBTransSum(model, qNCBTransSum);

				List<QCAReqCreditOutStandingCrediLimit> qCAReqCreditOutStandings = GetCAReqCreditOutStanding(RefGUID);
				SetModelByCAReqCreditOutStanding(model, qCAReqCreditOutStandings);

				QCAReqCreditOutStandingSumCrediLimit qCAReqCreditOutStandingSum = GetCAReqCreditOutStandingSum(RefGUID);
				SetModelByCAReqCreditOutStandingSum(model, qCAReqCreditOutStandingSum);

				GetCAReqCreditOutStandingAsOfDate(RefGUID, model);

				List<QActionHistoryCrediLimit> qActionHistory_8 = GetActionHistory(RefGUID);
				QActionHistoryCrediLimit qActionHistory_Row_1 = GetActionHistoryEachRow(1, qActionHistory_8);
				QActionHistoryCrediLimit qActionHistory_Row_2 = GetActionHistoryEachRow(2, qActionHistory_8);
				QActionHistoryCrediLimit qActionHistory_Row_3 = GetActionHistoryEachRow(3, qActionHistory_8);
				QActionHistoryCrediLimit qActionHistory_Row_4 = GetActionHistoryEachRow(4, qActionHistory_8);
				QActionHistoryCrediLimit qActionHistory_Row_5 = GetActionHistoryEachRow(5, qActionHistory_8);
				QActionHistoryCrediLimit qActionHistory_Row_6 = GetActionHistoryEachRow(6, qActionHistory_8);
				QActionHistoryCrediLimit qActionHistory_Row_7 = GetActionHistoryEachRow(7, qActionHistory_8);
				QActionHistoryCrediLimit qActionHistory_Row_8 = GetActionHistoryEachRow(8, qActionHistory_8);
				SetModelByActionHistoryRow1(model, qActionHistory_Row_1);
				SetModelByActionHistoryRow2(model, qActionHistory_Row_2);
				SetModelByActionHistoryRow3(model, qActionHistory_Row_3);
				SetModelByActionHistoryRow4(model, qActionHistory_Row_4);
				SetModelByActionHistoryRow5(model, qActionHistory_Row_5);


				List <QCreditAppReqAssignmentCrediLimit> qCreditAppReqAssignment_10 = GetqCreditAppReqAssignment(RefGUID);
				QCreditAppReqAssignmentCrediLimit qCreditAppReqAssignment_Row_1 = GetqCreditAppReqAssignmentEachRow(1, qCreditAppReqAssignment_10);
				QCreditAppReqAssignmentCrediLimit qCreditAppReqAssignment_Row_2 = GetqCreditAppReqAssignmentEachRow(2, qCreditAppReqAssignment_10);
				QCreditAppReqAssignmentCrediLimit qCreditAppReqAssignment_Row_3 = GetqCreditAppReqAssignmentEachRow(3, qCreditAppReqAssignment_10);
				QCreditAppReqAssignmentCrediLimit qCreditAppReqAssignment_Row_4 = GetqCreditAppReqAssignmentEachRow(4, qCreditAppReqAssignment_10);
				QCreditAppReqAssignmentCrediLimit qCreditAppReqAssignment_Row_5 = GetqCreditAppReqAssignmentEachRow(5, qCreditAppReqAssignment_10);
				QCreditAppReqAssignmentCrediLimit qCreditAppReqAssignment_Row_6 = GetqCreditAppReqAssignmentEachRow(6, qCreditAppReqAssignment_10);
				QCreditAppReqAssignmentCrediLimit qCreditAppReqAssignment_Row_7 = GetqCreditAppReqAssignmentEachRow(7, qCreditAppReqAssignment_10);
				QCreditAppReqAssignmentCrediLimit qCreditAppReqAssignment_Row_8 = GetqCreditAppReqAssignmentEachRow(8, qCreditAppReqAssignment_10);
				QCreditAppReqAssignmentCrediLimit qCreditAppReqAssignment_Row_9 = GetqCreditAppReqAssignmentEachRow(9, qCreditAppReqAssignment_10);
				QCreditAppReqAssignmentCrediLimit qCreditAppReqAssignment_Row_10 = GetqCreditAppReqAssignmentEachRow(10, qCreditAppReqAssignment_10);
				SetModelByCreditAppReqAssignmentRow1(model, qCreditAppReqAssignment_Row_1);
				SetModelByCreditAppReqAssignmentRow2(model, qCreditAppReqAssignment_Row_2);
				SetModelByCreditAppReqAssignmentRow3(model, qCreditAppReqAssignment_Row_3);
				SetModelByCreditAppReqAssignmentRow4(model, qCreditAppReqAssignment_Row_4);
				SetModelByCreditAppReqAssignmentRow5(model, qCreditAppReqAssignment_Row_5);
				SetModelByCreditAppReqAssignmentRow6(model, qCreditAppReqAssignment_Row_6);
				SetModelByCreditAppReqAssignmentRow7(model, qCreditAppReqAssignment_Row_7);
				SetModelByCreditAppReqAssignmentRow8(model, qCreditAppReqAssignment_Row_8);
				SetModelByCreditAppReqAssignmentRow9(model, qCreditAppReqAssignment_Row_9);
				SetModelByCreditAppReqAssignmentRow10(model, qCreditAppReqAssignment_Row_10);
				var originalCreditAppTableId = GetOriginalCreditAppTableId(qCreditAppRequestTable.QCreditAppRequestTable_RefCreditAppTableGUID);
				SetModelByOriginalCreditAppId(model, originalCreditAppTableId);


				QSumCreditAppReqAssignmentCrediLimit qSumCreditAppReqAssignment = GetSumCreditAppReqAssignment(RefGUID);
				SetModelBySumCreditAppReqAssignment(model, qSumCreditAppReqAssignment);
				SetModelVariableCustCompanyRegistrationID(model, qCreditAppRequestTable);
				SetModelVariableCustComEstablishedDate(model, qCreditAppRequestTable);
				SetModelVariableApprovedCreditLimitAmt(model, qCreditAppRequestTable.QCreditAppRequestTable_CreditLimitRequest, qCreditAppRequestTable.QOriginalCreditAppRequest_CreditLimitRequest);
				SetModelQSumCreditAppReqBusinessCollateral_SumBusinessCollateralValue(model, sumCreditAppReqBusinessCollateral);
				return model;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByRetentionConditionTransRow3(BookmarkDocumentCustomerAmendCreditLimitView model, QRetentionConditionTransCreditAppReqAssignmentCrediLimit retentionModel)
		{
			try
			{

				decimal? deductionMethod = retentionModel.QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionDeductionMethod;
				decimal? calculateBase = retentionModel.QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionCalculateBase;
				model.QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionDeductionMethod_Row_3 = null == deductionMethod ? null : SystemStaticData.GetTranslatedMessage(((RetentionDeductionMethod)deductionMethod).GetAttrCode());
				model.QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionCalculateBase_Row_3 = null == calculateBase ? null : SystemStaticData.GetTranslatedMessage(((RetentionCalculateBase)calculateBase).GetAttrCode());
				model.QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionPct_Row_3 = retentionModel.QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionPct;
				model.QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionAmount_Row_3 = retentionModel.QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionAmount;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByRetentionConditionTransRow2(BookmarkDocumentCustomerAmendCreditLimitView model, QRetentionConditionTransCreditAppReqAssignmentCrediLimit retentionModel)
		{
			try
			{

				decimal? deductionMethod = retentionModel.QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionDeductionMethod;
				decimal? calculateBase = retentionModel.QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionCalculateBase;
				model.QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionDeductionMethod_Row_2 = null == deductionMethod ? null : SystemStaticData.GetTranslatedMessage(((RetentionDeductionMethod)deductionMethod).GetAttrCode());
				model.QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionCalculateBase_Row_2 = null == calculateBase ? null : SystemStaticData.GetTranslatedMessage(((RetentionCalculateBase)calculateBase).GetAttrCode());
				model.QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionPct_Row_2 = retentionModel.QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionPct;
				model.QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionAmount_Row_2 = retentionModel.QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionAmount;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void SetModelByRetentionConditionTransRow1(BookmarkDocumentCustomerAmendCreditLimitView model, QRetentionConditionTransCreditAppReqAssignmentCrediLimit retentionModel)
        {
			try
			{
				decimal? deductionMethod = retentionModel.QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionDeductionMethod;
				decimal? calculateBase = retentionModel.QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionCalculateBase;
				model.QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionDeductionMethod_Row_1 = null == deductionMethod?null:  SystemStaticData.GetTranslatedMessage(((RetentionDeductionMethod)deductionMethod).GetAttrCode());
				model.QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionCalculateBase_Row_1 = null == calculateBase ? null : SystemStaticData.GetTranslatedMessage(((RetentionCalculateBase)calculateBase).GetAttrCode()) ;
				model.QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionPct_Row_1 = retentionModel.QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionPct;
				model.QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionAmount_Row_1 = retentionModel.QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionAmount;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private QRetentionConditionTransCreditAppReqAssignmentCrediLimit GetQRetentionConditionTransEachRow(int index, List<QRetentionConditionTransCreditAppReqAssignmentCrediLimit> qRetentionConditionTrans)

        {
			try
			{
				if(index< qRetentionConditionTrans.Count())
                {
					return qRetentionConditionTrans[index];
                }
                else
                {
					return new QRetentionConditionTransCreditAppReqAssignmentCrediLimit();

				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private List< QRetentionConditionTransCreditAppReqAssignmentCrediLimit> GetQRetentionConditionTrans(Guid? qCreditAppRequestTable_RefCreditAppTableGUID)
        {
			try
			{
				var result = (from retentionTrans in db.Set<RetentionConditionTrans>()
							  where retentionTrans.RefGUID == qCreditAppRequestTable_RefCreditAppTableGUID
							  select new QRetentionConditionTransCreditAppReqAssignmentCrediLimit
							  {
								  QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionCalculateBase = retentionTrans.RetentionCalculateBase,
								  QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionPct = retentionTrans.RetentionPct,
								  QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionDeductionMethod = retentionTrans.RetentionDeductionMethod,
								  QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionAmount = retentionTrans.RetentionAmount

							  }
							  ).OrderBy(or => or.QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionDeductionMethod).Take(3).ToList();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private void SetModelByOriginalCreditAppId(BookmarkDocumentCustomerAmendCreditLimitView model, string originalCreditAppTableId)
        {
			try
			{
				model.OriginalCreditAppTableId = originalCreditAppTableId;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
			

		}

        private string GetOriginalCreditAppTableId(Guid? refGUID)
        {
            try
            {
				var result = (from originalCreditApp in db.Set<CreditAppTable>()
							  where originalCreditApp.CreditAppTableGUID == refGUID
							  select originalCreditApp.CreditAppId
							  ).FirstOrDefault();
                if (result != null)
                {
					return result;
                }
                else
                {
					return "";
                }
            }
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private void SetModelQSumCreditAppReqBusinessCollateral_SumBusinessCollateralValue(BookmarkDocumentCustomerAmendCreditLimitView model, decimal sumCreditAppReqBusinessCollateral)
        {
			try
			{
				model.QSumCreditAppReqBusinessCollateral_SumBusinessCollateralValue = sumCreditAppReqBusinessCollateral;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private void SetModelVariableApprovedCreditLimitAmt(BookmarkDocumentCustomerAmendCreditLimitView model, decimal a, decimal b)
        {
			try
			{
				model.QCreditAppRequestTable_CreditLimitRequest = (a - b).DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
        }
		private void SetModelVariableCustComEstablishedDate(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppRequestTableCrediLimit data)
        {
			try
			{
				if (data.QCustomer_RecordType == (int)RecordType.Person)
				{
					model.QVariable_CustComEstablishedDate = data.QCustomer_DateOfBirth.DateNullToString();
				}
				if (data.QCustomer_RecordType == (int)RecordType.Organization)
				{
					model.QVariable_CustComEstablishedDate = data.QCustomer_DateOfEstablish.DateNullToString();
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
			
		}

        private void SetModelVariableCustCompanyRegistrationID(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppRequestTableCrediLimit data)
        {
			try
			{
				if (data.QCustomer_IdentificationType == (int)IdentificationType.TaxId)
				{
					model.QVariable_CustCompanyRegistrationID = data.QCustomer_TaxID;
				}
				if (data.QCustomer_IdentificationType == (int)IdentificationType.PassportId)
				{
					model.QVariable_CustCompanyRegistrationID = data.QCustomer_PassportID;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        private void SetModelBySumCreditAppReqAssignment(BookmarkDocumentCustomerAmendCreditLimitView model, QSumCreditAppReqAssignmentCrediLimit data)
        {

			try
			{
				model.QSumCreditAppReqAssignment_SumBuyerAgreementAmount = data.QSumCreditAppReqAssignment_SumBuyerAgreementAmount.DecimalToString();
				model.QSumCreditAppReqAssignment_SumAssignmentAgreementAmount = data.QSumCreditAppReqAssignment_SumAssignmentAgreementAmount.DecimalToString();
				model.QSumCreditAppReqAssignment_SumSumRemainingAmount = data.QSumCreditAppReqAssignment_SumSumRemainingAmount.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
        #region SetModelByCreditAppReqAssignment
        private void SetModelByCreditAppReqAssignmentRow1(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqAssignmentCrediLimit data)
		{
			try
			{
				if (data == null)
				{
					return;
				}
				model.QVariable_IsNew1 = GetVariableIsNew(data.QCreditAppReqAssignment_IsNew);
				model.QCreditAppReqAssignment_IsNew_1 = data.QCreditAppReqAssignment_IsNew;
				model.QCreditAppReqAssignment_AssignmentBuyer_1 = data.QCreditAppReqAssignment_AssignmentBuyer;
				model.QCreditAppReqAssignment_RefAgreementId_1 = data.QCreditAppReqAssignment_RefAgreementId;
				model.QCreditAppReqAssignment_BuyerAgreementAmount_1 = data.QCreditAppReqAssignment_BuyerAgreementAmount.DecimalToString();
				model.QCreditAppReqAssignment_AssignmentAgreementAmount_1 = data.QCreditAppReqAssignment_AssignmentAgreementAmount.DecimalToString();
				model.QCreditAppReqAssignment_AssignmentAgreementDate_1 = data.QCreditAppReqAssignment_AssignmentAgreementDate.DateNullToString();
				model.QCreditAppReqAssignment_RemainingAmount_1 = data.QCreditAppReqAssignment_RemainingAmount.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByCreditAppReqAssignmentRow2(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqAssignmentCrediLimit data)
		{
			try
			{
				if (data == null)
				{
					return;
				}
				model.QVariable_IsNew2 = GetVariableIsNew(data.QCreditAppReqAssignment_IsNew);
				model.QCreditAppReqAssignment_IsNew_2 = data.QCreditAppReqAssignment_IsNew;
				model.QCreditAppReqAssignment_AssignmentBuyer_2 = data.QCreditAppReqAssignment_AssignmentBuyer;
				model.QCreditAppReqAssignment_RefAgreementId_2 = data.QCreditAppReqAssignment_RefAgreementId;
				model.QCreditAppReqAssignment_BuyerAgreementAmount_2 = data.QCreditAppReqAssignment_BuyerAgreementAmount.DecimalToString();
				model.QCreditAppReqAssignment_AssignmentAgreementAmount_2 = data.QCreditAppReqAssignment_AssignmentAgreementAmount.DecimalToString();
				model.QCreditAppReqAssignment_AssignmentAgreementDate_2 = data.QCreditAppReqAssignment_AssignmentAgreementDate.DateNullToString();
				model.QCreditAppReqAssignment_RemainingAmount_2 = data.QCreditAppReqAssignment_RemainingAmount.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByCreditAppReqAssignmentRow3(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqAssignmentCrediLimit data)
		{
			try
			{
				if (data == null)
				{
					return;
				}
				model.QVariable_IsNew3 = GetVariableIsNew(data.QCreditAppReqAssignment_IsNew);
				model.QCreditAppReqAssignment_IsNew_3 = data.QCreditAppReqAssignment_IsNew;
				model.QCreditAppReqAssignment_AssignmentBuyer_3 = data.QCreditAppReqAssignment_AssignmentBuyer;
				model.QCreditAppReqAssignment_RefAgreementId_3 = data.QCreditAppReqAssignment_RefAgreementId;
				model.QCreditAppReqAssignment_BuyerAgreementAmount_3 = data.QCreditAppReqAssignment_BuyerAgreementAmount.DecimalToString();
				model.QCreditAppReqAssignment_AssignmentAgreementAmount_3 = data.QCreditAppReqAssignment_AssignmentAgreementAmount.DecimalToString();
				model.QCreditAppReqAssignment_AssignmentAgreementDate_3 = data.QCreditAppReqAssignment_AssignmentAgreementDate.DateNullToString();
				model.QCreditAppReqAssignment_RemainingAmount_3 = data.QCreditAppReqAssignment_RemainingAmount.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByCreditAppReqAssignmentRow4(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqAssignmentCrediLimit data)
		{
			try
			{
				if (data == null)
				{
					return;
				}
				model.QVariable_IsNew4 = GetVariableIsNew(data.QCreditAppReqAssignment_IsNew);
				model.QCreditAppReqAssignment_IsNew_4 = data.QCreditAppReqAssignment_IsNew;
				model.QCreditAppReqAssignment_AssignmentBuyer_4 = data.QCreditAppReqAssignment_AssignmentBuyer;
				model.QCreditAppReqAssignment_RefAgreementId_4 = data.QCreditAppReqAssignment_RefAgreementId;
				model.QCreditAppReqAssignment_BuyerAgreementAmount_4 = data.QCreditAppReqAssignment_BuyerAgreementAmount.DecimalToString();
				model.QCreditAppReqAssignment_AssignmentAgreementAmount_4 = data.QCreditAppReqAssignment_AssignmentAgreementAmount.DecimalToString();
				model.QCreditAppReqAssignment_AssignmentAgreementDate_4 = data.QCreditAppReqAssignment_AssignmentAgreementDate.DateNullToString();
				model.QCreditAppReqAssignment_RemainingAmount_4 = data.QCreditAppReqAssignment_RemainingAmount.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByCreditAppReqAssignmentRow5(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqAssignmentCrediLimit data)
		{
			try
			{
				if (data == null)
				{
					return;
				}
				model.QVariable_IsNew5 = GetVariableIsNew(data.QCreditAppReqAssignment_IsNew);
				model.QCreditAppReqAssignment_IsNew_5 = data.QCreditAppReqAssignment_IsNew;
				model.QCreditAppReqAssignment_AssignmentBuyer_5 = data.QCreditAppReqAssignment_AssignmentBuyer;
				model.QCreditAppReqAssignment_RefAgreementId_5 = data.QCreditAppReqAssignment_RefAgreementId;
				model.QCreditAppReqAssignment_BuyerAgreementAmount_5 = data.QCreditAppReqAssignment_BuyerAgreementAmount.DecimalToString();
				model.QCreditAppReqAssignment_AssignmentAgreementAmount_5 = data.QCreditAppReqAssignment_AssignmentAgreementAmount.DecimalToString();
				model.QCreditAppReqAssignment_AssignmentAgreementDate_5 = data.QCreditAppReqAssignment_AssignmentAgreementDate.DateNullToString();
				model.QCreditAppReqAssignment_RemainingAmount_5 = data.QCreditAppReqAssignment_RemainingAmount.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByCreditAppReqAssignmentRow6(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqAssignmentCrediLimit data)
		{
			try
			{
				if (data == null)
				{
					return;
				}
				model.QVariable_IsNew6 = GetVariableIsNew(data.QCreditAppReqAssignment_IsNew);
				model.QCreditAppReqAssignment_IsNew_6 = data.QCreditAppReqAssignment_IsNew;
				model.QCreditAppReqAssignment_AssignmentBuyer_6 = data.QCreditAppReqAssignment_AssignmentBuyer;
				model.QCreditAppReqAssignment_RefAgreementId_6 = data.QCreditAppReqAssignment_RefAgreementId;
				model.QCreditAppReqAssignment_BuyerAgreementAmount_6 = data.QCreditAppReqAssignment_BuyerAgreementAmount.DecimalToString();
				model.QCreditAppReqAssignment_AssignmentAgreementAmount_6 = data.QCreditAppReqAssignment_AssignmentAgreementAmount.DecimalToString();
				model.QCreditAppReqAssignment_AssignmentAgreementDate_6 = data.QCreditAppReqAssignment_AssignmentAgreementDate.DateNullToString();
				model.QCreditAppReqAssignment_RemainingAmount_6 = data.QCreditAppReqAssignment_RemainingAmount.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByCreditAppReqAssignmentRow7(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqAssignmentCrediLimit data)
		{
			try
			{
				if (data == null)
				{
					return;
				}
				model.QVariable_IsNew7 = GetVariableIsNew(data.QCreditAppReqAssignment_IsNew);
				model.QCreditAppReqAssignment_IsNew_7 = data.QCreditAppReqAssignment_IsNew;
				model.QCreditAppReqAssignment_AssignmentBuyer_7 = data.QCreditAppReqAssignment_AssignmentBuyer;
				model.QCreditAppReqAssignment_RefAgreementId_7 = data.QCreditAppReqAssignment_RefAgreementId;
				model.QCreditAppReqAssignment_BuyerAgreementAmount_7 = data.QCreditAppReqAssignment_BuyerAgreementAmount.DecimalToString();
				model.QCreditAppReqAssignment_AssignmentAgreementAmount_7 = data.QCreditAppReqAssignment_AssignmentAgreementAmount.DecimalToString();
				model.QCreditAppReqAssignment_AssignmentAgreementDate_7 = data.QCreditAppReqAssignment_AssignmentAgreementDate.DateNullToString();
				model.QCreditAppReqAssignment_RemainingAmount_7 = data.QCreditAppReqAssignment_RemainingAmount.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByCreditAppReqAssignmentRow8(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqAssignmentCrediLimit data)
		{
			try
			{
				if (data == null)
				{
					return;
				}
				model.QVariable_IsNew8 = GetVariableIsNew(data.QCreditAppReqAssignment_IsNew);
				model.QCreditAppReqAssignment_IsNew_8 = data.QCreditAppReqAssignment_IsNew;
				model.QCreditAppReqAssignment_AssignmentBuyer_8 = data.QCreditAppReqAssignment_AssignmentBuyer;
				model.QCreditAppReqAssignment_RefAgreementId_8 = data.QCreditAppReqAssignment_RefAgreementId;
				model.QCreditAppReqAssignment_BuyerAgreementAmount_8 = data.QCreditAppReqAssignment_BuyerAgreementAmount.DecimalToString();
				model.QCreditAppReqAssignment_AssignmentAgreementAmount_8 = data.QCreditAppReqAssignment_AssignmentAgreementAmount.DecimalToString();
				model.QCreditAppReqAssignment_AssignmentAgreementDate_8 = data.QCreditAppReqAssignment_AssignmentAgreementDate.DateNullToString();
				model.QCreditAppReqAssignment_RemainingAmount_8 = data.QCreditAppReqAssignment_RemainingAmount.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByCreditAppReqAssignmentRow9(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqAssignmentCrediLimit data)
		{
			try
			{
                if (data == null)
                {
					return;
                }
				model.QVariable_IsNew9 = GetVariableIsNew(data.QCreditAppReqAssignment_IsNew);
				model.QCreditAppReqAssignment_IsNew_9 = data.QCreditAppReqAssignment_IsNew;
				model.QCreditAppReqAssignment_AssignmentBuyer_9 = data.QCreditAppReqAssignment_AssignmentBuyer;
				model.QCreditAppReqAssignment_RefAgreementId_9 = data.QCreditAppReqAssignment_RefAgreementId;
				model.QCreditAppReqAssignment_BuyerAgreementAmount_9 = data.QCreditAppReqAssignment_BuyerAgreementAmount.DecimalToString();
				model.QCreditAppReqAssignment_AssignmentAgreementAmount_9 = data.QCreditAppReqAssignment_AssignmentAgreementAmount.DecimalToString();
				model.QCreditAppReqAssignment_AssignmentAgreementDate_9 = data.QCreditAppReqAssignment_AssignmentAgreementDate.DateNullToString();
				model.QCreditAppReqAssignment_RemainingAmount_9 = data.QCreditAppReqAssignment_RemainingAmount.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByCreditAppReqAssignmentRow10(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqAssignmentCrediLimit data)
		{
			try
			{
				if (data == null)
				{
					return;
				}
				model.QVariable_IsNew10 = GetVariableIsNew(data.QCreditAppReqAssignment_IsNew);
				model.QCreditAppReqAssignment_IsNew_10 = data.QCreditAppReqAssignment_IsNew;
				model.QCreditAppReqAssignment_AssignmentBuyer_10 = data.QCreditAppReqAssignment_AssignmentBuyer;
				model.QCreditAppReqAssignment_RefAgreementId_10 = data.QCreditAppReqAssignment_RefAgreementId;
				model.QCreditAppReqAssignment_BuyerAgreementAmount_10 = data.QCreditAppReqAssignment_BuyerAgreementAmount.DecimalToString();
				model.QCreditAppReqAssignment_AssignmentAgreementAmount_10 = data.QCreditAppReqAssignment_AssignmentAgreementAmount.DecimalToString();
				model.QCreditAppReqAssignment_AssignmentAgreementDate_10 = data.QCreditAppReqAssignment_AssignmentAgreementDate.DateNullToString();
				model.QCreditAppReqAssignment_RemainingAmount_10 = data.QCreditAppReqAssignment_RemainingAmount.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		#endregion SetModelByCreditAppReqAssignment
		#region SetModelByActionHistory
		private void SetModelByActionHistoryRow1(BookmarkDocumentCustomerAmendCreditLimitView model, QActionHistoryCrediLimit data)
		{
			try
			{
				model.QActionHistory_ApproverName_1 = data.QActionHistory_ApproverName;
				model.QActionHistory_Comment_1 = data.QActionHistory_Comment;
				model.QActionHistory_CreatedDateTime_1 = data.QActionHistory_CreatedDateTime.DateNullToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByActionHistoryRow2(BookmarkDocumentCustomerAmendCreditLimitView model, QActionHistoryCrediLimit data)
		{
			try
			{
				model.QActionHistory_ApproverName_2 = data.QActionHistory_ApproverName;
				model.QActionHistory_Comment_2 = data.QActionHistory_Comment;
				model.QActionHistory_CreatedDateTime_2 = data.QActionHistory_CreatedDateTime.DateNullToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByActionHistoryRow3(BookmarkDocumentCustomerAmendCreditLimitView model, QActionHistoryCrediLimit data)
		{
			try
			{
				model.QActionHistory_ApproverName_3 = data.QActionHistory_ApproverName;
				model.QActionHistory_Comment_3 = data.QActionHistory_Comment;
				model.QActionHistory_CreatedDateTime_3 = data.QActionHistory_CreatedDateTime.DateNullToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByActionHistoryRow4(BookmarkDocumentCustomerAmendCreditLimitView model, QActionHistoryCrediLimit data)
		{
			try
			{
				model.QActionHistory_ApproverName_4 = data.QActionHistory_ApproverName;
				model.QActionHistory_Comment_4 = data.QActionHistory_Comment;
				model.QActionHistory_CreatedDateTime_4 = data.QActionHistory_CreatedDateTime.DateNullToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByActionHistoryRow5(BookmarkDocumentCustomerAmendCreditLimitView model, QActionHistoryCrediLimit data)
		{
			try
			{
				model.QActionHistory_ApproverName_5 = data.QActionHistory_ApproverName;
				model.QActionHistory_Comment_5 = data.QActionHistory_Comment;
				model.QActionHistory_CreatedDateTime_5 = data.QActionHistory_CreatedDateTime.DateNullToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}

        #endregion SetModelByActionHistory
        private void SetModelByCAReqCreditOutStandingSum(BookmarkDocumentCustomerAmendCreditLimitView model, QCAReqCreditOutStandingSumCrediLimit data)
		{
			try
			{
				model.QCAReqCreditOutStandingSum_SumAccumRetentionAmount = data.QCAReqCreditOutStandingSum_SumAccumRetentionAmount.DecimalToString();
				model.QCAReqCreditOutStandingSum_SumApprovedCreditLimit = data.QCAReqCreditOutStandingSum_SumApprovedCreditLimit.DecimalToString();
				model.QCAReqCreditOutStandingSum_SumARBalance = data.QCAReqCreditOutStandingSum_SumARBalance.DecimalToString();
				model.QCAReqCreditOutStandingSum_SumCreditLimitBalance = data.QCAReqCreditOutStandingSum_SumCreditLimitBalance.DecimalToString();
				model.QCAReqCreditOutStandingSum_SumReserveToBeRefund = data.QCAReqCreditOutStandingSum_SumReserveToBeRefund.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}


		}
		private void SetModelByCAReqCreditOutStanding(BookmarkDocumentCustomerAmendCreditLimitView model, List<QCAReqCreditOutStandingCrediLimit> datas)
		{
			try
			{
				#region Index 1 in spec
				model.QVariable_CrditLimitAmtStart1 = (datas.ElementAtOrDefault(0) != null) ? datas.ElementAt(0).QCAReqCreditOutStanding_ApprovedCreditLimit.DecimalToString() : string.Empty;
				model.QVariable_AROutstandingAmtStart = (datas.ElementAtOrDefault(0) != null) ? datas.ElementAt(0).QCAReqCreditOutStanding_ARBalance.DecimalToString() : string.Empty;
				model.QVariable_CreditLimitRemainingStart1 = (datas.ElementAtOrDefault(0) != null) ? datas.ElementAt(0).QCAReqCreditOutStanding_CreditLimitBalance.DecimalToString() : string.Empty;
				model.QVariable_ReserveOutstandingStart1 = (datas.ElementAtOrDefault(0) != null) ? datas.ElementAt(0).QCAReqCreditOutStanding_ReserveToBeRefund.DecimalToString() : string.Empty;
				model.QVariable_RetentionOutstandingStart1 = (datas.ElementAtOrDefault(0) != null) ? datas.ElementAt(0).QCAReqCreditOutStanding_AccumRetentionAmount.DecimalToString() : string.Empty;
				#endregion Index 1 in spec
				#region Index 2 in spec
				model.QVariable_CrditLimitAmtSecond1 = (datas.ElementAtOrDefault(1) != null) ? datas.ElementAt(1).QCAReqCreditOutStanding_ApprovedCreditLimit.DecimalToString() : string.Empty;
				model.QVariable_AROutstandingAmtSecond = (datas.ElementAtOrDefault(1) != null) ? datas.ElementAt(1).QCAReqCreditOutStanding_ARBalance.DecimalToString() : string.Empty;
				model.QVariable_CreditLimitRemainingSecond1 = (datas.ElementAtOrDefault(1) != null) ? datas.ElementAt(1).QCAReqCreditOutStanding_CreditLimitBalance.DecimalToString() : string.Empty;
				model.QVariable_ReserveOutstandingSecond1 = (datas.ElementAtOrDefault(1) != null) ? datas.ElementAt(1).QCAReqCreditOutStanding_ReserveToBeRefund.DecimalToString() : string.Empty;
				model.QVariable_RetentionOutstandingSecond1 = (datas.ElementAtOrDefault(1) != null) ? datas.ElementAt(1).QCAReqCreditOutStanding_AccumRetentionAmount.DecimalToString() : string.Empty;
				#endregion Index 2 in spec
				#region Index 3 in spec
				model.QVariable_CrditLimitAmtFirst1 = (datas.ElementAtOrDefault(2) != null) ? datas.ElementAt(2).QCAReqCreditOutStanding_ApprovedCreditLimit.DecimalToString() : string.Empty;
				model.QVariable_AROutstandingAmtFirst = (datas.ElementAtOrDefault(2) != null) ? datas.ElementAt(2).QCAReqCreditOutStanding_ARBalance.DecimalToString() : string.Empty;
				model.QVariable_CreditLimitRemainingFirst1 = (datas.ElementAtOrDefault(2) != null) ? datas.ElementAt(2).QCAReqCreditOutStanding_CreditLimitBalance.DecimalToString() : string.Empty;
				model.QVariable_ReserveOutstandingFirst1 = (datas.ElementAtOrDefault(2) != null) ? datas.ElementAt(2).QCAReqCreditOutStanding_ReserveToBeRefund.DecimalToString() : string.Empty;
				model.QVariable_RetentionOutstandingFirst1 = (datas.ElementAtOrDefault(2) != null) ? datas.ElementAt(2).QCAReqCreditOutStanding_AccumRetentionAmount.DecimalToString() : string.Empty;
				model.QVariable_RetentionOutstandingFirst1 = (datas.ElementAtOrDefault(2) != null) ? datas.ElementAt(2).QCAReqCreditOutStanding_AccumRetentionAmount.DecimalToString() : string.Empty;
				#endregion Index 3 in spec
				#region Index 4 in spec
				model.QVariable_CrditLimitAmtThird1 = (datas.ElementAtOrDefault(3) != null) ? datas.ElementAt(3).QCAReqCreditOutStanding_ApprovedCreditLimit.DecimalToString() : string.Empty;
				model.QVariable_AROutstandingAmtThird = (datas.ElementAtOrDefault(3) != null) ? datas.ElementAt(3).QCAReqCreditOutStanding_ARBalance.DecimalToString() : string.Empty;
				model.QVariable_CreditLimitRemainingThird1 = (datas.ElementAtOrDefault(3) != null) ? datas.ElementAt(3).QCAReqCreditOutStanding_CreditLimitBalance.DecimalToString() : string.Empty;
				model.QVariable_ReserveOutstandingThird1 = (datas.ElementAtOrDefault(3) != null) ? datas.ElementAt(3).QCAReqCreditOutStanding_ReserveToBeRefund.DecimalToString() : string.Empty;
				model.QVariable_RetentionOutstandingThird1 = (datas.ElementAtOrDefault(3) != null) ? datas.ElementAt(3).QCAReqCreditOutStanding_AccumRetentionAmount.DecimalToString() : string.Empty;
				#endregion Index 4 in spec
				#region Index 5 in spec
				model.QVariable_CrditLimitAmtFourth1 = (datas.ElementAtOrDefault(4) != null) ? datas.ElementAt(4).QCAReqCreditOutStanding_ApprovedCreditLimit.DecimalToString() : string.Empty;
				model.QVariable_AROutstandingAmtFourth = (datas.ElementAtOrDefault(4) != null) ? datas.ElementAt(4).QCAReqCreditOutStanding_ARBalance.DecimalToString() : string.Empty;
				model.QVariable_CreditLimitRemainingFourth1 = (datas.ElementAtOrDefault(4) != null) ? datas.ElementAt(4).QCAReqCreditOutStanding_CreditLimitBalance.DecimalToString() : string.Empty;
				model.QVariable_ReserveOutstandingFourth1 = (datas.ElementAtOrDefault(4) != null) ? datas.ElementAt(4).QCAReqCreditOutStanding_ReserveToBeRefund.DecimalToString() : string.Empty;
				model.QVariable_RetentionOutstandingFourth1 = (datas.ElementAtOrDefault(4) != null) ? datas.ElementAt(4).QCAReqCreditOutStanding_AccumRetentionAmount.DecimalToString() : string.Empty;
				#endregion Index 5 in spec
				#region Index 6 in spec
				model.QVariable_CrditLimitAmtFifth1 = (datas.ElementAtOrDefault(5) != null) ? datas.ElementAt(5).QCAReqCreditOutStanding_ApprovedCreditLimit.DecimalToString() : string.Empty;
				model.QVariable_AROutstandingAmtFifth = (datas.ElementAtOrDefault(5) != null) ? datas.ElementAt(5).QCAReqCreditOutStanding_ARBalance.DecimalToString() : string.Empty;
				model.QVariable_CreditLimitRemainingFifth1 = (datas.ElementAtOrDefault(5) != null) ? datas.ElementAt(5).QCAReqCreditOutStanding_CreditLimitBalance.DecimalToString() : string.Empty;
				model.QVariable_ReserveOutstandingFifth1 = (datas.ElementAtOrDefault(5) != null) ? datas.ElementAt(5).QCAReqCreditOutStanding_ReserveToBeRefund.DecimalToString() : string.Empty;
				model.QVariable_RetentionOutstandingFifth1 = (datas.ElementAtOrDefault(5) != null) ? datas.ElementAt(5).QCAReqCreditOutStanding_AccumRetentionAmount.DecimalToString() : string.Empty;
				#endregion Index 6 in spec
				#region Index 7 in spec
				model.QVariable_CrditLimitAmtSixth1 = (datas.ElementAtOrDefault(6) != null) ? datas.ElementAt(6).QCAReqCreditOutStanding_ApprovedCreditLimit.DecimalToString() : string.Empty;
				model.QVariable_AROutstandingAmtSixth = (datas.ElementAtOrDefault(6) != null) ? datas.ElementAt(6).QCAReqCreditOutStanding_ARBalance.DecimalToString() : string.Empty;
				model.QVariable_CreditLimitRemainingSixth1 = (datas.ElementAtOrDefault(6) != null) ? datas.ElementAt(6).QCAReqCreditOutStanding_CreditLimitBalance.DecimalToString() : string.Empty;
				model.QVariable_ReserveOutstandingSixth1 = (datas.ElementAtOrDefault(6) != null) ? datas.ElementAt(6).QCAReqCreditOutStanding_ReserveToBeRefund.DecimalToString() : string.Empty;
				model.QVariable_RetentionOutstandingSixth1 = (datas.ElementAtOrDefault(6) != null) ? datas.ElementAt(6).QCAReqCreditOutStanding_AccumRetentionAmount.DecimalToString() : string.Empty;
				#endregion Index 7 in spec
				#region Index 8 in spec
				model.QVariable_CrditLimitAmtSeventh1 = (datas.ElementAtOrDefault(7) != null) ? datas.ElementAt(7).QCAReqCreditOutStanding_ApprovedCreditLimit.DecimalToString() : string.Empty;
				model.QVariable_AROutstandingAmtSeventh = (datas.ElementAtOrDefault(7) != null) ? datas.ElementAt(7).QCAReqCreditOutStanding_ARBalance.DecimalToString() : string.Empty;
				model.QVariable_CreditLimitRemainingSeventh1 = (datas.ElementAtOrDefault(7) != null) ? datas.ElementAt(7).QCAReqCreditOutStanding_CreditLimitBalance.DecimalToString() : string.Empty;
				model.QVariable_ReserveOutstandingSeventh1 = (datas.ElementAtOrDefault(7) != null) ? datas.ElementAt(7).QCAReqCreditOutStanding_ReserveToBeRefund.DecimalToString() : string.Empty;
				model.QVariable_RetentionOutstandingSeventh1 = (datas.ElementAtOrDefault(7) != null) ? datas.ElementAt(7).QCAReqCreditOutStanding_AccumRetentionAmount.DecimalToString() : string.Empty;
				#endregion Index 8 in spec
				#region Index 9 in spec
				model.QVariable_CrditLimitAmtEighth1 = (datas.ElementAtOrDefault(8) != null) ? datas.ElementAt(8).QCAReqCreditOutStanding_ApprovedCreditLimit.DecimalToString() : string.Empty;
				model.QVariable_AROutstandingAmtEighth = (datas.ElementAtOrDefault(8) != null) ? datas.ElementAt(8).QCAReqCreditOutStanding_ARBalance.DecimalToString() : string.Empty;
				model.QVariable_CreditLimitRemainingEighth1 = (datas.ElementAtOrDefault(8) != null) ? datas.ElementAt(8).QCAReqCreditOutStanding_CreditLimitBalance.DecimalToString() : string.Empty;
				model.QVariable_ReserveOutstandingEighth1 = (datas.ElementAtOrDefault(8) != null) ? datas.ElementAt(8).QCAReqCreditOutStanding_ReserveToBeRefund.DecimalToString() : string.Empty;
				model.QVariable_RetentionOutstandingEighth1 = (datas.ElementAtOrDefault(8) != null) ? datas.ElementAt(8).QCAReqCreditOutStanding_AccumRetentionAmount.DecimalToString() : string.Empty;
				#endregion Index 9 in spec
				#region Index 10 in spec
				model.QVariable_CrditLimitAmtNineth1 = (datas.ElementAtOrDefault(9) != null) ? datas.ElementAt(9).QCAReqCreditOutStanding_ApprovedCreditLimit.DecimalToString() : string.Empty;
				model.QVariable_AROutstandingAmtNineth = (datas.ElementAtOrDefault(9) != null) ? datas.ElementAt(9).QCAReqCreditOutStanding_ARBalance.DecimalToString() : string.Empty;
				model.QVariable_CreditLimitRemainingNineth1 = (datas.ElementAtOrDefault(9) != null) ? datas.ElementAt(9).QCAReqCreditOutStanding_CreditLimitBalance.DecimalToString() : string.Empty;
				model.QVariable_ReserveOutstandingNineth1 = (datas.ElementAtOrDefault(9) != null) ? datas.ElementAt(9).QCAReqCreditOutStanding_ReserveToBeRefund.DecimalToString() : string.Empty;
				model.QVariable_RetentionOutstandingNineth1 = (datas.ElementAtOrDefault(9) != null) ? datas.ElementAt(9).QCAReqCreditOutStanding_AccumRetentionAmount.DecimalToString() : string.Empty;
				#endregion Index 10 in spec
				#region Index 11 in spec
				model.QVariable_CrditLimitAmtTenth1 = (datas.ElementAtOrDefault(10) != null) ? datas.ElementAt(10).QCAReqCreditOutStanding_ApprovedCreditLimit.DecimalToString() : string.Empty;
				model.QVariable_AROutstandingAmtTenth = (datas.ElementAtOrDefault(10) != null) ? datas.ElementAt(10).QCAReqCreditOutStanding_ARBalance.DecimalToString() : string.Empty;
				model.QVariable_CreditLimitRemainingTenth1 = (datas.ElementAtOrDefault(10) != null) ? datas.ElementAt(10).QCAReqCreditOutStanding_CreditLimitBalance.DecimalToString() : string.Empty;
				model.QVariable_ReserveOutstandingTenth1 = (datas.ElementAtOrDefault(10) != null) ? datas.ElementAt(10).QCAReqCreditOutStanding_ReserveToBeRefund.DecimalToString() : string.Empty;
				model.QVariable_RetentionOutstandingTenth1 = (datas.ElementAtOrDefault(10) != null) ? datas.ElementAt(10).QCAReqCreditOutStanding_AccumRetentionAmount.DecimalToString() : string.Empty;
				#endregion Index 11 in spec
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		#region SetModelByNCBTransSum
		private void SetModelByNCBTransSum(BookmarkDocumentCustomerAmendCreditLimitView model, QNCBTransSumCrediLimit data)
		{
			try
			{
				model.QNCBTransSum_LoanLimit = data.QNCBTransSum_LoanLimit.DecimalToString();
				model.QNCBTransSum_TotalLoanLimit = data.QNCBTransSum_TotalLoanLimit.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}


		}
		#endregion SetModelByNCBTransSum
		#region SetModelByNCBTrans
		private void SetModelByNCBTransRow1(BookmarkDocumentCustomerAmendCreditLimitView model, QNCBTransCrediLimit data)
		{
			try
			{
				model.QNCBTrans_Institution_1 = data.QNCBTrans_Institution;
				model.QNCBTrans_LoanType_1 = data.QNCBTrans_LoanType;
				model.QNCBTrans_LoanLimit_1 = data.QNCBTrans_LoanLimit.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByNCBTransRow2(BookmarkDocumentCustomerAmendCreditLimitView model, QNCBTransCrediLimit data)
		{
			try
			{
				model.QNCBTrans_Institution_2 = data.QNCBTrans_Institution;
				model.QNCBTrans_LoanType_2 = data.QNCBTrans_LoanType;
				model.QNCBTrans_LoanLimit_2 = data.QNCBTrans_LoanLimit.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByNCBTransRow3(BookmarkDocumentCustomerAmendCreditLimitView model, QNCBTransCrediLimit data)
		{
			try
			{
				model.QNCBTrans_Institution_3 = data.QNCBTrans_Institution;
				model.QNCBTrans_LoanType_3 = data.QNCBTrans_LoanType;
				model.QNCBTrans_LoanLimit_3 = data.QNCBTrans_LoanLimit.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByNCBTransRow4(BookmarkDocumentCustomerAmendCreditLimitView model, QNCBTransCrediLimit data)
		{
			try
			{
				model.QNCBTrans_Institution_4 = data.QNCBTrans_Institution;
				model.QNCBTrans_LoanType_4 = data.QNCBTrans_LoanType;
				model.QNCBTrans_LoanLimit_4 = data.QNCBTrans_LoanLimit.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByNCBTransRow5(BookmarkDocumentCustomerAmendCreditLimitView model, QNCBTransCrediLimit data)
		{
			try
			{
				model.QNCBTrans_Institution_5 = data.QNCBTrans_Institution;
				model.QNCBTrans_LoanType_5 = data.QNCBTrans_LoanType;
				model.QNCBTrans_LoanLimit_5 = data.QNCBTrans_LoanLimit.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByNCBTransRow6(BookmarkDocumentCustomerAmendCreditLimitView model, QNCBTransCrediLimit data)
		{
			try
			{
				model.QNCBTrans_Institution_6 = data.QNCBTrans_Institution;
				model.QNCBTrans_LoanType_6 = data.QNCBTrans_LoanType;
				model.QNCBTrans_LoanLimit_6 = data.QNCBTrans_LoanLimit.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByNCBTransRow7(BookmarkDocumentCustomerAmendCreditLimitView model, QNCBTransCrediLimit data)
		{
			try
			{
				model.QNCBTrans_Institution_7 = data.QNCBTrans_Institution;
				model.QNCBTrans_LoanType_7 = data.QNCBTrans_LoanType;
				model.QNCBTrans_LoanLimit_7 = data.QNCBTrans_LoanLimit.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByNCBTransRow8(BookmarkDocumentCustomerAmendCreditLimitView model, QNCBTransCrediLimit data)
		{
			try
			{
				model.QNCBTrans_Institution_8 = data.QNCBTrans_Institution;
				model.QNCBTrans_LoanType_8 = data.QNCBTrans_LoanType;
				model.QNCBTrans_LoanLimit_8 = data.QNCBTrans_LoanLimit.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		#endregion SetModelByNCBTrans
		private void SetModelByTaxReportTransSum(BookmarkDocumentCustomerAmendCreditLimitView model, QTaxReportTransSumCrediLimit data)
		{
			try
			{
				model.QTaxReportTransSum_SumTaxReport = data.QTaxReportTransSum_SumTaxReport.DecimalToString();
				model.QTaxReportTransSum_TaxReportCOUNT = data.QTaxReportTransSum_TaxReportCOUNT;
				if (data.QTaxReportTransSum_SumTaxReport != 0 && data.QTaxReportTransSum_TaxReportCOUNT != 0)
				{
					model.QVariable_AverageOutputVATPerMonth = (data.QTaxReportTransSum_SumTaxReport / data.QTaxReportTransSum_TaxReportCOUNT).DecimalToString();
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}

        private void SetModelByTaxReportEnd(BookmarkDocumentCustomerAmendCreditLimitView model, QTaxReportEndCrediLimit data)
        {
			try
			{
				model.QTaxReportEnd_TaxMonth = TranslateMonth(data.QTaxReportEnd_TaxMonth, " ");
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}

        private void SetModelByTaxReportStart(BookmarkDocumentCustomerAmendCreditLimitView model, QTaxReportStartCrediLimit data)
		{
			try
			{
				model.QTaxReportStart_TaxMonth = TranslateMonth(data.QTaxReportStart_TaxMonth, " ");
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private string TranslateMonth(string month, string separator)
		{
			try
			{
				if(string.IsNullOrWhiteSpace(month))
                {
					return string.Empty;
                }
				ISharedService sharedService = new SharedService(db);
				var sp = month.Split(separator);
				DateTime dateTime = new DateTime(DateTime.Now.Year, Convert.ToInt32(sp[0]), 1);
				var spWord = sharedService.DateToWord(dateTime, TextConstants.EN, TextConstants.DMY).Split(" ");
				return string.Concat(spWord[1], separator, sp[1]);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}


		}
		#region  SetModelByTaxReportTrans
		private void SetModelByTaxReportTransRow1(BookmarkDocumentCustomerAmendCreditLimitView model, QTaxReportTransCrediLimit data)
		{
			try
			{
				model.QTaxReportTrans_TaxMonth_1 = TranslateMonth(data.QTaxReportTrans_TaxMonth, "-");
				model.QTaxReportTrans_Amount_1 = data.QTaxReportTrans_Amount.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}

		private void SetModelByTaxReportTransRow2(BookmarkDocumentCustomerAmendCreditLimitView model, QTaxReportTransCrediLimit data)
		{
			try
			{
				model.QTaxReportTrans_TaxMonth_2 = TranslateMonth(data.QTaxReportTrans_TaxMonth, "-");
				model.QTaxReportTrans_Amount_2 = data.QTaxReportTrans_Amount.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByTaxReportTransRow3(BookmarkDocumentCustomerAmendCreditLimitView model, QTaxReportTransCrediLimit data)
		{
			try
			{
				model.QTaxReportTrans_TaxMonth_3 = TranslateMonth(data.QTaxReportTrans_TaxMonth, "-");
				model.QTaxReportTrans_Amount_3 = data.QTaxReportTrans_Amount.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByTaxReportTransRow4(BookmarkDocumentCustomerAmendCreditLimitView model, QTaxReportTransCrediLimit data)
		{
			try
			{
				model.QTaxReportTrans_TaxMonth_4 = TranslateMonth(data.QTaxReportTrans_TaxMonth, "-");
				model.QTaxReportTrans_Amount_4 = data.QTaxReportTrans_Amount.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByTaxReportTransRow5(BookmarkDocumentCustomerAmendCreditLimitView model, QTaxReportTransCrediLimit data)
		{
			try
			{
				model.QTaxReportTrans_TaxMonth_5 = TranslateMonth(data.QTaxReportTrans_TaxMonth, "-");
				model.QTaxReportTrans_Amount_5 = data.QTaxReportTrans_Amount.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByTaxReportTransRow6(BookmarkDocumentCustomerAmendCreditLimitView model, QTaxReportTransCrediLimit data)
		{
			try
			{
				model.QTaxReportTrans_TaxMonth_6 = TranslateMonth(data.QTaxReportTrans_TaxMonth, "-");
				model.QTaxReportTrans_Amount_6 = data.QTaxReportTrans_Amount.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByTaxReportTransRow7(BookmarkDocumentCustomerAmendCreditLimitView model, QTaxReportTransCrediLimit data)
		{
			try
			{
				model.QTaxReportTrans_TaxMonth_7 = TranslateMonth(data.QTaxReportTrans_TaxMonth, "-");
				model.QTaxReportTrans_Amount_7 = data.QTaxReportTrans_Amount.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByTaxReportTransRow8(BookmarkDocumentCustomerAmendCreditLimitView model, QTaxReportTransCrediLimit data)
		{
			try
			{
				model.QTaxReportTrans_TaxMonth_8 = TranslateMonth(data.QTaxReportTrans_TaxMonth, "-");
				model.QTaxReportTrans_Amount_8 = data.QTaxReportTrans_Amount.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByTaxReportTransRow9(BookmarkDocumentCustomerAmendCreditLimitView model, QTaxReportTransCrediLimit data)
		{
			try
			{
				model.QTaxReportTrans_TaxMonth_9 = TranslateMonth(data.QTaxReportTrans_TaxMonth, "-");
				model.QTaxReportTrans_Amount_9 = data.QTaxReportTrans_Amount.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByTaxReportTransRow10(BookmarkDocumentCustomerAmendCreditLimitView model, QTaxReportTransCrediLimit data)
		{
			try
			{
				model.QTaxReportTrans_TaxMonth_10 = TranslateMonth(data.QTaxReportTrans_TaxMonth, "-");
				model.QTaxReportTrans_Amount_10 = data.QTaxReportTrans_Amount.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByTaxReportTransRow11(BookmarkDocumentCustomerAmendCreditLimitView model, QTaxReportTransCrediLimit data)
		{
			try
			{
				model.QTaxReportTrans_TaxMonth_11 = TranslateMonth(data.QTaxReportTrans_TaxMonth, "-");
				model.QTaxReportTrans_Amount_11 = data.QTaxReportTrans_Amount.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByTaxReportTransRow12(BookmarkDocumentCustomerAmendCreditLimitView model, QTaxReportTransCrediLimit data)
		{
			try
			{
				model.QTaxReportTrans_TaxMonth_12 = TranslateMonth(data.QTaxReportTrans_TaxMonth, "-");
				model.QTaxReportTrans_Amount_12 = data.QTaxReportTrans_Amount.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		#endregion SetModelByTaxReportTrans
		#region SetModelByCreditAppReqLineFin
		private void SetModelByCreditAppReqLineFinRow1(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqLineFinCrediLimit data)
		{
			try
			{
				model.QCreditAppReqLineFin_Year_1 = data.QCreditAppReqLineFin_Year;
				model.QCreditAppReqLineFin_RegisteredCapital_1 = data.QCreditAppReqLineFin_RegisteredCapital.DecimalToString();
				model.QCreditAppReqLineFin_PaidCapital_1 = data.QCreditAppReqLineFin_PaidCapital.DecimalToString();
				model.QCreditAppReqLineFin_TotalAsset_1 = data.QCreditAppReqLineFin_TotalAsset.DecimalToString();
				model.QCreditAppReqLineFin_TotalLiability_1 = data.QCreditAppReqLineFin_TotalLiability.DecimalToString();
				model.QCreditAppReqLineFin_TotalEquity_1 = data.QCreditAppReqLineFin_TotalEquity.DecimalToString();
				model.QCreditAppReqLineFin_TotalRevenue_1 = data.QCreditAppReqLineFin_TotalRevenue.DecimalToString();
				model.QCreditAppReqLineFin_TotalCOGS_1 = data.QCreditAppReqLineFin_TotalCOGS.DecimalToString();
				model.QCreditAppReqLineFin_TotalGrossProfit_1 = data.QCreditAppReqLineFin_TotalGrossProfit.DecimalToString();
				model.QCreditAppReqLineFin_TotalOperExpFirst_1 = data.QCreditAppReqLineFin_TotalOperExpFirst.DecimalToString();
				model.QCreditAppReqLineFin_TotalNetProfitFirst_1 = data.QCreditAppReqLineFin_TotalNetProfitFirst.DecimalToString();
				model.QCreditAppReqLineFin_NetProfitPercent_1 = data.QCreditAppReqLineFin_NetProfitPercent.DecimalToString();
				model.QCreditAppReqLineFin_lDE_1 = data.QCreditAppReqLineFin_lDE.DecimalToString();
				model.QCreditAppReqLineFin_QuickRatio_1 = data.QCreditAppReqLineFin_QuickRatio.DecimalToString();
				model.QCreditAppReqLineFin_IntCoverageRatio_1 = data.QCreditAppReqLineFin_IntCoverageRatio.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}


		}
		private void SetModelByCreditAppReqLineFinRow2(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqLineFinCrediLimit data)
		{
			try
			{
				model.QCreditAppReqLineFin_Year_2 = data.QCreditAppReqLineFin_Year;
				model.QCreditAppReqLineFin_RegisteredCapital_2 = data.QCreditAppReqLineFin_RegisteredCapital.DecimalToString();
				model.QCreditAppReqLineFin_PaidCapital_2 = data.QCreditAppReqLineFin_PaidCapital.DecimalToString();
				model.QCreditAppReqLineFin_TotalAsset_2 = data.QCreditAppReqLineFin_TotalAsset.DecimalToString();
				model.QCreditAppReqLineFin_TotalLiability_2 = data.QCreditAppReqLineFin_TotalLiability.DecimalToString();
				model.QCreditAppReqLineFin_TotalEquity_2 = data.QCreditAppReqLineFin_TotalEquity.DecimalToString();
				model.QCreditAppReqLineFin_TotalRevenue_2 = data.QCreditAppReqLineFin_TotalRevenue.DecimalToString();
				model.QCreditAppReqLineFin_TotalCOGS_2 = data.QCreditAppReqLineFin_TotalCOGS.DecimalToString();
				model.QCreditAppReqLineFin_TotalGrossProfit_2 = data.QCreditAppReqLineFin_TotalGrossProfit.DecimalToString();
				model.QCreditAppReqLineFin_TotalOperExpFirst_2 = data.QCreditAppReqLineFin_TotalOperExpFirst.DecimalToString();
				model.QCreditAppReqLineFin_TotalNetProfitFirst_2 = data.QCreditAppReqLineFin_TotalNetProfitFirst.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByCreditAppReqLineFinRow3(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqLineFinCrediLimit data)
		{
			try
			{
				model.QCreditAppReqLineFin_Year_3 = data.QCreditAppReqLineFin_Year;
				model.QCreditAppReqLineFin_RegisteredCapital_3 = data.QCreditAppReqLineFin_RegisteredCapital.DecimalToString();
				model.QCreditAppReqLineFin_PaidCapital_3 = data.QCreditAppReqLineFin_PaidCapital.DecimalToString();
				model.QCreditAppReqLineFin_TotalAsset_3 = data.QCreditAppReqLineFin_TotalAsset.DecimalToString();
				model.QCreditAppReqLineFin_TotalLiability_3 = data.QCreditAppReqLineFin_TotalLiability.DecimalToString();
				model.QCreditAppReqLineFin_TotalEquity_3 = data.QCreditAppReqLineFin_TotalEquity.DecimalToString();
				model.QCreditAppReqLineFin_TotalRevenue_3 = data.QCreditAppReqLineFin_TotalRevenue.DecimalToString();
				model.QCreditAppReqLineFin_TotalCOGS_3 = data.QCreditAppReqLineFin_TotalCOGS.DecimalToString();
				model.QCreditAppReqLineFin_TotalGrossProfit_3 = data.QCreditAppReqLineFin_TotalGrossProfit.DecimalToString();
				model.QCreditAppReqLineFin_TotalOperExpFirst_3 = data.QCreditAppReqLineFin_TotalOperExpFirst.DecimalToString();
				model.QCreditAppReqLineFin_TotalNetProfitFirst_3 = data.QCreditAppReqLineFin_TotalNetProfitFirst.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByCreditAppReqLineFinRow4(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqLineFinCrediLimit data)
		{
			try
			{
				model.QCreditAppReqLineFin_Year_4 = data.QCreditAppReqLineFin_Year;
				model.QCreditAppReqLineFin_RegisteredCapital_4 = data.QCreditAppReqLineFin_RegisteredCapital.DecimalToString();
				model.QCreditAppReqLineFin_PaidCapital_4 = data.QCreditAppReqLineFin_PaidCapital.DecimalToString();
				model.QCreditAppReqLineFin_TotalAsset_4 = data.QCreditAppReqLineFin_TotalAsset.DecimalToString();
				model.QCreditAppReqLineFin_TotalLiability_4 = data.QCreditAppReqLineFin_TotalLiability.DecimalToString();
				model.QCreditAppReqLineFin_TotalEquity_4 = data.QCreditAppReqLineFin_TotalEquity.DecimalToString();
				model.QCreditAppReqLineFin_TotalRevenue_4 = data.QCreditAppReqLineFin_TotalRevenue.DecimalToString();
				model.QCreditAppReqLineFin_TotalCOGS_4 = data.QCreditAppReqLineFin_TotalCOGS.DecimalToString();
				model.QCreditAppReqLineFin_TotalGrossProfit_4 = data.QCreditAppReqLineFin_TotalGrossProfit.DecimalToString();
				model.QCreditAppReqLineFin_TotalOperExpFirst_4 = data.QCreditAppReqLineFin_TotalOperExpFirst.DecimalToString();
				model.QCreditAppReqLineFin_TotalNetProfitFirst_4 = data.QCreditAppReqLineFin_TotalNetProfitFirst.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByCreditAppReqLineFinRow5(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqLineFinCrediLimit data)
		{
			try
			{
				model.QCreditAppReqLineFin_Year_5 = data.QCreditAppReqLineFin_Year;
				model.QCreditAppReqLineFin_RegisteredCapital_5 = data.QCreditAppReqLineFin_RegisteredCapital.DecimalToString();
				model.QCreditAppReqLineFin_PaidCapital_5 = data.QCreditAppReqLineFin_PaidCapital.DecimalToString();
				model.QCreditAppReqLineFin_TotalAsset_5 = data.QCreditAppReqLineFin_TotalAsset.DecimalToString();
				model.QCreditAppReqLineFin_TotalLiability_5 = data.QCreditAppReqLineFin_TotalLiability.DecimalToString();
				model.QCreditAppReqLineFin_TotalEquity_5 = data.QCreditAppReqLineFin_TotalEquity.DecimalToString();
				model.QCreditAppReqLineFin_TotalRevenue_5 = data.QCreditAppReqLineFin_TotalRevenue.DecimalToString();
				model.QCreditAppReqLineFin_TotalCOGS_5 = data.QCreditAppReqLineFin_TotalCOGS.DecimalToString();
				model.QCreditAppReqLineFin_TotalGrossProfit_5 = data.QCreditAppReqLineFin_TotalGrossProfit.DecimalToString();
				model.QCreditAppReqLineFin_TotalOperExpFirst_5 = data.QCreditAppReqLineFin_TotalOperExpFirst.DecimalToString();
				model.QCreditAppReqLineFin_TotalNetProfitFirst_5 = data.QCreditAppReqLineFin_TotalNetProfitFirst.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		#endregion SetModelByCreditAppReqLineFin
		#region SetModelByOwnerTrans
		private void SetModelByOwnerTransRow1(BookmarkDocumentCustomerAmendCreditLimitView model, QOwnerTransCrediLimit data)
        {
			try
			{
				model.QOwnerTrans_ShareHolderPersonName_1 = data.QOwnerTrans_ShareHolderPersonName;
				model.QOwnerTrans_PropotionOfShareholderPct_1 = data.QOwnerTrans_PropotionOfShareholderPct.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByOwnerTransRow2(BookmarkDocumentCustomerAmendCreditLimitView model, QOwnerTransCrediLimit data)
		{
			try
			{
				model.QOwnerTrans_ShareHolderPersonName_2 = data.QOwnerTrans_ShareHolderPersonName;
				model.QOwnerTrans_PropotionOfShareholderPct_2 = data.QOwnerTrans_PropotionOfShareholderPct.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByOwnerTransRow3(BookmarkDocumentCustomerAmendCreditLimitView model, QOwnerTransCrediLimit data)
		{
			try
			{
				model.QOwnerTrans_ShareHolderPersonName_3 = data.QOwnerTrans_ShareHolderPersonName;
				model.QOwnerTrans_PropotionOfShareholderPct_3 = data.QOwnerTrans_PropotionOfShareholderPct.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByOwnerTransRow4(BookmarkDocumentCustomerAmendCreditLimitView model, QOwnerTransCrediLimit data)
		{
			try
			{
				model.QOwnerTrans_ShareHolderPersonName_4 = data.QOwnerTrans_ShareHolderPersonName;
				model.QOwnerTrans_PropotionOfShareholderPct_4 = data.QOwnerTrans_PropotionOfShareholderPct.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByOwnerTransRow5(BookmarkDocumentCustomerAmendCreditLimitView model, QOwnerTransCrediLimit data)
		{
			try
			{
				model.QOwnerTrans_ShareHolderPersonName_5 = data.QOwnerTrans_ShareHolderPersonName;
				model.QOwnerTrans_PropotionOfShareholderPct_5 = data.QOwnerTrans_PropotionOfShareholderPct.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByOwnerTransRow6(BookmarkDocumentCustomerAmendCreditLimitView model, QOwnerTransCrediLimit data)
		{
			try
			{
				model.QOwnerTrans_ShareHolderPersonName_6 = data.QOwnerTrans_ShareHolderPersonName;
				model.QOwnerTrans_PropotionOfShareholderPct_6 = data.QOwnerTrans_PropotionOfShareholderPct.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByOwnerTransRow7(BookmarkDocumentCustomerAmendCreditLimitView model, QOwnerTransCrediLimit data)
		{
			try
			{
				model.QOwnerTrans_ShareHolderPersonName_7 = data.QOwnerTrans_ShareHolderPersonName;
				model.QOwnerTrans_PropotionOfShareholderPct_7 = data.QOwnerTrans_PropotionOfShareholderPct.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByOwnerTransRow8(BookmarkDocumentCustomerAmendCreditLimitView model, QOwnerTransCrediLimit data)
		{
			try
			{
				model.QOwnerTrans_ShareHolderPersonName_8 = data.QOwnerTrans_ShareHolderPersonName;
				model.QOwnerTrans_PropotionOfShareholderPct_8 = data.QOwnerTrans_PropotionOfShareholderPct.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		#endregion SetModelByOwnerTrans
		private string GetVariableIsNew(bool qCreditAppReqBusinessCollateral_IsNew)
		{
			try
			{
				if (qCreditAppReqBusinessCollateral_IsNew)
				{
					return "Yes";
				}
				else
				{
					return "No";
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
	
		}
		#region  SetModelByAuthorizedPersonTrans
		private void SetModelByAuthorizedPersonTransRow1(BookmarkDocumentCustomerAmendCreditLimitView model, QAuthorizedPersonTransCrediLimit data)
		{
			try
			{
				IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService(db);
				model.QAuthorizedPersonTrans_AuthorizedPersonName_1 = data.QAuthorizedPersonTrans_AuthorizedPersonName;
				model.QVariable_AuthorizedPersonTrans1Age = relatedPersonTableService.CalcAge(data.QAuthorizedPersonTrans_DateOfBirth);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByAuthorizedPersonTransRow2(BookmarkDocumentCustomerAmendCreditLimitView model, QAuthorizedPersonTransCrediLimit data)
		{
			try
			{
				IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService(db);
				model.QAuthorizedPersonTrans_AuthorizedPersonName_2 = data.QAuthorizedPersonTrans_AuthorizedPersonName;
				model.QVariable_AuthorizedPersonTrans2Age = relatedPersonTableService.CalcAge(data.QAuthorizedPersonTrans_DateOfBirth);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByAuthorizedPersonTransRow3(BookmarkDocumentCustomerAmendCreditLimitView model, QAuthorizedPersonTransCrediLimit data)
		{
			try
			{
				IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService(db);
				model.QAuthorizedPersonTrans_AuthorizedPersonName_3 = data.QAuthorizedPersonTrans_AuthorizedPersonName;
				model.QVariable_AuthorizedPersonTrans3Age = relatedPersonTableService.CalcAge(data.QAuthorizedPersonTrans_DateOfBirth);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByAuthorizedPersonTransRow4(BookmarkDocumentCustomerAmendCreditLimitView model, QAuthorizedPersonTransCrediLimit data)
		{
			try
			{
				IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService(db);
				model.QAuthorizedPersonTrans_AuthorizedPersonName_4 = data.QAuthorizedPersonTrans_AuthorizedPersonName;
				model.QVariable_AuthorizedPersonTrans4Age = relatedPersonTableService.CalcAge(data.QAuthorizedPersonTrans_DateOfBirth);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByAuthorizedPersonTransRow5(BookmarkDocumentCustomerAmendCreditLimitView model, QAuthorizedPersonTransCrediLimit data)
		{
			try
			{
				IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService(db);
				model.QAuthorizedPersonTrans_AuthorizedPersonName_5 = data.QAuthorizedPersonTrans_AuthorizedPersonName;
				model.QVariable_AuthorizedPersonTrans5Age = relatedPersonTableService.CalcAge(data.QAuthorizedPersonTrans_DateOfBirth);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByAuthorizedPersonTransRow6(BookmarkDocumentCustomerAmendCreditLimitView model, QAuthorizedPersonTransCrediLimit data)
		{
			try
			{
				IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService(db);
				model.QAuthorizedPersonTrans_AuthorizedPersonName_6 = data.QAuthorizedPersonTrans_AuthorizedPersonName;
				model.QVariable_AuthorizedPersonTrans6Age = relatedPersonTableService.CalcAge(data.QAuthorizedPersonTrans_DateOfBirth);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByAuthorizedPersonTransRow7(BookmarkDocumentCustomerAmendCreditLimitView model, QAuthorizedPersonTransCrediLimit data)
		{
			try
			{
				IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService(db);
				model.QAuthorizedPersonTrans_AuthorizedPersonName_7 = data.QAuthorizedPersonTrans_AuthorizedPersonName;
				model.QVariable_AuthorizedPersonTrans7Age = relatedPersonTableService.CalcAge(data.QAuthorizedPersonTrans_DateOfBirth);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByAuthorizedPersonTransRow8(BookmarkDocumentCustomerAmendCreditLimitView model, QAuthorizedPersonTransCrediLimit data)
		{
			try
			{
				IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService(db);
				model.QAuthorizedPersonTrans_AuthorizedPersonName_8 = data.QAuthorizedPersonTrans_AuthorizedPersonName;
				model.QVariable_AuthorizedPersonTrans8Age = relatedPersonTableService.CalcAge(data.QAuthorizedPersonTrans_DateOfBirth);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		#endregion SetModelByAuthorizedPersonTrans
		#region  SetModelByCreditAppReqBusinessCollateral

		private void SetModelByCreditAppReqBusinessCollateralRow1(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqBusinessCollateralCrediLimit data)
		{
			try
			{
				model.QCreditAppReqBusinessCollateral_IsNew_1 = data.QCreditAppReqBusinessCollateral_IsNew;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralType_1 = data.QCreditAppReqBusinessCollateral_BusinessCollateralType;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralSubType_1 = data.QCreditAppReqBusinessCollateral_BusinessCollateralSubType;
				model.QCreditAppReqBusinessCollateral_Description_1 = data.QCreditAppReqBusinessCollateral_Description;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralValue_1 = data.QCreditAppReqBusinessCollateral_BusinessCollateralValue.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}


		}



        private void SetModelByCreditAppReqBusinessCollateralRow2(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqBusinessCollateralCrediLimit data)
		{
			try
			{
				model.QCreditAppReqBusinessCollateral_IsNew_2 = data.QCreditAppReqBusinessCollateral_IsNew;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralType_2 = data.QCreditAppReqBusinessCollateral_BusinessCollateralType;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralSubType_2 = data.QCreditAppReqBusinessCollateral_BusinessCollateralSubType;
				model.QCreditAppReqBusinessCollateral_Description_2 = data.QCreditAppReqBusinessCollateral_Description;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralValue_2 = data.QCreditAppReqBusinessCollateral_BusinessCollateralValue.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}


		}
		private void SetModelByCreditAppReqBusinessCollateralRow3(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqBusinessCollateralCrediLimit data)
		{
			try
			{
				model.QCreditAppReqBusinessCollateral_IsNew_3 = data.QCreditAppReqBusinessCollateral_IsNew;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralType_3 = data.QCreditAppReqBusinessCollateral_BusinessCollateralType;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralSubType_3 = data.QCreditAppReqBusinessCollateral_BusinessCollateralSubType;
				model.QCreditAppReqBusinessCollateral_Description_3 = data.QCreditAppReqBusinessCollateral_Description;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralValue_3 = data.QCreditAppReqBusinessCollateral_BusinessCollateralValue.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}


		}
		private void SetModelByCreditAppReqBusinessCollateralRow4(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqBusinessCollateralCrediLimit data)
		{
			try
			{
				model.QCreditAppReqBusinessCollateral_IsNew_4 = data.QCreditAppReqBusinessCollateral_IsNew;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralType_4 = data.QCreditAppReqBusinessCollateral_BusinessCollateralType;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralSubType_4 = data.QCreditAppReqBusinessCollateral_BusinessCollateralSubType;
				model.QCreditAppReqBusinessCollateral_Description_4 = data.QCreditAppReqBusinessCollateral_Description;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralValue_4 = data.QCreditAppReqBusinessCollateral_BusinessCollateralValue.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}


		}
		private void SetModelByCreditAppReqBusinessCollateralRow5(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqBusinessCollateralCrediLimit data)
		{
			try
			{
				model.QCreditAppReqBusinessCollateral_IsNew_5 = data.QCreditAppReqBusinessCollateral_IsNew;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralType_5 = data.QCreditAppReqBusinessCollateral_BusinessCollateralType;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralSubType_5 = data.QCreditAppReqBusinessCollateral_BusinessCollateralSubType;
				model.QCreditAppReqBusinessCollateral_Description_5 = data.QCreditAppReqBusinessCollateral_Description;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralValue_5 = data.QCreditAppReqBusinessCollateral_BusinessCollateralValue.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}


		}
		private void SetModelByCreditAppReqBusinessCollateralRow6(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqBusinessCollateralCrediLimit data)
		{
			try
			{
				model.QCreditAppReqBusinessCollateral_IsNew_6 = data.QCreditAppReqBusinessCollateral_IsNew;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralType_6 = data.QCreditAppReqBusinessCollateral_BusinessCollateralType;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralSubType_6 = data.QCreditAppReqBusinessCollateral_BusinessCollateralSubType;
				model.QCreditAppReqBusinessCollateral_Description_6 = data.QCreditAppReqBusinessCollateral_Description;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralValue_6 = data.QCreditAppReqBusinessCollateral_BusinessCollateralValue.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}


		}
		private void SetModelByCreditAppReqBusinessCollateralRow7(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqBusinessCollateralCrediLimit data)
		{
			try
			{
				model.QCreditAppReqBusinessCollateral_IsNew_7 = data.QCreditAppReqBusinessCollateral_IsNew;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralType_7 = data.QCreditAppReqBusinessCollateral_BusinessCollateralType;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralSubType_7 = data.QCreditAppReqBusinessCollateral_BusinessCollateralSubType;
				model.QCreditAppReqBusinessCollateral_Description_7 = data.QCreditAppReqBusinessCollateral_Description;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralValue_7 = data.QCreditAppReqBusinessCollateral_BusinessCollateralValue.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}


		}
		private void SetModelByCreditAppReqBusinessCollateralRow8(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqBusinessCollateralCrediLimit data)
		{
			try
			{
				model.QCreditAppReqBusinessCollateral_IsNew_8 = data.QCreditAppReqBusinessCollateral_IsNew;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralType_8 = data.QCreditAppReqBusinessCollateral_BusinessCollateralType;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralSubType_8 = data.QCreditAppReqBusinessCollateral_BusinessCollateralSubType;
				model.QCreditAppReqBusinessCollateral_Description_8 = data.QCreditAppReqBusinessCollateral_Description;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralValue_8 = data.QCreditAppReqBusinessCollateral_BusinessCollateralValue.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}


		}
		private void SetModelByCreditAppReqBusinessCollateralRow9(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqBusinessCollateralCrediLimit data)
		{
			try
			{
				model.QCreditAppReqBusinessCollateral_IsNew_9 = data.QCreditAppReqBusinessCollateral_IsNew;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralType_9 = data.QCreditAppReqBusinessCollateral_BusinessCollateralType;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralSubType_9 = data.QCreditAppReqBusinessCollateral_BusinessCollateralSubType;
				model.QCreditAppReqBusinessCollateral_Description_9 = data.QCreditAppReqBusinessCollateral_Description;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralValue_9 = data.QCreditAppReqBusinessCollateral_BusinessCollateralValue.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}


		}
		private void SetModelByCreditAppReqBusinessCollateralRow10(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqBusinessCollateralCrediLimit data)
		{
			try
			{
				model.QCreditAppReqBusinessCollateral_IsNew_10 = data.QCreditAppReqBusinessCollateral_IsNew;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralType_10 = data.QCreditAppReqBusinessCollateral_BusinessCollateralType;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralSubType_10 = data.QCreditAppReqBusinessCollateral_BusinessCollateralSubType;
				model.QCreditAppReqBusinessCollateral_Description_10 = data.QCreditAppReqBusinessCollateral_Description;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralValue_10 = data.QCreditAppReqBusinessCollateral_BusinessCollateralValue.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}


		}
		private void SetModelByCreditAppReqBusinessCollateralRow11(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqBusinessCollateralCrediLimit data)
		{
			try
			{
				model.QCreditAppReqBusinessCollateral_IsNew_11 = data.QCreditAppReqBusinessCollateral_IsNew;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralType_11 = data.QCreditAppReqBusinessCollateral_BusinessCollateralType;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralSubType_11 = data.QCreditAppReqBusinessCollateral_BusinessCollateralSubType;
				model.QCreditAppReqBusinessCollateral_Description_11 = data.QCreditAppReqBusinessCollateral_Description;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralValue_11 = data.QCreditAppReqBusinessCollateral_BusinessCollateralValue.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}


		}
		private void SetModelByCreditAppReqBusinessCollateralRow12(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqBusinessCollateralCrediLimit data)
		{
			try
			{
				model.QCreditAppReqBusinessCollateral_IsNew_12 = data.QCreditAppReqBusinessCollateral_IsNew;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralType_12 = data.QCreditAppReqBusinessCollateral_BusinessCollateralType;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralSubType_12 = data.QCreditAppReqBusinessCollateral_BusinessCollateralSubType;
				model.QCreditAppReqBusinessCollateral_Description_12 = data.QCreditAppReqBusinessCollateral_Description;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralValue_12 = data.QCreditAppReqBusinessCollateral_BusinessCollateralValue.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}


		}
		private void SetModelByCreditAppReqBusinessCollateralRow13(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqBusinessCollateralCrediLimit data)
		{
			try
			{
				model.QCreditAppReqBusinessCollateral_IsNew_13 = data.QCreditAppReqBusinessCollateral_IsNew;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralType_13 = data.QCreditAppReqBusinessCollateral_BusinessCollateralType;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralSubType_13 = data.QCreditAppReqBusinessCollateral_BusinessCollateralSubType;
				model.QCreditAppReqBusinessCollateral_Description_13 = data.QCreditAppReqBusinessCollateral_Description;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralValue_13 = data.QCreditAppReqBusinessCollateral_BusinessCollateralValue.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}


		}
		private void SetModelByCreditAppReqBusinessCollateralRow14(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqBusinessCollateralCrediLimit data)
		{
			try
			{
				model.QCreditAppReqBusinessCollateral_IsNew_14 = data.QCreditAppReqBusinessCollateral_IsNew;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralType_14 = data.QCreditAppReqBusinessCollateral_BusinessCollateralType;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralSubType_14 = data.QCreditAppReqBusinessCollateral_BusinessCollateralSubType;
				model.QCreditAppReqBusinessCollateral_Description_14 = data.QCreditAppReqBusinessCollateral_Description;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralValue_14 = data.QCreditAppReqBusinessCollateral_BusinessCollateralValue.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}


		}
		private void SetModelByCreditAppReqBusinessCollateralRow15(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppReqBusinessCollateralCrediLimit data)
		{
			try
			{
				model.QCreditAppReqBusinessCollateral_IsNew_15 = data.QCreditAppReqBusinessCollateral_IsNew;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralType_15 = data.QCreditAppReqBusinessCollateral_BusinessCollateralType;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralSubType_15 = data.QCreditAppReqBusinessCollateral_BusinessCollateralSubType;
				model.QCreditAppReqBusinessCollateral_Description_15 = data.QCreditAppReqBusinessCollateral_Description;
				model.QCreditAppReqBusinessCollateral_BusinessCollateralValue_15 = data.QCreditAppReqBusinessCollateral_BusinessCollateralValue.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}


		}
		#endregion SetModelByCreditAppReqBusinessCollateral
		#region SetModelByGuarantorTrans
		private void SetModelByGuarantorTransRow1(BookmarkDocumentCustomerAmendCreditLimitView model, QGuarantorTransCrediLimit data)
		{
			try
			{
				model.QGuarantorTrans_RelatedPersonName_1 = data.QGuarantorTrans_RelatedPersonName;
				model.QGuarantorTrans_GuarantorType_1 = data.QGuarantorTrans_GuarantorType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByGuarantorTransRow2(BookmarkDocumentCustomerAmendCreditLimitView model, QGuarantorTransCrediLimit data)
		{
			try
			{
				model.QGuarantorTrans_RelatedPersonName_2 = data.QGuarantorTrans_RelatedPersonName;
				model.QGuarantorTrans_GuarantorType_2 = data.QGuarantorTrans_GuarantorType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByGuarantorTransRow3(BookmarkDocumentCustomerAmendCreditLimitView model, QGuarantorTransCrediLimit data)
		{
			try
			{
				model.QGuarantorTrans_RelatedPersonName_3 = data.QGuarantorTrans_RelatedPersonName;
				model.QGuarantorTrans_GuarantorType_3 = data.QGuarantorTrans_GuarantorType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByGuarantorTransRow4(BookmarkDocumentCustomerAmendCreditLimitView model, QGuarantorTransCrediLimit data)
		{
			try
			{
				model.QGuarantorTrans_RelatedPersonName_4 = data.QGuarantorTrans_RelatedPersonName;
				model.QGuarantorTrans_GuarantorType_4 = data.QGuarantorTrans_GuarantorType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByGuarantorTransRow5(BookmarkDocumentCustomerAmendCreditLimitView model, QGuarantorTransCrediLimit data)
		{
			try
			{
				model.QGuarantorTrans_RelatedPersonName_5 = data.QGuarantorTrans_RelatedPersonName;
				model.QGuarantorTrans_GuarantorType_5 = data.QGuarantorTrans_GuarantorType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByGuarantorTransRow6(BookmarkDocumentCustomerAmendCreditLimitView model, QGuarantorTransCrediLimit data)
		{
			try
			{
				model.QGuarantorTrans_RelatedPersonName_6 = data.QGuarantorTrans_RelatedPersonName;
				model.QGuarantorTrans_GuarantorType_6 = data.QGuarantorTrans_GuarantorType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByGuarantorTransRow7(BookmarkDocumentCustomerAmendCreditLimitView model, QGuarantorTransCrediLimit data)
		{
			try
			{
				model.QGuarantorTrans_RelatedPersonName_7 = data.QGuarantorTrans_RelatedPersonName;
				model.QGuarantorTrans_GuarantorType_7 = data.QGuarantorTrans_GuarantorType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByGuarantorTransRow8(BookmarkDocumentCustomerAmendCreditLimitView model, QGuarantorTransCrediLimit data)
		{
			try
			{
				model.QGuarantorTrans_RelatedPersonName_8 = data.QGuarantorTrans_RelatedPersonName;
				model.QGuarantorTrans_GuarantorType_8 = data.QGuarantorTrans_GuarantorType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		#endregion SetModelByGuarantorTrans
		#region SetModelBySumServiceFeeConditionTrans
		private void SetModelBySumServiceFeeConditionTrans(BookmarkDocumentCustomerAmendCreditLimitView model, decimal qSumServiceFeeConditionTrans)
		{
			try
			{
				model.QSumServiceFeeConditionTrans_ServiceFeeAmt = qSumServiceFeeConditionTrans.DecimalToString();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
        #endregion SetModelBySumServiceFeeConditionTrans
        #region SetModelByServiceFeeConditionTrans
        private void SetModelByServiceFeeConditionTransRow1(BookmarkDocumentCustomerAmendCreditLimitView model, QServiceFeeConditionTransCrediLimit data)
        {
			try
			{
				model.QServiceFeeConditionTrans_Description_1 = data.QServiceFeeConditionTrans_Description;
				model.QServiceFeeConditionTrans_AmountBeforeTax_1 = data.QServiceFeeConditionTrans_AmountBeforeTax.DecimalToString();
				model.QServiceFeeConditionTrans_ServicefeeTypeId_1 = data.QServiceFeeConditionTrans_ServiceFeeDesc;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByServiceFeeConditionTransRow2(BookmarkDocumentCustomerAmendCreditLimitView model, QServiceFeeConditionTransCrediLimit data)
		{
			try
			{
				model.QServiceFeeConditionTrans_Description_2 = data.QServiceFeeConditionTrans_Description;
				model.QServiceFeeConditionTrans_AmountBeforeTax_2 = data.QServiceFeeConditionTrans_AmountBeforeTax.DecimalToString();
				model.QServiceFeeConditionTrans_ServicefeeTypeId_2 = data.QServiceFeeConditionTrans_ServiceFeeDesc;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByServiceFeeConditionTransRow3(BookmarkDocumentCustomerAmendCreditLimitView model, QServiceFeeConditionTransCrediLimit data)
		{
			try
			{
				model.QServiceFeeConditionTrans_Description_3 = data.QServiceFeeConditionTrans_Description;
				model.QServiceFeeConditionTrans_AmountBeforeTax_3 = data.QServiceFeeConditionTrans_AmountBeforeTax.DecimalToString();
				model.QServiceFeeConditionTrans_ServicefeeTypeId_3 = data.QServiceFeeConditionTrans_ServiceFeeDesc;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByServiceFeeConditionTransRow4(BookmarkDocumentCustomerAmendCreditLimitView model, QServiceFeeConditionTransCrediLimit data)
		{
			try
			{
				model.QServiceFeeConditionTrans_Description_4 = data.QServiceFeeConditionTrans_Description;
				model.QServiceFeeConditionTrans_AmountBeforeTax_4 = data.QServiceFeeConditionTrans_AmountBeforeTax.DecimalToString();
				model.QServiceFeeConditionTrans_ServicefeeTypeId_4 = data.QServiceFeeConditionTrans_ServiceFeeDesc;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByServiceFeeConditionTransRow5(BookmarkDocumentCustomerAmendCreditLimitView model, QServiceFeeConditionTransCrediLimit data)
		{
			try
			{
				model.QServiceFeeConditionTrans_Description_5 = data.QServiceFeeConditionTrans_Description;
				model.QServiceFeeConditionTrans_AmountBeforeTax_5 = data.QServiceFeeConditionTrans_AmountBeforeTax.DecimalToString();
				model.QServiceFeeConditionTrans_ServicefeeTypeId_5 = data.QServiceFeeConditionTrans_ServiceFeeDesc;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByServiceFeeConditionTransRow6(BookmarkDocumentCustomerAmendCreditLimitView model, QServiceFeeConditionTransCrediLimit data)
		{
			try
			{
				model.QServiceFeeConditionTrans_Description_6 = data.QServiceFeeConditionTrans_Description;
				model.QServiceFeeConditionTrans_AmountBeforeTax_6 = data.QServiceFeeConditionTrans_AmountBeforeTax.DecimalToString();
				model.QServiceFeeConditionTrans_ServicefeeTypeId_6 = data.QServiceFeeConditionTrans_ServiceFeeDesc;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByServiceFeeConditionTransRow7(BookmarkDocumentCustomerAmendCreditLimitView model, QServiceFeeConditionTransCrediLimit data)
		{
			try
			{
				model.QServiceFeeConditionTrans_Description_7 = data.QServiceFeeConditionTrans_Description;
				model.QServiceFeeConditionTrans_AmountBeforeTax_7 = data.QServiceFeeConditionTrans_AmountBeforeTax.DecimalToString();
				model.QServiceFeeConditionTrans_ServicefeeTypeId_7 = data.QServiceFeeConditionTrans_ServiceFeeDesc;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByServiceFeeConditionTransRow8(BookmarkDocumentCustomerAmendCreditLimitView model, QServiceFeeConditionTransCrediLimit data)
		{
			try
			{
				model.QServiceFeeConditionTrans_Description_8 = data.QServiceFeeConditionTrans_Description;
				model.QServiceFeeConditionTrans_AmountBeforeTax_8 = data.QServiceFeeConditionTrans_AmountBeforeTax.DecimalToString();
				model.QServiceFeeConditionTrans_ServicefeeTypeId_8 = data.QServiceFeeConditionTrans_ServiceFeeDesc;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByServiceFeeConditionTransRow9(BookmarkDocumentCustomerAmendCreditLimitView model, QServiceFeeConditionTransCrediLimit data)
		{
			try
			{
				model.QServiceFeeConditionTrans_Description_9 = data.QServiceFeeConditionTrans_Description;
				model.QServiceFeeConditionTrans_AmountBeforeTax_9 = data.QServiceFeeConditionTrans_AmountBeforeTax.DecimalToString();
				model.QServiceFeeConditionTrans_ServicefeeTypeId_9 = data.QServiceFeeConditionTrans_ServiceFeeDesc;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		private void SetModelByServiceFeeConditionTransRow10(BookmarkDocumentCustomerAmendCreditLimitView model, QServiceFeeConditionTransCrediLimit data)
		{
			try
			{
				model.QServiceFeeConditionTrans_Description_10 = data.QServiceFeeConditionTrans_Description;
				model.QServiceFeeConditionTrans_AmountBeforeTax_10 = data.QServiceFeeConditionTrans_AmountBeforeTax.DecimalToString();
				model.QServiceFeeConditionTrans_ServicefeeTypeId_10 = data.QServiceFeeConditionTrans_ServiceFeeDesc;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		#endregion SetModelByServiceFeeConditionTrans
		#region SetModelByCreditAppRequestTable
		private void SetModelByCreditAppRequestTable(BookmarkDocumentCustomerAmendCreditLimitView model, QCreditAppRequestTableCrediLimit qCreditAppRequestTable)
        {
			try
			{
				model.QCreditAppRequestTable_CreditAppRequestId = qCreditAppRequestTable.QCreditAppRequestTable_CreditAppRequestId;
				model.QCreditAppRequestTable_RefCreditAppTableGUID = qCreditAppRequestTable.QCreditAppRequestTable_RefCreditAppTableGUID.GuidNullToString();
				model.QCreditAppRequestTable_DocumentStatusGUID = qCreditAppRequestTable.QCreditAppRequestTable_DocumentStatusGUID.GuidNullToString();
				model.QCreditAppRequestTable_RequestDate = qCreditAppRequestTable.QCreditAppRequestTable_RequestDate.DateNullToString();
				model.QCreditAppRequestTable_CustomerTableGUID = qCreditAppRequestTable.QCreditAppRequestTable_CustomerTableGUID.GuidNullToString();
				model.QCreditAppRequestTable_Remark = qCreditAppRequestTable.QCreditAppRequestTable_Remark;
				model.QCreditAppRequestTable_ProductType = SystemStaticData.GetTranslatedMessage(((ProductType)qCreditAppRequestTable.QCreditAppRequestTable_ProductType).GetAttrCode());
				model.QCreditAppRequestTable_ProductSubTypeGUID = qCreditAppRequestTable.QCreditAppRequestTable_ProductSubTypeGUID.GuidNullToString();
				model.QCreditAppRequestTable_CreditLimitTypeGUID = qCreditAppRequestTable.QCreditAppRequestTable_CreditLimitTypeGUID.GuidNullToString();
				model.QCreditAppRequestTable_CreditLimitExpiration = SystemStaticData.GetTranslatedMessage(((CreditLimitExpiration)qCreditAppRequestTable.QCreditAppRequestTable_CreditLimitExpiration).GetAttrCode());
				model.QCreditAppRequestTable_StartDate = qCreditAppRequestTable.QCreditAppRequestTable_StartDate.DateNullToString();
				model.QCreditAppRequestTable_ExpiryDate = qCreditAppRequestTable.QCreditAppRequestTable_ExpiryDate.DateNullToString();
                model.QCreditAppRequestTable_ApprovedCreditLimitRequest = qCreditAppRequestTable.QOriginalCreditAppRequest_CreditLimitRequest.DecimalToString();
                model.QVariable_NewCreditLimitAmt = qCreditAppRequestTable.QCreditAppRequestTable_CreditLimitRequest.DecimalToString();
                model.QCreditAppRequestTable_InterestTypeGUID = qCreditAppRequestTable.QCreditAppRequestTable_InterestTypeGUID.GuidNullToString();
				model.QCreditAppRequestTable_InterestAdjustment = qCreditAppRequestTable.QCreditAppRequestTable_InterestAdjustment.DecimalToString();
				model.QCreditAppRequestTable_MaxPurchasePct = qCreditAppRequestTable.QCreditAppRequestTable_MaxPurchasePct.DecimalToString();
				model.QCreditAppRequestTable_MaxRetentionPct = qCreditAppRequestTable.QCreditAppRequestTable_MaxRetentionPct.DecimalToString();
				model.QCreditAppRequestTable_MaxRetentionAmount = qCreditAppRequestTable.QCreditAppRequestTable_MaxRetentionAmount.DecimalToString();
				model.QCreditAppRequestTable_CACondition = qCreditAppRequestTable.QCreditAppRequestTable_CACondition;
				model.QCreditAppRequestTable_PDCBankGUID = qCreditAppRequestTable.QCreditAppRequestTable_PDCBankGUID.GuidNullToString();
				model.QCreditAppRequestTable_CreditRequestFeePct = qCreditAppRequestTable.QCreditAppRequestTable_CreditRequestFeePct.DecimalToString();
				model.QCreditAppRequestTable_CreditRequestFeeAmount = qCreditAppRequestTable.QCreditAppRequestTable_CreditRequestFeeAmount.DecimalToString();
				model.QCreditAppRequestTable_PurchaseFeePct = qCreditAppRequestTable.QCreditAppRequestTable_PurchaseFeePct.DecimalToString();
				model.QCreditAppRequestTable_PurchaseFeeCalculateBase = SystemStaticData.GetTranslatedMessage(((PurchaseFeeCalculateBase)qCreditAppRequestTable.QCreditAppRequestTable_PurchaseFeeCalculateBase).GetAttrCode());
				model.QCreditAppRequestTable_RegisteredAddressGUID = qCreditAppRequestTable.QCreditAppRequestTable_RegisteredAddressGUID.ToString();
				model.QCreditAppRequestTable_IntroducedByGUID = qCreditAppRequestTable.QCreditAppRequestTable_IntroducedByGUID.GuidNullToString();
				model.QCreditAppRequestTable_SalesAvgPerMonth = qCreditAppRequestTable.QCreditAppRequestTable_SalesAvgPerMonth.DecimalToString();
				model.QCreditAppRequestTable_MarketingComment = qCreditAppRequestTable.QCreditAppRequestTable_MarketingComment;
				model.QCreditAppRequestTable_CreditComment = qCreditAppRequestTable.QCreditAppRequestTable_CreditComment;
				model.QCreditAppRequestTable_ApproverComment = qCreditAppRequestTable.QCreditAppRequestTable_ApproverComment;
				model.QCreditAppRequestTable_CustomerCreditLimit = qCreditAppRequestTable.QCreditAppRequestTable_CustomerCreditLimit.DecimalToString();
				model.QCreditAppRequestTable_NCBCheckedDate = qCreditAppRequestTable.QCreditAppRequestTable_NCBCheckedDate.DateNullToString();
				model.QCreditAppRequestTable_Description = qCreditAppRequestTable.QCreditAppRequestTable_Description;


				model.DocumentStatus_Status = qCreditAppRequestTable.DocumentStatus_Status;
				model.KYCSetup_KYC = qCreditAppRequestTable.KYCSetup_KYC;
				model.CreditScoring_CreditScoring = qCreditAppRequestTable.CreditScoring_CreditScoring;
				model.ProductSubType_ProductSubType = qCreditAppRequestTable.ProductSubType_ProductSubType;
				model.CreditLimitType_CreditLimitType = qCreditAppRequestTable.CreditLimitType_CreditLimitType;
				model.InterestType_InterestType = qCreditAppRequestTable.InterestType_InterestType;
				model.AddressTrans_RegisteredAddress = qCreditAppRequestTable.AddressTrans_RegisteredAddress;
				model.BusinessType_BusinessType = qCreditAppRequestTable.BusinessType_BusinessType;
				model.IntroducedBy_IntroducedBy = qCreditAppRequestTable.IntroducedBy_IntroducedBy;

				model.QCustomer_CustomerId = qCreditAppRequestTable.QCustomer_CustomerId;
				model.QCustomer_CustomerName = qCreditAppRequestTable.QCustomer_CustomerName;
				model.QCustomer_ResponsibleByGUID = qCreditAppRequestTable.QCustomer_ResponsibleByGUID.GuidNullToString();
				model.QCustomer_IdentificationType = qCreditAppRequestTable.QCustomer_IdentificationType.ToString();
				model.QCustomer_TaxID = qCreditAppRequestTable.QCustomer_TaxID;
				model.QCustomer_PassportID = qCreditAppRequestTable.QCustomer_PassportID;
				model.QCustomer_RecordType = qCreditAppRequestTable.QCustomer_RecordType.ToString();
				model.QCustomer_DateOfEstablish = qCreditAppRequestTable.QCustomer_DateOfEstablish.DateNullToString();
				model.QCustomer_DateOfBirth = qCreditAppRequestTable.QCustomer_DateOfBirth.DateNullToString();
				model.QCustomer_BusinessTypeGUID = qCreditAppRequestTable.QCustomer_BusinessTypeGUID.GuidNullToString();
				model.QCustomer_IntroducedByGUID = qCreditAppRequestTable.QCustomer_IntroducedByGUID.GuidNullToString();
				model.QCustomer_ResponsibleBy = qCreditAppRequestTable.QCustomer_ResponsibleBy;

				model.QOriginalCreditAppRequest_OriginalCreditAppRequestId = qCreditAppRequestTable.QOriginalCreditAppRequest_OriginalCreditAppRequestId;
				model.QOriginalCreditAppRequest_ApprovedCreditLimitRequest = qCreditAppRequestTable.QOriginalCreditAppRequest_ApprovedCreditLimitRequest.DecimalToString();
				model.QOriginalCreditAppRequest_CreditLimitRequest = qCreditAppRequestTable.QOriginalCreditAppRequest_CreditLimitRequest.DecimalToString();

				model.QBankAccountControl_Instituetion = qCreditAppRequestTable.QBankAccountControl_Instituetion;
				model.QBankAccountControl_BankBranch = qCreditAppRequestTable.QBankAccountControl_BankBranch;
				model.QBankAccountControl_AccountType = qCreditAppRequestTable.QBankAccountControl_AccountType;
				model.QBankAccountControl_BankAccountName = qCreditAppRequestTable.QBankAccountControl_BankAccountName;

				model.QPDCBank_CustPDCBankName = qCreditAppRequestTable.QPDCBank_CustPDCBankName;
				model.QPDCBank_CustPDCBankBranch = qCreditAppRequestTable.QPDCBank_CustPDCBankBranch;
				model.QPDCBank_CustPDCBankType = qCreditAppRequestTable.QPDCBank_CustPDCBankType;
				model.QPDCBank_CustPDCBankAccountName = qCreditAppRequestTable.QPDCBank_CustPDCBankAccountName;

				IInterestTypeService interestTypeService = new InterestTypeService(db);
				model.QCreditAppRequestTable_TotalInterestPct = qCreditAppRequestTable.QCreditAppRequestTable_TotalInterestCA;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion SetModelByCreditAppRequestTable
		private QCreditAppReqAssignmentCrediLimit GetqCreditAppReqAssignmentEachRow(int row, List<QCreditAppReqAssignmentCrediLimit> modelList)
        {
			try
			{
				var result = modelList.Where(w => w.QCreditAppReqAssignment_Row == row).FirstOrDefault();
                if (result != null)
                {
					return result;
                }
                else
                {
					return null;
                }
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private QActionHistoryCrediLimit GetActionHistoryEachRow(int row, List<QActionHistoryCrediLimit> modelList)
        {
			try
			{
				var result = modelList.Where(w => w.QActionHistory_Row == row).FirstOrDefault();
				if (result != null)
				{
					return result;
				}
				else
				{
					return new QActionHistoryCrediLimit();
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private QNCBTransCrediLimit GetNCBTransEachRow(int row, List<QNCBTransCrediLimit> modelList)
        {
			try
			{
				var result = modelList.Where(w => w.QNCBTrans_Row == row).FirstOrDefault();
				if (result != null)
				{
					return result;
				}
				else
				{
					return new QNCBTransCrediLimit();
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private QTaxReportTransCrediLimit GetTaxReportTransEachRow(int row, List<QTaxReportTransCrediLimit> modelList)
        {
			try
			{
				var result = modelList.Where(w => w.QTaxReportTrans_Row == row).FirstOrDefault();
				if (result != null)
				{
					return result;
				}
				else
				{
					return new QTaxReportTransCrediLimit();
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private QOwnerTransCrediLimit GetOwnerTransEachRow(int row, List<QOwnerTransCrediLimit> modelList)
        {
			try
			{
				var result = modelList.Where(w => w.QOwnerTrans_Row == row).FirstOrDefault();
				if (result != null)
				{
					return result;
				}
				else
				{
					return new QOwnerTransCrediLimit();
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private QAuthorizedPersonTransCrediLimit GetAuthorizedPersonTransEachRow(int row, List<QAuthorizedPersonTransCrediLimit> modelList)
        {
			try
			{
				var result = modelList.Where(w => w.QAuthorizedPersonTrans_Row == row).FirstOrDefault();
				if (result != null)
				{
					return result;
				}
				else
				{
					return new QAuthorizedPersonTransCrediLimit();
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private QCreditAppReqBusinessCollateralCrediLimit GetCreditAppReqBusinessCollateralEachRow(int row, List<QCreditAppReqBusinessCollateralCrediLimit> modelList)
        {
			try
			{
				var result = modelList.Where(w => w.QCreditAppReqBusinessCollateral_Row == row).FirstOrDefault();
				if (result != null)
				{
					return result;
				}
				else
				{
					return new QCreditAppReqBusinessCollateralCrediLimit();
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private QGuarantorTransCrediLimit GetGuarantorTransEachRow(int row, List<QGuarantorTransCrediLimit> modelList)
        {
			try
			{
				var result = modelList.Where(w => w.QGuarantorTrans_Row == row).FirstOrDefault();
				if (result != null)
				{
					return result;
				}
				else
				{
					return new QGuarantorTransCrediLimit();
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private QServiceFeeConditionTransCrediLimit GetServiceFeeConditionTransEachRow(int row, List<QServiceFeeConditionTransCrediLimit> modelList)
        {
            try
            {
				var result = modelList.Where(w => w.QServiceFeeConditionTrans_Row == row).FirstOrDefault();
				if (result != null)
				{
					return result;
				}
				else
				{
					return new QServiceFeeConditionTransCrediLimit();
				}
			} catch(Exception e)
            {
				throw SmartAppUtil.AddStackTrace(e);
			}
        }

        private QSumCreditAppReqAssignmentCrediLimit GetSumCreditAppReqAssignment(Guid refGUID)
        {
			try
			{
				var result = (from creditAppReqAssignment in db.Set<CreditAppReqAssignment>()
							  where creditAppReqAssignment.CreditAppRequestTableGUID == refGUID
							  select new QSumCreditAppReqAssignmentCrediLimit
							  {
								  QSumCreditAppReqAssignment_SumBuyerAgreementAmount = creditAppReqAssignment.BuyerAgreementAmount,
								  QSumCreditAppReqAssignment_SumAssignmentAgreementAmount = creditAppReqAssignment.AssignmentAgreementAmount,
								  QSumCreditAppReqAssignment_SumSumRemainingAmount = creditAppReqAssignment.RemainingAmount
							  }
						  ).ToList();
				if (result != null)
				{
					return new QSumCreditAppReqAssignmentCrediLimit
					{
						QSumCreditAppReqAssignment_SumBuyerAgreementAmount = result.Sum(su => su.QSumCreditAppReqAssignment_SumBuyerAgreementAmount),
						QSumCreditAppReqAssignment_SumAssignmentAgreementAmount = result.Sum(su => su.QSumCreditAppReqAssignment_SumAssignmentAgreementAmount),
						QSumCreditAppReqAssignment_SumSumRemainingAmount = result.Sum(su => su.QSumCreditAppReqAssignment_SumSumRemainingAmount),
					};
				}
				else
				{
					return new QSumCreditAppReqAssignmentCrediLimit();

				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
			
			

        }

        private List<QCreditAppReqAssignmentCrediLimit> GetqCreditAppReqAssignment(Guid refGUID)
        {
			try
			{
				var result = (from creditAppReqAssignment in db.Set<CreditAppReqAssignment>()
							  join buyerTable in db.Set<BuyerTable>()
							  on creditAppReqAssignment.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
							  from buyerTable in ljbuyerTable.DefaultIfEmpty()

							  join assignmentAgreement in db.Set<AssignmentAgreementTable>()
							  on creditAppReqAssignment.AssignmentAgreementTableGUID equals assignmentAgreement.AssignmentAgreementTableGUID into ljassignmentAgreement
							  from assignmentAgreement in ljassignmentAgreement.DefaultIfEmpty()
							  join buyerAgreementTable in db.Set<BuyerAgreementTable>() 
							  on creditAppReqAssignment.BuyerAgreementTableGUID equals buyerAgreementTable.BuyerAgreementTableGUID
							  into ljBuyerAgreementTable from buyerAgreementTable in ljBuyerAgreementTable.DefaultIfEmpty()
							  where creditAppReqAssignment.CreditAppRequestTableGUID == refGUID
							  orderby creditAppReqAssignment.CreatedDateTime

							  select new QCreditAppReqAssignmentCrediLimit
							  {
								  QCreditAppReqAssignment_IsNew = creditAppReqAssignment.IsNew,
								  QCreditAppReqAssignment_AssignmentBuyer = String.Concat(buyerTable.BuyerName, " (", buyerTable.BuyerId, ")"),
								  QCreditAppReqAssignment_RefAgreementId = (buyerAgreementTable != null) ? buyerAgreementTable.BuyerAgreementId : string.Empty,
								  QCreditAppReqAssignment_BuyerAgreementAmount = creditAppReqAssignment.BuyerAgreementAmount,
								  QCreditAppReqAssignment_AssignmentAgreementAmount = creditAppReqAssignment.AssignmentAgreementAmount,
								  QCreditAppReqAssignment_AssignmentAgreementDate = assignmentAgreement.AgreementDate,
								  QCreditAppReqAssignment_RemainingAmount = creditAppReqAssignment.RemainingAmount,
								  QCreditAppReqAssignment_Row = 0

							  }
						  ).Take(10).ToList();
                if (result != null)
                {
					int number = 1;
					foreach (var item in result)
					{
						item.QCreditAppReqAssignment_Row = number;
						number++;
					}
					return result;
                }
                else
                {
					return new List<QCreditAppReqAssignmentCrediLimit>() ;
				}
				
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
			
        }

        private List<QActionHistoryCrediLimit> GetActionHistory(Guid refGUID)
        {
			try
			{
				var result = (from actionHistory in db.Set<ActionHistory>()
							  join employeeTable in db.Set<EmployeeTable>()
							  on actionHistory.CreatedBy equals employeeTable.EmployeeId into ljemployeeTable
							  from employeeTable in ljemployeeTable.DefaultIfEmpty()
							  where (actionHistory.ActivityName == "Assist MD approve"
							  || actionHistory.ActivityName == "Board 2 in 3 approve"
							  || actionHistory.ActivityName == "Board 3 in 4 approve")
							  && actionHistory.ActionName == "Approve"
							  && actionHistory.RefGUID == refGUID
							  orderby actionHistory.CreatedDateTime
							  select new QActionHistoryCrediLimit
							  {
								  QActionHistory_ApproverName = employeeTable.Name,
								  QActionHistory_Comment = actionHistory.Comment,
								  QActionHistory_CreatedDateTime = actionHistory.CreatedDateTime,
								  QActionHistory_Row = 0

							  }
										  ).Take(8).ToList();
                if (result != null)
                {
					int number = 1;
					foreach (var item in result)
					{
						item.QActionHistory_Row = number;
						number++;
					}
					return result;
                }
                else
                {
					return new List<QActionHistoryCrediLimit>();

				}
				
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
			
		}

        private QCAReqCreditOutStandingSumCrediLimit GetCAReqCreditOutStandingSum(Guid refGUID)
        {
			try
			{
				var result = (from cAReqCreditOutStanding in db.Set<CAReqCreditOutStanding>()
							  join CreditAppRequestTable in db.Set<CreditAppRequestTable>()
							  on cAReqCreditOutStanding.CreditAppRequestTableGUID equals CreditAppRequestTable.CreditAppRequestTableGUID
							  where cAReqCreditOutStanding.CreditAppRequestTableGUID == refGUID
							  select new QCAReqCreditOutStandingSumCrediLimit
							  {
								  QCAReqCreditOutStandingSum_SumApprovedCreditLimit = cAReqCreditOutStanding.ApprovedCreditLimit,
								  QCAReqCreditOutStandingSum_SumARBalance = cAReqCreditOutStanding.ARBalance,
								  QCAReqCreditOutStandingSum_SumCreditLimitBalance = cAReqCreditOutStanding.CreditLimitBalance,
								  QCAReqCreditOutStandingSum_SumReserveToBeRefund = cAReqCreditOutStanding.ReserveToBeRefund,
								  QCAReqCreditOutStandingSum_SumAccumRetentionAmount = cAReqCreditOutStanding.AccumRetentionAmount

							  }
						   ).ToList();
				if (result != null)
				{
					return new QCAReqCreditOutStandingSumCrediLimit
					{
						QCAReqCreditOutStandingSum_SumApprovedCreditLimit = result.Sum(su => su.QCAReqCreditOutStandingSum_SumApprovedCreditLimit),
						QCAReqCreditOutStandingSum_SumARBalance = result.Sum(su => su.QCAReqCreditOutStandingSum_SumARBalance),
						QCAReqCreditOutStandingSum_SumCreditLimitBalance = result.Sum(su => su.QCAReqCreditOutStandingSum_SumCreditLimitBalance),
						QCAReqCreditOutStandingSum_SumReserveToBeRefund = result.Sum(su => su.QCAReqCreditOutStandingSum_SumReserveToBeRefund),
						QCAReqCreditOutStandingSum_SumAccumRetentionAmount = result.Sum(su => su.QCAReqCreditOutStandingSum_SumAccumRetentionAmount)

					};
				}
				else
				{
					return new QCAReqCreditOutStandingSumCrediLimit();

				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
			
			
        }

        private List<QCAReqCreditOutStandingCrediLimit> GetCAReqCreditOutStanding(Guid refGUID)
        {
			try
			{
				var result = (from creditLimitType in db.Set<CreditLimitType>()
							  join cAReqCreditOutStanding in db.Set<CAReqCreditOutStanding>()
															   .Where(w => w.CreditAppRequestTableGUID == refGUID)
															   .GroupBy(g => new { g.AsOfDate, g.CreditLimitTypeGUID, g.CreditAppRequestTableGUID })
															   .Select(s => new
															   {
																   s.Key.AsOfDate,
																   s.Key.CreditLimitTypeGUID,
																   s.Key.CreditAppRequestTableGUID,
																   QCAReqCreditOutStanding_ApprovedCreditLimit = s.Sum(su => su.ApprovedCreditLimit),
																   QCAReqCreditOutStanding_ARBalance = s.Sum(su => su.ARBalance),
																   QCAReqCreditOutStanding_CreditLimitBalance = s.Sum(su => su.CreditLimitBalance),
																   QCAReqCreditOutStanding_ReserveToBeRefund = s.Sum(su => su.ReserveToBeRefund),
																   QCAReqCreditOutStanding_AccumRetentionAmount = s.Sum(su => su.AccumRetentionAmount),
															   })
							  on creditLimitType.CreditLimitTypeGUID equals cAReqCreditOutStanding.CreditLimitTypeGUID into ljCAReqCreditOutStanding
							  from cAReqCreditOutStanding in ljCAReqCreditOutStanding.DefaultIfEmpty()
							  select new QCAReqCreditOutStandingCrediLimit
							  {
								  QCAReqCreditOutStanding_AsOfDate = cAReqCreditOutStanding.AsOfDate,
								  QCAReqCreditOutStanding_ProductType = creditLimitType.ProductType,
								  QCAReqCreditOutStanding_CreditLimitTypeId = creditLimitType.CreditLimitTypeId,
								  QCAReqCreditOutStanding_ApprovedCreditLimit = cAReqCreditOutStanding.QCAReqCreditOutStanding_ApprovedCreditLimit,
								  QCAReqCreditOutStanding_ARBalance = cAReqCreditOutStanding.QCAReqCreditOutStanding_ARBalance,
								  QCAReqCreditOutStanding_CreditLimitBalance = cAReqCreditOutStanding.QCAReqCreditOutStanding_CreditLimitBalance,
								  QCAReqCreditOutStanding_ReserveToBeRefund = cAReqCreditOutStanding.QCAReqCreditOutStanding_ReserveToBeRefund,
								  QCAReqCreditOutStanding_AccumRetentionAmount = cAReqCreditOutStanding.QCAReqCreditOutStanding_AccumRetentionAmount,
								  QCAReqCreditOutStanding_Row = creditLimitType.BookmarkOrdering,
							  }
						   ).OrderBy(o => o.QCAReqCreditOutStanding_Row).Take(11).ToList();
                if (result != null)
                {
					int number = 1;
					foreach (var item in result)
					{
						item.QCAReqCreditOutStanding_Row = number;
						number++;
					}
					return result;
                }
				else
				{
					return new List<QCAReqCreditOutStandingCrediLimit>();

				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
			
        }
		private void GetCAReqCreditOutStandingAsOfDate(Guid refGUID, BookmarkDocumentCustomerAmendCreditLimitView model)
        {
            try
            {
				QCAReqCreditOutStandingCrediLimit result = 
				(from caReqCreditOutStanding in db.Set<CAReqCreditOutStanding>()
				 where caReqCreditOutStanding.CreditAppRequestTableGUID == refGUID
				 select new QCAReqCreditOutStandingCrediLimit
				 {
					 QCAReqCreditOutStanding_AsOfDate = caReqCreditOutStanding.AsOfDate,
				 }).AsNoTracking().FirstOrDefault();
				model.QCAReqCreditOutStanding_AsOfDate = (result != null) ? result.QCAReqCreditOutStanding_AsOfDate.DateNullToString() : string.Empty;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private QNCBTransSumCrediLimit GetNCBTransSum(Guid refGUID)
        {
			try
			{
				var result = (from ncbTrans in db.Set<NCBTrans>()
							  where ncbTrans.RefGUID == refGUID
							  group new
							  {
								  ncbTrans
							  }
							  by new
							  {
								  ncbTrans.RefGUID
							  } into g

							  select new QNCBTransSumCrediLimit
							  {

								 // QNCBTransSum_LoanLimit = g.Key.CreditLimit,
								  QNCBTransSum_TotalLoanLimit = g.Sum(su => su.ncbTrans.CreditLimit)
							  }
						  ).FirstOrDefault();
				if (result != null)
				{
					return result;

				}
				else
				{
					return new QNCBTransSumCrediLimit();

				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
			

        }

        private List<QNCBTransCrediLimit> GetNCBTrans(Guid refGUID)
        {
			try
			{
				var result = (from ncbTrans in db.Set<NCBTrans>()
							  join bankGroup in db.Set<BankGroup>()
							  on ncbTrans.BankGroupGUID equals bankGroup.BankGroupGUID into ljbankGroup
							  from bankGroup in ljbankGroup.DefaultIfEmpty()

							  join creditType in db.Set<CreditType>()
							  on ncbTrans.CreditTypeGUID equals creditType.CreditTypeGUID into ljcreditType
							  from creditType in ljcreditType.DefaultIfEmpty()

							  where ncbTrans.RefGUID == refGUID
							  orderby ncbTrans.CreatedDateTime
							  select new QNCBTransCrediLimit
							  {

								  QNCBTrans_Institution = bankGroup.Description,
								  QNCBTrans_LoanType = creditType.Description,
								  QNCBTrans_LoanLimit = ncbTrans.CreditLimit,
								  QNCBTrans_Row = 0
							  }
						   ).Take(8).ToList();
                if (result != null)
                {
					int number = 1;
					foreach (var item in result)
					{
						item.QNCBTrans_Row = number;
						number++;
					}
					return result;
                }
                else
                {
					return new List<QNCBTransCrediLimit>();

				}
				
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
			

		}

        private QTaxReportTransSumCrediLimit GetTaxReportTransSum(Guid refGUID)
        {
			try
			{
				var result = (from taxReportTrans in db.Set<TaxReportTrans>()
							  where taxReportTrans.RefGUID == refGUID
							  select new QTaxReportTransSumCrediLimit
							  {
								  QTaxReportTransSum_SumTaxReport = taxReportTrans.Amount
							  }
										  ).ToList();
                if (result != null)
                {
					return new QTaxReportTransSumCrediLimit
					{
						QTaxReportTransSum_SumTaxReport = result.Sum(su => su.QTaxReportTransSum_SumTaxReport),
						QTaxReportTransSum_TaxReportCOUNT = result.Count()
					};
                }
                else
                {
					return new QTaxReportTransSumCrediLimit();

				}
				
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
			
		
        }

        private QTaxReportEndCrediLimit GetTaxReportEnd(Guid refGUID)
        {
			try
			{
				var result = (from taxReportTrans in db.Set<TaxReportTrans>()
							  where taxReportTrans.RefGUID == refGUID
							  orderby taxReportTrans.Year descending, taxReportTrans.Month descending
							  select new QTaxReportEndCrediLimit
							  {
								  QTaxReportEnd_TaxMonth = String.Concat(taxReportTrans.Month, " ", taxReportTrans.Year)
							  }
							).FirstOrDefault();
				if (result != null)
				{
					return result;
				}
				else
				{
					return new QTaxReportEndCrediLimit();

				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
			
		}

        private QTaxReportStartCrediLimit GetTaxReportStart(Guid refGUID)
        {
			try
			{
				var result = (from taxReportTrans in db.Set<TaxReportTrans>()
							  where taxReportTrans.RefGUID == refGUID
							  orderby taxReportTrans.Year, taxReportTrans.Month
							  select new QTaxReportStartCrediLimit
							  {
								  QTaxReportStart_TaxMonth = String.Concat(taxReportTrans.Month, " ", taxReportTrans.Year)
							  }
			).FirstOrDefault();
				if (result != null)
				{
					return result;
				}
				else
				{
					return new QTaxReportStartCrediLimit();

				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
			

        }

        private List<QTaxReportTransCrediLimit> GetTaxReportTransList(Guid refGUID)
        {
			try
			{
				var result = (from taxReportTrans in db.Set<TaxReportTrans>()
							  where taxReportTrans.RefGUID == refGUID
							  orderby taxReportTrans.Year, taxReportTrans.Month
							  select new QTaxReportTransCrediLimit
							  {

								  QTaxReportTrans_TaxMonth = String.Concat(taxReportTrans.Month, "-", taxReportTrans.Year),
								  QTaxReportTrans_Amount = taxReportTrans.Amount,
								  QTaxReportTrans_Row = 0,
							  }
							  ).Take(12).ToList();
                if (result != null)
                {
					int number = 1;
					foreach (var item in result)
					{
						item.QTaxReportTrans_Row = number;
						number++;
					}
					return result;
                }
                else
                {
					return new List<QTaxReportTransCrediLimit>();

				}
				
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private QCreditAppReqLineFinCrediLimit GetCreditAppReqLineFinEachRow(int row, List<QCreditAppReqLineFinCrediLimit> mainCreditAppReqLineFin, Guid refGUID)
        {
			try
			{
				var data = mainCreditAppReqLineFin.Where(wh => wh.QCreditAppReqLineFin_Row == row).FirstOrDefault();
				if (data != null)
                {
					var model = new QCreditAppReqLineFinCrediLimit();
					model.QCreditAppReqLineFin_RegisteredCapital = GetFinanceStatementAmount(1, data, refGUID);
					model.QCreditAppReqLineFin_PaidCapital = GetFinanceStatementAmount(2, data, refGUID);
					model.QCreditAppReqLineFin_TotalAsset = GetFinanceStatementAmount(3, data, refGUID);
					model.QCreditAppReqLineFin_TotalLiability = GetFinanceStatementAmount(4, data, refGUID);
					model.QCreditAppReqLineFin_TotalEquity = GetFinanceStatementAmount(5, data, refGUID);
					model.QCreditAppReqLineFin_TotalRevenue = GetFinanceStatementAmount(6, data, refGUID);
					model.QCreditAppReqLineFin_TotalCOGS = GetFinanceStatementAmount(7, data, refGUID);
					model.QCreditAppReqLineFin_TotalGrossProfit = GetFinanceStatementAmount(8, data, refGUID);
					model.QCreditAppReqLineFin_TotalOperExpFirst = GetFinanceStatementAmount(9, data, refGUID);
					model.QCreditAppReqLineFin_TotalNetProfitFirst = GetFinanceStatementAmount(10, data, refGUID);
					model.QCreditAppReqLineFin_NetProfitPercent = GetFinanceStatementAmount(11, data, refGUID);
					model.QCreditAppReqLineFin_lDE = GetFinanceStatementAmount(12, data, refGUID);
					model.QCreditAppReqLineFin_QuickRatio = GetFinanceStatementAmount(13, data, refGUID);
					model.QCreditAppReqLineFin_IntCoverageRatio = GetFinanceStatementAmount(14, data, refGUID);
					model.QCreditAppReqLineFin_Year = data.QCreditAppReqLineFin_Year;
					return model;
                }
                else
                {
					return new QCreditAppReqLineFinCrediLimit();
				}

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private decimal GetFinanceStatementAmount(int ordering, QCreditAppReqLineFinCrediLimit mainCreditAppReqLineFin , Guid refGUID)
        {
			try
			{
				var result = (from financialStatementTrans in db.Set<FinancialStatementTrans>()
							  where financialStatementTrans.RefGUID == refGUID
							  && financialStatementTrans.RefType == (int)RefType.CreditAppRequestTable
							  && financialStatementTrans.Year == mainCreditAppReqLineFin.QCreditAppReqLineFin_Year
							  && financialStatementTrans.Ordering == ordering
							  select financialStatementTrans.Amount
							  ).FirstOrDefault();


					return result;
        
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #region QCreditAppReqLineFin 
        private List<QCreditAppReqLineFinCrediLimit> GetCreditAppReqLineFinList(Guid refGUID)
        {
			try
			{
				var result = (from financialStatementTrans in db.Set<FinancialStatementTrans>()
							  
							  where financialStatementTrans.RefType == (int)RefType.CreditAppRequestTable
						
							  && financialStatementTrans.RefGUID == refGUID
							  
							  group new
                              {
								  financialStatementTrans
							  }
							  by new
                              {
								  financialStatementTrans.Year
								//  financialStatementTrans.Ordering
							  } into g
							  select new QCreditAppReqLineFinCrediLimit
							  {

								  QCreditAppReqLineFin_Year = g.Key.Year,
								//  QCreditAppReqLineFin_Row = g.Key.Ordering
							  }
							  ).OrderByDescending(o=>o.QCreditAppReqLineFin_Year).Take(5).ToList();
                if (result != null)
                {
					int number = 1;
					foreach (var item in result)
					{
						item.QCreditAppReqLineFin_Row = number;
						number++;
					}
					return result;
                }
                else
                {
					return new List<QCreditAppReqLineFinCrediLimit>();

				}
				
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion QCreditAppReqLineFin 
		private List<QOwnerTransCrediLimit> GetOwnerTransList(Guid refGUID)
        {
			try
			{
				var result = (from ownerTrans in db.Set<OwnerTrans>()
							  join relatedPerson in db.Set<RelatedPersonTable>()
							  on ownerTrans.RelatedPersonTableGUID equals relatedPerson.RelatedPersonTableGUID into ljrelatedPerson
							  from relatedPerson in ljrelatedPerson.DefaultIfEmpty()
							  where ownerTrans.RefGUID == refGUID
							  select new QOwnerTransCrediLimit
							  {
								  QOwnerTrans_ShareHolderPersonName = relatedPerson.Name,
								  QOwnerTrans_PropotionOfShareholderPct = ownerTrans.PropotionOfShareholderPct,
								  QOwnerTrans_Row = ownerTrans.Ordering
							  }).OrderBy(o => o.QOwnerTrans_Row).Take(8).ToList();
			
                if (result!=null)
                {
					int number = 1;
					foreach (var item in result)
					{
						item.QOwnerTrans_Row = number;
						number++;
					}
					return result;
				}
                else
                {
					return new List<QOwnerTransCrediLimit>();

				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private List<QAuthorizedPersonTransCrediLimit> GetAuthorizedPersonTransList(Guid refGUID)
        {
			try
			{
				var result = (from authorizedPersonTrans in db.Set<AuthorizedPersonTrans>()
							  join relatedPersonTable in db.Set<RelatedPersonTable>()
							  on authorizedPersonTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID into ljrelatedPersonTable
							  from relatedPersonTable in ljrelatedPersonTable.DefaultIfEmpty()
							  where authorizedPersonTrans.RefGUID == refGUID
							  select new QAuthorizedPersonTransCrediLimit
							  {
								  QAuthorizedPersonTrans_AuthorizedPersonName = relatedPersonTable.Name,
								  QAuthorizedPersonTrans_DateOfBirth = relatedPersonTable.DateOfBirth,
								  QAuthorizedPersonTrans_Row = authorizedPersonTrans.Ordering
							  }).OrderBy(o => o.QAuthorizedPersonTrans_Row).Take(8).ToList();
				
                if (result != null)
                {
					int number = 1;
					foreach (var item in result)
					{
						item.QAuthorizedPersonTrans_Row = number;
						number++;
					}
					return result;
                }
                else
                {
					return new List<QAuthorizedPersonTransCrediLimit>();

				}

			
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private decimal GetSumCreditAppReqBusinessCollateral(Guid refGUID)
        {
			try
			{
				decimal result = (from creditAppReqBusinessCollateral in db.Set<CreditAppReqBusinessCollateral>()
							  where creditAppReqBusinessCollateral.CreditAppRequestTableGUID == refGUID
							  orderby creditAppReqBusinessCollateral.CreatedDateTime
							  select creditAppReqBusinessCollateral.BusinessCollateralValue
							   ).Take(10).Sum();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private List<QCreditAppReqBusinessCollateralCrediLimit> GetCreditAppReqBusinessCollateralList(Guid refGUID)
        {
			try
			{
				var result = (
					from creditAppReqBusinessColeral in db.Set<CreditAppReqBusinessCollateral>()
					join businessCollateralType in db.Set<BusinessCollateralType>()
					on creditAppReqBusinessColeral.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID into ljbusinessCollateralType
					from businessCollateralType in ljbusinessCollateralType.DefaultIfEmpty()

					join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
					on creditAppReqBusinessColeral.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
					from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()
					where creditAppReqBusinessColeral.CreditAppRequestTableGUID == refGUID
					select new QCreditAppReqBusinessCollateralCrediLimit
					{
						QCreditAppReqBusinessCollateral_IsNew = (creditAppReqBusinessColeral.IsNew) ? "Yes": "No",
						 QCreditAppReqBusinessCollateral_BusinessCollateralType = businessCollateralType.Description,
						 QCreditAppReqBusinessCollateral_BusinessCollateralSubType = businessCollateralSubType.Description,
						 QCreditAppReqBusinessCollateral_Description = creditAppReqBusinessColeral.Description,
						 QCreditAppReqBusinessCollateral_BusinessCollateralValue = creditAppReqBusinessColeral.BusinessCollateralValue,
						QCreditAppReqBusinessCollateral_CreateDateTime = creditAppReqBusinessColeral.CreatedDateTime,
						QCreditAppReqBusinessCollateral_Row = 0

					}


					).OrderBy(o => o.QCreditAppReqBusinessCollateral_CreateDateTime).Take(15).ToList();

				
                if (result != null)
                {
					var number = 1;
					foreach (var item in result)
					{
						item.QCreditAppReqBusinessCollateral_Row = number;
						number++;
					}
					return result;
                }
                else
                {
					return new List<QCreditAppReqBusinessCollateralCrediLimit>();

				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private List<QGuarantorTransCrediLimit> GetGuarantorTransList(Guid refGUID)
        {
			try
			{
				var result = (from guarantorTrans in db.Set<GuarantorTrans>()
							  join relatedPersonTable in db.Set<RelatedPersonTable>()
							  on guarantorTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID into ljrelatedPersonTable
							  from relatedPersonTable in ljrelatedPersonTable.DefaultIfEmpty()
							  join guarantorType in db.Set<GuarantorType>()
							  on guarantorTrans.GuarantorTypeGUID equals guarantorType.GuarantorTypeGUID into ljguarantorType
							  from guarantorType in ljguarantorType.DefaultIfEmpty()
							  //join creditAppTable in db.Set<CreditAppTable>()
							  //on guarantorTrans.RefGUID equals creditAppTable.CreditAppTableGUID
							  //join creditAppRequestTable in db.Set<CreditAppRequestTable>()
							  //on creditAppTable.CreditAppTableGUID equals creditAppRequestTable.RefCreditAppTableGUID
							  where guarantorTrans.RefGUID == refGUID
							  select new QGuarantorTransCrediLimit
							  {
								  QGuarantorTrans_RelatedPersonName = relatedPersonTable.Name,
								  QGuarantorTrans_GuarantorType = guarantorType.Description,
								  QGuarantorTrans_Row = guarantorTrans.Ordering
							  }
							  ).OrderBy(o => o.QGuarantorTrans_Row).Take(8).ToList();
				
	
                if (result != null)
                {
					var number = 1;
					foreach (var item in result)
					{
						item.QGuarantorTrans_Row = number;
						number++;
					}
					return result;
                }
                else
                {
					return new List<QGuarantorTransCrediLimit>();

				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private decimal GetSumServiceFeeConditionTrans(Guid refGUID)
        {

			try
			{
				var result = (from serviceFeeConditionTrans in db.Set<ServiceFeeConditionTrans>()
							  //join invoiceRevenueType in db.Set<InvoiceRevenueType>()
							  //on serviceFeeConditionTrans.InvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID into ljinvoiceRevenueType
							  //from invoiceRevenueType in ljinvoiceRevenueType.DefaultIfEmpty()
							  where serviceFeeConditionTrans.RefGUID == refGUID
							  select new QSumServiceFeeConditionTransCrediLimit
							  {

								  QSumServiceFeeConditionTrans_ServiceFeeAmt = serviceFeeConditionTrans.AmountBeforeTax

							  }
							  ).ToList().Sum(s => s.QSumServiceFeeConditionTrans_ServiceFeeAmt);
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private List<QServiceFeeConditionTransCrediLimit> GetServiceFeeConditionTransList(Guid refGUID)
        {
			try
			{
				var result = (from serviceFeeConditionTrans in db.Set<ServiceFeeConditionTrans>()
							  join invoiceRevenueType in db.Set<InvoiceRevenueType>()
							  on serviceFeeConditionTrans.InvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID into ljinvoiceRevenueType
							  from invoiceRevenueType in ljinvoiceRevenueType.DefaultIfEmpty()
							  where serviceFeeConditionTrans.RefGUID == refGUID
							  
							  select new QServiceFeeConditionTransCrediLimit
							  {
								  QServiceFeeConditionTrans_Description = serviceFeeConditionTrans.Description,
								  QServiceFeeConditionTrans_AmountBeforeTax = serviceFeeConditionTrans.AmountBeforeTax,
								  QServiceFeeConditionTrans_ServicefeeTypeId  = invoiceRevenueType.RevenueTypeId,
								  QServiceFeeConditionTrans_Row  = serviceFeeConditionTrans.Ordering,
								  QServiceFeeConditionTrans_ServiceFeeDesc = invoiceRevenueType.Description,

							  }).OrderBy(or =>or.QServiceFeeConditionTrans_Row).Take(10).ToList();


                if (result != null)
                {
					int number = 1;
					foreach (var item in result)
					{
						item.QServiceFeeConditionTrans_Row = number;
						number++;
					}
					return result;
				}
                else
                {
					return new List<QServiceFeeConditionTransCrediLimit>();
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        private QCreditAppRequestTableCrediLimit GetCreditAppRequestQuery(Guid refGUID , string refType)
		{
			try
			{
				var result = (from qCreditAppRequestTable in db.Set<CreditAppRequestTable>()

							  join documentProcess in db.Set<DocumentProcess>()
							  on refType equals documentProcess.ProcessId into ljdocumentProcess
							  from documentProcess in ljdocumentProcess.DefaultIfEmpty()

							  join documentStatus in db.Set<DocumentStatus>()
							  on qCreditAppRequestTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus
							  from documentStatus in ljdocumentStatus.DefaultIfEmpty()

							  join kycSetup in db.Set<KYCSetup>()
							  on qCreditAppRequestTable.KYCGUID equals kycSetup.KYCSetupGUID into ljkycSetup
							  from kycSetup in ljkycSetup.DefaultIfEmpty()

							  join creditScrolling in db.Set<CreditScoring>()
							  on qCreditAppRequestTable.CreditScoringGUID equals creditScrolling.CreditScoringGUID into ljcreditScrolling
							  from creditScrolling in ljcreditScrolling.DefaultIfEmpty()

							  join documentReson in db.Set<DocumentReason>()
							  on qCreditAppRequestTable.DocumentReasonGUID equals documentReson.DocumentReasonGUID into ljdocumentReson
							  from documentReson in ljdocumentReson.DefaultIfEmpty()



							  join productSubType in db.Set<ProductSubType>()
							  on qCreditAppRequestTable.ProductSubTypeGUID equals productSubType.ProductSubTypeGUID into ljproductSubType
							  from productSubType in ljproductSubType.DefaultIfEmpty()

							  join creditLimitType in db.Set<CreditLimitType>()
							  on qCreditAppRequestTable.CreditLimitTypeGUID equals creditLimitType.CreditLimitTypeGUID into ljcreditLimitType
							  from creditLimitType in ljcreditLimitType.DefaultIfEmpty()

							  join interestType in db.Set<InterestType>()
							  on qCreditAppRequestTable.InterestTypeGUID equals interestType.InterestTypeGUID into ljinterestType
							  from interestType in ljinterestType.DefaultIfEmpty()

							  join addressTrans in db.Set<AddressTrans>()
							  on qCreditAppRequestTable.RegisteredAddressGUID equals addressTrans.AddressTransGUID into ljaddressTrans
							  from addressTrans in ljaddressTrans.DefaultIfEmpty()

							  join customerTable in db.Set<CustomerTable>()
							  on qCreditAppRequestTable.CustomerTableGUID equals customerTable.CustomerTableGUID into ljcustomerTable
							  from customerTable in ljcustomerTable.DefaultIfEmpty()

							  join employeeTable in db.Set<EmployeeTable>()
							  on customerTable.ResponsibleByGUID equals employeeTable.EmployeeTableGUID into ljemployeeTable
							  from employeeTable in ljemployeeTable.DefaultIfEmpty()




							  join businessType in db.Set<BusinessType>()
							  on customerTable.BusinessTypeGUID equals businessType.BusinessTypeGUID into ljbusinessType
							  from businessType in ljbusinessType.DefaultIfEmpty()

							  join introductionBy in db.Set<IntroducedBy>()
							  on customerTable.IntroducedByGUID equals introductionBy.IntroducedByGUID into ljintroductionBy
							  from introductionBy in ljintroductionBy.DefaultIfEmpty()

							  join originalCreditAppRequest in db.Set<CreditAppRequestTable>()
							  on qCreditAppRequestTable.RefCreditAppTableGUID equals originalCreditAppRequest.CreditAppRequestTableGUID into ljoriginalCreditAppRequest
							  from originalCreditAppRequest in ljoriginalCreditAppRequest.DefaultIfEmpty()

							  join bankAccountControl in db.Set<CustBank>()
							  on qCreditAppRequestTable.BankAccountControlGUID equals bankAccountControl.CustBankGUID into ljbankAccountControl
							  from bankAccountControl in ljbankAccountControl.DefaultIfEmpty()

							  join bankGroup in db.Set<BankGroup>()
							  on bankAccountControl.BankGroupGUID equals bankGroup.BankGroupGUID into ljbankGroup
							  from bankGroup in ljbankGroup.DefaultIfEmpty()

							  join bankType in db.Set<BankType>()
							  on bankAccountControl.BankTypeGUID equals bankType.BankTypeGUID into ljbankType
							  from bankType in ljbankType.DefaultIfEmpty()

							  join custBank in db.Set<CustBank>()
							  on qCreditAppRequestTable.PDCBankGUID equals custBank.CustBankGUID into ljcustBank
							  from custBank in ljcustBank.DefaultIfEmpty()

                              join custBankGroup in db.Set<BankGroup>()
                              on custBank.BankGroupGUID equals custBankGroup.BankGroupGUID into ljcustBankGroup
                              from custBankGroup in ljcustBankGroup.DefaultIfEmpty()

                              join custBankType in db.Set<BankType>()
							  on custBank.BankTypeGUID equals custBankType.BankTypeGUID into ljcustBankType
							  from custBankType in ljcustBankType.DefaultIfEmpty()

							  join originalCreditAppTable in db.Set<CreditAppTable>()
							  on qCreditAppRequestTable.RefCreditAppTableGUID equals originalCreditAppTable.CreditAppTableGUID into ljoriginalCreditAppTable
							  from originalCreditAppTable in ljoriginalCreditAppTable.DefaultIfEmpty()


							  join creditAppRequestTableAmend in db.Set<CreditAppRequestTableAmend>() 
							  on qCreditAppRequestTable.CreditAppRequestTableGUID equals creditAppRequestTableAmend.CreditAppRequestTableGUID 
							  into ljCreditAppRequestTableAmend from creditAppRequestTableAmend in ljCreditAppRequestTableAmend.DefaultIfEmpty()
							  where qCreditAppRequestTable.CreditAppRequestTableGUID == refGUID
							  select new QCreditAppRequestTableCrediLimit
							  {
								  QCustomer_CustomerId = customerTable.CustomerId,
								  QCustomer_CustomerName = customerTable.Name,
								  QCustomer_ResponsibleByGUID = customerTable.ResponsibleByGUID,
								  QCustomer_IdentificationType = customerTable.IdentificationType,
								  QCustomer_TaxID = customerTable.TaxID,
								  QCustomer_PassportID = customerTable.PassportID,
								  QCustomer_RecordType = customerTable.RecordType,
								  QCustomer_DateOfEstablish = customerTable.DateOfEstablish,
								  QCustomer_DateOfBirth = customerTable.DateOfBirth,
								  QCustomer_BusinessTypeGUID = customerTable.BusinessTypeGUID,
								  QCustomer_IntroducedByGUID = customerTable.IntroducedByGUID,
								  QCustomer_ResponsibleBy = employeeTable.Name,

								  QOriginalCreditAppRequest_OriginalCreditAppRequestId = originalCreditAppRequest.CreditAppRequestId,
								  QOriginalCreditAppRequest_ApprovedCreditLimitRequest = originalCreditAppRequest.ApprovedCreditLimitRequest,
								  QOriginalCreditAppRequest_CreditLimitRequest = (creditAppRequestTableAmend != null) ? creditAppRequestTableAmend.OriginalCreditLimit: 0,

								  QBankAccountControl_Instituetion = bankGroup.Description,
								  QBankAccountControl_BankBranch = bankAccountControl.BankBranch,
								  QBankAccountControl_AccountType = bankType.Description,
								  QBankAccountControl_BankAccountName = custBank.AccountNumber,

								  QCreditAppRequestTable_CreditAppRequestId = qCreditAppRequestTable.CreditAppRequestId,
								  QCreditAppRequestTable_RefCreditAppTableGUID = qCreditAppRequestTable.RefCreditAppTableGUID,
								  QCreditAppRequestTable_DocumentStatusGUID = qCreditAppRequestTable.DocumentStatusGUID,
								  QCreditAppRequestTable_RequestDate = qCreditAppRequestTable.RequestDate,
								  QCreditAppRequestTable_CustomerTableGUID = qCreditAppRequestTable.CustomerTableGUID,
								  QCreditAppRequestTable_Remark = qCreditAppRequestTable.Remark,
								  QCreditAppRequestTable_ProductType = qCreditAppRequestTable.ProductType,
								  QCreditAppRequestTable_ProductSubTypeGUID = qCreditAppRequestTable.ProductSubTypeGUID,
								  QCreditAppRequestTable_CreditLimitTypeGUID = qCreditAppRequestTable.CreditLimitTypeGUID,
								  QCreditAppRequestTable_CreditLimitExpiration = qCreditAppRequestTable.CreditLimitExpiration,
								  QCreditAppRequestTable_StartDate = qCreditAppRequestTable.StartDate,
								  QCreditAppRequestTable_ExpiryDate = qCreditAppRequestTable.ExpiryDate,
								  QCreditAppRequestTable_ApprovedCreditLimitRequest = qCreditAppRequestTable.ApprovedCreditLimitRequest,
								  QCreditAppRequestTable_CreditLimitRequest = qCreditAppRequestTable.CreditLimitRequest,
								  QCreditAppRequestTable_InterestTypeGUID = qCreditAppRequestTable.InterestTypeGUID,
								  QCreditAppRequestTable_InterestAdjustment = qCreditAppRequestTable.InterestAdjustment,
								  QCreditAppRequestTable_MaxPurchasePct = qCreditAppRequestTable.MaxPurchasePct,
								  QCreditAppRequestTable_MaxRetentionPct = qCreditAppRequestTable.MaxRetentionPct,
								  QCreditAppRequestTable_MaxRetentionAmount = qCreditAppRequestTable.MaxRetentionAmount,
								  QCreditAppRequestTable_CACondition = qCreditAppRequestTable.CACondition,
								  QCreditAppRequestTable_PDCBankGUID = qCreditAppRequestTable.PDCBankGUID,
								  QCreditAppRequestTable_CreditRequestFeePct = qCreditAppRequestTable.CreditRequestFeePct,
								  QCreditAppRequestTable_CreditRequestFeeAmount = qCreditAppRequestTable.CreditRequestFeeAmount,
								  QCreditAppRequestTable_PurchaseFeePct = qCreditAppRequestTable.PurchaseFeePct,
								  QCreditAppRequestTable_PurchaseFeeCalculateBase = qCreditAppRequestTable.PurchaseFeeCalculateBase,
								  QCreditAppRequestTable_RegisteredAddressGUID = qCreditAppRequestTable.RegisteredAddressGUID,
								  QCreditAppRequestTable_IntroducedByGUID = customerTable.IntroducedByGUID,
								  QCreditAppRequestTable_SalesAvgPerMonth = qCreditAppRequestTable.SalesAvgPerMonth,
								  QCreditAppRequestTable_MarketingComment = qCreditAppRequestTable.MarketingComment,
								  QCreditAppRequestTable_CreditComment = qCreditAppRequestTable.CreditComment,
								  QCreditAppRequestTable_ApproverComment = qCreditAppRequestTable.ApproverComment,
								  QCreditAppRequestTable_CustomerCreditLimit = qCreditAppRequestTable.CustomerCreditLimit,
								  QCreditAppRequestTable_NCBCheckedDate = qCreditAppRequestTable.NCBCheckedDate,
								  QCreditAppRequestTable_Description = qCreditAppRequestTable.Description,

								  DocumentStatus_Status = documentStatus.Description,
								  KYCSetup_KYC = kycSetup.Description,
								  CreditScoring_CreditScoring = creditScrolling.Description,
								  ProductSubType_ProductSubType = productSubType.Description,
								  CreditLimitType_CreditLimitType = creditLimitType.Description,
								  InterestType_InterestType = interestType.Description,
								  AddressTrans_RegisteredAddress = addressTrans.Address1 + " " + addressTrans.Address2,
								  BusinessType_BusinessType = businessType.Description,
								  IntroducedBy_IntroducedBy = introductionBy.Description,

								  QPDCBank_CustPDCBankName = custBankGroup.Description,
								  QPDCBank_CustPDCBankBranch = custBank.BankBranch,
								  QPDCBank_CustPDCBankType = custBankType.Description,
								  QPDCBank_CustPDCBankAccountName = custBank.AccountNumber,

								  QCreditAppRequestTable_OriginalCreditAppTableId = originalCreditAppTable.CreditAppId,
									QCreditAppRequestTable_TotalInterestCA = originalCreditAppTable.TotalInterestPct
	}
									).FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
			
		}
		#endregion

		public List<CustomerTable> GetCustomerTableByIdNoTracking(List<Guid> customerTableGuids)
		{
			try
			{
				List<CustomerTable> customerTables = Entity.Where(w => customerTableGuids.Contains(w.CustomerTableGUID)).AsNoTracking().ToList();
				return customerTables;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

