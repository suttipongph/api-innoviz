using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICAReqRetentionOutstandingRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CAReqRetentionOutstandingItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<CAReqRetentionOutstandingListView> GetListvw(SearchParameter search);
		CAReqRetentionOutstandingItemView GetByIdvw(Guid id);
		CAReqRetentionOutstanding Find(params object[] keyValues);
		CAReqRetentionOutstanding GetCAReqRetentionOutstandingByIdNoTracking(Guid guid);
		CAReqRetentionOutstanding CreateCAReqRetentionOutstanding(CAReqRetentionOutstanding caReqRetentionOutstanding);
		void CreateCAReqRetentionOutstandingVoid(CAReqRetentionOutstanding caReqRetentionOutstanding);
		CAReqRetentionOutstanding UpdateCAReqRetentionOutstanding(CAReqRetentionOutstanding dbCAReqRetentionOutstanding, CAReqRetentionOutstanding inputCAReqRetentionOutstanding, List<string> skipUpdateFields = null);
		void UpdateCAReqRetentionOutstandingVoid(CAReqRetentionOutstanding dbCAReqRetentionOutstanding, CAReqRetentionOutstanding inputCAReqRetentionOutstanding, List<string> skipUpdateFields = null);
		void Remove(CAReqRetentionOutstanding item);
		CAReqRetentionOutstanding GetCAReqRetentionOutstandingByIdNoTrackingByAccessLevel(Guid guid);
	}
	public class CAReqRetentionOutstandingRepo : CompanyBaseRepository<CAReqRetentionOutstanding>, ICAReqRetentionOutstandingRepo
	{
		public CAReqRetentionOutstandingRepo(SmartAppDbContext context) : base(context) { }
		public CAReqRetentionOutstandingRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CAReqRetentionOutstanding GetCAReqRetentionOutstandingByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CAReqRetentionOutstandingGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CAReqRetentionOutstandingItemViewMap> GetDropDownQuery()
		{
			return (from caReqRetentionOutstanding in Entity
					select new CAReqRetentionOutstandingItemViewMap
					{
						CompanyGUID = caReqRetentionOutstanding.CompanyGUID,
						Owner = caReqRetentionOutstanding.Owner,
						OwnerBusinessUnitGUID = caReqRetentionOutstanding.OwnerBusinessUnitGUID,
						CAReqRetentionOutstandingGUID = caReqRetentionOutstanding.CAReqRetentionOutstandingGUID
					});
		}
		public IEnumerable<SelectItem<CAReqRetentionOutstandingItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CAReqRetentionOutstanding>(search, SysParm.CompanyGUID);
				var caReqRetentionOutstanding = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CAReqRetentionOutstandingItemViewMap, CAReqRetentionOutstandingItemView>().ToDropDownItem(search.ExcludeRowData);


				return caReqRetentionOutstanding;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CAReqRetentionOutstandingListViewMap> GetListQuery()
		{
			return (from caReqRetentionOutstanding in Entity
					join creditAppTable in db.Set<CreditAppTable>()
					on caReqRetentionOutstanding.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljCreditAppTable
					from creditAppTable in ljCreditAppTable.DefaultIfEmpty()
					join customerTable in db.Set<CustomerTable>()
					on caReqRetentionOutstanding.CustomerTableGUID equals customerTable.CustomerTableGUID into ljCustomerTable
					from customerTable in ljCustomerTable.DefaultIfEmpty()
					select new CAReqRetentionOutstandingListViewMap
				{
						CompanyGUID = caReqRetentionOutstanding.CompanyGUID,
						Owner = caReqRetentionOutstanding.Owner,
						OwnerBusinessUnitGUID = caReqRetentionOutstanding.OwnerBusinessUnitGUID,
						CAReqRetentionOutstandingGUID = caReqRetentionOutstanding.CAReqRetentionOutstandingGUID,
						ProductType = caReqRetentionOutstanding.ProductType,
						CreditAppTableGUID = caReqRetentionOutstanding.CreditAppTableGUID,
						CustomerTableGUID = caReqRetentionOutstanding.CustomerTableGUID,
						CreditAppRequestTableGUID = caReqRetentionOutstanding.CreditAppRequestTableGUID,
						MaximumRetention = caReqRetentionOutstanding.MaximumRetention,
						AccumRetentionAmount = caReqRetentionOutstanding.AccumRetentionAmount,
						RemainingAmount = caReqRetentionOutstanding.RemainingAmount,
						CreditAppTable_Values = (creditAppTable != null) ? SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description) : string.Empty,
						CreditAppTable_CreditAppId = (creditAppTable != null) ? creditAppTable.CreditAppId: string.Empty,
						CustomerTable_Values = (customerTable != null) ? SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name) : string.Empty,
						CustomerTable_CustomerId = (customerTable != null) ? customerTable.CustomerId : string.Empty,
					});
		}
		public SearchResult<CAReqRetentionOutstandingListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CAReqRetentionOutstandingListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CAReqRetentionOutstanding>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CAReqRetentionOutstandingListViewMap, CAReqRetentionOutstandingListView>();
				result = list.SetSearchResult<CAReqRetentionOutstandingListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CAReqRetentionOutstandingItemViewMap> GetItemQuery()
		{
			return (from caReqRetentionOutstanding in Entity
					join company in db.Set<Company>()
					on caReqRetentionOutstanding.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on caReqRetentionOutstanding.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCAReqRetentionOutstandingOwnerBU
					from ownerBU in ljCAReqRetentionOutstandingOwnerBU.DefaultIfEmpty()
					select new CAReqRetentionOutstandingItemViewMap
					{
						CompanyGUID = caReqRetentionOutstanding.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = caReqRetentionOutstanding.Owner,
						OwnerBusinessUnitGUID = caReqRetentionOutstanding.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = caReqRetentionOutstanding.CreatedBy,
						CreatedDateTime = caReqRetentionOutstanding.CreatedDateTime,
						ModifiedBy = caReqRetentionOutstanding.ModifiedBy,
						ModifiedDateTime = caReqRetentionOutstanding.ModifiedDateTime,
						CAReqRetentionOutstandingGUID = caReqRetentionOutstanding.CAReqRetentionOutstandingGUID,
						AccumRetentionAmount = caReqRetentionOutstanding.AccumRetentionAmount,
						CreditAppRequestTableGUID = caReqRetentionOutstanding.CreditAppRequestTableGUID,
						CreditAppTableGUID = caReqRetentionOutstanding.CreditAppTableGUID,
						CustomerTableGUID = caReqRetentionOutstanding.CustomerTableGUID,
						MaximumRetention = caReqRetentionOutstanding.MaximumRetention,
						ProductType = caReqRetentionOutstanding.ProductType,
						RemainingAmount = caReqRetentionOutstanding.RemainingAmount,
					
						RowVersion = caReqRetentionOutstanding.RowVersion,
					});
		}
		public CAReqRetentionOutstandingItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<CAReqRetentionOutstandingItemViewMap, CAReqRetentionOutstandingItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public CAReqRetentionOutstanding CreateCAReqRetentionOutstanding(CAReqRetentionOutstanding caReqRetentionOutstanding)
		{
			try
			{
				caReqRetentionOutstanding.CAReqRetentionOutstandingGUID = Guid.NewGuid();
				base.Add(caReqRetentionOutstanding);
				return caReqRetentionOutstanding;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCAReqRetentionOutstandingVoid(CAReqRetentionOutstanding caReqRetentionOutstanding)
		{
			try
			{
				CreateCAReqRetentionOutstanding(caReqRetentionOutstanding);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CAReqRetentionOutstanding UpdateCAReqRetentionOutstanding(CAReqRetentionOutstanding dbCAReqRetentionOutstanding, CAReqRetentionOutstanding inputCAReqRetentionOutstanding, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCAReqRetentionOutstanding = dbCAReqRetentionOutstanding.MapUpdateValues<CAReqRetentionOutstanding>(inputCAReqRetentionOutstanding);
				base.Update(dbCAReqRetentionOutstanding);
				return dbCAReqRetentionOutstanding;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCAReqRetentionOutstandingVoid(CAReqRetentionOutstanding dbCAReqRetentionOutstanding, CAReqRetentionOutstanding inputCAReqRetentionOutstanding, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCAReqRetentionOutstanding = dbCAReqRetentionOutstanding.MapUpdateValues<CAReqRetentionOutstanding>(inputCAReqRetentionOutstanding, skipUpdateFields);
				base.Update(dbCAReqRetentionOutstanding);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public CAReqRetentionOutstanding GetCAReqRetentionOutstandingByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CAReqRetentionOutstandingGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

