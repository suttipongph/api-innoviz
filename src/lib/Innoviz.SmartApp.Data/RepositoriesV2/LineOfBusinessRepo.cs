using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ILineOfBusinessRepo
	{
		#region DropDown
		IEnumerable<SelectItem<LineOfBusinessItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<LineOfBusinessListView> GetListvw(SearchParameter search);
		LineOfBusinessItemView GetByIdvw(Guid id);
		LineOfBusiness Find(params object[] keyValues);
		LineOfBusiness GetLineOfBusinessByIdNoTracking(Guid guid);
		LineOfBusiness CreateLineOfBusiness(LineOfBusiness lineOfBusiness);
		void CreateLineOfBusinessVoid(LineOfBusiness lineOfBusiness);
		LineOfBusiness UpdateLineOfBusiness(LineOfBusiness dbLineOfBusiness, LineOfBusiness inputLineOfBusiness, List<string> skipUpdateFields = null);
		void UpdateLineOfBusinessVoid(LineOfBusiness dbLineOfBusiness, LineOfBusiness inputLineOfBusiness, List<string> skipUpdateFields = null);
		void Remove(LineOfBusiness item);
	}
	public class LineOfBusinessRepo : CompanyBaseRepository<LineOfBusiness>, ILineOfBusinessRepo
	{
		public LineOfBusinessRepo(SmartAppDbContext context) : base(context) { }
		public LineOfBusinessRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public LineOfBusiness GetLineOfBusinessByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.LineOfBusinessGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<LineOfBusinessItemViewMap> GetDropDownQuery()
		{
			return (from lineOfBusiness in Entity
					select new LineOfBusinessItemViewMap
					{
						CompanyGUID = lineOfBusiness.CompanyGUID,
						Owner = lineOfBusiness.Owner,
						OwnerBusinessUnitGUID = lineOfBusiness.OwnerBusinessUnitGUID,
						LineOfBusinessGUID = lineOfBusiness.LineOfBusinessGUID,
						LineOfBusinessId = lineOfBusiness.LineOfBusinessId,
						Description = lineOfBusiness.Description
					});
		}
		public IEnumerable<SelectItem<LineOfBusinessItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<LineOfBusiness>(search, SysParm.CompanyGUID);
				var lineOfBusiness = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<LineOfBusinessItemViewMap, LineOfBusinessItemView>().ToDropDownItem(search.ExcludeRowData);


				return lineOfBusiness;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<LineOfBusinessListViewMap> GetListQuery()
		{
			return (from lineOfBusiness in Entity
				select new LineOfBusinessListViewMap
				{
						CompanyGUID = lineOfBusiness.CompanyGUID,
						Owner = lineOfBusiness.Owner,
						OwnerBusinessUnitGUID = lineOfBusiness.OwnerBusinessUnitGUID,
						LineOfBusinessGUID = lineOfBusiness.LineOfBusinessGUID,
						LineOfBusinessId = lineOfBusiness.LineOfBusinessId,
						Description = lineOfBusiness.Description
				});
		}
		public SearchResult<LineOfBusinessListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<LineOfBusinessListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<LineOfBusiness>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<LineOfBusinessListViewMap, LineOfBusinessListView>();
				result = list.SetSearchResult<LineOfBusinessListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<LineOfBusinessItemViewMap> GetItemQuery()
		{
			return (from lineOfBusiness in Entity
					join ownerBU in db.Set<BusinessUnit>()
					on lineOfBusiness.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljLineOfBusinessOwnerBU
					from ownerBU in ljLineOfBusinessOwnerBU.DefaultIfEmpty()
					join company in db.Set<Company>()
					on lineOfBusiness.CompanyGUID equals company.CompanyGUID into ljLineOfBusinessCompany
					from company in ljLineOfBusinessCompany.DefaultIfEmpty()
					select new LineOfBusinessItemViewMap
					{
						CompanyGUID = lineOfBusiness.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = lineOfBusiness.Owner,
						OwnerBusinessUnitGUID = lineOfBusiness.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = lineOfBusiness.CreatedBy,
						CreatedDateTime = lineOfBusiness.CreatedDateTime,
						ModifiedBy = lineOfBusiness.ModifiedBy,
						ModifiedDateTime = lineOfBusiness.ModifiedDateTime,
						LineOfBusinessGUID = lineOfBusiness.LineOfBusinessGUID,
						Description = lineOfBusiness.Description,
						LineOfBusinessId = lineOfBusiness.LineOfBusinessId,
					
						RowVersion = lineOfBusiness.RowVersion,
					});
		}
		public LineOfBusinessItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<LineOfBusinessItemViewMap, LineOfBusinessItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public LineOfBusiness CreateLineOfBusiness(LineOfBusiness lineOfBusiness)
		{
			try
			{
				lineOfBusiness.LineOfBusinessGUID = Guid.NewGuid();
				base.Add(lineOfBusiness);
				return lineOfBusiness;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateLineOfBusinessVoid(LineOfBusiness lineOfBusiness)
		{
			try
			{
				CreateLineOfBusiness(lineOfBusiness);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public LineOfBusiness UpdateLineOfBusiness(LineOfBusiness dbLineOfBusiness, LineOfBusiness inputLineOfBusiness, List<string> skipUpdateFields = null)
		{
			try
			{
				dbLineOfBusiness = dbLineOfBusiness.MapUpdateValues<LineOfBusiness>(inputLineOfBusiness);
				base.Update(dbLineOfBusiness);
				return dbLineOfBusiness;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateLineOfBusinessVoid(LineOfBusiness dbLineOfBusiness, LineOfBusiness inputLineOfBusiness, List<string> skipUpdateFields = null)
		{
			try
			{
				dbLineOfBusiness = dbLineOfBusiness.MapUpdateValues<LineOfBusiness>(inputLineOfBusiness, skipUpdateFields);
				base.Update(dbLineOfBusiness);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

