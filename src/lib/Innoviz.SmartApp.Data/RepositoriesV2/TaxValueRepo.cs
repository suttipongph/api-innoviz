using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ITaxValueRepo
	{
		#region DropDown
		IEnumerable<SelectItem<TaxValueItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<TaxValueListView> GetListvw(SearchParameter search);
		TaxValueItemView GetByIdvw(Guid id);
		TaxValue Find(params object[] keyValues);
		TaxValue GetTaxValueByIdNoTracking(Guid guid);
		TaxValue CreateTaxValue(TaxValue taxValue);
		void CreateTaxValueVoid(TaxValue taxValue);
		TaxValue UpdateTaxValue(TaxValue dbTaxValue, TaxValue inputTaxValue, List<string> skipUpdateFields = null);
		void UpdateTaxValueVoid(TaxValue dbTaxValue, TaxValue inputTaxValue, List<string> skipUpdateFields = null);
		void Remove(TaxValue item);
		TaxValue GetByTaxTableGUIDAndDateNoTracking(Guid taxTableGuid, DateTime asOfDate, bool isThrow = true);
	}
	public class TaxValueRepo : CompanyBaseRepository<TaxValue>, ITaxValueRepo
	{
		public TaxValueRepo(SmartAppDbContext context) : base(context) { }
		public TaxValueRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public TaxValue GetTaxValueByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.TaxValueGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<TaxValueItemViewMap> GetDropDownQuery()
		{
			return (from taxValue in Entity
					select new TaxValueItemViewMap
					{
						CompanyGUID = taxValue.CompanyGUID,
						Owner = taxValue.Owner,
						OwnerBusinessUnitGUID = taxValue.OwnerBusinessUnitGUID,
						TaxValueGUID = taxValue.TaxValueGUID
					});
		}
		public IEnumerable<SelectItem<TaxValueItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<TaxValue>(search, SysParm.CompanyGUID, DateTime.MinValue);
				var taxValue = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<TaxValueItemViewMap, TaxValueItemView>().ToDropDownItem(search.ExcludeRowData);


				return taxValue;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<TaxValueListViewMap> GetListQuery()
		{
			return (from taxValue in Entity
				select new TaxValueListViewMap
				{
						CompanyGUID = taxValue.CompanyGUID,
						Owner = taxValue.Owner,
						OwnerBusinessUnitGUID = taxValue.OwnerBusinessUnitGUID,
						TaxValueGUID = taxValue.TaxValueGUID,
						TaxTableGUID = taxValue.TaxTableGUID,
						Value = taxValue.Value,
						EffectiveFrom = taxValue.EffectiveFrom,
						EffectiveTo = taxValue.EffectiveTo
				});
		}
		public SearchResult<TaxValueListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<TaxValueListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<TaxValue>(search, SysParm.CompanyGUID, DateTime.MinValue);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<TaxValueListViewMap, TaxValueListView>();
				result = list.SetSearchResult<TaxValueListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<TaxValueItemViewMap> GetItemQuery()
		{
			return (from taxValue in Entity
					join company in db.Set<Company>()
					on taxValue.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on taxValue.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljTaxValueOwnerBU
					from ownerBU in ljTaxValueOwnerBU.DefaultIfEmpty()
					join taxTable in db.Set<TaxTable>() on taxValue.TaxTableGUID equals taxTable.TaxTableGUID
					select new TaxValueItemViewMap
					{
						CompanyGUID = taxValue.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = taxValue.Owner,
						OwnerBusinessUnitGUID = taxValue.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = taxValue.CreatedBy,
						CreatedDateTime = taxValue.CreatedDateTime,
						ModifiedBy = taxValue.ModifiedBy,
						ModifiedDateTime = taxValue.ModifiedDateTime,
						TaxValueGUID = taxValue.TaxValueGUID,
						EffectiveFrom = taxValue.EffectiveFrom,
						EffectiveTo = taxValue.EffectiveTo,
						TaxTableGUID = taxValue.TaxTableGUID,
						Value = taxValue.Value,
						TaxTable_Values = SmartAppUtil.GetDropDownLabel(taxTable.TaxCode, taxTable.Description),
					
						RowVersion = taxValue.RowVersion,
					});
		}
		public TaxValueItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<TaxValueItemViewMap, TaxValueItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public TaxValue CreateTaxValue(TaxValue taxValue)
		{
			try
			{
				taxValue.TaxValueGUID = Guid.NewGuid();
				base.Add(taxValue);
				return taxValue;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateTaxValueVoid(TaxValue taxValue)
		{
			try
			{
				CreateTaxValue(taxValue);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public TaxValue UpdateTaxValue(TaxValue dbTaxValue, TaxValue inputTaxValue, List<string> skipUpdateFields = null)
		{
			try
			{
				dbTaxValue = dbTaxValue.MapUpdateValues<TaxValue>(inputTaxValue);
				base.Update(dbTaxValue);
				return dbTaxValue;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateTaxValueVoid(TaxValue dbTaxValue, TaxValue inputTaxValue, List<string> skipUpdateFields = null)
		{
			try
			{
				dbTaxValue = dbTaxValue.MapUpdateValues<TaxValue>(inputTaxValue, skipUpdateFields);
				base.Update(dbTaxValue);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public DateTime? MaxEffectiveTo(Guid? taxTableGUID)
		{
			try
			{
				List<TaxValue> taxValues = Entity.Where(o => o.TaxTableGUID == taxTableGUID).AsNoTracking().ToList();
				if (taxValues.Count() == 0)
				{
					return null;
				}
				else if (taxValues.Any(o => o.EffectiveTo == null))
				{
					return null;
				}
				else
				{
					return taxValues.Max(o => o.EffectiveTo);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool BetweenEffectiveDate(TaxValue item)
		{
			try
			{
				DateTime? maxEffectiveTo = MaxEffectiveTo(item.TaxTableGUID);
				DateTime? DBeffectiveTo = (maxEffectiveTo != null) ? maxEffectiveTo : DateTime.MaxValue.AddDays(-1);

				DateTime? effectiveTo = (item.EffectiveTo != null) ? item.EffectiveTo : DateTime.MaxValue;

				if (!Entity.Any(o => o.TaxTableGUID == item.TaxTableGUID))
				{
					return false;
				}

				var notBetween = Entity.Any(o =>
								(item.EffectiveFrom >= o.EffectiveFrom && item.EffectiveFrom <= o.EffectiveTo && item.TaxValueGUID != o.TaxValueGUID &&
								o.TaxTableGUID == item.TaxTableGUID && o.CompanyGUID == item.CompanyGUID)

							|| (item.EffectiveFrom <= o.EffectiveFrom && item.EffectiveTo >= o.EffectiveTo && item.TaxValueGUID != o.TaxValueGUID &&
								o.TaxTableGUID == item.TaxTableGUID && o.CompanyGUID == item.CompanyGUID));

				if (notBetween)
				{
					return notBetween;
				}

				if (maxEffectiveTo == null && DBeffectiveTo != DateTime.MaxValue.AddDays(-1))
				{
					return notBetween;
				}

				var maxRow = Entity.Where(o => o.EffectiveTo == maxEffectiveTo &&
											   o.TaxTableGUID == item.TaxTableGUID &&
											   o.CompanyGUID == item.CompanyGUID).AsNoTracking().FirstOrDefault();

				if (item.TaxValueGUID == maxRow.TaxValueGUID)
				{
					return notBetween;
				}
				if (maxEffectiveTo == null && effectiveTo < DBeffectiveTo && effectiveTo >= maxRow.EffectiveFrom && item.TaxValueGUID != maxRow.TaxValueGUID)
				{
					return true;
				}

				var maxDB = Entity.Any(o => o.TaxTableGUID == item.TaxTableGUID &&
						o.CompanyGUID == item.CompanyGUID &&
						(effectiveTo >= DBeffectiveTo && item.EffectiveFrom <= DBeffectiveTo)
						&& item.TaxValueGUID != o.TaxValueGUID
					   );

				if (maxDB)
				{
					return maxDB;
				}
				var intervene = Entity.Any(o => item.EffectiveTo < o.EffectiveTo && item.EffectiveFrom < o.EffectiveFrom && item.EffectiveTo >= o.EffectiveFrom
											 && item.TaxValueGUID != o.TaxValueGUID && o.TaxTableGUID == item.TaxTableGUID && o.CompanyGUID == item.CompanyGUID);

				return intervene;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public override void ValidateAdd(TaxValue item)
		{
			base.ValidateAdd(item);
			DateTime? effectiveTo = (item.EffectiveTo != null) ? item.EffectiveTo : DateTime.MaxValue;

			if (!(item.Value >= 0))
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				ex.AddData("ERROR.00341");
				throw SmartAppUtil.AddStackTrace(ex);
			}

			if (!(item.EffectiveFrom <= effectiveTo))
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				ex.AddData("ERROR.00305");
				throw SmartAppUtil.AddStackTrace(ex);
			}

			if (BetweenEffectiveDate(item))
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				ex.AddData("ERROR.00651", new string[] { item.EffectiveFrom.DateToString(), effectiveTo.DateToString() });
				throw SmartAppUtil.AddStackTrace(ex);
			}

		}
		public override void ValidateUpdate(TaxValue item)
		{
			base.ValidateUpdate(item);
			DateTime? effectiveTo = (item.EffectiveTo != null) ? item.EffectiveTo : DateTime.MaxValue;

			if (!(item.Value >= 0))
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				ex.AddData("ERROR.00341");
				throw SmartAppUtil.AddStackTrace(ex);
			}

			if (!(item.EffectiveFrom <= effectiveTo))
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				ex.AddData("ERROR.00305");
				throw SmartAppUtil.AddStackTrace(ex);
			}

			if (BetweenEffectiveDate(item))
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				ex.AddData("ERROR.00651", item.EffectiveFrom.DateToString(), effectiveTo.DateToString());
				throw SmartAppUtil.AddStackTrace(ex);
			}

		}
		public TaxValue GetByTaxTableGUIDAndDateNoTracking(Guid taxTableGuid, DateTime asOfDate, bool isThrow = true)
		{
			try
			{
				TaxValue taxValue = new TaxValue();
				taxValue = Entity.Where(a => a.TaxTableGUID == taxTableGuid &&
											a.EffectiveFrom.Date <= asOfDate.Date &&
											(a.EffectiveTo >= asOfDate.Date || a.EffectiveTo == null))
											.AsNoTracking()
											.FirstOrDefault();

				if (taxValue == null && isThrow)
				{
					ITaxTableRepo taxTableRepo = new TaxTableRepo(db);
					TaxTable taxTable = taxTableRepo.GetTaxTableByIdNoTracking(taxTableGuid);
					SmartAppException ex = new SmartAppException("ERROR.ERROR");
					ex.AddData("ERROR.90007", new string[] { "LABEL.TAX_VALUE",SmartAppUtil.GetDropDownLabel(taxTable.TaxCode, taxTable.Description) });
					throw SmartAppUtil.AddStackTrace(ex);
				}
				return taxValue;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
	}
}

