using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IBusinessCollateralStatusRepo
	{
		#region DropDown
		IEnumerable<SelectItem<BusinessCollateralStatusItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<BusinessCollateralStatusListView> GetListvw(SearchParameter search);
		BusinessCollateralStatusItemView GetByIdvw(Guid id);
		BusinessCollateralStatus Find(params object[] keyValues);
		BusinessCollateralStatus GetBusinessCollateralStatusByIdNoTracking(Guid guid);
		BusinessCollateralStatus CreateBusinessCollateralStatus(BusinessCollateralStatus businessCollateralStatus);
		void CreateBusinessCollateralStatusVoid(BusinessCollateralStatus businessCollateralStatus);
		BusinessCollateralStatus UpdateBusinessCollateralStatus(BusinessCollateralStatus dbBusinessCollateralStatus, BusinessCollateralStatus inputBusinessCollateralStatus, List<string> skipUpdateFields = null);
		void UpdateBusinessCollateralStatusVoid(BusinessCollateralStatus dbBusinessCollateralStatus, BusinessCollateralStatus inputBusinessCollateralStatus, List<string> skipUpdateFields = null);
		void Remove(BusinessCollateralStatus item);
	}
	public class BusinessCollateralStatusRepo : CompanyBaseRepository<BusinessCollateralStatus>, IBusinessCollateralStatusRepo
	{
		public BusinessCollateralStatusRepo(SmartAppDbContext context) : base(context) { }
		public BusinessCollateralStatusRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BusinessCollateralStatus GetBusinessCollateralStatusByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BusinessCollateralStatusGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<BusinessCollateralStatusItemViewMap> GetDropDownQuery()
		{
			return (from businessCollateralStatus in Entity
					select new BusinessCollateralStatusItemViewMap
					{
						CompanyGUID = businessCollateralStatus.CompanyGUID,
						Owner = businessCollateralStatus.Owner,
						OwnerBusinessUnitGUID = businessCollateralStatus.OwnerBusinessUnitGUID,
						BusinessCollateralStatusGUID = businessCollateralStatus.BusinessCollateralStatusGUID,
						BusinessCollateralStatusId = businessCollateralStatus.BusinessCollateralStatusId,
						Description = businessCollateralStatus.Description
					});
			
		}
		public IEnumerable<SelectItem<BusinessCollateralStatusItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<BusinessCollateralStatus>(search, SysParm.CompanyGUID);
				var businessCollateralStatus = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<BusinessCollateralStatusItemViewMap, BusinessCollateralStatusItemView>().ToDropDownItem(search.ExcludeRowData);


				return businessCollateralStatus;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<BusinessCollateralStatusListViewMap> GetListQuery()
		{
			return (from businessCollateralStatus in Entity
				select new BusinessCollateralStatusListViewMap
				{
						CompanyGUID = businessCollateralStatus.CompanyGUID,
						Owner = businessCollateralStatus.Owner,
						OwnerBusinessUnitGUID = businessCollateralStatus.OwnerBusinessUnitGUID,
						BusinessCollateralStatusGUID = businessCollateralStatus.BusinessCollateralStatusGUID,
						BusinessCollateralStatusId = businessCollateralStatus.BusinessCollateralStatusId,
						Description = businessCollateralStatus.Description,
				});
		}
		public SearchResult<BusinessCollateralStatusListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<BusinessCollateralStatusListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<BusinessCollateralStatus>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<BusinessCollateralStatusListViewMap, BusinessCollateralStatusListView>();
				result = list.SetSearchResult<BusinessCollateralStatusListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<BusinessCollateralStatusItemViewMap> GetItemQuery()
		{
			return (from businessCollateralStatus in Entity
					join company in db.Set<Company>()
					on businessCollateralStatus.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on businessCollateralStatus.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljBusinessCollateralStatusOwnerBU
					from ownerBU in ljBusinessCollateralStatusOwnerBU.DefaultIfEmpty()
					select new BusinessCollateralStatusItemViewMap
					{
						CompanyGUID = businessCollateralStatus.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = businessCollateralStatus.Owner,
						OwnerBusinessUnitGUID = businessCollateralStatus.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = businessCollateralStatus.CreatedBy,
						CreatedDateTime = businessCollateralStatus.CreatedDateTime,
						ModifiedBy = businessCollateralStatus.ModifiedBy,
						ModifiedDateTime = businessCollateralStatus.ModifiedDateTime,
						BusinessCollateralStatusGUID = businessCollateralStatus.BusinessCollateralStatusGUID,
						BusinessCollateralStatusId = businessCollateralStatus.BusinessCollateralStatusId,
						Description = businessCollateralStatus.Description,
					
						RowVersion = businessCollateralStatus.RowVersion,
					});
		}
		public BusinessCollateralStatusItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<BusinessCollateralStatusItemViewMap, BusinessCollateralStatusItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public BusinessCollateralStatus CreateBusinessCollateralStatus(BusinessCollateralStatus businessCollateralStatus)
		{
			try
			{
				businessCollateralStatus.BusinessCollateralStatusGUID = Guid.NewGuid();
				base.Add(businessCollateralStatus);
				return businessCollateralStatus;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateBusinessCollateralStatusVoid(BusinessCollateralStatus businessCollateralStatus)
		{
			try
			{
				CreateBusinessCollateralStatus(businessCollateralStatus);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BusinessCollateralStatus UpdateBusinessCollateralStatus(BusinessCollateralStatus dbBusinessCollateralStatus, BusinessCollateralStatus inputBusinessCollateralStatus, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBusinessCollateralStatus = dbBusinessCollateralStatus.MapUpdateValues<BusinessCollateralStatus>(inputBusinessCollateralStatus);
				base.Update(dbBusinessCollateralStatus);
				return dbBusinessCollateralStatus;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateBusinessCollateralStatusVoid(BusinessCollateralStatus dbBusinessCollateralStatus, BusinessCollateralStatus inputBusinessCollateralStatus, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBusinessCollateralStatus = dbBusinessCollateralStatus.MapUpdateValues<BusinessCollateralStatus>(inputBusinessCollateralStatus, skipUpdateFields);
				base.Update(dbBusinessCollateralStatus);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

