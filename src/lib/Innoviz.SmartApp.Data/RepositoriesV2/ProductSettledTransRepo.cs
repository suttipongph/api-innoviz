using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface IProductSettledTransRepo
    {
        #region DropDown
        IEnumerable<SelectItem<ProductSettledTransItemView>> GetDropDownItem(SearchParameter search);
        #endregion DropDown
        SearchResult<ProductSettledTransListView> GetListvw(SearchParameter search);
        SearchResult<ProductSettledTransListView> GetListvwByWithdrawal(SearchParameter search);
        ProductSettledTransItemView GetByIdvw(Guid id);
        ProductSettledTrans Find(params object[] keyValues);
        ProductSettledTrans GetProductSettledTransByIdNoTracking(Guid guid);
        ProductSettledTrans CreateProductSettledTrans(ProductSettledTrans productSettledTrans);
        void CreateProductSettledTransVoid(ProductSettledTrans productSettledTrans);
        ProductSettledTrans UpdateProductSettledTrans(ProductSettledTrans dbProductSettledTrans, ProductSettledTrans inputProductSettledTrans, List<string> skipUpdateFields = null);
        void UpdateProductSettledTransVoid(ProductSettledTrans dbProductSettledTrans, ProductSettledTrans inputProductSettledTrans, List<string> skipUpdateFields = null);
        void Remove(ProductSettledTrans item);
        ProductSettledTrans GetProductSettledTransByIdNoTrackingByAccessLevel(Guid guid);
        IEnumerable<ProductSettledTrans> GetProductSettledTransByComapnyNoTracking(Guid guid);
        List<ProductSettledTrans> GetProductSettledTransForUpdatePurchaseWorkflowConditionK2(Guid companyGUID);
        #region shared
        IEnumerable<ProductSettledTrans> GetProductSettledTransGetPurchaseLineAmountBalance(Guid purchaseLineGuid, Guid statusGuid);
        ProductSettledTrans GetProductSettledTransByInvoiceSettlementDetailGuid(Guid invoiceSettlementDetailGuid);
        List<ProductSettledTrans> GetProductSettledTransByInvoiceSettlementDetailGuid(IEnumerable<Guid> invoiceSettlementDetailGuids);
        IEnumerable<ProductSettledTrans> GetProductSettledTransByOriginalRefGuid(Guid originalRefGuid);
        #endregion
        List<ProductSettledTrans> GetProductSettledTransByOriginalRefGuidAndReceiptTempStatus(IEnumerable<Guid> originalRefGuids, TemporaryReceiptStatus receiptTempStatus);
        void ValidateAdd(IEnumerable<ProductSettledTrans> items);
    }
    public class ProductSettledTransRepo : CompanyBaseRepository<ProductSettledTrans>, IProductSettledTransRepo
    {
        public ProductSettledTransRepo(SmartAppDbContext context) : base(context) { }
        public ProductSettledTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public ProductSettledTrans GetProductSettledTransByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.ProductSettledTransGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        private IQueryable<ProductSettledTransItemViewMap> GetDropDownQuery()
        {
            return (from productSettledTrans in Entity
                    select new ProductSettledTransItemViewMap
                    {
                        CompanyGUID = productSettledTrans.CompanyGUID,
                        Owner = productSettledTrans.Owner,
                        OwnerBusinessUnitGUID = productSettledTrans.OwnerBusinessUnitGUID,
                        ProductSettledTransGUID = productSettledTrans.ProductSettledTransGUID
                    });
        }
        public IEnumerable<SelectItem<ProductSettledTransItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<ProductSettledTrans>(search, SysParm.CompanyGUID);
                var productSettledTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<ProductSettledTransItemViewMap, ProductSettledTransItemView>().ToDropDownItem(search.ExcludeRowData);


                return productSettledTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #region GetListvw
        private IQueryable<ProductSettledTransListViewMap> GetListQuery()
        {
            var result= (from productSettledTrans in Entity
                    join invoiceSettlementDetail in db.Set<InvoiceSettlementDetail>()
                    on productSettledTrans.InvoiceSettlementDetailGUID equals invoiceSettlementDetail.InvoiceSettlementDetailGUID into ljinvoiceSettlementDetail
                    from invoiceSettlementDetail in ljinvoiceSettlementDetail.DefaultIfEmpty()
                    join invoiceTable in db.Set<InvoiceTable>()
                    on invoiceSettlementDetail.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljInvoiceTable
                    from invoiceTable in ljInvoiceTable.DefaultIfEmpty()
                    select new ProductSettledTransListViewMap
                    {
                        CompanyGUID = productSettledTrans.CompanyGUID,
                        Owner = productSettledTrans.Owner,
                        OwnerBusinessUnitGUID = productSettledTrans.OwnerBusinessUnitGUID,
                        ProductSettledTransGUID = productSettledTrans.ProductSettledTransGUID,
                        InvoiceSettlementDetailGUID = productSettledTrans.InvoiceSettlementDetailGUID,
                        DocumentId = productSettledTrans.DocumentId,
                        ProductType = productSettledTrans.ProductType,
                        InterestDate = productSettledTrans.InterestDate,
                        SettledDate = productSettledTrans.SettledDate,
                        InterestDay = productSettledTrans.InterestDay,
                        SettledPurchaseAmount = productSettledTrans.SettledPurchaseAmount,
                        SettledReserveAmount = productSettledTrans.SettledReserveAmount,
                        SettledInvoiceAmount = productSettledTrans.SettledInvoiceAmount,
                        InterestCalcAmount = productSettledTrans.InterestCalcAmount,
                        RefType = productSettledTrans.RefType,
                        OriginalRefGUID = productSettledTrans.OriginalRefGUID,
                        InvoiceSettlementDetail_Values = (invoiceTable != null) ? SmartAppUtil.GetDropDownLabel(invoiceTable.InvoiceId, invoiceTable.IssuedDate.DateToString()) : string.Empty,
                        InvoiceTable_InvoiceId = (invoiceTable != null) ? invoiceTable.InvoiceId : string.Empty,
                        
                    });
            return result;
        }
        public SearchResult<ProductSettledTransListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<ProductSettledTransListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<ProductSettledTrans>(search, SysParm.CompanyGUID);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<ProductSettledTransListViewMap, ProductSettledTransListView>();
                result = list.SetSearchResult<ProductSettledTransListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        private IQueryable<ProductSettledTransListViewMap> GetListQueryByWithdrawal()
        {
            var result = (from productSettledTrans in Entity

                          join invoiceSettlementDetail in db.Set<InvoiceSettlementDetail>()
                          on productSettledTrans.InvoiceSettlementDetailGUID equals invoiceSettlementDetail.InvoiceSettlementDetailGUID

                          join withdrawalLine in db.Set<WithdrawalLine>()
                          on productSettledTrans.OriginalRefGUID equals withdrawalLine.WithdrawalLineGUID

                          join withdrawalTable in db.Set<WithdrawalTable>()
                          on withdrawalLine.WithdrawalTableGUID equals withdrawalTable.WithdrawalTableGUID

                          join invoiceTable in db.Set<InvoiceTable>()
                          on invoiceSettlementDetail.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljInvoiceTable
                          from invoiceTable in ljInvoiceTable.DefaultIfEmpty()

                          select new ProductSettledTransListViewMap
                          {
                              CompanyGUID = productSettledTrans.CompanyGUID,
                              Owner = productSettledTrans.Owner,
                              OwnerBusinessUnitGUID = productSettledTrans.OwnerBusinessUnitGUID,
                              ProductSettledTransGUID = productSettledTrans.ProductSettledTransGUID,
                              InvoiceSettlementDetailGUID = productSettledTrans.InvoiceSettlementDetailGUID,
                              DocumentId = productSettledTrans.DocumentId,
                              ProductType = productSettledTrans.ProductType,
                              InterestDate = productSettledTrans.InterestDate,
                              SettledDate = productSettledTrans.SettledDate,
                              InterestDay = productSettledTrans.InterestDay,
                              SettledPurchaseAmount = productSettledTrans.SettledPurchaseAmount,
                              SettledReserveAmount = productSettledTrans.SettledReserveAmount,
                              SettledInvoiceAmount = productSettledTrans.SettledInvoiceAmount,
                              InterestCalcAmount = productSettledTrans.InterestCalcAmount,
                              RefType = productSettledTrans.RefType,
                              OriginalRefGUID = productSettledTrans.OriginalRefGUID,
                              InvoiceSettlementDetail_Values = (invoiceTable != null) ? SmartAppUtil.GetDropDownLabel(invoiceTable.InvoiceId, invoiceTable.IssuedDate.DateToString()) : string.Empty,
                              InvoiceTable_InvoiceId = (invoiceTable != null) ? invoiceTable.InvoiceId : string.Empty,

                              WithdrawalTable_WithdrawalTableGUID = withdrawalTable.WithdrawalTableGUID,
                          });
            return result;
        }
        public SearchResult<ProductSettledTransListView> GetListvwByWithdrawal(SearchParameter search)
        {
            var result = new SearchResult<ProductSettledTransListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<ProductSettledTrans>(search, SysParm.CompanyGUID);
                var total = GetListQueryByWithdrawal().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQueryByWithdrawal().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<ProductSettledTransListViewMap, ProductSettledTransListView>();
                result = list.SetSearchResult<ProductSettledTransListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<ProductSettledTransItemViewMap> GetItemQuery()
        {
            return (from productSettledTrans in Entity
                    join company in db.Set<Company>()
                    on productSettledTrans.CompanyGUID equals company.CompanyGUID
                    join ownerBU in db.Set<BusinessUnit>()
                    on productSettledTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljProductSettledTransOwnerBU
                    from ownerBU in ljProductSettledTransOwnerBU.DefaultIfEmpty()
                    join invoiceSettlementDetail in db.Set<InvoiceSettlementDetail>()
                    on productSettledTrans.InvoiceSettlementDetailGUID equals invoiceSettlementDetail.InvoiceSettlementDetailGUID
                    join invoiceTable in db.Set<InvoiceTable>()
                    on invoiceSettlementDetail.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljInvoiceTable
                    from invoiceTable in ljInvoiceTable.DefaultIfEmpty()
                    select new ProductSettledTransItemViewMap
                    {
                        CompanyGUID = productSettledTrans.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = productSettledTrans.Owner,
                        OwnerBusinessUnitGUID = productSettledTrans.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        CreatedBy = productSettledTrans.CreatedBy,
                        CreatedDateTime = productSettledTrans.CreatedDateTime,
                        ModifiedBy = productSettledTrans.ModifiedBy,
                        ModifiedDateTime = productSettledTrans.ModifiedDateTime,
                        ProductSettledTransGUID = productSettledTrans.ProductSettledTransGUID,
                        DocumentId = productSettledTrans.DocumentId,
                        InterestCalcAmount = productSettledTrans.InterestCalcAmount,
                        InterestDate = productSettledTrans.InterestDate,
                        InterestDay = productSettledTrans.InterestDay,
                        LastReceivedDate = productSettledTrans.LastReceivedDate,
                        InvoiceSettlementDetailGUID = productSettledTrans.InvoiceSettlementDetailGUID,
                        OriginalDocumentId = productSettledTrans.OriginalDocumentId,
                        OriginalInterestCalcAmount = productSettledTrans.OriginalInterestCalcAmount,
                        OriginalRefGUID = productSettledTrans.OriginalRefGUID,
                        PDCInterestOutstanding = productSettledTrans.PDCInterestOutstanding,
                        PostedInterestAmount = productSettledTrans.PostedInterestAmount,
                        ProductType = productSettledTrans.ProductType,
                        RefGUID = productSettledTrans.RefGUID,
                        RefType = productSettledTrans.RefType,
                        ReserveToBeRefund = productSettledTrans.ReserveToBeRefund,
                        SettledDate = productSettledTrans.SettledDate,
                        SettledInvoiceAmount = productSettledTrans.SettledInvoiceAmount,
                        SettledPurchaseAmount = productSettledTrans.SettledPurchaseAmount,
                        SettledReserveAmount = productSettledTrans.SettledReserveAmount,
                        TotalRefundAdditionalIntAmount = productSettledTrans.TotalRefundAdditionalIntAmount,
                        InvoiceSettlementDetail_Values = (invoiceTable != null) ? SmartAppUtil.GetDropDownLabel(invoiceTable.InvoiceId, invoiceTable.IssuedDate.DateToString()) : string.Empty,
                        RefId = (invoiceTable != null) ? SmartAppUtil.GetDropDownLabel(invoiceTable.InvoiceId, invoiceTable.IssuedDate.DateToString()) : string.Empty,
                    
                        RowVersion = productSettledTrans.RowVersion,
                    });
        }
        public ProductSettledTransItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<ProductSettledTransItemViewMap, ProductSettledTransItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public ProductSettledTrans CreateProductSettledTrans(ProductSettledTrans productSettledTrans)
        {
            try
            {
                productSettledTrans.ProductSettledTransGUID = Guid.NewGuid();
                base.Add(productSettledTrans);
                return productSettledTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateProductSettledTransVoid(ProductSettledTrans productSettledTrans)
        {
            try
            {
                CreateProductSettledTrans(productSettledTrans);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ProductSettledTrans UpdateProductSettledTrans(ProductSettledTrans dbProductSettledTrans, ProductSettledTrans inputProductSettledTrans, List<string> skipUpdateFields = null)
        {
            try
            {
                dbProductSettledTrans = dbProductSettledTrans.MapUpdateValues<ProductSettledTrans>(inputProductSettledTrans);
                base.Update(dbProductSettledTrans);
                return dbProductSettledTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateProductSettledTransVoid(ProductSettledTrans dbProductSettledTrans, ProductSettledTrans inputProductSettledTrans, List<string> skipUpdateFields = null)
        {
            try
            {
                dbProductSettledTrans = dbProductSettledTrans.MapUpdateValues<ProductSettledTrans>(inputProductSettledTrans, skipUpdateFields);
                base.Update(dbProductSettledTrans);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Create, Update, Delete
        public ProductSettledTrans GetProductSettledTransByIdNoTrackingByAccessLevel(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.ProductSettledTransGUID == guid)
                    .FilterByAccessLevel(SysParm.AccessLevel)
                    .AsNoTracking()
                    .FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<ProductSettledTrans> GetProductSettledTransByComapnyNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.CompanyGUID == guid)
                    .AsNoTracking();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<ProductSettledTrans> GetProductSettledTransForUpdatePurchaseWorkflowConditionK2(Guid companyGUID)
        {
            try
            {
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(Convert.ToInt32(TemporaryReceiptStatus.Posted).ToString());
                List<ProductSettledTrans> productSettledTrans = (from productSettledTran in Entity
                                                                        join Company in db.Set<Company>().Where(t => t.CompanyGUID == companyGUID) on productSettledTran.CompanyGUID equals Company.CompanyGUID
                                                                        join invoiceSettlementDetail in db.Set<InvoiceSettlementDetail>().Where(t => t.RefType == (int)RefType.ReceiptTemp)
                                                                        on productSettledTran.InvoiceSettlementDetailGUID equals invoiceSettlementDetail.InvoiceSettlementDetailGUID
                                                                        join receiptTempTable in db.Set<ReceiptTempTable>().Where(t => t.DocumentStatusGUID == documentStatus.DocumentStatusGUID)
                                                                        on invoiceSettlementDetail.RefGUID equals receiptTempTable.ReceiptTempTableGUID
                                                                        select productSettledTran)
                                                                        .AsNoTracking().ToList();
                return productSettledTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region shared
        public IEnumerable<ProductSettledTrans> GetProductSettledTransGetPurchaseLineAmountBalance(Guid purchaseLineGuid, Guid statusGuid)
        {
            try
            {
                IEnumerable<ProductSettledTrans> productSettledTrans = (from productSettledTran in Entity
                                                                        join invoiceSettlementDetail in db.Set<InvoiceSettlementDetail>()
                                                                        on productSettledTran.InvoiceSettlementDetailGUID equals invoiceSettlementDetail.InvoiceSettlementDetailGUID
                                                                        join receiptTempTable in db.Set<ReceiptTempTable>()
                                                                        on invoiceSettlementDetail.RefGUID equals receiptTempTable.ReceiptTempTableGUID
                                                                        where productSettledTran.RefGUID == purchaseLineGuid
                                                                        && receiptTempTable.DocumentStatusGUID == statusGuid
                                                                        select productSettledTran)
                                                                        .AsNoTracking().ToList();
                return productSettledTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public ProductSettledTrans GetProductSettledTransByInvoiceSettlementDetailGuid(Guid invoiceSettlementDetailGuid)
        {
            try
            {
                ProductSettledTrans productSettledTrans = Entity.Where(w => w.InvoiceSettlementDetailGUID == invoiceSettlementDetailGuid)
                    .AsNoTracking().FirstOrDefault();
                return productSettledTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<ProductSettledTrans> GetProductSettledTransByInvoiceSettlementDetailGuid(IEnumerable<Guid> invoiceSettlementDetailGuids)
        {
            try
            {
                var result = Entity.Where(w => invoiceSettlementDetailGuids.Contains(w.InvoiceSettlementDetailGUID))
                                    .AsNoTracking()
                                    .ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<ProductSettledTrans> GetProductSettledTransByOriginalRefGuid(Guid originalRefGuid)
        {
            try
            {
                IEnumerable<ProductSettledTrans> productSettledTrans = Entity.Where(w => w.OriginalRefGUID == originalRefGuid)
                    .AsNoTracking().ToList();
                return productSettledTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        public List<ProductSettledTrans> GetProductSettledTransByOriginalRefGuidAndReceiptTempStatus(IEnumerable<Guid> originalRefGuids, TemporaryReceiptStatus receiptTempStatus)
        {
            try
            {
                string receiptTempStatusId = ((int)receiptTempStatus).ToString();
                var result =
                    (from productSettledTrans in Entity.Where(w => originalRefGuids.Contains(w.OriginalRefGUID.Value))
                     join invoiceSettlementDetail in db.Set<InvoiceSettlementDetail>()
                     on productSettledTrans.InvoiceSettlementDetailGUID equals invoiceSettlementDetail.InvoiceSettlementDetailGUID
                     join receiptTempTable in db.Set<ReceiptTempTable>()
                     on invoiceSettlementDetail.RefGUID equals receiptTempTable.ReceiptTempTableGUID
                     join documentStatus in db.Set<DocumentStatus>().Where(w => w.StatusId == receiptTempStatusId)
                     on receiptTempTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
                     select productSettledTrans)
                     .AsNoTracking()
                     .ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Validate
        public override void ValidateAdd(IEnumerable<ProductSettledTrans> items)
        {
            base.ValidateAdd(items);
        }
        #endregion Validate
    }
}

