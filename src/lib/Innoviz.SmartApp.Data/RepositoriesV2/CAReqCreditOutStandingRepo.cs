using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICAReqCreditOutStandingRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CAReqCreditOutStandingItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<CAReqCreditOutStandingListView> GetListvw(SearchParameter search);
		CAReqCreditOutStandingItemView GetByIdvw(Guid id);
		CAReqCreditOutStanding Find(params object[] keyValues);
		CAReqCreditOutStanding GetCAReqCreditOutStandingByIdNoTracking(Guid guid);
		CAReqCreditOutStanding CreateCAReqCreditOutStanding(CAReqCreditOutStanding caReqCreditOutStanding);
		void CreateCAReqCreditOutStandingVoid(CAReqCreditOutStanding caReqCreditOutStanding);
		CAReqCreditOutStanding UpdateCAReqCreditOutStanding(CAReqCreditOutStanding dbCAReqCreditOutStanding, CAReqCreditOutStanding inputCAReqCreditOutStanding, List<string> skipUpdateFields = null);
		void UpdateCAReqCreditOutStandingVoid(CAReqCreditOutStanding dbCAReqCreditOutStanding, CAReqCreditOutStanding inputCAReqCreditOutStanding, List<string> skipUpdateFields = null);
		void Remove(CAReqCreditOutStanding item);
		CAReqCreditOutStanding GetCAReqCreditOutStandingByIdNoTrackingByAccessLevel(Guid guid);
	}
	public class CAReqCreditOutStandingRepo : CompanyBaseRepository<CAReqCreditOutStanding>, ICAReqCreditOutStandingRepo
	{
		public CAReqCreditOutStandingRepo(SmartAppDbContext context) : base(context) { }
		public CAReqCreditOutStandingRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CAReqCreditOutStanding GetCAReqCreditOutStandingByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CAReqCreditOutStandingGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CAReqCreditOutStandingItemViewMap> GetDropDownQuery()
		{
			return (from caReqCreditOutStanding in Entity
					select new CAReqCreditOutStandingItemViewMap
					{
						CompanyGUID = caReqCreditOutStanding.CompanyGUID,
						Owner = caReqCreditOutStanding.Owner,
						OwnerBusinessUnitGUID = caReqCreditOutStanding.OwnerBusinessUnitGUID,
						CAReqCreditOutStandingGUID = caReqCreditOutStanding.CAReqCreditOutStandingGUID,
						ProductType = caReqCreditOutStanding.ProductType,
						CreditLimitTypeGUID = caReqCreditOutStanding.CreditLimitTypeGUID
					});
		}
		public IEnumerable<SelectItem<CAReqCreditOutStandingItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CAReqCreditOutStanding>(search, SysParm.CompanyGUID);
				var caReqCreditOutStanding = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CAReqCreditOutStandingItemViewMap, CAReqCreditOutStandingItemView>().ToDropDownItem(search.ExcludeRowData);


				return caReqCreditOutStanding;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CAReqCreditOutStandingListViewMap> GetListQuery()
		{
			return (from caReqCreditOutStanding in Entity
					join credltLimitType in db.Set<CreditLimitType>() 
					on caReqCreditOutStanding.CreditLimitTypeGUID equals credltLimitType.CreditLimitTypeGUID into ljCreditLimitType
					from credltLimitType in ljCreditLimitType.DefaultIfEmpty()
					join creditAppTable in db.Set<CreditAppTable>()
					on caReqCreditOutStanding.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljCreditAppTable
					from creditAppTable in ljCreditAppTable.DefaultIfEmpty()
					join customerTable in db.Set<CustomerTable>()
					on caReqCreditOutStanding.CustomerTableGUID equals customerTable.CustomerTableGUID into ljCustomerTable
					from customerTable in ljCustomerTable.DefaultIfEmpty()
					select new CAReqCreditOutStandingListViewMap
				{
						CompanyGUID = caReqCreditOutStanding.CompanyGUID,
						Owner = caReqCreditOutStanding.Owner,
						OwnerBusinessUnitGUID = caReqCreditOutStanding.OwnerBusinessUnitGUID,
						CAReqCreditOutStandingGUID = caReqCreditOutStanding.CAReqCreditOutStandingGUID,
						ProductType = caReqCreditOutStanding.ProductType,
						ReserveToBeRefund = caReqCreditOutStanding.ReserveToBeRefund,
						CreditLimitTypeGUID = caReqCreditOutStanding.CreditLimitTypeGUID,
						CreditAppTableGUID = caReqCreditOutStanding.CreditAppTableGUID,
						CustomerTableGUID = caReqCreditOutStanding.CustomerTableGUID,
						AsOfDate = caReqCreditOutStanding.AsOfDate,
						ApprovedCreditLimit = caReqCreditOutStanding.ApprovedCreditLimit,
						CreditLimitBalance = caReqCreditOutStanding.CreditLimitBalance,
						ARBalance = caReqCreditOutStanding.ARBalance,
						AccumRetentionAmount = caReqCreditOutStanding.AccumRetentionAmount,
						CreditAppRequestTableGUID = caReqCreditOutStanding.CreditAppRequestTableGUID,
						CreditLimitType_Values = (credltLimitType != null) ? SmartAppUtil.GetDropDownLabel(credltLimitType.CreditLimitTypeId, credltLimitType.Description): string.Empty,
						CreditAppTable_Values = (creditAppTable != null) ? SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description) : string.Empty,
						CustomerTable_Values = (customerTable != null) ? SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name) : string.Empty,
					    CreditLimitType_CreditLimitTypeId = (credltLimitType != null) ? credltLimitType.CreditLimitTypeId : string.Empty,
						CreditAppTable_CreditAppId = (creditAppTable != null) ? creditAppTable.CreditAppId : string.Empty,
						CustomerTable_CustomerId = (customerTable != null) ? customerTable.CustomerId : string.Empty,
					});
		}
		public SearchResult<CAReqCreditOutStandingListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CAReqCreditOutStandingListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CAReqCreditOutStanding>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CAReqCreditOutStandingListViewMap, CAReqCreditOutStandingListView>();
				result = list.SetSearchResult<CAReqCreditOutStandingListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CAReqCreditOutStandingItemViewMap> GetItemQuery()
		{
			return (from caReqCreditOutStanding in Entity
					join company in db.Set<Company>()
					on caReqCreditOutStanding.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on caReqCreditOutStanding.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCAReqCreditOutStandingOwnerBU
					from ownerBU in ljCAReqCreditOutStandingOwnerBU.DefaultIfEmpty()
					select new CAReqCreditOutStandingItemViewMap
					{
						CompanyGUID = caReqCreditOutStanding.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = caReqCreditOutStanding.Owner,
						OwnerBusinessUnitGUID = caReqCreditOutStanding.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = caReqCreditOutStanding.CreatedBy,
						CreatedDateTime = caReqCreditOutStanding.CreatedDateTime,
						ModifiedBy = caReqCreditOutStanding.ModifiedBy,
						ModifiedDateTime = caReqCreditOutStanding.ModifiedDateTime,
						CAReqCreditOutStandingGUID = caReqCreditOutStanding.CAReqCreditOutStandingGUID,
						AccumRetentionAmount = caReqCreditOutStanding.AccumRetentionAmount,
						ApprovedCreditLimit = caReqCreditOutStanding.ApprovedCreditLimit,
						ARBalance = caReqCreditOutStanding.ARBalance,
						AsOfDate = caReqCreditOutStanding.AsOfDate,
						CreditAppRequestTableGUID = caReqCreditOutStanding.CreditAppRequestTableGUID,
						CreditAppTableGUID = caReqCreditOutStanding.CreditAppTableGUID,
						CreditLimitBalance = caReqCreditOutStanding.CreditLimitBalance,
						CreditLimitTypeGUID = caReqCreditOutStanding.CreditLimitTypeGUID,
						CustomerTableGUID = caReqCreditOutStanding.CustomerTableGUID,
						ProductType = caReqCreditOutStanding.ProductType,
						ReserveToBeRefund = caReqCreditOutStanding.ReserveToBeRefund,
					
						RowVersion = caReqCreditOutStanding.RowVersion,
					});
		}
		public CAReqCreditOutStandingItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<CAReqCreditOutStandingItemViewMap, CAReqCreditOutStandingItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public CAReqCreditOutStanding CreateCAReqCreditOutStanding(CAReqCreditOutStanding caReqCreditOutStanding)
		{
			try
			{
				caReqCreditOutStanding.CAReqCreditOutStandingGUID = Guid.NewGuid();
				base.Add(caReqCreditOutStanding);
				return caReqCreditOutStanding;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCAReqCreditOutStandingVoid(CAReqCreditOutStanding caReqCreditOutStanding)
		{
			try
			{
				CreateCAReqCreditOutStanding(caReqCreditOutStanding);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CAReqCreditOutStanding UpdateCAReqCreditOutStanding(CAReqCreditOutStanding dbCAReqCreditOutStanding, CAReqCreditOutStanding inputCAReqCreditOutStanding, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCAReqCreditOutStanding = dbCAReqCreditOutStanding.MapUpdateValues<CAReqCreditOutStanding>(inputCAReqCreditOutStanding);
				base.Update(dbCAReqCreditOutStanding);
				return dbCAReqCreditOutStanding;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCAReqCreditOutStandingVoid(CAReqCreditOutStanding dbCAReqCreditOutStanding, CAReqCreditOutStanding inputCAReqCreditOutStanding, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCAReqCreditOutStanding = dbCAReqCreditOutStanding.MapUpdateValues<CAReqCreditOutStanding>(inputCAReqCreditOutStanding, skipUpdateFields);
				base.Update(dbCAReqCreditOutStanding);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public CAReqCreditOutStanding GetCAReqCreditOutStandingByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CAReqCreditOutStandingGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

