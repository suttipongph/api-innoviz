using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IWithholdingTaxValueRepo
	{
		#region DropDown
		IEnumerable<SelectItem<WithholdingTaxValueItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<WithholdingTaxValueListView> GetListvw(SearchParameter search);
		WithholdingTaxValueItemView GetByIdvw(Guid id);
		WithholdingTaxValue Find(params object[] keyValues);
		WithholdingTaxValue GetWithholdingTaxValueByIdNoTracking(Guid guid);
		WithholdingTaxValue CreateWithholdingTaxValue(WithholdingTaxValue withholdingTaxValue);
		void CreateWithholdingTaxValueVoid(WithholdingTaxValue withholdingTaxValue);
		WithholdingTaxValue UpdateWithholdingTaxValue(WithholdingTaxValue dbWithholdingTaxValue, WithholdingTaxValue inputWithholdingTaxValue, List<string> skipUpdateFields = null);
		void UpdateWithholdingTaxValueVoid(WithholdingTaxValue dbWithholdingTaxValue, WithholdingTaxValue inputWithholdingTaxValue, List<string> skipUpdateFields = null);
		void Remove(WithholdingTaxValue item);
		#region shared
		WithholdingTaxValue GetByWHTTableGUIDAndDateNoTracking(Guid whtTableGuid, DateTime asOfDate, bool isThrow = true);
		#endregion
	}
    public class WithholdingTaxValueRepo : CompanyBaseRepository<WithholdingTaxValue>, IWithholdingTaxValueRepo
	{
		public WithholdingTaxValueRepo(SmartAppDbContext context) : base(context) { }
		public WithholdingTaxValueRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public WithholdingTaxValue GetWithholdingTaxValueByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.WithholdingTaxValueGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<WithholdingTaxValueItemViewMap> GetDropDownQuery()
		{
			return (from withholdingTaxValue in Entity
					select new WithholdingTaxValueItemViewMap
					{
						CompanyGUID = withholdingTaxValue.CompanyGUID,
						Owner = withholdingTaxValue.Owner,
						OwnerBusinessUnitGUID = withholdingTaxValue.OwnerBusinessUnitGUID,
						WithholdingTaxValueGUID = withholdingTaxValue.WithholdingTaxValueGUID
					});
		}
		public IEnumerable<SelectItem<WithholdingTaxValueItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<WithholdingTaxValue>(search, SysParm.CompanyGUID, DateTime.MinValue);
				var withholdingTaxValue = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<WithholdingTaxValueItemViewMap, WithholdingTaxValueItemView>().ToDropDownItem(search.ExcludeRowData);


				return withholdingTaxValue;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<WithholdingTaxValueListViewMap> GetListQuery()
		{
			return (from withholdingTaxValue in Entity
				select new WithholdingTaxValueListViewMap
				{
						CompanyGUID = withholdingTaxValue.CompanyGUID,
						Owner = withholdingTaxValue.Owner,
						OwnerBusinessUnitGUID = withholdingTaxValue.OwnerBusinessUnitGUID,
						WithholdingTaxValueGUID = withholdingTaxValue.WithholdingTaxValueGUID,
						Value = withholdingTaxValue.Value,
						EffectiveFrom = withholdingTaxValue.EffectiveFrom,
						EffectiveTo = withholdingTaxValue.EffectiveTo,
						WithholdingTaxTableGUID = withholdingTaxValue.WithholdingTaxTableGUID
				});
		}
		public SearchResult<WithholdingTaxValueListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<WithholdingTaxValueListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<WithholdingTaxValue>(search, SysParm.CompanyGUID, DateTime.MinValue);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<WithholdingTaxValueListViewMap, WithholdingTaxValueListView>();
				result = list.SetSearchResult<WithholdingTaxValueListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<WithholdingTaxValueItemViewMap> GetItemQuery()
		{
			return (from withholdingTaxValue in Entity
					join company in db.Set<Company>()
					on withholdingTaxValue.CompanyGUID equals company.CompanyGUID
					join withholdingTaxTable in db.Set<WithholdingTaxTable>()
					on withholdingTaxValue.WithholdingTaxTableGUID equals withholdingTaxTable.WithholdingTaxTableGUID
					join ownerBU in db.Set<BusinessUnit>()
					on withholdingTaxValue.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljWithholdingTaxValueOwnerBU
					from ownerBU in ljWithholdingTaxValueOwnerBU.DefaultIfEmpty()
					select new WithholdingTaxValueItemViewMap
					{
						CompanyGUID = withholdingTaxValue.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = withholdingTaxValue.Owner,
						OwnerBusinessUnitGUID = withholdingTaxValue.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = withholdingTaxValue.CreatedBy,
						CreatedDateTime = withholdingTaxValue.CreatedDateTime,
						ModifiedBy = withholdingTaxValue.ModifiedBy,
						ModifiedDateTime = withholdingTaxValue.ModifiedDateTime,
						WithholdingTaxValueGUID = withholdingTaxValue.WithholdingTaxValueGUID,
						EffectiveFrom = withholdingTaxValue.EffectiveFrom,
						EffectiveTo = withholdingTaxValue.EffectiveTo,
						Value = withholdingTaxValue.Value,
						WithholdingTaxTableGUID = withholdingTaxValue.WithholdingTaxTableGUID,
						WithholdingTaxTable_Values = SmartAppUtil.GetDropDownLabel(withholdingTaxTable.WHTCode, withholdingTaxTable.Description),
					
						RowVersion = withholdingTaxValue.RowVersion,
					});
		}
		public WithholdingTaxValueItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<WithholdingTaxValueItemViewMap, WithholdingTaxValueItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public WithholdingTaxValue CreateWithholdingTaxValue(WithholdingTaxValue withholdingTaxValue)
		{
			try
			{
				withholdingTaxValue.WithholdingTaxValueGUID = Guid.NewGuid();
				base.Add(withholdingTaxValue);
				return withholdingTaxValue;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateWithholdingTaxValueVoid(WithholdingTaxValue withholdingTaxValue)
		{
			try
			{
				CreateWithholdingTaxValue(withholdingTaxValue);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public WithholdingTaxValue UpdateWithholdingTaxValue(WithholdingTaxValue dbWithholdingTaxValue, WithholdingTaxValue inputWithholdingTaxValue, List<string> skipUpdateFields = null)
		{
			try
			{
				dbWithholdingTaxValue = dbWithholdingTaxValue.MapUpdateValues<WithholdingTaxValue>(inputWithholdingTaxValue);
				base.Update(dbWithholdingTaxValue);
				return dbWithholdingTaxValue;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateWithholdingTaxValueVoid(WithholdingTaxValue dbWithholdingTaxValue, WithholdingTaxValue inputWithholdingTaxValue, List<string> skipUpdateFields = null)
		{
			try
			{
				dbWithholdingTaxValue = dbWithholdingTaxValue.MapUpdateValues<WithholdingTaxValue>(inputWithholdingTaxValue, skipUpdateFields);
				base.Update(dbWithholdingTaxValue);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public DateTime? MaxEffectiveTo(Guid? withholdingTaxTableGUID)
		{
			try
			{
				List<WithholdingTaxValue> withholdingTaxValues = Entity.Where(o => o.WithholdingTaxTableGUID == withholdingTaxTableGUID).AsNoTracking().ToList();
				if (withholdingTaxValues.Count() == 0)
				{
					return null;
				}
				else if (withholdingTaxValues.Any(o => o.EffectiveTo == null))
				{
					return null;
				}
				else
				{
					return withholdingTaxValues.Max(o => o.EffectiveTo);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool BetweenEffectiveDate(WithholdingTaxValue item)
		{
			try
			{
				DateTime? maxEffectiveTo = MaxEffectiveTo(item.WithholdingTaxTableGUID);
				DateTime? DBeffectiveTo = (maxEffectiveTo != null) ? maxEffectiveTo : DateTime.MaxValue.AddDays(-1);

				DateTime? effectiveTo = (item.EffectiveTo != null) ? item.EffectiveTo : DateTime.MaxValue;

				if (!Entity.Any(o => o.WithholdingTaxTableGUID == item.WithholdingTaxTableGUID))
				{
					return false;
				}

				var notBetween = Entity.Any(o =>
								(item.EffectiveFrom >= o.EffectiveFrom && item.EffectiveFrom <= o.EffectiveTo && item.WithholdingTaxValueGUID != o.WithholdingTaxValueGUID &&
								o.WithholdingTaxTableGUID == item.WithholdingTaxTableGUID && o.CompanyGUID == item.CompanyGUID)

							|| (item.EffectiveFrom <= o.EffectiveFrom && item.EffectiveTo >= o.EffectiveTo && item.WithholdingTaxValueGUID != o.WithholdingTaxValueGUID &&
								o.WithholdingTaxTableGUID == item.WithholdingTaxTableGUID && o.CompanyGUID == item.CompanyGUID));

				if (notBetween)
				{
					return notBetween;
				}

				if (maxEffectiveTo == null && DBeffectiveTo != DateTime.MaxValue.AddDays(-1))
				{
					return notBetween;
				}

				var maxRow = Entity.Where(o => o.EffectiveTo == maxEffectiveTo &&
											   o.WithholdingTaxTableGUID == item.WithholdingTaxTableGUID &&
											   o.CompanyGUID == item.CompanyGUID).AsNoTracking().FirstOrDefault();

				if (item.WithholdingTaxValueGUID == maxRow.WithholdingTaxValueGUID)
				{
					return notBetween;
				}
				if (maxEffectiveTo == null && effectiveTo < DBeffectiveTo && effectiveTo >= maxRow.EffectiveFrom && item.WithholdingTaxValueGUID != maxRow.WithholdingTaxValueGUID)
				{
					return true;
				}

				var maxDB = Entity.Any(o => o.WithholdingTaxTableGUID == item.WithholdingTaxTableGUID &&
						o.CompanyGUID == item.CompanyGUID &&
						(effectiveTo >= DBeffectiveTo && item.EffectiveFrom <= DBeffectiveTo)
						&& item.WithholdingTaxValueGUID != o.WithholdingTaxValueGUID
					   );

				if (maxDB)
				{
					return maxDB;
				}
				var intervene = Entity.Any(o => item.EffectiveTo < o.EffectiveTo && item.EffectiveFrom < o.EffectiveFrom && item.EffectiveTo >= o.EffectiveFrom
											 && item.WithholdingTaxValueGUID != o.WithholdingTaxValueGUID && o.WithholdingTaxTableGUID == item.WithholdingTaxTableGUID && o.CompanyGUID == item.CompanyGUID);

				return intervene;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public override void ValidateAdd(WithholdingTaxValue item)
		{
			base.ValidateAdd(item);
			DateTime? effectiveTo = (item.EffectiveTo != null) ? item.EffectiveTo : DateTime.MaxValue;

			if (!(item.Value >= 0))
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				ex.AddData("ERROR.00341");
				throw SmartAppUtil.AddStackTrace(ex);
			}

			if (!(item.EffectiveFrom <= effectiveTo))
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				ex.AddData("ERROR.00305");
				throw SmartAppUtil.AddStackTrace(ex);
			}

			if (BetweenEffectiveDate(item))
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				ex.AddData("ERROR.00651", new string[] { item.EffectiveFrom.DateToString(), effectiveTo.DateToString() });
				throw SmartAppUtil.AddStackTrace(ex);
			}

		}
		public override void ValidateUpdate(WithholdingTaxValue item)
		{
			base.ValidateUpdate(item);
			DateTime? effectiveTo = (item.EffectiveTo != null) ? item.EffectiveTo : DateTime.MaxValue;

			if (!(item.Value >= 0))
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				ex.AddData("ERROR.00341");
				throw SmartAppUtil.AddStackTrace(ex);
			}

			if (!(item.EffectiveFrom <= effectiveTo))
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				ex.AddData("ERROR.00305");
				throw SmartAppUtil.AddStackTrace(ex);
			}

			if (BetweenEffectiveDate(item))
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				ex.AddData("ERROR.00651", item.EffectiveFrom.DateToString(), effectiveTo.DateToString());
				throw SmartAppUtil.AddStackTrace(ex);
			}

		}

		#region shared
		public WithholdingTaxValue GetByWHTTableGUIDAndDateNoTracking(Guid whtTableGuid, DateTime asOfDate, bool isThrow = true)
        {
            try
            {
				WithholdingTaxValue	whtValue = Entity.Where(w => w.WithholdingTaxTableGUID == whtTableGuid
													   && w.EffectiveFrom <= asOfDate
													   && (w.EffectiveTo >= asOfDate ||
															   w.EffectiveTo == null)).AsNoTracking().FirstOrDefault();

				if (whtValue == null && isThrow)
                {
					SmartAppException ex = new SmartAppException("ERROR.ERROR");
					IWithholdingTaxTableRepo withholdingTaxTableRepo = new WithholdingTaxTableRepo(db);
					WithholdingTaxTable withholdingTaxTable = withholdingTaxTableRepo.GetWithholdingTaxTableByIdNoTracking(whtTableGuid);
					ex.AddData("ERROR.90007", new string[] {"LABEL.WITHHOLDING_TAX_VALUE", withholdingTaxTable.WHTCode });
					throw SmartAppUtil.AddStackTrace(ex);
				}
				else
					return whtValue;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}

