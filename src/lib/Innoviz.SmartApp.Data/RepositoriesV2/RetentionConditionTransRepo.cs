using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IRetentionConditionTransRepo
	{
		#region DropDown
		IEnumerable<SelectItem<RetentionConditionTransItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<RetentionConditionTransListView> GetListvw(SearchParameter search);
		RetentionConditionTransItemView GetByIdvw(Guid id);
		RetentionConditionTrans Find(params object[] keyValues);
		RetentionConditionTrans GetRetentionConditionTransByIdNoTracking(Guid guid);
		RetentionConditionTrans CreateRetentionConditionTrans(RetentionConditionTrans retentionConditionTrans);
		void CreateRetentionConditionTransVoid(RetentionConditionTrans retentionConditionTrans);
		RetentionConditionTrans UpdateRetentionConditionTrans(RetentionConditionTrans dbRetentionConditionTrans, RetentionConditionTrans inputRetentionConditionTrans, List<string> skipUpdateFields = null);
		void UpdateRetentionConditionTransVoid(RetentionConditionTrans dbRetentionConditionTrans, RetentionConditionTrans inputRetentionConditionTrans, List<string> skipUpdateFields = null);
		void Remove(RetentionConditionTrans item);
		RetentionConditionTrans GetRetentionConditionTransByIdNoTrackingByAccessLevel(Guid guid);
		IEnumerable<RetentionConditionTrans> GetRetentionConditionTransByReference(Guid refId, int refType);
		void ValidateAdd(RetentionConditionTrans item);
		void ValidateAdd(IEnumerable<RetentionConditionTrans> items);
	}
	public class RetentionConditionTransRepo : CompanyBaseRepository<RetentionConditionTrans>, IRetentionConditionTransRepo
	{
		public RetentionConditionTransRepo(SmartAppDbContext context) : base(context) { }
		public RetentionConditionTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public RetentionConditionTrans GetRetentionConditionTransByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.RetentionConditionTransGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<RetentionConditionTransItemViewMap> GetDropDownQuery()
		{
			return (from retentionConditionTrans in Entity
					select new RetentionConditionTransItemViewMap
					{
						CompanyGUID = retentionConditionTrans.CompanyGUID,
						Owner = retentionConditionTrans.Owner,
						OwnerBusinessUnitGUID = retentionConditionTrans.OwnerBusinessUnitGUID,
						RetentionConditionTransGUID = retentionConditionTrans.RetentionConditionTransGUID,
						RetentionDeductionMethod = retentionConditionTrans.RetentionDeductionMethod,
						RetentionCalculateBase = retentionConditionTrans.RetentionCalculateBase
					});
		}
		public IEnumerable<SelectItem<RetentionConditionTransItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<RetentionConditionTrans>(search, SysParm.CompanyGUID);
				var retentionConditionTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<RetentionConditionTransItemViewMap, RetentionConditionTransItemView>().ToDropDownItem(search.ExcludeRowData);


				return retentionConditionTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<RetentionConditionTransListViewMap> GetListQuery()
		{
			return (from retentionConditionTrans in Entity
				select new RetentionConditionTransListViewMap
				{
						CompanyGUID = retentionConditionTrans.CompanyGUID,
						Owner = retentionConditionTrans.Owner,
						OwnerBusinessUnitGUID = retentionConditionTrans.OwnerBusinessUnitGUID,
						RetentionConditionTransGUID = retentionConditionTrans.RetentionConditionTransGUID,
						ProductType = retentionConditionTrans.ProductType,
						RetentionDeductionMethod = retentionConditionTrans.RetentionDeductionMethod,
						RetentionCalculateBase = retentionConditionTrans.RetentionCalculateBase,
						RetentionPct = retentionConditionTrans.RetentionPct,
						RetentionAmount = retentionConditionTrans.RetentionAmount,
						RefGUID = retentionConditionTrans.RefGUID,
				});
		}
		public SearchResult<RetentionConditionTransListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<RetentionConditionTransListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<RetentionConditionTrans>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<RetentionConditionTransListViewMap, RetentionConditionTransListView>();
				result = list.SetSearchResult<RetentionConditionTransListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<RetentionConditionTransItemViewMap> GetItemQuery()
		{
			return (from retentionConditionTrans in Entity
					join company in db.Set<Company>()
					on retentionConditionTrans.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on retentionConditionTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljRetentionConditionTransOwnerBU
					from ownerBU in ljRetentionConditionTransOwnerBU.DefaultIfEmpty()
					join creditAppRequestTable in db.Set<CreditAppRequestTable>()
					on retentionConditionTrans.RefGUID equals creditAppRequestTable.CreditAppRequestTableGUID into ljCreditAppRequestTable
					from creditAppRequestTable in ljCreditAppRequestTable.DefaultIfEmpty()
					join creditAppTable in db.Set<CreditAppTable>()
					on retentionConditionTrans.RefGUID equals creditAppTable.CreditAppTableGUID into ljCreditAppTable
					from creditAppTable in ljCreditAppTable.DefaultIfEmpty()
					select new RetentionConditionTransItemViewMap
					{
						CompanyGUID = retentionConditionTrans.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = retentionConditionTrans.Owner,
						OwnerBusinessUnitGUID = retentionConditionTrans.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = retentionConditionTrans.CreatedBy,
						CreatedDateTime = retentionConditionTrans.CreatedDateTime,
						ModifiedBy = retentionConditionTrans.ModifiedBy,
						ModifiedDateTime = retentionConditionTrans.ModifiedDateTime,
						RetentionConditionTransGUID = retentionConditionTrans.RetentionConditionTransGUID,
						ProductType = retentionConditionTrans.ProductType,
						RefGUID = retentionConditionTrans.RefGUID,
						RefType = retentionConditionTrans.RefType,
						RetentionAmount = retentionConditionTrans.RetentionAmount,
						RetentionCalculateBase = retentionConditionTrans.RetentionCalculateBase,
						RetentionDeductionMethod = retentionConditionTrans.RetentionDeductionMethod,
						RetentionPct = retentionConditionTrans.RetentionPct,
						RefId = (creditAppRequestTable != null && retentionConditionTrans.RefType == (int)RefType.CreditAppRequestTable) ? creditAppRequestTable.CreditAppRequestId :
								(creditAppTable != null && retentionConditionTrans.RefType == (int)RefType.CreditAppTable) ? creditAppTable.CreditAppId :
																																  null,
					
						RowVersion = retentionConditionTrans.RowVersion,
					});
		}
		public RetentionConditionTransItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<RetentionConditionTransItemViewMap, RetentionConditionTransItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public RetentionConditionTrans CreateRetentionConditionTrans(RetentionConditionTrans retentionConditionTrans)
		{
			try
			{
				retentionConditionTrans.RetentionConditionTransGUID = Guid.NewGuid();
				base.Add(retentionConditionTrans);
				return retentionConditionTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateRetentionConditionTransVoid(RetentionConditionTrans retentionConditionTrans)
		{
			try
			{
				CreateRetentionConditionTrans(retentionConditionTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public RetentionConditionTrans UpdateRetentionConditionTrans(RetentionConditionTrans dbRetentionConditionTrans, RetentionConditionTrans inputRetentionConditionTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbRetentionConditionTrans = dbRetentionConditionTrans.MapUpdateValues<RetentionConditionTrans>(inputRetentionConditionTrans);
				base.Update(dbRetentionConditionTrans);
				return dbRetentionConditionTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateRetentionConditionTransVoid(RetentionConditionTrans dbRetentionConditionTrans, RetentionConditionTrans inputRetentionConditionTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbRetentionConditionTrans = dbRetentionConditionTrans.MapUpdateValues<RetentionConditionTrans>(inputRetentionConditionTrans, skipUpdateFields);
				base.Update(dbRetentionConditionTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public RetentionConditionTrans GetRetentionConditionTransByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.RetentionConditionTransGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region GetRetentionConditionTransByReference
		public IEnumerable<RetentionConditionTrans> GetRetentionConditionTransByReference(Guid refId, int refType)
		{
			try
			{
				Guid companyGUID = SysParm.CompanyGUID.StringToGuid();
				return Entity.Where(t => t.CompanyGUID == companyGUID && t.RefGUID == refId && t.RefType == refType).AsNoTracking();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion
        public override void ValidateAdd(IEnumerable<RetentionConditionTrans> items)
        {
            base.ValidateAdd(items);
        }
        public override void ValidateAdd(RetentionConditionTrans item)
        {
            base.ValidateAdd(item);
        }
	}
}

