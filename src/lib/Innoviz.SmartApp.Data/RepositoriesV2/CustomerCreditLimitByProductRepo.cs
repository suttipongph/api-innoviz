using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICustomerCreditLimitByProductRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CustomerCreditLimitByProductItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<CustomerCreditLimitByProductListView> GetListvw(SearchParameter search);
		CustomerCreditLimitByProductItemView GetByIdvw(Guid id);
		CustomerCreditLimitByProduct Find(params object[] keyValues);
		CustomerCreditLimitByProduct GetCustomerCreditLimitByProductByIdNoTracking(Guid guid);
		CustomerCreditLimitByProduct CreateCustomerCreditLimitByProduct(CustomerCreditLimitByProduct customerCreditLimitByProduct);
		void CreateCustomerCreditLimitByProductVoid(CustomerCreditLimitByProduct customerCreditLimitByProduct);
		CustomerCreditLimitByProduct UpdateCustomerCreditLimitByProduct(CustomerCreditLimitByProduct dbCustomerCreditLimitByProduct, CustomerCreditLimitByProduct inputCustomerCreditLimitByProduct, List<string> skipUpdateFields = null);
		void UpdateCustomerCreditLimitByProductVoid(CustomerCreditLimitByProduct dbCustomerCreditLimitByProduct, CustomerCreditLimitByProduct inputCustomerCreditLimitByProduct, List<string> skipUpdateFields = null);
		void Remove(CustomerCreditLimitByProduct item);
		void ValidateAdd(CustomerCreditLimitByProduct item);
		void ValidateAdd(IEnumerable<CustomerCreditLimitByProduct> items);
	}
	public class CustomerCreditLimitByProductRepo : CompanyBaseRepository<CustomerCreditLimitByProduct>, ICustomerCreditLimitByProductRepo
	{
		public CustomerCreditLimitByProductRepo(SmartAppDbContext context) : base(context) { }
		public CustomerCreditLimitByProductRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CustomerCreditLimitByProduct GetCustomerCreditLimitByProductByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CustomerCreditLimitByProductGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CustomerCreditLimitByProductItemViewMap> GetDropDownQuery()
		{
			return (from customerCreditLimitByProduct in Entity
					select new CustomerCreditLimitByProductItemViewMap
					{
						CompanyGUID = customerCreditLimitByProduct.CompanyGUID,
						Owner = customerCreditLimitByProduct.Owner,
						OwnerBusinessUnitGUID = customerCreditLimitByProduct.OwnerBusinessUnitGUID,
						CustomerCreditLimitByProductGUID = customerCreditLimitByProduct.CustomerCreditLimitByProductGUID
					});
		}
		public IEnumerable<SelectItem<CustomerCreditLimitByProductItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CustomerCreditLimitByProduct>(search, SysParm.CompanyGUID);
				var customerCreditLimitByProduct = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CustomerCreditLimitByProductItemViewMap, CustomerCreditLimitByProductItemView>().ToDropDownItem(search.ExcludeRowData);


				return customerCreditLimitByProduct;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CustomerCreditLimitByProductListViewMap> GetListQuery()
		{
			return (from customerCreditLimitByProduct in Entity
				select new CustomerCreditLimitByProductListViewMap
				{
						CompanyGUID = customerCreditLimitByProduct.CompanyGUID,
						Owner = customerCreditLimitByProduct.Owner,
						OwnerBusinessUnitGUID = customerCreditLimitByProduct.OwnerBusinessUnitGUID,
						CustomerCreditLimitByProductGUID = customerCreditLimitByProduct.CustomerCreditLimitByProductGUID,
						ProductType = customerCreditLimitByProduct.ProductType,
						CreditLimit = customerCreditLimitByProduct.CreditLimit,
					    CustomerTableGUID = customerCreditLimitByProduct.CustomerTableGUID
				});
		}
		public SearchResult<CustomerCreditLimitByProductListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CustomerCreditLimitByProductListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CustomerCreditLimitByProduct>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CustomerCreditLimitByProductListViewMap, CustomerCreditLimitByProductListView>();
				result = list.SetSearchResult<CustomerCreditLimitByProductListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CustomerCreditLimitByProductItemViewMap> GetItemQuery()
		{
			return (from customerCreditLimitByProduct in Entity
					join company in db.Set<Company>()
					on customerCreditLimitByProduct.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on customerCreditLimitByProduct.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCustomerCreditLimitByProductOwnerBU
					from ownerBU in ljCustomerCreditLimitByProductOwnerBU.DefaultIfEmpty()
					join customerTable in db.Set<CustomerTable>() 
					on customerCreditLimitByProduct.CustomerTableGUID equals customerTable.CustomerTableGUID
					select new CustomerCreditLimitByProductItemViewMap
					{
						CompanyGUID = customerCreditLimitByProduct.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = customerCreditLimitByProduct.Owner,
						OwnerBusinessUnitGUID = customerCreditLimitByProduct.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = customerCreditLimitByProduct.CreatedBy,
						CreatedDateTime = customerCreditLimitByProduct.CreatedDateTime,
						ModifiedBy = customerCreditLimitByProduct.ModifiedBy,
						ModifiedDateTime = customerCreditLimitByProduct.ModifiedDateTime,
						CustomerCreditLimitByProductGUID = customerCreditLimitByProduct.CustomerCreditLimitByProductGUID,
						CreditLimit = customerCreditLimitByProduct.CreditLimit,
						CustomerTableGUID = customerCreditLimitByProduct.CustomerTableGUID,
						ProductType = customerCreditLimitByProduct.ProductType,
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
					
						RowVersion = customerCreditLimitByProduct.RowVersion,
					});
		}
		public CustomerCreditLimitByProductItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<CustomerCreditLimitByProductItemViewMap, CustomerCreditLimitByProductItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public CustomerCreditLimitByProduct CreateCustomerCreditLimitByProduct(CustomerCreditLimitByProduct customerCreditLimitByProduct)
		{
			try
			{
				customerCreditLimitByProduct.CustomerCreditLimitByProductGUID = Guid.NewGuid();
				base.Add(customerCreditLimitByProduct);
				return customerCreditLimitByProduct;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCustomerCreditLimitByProductVoid(CustomerCreditLimitByProduct customerCreditLimitByProduct)
		{
			try
			{
				CreateCustomerCreditLimitByProduct(customerCreditLimitByProduct);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CustomerCreditLimitByProduct UpdateCustomerCreditLimitByProduct(CustomerCreditLimitByProduct dbCustomerCreditLimitByProduct, CustomerCreditLimitByProduct inputCustomerCreditLimitByProduct, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCustomerCreditLimitByProduct = dbCustomerCreditLimitByProduct.MapUpdateValues<CustomerCreditLimitByProduct>(inputCustomerCreditLimitByProduct);
				base.Update(dbCustomerCreditLimitByProduct);
				return dbCustomerCreditLimitByProduct;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCustomerCreditLimitByProductVoid(CustomerCreditLimitByProduct dbCustomerCreditLimitByProduct, CustomerCreditLimitByProduct inputCustomerCreditLimitByProduct, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCustomerCreditLimitByProduct = dbCustomerCreditLimitByProduct.MapUpdateValues<CustomerCreditLimitByProduct>(inputCustomerCreditLimitByProduct, skipUpdateFields);
				base.Update(dbCustomerCreditLimitByProduct);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

