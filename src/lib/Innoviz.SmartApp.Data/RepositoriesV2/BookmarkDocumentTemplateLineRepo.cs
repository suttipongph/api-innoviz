using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IBookmarkDocumentTemplateLineRepo
	{
		#region DropDown
		IEnumerable<SelectItem<BookmarkDocumentTemplateLineItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<BookmarkDocumentTemplateLineListView> GetListvw(SearchParameter search);
		BookmarkDocumentTemplateLineItemView GetByIdvw(Guid id);
		BookmarkDocumentTemplateLine Find(params object[] keyValues);
		BookmarkDocumentTemplateLine GetBookmarkDocumentTemplateLineByIdNoTracking(Guid guid);
		BookmarkDocumentTemplateLine CreateBookmarkDocumentTemplateLine(BookmarkDocumentTemplateLine bookmarkDocumentTemplateLine);
		void CreateBookmarkDocumentTemplateLineVoid(BookmarkDocumentTemplateLine bookmarkDocumentTemplateLine);
		BookmarkDocumentTemplateLine UpdateBookmarkDocumentTemplateLine(BookmarkDocumentTemplateLine dbBookmarkDocumentTemplateLine, BookmarkDocumentTemplateLine inputBookmarkDocumentTemplateLine, List<string> skipUpdateFields = null);
		void UpdateBookmarkDocumentTemplateLineVoid(BookmarkDocumentTemplateLine dbBookmarkDocumentTemplateLine, BookmarkDocumentTemplateLine inputBookmarkDocumentTemplateLine, List<string> skipUpdateFields = null);
		void Remove(BookmarkDocumentTemplateLine item);
		BookmarkDocumentTemplateLine GetBookmarkDocumentTemplateLineByIdNoTrackingByAccessLevel(Guid guid);
		List<BookmarkDocumentTemplateLine> GetDocumentTemplateLineByBookmarkDocumentTemplateTable(Guid guid);
	}
	public class BookmarkDocumentTemplateLineRepo : CompanyBaseRepository<BookmarkDocumentTemplateLine>, IBookmarkDocumentTemplateLineRepo
	{
		public BookmarkDocumentTemplateLineRepo(SmartAppDbContext context) : base(context) { }
		public BookmarkDocumentTemplateLineRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BookmarkDocumentTemplateLine GetBookmarkDocumentTemplateLineByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BookmarkDocumentTemplateLineGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<BookmarkDocumentTemplateLineItemViewMap> GetDropDownQuery()
		{
			return (from bookmarkDocumentTemplateLine in Entity
					select new BookmarkDocumentTemplateLineItemViewMap
					{
						CompanyGUID = bookmarkDocumentTemplateLine.CompanyGUID,
						Owner = bookmarkDocumentTemplateLine.Owner,
						OwnerBusinessUnitGUID = bookmarkDocumentTemplateLine.OwnerBusinessUnitGUID,
						BookmarkDocumentTemplateLineGUID = bookmarkDocumentTemplateLine.BookmarkDocumentTemplateLineGUID
					});
		}
		public IEnumerable<SelectItem<BookmarkDocumentTemplateLineItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<BookmarkDocumentTemplateLine>(search, SysParm.CompanyGUID);
				var bookmarkDocumentTemplateLine = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<BookmarkDocumentTemplateLineItemViewMap, BookmarkDocumentTemplateLineItemView>().ToDropDownItem(search.ExcludeRowData);


				return bookmarkDocumentTemplateLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<BookmarkDocumentTemplateLineListViewMap> GetListQuery()
		{
			return (from bookmarkDocumentTemplateLine in Entity
					join bookmarkDocument in db.Set<BookmarkDocument>()
					on bookmarkDocumentTemplateLine.BookmarkDocumentGUID equals bookmarkDocument.BookmarkDocumentGUID into ljBookmarkDocument
					from bookmarkDocument in ljBookmarkDocument.DefaultIfEmpty()
					join documentTemplate in db.Set<DocumentTemplateTable>()
					on bookmarkDocumentTemplateLine.DocumentTemplateTableGUID equals documentTemplate.DocumentTemplateTableGUID into ljDocumentTemplate
					from documentTemplate in ljDocumentTemplate.DefaultIfEmpty()
					select new BookmarkDocumentTemplateLineListViewMap
				{
						CompanyGUID = bookmarkDocumentTemplateLine.CompanyGUID,
						Owner = bookmarkDocumentTemplateLine.Owner,
						OwnerBusinessUnitGUID = bookmarkDocumentTemplateLine.OwnerBusinessUnitGUID,
						BookmarkDocumentTemplateLineGUID = bookmarkDocumentTemplateLine.BookmarkDocumentTemplateLineGUID,
						BookmarkDocumentGUID = bookmarkDocumentTemplateLine.BookmarkDocumentGUID,
						DocumentTemplateTableGUID = bookmarkDocumentTemplateLine.DocumentTemplateTableGUID,
						BookmarkDocumentId = bookmarkDocument.BookmarkDocumentId,
						TemplateId = documentTemplate.TemplateId,
						BookmarkDocument_Values = SmartAppUtil.GetDropDownLabel(bookmarkDocument.BookmarkDocumentId,bookmarkDocument.Description),
						DocumentTemplateTable_Values = SmartAppUtil.GetDropDownLabel(documentTemplate.TemplateId, documentTemplate.Description),
						BookmarkDocumentTemplateTableGUID = bookmarkDocumentTemplateLine.BookmarkDocumentTemplateTableGUID,
						DocumentTemplateType = documentTemplate.DocumentTemplateType
				});
		}
		public SearchResult<BookmarkDocumentTemplateLineListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<BookmarkDocumentTemplateLineListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<BookmarkDocumentTemplateLine>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<BookmarkDocumentTemplateLineListViewMap, BookmarkDocumentTemplateLineListView>();
				result = list.SetSearchResult<BookmarkDocumentTemplateLineListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<BookmarkDocumentTemplateLineItemViewMap> GetItemQuery()
		{
			return (from bookmarkDocumentTemplateLine in Entity
					join company in db.Set<Company>()
					on bookmarkDocumentTemplateLine.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on bookmarkDocumentTemplateLine.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljBookmarkDocumentTemplateLineOwnerBU
					from ownerBU in ljBookmarkDocumentTemplateLineOwnerBU.DefaultIfEmpty()
					join documentTemplate in db.Set<DocumentTemplateTable>()
					on bookmarkDocumentTemplateLine.DocumentTemplateTableGUID equals documentTemplate.DocumentTemplateTableGUID into ljDocumentTemplate
					from documentTemplate in ljDocumentTemplate.DefaultIfEmpty()
					select new BookmarkDocumentTemplateLineItemViewMap
					{
						CompanyGUID = bookmarkDocumentTemplateLine.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = bookmarkDocumentTemplateLine.Owner,
						OwnerBusinessUnitGUID = bookmarkDocumentTemplateLine.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = bookmarkDocumentTemplateLine.CreatedBy,
						CreatedDateTime = bookmarkDocumentTemplateLine.CreatedDateTime,
						ModifiedBy = bookmarkDocumentTemplateLine.ModifiedBy,
						ModifiedDateTime = bookmarkDocumentTemplateLine.ModifiedDateTime,
						BookmarkDocumentTemplateLineGUID = bookmarkDocumentTemplateLine.BookmarkDocumentTemplateLineGUID,
						BookmarkDocumentGUID = bookmarkDocumentTemplateLine.BookmarkDocumentGUID,
						BookmarkDocumentTemplateTableGUID = bookmarkDocumentTemplateLine.BookmarkDocumentTemplateTableGUID,
						DocumentTemplateTableGUID = bookmarkDocumentTemplateLine.DocumentTemplateTableGUID,
						DocumentTemplateType = documentTemplate.DocumentTemplateType,
					
						RowVersion = bookmarkDocumentTemplateLine.RowVersion,
					});
		}
		public BookmarkDocumentTemplateLineItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<BookmarkDocumentTemplateLineItemViewMap, BookmarkDocumentTemplateLineItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public BookmarkDocumentTemplateLine CreateBookmarkDocumentTemplateLine(BookmarkDocumentTemplateLine bookmarkDocumentTemplateLine)
		{
			try
			{
				bookmarkDocumentTemplateLine.BookmarkDocumentTemplateLineGUID = Guid.NewGuid();
				base.Add(bookmarkDocumentTemplateLine);
				return bookmarkDocumentTemplateLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateBookmarkDocumentTemplateLineVoid(BookmarkDocumentTemplateLine bookmarkDocumentTemplateLine)
		{
			try
			{
				CreateBookmarkDocumentTemplateLine(bookmarkDocumentTemplateLine);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BookmarkDocumentTemplateLine UpdateBookmarkDocumentTemplateLine(BookmarkDocumentTemplateLine dbBookmarkDocumentTemplateLine, BookmarkDocumentTemplateLine inputBookmarkDocumentTemplateLine, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBookmarkDocumentTemplateLine = dbBookmarkDocumentTemplateLine.MapUpdateValues<BookmarkDocumentTemplateLine>(inputBookmarkDocumentTemplateLine);
				base.Update(dbBookmarkDocumentTemplateLine);
				return dbBookmarkDocumentTemplateLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateBookmarkDocumentTemplateLineVoid(BookmarkDocumentTemplateLine dbBookmarkDocumentTemplateLine, BookmarkDocumentTemplateLine inputBookmarkDocumentTemplateLine, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBookmarkDocumentTemplateLine = dbBookmarkDocumentTemplateLine.MapUpdateValues<BookmarkDocumentTemplateLine>(inputBookmarkDocumentTemplateLine, skipUpdateFields);
				base.Update(dbBookmarkDocumentTemplateLine);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		
		public BookmarkDocumentTemplateLine GetBookmarkDocumentTemplateLineByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BookmarkDocumentTemplateLineGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        public List<BookmarkDocumentTemplateLine> GetDocumentTemplateLineByBookmarkDocumentTemplateTable(Guid guid)
        {
            try
            {
				var result = Entity.Where(item =>item.BookmarkDocumentTemplateTableGUID == guid)
					.ToList();
				return result;

			}
			catch (Exception e)
            {
				throw SmartAppUtil.AddStackTrace(e);
			}
        }
    }
}

