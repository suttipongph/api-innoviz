using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IKYCSetupRepo
	{
		#region DropDown
		IEnumerable<SelectItem<KYCSetupItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<KYCSetupListView> GetListvw(SearchParameter search);
		KYCSetupItemView GetByIdvw(Guid id);
		KYCSetup Find(params object[] keyValues);
		KYCSetup GetKYCSetupByIdNoTracking(Guid guid);
		KYCSetup CreateKYCSetup(KYCSetup kycSetup);
		void CreateKYCSetupVoid(KYCSetup kycSetup);
		KYCSetup UpdateKYCSetup(KYCSetup dbKYCSetup, KYCSetup inputKYCSetup, List<string> skipUpdateFields = null);
		void UpdateKYCSetupVoid(KYCSetup dbKYCSetup, KYCSetup inputKYCSetup, List<string> skipUpdateFields = null);
		void Remove(KYCSetup item);
	}
	public class KYCSetupRepo : CompanyBaseRepository<KYCSetup>, IKYCSetupRepo
	{
		public KYCSetupRepo(SmartAppDbContext context) : base(context) { }
		public KYCSetupRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public KYCSetup GetKYCSetupByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.KYCSetupGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<KYCSetupItemViewMap> GetDropDownQuery()
		{
			return (from kycSetup in Entity
					select new KYCSetupItemViewMap
					{
						CompanyGUID = kycSetup.CompanyGUID,
						Owner = kycSetup.Owner,
						OwnerBusinessUnitGUID = kycSetup.OwnerBusinessUnitGUID,
						KYCSetupGUID = kycSetup.KYCSetupGUID,
						KYCId = kycSetup.KYCId,
						Description = kycSetup.Description
					});
		}
		public IEnumerable<SelectItem<KYCSetupItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<KYCSetup>(search, SysParm.CompanyGUID);
				var kycSetup = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<KYCSetupItemViewMap, KYCSetupItemView>().ToDropDownItem(search.ExcludeRowData);


				return kycSetup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<KYCSetupListViewMap> GetListQuery()
		{
			return (from kycSetup in Entity
				select new KYCSetupListViewMap
				{
						CompanyGUID = kycSetup.CompanyGUID,
						Owner = kycSetup.Owner,
						OwnerBusinessUnitGUID = kycSetup.OwnerBusinessUnitGUID,
						KYCSetupGUID = kycSetup.KYCSetupGUID,
						KYCId = kycSetup.KYCId,
						Description = kycSetup.Description,
				});
		}
		public SearchResult<KYCSetupListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<KYCSetupListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<KYCSetup>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<KYCSetupListViewMap, KYCSetupListView>();
				result = list.SetSearchResult<KYCSetupListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<KYCSetupItemViewMap> GetItemQuery()
		{
			return (from kycSetup in Entity
					join ownerBU in db.Set<BusinessUnit>()
					on kycSetup.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljKYCSetupOwnerBU
					from ownerBU in ljKYCSetupOwnerBU.DefaultIfEmpty()
					join company in db.Set<Company>() on kycSetup.CompanyGUID equals company.CompanyGUID
					select new KYCSetupItemViewMap
					{
						CompanyGUID = kycSetup.CompanyGUID,
						Owner = kycSetup.Owner,
						OwnerBusinessUnitGUID = kycSetup.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CompanyId = company.CompanyId,
						CreatedBy = kycSetup.CreatedBy,
						CreatedDateTime = kycSetup.CreatedDateTime,
						ModifiedBy = kycSetup.ModifiedBy,
						ModifiedDateTime = kycSetup.ModifiedDateTime,
						KYCSetupGUID = kycSetup.KYCSetupGUID,
						Description = kycSetup.Description,
						KYCId = kycSetup.KYCId,
					
						RowVersion = kycSetup.RowVersion,
					});
		}
		public KYCSetupItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<KYCSetupItemViewMap, KYCSetupItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public KYCSetup CreateKYCSetup(KYCSetup kycSetup)
		{
			try
			{
				kycSetup.KYCSetupGUID = Guid.NewGuid();
				base.Add(kycSetup);
				return kycSetup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateKYCSetupVoid(KYCSetup kycSetup)
		{
			try
			{
				CreateKYCSetup(kycSetup);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public KYCSetup UpdateKYCSetup(KYCSetup dbKYCSetup, KYCSetup inputKYCSetup, List<string> skipUpdateFields = null)
		{
			try
			{
				dbKYCSetup = dbKYCSetup.MapUpdateValues<KYCSetup>(inputKYCSetup);
				base.Update(dbKYCSetup);
				return dbKYCSetup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateKYCSetupVoid(KYCSetup dbKYCSetup, KYCSetup inputKYCSetup, List<string> skipUpdateFields = null)
		{
			try
			{
				dbKYCSetup = dbKYCSetup.MapUpdateValues<KYCSetup>(inputKYCSetup, skipUpdateFields);
				base.Update(dbKYCSetup);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

