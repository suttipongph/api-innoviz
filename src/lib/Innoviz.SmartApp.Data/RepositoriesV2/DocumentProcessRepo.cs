using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IDocumentProcessRepo
	{
		#region DropDown
		IEnumerable<SelectItem<DocumentProcessItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<DocumentProcessListView> GetListvw(SearchParameter search);
		DocumentProcessItemView GetByIdvw(Guid id);
		DocumentProcess Find(params object[] keyValues);
		DocumentProcess GetDocumentProcessByIdNoTracking(Guid guid);
		DocumentProcess CreateDocumentProcess(DocumentProcess documentProcess);
		void CreateDocumentProcessVoid(DocumentProcess documentProcess);
		DocumentProcess UpdateDocumentProcess(DocumentProcess dbDocumentProcess, DocumentProcess inputDocumentProcess, List<string> skipUpdateFields = null);
		void UpdateDocumentProcessVoid(DocumentProcess dbDocumentProcess, DocumentProcess inputDocumentProcess, List<string> skipUpdateFields = null);
		void Remove(DocumentProcess item);
	}
	public class DocumentProcessRepo : CompanyBaseRepository<DocumentProcess>, IDocumentProcessRepo
	{
		public DocumentProcessRepo(SmartAppDbContext context) : base(context) { }
		public DocumentProcessRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public DocumentProcess GetDocumentProcessByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.DocumentProcessGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<DocumentProcessItemViewMap> GetDropDownQuery()
		{
			return (from documentProcess in Entity
					select new DocumentProcessItemViewMap
					{
						DocumentProcessGUID = documentProcess.DocumentProcessGUID,
						Description = documentProcess.Description
					});
		}
		public IEnumerable<SelectItem<DocumentProcessItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<DocumentProcess>(search);
				var documentProcess = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<DocumentProcessItemViewMap, DocumentProcessItemView>().ToDropDownItem(search.ExcludeRowData);


				return documentProcess;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<DocumentProcessListViewMap> GetListQuery()
		{
			return (from documentProcess in Entity
				select new DocumentProcessListViewMap
				{
						DocumentProcessGUID = documentProcess.DocumentProcessGUID,
						ProcessId = documentProcess.ProcessId,
						Description = documentProcess.Description,
				});
		}
		public SearchResult<DocumentProcessListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<DocumentProcessListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<DocumentProcess>(search);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<DocumentProcessListViewMap, DocumentProcessListView>();
				result = list.SetSearchResult<DocumentProcessListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<DocumentProcessItemViewMap> GetItemQuery()
		{
			return (from documentProcess in Entity
					select new DocumentProcessItemViewMap
					{
						CreatedBy = documentProcess.CreatedBy,
						CreatedDateTime = documentProcess.CreatedDateTime,
						ModifiedBy = documentProcess.ModifiedBy,
						ModifiedDateTime = documentProcess.ModifiedDateTime,
						DocumentProcessGUID = documentProcess.DocumentProcessGUID,
						Description = documentProcess.Description,
						ProcessId = documentProcess.ProcessId,
					
						RowVersion = documentProcess.RowVersion,
					});
		}
		public DocumentProcessItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<DocumentProcessItemViewMap, DocumentProcessItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public DocumentProcess CreateDocumentProcess(DocumentProcess documentProcess)
		{
			try
			{
				documentProcess.DocumentProcessGUID = Guid.NewGuid();
				base.Add(documentProcess);
				return documentProcess;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateDocumentProcessVoid(DocumentProcess documentProcess)
		{
			try
			{
				CreateDocumentProcess(documentProcess);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentProcess UpdateDocumentProcess(DocumentProcess dbDocumentProcess, DocumentProcess inputDocumentProcess, List<string> skipUpdateFields = null)
		{
			try
			{
				dbDocumentProcess = dbDocumentProcess.MapUpdateValues<DocumentProcess>(inputDocumentProcess);
				base.Update(dbDocumentProcess);
				return dbDocumentProcess;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateDocumentProcessVoid(DocumentProcess dbDocumentProcess, DocumentProcess inputDocumentProcess, List<string> skipUpdateFields = null)
		{
			try
			{
				dbDocumentProcess = dbDocumentProcess.MapUpdateValues<DocumentProcess>(inputDocumentProcess, skipUpdateFields);
				base.Update(dbDocumentProcess);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

