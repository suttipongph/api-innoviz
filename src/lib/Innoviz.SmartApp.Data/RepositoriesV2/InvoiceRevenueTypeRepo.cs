using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IInvoiceRevenueTypeRepo
	{
		#region DropDown
		IEnumerable<SelectItem<InvoiceRevenueTypeItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<InvoiceRevenueTypeListView> GetListvw(SearchParameter search);
		InvoiceRevenueTypeItemView GetByIdvw(Guid id);
		InvoiceRevenueType Find(params object[] keyValues);
		InvoiceRevenueType GetInvoiceRevenueTypeByIdNoTracking(Guid? guid);
		List<InvoiceRevenueType> GetInvoiceRevenueTypeByIdNoTracking(IEnumerable<Guid> invoiceRevenueTypeGuids);
		InvoiceRevenueType CreateInvoiceRevenueType(InvoiceRevenueType invoiceRevenueType);
		void CreateInvoiceRevenueTypeVoid(InvoiceRevenueType invoiceRevenueType);
		InvoiceRevenueType UpdateInvoiceRevenueType(InvoiceRevenueType dbInvoiceRevenueType, InvoiceRevenueType inputInvoiceRevenueType, List<string> skipUpdateFields = null);
		void UpdateInvoiceRevenueTypeVoid(InvoiceRevenueType dbInvoiceRevenueType, InvoiceRevenueType inputInvoiceRevenueType, List<string> skipUpdateFields = null);
		void Remove(InvoiceRevenueType item);
		InvoiceRevenueType GetInvoiceRevenueTypeByIdNoTrackingByAccessLevel(Guid guid);
		List<InvoiceRevenueType> GetInvoiceRevenueTypeByCompanyNoTracking(Guid companyGUID);
	}
	public class InvoiceRevenueTypeRepo : CompanyBaseRepository<InvoiceRevenueType>, IInvoiceRevenueTypeRepo
	{
		public InvoiceRevenueTypeRepo(SmartAppDbContext context) : base(context) { }
		public InvoiceRevenueTypeRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public InvoiceRevenueType GetInvoiceRevenueTypeByIdNoTracking(Guid? guid)
		{
			try
			{
				var result = Entity.Where(item => item.InvoiceRevenueTypeGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<InvoiceRevenueType> GetInvoiceRevenueTypeByIdNoTracking(IEnumerable<Guid> invoiceRevenueTypeGuids)
        {
            try
            {
				var result = Entity.Where(w => invoiceRevenueTypeGuids.Contains(w.InvoiceRevenueTypeGUID))
								.AsNoTracking()
								.ToList();
				return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public InvoiceRevenueType GetInvoiceRevenueTypeByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.InvoiceRevenueTypeGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<InvoiceRevenueTypeItemViewMap> GetDropDownQuery()
		{
			return (from invoiceRevenueType in Entity
					select new InvoiceRevenueTypeItemViewMap
					{
						CompanyGUID = invoiceRevenueType.CompanyGUID,
						Owner = invoiceRevenueType.Owner,
						OwnerBusinessUnitGUID = invoiceRevenueType.OwnerBusinessUnitGUID,
						InvoiceRevenueTypeGUID = invoiceRevenueType.InvoiceRevenueTypeGUID,
						RevenueTypeId = invoiceRevenueType.RevenueTypeId,
						Description = invoiceRevenueType.Description,
						ProductType = invoiceRevenueType.ProductType,
						FeeAmount = invoiceRevenueType.FeeAmount,
						ServiceFeeCategory = invoiceRevenueType.ServiceFeeCategory,
						FeeTaxGUID = invoiceRevenueType.FeeTaxGUID,
						FeeTaxAmount = invoiceRevenueType.FeeTaxAmount,
						FeeWHTGUID = invoiceRevenueType.FeeWHTGUID,
						IntercompanyTableGUID = invoiceRevenueType.IntercompanyTableGUID
					});
		}
		public IEnumerable<SelectItem<InvoiceRevenueTypeItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<InvoiceRevenueType>(search, SysParm.CompanyGUID);
				var invoiceRevenueType = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<InvoiceRevenueTypeItemViewMap, InvoiceRevenueTypeItemView>().ToDropDownItem(search.ExcludeRowData);


				return invoiceRevenueType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<InvoiceRevenueTypeListViewMap> GetListQuery()
		{
			return (from invoiceRevenueType in Entity
					join withholdingTaxTable in db.Set<WithholdingTaxTable>()
					on invoiceRevenueType.FeeWHTGUID equals withholdingTaxTable.WithholdingTaxTableGUID into ljWithholdingTaxTable
					from withholdingTaxTable in ljWithholdingTaxTable.DefaultIfEmpty()
					join taxTable in db.Set<TaxTable>()
					on invoiceRevenueType.FeeTaxGUID equals taxTable.TaxTableGUID into ljTaxTable
					from taxTable in ljTaxTable.DefaultIfEmpty()
					join serviceFeeRevenueType in db.Set<InvoiceRevenueType>()
					on invoiceRevenueType.ServiceFeeRevenueTypeGUID equals serviceFeeRevenueType.InvoiceRevenueTypeGUID into ljInvoiceRevenueType
					from serviceFeeRevenueType in ljInvoiceRevenueType.DefaultIfEmpty()
					select new InvoiceRevenueTypeListViewMap
				{
						CompanyGUID = invoiceRevenueType.CompanyGUID,
						Owner = invoiceRevenueType.Owner,
						OwnerBusinessUnitGUID = invoiceRevenueType.OwnerBusinessUnitGUID,
						InvoiceRevenueTypeGUID = invoiceRevenueType.InvoiceRevenueTypeGUID,
						RevenueTypeId = invoiceRevenueType.RevenueTypeId,
						Description = invoiceRevenueType.Description,
						FeeAmount = invoiceRevenueType.FeeAmount,
						FeeTaxGUID = invoiceRevenueType.FeeTaxGUID,
						FeeTaxAmount = invoiceRevenueType.FeeTaxAmount,
						FeeWHTGUID = invoiceRevenueType.FeeWHTGUID,
						ServiceFeeRevenueTypeGUID = invoiceRevenueType.ServiceFeeRevenueTypeGUID,
						AssetFeeType = invoiceRevenueType.AssetFeeType,
						ServiceFeeCategory = invoiceRevenueType.ServiceFeeCategory,
						WithholdingTaxTable_Values = (withholdingTaxTable != null) ? SmartAppUtil.GetDropDownLabel(withholdingTaxTable.WHTCode, withholdingTaxTable.Description) : null,
						WithholdingTaxTable_WHTCode = (withholdingTaxTable != null) ? withholdingTaxTable.WHTCode : null,
						TaxTable_Values = (taxTable != null) ? SmartAppUtil.GetDropDownLabel(taxTable.TaxCode, taxTable.Description) : null,
						TaxTable_TaxCode = (taxTable != null) ? taxTable.TaxCode : null,
						InvoiceRevenueType_Values = (serviceFeeRevenueType != null) ? SmartAppUtil.GetDropDownLabel(serviceFeeRevenueType.RevenueTypeId, serviceFeeRevenueType.Description) : null,
						InvoiceRevenueType_revenueTypeId = (serviceFeeRevenueType != null) ? serviceFeeRevenueType.RevenueTypeId : null
					});
		}
		public SearchResult<InvoiceRevenueTypeListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<InvoiceRevenueTypeListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<InvoiceRevenueType>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<InvoiceRevenueTypeListViewMap, InvoiceRevenueTypeListView>();
				result = list.SetSearchResult<InvoiceRevenueTypeListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<InvoiceRevenueTypeItemViewMap> GetItemQuery()
		{
			return (from invoiceRevenueType in Entity
					join company in db.Set<Company>()
					on invoiceRevenueType.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on invoiceRevenueType.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljInvoiceRevenueTypeOwnerBU
					from ownerBU in ljInvoiceRevenueTypeOwnerBU.DefaultIfEmpty()
					select new InvoiceRevenueTypeItemViewMap
					{
						CompanyGUID = invoiceRevenueType.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = invoiceRevenueType.Owner,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						OwnerBusinessUnitGUID = invoiceRevenueType.OwnerBusinessUnitGUID,
						CreatedBy = invoiceRevenueType.CreatedBy,
						CreatedDateTime = invoiceRevenueType.CreatedDateTime,
						ModifiedBy = invoiceRevenueType.ModifiedBy,
						ModifiedDateTime = invoiceRevenueType.ModifiedDateTime,
						InvoiceRevenueTypeGUID = invoiceRevenueType.InvoiceRevenueTypeGUID,
						AssetFeeType = invoiceRevenueType.AssetFeeType,
						Description = invoiceRevenueType.Description,
						FeeAmount = invoiceRevenueType.FeeAmount,
						FeeInvoiceText = invoiceRevenueType.FeeInvoiceText,
						FeeLedgerAccount = invoiceRevenueType.FeeLedgerAccount,
						FeeTaxAmount = invoiceRevenueType.FeeTaxAmount,
						FeeTaxGUID = invoiceRevenueType.FeeTaxGUID,
						FeeWHTGUID = invoiceRevenueType.FeeWHTGUID,
						IntercompanyTableGUID = invoiceRevenueType.IntercompanyTableGUID,
						ProductType = invoiceRevenueType.ProductType,
						RevenueTypeId = invoiceRevenueType.RevenueTypeId,
						ServiceFeeCategory = invoiceRevenueType.ServiceFeeCategory,
						ServiceFeeRevenueTypeGUID = invoiceRevenueType.ServiceFeeRevenueTypeGUID,
					
						RowVersion = invoiceRevenueType.RowVersion,
					});
		}
		public InvoiceRevenueTypeItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.ToMap<InvoiceRevenueTypeItemViewMap, InvoiceRevenueTypeItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public InvoiceRevenueType CreateInvoiceRevenueType(InvoiceRevenueType invoiceRevenueType)
		{
			try
			{
				invoiceRevenueType.InvoiceRevenueTypeGUID = Guid.NewGuid();
				base.Add(invoiceRevenueType);
				return invoiceRevenueType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateInvoiceRevenueTypeVoid(InvoiceRevenueType invoiceRevenueType)
		{
			try
			{
				CreateInvoiceRevenueType(invoiceRevenueType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public InvoiceRevenueType UpdateInvoiceRevenueType(InvoiceRevenueType dbInvoiceRevenueType, InvoiceRevenueType inputInvoiceRevenueType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbInvoiceRevenueType = dbInvoiceRevenueType.MapUpdateValues<InvoiceRevenueType>(inputInvoiceRevenueType);
				CheckTaxAndWHT(inputInvoiceRevenueType);
				base.Update(dbInvoiceRevenueType);
				return dbInvoiceRevenueType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateInvoiceRevenueTypeVoid(InvoiceRevenueType dbInvoiceRevenueType, InvoiceRevenueType inputInvoiceRevenueType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbInvoiceRevenueType = dbInvoiceRevenueType.MapUpdateValues<InvoiceRevenueType>(inputInvoiceRevenueType, skipUpdateFields);
				base.Update(dbInvoiceRevenueType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public override void ValidateAdd(InvoiceRevenueType item)
		{
			CheckTaxAmount(item);
			CheckTaxAndWHT(item);
			base.ValidateAdd(item);
		}
		
		public void CheckTaxAmount(InvoiceRevenueType item)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");

				if (item.FeeTaxAmount < 0)
				{
					ex.AddData("ERROR.GREATER_THAN_EQUAL_OR_ZERO", new string[] { "LABEL.TAX_AMOUNT" });
				}

				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CheckTaxAndWHT(InvoiceRevenueType item)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				if (item.FeeTaxGUID != null || item.FeeWHTGUID != null)
				{
					IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
					InvoiceType invoiceType = invoiceTypeRepo.GetInvoiceTypeByInvoiceRevenueTypeNoTracking(item.InvoiceRevenueTypeGUID);

                    if (invoiceType != null)
                    {
						if (invoiceType.SuspenseInvoiceType != (int)SuspenseInvoiceType.None ||
					       (invoiceType.ProductInvoice == true && ((invoiceType.ProductType == (int)ProductType.Factoring) ||
						   (invoiceType.ProductType == (int)ProductType.ProjectFinance))))
						{
							ex.AddData("ERROR.90119");
						}
					}
				}
				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete

		public List<InvoiceRevenueType> GetInvoiceRevenueTypeByCompanyNoTracking(Guid companyGUID)
		{
			try
			{
				List<InvoiceRevenueType> invoiceRevenueTypes = Entity.Where(w => w.CompanyGUID == companyGUID).AsNoTracking().ToList();
				return invoiceRevenueTypes;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

