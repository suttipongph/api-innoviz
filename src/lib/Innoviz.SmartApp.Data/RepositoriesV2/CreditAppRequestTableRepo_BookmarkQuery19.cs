﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.Repositories;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModels;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Core.Service.ConditionService;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public partial interface ICreditAppRequestTableRepo
	{
        GenBookmarkBuyerCreditApplicationView BookmarkBuyerCreditApplicationView(Guid refGUID, Guid bookmarkDocumentTransGuid);
    }
	public partial class CreditAppRequestTableRepo : CompanyBaseRepository<CreditAppRequestTable>, ICreditAppRequestTableRepo
	{
        public GenBookmarkBuyerCreditApplicationView BookmarkBuyerCreditApplicationView(Guid refGUID, Guid bookmarkDocumentTransGuid)
        {
            try
            {
                int docTemplateType = (from bookmarkDoc in db.Set<BookmarkDocument>()
                                        join bookmarkDocTrans in db.Set<BookmarkDocumentTrans>().Where(w => w.BookmarkDocumentTransGUID == bookmarkDocumentTransGuid)
                                        on bookmarkDoc.BookmarkDocumentGUID equals bookmarkDocTrans.BookmarkDocumentGUID
                                        select bookmarkDoc.DocumentTemplateType).FirstOrDefault();
                GenBookmarkBuyerCreditApplicationView result = (from creditAppRequestTable in Entity.Where(w => w.CreditAppRequestTableGUID == refGUID)
                                                                join creditAppTable in db.Set<CreditAppTable>()
                                                                on creditAppRequestTable.RefCreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljCreditAppTable
                                                                from creditAppTable in ljCreditAppTable.DefaultIfEmpty()

                                                                join documentStatus in db.Set<DocumentStatus>()
                                                                on creditAppRequestTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljDocumentStatus
                                                                from documentStatus in ljDocumentStatus.DefaultIfEmpty()

                                                                join kycSetup in db.Set<KYCSetup>()
                                                                on creditAppRequestTable.KYCGUID equals kycSetup.KYCSetupGUID into ljKycSetup
                                                                from kycSetup in ljKycSetup.DefaultIfEmpty()

                                                                join documentReason in db.Set<DocumentReason>()
                                                                on creditAppRequestTable.DocumentReasonGUID equals documentReason.DocumentReasonGUID into ljDocumentReason
                                                                from documentReason in ljDocumentReason.DefaultIfEmpty()

                                                                join customerTable in db.Set<CustomerTable>()
                                                                on creditAppRequestTable.CustomerTableGUID equals customerTable.CustomerTableGUID into ljCustomerTable
                                                                from customerTable in ljCustomerTable.DefaultIfEmpty()

                                                                join employeeTable in db.Set<EmployeeTable>()
                                                                on customerTable.ResponsibleByGUID equals employeeTable.EmployeeTableGUID into ljEmployeeTable
                                                                from employeeTable in ljEmployeeTable.DefaultIfEmpty()

                                                                join creditScoring in db.Set<CreditScoring>()
                                                                on creditAppRequestTable.CreditScoringGUID equals creditScoring.CreditScoringGUID into ljCreditScoring
                                                                from creditScoring in ljCreditScoring.DefaultIfEmpty()

                                                                select new GenBookmarkBuyerCreditApplicationView
                                                                {
                                                                    CANumber = creditAppRequestTable.CreditAppRequestId,
                                                                    RefCANumber = creditAppTable != null ? creditAppTable.CreditAppId : "",
                                                                    CAStatus = documentStatus != null ? documentStatus.Description : "",
                                                                    CACreateDate = creditAppRequestTable.RequestDate,
                                                                    CustomerName1 = customerTable != null ? customerTable.Name : "",
                                                                    CustomerCode = customerTable != null ? customerTable.CustomerId : "",
                                                                    SalesResp = employeeTable != null ? employeeTable.Name : "",
                                                                    KYCHeader = kycSetup != null ? kycSetup.Description : "",
                                                                    CreditScoring = creditScoring != null ? creditScoring.Description : "",
                                                                    CADesp = creditAppRequestTable.Description,
                                                                    Purpose = documentReason != null ? documentReason.Description : "",
                                                                    CARemark = creditAppRequestTable.Remark,
                                                                    MarketingComment = creditAppRequestTable.MarketingComment,
                                                                    CreditComment = creditAppRequestTable.CreditComment,
                                                                    ApproverComment = creditAppRequestTable.ApproverComment,
                                                                    CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
                                                                    ProductType = creditAppRequestTable.ProductType //R03
                                                                }).AsNoTracking().FirstOrDefault();

                List<QCAReqLine> QCAReqLineAll = (from creditAppRequestLine in db.Set<CreditAppRequestLine>().Where(w => w.CreditAppRequestTableGUID == result.CreditAppRequestTableGUID).OrderBy(o => o.LineNum)
                                                   join buyerTable in db.Set<BuyerTable>()
                                                       on creditAppRequestLine.BuyerTableGUID equals buyerTable.BuyerTableGUID
                                                   join businessSegment in db.Set<BusinessSegment>()
                                                       on buyerTable.BusinessSegmentGUID equals businessSegment.BusinessSegmentGUID
                                                   join billingResponsibleBy in db.Set<BillingResponsibleBy>()
                                                       on creditAppRequestLine.BillingResponsibleByGUID equals billingResponsibleBy.BillingResponsibleByGUID into ljBillingRes
                                                   from billingResponsibleBy in ljBillingRes.DefaultIfEmpty()
                                                   join methodOfPayment in db.Set<MethodOfPayment>()
                                                      on creditAppRequestLine.MethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID into ljMethodOfPaym
                                                   from methodOfPayment in ljMethodOfPaym.DefaultIfEmpty()
                                                   join invoiceAddress in db.Set<AddressTrans>()
                                                      on creditAppRequestLine.InvoiceAddressGUID equals invoiceAddress.AddressTransGUID
                                                      into ljInvoiceAddress
                                                   from invoiceAddress in ljInvoiceAddress.DefaultIfEmpty()
                                                   join lineOfBusiness in db.Set<LineOfBusiness>()
                                                       on buyerTable.LineOfBusinessGUID equals lineOfBusiness.LineOfBusinessGUID
                                                       into ljLineOfBusiness
                                                   from lineOfBusiness in ljLineOfBusiness.DefaultIfEmpty()
                                                   join blacklistStatus in db.Set<BlacklistStatus>()
                                                       on creditAppRequestLine.BlacklistStatusGUID equals blacklistStatus.BlacklistStatusGUID
                                                       into ljBlacklistStatus
                                                   from blacklistStatus in ljBlacklistStatus.DefaultIfEmpty()
                                                   join creditScoring in db.Set<CreditScoring>()
                                                       on creditAppRequestLine.CreditScoringGUID equals creditScoring.CreditScoringGUID
                                                       into ljCreditScoring
                                                   from creditScoring in ljCreditScoring.DefaultIfEmpty()

                                                   join billingContactPersonTrans in db.Set<ContactPersonTrans>()
                                                       on creditAppRequestLine.BillingContactPersonGUID equals billingContactPersonTrans.ContactPersonTransGUID
                                                       into ljBillingContactPersonTrans
                                                   from billingContactPersonTrans in ljBillingContactPersonTrans.DefaultIfEmpty()
                                                   join relatedBillingContactPerson in db.Set<RelatedPersonTable>()
                                                      on billingContactPersonTrans.RelatedPersonTableGUID equals relatedBillingContactPerson.RelatedPersonTableGUID
                                                      into ljRelatedBillingContactPerson
                                                   from relatedBillingContactPerson in ljRelatedBillingContactPerson.DefaultIfEmpty()
                                                   join buyerBillingAddress in db.Set<AddressTrans>()
                                                      on creditAppRequestLine.BillingAddressGUID equals buyerBillingAddress.AddressTransGUID
                                                      into ljBuyerBillingAddress
                                                   from buyerBillingAddress in ljBuyerBillingAddress.DefaultIfEmpty()

                                                   join receiptContactPersonTrans in db.Set<ContactPersonTrans>()
                                                       on creditAppRequestLine.ReceiptContactPersonGUID equals receiptContactPersonTrans.ContactPersonTransGUID
                                                       into ljReceiptContactPersonTrans
                                                   from receiptContactPersonTrans in ljReceiptContactPersonTrans.DefaultIfEmpty()
                                                   join relatedReceiptContactPerson in db.Set<RelatedPersonTable>()
                                                       on receiptContactPersonTrans.RelatedPersonTableGUID equals relatedReceiptContactPerson.RelatedPersonTableGUID
                                                       into ljRelatedReceiptContactPerson
                                                   from relatedReceiptContactPerson in ljRelatedReceiptContactPerson.DefaultIfEmpty()
                                                   join buyerReceiptAddress in db.Set<AddressTrans>()
                                                      on creditAppRequestLine.ReceiptAddressGUID equals buyerReceiptAddress.AddressTransGUID
                                                      into ljBuyerReceiptAddress
                                                   from buyerReceiptAddress in ljBuyerReceiptAddress.DefaultIfEmpty()
                                                   select new QCAReqLine
                                                   {
                                                       CreditAppRequestLineGUID = creditAppRequestLine.CreditAppRequestLineGUID,
                                                       LineNum = creditAppRequestLine.LineNum,
                                                       BuyerName = buyerTable.BuyerName,
                                                       BusinessSegment = businessSegment.Description,
                                                       BusinessSegmentId = businessSegment.BusinessSegmentId,
                                                       BusinessSegmentType = businessSegment.BusinessSegmentType,
                                                       CreditLimitLineRequest = creditAppRequestLine.CreditLimitLineRequest,
                                                       MaxPurchasePct = creditAppRequestLine.MaxPurchasePct,
                                                       AssignmentMethodRemark = creditAppRequestLine.AssignmentMethodRemark,
                                                       BuyerCreditLimit = creditAppRequestLine.BuyerCreditLimit,

                                                       BillingResponsibleBy = billingResponsibleBy != null ? billingResponsibleBy.Description : "",
                                                       MethodOfPayment = methodOfPayment != null ? methodOfPayment.Description : "",
                                                       TaxId = buyerTable.TaxId,
                                                       InvoiceAddress = invoiceAddress != null ? (invoiceAddress.Address1 + " " + invoiceAddress.Address2) : "",
                                                       DateOfEstablish = buyerTable.DateOfEstablish,
                                                       LineOfBusiness = lineOfBusiness != null ? lineOfBusiness.Description : "",
                                                       BlacklistStatus = blacklistStatus != null ? blacklistStatus.Description : "",
                                                       CreditScoring = creditScoring != null ? creditScoring.Description : "",
                                                       PurchaseFeePct = creditAppRequestLine.PurchaseFeePct,
                                                       PurchaseFeeCalculateBase = creditAppRequestLine.PurchaseFeeCalculateBase,
                                                       AcceptanceDocument = creditAppRequestLine.AcceptanceDocument,

                                                       BillingDescription = creditAppRequestLine.BillingDescription,
                                                       BillingContactPersonName = relatedBillingContactPerson != null ? relatedBillingContactPerson.Name : "",
                                                       BillingContactPersonPosition = relatedBillingContactPerson != null ? relatedBillingContactPerson.Position : "",
                                                       BillingContactPersonPhone = relatedBillingContactPerson != null ? relatedBillingContactPerson.Phone : "",
                                                       BuyerBillingAddress = buyerBillingAddress != null ? (buyerBillingAddress.Address1 + " " + buyerBillingAddress.Address2) : "",

                                                       ReceiptDescription = creditAppRequestLine.ReceiptDescription,
                                                       ReceiptContactPersonName = relatedReceiptContactPerson != null ? relatedReceiptContactPerson.Name : "",
                                                       ReceiptContactPersonPosition = relatedReceiptContactPerson != null ? relatedReceiptContactPerson.Position : "",
                                                       ReceiptContactPersonPhone = relatedReceiptContactPerson != null ? relatedReceiptContactPerson.Phone : "",
                                                       BuyerReceiptAddress = buyerReceiptAddress != null ? (buyerReceiptAddress.Address1 + " " + buyerReceiptAddress.Address2) : "",

                                                       MarketingComment = creditAppRequestLine.MarketingComment,
                                                       CreditComment = creditAppRequestLine.CreditComment,
                                                       ApproverComment = creditAppRequestLine.ApproverComment,

                                                       //R03
                                                       BuyerTableGUID = creditAppRequestLine.BuyerTableGUID,
                                                       LineCondition = creditAppRequestLine.LineCondition
                                                       //R03
                                                   }).ToList();

                List<QCAReqLine> QCAReqLineList = new List<QCAReqLine>();

                switch (docTemplateType)
                {
                    case (int)DocumentTemplateType.BuyerCreditApplication_Private:
                        QCAReqLineList = QCAReqLineAll.Where(w => w.BusinessSegmentType == (int)BusinessSegmentType.Private).Take(10).ToList();
                        break;
                    case (int)DocumentTemplateType.BuyerCreditApplication_Government:
                        QCAReqLineList = QCAReqLineAll.Where(w => w.BusinessSegmentType == (int)BusinessSegmentType.Goverment).Take(10).ToList();
                        break;
                    default:
                        break;
                }

                IFinancialStatementTransRepo financialStatmentTransRepo = new FinancialStatementTransRepo(db);
                IServiceFeeConditionTransRepo serviceFeeConditionTransRepo = new ServiceFeeConditionTransRepo(db);
                IDocumentConditionTransRepo documentConditionTransRepo = new DocumentConditionTransRepo(db);
                foreach (var item in QCAReqLineList)
                {
                    item.ListFinancial = new List<QCreditAppReqLineFin_BuyerCreditApplication>();
                    List<FinancialStatementTrans> financialStatmentTrans = financialStatmentTransRepo.GetFinancialStatementTransByRefernece(item.CreditAppRequestLineGUID, (int)RefType.CreditAppRequestLine)
                                                                                                     .OrderByDescending(o => o.Year).ThenBy(o => o.Ordering).ToList();
                    var yearDistinctList = financialStatmentTrans.GroupBy(g => g.Year).Select(s => new DistinctYear
                                                                                            {
                                                                                                Year = s.Key
                                                                                            }).Take(3);
                    foreach (var itemDistinctYear in yearDistinctList)
                    {
                        var QCreditAppReqLineFin = GetCreditAppReqLineFinancial(financialStatmentTrans, item.CreditAppRequestLineGUID, itemDistinctYear.Year);
                        item.ListFinancial.Add(QCreditAppReqLineFin);
                    }

                    var QServiceFeeConditionLine = serviceFeeConditionTransRepo.GetServiceFeeConditionLineByBuyerCreditApplication(item.CreditAppRequestLineGUID);
                    item.ListServiceFeeCondition = new List<QServiceFeeConditionLine_BuyerCreditApplication>();
                    item.ListServiceFeeCondition.AddRange(QServiceFeeConditionLine);
                    item.SumServiceFeeCondition = serviceFeeConditionTransRepo.GetSumServiceFeeConditionByCreditAppRequestLine(item.CreditAppRequestLineGUID);

                    var QBillingDocument = documentConditionTransRepo.GetDocumentConditionTransByBuyerCreditApplication(item.CreditAppRequestLineGUID, (int)DocConVerifyType.Billing);
                    item.ListBillingDocumentCondition = new List<QDocumentCondition_BuyerCreditApplication>();
                    item.ListBillingDocumentCondition.AddRange(QBillingDocument);

                    var QReceiptDocument = documentConditionTransRepo.GetDocumentConditionTransByBuyerCreditApplication(item.CreditAppRequestLineGUID, (int)DocConVerifyType.Receipt);
                    item.ListReceiptDocumentCondition = new List<QDocumentCondition_BuyerCreditApplication>();
                    item.ListReceiptDocumentCondition.AddRange(QReceiptDocument);

                    //R03
                    ICustTransService custTransService = new CustTransService(db);
                    item.AllCustomerBuyerOutstanding = custTransService.GetAllCustomerBuyerOutstanding(result.ProductType, item.BuyerTableGUID);
                }

                result.BuyerQtyPrivate = QCAReqLineList.Count();
                result.AllBuyerCoRemainingAmount_R01 = QCAReqLineList.Select(s => s.BuyerCreditLimit).Sum(); //R03
                #region QCAReqLine1
                if (CheckQCAReqLineElementIsNotNull(QCAReqLineList, 0))
                {
                    QCAReqLine QCAReqLine1 = QCAReqLineList[0];
                    result.BuyerName1st_1 = QCAReqLine1.BuyerName;
                    result.BuyerCreditLimitRequest1st_1 = QCAReqLine1.CreditLimitLineRequest;
                    result.BuyerCreditLimitAllCustomer1st_1 = QCAReqLine1.BuyerCreditLimit;
                    result.PurchasePercent1st_1 = QCAReqLine1.MaxPurchasePct;
                    result.AssignmentCondition1st_1 = QCAReqLine1.AssignmentMethodRemark;
                    result.BillingResponsible1st_1 = QCAReqLine1.BillingResponsibleBy;
                    result.PaymentMethod1st_1 = QCAReqLine1.MethodOfPayment;
                    result.BusinessSegment1st_2 = QCAReqLine1.BusinessSegment;
                    result.BuyerNameCARequestLine1st_1 = QCAReqLine1.BuyerName;
                    result.BuyerTaxID1st_1 = QCAReqLine1.TaxId;
                    result.BuyerAddress1st_1 = QCAReqLine1.InvoiceAddress;
                    result.BuyerCompanyRegisteredDate1st_1 = QCAReqLine1.DateOfEstablish;
                    result.BuyerLineOfBusiness1st_1 = QCAReqLine1.LineOfBusiness;
                    result.BuyerBlacklistStatus1st_1 = QCAReqLine1.BlacklistStatus;
                    result.BuyerCreditScoring1st_1 = QCAReqLine1.CreditScoring;
                    result.BuyerCreditLimitRequest1st_2 = QCAReqLine1.CreditLimitLineRequest;
                    result.BuyerCreditLimitAllCustomer1st_2 = QCAReqLine1.BuyerCreditLimit;
                    result.PurchasePercent1st_2 = QCAReqLine1.MaxPurchasePct;
                    result.AssignmentCondition1st_2 = QCAReqLine1.AssignmentMethodRemark;
                    result.FactoringPurchaseFee1st_1 = QCAReqLine1.PurchaseFeePct;
                    result.FactoringPurchaseFeeCalBase1st_1 = SystemStaticData.GetTranslatedMessage(((PurchaseFeeCalculateBase)QCAReqLine1.PurchaseFeeCalculateBase).GetAttrCode());
                    result.BillingResponsible1st_2 = QCAReqLine1.BillingResponsibleBy;
                    result.BillingDescriptiong1st_1 = QCAReqLine1.BillingDescription;
                    result.BuyerBillingContactPerson1st_1 = QCAReqLine1.BillingContactPersonName;
                    result.BuyerBillingContactPersonPosition1st_1 = QCAReqLine1.BillingContactPersonPosition;
                    result.BuyerBillingAddress1st_1 = QCAReqLine1.BuyerBillingAddress;
                    result.BuyerBillingContactPersonTel1st_1 = QCAReqLine1.BillingContactPersonPhone;
                    result.ReceiptDescriptiong1st_1 = QCAReqLine1.ReceiptDescription;
                    result.PaymentMethod1st_2 = QCAReqLine1.MethodOfPayment;
                    result.BuyerReceiptContactPerson1st_1 = QCAReqLine1.ReceiptContactPersonName;
                    result.BuyerReceiptContactPersonPosition1st_1 = QCAReqLine1.ReceiptContactPersonPosition;
                    result.BuyerReceiptAddress1st_1 = QCAReqLine1.BuyerReceiptAddress;
                    result.BuyerReceiptContactPersonTel1st_1 = QCAReqLine1.ReceiptContactPersonPhone;
                    result.MarketingComment1st = QCAReqLine1.MarketingComment;
                    result.CreditComment1st = QCAReqLine1.CreditComment;
                    result.ApproverComment1st = QCAReqLine1.ApproverComment;
                    result.AcceptanceDocument1st_1 = QCAReqLine1.AcceptanceDocument == true ? TextConstants.Yes : TextConstants.No;
                    //R03
                    result.AROutstandingAllCustomer_1 = QCAReqLine1.AllCustomerBuyerOutstanding;
                    result.PurchaseConditionCust_1 = QCAReqLine1.LineCondition;
                    //R03

                    #region QCreditAppReqLineFin
                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine1.ListFinancial, 0))
                    {
                        result.BuyerFinYearFirst1_1st_1 = QCAReqLine1.ListFinancial[0].Year;
                        result.BuyerFinRegisteredCapitalFirst1_1st_1 = QCAReqLine1.ListFinancial[0].RegisteredCapital;
                        result.BuyerFinPaidCapitalFirst1_1st_1 = QCAReqLine1.ListFinancial[0].PaidCapital;
                        result.BuyerFinTotalAssetFirst1_1st_1 = QCAReqLine1.ListFinancial[0].TotalAsset;
                        result.BuyerFinTotalLiabilityFirst1_1st_1 = QCAReqLine1.ListFinancial[0].TotalLiability;
                        result.BuyerFinTotalEquityFirst1_1st_1 = QCAReqLine1.ListFinancial[0].TotalEquity;
                        result.BuyerFinYearFirst2_1st_1 = QCAReqLine1.ListFinancial[0].Year;
                        result.BuyerFinTotalRevenueFirst1_1st_1 = QCAReqLine1.ListFinancial[0].TotalRevenue;
                        result.BuyerFinTotalCOGSFirst1_1st_1 = QCAReqLine1.ListFinancial[0].TotalCOGS;
                        result.BuyerFinTotalGrossProfitFirst1_1st_1 = QCAReqLine1.ListFinancial[0].TotalGrossProfit;
                        result.BuyerFinTotalOperExpFirst1_1st_1 = QCAReqLine1.ListFinancial[0].TotalOperExp;
                        result.BuyerFinTotalNetProfitFirst1_1st_1 = QCAReqLine1.ListFinancial[0].TotalNetProfit;
                        result.BuyerFinNetProfitPercent1_1st_1 = QCAReqLine1.ListFinancial[0].NetProfitPercent;
                        result.BuyerFinlDE1_1st_1 = QCAReqLine1.ListFinancial[0].lDE;
                        result.BuyerFinQuickRatio1_1st_1 = QCAReqLine1.ListFinancial[0].QuickRatio;
                        result.BuyerFinIntCoverageRatio1_1st_1 = QCAReqLine1.ListFinancial[0].IntCoverageRatio;
                    }

                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine1.ListFinancial, 1))
                    {
                        result.BuyerFinYearSecond1_1st_1 = QCAReqLine1.ListFinancial[1].Year;
                        result.BuyerFinRegisteredCapitalSecond1_1st_1 = QCAReqLine1.ListFinancial[1].RegisteredCapital;
                        result.BuyerFinPaidCapitalSecond1_1st_1 = QCAReqLine1.ListFinancial[1].PaidCapital;
                        result.BuyerFinTotalAssetSecond1_1st_1 = QCAReqLine1.ListFinancial[1].TotalAsset;
                        result.BuyerFinTotalLiabilitySecond1_1st_1 = QCAReqLine1.ListFinancial[1].TotalLiability;
                        result.BuyerFinTotalEquitySecond1_1st_1 = QCAReqLine1.ListFinancial[1].TotalEquity;
                        result.BuyerFinYearSecond2_1st_1 = QCAReqLine1.ListFinancial[1].Year;
                        result.BuyerFinTotalRevenueSecond1_1st_1 = QCAReqLine1.ListFinancial[1].TotalRevenue;
                        result.BuyerFinTotalCOGSSecond1_1st_1 = QCAReqLine1.ListFinancial[1].TotalCOGS;
                        result.BuyerFinTotalGrossProfitSecond1_1st_1 = QCAReqLine1.ListFinancial[1].TotalGrossProfit;
                        result.BuyerFinTotalOperExpSecond1_1st_1 = QCAReqLine1.ListFinancial[1].TotalOperExp;
                        result.BuyerFinTotalNetProfitSecond1_1st_1 = QCAReqLine1.ListFinancial[1].TotalNetProfit;
                    }

                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine1.ListFinancial, 2))
                    {
                        result.BuyerFinYearThird1_1st_1 = QCAReqLine1.ListFinancial[2].Year;
                        result.BuyerFinRegisteredCapitalThird1_1st_1 = QCAReqLine1.ListFinancial[2].RegisteredCapital;
                        result.BuyerFinPaidCapitalThird1_1st_1 = QCAReqLine1.ListFinancial[2].PaidCapital;
                        result.BuyerFinTotalAssetThird1_1st_1 = QCAReqLine1.ListFinancial[2].TotalAsset;
                        result.BuyerFinTotalLiabilityThird1_1st_1 = QCAReqLine1.ListFinancial[2].TotalLiability;
                        result.BuyerFinTotalEquityThird1_1st_1 = QCAReqLine1.ListFinancial[2].TotalEquity;
                        result.BuyerFinYearThird2_1st_1 = QCAReqLine1.ListFinancial[2].Year;
                        result.BuyerFinTotalRevenueThird1_1st_1 = QCAReqLine1.ListFinancial[2].TotalRevenue;
                        result.BuyerFinTotalCOGSThird1_1st_1 = QCAReqLine1.ListFinancial[2].TotalCOGS;
                        result.BuyerFinTotalGrossProfitThird1_1st_1 = QCAReqLine1.ListFinancial[2].TotalGrossProfit;
                        result.BuyerFinTotalOperExpThird1_1st_1 = QCAReqLine1.ListFinancial[2].TotalOperExp;
                        result.BuyerFinTotalNetProfitThird1_1st_1 = QCAReqLine1.ListFinancial[2].TotalNetProfit;
                    }
                    #endregion QCreditAppReqLineFin

                    #region QDocumentCondition - Billing
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine1.ListBillingDocumentCondition, 0)) result.BillingDocumentFirst1_1st_1 = QCAReqLine1.ListBillingDocumentCondition[0].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine1.ListBillingDocumentCondition, 1)) result.BillingDocumentSecond1_1st_1 = QCAReqLine1.ListBillingDocumentCondition[1].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine1.ListBillingDocumentCondition, 2)) result.BillingDocumentThird1_1st_1 = QCAReqLine1.ListBillingDocumentCondition[2].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine1.ListBillingDocumentCondition, 3)) result.BillingDocumentFourth_1st_1 = QCAReqLine1.ListBillingDocumentCondition[3].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine1.ListBillingDocumentCondition, 4)) result.BillingDocumentFifth_1st_1 = QCAReqLine1.ListBillingDocumentCondition[4].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine1.ListBillingDocumentCondition, 5)) result.BillingDocumentSixth1_1st_1 = QCAReqLine1.ListBillingDocumentCondition[5].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine1.ListBillingDocumentCondition, 6)) result.BillingDocumentSeventh_1st_1 = QCAReqLine1.ListBillingDocumentCondition[6].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine1.ListBillingDocumentCondition, 7)) result.BillingDocumentEigth1_1st_1 = QCAReqLine1.ListBillingDocumentCondition[7].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine1.ListBillingDocumentCondition, 8)) result.BillingDocumentNineth1_1st_1 = QCAReqLine1.ListBillingDocumentCondition[8].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine1.ListBillingDocumentCondition, 9)) result.BillingDocumentTenth_1st_1 = QCAReqLine1.ListBillingDocumentCondition[9].Description;
                    #endregion QDocumentCondition - Billing

                    #region QDocumentCondition - Receipt
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine1.ListReceiptDocumentCondition, 0)) result.ReceiptDocumentFirst1_1st_1 = QCAReqLine1.ListReceiptDocumentCondition[0].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine1.ListReceiptDocumentCondition, 1)) result.ReceiptDocumentSecond1_1st_1 = QCAReqLine1.ListReceiptDocumentCondition[1].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine1.ListReceiptDocumentCondition, 2)) result.ReceiptDocumentThird1_1st_1 = QCAReqLine1.ListReceiptDocumentCondition[2].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine1.ListReceiptDocumentCondition, 3)) result.ReceiptDocumentFourth_1st_1 = QCAReqLine1.ListReceiptDocumentCondition[3].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine1.ListReceiptDocumentCondition, 4)) result.ReceiptDocumentFifth_1st_1 = QCAReqLine1.ListReceiptDocumentCondition[4].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine1.ListReceiptDocumentCondition, 5)) result.ReceiptDocumentSixth1_1st_1 = QCAReqLine1.ListReceiptDocumentCondition[5].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine1.ListReceiptDocumentCondition, 6)) result.ReceiptDocumentSeventh_1st_1 = QCAReqLine1.ListReceiptDocumentCondition[6].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine1.ListReceiptDocumentCondition, 7)) result.ReceiptDocumentEigth1_1st_1 = QCAReqLine1.ListReceiptDocumentCondition[7].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine1.ListReceiptDocumentCondition, 8)) result.ReceiptDocumentNineth1_1st_1 = QCAReqLine1.ListReceiptDocumentCondition[8].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine1.ListReceiptDocumentCondition, 9)) result.ReceiptDocumentTenth_1st_1 = QCAReqLine1.ListReceiptDocumentCondition[9].Description;
                    #endregion QDocumentCondition - Receipt

                    #region QServiceFeeCondition
                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine1.ListServiceFeeCondition, 0))
                    { 
                        result.ServiceFeeDesc01_1st_1 = QCAReqLine1.ListServiceFeeCondition[0].ServicefeeType;
                        result.ServiceFeeAmt01_1st_1 = QCAReqLine1.ListServiceFeeCondition[0].AmountBeforeTax;
                        result.ServiceFeeRemark01_1st_1 = QCAReqLine1.ListServiceFeeCondition[0].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine1.ListServiceFeeCondition, 1))
                    {
                        result.ServiceFeeDesc02_1st_1 = QCAReqLine1.ListServiceFeeCondition[1].ServicefeeType;
                        result.ServiceFeeAmt02_1st_1 = QCAReqLine1.ListServiceFeeCondition[1].AmountBeforeTax;
                        result.ServiceFeeRemark02_1st_1 = QCAReqLine1.ListServiceFeeCondition[1].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine1.ListServiceFeeCondition, 2))
                    {
                        result.ServiceFeeDesc03_1st_1 = QCAReqLine1.ListServiceFeeCondition[2].ServicefeeType;
                        result.ServiceFeeAmt03_1st_1 = QCAReqLine1.ListServiceFeeCondition[2].AmountBeforeTax;
                        result.ServiceFeeRemark03_1st_1 = QCAReqLine1.ListServiceFeeCondition[2].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine1.ListServiceFeeCondition, 3))
                    {
                        result.ServiceFeeDesc04_1st_1 = QCAReqLine1.ListServiceFeeCondition[3].ServicefeeType;
                        result.ServiceFeeAmt04_1st_1 = QCAReqLine1.ListServiceFeeCondition[3].AmountBeforeTax;
                        result.ServiceFeeRemark04_1st_1 = QCAReqLine1.ListServiceFeeCondition[3].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine1.ListServiceFeeCondition, 4))
                    {
                        result.ServiceFeeDesc05_1st_1 = QCAReqLine1.ListServiceFeeCondition[4].ServicefeeType;
                        result.ServiceFeeAmt05_1st_1 = QCAReqLine1.ListServiceFeeCondition[4].AmountBeforeTax;
                        result.ServiceFeeRemark05_1st_1 = QCAReqLine1.ListServiceFeeCondition[4].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine1.ListServiceFeeCondition, 5))
                    {
                        result.ServiceFeeDesc06_1st_1 = QCAReqLine1.ListServiceFeeCondition[5].ServicefeeType;
                        result.ServiceFeeAmt06_1st_1 = QCAReqLine1.ListServiceFeeCondition[5].AmountBeforeTax;
                        result.ServiceFeeRemark06_1st_1 = QCAReqLine1.ListServiceFeeCondition[5].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine1.ListServiceFeeCondition, 6))
                    {
                        result.ServiceFeeDesc07_1st_1 = QCAReqLine1.ListServiceFeeCondition[6].ServicefeeType;
                        result.ServiceFeeAmt07_1st_1 = QCAReqLine1.ListServiceFeeCondition[6].AmountBeforeTax;
                        result.ServiceFeeRemark07_1st_1 = QCAReqLine1.ListServiceFeeCondition[6].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine1.ListServiceFeeCondition, 7))
                    {
                        result.ServiceFeeDesc08_1st_1 = QCAReqLine1.ListServiceFeeCondition[7].ServicefeeType;
                        result.ServiceFeeAmt08_1st_1 = QCAReqLine1.ListServiceFeeCondition[7].AmountBeforeTax;
                        result.ServiceFeeRemark08_1st_1 = QCAReqLine1.ListServiceFeeCondition[7].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine1.ListServiceFeeCondition, 8))
                    {
                        result.ServiceFeeDesc09_1st_1 = QCAReqLine1.ListServiceFeeCondition[8].ServicefeeType;
                        result.ServiceFeeAmt09_1st_1 = QCAReqLine1.ListServiceFeeCondition[8].AmountBeforeTax;
                        result.ServiceFeeRemark09_1st_1 = QCAReqLine1.ListServiceFeeCondition[8].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine1.ListServiceFeeCondition, 9))
                    {
                        result.ServiceFeeDesc10_1st_1 = QCAReqLine1.ListServiceFeeCondition[9].ServicefeeType;
                        result.ServiceFeeAmt10_1st_1 = QCAReqLine1.ListServiceFeeCondition[9].AmountBeforeTax;
                        result.ServiceFeeRemark10_1st_1 = QCAReqLine1.ListServiceFeeCondition[9].Description;
                    }
                    #endregion QServiceFeeCondition

                    result.SumServiceFeeAmt_1st_1 = QCAReqLine1.SumServiceFeeCondition;
                }
                #endregion QCAReqLine1
                #region QCAReqLine2
                if (CheckQCAReqLineElementIsNotNull(QCAReqLineList, 1))
                {
                    QCAReqLine QCAReqLine2 = QCAReqLineList[1];
                    result.BuyerName2nd_1 = QCAReqLine2.BuyerName;
                    result.BuyerCreditLimitRequest2nd_1 = QCAReqLine2.CreditLimitLineRequest;
                    result.BuyerCreditLimitAllCustomer2nd_1 = QCAReqLine2.BuyerCreditLimit;
                    result.PurchasePercent2nd_1 = QCAReqLine2.MaxPurchasePct;
                    result.AssignmentCondition2nd_1 = QCAReqLine2.AssignmentMethodRemark;
                    result.BillingResponsible2nd_1 = QCAReqLine2.BillingResponsibleBy;
                    result.PaymentMethod2nd_1 = QCAReqLine2.MethodOfPayment;
                    result.BusinessSegment2nd_2 = QCAReqLine2.BusinessSegment;
                    result.BuyerNameCARequestLine2nd_1 = QCAReqLine2.BuyerName;
                    result.BuyerTaxID2nd_1 = QCAReqLine2.TaxId;
                    result.BuyerAddress2nd_1 = QCAReqLine2.InvoiceAddress;
                    result.BuyerCompanyRegisteredDate2nd_1 = QCAReqLine2.DateOfEstablish;
                    result.BuyerLineOfBusiness2nd_1 = QCAReqLine2.LineOfBusiness;
                    result.BuyerBlacklistStatus2nd_1 = QCAReqLine2.BlacklistStatus;
                    result.BuyerCreditScoring2nd_1 = QCAReqLine2.CreditScoring;
                    result.BuyerCreditLimitRequest2nd_2 = QCAReqLine2.CreditLimitLineRequest;
                    result.BuyerCreditLimitAllCustomer2nd_2 = QCAReqLine2.BuyerCreditLimit;
                    result.PurchasePercent2nd_2 = QCAReqLine2.MaxPurchasePct;
                    result.AssignmentCondition2nd_2 = QCAReqLine2.AssignmentMethodRemark;
                    result.FactoringPurchaseFee2nd_1 = QCAReqLine2.PurchaseFeePct;
                    result.FactoringPurchaseFeeCalBase2nd_1 = SystemStaticData.GetTranslatedMessage(((PurchaseFeeCalculateBase)QCAReqLine2.PurchaseFeeCalculateBase).GetAttrCode());
                    result.BillingResponsible2nd_2 = QCAReqLine2.BillingResponsibleBy;
                    result.BillingDescriptiong2nd_1 = QCAReqLine2.BillingDescription;
                    result.BuyerBillingContactPerson2nd_1 = QCAReqLine2.BillingContactPersonName;
                    result.BuyerBillingContactPersonPosition2nd_1 = QCAReqLine2.BillingContactPersonPosition;
                    result.BuyerBillingAddress2nd_1 = QCAReqLine2.BuyerBillingAddress;
                    result.BuyerBillingContactPersonTel2nd_1 = QCAReqLine2.BillingContactPersonPhone;
                    result.ReceiptDescriptiong2nd_1 = QCAReqLine2.ReceiptDescription;
                    result.PaymentMethod2nd_2 = QCAReqLine2.MethodOfPayment;
                    result.BuyerReceiptContactPerson2nd_1 = QCAReqLine2.ReceiptContactPersonName;
                    result.BuyerReceiptContactPersonPosition2nd_1 = QCAReqLine2.ReceiptContactPersonPosition;
                    result.BuyerReceiptAddress2nd_1 = QCAReqLine2.BuyerReceiptAddress;
                    result.BuyerReceiptContactPersonTel2nd_1 = QCAReqLine2.ReceiptContactPersonPhone;
                    result.MarketingComment2nd = QCAReqLine2.MarketingComment;
                    result.CreditComment2nd = QCAReqLine2.CreditComment;
                    result.ApproverComment2nd = QCAReqLine2.ApproverComment;
                    result.AcceptanceDocument2nd_1 = QCAReqLine2.AcceptanceDocument == true ? TextConstants.Yes : TextConstants.No;
                    //R03
                    result.AROutstandingAllCustomer_2 = QCAReqLine2.AllCustomerBuyerOutstanding;
                    result.PurchaseConditionCust_2 = QCAReqLine2.LineCondition;
                    //R03

                    #region QCreditAppReqLineFin
                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine2.ListFinancial, 0))
                    {
                        result.BuyerFinYearFirst1_2nd_1 = QCAReqLine2.ListFinancial[0].Year;
                        result.BuyerFinRegisteredCapitalFirst1_2nd_1 = QCAReqLine2.ListFinancial[0].RegisteredCapital;
                        result.BuyerFinPaidCapitalFirst1_2nd_1 = QCAReqLine2.ListFinancial[0].PaidCapital;
                        result.BuyerFinTotalAssetFirst1_2nd_1 = QCAReqLine2.ListFinancial[0].TotalAsset;
                        result.BuyerFinTotalLiabilityFirst1_2nd_1 = QCAReqLine2.ListFinancial[0].TotalLiability;
                        result.BuyerFinTotalEquityFirst1_2nd_1 = QCAReqLine2.ListFinancial[0].TotalEquity;
                        result.BuyerFinYearFirst2_2nd_1 = QCAReqLine2.ListFinancial[0].Year;
                        result.BuyerFinTotalRevenueFirst1_2nd_1 = QCAReqLine2.ListFinancial[0].TotalRevenue;
                        result.BuyerFinTotalCOGSFirst1_2nd_1 = QCAReqLine2.ListFinancial[0].TotalCOGS;
                        result.BuyerFinTotalGrossProfitFirst1_2nd_1 = QCAReqLine2.ListFinancial[0].TotalGrossProfit;
                        result.BuyerFinTotalOperExpFirst1_2nd_1 = QCAReqLine2.ListFinancial[0].TotalOperExp;
                        result.BuyerFinTotalNetProfitFirst1_2nd_1 = QCAReqLine2.ListFinancial[0].TotalNetProfit;
                        result.BuyerFinNetProfitPercent1_2nd_1 = QCAReqLine2.ListFinancial[0].NetProfitPercent;
                        result.BuyerFinlDE1_2nd_1 = QCAReqLine2.ListFinancial[0].lDE;
                        result.BuyerFinQuickRatio1_2nd_1 = QCAReqLine2.ListFinancial[0].QuickRatio;
                        result.BuyerFinIntCoverageRatio1_2nd_1 = QCAReqLine2.ListFinancial[0].IntCoverageRatio;
                    }

                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine2.ListFinancial, 1))
                    {
                        result.BuyerFinYearSecond1_2nd_1 = QCAReqLine2.ListFinancial[1].Year;
                        result.BuyerFinRegisteredCapitalSecond1_2nd_1 = QCAReqLine2.ListFinancial[1].RegisteredCapital;
                        result.BuyerFinPaidCapitalSecond1_2nd_1 = QCAReqLine2.ListFinancial[1].PaidCapital;
                        result.BuyerFinTotalAssetSecond1_2nd_1 = QCAReqLine2.ListFinancial[1].TotalAsset;
                        result.BuyerFinTotalLiabilitySecond1_2nd_1 = QCAReqLine2.ListFinancial[1].TotalLiability;
                        result.BuyerFinTotalEquitySecond1_2nd_1 = QCAReqLine2.ListFinancial[1].TotalEquity;
                        result.BuyerFinYearSecond2_2nd_1 = QCAReqLine2.ListFinancial[1].Year;
                        result.BuyerFinTotalRevenueSecond1_2nd_1 = QCAReqLine2.ListFinancial[1].TotalRevenue;
                        result.BuyerFinTotalCOGSSecond1_2nd_1 = QCAReqLine2.ListFinancial[1].TotalCOGS;
                        result.BuyerFinTotalGrossProfitSecond1_2nd_1 = QCAReqLine2.ListFinancial[1].TotalGrossProfit;
                        result.BuyerFinTotalOperExpSecond1_2nd_1 = QCAReqLine2.ListFinancial[1].TotalOperExp;
                        result.BuyerFinTotalNetProfitSecond1_2nd_1 = QCAReqLine2.ListFinancial[1].TotalNetProfit;
                    }

                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine2.ListFinancial, 2))
                    {
                        result.BuyerFinYearThird1_2nd_1 = QCAReqLine2.ListFinancial[2].Year;
                        result.BuyerFinRegisteredCapitalThird1_2nd_1 = QCAReqLine2.ListFinancial[2].RegisteredCapital;
                        result.BuyerFinPaidCapitalThird1_2nd_1 = QCAReqLine2.ListFinancial[2].PaidCapital;
                        result.BuyerFinTotalAssetThird1_2nd_1 = QCAReqLine2.ListFinancial[2].TotalAsset;
                        result.BuyerFinTotalLiabilityThird1_2nd_1 = QCAReqLine2.ListFinancial[2].TotalLiability;
                        result.BuyerFinTotalEquityThird1_2nd_1 = QCAReqLine2.ListFinancial[2].TotalEquity;
                        result.BuyerFinYearThird2_2nd_1 = QCAReqLine2.ListFinancial[2].Year;
                        result.BuyerFinTotalRevenueThird1_2nd_1 = QCAReqLine2.ListFinancial[2].TotalRevenue;
                        result.BuyerFinTotalCOGSThird1_2nd_1 = QCAReqLine2.ListFinancial[2].TotalCOGS;
                        result.BuyerFinTotalGrossProfitThird1_2nd_1 = QCAReqLine2.ListFinancial[2].TotalGrossProfit;
                        result.BuyerFinTotalOperExpThird1_2nd_1 = QCAReqLine2.ListFinancial[2].TotalOperExp;
                        result.BuyerFinTotalNetProfitThird1_2nd_1 = QCAReqLine2.ListFinancial[2].TotalNetProfit;
                    }
                    #endregion QCreditAppReqLineFin

                    #region QDocumentCondition - Billing
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine2.ListBillingDocumentCondition, 0)) result.BillingDocumentFirst1_2nd_1 = QCAReqLine2.ListBillingDocumentCondition[0].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine2.ListBillingDocumentCondition, 1)) result.BillingDocumentSecond1_2nd_1 = QCAReqLine2.ListBillingDocumentCondition[1].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine2.ListBillingDocumentCondition, 2)) result.BillingDocumentThird1_2nd_1 = QCAReqLine2.ListBillingDocumentCondition[2].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine2.ListBillingDocumentCondition, 3)) result.BillingDocumentFourth_2nd_1 = QCAReqLine2.ListBillingDocumentCondition[3].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine2.ListBillingDocumentCondition, 4)) result.BillingDocumentFifth_2nd_1 = QCAReqLine2.ListBillingDocumentCondition[4].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine2.ListBillingDocumentCondition, 5)) result.BillingDocumentSixth1_2nd_1 = QCAReqLine2.ListBillingDocumentCondition[5].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine2.ListBillingDocumentCondition, 6)) result.BillingDocumentSeventh_2nd_1 = QCAReqLine2.ListBillingDocumentCondition[6].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine2.ListBillingDocumentCondition, 7)) result.BillingDocumentEigth1_2nd_1 = QCAReqLine2.ListBillingDocumentCondition[7].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine2.ListBillingDocumentCondition, 8)) result.BillingDocumentNineth1_2nd_1 = QCAReqLine2.ListBillingDocumentCondition[8].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine2.ListBillingDocumentCondition, 9)) result.BillingDocumentTenth_2nd_1 = QCAReqLine2.ListBillingDocumentCondition[9].Description;
                    #endregion QDocumentCondition - Billing

                    #region QDocumentCondition - Receipt
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine2.ListReceiptDocumentCondition, 0)) result.ReceiptDocumentFirst1_2nd_1 = QCAReqLine2.ListReceiptDocumentCondition[0].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine2.ListReceiptDocumentCondition, 1)) result.ReceiptDocumentSecond1_2nd_1 = QCAReqLine2.ListReceiptDocumentCondition[1].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine2.ListReceiptDocumentCondition, 2)) result.ReceiptDocumentThird1_2nd_1 = QCAReqLine2.ListReceiptDocumentCondition[2].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine2.ListReceiptDocumentCondition, 3)) result.ReceiptDocumentFourth_2nd_1 = QCAReqLine2.ListReceiptDocumentCondition[3].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine2.ListReceiptDocumentCondition, 4)) result.ReceiptDocumentFifth_2nd_1 = QCAReqLine2.ListReceiptDocumentCondition[4].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine2.ListReceiptDocumentCondition, 5)) result.ReceiptDocumentSixth1_2nd_1 = QCAReqLine2.ListReceiptDocumentCondition[5].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine2.ListReceiptDocumentCondition, 6)) result.ReceiptDocumentSeventh_2nd_1 = QCAReqLine2.ListReceiptDocumentCondition[6].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine2.ListReceiptDocumentCondition, 7)) result.ReceiptDocumentEigth1_2nd_1 = QCAReqLine2.ListReceiptDocumentCondition[7].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine2.ListReceiptDocumentCondition, 8)) result.ReceiptDocumentNineth1_2nd_1 = QCAReqLine2.ListReceiptDocumentCondition[8].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine2.ListReceiptDocumentCondition, 9)) result.ReceiptDocumentTenth_2nd_1 = QCAReqLine2.ListReceiptDocumentCondition[9].Description;
                    #endregion QDocumentCondition - Receipt

                    #region QServiceFeeCondition
                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine2.ListServiceFeeCondition, 0))
                    {
                        result.ServiceFeeDesc01_2nd_1 = QCAReqLine2.ListServiceFeeCondition[0].ServicefeeType;
                        result.ServiceFeeAmt01_2nd_1 = QCAReqLine2.ListServiceFeeCondition[0].AmountBeforeTax;
                        result.ServiceFeeRemark01_2nd_1 = QCAReqLine2.ListServiceFeeCondition[0].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine2.ListServiceFeeCondition, 1))
                    {
                        result.ServiceFeeDesc02_2nd_1 = QCAReqLine2.ListServiceFeeCondition[1].ServicefeeType;
                        result.ServiceFeeAmt02_2nd_1 = QCAReqLine2.ListServiceFeeCondition[1].AmountBeforeTax;
                        result.ServiceFeeRemark02_2nd_1 = QCAReqLine2.ListServiceFeeCondition[1].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine2.ListServiceFeeCondition, 2))
                    {
                        result.ServiceFeeDesc03_2nd_1 = QCAReqLine2.ListServiceFeeCondition[2].ServicefeeType;
                        result.ServiceFeeAmt03_2nd_1 = QCAReqLine2.ListServiceFeeCondition[2].AmountBeforeTax;
                        result.ServiceFeeRemark03_2nd_1 = QCAReqLine2.ListServiceFeeCondition[2].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine2.ListServiceFeeCondition, 3))
                    {
                        result.ServiceFeeDesc04_2nd_1 = QCAReqLine2.ListServiceFeeCondition[3].ServicefeeType;
                        result.ServiceFeeAmt04_2nd_1 = QCAReqLine2.ListServiceFeeCondition[3].AmountBeforeTax;
                        result.ServiceFeeRemark04_2nd_1 = QCAReqLine2.ListServiceFeeCondition[3].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine2.ListServiceFeeCondition, 4))
                    {
                        result.ServiceFeeDesc05_2nd_1 = QCAReqLine2.ListServiceFeeCondition[4].ServicefeeType;
                        result.ServiceFeeAmt05_2nd_1 = QCAReqLine2.ListServiceFeeCondition[4].AmountBeforeTax;
                        result.ServiceFeeRemark05_2nd_1 = QCAReqLine2.ListServiceFeeCondition[4].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine2.ListServiceFeeCondition, 5))
                    {
                        result.ServiceFeeDesc06_2nd_1 = QCAReqLine2.ListServiceFeeCondition[5].ServicefeeType;
                        result.ServiceFeeAmt06_2nd_1 = QCAReqLine2.ListServiceFeeCondition[5].AmountBeforeTax;
                        result.ServiceFeeRemark06_2nd_1 = QCAReqLine2.ListServiceFeeCondition[5].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine2.ListServiceFeeCondition, 6))
                    {
                        result.ServiceFeeDesc07_2nd_1 = QCAReqLine2.ListServiceFeeCondition[6].ServicefeeType;
                        result.ServiceFeeAmt07_2nd_1 = QCAReqLine2.ListServiceFeeCondition[6].AmountBeforeTax;
                        result.ServiceFeeRemark07_2nd_1 = QCAReqLine2.ListServiceFeeCondition[6].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine2.ListServiceFeeCondition, 7))
                    {
                        result.ServiceFeeDesc08_2nd_1 = QCAReqLine2.ListServiceFeeCondition[7].ServicefeeType;
                        result.ServiceFeeAmt08_2nd_1 = QCAReqLine2.ListServiceFeeCondition[7].AmountBeforeTax;
                        result.ServiceFeeRemark08_2nd_1 = QCAReqLine2.ListServiceFeeCondition[7].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine2.ListServiceFeeCondition, 8))
                    {
                        result.ServiceFeeDesc09_2nd_1 = QCAReqLine2.ListServiceFeeCondition[8].ServicefeeType;
                        result.ServiceFeeAmt09_2nd_1 = QCAReqLine2.ListServiceFeeCondition[8].AmountBeforeTax;
                        result.ServiceFeeRemark09_2nd_1 = QCAReqLine2.ListServiceFeeCondition[8].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine2.ListServiceFeeCondition, 9))
                    {
                        result.ServiceFeeDesc10_2nd_1 = QCAReqLine2.ListServiceFeeCondition[9].ServicefeeType;
                        result.ServiceFeeAmt10_2nd_1 = QCAReqLine2.ListServiceFeeCondition[9].AmountBeforeTax;
                        result.ServiceFeeRemark10_2nd_1 = QCAReqLine2.ListServiceFeeCondition[9].Description;
                    }
                    #endregion QServiceFeeCondition

                    result.SumServiceFeeAmt_2nd_1 = QCAReqLine2.SumServiceFeeCondition;
                }
                #endregion QCAReqLine2
                #region QCAReqLine3
                if (CheckQCAReqLineElementIsNotNull(QCAReqLineList, 2))
                {
                    QCAReqLine QCAReqLine3 = QCAReqLineList[2];
                    result.BuyerName3rd_1 = QCAReqLine3.BuyerName;
                    result.BuyerCreditLimitRequest3rd_1 = QCAReqLine3.CreditLimitLineRequest;
                    result.BuyerCreditLimitAllCustomer3rd_1 = QCAReqLine3.BuyerCreditLimit;
                    result.PurchasePercent3rd_1 = QCAReqLine3.MaxPurchasePct;
                    result.AssignmentCondition3rd_1 = QCAReqLine3.AssignmentMethodRemark;
                    result.BillingResponsible3rd_1 = QCAReqLine3.BillingResponsibleBy;
                    result.PaymentMethod3rd_1 = QCAReqLine3.MethodOfPayment;
                    result.BusinessSegment3rd_2 = QCAReqLine3.BusinessSegment;
                    result.BuyerNameCARequestLine3rd_1 = QCAReqLine3.BuyerName;
                    result.BuyerTaxID3rd_1 = QCAReqLine3.TaxId;
                    result.BuyerAddress3rd_1 = QCAReqLine3.InvoiceAddress;
                    result.BuyerCompanyRegisteredDate3rd_1 = QCAReqLine3.DateOfEstablish;
                    result.BuyerLineOfBusiness3rd_1 = QCAReqLine3.LineOfBusiness;
                    result.BuyerBlacklistStatus3rd_1 = QCAReqLine3.BlacklistStatus;
                    result.BuyerCreditScoring3rd_1 = QCAReqLine3.CreditScoring;
                    result.BuyerCreditLimitRequest3rd_2 = QCAReqLine3.CreditLimitLineRequest;
                    result.BuyerCreditLimitAllCustomer3rd_2 = QCAReqLine3.BuyerCreditLimit;
                    result.PurchasePercent3rd_2 = QCAReqLine3.MaxPurchasePct;
                    result.AssignmentCondition3rd_2 = QCAReqLine3.AssignmentMethodRemark;
                    result.FactoringPurchaseFee3rd_1 = QCAReqLine3.PurchaseFeePct;
                    result.FactoringPurchaseFeeCalBase3rd_1 = SystemStaticData.GetTranslatedMessage(((PurchaseFeeCalculateBase)QCAReqLine3.PurchaseFeeCalculateBase).GetAttrCode());
                    result.BillingResponsible3rd_2 = QCAReqLine3.BillingResponsibleBy;
                    result.BillingDescriptiong3rd_1 = QCAReqLine3.BillingDescription;
                    result.BuyerBillingContactPerson3rd_1 = QCAReqLine3.BillingContactPersonName;
                    result.BuyerBillingContactPersonPosition3rd_1 = QCAReqLine3.BillingContactPersonPosition;
                    result.BuyerBillingAddress3rd_1 = QCAReqLine3.BuyerBillingAddress;
                    result.BuyerBillingContactPersonTel3rd_1 = QCAReqLine3.BillingContactPersonPhone;
                    result.ReceiptDescriptiong3rd_1 = QCAReqLine3.ReceiptDescription;
                    result.PaymentMethod3rd_2 = QCAReqLine3.MethodOfPayment;
                    result.BuyerReceiptContactPerson3rd_1 = QCAReqLine3.ReceiptContactPersonName;
                    result.BuyerReceiptContactPersonPosition3rd_1 = QCAReqLine3.ReceiptContactPersonPosition;
                    result.BuyerReceiptAddress3rd_1 = QCAReqLine3.BuyerReceiptAddress;
                    result.BuyerReceiptContactPersonTel3rd_1 = QCAReqLine3.ReceiptContactPersonPhone;
                    result.MarketingComment3rd = QCAReqLine3.MarketingComment;
                    result.CreditComment3rd = QCAReqLine3.CreditComment;
                    result.ApproverComment3rd = QCAReqLine3.ApproverComment;
                    result.AcceptanceDocument3rd_1 = QCAReqLine3.AcceptanceDocument == true ? TextConstants.Yes : TextConstants.No;
                    //R03
                    result.AROutstandingAllCustomer_3 = QCAReqLine3.AllCustomerBuyerOutstanding; 
                    result.PurchaseConditionCust_3 = QCAReqLine3.LineCondition;
                    //R03

                    #region QCreditAppReqLineFin
                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine3.ListFinancial, 0))
                    {
                        result.BuyerFinYearFirst1_3rd_1 = QCAReqLine3.ListFinancial[0].Year;
                        result.BuyerFinRegisteredCapitalFirst1_3rd_1 = QCAReqLine3.ListFinancial[0].RegisteredCapital;
                        result.BuyerFinPaidCapitalFirst1_3rd_1 = QCAReqLine3.ListFinancial[0].PaidCapital;
                        result.BuyerFinTotalAssetFirst1_3rd_1 = QCAReqLine3.ListFinancial[0].TotalAsset;
                        result.BuyerFinTotalLiabilityFirst1_3rd_1 = QCAReqLine3.ListFinancial[0].TotalLiability;
                        result.BuyerFinTotalEquityFirst1_3rd_1 = QCAReqLine3.ListFinancial[0].TotalEquity;
                        result.BuyerFinYearFirst2_3rd_1 = QCAReqLine3.ListFinancial[0].Year;
                        result.BuyerFinTotalRevenueFirst1_3rd_1 = QCAReqLine3.ListFinancial[0].TotalRevenue;
                        result.BuyerFinTotalCOGSFirst1_3rd_1 = QCAReqLine3.ListFinancial[0].TotalCOGS;
                        result.BuyerFinTotalGrossProfitFirst1_3rd_1 = QCAReqLine3.ListFinancial[0].TotalGrossProfit;
                        result.BuyerFinTotalOperExpFirst1_3rd_1 = QCAReqLine3.ListFinancial[0].TotalOperExp;
                        result.BuyerFinTotalNetProfitFirst1_3rd_1 = QCAReqLine3.ListFinancial[0].TotalNetProfit;
                        result.BuyerFinNetProfitPercent1_3rd_1 = QCAReqLine3.ListFinancial[0].NetProfitPercent;
                        result.BuyerFinlDE1_3rd_1 = QCAReqLine3.ListFinancial[0].lDE;
                        result.BuyerFinQuickRatio1_3rd_1 = QCAReqLine3.ListFinancial[0].QuickRatio;
                        result.BuyerFinIntCoverageRatio1_3rd_1 = QCAReqLine3.ListFinancial[0].IntCoverageRatio;
                    }

                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine3.ListFinancial, 1))
                    {
                        result.BuyerFinYearSecond1_3rd_1 = QCAReqLine3.ListFinancial[1].Year;
                        result.BuyerFinRegisteredCapitalSecond1_3rd_1 = QCAReqLine3.ListFinancial[1].RegisteredCapital;
                        result.BuyerFinPaidCapitalSecond1_3rd_1 = QCAReqLine3.ListFinancial[1].PaidCapital;
                        result.BuyerFinTotalAssetSecond1_3rd_1 = QCAReqLine3.ListFinancial[1].TotalAsset;
                        result.BuyerFinTotalLiabilitySecond1_3rd_1 = QCAReqLine3.ListFinancial[1].TotalLiability;
                        result.BuyerFinTotalEquitySecond1_3rd_1 = QCAReqLine3.ListFinancial[1].TotalEquity;
                        result.BuyerFinYearSecond2_3rd_1 = QCAReqLine3.ListFinancial[1].Year;
                        result.BuyerFinTotalRevenueSecond1_3rd_1 = QCAReqLine3.ListFinancial[1].TotalRevenue;
                        result.BuyerFinTotalCOGSSecond1_3rd_1 = QCAReqLine3.ListFinancial[1].TotalCOGS;
                        result.BuyerFinTotalGrossProfitSecond1_3rd_1 = QCAReqLine3.ListFinancial[1].TotalGrossProfit;
                        result.BuyerFinTotalOperExpSecond1_3rd_1 = QCAReqLine3.ListFinancial[1].TotalOperExp;
                        result.BuyerFinTotalNetProfitSecond1_3rd_1 = QCAReqLine3.ListFinancial[1].TotalNetProfit;
                    }

                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine3.ListFinancial, 2))
                    {
                        result.BuyerFinYearThird1_3rd_1 = QCAReqLine3.ListFinancial[2].Year;
                        result.BuyerFinRegisteredCapitalThird1_3rd_1 = QCAReqLine3.ListFinancial[2].RegisteredCapital;
                        result.BuyerFinPaidCapitalThird1_3rd_1 = QCAReqLine3.ListFinancial[2].PaidCapital;
                        result.BuyerFinTotalAssetThird1_3rd_1 = QCAReqLine3.ListFinancial[2].TotalAsset;
                        result.BuyerFinTotalLiabilityThird1_3rd_1 = QCAReqLine3.ListFinancial[2].TotalLiability;
                        result.BuyerFinTotalEquityThird1_3rd_1 = QCAReqLine3.ListFinancial[2].TotalEquity;
                        result.BuyerFinYearThird2_3rd_1 = QCAReqLine3.ListFinancial[2].Year;
                        result.BuyerFinTotalRevenueThird1_3rd_1 = QCAReqLine3.ListFinancial[2].TotalRevenue;
                        result.BuyerFinTotalCOGSThird1_3rd_1 = QCAReqLine3.ListFinancial[2].TotalCOGS;
                        result.BuyerFinTotalGrossProfitThird1_3rd_1 = QCAReqLine3.ListFinancial[2].TotalGrossProfit;
                        result.BuyerFinTotalOperExpThird1_3rd_1 = QCAReqLine3.ListFinancial[2].TotalOperExp;
                        result.BuyerFinTotalNetProfitThird1_3rd_1 = QCAReqLine3.ListFinancial[2].TotalNetProfit;

                    }
                    #endregion QCreditAppReqLineFin

                    #region QDocumentCondition - Billing
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine3.ListBillingDocumentCondition, 0)) result.BillingDocumentFirst1_3rd_1 = QCAReqLine3.ListBillingDocumentCondition[0].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine3.ListBillingDocumentCondition, 1)) result.BillingDocumentSecond1_3rd_1 = QCAReqLine3.ListBillingDocumentCondition[1].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine3.ListBillingDocumentCondition, 2)) result.BillingDocumentThird1_3rd_1 = QCAReqLine3.ListBillingDocumentCondition[2].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine3.ListBillingDocumentCondition, 3)) result.BillingDocumentFourth_3rd_1 = QCAReqLine3.ListBillingDocumentCondition[3].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine3.ListBillingDocumentCondition, 4)) result.BillingDocumentFifth_3rd_1 = QCAReqLine3.ListBillingDocumentCondition[4].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine3.ListBillingDocumentCondition, 5)) result.BillingDocumentSixth1_3rd_1 = QCAReqLine3.ListBillingDocumentCondition[5].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine3.ListBillingDocumentCondition, 6)) result.BillingDocumentSeventh_3rd_1 = QCAReqLine3.ListBillingDocumentCondition[6].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine3.ListBillingDocumentCondition, 7)) result.BillingDocumentEigth1_3rd_1 = QCAReqLine3.ListBillingDocumentCondition[7].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine3.ListBillingDocumentCondition, 8)) result.BillingDocumentNineth1_3rd_1 = QCAReqLine3.ListBillingDocumentCondition[8].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine3.ListBillingDocumentCondition, 9)) result.BillingDocumentTenth_3rd_1 = QCAReqLine3.ListBillingDocumentCondition[9].Description;
                    #endregion QDocumentCondition - Billing

                    #region QDocumentCondition - Receipt
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine3.ListReceiptDocumentCondition, 0)) result.ReceiptDocumentFirst1_3rd_1 = QCAReqLine3.ListReceiptDocumentCondition[0].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine3.ListReceiptDocumentCondition, 1)) result.ReceiptDocumentSecond1_3rd_1 = QCAReqLine3.ListReceiptDocumentCondition[1].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine3.ListReceiptDocumentCondition, 2)) result.ReceiptDocumentThird1_3rd_1 = QCAReqLine3.ListReceiptDocumentCondition[2].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine3.ListReceiptDocumentCondition, 3)) result.ReceiptDocumentFourth_3rd_1 = QCAReqLine3.ListReceiptDocumentCondition[3].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine3.ListReceiptDocumentCondition, 4)) result.ReceiptDocumentFifth_3rd_1 = QCAReqLine3.ListReceiptDocumentCondition[4].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine3.ListReceiptDocumentCondition, 5)) result.ReceiptDocumentSixth1_3rd_1 = QCAReqLine3.ListReceiptDocumentCondition[5].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine3.ListReceiptDocumentCondition, 6)) result.ReceiptDocumentSeventh_3rd_1 = QCAReqLine3.ListReceiptDocumentCondition[6].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine3.ListReceiptDocumentCondition, 7)) result.ReceiptDocumentEigth1_3rd_1 = QCAReqLine3.ListReceiptDocumentCondition[7].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine3.ListReceiptDocumentCondition, 8)) result.ReceiptDocumentNineth1_3rd_1 = QCAReqLine3.ListReceiptDocumentCondition[8].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine3.ListReceiptDocumentCondition, 9)) result.ReceiptDocumentTenth_3rd_1 = QCAReqLine3.ListReceiptDocumentCondition[9].Description;
                    #endregion QDocumentCondition - Receipt

                    #region QServiceFeeCondition
                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine3.ListServiceFeeCondition, 0))
                    {
                        result.ServiceFeeDesc01_3rd_1 = QCAReqLine3.ListServiceFeeCondition[0].ServicefeeType;
                        result.ServiceFeeAmt01_3rd_1 = QCAReqLine3.ListServiceFeeCondition[0].AmountBeforeTax;
                        result.ServiceFeeRemark01_3rd_1 = QCAReqLine3.ListServiceFeeCondition[0].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine3.ListServiceFeeCondition, 1))
                    {
                        result.ServiceFeeDesc02_3rd_1 = QCAReqLine3.ListServiceFeeCondition[1].ServicefeeType;
                        result.ServiceFeeAmt02_3rd_1 = QCAReqLine3.ListServiceFeeCondition[1].AmountBeforeTax;
                        result.ServiceFeeRemark02_3rd_1 = QCAReqLine3.ListServiceFeeCondition[1].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine3.ListServiceFeeCondition, 2))
                    {
                        result.ServiceFeeDesc03_3rd_1 = QCAReqLine3.ListServiceFeeCondition[2].ServicefeeType;
                        result.ServiceFeeAmt03_3rd_1 = QCAReqLine3.ListServiceFeeCondition[2].AmountBeforeTax;
                        result.ServiceFeeRemark03_3rd_1 = QCAReqLine3.ListServiceFeeCondition[2].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine3.ListServiceFeeCondition, 3))
                    {
                        result.ServiceFeeDesc04_3rd_1 = QCAReqLine3.ListServiceFeeCondition[3].ServicefeeType;
                        result.ServiceFeeAmt04_3rd_1 = QCAReqLine3.ListServiceFeeCondition[3].AmountBeforeTax;
                        result.ServiceFeeRemark04_3rd_1 = QCAReqLine3.ListServiceFeeCondition[3].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine3.ListServiceFeeCondition, 4))
                    {
                        result.ServiceFeeDesc05_3rd_1 = QCAReqLine3.ListServiceFeeCondition[4].ServicefeeType;
                        result.ServiceFeeAmt05_3rd_1 = QCAReqLine3.ListServiceFeeCondition[4].AmountBeforeTax;
                        result.ServiceFeeRemark05_3rd_1 = QCAReqLine3.ListServiceFeeCondition[4].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine3.ListServiceFeeCondition, 5))
                    {
                        result.ServiceFeeDesc06_3rd_1 = QCAReqLine3.ListServiceFeeCondition[5].ServicefeeType;
                        result.ServiceFeeAmt06_3rd_1 = QCAReqLine3.ListServiceFeeCondition[5].AmountBeforeTax;
                        result.ServiceFeeRemark06_3rd_1 = QCAReqLine3.ListServiceFeeCondition[5].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine3.ListServiceFeeCondition, 6))
                    {
                        result.ServiceFeeDesc07_3rd_1 = QCAReqLine3.ListServiceFeeCondition[6].ServicefeeType;
                        result.ServiceFeeAmt07_3rd_1 = QCAReqLine3.ListServiceFeeCondition[6].AmountBeforeTax;
                        result.ServiceFeeRemark07_3rd_1 = QCAReqLine3.ListServiceFeeCondition[6].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine3.ListServiceFeeCondition, 7))
                    {
                        result.ServiceFeeDesc08_3rd_1 = QCAReqLine3.ListServiceFeeCondition[7].ServicefeeType;
                        result.ServiceFeeAmt08_3rd_1 = QCAReqLine3.ListServiceFeeCondition[7].AmountBeforeTax;
                        result.ServiceFeeRemark08_3rd_1 = QCAReqLine3.ListServiceFeeCondition[7].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine3.ListServiceFeeCondition, 8))
                    {
                        result.ServiceFeeDesc09_3rd_1 = QCAReqLine3.ListServiceFeeCondition[8].ServicefeeType;
                        result.ServiceFeeAmt09_3rd_1 = QCAReqLine3.ListServiceFeeCondition[8].AmountBeforeTax;
                        result.ServiceFeeRemark09_3rd_1 = QCAReqLine3.ListServiceFeeCondition[8].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine3.ListServiceFeeCondition, 9))
                    {
                        result.ServiceFeeDesc10_3rd_1 = QCAReqLine3.ListServiceFeeCondition[9].ServicefeeType;
                        result.ServiceFeeAmt10_3rd_1 = QCAReqLine3.ListServiceFeeCondition[9].AmountBeforeTax;
                        result.ServiceFeeRemark10_3rd_1 = QCAReqLine3.ListServiceFeeCondition[9].Description;
                    }
                    #endregion QServiceFeeCondition

                    result.SumServiceFeeAmt_3rd_1 = QCAReqLine3.SumServiceFeeCondition;
                }
                #endregion QCAReqLine3
                #region QCAReqLine4
                if (CheckQCAReqLineElementIsNotNull(QCAReqLineList, 3))
                {
                    QCAReqLine QCAReqLine4 = QCAReqLineList[3];
                    result.BuyerName4th_1 = QCAReqLine4.BuyerName;
                    result.BuyerCreditLimitRequest4th_1 = QCAReqLine4.CreditLimitLineRequest;
                    result.BuyerCreditLimitAllCustomer4th_1 = QCAReqLine4.BuyerCreditLimit;
                    result.PurchasePercent4th_1 = QCAReqLine4.MaxPurchasePct;
                    result.AssignmentCondition4th_1 = QCAReqLine4.AssignmentMethodRemark;
                    result.BillingResponsible4th_1 = QCAReqLine4.BillingResponsibleBy;
                    result.PaymentMethod4th_1 = QCAReqLine4.MethodOfPayment;
                    result.BusinessSegment4th_2 = QCAReqLine4.BusinessSegment;
                    result.BuyerNameCARequestLine4th_1 = QCAReqLine4.BuyerName;
                    result.BuyerTaxID4th_1 = QCAReqLine4.TaxId;
                    result.BuyerAddress4th_1 = QCAReqLine4.InvoiceAddress;
                    result.BuyerCompanyRegisteredDate4th_1 = QCAReqLine4.DateOfEstablish;
                    result.BuyerLineOfBusiness4th_1 = QCAReqLine4.LineOfBusiness;
                    result.BuyerBlacklistStatus4th_1 = QCAReqLine4.BlacklistStatus;
                    result.BuyerCreditScoring4th_1 = QCAReqLine4.CreditScoring;
                    result.BuyerCreditLimitRequest4th_2 = QCAReqLine4.CreditLimitLineRequest;
                    result.BuyerCreditLimitAllCustomer4th_2 = QCAReqLine4.BuyerCreditLimit;
                    result.PurchasePercent4th_2 = QCAReqLine4.MaxPurchasePct;
                    result.AssignmentCondition4th_2 = QCAReqLine4.AssignmentMethodRemark;
                    result.FactoringPurchaseFee4th_1 = QCAReqLine4.PurchaseFeePct;
                    result.FactoringPurchaseFeeCalBase4th_1 = SystemStaticData.GetTranslatedMessage(((PurchaseFeeCalculateBase)QCAReqLine4.PurchaseFeeCalculateBase).GetAttrCode());
                    result.BillingResponsible4th_2 = QCAReqLine4.BillingResponsibleBy;
                    result.BillingDescriptiong4th_1 = QCAReqLine4.BillingDescription;
                    result.BuyerBillingContactPerson4th_1 = QCAReqLine4.BillingContactPersonName;
                    result.BuyerBillingContactPersonPosition4th_1 = QCAReqLine4.BillingContactPersonPosition;
                    result.BuyerBillingAddress4th_1 = QCAReqLine4.BuyerBillingAddress;
                    result.BuyerBillingContactPersonTel4th_1 = QCAReqLine4.BillingContactPersonPhone;
                    result.ReceiptDescriptiong4th_1 = QCAReqLine4.ReceiptDescription;
                    result.PaymentMethod4th_2 = QCAReqLine4.MethodOfPayment;
                    result.BuyerReceiptContactPerson4th_1 = QCAReqLine4.ReceiptContactPersonName;
                    result.BuyerReceiptContactPersonPosition4th_1 = QCAReqLine4.ReceiptContactPersonPosition;
                    result.BuyerReceiptAddress4th_1 = QCAReqLine4.BuyerReceiptAddress;
                    result.BuyerReceiptContactPersonTel4th_1 = QCAReqLine4.ReceiptContactPersonPhone;
                    result.MarketingComment4th = QCAReqLine4.MarketingComment;
                    result.CreditComment4th = QCAReqLine4.CreditComment;
                    result.ApproverComment4th = QCAReqLine4.ApproverComment;
                    result.AcceptanceDocument4th_1 = QCAReqLine4.AcceptanceDocument == true ? TextConstants.Yes : TextConstants.No;
                    //R03
                    result.AROutstandingAllCustomer_4 = QCAReqLine4.AllCustomerBuyerOutstanding;
                    result.PurchaseConditionCust_4 = QCAReqLine4.LineCondition;
                    //R03

                    #region QCreditAppReqLineFin
                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine4.ListFinancial, 0))
                    {
                        result.BuyerFinYearFirst1_4th_1 = QCAReqLine4.ListFinancial[0].Year;
                        result.BuyerFinRegisteredCapitalFirst1_4th_1 = QCAReqLine4.ListFinancial[0].RegisteredCapital;
                        result.BuyerFinPaidCapitalFirst1_4th_1 = QCAReqLine4.ListFinancial[0].PaidCapital;
                        result.BuyerFinTotalAssetFirst1_4th_1 = QCAReqLine4.ListFinancial[0].TotalAsset;
                        result.BuyerFinTotalLiabilityFirst1_4th_1 = QCAReqLine4.ListFinancial[0].TotalLiability;
                        result.BuyerFinTotalEquityFirst1_4th_1 = QCAReqLine4.ListFinancial[0].TotalEquity;
                        result.BuyerFinYearFirst2_4th_1 = QCAReqLine4.ListFinancial[0].Year;
                        result.BuyerFinTotalRevenueFirst1_4th_1 = QCAReqLine4.ListFinancial[0].TotalRevenue;
                        result.BuyerFinTotalCOGSFirst1_4th_1 = QCAReqLine4.ListFinancial[0].TotalCOGS;
                        result.BuyerFinTotalGrossProfitFirst1_4th_1 = QCAReqLine4.ListFinancial[0].TotalGrossProfit;
                        result.BuyerFinTotalOperExpFirst1_4th_1 = QCAReqLine4.ListFinancial[0].TotalOperExp;
                        result.BuyerFinTotalNetProfitFirst1_4th_1 = QCAReqLine4.ListFinancial[0].TotalNetProfit;
                        result.BuyerFinNetProfitPercent1_4th_1 = QCAReqLine4.ListFinancial[0].NetProfitPercent;
                        result.BuyerFinlDE1_4th_1 = QCAReqLine4.ListFinancial[0].lDE;
                        result.BuyerFinQuickRatio1_4th_1 = QCAReqLine4.ListFinancial[0].QuickRatio;
                        result.BuyerFinIntCoverageRatio1_4th_1 = QCAReqLine4.ListFinancial[0].IntCoverageRatio;
                    }

                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine4.ListFinancial, 1))
                    {
                        result.BuyerFinYearSecond1_4th_1 = QCAReqLine4.ListFinancial[1].Year;
                        result.BuyerFinRegisteredCapitalSecond1_4th_1 = QCAReqLine4.ListFinancial[1].RegisteredCapital;
                        result.BuyerFinPaidCapitalSecond1_4th_1 = QCAReqLine4.ListFinancial[1].PaidCapital;
                        result.BuyerFinTotalAssetSecond1_4th_1 = QCAReqLine4.ListFinancial[1].TotalAsset;
                        result.BuyerFinTotalLiabilitySecond1_4th_1 = QCAReqLine4.ListFinancial[1].TotalLiability;
                        result.BuyerFinTotalEquitySecond1_4th_1 = QCAReqLine4.ListFinancial[1].TotalEquity;
                        result.BuyerFinYearSecond2_4th_1 = QCAReqLine4.ListFinancial[1].Year;
                        result.BuyerFinTotalRevenueSecond1_4th_1 = QCAReqLine4.ListFinancial[1].TotalRevenue;
                        result.BuyerFinTotalCOGSSecond1_4th_1 = QCAReqLine4.ListFinancial[1].TotalCOGS;
                        result.BuyerFinTotalGrossProfitSecond1_4th_1 = QCAReqLine4.ListFinancial[1].TotalGrossProfit;
                        result.BuyerFinTotalOperExpSecond1_4th_1 = QCAReqLine4.ListFinancial[1].TotalOperExp;
                        result.BuyerFinTotalNetProfitSecond1_4th_1 = QCAReqLine4.ListFinancial[1].TotalNetProfit;
                    }

                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine4.ListFinancial, 2))
                    {
                        result.BuyerFinYearThird1_4th_1 = QCAReqLine4.ListFinancial[2].Year;
                        result.BuyerFinRegisteredCapitalThird1_4th_1 = QCAReqLine4.ListFinancial[2].RegisteredCapital;
                        result.BuyerFinPaidCapitalThird1_4th_1 = QCAReqLine4.ListFinancial[2].PaidCapital;
                        result.BuyerFinTotalAssetThird1_4th_1 = QCAReqLine4.ListFinancial[2].TotalAsset;
                        result.BuyerFinTotalLiabilityThird1_4th_1 = QCAReqLine4.ListFinancial[2].TotalLiability;
                        result.BuyerFinTotalEquityThird1_4th_1 = QCAReqLine4.ListFinancial[2].TotalEquity;
                        result.BuyerFinYearThird2_4th_1 = QCAReqLine4.ListFinancial[2].Year;
                        result.BuyerFinTotalRevenueThird1_4th_1 = QCAReqLine4.ListFinancial[2].TotalRevenue;
                        result.BuyerFinTotalCOGSThird1_4th_1 = QCAReqLine4.ListFinancial[2].TotalCOGS;
                        result.BuyerFinTotalGrossProfitThird1_4th_1 = QCAReqLine4.ListFinancial[2].TotalGrossProfit;
                        result.BuyerFinTotalOperExpThird1_4th_1 = QCAReqLine4.ListFinancial[2].TotalOperExp;
                        result.BuyerFinTotalNetProfitThird1_4th_1 = QCAReqLine4.ListFinancial[2].TotalNetProfit;
                    }
                    #endregion QCreditAppReqLineFin

                    #region QDocumentCondition - Billing
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine4.ListBillingDocumentCondition, 0)) result.BillingDocumentFirst1_4th_1 = QCAReqLine4.ListBillingDocumentCondition[0].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine4.ListBillingDocumentCondition, 1)) result.BillingDocumentSecond1_4th_1 = QCAReqLine4.ListBillingDocumentCondition[1].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine4.ListBillingDocumentCondition, 2)) result.BillingDocumentThird1_4th_1 = QCAReqLine4.ListBillingDocumentCondition[2].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine4.ListBillingDocumentCondition, 3)) result.BillingDocumentFourth_4th_1 = QCAReqLine4.ListBillingDocumentCondition[3].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine4.ListBillingDocumentCondition, 4)) result.BillingDocumentFifth_4th_1 = QCAReqLine4.ListBillingDocumentCondition[4].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine4.ListBillingDocumentCondition, 5)) result.BillingDocumentSixth1_4th_1 = QCAReqLine4.ListBillingDocumentCondition[5].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine4.ListBillingDocumentCondition, 6)) result.BillingDocumentSeventh_4th_1 = QCAReqLine4.ListBillingDocumentCondition[6].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine4.ListBillingDocumentCondition, 7)) result.BillingDocumentEigth1_4th_1 = QCAReqLine4.ListBillingDocumentCondition[7].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine4.ListBillingDocumentCondition, 8)) result.BillingDocumentNineth1_4th_1 = QCAReqLine4.ListBillingDocumentCondition[8].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine4.ListBillingDocumentCondition, 9)) result.BillingDocumentTenth_4th_1 = QCAReqLine4.ListBillingDocumentCondition[9].Description;
                    #endregion QDocumentCondition - Billing

                    #region QDocumentCondition - Receipt
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine4.ListReceiptDocumentCondition, 0)) result.ReceiptDocumentFirst1_4th_1 = QCAReqLine4.ListReceiptDocumentCondition[0].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine4.ListReceiptDocumentCondition, 1)) result.ReceiptDocumentSecond1_4th_1 = QCAReqLine4.ListReceiptDocumentCondition[1].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine4.ListReceiptDocumentCondition, 2)) result.ReceiptDocumentThird1_4th_1 = QCAReqLine4.ListReceiptDocumentCondition[2].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine4.ListReceiptDocumentCondition, 3)) result.ReceiptDocumentFourth_4th_1 = QCAReqLine4.ListReceiptDocumentCondition[3].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine4.ListReceiptDocumentCondition, 4)) result.ReceiptDocumentFifth_4th_1 = QCAReqLine4.ListReceiptDocumentCondition[4].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine4.ListReceiptDocumentCondition, 5)) result.ReceiptDocumentSixth1_4th_1 = QCAReqLine4.ListReceiptDocumentCondition[5].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine4.ListReceiptDocumentCondition, 6)) result.ReceiptDocumentSeventh_4th_1 = QCAReqLine4.ListReceiptDocumentCondition[6].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine4.ListReceiptDocumentCondition, 7)) result.ReceiptDocumentEigth1_4th_1 = QCAReqLine4.ListReceiptDocumentCondition[7].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine4.ListReceiptDocumentCondition, 8)) result.ReceiptDocumentNineth1_4th_1 = QCAReqLine4.ListReceiptDocumentCondition[8].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine4.ListReceiptDocumentCondition, 9)) result.ReceiptDocumentTenth_4th_1 = QCAReqLine4.ListReceiptDocumentCondition[9].Description;
                    #endregion QDocumentCondition - Receipt

                    #region QServiceFeeCondition
                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine4.ListServiceFeeCondition, 0))
                    {
                        result.ServiceFeeDesc01_4th_1 = QCAReqLine4.ListServiceFeeCondition[0].ServicefeeType;
                        result.ServiceFeeAmt01_4th_1 = QCAReqLine4.ListServiceFeeCondition[0].AmountBeforeTax;
                        result.ServiceFeeRemark01_4th_1 = QCAReqLine4.ListServiceFeeCondition[0].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine4.ListServiceFeeCondition, 1))
                    {
                        result.ServiceFeeDesc02_4th_1 = QCAReqLine4.ListServiceFeeCondition[1].ServicefeeType;
                        result.ServiceFeeAmt02_4th_1 = QCAReqLine4.ListServiceFeeCondition[1].AmountBeforeTax;
                        result.ServiceFeeRemark02_4th_1 = QCAReqLine4.ListServiceFeeCondition[1].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine4.ListServiceFeeCondition, 2))
                    {
                        result.ServiceFeeDesc03_4th_1 = QCAReqLine4.ListServiceFeeCondition[2].ServicefeeType;
                        result.ServiceFeeAmt03_4th_1 = QCAReqLine4.ListServiceFeeCondition[2].AmountBeforeTax;
                        result.ServiceFeeRemark03_4th_1 = QCAReqLine4.ListServiceFeeCondition[2].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine4.ListServiceFeeCondition, 3))
                    {
                        result.ServiceFeeDesc04_4th_1 = QCAReqLine4.ListServiceFeeCondition[3].ServicefeeType;
                        result.ServiceFeeAmt04_4th_1 = QCAReqLine4.ListServiceFeeCondition[3].AmountBeforeTax;
                        result.ServiceFeeRemark04_4th_1 = QCAReqLine4.ListServiceFeeCondition[3].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine4.ListServiceFeeCondition, 4))
                    {
                        result.ServiceFeeDesc05_4th_1 = QCAReqLine4.ListServiceFeeCondition[4].ServicefeeType;
                        result.ServiceFeeAmt05_4th_1 = QCAReqLine4.ListServiceFeeCondition[4].AmountBeforeTax;
                        result.ServiceFeeRemark05_4th_1 = QCAReqLine4.ListServiceFeeCondition[4].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine4.ListServiceFeeCondition, 5))
                    {
                        result.ServiceFeeDesc06_4th_1 = QCAReqLine4.ListServiceFeeCondition[5].ServicefeeType;
                        result.ServiceFeeAmt06_4th_1 = QCAReqLine4.ListServiceFeeCondition[5].AmountBeforeTax;
                        result.ServiceFeeRemark06_4th_1 = QCAReqLine4.ListServiceFeeCondition[5].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine4.ListServiceFeeCondition, 6))
                    {
                        result.ServiceFeeDesc07_4th_1 = QCAReqLine4.ListServiceFeeCondition[6].ServicefeeType;
                        result.ServiceFeeAmt07_4th_1 = QCAReqLine4.ListServiceFeeCondition[6].AmountBeforeTax;
                        result.ServiceFeeRemark07_4th_1 = QCAReqLine4.ListServiceFeeCondition[6].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine4.ListServiceFeeCondition, 7))
                    {
                        result.ServiceFeeDesc08_4th_1 = QCAReqLine4.ListServiceFeeCondition[7].ServicefeeType;
                        result.ServiceFeeAmt08_4th_1 = QCAReqLine4.ListServiceFeeCondition[7].AmountBeforeTax;
                        result.ServiceFeeRemark08_4th_1 = QCAReqLine4.ListServiceFeeCondition[7].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine4.ListServiceFeeCondition, 8))
                    {
                        result.ServiceFeeDesc09_4th_1 = QCAReqLine4.ListServiceFeeCondition[8].ServicefeeType;
                        result.ServiceFeeAmt09_4th_1 = QCAReqLine4.ListServiceFeeCondition[8].AmountBeforeTax;
                        result.ServiceFeeRemark09_4th_1 = QCAReqLine4.ListServiceFeeCondition[8].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine4.ListServiceFeeCondition, 9))
                    {
                        result.ServiceFeeDesc10_4th_1 = QCAReqLine4.ListServiceFeeCondition[9].ServicefeeType;
                        result.ServiceFeeAmt10_4th_1 = QCAReqLine4.ListServiceFeeCondition[9].AmountBeforeTax;
                        result.ServiceFeeRemark10_4th_1 = QCAReqLine4.ListServiceFeeCondition[9].Description;
                    }
                    #endregion QServiceFeeCondition

                    result.SumServiceFeeAmt_4th_1 = QCAReqLine4.SumServiceFeeCondition;
                }
                #endregion QCAReqLine4
                #region QCAReqLine5
                if (CheckQCAReqLineElementIsNotNull(QCAReqLineList, 4))
                {
                    QCAReqLine QCAReqLine5 = QCAReqLineList[4];
                    result.BuyerName5th_1 = QCAReqLine5.BuyerName;
                    result.BuyerCreditLimitRequest5th_1 = QCAReqLine5.CreditLimitLineRequest;
                    result.BuyerCreditLimitAllCustomer5th_1 = QCAReqLine5.BuyerCreditLimit;
                    result.PurchasePercent5th_1 = QCAReqLine5.MaxPurchasePct;
                    result.AssignmentCondition5th_1 = QCAReqLine5.AssignmentMethodRemark;
                    result.BillingResponsible5th_1 = QCAReqLine5.BillingResponsibleBy;
                    result.PaymentMethod5th_1 = QCAReqLine5.MethodOfPayment;
                    result.BusinessSegment5th_2 = QCAReqLine5.BusinessSegment;
                    result.BuyerNameCARequestLine5th_1 = QCAReqLine5.BuyerName;
                    result.BuyerTaxID5th_1 = QCAReqLine5.TaxId;
                    result.BuyerAddress5th_1 = QCAReqLine5.InvoiceAddress;
                    result.BuyerCompanyRegisteredDate5th_1 = QCAReqLine5.DateOfEstablish;
                    result.BuyerLineOfBusiness5th_1 = QCAReqLine5.LineOfBusiness;
                    result.BuyerBlacklistStatus5th_1 = QCAReqLine5.BlacklistStatus;
                    result.BuyerCreditScoring5th_1 = QCAReqLine5.CreditScoring;
                    result.BuyerCreditLimitRequest5th_2 = QCAReqLine5.CreditLimitLineRequest;
                    result.BuyerCreditLimitAllCustomer5th_2 = QCAReqLine5.BuyerCreditLimit;
                    result.PurchasePercent5th_2 = QCAReqLine5.MaxPurchasePct;
                    result.AssignmentCondition5th_2 = QCAReqLine5.AssignmentMethodRemark;
                    result.FactoringPurchaseFee5th_1 = QCAReqLine5.PurchaseFeePct;
                    result.FactoringPurchaseFeeCalBase5th_1 = SystemStaticData.GetTranslatedMessage(((PurchaseFeeCalculateBase)QCAReqLine5.PurchaseFeeCalculateBase).GetAttrCode());
                    result.BillingResponsible5th_2 = QCAReqLine5.BillingResponsibleBy;
                    result.BillingDescriptiong5th_1 = QCAReqLine5.BillingDescription;
                    result.BuyerBillingContactPerson5th_1 = QCAReqLine5.BillingContactPersonName;
                    result.BuyerBillingContactPersonPosition5th_1 = QCAReqLine5.BillingContactPersonPosition;
                    result.BuyerBillingAddress5th_1 = QCAReqLine5.BuyerBillingAddress;
                    result.BuyerBillingContactPersonTel5th_1 = QCAReqLine5.BillingContactPersonPhone;
                    result.ReceiptDescriptiong5th_1 = QCAReqLine5.ReceiptDescription;
                    result.PaymentMethod5th_2 = QCAReqLine5.MethodOfPayment;
                    result.BuyerReceiptContactPerson5th_1 = QCAReqLine5.ReceiptContactPersonName;
                    result.BuyerReceiptContactPersonPosition5th_1 = QCAReqLine5.ReceiptContactPersonPosition;
                    result.BuyerReceiptAddress5th_1 = QCAReqLine5.BuyerReceiptAddress;
                    result.BuyerReceiptContactPersonTel5th_1 = QCAReqLine5.ReceiptContactPersonPhone;
                    result.MarketingComment5th = QCAReqLine5.MarketingComment;
                    result.CreditComment5th = QCAReqLine5.CreditComment;
                    result.ApproverComment5th = QCAReqLine5.ApproverComment;
                    result.AcceptanceDocument5th_1 = QCAReqLine5.AcceptanceDocument == true ? TextConstants.Yes : TextConstants.No;
                    //R03
                    result.AROutstandingAllCustomer_5 = QCAReqLine5.AllCustomerBuyerOutstanding;
                    result.PurchaseConditionCust_5 = QCAReqLine5.LineCondition;
                    //R03

                    #region QCreditAppReqLineFin
                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine5.ListFinancial, 0))
                    {
                        result.BuyerFinYearFirst1_5th_1 = QCAReqLine5.ListFinancial[0].Year;
                        result.BuyerFinRegisteredCapitalFirst1_5th_1 = QCAReqLine5.ListFinancial[0].RegisteredCapital;
                        result.BuyerFinPaidCapitalFirst1_5th_1 = QCAReqLine5.ListFinancial[0].PaidCapital;
                        result.BuyerFinTotalAssetFirst1_5th_1 = QCAReqLine5.ListFinancial[0].TotalAsset;
                        result.BuyerFinTotalLiabilityFirst1_5th_1 = QCAReqLine5.ListFinancial[0].TotalLiability;
                        result.BuyerFinTotalEquityFirst1_5th_1 = QCAReqLine5.ListFinancial[0].TotalEquity;
                        result.BuyerFinYearFirst2_5th_1 = QCAReqLine5.ListFinancial[0].Year;
                        result.BuyerFinTotalRevenueFirst1_5th_1 = QCAReqLine5.ListFinancial[0].TotalRevenue;
                        result.BuyerFinTotalCOGSFirst1_5th_1 = QCAReqLine5.ListFinancial[0].TotalCOGS;
                        result.BuyerFinTotalGrossProfitFirst1_5th_1 = QCAReqLine5.ListFinancial[0].TotalGrossProfit;
                        result.BuyerFinTotalOperExpFirst1_5th_1 = QCAReqLine5.ListFinancial[0].TotalOperExp;
                        result.BuyerFinTotalNetProfitFirst1_5th_1 = QCAReqLine5.ListFinancial[0].TotalNetProfit;
                        result.BuyerFinNetProfitPercent1_5th_1 = QCAReqLine5.ListFinancial[0].NetProfitPercent;
                        result.BuyerFinlDE1_5th_1 = QCAReqLine5.ListFinancial[0].lDE;
                        result.BuyerFinQuickRatio1_5th_1 = QCAReqLine5.ListFinancial[0].QuickRatio;
                        result.BuyerFinIntCoverageRatio1_5th_1 = QCAReqLine5.ListFinancial[0].IntCoverageRatio;
                    }

                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine5.ListFinancial, 1))
                    {
                        result.BuyerFinYearSecond1_5th_1 = QCAReqLine5.ListFinancial[1].Year;
                        result.BuyerFinRegisteredCapitalSecond1_5th_1 = QCAReqLine5.ListFinancial[1].RegisteredCapital;
                        result.BuyerFinPaidCapitalSecond1_5th_1 = QCAReqLine5.ListFinancial[1].PaidCapital;
                        result.BuyerFinTotalAssetSecond1_5th_1 = QCAReqLine5.ListFinancial[1].TotalAsset;
                        result.BuyerFinTotalLiabilitySecond1_5th_1 = QCAReqLine5.ListFinancial[1].TotalLiability;
                        result.BuyerFinTotalEquitySecond1_5th_1 = QCAReqLine5.ListFinancial[1].TotalEquity;
                        result.BuyerFinYearSecond2_5th_1 = QCAReqLine5.ListFinancial[1].Year;
                        result.BuyerFinTotalRevenueSecond1_5th_1 = QCAReqLine5.ListFinancial[1].TotalRevenue;
                        result.BuyerFinTotalCOGSSecond1_5th_1 = QCAReqLine5.ListFinancial[1].TotalCOGS;
                        result.BuyerFinTotalGrossProfitSecond1_5th_1 = QCAReqLine5.ListFinancial[1].TotalGrossProfit;
                        result.BuyerFinTotalOperExpSecond1_5th_1 = QCAReqLine5.ListFinancial[1].TotalOperExp;
                        result.BuyerFinTotalNetProfitSecond1_5th_1 = QCAReqLine5.ListFinancial[1].TotalNetProfit;
                    }

                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine5.ListFinancial, 2))
                    {
                        result.BuyerFinYearThird1_5th_1 = QCAReqLine5.ListFinancial[2].Year;
                        result.BuyerFinRegisteredCapitalThird1_5th_1 = QCAReqLine5.ListFinancial[2].RegisteredCapital;
                        result.BuyerFinPaidCapitalThird1_5th_1 = QCAReqLine5.ListFinancial[2].PaidCapital;
                        result.BuyerFinTotalAssetThird1_5th_1 = QCAReqLine5.ListFinancial[2].TotalAsset;
                        result.BuyerFinTotalLiabilityThird1_5th_1 = QCAReqLine5.ListFinancial[2].TotalLiability;
                        result.BuyerFinTotalEquityThird1_5th_1 = QCAReqLine5.ListFinancial[2].TotalEquity;
                        result.BuyerFinYearThird2_5th_1 = QCAReqLine5.ListFinancial[2].Year;
                        result.BuyerFinTotalRevenueThird1_5th_1 = QCAReqLine5.ListFinancial[2].TotalRevenue;
                        result.BuyerFinTotalCOGSThird1_5th_1 = QCAReqLine5.ListFinancial[2].TotalCOGS;
                        result.BuyerFinTotalGrossProfitThird1_5th_1 = QCAReqLine5.ListFinancial[2].TotalGrossProfit;
                        result.BuyerFinTotalOperExpThird1_5th_1 = QCAReqLine5.ListFinancial[2].TotalOperExp;
                        result.BuyerFinTotalNetProfitThird1_5th_1 = QCAReqLine5.ListFinancial[2].TotalNetProfit;
                    }
                    #endregion QCreditAppReqLineFin

                    #region QDocumentCondition - Billing
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine5.ListBillingDocumentCondition, 0)) result.BillingDocumentFirst1_5th_1 = QCAReqLine5.ListBillingDocumentCondition[0].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine5.ListBillingDocumentCondition, 1)) result.BillingDocumentSecond1_5th_1 = QCAReqLine5.ListBillingDocumentCondition[1].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine5.ListBillingDocumentCondition, 2)) result.BillingDocumentThird1_5th_1 = QCAReqLine5.ListBillingDocumentCondition[2].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine5.ListBillingDocumentCondition, 3)) result.BillingDocumentFourth_5th_1 = QCAReqLine5.ListBillingDocumentCondition[3].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine5.ListBillingDocumentCondition, 4)) result.BillingDocumentFifth_5th_1 = QCAReqLine5.ListBillingDocumentCondition[4].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine5.ListBillingDocumentCondition, 5)) result.BillingDocumentSixth1_5th_1 = QCAReqLine5.ListBillingDocumentCondition[5].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine5.ListBillingDocumentCondition, 6)) result.BillingDocumentSeventh_5th_1 = QCAReqLine5.ListBillingDocumentCondition[6].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine5.ListBillingDocumentCondition, 7)) result.BillingDocumentEigth1_5th_1 = QCAReqLine5.ListBillingDocumentCondition[7].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine5.ListBillingDocumentCondition, 8)) result.BillingDocumentNineth1_5th_1 = QCAReqLine5.ListBillingDocumentCondition[8].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine5.ListBillingDocumentCondition, 9)) result.BillingDocumentTenth_5th_1 = QCAReqLine5.ListBillingDocumentCondition[9].Description;
                    #endregion QDocumentCondition - Billing

                    #region QDocumentCondition - Receipt
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine5.ListReceiptDocumentCondition, 0)) result.ReceiptDocumentFirst1_5th_1 = QCAReqLine5.ListReceiptDocumentCondition[0].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine5.ListReceiptDocumentCondition, 1)) result.ReceiptDocumentSecond1_5th_1 = QCAReqLine5.ListReceiptDocumentCondition[1].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine5.ListReceiptDocumentCondition, 2)) result.ReceiptDocumentThird1_5th_1 = QCAReqLine5.ListReceiptDocumentCondition[2].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine5.ListReceiptDocumentCondition, 3)) result.ReceiptDocumentFourth_5th_1 = QCAReqLine5.ListReceiptDocumentCondition[3].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine5.ListReceiptDocumentCondition, 4)) result.ReceiptDocumentFifth_5th_1 = QCAReqLine5.ListReceiptDocumentCondition[4].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine5.ListReceiptDocumentCondition, 5)) result.ReceiptDocumentSixth1_5th_1 = QCAReqLine5.ListReceiptDocumentCondition[5].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine5.ListReceiptDocumentCondition, 6)) result.ReceiptDocumentSeventh_5th_1 = QCAReqLine5.ListReceiptDocumentCondition[6].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine5.ListReceiptDocumentCondition, 7)) result.ReceiptDocumentEigth1_5th_1 = QCAReqLine5.ListReceiptDocumentCondition[7].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine5.ListReceiptDocumentCondition, 8)) result.ReceiptDocumentNineth1_5th_1 = QCAReqLine5.ListReceiptDocumentCondition[8].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine5.ListReceiptDocumentCondition, 9)) result.ReceiptDocumentTenth_5th_1 = QCAReqLine5.ListReceiptDocumentCondition[9].Description;
                    #endregion QDocumentCondition - Receipt

                    #region QServiceFeeCondition
                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine5.ListServiceFeeCondition, 0))
                    {
                        result.ServiceFeeDesc01_5th_1 = QCAReqLine5.ListServiceFeeCondition[0].ServicefeeType;
                        result.ServiceFeeAmt01_5th_1 = QCAReqLine5.ListServiceFeeCondition[0].AmountBeforeTax;
                        result.ServiceFeeRemark01_5th_1 = QCAReqLine5.ListServiceFeeCondition[0].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine5.ListServiceFeeCondition, 1))
                    {
                        result.ServiceFeeDesc02_5th_1 = QCAReqLine5.ListServiceFeeCondition[1].ServicefeeType;
                        result.ServiceFeeAmt02_5th_1 = QCAReqLine5.ListServiceFeeCondition[1].AmountBeforeTax;
                        result.ServiceFeeRemark02_5th_1 = QCAReqLine5.ListServiceFeeCondition[1].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine5.ListServiceFeeCondition, 2))
                    {
                        result.ServiceFeeDesc03_5th_1 = QCAReqLine5.ListServiceFeeCondition[2].ServicefeeType;
                        result.ServiceFeeAmt03_5th_1 = QCAReqLine5.ListServiceFeeCondition[2].AmountBeforeTax;
                        result.ServiceFeeRemark03_5th_1 = QCAReqLine5.ListServiceFeeCondition[2].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine5.ListServiceFeeCondition, 3))
                    {
                        result.ServiceFeeDesc04_5th_1 = QCAReqLine5.ListServiceFeeCondition[3].ServicefeeType;
                        result.ServiceFeeAmt04_5th_1 = QCAReqLine5.ListServiceFeeCondition[3].AmountBeforeTax;
                        result.ServiceFeeRemark04_5th_1 = QCAReqLine5.ListServiceFeeCondition[3].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine5.ListServiceFeeCondition, 4))
                    {
                        result.ServiceFeeDesc05_5th_1 = QCAReqLine5.ListServiceFeeCondition[4].ServicefeeType;
                        result.ServiceFeeAmt05_5th_1 = QCAReqLine5.ListServiceFeeCondition[4].AmountBeforeTax;
                        result.ServiceFeeRemark05_5th_1 = QCAReqLine5.ListServiceFeeCondition[4].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine5.ListServiceFeeCondition, 5))
                    {
                        result.ServiceFeeDesc06_5th_1 = QCAReqLine5.ListServiceFeeCondition[5].ServicefeeType;
                        result.ServiceFeeAmt06_5th_1 = QCAReqLine5.ListServiceFeeCondition[5].AmountBeforeTax;
                        result.ServiceFeeRemark06_5th_1 = QCAReqLine5.ListServiceFeeCondition[5].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine5.ListServiceFeeCondition, 6))
                    {
                        result.ServiceFeeDesc07_5th_1 = QCAReqLine5.ListServiceFeeCondition[6].ServicefeeType;
                        result.ServiceFeeAmt07_5th_1 = QCAReqLine5.ListServiceFeeCondition[6].AmountBeforeTax;
                        result.ServiceFeeRemark07_5th_1 = QCAReqLine5.ListServiceFeeCondition[6].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine5.ListServiceFeeCondition, 7))
                    {
                        result.ServiceFeeDesc08_5th_1 = QCAReqLine5.ListServiceFeeCondition[7].ServicefeeType;
                        result.ServiceFeeAmt08_5th_1 = QCAReqLine5.ListServiceFeeCondition[7].AmountBeforeTax;
                        result.ServiceFeeRemark08_5th_1 = QCAReqLine5.ListServiceFeeCondition[7].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine5.ListServiceFeeCondition, 8))
                    {
                        result.ServiceFeeDesc09_5th_1 = QCAReqLine5.ListServiceFeeCondition[8].ServicefeeType;
                        result.ServiceFeeAmt09_5th_1 = QCAReqLine5.ListServiceFeeCondition[8].AmountBeforeTax;
                        result.ServiceFeeRemark09_5th_1 = QCAReqLine5.ListServiceFeeCondition[8].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine5.ListServiceFeeCondition, 9))
                    {
                        result.ServiceFeeDesc10_5th_1 = QCAReqLine5.ListServiceFeeCondition[9].ServicefeeType;
                        result.ServiceFeeAmt10_5th_1 = QCAReqLine5.ListServiceFeeCondition[9].AmountBeforeTax;
                        result.ServiceFeeRemark10_5th_1 = QCAReqLine5.ListServiceFeeCondition[9].Description;
                    }
                    #endregion QServiceFeeCondition

                    result.SumServiceFeeAmt_5th_1 = QCAReqLine5.SumServiceFeeCondition;
                }
                #endregion QCAReqLine5
                #region QCAReqLine6
                if (CheckQCAReqLineElementIsNotNull(QCAReqLineList, 5))
                {
                    QCAReqLine QCAReqLine6 = QCAReqLineList[5];
                    result.BuyerName6th_1 = QCAReqLine6.BuyerName;
                    result.BuyerCreditLimitRequest6th_1 = QCAReqLine6.CreditLimitLineRequest;
                    result.BuyerCreditLimitAllCustomer6th_1 = QCAReqLine6.BuyerCreditLimit;
                    result.PurchasePercent6th_1 = QCAReqLine6.MaxPurchasePct;
                    result.AssignmentCondition6th_1 = QCAReqLine6.AssignmentMethodRemark;
                    result.BillingResponsible6th_1 = QCAReqLine6.BillingResponsibleBy;
                    result.PaymentMethod6th_1 = QCAReqLine6.MethodOfPayment;
                    result.BusinessSegment6th_2 = QCAReqLine6.BusinessSegment;
                    result.BuyerNameCARequestLine6th_1 = QCAReqLine6.BuyerName;
                    result.BuyerTaxID6th_1 = QCAReqLine6.TaxId;
                    result.BuyerAddress6th_1 = QCAReqLine6.InvoiceAddress;
                    result.BuyerCompanyRegisteredDate6th_1 = QCAReqLine6.DateOfEstablish;
                    result.BuyerLineOfBusiness6th_1 = QCAReqLine6.LineOfBusiness;
                    result.BuyerBlacklistStatus6th_1 = QCAReqLine6.BlacklistStatus;
                    result.BuyerCreditScoring6th_1 = QCAReqLine6.CreditScoring;
                    result.BuyerCreditLimitRequest6th_2 = QCAReqLine6.CreditLimitLineRequest;
                    result.BuyerCreditLimitAllCustomer6th_2 = QCAReqLine6.BuyerCreditLimit;
                    result.PurchasePercent6th_2 = QCAReqLine6.MaxPurchasePct;
                    result.AssignmentCondition6th_2 = QCAReqLine6.AssignmentMethodRemark;
                    result.FactoringPurchaseFee6th_1 = QCAReqLine6.PurchaseFeePct;
                    result.FactoringPurchaseFeeCalBase6th_1 = SystemStaticData.GetTranslatedMessage(((PurchaseFeeCalculateBase)QCAReqLine6.PurchaseFeeCalculateBase).GetAttrCode());
                    result.BillingResponsible6th_2 = QCAReqLine6.BillingResponsibleBy;
                    result.BillingDescriptiong6th_1 = QCAReqLine6.BillingDescription;
                    result.BuyerBillingContactPerson6th_1 = QCAReqLine6.BillingContactPersonName;
                    result.BuyerBillingContactPersonPosition6th_1 = QCAReqLine6.BillingContactPersonPosition;
                    result.BuyerBillingAddress6th_1 = QCAReqLine6.BuyerBillingAddress;
                    result.BuyerBillingContactPersonTel6th_1 = QCAReqLine6.BillingContactPersonPhone;
                    result.ReceiptDescriptiong6th_1 = QCAReqLine6.ReceiptDescription;
                    result.PaymentMethod6th_2 = QCAReqLine6.MethodOfPayment;
                    result.BuyerReceiptContactPerson6th_1 = QCAReqLine6.ReceiptContactPersonName;
                    result.BuyerReceiptContactPersonPosition6th_1 = QCAReqLine6.ReceiptContactPersonPosition;
                    result.BuyerReceiptAddress6th_1 = QCAReqLine6.BuyerReceiptAddress;
                    result.BuyerReceiptContactPersonTel6th_1 = QCAReqLine6.ReceiptContactPersonPhone;
                    result.MarketingComment6th = QCAReqLine6.MarketingComment;
                    result.CreditComment6th = QCAReqLine6.CreditComment;
                    result.ApproverComment6th = QCAReqLine6.ApproverComment;
                    result.AcceptanceDocument6th_1 = QCAReqLine6.AcceptanceDocument == true ? TextConstants.Yes : TextConstants.No;
                    //R03
                    result.AROutstandingAllCustomer_6 = QCAReqLine6.AllCustomerBuyerOutstanding;
                    result.PurchaseConditionCust_6 = QCAReqLine6.LineCondition;
                    //R03

                    #region QCreditAppReqLineFin
                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine6.ListFinancial, 0))
                    {
                        result.BuyerFinYearFirst1_6th_1 = QCAReqLine6.ListFinancial[0].Year;
                        result.BuyerFinRegisteredCapitalFirst1_6th_1 = QCAReqLine6.ListFinancial[0].RegisteredCapital;
                        result.BuyerFinPaidCapitalFirst1_6th_1 = QCAReqLine6.ListFinancial[0].PaidCapital;
                        result.BuyerFinTotalAssetFirst1_6th_1 = QCAReqLine6.ListFinancial[0].TotalAsset;
                        result.BuyerFinTotalLiabilityFirst1_6th_1 = QCAReqLine6.ListFinancial[0].TotalLiability;
                        result.BuyerFinTotalEquityFirst1_6th_1 = QCAReqLine6.ListFinancial[0].TotalEquity;
                        result.BuyerFinYearFirst2_6th_1 = QCAReqLine6.ListFinancial[0].Year;
                        result.BuyerFinTotalRevenueFirst1_6th_1 = QCAReqLine6.ListFinancial[0].TotalRevenue;
                        result.BuyerFinTotalCOGSFirst1_6th_1 = QCAReqLine6.ListFinancial[0].TotalCOGS;
                        result.BuyerFinTotalGrossProfitFirst1_6th_1 = QCAReqLine6.ListFinancial[0].TotalGrossProfit;
                        result.BuyerFinTotalOperExpFirst1_6th_1 = QCAReqLine6.ListFinancial[0].TotalOperExp;
                        result.BuyerFinTotalNetProfitFirst1_6th_1 = QCAReqLine6.ListFinancial[0].TotalNetProfit;
                        result.BuyerFinNetProfitPercent1_6th_1 = QCAReqLine6.ListFinancial[0].NetProfitPercent;
                        result.BuyerFinlDE1_6th_1 = QCAReqLine6.ListFinancial[0].lDE;
                        result.BuyerFinQuickRatio1_6th_1 = QCAReqLine6.ListFinancial[0].QuickRatio;
                        result.BuyerFinIntCoverageRatio1_6th_1 = QCAReqLine6.ListFinancial[0].IntCoverageRatio;
                    }

                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine6.ListFinancial, 1))
                    {
                        result.BuyerFinYearSecond1_6th_1 = QCAReqLine6.ListFinancial[1].Year;
                        result.BuyerFinRegisteredCapitalSecond1_6th_1 = QCAReqLine6.ListFinancial[1].RegisteredCapital;
                        result.BuyerFinPaidCapitalSecond1_6th_1 = QCAReqLine6.ListFinancial[1].PaidCapital;
                        result.BuyerFinTotalAssetSecond1_6th_1 = QCAReqLine6.ListFinancial[1].TotalAsset;
                        result.BuyerFinTotalLiabilitySecond1_6th_1 = QCAReqLine6.ListFinancial[1].TotalLiability;
                        result.BuyerFinTotalEquitySecond1_6th_1 = QCAReqLine6.ListFinancial[1].TotalEquity;
                        result.BuyerFinYearSecond2_6th_1 = QCAReqLine6.ListFinancial[1].Year;
                        result.BuyerFinTotalRevenueSecond1_6th_1 = QCAReqLine6.ListFinancial[1].TotalRevenue;
                        result.BuyerFinTotalCOGSSecond1_6th_1 = QCAReqLine6.ListFinancial[1].TotalCOGS;
                        result.BuyerFinTotalGrossProfitSecond1_6th_1 = QCAReqLine6.ListFinancial[1].TotalGrossProfit;
                        result.BuyerFinTotalOperExpSecond1_6th_1 = QCAReqLine6.ListFinancial[1].TotalOperExp;
                        result.BuyerFinTotalNetProfitSecond1_6th_1 = QCAReqLine6.ListFinancial[1].TotalNetProfit;
                    }

                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine6.ListFinancial, 2))
                    {
                        result.BuyerFinYearThird1_6th_1 = QCAReqLine6.ListFinancial[2].Year;
                        result.BuyerFinRegisteredCapitalThird1_6th_1 = QCAReqLine6.ListFinancial[2].RegisteredCapital;
                        result.BuyerFinPaidCapitalThird1_6th_1 = QCAReqLine6.ListFinancial[2].PaidCapital;
                        result.BuyerFinTotalAssetThird1_6th_1 = QCAReqLine6.ListFinancial[2].TotalAsset;
                        result.BuyerFinTotalLiabilityThird1_6th_1 = QCAReqLine6.ListFinancial[2].TotalLiability;
                        result.BuyerFinTotalEquityThird1_6th_1 = QCAReqLine6.ListFinancial[2].TotalEquity;
                        result.BuyerFinYearThird2_6th_1 = QCAReqLine6.ListFinancial[2].Year;
                        result.BuyerFinTotalRevenueThird1_6th_1 = QCAReqLine6.ListFinancial[2].TotalRevenue;
                        result.BuyerFinTotalCOGSThird1_6th_1 = QCAReqLine6.ListFinancial[2].TotalCOGS;
                        result.BuyerFinTotalGrossProfitThird1_6th_1 = QCAReqLine6.ListFinancial[2].TotalGrossProfit;
                        result.BuyerFinTotalOperExpThird1_6th_1 = QCAReqLine6.ListFinancial[2].TotalOperExp;
                        result.BuyerFinTotalNetProfitThird1_6th_1 = QCAReqLine6.ListFinancial[2].TotalNetProfit;

                    }
                    #endregion QCreditAppReqLineFin

                    #region QDocumentCondition - Billing
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine6.ListBillingDocumentCondition, 0)) result.BillingDocumentFirst1_6th_1 = QCAReqLine6.ListBillingDocumentCondition[0].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine6.ListBillingDocumentCondition, 1)) result.BillingDocumentSecond1_6th_1 = QCAReqLine6.ListBillingDocumentCondition[1].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine6.ListBillingDocumentCondition, 2)) result.BillingDocumentThird1_6th_1 = QCAReqLine6.ListBillingDocumentCondition[2].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine6.ListBillingDocumentCondition, 3)) result.BillingDocumentFourth_6th_1 = QCAReqLine6.ListBillingDocumentCondition[3].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine6.ListBillingDocumentCondition, 4)) result.BillingDocumentFifth_6th_1 = QCAReqLine6.ListBillingDocumentCondition[4].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine6.ListBillingDocumentCondition, 5)) result.BillingDocumentSixth1_6th_1 = QCAReqLine6.ListBillingDocumentCondition[5].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine6.ListBillingDocumentCondition, 6)) result.BillingDocumentSeventh_6th_1 = QCAReqLine6.ListBillingDocumentCondition[6].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine6.ListBillingDocumentCondition, 7)) result.BillingDocumentEigth1_6th_1 = QCAReqLine6.ListBillingDocumentCondition[7].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine6.ListBillingDocumentCondition, 8)) result.BillingDocumentNineth1_6th_1 = QCAReqLine6.ListBillingDocumentCondition[8].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine6.ListBillingDocumentCondition, 9)) result.BillingDocumentTenth_6th_1 = QCAReqLine6.ListBillingDocumentCondition[9].Description;
                    #endregion QDocumentCondition - Billing

                    #region QDocumentCondition - Receipt
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine6.ListReceiptDocumentCondition, 0)) result.ReceiptDocumentFirst1_6th_1 = QCAReqLine6.ListReceiptDocumentCondition[0].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine6.ListReceiptDocumentCondition, 1)) result.ReceiptDocumentSecond1_6th_1 = QCAReqLine6.ListReceiptDocumentCondition[1].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine6.ListReceiptDocumentCondition, 2)) result.ReceiptDocumentThird1_6th_1 = QCAReqLine6.ListReceiptDocumentCondition[2].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine6.ListReceiptDocumentCondition, 3)) result.ReceiptDocumentFourth_6th_1 = QCAReqLine6.ListReceiptDocumentCondition[3].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine6.ListReceiptDocumentCondition, 4)) result.ReceiptDocumentFifth_6th_1 = QCAReqLine6.ListReceiptDocumentCondition[4].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine6.ListReceiptDocumentCondition, 5)) result.ReceiptDocumentSixth1_6th_1 = QCAReqLine6.ListReceiptDocumentCondition[5].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine6.ListReceiptDocumentCondition, 6)) result.ReceiptDocumentSeventh_6th_1 = QCAReqLine6.ListReceiptDocumentCondition[6].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine6.ListReceiptDocumentCondition, 7)) result.ReceiptDocumentEigth1_6th_1 = QCAReqLine6.ListReceiptDocumentCondition[7].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine6.ListReceiptDocumentCondition, 8)) result.ReceiptDocumentNineth1_6th_1 = QCAReqLine6.ListReceiptDocumentCondition[8].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine6.ListReceiptDocumentCondition, 9)) result.ReceiptDocumentTenth_6th_1 = QCAReqLine6.ListReceiptDocumentCondition[9].Description;
                    #endregion QDocumentCondition - Receipt

                    #region QServiceFeeCondition
                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine6.ListServiceFeeCondition, 0))
                    {
                        result.ServiceFeeDesc01_6th_1 = QCAReqLine6.ListServiceFeeCondition[0].ServicefeeType;
                        result.ServiceFeeAmt01_6th_1 = QCAReqLine6.ListServiceFeeCondition[0].AmountBeforeTax;
                        result.ServiceFeeRemark01_6th_1 = QCAReqLine6.ListServiceFeeCondition[0].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine6.ListServiceFeeCondition, 1))
                    {
                        result.ServiceFeeDesc02_6th_1 = QCAReqLine6.ListServiceFeeCondition[1].ServicefeeType;
                        result.ServiceFeeAmt02_6th_1 = QCAReqLine6.ListServiceFeeCondition[1].AmountBeforeTax;
                        result.ServiceFeeRemark02_6th_1 = QCAReqLine6.ListServiceFeeCondition[1].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine6.ListServiceFeeCondition, 2))
                    {
                        result.ServiceFeeDesc03_6th_1 = QCAReqLine6.ListServiceFeeCondition[2].ServicefeeType;
                        result.ServiceFeeAmt03_6th_1 = QCAReqLine6.ListServiceFeeCondition[2].AmountBeforeTax;
                        result.ServiceFeeRemark03_6th_1 = QCAReqLine6.ListServiceFeeCondition[2].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine6.ListServiceFeeCondition, 3))
                    {
                        result.ServiceFeeDesc04_6th_1 = QCAReqLine6.ListServiceFeeCondition[3].ServicefeeType;
                        result.ServiceFeeAmt04_6th_1 = QCAReqLine6.ListServiceFeeCondition[3].AmountBeforeTax;
                        result.ServiceFeeRemark04_6th_1 = QCAReqLine6.ListServiceFeeCondition[3].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine6.ListServiceFeeCondition, 4))
                    {
                        result.ServiceFeeDesc05_6th_1 = QCAReqLine6.ListServiceFeeCondition[4].ServicefeeType;
                        result.ServiceFeeAmt05_6th_1 = QCAReqLine6.ListServiceFeeCondition[4].AmountBeforeTax;
                        result.ServiceFeeRemark05_6th_1 = QCAReqLine6.ListServiceFeeCondition[4].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine6.ListServiceFeeCondition, 5))
                    {
                        result.ServiceFeeDesc06_6th_1 = QCAReqLine6.ListServiceFeeCondition[5].ServicefeeType;
                        result.ServiceFeeAmt06_6th_1 = QCAReqLine6.ListServiceFeeCondition[5].AmountBeforeTax;
                        result.ServiceFeeRemark06_6th_1 = QCAReqLine6.ListServiceFeeCondition[5].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine6.ListServiceFeeCondition, 6))
                    {
                        result.ServiceFeeDesc07_6th_1 = QCAReqLine6.ListServiceFeeCondition[6].ServicefeeType;
                        result.ServiceFeeAmt07_6th_1 = QCAReqLine6.ListServiceFeeCondition[6].AmountBeforeTax;
                        result.ServiceFeeRemark07_6th_1 = QCAReqLine6.ListServiceFeeCondition[6].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine6.ListServiceFeeCondition, 7))
                    {
                        result.ServiceFeeDesc08_6th_1 = QCAReqLine6.ListServiceFeeCondition[7].ServicefeeType;
                        result.ServiceFeeAmt08_6th_1 = QCAReqLine6.ListServiceFeeCondition[7].AmountBeforeTax;
                        result.ServiceFeeRemark08_6th_1 = QCAReqLine6.ListServiceFeeCondition[7].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine6.ListServiceFeeCondition, 8))
                    {
                        result.ServiceFeeDesc09_6th_1 = QCAReqLine6.ListServiceFeeCondition[8].ServicefeeType;
                        result.ServiceFeeAmt09_6th_1 = QCAReqLine6.ListServiceFeeCondition[8].AmountBeforeTax;
                        result.ServiceFeeRemark09_6th_1 = QCAReqLine6.ListServiceFeeCondition[8].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine6.ListServiceFeeCondition, 9))
                    {
                        result.ServiceFeeDesc10_6th_1 = QCAReqLine6.ListServiceFeeCondition[9].ServicefeeType;
                        result.ServiceFeeAmt10_6th_1 = QCAReqLine6.ListServiceFeeCondition[9].AmountBeforeTax;
                        result.ServiceFeeRemark10_6th_1 = QCAReqLine6.ListServiceFeeCondition[9].Description;
                    }
                    #endregion QServiceFeeCondition

                    result.SumServiceFeeAmt_6th_1 = QCAReqLine6.SumServiceFeeCondition;
                }
                #endregion QCAReqLine6
                #region QCAReqLine7
                if (CheckQCAReqLineElementIsNotNull(QCAReqLineList, 6))
                {
                    QCAReqLine QCAReqLine7 = QCAReqLineList[6];
                    result.BuyerName7th_1 = QCAReqLine7.BuyerName;
                    result.BuyerCreditLimitRequest7th_1 = QCAReqLine7.CreditLimitLineRequest;
                    result.BuyerCreditLimitAllCustomer7th_1 = QCAReqLine7.BuyerCreditLimit;
                    result.PurchasePercent7th_1 = QCAReqLine7.MaxPurchasePct;
                    result.AssignmentCondition7th_1 = QCAReqLine7.AssignmentMethodRemark;
                    result.BillingResponsible7th_1 = QCAReqLine7.BillingResponsibleBy;
                    result.PaymentMethod7th_1 = QCAReqLine7.MethodOfPayment;
                    result.BusinessSegment7th_2 = QCAReqLine7.BusinessSegment;
                    result.BuyerNameCARequestLine7th_1 = QCAReqLine7.BuyerName;
                    result.BuyerTaxID7th_1 = QCAReqLine7.TaxId;
                    result.BuyerAddress7th_1 = QCAReqLine7.InvoiceAddress;
                    result.BuyerCompanyRegisteredDate7th_1 = QCAReqLine7.DateOfEstablish;
                    result.BuyerLineOfBusiness7th_1 = QCAReqLine7.LineOfBusiness;
                    result.BuyerBlacklistStatus7th_1 = QCAReqLine7.BlacklistStatus;
                    result.BuyerCreditScoring7th_1 = QCAReqLine7.CreditScoring;
                    result.BuyerCreditLimitRequest7th_2 = QCAReqLine7.CreditLimitLineRequest;
                    result.BuyerCreditLimitAllCustomer7th_2 = QCAReqLine7.BuyerCreditLimit;
                    result.PurchasePercent7th_2 = QCAReqLine7.MaxPurchasePct;
                    result.AssignmentCondition7th_2 = QCAReqLine7.AssignmentMethodRemark;
                    result.FactoringPurchaseFee7th_1 = QCAReqLine7.PurchaseFeePct;
                    result.FactoringPurchaseFeeCalBase7th_1 = SystemStaticData.GetTranslatedMessage(((PurchaseFeeCalculateBase)QCAReqLine7.PurchaseFeeCalculateBase).GetAttrCode());
                    result.BillingResponsible7th_2 = QCAReqLine7.BillingResponsibleBy;
                    result.BillingDescriptiong7th_1 = QCAReqLine7.BillingDescription;
                    result.BuyerBillingContactPerson7th_1 = QCAReqLine7.BillingContactPersonName;
                    result.BuyerBillingContactPersonPosition7th_1 = QCAReqLine7.BillingContactPersonPosition;
                    result.BuyerBillingAddress7th_1 = QCAReqLine7.BuyerBillingAddress;
                    result.BuyerBillingContactPersonTel7th_1 = QCAReqLine7.BillingContactPersonPhone;
                    result.ReceiptDescriptiong7th_1 = QCAReqLine7.ReceiptDescription;
                    result.PaymentMethod7th_2 = QCAReqLine7.MethodOfPayment;
                    result.BuyerReceiptContactPerson7th_1 = QCAReqLine7.ReceiptContactPersonName;
                    result.BuyerReceiptContactPersonPosition7th_1 = QCAReqLine7.ReceiptContactPersonPosition;
                    result.BuyerReceiptAddress7th_1 = QCAReqLine7.BuyerReceiptAddress;
                    result.BuyerReceiptContactPersonTel7th_1 = QCAReqLine7.ReceiptContactPersonPhone;
                    result.MarketingComment7th = QCAReqLine7.MarketingComment;
                    result.CreditComment7th = QCAReqLine7.CreditComment;
                    result.ApproverComment7th = QCAReqLine7.ApproverComment;
                    result.AcceptanceDocument7th_1 = QCAReqLine7.AcceptanceDocument == true ? TextConstants.Yes : TextConstants.No;
                    //R03
                    result.AROutstandingAllCustomer_7 = QCAReqLine7.AllCustomerBuyerOutstanding;
                    result.PurchaseConditionCust_7 = QCAReqLine7.LineCondition;
                    //R03

                    #region QCreditAppReqLineFin
                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine7.ListFinancial, 0))
                    {
                        result.BuyerFinYearFirst1_7th_1 = QCAReqLine7.ListFinancial[0].Year;
                        result.BuyerFinRegisteredCapitalFirst1_7th_1 = QCAReqLine7.ListFinancial[0].RegisteredCapital;
                        result.BuyerFinPaidCapitalFirst1_7th_1 = QCAReqLine7.ListFinancial[0].PaidCapital;
                        result.BuyerFinTotalAssetFirst1_7th_1 = QCAReqLine7.ListFinancial[0].TotalAsset;
                        result.BuyerFinTotalLiabilityFirst1_7th_1 = QCAReqLine7.ListFinancial[0].TotalLiability;
                        result.BuyerFinTotalEquityFirst1_7th_1 = QCAReqLine7.ListFinancial[0].TotalEquity;
                        result.BuyerFinYearFirst2_7th_1 = QCAReqLine7.ListFinancial[0].Year;
                        result.BuyerFinTotalRevenueFirst1_7th_1 = QCAReqLine7.ListFinancial[0].TotalRevenue;
                        result.BuyerFinTotalCOGSFirst1_7th_1 = QCAReqLine7.ListFinancial[0].TotalCOGS;
                        result.BuyerFinTotalGrossProfitFirst1_7th_1 = QCAReqLine7.ListFinancial[0].TotalGrossProfit;
                        result.BuyerFinTotalOperExpFirst1_7th_1 = QCAReqLine7.ListFinancial[0].TotalOperExp;
                        result.BuyerFinTotalNetProfitFirst1_7th_1 = QCAReqLine7.ListFinancial[0].TotalNetProfit;
                        result.BuyerFinNetProfitPercent1_7th_1 = QCAReqLine7.ListFinancial[0].NetProfitPercent;
                        result.BuyerFinlDE1_7th_1 = QCAReqLine7.ListFinancial[0].lDE;
                        result.BuyerFinQuickRatio1_7th_1 = QCAReqLine7.ListFinancial[0].QuickRatio;
                        result.BuyerFinIntCoverageRatio1_7th_1 = QCAReqLine7.ListFinancial[0].IntCoverageRatio;
                    }

                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine7.ListFinancial, 1))
                    {
                        result.BuyerFinYearSecond1_7th_1 = QCAReqLine7.ListFinancial[1].Year;
                        result.BuyerFinRegisteredCapitalSecond1_7th_1 = QCAReqLine7.ListFinancial[1].RegisteredCapital;
                        result.BuyerFinPaidCapitalSecond1_7th_1 = QCAReqLine7.ListFinancial[1].PaidCapital;
                        result.BuyerFinTotalAssetSecond1_7th_1 = QCAReqLine7.ListFinancial[1].TotalAsset;
                        result.BuyerFinTotalLiabilitySecond1_7th_1 = QCAReqLine7.ListFinancial[1].TotalLiability;
                        result.BuyerFinTotalEquitySecond1_7th_1 = QCAReqLine7.ListFinancial[1].TotalEquity;
                        result.BuyerFinYearSecond2_7th_1 = QCAReqLine7.ListFinancial[1].Year;
                        result.BuyerFinTotalRevenueSecond1_7th_1 = QCAReqLine7.ListFinancial[1].TotalRevenue;
                        result.BuyerFinTotalCOGSSecond1_7th_1 = QCAReqLine7.ListFinancial[1].TotalCOGS;
                        result.BuyerFinTotalGrossProfitSecond1_7th_1 = QCAReqLine7.ListFinancial[1].TotalGrossProfit;
                        result.BuyerFinTotalOperExpSecond1_7th_1 = QCAReqLine7.ListFinancial[1].TotalOperExp;
                        result.BuyerFinTotalNetProfitSecond1_7th_1 = QCAReqLine7.ListFinancial[1].TotalNetProfit;
                    }

                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine7.ListFinancial, 2))
                    {
                        result.BuyerFinYearThird1_7th_1 = QCAReqLine7.ListFinancial[2].Year;
                        result.BuyerFinRegisteredCapitalThird1_7th_1 = QCAReqLine7.ListFinancial[2].RegisteredCapital;
                        result.BuyerFinPaidCapitalThird1_7th_1 = QCAReqLine7.ListFinancial[2].PaidCapital;
                        result.BuyerFinTotalAssetThird1_7th_1 = QCAReqLine7.ListFinancial[2].TotalAsset;
                        result.BuyerFinTotalLiabilityThird1_7th_1 = QCAReqLine7.ListFinancial[2].TotalLiability;
                        result.BuyerFinTotalEquityThird1_7th_1 = QCAReqLine7.ListFinancial[2].TotalEquity;
                        result.BuyerFinYearThird2_7th_1 = QCAReqLine7.ListFinancial[2].Year;
                        result.BuyerFinTotalRevenueThird1_7th_1 = QCAReqLine7.ListFinancial[2].TotalRevenue;
                        result.BuyerFinTotalCOGSThird1_7th_1 = QCAReqLine7.ListFinancial[2].TotalCOGS;
                        result.BuyerFinTotalGrossProfitThird1_7th_1 = QCAReqLine7.ListFinancial[2].TotalGrossProfit;
                        result.BuyerFinTotalOperExpThird1_7th_1 = QCAReqLine7.ListFinancial[2].TotalOperExp;
                        result.BuyerFinTotalNetProfitThird1_7th_1 = QCAReqLine7.ListFinancial[2].TotalNetProfit;
                    }
                    #endregion QCreditAppReqLineFin

                    #region QDocumentCondition - Billing
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine7.ListBillingDocumentCondition, 0)) result.BillingDocumentFirst1_7th_1 = QCAReqLine7.ListBillingDocumentCondition[0].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine7.ListBillingDocumentCondition, 1)) result.BillingDocumentSecond1_7th_1 = QCAReqLine7.ListBillingDocumentCondition[1].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine7.ListBillingDocumentCondition, 2)) result.BillingDocumentThird1_7th_1 = QCAReqLine7.ListBillingDocumentCondition[2].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine7.ListBillingDocumentCondition, 3)) result.BillingDocumentFourth_7th_1 = QCAReqLine7.ListBillingDocumentCondition[3].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine7.ListBillingDocumentCondition, 4)) result.BillingDocumentFifth_7th_1 = QCAReqLine7.ListBillingDocumentCondition[4].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine7.ListBillingDocumentCondition, 5)) result.BillingDocumentSixth1_7th_1 = QCAReqLine7.ListBillingDocumentCondition[5].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine7.ListBillingDocumentCondition, 6)) result.BillingDocumentSeventh_7th_1 = QCAReqLine7.ListBillingDocumentCondition[6].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine7.ListBillingDocumentCondition, 7)) result.BillingDocumentEigth1_7th_1 = QCAReqLine7.ListBillingDocumentCondition[7].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine7.ListBillingDocumentCondition, 8)) result.BillingDocumentNineth1_7th_1 = QCAReqLine7.ListBillingDocumentCondition[8].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine7.ListBillingDocumentCondition, 9)) result.BillingDocumentTenth_7th_1 = QCAReqLine7.ListBillingDocumentCondition[9].Description;
                    #endregion QDocumentCondition - Billing

                    #region QDocumentCondition - Receipt
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine7.ListReceiptDocumentCondition, 0)) result.ReceiptDocumentFirst1_7th_1 = QCAReqLine7.ListReceiptDocumentCondition[0].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine7.ListReceiptDocumentCondition, 1)) result.ReceiptDocumentSecond1_7th_1 = QCAReqLine7.ListReceiptDocumentCondition[1].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine7.ListReceiptDocumentCondition, 2)) result.ReceiptDocumentThird1_7th_1 = QCAReqLine7.ListReceiptDocumentCondition[2].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine7.ListReceiptDocumentCondition, 3)) result.ReceiptDocumentFourth_7th_1 = QCAReqLine7.ListReceiptDocumentCondition[3].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine7.ListReceiptDocumentCondition, 4)) result.ReceiptDocumentFifth_7th_1 = QCAReqLine7.ListReceiptDocumentCondition[4].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine7.ListReceiptDocumentCondition, 5)) result.ReceiptDocumentSixth1_7th_1 = QCAReqLine7.ListReceiptDocumentCondition[5].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine7.ListReceiptDocumentCondition, 6)) result.ReceiptDocumentSeventh_7th_1 = QCAReqLine7.ListReceiptDocumentCondition[6].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine7.ListReceiptDocumentCondition, 7)) result.ReceiptDocumentEigth1_7th_1 = QCAReqLine7.ListReceiptDocumentCondition[7].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine7.ListReceiptDocumentCondition, 8)) result.ReceiptDocumentNineth1_7th_1 = QCAReqLine7.ListReceiptDocumentCondition[8].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine7.ListReceiptDocumentCondition, 9)) result.ReceiptDocumentTenth_7th_1 = QCAReqLine7.ListReceiptDocumentCondition[9].Description;
                    #endregion QDocumentCondition - Receipt

                    #region QServiceFeeCondition
                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine7.ListServiceFeeCondition, 0))
                    {
                        result.ServiceFeeDesc01_7th_1 = QCAReqLine7.ListServiceFeeCondition[0].ServicefeeType;
                        result.ServiceFeeAmt01_7th_1 = QCAReqLine7.ListServiceFeeCondition[0].AmountBeforeTax;
                        result.ServiceFeeRemark01_7th_1 = QCAReqLine7.ListServiceFeeCondition[0].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine7.ListServiceFeeCondition, 1))
                    {
                        result.ServiceFeeDesc02_7th_1 = QCAReqLine7.ListServiceFeeCondition[1].ServicefeeType;
                        result.ServiceFeeAmt02_7th_1 = QCAReqLine7.ListServiceFeeCondition[1].AmountBeforeTax;
                        result.ServiceFeeRemark02_7th_1 = QCAReqLine7.ListServiceFeeCondition[1].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine7.ListServiceFeeCondition, 2))
                    {
                        result.ServiceFeeDesc03_7th_1 = QCAReqLine7.ListServiceFeeCondition[2].ServicefeeType;
                        result.ServiceFeeAmt03_7th_1 = QCAReqLine7.ListServiceFeeCondition[2].AmountBeforeTax;
                        result.ServiceFeeRemark03_7th_1 = QCAReqLine7.ListServiceFeeCondition[2].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine7.ListServiceFeeCondition, 3))
                    {
                        result.ServiceFeeDesc04_7th_1 = QCAReqLine7.ListServiceFeeCondition[3].ServicefeeType;
                        result.ServiceFeeAmt04_7th_1 = QCAReqLine7.ListServiceFeeCondition[3].AmountBeforeTax;
                        result.ServiceFeeRemark04_7th_1 = QCAReqLine7.ListServiceFeeCondition[3].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine7.ListServiceFeeCondition, 4))
                    {
                        result.ServiceFeeDesc05_7th_1 = QCAReqLine7.ListServiceFeeCondition[4].ServicefeeType;
                        result.ServiceFeeAmt05_7th_1 = QCAReqLine7.ListServiceFeeCondition[4].AmountBeforeTax;
                        result.ServiceFeeRemark05_7th_1 = QCAReqLine7.ListServiceFeeCondition[4].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine7.ListServiceFeeCondition, 5))
                    {
                        result.ServiceFeeDesc06_7th_1 = QCAReqLine7.ListServiceFeeCondition[5].ServicefeeType;
                        result.ServiceFeeAmt06_7th_1 = QCAReqLine7.ListServiceFeeCondition[5].AmountBeforeTax;
                        result.ServiceFeeRemark06_7th_1 = QCAReqLine7.ListServiceFeeCondition[5].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine7.ListServiceFeeCondition, 6))
                    {
                        result.ServiceFeeDesc07_7th_1 = QCAReqLine7.ListServiceFeeCondition[6].ServicefeeType;
                        result.ServiceFeeAmt07_7th_1 = QCAReqLine7.ListServiceFeeCondition[6].AmountBeforeTax;
                        result.ServiceFeeRemark07_7th_1 = QCAReqLine7.ListServiceFeeCondition[6].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine7.ListServiceFeeCondition, 7))
                    {
                        result.ServiceFeeDesc08_7th_1 = QCAReqLine7.ListServiceFeeCondition[7].ServicefeeType;
                        result.ServiceFeeAmt08_7th_1 = QCAReqLine7.ListServiceFeeCondition[7].AmountBeforeTax;
                        result.ServiceFeeRemark08_7th_1 = QCAReqLine7.ListServiceFeeCondition[7].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine7.ListServiceFeeCondition, 8))
                    {
                        result.ServiceFeeDesc09_7th_1 = QCAReqLine7.ListServiceFeeCondition[8].ServicefeeType;
                        result.ServiceFeeAmt09_7th_1 = QCAReqLine7.ListServiceFeeCondition[8].AmountBeforeTax;
                        result.ServiceFeeRemark09_7th_1 = QCAReqLine7.ListServiceFeeCondition[8].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine7.ListServiceFeeCondition, 9))
                    {
                        result.ServiceFeeDesc10_7th_1 = QCAReqLine7.ListServiceFeeCondition[9].ServicefeeType;
                        result.ServiceFeeAmt10_7th_1 = QCAReqLine7.ListServiceFeeCondition[9].AmountBeforeTax;
                        result.ServiceFeeRemark10_7th_1 = QCAReqLine7.ListServiceFeeCondition[9].Description;
                    }
                    #endregion QServiceFeeCondition

                    result.SumServiceFeeAmt_7th_1 = QCAReqLine7.SumServiceFeeCondition;
                }
                #endregion QCAReqLine7
                #region QCAReqLine8
                if (CheckQCAReqLineElementIsNotNull(QCAReqLineList, 7))
                {
                    QCAReqLine QCAReqLine8 = QCAReqLineList[7];
                    result.BuyerName8th_1 = QCAReqLine8.BuyerName;
                    result.BuyerCreditLimitRequest8th_1 = QCAReqLine8.CreditLimitLineRequest;
                    result.BuyerCreditLimitAllCustomer8th_1 = QCAReqLine8.BuyerCreditLimit;
                    result.PurchasePercent8th_1 = QCAReqLine8.MaxPurchasePct;
                    result.AssignmentCondition8th_1 = QCAReqLine8.AssignmentMethodRemark;
                    result.BillingResponsible8th_1 = QCAReqLine8.BillingResponsibleBy;
                    result.PaymentMethod8th_1 = QCAReqLine8.MethodOfPayment;
                    result.BusinessSegment8th_2 = QCAReqLine8.BusinessSegment;
                    result.BuyerNameCARequestLine8th_1 = QCAReqLine8.BuyerName;
                    result.BuyerTaxID8th_1 = QCAReqLine8.TaxId;
                    result.BuyerAddress8th_1 = QCAReqLine8.InvoiceAddress;
                    result.BuyerCompanyRegisteredDate8th_1 = QCAReqLine8.DateOfEstablish;
                    result.BuyerLineOfBusiness8th_1 = QCAReqLine8.LineOfBusiness;
                    result.BuyerBlacklistStatus8th_1 = QCAReqLine8.BlacklistStatus;
                    result.BuyerCreditScoring8th_1 = QCAReqLine8.CreditScoring;
                    result.BuyerCreditLimitRequest8th_2 = QCAReqLine8.CreditLimitLineRequest;
                    result.BuyerCreditLimitAllCustomer8th_2 = QCAReqLine8.BuyerCreditLimit;
                    result.PurchasePercent8th_2 = QCAReqLine8.MaxPurchasePct;
                    result.AssignmentCondition8th_2 = QCAReqLine8.AssignmentMethodRemark;
                    result.FactoringPurchaseFee8th_1 = QCAReqLine8.PurchaseFeePct;
                    result.FactoringPurchaseFeeCalBase8th_1 = SystemStaticData.GetTranslatedMessage(((PurchaseFeeCalculateBase)QCAReqLine8.PurchaseFeeCalculateBase).GetAttrCode());
                    result.BillingResponsible8th_2 = QCAReqLine8.BillingResponsibleBy;
                    result.BillingDescriptiong8th_1 = QCAReqLine8.BillingDescription;
                    result.BuyerBillingContactPerson8th_1 = QCAReqLine8.BillingContactPersonName;
                    result.BuyerBillingContactPersonPosition8th_1 = QCAReqLine8.BillingContactPersonPosition;
                    result.BuyerBillingAddress8th_1 = QCAReqLine8.BuyerBillingAddress;
                    result.BuyerBillingContactPersonTel8th_1 = QCAReqLine8.BillingContactPersonPhone;
                    result.ReceiptDescriptiong8th_1 = QCAReqLine8.ReceiptDescription;
                    result.PaymentMethod8th_2 = QCAReqLine8.MethodOfPayment;
                    result.BuyerReceiptContactPerson8th_1 = QCAReqLine8.ReceiptContactPersonName;
                    result.BuyerReceiptContactPersonPosition8th_1 = QCAReqLine8.ReceiptContactPersonPosition;
                    result.BuyerReceiptAddress8th_1 = QCAReqLine8.BuyerReceiptAddress;
                    result.BuyerReceiptContactPersonTel8th_1 = QCAReqLine8.ReceiptContactPersonPhone;
                    result.MarketingComment8th = QCAReqLine8.MarketingComment;
                    result.CreditComment8th = QCAReqLine8.CreditComment;
                    result.ApproverComment8th = QCAReqLine8.ApproverComment;
                    result.AcceptanceDocument8th_1 = QCAReqLine8.AcceptanceDocument == true ? TextConstants.Yes : TextConstants.No;
                    //R03
                    result.AROutstandingAllCustomer_8 = QCAReqLine8.AllCustomerBuyerOutstanding;
                    result.PurchaseConditionCust_8 = QCAReqLine8.LineCondition;
                    //R03

                    #region QCreditAppReqLineFin
                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine8.ListFinancial, 0))
                    {
                        result.BuyerFinYearFirst1_8th_1 = QCAReqLine8.ListFinancial[0].Year;
                        result.BuyerFinRegisteredCapitalFirst1_8th_1 = QCAReqLine8.ListFinancial[0].RegisteredCapital;
                        result.BuyerFinPaidCapitalFirst1_8th_1 = QCAReqLine8.ListFinancial[0].PaidCapital;
                        result.BuyerFinTotalAssetFirst1_8th_1 = QCAReqLine8.ListFinancial[0].TotalAsset;
                        result.BuyerFinTotalLiabilityFirst1_8th_1 = QCAReqLine8.ListFinancial[0].TotalLiability;
                        result.BuyerFinTotalEquityFirst1_8th_1 = QCAReqLine8.ListFinancial[0].TotalEquity;
                        result.BuyerFinYearFirst2_8th_1 = QCAReqLine8.ListFinancial[0].Year;
                        result.BuyerFinTotalRevenueFirst1_8th_1 = QCAReqLine8.ListFinancial[0].TotalRevenue;
                        result.BuyerFinTotalCOGSFirst1_8th_1 = QCAReqLine8.ListFinancial[0].TotalCOGS;
                        result.BuyerFinTotalGrossProfitFirst1_8th_1 = QCAReqLine8.ListFinancial[0].TotalGrossProfit;
                        result.BuyerFinTotalOperExpFirst1_8th_1 = QCAReqLine8.ListFinancial[0].TotalOperExp;
                        result.BuyerFinTotalNetProfitFirst1_8th_1 = QCAReqLine8.ListFinancial[0].TotalNetProfit;
                        result.BuyerFinNetProfitPercent1_8th_1 = QCAReqLine8.ListFinancial[0].NetProfitPercent;
                        result.BuyerFinlDE1_8th_1 = QCAReqLine8.ListFinancial[0].lDE;
                        result.BuyerFinQuickRatio1_8th_1 = QCAReqLine8.ListFinancial[0].QuickRatio;
                        result.BuyerFinIntCoverageRatio1_8th_1 = QCAReqLine8.ListFinancial[0].IntCoverageRatio;
                    }

                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine8.ListFinancial, 1))
                    {
                        result.BuyerFinYearSecond1_8th_1 = QCAReqLine8.ListFinancial[1].Year;
                        result.BuyerFinRegisteredCapitalSecond1_8th_1 = QCAReqLine8.ListFinancial[1].RegisteredCapital;
                        result.BuyerFinPaidCapitalSecond1_8th_1 = QCAReqLine8.ListFinancial[1].PaidCapital;
                        result.BuyerFinTotalAssetSecond1_8th_1 = QCAReqLine8.ListFinancial[1].TotalAsset;
                        result.BuyerFinTotalLiabilitySecond1_8th_1 = QCAReqLine8.ListFinancial[1].TotalLiability;
                        result.BuyerFinTotalEquitySecond1_8th_1 = QCAReqLine8.ListFinancial[1].TotalEquity;
                        result.BuyerFinYearSecond2_8th_1 = QCAReqLine8.ListFinancial[1].Year;
                        result.BuyerFinTotalRevenueSecond1_8th_1 = QCAReqLine8.ListFinancial[1].TotalRevenue;
                        result.BuyerFinTotalCOGSSecond1_8th_1 = QCAReqLine8.ListFinancial[1].TotalCOGS;
                        result.BuyerFinTotalGrossProfitSecond1_8th_1 = QCAReqLine8.ListFinancial[1].TotalGrossProfit;
                        result.BuyerFinTotalOperExpSecond1_8th_1 = QCAReqLine8.ListFinancial[1].TotalOperExp;
                        result.BuyerFinTotalNetProfitSecond1_8th_1 = QCAReqLine8.ListFinancial[1].TotalNetProfit;
                    }

                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine8.ListFinancial, 2))
                    {
                        result.BuyerFinYearThird1_8th_1 = QCAReqLine8.ListFinancial[2].Year;
                        result.BuyerFinRegisteredCapitalThird1_8th_1 = QCAReqLine8.ListFinancial[2].RegisteredCapital;
                        result.BuyerFinPaidCapitalThird1_8th_1 = QCAReqLine8.ListFinancial[2].PaidCapital;
                        result.BuyerFinTotalAssetThird1_8th_1 = QCAReqLine8.ListFinancial[2].TotalAsset;
                        result.BuyerFinTotalLiabilityThird1_8th_1 = QCAReqLine8.ListFinancial[2].TotalLiability;
                        result.BuyerFinTotalEquityThird1_8th_1 = QCAReqLine8.ListFinancial[2].TotalEquity;
                        result.BuyerFinYearThird2_8th_1 = QCAReqLine8.ListFinancial[2].Year;
                        result.BuyerFinTotalRevenueThird1_8th_1 = QCAReqLine8.ListFinancial[2].TotalRevenue;
                        result.BuyerFinTotalCOGSThird1_8th_1 = QCAReqLine8.ListFinancial[2].TotalCOGS;
                        result.BuyerFinTotalGrossProfitThird1_8th_1 = QCAReqLine8.ListFinancial[2].TotalGrossProfit;
                        result.BuyerFinTotalOperExpThird1_8th_1 = QCAReqLine8.ListFinancial[2].TotalOperExp;
                        result.BuyerFinTotalNetProfitThird1_8th_1 = QCAReqLine8.ListFinancial[2].TotalNetProfit;
                    }
                    #endregion QCreditAppReqLineFin

                    #region QDocumentCondition - Billing
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine8.ListBillingDocumentCondition, 0)) result.BillingDocumentFirst1_8th_1 = QCAReqLine8.ListBillingDocumentCondition[0].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine8.ListBillingDocumentCondition, 1)) result.BillingDocumentSecond1_8th_1 = QCAReqLine8.ListBillingDocumentCondition[1].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine8.ListBillingDocumentCondition, 2)) result.BillingDocumentThird1_8th_1 = QCAReqLine8.ListBillingDocumentCondition[2].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine8.ListBillingDocumentCondition, 3)) result.BillingDocumentFourth_8th_1 = QCAReqLine8.ListBillingDocumentCondition[3].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine8.ListBillingDocumentCondition, 4)) result.BillingDocumentFifth_8th_1 = QCAReqLine8.ListBillingDocumentCondition[4].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine8.ListBillingDocumentCondition, 5)) result.BillingDocumentSixth1_8th_1 = QCAReqLine8.ListBillingDocumentCondition[5].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine8.ListBillingDocumentCondition, 6)) result.BillingDocumentSeventh_8th_1 = QCAReqLine8.ListBillingDocumentCondition[6].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine8.ListBillingDocumentCondition, 7)) result.BillingDocumentEigth1_8th_1 = QCAReqLine8.ListBillingDocumentCondition[7].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine8.ListBillingDocumentCondition, 8)) result.BillingDocumentNineth1_8th_1 = QCAReqLine8.ListBillingDocumentCondition[8].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine8.ListBillingDocumentCondition, 9)) result.BillingDocumentTenth_8th_1 = QCAReqLine8.ListBillingDocumentCondition[9].Description;
                    #endregion QDocumentCondition - Billing

                    #region QDocumentCondition - Receipt
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine8.ListReceiptDocumentCondition, 0)) result.ReceiptDocumentFirst1_8th_1 = QCAReqLine8.ListReceiptDocumentCondition[0].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine8.ListReceiptDocumentCondition, 1)) result.ReceiptDocumentSecond1_8th_1 = QCAReqLine8.ListReceiptDocumentCondition[1].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine8.ListReceiptDocumentCondition, 2)) result.ReceiptDocumentThird1_8th_1 = QCAReqLine8.ListReceiptDocumentCondition[2].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine8.ListReceiptDocumentCondition, 3)) result.ReceiptDocumentFourth_8th_1 = QCAReqLine8.ListReceiptDocumentCondition[3].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine8.ListReceiptDocumentCondition, 4)) result.ReceiptDocumentFifth_8th_1 = QCAReqLine8.ListReceiptDocumentCondition[4].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine8.ListReceiptDocumentCondition, 5)) result.ReceiptDocumentSixth1_8th_1 = QCAReqLine8.ListReceiptDocumentCondition[5].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine8.ListReceiptDocumentCondition, 6)) result.ReceiptDocumentSeventh_8th_1 = QCAReqLine8.ListReceiptDocumentCondition[6].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine8.ListReceiptDocumentCondition, 7)) result.ReceiptDocumentEigth1_8th_1 = QCAReqLine8.ListReceiptDocumentCondition[7].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine8.ListReceiptDocumentCondition, 8)) result.ReceiptDocumentNineth1_8th_1 = QCAReqLine8.ListReceiptDocumentCondition[8].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine8.ListReceiptDocumentCondition, 9)) result.ReceiptDocumentTenth_8th_1 = QCAReqLine8.ListReceiptDocumentCondition[9].Description;
                    #endregion QDocumentCondition - Receipt

                    #region QServiceFeeCondition
                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine8.ListServiceFeeCondition, 0))
                    {
                        result.ServiceFeeDesc01_8th_1 = QCAReqLine8.ListServiceFeeCondition[0].ServicefeeType;
                        result.ServiceFeeAmt01_8th_1 = QCAReqLine8.ListServiceFeeCondition[0].AmountBeforeTax;
                        result.ServiceFeeRemark01_8th_1 = QCAReqLine8.ListServiceFeeCondition[0].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine8.ListServiceFeeCondition, 1))
                    {
                        result.ServiceFeeDesc02_8th_1 = QCAReqLine8.ListServiceFeeCondition[1].ServicefeeType;
                        result.ServiceFeeAmt02_8th_1 = QCAReqLine8.ListServiceFeeCondition[1].AmountBeforeTax;
                        result.ServiceFeeRemark02_8th_1 = QCAReqLine8.ListServiceFeeCondition[1].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine8.ListServiceFeeCondition, 2))
                    {
                        result.ServiceFeeDesc03_8th_1 = QCAReqLine8.ListServiceFeeCondition[2].ServicefeeType;
                        result.ServiceFeeAmt03_8th_1 = QCAReqLine8.ListServiceFeeCondition[2].AmountBeforeTax;
                        result.ServiceFeeRemark03_8th_1 = QCAReqLine8.ListServiceFeeCondition[2].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine8.ListServiceFeeCondition, 3))
                    {
                        result.ServiceFeeDesc04_8th_1 = QCAReqLine8.ListServiceFeeCondition[3].ServicefeeType;
                        result.ServiceFeeAmt04_8th_1 = QCAReqLine8.ListServiceFeeCondition[3].AmountBeforeTax;
                        result.ServiceFeeRemark04_8th_1 = QCAReqLine8.ListServiceFeeCondition[3].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine8.ListServiceFeeCondition, 4))
                    {
                        result.ServiceFeeDesc05_8th_1 = QCAReqLine8.ListServiceFeeCondition[4].ServicefeeType;
                        result.ServiceFeeAmt05_8th_1 = QCAReqLine8.ListServiceFeeCondition[4].AmountBeforeTax;
                        result.ServiceFeeRemark05_8th_1 = QCAReqLine8.ListServiceFeeCondition[4].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine8.ListServiceFeeCondition, 5))
                    {
                        result.ServiceFeeDesc06_8th_1 = QCAReqLine8.ListServiceFeeCondition[5].ServicefeeType;
                        result.ServiceFeeAmt06_8th_1 = QCAReqLine8.ListServiceFeeCondition[5].AmountBeforeTax;
                        result.ServiceFeeRemark06_8th_1 = QCAReqLine8.ListServiceFeeCondition[5].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine8.ListServiceFeeCondition, 6))
                    {
                        result.ServiceFeeDesc07_8th_1 = QCAReqLine8.ListServiceFeeCondition[6].ServicefeeType;
                        result.ServiceFeeAmt07_8th_1 = QCAReqLine8.ListServiceFeeCondition[6].AmountBeforeTax;
                        result.ServiceFeeRemark07_8th_1 = QCAReqLine8.ListServiceFeeCondition[6].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine8.ListServiceFeeCondition, 7))
                    {
                        result.ServiceFeeDesc08_8th_1 = QCAReqLine8.ListServiceFeeCondition[7].ServicefeeType;
                        result.ServiceFeeAmt08_8th_1 = QCAReqLine8.ListServiceFeeCondition[7].AmountBeforeTax;
                        result.ServiceFeeRemark08_8th_1 = QCAReqLine8.ListServiceFeeCondition[7].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine8.ListServiceFeeCondition, 8))
                    {
                        result.ServiceFeeDesc09_8th_1 = QCAReqLine8.ListServiceFeeCondition[8].ServicefeeType;
                        result.ServiceFeeAmt09_8th_1 = QCAReqLine8.ListServiceFeeCondition[8].AmountBeforeTax;
                        result.ServiceFeeRemark09_8th_1 = QCAReqLine8.ListServiceFeeCondition[8].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine8.ListServiceFeeCondition, 9))
                    {
                        result.ServiceFeeDesc10_8th_1 = QCAReqLine8.ListServiceFeeCondition[9].ServicefeeType;
                        result.ServiceFeeAmt10_8th_1 = QCAReqLine8.ListServiceFeeCondition[9].AmountBeforeTax;
                        result.ServiceFeeRemark10_8th_1 = QCAReqLine8.ListServiceFeeCondition[9].Description;
                    }
                    #endregion QServiceFeeCondition

                    result.SumServiceFeeAmt_8th_1 = QCAReqLine8.SumServiceFeeCondition;
                }
                #endregion QCAReqLine8
                #region QCAReqLine9
                if (CheckQCAReqLineElementIsNotNull(QCAReqLineList, 8))
                {
                    QCAReqLine QCAReqLine9 = QCAReqLineList[8];
                    result.BuyerName9th_1 = QCAReqLine9.BuyerName;
                    result.BuyerCreditLimitRequest9th_1 = QCAReqLine9.CreditLimitLineRequest;
                    result.BuyerCreditLimitAllCustomer9th_1 = QCAReqLine9.BuyerCreditLimit;
                    result.PurchasePercent9th_1 = QCAReqLine9.MaxPurchasePct;
                    result.AssignmentCondition9th_1 = QCAReqLine9.AssignmentMethodRemark;
                    result.BillingResponsible9th_1 = QCAReqLine9.BillingResponsibleBy;
                    result.PaymentMethod9th_1 = QCAReqLine9.MethodOfPayment;
                    result.BusinessSegment9th_2 = QCAReqLine9.BusinessSegment;
                    result.BuyerNameCARequestLine9th_1 = QCAReqLine9.BuyerName;
                    result.BuyerTaxID9th_1 = QCAReqLine9.TaxId;
                    result.BuyerAddress9th_1 = QCAReqLine9.InvoiceAddress;
                    result.BuyerCompanyRegisteredDate9th_1 = QCAReqLine9.DateOfEstablish;
                    result.BuyerLineOfBusiness9th_1 = QCAReqLine9.LineOfBusiness;
                    result.BuyerBlacklistStatus9th_1 = QCAReqLine9.BlacklistStatus;
                    result.BuyerCreditScoring9th_1 = QCAReqLine9.CreditScoring;
                    result.BuyerCreditLimitRequest9th_2 = QCAReqLine9.CreditLimitLineRequest;
                    result.BuyerCreditLimitAllCustomer9th_2 = QCAReqLine9.BuyerCreditLimit;
                    result.PurchasePercent9th_2 = QCAReqLine9.MaxPurchasePct;
                    result.AssignmentCondition9th_2 = QCAReqLine9.AssignmentMethodRemark;
                    result.FactoringPurchaseFee9th_1 = QCAReqLine9.PurchaseFeePct;
                    result.FactoringPurchaseFeeCalBase9th_1 = SystemStaticData.GetTranslatedMessage(((PurchaseFeeCalculateBase)QCAReqLine9.PurchaseFeeCalculateBase).GetAttrCode());
                    result.BillingResponsible9th_2 = QCAReqLine9.BillingResponsibleBy;
                    result.BillingDescriptiong9th_1 = QCAReqLine9.BillingDescription;
                    result.BuyerBillingContactPerson9th_1 = QCAReqLine9.BillingContactPersonName;
                    result.BuyerBillingContactPersonPosition9th_1 = QCAReqLine9.BillingContactPersonPosition;
                    result.BuyerBillingAddress9th_1 = QCAReqLine9.BuyerBillingAddress;
                    result.BuyerBillingContactPersonTel9th_1 = QCAReqLine9.BillingContactPersonPhone;
                    result.ReceiptDescriptiong9th_1 = QCAReqLine9.ReceiptDescription;
                    result.PaymentMethod9th_2 = QCAReqLine9.MethodOfPayment;
                    result.BuyerReceiptContactPerson9th_1 = QCAReqLine9.ReceiptContactPersonName;
                    result.BuyerReceiptContactPersonPosition9th_1 = QCAReqLine9.ReceiptContactPersonPosition;
                    result.BuyerReceiptAddress9th_1 = QCAReqLine9.BuyerReceiptAddress;
                    result.BuyerReceiptContactPersonTel9th_1 = QCAReqLine9.ReceiptContactPersonPhone;
                    result.MarketingComment9th = QCAReqLine9.MarketingComment;
                    result.CreditComment9th = QCAReqLine9.CreditComment;
                    result.ApproverComment9th = QCAReqLine9.ApproverComment;
                    result.AcceptanceDocument9th_1 = QCAReqLine9.AcceptanceDocument == true ? TextConstants.Yes : TextConstants.No;
                    //R03
                    result.AROutstandingAllCustomer_9 = QCAReqLine9.AllCustomerBuyerOutstanding;
                    result.PurchaseConditionCust_9 = QCAReqLine9.LineCondition;
                    //R03

                    #region QCreditAppReqLineFin
                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine9.ListFinancial, 0))
                    {
                        result.BuyerFinYearFirst1_9th_1 = QCAReqLine9.ListFinancial[0].Year;
                        result.BuyerFinRegisteredCapitalFirst1_9th_1 = QCAReqLine9.ListFinancial[0].RegisteredCapital;
                        result.BuyerFinPaidCapitalFirst1_9th_1 = QCAReqLine9.ListFinancial[0].PaidCapital;
                        result.BuyerFinTotalAssetFirst1_9th_1 = QCAReqLine9.ListFinancial[0].TotalAsset;
                        result.BuyerFinTotalLiabilityFirst1_9th_1 = QCAReqLine9.ListFinancial[0].TotalLiability;
                        result.BuyerFinTotalEquityFirst1_9th_1 = QCAReqLine9.ListFinancial[0].TotalEquity;
                        result.BuyerFinYearFirst2_9th_1 = QCAReqLine9.ListFinancial[0].Year;
                        result.BuyerFinTotalRevenueFirst1_9th_1 = QCAReqLine9.ListFinancial[0].TotalRevenue;
                        result.BuyerFinTotalCOGSFirst1_9th_1 = QCAReqLine9.ListFinancial[0].TotalCOGS;
                        result.BuyerFinTotalGrossProfitFirst1_9th_1 = QCAReqLine9.ListFinancial[0].TotalGrossProfit;
                        result.BuyerFinTotalOperExpFirst1_9th_1 = QCAReqLine9.ListFinancial[0].TotalOperExp;
                        result.BuyerFinTotalNetProfitFirst1_9th_1 = QCAReqLine9.ListFinancial[0].TotalNetProfit;
                        result.BuyerFinNetProfitPercent1_9th_1 = QCAReqLine9.ListFinancial[0].NetProfitPercent;
                        result.BuyerFinlDE1_9th_1 = QCAReqLine9.ListFinancial[0].lDE;
                        result.BuyerFinQuickRatio1_9th_1 = QCAReqLine9.ListFinancial[0].QuickRatio;
                        result.BuyerFinIntCoverageRatio1_9th_1 = QCAReqLine9.ListFinancial[0].IntCoverageRatio;
                    }

                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine9.ListFinancial, 1))
                    {
                        result.BuyerFinYearSecond1_9th_1 = QCAReqLine9.ListFinancial[1].Year;
                        result.BuyerFinRegisteredCapitalSecond1_9th_1 = QCAReqLine9.ListFinancial[1].RegisteredCapital;
                        result.BuyerFinPaidCapitalSecond1_9th_1 = QCAReqLine9.ListFinancial[1].PaidCapital;
                        result.BuyerFinTotalAssetSecond1_9th_1 = QCAReqLine9.ListFinancial[1].TotalAsset;
                        result.BuyerFinTotalLiabilitySecond1_9th_1 = QCAReqLine9.ListFinancial[1].TotalLiability;
                        result.BuyerFinTotalEquitySecond1_9th_1 = QCAReqLine9.ListFinancial[1].TotalEquity;
                        result.BuyerFinYearSecond2_9th_1 = QCAReqLine9.ListFinancial[1].Year;
                        result.BuyerFinTotalRevenueSecond1_9th_1 = QCAReqLine9.ListFinancial[1].TotalRevenue;
                        result.BuyerFinTotalCOGSSecond1_9th_1 = QCAReqLine9.ListFinancial[1].TotalCOGS;
                        result.BuyerFinTotalGrossProfitSecond1_9th_1 = QCAReqLine9.ListFinancial[1].TotalGrossProfit;
                        result.BuyerFinTotalOperExpSecond1_9th_1 = QCAReqLine9.ListFinancial[1].TotalOperExp;
                        result.BuyerFinTotalNetProfitSecond1_9th_1 = QCAReqLine9.ListFinancial[1].TotalNetProfit;
                    }

                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine9.ListFinancial, 2))
                    {
                        result.BuyerFinYearThird1_9th_1 = QCAReqLine9.ListFinancial[2].Year;
                        result.BuyerFinRegisteredCapitalThird1_9th_1 = QCAReqLine9.ListFinancial[2].RegisteredCapital;
                        result.BuyerFinPaidCapitalThird1_9th_1 = QCAReqLine9.ListFinancial[2].PaidCapital;
                        result.BuyerFinTotalAssetThird1_9th_1 = QCAReqLine9.ListFinancial[2].TotalAsset;
                        result.BuyerFinTotalLiabilityThird1_9th_1 = QCAReqLine9.ListFinancial[2].TotalLiability;
                        result.BuyerFinTotalEquityThird1_9th_1 = QCAReqLine9.ListFinancial[2].TotalEquity;
                        result.BuyerFinYearThird2_9th_1 = QCAReqLine9.ListFinancial[2].Year;
                        result.BuyerFinTotalRevenueThird1_9th_1 = QCAReqLine9.ListFinancial[2].TotalRevenue;
                        result.BuyerFinTotalCOGSThird1_9th_1 = QCAReqLine9.ListFinancial[2].TotalCOGS;
                        result.BuyerFinTotalGrossProfitThird1_9th_1 = QCAReqLine9.ListFinancial[2].TotalGrossProfit;
                        result.BuyerFinTotalOperExpThird1_9th_1 = QCAReqLine9.ListFinancial[2].TotalOperExp;
                        result.BuyerFinTotalNetProfitThird1_9th_1 = QCAReqLine9.ListFinancial[2].TotalNetProfit;
                    }
                    #endregion QCreditAppReqLineFin

                    #region QDocumentCondition - Billing
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine9.ListBillingDocumentCondition, 0)) result.BillingDocumentFirst1_9th_1 = QCAReqLine9.ListBillingDocumentCondition[0].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine9.ListBillingDocumentCondition, 1)) result.BillingDocumentSecond1_9th_1 = QCAReqLine9.ListBillingDocumentCondition[1].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine9.ListBillingDocumentCondition, 2)) result.BillingDocumentThird1_9th_1 = QCAReqLine9.ListBillingDocumentCondition[2].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine9.ListBillingDocumentCondition, 3)) result.BillingDocumentFourth_9th_1 = QCAReqLine9.ListBillingDocumentCondition[3].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine9.ListBillingDocumentCondition, 4)) result.BillingDocumentFifth_9th_1 = QCAReqLine9.ListBillingDocumentCondition[4].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine9.ListBillingDocumentCondition, 5)) result.BillingDocumentSixth1_9th_1 = QCAReqLine9.ListBillingDocumentCondition[5].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine9.ListBillingDocumentCondition, 6)) result.BillingDocumentSeventh_9th_1 = QCAReqLine9.ListBillingDocumentCondition[6].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine9.ListBillingDocumentCondition, 7)) result.BillingDocumentEigth1_9th_1 = QCAReqLine9.ListBillingDocumentCondition[7].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine9.ListBillingDocumentCondition, 8)) result.BillingDocumentNineth1_9th_1 = QCAReqLine9.ListBillingDocumentCondition[8].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine9.ListBillingDocumentCondition, 9)) result.BillingDocumentTenth_9th_1 = QCAReqLine9.ListBillingDocumentCondition[9].Description;
                    #endregion QDocumentCondition - Billing

                    #region QDocumentCondition - Receipt
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine9.ListReceiptDocumentCondition, 0)) result.ReceiptDocumentFirst1_9th_1 = QCAReqLine9.ListReceiptDocumentCondition[0].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine9.ListReceiptDocumentCondition, 1)) result.ReceiptDocumentSecond1_9th_1 = QCAReqLine9.ListReceiptDocumentCondition[1].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine9.ListReceiptDocumentCondition, 2)) result.ReceiptDocumentThird1_9th_1 = QCAReqLine9.ListReceiptDocumentCondition[2].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine9.ListReceiptDocumentCondition, 3)) result.ReceiptDocumentFourth_9th_1 = QCAReqLine9.ListReceiptDocumentCondition[3].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine9.ListReceiptDocumentCondition, 4)) result.ReceiptDocumentFifth_9th_1 = QCAReqLine9.ListReceiptDocumentCondition[4].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine9.ListReceiptDocumentCondition, 5)) result.ReceiptDocumentSixth1_9th_1 = QCAReqLine9.ListReceiptDocumentCondition[5].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine9.ListReceiptDocumentCondition, 6)) result.ReceiptDocumentSeventh_9th_1 = QCAReqLine9.ListReceiptDocumentCondition[6].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine9.ListReceiptDocumentCondition, 7)) result.ReceiptDocumentEigth1_9th_1 = QCAReqLine9.ListReceiptDocumentCondition[7].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine9.ListReceiptDocumentCondition, 8)) result.ReceiptDocumentNineth1_9th_1 = QCAReqLine9.ListReceiptDocumentCondition[8].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine9.ListReceiptDocumentCondition, 9)) result.ReceiptDocumentTenth_9th_1 = QCAReqLine9.ListReceiptDocumentCondition[9].Description;
                    #endregion QDocumentCondition - Receipt

                    #region QServiceFeeCondition
                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine9.ListServiceFeeCondition, 0))
                    {
                        result.ServiceFeeDesc01_9th_1 = QCAReqLine9.ListServiceFeeCondition[0].ServicefeeType;
                        result.ServiceFeeAmt01_9th_1 = QCAReqLine9.ListServiceFeeCondition[0].AmountBeforeTax;
                        result.ServiceFeeRemark01_9th_1 = QCAReqLine9.ListServiceFeeCondition[0].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine9.ListServiceFeeCondition, 1))
                    {
                        result.ServiceFeeDesc02_9th_1 = QCAReqLine9.ListServiceFeeCondition[1].ServicefeeType;
                        result.ServiceFeeAmt02_9th_1 = QCAReqLine9.ListServiceFeeCondition[1].AmountBeforeTax;
                        result.ServiceFeeRemark02_9th_1 = QCAReqLine9.ListServiceFeeCondition[1].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine9.ListServiceFeeCondition, 2))
                    {
                        result.ServiceFeeDesc03_9th_1 = QCAReqLine9.ListServiceFeeCondition[2].ServicefeeType;
                        result.ServiceFeeAmt03_9th_1 = QCAReqLine9.ListServiceFeeCondition[2].AmountBeforeTax;
                        result.ServiceFeeRemark03_9th_1 = QCAReqLine9.ListServiceFeeCondition[2].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine9.ListServiceFeeCondition, 3))
                    {
                        result.ServiceFeeDesc04_9th_1 = QCAReqLine9.ListServiceFeeCondition[3].ServicefeeType;
                        result.ServiceFeeAmt04_9th_1 = QCAReqLine9.ListServiceFeeCondition[3].AmountBeforeTax;
                        result.ServiceFeeRemark04_9th_1 = QCAReqLine9.ListServiceFeeCondition[3].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine9.ListServiceFeeCondition, 4))
                    {
                        result.ServiceFeeDesc05_9th_1 = QCAReqLine9.ListServiceFeeCondition[4].ServicefeeType;
                        result.ServiceFeeAmt05_9th_1 = QCAReqLine9.ListServiceFeeCondition[4].AmountBeforeTax;
                        result.ServiceFeeRemark05_9th_1 = QCAReqLine9.ListServiceFeeCondition[4].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine9.ListServiceFeeCondition, 5))
                    {
                        result.ServiceFeeDesc06_9th_1 = QCAReqLine9.ListServiceFeeCondition[5].ServicefeeType;
                        result.ServiceFeeAmt06_9th_1 = QCAReqLine9.ListServiceFeeCondition[5].AmountBeforeTax;
                        result.ServiceFeeRemark06_9th_1 = QCAReqLine9.ListServiceFeeCondition[5].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine9.ListServiceFeeCondition, 6))
                    {
                        result.ServiceFeeDesc07_9th_1 = QCAReqLine9.ListServiceFeeCondition[6].ServicefeeType;
                        result.ServiceFeeAmt07_9th_1 = QCAReqLine9.ListServiceFeeCondition[6].AmountBeforeTax;
                        result.ServiceFeeRemark07_9th_1 = QCAReqLine9.ListServiceFeeCondition[6].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine9.ListServiceFeeCondition, 7))
                    {
                        result.ServiceFeeDesc08_9th_1 = QCAReqLine9.ListServiceFeeCondition[7].ServicefeeType;
                        result.ServiceFeeAmt08_9th_1 = QCAReqLine9.ListServiceFeeCondition[7].AmountBeforeTax;
                        result.ServiceFeeRemark08_9th_1 = QCAReqLine9.ListServiceFeeCondition[7].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine9.ListServiceFeeCondition, 8))
                    {
                        result.ServiceFeeDesc09_9th_1 = QCAReqLine9.ListServiceFeeCondition[8].ServicefeeType;
                        result.ServiceFeeAmt09_9th_1 = QCAReqLine9.ListServiceFeeCondition[8].AmountBeforeTax;
                        result.ServiceFeeRemark09_9th_1 = QCAReqLine9.ListServiceFeeCondition[8].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine9.ListServiceFeeCondition, 9))
                    {
                        result.ServiceFeeDesc10_9th_1 = QCAReqLine9.ListServiceFeeCondition[9].ServicefeeType;
                        result.ServiceFeeAmt10_9th_1 = QCAReqLine9.ListServiceFeeCondition[9].AmountBeforeTax;
                        result.ServiceFeeRemark10_9th_1 = QCAReqLine9.ListServiceFeeCondition[9].Description;
                    }
                    #endregion QServiceFeeCondition

                    result.SumServiceFeeAmt_9th_1 = QCAReqLine9.SumServiceFeeCondition;
                }
                #endregion QCAReqLine9
                #region QCAReqLine10
                if (CheckQCAReqLineElementIsNotNull(QCAReqLineList, 9))
                {
                    QCAReqLine QCAReqLine10 = QCAReqLineList[9];
                    result.BuyerName10th_1 = QCAReqLine10.BuyerName;
                    result.BuyerCreditLimitRequest10th_1 = QCAReqLine10.CreditLimitLineRequest;
                    result.BuyerCreditLimitAllCustomer10th_1 = QCAReqLine10.BuyerCreditLimit;
                    result.PurchasePercent10th_1 = QCAReqLine10.MaxPurchasePct;
                    result.AssignmentCondition10th_1 = QCAReqLine10.AssignmentMethodRemark;
                    result.BillingResponsible10th_1 = QCAReqLine10.BillingResponsibleBy;
                    result.PaymentMethod10th_1 = QCAReqLine10.MethodOfPayment;
                    result.BusinessSegment10th_2 = QCAReqLine10.BusinessSegment;
                    result.BuyerNameCARequestLine10th_1 = QCAReqLine10.BuyerName;
                    result.BuyerTaxID10th_1 = QCAReqLine10.TaxId;
                    result.BuyerAddress10th_1 = QCAReqLine10.InvoiceAddress;
                    result.BuyerCompanyRegisteredDate10th_1 = QCAReqLine10.DateOfEstablish;
                    result.BuyerLineOfBusiness10th_1 = QCAReqLine10.LineOfBusiness;
                    result.BuyerBlacklistStatus10th_1 = QCAReqLine10.BlacklistStatus;
                    result.BuyerCreditScoring10th_1 = QCAReqLine10.CreditScoring;
                    result.BuyerCreditLimitRequest10th_2 = QCAReqLine10.CreditLimitLineRequest;
                    result.BuyerCreditLimitAllCustomer10th_2 = QCAReqLine10.BuyerCreditLimit;
                    result.PurchasePercent10th_2 = QCAReqLine10.MaxPurchasePct;
                    result.AssignmentCondition10th_2 = QCAReqLine10.AssignmentMethodRemark;
                    result.FactoringPurchaseFee10th_1 = QCAReqLine10.PurchaseFeePct;
                    result.FactoringPurchaseFeeCalBase10th_1 = SystemStaticData.GetTranslatedMessage(((PurchaseFeeCalculateBase)QCAReqLine10.PurchaseFeeCalculateBase).GetAttrCode());
                    result.BillingResponsible10th_2 = QCAReqLine10.BillingResponsibleBy;
                    result.BillingDescriptiong10th_1 = QCAReqLine10.BillingDescription;
                    result.BuyerBillingContactPerson10th_1 = QCAReqLine10.BillingContactPersonName;
                    result.BuyerBillingContactPersonPosition10th_1 = QCAReqLine10.BillingContactPersonPosition;
                    result.BuyerBillingAddress10th_1 = QCAReqLine10.BuyerBillingAddress;
                    result.BuyerBillingContactPersonTel10th_1 = QCAReqLine10.BillingContactPersonPhone;
                    result.ReceiptDescriptiong10th_1 = QCAReqLine10.ReceiptDescription;
                    result.PaymentMethod10th_2 = QCAReqLine10.MethodOfPayment;
                    result.BuyerReceiptContactPerson10th_1 = QCAReqLine10.ReceiptContactPersonName;
                    result.BuyerReceiptContactPersonPosition10th_1 = QCAReqLine10.ReceiptContactPersonPosition;
                    result.BuyerReceiptAddress10th_1 = QCAReqLine10.BuyerReceiptAddress;
                    result.BuyerReceiptContactPersonTel10th_1 = QCAReqLine10.ReceiptContactPersonPhone;
                    result.MarketingComment10th = QCAReqLine10.MarketingComment;
                    result.CreditComment10th = QCAReqLine10.CreditComment;
                    result.ApproverComment10th = QCAReqLine10.ApproverComment;
                    result.AcceptanceDocument10th_1 = QCAReqLine10.AcceptanceDocument == true ? TextConstants.Yes : TextConstants.No;
                    //R03
                    result.AROutstandingAllCustomer_10 = QCAReqLine10.AllCustomerBuyerOutstanding;
                    result.PurchaseConditionCust_10 = QCAReqLine10.LineCondition;
                    //R03

                    #region QCreditAppReqLineFin
                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine10.ListFinancial, 0))
                    {
                        result.BuyerFinYearFirst1_10th_1 = QCAReqLine10.ListFinancial[0].Year;
                        result.BuyerFinRegisteredCapitalFirst1_10th_1 = QCAReqLine10.ListFinancial[0].RegisteredCapital;
                        result.BuyerFinPaidCapitalFirst1_10th_1 = QCAReqLine10.ListFinancial[0].PaidCapital;
                        result.BuyerFinTotalAssetFirst1_10th_1 = QCAReqLine10.ListFinancial[0].TotalAsset;
                        result.BuyerFinTotalLiabilityFirst1_10th_1 = QCAReqLine10.ListFinancial[0].TotalLiability;
                        result.BuyerFinTotalEquityFirst1_10th_1 = QCAReqLine10.ListFinancial[0].TotalEquity;
                        result.BuyerFinYearFirst2_10th_1 = QCAReqLine10.ListFinancial[0].Year;
                        result.BuyerFinTotalRevenueFirst1_10th_1 = QCAReqLine10.ListFinancial[0].TotalRevenue;
                        result.BuyerFinTotalCOGSFirst1_10th_1 = QCAReqLine10.ListFinancial[0].TotalCOGS;
                        result.BuyerFinTotalGrossProfitFirst1_10th_1 = QCAReqLine10.ListFinancial[0].TotalGrossProfit;
                        result.BuyerFinTotalOperExpFirst1_10th_1 = QCAReqLine10.ListFinancial[0].TotalOperExp;
                        result.BuyerFinTotalNetProfitFirst1_10th_1 = QCAReqLine10.ListFinancial[0].TotalNetProfit;
                        result.BuyerFinNetProfitPercent1_10th_1 = QCAReqLine10.ListFinancial[0].NetProfitPercent;
                        result.BuyerFinlDE1_10th_1 = QCAReqLine10.ListFinancial[0].lDE;
                        result.BuyerFinQuickRatio1_10th_1 = QCAReqLine10.ListFinancial[0].QuickRatio;
                        result.BuyerFinIntCoverageRatio1_10th_1 = QCAReqLine10.ListFinancial[0].IntCoverageRatio;
                    }

                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine10.ListFinancial, 1))
                    {
                        result.BuyerFinYearSecond1_10th_1 = QCAReqLine10.ListFinancial[1].Year;
                        result.BuyerFinRegisteredCapitalSecond1_10th_1 = QCAReqLine10.ListFinancial[1].RegisteredCapital;
                        result.BuyerFinPaidCapitalSecond1_10th_1 = QCAReqLine10.ListFinancial[1].PaidCapital;
                        result.BuyerFinTotalAssetSecond1_10th_1 = QCAReqLine10.ListFinancial[1].TotalAsset;
                        result.BuyerFinTotalLiabilitySecond1_10th_1 = QCAReqLine10.ListFinancial[1].TotalLiability;
                        result.BuyerFinTotalEquitySecond1_10th_1 = QCAReqLine10.ListFinancial[1].TotalEquity;
                        result.BuyerFinYearSecond2_10th_1 = QCAReqLine10.ListFinancial[1].Year;
                        result.BuyerFinTotalRevenueSecond1_10th_1 = QCAReqLine10.ListFinancial[1].TotalRevenue;
                        result.BuyerFinTotalCOGSSecond1_10th_1 = QCAReqLine10.ListFinancial[1].TotalCOGS;
                        result.BuyerFinTotalGrossProfitSecond1_10th_1 = QCAReqLine10.ListFinancial[1].TotalGrossProfit;
                        result.BuyerFinTotalOperExpSecond1_10th_1 = QCAReqLine10.ListFinancial[1].TotalOperExp;
                        result.BuyerFinTotalNetProfitSecond1_10th_1 = QCAReqLine10.ListFinancial[1].TotalNetProfit;
                    }

                    if (CheckQCreditAppReqLineFinElementIsNotNull(QCAReqLine10.ListFinancial, 2))
                    {
                        result.BuyerFinYearThird1_10th_1 = QCAReqLine10.ListFinancial[2].Year;
                        result.BuyerFinRegisteredCapitalThird1_10th_1 = QCAReqLine10.ListFinancial[2].RegisteredCapital;
                        result.BuyerFinPaidCapitalThird1_10th_1 = QCAReqLine10.ListFinancial[2].PaidCapital;
                        result.BuyerFinTotalAssetThird1_10th_1 = QCAReqLine10.ListFinancial[2].TotalAsset;
                        result.BuyerFinTotalLiabilityThird1_10th_1 = QCAReqLine10.ListFinancial[2].TotalLiability;
                        result.BuyerFinTotalEquityThird1_10th_1 = QCAReqLine10.ListFinancial[2].TotalEquity;
                        result.BuyerFinYearThird2_10th_1 = QCAReqLine10.ListFinancial[2].Year;
                        result.BuyerFinTotalRevenueThird1_10th_1 = QCAReqLine10.ListFinancial[2].TotalRevenue;
                        result.BuyerFinTotalCOGSThird1_10th_1 = QCAReqLine10.ListFinancial[2].TotalCOGS;
                        result.BuyerFinTotalGrossProfitThird1_10th_1 = QCAReqLine10.ListFinancial[2].TotalGrossProfit;
                        result.BuyerFinTotalOperExpThird1_10th_1 = QCAReqLine10.ListFinancial[2].TotalOperExp;
                        result.BuyerFinTotalNetProfitThird1_10th_1 = QCAReqLine10.ListFinancial[2].TotalNetProfit;
                    }
                    #endregion QCreditAppReqLineFin

                    #region QDocumentCondition - Billing
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine10.ListBillingDocumentCondition, 0)) result.BillingDocumentFirst1_10th_1 = QCAReqLine10.ListBillingDocumentCondition[0].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine10.ListBillingDocumentCondition, 1)) result.BillingDocumentSecond1_10th_1 = QCAReqLine10.ListBillingDocumentCondition[1].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine10.ListBillingDocumentCondition, 2)) result.BillingDocumentThird1_10th_1 = QCAReqLine10.ListBillingDocumentCondition[2].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine10.ListBillingDocumentCondition, 3)) result.BillingDocumentFourth_10th_1 = QCAReqLine10.ListBillingDocumentCondition[3].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine10.ListBillingDocumentCondition, 4)) result.BillingDocumentFifth_10th_1 = QCAReqLine10.ListBillingDocumentCondition[4].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine10.ListBillingDocumentCondition, 5)) result.BillingDocumentSixth1_10th_1 = QCAReqLine10.ListBillingDocumentCondition[5].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine10.ListBillingDocumentCondition, 6)) result.BillingDocumentSeventh_10th_1 = QCAReqLine10.ListBillingDocumentCondition[6].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine10.ListBillingDocumentCondition, 7)) result.BillingDocumentEigth1_10th_1 = QCAReqLine10.ListBillingDocumentCondition[7].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine10.ListBillingDocumentCondition, 8)) result.BillingDocumentNineth1_10th_1 = QCAReqLine10.ListBillingDocumentCondition[8].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine10.ListBillingDocumentCondition, 9)) result.BillingDocumentTenth_10th_1 = QCAReqLine10.ListBillingDocumentCondition[9].Description;
                    #endregion QDocumentCondition - Billing

                    #region QDocumentCondition - Receipt
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine10.ListReceiptDocumentCondition, 0)) result.ReceiptDocumentFirst1_10th_1 = QCAReqLine10.ListReceiptDocumentCondition[0].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine10.ListReceiptDocumentCondition, 1)) result.ReceiptDocumentSecond1_10th_1 = QCAReqLine10.ListReceiptDocumentCondition[1].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine10.ListReceiptDocumentCondition, 2)) result.ReceiptDocumentThird1_10th_1 = QCAReqLine10.ListReceiptDocumentCondition[2].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine10.ListReceiptDocumentCondition, 3)) result.ReceiptDocumentFourth_10th_1 = QCAReqLine10.ListReceiptDocumentCondition[3].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine10.ListReceiptDocumentCondition, 4)) result.ReceiptDocumentFifth_10th_1 = QCAReqLine10.ListReceiptDocumentCondition[4].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine10.ListReceiptDocumentCondition, 5)) result.ReceiptDocumentSixth1_10th_1 = QCAReqLine10.ListReceiptDocumentCondition[5].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine10.ListReceiptDocumentCondition, 6)) result.ReceiptDocumentSeventh_10th_1 = QCAReqLine10.ListReceiptDocumentCondition[6].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine10.ListReceiptDocumentCondition, 7)) result.ReceiptDocumentEigth1_10th_1 = QCAReqLine10.ListReceiptDocumentCondition[7].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine10.ListReceiptDocumentCondition, 8)) result.ReceiptDocumentNineth1_10th_1 = QCAReqLine10.ListReceiptDocumentCondition[8].Description;
                    if (CheckQDocumentConditionElementIsNotNull(QCAReqLine10.ListReceiptDocumentCondition, 9)) result.ReceiptDocumentTenth_10th_1 = QCAReqLine10.ListReceiptDocumentCondition[9].Description;
                    #endregion QDocumentCondition - Receipt

                    #region QServiceFeeCondition
                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine10.ListServiceFeeCondition, 0))
                    {
                        result.ServiceFeeDesc01_10th_1 = QCAReqLine10.ListServiceFeeCondition[0].ServicefeeType;
                        result.ServiceFeeAmt01_10th_1 = QCAReqLine10.ListServiceFeeCondition[0].AmountBeforeTax;
                        result.ServiceFeeRemark01_10th_1 = QCAReqLine10.ListServiceFeeCondition[0].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine10.ListServiceFeeCondition, 1))
                    {
                        result.ServiceFeeDesc02_10th_1 = QCAReqLine10.ListServiceFeeCondition[1].ServicefeeType;
                        result.ServiceFeeAmt02_10th_1 = QCAReqLine10.ListServiceFeeCondition[1].AmountBeforeTax;
                        result.ServiceFeeRemark02_10th_1 = QCAReqLine10.ListServiceFeeCondition[1].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine10.ListServiceFeeCondition, 2))
                    {
                        result.ServiceFeeDesc03_10th_1 = QCAReqLine10.ListServiceFeeCondition[2].ServicefeeType;
                        result.ServiceFeeAmt03_10th_1 = QCAReqLine10.ListServiceFeeCondition[2].AmountBeforeTax;
                        result.ServiceFeeRemark03_10th_1 = QCAReqLine10.ListServiceFeeCondition[2].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine10.ListServiceFeeCondition, 3))
                    {
                        result.ServiceFeeDesc04_10th_1 = QCAReqLine10.ListServiceFeeCondition[3].ServicefeeType;
                        result.ServiceFeeAmt04_10th_1 = QCAReqLine10.ListServiceFeeCondition[3].AmountBeforeTax;
                        result.ServiceFeeRemark04_10th_1 = QCAReqLine10.ListServiceFeeCondition[3].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine10.ListServiceFeeCondition, 4))
                    {
                        result.ServiceFeeDesc05_10th_1 = QCAReqLine10.ListServiceFeeCondition[4].ServicefeeType;
                        result.ServiceFeeAmt05_10th_1 = QCAReqLine10.ListServiceFeeCondition[4].AmountBeforeTax;
                        result.ServiceFeeRemark05_10th_1 = QCAReqLine10.ListServiceFeeCondition[4].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine10.ListServiceFeeCondition, 5))
                    {
                        result.ServiceFeeDesc06_10th_1 = QCAReqLine10.ListServiceFeeCondition[5].ServicefeeType;
                        result.ServiceFeeAmt06_10th_1 = QCAReqLine10.ListServiceFeeCondition[5].AmountBeforeTax;
                        result.ServiceFeeRemark06_10th_1 = QCAReqLine10.ListServiceFeeCondition[5].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine10.ListServiceFeeCondition, 6))
                    {
                        result.ServiceFeeDesc07_10th_1 = QCAReqLine10.ListServiceFeeCondition[6].ServicefeeType;
                        result.ServiceFeeAmt07_10th_1 = QCAReqLine10.ListServiceFeeCondition[6].AmountBeforeTax;
                        result.ServiceFeeRemark07_10th_1 = QCAReqLine10.ListServiceFeeCondition[6].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine10.ListServiceFeeCondition, 7))
                    {
                        result.ServiceFeeDesc08_10th_1 = QCAReqLine10.ListServiceFeeCondition[7].ServicefeeType;
                        result.ServiceFeeAmt08_10th_1 = QCAReqLine10.ListServiceFeeCondition[7].AmountBeforeTax;
                        result.ServiceFeeRemark08_10th_1 = QCAReqLine10.ListServiceFeeCondition[7].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine10.ListServiceFeeCondition, 8))
                    {
                        result.ServiceFeeDesc09_10th_1 = QCAReqLine10.ListServiceFeeCondition[8].ServicefeeType;
                        result.ServiceFeeAmt09_10th_1 = QCAReqLine10.ListServiceFeeCondition[8].AmountBeforeTax;
                        result.ServiceFeeRemark09_10th_1 = QCAReqLine10.ListServiceFeeCondition[8].Description;
                    }

                    if (CheckQServiceFeeConditionElementIsNotNull(QCAReqLine10.ListServiceFeeCondition, 9))
                    {
                        result.ServiceFeeDesc10_10th_1 = QCAReqLine10.ListServiceFeeCondition[9].ServicefeeType;
                        result.ServiceFeeAmt10_10th_1 = QCAReqLine10.ListServiceFeeCondition[9].AmountBeforeTax;
                        result.ServiceFeeRemark10_10th_1 = QCAReqLine10.ListServiceFeeCondition[9].Description;
                    }
                    #endregion QServiceFeeCondition

                    result.SumServiceFeeAmt_10th_1 = QCAReqLine10.SumServiceFeeCondition;
                }
                #endregion QCAReqLine10

                #region QCreditAppReqAssignment
                ICreditAppReqAssignmentRepo creditAppReqAssignmentRepo = new CreditAppReqAssignmentRepo(db);
                List<CreditAppReqAssignment> creditAppReqAssignments = creditAppReqAssignmentRepo.GetCreditAppReqAssignmentByCreditAppReqNoTracking(result.CreditAppRequestTableGUID).OrderBy(o => o.CreatedDateTime).ToList();
                List<QBuyerCreditAppReqAssignment> QCreditAppReqAssignmentList = (from creditAppReqAssignment in creditAppReqAssignments
                                                                                  join buyerTable in db.Set<BuyerTable>()
                                                                                     on creditAppReqAssignment.BuyerTableGUID equals buyerTable.BuyerTableGUID
                                                                                     into ljCreditAppReqAssignment
                                                                                  from buyerTable in ljCreditAppReqAssignment.DefaultIfEmpty()
                                                                                  join buyerAgreementTable in db.Set<BuyerAgreementTable>()
                                                                                     on creditAppReqAssignment.BuyerAgreementTableGUID equals buyerAgreementTable.BuyerAgreementTableGUID
                                                                                     into ljBuyerAgreementTable
                                                                                  from buyerAgreementTable in ljBuyerAgreementTable.DefaultIfEmpty()
                                                                                  join assignmentAgreementTable in db.Set<AssignmentAgreementTable>()
                                                                                     on creditAppReqAssignment.AssignmentAgreementTableGUID equals assignmentAgreementTable.AssignmentAgreementTableGUID
                                                                                     into ljAssignmentAgreementTable
                                                                                  from assignmentAgreementTable in ljAssignmentAgreementTable.DefaultIfEmpty()
                                                                                  select new QBuyerCreditAppReqAssignment
                                                                                  {
                                                                                      IsNew = creditAppReqAssignment.IsNew,
                                                                                      AssignmentBuyer = buyerTable.BuyerName + " (" + buyerTable.BuyerId + ")",
                                                                                      AssignmentRefAgreementId = buyerAgreementTable != null ? buyerAgreementTable.ReferenceAgreementID : "",
                                                                                      AssignmentBuyerAgreementAmount = creditAppReqAssignment.BuyerAgreementAmount,
                                                                                      AssignmentAgreementAmount = creditAppReqAssignment.AssignmentAgreementAmount,
                                                                                      AssignmentAgreementDate = assignmentAgreementTable != null ? assignmentAgreementTable.AgreementDate.DateToString() : "",
                                                                                      RemainingAmount = creditAppReqAssignment.RemainingAmount
                                                                                  }).Take(10).ToList();

                //QSumCreditAppReqAssignment
                result.SumAssignmentBuyerAgreementAmount = creditAppReqAssignments.Select(s => s.BuyerAgreementAmount).Sum();
                result.SumAssignmentAgreementAmount = creditAppReqAssignments.Select(s => s.AssignmentAgreementAmount).Sum();
                result.SumAssignmentRemainingAmount = creditAppReqAssignments.Select(s => s.RemainingAmount).Sum();

                //QCreditAppReqAssignment1
                if (CheckQBuyerCreditAppReqAssignmentElementIsNotNull(QCreditAppReqAssignmentList, 0))
                {
                    QBuyerCreditAppReqAssignment QCreditAppReqAssignment1 = QCreditAppReqAssignmentList[0];
                    result.AssignmentNewFirst1 = QCreditAppReqAssignment1.IsNew == true ? TextConstants.Yes : TextConstants.No;
                    result.AssignmentBuyerFirst1 = QCreditAppReqAssignment1.AssignmentBuyer;
                    result.AssignmentRefAgreementIdFirst1 = QCreditAppReqAssignment1.AssignmentRefAgreementId;
                    result.AssignmentBuyerAgreementAmountFirst1 = QCreditAppReqAssignment1.AssignmentBuyerAgreementAmount;
                    result.AssignmentAgreementAmountFirst1 = QCreditAppReqAssignment1.AssignmentAgreementAmount;
                    result.AssignmentAgreementDateFirst1 = QCreditAppReqAssignment1.AssignmentAgreementDate;
                    result.AssignmentRemainingAmountFirst1 = QCreditAppReqAssignment1.RemainingAmount;
                }

                //QCreditAppReqAssignment2
                if (CheckQBuyerCreditAppReqAssignmentElementIsNotNull(QCreditAppReqAssignmentList, 1))
                {
                    QBuyerCreditAppReqAssignment QCreditAppReqAssignment2 = QCreditAppReqAssignmentList[1];
                    result.AssignmentNewSecond1 = QCreditAppReqAssignment2.IsNew == true ? TextConstants.Yes : TextConstants.No;
                    result.AssignmentBuyerSecond1 = QCreditAppReqAssignment2.AssignmentBuyer;
                    result.AssignmentRefAgreementIdSecond1 = QCreditAppReqAssignment2.AssignmentRefAgreementId;
                    result.AssignmentBuyerAgreementAmountSecond1 = QCreditAppReqAssignment2.AssignmentBuyerAgreementAmount;
                    result.AssignmentAgreementAmountSecond1 = QCreditAppReqAssignment2.AssignmentAgreementAmount;
                    result.AssignmentAgreementDateSecond1 = QCreditAppReqAssignment2.AssignmentAgreementDate;
                    result.AssignmentRemainingAmountSecond1 = QCreditAppReqAssignment2.RemainingAmount;
                }

                //QCreditAppReqAssignment3
                if (CheckQBuyerCreditAppReqAssignmentElementIsNotNull(QCreditAppReqAssignmentList, 2))
                {
                    QBuyerCreditAppReqAssignment QCreditAppReqAssignment3 = QCreditAppReqAssignmentList[2];
                    result.AssignmentNewThird1 = QCreditAppReqAssignment3.IsNew == true ? TextConstants.Yes : TextConstants.No;
                    result.AssignmentBuyerThird1 = QCreditAppReqAssignment3.AssignmentBuyer;
                    result.AssignmentRefAgreementIdThird1 = QCreditAppReqAssignment3.AssignmentRefAgreementId;
                    result.AssignmentBuyerAgreementAmountThird1 = QCreditAppReqAssignment3.AssignmentBuyerAgreementAmount;
                    result.AssignmentAgreementAmountThird1 = QCreditAppReqAssignment3.AssignmentAgreementAmount;
                    result.AssignmentAgreementDateThird1 = QCreditAppReqAssignment3.AssignmentAgreementDate;
                    result.AssignmentRemainingAmountThird1 = QCreditAppReqAssignment3.RemainingAmount;
                }

                //QCreditAppReqAssignment4
                if (CheckQBuyerCreditAppReqAssignmentElementIsNotNull(QCreditAppReqAssignmentList, 3))
                {
                    QBuyerCreditAppReqAssignment QCreditAppReqAssignment4 = QCreditAppReqAssignmentList[3];
                    result.AssignmentNewFourth1 = QCreditAppReqAssignment4.IsNew == true ? TextConstants.Yes : TextConstants.No;
                    result.AssignmentBuyerFourth1 = QCreditAppReqAssignment4.AssignmentBuyer;
                    result.AssignmentRefAgreementIdFourth1 = QCreditAppReqAssignment4.AssignmentRefAgreementId;
                    result.AssignmentBuyerAgreementAmountFourth1 = QCreditAppReqAssignment4.AssignmentBuyerAgreementAmount;
                    result.AssignmentAgreementAmountFourth1 = QCreditAppReqAssignment4.AssignmentAgreementAmount;
                    result.AssignmentAgreementDateFourth1 = QCreditAppReqAssignment4.AssignmentAgreementDate;
                    result.AssignmentRemainingAmountFourth1 = QCreditAppReqAssignment4.RemainingAmount;
                }

                //QCreditAppReqAssignment5
                if (CheckQBuyerCreditAppReqAssignmentElementIsNotNull(QCreditAppReqAssignmentList, 4))
                {
                    QBuyerCreditAppReqAssignment QCreditAppReqAssignment5 = QCreditAppReqAssignmentList[4];
                    result.AssignmentNewFifth1 = QCreditAppReqAssignment5.IsNew == true ? TextConstants.Yes : TextConstants.No;
                    result.AssignmentBuyerFifth1 = QCreditAppReqAssignment5.AssignmentBuyer;
                    result.AssignmentRefAgreementIdFifth1 = QCreditAppReqAssignment5.AssignmentRefAgreementId;
                    result.AssignmentBuyerAgreementAmountFifth1 = QCreditAppReqAssignment5.AssignmentBuyerAgreementAmount;
                    result.AssignmentAgreementAmountFifth1 = QCreditAppReqAssignment5.AssignmentAgreementAmount;
                    result.AssignmentAgreementDateFifth1 = QCreditAppReqAssignment5.AssignmentAgreementDate;
                    result.AssignmentRemainingAmountFifth1 = QCreditAppReqAssignment5.RemainingAmount;
                }

                //QCreditAppReqAssignment6
                if (CheckQBuyerCreditAppReqAssignmentElementIsNotNull(QCreditAppReqAssignmentList, 5))
                {
                    QBuyerCreditAppReqAssignment QCreditAppReqAssignment6 = QCreditAppReqAssignmentList[5];
                    result.AssignmentNewSixth1 = QCreditAppReqAssignment6.IsNew == true ? TextConstants.Yes : TextConstants.No;
                    result.AssignmentBuyerSixth1 = QCreditAppReqAssignment6.AssignmentBuyer;
                    result.AssignmentRefAgreementIdSixth1 = QCreditAppReqAssignment6.AssignmentRefAgreementId;
                    result.AssignmentBuyerAgreementAmountSixth1 = QCreditAppReqAssignment6.AssignmentBuyerAgreementAmount;
                    result.AssignmentAgreementAmountSixth1 = QCreditAppReqAssignment6.AssignmentAgreementAmount;
                    result.AssignmentAgreementDateSixth1 = QCreditAppReqAssignment6.AssignmentAgreementDate;
                    result.AssignmentRemainingAmountSixth1 = QCreditAppReqAssignment6.RemainingAmount;
                }

                //QCreditAppReqAssignment7
                if (CheckQBuyerCreditAppReqAssignmentElementIsNotNull(QCreditAppReqAssignmentList, 6))
                {
                    QBuyerCreditAppReqAssignment QCreditAppReqAssignment7 = QCreditAppReqAssignmentList[6];
                    result.AssignmentNewSeventh1 = QCreditAppReqAssignment7.IsNew == true ? TextConstants.Yes : TextConstants.No;
                    result.AssignmentBuyerSeventh1 = QCreditAppReqAssignment7.AssignmentBuyer;
                    result.AssignmentRefAgreementIdSeventh1 = QCreditAppReqAssignment7.AssignmentRefAgreementId;
                    result.AssignmentBuyerAgreementAmountSeventh1 = QCreditAppReqAssignment7.AssignmentBuyerAgreementAmount;
                    result.AssignmentAgreementAmountSeventh1 = QCreditAppReqAssignment7.AssignmentAgreementAmount;
                    result.AssignmentAgreementDateSeventh1 = QCreditAppReqAssignment7.AssignmentAgreementDate;
                    result.AssignmentRemainingAmountSeventh1 = QCreditAppReqAssignment7.RemainingAmount;
                }

                //QCreditAppReqAssignment8
                if (CheckQBuyerCreditAppReqAssignmentElementIsNotNull(QCreditAppReqAssignmentList, 7))
                {
                    QBuyerCreditAppReqAssignment QCreditAppReqAssignment8 = QCreditAppReqAssignmentList[7];
                    result.AssignmentNewEighth1 = QCreditAppReqAssignment8.IsNew == true ? TextConstants.Yes : TextConstants.No;
                    result.AssignmentBuyerEighth1 = QCreditAppReqAssignment8.AssignmentBuyer;
                    result.AssignmentRefAgreementIdEighth1 = QCreditAppReqAssignment8.AssignmentRefAgreementId;
                    result.AssignmentBuyerAgreementAmountEighth1 = QCreditAppReqAssignment8.AssignmentBuyerAgreementAmount;
                    result.AssignmentAgreementAmountEighth1 = QCreditAppReqAssignment8.AssignmentAgreementAmount;
                    result.AssignmentAgreementDateEighth1 = QCreditAppReqAssignment8.AssignmentAgreementDate;
                    result.AssignmentRemainingAmountEighth1 = QCreditAppReqAssignment8.RemainingAmount;
                }

                //QCreditAppReqAssignment9
                if (CheckQBuyerCreditAppReqAssignmentElementIsNotNull(QCreditAppReqAssignmentList, 8))
                {
                    QBuyerCreditAppReqAssignment QCreditAppReqAssignment9 = QCreditAppReqAssignmentList[8];
                    result.AssignmentNewNineth1 = QCreditAppReqAssignment9.IsNew == true ? TextConstants.Yes : TextConstants.No;
                    result.AssignmentBuyerNineth1 = QCreditAppReqAssignment9.AssignmentBuyer;
                    result.AssignmentRefAgreementIdNineth1 = QCreditAppReqAssignment9.AssignmentRefAgreementId;
                    result.AssignmentBuyerAgreementAmountNineth1 = QCreditAppReqAssignment9.AssignmentBuyerAgreementAmount;
                    result.AssignmentAgreementAmountNineth1 = QCreditAppReqAssignment9.AssignmentAgreementAmount;
                    result.AssignmentAgreementDateNineth1 = QCreditAppReqAssignment9.AssignmentAgreementDate;
                    result.AssignmentRemainingAmountNineth1 = QCreditAppReqAssignment9.RemainingAmount;
                }

                //QCreditAppReqAssignment10
                if (CheckQBuyerCreditAppReqAssignmentElementIsNotNull(QCreditAppReqAssignmentList, 9))
                {
                    QBuyerCreditAppReqAssignment QCreditAppReqAssignment10 = QCreditAppReqAssignmentList[9];
                    result.AssignmentNewTenth1 = QCreditAppReqAssignment10.IsNew == true ? TextConstants.Yes : TextConstants.No;
                    result.AssignmentBuyerTenth1 = QCreditAppReqAssignment10.AssignmentBuyer;
                    result.AssignmentRefAgreementIdTenth1 = QCreditAppReqAssignment10.AssignmentRefAgreementId;
                    result.AssignmentBuyerAgreementAmountTenth1 = QCreditAppReqAssignment10.AssignmentBuyerAgreementAmount;
                    result.AssignmentAgreementAmountTenth1 = QCreditAppReqAssignment10.AssignmentAgreementAmount;
                    result.AssignmentAgreementDateTenth1 = QCreditAppReqAssignment10.AssignmentAgreementDate;
                    result.AssignmentRemainingAmountTenth1 = QCreditAppReqAssignment10.RemainingAmount;
                }
                #endregion QCreditAppReqAssignment

                #region QCreditAppReqBusinessCollateral
                ICreditAppReqBusinessCollateralRepo creditAppReqBusinessCollateralRepo = new CreditAppReqBusinessCollateralRepo(db);
                List<CreditAppReqBusinessCollateral> creditAppReqBusinessCollaterals = creditAppReqBusinessCollateralRepo.GetCreditAppReqBusinessCollateralByCreditAppReqNoTracking(result.CreditAppRequestTableGUID).OrderBy(o => o.CreatedDateTime).ToList();
                List<QBuyerCreditAppReqBusinessCollateral> QCreditAppReqBusinessCollateralList = (from creditAppReqBusinessCollateral in creditAppReqBusinessCollaterals
                                                                                                  join businessCollateralType in db.Set<BusinessCollateralType>()
                                                                                                     on creditAppReqBusinessCollateral.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID
                                                                                                  join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                                                                                                     on creditAppReqBusinessCollateral.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID
                                                                                                  select new QBuyerCreditAppReqBusinessCollateral
                                                                                                  {
                                                                                                      IsNew = creditAppReqBusinessCollateral.IsNew,
                                                                                                      BusinessCollateralType = businessCollateralType.Description,
                                                                                                      BusinessCollateralSubType = businessCollateralSubType.Description,
                                                                                                      Description = creditAppReqBusinessCollateral.Description,
                                                                                                      BusinessCollateralValue = creditAppReqBusinessCollateral.BusinessCollateralValue
                                                                                                  }).Take(15).ToList();

                //QSumCreditAppReqBusinessCollateral
                result.SumBusinessCollateralValue = creditAppReqBusinessCollaterals.Take(10).Select(s => s.BusinessCollateralValue).Sum();
                //QCreditAppReqBusinessCollateral1
                if (CheckQBuyerCreditAppReqBusinessCollateralElementIsNotNull(QCreditAppReqBusinessCollateralList, 0))
                {
                    QBuyerCreditAppReqBusinessCollateral QCreditAppReqBusinessCollateral1 = QCreditAppReqBusinessCollateralList[0];
                    result.BusinessCollateralNewFirst1 = QCreditAppReqBusinessCollateral1.IsNew == true ? TextConstants.Yes : TextConstants.No;
                    result.BusinessCollateralTypeFirst1 = QCreditAppReqBusinessCollateral1.BusinessCollateralType;
                    result.BusinessCollateralSubTypeFirst1 = QCreditAppReqBusinessCollateral1.BusinessCollateralSubType;
                    result.BusinessCollateralDescFirst1 = QCreditAppReqBusinessCollateral1.Description;
                    result.BusinessCollateralValueFirst1 = QCreditAppReqBusinessCollateral1.BusinessCollateralValue;
                }

                //QCreditAppReqBusinessCollateral2
                if (CheckQBuyerCreditAppReqBusinessCollateralElementIsNotNull(QCreditAppReqBusinessCollateralList, 1))
                {
                    QBuyerCreditAppReqBusinessCollateral QCreditAppReqBusinessCollateral2 = QCreditAppReqBusinessCollateralList[1];
                    result.BusinessCollateralNewSecond1 = QCreditAppReqBusinessCollateral2.IsNew == true ? TextConstants.Yes : TextConstants.No;
                    result.BusinessCollateralTypeSecond1 = QCreditAppReqBusinessCollateral2.BusinessCollateralType;
                    result.BusinessCollateralSubTypeSecond1 = QCreditAppReqBusinessCollateral2.BusinessCollateralSubType;
                    result.BusinessCollateralDescSecond1 = QCreditAppReqBusinessCollateral2.Description;
                    result.BusinessCollateralValueSecond1 = QCreditAppReqBusinessCollateral2.BusinessCollateralValue;
                }

                //QCreditAppReqBusinessCollateral3
                if (CheckQBuyerCreditAppReqBusinessCollateralElementIsNotNull(QCreditAppReqBusinessCollateralList, 2))
                {
                    QBuyerCreditAppReqBusinessCollateral QCreditAppReqBusinessCollateral3 = QCreditAppReqBusinessCollateralList[2];
                    result.BusinessCollateralNewThird1 = QCreditAppReqBusinessCollateral3.IsNew == true ? TextConstants.Yes : TextConstants.No;
                    result.BusinessCollateralTypeThird1 = QCreditAppReqBusinessCollateral3.BusinessCollateralType;
                    result.BusinessCollateralSubTypeThird1 = QCreditAppReqBusinessCollateral3.BusinessCollateralSubType;
                    result.BusinessCollateralDescThird1 = QCreditAppReqBusinessCollateral3.Description;
                    result.BusinessCollateralValueThird1 = QCreditAppReqBusinessCollateral3.BusinessCollateralValue;
                }

                //QCreditAppReqBusinessCollateral4
                if (CheckQBuyerCreditAppReqBusinessCollateralElementIsNotNull(QCreditAppReqBusinessCollateralList, 3))
                {
                    QBuyerCreditAppReqBusinessCollateral QCreditAppReqBusinessCollateral4 = QCreditAppReqBusinessCollateralList[3];
                    result.BusinessCollateralNewFourth1 = QCreditAppReqBusinessCollateral4.IsNew == true ? TextConstants.Yes : TextConstants.No;
                    result.BusinessCollateralTypeFourth1 = QCreditAppReqBusinessCollateral4.BusinessCollateralType;
                    result.BusinessCollateralSubTypeFourth1 = QCreditAppReqBusinessCollateral4.BusinessCollateralSubType;
                    result.BusinessCollateralDescFourth1 = QCreditAppReqBusinessCollateral4.Description;
                    result.BusinessCollateralValueFourth1 = QCreditAppReqBusinessCollateral4.BusinessCollateralValue;
                }

                //QCreditAppReqBusinessCollateral5
                if (CheckQBuyerCreditAppReqBusinessCollateralElementIsNotNull(QCreditAppReqBusinessCollateralList, 4))
                {
                    QBuyerCreditAppReqBusinessCollateral QCreditAppReqBusinessCollateral5 = QCreditAppReqBusinessCollateralList[4];
                    result.BusinessCollateralNewFifth1 = QCreditAppReqBusinessCollateral5.IsNew == true ? TextConstants.Yes : TextConstants.No;
                    result.BusinessCollateralTypeFifth1 = QCreditAppReqBusinessCollateral5.BusinessCollateralType;
                    result.BusinessCollateralSubTypeFifth1 = QCreditAppReqBusinessCollateral5.BusinessCollateralSubType;
                    result.BusinessCollateralDescFifth1 = QCreditAppReqBusinessCollateral5.Description;
                    result.BusinessCollateralValueFifth1 = QCreditAppReqBusinessCollateral5.BusinessCollateralValue;
                }

                //QCreditAppReqBusinessCollateral6
                if (CheckQBuyerCreditAppReqBusinessCollateralElementIsNotNull(QCreditAppReqBusinessCollateralList, 5))
                {
                    QBuyerCreditAppReqBusinessCollateral QCreditAppReqBusinessCollateral6 = QCreditAppReqBusinessCollateralList[5];
                    result.BusinessCollateralNewSixth1 = QCreditAppReqBusinessCollateral6.IsNew == true ? TextConstants.Yes : TextConstants.No;
                    result.BusinessCollateralTypeSixth1 = QCreditAppReqBusinessCollateral6.BusinessCollateralType;
                    result.BusinessCollateralSubTypeSixth1 = QCreditAppReqBusinessCollateral6.BusinessCollateralSubType;
                    result.BusinessCollateralDescSixth1 = QCreditAppReqBusinessCollateral6.Description;
                    result.BusinessCollateralValueSixth1 = QCreditAppReqBusinessCollateral6.BusinessCollateralValue;
                }

                //QCreditAppReqBusinessCollateral7
                if (CheckQBuyerCreditAppReqBusinessCollateralElementIsNotNull(QCreditAppReqBusinessCollateralList, 6))
                {
                    QBuyerCreditAppReqBusinessCollateral QCreditAppReqBusinessCollateral7 = QCreditAppReqBusinessCollateralList[6];
                    result.BusinessCollateralNewSeventh1 = QCreditAppReqBusinessCollateral7.IsNew == true ? TextConstants.Yes : TextConstants.No;
                    result.BusinessCollateralTypeSeventh1 = QCreditAppReqBusinessCollateral7.BusinessCollateralType;
                    result.BusinessCollateralSubTypeSeventh1 = QCreditAppReqBusinessCollateral7.BusinessCollateralSubType;
                    result.BusinessCollateralDescSeventh1 = QCreditAppReqBusinessCollateral7.Description;
                    result.BusinessCollateralValueSeventh1 = QCreditAppReqBusinessCollateral7.BusinessCollateralValue;
                }

                //QCreditAppReqBusinessCollateral8
                if (CheckQBuyerCreditAppReqBusinessCollateralElementIsNotNull(QCreditAppReqBusinessCollateralList, 7))
                {
                    QBuyerCreditAppReqBusinessCollateral QCreditAppReqBusinessCollateral8 = QCreditAppReqBusinessCollateralList[7];
                    result.BusinessCollateralNewEighth1 = QCreditAppReqBusinessCollateral8.IsNew == true ? TextConstants.Yes : TextConstants.No;
                    result.BusinessCollateralTypeEighth1 = QCreditAppReqBusinessCollateral8.BusinessCollateralType;
                    result.BusinessCollateralSubTypeEighth1 = QCreditAppReqBusinessCollateral8.BusinessCollateralSubType;
                    result.BusinessCollateralDescEighth1 = QCreditAppReqBusinessCollateral8.Description;
                    result.BusinessCollateralValueEighth1 = QCreditAppReqBusinessCollateral8.BusinessCollateralValue;
                }

                //QCreditAppReqBusinessCollateral9
                if (CheckQBuyerCreditAppReqBusinessCollateralElementIsNotNull(QCreditAppReqBusinessCollateralList, 8))
                {
                    QBuyerCreditAppReqBusinessCollateral QCreditAppReqBusinessCollateral9 = QCreditAppReqBusinessCollateralList[8];
                    result.BusinessCollateralNewNineth1 = QCreditAppReqBusinessCollateral9.IsNew == true ? TextConstants.Yes : TextConstants.No;
                    result.BusinessCollateralTypeNineth1 = QCreditAppReqBusinessCollateral9.BusinessCollateralType;
                    result.BusinessCollateralSubTypeNineth1 = QCreditAppReqBusinessCollateral9.BusinessCollateralSubType;
                    result.BusinessCollateralDescNineth1 = QCreditAppReqBusinessCollateral9.Description;
                    result.BusinessCollateralValueNineth1 = QCreditAppReqBusinessCollateral9.BusinessCollateralValue;
                }

                //QCreditAppReqBusinessCollateral10
                if (CheckQBuyerCreditAppReqBusinessCollateralElementIsNotNull(QCreditAppReqBusinessCollateralList, 9))
                {
                    QBuyerCreditAppReqBusinessCollateral QCreditAppReqBusinessCollateral10 = QCreditAppReqBusinessCollateralList[9];
                    result.BusinessCollateralNewTenth1 = QCreditAppReqBusinessCollateral10.IsNew == true ? TextConstants.Yes : TextConstants.No;
                    result.BusinessCollateralTypeTenth1 = QCreditAppReqBusinessCollateral10.BusinessCollateralType;
                    result.BusinessCollateralSubTypeTenth1 = QCreditAppReqBusinessCollateral10.BusinessCollateralSubType;
                    result.BusinessCollateralDescTenth1 = QCreditAppReqBusinessCollateral10.Description;
                    result.BusinessCollateralValueTenth1 = QCreditAppReqBusinessCollateral10.BusinessCollateralValue;
                }

                //QCreditAppReqBusinessCollateral11
                if (CheckQBuyerCreditAppReqBusinessCollateralElementIsNotNull(QCreditAppReqBusinessCollateralList, 10))
                {
                    QBuyerCreditAppReqBusinessCollateral QCreditAppReqBusinessCollateral11 = QCreditAppReqBusinessCollateralList[10];
                    result.BusinessCollateralNewEleventh1 = QCreditAppReqBusinessCollateral11.IsNew == true ? TextConstants.Yes : TextConstants.No;
                    result.BusinessCollateralTypeEleventh1 = QCreditAppReqBusinessCollateral11.BusinessCollateralType;
                    result.BusinessCollateralSubTypeEleventh1 = QCreditAppReqBusinessCollateral11.BusinessCollateralSubType;
                    result.BusinessCollateralDescEleventh1 = QCreditAppReqBusinessCollateral11.Description;
                    result.BusinessCollateralValueEleventh1 = QCreditAppReqBusinessCollateral11.BusinessCollateralValue;
                }

                //QCreditAppReqBusinessCollateral12
                if (CheckQBuyerCreditAppReqBusinessCollateralElementIsNotNull(QCreditAppReqBusinessCollateralList, 11))
                {
                    QBuyerCreditAppReqBusinessCollateral QCreditAppReqBusinessCollateral12 = QCreditAppReqBusinessCollateralList[11];
                    result.BusinessCollateralNewTwelfth1 = QCreditAppReqBusinessCollateral12.IsNew == true ? TextConstants.Yes : TextConstants.No;
                    result.BusinessCollateralTypeTwelfth1 = QCreditAppReqBusinessCollateral12.BusinessCollateralType;
                    result.BusinessCollateralSubTypeTwelfth1 = QCreditAppReqBusinessCollateral12.BusinessCollateralSubType;
                    result.BusinessCollateralDescTwelfth1 = QCreditAppReqBusinessCollateral12.Description;
                    result.BusinessCollateralValueTwelfth1 = QCreditAppReqBusinessCollateral12.BusinessCollateralValue;
                }

                //QCreditAppReqBusinessCollateral13
                if (CheckQBuyerCreditAppReqBusinessCollateralElementIsNotNull(QCreditAppReqBusinessCollateralList, 12))
                {
                    QBuyerCreditAppReqBusinessCollateral QCreditAppReqBusinessCollateral13 = QCreditAppReqBusinessCollateralList[12];
                    result.BusinessCollateralNewThirteenth1 = QCreditAppReqBusinessCollateral13.IsNew == true ? TextConstants.Yes : TextConstants.No;
                    result.BusinessCollateralTypeThirteenth1 = QCreditAppReqBusinessCollateral13.BusinessCollateralType;
                    result.BusinessCollateralSubTypeThirteenth1 = QCreditAppReqBusinessCollateral13.BusinessCollateralSubType;
                    result.BusinessCollateralDescThirteenth1 = QCreditAppReqBusinessCollateral13.Description;
                    result.BusinessCollateralValueThirteenth1 = QCreditAppReqBusinessCollateral13.BusinessCollateralValue;
                }

                //QCreditAppReqBusinessCollateral14
                if (CheckQBuyerCreditAppReqBusinessCollateralElementIsNotNull(QCreditAppReqBusinessCollateralList, 13))
                {
                    QBuyerCreditAppReqBusinessCollateral QCreditAppReqBusinessCollateral14 = QCreditAppReqBusinessCollateralList[13];
                    result.BusinessCollateralNewFourteenth1 = QCreditAppReqBusinessCollateral14.IsNew == true ? TextConstants.Yes : TextConstants.No;
                    result.BusinessCollateralTypeFourteenth1 = QCreditAppReqBusinessCollateral14.BusinessCollateralType;
                    result.BusinessCollateralSubTypeFourteenth1 = QCreditAppReqBusinessCollateral14.BusinessCollateralSubType;
                    result.BusinessCollateralDescFourteenth1 = QCreditAppReqBusinessCollateral14.Description;
                    result.BusinessCollateralValueFourteenth1 = QCreditAppReqBusinessCollateral14.BusinessCollateralValue;
                }

                //QCreditAppReqBusinessCollateral15
                if (CheckQBuyerCreditAppReqBusinessCollateralElementIsNotNull(QCreditAppReqBusinessCollateralList, 14))
                {
                    QBuyerCreditAppReqBusinessCollateral QCreditAppReqBusinessCollateral15 = QCreditAppReqBusinessCollateralList[14];
                    result.BusinessCollateralNewFifteenth1 = QCreditAppReqBusinessCollateral15.IsNew == true ? TextConstants.Yes : TextConstants.No;
                    result.BusinessCollateralTypeFifteenth1 = QCreditAppReqBusinessCollateral15.BusinessCollateralType;
                    result.BusinessCollateralSubTypeFifteenth1 = QCreditAppReqBusinessCollateral15.BusinessCollateralSubType;
                    result.BusinessCollateralDescFifteenth1 = QCreditAppReqBusinessCollateral15.Description;
                    result.BusinessCollateralValueFifteenth1 = QCreditAppReqBusinessCollateral15.BusinessCollateralValue;
                }
                #endregion QCreditAppReqBusinessCollateral

                #region QActionHistory
                List<QBuyerActionHistory> QActionHistoryList = (from actionHistory in db.Set<ActionHistory>()
                                                                .Where(w => (w.ActivityName == TextConstants.AssistMDApprove 
                                                                            || w.ActivityName == TextConstants.Board2In3Approve 
                                                                            || w.ActivityName == TextConstants.Board3In4Approve) 
                                                                         && (w.ActionName == TextConstants.Approve) 
                                                                         && (w.RefGUID == result.CreditAppRequestTableGUID))
                                                                select new QBuyerActionHistory
                                                                {
                                                                    Comment = actionHistory.Comment,
                                                                    CreatedDateTime = actionHistory.CreatedDateTime,
                                                                    ApproverName = actionHistory.CreatedBy
                                                                }).AsNoTracking().Take(5).ToList();

                if (CheckQBuyerActionHistoryElementIsNotNull(QActionHistoryList, 0))
                {
                    QBuyerActionHistory QBuyerActionHistory1 = QActionHistoryList[0];
                    result.ApproverNameFirst1 = QBuyerActionHistory1.ApproverName;
                    result.ApproverReasonFirst1 = QBuyerActionHistory1.Comment;
                    result.ApprovedDateFirst1 = QBuyerActionHistory1.CreatedDateTime.DateTimeToString();
                }
                if (CheckQBuyerActionHistoryElementIsNotNull(QActionHistoryList, 1))
                {
                    QBuyerActionHistory QBuyerActionHistory2 = QActionHistoryList[1];
                    result.ApproverNameSecond1 = QBuyerActionHistory2.ApproverName;
                    result.ApproverReasonSecond1 = QBuyerActionHistory2.Comment;
                    result.ApprovedDateSecond1 = QBuyerActionHistory2.CreatedDateTime.DateTimeToString();
                }
                if (CheckQBuyerActionHistoryElementIsNotNull(QActionHistoryList, 2))
                {
                    QBuyerActionHistory QBuyerActionHistory3 = QActionHistoryList[2];
                    result.ApproverNameThird1 = QBuyerActionHistory3.ApproverName;
                    result.ApproverReasonThird1 = QBuyerActionHistory3.Comment;
                    result.ApprovedDateThird1 = QBuyerActionHistory3.CreatedDateTime.DateTimeToString();
                }
                if (CheckQBuyerActionHistoryElementIsNotNull(QActionHistoryList, 3))
                {
                    QBuyerActionHistory QBuyerActionHistory4 = QActionHistoryList[3];
                    result.ApproverNameFourth1 = QBuyerActionHistory4.ApproverName;
                    result.ApproverReasonFourth1 = QBuyerActionHistory4.Comment;
                    result.ApprovedDateFourth1 = QBuyerActionHistory4.CreatedDateTime.DateTimeToString();
                }
                if (CheckQBuyerActionHistoryElementIsNotNull(QActionHistoryList, 4))
                {
                    QBuyerActionHistory QBuyerActionHistory5 = QActionHistoryList[4];
                    result.ApproverNameFifth1 = QBuyerActionHistory5.ApproverName;
                    result.ApproverReasonFifth1 = QBuyerActionHistory5.Comment;
                    result.ApprovedDateFifth1 = QBuyerActionHistory5.CreatedDateTime.DateTimeToString();
                }
                #endregion QActionHistory

                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        public QCreditAppReqLineFin_BuyerCreditApplication GetCreditAppReqLineFinancial(List<FinancialStatementTrans> financialStatementTrans, Guid CAReqLineGUID, int year)
        {
            try
            {
                var financialStatementTransByYear = financialStatementTrans.Where(w => w.Year == year);
                QCreditAppReqLineFin_BuyerCreditApplication result = new QCreditAppReqLineFin_BuyerCreditApplication
                {
                    CreditAppRequestLineGUID = CAReqLineGUID,
                    Year = year,
                    RegisteredCapital = financialStatementTransByYear.Where(w => w.Ordering == 1).Select(s => s.Amount).FirstOrDefault(),
                    PaidCapital = financialStatementTransByYear.Where(w => w.Ordering == 2).Select(s => s.Amount).FirstOrDefault(),
                    TotalAsset = financialStatementTransByYear.Where(w => w.Ordering == 3).Select(s => s.Amount).FirstOrDefault(),
                    TotalLiability = financialStatementTransByYear.Where(w => w.Ordering == 4).Select(s => s.Amount).FirstOrDefault(),
                    TotalEquity = financialStatementTransByYear.Where(w => w.Ordering == 5).Select(s => s.Amount).FirstOrDefault(),
                    TotalRevenue = financialStatementTransByYear.Where(w => w.Ordering == 6).Select(s => s.Amount).FirstOrDefault(),
                    TotalCOGS = financialStatementTransByYear.Where(w => w.Ordering == 7).Select(s => s.Amount).FirstOrDefault(),
                    TotalGrossProfit = financialStatementTransByYear.Where(w => w.Ordering == 8).Select(s => s.Amount).FirstOrDefault(),
                    TotalOperExp = financialStatementTransByYear.Where(w => w.Ordering == 9).Select(s => s.Amount).FirstOrDefault(),
                    TotalNetProfit = financialStatementTransByYear.Where(w => w.Ordering == 10).Select(s => s.Amount).FirstOrDefault(),
                    NetProfitPercent = financialStatementTransByYear.Where(w => w.Ordering == 11).Select(s => s.Amount).FirstOrDefault(),
                    lDE = financialStatementTransByYear.Where(w => w.Ordering == 12).Select(s => s.Amount).FirstOrDefault(),
                    QuickRatio = financialStatementTransByYear.Where(w => w.Ordering == 13).Select(s => s.Amount).FirstOrDefault(),
                    IntCoverageRatio = financialStatementTransByYear.Where(w => w.Ordering == 14).Select(s => s.Amount).FirstOrDefault()
                };

                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private static bool CheckQCAReqLineElementIsNotNull(List<QCAReqLine> QCAReqLine, int index)
        {
            return QCAReqLine.ElementAtOrDefault(index) != null;
        }

        private static bool CheckQCreditAppReqLineFinElementIsNotNull(List<QCreditAppReqLineFin_BuyerCreditApplication> QCreditAppReqLineFin, int index)
        {
            return QCreditAppReqLineFin.ElementAtOrDefault(index) != null;
        }

        private static bool CheckQDocumentConditionElementIsNotNull(List<QDocumentCondition_BuyerCreditApplication> QDocumentCondition, int index)
        {
            return QDocumentCondition.ElementAtOrDefault(index) != null;
        }
        private static bool CheckQServiceFeeConditionElementIsNotNull(List<QServiceFeeConditionLine_BuyerCreditApplication> QServiceFeeConditionLine, int index)
        {
            return QServiceFeeConditionLine.ElementAtOrDefault(index) != null;
        }
        private static bool CheckQBuyerCreditAppReqAssignmentElementIsNotNull(List<QBuyerCreditAppReqAssignment> QBuyerCreditAppReqAssignment, int index)
        {
            return QBuyerCreditAppReqAssignment.ElementAtOrDefault(index) != null;
        }
        private static bool CheckQBuyerCreditAppReqBusinessCollateralElementIsNotNull(List<QBuyerCreditAppReqBusinessCollateral> QBuyerCreditAppReqBusinessCollateral, int index)
        {
            return QBuyerCreditAppReqBusinessCollateral.ElementAtOrDefault(index) != null;
        }
        private static bool CheckQBuyerActionHistoryElementIsNotNull(List<QBuyerActionHistory> QBuyerActionHistory, int index)
        {
            return QBuyerActionHistory.ElementAtOrDefault(index) != null;
        }
    }
}
