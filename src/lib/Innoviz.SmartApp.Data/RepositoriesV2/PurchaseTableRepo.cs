using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IPurchaseTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<PurchaseTableItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<PurchaseTableListView> GetListvw(SearchParameter search);
		PurchaseTableItemView GetByIdvw(Guid id);
		PurchaseTable Find(params object[] keyValues);
		PurchaseTable GetPurchaseTableByIdNoTracking(Guid guid);
		List<PurchaseTable> GetPurchaseTableByIdNoTracking(IEnumerable<Guid> purchaseTableGUIDs);
		PurchaseTable CreatePurchaseTable(PurchaseTable purchaseTable);
		void CreatePurchaseTableVoid(PurchaseTable purchaseTable);
		PurchaseTable UpdatePurchaseTable(PurchaseTable dbPurchaseTable, PurchaseTable inputPurchaseTable, List<string> skipUpdateFields = null);
		void UpdatePurchaseTableVoid(PurchaseTable dbPurchaseTable, PurchaseTable inputPurchaseTable, List<string> skipUpdateFields = null);
		void Remove(PurchaseTable item);
		PurchaseTable GetPurchaseTableByIdNoTrackingByAccessLevel(Guid guid);
		AccessModeView purchaseTableRepoGetAccessModeChequeTableByPurchase(Guid guid);
		IEnumerable<PurchaseTable> GetPurchaseTableByCompanyNoTracking(Guid guid);
		void ValidateAdd(IEnumerable<PurchaseTable> items);
	}
	public class PurchaseTableRepo : CompanyBaseRepository<PurchaseTable>, IPurchaseTableRepo
	{
		public PurchaseTableRepo(SmartAppDbContext context) : base(context) { }
		public PurchaseTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public PurchaseTable GetPurchaseTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.PurchaseTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<PurchaseTable> GetPurchaseTableByIdNoTracking(IEnumerable<Guid> purchaseTableGUIDs)
		{
			try
			{
				var result = Entity.Where(w => purchaseTableGUIDs.Contains(w.PurchaseTableGUID))
									.AsNoTracking()
									.ToList();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<PurchaseTableItemViewMap> GetDropDownQuery()
		{
			return (from purchaseTable in Entity
					select new PurchaseTableItemViewMap
					{
						CompanyGUID = purchaseTable.CompanyGUID,
						Owner = purchaseTable.Owner,
						OwnerBusinessUnitGUID = purchaseTable.OwnerBusinessUnitGUID,
						PurchaseTableGUID = purchaseTable.PurchaseTableGUID,
						PurchaseId = purchaseTable.PurchaseId,
						Description = purchaseTable.Description
					});
		}
		public IEnumerable<SelectItem<PurchaseTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<PurchaseTable>(search, SysParm.CompanyGUID);
				var purchaseTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<PurchaseTableItemViewMap, PurchaseTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return purchaseTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<PurchaseTableListViewMap> GetListQuery()
		{
			return (from purchaseTable in Entity
					join customerTable in db.Set<CustomerTable>()
					on purchaseTable.CustomerTableGUID equals customerTable.CustomerTableGUID
					join documentStatus in db.Set<DocumentStatus>()
					on purchaseTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
					select new PurchaseTableListViewMap
				{
						CompanyGUID = purchaseTable.CompanyGUID,
						Owner = purchaseTable.Owner,
						OwnerBusinessUnitGUID = purchaseTable.OwnerBusinessUnitGUID,
						PurchaseTableGUID = purchaseTable.PurchaseTableGUID,
						PurchaseId = purchaseTable.PurchaseId,
						Description = purchaseTable.Description,
						CustomerTableGUID = purchaseTable.CustomerTableGUID,
						PurchaseDate = purchaseTable.PurchaseDate,
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
						CustomerTable_CustomerId = customerTable.CustomerId,
						CreditAppTableGUID = purchaseTable.CreditAppTableGUID,
						RollBill = purchaseTable.Rollbill,
						DocumentStatusGUID = documentStatus.DocumentStatusGUID,
						DocumentStatus_StatusId = documentStatus.StatusId,
						DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
					});
		}
		public SearchResult<PurchaseTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<PurchaseTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<PurchaseTable>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<PurchaseTableListViewMap, PurchaseTableListView>();
				result = list.SetSearchResult<PurchaseTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<PurchaseTableItemViewMap> GetItemQuery()
		{
			var result = from purchaseTable in Entity
						 join company in db.Set<Company>()
						 on purchaseTable.CompanyGUID equals company.CompanyGUID

						 join documentStatus in db.Set<DocumentStatus>()
						 on purchaseTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID

						 join customerTable in db.Set<CustomerTable>()
						 on purchaseTable.CustomerTableGUID equals customerTable.CustomerTableGUID

						 join creditAppTable in db.Set<CreditAppTable>()
						 on purchaseTable.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID

						 join documentReason in db.Set<DocumentReason>()
						 on purchaseTable.DocumentReasonGUID equals documentReason.DocumentReasonGUID into ljDocumentReason
						 from documentReason in ljDocumentReason.DefaultIfEmpty()

						 join ownerBU in db.Set<BusinessUnit>()
						 on purchaseTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljPurchaseTableOwnerBU
						 from ownerBU in ljPurchaseTableOwnerBU.DefaultIfEmpty()
						 select new PurchaseTableItemViewMap
						 {
							 CompanyGUID = purchaseTable.CompanyGUID,
							 CompanyId = company.CompanyId,
							 Owner = purchaseTable.Owner,
							 OwnerBusinessUnitGUID = purchaseTable.OwnerBusinessUnitGUID,
							 OwnerBusinessUnitId = ownerBU.BusinessUnitId,
							 CreatedBy = purchaseTable.CreatedBy,
							 CreatedDateTime = purchaseTable.CreatedDateTime,
							 ModifiedBy = purchaseTable.ModifiedBy,
							 ModifiedDateTime = purchaseTable.ModifiedDateTime,
							 PurchaseTableGUID = purchaseTable.PurchaseTableGUID,
							 AdditionalPurchase = purchaseTable.AdditionalPurchase,
							 CreditAppTableGUID = purchaseTable.CreditAppTableGUID,
							 CustomerTableGUID = purchaseTable.CustomerTableGUID,
							 Description = purchaseTable.Description,
							 DiffChequeIssuedName = purchaseTable.DiffChequeIssuedName,
							 Dimension1GUID = purchaseTable.Dimension1GUID,
							 Dimension2GUID = purchaseTable.Dimension2GUID,
							 Dimension3GUID = purchaseTable.Dimension3GUID,
							 Dimension4GUID = purchaseTable.Dimension4GUID,
							 Dimension5GUID = purchaseTable.Dimension5GUID,
							 DocumentStatusGUID = purchaseTable.DocumentStatusGUID,
							 OverCreditBuyer = purchaseTable.OverCreditBuyer,
							 OverCreditCA = purchaseTable.OverCreditCA,
							 OverCreditCALine = purchaseTable.OverCreditCALine,
							 ProductType = purchaseTable.ProductType,
							 PurchaseDate = purchaseTable.PurchaseDate,
							 PurchaseId = purchaseTable.PurchaseId,
							 ReceiptTempTableGUID = purchaseTable.ReceiptTempTableGUID,
							 RetentionCalculateBase = purchaseTable.RetentionCalculateBase,
							 RetentionFixedAmount = purchaseTable.RetentionFixedAmount,
							 RetentionPct = purchaseTable.RetentionPct,
							 Rollbill = purchaseTable.Rollbill,
							 SumPurchaseAmount = purchaseTable.SumPurchaseAmount,
							 TotalInterestPct = purchaseTable.TotalInterestPct,
							 DocumentStatus_StatusId = documentStatus.StatusId,
							 CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
							 CreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
							 ProcessInstanceId = purchaseTable.ProcessInstanceId,
							 DocumentStatus_Description = documentStatus.Description,
							 Remark = purchaseTable.Remark,
							 OperReportSignatureGUID = purchaseTable.OperReportSignatureGUID,
							 DocumentReasonGUID = purchaseTable.DocumentReasonGUID,
							 DocumentReason_Values = SmartAppUtil.GetDropDownLabel(documentReason.ReasonId, documentReason.Description),
							 RowVersion = purchaseTable.RowVersion
						 };
			return result;
		}
		public PurchaseTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<PurchaseTableItemViewMap, PurchaseTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public PurchaseTable CreatePurchaseTable(PurchaseTable purchaseTable)
		{
			try
			{
				purchaseTable.PurchaseTableGUID = Guid.NewGuid();
				base.Add(purchaseTable);
				return purchaseTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreatePurchaseTableVoid(PurchaseTable purchaseTable)
		{
			try
			{
				CreatePurchaseTable(purchaseTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public PurchaseTable UpdatePurchaseTable(PurchaseTable dbPurchaseTable, PurchaseTable inputPurchaseTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbPurchaseTable = dbPurchaseTable.MapUpdateValues<PurchaseTable>(inputPurchaseTable);
				base.Update(dbPurchaseTable);
				return dbPurchaseTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdatePurchaseTableVoid(PurchaseTable dbPurchaseTable, PurchaseTable inputPurchaseTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbPurchaseTable = dbPurchaseTable.MapUpdateValues<PurchaseTable>(inputPurchaseTable, skipUpdateFields);
				base.Update(dbPurchaseTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public override void ValidateAdd(PurchaseTable item)
		{
			CheckDate(item);
			base.ValidateAdd(item);
		}
		public override void ValidateUpdate(PurchaseTable item)
		{
			CheckDate(item);
			base.ValidateUpdate(item);
		}
		public void CheckDate(PurchaseTable item)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				CreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
				CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTracking(item.CreditAppTableGUID);

				if(creditAppTable.InactiveDate != null)
                {
					if(item.PurchaseDate >= creditAppTable.InactiveDate )
                    {
						ex.AddData("ERROR.90108", new string[] { "LABEL.PURCHASE_DATE", "LABEL.INACTIVE_DATE", "LABEL.CREDIT_APPLICATION" });
					}
                }


				if (creditAppTable.InactiveDate == null)
				{
					if (item.PurchaseDate >= creditAppTable.ExpiryDate)
					{
						ex.AddData("ERROR.90108", new string[] { "LABEL.PURCHASE_DATE", "LABEL.EXPIRY_DATE", "LABEL.CREDIT_APPLICATION" });
					}
				}

				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public PurchaseTable GetPurchaseTableByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.PurchaseTableGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        public AccessModeView purchaseTableRepoGetAccessModeChequeTableByPurchase(Guid guid)
        {
			var result = (from purchaseTable in db.Set<PurchaseTable>()
						  join documentStatus in db.Set<DocumentStatus>()
						  on purchaseTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
						  into ljdocumentStatus
						  from documentStatus in ljdocumentStatus.DefaultIfEmpty()
						  where purchaseTable.PurchaseTableGUID == guid
						  select new AccessModeView
						  {
							  CanCreate = (documentStatus.StatusId == ((int)ChequeDocumentStatus.Created).ToString()),
							  CanDelete = true,
							  CanView = true
						  }
						  ).FirstOrDefault();
			return result;
        }
		public IEnumerable<PurchaseTable> GetPurchaseTableByCompanyNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CompanyGUID == guid)
					.AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #region Validate
        public override void ValidateAdd(IEnumerable<PurchaseTable> items)
        {
            base.ValidateAdd(items);
        }
        #endregion Validate
    }
}
