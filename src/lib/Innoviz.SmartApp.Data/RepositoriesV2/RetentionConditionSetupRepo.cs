using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IRetentionConditionSetupRepo
	{
		#region DropDown
		IEnumerable<SelectItem<RetentionConditionSetupItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<RetentionConditionSetupListView> GetListvw(SearchParameter search);
		RetentionConditionSetupItemView GetByIdvw(Guid id);
		RetentionConditionSetup Find(params object[] keyValues);
		RetentionConditionSetup GetRetentionConditionSetupByIdNoTracking(Guid guid);
		RetentionConditionSetup CreateRetentionConditionSetup(RetentionConditionSetup retentionConditionSetup);
		void CreateRetentionConditionSetupVoid(RetentionConditionSetup retentionConditionSetup);
		RetentionConditionSetup UpdateRetentionConditionSetup(RetentionConditionSetup dbRetentionConditionSetup, RetentionConditionSetup inputRetentionConditionSetup, List<string> skipUpdateFields = null);
		void UpdateRetentionConditionSetupVoid(RetentionConditionSetup dbRetentionConditionSetup, RetentionConditionSetup inputRetentionConditionSetup, List<string> skipUpdateFields = null);
		void Remove(RetentionConditionSetup item);
		IEnumerable<RetentionConditionSetup> GetRetentionConditionSetupByCompanyNoTracking(Guid guid);
	}
	public class RetentionConditionSetupRepo : CompanyBaseRepository<RetentionConditionSetup>, IRetentionConditionSetupRepo
	{
		public RetentionConditionSetupRepo(SmartAppDbContext context) : base(context) { }
		public RetentionConditionSetupRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public RetentionConditionSetup GetRetentionConditionSetupByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.RetentionConditionSetupGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<RetentionConditionSetupItemViewMap> GetDropDownQuery()
		{
			return (from retentionConditionSetup in Entity
					select new RetentionConditionSetupItemViewMap
					{
						CompanyGUID = retentionConditionSetup.CompanyGUID,
						Owner = retentionConditionSetup.Owner,
						OwnerBusinessUnitGUID = retentionConditionSetup.OwnerBusinessUnitGUID,
						RetentionConditionSetupGUID = retentionConditionSetup.RetentionConditionSetupGUID
					});
		}
		public IEnumerable<SelectItem<RetentionConditionSetupItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<RetentionConditionSetup>(search, SysParm.CompanyGUID);
				var retentionConditionSetup = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<RetentionConditionSetupItemViewMap, RetentionConditionSetupItemView>().ToDropDownItem(search.ExcludeRowData);


				return retentionConditionSetup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<RetentionConditionSetupListViewMap> GetListQuery()
		{
			return (from retentionConditionSetup in Entity
				select new RetentionConditionSetupListViewMap
				{
						CompanyGUID = retentionConditionSetup.CompanyGUID,
						Owner = retentionConditionSetup.Owner,
						OwnerBusinessUnitGUID = retentionConditionSetup.OwnerBusinessUnitGUID,
						RetentionConditionSetupGUID = retentionConditionSetup.RetentionConditionSetupGUID,
						ProductType = retentionConditionSetup.ProductType,
						RetentionDeductionMethod = retentionConditionSetup.RetentionDeductionMethod,
						RetentionCalculateBase = retentionConditionSetup.RetentionCalculateBase,
				});
		}
		public SearchResult<RetentionConditionSetupListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<RetentionConditionSetupListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<RetentionConditionSetup>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<RetentionConditionSetupListViewMap, RetentionConditionSetupListView>();
				result = list.SetSearchResult<RetentionConditionSetupListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<RetentionConditionSetupItemViewMap> GetItemQuery()
		{
			return (from retentionConditionSetup in Entity
					join company in db.Set<Company>()
					on retentionConditionSetup.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on retentionConditionSetup.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljRetentionConditionSetupOwnerBU
					from ownerBU in ljRetentionConditionSetupOwnerBU.DefaultIfEmpty()
					select new RetentionConditionSetupItemViewMap
					{
						CompanyGUID = retentionConditionSetup.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = retentionConditionSetup.Owner,
						OwnerBusinessUnitGUID = retentionConditionSetup.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = retentionConditionSetup.CreatedBy,
						CreatedDateTime = retentionConditionSetup.CreatedDateTime,
						ModifiedBy = retentionConditionSetup.ModifiedBy,
						ModifiedDateTime = retentionConditionSetup.ModifiedDateTime,
						RetentionConditionSetupGUID = retentionConditionSetup.RetentionConditionSetupGUID,
						ProductType = retentionConditionSetup.ProductType,
						RetentionCalculateBase = retentionConditionSetup.RetentionCalculateBase,
						RetentionDeductionMethod = retentionConditionSetup.RetentionDeductionMethod,
					
						RowVersion = retentionConditionSetup.RowVersion,
					});
		}
		public RetentionConditionSetupItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<RetentionConditionSetupItemViewMap, RetentionConditionSetupItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public RetentionConditionSetup CreateRetentionConditionSetup(RetentionConditionSetup retentionConditionSetup)
		{
			try
			{
				retentionConditionSetup.RetentionConditionSetupGUID = Guid.NewGuid();
				base.Add(retentionConditionSetup);
				return retentionConditionSetup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateRetentionConditionSetupVoid(RetentionConditionSetup retentionConditionSetup)
		{
			try
			{
				CreateRetentionConditionSetup(retentionConditionSetup);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public RetentionConditionSetup UpdateRetentionConditionSetup(RetentionConditionSetup dbRetentionConditionSetup, RetentionConditionSetup inputRetentionConditionSetup, List<string> skipUpdateFields = null)
		{
			try
			{
				dbRetentionConditionSetup = dbRetentionConditionSetup.MapUpdateValues<RetentionConditionSetup>(inputRetentionConditionSetup);
				base.Update(dbRetentionConditionSetup);
				return dbRetentionConditionSetup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateRetentionConditionSetupVoid(RetentionConditionSetup dbRetentionConditionSetup, RetentionConditionSetup inputRetentionConditionSetup, List<string> skipUpdateFields = null)
		{
			try
			{
				dbRetentionConditionSetup = dbRetentionConditionSetup.MapUpdateValues<RetentionConditionSetup>(inputRetentionConditionSetup, skipUpdateFields);
				base.Update(dbRetentionConditionSetup);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public IEnumerable<RetentionConditionSetup> GetRetentionConditionSetupByCompanyNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CompanyGUID == guid).AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

