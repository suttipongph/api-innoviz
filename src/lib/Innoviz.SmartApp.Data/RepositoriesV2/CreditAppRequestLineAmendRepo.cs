using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICreditAppRequestLineAmendRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CreditAppRequestLineAmendItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<CreditAppRequestLineAmendListView> GetListvw(SearchParameter search);
		CreditAppRequestLineAmendItemView GetByIdvw(Guid id);
		CreditAppRequestLineAmend Find(params object[] keyValues);
		CreditAppRequestLineAmend GetCreditAppRequestLineAmendByIdNoTracking(Guid guid);
		CreditAppRequestLineAmend CreateCreditAppRequestLineAmend(CreditAppRequestLineAmend creditAppRequestLineAmend);
		void CreateCreditAppRequestLineAmendVoid(CreditAppRequestLineAmend creditAppRequestLineAmend);
		CreditAppRequestLineAmend UpdateCreditAppRequestLineAmend(CreditAppRequestLineAmend dbCreditAppRequestLineAmend, CreditAppRequestLineAmend inputCreditAppRequestLineAmend, List<string> skipUpdateFields = null);
		void UpdateCreditAppRequestLineAmendVoid(CreditAppRequestLineAmend dbCreditAppRequestLineAmend, CreditAppRequestLineAmend inputCreditAppRequestLineAmend, List<string> skipUpdateFields = null);
		void Remove(CreditAppRequestLineAmend item);
		CreditAppRequestLineAmend GetCreditAppRequestLineAmendByIdNoTrackingByAccessLevel(Guid guid);
		IEnumerable<CreditAppRequestLineAmend> GetCreditAppRequestLineAmendByCreditAppRequestTable(Guid guid);
		#region InitialData
		public CreditAppRequestLineAmendItemView GetCreditAppRequestLineAmendInitialData(Guid creditAppLineGUID);
        #endregion
    }
    public class CreditAppRequestLineAmendRepo : CompanyBaseRepository<CreditAppRequestLineAmend>, ICreditAppRequestLineAmendRepo
	{
		public CreditAppRequestLineAmendRepo(SmartAppDbContext context) : base(context) { }
		public CreditAppRequestLineAmendRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CreditAppRequestLineAmend GetCreditAppRequestLineAmendByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CreditAppRequestLineAmendGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CreditAppRequestLineAmendItemViewMap> GetDropDownQuery()
		{
			return (from creditAppRequestLineAmend in Entity
					select new CreditAppRequestLineAmendItemViewMap
					{
						CompanyGUID = creditAppRequestLineAmend.CompanyGUID,
						Owner = creditAppRequestLineAmend.Owner,
						OwnerBusinessUnitGUID = creditAppRequestLineAmend.OwnerBusinessUnitGUID,
						CreditAppRequestLineAmendGUID = creditAppRequestLineAmend.CreditAppRequestLineAmendGUID,
						//CreditAppRequestId = creditAppRequestLineAmend.CreditAppRequestId,
						//Description = creditAppRequestLineAmend.Description
					});
		}
		public IEnumerable<SelectItem<CreditAppRequestLineAmendItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditAppRequestLineAmend>(search, SysParm.CompanyGUID);
				var creditAppRequestLineAmend = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CreditAppRequestLineAmendItemViewMap, CreditAppRequestLineAmendItemView>().ToDropDownItem(search.ExcludeRowData);


				return creditAppRequestLineAmend;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CreditAppRequestLineAmendListViewMap> GetListQuery()
		{
			return (from creditAppRequestLineAmend in Entity
					join creditAppRequestLine in db.Set<CreditAppRequestLine>() on creditAppRequestLineAmend.CreditAppRequestLineGUID equals creditAppRequestLine.CreditAppRequestLineGUID
					join creditAppRequestTable in db.Set<CreditAppRequestTable>() on creditAppRequestLine.CreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID
					join buyerTable in db.Set<BuyerTable>() on creditAppRequestLine.BuyerTableGUID equals buyerTable.BuyerTableGUID
					join customerTable in db.Set<CustomerTable>() on creditAppRequestTable.CustomerTableGUID equals customerTable.CustomerTableGUID
					join documentStatus in db.Set<DocumentStatus>() on creditAppRequestTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
					where (creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.AmendBuyerInfo 
					    || creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.AmendCustomerBuyerCreditLimit)
					select new CreditAppRequestLineAmendListViewMap
				{
						CompanyGUID = creditAppRequestLineAmend.CompanyGUID,
						Owner = creditAppRequestLineAmend.Owner,
						OwnerBusinessUnitGUID = creditAppRequestLineAmend.OwnerBusinessUnitGUID,
						CreditAppRequestLineAmendGUID = creditAppRequestLineAmend.CreditAppRequestLineAmendGUID,
						CreditAppRequestTable_CreditAppRequestId = creditAppRequestTable.CreditAppRequestId,
						CreditAppRequestTable_Description = creditAppRequestTable.Description,
						CreditAppRequestTable_CreditAppRequestType = creditAppRequestTable.CreditAppRequestType,
						CreditAppRequestTable_RequestDate = creditAppRequestTable.RequestDate,
						DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
						DocumentStatus_StatusId = documentStatus.StatusId,
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
						CustomerTable_CustomerId = customerTable.CustomerId,
						BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
						BuyerTable_BuyerId = buyerTable.BuyerId,
						CreditAppRequestTable_DocumentStatusGUID = documentStatus.DocumentStatusGUID,
						CreditAppRequestTable_CustomerTableGUID = customerTable.CustomerTableGUID,
						CreditAppRequestTable_BuyerTableGUID = buyerTable.BuyerTableGUID,
						CreditAppRequestLine_RefCreditAppLineGUID = creditAppRequestLine.RefCreditAppLineGUID,
					});
		}
		public SearchResult<CreditAppRequestLineAmendListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CreditAppRequestLineAmendListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditAppRequestLineAmend>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CreditAppRequestLineAmendListViewMap, CreditAppRequestLineAmendListView>();
				result = list.SetSearchResult<CreditAppRequestLineAmendListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CreditAppRequestLineAmendItemViewMap> GetItemQuery()
		{
            var address = db.Set<AddressTrans>().Where(w => w.RefType == (int)RefType.Buyer);
            var addressLabel = (from creditAppRequestLineAmend in Entity
                                join billingAddress in address on creditAppRequestLineAmend.OriginalBillingAddressGUID equals billingAddress.AddressTransGUID into ljCreditAppRequestLineBillingAddress
                                from billingAddress in ljCreditAppRequestLineBillingAddress.DefaultIfEmpty()
								join receiptAddress in address on creditAppRequestLineAmend.OriginalReceiptAddressGUID equals receiptAddress.AddressTransGUID into ljCreditAppRequestLineReceiptAddress
								from receiptAddress in ljCreditAppRequestLineReceiptAddress.DefaultIfEmpty()
								join creditAppRequestLine in db.Set<CreditAppRequestLine>() on creditAppRequestLineAmend.CreditAppRequestLineGUID equals creditAppRequestLine.CreditAppRequestLineGUID
								join newBillingAddress in address on creditAppRequestLine.BillingAddressGUID equals newBillingAddress.AddressTransGUID into ljCreditAppRequestLineNewBillingAddress
								from newBillingAddress in ljCreditAppRequestLineNewBillingAddress.DefaultIfEmpty()
								join newReceiptAddress in address on creditAppRequestLine.ReceiptAddressGUID equals newReceiptAddress.AddressTransGUID into ljCreditAppRequestLineNewReceiptAddress
								from newReceiptAddress in ljCreditAppRequestLineNewReceiptAddress.DefaultIfEmpty()
								select new CreditAppRequestLineAmendItemViewMap
								{
									CreditAppRequestLineAmendGUID = creditAppRequestLineAmend.CreditAppRequestLineAmendGUID,
									OriginalBillingAddress_Values = (billingAddress != null) ? SmartAppUtil.GetDropDownLabel(billingAddress.Name, billingAddress.Address1) : null,
                                    UnboundOriginalBillingAddress = (billingAddress != null) ? SmartAppUtil.GetAddressLabel(billingAddress.Address1, billingAddress.Address2) : null,
									UnboundNewBillingAddress = (newBillingAddress != null) ? SmartAppUtil.GetAddressLabel(newBillingAddress.Address1, newBillingAddress.Address2) : null,
									
									OriginalReceiptAddress_Values = (receiptAddress != null) ? SmartAppUtil.GetDropDownLabel(receiptAddress.Name, receiptAddress.Address1) : null,
									UnboundOriginalReceiptAddress = (receiptAddress != null) ? SmartAppUtil.GetAddressLabel(receiptAddress.Address1, receiptAddress.Address2) : null,
									unboundNewReceiptAddress = (newReceiptAddress != null) ? SmartAppUtil.GetAddressLabel(newReceiptAddress.Address1, newBillingAddress.Address2) : null,

								});
            var otherLabel = (from creditAppRequestLineAmend in Entity
									  join assignmentMethod in db.Set<AssignmentMethod>() on creditAppRequestLineAmend.OriginalAssignmentMethodGUID equals assignmentMethod.AssignmentMethodGUID into ljAssignmentMethod
									  from assignmentMethod in ljAssignmentMethod.DefaultIfEmpty()
									  join methodOfPayment in db.Set<MethodOfPayment>() on creditAppRequestLineAmend.OriginalMethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID into ljMethodOfPayment
									  from methodOfPayment in ljMethodOfPayment.DefaultIfEmpty()
							  select new CreditAppRequestLineAmendItemViewMap
									  {
										  CreditAppRequestLineAmendGUID = creditAppRequestLineAmend.CreditAppRequestLineAmendGUID,
										  OriginalAssignmentMethod_Values = (assignmentMethod != null) ? SmartAppUtil.GetDropDownLabel(assignmentMethod.AssignmentMethodId, assignmentMethod.Description) : string.Empty,
										  OriginalMethodOfPayment_Values = (methodOfPayment != null) ? SmartAppUtil.GetDropDownLabel(methodOfPayment.MethodOfPaymentId, methodOfPayment.Description) : string.Empty,
									  });
			return (from creditAppRequestLineAmend in Entity
					join company in db.Set<Company>()
					on creditAppRequestLineAmend.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on creditAppRequestLineAmend.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCreditAppRequestLineAmendOwnerBU
					from ownerBU in ljCreditAppRequestLineAmendOwnerBU.DefaultIfEmpty()
                    join creditAppRequestLine in db.Set<CreditAppRequestLine>() on creditAppRequestLineAmend.CreditAppRequestLineGUID equals creditAppRequestLine.CreditAppRequestLineGUID
                    join creditAppRequestTable in db.Set<CreditAppRequestTable>() on creditAppRequestLine.CreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID
                    join buyerTable in db.Set<BuyerTable>() on creditAppRequestLine.BuyerTableGUID equals buyerTable.BuyerTableGUID
                    join customerTable in db.Set<CustomerTable>() on creditAppRequestTable.CustomerTableGUID equals customerTable.CustomerTableGUID
                    join creditAppTable in db.Set<CreditAppTable>() on creditAppRequestTable.RefCreditAppTableGUID equals creditAppTable.CreditAppTableGUID
                    join documentStatus in db.Set<DocumentStatus>() on creditAppRequestTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
                    join other in otherLabel on creditAppRequestLineAmend.CreditAppRequestLineAmendGUID equals other.CreditAppRequestLineAmendGUID into ljOher
                    from other in ljOher.DefaultIfEmpty()
                    join addsLabel in addressLabel on creditAppRequestLineAmend.CreditAppRequestLineAmendGUID equals addsLabel.CreditAppRequestLineAmendGUID into ljAddsLabel
                    from addsLabel in ljAddsLabel.DefaultIfEmpty()
                    select new CreditAppRequestLineAmendItemViewMap
					{
						CompanyGUID = creditAppRequestLineAmend.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = creditAppRequestLineAmend.Owner,
						OwnerBusinessUnitGUID = creditAppRequestLineAmend.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = creditAppRequestLineAmend.CreatedBy,
						CreatedDateTime = creditAppRequestLineAmend.CreatedDateTime,
						ModifiedBy = creditAppRequestLineAmend.ModifiedBy,
						ModifiedDateTime = creditAppRequestLineAmend.ModifiedDateTime,
						CreditAppRequestLineAmendGUID = creditAppRequestLineAmend.CreditAppRequestLineAmendGUID,
						AmendAssignmentMethod = creditAppRequestLineAmend.AmendAssignmentMethod,
						AmendBillingDocumentCondition = creditAppRequestLineAmend.AmendBillingDocumentCondition,
						AmendBillingInformation = creditAppRequestLineAmend.AmendBillingInformation,
						AmendRate = creditAppRequestLineAmend.AmendRate,
						AmendReceiptDocumentCondition = creditAppRequestLineAmend.AmendReceiptDocumentCondition,
						AmendReceiptInformation = creditAppRequestLineAmend.AmendReceiptInformation,
						CreditAppRequestLineGUID = creditAppRequestLineAmend.CreditAppRequestLineGUID,
						OriginalAssignmentMethodGUID = creditAppRequestLineAmend.OriginalAssignmentMethodGUID,
						OriginalAssignmentMethodRemark = creditAppRequestLineAmend.OriginalAssignmentMethodRemark,
						OriginalBillingAddressGUID = creditAppRequestLineAmend.OriginalBillingAddressGUID,
						OriginalCreditLimitLineRequest = creditAppRequestLineAmend.OriginalCreditLimitLineRequest,
						OriginalMaxPurchasePct = creditAppRequestLineAmend.OriginalMaxPurchasePct,
						OriginalMethodOfPaymentGUID = creditAppRequestLineAmend.OriginalMethodOfPaymentGUID,
						OriginalPurchaseFeeCalculateBase = creditAppRequestLineAmend.OriginalPurchaseFeeCalculateBase,
						OriginalPurchaseFeePct = creditAppRequestLineAmend.OriginalPurchaseFeePct,
						OriginalReceiptAddressGUID = creditAppRequestLineAmend.OriginalReceiptAddressGUID,
						OriginalReceiptRemark = creditAppRequestLineAmend.OriginalReceiptRemark,

                        OriginalBillingAddress_Values = (addsLabel != null) ? addsLabel.OriginalBillingAddress_Values : string.Empty,
                        UnboundOriginalBillingAddress = (addsLabel != null) ? addsLabel.UnboundOriginalBillingAddress : string.Empty,
                        OriginalReceiptAddress_Values = (addsLabel != null) ? addsLabel.OriginalReceiptAddress_Values : string.Empty,
                        UnboundOriginalReceiptAddress = (addsLabel != null) ? addsLabel.UnboundOriginalReceiptAddress : string.Empty,

                        OriginalMethodOfPayment_Values = (other != null) ? other.OriginalMethodOfPayment_Values : string.Empty,
                        OriginalAssignmentMethod_Values = (other != null) ? other.OriginalAssignmentMethod_Values : string.Empty,

                        #region CreditAppRequest
                        CreditAppRequestTable_CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
                        CreditAppRequestTable_CustomerTableGUID = creditAppRequestTable.CustomerTableGUID,
                        CreditAppRequestTable_CreditAppRequestId = creditAppRequestTable.CreditAppRequestId,
                        CreditAppRequestTable_Description = creditAppRequestTable.Description,
                        CreditAppRequestTable_CreditAppRequestType = creditAppRequestTable.CreditAppRequestType,
                        CreditAppRequestTable_RequestDate = creditAppRequestTable.RequestDate,
                        CreditAppRequestTable_CustomerCreditLimit = creditAppRequestTable.CustomerCreditLimit,
                        CreditAppRequestTable_Remark = creditAppRequestTable.Remark,
                        CreditAppRequestTable_ApprovedDate = creditAppRequestTable.ApprovedDate,
                        DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
                        DocumentStatus_StatusId = documentStatus.StatusId,
                        CreditAppRequestTable_DocumentStatusGUID = documentStatus.DocumentStatusGUID,
                        CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                        RefCreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
                        RefCreditAppTableGUID = creditAppTable.CreditAppTableGUID,
						CreditAppRequestTable_CACondition = creditAppRequestTable.CACondition,
						#endregion CreditAppRequest
						#region CreditAppRequestLine
						CreditAppRequestLine_NewCreditLimitLineRequest = creditAppRequestLine.CreditLimitLineRequest,
                        CreditAppRequestLine_BuyerCreditLimit = creditAppRequestLine.BuyerCreditLimit,
                        CreditAppRequestLine_ApprovedCreditLimitLineRequest = creditAppRequestLine.ApprovedCreditLimitLineRequest,
                        CreditAppRequestLine_NewMaxPurchasePct = creditAppRequestLine.MaxPurchasePct,
                        CreditAppRequestLine_NewPurchaseFeePct = creditAppRequestLine.PurchaseFeePct,
                        CreditAppRequestLine_NewPurchaseFeeCalculateBase = creditAppRequestLine.PurchaseFeeCalculateBase,
                        CreditAppRequestLine_AssignmentMethodGUID = creditAppRequestLine.AssignmentMethodGUID,
                        CreditAppRequestLine_NewAssignmentMethodRemark = creditAppRequestLine.AssignmentMethodRemark,
                        CreditAppRequestLine_MarketingComment = creditAppRequestLine.MarketingComment,
                        CreditAppRequestLine_CreditComment = creditAppRequestLine.CreditComment,
                        CreditAppRequestLine_ApproverComment = creditAppRequestLine.ApproverComment,
                        CreditAppRequestLine_NewMethodOfPaymentGUID = creditAppRequestLine.MethodOfPaymentGUID,
                        CreditAppRequestLine_NewReceiptRemark = creditAppRequestLine.ReceiptRemark,
                        CreditAppRequestLine_NewReceiptAddressGUID = creditAppRequestLine.ReceiptAddressGUID,
                        CreditAppRequestLine_NewBillingAddressGUID = creditAppRequestLine.BillingAddressGUID,
                        CreditAppRequestLine_RefCreditAppLineGUID = creditAppRequestLine.RefCreditAppLineGUID,
                        BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
                        CreditAppRequestLine_BuyerTableGUID = buyerTable.BuyerTableGUID,
                        UnboundNewBillingAddress = (addsLabel != null) ? addsLabel.UnboundNewBillingAddress : string.Empty,
                        unboundNewReceiptAddress = (addsLabel != null) ? addsLabel.unboundNewReceiptAddress : string.Empty,
						CreditAppRequestLine_BlacklistStatusGUID = creditAppRequestTable.BlacklistStatusGUID,
						CreditAppRequestLine_CreditScoringGUID = creditAppRequestTable.CreditScoringGUID,
						CreditAppRequestLine_KYCSetupGUID = creditAppRequestTable.KYCGUID,
						CreditAppRequestLine_CustomerBuyerOutstanding = creditAppRequestLine.CustomerBuyerOutstanding,
						CreditAppRequestLine_AllCustomerBuyerOutstanding = creditAppRequestLine.AllCustomerBuyerOutstanding,
						#endregion CreditAppRequestLine
						ProcessInstanceId = creditAppRequestTable.ProcessInstanceId,
                    
						RowVersion = creditAppRequestLineAmend.RowVersion,
					});
		}
		public CreditAppRequestLineAmendItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<CreditAppRequestLineAmendItemViewMap, CreditAppRequestLineAmendItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public CreditAppRequestLineAmend CreateCreditAppRequestLineAmend(CreditAppRequestLineAmend creditAppRequestLineAmend)
		{
			try
			{
				creditAppRequestLineAmend.CreditAppRequestLineAmendGUID = Guid.NewGuid();
				base.Add(creditAppRequestLineAmend);
				return creditAppRequestLineAmend;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCreditAppRequestLineAmendVoid(CreditAppRequestLineAmend creditAppRequestLineAmend)
		{
			try
			{
				CreateCreditAppRequestLineAmend(creditAppRequestLineAmend);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditAppRequestLineAmend UpdateCreditAppRequestLineAmend(CreditAppRequestLineAmend dbCreditAppRequestLineAmend, CreditAppRequestLineAmend inputCreditAppRequestLineAmend, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCreditAppRequestLineAmend = dbCreditAppRequestLineAmend.MapUpdateValues<CreditAppRequestLineAmend>(inputCreditAppRequestLineAmend);
				base.Update(dbCreditAppRequestLineAmend);
				return dbCreditAppRequestLineAmend;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCreditAppRequestLineAmendVoid(CreditAppRequestLineAmend dbCreditAppRequestLineAmend, CreditAppRequestLineAmend inputCreditAppRequestLineAmend, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCreditAppRequestLineAmend = dbCreditAppRequestLineAmend.MapUpdateValues<CreditAppRequestLineAmend>(inputCreditAppRequestLineAmend, skipUpdateFields);
				base.Update(dbCreditAppRequestLineAmend);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public CreditAppRequestLineAmend GetCreditAppRequestLineAmendByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CreditAppRequestLineAmendGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<CreditAppRequestLineAmend> GetCreditAppRequestLineAmendByCreditAppRequestTable(Guid guid)
		{
			try
			{
				var result = (from creditAppRequestLineAmend in Entity
							  join creditAppRequestLine in db.Set<CreditAppRequestLine>()
							  on creditAppRequestLineAmend.CreditAppRequestLineGUID equals creditAppRequestLine.CreditAppRequestLineGUID
							  where creditAppRequestLine.CreditAppRequestTableGUID == guid
							  select creditAppRequestLineAmend);
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region InitialData
		public CreditAppRequestLineAmendItemView GetCreditAppRequestLineAmendInitialData(Guid creditAppLineGUID)
		{
			try
			{
				string draft = ((int)CreditAppRequestStatus.Draft).ToString();
				var result =  
						(from creditAppLine in db.Set<CreditAppLine>()
						 join creditAppTable in db.Set<CreditAppTable>() on creditAppLine.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID
						 join buyerTable in db.Set<BuyerTable>() on creditAppLine.BuyerTableGUID equals buyerTable.BuyerTableGUID
						 join customerTable in db.Set<CustomerTable>() on creditAppTable.CustomerTableGUID equals customerTable.CustomerTableGUID
						 join documentStatus in db.Set<DocumentStatus>() on draft equals documentStatus.StatusId
						 join creditAppRequestTable in db.Set<CreditAppRequestTable>() on creditAppTable.RefCreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID
						 join creditAppRequestLine in db.Set<CreditAppRequestLine>() on creditAppLine.RefCreditAppRequestLineGUID equals creditAppRequestLine.CreditAppRequestLineGUID into ljCreditAppLineCreditAppRequestLine
						 from creditAppRequestLine in ljCreditAppLineCreditAppRequestLine.DefaultIfEmpty()
						 where creditAppLine.CreditAppLineGUID == creditAppLineGUID
						 select new CreditAppRequestLineAmendItemView
						 {
							 CreditAppRequestLineAmendGUID = new Guid().GuidNullToString(),
							 #region CreditAppRequest
							 //CreditAppRequestTable_Values = creditAppRequestTable.CreditAppRequestId,
                             //CreditAppRequestTable_Description = creditAppRequestTable.Description,
                             CreditAppRequestTable_CreditAppRequestType = creditAppRequestTable.CreditAppRequestType,
							 CreditAppRequestTable_RequestDate = DateTime.Now.DateToString(),
							 CreditAppRequestTable_CustomerCreditLimit = creditAppRequestTable.CustomerCreditLimit,
							 //CreditAppRequestTable_Remark = creditAppRequestTable.Remark,
							 //CreditAppRequestTable_ApprovedDate = creditAppRequestTable.ApprovedDate.DateNullToString(),
							 DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
							 DocumentStatus_StatusId = documentStatus.StatusId,
							 CreditAppRequestTable_DocumentStatusGUID = documentStatus.DocumentStatusGUID.GuidNullToString(),
							 CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
							 BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
							 RefCreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
							 RefCreditAppTableGUID = creditAppTable.CreditAppTableGUID.GuidNullToString(),
							 CreditAppRequestLine_RefCreditAppLineGUID = creditAppLineGUID.GuidNullToString(),
							 #endregion CreditAppRequest
							 #region CreditAppRequestLine

							 //CreditAppRequestLine_RefCreditAppLineGUID = creditAppLineGUID.GuidNullToString(),
							 //                     CreditAppRequestLine_NewCreditLimitLineRequest = (creditAppRequestLine != null) ? creditAppRequestLine.CreditLimitLineRequest: 0,
							 //CreditAppRequestLine_BuyerCreditLimit = (creditAppRequestLine != null) ? creditAppRequestLine.BuyerCreditLimit : 0,
							 //CreditAppRequestLine_ApprovedCreditLimitLineRequest = (creditAppRequestLine != null) ? creditAppRequestLine.ApprovedCreditLimitLineRequest : 0,
							 //CreditAppRequestLine_NewMaxPurchasePct = (creditAppRequestLine != null) ? creditAppRequestLine.MaxPurchasePct : 0,
							 //CreditAppRequestLine_NewPurchaseFeePct = (creditAppRequestLine != null) ? creditAppRequestLine.PurchaseFeePct : 0,
							 //CreditAppRequestLine_NewPurchaseFeeCalculateBase = (creditAppRequestLine != null) ? creditAppRequestLine.PurchaseFeeCalculateBase : 0,
							 //CreditAppRequestLine_AssignmentMethodGUID = (creditAppRequestLine != null) ? creditAppRequestLine.AssignmentMethodGUID.GuidNullToString() : null,
							 //CreditAppRequestLine_NewAssignmentMethodRemark = (creditAppRequestLine != null) ? creditAppRequestLine.AssignmentMethodRemark : string.Empty,
							 //CreditAppRequestLine_MarketingComment = (creditAppRequestLine != null) ? creditAppRequestLine.MarketingComment : string.Empty,
							 //CreditAppRequestLine_CreditComment = (creditAppRequestLine != null) ? creditAppRequestLine.CreditComment : string.Empty,
							 //CreditAppRequestLine_ApproverComment = (creditAppRequestLine != null) ? creditAppRequestLine.ApproverComment : null,
							 //CreditAppRequestLine_NewMethodOfPaymentGUID = (creditAppRequestLine != null) ? creditAppRequestLine.MethodOfPaymentGUID.GuidNullToString() : null,
							 //CreditAppRequestLine_NewReceiptRemark = (creditAppRequestLine != null) ? creditAppRequestLine.ReceiptRemark : string.Empty,
							 //CreditAppRequestLine_NewReceiptAddressGUID = (creditAppRequestLine != null) ? creditAppRequestLine.ReceiptAddressGUID.GuidNullToString() : null,
							 #endregion CreditAppRequestLine
						 }).FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion InitialData
	}
}

