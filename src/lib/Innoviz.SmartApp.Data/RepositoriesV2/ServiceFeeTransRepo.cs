using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface IServiceFeeTransRepo
    {
        #region DropDown
        IEnumerable<SelectItem<ServiceFeeTransItemView>> GetDropDownItem(SearchParameter search);
        #endregion DropDown
        SearchResult<ServiceFeeTransListView> GetListvw(SearchParameter search);
        ServiceFeeTransItemView GetByIdvw(Guid id);
        ServiceFeeTrans Find(params object[] keyValues);
        ServiceFeeTrans GetServiceFeeTransByIdNoTracking(Guid guid);
        ServiceFeeTrans CreateServiceFeeTrans(ServiceFeeTrans serviceFeeTrans);
        void CreateServiceFeeTransVoid(ServiceFeeTrans serviceFeeTrans);
        ServiceFeeTrans UpdateServiceFeeTrans(ServiceFeeTrans dbServiceFeeTrans, ServiceFeeTrans inputServiceFeeTrans, List<string> skipUpdateFields = null);
        void UpdateServiceFeeTransVoid(ServiceFeeTrans dbServiceFeeTrans, ServiceFeeTrans inputServiceFeeTrans, List<string> skipUpdateFields = null);
        void Remove(ServiceFeeTrans item);
        ServiceFeeTrans GetServiceFeeTransByIdNoTrackingByAccessLevel(Guid guid);
        IEnumerable<ServiceFeeTrans> GetServiceFeeTransByReferenceNoTracking(RefType refType, Guid refGUID);
        ServiceFeeTrans GetServiceFeeTransByMessengerJob(Guid refGUID, int productType);
        void ValidateAdd(ServiceFeeTrans item);
        void ValidateAdd(IEnumerable<ServiceFeeTrans> items);


        #region shared GenInvoiceFromServiceFeeTrans
        public IEnumerable<ServiceFeeTrans> GetServiceFeeTransInvoiceRevenueType();
        int GetMaxOrdering(Guid refGUID, int productType);
        #endregion
    }
    public class ServiceFeeTransRepo : CompanyBaseRepository<ServiceFeeTrans>, IServiceFeeTransRepo
    {
        public ServiceFeeTransRepo(SmartAppDbContext context) : base(context) { }
        public ServiceFeeTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public ServiceFeeTrans GetServiceFeeTransByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.ServiceFeeTransGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public int GetMaxOrdering(Guid refGUID, int refType)
        {
            try
            {
                var result = Entity.Where(item => item.RefGUID == refGUID && item.RefType == refType).AsNoTracking().OrderByDescending(or => or.Ordering).Select(sl => sl.Ordering).FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        private IQueryable<ServiceFeeTransItemViewMap> GetDropDownQuery()
        {
            return (from serviceFeeTrans in Entity
                    select new ServiceFeeTransItemViewMap
                    {
                        CompanyGUID = serviceFeeTrans.CompanyGUID,
                        Owner = serviceFeeTrans.Owner,
                        OwnerBusinessUnitGUID = serviceFeeTrans.OwnerBusinessUnitGUID,
                        ServiceFeeTransGUID = serviceFeeTrans.ServiceFeeTransGUID,
                        InvoiceRevenueTypeGUID = serviceFeeTrans.InvoiceRevenueTypeGUID,
                        Description = serviceFeeTrans.Description
                    });
        }
        public IEnumerable<SelectItem<ServiceFeeTransItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<ServiceFeeTrans>(search, SysParm.CompanyGUID);
                var serviceFeeTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<ServiceFeeTransItemViewMap, ServiceFeeTransItemView>().ToDropDownItem(search.ExcludeRowData);


                return serviceFeeTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #region GetListvw
        private IQueryable<ServiceFeeTransListViewMap> GetListQuery()
        {
            return (from serviceFeeTrans in Entity
                    join invoiceRevenueType in db.Set<InvoiceRevenueType>()
                    on serviceFeeTrans.InvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID
                    select new ServiceFeeTransListViewMap
                    {
                        CompanyGUID = serviceFeeTrans.CompanyGUID,
                        Owner = serviceFeeTrans.Owner,
                        OwnerBusinessUnitGUID = serviceFeeTrans.OwnerBusinessUnitGUID,
                        ServiceFeeTransGUID = serviceFeeTrans.ServiceFeeTransGUID,
                        Ordering = serviceFeeTrans.Ordering,
                        Description = serviceFeeTrans.Description,
                        InvoiceRevenueTypeGUID = serviceFeeTrans.InvoiceRevenueTypeGUID,
                        IncludeTax = serviceFeeTrans.IncludeTax,
                        FeeAmount = serviceFeeTrans.FeeAmount,
                        AmountBeforeTax = serviceFeeTrans.AmountBeforeTax,
                        InvoiceRevenueType_Values = SmartAppUtil.GetDropDownLabel(invoiceRevenueType.RevenueTypeId, invoiceRevenueType.Description),
                        InvoiceRevenueType_RevenueTypeId = invoiceRevenueType.RevenueTypeId,
                        RefGUID = serviceFeeTrans.RefGUID
                    });
        }
        public SearchResult<ServiceFeeTransListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<ServiceFeeTransListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<ServiceFeeTrans>(search, SysParm.CompanyGUID);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<ServiceFeeTransListViewMap, ServiceFeeTransListView>();
                result = list.SetSearchResult<ServiceFeeTransListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<ServiceFeeTransItemViewMap> GetItemQuery()
        {
            return (from serviceFeeTrans in Entity
                    join company in db.Set<Company>()
                    on serviceFeeTrans.CompanyGUID equals company.CompanyGUID
                    join ownerBU in db.Set<BusinessUnit>()
                    on serviceFeeTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljServiceFeeTransOwnerBU
                    from ownerBU in ljServiceFeeTransOwnerBU.DefaultIfEmpty()
                    join invoiceRevenueType in db.Set<InvoiceRevenueType>()
                    on serviceFeeTrans.InvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID
                    select new ServiceFeeTransItemViewMap
                    {
                        CompanyGUID = serviceFeeTrans.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = serviceFeeTrans.Owner,
                        OwnerBusinessUnitGUID = serviceFeeTrans.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        CreatedBy = serviceFeeTrans.CreatedBy,
                        CreatedDateTime = serviceFeeTrans.CreatedDateTime,
                        ModifiedBy = serviceFeeTrans.ModifiedBy,
                        ModifiedDateTime = serviceFeeTrans.ModifiedDateTime,
                        ServiceFeeTransGUID = serviceFeeTrans.ServiceFeeTransGUID,
                        AmountBeforeTax = serviceFeeTrans.AmountBeforeTax,
                        AmountIncludeTax = serviceFeeTrans.AmountIncludeTax,
                        CNReasonGUID = serviceFeeTrans.CNReasonGUID,
                        Description = serviceFeeTrans.Description,
                        Dimension1GUID = serviceFeeTrans.Dimension1GUID,
                        Dimension2GUID = serviceFeeTrans.Dimension2GUID,
                        Dimension3GUID = serviceFeeTrans.Dimension3GUID,
                        Dimension4GUID = serviceFeeTrans.Dimension4GUID,
                        Dimension5GUID = serviceFeeTrans.Dimension5GUID,
                        FeeAmount = serviceFeeTrans.FeeAmount,
                        IncludeTax = serviceFeeTrans.IncludeTax,
                        InvoiceRevenueTypeGUID = serviceFeeTrans.InvoiceRevenueTypeGUID,
                        Ordering = serviceFeeTrans.Ordering,
                        OrigInvoiceAmount = serviceFeeTrans.OrigInvoiceAmount,
                        OrigInvoiceId = serviceFeeTrans.OrigInvoiceId,
                        OrigTaxInvoiceAmount = serviceFeeTrans.OrigTaxInvoiceAmount,
                        OrigTaxInvoiceId = serviceFeeTrans.OrigTaxInvoiceId,
                        RefGUID = serviceFeeTrans.RefGUID,
                        RefType = serviceFeeTrans.RefType,
                        SettleAmount = serviceFeeTrans.SettleAmount,
                        SettleInvoiceAmount = serviceFeeTrans.SettleInvoiceAmount,
                        SettleWHTAmount = serviceFeeTrans.SettleWHTAmount,
                        TaxAmount = serviceFeeTrans.TaxAmount,
                        TaxTableGUID = serviceFeeTrans.TaxTableGUID,
                        WHTAmount = serviceFeeTrans.WHTAmount,
                        WithholdingTaxTableGUID = serviceFeeTrans.WithholdingTaxTableGUID,
                        WHTSlipReceivedByCustomer = serviceFeeTrans.WHTSlipReceivedByCustomer,
                        InvoiceRevenueType_InterompanyTableGUID = invoiceRevenueType.IntercompanyTableGUID,
                    
                        RowVersion = serviceFeeTrans.RowVersion,
                    });
        }
        public ServiceFeeTransItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<ServiceFeeTransItemViewMap, ServiceFeeTransItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public ServiceFeeTrans CreateServiceFeeTrans(ServiceFeeTrans serviceFeeTrans)
        {
            try
            {
                serviceFeeTrans.ServiceFeeTransGUID = Guid.NewGuid();
                base.Add(serviceFeeTrans);
                return serviceFeeTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateServiceFeeTransVoid(ServiceFeeTrans serviceFeeTrans)
        {
            try
            {
                CreateServiceFeeTrans(serviceFeeTrans);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ServiceFeeTrans UpdateServiceFeeTrans(ServiceFeeTrans dbServiceFeeTrans, ServiceFeeTrans inputServiceFeeTrans, List<string> skipUpdateFields = null)
        {
            try
            {
                dbServiceFeeTrans = dbServiceFeeTrans.MapUpdateValues<ServiceFeeTrans>(inputServiceFeeTrans);
                base.Update(dbServiceFeeTrans);
                return dbServiceFeeTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateServiceFeeTransVoid(ServiceFeeTrans dbServiceFeeTrans, ServiceFeeTrans inputServiceFeeTrans, List<string> skipUpdateFields = null)
        {
            try
            {
                dbServiceFeeTrans = dbServiceFeeTrans.MapUpdateValues<ServiceFeeTrans>(inputServiceFeeTrans, skipUpdateFields);
                base.Update(dbServiceFeeTrans);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Create, Update, Delete
        public ServiceFeeTrans GetServiceFeeTransByIdNoTrackingByAccessLevel(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.ServiceFeeTransGUID == guid)
                    .FilterByAccessLevel(SysParm.AccessLevel)
                    .AsNoTracking()
                    .FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public ServiceFeeTrans GetServiceFeeTransByMessengerJob(Guid refGUID, int refType)
        {
            try
            {
                var result = Entity.Where(item => item.RefGUID == refGUID && item.RefType == refType)
                    .FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public override void ValidateAdd(IEnumerable<ServiceFeeTrans> items)
        {
            base.ValidateAdd(items);
        }
        public override void ValidateAdd(ServiceFeeTrans item)
        {
            base.ValidateAdd(item);
        }

        #region GetServiceFeeTransByReferenceNoTracking
        public IEnumerable<ServiceFeeTrans> GetServiceFeeTransByReferenceNoTracking(RefType refType, Guid refGUID)
        {
            try
            {
                var result = Entity.Where(w => w.RefType == (int)refType
                                       && w.RefGUID == refGUID)
                    .AsNoTracking()
                    .ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region function
        #region shared GenInvoiceFromServiceFeeTrans
        public IEnumerable<ServiceFeeTrans> GetServiceFeeTransInvoiceRevenueType()
        {
            try
            {
                var list = (from serviceFeeTrans in Entity
                            join invoiceRevenueType in db.Set<InvoiceRevenueType>()
                            on serviceFeeTrans.InvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID into ljServiceFeeTransInvRevenue
                            from invoiceRevenueType in ljServiceFeeTransInvRevenue.DefaultIfEmpty()
                            where invoiceRevenueType.IntercompanyTableGUID == null && serviceFeeTrans.FeeAmount != 0
                            select serviceFeeTrans).AsNoTracking();
                return list;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #endregion
    }
}

