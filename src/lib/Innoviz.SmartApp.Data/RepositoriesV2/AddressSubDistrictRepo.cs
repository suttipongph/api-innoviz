using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IAddressSubDistrictRepo
	{
		#region DropDown
		IEnumerable<SelectItem<AddressSubDistrictItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<AddressSubDistrictListView> GetListvw(SearchParameter search);
		AddressSubDistrictItemView GetByIdvw(Guid id);
		AddressSubDistrict Find(params object[] keyValues);
		AddressSubDistrict GetAddressSubDistrictByIdNoTracking(Guid guid);
		AddressSubDistrict CreateAddressSubDistrict(AddressSubDistrict addressSubDistrict);
		void CreateAddressSubDistrictVoid(AddressSubDistrict addressSubDistrict);
		AddressSubDistrict UpdateAddressSubDistrict(AddressSubDistrict dbAddressSubDistrict, AddressSubDistrict inputAddressSubDistrict, List<string> skipUpdateFields = null);
		void UpdateAddressSubDistrictVoid(AddressSubDistrict dbAddressSubDistrict, AddressSubDistrict inputAddressSubDistrict, List<string> skipUpdateFields = null);
		void Remove(AddressSubDistrict item);
		List<AddressSubDistrict> GetAddressSubDistrictByCompanyNoTracking(Guid companyGUID);
	}
	public class AddressSubDistrictRepo : CompanyBaseRepository<AddressSubDistrict>, IAddressSubDistrictRepo
	{
		public AddressSubDistrictRepo(SmartAppDbContext context) : base(context) { }
		public AddressSubDistrictRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public AddressSubDistrict GetAddressSubDistrictByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.AddressSubDistrictGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<AddressSubDistrictItemViewMap> GetDropDownQuery()
		{
			return (from addressSubDistrict in Entity
					select new AddressSubDistrictItemViewMap
					{
						CompanyGUID = addressSubDistrict.CompanyGUID,
						Owner = addressSubDistrict.Owner,
						OwnerBusinessUnitGUID = addressSubDistrict.OwnerBusinessUnitGUID,
						AddressSubDistrictGUID = addressSubDistrict.AddressSubDistrictGUID,
						SubDistrictId = addressSubDistrict.SubDistrictId,
						Name = addressSubDistrict.Name,
						AddressDistrictGUID = addressSubDistrict.AddressDistrictGUID
					});
		}
		public IEnumerable<SelectItem<AddressSubDistrictItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<AddressSubDistrict>(search, SysParm.CompanyGUID);
				var addressSubDistrict = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<AddressSubDistrictItemViewMap, AddressSubDistrictItemView>().ToDropDownItem(search.ExcludeRowData);


				return addressSubDistrict;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<AddressSubDistrictListViewMap> GetListQuery()
		{
			return (from addressSubDistrict in Entity
					join addressDistrict in db.Set<AddressDistrict>()
					on addressSubDistrict.AddressDistrictGUID equals addressDistrict.AddressDistrictGUID into ljAddressDistrict
					from addressDistrict in ljAddressDistrict.DefaultIfEmpty()
					join addressProvince in db.Set<AddressProvince>()
					on addressDistrict.AddressProvinceGUID equals addressProvince.AddressProvinceGUID into ljAddressProvince
					from addressProvince in ljAddressProvince.DefaultIfEmpty()
					select new AddressSubDistrictListViewMap
				{
						CompanyGUID = addressSubDistrict.CompanyGUID,
						Owner = addressSubDistrict.Owner,
						OwnerBusinessUnitGUID = addressSubDistrict.OwnerBusinessUnitGUID,
						AddressSubDistrictGUID = addressSubDistrict.AddressSubDistrictGUID,
						AddressDistrictGUID = addressSubDistrict.AddressDistrictGUID,
						addressDistrict_DistrictId = addressDistrict.DistrictId,
						addressProvince_ProvinceId = addressProvince.ProvinceId,
						SubDistrictId = addressSubDistrict.SubDistrictId,
						Name = addressSubDistrict.Name,
						AddressDistrict_Values = SmartAppUtil.GetDropDownLabel(addressDistrict.DistrictId,addressDistrict.Name),
						AddressProvince_Values = SmartAppUtil.GetDropDownLabel(addressProvince.ProvinceId,addressProvince.Name),
						AddressProvinceGUID = addressProvince.AddressProvinceGUID
					});
		}
		public SearchResult<AddressSubDistrictListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<AddressSubDistrictListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<AddressSubDistrict>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<AddressSubDistrictListViewMap, AddressSubDistrictListView>();
				result = list.SetSearchResult<AddressSubDistrictListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<AddressSubDistrictItemViewMap> GetItemQuery()
		{
			return (from addressSubDistrict in Entity
					join company in db.Set<Company>()
					on addressSubDistrict.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on addressSubDistrict.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljAddressSubDistrictOwnerBU
					from ownerBU in ljAddressSubDistrictOwnerBU.DefaultIfEmpty()
					join addressDistrict in db.Set<AddressDistrict>()
					on addressSubDistrict.AddressDistrictGUID equals addressDistrict.AddressDistrictGUID into ljAddressDistrict
					from addressDistrict in ljAddressDistrict.DefaultIfEmpty()
					select new AddressSubDistrictItemViewMap
					{
						CompanyGUID = addressSubDistrict.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = addressSubDistrict.Owner,
						OwnerBusinessUnitGUID = addressSubDistrict.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = addressSubDistrict.CreatedBy,
						CreatedDateTime = addressSubDistrict.CreatedDateTime,
						ModifiedBy = addressSubDistrict.ModifiedBy,
						ModifiedDateTime = addressSubDistrict.ModifiedDateTime,
						AddressSubDistrictGUID = addressSubDistrict.AddressSubDistrictGUID,
						AddressDistrictGUID = addressSubDistrict.AddressDistrictGUID,
						Name = addressSubDistrict.Name,
						SubDistrictId = addressSubDistrict.SubDistrictId,
						addressDistrict_DistrictId = addressDistrict.DistrictId,
					
						RowVersion = addressSubDistrict.RowVersion,
					});
		}
		public AddressSubDistrictItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<AddressSubDistrictItemViewMap, AddressSubDistrictItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public AddressSubDistrict CreateAddressSubDistrict(AddressSubDistrict addressSubDistrict)
		{
			try
			{
				addressSubDistrict.AddressSubDistrictGUID = Guid.NewGuid();
				base.Add(addressSubDistrict);
				return addressSubDistrict;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateAddressSubDistrictVoid(AddressSubDistrict addressSubDistrict)
		{
			try
			{
				CreateAddressSubDistrict(addressSubDistrict);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AddressSubDistrict UpdateAddressSubDistrict(AddressSubDistrict dbAddressSubDistrict, AddressSubDistrict inputAddressSubDistrict, List<string> skipUpdateFields = null)
		{
			try
			{
				dbAddressSubDistrict = dbAddressSubDistrict.MapUpdateValues<AddressSubDistrict>(inputAddressSubDistrict);
				base.Update(dbAddressSubDistrict);
				return dbAddressSubDistrict;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateAddressSubDistrictVoid(AddressSubDistrict dbAddressSubDistrict, AddressSubDistrict inputAddressSubDistrict, List<string> skipUpdateFields = null)
		{
			try
			{
				dbAddressSubDistrict = dbAddressSubDistrict.MapUpdateValues<AddressSubDistrict>(inputAddressSubDistrict, skipUpdateFields);
				base.Update(dbAddressSubDistrict);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public List<AddressSubDistrict> GetAddressSubDistrictByCompanyNoTracking(Guid companyGUID)
		{
			try
			{
				List<AddressSubDistrict> addressSubDistricts = Entity.Where(w => w.CompanyGUID == companyGUID).AsNoTracking().ToList();
				return addressSubDistricts;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

