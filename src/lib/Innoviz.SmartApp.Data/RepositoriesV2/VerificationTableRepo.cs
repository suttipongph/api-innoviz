using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface IVerificationTableRepo
    {
        #region DropDown
        IEnumerable<SelectItem<VerificationTableItemView>> GetDropDownItem(SearchParameter search);
        IEnumerable<SelectItem<VerificationTableItemView>> GetDropDownItemBy(SearchParameter search);
        #endregion DropDown
        SearchResult<VerificationTableListView> GetListvw(SearchParameter search);
        VerificationTableItemView GetByIdvw(Guid id);
        VerificationTable Find(params object[] keyValues);
        VerificationTable GetVerificationTableByIdNoTracking(Guid guid);
        VerificationTable CreateVerificationTable(VerificationTable verificationTable);
        void CreateVerificationTableVoid(VerificationTable verificationTable);
        VerificationTable UpdateVerificationTable(VerificationTable dbVerificationTable, VerificationTable inputVerificationTable, List<string> skipUpdateFields = null);
        void UpdateVerificationTableVoid(VerificationTable dbVerificationTable, VerificationTable inputVerificationTable, List<string> skipUpdateFields = null);
        void Remove(VerificationTable item);
        VerificationTable GetVerificationTableByIdNoTrackingByAccessLevel(Guid guid);
        IEnumerable<VerificationTable> GetVerificationTableByPurchaseTableNoTracking(Guid purchaseTableGUID);
        IEnumerable<VerificationTable> GetVerificationTableByVerificationTableGUIDListNoTracking(List<Guid> verificationTableGUID);
        void ValidateAdd(VerificationTable item);
        void ValidateAdd(IEnumerable<VerificationTable> items);
    }
    public class VerificationTableRepo : CompanyBaseRepository<VerificationTable>, IVerificationTableRepo
    {
        public VerificationTableRepo(SmartAppDbContext context) : base(context) { }
        public VerificationTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public VerificationTable GetVerificationTableByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.VerificationTableGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        private IQueryable<VerificationTableItemViewMap> GetDropDownQuery()
        {
            return (from verificationTable in Entity
                    join documentStatus in db.Set<DocumentStatus>() on verificationTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
                    join buyerTable in db.Set<BuyerTable>() on verificationTable.BuyerTableGUID equals buyerTable.BuyerTableGUID
                    select new VerificationTableItemViewMap
                    {
                        CompanyGUID = verificationTable.CompanyGUID,
                        Owner = verificationTable.Owner,
                        OwnerBusinessUnitGUID = verificationTable.OwnerBusinessUnitGUID,
                        VerificationTableGUID = verificationTable.VerificationTableGUID,
                        VerificationId = verificationTable.VerificationId,
                        Description = verificationTable.Description,
                        BuyerTableGUID = verificationTable.BuyerTableGUID,
                        CreditAppTableGUID = verificationTable.CreditAppTableGUID,
                        DocumentStatus_StatusId = documentStatus.StatusId,
                        VerificationDate = verificationTable.VerificationDate,
                        BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
                        Remark = verificationTable.Remark

                    });
        }
        public IEnumerable<SelectItem<VerificationTableItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<VerificationTable>(search, SysParm.CompanyGUID);
                var verificationTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<VerificationTableItemViewMap, VerificationTableItemView>().ToDropDownItem(search.ExcludeRowData);


                return verificationTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private IQueryable<VerificationTableItemViewMap> GetDropDownQueryBy()
        {
            return (from verificationTable in Entity

                    join company in db.Set<Company>()
                    on verificationTable.CompanyGUID equals company.CompanyGUID into ljcompany
                    from company in ljcompany.DefaultIfEmpty()

                    join ownerBU in db.Set<BusinessUnit>()
                    on verificationTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljownerBU
                    from ownerBU in ljownerBU.DefaultIfEmpty()

                    join documentStatus in db.Set<DocumentStatus>() 
                    on verificationTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus
                    from documentStatus in ljdocumentStatus.DefaultIfEmpty()

                    join customerTable in db.Set<CustomerTable>() 
                    on verificationTable.CustomerTableGUID equals customerTable.CustomerTableGUID into ljcustomerTable
                    from customerTable in ljcustomerTable.DefaultIfEmpty()

                    join creditAppTable in db.Set<CreditAppTable>()
                    on verificationTable.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljcreditAppTable
                    from creditAppTable in ljcreditAppTable.DefaultIfEmpty()

                    join buyerTable in db.Set<BuyerTable>()
                    on verificationTable.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
                    from buyerTable in ljbuyerTable.DefaultIfEmpty()
                    select new VerificationTableItemViewMap
                    {
                        CompanyGUID = verificationTable.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = verificationTable.Owner,
                        OwnerBusinessUnitGUID = verificationTable.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        CreatedBy = verificationTable.CreatedBy,
                        CreatedDateTime = verificationTable.CreatedDateTime,
                        ModifiedBy = verificationTable.ModifiedBy,
                        ModifiedDateTime = verificationTable.ModifiedDateTime,
                        VerificationTableGUID = verificationTable.VerificationTableGUID,
                        BuyerTableGUID = verificationTable.BuyerTableGUID,
                        CreditAppTableGUID = verificationTable.CreditAppTableGUID,
                        CreditAppTable_Value = creditAppTable.CreditAppId,
                        CustomerTableGUID = verificationTable.CustomerTableGUID,
                        CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                        Description = verificationTable.Description,
                        DocumentStatusGUID = verificationTable.DocumentStatusGUID,
                        Remark = verificationTable.Remark,
                        VerificationDate = verificationTable.VerificationDate,
                        VerificationId = verificationTable.VerificationId,
                        DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
                        DocumentStatus_StatusId = documentStatus.StatusId,
                        originalDocumentStatusGUID = verificationTable.DocumentStatusGUID,
                        CreditAppTable_Values = creditAppTable.CreditAppId,
                        BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
                    });
        }
        public IEnumerable<SelectItem<VerificationTableItemView>> GetDropDownItemBy(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<VerificationTable>(search, SysParm.CompanyGUID);
                var verificationTable = GetDropDownQueryBy().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<VerificationTableItemViewMap, VerificationTableItemView>().ToDropDownItem(search.ExcludeRowData);


                return verificationTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #region GetListvw
        private IQueryable<VerificationTableListViewMap> GetListQuery()
        {

            return (from verificationTable in Entity
                    join documentStatus in db.Set<DocumentStatus>() on verificationTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
                    join customerTable in db.Set<CustomerTable>() on verificationTable.CustomerTableGUID equals customerTable.CustomerTableGUID
                    join buyerTable in db.Set<BuyerTable>() on verificationTable.BuyerTableGUID equals buyerTable.BuyerTableGUID
                    join creditAppTable in db.Set<CreditAppTable>() on verificationTable.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID
                    select new VerificationTableListViewMap
                    {
                        CompanyGUID = verificationTable.CompanyGUID,
                        Owner = verificationTable.Owner,
                        OwnerBusinessUnitGUID = verificationTable.OwnerBusinessUnitGUID,
                        VerificationTableGUID = verificationTable.VerificationTableGUID,
                        VerificationId = verificationTable.VerificationId,
                        Description = verificationTable.Description,
                        VerificationDate = verificationTable.VerificationDate,
                        CustomerTableGUID = verificationTable.CustomerTableGUID,
                        CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                        BuyerTableGUID = verificationTable.BuyerTableGUID,
                        DocumentStatusGUID = verificationTable.DocumentStatusGUID,
                        BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
                        DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
                        DocumentStatus_StatusId = documentStatus.StatusId,
                        CreditAppTableGUID = verificationTable.CreditAppTableGUID,
                        CreditAppTable_Values = creditAppTable.CreditAppId,

                    });
        }
        public SearchResult<VerificationTableListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<VerificationTableListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<VerificationTable>(search, SysParm.CompanyGUID);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<VerificationTableListViewMap, VerificationTableListView>();
                result = list.SetSearchResult<VerificationTableListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<VerificationTableItemViewMap> GetItemQuery(Guid guid)
        {
            IVerificationLineRepo verificationLineRepo = new VerificationLineRepo(db);
            int countVerificationLines = verificationLineRepo.GetVerificationLineByVerificationTableNoTracking(guid).Count();

            return (from verificationTable in Entity
                    join company in db.Set<Company>()
                    on verificationTable.CompanyGUID equals company.CompanyGUID
                    join ownerBU in db.Set<BusinessUnit>()
                    on verificationTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljVerificationTableOwnerBU
                    from ownerBU in ljVerificationTableOwnerBU.DefaultIfEmpty()
                    join documentStatus in db.Set<DocumentStatus>() on verificationTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
                    join customerTable in db.Set<CustomerTable>() on verificationTable.CustomerTableGUID equals customerTable.CustomerTableGUID
                    join creditAppTable in db.Set<CreditAppTable>() on verificationTable.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID
                    join buyerTable in db.Set<BuyerTable>() on verificationTable.BuyerTableGUID equals buyerTable.BuyerTableGUID

                    select new VerificationTableItemViewMap
                    {
                        CompanyGUID = verificationTable.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = verificationTable.Owner,
                        OwnerBusinessUnitGUID = verificationTable.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        CreatedBy = verificationTable.CreatedBy,
                        CreatedDateTime = verificationTable.CreatedDateTime,
                        ModifiedBy = verificationTable.ModifiedBy,
                        ModifiedDateTime = verificationTable.ModifiedDateTime,
                        VerificationTableGUID = verificationTable.VerificationTableGUID,
                        BuyerTableGUID = verificationTable.BuyerTableGUID,
                        CreditAppTableGUID = verificationTable.CreditAppTableGUID,
                        CreditAppTable_Value = creditAppTable.CreditAppId,
                        CustomerTableGUID = verificationTable.CustomerTableGUID,
                        CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                        Description = verificationTable.Description,
                        DocumentStatusGUID = verificationTable.DocumentStatusGUID,
                        Remark = verificationTable.Remark,
                        VerificationDate = verificationTable.VerificationDate,
                        VerificationId = verificationTable.VerificationId,
                        DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
                        DocumentStatus_StatusId = documentStatus.StatusId,
                        originalDocumentStatusGUID = verificationTable.DocumentStatusGUID,
                        CreditAppTable_Values = creditAppTable.CreditAppId,
                        BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
                        TotalVericationLine = countVerificationLines,
                    
                        RowVersion = verificationTable.RowVersion,
                    });
        }
        public VerificationTableItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery(guid)
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<VerificationTableItemViewMap, VerificationTableItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public VerificationTable CreateVerificationTable(VerificationTable verificationTable)
        {
            try
            {
                verificationTable.VerificationTableGUID = Guid.NewGuid();
                base.Add(verificationTable);
                return verificationTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateVerificationTableVoid(VerificationTable verificationTable)
        {
            try
            {
                CreateVerificationTable(verificationTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public VerificationTable UpdateVerificationTable(VerificationTable dbVerificationTable, VerificationTable inputVerificationTable, List<string> skipUpdateFields = null)
        {
            try
            {
                dbVerificationTable = dbVerificationTable.MapUpdateValues<VerificationTable>(inputVerificationTable);
                base.Update(dbVerificationTable);
                return dbVerificationTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateVerificationTableVoid(VerificationTable dbVerificationTable, VerificationTable inputVerificationTable, List<string> skipUpdateFields = null)
        {
            try
            {
                dbVerificationTable = dbVerificationTable.MapUpdateValues<VerificationTable>(inputVerificationTable, skipUpdateFields);
                base.Update(dbVerificationTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Create, Update, Delete
        #region Function
        public VerificationTable GetVerificationTableByIdNoTrackingByAccessLevel(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.VerificationTableGUID == guid)
                    .FilterByAccessLevel(SysParm.AccessLevel)
                    .AsNoTracking()
                    .FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Function
        public IEnumerable<VerificationTable> GetVerificationTableByPurchaseTableNoTracking(Guid purchaseTableGUID)
        {
            try
            {
                var result = (from verificationTable in Entity

                              join verificationTrans in db.Set<VerificationTrans>()
                              on verificationTable.VerificationTableGUID equals verificationTrans.VerificationTableGUID

                              join purchaseLine in db.Set<PurchaseLine>()
                              on verificationTrans.RefGUID equals purchaseLine.PurchaseLineGUID

                              join purchaseTable in db.Set<PurchaseTable>()
                              on purchaseLine.PurchaseTableGUID equals purchaseTable.PurchaseTableGUID

                              where verificationTrans.RefType == (int)RefType.PurchaseLine && purchaseLine.PurchaseTableGUID == purchaseTableGUID
                              select verificationTable)
                              .AsNoTracking();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<VerificationTable> GetVerificationTableByVerificationTableGUIDListNoTracking(List<Guid> verificationTableGUID)
        {
            try
            {
                var result = (from verificationTable in Entity
                              where verificationTableGUID.Contains(verificationTable.VerificationTableGUID)
                              select verificationTable)
                              .AsNoTracking();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public override void ValidateAdd(IEnumerable<VerificationTable> items)
        {
            base.ValidateAdd(items);
        }
        public override void ValidateAdd(VerificationTable item)
        {
            base.ValidateAdd(item);
        }
    }
}

