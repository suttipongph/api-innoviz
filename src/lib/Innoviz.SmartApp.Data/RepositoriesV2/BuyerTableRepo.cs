using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface IBuyerTableRepo
    {
        #region DropDown
        IEnumerable<SelectItem<BuyerTableItemView>> GetDropDownItem(SearchParameter search);
        IEnumerable<SelectItem<BuyerTableItemView>> GetDropDownItemByCreditAppRequest(SearchParameter search);
        IEnumerable<SelectItem<BuyerTableItemView>> GetDropDownItemByMainAgreementTable(SearchParameter search);
        IEnumerable<SelectItem<BuyerTableItemView>> GetDropDownItemByPurchaseLine(SearchParameter search, DateTime expiryDate);
        #endregion DropDown
        SearchResult<BuyerTableListView> GetListvw(SearchParameter search);
        BuyerTableItemView GetByIdvw(Guid id);
        BuyerTable Find(params object[] keyValues);
        BuyerTable GetBuyerTableByIdNoTracking(Guid guid);
        BuyerTable GetBuyerTableByIdNoTrackingByAccessLevel(Guid guid);
        BuyerTable CreateBuyerTable(BuyerTable buyerTable);
        void CreateBuyerTableVoid(BuyerTable buyerTable);
        BuyerTable UpdateBuyerTable(BuyerTable dbBuyerTable, BuyerTable inputBuyerTable, List<string> skipUpdateFields = null);
        void UpdateBuyerTableVoid(BuyerTable dbBuyerTable, BuyerTable inputBuyerTable, List<string> skipUpdateFields = null);
        void Remove(BuyerTable item);
        #region function
        UpdateBuyerTableBlacklistStatusResultView GetBlacklistStatusById(Guid guid);
        #endregion
        void ValidateAdd(BuyerTable item);
        void ValidateAdd(IEnumerable<BuyerTable> items);
    }
    public class BuyerTableRepo : CompanyBaseRepository<BuyerTable>, IBuyerTableRepo
    {
        public BuyerTableRepo(SmartAppDbContext context) : base(context) { }
        public BuyerTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public BuyerTable GetBuyerTableByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.BuyerTableGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BuyerTable GetBuyerTableByIdNoTrackingByAccessLevel(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.BuyerTableGUID == guid)
                    .FilterByAccessLevel(SysParm.AccessLevel)
                    .AsNoTracking()
                    .FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        private IQueryable<BuyerTableItemViewMap> GetDropDownQuery()
        {
            return (from buyerTable in Entity
                    select new BuyerTableItemViewMap
                    {
                        CompanyGUID = buyerTable.CompanyGUID,
                        Owner = buyerTable.Owner,
                        OwnerBusinessUnitGUID = buyerTable.OwnerBusinessUnitGUID,
                        BuyerTableGUID = buyerTable.BuyerTableGUID,
                        BuyerId = buyerTable.BuyerId,
                        BuyerName = buyerTable.BuyerName,
                        TaxId = buyerTable.TaxId,
                        PassportId = buyerTable.PassportId,
                        IdentificationType = buyerTable.IdentificationType,
                    });
        }
        public IEnumerable<SelectItem<BuyerTableItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<BuyerTable>(search, SysParm.CompanyGUID);
                var buyerTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
                    .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                    .Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
                    .ToMaps<BuyerTableItemViewMap, BuyerTableItemView>().ToDropDownItem(search.ExcludeRowData);
                return buyerTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private IQueryable<BuyerTableItemViewMap> GetDropDownQueryByCreditAppRequest()
        {
            return (from buyerTable in Entity
                    join blacklistStatus in db.Set<BlacklistStatus>() on buyerTable.BlacklistStatusGUID equals blacklistStatus.BlacklistStatusGUID into ljBuyerTableBacklistStatus
                    from blacklistStatus in ljBuyerTableBacklistStatus.DefaultIfEmpty()
                    join businessSegment in db.Set<BusinessSegment>() on buyerTable.BusinessSegmentGUID equals businessSegment.BusinessSegmentGUID 
                    join lineOfBusiness in db.Set<LineOfBusiness>() on buyerTable.LineOfBusinessGUID equals lineOfBusiness.LineOfBusinessGUID into ljBuyerTableLineOfBusiness
                    from lineOfBusiness in ljBuyerTableLineOfBusiness.DefaultIfEmpty()
                    join businessType in db.Set<BusinessType>() on buyerTable.BusinessTypeGUID equals businessType.BusinessTypeGUID into ljBuyerTableBusinessType
                    from businessType in ljBuyerTableBusinessType.DefaultIfEmpty()
                    select new BuyerTableItemViewMap
                    {
                        CompanyGUID = buyerTable.CompanyGUID,
                        Owner = buyerTable.Owner,
                        OwnerBusinessUnitGUID = buyerTable.OwnerBusinessUnitGUID,
                        BuyerTableGUID = buyerTable.BuyerTableGUID,
                        BuyerId = buyerTable.BuyerId,
                        BuyerName = buyerTable.BuyerName,
                        DateOfEstablish = buyerTable.DateOfEstablish,
                        CreditScoringGUID = buyerTable.CreditScoringGUID,
                        KYCSetupGUID = buyerTable.KYCSetupGUID,
                        BlacklistStatusGUID = buyerTable.BlacklistStatusGUID,
                        BlacklistStatus_Values = (blacklistStatus != null) ? SmartAppUtil.GetDropDownLabel(blacklistStatus.BlacklistStatusId, blacklistStatus.Description) : null,
                        BusinessSegment_Values = SmartAppUtil.GetDropDownLabel(businessSegment.BusinessSegmentId, businessSegment.Description),
                        BusinessType_Values = (businessType != null) ? SmartAppUtil.GetDropDownLabel(businessType.BusinessTypeId, businessType.Description) : null,
                        LineOfBusiness_Values = (lineOfBusiness != null) ? SmartAppUtil.GetDropDownLabel(lineOfBusiness.LineOfBusinessId, lineOfBusiness.Description) : null,
                    });
        }
        public IEnumerable<SelectItem<BuyerTableItemView>> GetDropDownItemByCreditAppRequest(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<BuyerTable>(search, SysParm.CompanyGUID);
                var buyerTable = GetDropDownQueryByCreditAppRequest().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<BuyerTableItemViewMap, BuyerTableItemView>().ToDropDownItem(search.ExcludeRowData);


                return buyerTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private IQueryable<BuyerTableItemViewMap> GetDropDownQueryByMainAgreementTable()
        {
            return (from buyerTable in Entity
                    join company in db.Set<Company>() on buyerTable.CompanyGUID equals company.CompanyGUID
                    join creditAppLine in db.Set<CreditAppLine>() on buyerTable.BuyerTableGUID equals creditAppLine.BuyerTableGUID
                    join creditAppTable in db.Set<CreditAppTable>() on creditAppLine.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID
                    group buyerTable by new {
                                              buyerTable.BuyerTableGUID,
                                              buyerTable.CompanyGUID, 
                                              buyerTable.BuyerId, 
                                              buyerTable.BuyerName, 
                                              creditAppTable.CreditAppTableGUID,
                    } into g
                    select new BuyerTableItemViewMap
                    {
                        CompanyGUID = g.Key.CompanyGUID,
                        BuyerId = g.Key.BuyerId,
                        BuyerName = g.Key.BuyerName,
                        CreditAppTable_CreditAppTableGUID = g.Key.CreditAppTableGUID,
                        BuyerTableGUID = g.Key.BuyerTableGUID
                    });
        }
        private IQueryable<BuyerTableItemViewMap> GetDropDownQueryByPurchaseLine()
        {
            return (from buyerTable in Entity

                    join creditAppLine in db.Set<CreditAppLine>()
                    on buyerTable.BuyerTableGUID equals creditAppLine.BuyerTableGUID

                    join methodOfPayment in db.Set<MethodOfPayment>()
                    on creditAppLine.MethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID into ljMethodOfPayment
                    from methodOfPayment in ljMethodOfPayment.DefaultIfEmpty()
                    select new BuyerTableItemViewMap
                    {
                        CompanyGUID = buyerTable.CompanyGUID,
                        Owner = buyerTable.Owner,
                        OwnerBusinessUnitGUID = buyerTable.OwnerBusinessUnitGUID,
                        BuyerTableGUID = buyerTable.BuyerTableGUID,
                        BuyerId = buyerTable.BuyerId,
                        BuyerName = buyerTable.BuyerName,
                        TaxId = buyerTable.TaxId,
                        PassportId = buyerTable.PassportId,
                        IdentificationType = buyerTable.IdentificationType,
                        CreditAppTable_CreditAppTableGUID = creditAppLine.CreditAppTableGUID,
                        CreditAppLine_ExpiryDate = creditAppLine.ExpiryDate,
                        CreditAppLine_CreditAppLineGUID = creditAppLine.CreditAppLineGUID,
                        PurchaseLine_PurchaseFeeCalculateBase = creditAppLine.PurchaseFeeCalculateBase,
                        MethodOfPayment_MethodOfPaymentGUID = (methodOfPayment != null) ? (Guid?)methodOfPayment.MethodOfPaymentGUID : null,
                        MethodOfPayment_Values = (methodOfPayment != null) ? SmartAppUtil.GetDropDownLabel(methodOfPayment.MethodOfPaymentId, methodOfPayment.Description): null,
                        CreditAppLine_MaxPurchasePct = creditAppLine.MaxPurchasePct,
                        CreditAppLine_Values = creditAppLine.LineNum.ToString(),
                        CreditAppLine_PurchaseFeePct = creditAppLine.PurchaseFeePct
                    });
        }
        public IEnumerable<SelectItem<BuyerTableItemView>> GetDropDownItemByMainAgreementTable(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<BuyerTable>(search, SysParm.CompanyGUID);
                var buyerTable = GetDropDownQueryByMainAgreementTable().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<BuyerTableItemViewMap, BuyerTableItemView>().ToDropDownItem(search.ExcludeRowData);


                return buyerTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<BuyerTableItemView>> GetDropDownItemByPurchaseLine(SearchParameter search,DateTime purchaseDate)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<BuyerTable>(search, SysParm.CompanyGUID);
                var buyerTable = GetDropDownQueryByPurchaseLine().Where(t => t.CreditAppLine_ExpiryDate > purchaseDate).Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<BuyerTableItemViewMap, BuyerTableItemView>().ToDropDownItem(search.ExcludeRowData);


                return buyerTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #region GetListvw
        private IQueryable<BuyerTableListViewMap> GetListQuery()
        {
            return (from buyerTable in Entity
                    join documentStatus in db.Set<DocumentStatus>() on buyerTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljDocumentStatus
                    from documentStatus in ljDocumentStatus.DefaultIfEmpty()
                    select new BuyerTableListViewMap
                    {
                        CompanyGUID = buyerTable.CompanyGUID,
                        Owner = buyerTable.Owner,
                        OwnerBusinessUnitGUID = buyerTable.OwnerBusinessUnitGUID,
                        BuyerTableGUID = buyerTable.BuyerTableGUID,
                        BuyerId = buyerTable.BuyerId,
                        BuyerName = buyerTable.BuyerName,
                        PassportId = buyerTable.PassportId,
                        TaxId = buyerTable.TaxId,
                        DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
                        DocumentStatus_StatusId = documentStatus.StatusId,
                        DocumentStatusGUID = documentStatus.DocumentStatusGUID,
                    });
        }
        public SearchResult<BuyerTableListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<BuyerTableListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<BuyerTable>(search, SysParm.CompanyGUID);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<BuyerTableListViewMap, BuyerTableListView>();
                result = list.SetSearchResult<BuyerTableListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<BuyerTableItemViewMap> GetItemQuery()
        {
            return (from buyerTable in Entity
                    join ownerBU in db.Set<BusinessUnit>()
                    on buyerTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljBuyerTableOwnerBU
                    from ownerBU in ljBuyerTableOwnerBU.DefaultIfEmpty()
                    join company in db.Set<Company>() on buyerTable.CompanyGUID equals company.CompanyGUID
                    select new BuyerTableItemViewMap
                    {
                        CompanyGUID = buyerTable.CompanyGUID,
                        Owner = buyerTable.Owner,
                        OwnerBusinessUnitGUID = buyerTable.OwnerBusinessUnitGUID,
                        CreatedBy = buyerTable.CreatedBy,
                        CreatedDateTime = buyerTable.CreatedDateTime,
                        ModifiedBy = buyerTable.ModifiedBy,
                        ModifiedDateTime = buyerTable.ModifiedDateTime,
                        BuyerTableGUID = buyerTable.BuyerTableGUID,
                        AltName = buyerTable.AltName,
                        BlacklistStatusGUID = buyerTable.BlacklistStatusGUID,
                        BusinessSegmentGUID = buyerTable.BusinessSegmentGUID,
                        BusinessSizeGUID = buyerTable.BusinessSizeGUID,
                        BusinessTypeGUID = buyerTable.BusinessTypeGUID,
                        BuyerId = buyerTable.BuyerId,
                        BuyerName = buyerTable.BuyerName,
                        CreditScoringGUID = buyerTable.CreditScoringGUID,
                        CurrencyGUID = buyerTable.CurrencyGUID,
                        DateOfEstablish = buyerTable.DateOfEstablish,
                        DateOfExpiry = buyerTable.DateOfExpiry,
                        DateOfIssue = buyerTable.DateOfIssue,
                        DocumentStatusGUID = buyerTable.DocumentStatusGUID,
                        FixedAssets = buyerTable.FixedAssets,
                        IdentificationType = buyerTable.IdentificationType,
                        IssuedBy = buyerTable.IssuedBy,
                        KYCSetupGUID = buyerTable.KYCSetupGUID,
                        Labor = buyerTable.Labor,
                        LineOfBusinessGUID = buyerTable.LineOfBusinessGUID,
                        PaidUpCapital = buyerTable.PaidUpCapital,
                        PassportId = buyerTable.PassportId,
                        ReferenceId = buyerTable.ReferenceId,
                        RegisteredCapital = buyerTable.RegisteredCapital,
                        TaxId = buyerTable.TaxId,
                        WorkPermitId = buyerTable.WorkPermitId,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        CompanyId = company.CompanyId,
                    
                        RowVersion = buyerTable.RowVersion,
                    });
        }
        public BuyerTableItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<BuyerTableItemViewMap, BuyerTableItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public BuyerTable CreateBuyerTable(BuyerTable buyerTable)
        {
            try
            {
                buyerTable.BuyerTableGUID = Guid.NewGuid();
                base.Add(buyerTable);
                return buyerTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateBuyerTableVoid(BuyerTable buyerTable)
        {
            try
            {
                CreateBuyerTable(buyerTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BuyerTable UpdateBuyerTable(BuyerTable dbBuyerTable, BuyerTable inputBuyerTable, List<string> skipUpdateFields = null)
        {
            try
            {
                dbBuyerTable = dbBuyerTable.MapUpdateValues<BuyerTable>(inputBuyerTable);
                base.Update(dbBuyerTable);
                return dbBuyerTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateBuyerTableVoid(BuyerTable dbBuyerTable, BuyerTable inputBuyerTable, List<string> skipUpdateFields = null)
        {
            try
            {
                dbBuyerTable = dbBuyerTable.MapUpdateValues<BuyerTable>(inputBuyerTable, skipUpdateFields);
                base.Update(dbBuyerTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Create, Update, Delete
        #region function
        public UpdateBuyerTableBlacklistStatusResultView GetBlacklistStatusById(Guid guid)
        {
            try
            {
                var result = (from buyerTable in Entity
                              join blacklistStatus in db.Set<BlacklistStatus>()
                              on buyerTable.BlacklistStatusGUID equals blacklistStatus.BlacklistStatusGUID into ljBuyerTableBlacklistStatus
                              from blacklistStatus in ljBuyerTableBlacklistStatus.DefaultIfEmpty()
                              where buyerTable.BuyerTableGUID == guid
                              select new UpdateBuyerTableBlacklistStatusResultView
                              {
                                  BuyerTableGUID = buyerTable.BuyerTableGUID.GuidNullToString(),
                                  OriginalBlacklistStatusGUID = blacklistStatus.BlacklistStatusGUID.GuidNullToString(),
                                  OriginalBlacklistStatus = blacklistStatus.Description,
                                  BuyerId = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName)

                              }).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }
}

