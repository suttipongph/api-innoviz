using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using Innoviz.SmartApp.Core.Constants;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICreditAppTransRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CreditAppTransItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<CreditAppTransListView> GetListvw(SearchParameter search);
		CreditAppTransItemView GetByIdvw(Guid id);
		CreditAppTrans Find(params object[] keyValues);
		CreditAppTrans GetCreditAppTransByIdNoTracking(Guid guid);
		CreditAppTrans CreateCreditAppTrans(CreditAppTrans creditAppTrans);
		void CreateCreditAppTransVoid(CreditAppTrans creditAppTrans);
		CreditAppTrans UpdateCreditAppTrans(CreditAppTrans dbCreditAppTrans, CreditAppTrans inputCreditAppTrans, List<string> skipUpdateFields = null);
		void UpdateCreditAppTransVoid(CreditAppTrans dbCreditAppTrans, CreditAppTrans inputCreditAppTrans, List<string> skipUpdateFields = null);
		void Remove(CreditAppTrans item);
		CreditAppTrans GetCreditAppTransByIdNoTrackingByAccessLevel(Guid guid);
		IEnumerable<CreditAppTrans> GetCreditAppTransByCreditAppTableNoTracking(Guid guid);
		IEnumerable<CreditAppTrans> GetCreditAppTransByCustomerTableNoTracking(Guid guid);
		IEnumerable<CreditAppTrans> GetCreditAppTransByBuyerTableNoTracking(Guid guid);
		IEnumerable<CreditAppTrans> GetCreditAppTransByCreditAppLineNoTracking(Guid guid);
		void ValidateAdd(CreditAppTrans item);
		void ValidateAdd(IEnumerable<CreditAppTrans> items);
	}
	public class CreditAppTransRepo : CompanyBaseRepository<CreditAppTrans>, ICreditAppTransRepo
	{
		public CreditAppTransRepo(SmartAppDbContext context) : base(context) { }
		public CreditAppTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CreditAppTrans GetCreditAppTransByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CreditAppTransGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CreditAppTransItemViewMap> GetDropDownQuery()
		{
			return (from creditAppTrans in Entity
					select new CreditAppTransItemViewMap
					{
						CompanyGUID = creditAppTrans.CompanyGUID,
						Owner = creditAppTrans.Owner,
						OwnerBusinessUnitGUID = creditAppTrans.OwnerBusinessUnitGUID,
						CreditAppTransGUID = creditAppTrans.CreditAppTransGUID
					});
		}
		public IEnumerable<SelectItem<CreditAppTransItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditAppTrans>(search, SysParm.CompanyGUID);
				var creditAppTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CreditAppTransItemViewMap, CreditAppTransItemView>().ToDropDownItem(search.ExcludeRowData);


				return creditAppTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CreditAppTransListViewMap> GetListQuery()
		{
			return (from creditAppTrans in Entity
					join creditAppTable in db.Set<CreditAppTable>()
					on creditAppTrans.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID
					join creditAppLine in db.Set<CreditAppLine>()
					on creditAppTrans.CreditAppLineGUID equals creditAppLine.CreditAppLineGUID into ljCreditAppLine
					from creditAppLine in ljCreditAppLine.DefaultIfEmpty()
					join customerTable in db.Set<CustomerTable>()
					on creditAppTrans.CustomerTableGUID equals customerTable.CustomerTableGUID
					join buyerTable in db.Set<BuyerTable>()
					on creditAppTrans.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljBuyerTable
					from buyerTable in ljBuyerTable.DefaultIfEmpty()
					select new CreditAppTransListViewMap
				{
						CompanyGUID = creditAppTrans.CompanyGUID,
						Owner = creditAppTrans.Owner,
						OwnerBusinessUnitGUID = creditAppTrans.OwnerBusinessUnitGUID,
						CreditAppTransGUID = creditAppTrans.CreditAppTransGUID,
						CreditAppTableGUID = creditAppTrans.CreditAppTableGUID,
						DocumentId = creditAppTrans.DocumentId,
						ProductType = creditAppTrans.ProductType,
						CustomerTableGUID = creditAppTrans.CustomerTableGUID,
						BuyerTableGUID = creditAppTrans.BuyerTableGUID,
						CreditAppLineGUID = creditAppTrans.CreditAppLineGUID,
						TransDate = creditAppTrans.TransDate,
						InvoiceAmount = creditAppTrans.InvoiceAmount,
						CreditDeductAmount = creditAppTrans.CreditDeductAmount,
						RefType = creditAppTrans.RefType,
						RefType_Values = ((RefType)creditAppTrans.RefType).ToString(),
						CreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId,creditAppTable.Description),
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId,customerTable.Name),
						BuyerTable_Values = (buyerTable != null) ? SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId,buyerTable.BuyerName): null,
						CreditAppLine_Values = (creditAppLine != null) ? SmartAppUtil.GetDropDownLabel(creditAppLine.LineNum.ToString(),creditAppLine.ApprovedCreditLimitLine.ToString()): null,
						CreditAppTable_CreditAppId = creditAppTable.CreditAppId,
						CustomerTable_CustomerId = customerTable.CustomerId,
						BuyerTable_BuyerId = (buyerTable != null ) ? buyerTable.BuyerId: null,
						CreditAppLine_LineNum = (creditAppLine != null) ? creditAppLine.LineNum.ToString(): null
					});
		}
		public SearchResult<CreditAppTransListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CreditAppTransListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditAppTrans>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CreditAppTransListViewMap, CreditAppTransListView>();
				result = list.SetSearchResult<CreditAppTransListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CreditAppTransItemViewMap> GetItemQuery()
		{
			return (from creditAppTrans in Entity
					join company in db.Set<Company>()
					on creditAppTrans.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on creditAppTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCreditAppTransOwnerBU
					from ownerBU in ljCreditAppTransOwnerBU.DefaultIfEmpty()
					join creditAppTable in db.Set<CreditAppTable>()
					on creditAppTrans.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljcreditAppTable
					from creditAppTable in ljcreditAppTable.DefaultIfEmpty()
					join creditAppLine in db.Set<CreditAppLine>()
					on creditAppTrans.CreditAppLineGUID equals creditAppLine.CreditAppLineGUID into ljcreditAppLine
					from creditAppLine in ljcreditAppLine.DefaultIfEmpty()
					join customerTable in db.Set<CustomerTable>()
					on creditAppTrans.CustomerTableGUID equals customerTable.CustomerTableGUID into ljcustomerTable
					from customerTable in ljcustomerTable.DefaultIfEmpty()
					join buyerTable in db.Set<BuyerTable>()
					on creditAppTrans.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
					from buyerTable in ljbuyerTable.DefaultIfEmpty()
					select new CreditAppTransItemViewMap
					{
						CompanyGUID = creditAppTrans.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = creditAppTrans.Owner,
						OwnerBusinessUnitGUID = creditAppTrans.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = creditAppTrans.CreatedBy,
						CreatedDateTime = creditAppTrans.CreatedDateTime,
						ModifiedBy = creditAppTrans.ModifiedBy,
						ModifiedDateTime = creditAppTrans.ModifiedDateTime,
						CreditAppTransGUID = creditAppTrans.CreditAppTransGUID,
						BuyerTableGUID = creditAppTrans.BuyerTableGUID,
						CreditAppLineGUID = creditAppTrans.CreditAppLineGUID,
						CreditAppTableGUID = creditAppTrans.CreditAppTableGUID,
						CreditDeductAmount = creditAppTrans.CreditDeductAmount,
						CustomerTableGUID = creditAppTrans.CustomerTableGUID,
						DocumentId = creditAppTrans.DocumentId,
						InvoiceAmount = creditAppTrans.InvoiceAmount,
						ProductType = creditAppTrans.ProductType,
						RefGUID = creditAppTrans.RefGUID,
						RefType = creditAppTrans.RefType,
						TransDate = creditAppTrans.TransDate,
						ProductType_Values = ((ProductType)creditAppTrans.ProductType).ToString(),
						RefType_Values = ((RefType)creditAppTrans.RefType).ToString(),
						CreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
						BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
						CreditAppLine_Values = SmartAppUtil.GetDropDownLabel(creditAppLine.LineNum.ToString(), creditAppLine.ApprovedCreditLimitLine.ToString()),
					
						RowVersion = creditAppTrans.RowVersion,
					});
		}
		public CreditAppTransItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<CreditAppTransItemViewMap, CreditAppTransItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public CreditAppTrans CreateCreditAppTrans(CreditAppTrans creditAppTrans)
		{
			try
			{
				creditAppTrans.CreditAppTransGUID = creditAppTrans.CreditAppTransGUID == Guid.Empty ? Guid.NewGuid() : creditAppTrans.CreditAppTransGUID;
				base.Add(creditAppTrans);
				return creditAppTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCreditAppTransVoid(CreditAppTrans creditAppTrans)
		{
			try
			{
				CreateCreditAppTrans(creditAppTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditAppTrans UpdateCreditAppTrans(CreditAppTrans dbCreditAppTrans, CreditAppTrans inputCreditAppTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCreditAppTrans = dbCreditAppTrans.MapUpdateValues<CreditAppTrans>(inputCreditAppTrans);
				base.Update(dbCreditAppTrans);
				return dbCreditAppTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCreditAppTransVoid(CreditAppTrans dbCreditAppTrans, CreditAppTrans inputCreditAppTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCreditAppTrans = dbCreditAppTrans.MapUpdateValues<CreditAppTrans>(inputCreditAppTrans, skipUpdateFields);
				base.Update(dbCreditAppTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public CreditAppTrans GetCreditAppTransByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CreditAppTransGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public IEnumerable<CreditAppTrans> GetCreditAppTransByCreditAppTableNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CreditAppTableGUID == guid)
					.AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<CreditAppTrans> GetCreditAppTransByCustomerTableNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CustomerTableGUID == guid)
					.AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<CreditAppTrans> GetCreditAppTransByBuyerTableNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BuyerTableGUID == guid)
					.AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<CreditAppTrans> GetCreditAppTransByCreditAppLineNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CreditAppLineGUID == guid)
					.AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public override void ValidateAdd(IEnumerable<CreditAppTrans> items)
		{
			base.ValidateAdd(items);
		}
		public override void ValidateAdd(CreditAppTrans item)
		{
			base.ValidateAdd(item);
		}
	}
}

