using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IBusinessSegmentRepo
	{
		#region DropDown
		IEnumerable<SelectItem<BusinessSegmentItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<BusinessSegmentListView> GetListvw(SearchParameter search);
		BusinessSegmentItemView GetByIdvw(Guid id);
		BusinessSegment Find(params object[] keyValues);
		BusinessSegment GetBusinessSegmentByIdNoTracking(Guid guid);
		BusinessSegment CreateBusinessSegment(BusinessSegment businessSegment);
		void CreateBusinessSegmentVoid(BusinessSegment businessSegment);
		BusinessSegment UpdateBusinessSegment(BusinessSegment dbBusinessSegment, BusinessSegment inputBusinessSegment, List<string> skipUpdateFields = null);
		void UpdateBusinessSegmentVoid(BusinessSegment dbBusinessSegment, BusinessSegment inputBusinessSegment, List<string> skipUpdateFields = null);
		void Remove(BusinessSegment item);
	}
	public class BusinessSegmentRepo : CompanyBaseRepository<BusinessSegment>, IBusinessSegmentRepo
	{
		public BusinessSegmentRepo(SmartAppDbContext context) : base(context) { }
		public BusinessSegmentRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BusinessSegment GetBusinessSegmentByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BusinessSegmentGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<BusinessSegmentItemViewMap> GetDropDownQuery()
		{
			return (from businessSegment in Entity
					select new BusinessSegmentItemViewMap
					{
						CompanyGUID = businessSegment.CompanyGUID,
						Owner = businessSegment.Owner,
						OwnerBusinessUnitGUID = businessSegment.OwnerBusinessUnitGUID,
						BusinessSegmentGUID = businessSegment.BusinessSegmentGUID,
						BusinessSegmentId = businessSegment.BusinessSegmentId,
						Description = businessSegment.Description
					});
		}
		public IEnumerable<SelectItem<BusinessSegmentItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<BusinessSegment>(search, SysParm.CompanyGUID);
				var businessSegment = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<BusinessSegmentItemViewMap, BusinessSegmentItemView>().ToDropDownItem(search.ExcludeRowData);


				return businessSegment;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<BusinessSegmentListViewMap> GetListQuery()
		{
			return (from businessSegment in Entity
				select new BusinessSegmentListViewMap
				{
						CompanyGUID = businessSegment.CompanyGUID,
						Owner = businessSegment.Owner,
						OwnerBusinessUnitGUID = businessSegment.OwnerBusinessUnitGUID,
						BusinessSegmentGUID = businessSegment.BusinessSegmentGUID,
						BusinessSegmentId = businessSegment.BusinessSegmentId,
						Description = businessSegment.Description,
						BusinessSegmentType = businessSegment.BusinessSegmentType
				});
		}
		public SearchResult<BusinessSegmentListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<BusinessSegmentListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<BusinessSegment>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<BusinessSegmentListViewMap, BusinessSegmentListView>();
				result = list.SetSearchResult<BusinessSegmentListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<BusinessSegmentItemViewMap> GetItemQuery()
		{
			return (from businessSegment in Entity
					join ownerBU in db.Set<BusinessUnit>()
					on businessSegment.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljBusinessSegmentOwnerBU
					from ownerBU in ljBusinessSegmentOwnerBU.DefaultIfEmpty()
					join company in db.Set<Company>() on businessSegment.CompanyGUID equals company.CompanyGUID
					select new BusinessSegmentItemViewMap
					{
						CompanyGUID = businessSegment.CompanyGUID,
						Owner = businessSegment.Owner,
						OwnerBusinessUnitGUID = businessSegment.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CompanyId = company.CompanyId,
						CreatedBy = businessSegment.CreatedBy,
						CreatedDateTime = businessSegment.CreatedDateTime,
						ModifiedBy = businessSegment.ModifiedBy,
						ModifiedDateTime = businessSegment.ModifiedDateTime,
						BusinessSegmentGUID = businessSegment.BusinessSegmentGUID,
						BusinessSegmentId = businessSegment.BusinessSegmentId,
						Description = businessSegment.Description,
						BusinessSegmentType = businessSegment.BusinessSegmentType,
					
						RowVersion = businessSegment.RowVersion,
					});
		}
		public BusinessSegmentItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<BusinessSegmentItemViewMap, BusinessSegmentItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public BusinessSegment CreateBusinessSegment(BusinessSegment businessSegment)
		{
			try
			{
				businessSegment.BusinessSegmentGUID = Guid.NewGuid();
				base.Add(businessSegment);
				return businessSegment;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateBusinessSegmentVoid(BusinessSegment businessSegment)
		{
			try
			{
				CreateBusinessSegment(businessSegment);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BusinessSegment UpdateBusinessSegment(BusinessSegment dbBusinessSegment, BusinessSegment inputBusinessSegment, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBusinessSegment = dbBusinessSegment.MapUpdateValues<BusinessSegment>(inputBusinessSegment);
				base.Update(dbBusinessSegment);
				return dbBusinessSegment;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateBusinessSegmentVoid(BusinessSegment dbBusinessSegment, BusinessSegment inputBusinessSegment, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBusinessSegment = dbBusinessSegment.MapUpdateValues<BusinessSegment>(inputBusinessSegment, skipUpdateFields);
				base.Update(dbBusinessSegment);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

