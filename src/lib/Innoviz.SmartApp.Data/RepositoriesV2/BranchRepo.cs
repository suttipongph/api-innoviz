using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IBranchRepo
	{
        #region DropDown
        IEnumerable<SelectItem<BranchItemView>> GetDropDownItem(SearchParameter search);
        IEnumerable<SelectItem<BranchItemView>> GetDropDownItemNotFiltered();
        #endregion DropDown
        SearchResult<BranchListView> GetListvw(SearchParameter search);
        BranchItemView GetByIdvw(Guid id);
        Branch Find(params object[] keyValues);
		Branch GetBranchByIdNoTracking(Guid guid);
		Branch CreateBranch(Branch branch);
		void CreateBranchVoid(Branch branch);
		Branch UpdateBranch(Branch dbBranch, Branch inputBranch, List<string> skipUpdateFields = null);
		void UpdateBranchVoid(Branch dbBranch, Branch inputBranch, List<string> skipUpdateFields = null);
		void Remove(Branch item);
        List<Branch> GetFirstBranchOfCompanyNoTracking(List<Guid> companyGUIDs);
        Branch GetDefaultBranchOfCompany(Guid companyGUID);
        List<Branch> GetBranchByCompanyIdNoTracking(string companyId);
        Branch GetBranchByCompanyGUIDNoTracking(Guid companyGUID);
    }
	public class BranchRepo : CompanyBaseRepository<Branch>, IBranchRepo
	{
		public BranchRepo(SmartAppDbContext context) : base(context) { }
		public BranchRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public Branch GetBranchByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BranchGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #region DropDown
        private IQueryable<BranchItemViewMap> GetDropDownQuery()
        {
            return (from branch in Entity
                    select new BranchItemViewMap
                    {
                        CompanyGUID = branch.CompanyGUID,
                        Owner = branch.Owner,
                        OwnerBusinessUnitGUID = branch.OwnerBusinessUnitGUID,
                        BranchGUID = branch.BranchGUID,
                        BranchId = branch.BranchId,
                        Name = branch.Name
                    });
        }
        public IEnumerable<SelectItem<BranchItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<Branch>(search, SysParm.CompanyGUID);
                var branch = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<BranchItemViewMap, BranchItemView>().ToDropDownItem(search.ExcludeRowData);


                return branch;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<BranchItemView>> GetDropDownItemNotFiltered()
        {
            try
            {
                var branch = GetDropDownQuery().ToMaps<BranchItemViewMap, BranchItemView>().ToDropDownItem();
                return branch;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #region GetListvw
        private IQueryable<BranchListViewMap> GetListQuery()
        {
            return (from branch in Entity
                    select new BranchListViewMap
                    {
                        CompanyGUID = branch.CompanyGUID,
                        Owner = branch.Owner,
                        OwnerBusinessUnitGUID = branch.OwnerBusinessUnitGUID,
                        BranchGUID = branch.BranchGUID,
                        BranchId = branch.BranchId,
                        Name = branch.Name,
                        TaxBranchGUID = branch.TaxBranchGUID
                    });
        }
        public SearchResult<BranchListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<BranchListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<Branch>(search, SysParm.CompanyGUID);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<BranchListViewMap, BranchListView>();
                result = list.SetSearchResult<BranchListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<BranchItemViewMap> GetItemQuery()
        {
            return (from branch in Entity
                    join ownerBU in db.Set<BusinessUnit>()
                    on branch.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljBranchOwnerBU
                    from ownerBU in ljBranchOwnerBU.DefaultIfEmpty()
                    select new BranchItemViewMap
                    {
                        CompanyGUID = branch.CompanyGUID,
                        Owner = branch.Owner,
                        OwnerBusinessUnitGUID = branch.OwnerBusinessUnitGUID,
                        CreatedBy = branch.CreatedBy,
                        CreatedDateTime = branch.CreatedDateTime,
                        ModifiedBy = branch.ModifiedBy,
                        ModifiedDateTime = branch.ModifiedDateTime,
                        BranchGUID = branch.BranchGUID,
                        BranchId = branch.BranchId,
                        Name = branch.Name,
                        TaxBranchGUID = branch.TaxBranchGUID,
                        TaxBranch_Values = SmartAppUtil.GetDropDownLabel(branch.BranchId, branch.Name),
                    
                        RowVersion = branch.RowVersion,
                    });
        }
        public BranchItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .ToMap<BranchItemViewMap, BranchItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public Branch CreateBranch(Branch branch)
		{
			try
			{
				branch.BranchGUID = Guid.NewGuid();
				base.Add(branch);
				return branch;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateBranchVoid(Branch branch)
		{
			try
			{
				CreateBranch(branch);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public Branch UpdateBranch(Branch dbBranch, Branch inputBranch, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBranch = dbBranch.MapUpdateValues<Branch>(inputBranch);
				base.Update(dbBranch);
				return dbBranch;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateBranchVoid(Branch dbBranch, Branch inputBranch, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBranch = dbBranch.MapUpdateValues<Branch>(inputBranch, skipUpdateFields);
				base.Update(dbBranch);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
        public List<Branch> GetFirstBranchOfCompanyNoTracking(List<Guid> companyGUIDs)
        {
            try
            {
                var result = Entity.Where(w => companyGUIDs.Contains(w.CompanyGUID))
                            .AsNoTracking()
                            .ToList()
                            .GroupBy(g => g.CompanyGUID)
                            .Select(s => s.FirstOrDefault())
                            .ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public Branch GetDefaultBranchOfCompany(Guid companyGUID)
        {
            try
            {
                var result =
                    (from company in db.Set<Company>().Where(w => w.CompanyGUID == companyGUID && w.DefaultBranchGUID.HasValue)
                     join branch in Entity.Where(w => w.CompanyGUID == companyGUID)
                     on company.DefaultBranchGUID.Value equals branch.BranchGUID
                     select branch)
                     .AsNoTracking()
                     .FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<Branch> GetBranchByCompanyIdNoTracking(string companyId)
        {
            try
            {
                var result = Entity.Where(w => w.CompanyId == companyId)
                                .AsNoTracking()
                                .ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public Branch GetBranchByCompanyGUIDNoTracking(Guid companyGUID)
        {
            try
            {
                Branch branch = Entity.Where(w => w.CompanyGUID == companyGUID).AsNoTracking().FirstOrDefault();
                return branch;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}

