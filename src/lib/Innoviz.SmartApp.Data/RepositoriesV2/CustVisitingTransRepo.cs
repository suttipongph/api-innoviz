using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface ICustVisitingTransRepo
    {
        #region DropDown
        IEnumerable<SelectItem<CustVisitingTransItemView>> GetDropDownItem(SearchParameter search);
        #endregion DropDown
        SearchResult<CustVisitingTransListView> GetListvw(SearchParameter search);
        CustVisitingTransItemView GetByIdvw(Guid id);
        CustVisitingTrans Find(params object[] keyValues);
        CustVisitingTrans GetCustVisitingTransByIdNoTracking(Guid guid);
        CustVisitingTrans GetCustVisitingTransByIdNoTrackingByAccessLevel(Guid guid);
        CustVisitingTrans CreateCustVisitingTrans(CustVisitingTrans custVisitingTrans);
        void CreateCustVisitingTransVoid(CustVisitingTrans custVisitingTrans);
        CustVisitingTrans UpdateCustVisitingTrans(CustVisitingTrans dbCustVisitingTrans, CustVisitingTrans inputCustVisitingTrans, List<string> skipUpdateFields = null);
        void UpdateCustVisitingTransVoid(CustVisitingTrans dbCustVisitingTrans, CustVisitingTrans inputCustVisitingTrans, List<string> skipUpdateFields = null);
        void Remove(CustVisitingTrans item);
        void ValidateAdd(CustVisitingTrans item);
        void ValidateAdd(IEnumerable<CustVisitingTrans> items);
        #region function
        IEnumerable<CustVisitingTrans> GetCopyCustVisitingByRefTypes(Guid refGUID, int refType);
        CopyCustVisitingView GetCopyCustVisitingById(Guid guid);
        #endregion

    }
    public class CustVisitingTransRepo : CompanyBaseRepository<CustVisitingTrans>, ICustVisitingTransRepo
    {
        public CustVisitingTransRepo(SmartAppDbContext context) : base(context) { }
        public CustVisitingTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public CustVisitingTrans GetCustVisitingTransByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.CustVisitingTransGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CustVisitingTrans GetCustVisitingTransByIdNoTrackingByAccessLevel(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.CustVisitingTransGUID == guid)
                                    .FilterByAccessLevel(SysParm.AccessLevel)
                                    .AsNoTracking()
                                    .FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        private IQueryable<CustVisitingTransItemViewMap> GetDropDownQuery()
        {
            return (from custVisitingTrans in Entity
                    join customerTable in db.Set<CustomerTable>()
                    on custVisitingTrans.RefGUID equals customerTable.CustomerTableGUID into ljCustomerTable
                    from customerTable in ljCustomerTable.DefaultIfEmpty()
                    join creditAppRequestTable in db.Set<CreditAppRequestTable>()
                    on custVisitingTrans.RefGUID equals creditAppRequestTable.CreditAppRequestTableGUID into ljCreditAppRequestTable
                    from creditAppRequestTable in ljCreditAppRequestTable.DefaultIfEmpty()
                    select new CustVisitingTransItemViewMap
                    {
                        CompanyGUID = custVisitingTrans.CompanyGUID,
                        Owner = custVisitingTrans.Owner,
                        OwnerBusinessUnitGUID = custVisitingTrans.OwnerBusinessUnitGUID,
                        CustVisitingTransGUID = custVisitingTrans.CustVisitingTransGUID,
                        CustVisitingDate = custVisitingTrans.CustVisitingDate,
                        Description = custVisitingTrans.Description,
                        RefId = (customerTable != null && custVisitingTrans.RefType == (int)RefType.Customer) ? customerTable.CustomerId :
                                (creditAppRequestTable != null && custVisitingTrans.RefType == (int)RefType.CreditAppRequestTable) ? creditAppRequestTable.CreditAppRequestId : null,
                        RefGUID = custVisitingTrans.RefGUID
                    });
        }
        public IEnumerable<SelectItem<CustVisitingTransItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<CustVisitingTrans>(search, SysParm.CompanyGUID);
                var custVisitingTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CustVisitingTransItemViewMap, CustVisitingTransItemView>().ToDropDownItem(search.ExcludeRowData);


                return custVisitingTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #region GetListvw
        private IQueryable<CustVisitingTransListViewMap> GetListQuery()
        {
            return (from custVisitingTrans in Entity
                    join employeeTable in db.Set<EmployeeTable>()
                    on custVisitingTrans.ResponsibleByGUID equals employeeTable.EmployeeTableGUID into ljEmployeeTable
                    from employeeTable in ljEmployeeTable.DefaultIfEmpty()
                    select new CustVisitingTransListViewMap
                    {
                        CompanyGUID = custVisitingTrans.CompanyGUID,
                        Owner = custVisitingTrans.Owner,
                        OwnerBusinessUnitGUID = custVisitingTrans.OwnerBusinessUnitGUID,
                        CustVisitingTransGUID = custVisitingTrans.CustVisitingTransGUID,
                        CustVisitingDate = custVisitingTrans.CustVisitingDate,
                        ResponsibleByGUID = custVisitingTrans.ResponsibleByGUID,
                        Description = custVisitingTrans.Description,
                        RefGUID = custVisitingTrans.RefGUID,
                        EmployeeTable_EmployeeId = employeeTable.EmployeeId,
                        EmployeeTable_Values = SmartAppUtil.GetDropDownLabel(employeeTable.EmployeeId, employeeTable.Name)
                    });
        }
        public SearchResult<CustVisitingTransListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<CustVisitingTransListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<CustVisitingTrans>(search, SysParm.CompanyGUID);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<CustVisitingTransListViewMap, CustVisitingTransListView>();
                result = list.SetSearchResult<CustVisitingTransListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<CustVisitingTransItemViewMap> GetItemQuery()
        {
            return (from custVisitingTrans in Entity
                    join company in db.Set<Company>()
                    on custVisitingTrans.CompanyGUID equals company.CompanyGUID
                    join ownerBU in db.Set<BusinessUnit>()
                    on custVisitingTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCustVisitingTransOwnerBU
                    from ownerBU in ljCustVisitingTransOwnerBU.DefaultIfEmpty()
                    join customerTable in db.Set<CustomerTable>()
                    on custVisitingTrans.RefGUID equals customerTable.CustomerTableGUID into ljCustomerTable
                    from customerTable in ljCustomerTable.DefaultIfEmpty()
                    join creditAppRequestTable in db.Set<CreditAppRequestTable>()
                    on custVisitingTrans.RefGUID equals creditAppRequestTable.CreditAppRequestTableGUID into ljCreditAppRequestTable
                    from creditAppRequestTable in ljCreditAppRequestTable.DefaultIfEmpty()
                    select new CustVisitingTransItemViewMap
                    {
                        CompanyGUID = custVisitingTrans.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = custVisitingTrans.Owner,
                        OwnerBusinessUnitGUID = custVisitingTrans.OwnerBusinessUnitGUID,
                        CreatedBy = custVisitingTrans.CreatedBy,
                        CreatedDateTime = custVisitingTrans.CreatedDateTime,
                        ModifiedBy = custVisitingTrans.ModifiedBy,
                        ModifiedDateTime = custVisitingTrans.ModifiedDateTime,
                        CustVisitingTransGUID = custVisitingTrans.CustVisitingTransGUID,
                        CustVisitingDate = custVisitingTrans.CustVisitingDate,
                        CustVisitingMemo = custVisitingTrans.CustVisitingMemo,
                        Description = custVisitingTrans.Description,
                        RefGUID = custVisitingTrans.RefGUID,
                        RefType = custVisitingTrans.RefType,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        ResponsibleByGUID = custVisitingTrans.ResponsibleByGUID,
                        RefId = (customerTable != null && custVisitingTrans.RefType == (int)RefType.Customer) ? customerTable.CustomerId :
                                (creditAppRequestTable != null && custVisitingTrans.RefType == (int)RefType.CreditAppRequestTable) ? creditAppRequestTable.CreditAppRequestId : null,
                    
                        RowVersion = custVisitingTrans.RowVersion,
                    });
        }
        public CustVisitingTransItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<CustVisitingTransItemViewMap, CustVisitingTransItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public CustVisitingTrans CreateCustVisitingTrans(CustVisitingTrans custVisitingTrans)
        {
            try
            {
                custVisitingTrans.CustVisitingTransGUID = Guid.NewGuid();
                base.Add(custVisitingTrans);
                return custVisitingTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateCustVisitingTransVoid(CustVisitingTrans custVisitingTrans)
        {
            try
            {
                CreateCustVisitingTrans(custVisitingTrans);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CustVisitingTrans UpdateCustVisitingTrans(CustVisitingTrans dbCustVisitingTrans, CustVisitingTrans inputCustVisitingTrans, List<string> skipUpdateFields = null)
        {
            try
            {
                dbCustVisitingTrans = dbCustVisitingTrans.MapUpdateValues<CustVisitingTrans>(inputCustVisitingTrans);
                base.Update(dbCustVisitingTrans);
                return dbCustVisitingTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateCustVisitingTransVoid(CustVisitingTrans dbCustVisitingTrans, CustVisitingTrans inputCustVisitingTrans, List<string> skipUpdateFields = null)
        {
            try
            {
                dbCustVisitingTrans = dbCustVisitingTrans.MapUpdateValues<CustVisitingTrans>(inputCustVisitingTrans, skipUpdateFields);
                base.Update(dbCustVisitingTrans);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Create, Update, Delete

        #region function
        public IEnumerable<CustVisitingTrans> GetCopyCustVisitingByRefTypes(Guid refGUID, int refType)
        {
            try
            {
                IEnumerable<CustVisitingTrans> list = Entity.Where(w => w.RefGUID == refGUID && w.RefType == refType)
                    .AsNoTracking().ToList();
                return list;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CopyCustVisitingView GetCopyCustVisitingById(Guid guid)
        {
            try
            {
                var result = (from creditAppReq in db.Set<CreditAppRequestTable>()
                              join custVisiting in Entity
                              on creditAppReq.CustomerTableGUID equals custVisiting.RefGUID into ljCustVisiting
                              from custVisiting in ljCustVisiting.DefaultIfEmpty()
                              where custVisiting.RefType == (int)RefType.Customer && creditAppReq.CreditAppRequestTableGUID == guid
                              select new CopyCustVisitingView
                              {
                                  CustomerTableGUID = creditAppReq.CustomerTableGUID.ToString()
                              }).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }
}
