using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IGuarantorTypeRepo
	{
		#region DropDown
		IEnumerable<SelectItem<GuarantorTypeItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<GuarantorTypeListView> GetListvw(SearchParameter search);
		GuarantorTypeItemView GetByIdvw(Guid id);
		GuarantorType Find(params object[] keyValues);
		GuarantorType GetGuarantorTypeByIdNoTracking(Guid guid);
		GuarantorType CreateGuarantorType(GuarantorType guarantorType);
		void CreateGuarantorTypeVoid(GuarantorType guarantorType);
		GuarantorType UpdateGuarantorType(GuarantorType dbGuarantorType, GuarantorType inputGuarantorType, List<string> skipUpdateFields = null);
		void UpdateGuarantorTypeVoid(GuarantorType dbGuarantorType, GuarantorType inputGuarantorType, List<string> skipUpdateFields = null);
		void Remove(GuarantorType item);
	}
	public class GuarantorTypeRepo : CompanyBaseRepository<GuarantorType>, IGuarantorTypeRepo
	{
		public GuarantorTypeRepo(SmartAppDbContext context) : base(context) { }
		public GuarantorTypeRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public GuarantorType GetGuarantorTypeByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.GuarantorTypeGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<GuarantorTypeItemViewMap> GetDropDownQuery()
		{
			return (from guarantorType in Entity
					select new GuarantorTypeItemViewMap
					{
						CompanyGUID = guarantorType.CompanyGUID,
						Owner = guarantorType.Owner,
						OwnerBusinessUnitGUID = guarantorType.OwnerBusinessUnitGUID,
						GuarantorTypeGUID = guarantorType.GuarantorTypeGUID,
						GuarantorTypeId = guarantorType.GuarantorTypeId,
						Description = guarantorType.Description
					});
		}
		public IEnumerable<SelectItem<GuarantorTypeItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<GuarantorType>(search, SysParm.CompanyGUID);
				var guarantorType = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<GuarantorTypeItemViewMap, GuarantorTypeItemView>().ToDropDownItem(search.ExcludeRowData);


				return guarantorType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<GuarantorTypeListViewMap> GetListQuery()
		{
			return (from guarantorType in Entity
				select new GuarantorTypeListViewMap
				{
						CompanyGUID = guarantorType.CompanyGUID,
						Owner = guarantorType.Owner,
						OwnerBusinessUnitGUID = guarantorType.OwnerBusinessUnitGUID,
						GuarantorTypeGUID = guarantorType.GuarantorTypeGUID,
						GuarantorTypeId = guarantorType.GuarantorTypeId,
						Description = guarantorType.Description,
				});
		}
		public SearchResult<GuarantorTypeListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<GuarantorTypeListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<GuarantorType>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<GuarantorTypeListViewMap, GuarantorTypeListView>();
				result = list.SetSearchResult<GuarantorTypeListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<GuarantorTypeItemViewMap> GetItemQuery()
		{
			return (from guarantorType in Entity
					join company in db.Set<Company>()
					on guarantorType.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on guarantorType.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljGuarantorTypeOwnerBU
					from ownerBU in ljGuarantorTypeOwnerBU.DefaultIfEmpty()
					select new GuarantorTypeItemViewMap
					{
						CompanyGUID = guarantorType.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = guarantorType.Owner,
						OwnerBusinessUnitGUID = guarantorType.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = guarantorType.CreatedBy,
						CreatedDateTime = guarantorType.CreatedDateTime,
						ModifiedBy = guarantorType.ModifiedBy,
						ModifiedDateTime = guarantorType.ModifiedDateTime,
						GuarantorTypeGUID = guarantorType.GuarantorTypeGUID,
						Description = guarantorType.Description,
						GuarantorTypeId = guarantorType.GuarantorTypeId,
					
						RowVersion = guarantorType.RowVersion,
					});
		}
		public GuarantorTypeItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<GuarantorTypeItemViewMap, GuarantorTypeItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public GuarantorType CreateGuarantorType(GuarantorType guarantorType)
		{
			try
			{
				guarantorType.GuarantorTypeGUID = Guid.NewGuid();
				base.Add(guarantorType);
				return guarantorType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateGuarantorTypeVoid(GuarantorType guarantorType)
		{
			try
			{
				CreateGuarantorType(guarantorType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public GuarantorType UpdateGuarantorType(GuarantorType dbGuarantorType, GuarantorType inputGuarantorType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbGuarantorType = dbGuarantorType.MapUpdateValues<GuarantorType>(inputGuarantorType);
				base.Update(dbGuarantorType);
				return dbGuarantorType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateGuarantorTypeVoid(GuarantorType dbGuarantorType, GuarantorType inputGuarantorType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbGuarantorType = dbGuarantorType.MapUpdateValues<GuarantorType>(inputGuarantorType, skipUpdateFields);
				base.Update(dbGuarantorType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

