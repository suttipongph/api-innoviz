using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface IMemoTransRepo
    {
        #region DropDown
        IEnumerable<SelectItem<MemoTransItemView>> GetDropDownItem(SearchParameter search);
        #endregion DropDown
        SearchResult<MemoTransListView> GetListvw(SearchParameter search);
        MemoTransItemView GetByIdvw(Guid id);
        MemoTrans Find(params object[] keyValues);
        MemoTrans GetMemoTransByIdNoTracking(Guid guid);
        MemoTrans CreateMemoTrans(MemoTrans memoTrans);
        void CreateMemoTransVoid(MemoTrans memoTrans);
        MemoTrans UpdateMemoTrans(MemoTrans dbMemoTrans, MemoTrans inputMemoTrans, List<string> skipUpdateFields = null);
        void UpdateMemoTransVoid(MemoTrans dbMemoTrans, MemoTrans inputMemoTrans, List<string> skipUpdateFields = null);
        void Remove(MemoTrans item);
    }
    public class MemoTransRepo : CompanyBaseRepository<MemoTrans>, IMemoTransRepo
    {
        public MemoTransRepo(SmartAppDbContext context) : base(context) { }
        public MemoTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public MemoTrans GetMemoTransByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.MemoTransGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        private IQueryable<MemoTransItemViewMap> GetDropDownQuery()
        {
            return (from memoTrans in Entity
                    select new MemoTransItemViewMap
                    {
                        CompanyGUID = memoTrans.CompanyGUID,
                        Owner = memoTrans.Owner,
                        OwnerBusinessUnitGUID = memoTrans.OwnerBusinessUnitGUID,
                        MemoTransGUID = memoTrans.MemoTransGUID,
                        Topic = memoTrans.Topic,
                        Memo = memoTrans.Memo
                    });
        }
        public IEnumerable<SelectItem<MemoTransItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<MemoTrans>(search, SysParm.CompanyGUID);
                var memoTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<MemoTransItemViewMap, MemoTransItemView>().ToDropDownItem(search.ExcludeRowData);


                return memoTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #region GetListvw
        private IQueryable<MemoTransListViewMap> GetListQuery()
        {
            var result = (from memoTrans in Entity
                    select new MemoTransListViewMap
                    {
                        CompanyGUID = memoTrans.CompanyGUID,
                        Owner = memoTrans.Owner,
                        OwnerBusinessUnitGUID = memoTrans.OwnerBusinessUnitGUID,
                        MemoTransGUID = memoTrans.MemoTransGUID,
                        Topic = memoTrans.Topic,
                        Memo = memoTrans.Memo,
                        RefGUID = memoTrans.RefGUID
                    });
            return result;
        }
        public SearchResult<MemoTransListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<MemoTransListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<MemoTrans>(search, SysParm.CompanyGUID);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<MemoTransListViewMap, MemoTransListView>();
                result = list.SetSearchResult<MemoTransListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<MemoTransItemViewMap> GetItemQuery()
        {
            return (from memoTrans in Entity
                    join company in db.Set<Company>()
                    on memoTrans.CompanyGUID equals company.CompanyGUID
                    join ownerBU in db.Set<BusinessUnit>()
                    on memoTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljMemoTransOwnerBU
                    from ownerBU in ljMemoTransOwnerBU.DefaultIfEmpty()
                    join buyer in db.Set<BuyerTable>() on memoTrans.RefGUID equals buyer.BuyerTableGUID into ljMemoTransBuyer
                    from buyer in ljMemoTransBuyer.DefaultIfEmpty()
                    join customer in db.Set<CustomerTable>() on memoTrans.RefGUID equals customer.CustomerTableGUID into ljMemoTransCustomer
                    from customer in ljMemoTransCustomer.DefaultIfEmpty()
                    join relatedPerson in db.Set<RelatedPersonTable>() on memoTrans.RefGUID equals relatedPerson.RelatedPersonTableGUID into ljMemoTransRelatedPerson
                    from relatedPerson in ljMemoTransRelatedPerson.DefaultIfEmpty()
                    join creditAppRequestTable in db.Set<CreditAppRequestTable>() on memoTrans.RefGUID equals creditAppRequestTable.CreditAppRequestTableGUID into ljMemoTransCreditAppRequestTable
                    from creditAppRequestTable in ljMemoTransCreditAppRequestTable.DefaultIfEmpty()
                    join creditAppRequestLine in db.Set<CreditAppRequestLine>() on memoTrans.RefGUID equals creditAppRequestLine.CreditAppRequestLineGUID into ljMemoTransCreditAppRequestLine
                    from creditAppRequestLine in ljMemoTransCreditAppRequestLine.DefaultIfEmpty()
                    join mainAgreementTable in db.Set<MainAgreementTable>() on memoTrans.RefGUID equals mainAgreementTable.MainAgreementTableGUID into ljMemoTransMainAgreementTable
                    from mainAgreementTable in ljMemoTransMainAgreementTable.DefaultIfEmpty()
                    join verification in db.Set<VerificationTable>() on memoTrans.RefGUID equals verification.VerificationTableGUID into ljverification
                    from verification in ljverification.DefaultIfEmpty()
                    join businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>() on memoTrans.RefGUID equals businessCollateralAgmTable.BusinessCollateralAgmTableGUID into ljBusinessCollateralAgmTable
                    from businessCollateralAgmTable in ljBusinessCollateralAgmTable.DefaultIfEmpty()

                    join assignmentAgreementTable in db.Set<AssignmentAgreementTable>() on memoTrans.RefGUID equals assignmentAgreementTable.AssignmentAgreementTableGUID into ljMemoTransAssignmentAgreementTable
                    from assignmentAgreementTable in ljMemoTransAssignmentAgreementTable.DefaultIfEmpty()

                    join collectionfollowup in db.Set<CollectionFollowUp>()
                    on memoTrans.RefGUID equals collectionfollowup.CollectionFollowUpGUID into ljcollecttionfollowup
                    from collectionfollowup in ljcollecttionfollowup.DefaultIfEmpty()

                    join purchaseTable in db.Set<PurchaseTable>()
                    on memoTrans.RefGUID equals purchaseTable.PurchaseTableGUID into ljpurchaseTable
                    from purchaseTable in ljpurchaseTable.DefaultIfEmpty()

                    join withdrawalTable in db.Set<WithdrawalTable>()
                    on memoTrans.RefGUID equals withdrawalTable.WithdrawalTableGUID into ljwithdarwal
                    from withdrawalTable in ljwithdarwal.DefaultIfEmpty()

                    join creditappreqLine in db.Set<CreditAppRequestLine>()
                    on memoTrans.RefGUID equals creditappreqLine.CreditAppRequestLineGUID into ljcreditapplinereq
                    from creditappreqLine in ljcreditapplinereq.DefaultIfEmpty()

                    join projectprogress in db.Set<ProjectProgressTable>()
                    on memoTrans.RefGUID equals projectprogress.ProjectProgressTableGUID into ljprojectprogress
                    from projectprogress in ljprojectprogress.DefaultIfEmpty()

                    join receiptTempTable in db.Set<ReceiptTempTable>()
                    on memoTrans.RefGUID equals receiptTempTable.ReceiptTempTableGUID into ljreceiptTempTable
                    from receiptTempTable in ljreceiptTempTable.DefaultIfEmpty()

                    join guarantorAgreementTable in db.Set<GuarantorAgreementTable>()
                    on memoTrans.RefGUID equals guarantorAgreementTable.GuarantorAgreementTableGUID into ljguarantorAgreementTable
                    from guarantorAgreementTable in ljguarantorAgreementTable.DefaultIfEmpty()
                    select new MemoTransItemViewMap
                    {
                        CompanyGUID = memoTrans.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = memoTrans.Owner,
                        OwnerBusinessUnitGUID = memoTrans.OwnerBusinessUnitGUID,
                        CreatedBy = memoTrans.CreatedBy,
                        CreatedDateTime = memoTrans.CreatedDateTime,
                        ModifiedBy = memoTrans.ModifiedBy,
                        ModifiedDateTime = memoTrans.ModifiedDateTime,
                        MemoTransGUID = memoTrans.MemoTransGUID,
                        Memo = memoTrans.Memo,
                        RefGUID = memoTrans.RefGUID,
                        RefId = (buyer != null && memoTrans.RefType == (int)RefType.Buyer) ? buyer.BuyerId :
                                (customer != null && memoTrans.RefType == (int)RefType.Customer) ? customer.CustomerId :
                                (relatedPerson != null && memoTrans.RefType == (int)RefType.RelatedPerson) ? relatedPerson.RelatedPersonId :
                                (creditAppRequestTable != null && memoTrans.RefType == (int)RefType.CreditAppRequestTable) ? creditAppRequestTable.CreditAppRequestId :
                                (creditAppRequestLine != null && memoTrans.RefType == (int)RefType.CreditAppRequestLine) ? creditAppRequestLine.LineNum.ToString() :
                                (mainAgreementTable != null && memoTrans.RefType == (int)RefType.MainAgreement) ? mainAgreementTable.InternalMainAgreementId :
                                (verification != null && memoTrans.RefType == (int)RefType.Verification) ? verification.VerificationId :
                                (creditAppRequestLine != null && memoTrans.RefType == (int)RefType.CreditAppRequestLine) ? creditAppRequestLine.LineNum.ToString() :
                                (businessCollateralAgmTable != null && memoTrans.RefType == (int)RefType.BusinessCollateralAgreement) ? businessCollateralAgmTable.InternalBusinessCollateralAgmId :
                                (assignmentAgreementTable != null && memoTrans.RefType == (int)RefType.AssignmentAgreement) ? assignmentAgreementTable.InternalAssignmentAgreementId :
                                (purchaseTable !=null &&memoTrans.RefType==(int)RefType.PurchaseTable)?purchaseTable.PurchaseId:
                                (collectionfollowup !=null &&memoTrans.RefType==(int)RefType.CollectionFollowUp)?SmartAppUtil.GetDropDownLabel(collectionfollowup.CollectionDate.DateToString(), collectionfollowup.Description) :
                                (withdrawalTable !=null &&memoTrans.RefType==(int)RefType.WithdrawalTable)?withdrawalTable.WithdrawalId:
                                (creditappreqLine !=null &&memoTrans.RefType==(int)RefType.CreditAppRequestLine)?creditappreqLine.LineNum.ToString():
                                (projectprogress !=null &&memoTrans.RefType==(int)RefType.ProjectProgress)?projectprogress.ProjectProgressId:
                                (receiptTempTable!=null &&memoTrans.RefType==(int)RefType.ReceiptTemp)?receiptTempTable.ReceiptTempId:
                                (guarantorAgreementTable !=null &&memoTrans.RefType==(int)RefType.GuarantorAgreement)?guarantorAgreementTable.InternalGuarantorAgreementId:
                                null,
                        RefType = memoTrans.RefType,
                        Topic = memoTrans.Topic,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                    
                        RowVersion = memoTrans.RowVersion,
                    });
        }
        public MemoTransItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<MemoTransItemViewMap, MemoTransItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public MemoTrans CreateMemoTrans(MemoTrans memoTrans)
        {
            try
            {
                memoTrans.MemoTransGUID = Guid.NewGuid();
                base.Add(memoTrans);
                return memoTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateMemoTransVoid(MemoTrans memoTrans)
        {
            try
            {
                CreateMemoTrans(memoTrans);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public MemoTrans UpdateMemoTrans(MemoTrans dbMemoTrans, MemoTrans inputMemoTrans, List<string> skipUpdateFields = null)
        {
            try
            {
                dbMemoTrans = dbMemoTrans.MapUpdateValues<MemoTrans>(inputMemoTrans);
                base.Update(dbMemoTrans);
                return dbMemoTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateMemoTransVoid(MemoTrans dbMemoTrans, MemoTrans inputMemoTrans, List<string> skipUpdateFields = null)
        {
            try
            {
                dbMemoTrans = dbMemoTrans.MapUpdateValues<MemoTrans>(inputMemoTrans, skipUpdateFields);
                base.Update(dbMemoTrans);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Create, Update, Delete

    }
}

