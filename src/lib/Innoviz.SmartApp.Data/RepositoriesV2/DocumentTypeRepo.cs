using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IDocumentTypeRepo
	{
		#region DropDown
		IEnumerable<SelectItem<DocumentTypeItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<DocumentTypeListView> GetListvw(SearchParameter search);
		DocumentTypeItemView GetByIdvw(Guid id);
		DocumentType Find(params object[] keyValues);
		DocumentType GetDocumentTypeByIdNoTracking(Guid guid);
		DocumentType CreateDocumentType(DocumentType documentType);
		void CreateDocumentTypeVoid(DocumentType documentType);
		DocumentType UpdateDocumentType(DocumentType dbDocumentType, DocumentType inputDocumentType, List<string> skipUpdateFields = null);
		void UpdateDocumentTypeVoid(DocumentType dbDocumentType, DocumentType inputDocumentType, List<string> skipUpdateFields = null);
		void Remove(DocumentType item);
	}
	public class DocumentTypeRepo : CompanyBaseRepository<DocumentType>, IDocumentTypeRepo
	{
		public DocumentTypeRepo(SmartAppDbContext context) : base(context) { }
		public DocumentTypeRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public DocumentType GetDocumentTypeByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.DocumentTypeGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<DocumentTypeItemViewMap> GetDropDownQuery()
		{
			return (from documentType in Entity
					select new DocumentTypeItemViewMap
					{
						CompanyGUID = documentType.CompanyGUID,
						Owner = documentType.Owner,
						OwnerBusinessUnitGUID = documentType.OwnerBusinessUnitGUID,
						DocumentTypeGUID = documentType.DocumentTypeGUID,
						DocumentTypeId = documentType.DocumentTypeId,
						Description = documentType.Description
					});
		}
		public IEnumerable<SelectItem<DocumentTypeItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<DocumentType>(search, SysParm.CompanyGUID);
				var documentType = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<DocumentTypeItemViewMap, DocumentTypeItemView>().ToDropDownItem(search.ExcludeRowData);


				return documentType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<DocumentTypeListViewMap> GetListQuery()
		{
			return (from documentType in Entity
				select new DocumentTypeListViewMap
				{
						CompanyGUID = documentType.CompanyGUID,
						Owner = documentType.Owner,
						OwnerBusinessUnitGUID = documentType.OwnerBusinessUnitGUID,
						DocumentTypeGUID = documentType.DocumentTypeGUID,
						DocumentTypeId = documentType.DocumentTypeId,
						Description = documentType.Description,
				});
		}
		public SearchResult<DocumentTypeListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<DocumentTypeListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<DocumentType>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<DocumentTypeListViewMap, DocumentTypeListView>();
				result = list.SetSearchResult<DocumentTypeListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<DocumentTypeItemViewMap> GetItemQuery()
		{
			return (from documentType in Entity
					join company in db.Set<Company>()
					on documentType.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on documentType.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljDocumentTypeOwnerBU
					from ownerBU in ljDocumentTypeOwnerBU.DefaultIfEmpty()
					select new DocumentTypeItemViewMap
					{
						CompanyGUID = documentType.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = documentType.Owner,
						OwnerBusinessUnitGUID = documentType.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = documentType.CreatedBy,
						CreatedDateTime = documentType.CreatedDateTime,
						ModifiedBy = documentType.ModifiedBy,
						ModifiedDateTime = documentType.ModifiedDateTime,
						DocumentTypeGUID = documentType.DocumentTypeGUID,
						Description = documentType.Description,
						DocumentTypeId = documentType.DocumentTypeId,
					
						RowVersion = documentType.RowVersion,
					});
		}
		public DocumentTypeItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<DocumentTypeItemViewMap, DocumentTypeItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public DocumentType CreateDocumentType(DocumentType documentType)
		{
			try
			{
				documentType.DocumentTypeGUID = Guid.NewGuid();
				base.Add(documentType);
				return documentType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateDocumentTypeVoid(DocumentType documentType)
		{
			try
			{
				CreateDocumentType(documentType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentType UpdateDocumentType(DocumentType dbDocumentType, DocumentType inputDocumentType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbDocumentType = dbDocumentType.MapUpdateValues<DocumentType>(inputDocumentType);
				base.Update(dbDocumentType);
				return dbDocumentType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateDocumentTypeVoid(DocumentType dbDocumentType, DocumentType inputDocumentType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbDocumentType = dbDocumentType.MapUpdateValues<DocumentType>(inputDocumentType, skipUpdateFields);
				base.Update(dbDocumentType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

