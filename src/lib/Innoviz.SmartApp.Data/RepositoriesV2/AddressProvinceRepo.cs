using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IAddressProvinceRepo
	{
		#region DropDown
		IEnumerable<SelectItem<AddressProvinceItemView>> GetDropDownItem(SearchParameter search);
		IEnumerable<SelectItem<AddressProvinceItemView>> GetDropDownItemByCountry(SearchParameter search);
		#endregion DropDown
		SearchResult<AddressProvinceListView> GetListvw(SearchParameter search);
		AddressProvinceItemView GetByIdvw(Guid id);
		AddressProvince Find(params object[] keyValues);
		AddressProvince GetAddressProvinceByIdNoTracking(Guid guid);
		AddressProvince CreateAddressProvince(AddressProvince addressProvince);
		void CreateAddressProvinceVoid(AddressProvince addressProvince);
		AddressProvince UpdateAddressProvince(AddressProvince dbAddressProvince, AddressProvince inputAddressProvince, List<string> skipUpdateFields = null);
		void UpdateAddressProvinceVoid(AddressProvince dbAddressProvince, AddressProvince inputAddressProvince, List<string> skipUpdateFields = null);
		void Remove(AddressProvince item);
		List<AddressProvince> GetAddressProvinceByCompanyNoTracking(Guid companyGUID);
	}
	public class AddressProvinceRepo : CompanyBaseRepository<AddressProvince>, IAddressProvinceRepo
	{
		public AddressProvinceRepo(SmartAppDbContext context) : base(context) { }
		public AddressProvinceRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public AddressProvince GetAddressProvinceByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.AddressProvinceGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<AddressProvinceItemViewMap> GetDropDownQuery()
		{
			return  (from addressProvince in Entity
					select new AddressProvinceItemViewMap
					{
						CompanyGUID = addressProvince.CompanyGUID,
						Owner = addressProvince.Owner,
						OwnerBusinessUnitGUID = addressProvince.OwnerBusinessUnitGUID,
						AddressProvinceGUID = addressProvince.AddressProvinceGUID,
						ProvinceId = addressProvince.ProvinceId,
						Name = addressProvince.Name,
						AddressCountryGUID = addressProvince.AddressCountryGUID
					}) ;
			
		}
        public IEnumerable<SelectItem<AddressProvinceItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<AddressProvince>(search, SysParm.CompanyGUID);
                var addressProvince = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<AddressProvinceItemViewMap, AddressProvinceItemView>().ToDropDownItem(search.ExcludeRowData);


                return addressProvince;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<AddressProvinceItemView>> GetDropDownItemByCountry(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<AddressProvince>(search, SysParm.CompanyGUID);
				var addressProvince = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<AddressProvinceItemViewMap, AddressProvinceItemView>().ToDropDownItem(search.ExcludeRowData);


				return addressProvince;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<AddressProvinceListViewMap> GetListQuery()
		{
			return (from addressProvince in Entity
					join addressCountry in db.Set<AddressCountry>()
					on addressProvince.AddressCountryGUID equals addressCountry.AddressCountryGUID into ljAddressCountry
					from addressCountry in ljAddressCountry.DefaultIfEmpty()
					select new AddressProvinceListViewMap
				{
						CompanyGUID = addressProvince.CompanyGUID,
						Owner = addressProvince.Owner,
						OwnerBusinessUnitGUID = addressProvince.OwnerBusinessUnitGUID,
						AddressProvinceGUID = addressProvince.AddressProvinceGUID,
						AddressCountryGUID = addressCountry.AddressCountryGUID,
						CountryId = addressCountry.CountryId,
						AddressCountry_Name = addressCountry.Name,
						ProvinceId = addressProvince.ProvinceId,
						Name = addressProvince.Name
				});
		}
		public SearchResult<AddressProvinceListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<AddressProvinceListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<AddressProvince>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<AddressProvinceListViewMap, AddressProvinceListView>();
				result = list.SetSearchResult<AddressProvinceListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<AddressProvinceItemViewMap> GetItemQuery()
		{
			return (from addressProvince in Entity
					join company in db.Set<Company>()
					on addressProvince.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on addressProvince.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljAddressProvinceOwnerBU
					from ownerBU in ljAddressProvinceOwnerBU.DefaultIfEmpty()
					join addressCountry in db.Set<AddressCountry>()
					on addressProvince.AddressCountryGUID equals addressCountry.AddressCountryGUID into ljAddressCountry
					from addressCountry in ljAddressCountry.DefaultIfEmpty()
					select new AddressProvinceItemViewMap
					{
						CompanyGUID = addressProvince.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = addressProvince.Owner,
						OwnerBusinessUnitGUID = addressProvince.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = addressProvince.CreatedBy,
						CreatedDateTime = addressProvince.CreatedDateTime,
						ModifiedBy = addressProvince.ModifiedBy,
						ModifiedDateTime = addressProvince.ModifiedDateTime,
						AddressProvinceGUID = addressProvince.AddressProvinceGUID,
						AddressCountryGUID = addressProvince.AddressCountryGUID,
						Name = addressProvince.Name,
						ProvinceId = addressProvince.ProvinceId,
						CountryId = addressCountry.CountryId,
					
						RowVersion = addressProvince.RowVersion,
					});
		}
		public AddressProvinceItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<AddressProvinceItemViewMap, AddressProvinceItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public AddressProvince CreateAddressProvince(AddressProvince addressProvince)
		{
			try
			{
				addressProvince.AddressProvinceGUID = Guid.NewGuid();
				base.Add(addressProvince);
				return addressProvince;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateAddressProvinceVoid(AddressProvince addressProvince)
		{
			try
			{
				CreateAddressProvince(addressProvince);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AddressProvince UpdateAddressProvince(AddressProvince dbAddressProvince, AddressProvince inputAddressProvince, List<string> skipUpdateFields = null)
		{
			try
			{
				dbAddressProvince = dbAddressProvince.MapUpdateValues<AddressProvince>(inputAddressProvince);
				base.Update(dbAddressProvince);
				return dbAddressProvince;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateAddressProvinceVoid(AddressProvince dbAddressProvince, AddressProvince inputAddressProvince, List<string> skipUpdateFields = null)
		{
			try
			{
				dbAddressProvince = dbAddressProvince.MapUpdateValues<AddressProvince>(inputAddressProvince, skipUpdateFields);
				base.Update(dbAddressProvince);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public List<AddressProvince> GetAddressProvinceByCompanyNoTracking(Guid companyGUID)
		{
			try
			{
				List<AddressProvince> addressProvinces = Entity.Where(w => w.CompanyGUID == companyGUID).AsNoTracking().ToList();
				return addressProvinces;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

