using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IIntercompanyInvoiceSettlementRepo
	{
		#region DropDown
		IEnumerable<SelectItem<IntercompanyInvoiceSettlementItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<IntercompanyInvoiceSettlementListView> GetListvw(SearchParameter search);
		IntercompanyInvoiceSettlementItemView GetByIdvw(Guid id);
		IntercompanyInvoiceSettlement Find(params object[] keyValues);
		IntercompanyInvoiceSettlement GetIntercompanyInvoiceSettlementByIdNoTracking(Guid guid);
		IntercompanyInvoiceSettlement CreateIntercompanyInvoiceSettlement(IntercompanyInvoiceSettlement intercompanyInvoiceSettlement);
		void CreateIntercompanyInvoiceSettlementVoid(IntercompanyInvoiceSettlement intercompanyInvoiceSettlement);
		IntercompanyInvoiceSettlement UpdateIntercompanyInvoiceSettlement(IntercompanyInvoiceSettlement dbIntercompanyInvoiceSettlement, IntercompanyInvoiceSettlement inputIntercompanyInvoiceSettlement, List<string> skipUpdateFields = null);
		void UpdateIntercompanyInvoiceSettlementVoid(IntercompanyInvoiceSettlement dbIntercompanyInvoiceSettlement, IntercompanyInvoiceSettlement inputIntercompanyInvoiceSettlement, List<string> skipUpdateFields = null);
		void Remove(IntercompanyInvoiceSettlement item);
		IntercompanyInvoiceSettlement GetIntercompanyInvoiceSettlementByIdNoTrackingByAccessLevel(Guid guid);
		int GetIntercompanyInvoiceBy(Guid intercompanyInvoiceTableGUID, string statusId);
		decimal GetSumSettleInvoiceAmount(Guid guid , Guid documentStatusGUID);


	}
	public class IntercompanyInvoiceSettlementRepo : CompanyBaseRepository<IntercompanyInvoiceSettlement>, IIntercompanyInvoiceSettlementRepo
	{
		public IntercompanyInvoiceSettlementRepo(SmartAppDbContext context) : base(context) { }
		public IntercompanyInvoiceSettlementRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public IntercompanyInvoiceSettlement GetIntercompanyInvoiceSettlementByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.IntercompanyInvoiceSettlementGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<IntercompanyInvoiceSettlementItemViewMap> GetDropDownQuery()
		{
			return (from intercompanyInvoiceSettlement in Entity
					select new IntercompanyInvoiceSettlementItemViewMap
					{
						CompanyGUID = intercompanyInvoiceSettlement.CompanyGUID,
						Owner = intercompanyInvoiceSettlement.Owner,
						OwnerBusinessUnitGUID = intercompanyInvoiceSettlement.OwnerBusinessUnitGUID,
						IntercompanyInvoiceSettlementGUID = intercompanyInvoiceSettlement.IntercompanyInvoiceSettlementGUID
					});
		}
		public IEnumerable<SelectItem<IntercompanyInvoiceSettlementItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<IntercompanyInvoiceSettlement>(search, SysParm.CompanyGUID);
				var intercompanyInvoiceSettlement = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<IntercompanyInvoiceSettlementItemViewMap, IntercompanyInvoiceSettlementItemView>().ToDropDownItem(search.ExcludeRowData);


				return intercompanyInvoiceSettlement;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<IntercompanyInvoiceSettlementListViewMap> GetListQuery()
		{

			return (from intercompanyInvoiceSettlement in Entity
					join documentStatus in db.Set<DocumentStatus>()
					on intercompanyInvoiceSettlement.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljIntercompanyInvoiceSettlementDocumentStatus
					from documentStatus in ljIntercompanyInvoiceSettlementDocumentStatus.DefaultIfEmpty()

					join methodOfPayment in db.Set<MethodOfPayment>()
					on intercompanyInvoiceSettlement.MethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID into ljmethodOfPayment
					from methodOfPayment in ljmethodOfPayment.DefaultIfEmpty()

					
					select new IntercompanyInvoiceSettlementListViewMap
				{
						CompanyGUID = intercompanyInvoiceSettlement.CompanyGUID,
						Owner = intercompanyInvoiceSettlement.Owner,
						OwnerBusinessUnitGUID = intercompanyInvoiceSettlement.OwnerBusinessUnitGUID,
						IntercompanyInvoiceSettlementGUID = intercompanyInvoiceSettlement.IntercompanyInvoiceSettlementGUID,
						TransDate = intercompanyInvoiceSettlement.TransDate,
						DocumentStatusGUID = intercompanyInvoiceSettlement.DocumentStatusGUID,
						WHTSlipReceivedByCustomer = intercompanyInvoiceSettlement.WHTSlipReceivedByCustomer,
						SettleAmount = intercompanyInvoiceSettlement.SettleAmount,
						SettleWHTAmount = intercompanyInvoiceSettlement.SettleWHTAmount,
						SettleInvoiceAmount = intercompanyInvoiceSettlement.SettleInvoiceAmount,
						IntercompanyInvoiceSettlementId = intercompanyInvoiceSettlement.IntercompanyInvoiceSettlementId,
						IntercompanyInvoiceTableGUID = intercompanyInvoiceSettlement.IntercompanyInvoiceTableGUID,
						DocumentStatus_Values = documentStatus.Description,
						DocumentStatus_StatusId = documentStatus.StatusId,
						MethodOfPayment_Values = SmartAppUtil.GetDropDownLabel(methodOfPayment.MethodOfPaymentId,methodOfPayment.Description),
						MethodOfPayment_MethodOfPaymentId = methodOfPayment.MethodOfPaymentId,
						MethodOfPaymentGUID = intercompanyInvoiceSettlement.MethodOfPaymentGUID,
						
					});
		}
		public SearchResult<IntercompanyInvoiceSettlementListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<IntercompanyInvoiceSettlementListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<IntercompanyInvoiceSettlement>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<IntercompanyInvoiceSettlementListViewMap, IntercompanyInvoiceSettlementListView>();
				result = list.SetSearchResult<IntercompanyInvoiceSettlementListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<IntercompanyInvoiceSettlementItemViewMap> GetItemQuery()
		{
			return (from intercompanyInvoiceSettlement in Entity
					join company in db.Set<Company>()
					on intercompanyInvoiceSettlement.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on intercompanyInvoiceSettlement.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljIntercompanyInvoiceSettlementOwnerBU
					from ownerBU in ljIntercompanyInvoiceSettlementOwnerBU.DefaultIfEmpty()

					join intercompanyInvoiceTable in db.Set<IntercompanyInvoiceTable>()
					on intercompanyInvoiceSettlement.IntercompanyInvoiceTableGUID equals intercompanyInvoiceTable.IntercompanyInvoiceTableGUID into ljIntercompanyInvoiceSettlementIntercompayInvoiceTable
					from intercompanyInvoiceTable in ljIntercompanyInvoiceSettlementIntercompayInvoiceTable.DefaultIfEmpty()

					join documentStatus in db.Set<DocumentStatus>()
					on intercompanyInvoiceSettlement.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljIntercompanyInvoiceSettlementDocumentStatus
					from documentStatus in ljIntercompanyInvoiceSettlementDocumentStatus.DefaultIfEmpty()

					join methodOfPayment in db.Set<MethodOfPayment>()
					on intercompanyInvoiceSettlement.MethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID into ljmethodOfPayment
					from methodOfPayment in ljmethodOfPayment.DefaultIfEmpty()

					join documentReason in db.Set<DocumentReason>()
					on intercompanyInvoiceSettlement.CNReasonGUID equals documentReason.DocumentReasonGUID into ljdocumentReason
					from documentReason in ljdocumentReason.DefaultIfEmpty()

					join refIntercompanyInvoiceSettlement in Entity
					on intercompanyInvoiceSettlement.RefIntercompanyInvoiceSettlementGUID equals refIntercompanyInvoiceSettlement.IntercompanyInvoiceSettlementGUID into ljrefIntercompanyInvoiceSettlement
					from refIntercompanyInvoiceSettlement in ljrefIntercompanyInvoiceSettlement.DefaultIfEmpty()


					select new IntercompanyInvoiceSettlementItemViewMap
					{
						CompanyGUID = intercompanyInvoiceSettlement.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = intercompanyInvoiceSettlement.Owner,
						OwnerBusinessUnitGUID = intercompanyInvoiceSettlement.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = intercompanyInvoiceSettlement.CreatedBy,
						CreatedDateTime = intercompanyInvoiceSettlement.CreatedDateTime,
						ModifiedBy = intercompanyInvoiceSettlement.ModifiedBy,
						ModifiedDateTime = intercompanyInvoiceSettlement.ModifiedDateTime,
						IntercompanyInvoiceSettlementGUID = intercompanyInvoiceSettlement.IntercompanyInvoiceSettlementGUID,
						DocumentStatusGUID = intercompanyInvoiceSettlement.DocumentStatusGUID,
						IntercompanyInvoiceTableGUID = intercompanyInvoiceSettlement.IntercompanyInvoiceTableGUID,
						SettleAmount = intercompanyInvoiceSettlement.SettleAmount,
						SettleInvoiceAmount = intercompanyInvoiceSettlement.SettleInvoiceAmount,
						SettleWHTAmount = intercompanyInvoiceSettlement.SettleWHTAmount,
						IntercompanyInvoiceSettlementId = intercompanyInvoiceSettlement.IntercompanyInvoiceSettlementId,
						TransDate = intercompanyInvoiceSettlement.TransDate,
						WHTSlipReceivedByCustomer = intercompanyInvoiceSettlement.WHTSlipReceivedByCustomer,
						IntercompanyInvoice_Values = SmartAppUtil.GetDropDownLabel(intercompanyInvoiceTable.IntercompanyInvoiceId, intercompanyInvoiceTable.IssuedDate.ToString()),
						DocumentStatus_Values = documentStatus.Description,
						DocumentStatus_StatusId = documentStatus.StatusId,
						MethodOfPayment_Values = SmartAppUtil.GetDropDownLabel(methodOfPayment.MethodOfPaymentId, methodOfPayment.Description),
						MethodOfPaymentGUID = intercompanyInvoiceSettlement.MethodOfPaymentGUID,
						IntercomapnyInvoice_InvoiceAmount = intercompanyInvoiceTable.InvoiceAmount,
						IntercomapnyInvoice_IssuedDate = intercompanyInvoiceTable.IssuedDate,
						CNReasonGUID = intercompanyInvoiceSettlement.CNReasonGUID,
						OrigInvoiceAmount = intercompanyInvoiceSettlement.OrigInvoiceAmount,
						OrigInvoiceId = intercompanyInvoiceSettlement.OrigInvoiceId,
						OrigTaxInvoiceAmount = intercompanyInvoiceSettlement.OrigTaxInvoiceAmount,
						OrigTaxInvoiceId = intercompanyInvoiceSettlement.OrigTaxInvoiceId,
						RefIntercompanyInvoiceSettlementGUID = intercompanyInvoiceSettlement.RefIntercompanyInvoiceSettlementGUID,
						CNReason_Values = (documentReason != null) ? SmartAppUtil.GetDropDownLabel(documentReason.ReasonId, documentReason.Description) : null,
						RefIntercompanyInvoiceSettlement_IntercompanyInvoiceSettlementId = (refIntercompanyInvoiceSettlement != null) ? refIntercompanyInvoiceSettlement.IntercompanyInvoiceSettlementId : null,
					
						RowVersion = intercompanyInvoiceSettlement.RowVersion,
					});
		}
		public IntercompanyInvoiceSettlementItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<IntercompanyInvoiceSettlementItemViewMap, IntercompanyInvoiceSettlementItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public IntercompanyInvoiceSettlement CreateIntercompanyInvoiceSettlement(IntercompanyInvoiceSettlement intercompanyInvoiceSettlement)
		{
			try
			{
				intercompanyInvoiceSettlement.IntercompanyInvoiceSettlementGUID = Guid.NewGuid();
				base.Add(intercompanyInvoiceSettlement);
				return intercompanyInvoiceSettlement;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateIntercompanyInvoiceSettlementVoid(IntercompanyInvoiceSettlement intercompanyInvoiceSettlement)
		{
			try
			{
				CreateIntercompanyInvoiceSettlement(intercompanyInvoiceSettlement);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IntercompanyInvoiceSettlement UpdateIntercompanyInvoiceSettlement(IntercompanyInvoiceSettlement dbIntercompanyInvoiceSettlement, IntercompanyInvoiceSettlement inputIntercompanyInvoiceSettlement, List<string> skipUpdateFields = null)
		{
			try
			{
				dbIntercompanyInvoiceSettlement = dbIntercompanyInvoiceSettlement.MapUpdateValues<IntercompanyInvoiceSettlement>(inputIntercompanyInvoiceSettlement);
				base.Update(dbIntercompanyInvoiceSettlement);
				return dbIntercompanyInvoiceSettlement;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateIntercompanyInvoiceSettlementVoid(IntercompanyInvoiceSettlement dbIntercompanyInvoiceSettlement, IntercompanyInvoiceSettlement inputIntercompanyInvoiceSettlement, List<string> skipUpdateFields = null)
		{
			try
			{
				dbIntercompanyInvoiceSettlement = dbIntercompanyInvoiceSettlement.MapUpdateValues<IntercompanyInvoiceSettlement>(inputIntercompanyInvoiceSettlement, skipUpdateFields);
				base.Update(dbIntercompanyInvoiceSettlement);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public IntercompanyInvoiceSettlement GetIntercompanyInvoiceSettlementByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.IntercompanyInvoiceSettlementGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public int GetIntercompanyInvoiceBy(Guid intercompanyInvoiceTableGUID, string statusId)
		{
			try
			{
				return (from interCompanyInvoiceSettlement in db.Set<IntercompanyInvoiceSettlement>()
						join documentStatus in db.Set<DocumentStatus>()
						on interCompanyInvoiceSettlement.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus
						from documentStatus in ljdocumentStatus.DefaultIfEmpty()
						where interCompanyInvoiceSettlement.IntercompanyInvoiceTableGUID == intercompanyInvoiceTableGUID && documentStatus.StatusId == statusId
						select new IntercompanyInvoiceSettlement
						{
							IntercompanyInvoiceSettlementGUID = interCompanyInvoiceSettlement.IntercompanyInvoiceSettlementGUID
						}
							  ).Count();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        public decimal GetSumSettleInvoiceAmount(Guid guid, Guid documentStatusGUID)
        {
            try {
				decimal sumSettleInvoiceAmount = (from intercompanyInvoiceSettlement in Entity
												  join intercompanyInvocieTable in db.Set<IntercompanyInvoiceTable>()
												  on intercompanyInvoiceSettlement.IntercompanyInvoiceTableGUID equals intercompanyInvocieTable.IntercompanyInvoiceTableGUID
												  where intercompanyInvoiceSettlement.DocumentStatusGUID == documentStatusGUID && intercompanyInvocieTable.IntercompanyInvoiceTableGUID == guid
												  select intercompanyInvoiceSettlement.SettleInvoiceAmount).Sum();
				return sumSettleInvoiceAmount;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
    }
}

