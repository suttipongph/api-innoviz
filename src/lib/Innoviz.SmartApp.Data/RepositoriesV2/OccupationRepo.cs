using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IOccupationRepo
	{
		#region DropDown
		IEnumerable<SelectItem<OccupationItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<OccupationListView> GetListvw(SearchParameter search);
		OccupationItemView GetByIdvw(Guid id);
		Occupation Find(params object[] keyValues);
		Occupation GetOccupationByIdNoTracking(Guid guid);
		Occupation CreateOccupation(Occupation occupation);
		void CreateOccupationVoid(Occupation occupation);
		Occupation UpdateOccupation(Occupation dbOccupation, Occupation inputOccupation, List<string> skipUpdateFields = null);
		void UpdateOccupationVoid(Occupation dbOccupation, Occupation inputOccupation, List<string> skipUpdateFields = null);
		void Remove(Occupation item);
	}
	public class OccupationRepo : CompanyBaseRepository<Occupation>, IOccupationRepo
	{
		public OccupationRepo(SmartAppDbContext context) : base(context) { }
		public OccupationRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public Occupation GetOccupationByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.OccupationGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<OccupationItemViewMap> GetDropDownQuery()
		{
			return (from occupation in Entity
					select new OccupationItemViewMap
					{
						CompanyGUID = occupation.CompanyGUID,
						Owner = occupation.Owner,
						OwnerBusinessUnitGUID = occupation.OwnerBusinessUnitGUID,
						OccupationGUID = occupation.OccupationGUID,
						OccupationId = occupation.OccupationId,
						Description = occupation.Description
					});
		}
		public IEnumerable<SelectItem<OccupationItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<Occupation>(search, SysParm.CompanyGUID);
				var occupation = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<OccupationItemViewMap, OccupationItemView>().ToDropDownItem(search.ExcludeRowData);


				return occupation;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<OccupationListViewMap> GetListQuery()
		{
			return (from occupation in Entity
					join company  in db.Set<Company>()
					on occupation.CompanyGUID equals company.CompanyGUID
				select new OccupationListViewMap
				{
						CompanyGUID = occupation.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = occupation.Owner,
						OwnerBusinessUnitGUID = occupation.OwnerBusinessUnitGUID,
						OccupationGUID = occupation.OccupationGUID,
						OccupationId = occupation.OccupationId,
						Description = occupation.Description
				});
		}
		public SearchResult<OccupationListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<OccupationListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<Occupation>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<OccupationListViewMap, OccupationListView>();
				result = list.SetSearchResult<OccupationListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<OccupationItemViewMap> GetItemQuery()
		{
			return (from occupation in Entity
					join company in db.Set<Company>()
					on occupation.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on occupation.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljOccupationOwnerBU
					from ownerBU in ljOccupationOwnerBU.DefaultIfEmpty()
					select new OccupationItemViewMap
					{
						CompanyGUID = occupation.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = occupation.Owner,
						OwnerBusinessUnitGUID = occupation.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = occupation.CreatedBy,
						CreatedDateTime = occupation.CreatedDateTime,
						ModifiedBy = occupation.ModifiedBy,
						ModifiedDateTime = occupation.ModifiedDateTime,
						OccupationGUID = occupation.OccupationGUID,
						Description = occupation.Description,
						OccupationId = occupation.OccupationId,
					
						RowVersion = occupation.RowVersion,
					});
		}
		public OccupationItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<OccupationItemViewMap, OccupationItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public Occupation CreateOccupation(Occupation occupation)
		{
			try
			{
				occupation.OccupationGUID = Guid.NewGuid();
				base.Add(occupation);
				return occupation;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateOccupationVoid(Occupation occupation)
		{
			try
			{
				CreateOccupation(occupation);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public Occupation UpdateOccupation(Occupation dbOccupation, Occupation inputOccupation, List<string> skipUpdateFields = null)
		{
			try
			{
				dbOccupation = dbOccupation.MapUpdateValues<Occupation>(inputOccupation);
				base.Update(dbOccupation);
				return dbOccupation;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateOccupationVoid(Occupation dbOccupation, Occupation inputOccupation, List<string> skipUpdateFields = null)
		{
			try
			{
				dbOccupation = dbOccupation.MapUpdateValues<Occupation>(inputOccupation, skipUpdateFields);
				base.Update(dbOccupation);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

