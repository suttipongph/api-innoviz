using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IDocumentStatusRepo
	{
		#region DropDown
		IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<DocumentStatusListView> GetListvw(SearchParameter search);
		DocumentStatusItemView GetByIdvw(Guid id);
		DocumentStatus Find(params object[] keyValues);
		DocumentStatus GetDocumentStatusByIdNoTracking(Guid guid);
		DocumentStatus CreateDocumentStatus(DocumentStatus documentStatus);
		void CreateDocumentStatusVoid(DocumentStatus documentStatus);
		DocumentStatus UpdateDocumentStatus(DocumentStatus dbDocumentStatus, DocumentStatus inputDocumentStatus, List<string> skipUpdateFields = null);
		void UpdateDocumentStatusVoid(DocumentStatus dbDocumentStatus, DocumentStatus inputDocumentStatus, List<string> skipUpdateFields = null);
		void Remove(DocumentStatus item);
		DocumentStatus GetDocumentStatusByStatusId(string statusId);
		DocumentStatus GetDocumentStatusByStatusIdNoTracking(string statusId);
		DocumentStatus GetDocumentStatusByDocumentStatusGUIDNoTracking(Guid documentStatusGUID);
		IQueryable<DocumentStatus> GetDocumentStatusByStatusIdNoTracking(string[] statusIds);
		IQueryable<DocumentStatus> GetDocumentStatusByProcessId(string processId);
	}
	public class DocumentStatusRepo : CompanyBaseRepository<DocumentStatus>, IDocumentStatusRepo
	{
		public DocumentStatusRepo(SmartAppDbContext context) : base(context) { }
		public DocumentStatusRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public DocumentStatus GetDocumentStatusByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.DocumentStatusGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<DocumentStatusItemViewMap> GetDropDownQuery()
		{
			return (from documentStatus in Entity
					join documentProcess in db.Set<DocumentProcess>()
					on documentStatus.DocumentProcessGUID equals documentProcess.DocumentProcessGUID
					select new DocumentStatusItemViewMap
					{
						DocumentStatusGUID = documentStatus.DocumentStatusGUID,
						Description = documentStatus.Description,
						ProcessId = documentProcess.ProcessId,
						DocumentProcessGUID = documentStatus.DocumentProcessGUID,
						StatusCode = documentStatus.StatusId
					});
		}
		public IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{

				var predicate = base.GetFilterLevelPredicate<DocumentStatus>(search);
				var documentStatus = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<DocumentStatusItemViewMap, DocumentStatusItemView>().ToDropDownItem(search.ExcludeRowData);


				return documentStatus;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<DocumentStatusListViewMap> GetListQuery()
		{
			return (from documentStatus in Entity
					select new DocumentStatusListViewMap
					{
						DocumentStatusGUID = documentStatus.DocumentStatusGUID,
						StatusId = documentStatus.StatusId,
						Description = documentStatus.Description,
						DocumentProcessGUID = documentStatus.DocumentProcessGUID
					});
		}
		public SearchResult<DocumentStatusListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<DocumentStatusListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<DocumentStatus>(search);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<DocumentStatusListViewMap, DocumentStatusListView>();
				result = list.SetSearchResult<DocumentStatusListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<DocumentStatusItemViewMap> GetItemQuery()
		{
			return (from documentStatus in Entity
					join documentProcess in db.Set<DocumentProcess>() on documentStatus.DocumentProcessGUID equals documentProcess.DocumentProcessGUID
					select new DocumentStatusItemViewMap
					{
						CreatedBy = documentStatus.CreatedBy,
						CreatedDateTime = documentStatus.CreatedDateTime,
						ModifiedBy = documentStatus.ModifiedBy,
						ModifiedDateTime = documentStatus.ModifiedDateTime,
						DocumentStatusGUID = documentStatus.DocumentStatusGUID,
						Description = documentStatus.Description,
						DocumentProcessGUID = documentStatus.DocumentProcessGUID,
						StatusCode = documentStatus.StatusCode,
						StatusId = documentStatus.StatusId,
						DocumentProcess_Values = SmartAppUtil.GetDropDownLabel(documentProcess.ProcessId, documentProcess.Description),
					
						RowVersion = documentStatus.RowVersion,
					});
		}
		public DocumentStatusItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<DocumentStatusItemViewMap, DocumentStatusItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public DocumentStatus CreateDocumentStatus(DocumentStatus documentStatus)
		{
			try
			{
				documentStatus.DocumentStatusGUID = Guid.NewGuid();
				base.Add(documentStatus);
				return documentStatus;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateDocumentStatusVoid(DocumentStatus documentStatus)
		{
			try
			{
				CreateDocumentStatus(documentStatus);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentStatus UpdateDocumentStatus(DocumentStatus dbDocumentStatus, DocumentStatus inputDocumentStatus, List<string> skipUpdateFields = null)
		{
			try
			{
				dbDocumentStatus = dbDocumentStatus.MapUpdateValues<DocumentStatus>(inputDocumentStatus);
				base.Update(dbDocumentStatus);
				return dbDocumentStatus;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateDocumentStatusVoid(DocumentStatus dbDocumentStatus, DocumentStatus inputDocumentStatus, List<string> skipUpdateFields = null)
		{
			try
			{
				dbDocumentStatus = dbDocumentStatus.MapUpdateValues<DocumentStatus>(inputDocumentStatus, skipUpdateFields);
				base.Update(dbDocumentStatus);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete

		public DocumentStatus GetDocumentStatusByStatusId(string statusId)
		{
			try
			{
				DocumentStatus documentStatus = Entity.Where(a => a.StatusId == statusId).FirstOrDefault();
				if (documentStatus == null)
				{
					SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
					smartAppException.AddData("ERROR.90034");
					throw smartAppException;
				}
				return documentStatus;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public DocumentStatus GetDocumentStatusByStatusIdNoTracking(string statusId)
		{
			try
			{
				DocumentStatus documentStatus = Entity.Where(a => a.StatusId == statusId)
					.AsNoTracking().FirstOrDefault();
				if (documentStatus == null)
				{
					SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
					smartAppException.AddData("ERROR.90034");
					throw smartAppException;
				}
				return documentStatus;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public DocumentStatus GetDocumentStatusByDocumentStatusGUIDNoTracking(Guid documentStatusGUID)
		{
			try
			{
				DocumentStatus documentStatus = Entity.Where(a => a.DocumentStatusGUID == documentStatusGUID)
					.AsNoTracking().FirstOrDefault();
				if (documentStatus == null)
				{
					SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
					smartAppException.AddData("ERROR.90034");
					throw smartAppException;
				}
				return documentStatus;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public IQueryable<DocumentStatus> GetDocumentStatusByStatusIdNoTracking(string[] statusIds)
		{
			try
			{
				IQueryable<DocumentStatus> documentStatus = Entity.Where(a => statusIds.Contains(a.StatusId)).AsNoTracking();
				if (!documentStatus.Any())
				{
					if (documentStatus.Count() != statusIds.Count())
					{
						SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
						smartAppException.AddData("ERROR.90034");
						throw smartAppException;
					}
				}
				return documentStatus;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public IQueryable<DocumentStatus> GetDocumentStatusByProcessId(string processId)
		{
			try
			{
				return (from documentProcess in db.Set<DocumentProcess>()
						join documentStatus in Entity
						on documentProcess.DocumentProcessGUID equals documentStatus.DocumentProcessGUID
						where documentProcess.ProcessId == processId
						select documentStatus).AsNoTracking();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}

	}
}

