using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface IAssignmentAgreementTableRepo
    {
        #region DropDown
        IEnumerable<SelectItem<AssignmentAgreementTableItemView>> GetDropDownItem(SearchParameter search);
        IEnumerable<SelectItem<AssignmentAgreementTableItemView>> GetDropDownItemByStatus(SearchParameter search);
        IEnumerable<SelectItem<AssignmentAgreementTableItemView>> GetDropDownItemByStatusAndProcessId(SearchParameter search);
        #endregion DropDown
        SearchResult<AssignmentAgreementTableListView> GetListvw(SearchParameter search);
        AssignmentAgreementTableItemView GetByIdvw(Guid id);
        AssignmentAgreementTable Find(params object[] keyValues);
        AssignmentAgreementTable GetAssignmentAgreementTableByIdNoTracking(Guid guid);
        AssignmentAgreementTable CreateAssignmentAgreementTable(AssignmentAgreementTable assignmentAgreementTable);
        void CreateAssignmentAgreementTableVoid(AssignmentAgreementTable assignmentAgreementTable);
        AssignmentAgreementTable UpdateAssignmentAgreementTable(AssignmentAgreementTable dbAssignmentAgreementTable, AssignmentAgreementTable inputAssignmentAgreementTable, List<string> skipUpdateFields = null);
        void UpdateAssignmentAgreementTableVoid(AssignmentAgreementTable dbAssignmentAgreementTable, AssignmentAgreementTable inputAssignmentAgreementTable, List<string> skipUpdateFields = null);
        void Remove(AssignmentAgreementTable item);
        AssignmentAgreementTable GetAssignmentAgreementTableByIdNoTrackingByAccessLevel(Guid guid);
        IEnumerable<AssignmentAgreementTable> GetAssignmentAgreementTableByCompanyNoTracking(Guid guid);
        #region function
        AssignmentAgreementTable GetAssignmentAgreementTableByInternalAssignmentId(string id);
        AssignmentAgreementTable GetAssignmentAgreementTableByAssignmentId(string id);
        SearchResult<AssignmentAgreementOutstandingView> GetAssignmentAgreementOutstanding(SearchParameter search);
        List<AssignmentAgreementOutstandingView> GetAssignmentAgreementOutstandingByCustomer(Guid customerTableGUID);
        List<AssignmentAgreementOutstandingView> GetAssignmentAgreementOutstandingByBuyer(Guid buyerTableGUID);
        List<AssignmentAgreementOutstandingView> GetAssignmentAgreementOutstandingByAssignmentAgreement(Guid assignmentAgreementTableGUID);
        CancelAssignmentAgreementView GetCancelAssignmentAgreementValue(Guid refGuid, Guid bookmarkDocumentTransGUID);

        #endregion

        AssignmentAgreementBookmarkView GetAssignmentAgreementBookmarkValue(Guid refGuid, Guid bookmarkDocumentTransGuid);
        void ValidateAdd(IEnumerable<AssignmentAgreementTable> items);
        List<AssignmentAgreementTable> GetListByRefAssignmentAgreementTableGUID(Guid refAssignmentAgreementTableGUID);
        bool IsDuplicateAssignmentAgreementId(Guid guid, string id);
    }
    public class AssignmentAgreementTableRepo : CompanyBaseRepository<AssignmentAgreementTable>, IAssignmentAgreementTableRepo
    {
        public AssignmentAgreementTableRepo(SmartAppDbContext context) : base(context) { }
        public AssignmentAgreementTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public AssignmentAgreementTable GetAssignmentAgreementTableByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.AssignmentAgreementTableGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        private IQueryable<AssignmentAgreementTableItemViewMap> GetDropDownQuery()
        {
            return (from assignmentAgreementTable in Entity
                    select new AssignmentAgreementTableItemViewMap
                    {
                        CompanyGUID = assignmentAgreementTable.CompanyGUID,
                        Owner = assignmentAgreementTable.Owner,
                        OwnerBusinessUnitGUID = assignmentAgreementTable.OwnerBusinessUnitGUID,
                        AssignmentAgreementTableGUID = assignmentAgreementTable.AssignmentAgreementTableGUID,
                        InternalAssignmentAgreementId = assignmentAgreementTable.InternalAssignmentAgreementId,
                        Description = assignmentAgreementTable.Description,
                        CustomerTableGUID = assignmentAgreementTable.CustomerTableGUID,
                        BuyerTableGUID = assignmentAgreementTable.BuyerTableGUID
                    });
        }
        public IEnumerable<SelectItem<AssignmentAgreementTableItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<AssignmentAgreementTable>(search, SysParm.CompanyGUID);
                var assignmentAgreementTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
                    .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                    .Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
                    .ToMaps<AssignmentAgreementTableItemViewMap, AssignmentAgreementTableItemView>().ToDropDownItem(search.ExcludeRowData);
                return assignmentAgreementTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private IQueryable<AssignmentAgreementTableItemViewMap> GetDropDownQueryByStatus()
        {
            return (from assignmentAgreementTable in Entity
                    join documentStatus in db.Set<DocumentStatus>() on assignmentAgreementTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
                    select new AssignmentAgreementTableItemViewMap
                    {
                        CompanyGUID = assignmentAgreementTable.CompanyGUID,
                        Owner = assignmentAgreementTable.Owner,
                        OwnerBusinessUnitGUID = assignmentAgreementTable.OwnerBusinessUnitGUID,
                        AssignmentAgreementTableGUID = assignmentAgreementTable.AssignmentAgreementTableGUID,
                        InternalAssignmentAgreementId = assignmentAgreementTable.InternalAssignmentAgreementId,
                        Description = assignmentAgreementTable.Description,
                        BuyerTableGUID = assignmentAgreementTable.BuyerTableGUID,
                        DocumentStatus_StatusId = documentStatus.StatusId,
                        CustomerTableGUID = assignmentAgreementTable.CustomerTableGUID,
                        AssignmentMethodGUID = assignmentAgreementTable.AssignmentMethodGUID
                    });
        }
        private IQueryable<AssignmentAgreementTableItemViewMap> GetDropDownQueryByStatusProcessId()
        {
            return (from assignmentAgreementTable in Entity
                    join documentStatus in db.Set<DocumentStatus>() 
                    on assignmentAgreementTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus
                    from documentStatus in ljdocumentStatus.DefaultIfEmpty()

                    join processStatus in db.Set<DocumentProcess>()
                    on documentStatus.DocumentProcessGUID equals processStatus.DocumentProcessGUID into ljprocessStatus
                    from processStatus in ljprocessStatus.DefaultIfEmpty()


                    select new AssignmentAgreementTableItemViewMap
                    {
                        CompanyGUID = assignmentAgreementTable.CompanyGUID,
                        Owner = assignmentAgreementTable.Owner,
                        OwnerBusinessUnitGUID = assignmentAgreementTable.OwnerBusinessUnitGUID,
                        AssignmentAgreementTableGUID = assignmentAgreementTable.AssignmentAgreementTableGUID,
                        InternalAssignmentAgreementId = assignmentAgreementTable.InternalAssignmentAgreementId,
                        Description = assignmentAgreementTable.Description,
                        BuyerTableGUID = assignmentAgreementTable.BuyerTableGUID,
                        DocumentStatus_StatusId = documentStatus.StatusId,
                        CustomerTableGUID = assignmentAgreementTable.CustomerTableGUID,
                        DocumentProcess_ProcessId = processStatus.ProcessId
                      
                    });
        }
        public IEnumerable<SelectItem<AssignmentAgreementTableItemView>> GetDropDownItemByStatus(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<AssignmentAgreementTable>(search, SysParm.CompanyGUID);
                var assignmentAgreementTable = GetDropDownQueryByStatus().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<AssignmentAgreementTableItemViewMap, AssignmentAgreementTableItemView>().ToDropDownItem(search.ExcludeRowData);


                return assignmentAgreementTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<AssignmentAgreementTableItemView>> GetDropDownItemByStatusAndProcessId(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<AssignmentAgreementTable>(search, SysParm.CompanyGUID);
                var assignmentAgreementTable = GetDropDownQueryByStatusProcessId().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<AssignmentAgreementTableItemViewMap, AssignmentAgreementTableItemView>().ToDropDownItem(search.ExcludeRowData);


                return assignmentAgreementTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #region GetListvw
        private IQueryable<AssignmentAgreementTableListViewMap> GetListQuery()
        {
            return (from assignmentAgreementTable in Entity
                    join assignmentMethod in db.Set<AssignmentMethod>()
                    on assignmentAgreementTable.AssignmentMethodGUID equals assignmentMethod.AssignmentMethodGUID into ljAssignMethod
                    from assignmentMethod in ljAssignMethod.DefaultIfEmpty()
                    join documentStatus in db.Set<DocumentStatus>()
                    on assignmentAgreementTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljAssignDocStatus
                    from documentStatus in ljAssignDocStatus.DefaultIfEmpty()
                    join customerTable in db.Set<CustomerTable>()
                    on assignmentAgreementTable.CustomerTableGUID equals customerTable.CustomerTableGUID into ljAssignCustomer
                    from customerTable in ljAssignCustomer.DefaultIfEmpty()
                    join buyerTable in db.Set<BuyerTable>()
                    on assignmentAgreementTable.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljAssignBuyer
                    from buyerTable in ljAssignBuyer.DefaultIfEmpty()
                   
                    
                    
                    join creditAppRequestTable in db.Set<CreditAppRequestTable>()
                    on assignmentAgreementTable.RefCreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID into ljAssignCreditAppRequest
                    from creditAppRequestTable in ljAssignCreditAppRequest.DefaultIfEmpty()

                    select new AssignmentAgreementTableListViewMap
                    {
                        CompanyGUID = assignmentAgreementTable.CompanyGUID,
                        Owner = assignmentAgreementTable.Owner,
                        OwnerBusinessUnitGUID = assignmentAgreementTable.OwnerBusinessUnitGUID,
                        AssignmentAgreementTableGUID = assignmentAgreementTable.AssignmentAgreementTableGUID,
                        InternalAssignmentAgreementId = assignmentAgreementTable.InternalAssignmentAgreementId,
                        AssignmentAgreementId = assignmentAgreementTable.AssignmentAgreementId,
                        BuyerTableGUID = assignmentAgreementTable.BuyerTableGUID,
                        CustomerTableGUID = assignmentAgreementTable.CustomerTableGUID,
                        AssignmentMethodGUID = assignmentAgreementTable.AssignmentMethodGUID,
                        AgreementDate = assignmentAgreementTable.AgreementDate,
                        AssignmentAgreementAmount = assignmentAgreementTable.AssignmentAgreementAmount,
                        DocumentStatusGUID = assignmentAgreementTable.DocumentStatusGUID,
                        AgreementDocType = assignmentAgreementTable.AgreementDocType,
                        AssignmentMethod_Values = SmartAppUtil.GetDropDownLabel(assignmentMethod.AssignmentMethodId, assignmentMethod.Description),
                        DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
                        CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                        BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
                        RefAssignmentAgreementTableGUID = assignmentAgreementTable.RefAssignmentAgreementTableGUID,
                        DocumentStatus_StatusId = documentStatus.StatusId,
                        BuyerTable_BuyerId = buyerTable.BuyerId,
                        CustomerTable_CustomerId = customerTable.CustomerId,
                        AssignmentMethod_AssignmentMethodId = assignmentMethod.AssignmentMethodId,
                        RefCreditAppRequestTableGUID = assignmentAgreementTable.RefCreditAppRequestTableGUID,
                        CreditAppReqAssignmentGUID = assignmentAgreementTable.CreditAppReqAssignmentGUID,
                        CreditAppRequestTable_Values = SmartAppUtil.GetDropDownLabel(creditAppRequestTable.CreditAppRequestId, creditAppRequestTable.Description),
                        CreditAppRequestTable_CreditAppRequestId = creditAppRequestTable.CreditAppRequestId,
                        Description = assignmentAgreementTable.Description
                   
                    });
        }
        public SearchResult<AssignmentAgreementTableListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<AssignmentAgreementTableListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<AssignmentAgreementTable>(search, SysParm.CompanyGUID);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<AssignmentAgreementTableListViewMap, AssignmentAgreementTableListView>();
                result = list.SetSearchResult<AssignmentAgreementTableListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<AssignmentAgreementTableItemViewMap> GetItemQuery()
        {
            return (from assignmentAgreementTable in Entity
                    join documentStatus in db.Set<DocumentStatus>()
                    on assignmentAgreementTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljAssignDocStatus
                    from documentStatus in ljAssignDocStatus.DefaultIfEmpty()
                    join company in db.Set<Company>()
                    on assignmentAgreementTable.CompanyGUID equals company.CompanyGUID
                    join customerTable in db.Set<CustomerTable>()
                    on assignmentAgreementTable.CustomerTableGUID equals customerTable.CustomerTableGUID into ljAssignCustomer
                    from customerTable in ljAssignCustomer.DefaultIfEmpty()
                    join buyerTable in db.Set<BuyerTable>()
                    on assignmentAgreementTable.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljAssignBuyer
                    from buyerTable in ljAssignBuyer.DefaultIfEmpty()
                    join ownerBU in db.Set<BusinessUnit>()
                    on assignmentAgreementTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljAssignmentAgreementTableOwnerBU
                    from ownerBU in ljAssignmentAgreementTableOwnerBU.DefaultIfEmpty()
                    join documentReason in db.Set<DocumentReason>()
                    on assignmentAgreementTable.DocumentReasonGUID equals documentReason.DocumentReasonGUID into ljdocumentReason
                    from documentReason in ljdocumentReason.DefaultIfEmpty()
                    join assignmentMethod in db.Set<AssignmentMethod>()
                    on assignmentAgreementTable.AssignmentMethodGUID equals assignmentMethod.AssignmentMethodGUID into ljassignmentMethod
                    from assignmentMethod in ljassignmentMethod.DefaultIfEmpty()
                    join refAssignmentAgreementTable in db.Set<AssignmentAgreementTable>()
                    on assignmentAgreementTable.RefAssignmentAgreementTableGUID equals refAssignmentAgreementTable.AssignmentAgreementTableGUID into ljrefAssignmentAgreementTable
                    from refAssignmentAgreementTable in ljrefAssignmentAgreementTable.DefaultIfEmpty()

                    select new AssignmentAgreementTableItemViewMap
                    {
                        CompanyGUID = assignmentAgreementTable.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = assignmentAgreementTable.Owner,
                        OwnerBusinessUnitGUID = assignmentAgreementTable.OwnerBusinessUnitGUID,
                        CreatedBy = assignmentAgreementTable.CreatedBy,
                        CreatedDateTime = assignmentAgreementTable.CreatedDateTime,
                        ModifiedBy = assignmentAgreementTable.ModifiedBy,
                        ModifiedDateTime = assignmentAgreementTable.ModifiedDateTime,
                        AssignmentAgreementTableGUID = assignmentAgreementTable.AssignmentAgreementTableGUID,
                        AcceptanceName = assignmentAgreementTable.AcceptanceName,
                        AgreementDate = assignmentAgreementTable.AgreementDate,
                        AgreementDocType = assignmentAgreementTable.AgreementDocType,
                        AssignmentAgreementAmount = assignmentAgreementTable.AssignmentAgreementAmount,
                        AssignmentAgreementId = assignmentAgreementTable.AssignmentAgreementId,
                        AssignmentMethodGUID = assignmentAgreementTable.AssignmentMethodGUID,
                        BuyerName = assignmentAgreementTable.BuyerName,
                        BuyerTableGUID = assignmentAgreementTable.BuyerTableGUID,
                        ConsortiumTableGUID = assignmentAgreementTable.ConsortiumTableGUID,
                        CustBankGUID = assignmentAgreementTable.CustBankGUID,
                        CustomerAltName = assignmentAgreementTable.CustomerAltName,
                        CustomerName = assignmentAgreementTable.CustomerName,
                        CustomerTableGUID = assignmentAgreementTable.CustomerTableGUID,
                        Description = assignmentAgreementTable.Description,
                        DocumentReasonGUID = assignmentAgreementTable.DocumentReasonGUID,
                        DocumentStatusGUID = assignmentAgreementTable.DocumentStatusGUID,
                        InternalAssignmentAgreementId = assignmentAgreementTable.InternalAssignmentAgreementId,
                        RefAssignmentAgreementTableGUID = assignmentAgreementTable.RefAssignmentAgreementTableGUID,
                        ReferenceAgreementId = assignmentAgreementTable.ReferenceAgreementId,
                        Remark = assignmentAgreementTable.Remark,
                        SigningDate = assignmentAgreementTable.SigningDate,
                        DocumentStatus_StatusId = documentStatus.StatusId,
                        DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
                        CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                        BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
                        AssignmentProductDescription = assignmentAgreementTable.AssignmentProductDescription,
                        ToWhomConcern = assignmentAgreementTable.ToWhomConcern,
                        CancelAuthorityPersonAddress = assignmentAgreementTable.CancelAuthorityPersonAddress,
                        CancelAuthorityPersonID = assignmentAgreementTable.CancelAuthorityPersonID,
                        CancelAuthorityPersonName = assignmentAgreementTable.CancelAuthorityPersonName,
                        CustRegisteredLocation = assignmentAgreementTable.CustRegisteredLocation,
                        RefCreditAppRequestTableGUID = assignmentAgreementTable.RefCreditAppRequestTableGUID,
                        CreditAppReqAssignmentGUID = assignmentAgreementTable.CreditAppReqAssignmentGUID,
                        DocumentReason_Values = (documentReason != null) ? SmartAppUtil.GetDropDownLabel(documentReason.ReasonId, documentReason.Description) : null,
                        AssignmentMethod_AssignmentMethodId = assignmentMethod.AssignmentMethodId,
                        AssignmentAgreementTable_InternalAssignmentAgreementId = (refAssignmentAgreementTable != null) ? refAssignmentAgreementTable.InternalAssignmentAgreementId : null,
                        AssignmentAgreementTable_AssignmentAgreementId = (refAssignmentAgreementTable != null) ? refAssignmentAgreementTable.AssignmentAgreementId : null,
                    
                        RowVersion = assignmentAgreementTable.RowVersion,
                    });
        }
        public AssignmentAgreementTableItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .ToMap<AssignmentAgreementTableItemViewMap, AssignmentAgreementTableItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public AssignmentAgreementTable CreateAssignmentAgreementTable(AssignmentAgreementTable assignmentAgreementTable)
        {
            try
            {
                assignmentAgreementTable.AssignmentAgreementTableGUID = assignmentAgreementTable.AssignmentAgreementTableGUID == Guid.Empty ? Guid.NewGuid() : assignmentAgreementTable.AssignmentAgreementTableGUID;
                base.Add(assignmentAgreementTable);
                return assignmentAgreementTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateAssignmentAgreementTableVoid(AssignmentAgreementTable assignmentAgreementTable)
        {
            try
            {
                CreateAssignmentAgreementTable(assignmentAgreementTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AssignmentAgreementTable UpdateAssignmentAgreementTable(AssignmentAgreementTable dbAssignmentAgreementTable, AssignmentAgreementTable inputAssignmentAgreementTable, List<string> skipUpdateFields = null)
        {
            try
            {
                dbAssignmentAgreementTable = dbAssignmentAgreementTable.MapUpdateValues<AssignmentAgreementTable>(inputAssignmentAgreementTable);
                base.Update(dbAssignmentAgreementTable);
                return dbAssignmentAgreementTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateAssignmentAgreementTableVoid(AssignmentAgreementTable dbAssignmentAgreementTable, AssignmentAgreementTable inputAssignmentAgreementTable, List<string> skipUpdateFields = null)
        {
            try
            {
                dbAssignmentAgreementTable = dbAssignmentAgreementTable.MapUpdateValues<AssignmentAgreementTable>(inputAssignmentAgreementTable, skipUpdateFields);
                base.Update(dbAssignmentAgreementTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Create, Update, Delete
        public AssignmentAgreementTable GetAssignmentAgreementTableByIdNoTrackingByAccessLevel(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.AssignmentAgreementTableGUID == guid)
                    .FilterByAccessLevel(SysParm.AccessLevel)
                    .AsNoTracking()
                    .FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<AssignmentAgreementTable> GetAssignmentAgreementTableByCompanyNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.CompanyGUID == guid)
                    .AsNoTracking();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region function
        public AssignmentAgreementTable GetAssignmentAgreementTableByInternalAssignmentId(string id)
        {
            try
            {
                var result = Entity.Where(item => item.InternalAssignmentAgreementId == id).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AssignmentAgreementTable GetAssignmentAgreementTableByAssignmentId(string id)
        {
            try
            {
                var result = Entity.Where(item => item.AssignmentAgreementId == id).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public SearchResult<AssignmentAgreementOutstandingView> GetAssignmentAgreementOutstanding(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<AssignmentAgreementTable>(search, SysParm.CompanyGUID);
                var total = GetItemAssignmentAgreementQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetItemAssignmentAgreementQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<AssignmentAgreementOutstandingMap, AssignmentAgreementOutstandingView>();
                var result = list.SetSearchResult<AssignmentAgreementOutstandingView>(total, search);
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private IQueryable<AssignmentAgreementOutstandingMap> GetItemAssignmentAgreementQuery()
        {
            return (from assignmentAgreementTable in Entity
                    join assignmentAgreementSettle in db.Set<AssignmentAgreementSettle>()
                    on assignmentAgreementTable.AssignmentAgreementTableGUID equals assignmentAgreementSettle.AssignmentAgreementTableGUID into ljassignmentAgreementSettle
                    from assignmentAgreementSettle in ljassignmentAgreementSettle.DefaultIfEmpty()
                    join customerTable in db.Set<CustomerTable>()
                    on assignmentAgreementTable.CustomerTableGUID equals customerTable.CustomerTableGUID into ljAssignCustomer
                    from customerTable in ljAssignCustomer.DefaultIfEmpty()
                    join buyerTable in db.Set<BuyerTable>()
                    on assignmentAgreementTable.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljAssignBuyer
                    from buyerTable in ljAssignBuyer.DefaultIfEmpty()
                    join documentStatus in db.Set<DocumentStatus>()
                    on assignmentAgreementTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
                    join assignmentMethod in db.Set<AssignmentMethod>()
                    on assignmentAgreementTable.AssignmentMethodGUID equals assignmentMethod.AssignmentMethodGUID
                    group new
                    {
                        assignmentAgreementTable,
                        assignmentAgreementSettle,
                        customerTable,
                        buyerTable,
                        documentStatus,
                        assignmentMethod
                    }
                    by new
                    {
                        assignmentAgreementTable.CustomerTableGUID,
                        assignmentAgreementTable.AssignmentAgreementTableGUID,
                        assignmentAgreementTable.BuyerTableGUID,
                        assignmentAgreementTable.AssignmentAgreementAmount,
                        assignmentAgreementTable.InternalAssignmentAgreementId,
                        assignmentAgreementTable.AssignmentAgreementId,
                        assignmentAgreementTable.DocumentStatusGUID,
                        assignmentAgreementTable.AssignmentMethodGUID,
                        assignmentAgreementTable.AgreementDocType,
                        assignmentAgreementTable.AgreementDate,
                        assignmentAgreementTable.Description,
                        assignmentAgreementTable.ReferenceAgreementId,
                        documentStatus.StatusId,
                        documentStatus_Description = documentStatus.Description,
                        assignmentMethod.AssignmentMethodId,
                        assignmentMethod_Description = assignmentMethod.Description,
                        customerTable.CustomerId,
                        CustomerName = customerTable.Name,
                        buyerTable.BuyerId,
                        buyerTable.BuyerName,
                        assignmentAgreementSettle.RefType
                    } into grp
                    select new AssignmentAgreementOutstandingMap()
                    {
                        InternalAssignmentAgreementId = grp.Key.InternalAssignmentAgreementId,
                        CustomerTableGUID = grp.Key.CustomerTableGUID,
                        AssignmentAgreementTableGUID = grp.Key.AssignmentAgreementTableGUID,
                        BuyerTableGUID = grp.Key.BuyerTableGUID,
                        AssignmentAgreementAmount = grp.Key.AssignmentAgreementAmount,
                        SettledAmount = grp.Sum(x => x.assignmentAgreementSettle.SettledAmount),
                        RemainingAmount = (grp.Key.AssignmentAgreementAmount > 0) ? grp.Sum(x => x.assignmentAgreementSettle.SettledAmount) > 0 ? grp.Key.AssignmentAgreementAmount - grp.Sum(x => x.assignmentAgreementSettle.SettledAmount) : grp.Key.AssignmentAgreementAmount - 0 : 0,
                        CustomerTable_CustomerId = grp.Key.CustomerId,
                        CustomerName = grp.Key.CustomerName,
                        BuyerTable_BuyerId = grp.Key.BuyerId,
                        BuyerName = grp.Key.BuyerName,
                        RefType = grp.Key.RefType,
                        CustomerTable_Values = "[" + grp.Key.CustomerId + "] " + grp.Key.CustomerName,
                        BuyerTable_Values = "[" + grp.Key.BuyerId + "] " + grp.Key.BuyerName,
                        AssignmentAgreementId = grp.Key.AssignmentAgreementId,
                        DocumentStatusGUID = grp.Key.DocumentStatusGUID,
                        DocumentStatus_StatusId = grp.Key.StatusId,
                        DocumentStatus_Values = grp.Key.documentStatus_Description,
                        AssignmentMethodGUID = grp.Key.AssignmentMethodGUID,
                        AssignmentMethod_InternalAssignmentAgreementId = grp.Key.InternalAssignmentAgreementId,
                        AssignmentMethod_Values = "[" + grp.Key.InternalAssignmentAgreementId + "] " + grp.Key.assignmentMethod_Description,
                        AgreementDocType = grp.Key.AgreementDocType,
                        AgreementDate = grp.Key.AgreementDate,
                        Description = grp.Key.Description,
                        ReferenceAgreementId = grp.Key.ReferenceAgreementId,
                    });
        }

        public List<AssignmentAgreementOutstandingView> GetAssignmentAgreementOutstandingByCustomer(Guid customerTableGUID)
        {
            try
            {
                return GetItemAssignmentAgreementQuery().Where(w => w.CustomerTableGUID == customerTableGUID).ToMaps<AssignmentAgreementOutstandingMap, AssignmentAgreementOutstandingView>();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<AssignmentAgreementOutstandingView> GetAssignmentAgreementOutstandingByBuyer(Guid buyerTableGUID)
        {
            try
            {
                return GetItemAssignmentAgreementQuery().Where(w => w.BuyerTableGUID == buyerTableGUID).ToMaps<AssignmentAgreementOutstandingMap, AssignmentAgreementOutstandingView>();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<AssignmentAgreementOutstandingView> GetAssignmentAgreementOutstandingByAssignmentAgreement(Guid assignmentAgreementTableGUID)
        {
            try
            {
                return GetItemAssignmentAgreementQuery().Where(w => w.AssignmentAgreementTableGUID == assignmentAgreementTableGUID).ToMaps<AssignmentAgreementOutstandingMap, AssignmentAgreementOutstandingView>();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public List<AssignmentAgreementTable> GetListByRefAssignmentAgreementTableGUID(Guid refAssignmentAgreementTableGUID)
        {
            try
            {
                var result = Entity.Where(item => item.RefAssignmentAgreementTableGUID == refAssignmentAgreementTableGUID).AsNoTracking().ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region Bookmark
        public CancelAssignmentAgreementView GetCancelAssignmentAgreementValue(Guid refGuid, Guid bookmarkDocumentTransGUID)
        {
            try
            {
                var QBookmarkDocumentTrans = (from bookmarkDocumentTrans in db.Set<BookmarkDocumentTrans>()
                                              where bookmarkDocumentTrans.BookmarkDocumentTransGUID == bookmarkDocumentTransGUID
                                              select bookmarkDocumentTrans).AsNoTracking().FirstOrDefault();


                var cancelAssignmentAgreementView = (from assignmentAgreementTable in Entity
                                                     join agreementTableInfo in db.Set<AgreementTableInfo>()
                                                     on new { assignmentAgreementTable.AssignmentAgreementTableGUID, refType = (int)RefType.AssignmentAgreement, assignmentAgreementTable.AgreementDocType }
                                                     equals new { AssignmentAgreementTableGUID = agreementTableInfo.RefGUID, refType = agreementTableInfo.RefType, AgreementDocType = (int)AgreementDocType.NoticeOfCancellation } into ljagreementTableInfo
                                                     from agreementTableInfo in ljagreementTableInfo.DefaultIfEmpty()

                                                     join authorizedPersonTypeBuyer in db.Set<AuthorizedPersonType>()
                                                     on agreementTableInfo.AuthorizedPersonTypeBuyerGUID equals authorizedPersonTypeBuyer.AuthorizedPersonTypeGUID into ljauthorizedPersonTypeBuyer
                                                     from authorizedPersonTypeBuyer in ljauthorizedPersonTypeBuyer.DefaultIfEmpty()

                                                     join authorizedPersonTypeCompany in db.Set<AuthorizedPersonType>()
                                                     on agreementTableInfo.AuthorizedPersonTypeCompanyGUID equals authorizedPersonTypeCompany.AuthorizedPersonTypeGUID into ljauthorizedPersonTypeCompany
                                                     from authorizedPersonTypeCompany in ljauthorizedPersonTypeCompany.DefaultIfEmpty()

                                                     join authorizedPersonCompany1 in db.Set<EmployeeTable>()
                                                     on agreementTableInfo.AuthorizedPersonTransCompany1GUID equals authorizedPersonCompany1.EmployeeTableGUID into ljauthorizedPersonCompany1
                                                     from authorizedPersonCompany1 in ljauthorizedPersonCompany1.DefaultIfEmpty()

                                                     join authorizedPersonCompany2 in db.Set<EmployeeTable>()
                                                     on agreementTableInfo.AuthorizedPersonTransCompany2GUID equals authorizedPersonCompany2.EmployeeTableGUID into ljauthorizedPersonCompany2
                                                     from authorizedPersonCompany2 in ljauthorizedPersonCompany2.DefaultIfEmpty()

                                                     join agreementTableInfoText in db.Set<AgreementTableInfoText>()
                                                     on agreementTableInfo.AgreementTableInfoGUID equals agreementTableInfoText.AgreementTableInfoGUID into ljagreementTableInfoText
                                                     from agreementTableInfoText in ljagreementTableInfoText.DefaultIfEmpty()

                                                     join assignmentAgreementTable2 in Entity
                                                     on assignmentAgreementTable.RefAssignmentAgreementTableGUID equals assignmentAgreementTable2.AssignmentAgreementTableGUID into ljassignmentAgreementTable2
                                                     from assignmentAgreementTable2 in ljassignmentAgreementTable2.DefaultIfEmpty()

                                                     join agreementTableInfo2 in db.Set<AgreementTableInfo>()
                                                     on new { assignmentAgreementTable2.AssignmentAgreementTableGUID, RefType = (int)RefType.AssignmentAgreement, assignmentAgreementTable.AgreementDocType }
                                                     equals new { AssignmentAgreementTableGUID = agreementTableInfo2.RefGUID, agreementTableInfo2.RefType, AgreementDocType = (int)AgreementDocType.New } into ljagreementTableInfo2
                                                     from agreementTableInfo2 in ljagreementTableInfo2.DefaultIfEmpty()

                                                     join custBank in db.Set<CustBank>()
                                                     on assignmentAgreementTable.CustBankGUID equals custBank.CustBankGUID into ljcustBank
                                                     from custBank in ljcustBank.DefaultIfEmpty()

                                                     join bankGroup in db.Set<BankGroup>()
                                                     on custBank.BankGroupGUID equals bankGroup.BankGroupGUID into ljbankGroup
                                                     from bankGroup in ljbankGroup.DefaultIfEmpty()

                                                     join bankType in db.Set<BankType>()
                                                     on custBank.BankTypeGUID equals bankType.BankTypeGUID into ljbankType
                                                     from bankType in ljbankType.DefaultIfEmpty()

                                                     join buyerTable in db.Set<BuyerTable>()
                                                     on assignmentAgreementTable.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljBuyerTable
                                                     from buyerTable in ljBuyerTable.DefaultIfEmpty()

                                                     where assignmentAgreementTable.AssignmentAgreementTableGUID == refGuid
                                                     select new CancelAssignmentAgreementView()
                                                     {
                                                         AgreementDate = assignmentAgreementTable2.AgreementDate.DateToString(),
                                                         AgreementCancelDate = assignmentAgreementTable.AgreementDate.DateToString(),
                                                         AssignmentManualNo1 = QBookmarkDocumentTrans.ReferenceExternalId,
                                                         AssignmentNo3 = assignmentAgreementTable2.AssignmentAgreementId,
                                                         AssignmentTextCancel1 = agreementTableInfoText != null ? agreementTableInfoText.Text1 : "",
                                                         ManualNo1 = QBookmarkDocumentTrans.ReferenceExternalId,
                                                         CustName1 = assignmentAgreementTable.CustomerName,
                                                         Dear1 = assignmentAgreementTable.ToWhomConcern,
                                                         Dear2 = assignmentAgreementTable.ToWhomConcern,
                                                         TextDateAssignment1 = QBookmarkDocumentTrans.ReferenceExternalDate,
                                                         TextDetailinvoice = QBookmarkDocumentTrans.Remark,
                                                         CompanyName1 = agreementTableInfo.CompanyName,
                                                         CompanyName2 = agreementTableInfo.CompanyName,
                                                         CustName2 = assignmentAgreementTable.CustomerName,
                                                         TextDateAssignment2 = QBookmarkDocumentTrans.ReferenceExternalDate,
                                                         CompanyName3 = agreementTableInfo.CompanyName,
                                                         CompanyName4 = agreementTableInfo.CompanyName,
                                                         CustName3 = assignmentAgreementTable.CustomerName,
                                                         CustName4 = assignmentAgreementTable.CustomerName,
                                                         CustBankName1 = bankGroup != null ? bankGroup.Description : "",
                                                         CustBankBranch1 = custBank != null ? custBank.BankBranch : "",
                                                         CustBankType1 = bankType != null ? bankType.Description : "",
                                                         MessengerAddress1 = assignmentAgreementTable.CancelAuthorityPersonAddress,
                                                         MessengerID1 = assignmentAgreementTable.CancelAuthorityPersonID,
                                                         MessengerName1 = assignmentAgreementTable.CancelAuthorityPersonName,
                                                         CustBankNo1 = custBank != null ? custBank.AccountNumber : "",
                                                         PositionReceive1 = authorizedPersonTypeBuyer != null ? authorizedPersonTypeBuyer.Description : "",
                                                         CompanyName5 = agreementTableInfo.CompanyName,
                                                         Dear3 = assignmentAgreementTable.ToWhomConcern,
                                                         AuthorityPersonFirst1 = authorizedPersonCompany1 != null ? authorizedPersonCompany1.Name : "",
                                                         AuthorityPersonSecond1 = authorizedPersonCompany2 != null ? authorizedPersonCompany2.Name : "",
                                                         TextDetailBuyer1 = assignmentAgreementTable.ReferenceAgreementId,
                                                         PositionLIT1 = authorizedPersonTypeCompany != null ? authorizedPersonTypeCompany.Description : "",
                                                         ReceivedName = buyerTable.BuyerName,
                                                         CustName = assignmentAgreementTable.CustomerName
                                                     }
                                                  ).AsNoTracking().FirstOrDefault();


                cancelAssignmentAgreementView.AssignmentCancelDate1 = DateToWordCustom(cancelAssignmentAgreementView.AgreementCancelDate.StringToDate(), TextConstants.TH, TextConstants.DMY);
                cancelAssignmentAgreementView.AssignmentDate1 = DateToWordCustom(cancelAssignmentAgreementView.AgreementDate.StringToDate(), TextConstants.TH, TextConstants.DMY);
                cancelAssignmentAgreementView.AssignmentDate2 = DateToWordCustom(cancelAssignmentAgreementView.AgreementDate.StringToDate(), TextConstants.TH, TextConstants.DMY);
                cancelAssignmentAgreementView.AssignmentDate3 = DateToWordCustom(cancelAssignmentAgreementView.AgreementDate.StringToDate(), TextConstants.TH, TextConstants.DMY);

                return cancelAssignmentAgreementView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region AssignmentAgreementBookmark
        public AssignmentAgreementBookmarkView GetAssignmentAgreementBookmarkValue(Guid refGuid, Guid bookmarkDocumentTransGuid)
        {
            try
            {
                // QAssignmentAgreementTable
                return (from assignmentAgreementTable in Entity.Where(w => w.AssignmentAgreementTableGUID == refGuid)
                            // QAgreementTableInfo
                        join agreementTableInfo in db.Set<AgreementTableInfo>().Where(w => w.RefType == (int)RefType.AssignmentAgreement)
                        on assignmentAgreementTable.AssignmentAgreementTableGUID equals agreementTableInfo.RefGUID
                        into ljAgreementTableInfo
                        from agreementTableInfo in ljAgreementTableInfo.DefaultIfEmpty()

                            // QBuyerAuthorizedPerson1
                        join authorizedPersonTransBuyer1 in db.Set<AuthorizedPersonTrans>()
                        on agreementTableInfo.AuthorizedPersonTransBuyer1GUID equals authorizedPersonTransBuyer1.AuthorizedPersonTransGUID
                        into ljPersonTrans1
                        from authorizedPersonTransBuyer1 in ljPersonTrans1.DefaultIfEmpty()
                        join relatedPersonTableBuyer1 in db.Set<RelatedPersonTable>()
                        on authorizedPersonTransBuyer1.RelatedPersonTableGUID equals relatedPersonTableBuyer1.RelatedPersonTableGUID
                        into ljRelatedPersonTableBuyer1
                        from relatedPersonTableBuyer1 in ljRelatedPersonTableBuyer1.DefaultIfEmpty()

                            // QBuyerAuthorizedPerson2
                        join authorizedPersonTransBuyer2 in db.Set< AuthorizedPersonTrans >()
                        on agreementTableInfo.AuthorizedPersonTransBuyer2GUID equals authorizedPersonTransBuyer2.AuthorizedPersonTransGUID
                        into ljPersonTrans2
                        from authorizedPersonTransBuyer2 in ljPersonTrans2.DefaultIfEmpty()
                        join relatedPersonTableBuyer2 in db.Set<RelatedPersonTable>()
                        on authorizedPersonTransBuyer2.RelatedPersonTableGUID equals relatedPersonTableBuyer2.RelatedPersonTableGUID        
                        into ljRelatedPersonTableBuyer2
                        from relatedPersonTableBuyer2 in ljRelatedPersonTableBuyer2.DefaultIfEmpty()

                            // QBuyerAuthorizedPerson3
                        join authorizedPersonTransBuyer3 in db.Set<AuthorizedPersonTrans>()
                        on agreementTableInfo.AuthorizedPersonTransBuyer3GUID equals authorizedPersonTransBuyer3.AuthorizedPersonTransGUID
                        into ljPersonTrans3
                        from authorizedPersonTransBuyer3 in ljPersonTrans3.DefaultIfEmpty()
                        join relatedPersonTableBuyer3 in db.Set<RelatedPersonTable>()
                        on authorizedPersonTransBuyer3.RelatedPersonTableGUID equals relatedPersonTableBuyer3.RelatedPersonTableGUID
                        into ljRelatedPersonTableBuyer3
                        from relatedPersonTableBuyer3 in ljRelatedPersonTableBuyer3.DefaultIfEmpty()

                            // QCustomerTable
                        join customerTable in db.Set<CustomerTable>()
                        on assignmentAgreementTable.CustomerTableGUID equals customerTable.CustomerTableGUID
                        
                        // QCustBank
                        join custBank in db.Set<CustBank>()
                        on assignmentAgreementTable.CustBankGUID equals custBank.CustBankGUID into ljCustBank
                        from custBank in ljCustBank.DefaultIfEmpty()

                        join cusBankGroup in db.Set<BankGroup>()
                        on custBank.BankGroupGUID equals cusBankGroup.BankGroupGUID into ljCustBankGroup
                        from cusBankGroup in ljCustBankGroup.DefaultIfEmpty()

                        join custBankType in db.Set<BankType>()
                        on custBank.BankTypeGUID equals custBankType.BankTypeGUID into ljCustBankType
                        from custBankType in ljCustBankType.DefaultIfEmpty()

                            // QAgreementTableInfoText
                        join agreementTableInfoText in db.Set<AgreementTableInfoText>()
                        on agreementTableInfo.AgreementTableInfoGUID equals agreementTableInfoText.AgreementTableInfoGUID
                        into ljAgreementTableInfoText
                        from agreementTableInfoText in ljAgreementTableInfoText.DefaultIfEmpty()

                            // QCustomerAuthorizedPerson1
                        join authorizedPersonTrans1 in db.Set<AuthorizedPersonTrans>()
                        on agreementTableInfo.AuthorizedPersonTransCustomer1GUID equals authorizedPersonTrans1.AuthorizedPersonTransGUID
                        into ljAuthorizedPersonTrans1
                        from authorizedPersonTrans1 in ljAuthorizedPersonTrans1.DefaultIfEmpty()
                        join relatedPersonTable1 in db.Set<RelatedPersonTable>()
                        on authorizedPersonTrans1.RelatedPersonTableGUID equals relatedPersonTable1.RelatedPersonTableGUID
                        into ljRelatedPersonTable1
                        from relatedPersonTable1 in ljRelatedPersonTable1.DefaultIfEmpty()

                            // QCustomerAuthorizedPerson2
                        join authorizedPersonTrans2 in db.Set<AuthorizedPersonTrans>()
                        on agreementTableInfo.AuthorizedPersonTransCustomer2GUID equals authorizedPersonTrans2.AuthorizedPersonTransGUID
                        into ljAuthorizedPersonTrans2
                        from authorizedPersonTrans2 in ljAuthorizedPersonTrans2.DefaultIfEmpty()
                        join relatedPersonTable2 in db.Set<RelatedPersonTable>()
                        on authorizedPersonTrans2.RelatedPersonTableGUID equals relatedPersonTable2.RelatedPersonTableGUID
                        into ljRelatedPersonTable2
                        from relatedPersonTable2 in ljRelatedPersonTable2.DefaultIfEmpty()

                            // QCustomerAuthorizedPerson3
                        join authorizedPersonTrans3 in db.Set<AuthorizedPersonTrans>()
                        on agreementTableInfo.AuthorizedPersonTransCustomer3GUID equals authorizedPersonTrans3.AuthorizedPersonTransGUID
                        into ljAuthorizedPersonTrans3
                        from authorizedPersonTrans3 in ljAuthorizedPersonTrans3.DefaultIfEmpty()
                        join relatedPersonTable3 in db.Set<RelatedPersonTable>()
                        on authorizedPersonTrans3.RelatedPersonTableGUID equals relatedPersonTable3.RelatedPersonTableGUID
                        into ljRelatedPersonTable3
                        from relatedPersonTable3 in ljRelatedPersonTable3.DefaultIfEmpty()

                            // QAuthorizedPersonTypeCustomer
                        join authorizedPersonTypeCustomer in db.Set<AuthorizedPersonType>()
                        on agreementTableInfo.AuthorizedPersonTypeCustomerGUID equals authorizedPersonTypeCustomer.AuthorizedPersonTypeGUID
                        into ljAuthorizedPersonTypeCustomer
                        from customerAuthorizedPersonType in ljAuthorizedPersonTypeCustomer.DefaultIfEmpty()

                            // QCompanyAuthorizedPerson1=
                        join authorizedPersonCompany1 in db.Set<EmployeeTable>()
                        on agreementTableInfo.AuthorizedPersonTransCompany1GUID equals authorizedPersonCompany1.EmployeeTableGUID
                        into ljAuthorizedPersonCompany1
                        from authorizedPersonCompany1 in ljAuthorizedPersonCompany1.DefaultIfEmpty()

                            // QCompanyAuthorizedPerson2
                        join authorizedPersonCompany2 in db.Set<EmployeeTable>()
                        on agreementTableInfo.AuthorizedPersonTransCompany2GUID equals authorizedPersonCompany2.EmployeeTableGUID
                        into ljAuthorizedPersonCompany2
                        from authorizedPersonCompany2 in ljAuthorizedPersonCompany2.DefaultIfEmpty()

                            // QCompanyAuthorizedPerson3
                        join authorizedPersonCompany3 in db.Set<EmployeeTable>()
                        on agreementTableInfo.AuthorizedPersonTransCompany3GUID equals authorizedPersonCompany3.EmployeeTableGUID
                        into ljAuthorizedPersonCompany3
                        from authorizedPersonCompany3 in ljAuthorizedPersonCompany3.DefaultIfEmpty()

                            // QAuthorizedPersonTypeCompany
                        join authorizedPersonTypeCompany in db.Set<AuthorizedPersonType>()
                        on agreementTableInfo.AuthorizedPersonTypeCompanyGUID equals authorizedPersonTypeCompany.AuthorizedPersonTypeGUID
                        into ljAuthorizedPersonTypeCompany
                        from authorizedPersonTypeCompany in ljAuthorizedPersonTypeCompany.DefaultIfEmpty()

                            // QWitnessCompany1
                        join witnessCompany1 in db.Set<EmployeeTable>()
                        on agreementTableInfo.WitnessCompany1GUID equals witnessCompany1.EmployeeTableGUID
                        into ljWitnessCompany1
                        from witnessCompany1 in ljWitnessCompany1.DefaultIfEmpty()

                            // QWitnessCompany1
                        join witnessCompany2 in db.Set<EmployeeTable>()
                        on agreementTableInfo.WitnessCompany2GUID equals witnessCompany2.EmployeeTableGUID
                        into ljWitnessCompany2
                        from witnessCompany2 in ljWitnessCompany2.DefaultIfEmpty()

                            // QCompanyParameter
                        join companyParameter in db.Set<CompanyParameter>()
                        on assignmentAgreementTable.CompanyGUID equals companyParameter.CompanyGUID

                            // QBookmarkDocumentTrans
                        join bookmarkDocumentTrans in db.Set<BookmarkDocumentTrans>()
                        on bookmarkDocumentTransGuid equals bookmarkDocumentTrans.BookmarkDocumentTransGUID

                        

                        select new AssignmentAgreementBookmarkView
                        {
                            // QAssignmentAgreementTable
                            AssignmentDate = assignmentAgreementTable.AgreementDate,

                            AssignmentNo1 = assignmentAgreementTable.AssignmentAgreementId,

                            AssignmentTextFromMasterBuyer = assignmentAgreementTable.AssignmentProductDescription,

                            Dear1 = assignmentAgreementTable.ToWhomConcern,
                            Dear2 = assignmentAgreementTable.ToWhomConcern,
                            Dear3 = assignmentAgreementTable.ToWhomConcern,
                            Dear4 = assignmentAgreementTable.ToWhomConcern,
                            Dear5 = assignmentAgreementTable.ToWhomConcern,

                            TextProvinceRegis1 = assignmentAgreementTable.CustRegisteredLocation,

                            ReceivedName1 = assignmentAgreementTable.BuyerName,
                            ReceivedName2 = assignmentAgreementTable.BuyerName,
                            ReceivedName3 = assignmentAgreementTable.BuyerName,
                            ReceivedName4 = assignmentAgreementTable.BuyerName,
                            ReceivedName5 = assignmentAgreementTable.BuyerName,

                            CustName1 = assignmentAgreementTable.CustomerName,
                            CustName2 = assignmentAgreementTable.CustomerName,
                            CustName3 = assignmentAgreementTable.CustomerName,
                            CustName4 = assignmentAgreementTable.CustomerName,
                            CustName5 = assignmentAgreementTable.CustomerName,
                            CustName6 = assignmentAgreementTable.CustomerName,
                            CustName7 = assignmentAgreementTable.CustomerName,
                            CustName8 = assignmentAgreementTable.CustomerName,
                            CustName9 = assignmentAgreementTable.CustomerName,
                            CustName10 = assignmentAgreementTable.CustomerName,
                            CustName11 = assignmentAgreementTable.CustomerName,
                            CustName12 = assignmentAgreementTable.CustomerName,
                            CustName13 = assignmentAgreementTable.CustomerName,
                            CustName14 = assignmentAgreementTable.CustomerName,

                            AssignmentPositionLIT1 = authorizedPersonTypeCompany != null ? authorizedPersonTypeCompany.Description : "",
                            CustBankBranch1 = custBank != null ? custBank.BankBranch : "",
                            CustBankBranch2 = custBank != null ? custBank.BankBranch : "",
                            CustBankBranch3 = custBank != null ? custBank.BankBranch : "",
                            CustBankName1 = cusBankGroup != null ? cusBankGroup.Description : "",
                            CustBankName2 = cusBankGroup != null ? cusBankGroup.Description : "",
                            CustBankName3 = cusBankGroup != null ? cusBankGroup.Description : "",
                            CustBankNo1 = custBank != null ? custBank.AccountNumber : "",
                            CustBankNo2 = custBank != null ? custBank.AccountNumber : "",
                            CustBankType1 = custBankType != null ? custBankType.Description : "",

                            // QAgreementTableInfo
                            CustAddress1 = agreementTableInfo.CustomerAddress1 + " " + agreementTableInfo.CustomerAddress2,

                            AuthorityPersonFirst1 = authorizedPersonCompany1 != null ? authorizedPersonCompany1.Name : "",
                            AuthorityPersonFirst2 = authorizedPersonCompany1 != null ? authorizedPersonCompany1.Name : "",
                            AuthorityPersonFirst3 = authorizedPersonCompany1 != null ? authorizedPersonCompany1.Name : "",
                            AuthorityPersonFirst4 = authorizedPersonCompany1 != null ? authorizedPersonCompany1.Name : "",
                            AuthorityPersonFirst5 = authorizedPersonCompany1 != null ? authorizedPersonCompany1.Name : "",

                            AuthorityPersonSecond1 = authorizedPersonCompany2 != null ? authorizedPersonCompany2.Name : "",
                            AuthorityPersonSecond2 = authorizedPersonCompany2 != null ? authorizedPersonCompany2.Name : "",
                            AuthorityPersonSecond3 = authorizedPersonCompany2 != null ? authorizedPersonCompany2.Name : "",
                            AuthorityPersonSecond4 = authorizedPersonCompany2 != null ? authorizedPersonCompany2.Name : "",
                            AuthorityPersonSecond5 = authorizedPersonCompany2 != null ? authorizedPersonCompany2.Name : "",

                            AuthorityPersonThird1 = authorizedPersonCompany3 != null ? authorizedPersonCompany3.Name : "",
                            AuthorityPersonThird2 = authorizedPersonCompany3 != null ? authorizedPersonCompany3.Name : "",
                            AuthorityPersonThird3 = authorizedPersonCompany3 != null ? authorizedPersonCompany3.Name : "",
                            AuthorityPersonThird4 = authorizedPersonCompany3 != null ? authorizedPersonCompany3.Name : "",
                            AuthorityPersonThird5 = authorizedPersonCompany3 != null ? authorizedPersonCompany3.Name : "",

                            WitnessFirst1 = witnessCompany1 != null ? witnessCompany1.Name : "",
                            WitnessSecond1 = witnessCompany2 != null ? witnessCompany2.Name : "",

                            // QAgreementTableInfoText
                            AssignmentTextFirst1 = agreementTableInfoText != null ? agreementTableInfoText.Text1 : "",
                            AssignmentTextFirst2 = agreementTableInfoText != null ? agreementTableInfoText.Text1 : "",
                            AssignmentTextFirst3 = agreementTableInfoText != null ? agreementTableInfoText.Text1 : "",
                            AssignmentTextFirst4 = agreementTableInfoText != null ? agreementTableInfoText.Text1 : "",

                            AssignmentTextSecond1 = agreementTableInfoText != null ? agreementTableInfoText.Text2 : "",
                            AssignmentTextSecond2 = agreementTableInfoText != null ? agreementTableInfoText.Text2 : "",
                            AssignmentTextSecond3 = agreementTableInfoText != null ? agreementTableInfoText.Text2 : "",

                            AssignmentTextThird1 = agreementTableInfoText != null ? agreementTableInfoText.Text3 : "",
                            AssignmentTextThird2 = agreementTableInfoText != null ? agreementTableInfoText.Text3 : "",
                            AssignmentTextThird3 = agreementTableInfoText != null ? agreementTableInfoText.Text3 : "",

                            AssignmentTextFour1 = agreementTableInfoText != null ? agreementTableInfoText.Text4 : "",
                            AssignmentTextFour2 = agreementTableInfoText != null ? agreementTableInfoText.Text4 : "",
                            AssignmentTextFour3 = agreementTableInfoText != null ? agreementTableInfoText.Text4 : "",

                            // QCustomerAuthorizedPerson1
                            AuthorizedCustFirst1 = relatedPersonTable1 != null ? relatedPersonTable1.Name : "",
                            AuthorizedCustFirst2 = relatedPersonTable1 != null ? relatedPersonTable1.Name : "",
                            AuthorizedCustFirst3 = relatedPersonTable1 != null ? relatedPersonTable1.Name : "",
                            AuthorizedCustFirst4 = relatedPersonTable1 != null ? relatedPersonTable1.Name : "",
                            AuthorizedCustFirst5 = relatedPersonTable1 != null ? relatedPersonTable1.Name : "",
                            AuthorizedCustFirst6 = relatedPersonTableBuyer1 != null ? relatedPersonTableBuyer1.Name : "",
                            AuthorizedCustFirst7 = relatedPersonTableBuyer1 != null ? relatedPersonTableBuyer1.Name : "",
                            AuthorizedCustFirst8 = relatedPersonTableBuyer1 != null ? relatedPersonTableBuyer1.Name : "",
                            AuthorizedCustFirst9 = relatedPersonTableBuyer1 != null ? relatedPersonTableBuyer1.Name : "",
                            AuthorizedCustFirst10 = relatedPersonTableBuyer1 != null ? relatedPersonTableBuyer1.Name : "",

                            // QCustomerAuthorizedPerson2
                            AuthorizedCustSecond1 = relatedPersonTableBuyer2 != null ? relatedPersonTableBuyer2.Name : "",
                            AuthorizedCustSecond2 = relatedPersonTableBuyer2 != null ? relatedPersonTableBuyer2.Name : "",
                            AuthorizedCustSecond3 = relatedPersonTableBuyer2 != null ? relatedPersonTableBuyer2.Name : "",
                            AuthorizedCustSecond4 = relatedPersonTableBuyer2 != null ? relatedPersonTableBuyer2.Name : "",
                            AuthorizedCustSecond5 = relatedPersonTableBuyer2 != null ? relatedPersonTableBuyer2.Name : "",
                            AuthorizedCustSecond6 = relatedPersonTable2 != null ? relatedPersonTable2.Name : "",
                            AuthorizedCustSecond7 = relatedPersonTable2 != null ? relatedPersonTable2.Name : "",
                            AuthorizedCustSecond8 = relatedPersonTable2 != null ? relatedPersonTable2.Name : "",
                            AuthorizedCustSecond9 = relatedPersonTable2 != null ? relatedPersonTable2.Name : "",
                            AuthorizedCustSecond10 = relatedPersonTable2 != null ? relatedPersonTable2.Name : "",

                            // QCustomerAuthorizedPerson3
                            AuthorizedCustThird1 = relatedPersonTable3 != null ? relatedPersonTable3.Name : "",
                            AuthorizedCustThird2 = relatedPersonTable3 != null ? relatedPersonTable3.Name : "",
                            AuthorizedCustThird3 = relatedPersonTable3 != null ? relatedPersonTable3.Name : "",
                            AuthorizedCustThird4 = relatedPersonTable3 != null ? relatedPersonTable3.Name : "",
                            AuthorizedCustThird5 = relatedPersonTable3 != null ? relatedPersonTable3.Name : "",
                            AuthorizedCustThird6 = relatedPersonTableBuyer3 != null ? relatedPersonTableBuyer3.Name : "",
                            AuthorizedCustThird7 = relatedPersonTableBuyer3 != null ? relatedPersonTableBuyer3.Name : "",
                            AuthorizedCustThird8 = relatedPersonTableBuyer3 != null ? relatedPersonTableBuyer3.Name : "",
                            AuthorizedCustThird9 = relatedPersonTableBuyer3 != null ? relatedPersonTableBuyer3.Name : "",
                            AuthorizedCustThird10 = relatedPersonTableBuyer3 != null ? relatedPersonTableBuyer3.Name : "",

                            // QCustomerTable
                            CustDateRegis = customerTable.DateOfEstablish,
                            CustTaxID1 = customerTable.TaxID,

                            // QCustomerAuthorizedPersonType
                            PositionCust1 = customerAuthorizedPersonType != null ? customerAuthorizedPersonType.Description : "",
                            PositionCust2 = customerAuthorizedPersonType != null ? customerAuthorizedPersonType.Description : "",

                            // QAuthorizedPersonTypeCompany
                            PositionLIT1 = authorizedPersonTypeCompany != null ? authorizedPersonTypeCompany.Description : "",
                            PositionLIT2 = authorizedPersonTypeCompany != null ? authorizedPersonTypeCompany.Description : "",
                            PositionLIT3 = authorizedPersonTypeCompany != null ? authorizedPersonTypeCompany.Description : "",
                            PositionLIT4 = authorizedPersonTypeCompany != null ? authorizedPersonTypeCompany.Description : "",
                            PositionLIT5 = authorizedPersonTypeCompany != null ? authorizedPersonTypeCompany.Description : "",

                            // QCompanyParameter
                            InterestRate1 = companyParameter.MaxInterestPct.ToString(),

                            // QBookmarkDocumentTrans
                            AssignmentManualDate1 = bookmarkDocumentTrans != null ? bookmarkDocumentTrans.ReferenceExternalDate : "",
                            AssignmentManualDate2 = bookmarkDocumentTrans != null ? bookmarkDocumentTrans.ReferenceExternalDate : "",
                            AssignmentManualDate3 = bookmarkDocumentTrans != null ? bookmarkDocumentTrans.ReferenceExternalDate : "",
                            AssignmentManualNo1 = bookmarkDocumentTrans != null ? bookmarkDocumentTrans.ReferenceExternalId : "",
                            AssignmentManualNo2 = bookmarkDocumentTrans != null ? bookmarkDocumentTrans.ReferenceExternalId : "",
                            AssignmentMemoManualDate1 = bookmarkDocumentTrans != null ? bookmarkDocumentTrans.ReferenceExternalDate : "",
                            AssignmentMemoManualDate2 = bookmarkDocumentTrans != null ? bookmarkDocumentTrans.ReferenceExternalDate : "",
                            AssignmentMemoManualNo1 = bookmarkDocumentTrans != null ? bookmarkDocumentTrans.ReferenceExternalId : "",
                            AssignmentMemoManualNo2 = bookmarkDocumentTrans != null ? bookmarkDocumentTrans.ReferenceExternalId : "",
                            TextAuthorizationDate1 = bookmarkDocumentTrans != null ? bookmarkDocumentTrans.ReferenceExternalDate : ""

                        }).AsNoTracking().FirstOrDefault();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Validate
        public override void ValidateUpdate(IEnumerable<AssignmentAgreementTable> items)
        {
            base.ValidateUpdate(items);
            items.ToList().ForEach(f => CheckDuplicateAssignmentAgreementId(f));
        }
        public override void ValidateAdd(IEnumerable<AssignmentAgreementTable> items)
        {
            base.ValidateAdd(items);
            items.ToList().ForEach(f => CheckDuplicateAssignmentAgreementId(f));
        }
        public void CheckDuplicateAssignmentAgreementId(AssignmentAgreementTable item)
        {
            try
            {
                SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
                bool isDuplicateAssignmentAgreementId = IsDuplicateAssignmentAgreementId(item.AssignmentAgreementTableGUID, item.AssignmentAgreementId);
                if (isDuplicateAssignmentAgreementId)
                {
                    smartAppException.AddData("ERROR.DUPLICATE", new string[] { "LABEL.ASSIGNMENT_AGREEMENT_ID" });
                }
                if (smartAppException.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(smartAppException);
                }
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public bool IsDuplicateAssignmentAgreementId(Guid guid, string id)
        {
            bool isDuplicateAssignmentAgreementId = Entity.Any(a => a.AssignmentAgreementId == id
                                                                    && a.CompanyGUID == db.GetCompanyFilter().StringToGuid()
                                                                    && !string.IsNullOrEmpty(a.AssignmentAgreementId)
                                                                    && a.AssignmentAgreementTableGUID != guid);
            return isDuplicateAssignmentAgreementId;
        }
        #endregion Validate

        private string DateToWordCustom(DateTime? dattime, string lang, string format)
        {
            try
            {
                if (dattime != null)
                {
                    ISharedService sharedService = new SharedService(db);
                    return sharedService.DateToWord((DateTime)dattime, lang, format);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }
    }
}

