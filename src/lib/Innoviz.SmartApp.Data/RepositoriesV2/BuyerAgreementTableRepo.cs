using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IBuyerAgreementTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<BuyerAgreementTableListView> GetListvw(SearchParameter search);
		BuyerAgreementTableItemView GetByIdvw(Guid id);
		BuyerAgreementTable Find(params object[] keyValues);
		BuyerAgreementTable GetBuyerAgreementTableByIdNoTracking(Guid guid);
		BuyerAgreementTable GetBuyerAgreementTableByIdNoTrackingByAccessLevel(Guid guid);
		BuyerAgreementTable CreateBuyerAgreementTable(BuyerAgreementTable buyerAgreementTable);
		void CreateBuyerAgreementTableVoid(BuyerAgreementTable buyerAgreementTable);
		BuyerAgreementTable UpdateBuyerAgreementTable(BuyerAgreementTable dbBuyerAgreementTable, BuyerAgreementTable inputBuyerAgreementTable, List<string> skipUpdateFields = null);
		void UpdateBuyerAgreementTableVoid(BuyerAgreementTable dbBuyerAgreementTable, BuyerAgreementTable inputBuyerAgreementTable, List<string> skipUpdateFields = null);
		void Remove(BuyerAgreementTable item);
		IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetDropDownItemByBuyerAgreementTrans(SearchParameter search);
		IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetDropDownItemByPurchaseLine(SearchParameter search);
		IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetDropDownItemByAssignmentAgreement(SearchParameter search);
		IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetDropDownItemByWithdrawalTable(SearchParameter search);
		#region function
		BuyerAgreementTableItemView GetBuyerAgreementTableSumLineById(Guid guid);
		#endregion
		void ValidateAdd(BuyerAgreementTable item);
		void ValidateAdd(IEnumerable<BuyerAgreementTable> items);
	}
    public class BuyerAgreementTableRepo : CompanyBaseRepository<BuyerAgreementTable>, IBuyerAgreementTableRepo
	{
		public BuyerAgreementTableRepo(SmartAppDbContext context) : base(context) { }
		public BuyerAgreementTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BuyerAgreementTable GetBuyerAgreementTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BuyerAgreementTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BuyerAgreementTable GetBuyerAgreementTableByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BuyerAgreementTableGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<BuyerAgreementTableItemViewMap> GetDropDownQuery()
		{
			return (from buyerAgreementTable in Entity
					select new BuyerAgreementTableItemViewMap
					{
						CompanyGUID = buyerAgreementTable.CompanyGUID,
						Owner = buyerAgreementTable.Owner,
						OwnerBusinessUnitGUID = buyerAgreementTable.OwnerBusinessUnitGUID,
						BuyerAgreementTableGUID = buyerAgreementTable.BuyerAgreementTableGUID,
						ReferenceAgreementID = buyerAgreementTable.ReferenceAgreementID,
						Description = buyerAgreementTable.Description,
						BuyerAgreementId = buyerAgreementTable.BuyerAgreementId,
						BuyerAgreementTable_Values = SmartAppUtil.GetDropDownLabel(buyerAgreementTable.BuyerAgreementId, buyerAgreementTable.Description),
						BuyerTableGUID = buyerAgreementTable.BuyerTableGUID,
						CustomerTableGUID = buyerAgreementTable.CustomerTableGUID
					});
		}
		private IQueryable<BuyerAgreementTableItemViewMap> GetDropDownQueryByBuyerAgreementTrans()
		{
			return (from buyerAgreementTable in Entity
					join creditAppRequestLine in db.Set<CreditAppRequestLine>()
					on buyerAgreementTable.BuyerTableGUID equals creditAppRequestLine.BuyerTableGUID
					join creditAppRequestTable in db.Set<CreditAppRequestTable>()
					on new { buyerAgreementTable.CustomerTableGUID, creditAppRequestLine.CreditAppRequestTableGUID } 
					equals new { creditAppRequestTable.CustomerTableGUID, creditAppRequestTable.CreditAppRequestTableGUID}
					select new BuyerAgreementTableItemViewMap
					{
						CompanyGUID = buyerAgreementTable.CompanyGUID,
						Owner = buyerAgreementTable.Owner,
						OwnerBusinessUnitGUID = buyerAgreementTable.OwnerBusinessUnitGUID,
						BuyerAgreementTableGUID = buyerAgreementTable.BuyerAgreementTableGUID,
						ReferenceAgreementID = buyerAgreementTable.ReferenceAgreementID,
						Description = buyerAgreementTable.Description,
						BuyerAgreementId = buyerAgreementTable.BuyerAgreementId,
						CreditAppRequestLine_CreditAppRequestLineGUID = creditAppRequestLine.CreditAppRequestLineGUID,
						BuyerAgreementTable_Values = SmartAppUtil.GetDropDownLabel(buyerAgreementTable.BuyerAgreementId, buyerAgreementTable.Description)
					});
		}
		private IQueryable<BuyerAgreementTableItemViewMap> GetDropDownQueryByPurchaseLine()
		{
			return (from buyerAgreementTable in Entity
					join buyerAgreementTrans in db.Set<BuyerAgreementTrans>()
					on buyerAgreementTable.BuyerAgreementTableGUID equals buyerAgreementTrans.BuyerAgreementTableGUID
					select new BuyerAgreementTableItemViewMap
					{ 
						CompanyGUID = buyerAgreementTable.CompanyGUID,
						Owner = buyerAgreementTable.Owner,
						OwnerBusinessUnitGUID = buyerAgreementTable.OwnerBusinessUnitGUID,
						BuyerAgreementTableGUID = buyerAgreementTable.BuyerAgreementTableGUID,
						ReferenceAgreementID = buyerAgreementTable.ReferenceAgreementID,
						Description = buyerAgreementTable.Description,
						BuyerAgreementId = buyerAgreementTable.BuyerAgreementId,
						BuyerAgreementTrans_RefGUID = buyerAgreementTrans.RefGUID,
						BuyerTableGUID = buyerAgreementTable.BuyerTableGUID,
						BuyerAgreementTable_Values = SmartAppUtil.GetDropDownLabel(buyerAgreementTable.BuyerAgreementId, buyerAgreementTable.Description)
					});
		}
		private IQueryable<BuyerAgreementTableItemViewMap> GetDropDownQueryJoinBuyerAgreementLine()
		{
			return (from buyerAgreementTable in Entity
					join buyerAgreementLine in db.Set<BuyerAgreementLine>()
					.GroupBy(g => g.BuyerAgreementTableGUID)
					.Select(s => new { BuyerAgreementTableGUID = s.Key, BuyerAgreementAmount = s.Sum(su => su.Amount) })
					on buyerAgreementTable.BuyerAgreementTableGUID equals buyerAgreementLine.BuyerAgreementTableGUID
					into ljBuyerAgreementLine
					from buyerAgreementLine in ljBuyerAgreementLine.DefaultIfEmpty()
					select new BuyerAgreementTableItemViewMap
					{
						CompanyGUID = buyerAgreementTable.CompanyGUID,
						Owner = buyerAgreementTable.Owner,
						OwnerBusinessUnitGUID = buyerAgreementTable.OwnerBusinessUnitGUID,
						BuyerAgreementTableGUID = buyerAgreementTable.BuyerAgreementTableGUID,
						ReferenceAgreementID = buyerAgreementTable.ReferenceAgreementID,
						Description = buyerAgreementTable.Description,
						BuyerAgreementId = buyerAgreementTable.BuyerAgreementId,
						BuyerTableGUID = buyerAgreementTable.BuyerTableGUID,
						CustomerTableGUID = buyerAgreementTable.CustomerTableGUID,
						BuyerAgreementTable_Values = SmartAppUtil.GetDropDownLabel(buyerAgreementTable.BuyerAgreementId, buyerAgreementTable.Description),
						BuyerAgreementAmount = (buyerAgreementLine != null) ? buyerAgreementLine.BuyerAgreementAmount : 0,
					});
		}
		private IQueryable<BuyerAgreementTableItemViewMap> GetDropDownQueryByWithdrawalTable()
		{
			return (from buyerAgreementTable in Entity
					join buyerAgreementTrans in db.Set<BuyerAgreementTrans>()
					on buyerAgreementTable.BuyerAgreementTableGUID equals buyerAgreementTrans.BuyerAgreementTableGUID
					group new { buyerAgreementTable.BuyerAgreementTableGUID, buyerAgreementTrans.RefGUID, buyerAgreementTrans.RefType } by
					new
					{
						buyerAgreementTable.CompanyGUID,
						buyerAgreementTable.Owner,
						buyerAgreementTable.OwnerBusinessUnitGUID,
						buyerAgreementTable.BuyerAgreementTableGUID,
						buyerAgreementTable.ReferenceAgreementID,
						buyerAgreementTable.Description,
						buyerAgreementTable.BuyerAgreementId,
						buyerAgreementTable.BuyerTableGUID,
						buyerAgreementTable.CustomerTableGUID,
						buyerAgreementTrans.RefGUID,
						buyerAgreementTrans.RefType
					} into g
					select new BuyerAgreementTableItemViewMap
					{
						CompanyGUID = g.Key.CompanyGUID,
						Owner = g.Key.Owner,
						OwnerBusinessUnitGUID = g.Key.OwnerBusinessUnitGUID,
						BuyerAgreementTableGUID = g.Key.BuyerAgreementTableGUID,
						ReferenceAgreementID = g.Key.ReferenceAgreementID,
						Description = g.Key.Description,
						BuyerAgreementId = g.Key.BuyerAgreementId,
						BuyerTableGUID = g.Key.BuyerTableGUID,
						CustomerTableGUID = g.Key.CustomerTableGUID,
						BuyerAgreementTrans_RefGUID = g.Key.RefGUID,
						BuyerAgreementTrans_RefType = g.Key.RefType
					});
		}
		public IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<BuyerAgreementTable>(search, SysParm.CompanyGUID);
				var buyerAgreementTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<BuyerAgreementTableItemViewMap, BuyerAgreementTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return buyerAgreementTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetDropDownItemByBuyerAgreementTrans(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<BuyerAgreementTable>(search, SysParm.CompanyGUID);
				var buyerAgreementTable = GetDropDownQueryByBuyerAgreementTrans().Where(predicate.Predicates, predicate.Values).ToMaps<BuyerAgreementTableItemViewMap, BuyerAgreementTableItemView>().ToDropDownItem(true);
				return buyerAgreementTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetDropDownItemByPurchaseLine(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<BuyerAgreementTable>(search, SysParm.CompanyGUID);
				var buyerAgreementTable = GetDropDownQueryByPurchaseLine().Where(predicate.Predicates, predicate.Values).ToMaps<BuyerAgreementTableItemViewMap, BuyerAgreementTableItemView>().ToDropDownItem(true);
				return buyerAgreementTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetDropDownItemByAssignmentAgreement(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<BuyerAgreementTable>(search, SysParm.CompanyGUID);
				var buyerAgreementTable = GetDropDownQueryJoinBuyerAgreementLine().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<BuyerAgreementTableItemViewMap, BuyerAgreementTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return buyerAgreementTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetDropDownItemByWithdrawalTable(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<BuyerAgreementTable>(search, SysParm.CompanyGUID);
				var buyerAgreementTrans = GetDropDownQueryByWithdrawalTable().Where(predicate.Predicates, predicate.Values).ToMaps<BuyerAgreementTableItemViewMap, BuyerAgreementTableItemView>().ToDropDownItem(search.ExcludeRowData);
				return buyerAgreementTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<BuyerAgreementTableListViewMap> GetListQuery()
		{
			return (from buyerAgreementTable in Entity 
					join customerTable in db.Set<CustomerTable>() on buyerAgreementTable.CustomerTableGUID equals customerTable.CustomerTableGUID
					select new BuyerAgreementTableListViewMap
					{
					CompanyGUID = buyerAgreementTable.CompanyGUID,
					BuyerAgreementTableGUID = buyerAgreementTable.BuyerAgreementTableGUID,
                    CustomerTableGUID = buyerAgreementTable.CustomerTableGUID,
                    BuyerAgreementId = buyerAgreementTable.BuyerAgreementId,
                    ReferenceAgreementID = buyerAgreementTable.ReferenceAgreementID,
                    Description = buyerAgreementTable.Description,
                    StartDate = buyerAgreementTable.StartDate,
                    EndDate = buyerAgreementTable.EndDate,
                    CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
					CustomerTable_CustomerId = customerTable.CustomerId,
					buyerAgreementAmount = 0,
					BuyerTableGUID = buyerAgreementTable.BuyerTableGUID
					});;
		}
		public SearchResult<BuyerAgreementTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<BuyerAgreementTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<BuyerAgreementTable>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<BuyerAgreementTableListViewMap, BuyerAgreementTableListView>();
				result = list.SetSearchResult<BuyerAgreementTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<BuyerAgreementTableItemViewMap> GetItemQuery()
		{
			return (from buyerAgreementTable in Entity
					join company in db.Set<Company>()
					on buyerAgreementTable.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on buyerAgreementTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljBuyerAgreementTableOwnerBU
					from ownerBU in ljBuyerAgreementTableOwnerBU.DefaultIfEmpty()
					join buyerAgreementLine in db.Set<BuyerAgreementLine>()
												 .GroupBy(g => g.BuyerAgreementTableGUID)
												 .Select( s => new { BuyerAgreementTableGUID = s.Key, Amount = s.Sum(su => su.Amount)} )
					on buyerAgreementTable.BuyerAgreementTableGUID equals buyerAgreementLine.BuyerAgreementTableGUID into ljBuyerAgreementLine
					from buyerAgreementLine in ljBuyerAgreementLine.DefaultIfEmpty()
					select new BuyerAgreementTableItemViewMap
					{
						CompanyGUID = buyerAgreementTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = buyerAgreementTable.Owner,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						OwnerBusinessUnitGUID = buyerAgreementTable.OwnerBusinessUnitGUID,
						CreatedBy = buyerAgreementTable.CreatedBy,
						CreatedDateTime = buyerAgreementTable.CreatedDateTime,
						ModifiedBy = buyerAgreementTable.ModifiedBy,
						ModifiedDateTime = buyerAgreementTable.ModifiedDateTime,
						BuyerAgreementTableGUID = buyerAgreementTable.BuyerAgreementTableGUID,
						BuyerAgreementId = buyerAgreementTable.BuyerAgreementId,
						BuyerTableGUID = buyerAgreementTable.BuyerTableGUID,
						CustomerTableGUID = buyerAgreementTable.CustomerTableGUID,
						Description = buyerAgreementTable.Description,
						EndDate = buyerAgreementTable.EndDate,
						MaximumCreditLimit = buyerAgreementTable.MaximumCreditLimit,
						Penalty = buyerAgreementTable.Penalty,
						ReferenceAgreementID = buyerAgreementTable.ReferenceAgreementID,
						Remark = buyerAgreementTable.Remark,
						StartDate = buyerAgreementTable.StartDate,
						BuyerAgreementAmount = (buyerAgreementLine != null) ? buyerAgreementLine.Amount : 0,
					
						RowVersion = buyerAgreementTable.RowVersion,
					});
		}
		public BuyerAgreementTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<BuyerAgreementTableItemViewMap, BuyerAgreementTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public BuyerAgreementTable CreateBuyerAgreementTable(BuyerAgreementTable buyerAgreementTable)
		{
			try
			{
				buyerAgreementTable.BuyerAgreementTableGUID = Guid.NewGuid();
				base.Add(buyerAgreementTable);
				return buyerAgreementTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateBuyerAgreementTableVoid(BuyerAgreementTable buyerAgreementTable)
		{
			try
			{
				CreateBuyerAgreementTable(buyerAgreementTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BuyerAgreementTable UpdateBuyerAgreementTable(BuyerAgreementTable dbBuyerAgreementTable, BuyerAgreementTable inputBuyerAgreementTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBuyerAgreementTable = dbBuyerAgreementTable.MapUpdateValues<BuyerAgreementTable>(inputBuyerAgreementTable);
				base.Update(dbBuyerAgreementTable);
				return dbBuyerAgreementTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateBuyerAgreementTableVoid(BuyerAgreementTable dbBuyerAgreementTable, BuyerAgreementTable inputBuyerAgreementTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBuyerAgreementTable = dbBuyerAgreementTable.MapUpdateValues<BuyerAgreementTable>(inputBuyerAgreementTable, skipUpdateFields);
				base.Update(dbBuyerAgreementTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		#region function
		public BuyerAgreementTableItemView GetBuyerAgreementTableSumLineById(Guid guid)
        {
			try
			{
				var result = (from buyerAgreementTable in Entity
							  join buyerAgreementLine in db.Set<BuyerAgreementLine>()
							  on buyerAgreementTable.BuyerAgreementTableGUID equals buyerAgreementLine.BuyerAgreementTableGUID into ljAssignBuyerLine
							  from buyerAgreementLine in ljAssignBuyerLine.DefaultIfEmpty()
							  where buyerAgreementTable.BuyerAgreementTableGUID == guid
							  group new { buyerAgreementTable, buyerAgreementLine }
					by new
					{
						buyerAgreementTable.BuyerAgreementTableGUID,
						buyerAgreementTable.StartDate,
						buyerAgreementTable.EndDate,
						buyerAgreementTable.ReferenceAgreementID
					} into g
							  select new BuyerAgreementTableItemView
							  {
								  BuyerAgreementTableGUID = g.Key.BuyerAgreementTableGUID.ToString(),
								  StartDate = g.Key.StartDate.DateToString(),
								  EndDate = g.Key.EndDate.DateToString(),
								  ReferenceAgreementID = g.Key.ReferenceAgreementID,
								  BuyerAgreementAmount = g.Sum(s => s.buyerAgreementLine.Amount)
							  })
				   .AsNoTracking()
				   .FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
        }
		#endregion
	}
}

