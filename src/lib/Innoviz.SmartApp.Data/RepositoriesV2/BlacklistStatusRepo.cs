using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IBlacklistStatusRepo
	{
		#region DropDown
		IEnumerable<SelectItem<BlacklistStatusItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<BlacklistStatusListView> GetListvw(SearchParameter search);
		BlacklistStatusItemView GetByIdvw(Guid id);
		BlacklistStatus Find(params object[] keyValues);
		BlacklistStatus GetBlacklistStatusByIdNoTracking(Guid guid);
		BlacklistStatus CreateBlacklistStatus(BlacklistStatus blacklistStatus);
		void CreateBlacklistStatusVoid(BlacklistStatus blacklistStatus);
		BlacklistStatus UpdateBlacklistStatus(BlacklistStatus dbBlacklistStatus, BlacklistStatus inputBlacklistStatus, List<string> skipUpdateFields = null);
		void UpdateBlacklistStatusVoid(BlacklistStatus dbBlacklistStatus, BlacklistStatus inputBlacklistStatus, List<string> skipUpdateFields = null);
		void Remove(BlacklistStatus item);
	}
	public class BlacklistStatusRepo : CompanyBaseRepository<BlacklistStatus>, IBlacklistStatusRepo
	{
		public BlacklistStatusRepo(SmartAppDbContext context) : base(context) { }
		public BlacklistStatusRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BlacklistStatus GetBlacklistStatusByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BlacklistStatusGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<BlacklistStatusItemViewMap> GetDropDownQuery()
		{
			return (from blacklistStatus in Entity
					select new BlacklistStatusItemViewMap
					{
						CompanyGUID = blacklistStatus.CompanyGUID,
						Owner = blacklistStatus.Owner,
						OwnerBusinessUnitGUID = blacklistStatus.OwnerBusinessUnitGUID,
						BlacklistStatusGUID = blacklistStatus.BlacklistStatusGUID,
						BlacklistStatusId = blacklistStatus.BlacklistStatusId,
						Description = blacklistStatus.Description
					});
		}
		public IEnumerable<SelectItem<BlacklistStatusItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<BlacklistStatus>(search, SysParm.CompanyGUID);
				var blacklistStatus = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<BlacklistStatusItemViewMap, BlacklistStatusItemView>().ToDropDownItem(search.ExcludeRowData);


				return blacklistStatus;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<BlacklistStatusListViewMap> GetListQuery()
		{
			return (from blacklistStatus in Entity
				select new BlacklistStatusListViewMap
				{
						CompanyGUID = blacklistStatus.CompanyGUID,
						Owner = blacklistStatus.Owner,
						OwnerBusinessUnitGUID = blacklistStatus.OwnerBusinessUnitGUID,
						BlacklistStatusGUID = blacklistStatus.BlacklistStatusGUID,
						BlacklistStatusId = blacklistStatus.BlacklistStatusId,
						Description = blacklistStatus.Description,
				});
		}
		public SearchResult<BlacklistStatusListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<BlacklistStatusListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<BlacklistStatus>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<BlacklistStatusListViewMap, BlacklistStatusListView>();
				result = list.SetSearchResult<BlacklistStatusListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<BlacklistStatusItemViewMap> GetItemQuery()
		{
			return (from blacklistStatus in Entity
					join ownerBU in db.Set<BusinessUnit>()
					on blacklistStatus.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljBlacklistStatusOwnerBU
					from ownerBU in ljBlacklistStatusOwnerBU.DefaultIfEmpty()
					join company in db.Set<Company>() on blacklistStatus.CompanyGUID equals company.CompanyGUID
					select new BlacklistStatusItemViewMap
					{
						CompanyGUID = blacklistStatus.CompanyGUID,
						Owner = blacklistStatus.Owner,
						OwnerBusinessUnitGUID = blacklistStatus.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CompanyId = company.CompanyId,
						CreatedBy = blacklistStatus.CreatedBy,
						CreatedDateTime = blacklistStatus.CreatedDateTime,
						ModifiedBy = blacklistStatus.ModifiedBy,
						ModifiedDateTime = blacklistStatus.ModifiedDateTime,
						BlacklistStatusGUID = blacklistStatus.BlacklistStatusGUID,
						BlacklistStatusId = blacklistStatus.BlacklistStatusId,
						Description = blacklistStatus.Description,
					
						RowVersion = blacklistStatus.RowVersion,
					});
		}
		public BlacklistStatusItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<BlacklistStatusItemViewMap, BlacklistStatusItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public BlacklistStatus CreateBlacklistStatus(BlacklistStatus blacklistStatus)
		{
			try
			{
				blacklistStatus.BlacklistStatusGUID = Guid.NewGuid();
				base.Add(blacklistStatus);
				return blacklistStatus;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateBlacklistStatusVoid(BlacklistStatus blacklistStatus)
		{
			try
			{
				CreateBlacklistStatus(blacklistStatus);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BlacklistStatus UpdateBlacklistStatus(BlacklistStatus dbBlacklistStatus, BlacklistStatus inputBlacklistStatus, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBlacklistStatus = dbBlacklistStatus.MapUpdateValues<BlacklistStatus>(inputBlacklistStatus);
				base.Update(dbBlacklistStatus);
				return dbBlacklistStatus;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateBlacklistStatusVoid(BlacklistStatus dbBlacklistStatus, BlacklistStatus inputBlacklistStatus, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBlacklistStatus = dbBlacklistStatus.MapUpdateValues<BlacklistStatus>(inputBlacklistStatus, skipUpdateFields);
				base.Update(dbBlacklistStatus);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

