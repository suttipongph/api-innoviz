using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IInvoiceSettlementDetailRepo
	{
		#region DropDown
		IEnumerable<SelectItem<InvoiceSettlementDetailItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<InvoiceSettlementDetailListView> GetListvw(SearchParameter search);
		InvoiceSettlementDetailItemView GetByIdvw(Guid id);
		InvoiceSettlementDetail Find(params object[] keyValues);
		InvoiceSettlementDetail GetInvoiceSettlementDetailByIdNoTracking(Guid guid);
		InvoiceSettlementDetail CreateInvoiceSettlementDetail(InvoiceSettlementDetail invoiceSettlementDetail);
		void CreateInvoiceSettlementDetailVoid(InvoiceSettlementDetail invoiceSettlementDetail);
		InvoiceSettlementDetail UpdateInvoiceSettlementDetail(InvoiceSettlementDetail dbInvoiceSettlementDetail, InvoiceSettlementDetail inputInvoiceSettlementDetail, List<string> skipUpdateFields = null);
		void UpdateInvoiceSettlementDetailVoid(InvoiceSettlementDetail dbInvoiceSettlementDetail, InvoiceSettlementDetail inputInvoiceSettlementDetail, List<string> skipUpdateFields = null);
		void Remove(InvoiceSettlementDetail item);
		InvoiceSettlementDetail GetInvoiceSettlementDetailByIdNoTrackingByAccessLevel(Guid guid);
		List<InvoiceSettlementDetail> GetByReference(RefType refType, Guid refGUID);
		List<InvoiceSettlementDetail> GetByReferenceAndSuspenseInvoiceType(RefType refType, Guid refGUID, SuspenseInvoiceType suspenseInvoiceType);
		IEnumerable<InvoiceSettlementDetail> GetInvoiceSettlementDetailByReferanceNoTracking(Guid refGUID, int refType);
		IEnumerable<InvoiceSettlementDetail> GetInvoiceSettlementDetailByRefReceiptTempPaymDetailNoTracking(Guid refReceiptTempPaymDetailGUID);
		void ValidateAdd(IEnumerable<InvoiceSettlementDetail> items);
		List<InvoiceSettlementDetail> GetInvoiceSettlementDetailByIdNoTracking(List<Guid> invoiceSettlementDetailGUID);
	}
	public class InvoiceSettlementDetailRepo : CompanyBaseRepository<InvoiceSettlementDetail>, IInvoiceSettlementDetailRepo
	{
		public InvoiceSettlementDetailRepo(SmartAppDbContext context) : base(context) { }
		public InvoiceSettlementDetailRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public InvoiceSettlementDetail GetInvoiceSettlementDetailByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.InvoiceSettlementDetailGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<InvoiceSettlementDetailItemViewMap> GetDropDownQuery()
		{
			return (from invoiceSettlementDetail in Entity
					join invoiceTable in db.Set<InvoiceTable>()
					on invoiceSettlementDetail.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljInvoiceTable
					from invoiceTable in ljInvoiceTable.DefaultIfEmpty()
					select new InvoiceSettlementDetailItemViewMap
					{
						CompanyGUID = invoiceSettlementDetail.CompanyGUID,
						Owner = invoiceSettlementDetail.Owner,
						OwnerBusinessUnitGUID = invoiceSettlementDetail.OwnerBusinessUnitGUID,
						InvoiceSettlementDetailGUID = invoiceSettlementDetail.InvoiceSettlementDetailGUID,
						InvoiceTableGUID = invoiceSettlementDetail.InvoiceTableGUID,
						InvoiceSettlementDetail_Values = (invoiceTable != null) ? SmartAppUtil.GetDropDownLabel(invoiceTable.InvoiceId, invoiceTable.IssuedDate.DateToString()) : string.Empty
					});
		}
		public IEnumerable<SelectItem<InvoiceSettlementDetailItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<InvoiceSettlementDetail>(search, SysParm.CompanyGUID);
				var invoiceSettlementDetail = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<InvoiceSettlementDetailItemViewMap, InvoiceSettlementDetailItemView>().ToDropDownItem(search.ExcludeRowData);


				return invoiceSettlementDetail;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<InvoiceSettlementDetailListViewMap> GetListQuery()
		{
			return (from invoiceSettlementDetail in Entity
					join invoiceTable in db.Set<InvoiceTable>() 
					on invoiceSettlementDetail.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljInvoiceTable
					from invoiceTable in ljInvoiceTable.DefaultIfEmpty()
					join invoiceType in db.Set<InvoiceType>() 
					on invoiceTable.InvoiceTypeGUID equals invoiceType.InvoiceTypeGUID into ljInvoiceType
					from invoiceType in ljInvoiceType.DefaultIfEmpty()
					select new InvoiceSettlementDetailListViewMap
					{
						CompanyGUID = invoiceSettlementDetail.CompanyGUID,
						Owner = invoiceSettlementDetail.Owner,
						OwnerBusinessUnitGUID = invoiceSettlementDetail.OwnerBusinessUnitGUID,
						InvoiceSettlementDetailGUID = invoiceSettlementDetail.InvoiceSettlementDetailGUID,
						ProductType = invoiceSettlementDetail.ProductType,
						WHTSlipReceivedByBuyer = invoiceSettlementDetail.WHTSlipReceivedByBuyer,
						WHTSlipReceivedByCustomer = invoiceSettlementDetail.WHTSlipReceivedByCustomer,
						SettleInvoiceAmount = invoiceSettlementDetail.SettleInvoiceAmount,
						DocumentId = invoiceSettlementDetail.DocumentId,
						InvoiceTableGUID = invoiceSettlementDetail.InvoiceTableGUID,
						InvoiceDueDate = invoiceSettlementDetail.InvoiceDueDate,
						InvoiceTypeGUID = invoiceSettlementDetail.InvoiceTypeGUID,
						InvoiceAmount = invoiceSettlementDetail.InvoiceAmount,
						BalanceAmount = invoiceSettlementDetail.BalanceAmount,
						SettleAmount = invoiceSettlementDetail.SettleAmount,
						WHTAmount = invoiceSettlementDetail.WHTAmount,
						InvoiceTable_Values = (invoiceTable != null) ? SmartAppUtil.GetDropDownLabel(invoiceTable.InvoiceId, invoiceTable.IssuedDate.DateToString()) : string.Empty,
						InvoiceTable_InvoiceId = (invoiceTable != null) ? invoiceTable.InvoiceId: string.Empty,
						InvoiceType_Values = (invoiceType != null) ? SmartAppUtil.GetDropDownLabel(invoiceType.InvoiceTypeId, invoiceType.Description) : string.Empty,
						InvoiceType_InvoiceTypeId = (invoiceType != null) ? invoiceType.InvoiceTypeId : string.Empty,
						InvoiceTable_InvoiceTypeGUID = (invoiceType != null) ? invoiceType.InvoiceTypeGUID : (Guid?)null,
						RefGUID = invoiceSettlementDetail.RefGUID,
						RefType = invoiceSettlementDetail.RefType,
						SuspenseInvoiceType = invoiceSettlementDetail.SuspenseInvoiceType,
					});;
		}
		public SearchResult<InvoiceSettlementDetailListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<InvoiceSettlementDetailListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<InvoiceSettlementDetail>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<InvoiceSettlementDetailListViewMap, InvoiceSettlementDetailListView>();
				result = list.SetSearchResult<InvoiceSettlementDetailListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<InvoiceSettlementDetailItemViewMap> GetItemQuery()
		{
			return (from invoiceSettlementDetail in Entity
					join company in db.Set<Company>()
					on invoiceSettlementDetail.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on invoiceSettlementDetail.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljInvoiceSettlementDetailOwnerBU
					from ownerBU in ljInvoiceSettlementDetailOwnerBU.DefaultIfEmpty()
					join invoiceTable in db.Set<InvoiceTable>() 
					on invoiceSettlementDetail.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljInvoiceTable
					from invoiceTable in ljInvoiceTable.DefaultIfEmpty()
                    join currency in db.Set<Currency>() on invoiceTable.CurrencyGUID equals currency.CurrencyGUID into ljCurrency
					from currency in ljCurrency.DefaultIfEmpty()
                    join creditAppTable in db.Set<CreditAppTable>()
                    on invoiceTable.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljCreditAppTable
                    from creditAppTable in ljCreditAppTable.DefaultIfEmpty()
                    join buyerAgreementTable in db.Set<BuyerAgreementTable>()
                    on invoiceSettlementDetail.BuyerAgreementTableGUID equals buyerAgreementTable.BuyerAgreementTableGUID into ljBuyerAgreementTable
                    from buyerAgreementTable in ljBuyerAgreementTable.DefaultIfEmpty()
                    join invoiceType in db.Set<InvoiceType>() on invoiceSettlementDetail.InvoiceTypeGUID equals invoiceType.InvoiceTypeGUID into ljInvoiceType
					from invoiceType in ljInvoiceType.DefaultIfEmpty()
					join assignmentAgreementTable in db.Set<AssignmentAgreementTable>()
                    on invoiceSettlementDetail.AssignmentAgreementTableGUID equals assignmentAgreementTable.AssignmentAgreementTableGUID into ljAssignmentAgreementTable
                    from assignmentAgreementTable in ljAssignmentAgreementTable.DefaultIfEmpty()

					// Referance
					join purchaseTable in db.Set<PurchaseTable>()
					on invoiceSettlementDetail.RefGUID equals purchaseTable.PurchaseTableGUID into ljPurchaseTable 
					from purchaseTable in ljPurchaseTable.DefaultIfEmpty()
					join receiptTempTable in db.Set<ReceiptTempTable>() 
					on invoiceSettlementDetail.RefGUID equals receiptTempTable.ReceiptTempTableGUID into ljReceiptTempTable
					from receiptTempTable in ljReceiptTempTable.DefaultIfEmpty()
					join withdrawalTable in db.Set<WithdrawalTable>() 
					on invoiceSettlementDetail.RefGUID equals withdrawalTable.WithdrawalTableGUID into ljWithdrawalTable
					from withdrawalTable in ljWithdrawalTable.DefaultIfEmpty()
                    join customerRefundTable in db.Set<CustomerRefundTable>()
                    on invoiceSettlementDetail.RefGUID equals customerRefundTable.CustomerRefundTableGUID into ljCustomerRefundTable
                    from customerRefundTable in ljCustomerRefundTable.DefaultIfEmpty()
					join paymentHistory in db.Set<PaymentHistory>().GroupBy(g => g.InvoiceTableGUID).Select(s => new { InvoiceTableGUID = s.Key, SumWHTAmount = s.Sum(su => su.WHTAmount) })
					on invoiceSettlementDetail.InvoiceTableGUID equals paymentHistory.InvoiceTableGUID into ljPaymentHistory
					from paymentHistory in ljPaymentHistory.DefaultIfEmpty()
					select new InvoiceSettlementDetailItemViewMap
					{
						#region invoiceSettlementDetail
						CompanyGUID = invoiceSettlementDetail.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = invoiceSettlementDetail.Owner,
						OwnerBusinessUnitGUID = invoiceSettlementDetail.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = invoiceSettlementDetail.CreatedBy,
						CreatedDateTime = invoiceSettlementDetail.CreatedDateTime,
						ModifiedBy = invoiceSettlementDetail.ModifiedBy,
						ModifiedDateTime = invoiceSettlementDetail.ModifiedDateTime,
						InvoiceSettlementDetailGUID = invoiceSettlementDetail.InvoiceSettlementDetailGUID,
						AssignmentAgreementTableGUID = invoiceSettlementDetail.AssignmentAgreementTableGUID,
						BalanceAmount = invoiceSettlementDetail.BalanceAmount,
						BuyerAgreementTableGUID = invoiceSettlementDetail.BuyerAgreementTableGUID,
						DocumentId = invoiceSettlementDetail.DocumentId,
						InterestCalculationMethod = invoiceSettlementDetail.InterestCalculationMethod,
						InvoiceAmount = invoiceSettlementDetail.InvoiceAmount,
						InvoiceDueDate = invoiceSettlementDetail.InvoiceDueDate,
						InvoiceTableGUID = invoiceSettlementDetail.InvoiceTableGUID,
						InvoiceTypeGUID = invoiceSettlementDetail.InvoiceTypeGUID,
						ProductType = invoiceSettlementDetail.ProductType,
						PurchaseAmount = invoiceSettlementDetail.PurchaseAmount,
						PurchaseAmountBalance = invoiceSettlementDetail.PurchaseAmountBalance,
						RefGUID = invoiceSettlementDetail.RefGUID,
						RefReceiptTempPaymDetailGUID = invoiceSettlementDetail.RefReceiptTempPaymDetailGUID,
						RefType = invoiceSettlementDetail.RefType,
						RetentionAmountAccum = invoiceSettlementDetail.RetentionAmountAccum,
						SettleAmount = invoiceSettlementDetail.SettleAmount,
						SettleAssignmentAmount = invoiceSettlementDetail.SettleAssignmentAmount,
						SettleInvoiceAmount = invoiceSettlementDetail.SettleInvoiceAmount,
						SettlePurchaseAmount = invoiceSettlementDetail.SettlePurchaseAmount,
						SettleReserveAmount = invoiceSettlementDetail.SettleReserveAmount,
						SettleTaxAmount = invoiceSettlementDetail.SettleTaxAmount,
						SuspenseInvoiceType = invoiceSettlementDetail.SuspenseInvoiceType,
						WHTAmount = invoiceSettlementDetail.WHTAmount,
						WHTSlipReceivedByBuyer = invoiceSettlementDetail.WHTSlipReceivedByBuyer,
						WHTSlipReceivedByCustomer = invoiceSettlementDetail.WHTSlipReceivedByCustomer,
						#endregion invoiceSettlementDetail
						#region invoice
						InvoiceTable_Values = (invoiceTable != null) ? SmartAppUtil.GetDropDownLabel(invoiceTable.InvoiceId, invoiceTable.IssuedDate.ToString()) : string.Empty,
                        InvoiceType_Values = (invoiceType != null) ? SmartAppUtil.GetDropDownLabel(invoiceType.InvoiceTypeId, invoiceType.Description) : string.Empty,
						Currency_Values = (currency != null) ? SmartAppUtil.GetDropDownLabel(currency.CurrencyId, currency.Name) : string.Empty,
                        BuyerAgreementTable_Values = (buyerAgreementTable != null) ? SmartAppUtil.GetDropDownLabel(buyerAgreementTable.BuyerAgreementId, buyerAgreementTable.Description) : string.Empty,
                        AssignmentAgreementTable_Values = (assignmentAgreementTable != null) ? SmartAppUtil.GetDropDownLabel(assignmentAgreementTable.AssignmentAgreementId, assignmentAgreementTable.Description) : string.Empty,
                        CreditAppTable_Values = (creditAppTable != null) ? SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description) : string.Empty,
						InvoiceTable_ProductInvoice = (invoiceTable != null) ? invoiceTable .ProductInvoice: false,
						InvoiceTable_ProductType = (invoiceTable != null) ? invoiceTable.ProductType : (int?)null,
						InvoiceTable_WHTAmount = (invoiceTable != null) ? 1 : 0,
						InvoiceTable_WHTBalance = (invoiceTable != null) ? invoiceTable.WHTAmount - ((paymentHistory != null) ? paymentHistory.SumWHTAmount: 0) : 0,

						#endregion invoice
						#region Referance
						RefId = (purchaseTable != null && invoiceSettlementDetail.RefType == (int)RefType.PurchaseTable)
                                ? purchaseTable.PurchaseId
                                : (receiptTempTable != null && invoiceSettlementDetail.RefType == (int)RefType.ReceiptTemp)
                                ? receiptTempTable.ReceiptTempId :
                                (withdrawalTable != null && invoiceSettlementDetail.RefType == (int)RefType.WithdrawalTable)
                                ? withdrawalTable.WithdrawalId :
                                (customerRefundTable != null && invoiceSettlementDetail.RefType == (int)RefType.CustomerRefund)
                                ? customerRefundTable.CustomerRefundId : string.Empty,
                        #endregion Referance
                        ReceivedFrom = (receiptTempTable != null && invoiceSettlementDetail.RefType == (int)RefType.ReceiptTemp)
										? receiptTempTable.ReceivedFrom : (int)ReceivedFrom.Customer, 
					
						RowVersion = invoiceSettlementDetail.RowVersion,
					});
		}
		public InvoiceSettlementDetailItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<InvoiceSettlementDetailItemViewMap, InvoiceSettlementDetailItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public InvoiceSettlementDetail CreateInvoiceSettlementDetail(InvoiceSettlementDetail invoiceSettlementDetail)
		{
			try
			{
				invoiceSettlementDetail.InvoiceSettlementDetailGUID = Guid.NewGuid();
				base.Add(invoiceSettlementDetail);
				return invoiceSettlementDetail;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateInvoiceSettlementDetailVoid(InvoiceSettlementDetail invoiceSettlementDetail)
		{
			try
			{
				CreateInvoiceSettlementDetail(invoiceSettlementDetail);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public InvoiceSettlementDetail UpdateInvoiceSettlementDetail(InvoiceSettlementDetail dbInvoiceSettlementDetail, InvoiceSettlementDetail inputInvoiceSettlementDetail, List<string> skipUpdateFields = null)
		{
			try
			{
				dbInvoiceSettlementDetail = dbInvoiceSettlementDetail.MapUpdateValues<InvoiceSettlementDetail>(inputInvoiceSettlementDetail);
				base.Update(dbInvoiceSettlementDetail);
				return dbInvoiceSettlementDetail;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateInvoiceSettlementDetailVoid(InvoiceSettlementDetail dbInvoiceSettlementDetail, InvoiceSettlementDetail inputInvoiceSettlementDetail, List<string> skipUpdateFields = null)
		{
			try
			{
				dbInvoiceSettlementDetail = dbInvoiceSettlementDetail.MapUpdateValues<InvoiceSettlementDetail>(inputInvoiceSettlementDetail, skipUpdateFields);
				base.Update(dbInvoiceSettlementDetail);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public InvoiceSettlementDetail GetInvoiceSettlementDetailByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.InvoiceSettlementDetailGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public List<InvoiceSettlementDetail> GetByReference(RefType refType, Guid refGUID)
		{
			try
			{
				var result = Entity.Where(w => w.RefType == (int)refType
									   && w.RefGUID == refGUID)
					.AsNoTracking()
					.ToList();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<InvoiceSettlementDetail> GetByReferenceAndSuspenseInvoiceType(RefType refType, Guid refGUID, SuspenseInvoiceType suspenseInvoiceType)
        {
            try
            {

				var result = Entity.Where(w => w.RefType == (int)refType &&
												w.RefGUID == refGUID &&
												w.SuspenseInvoiceType == (int)suspenseInvoiceType)
									.AsNoTracking()
									.ToList();
				return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public IEnumerable<InvoiceSettlementDetail> GetInvoiceSettlementDetailByReferanceNoTracking(Guid refGUID, int refType)
		{
			try
			{
				var result = Entity.Where(item => item.RefGUID == refGUID && item.RefType == refType)
					.AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Validate LIT1213, LIT1215
		public override void ValidateAdd(InvoiceSettlementDetail item)
		{
			ValidateCreateData(item);
			base.ValidateAdd(item);
		}
		public override void ValidateUpdate(InvoiceSettlementDetail item)
		{
			ValidateUpdateData(item);
			base.ValidateUpdate(item);
		}
		public void ValidateCreateData(InvoiceSettlementDetail item)
        {
            try
            {
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
				ICustTransRepo custTransRepo = new CustTransRepo(db);
				IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
				#region 1
				InvoiceTable invoiceTable = invoiceTableRepo.GetInvoiceTableByIdNoTracking(item.InvoiceTableGUID.GetValueOrDefault());
				CustTrans custTrans = custTransRepo.GetCustTransByInvoiceTableNoTracking(item.InvoiceTableGUID.GetValueOrDefault());
				if (custTrans != null && item.BalanceAmount != (custTrans.TransAmount - custTrans.SettleAmount))
					ex.AddData("ERROR.90046", new string[] { "LABEL.INVOICE_ID", invoiceTable.InvoiceId, "LABEL.SAVE_DATA" });
				#endregion
				#region 2
				InvoiceSettlementDetail dbinvoiceSettlementDetail = Entity.Where(w => w.InvoiceTableGUID == item.InvoiceTableGUID && w.RefGUID == item.RefGUID && w.InvoiceSettlementDetailGUID != item.InvoiceSettlementDetailGUID).AsNoTracking().FirstOrDefault();
				if (dbinvoiceSettlementDetail != null)
					ex.AddData("ERROR.90081", new string[] { "LABEL.INVOICE_ID", invoiceTable.InvoiceId, "LABEL.INVOICE_SETTLEMENT_DETAIL" });
				#endregion
				#region 3
				if (item.SettleInvoiceAmount == 0)
					ex.AddData("ERROR.90049", new string[] { "LABEL.SETTLE_INVOICE_AMOUNT" });
				#endregion
				#region 4
				if (invoiceTable.ProductInvoice && item.RefType == (int)RefType.ReceiptTemp)
				{
					ReceiptTempTable receiptTempTable = receiptTempTableRepo.GetReceiptTempTableByIdNoTracking(item.RefGUID.GetValueOrDefault());
					if (receiptTempTable != null && (receiptTempTable.ReceiptDate < invoiceTable.IssuedDate || receiptTempTable.TransDate < invoiceTable.IssuedDate))
						ex.AddData("ERROR.90115");
				}
				#endregion
				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void ValidateUpdateData(InvoiceSettlementDetail item)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
				ICustTransRepo custTransRepo = new CustTransRepo(db);
				#region 1
				InvoiceTable invoiceTable = invoiceTableRepo.GetInvoiceTableByIdNoTracking(item.InvoiceTableGUID.GetValueOrDefault());
				CustTrans custTrans = custTransRepo.GetCustTransByInvoiceTableNoTracking(item.InvoiceTableGUID.GetValueOrDefault());
				if (custTrans != null && item.BalanceAmount != (custTrans.TransAmount - custTrans.SettleAmount))
					ex.AddData("ERROR.90046", new string[] { "LABEL.INVOICE_ID", invoiceTable.InvoiceId, "LABEL.SAVE_DATA" });
				#endregion
				#region 2
				InvoiceSettlementDetail dbinvoiceSettlementDetail = Entity.Where(w => w.InvoiceTableGUID == item.InvoiceTableGUID && w.RefGUID == item.RefGUID && w.InvoiceSettlementDetailGUID != item.InvoiceSettlementDetailGUID).AsNoTracking().FirstOrDefault();
				if (dbinvoiceSettlementDetail != null)
					ex.AddData("ERROR.90081", new string[] { "LABEL.INVOICE_ID", invoiceTable.InvoiceId, "LABEL.INVOICE_SETTLEMENT_DETAIL" });
				#endregion
				#region 3
				if (item.SettleInvoiceAmount == 0)
					ex.AddData("ERROR.90049", new string[] { "LABEL.SETTLE_INVOICE_AMOUNT" });
				#endregion
				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		public IEnumerable<InvoiceSettlementDetail> GetInvoiceSettlementDetailByRefReceiptTempPaymDetailNoTracking(Guid refReceiptTempPaymDetailGUID)
		{
			try
			{
				var result = Entity.Where(item => item.RefReceiptTempPaymDetailGUID == refReceiptTempPaymDetailGUID).AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #region Validate
        public override void ValidateAdd(IEnumerable<InvoiceSettlementDetail> items)
        {
            base.ValidateAdd(items);
        }
        #endregion Validate
		public List<InvoiceSettlementDetail> GetInvoiceSettlementDetailByIdNoTracking(List<Guid> invoiceSettlementDetailGUID)
        {
            try
            {
				List<InvoiceSettlementDetail> invoiceSettlementDetails = Entity.Where(w => invoiceSettlementDetailGUID.Contains(w.InvoiceSettlementDetailGUID)).ToList();
				return invoiceSettlementDetails;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

