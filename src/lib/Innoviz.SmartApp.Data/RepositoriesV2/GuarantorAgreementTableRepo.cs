using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.Data.ServicesV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;
using Innoviz.SmartApp.Core.Constants;
using System.Globalization;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface IGuarantorAgreementTableRepo
    {
        #region DropDown
        IEnumerable<SelectItem<GuarantorAgreementTableItemView>> GetDropDownItem(SearchParameter search);
        #endregion DropDown
        SearchResult<GuarantorAgreementTableListView> GetListvw(SearchParameter search);
        GuarantorAgreementTableItemView GetByIdvw(Guid id);
        GuarantorAgreementTable Find(params object[] keyValues);
        GuarantorAgreementTable GetGuarantorAgreementTableByIdNoTracking(Guid guid);
        GuarantorAgreementTable GetGuarantorAgreementTableByGuarantorAgreementId(string id);
        GuarantorAgreementTable CreateGuarantorAgreementTable(GuarantorAgreementTable guarantorAgreementTable);
        void CreateGuarantorAgreementTableVoid(GuarantorAgreementTable guarantorAgreementTable);
        GuarantorAgreementTable UpdateGuarantorAgreementTable(GuarantorAgreementTable dbGuarantorAgreementTable, GuarantorAgreementTable inputGuarantorAgreementTable, List<string> skipUpdateFields = null);
        void UpdateGuarantorAgreementTableVoid(GuarantorAgreementTable dbGuarantorAgreementTable, GuarantorAgreementTable inputGuarantorAgreementTable, List<string> skipUpdateFields = null);
        void Remove(GuarantorAgreementTable item);
        GuarantorAgreementTable GetGuarantorAgreementTableByIdNoTrackingByAccessLevel(Guid guid);

        GuarantorAgreementTableItemView GetGuarantorAgreementTableInitialData(Guid mainAgreement);
        List<GuarantorAgreementTable> GetListByMainAgreementTable(Guid guid);
        CrossGuarantorAgreementBookmarkView GetCrossGuarantorAgreementBookmarkView(Guid refGUID);
        BookmaskDocumentGuarantorAgreement GetBookmaskDocumentGuarantorAgreement_ThaiPerson(Guid refGUID , Guid bookmarkDocumentTransGuid);
        BookmaskDocumentGuarantorAgreement GetBookmaskDocumentGuarantorAgreement_Foreigner(Guid refGUID, Guid bookmarkDocumentTransGuid);
        BookmaskDocumentGuarantorAgreement GetBookmaskDocumentGuarantorAgreement_Organization(Guid refGUID, Guid bookmarkDocumentTransGuid);
        void ValidateAdd(IEnumerable<GuarantorAgreementTable> items);
        void ValidateUpdate(IEnumerable<GuarantorAgreementTable> items);
        List<GuarantorAgreementTable> GetListExtendGuarantorAgreementByGuarantorAgreementTableGUID(Guid guid);
        bool IsDuplicateGuarantorAgreementId(Guid guid, string id);
    }
    public class GuarantorAgreementTableRepo : CompanyBaseRepository<GuarantorAgreementTable>, IGuarantorAgreementTableRepo
    {
        public GuarantorAgreementTableRepo(SmartAppDbContext context) : base(context) { }
        public GuarantorAgreementTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public GuarantorAgreementTable GetGuarantorAgreementTableByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.GuarantorAgreementTableGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GuarantorAgreementTable GetGuarantorAgreementTableByGuarantorAgreementId(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return null;
                }
                else
                {
                    var result = Entity.Where(item => item.GuarantorAgreementId == id && item.CompanyGUID == db.GetCompanyFilter().StringToGuid()).AsNoTracking().FirstOrDefault();
                    return result;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        private IQueryable<GuarantorAgreementTableItemViewMap> GetDropDownQuery()
        {
            return (from guarantorAgreementTable in Entity
                    select new GuarantorAgreementTableItemViewMap
                    {
                        CompanyGUID = guarantorAgreementTable.CompanyGUID,
                        Owner = guarantorAgreementTable.Owner,
                        OwnerBusinessUnitGUID = guarantorAgreementTable.OwnerBusinessUnitGUID,
                        GuarantorAgreementTableGUID = guarantorAgreementTable.GuarantorAgreementTableGUID,
                        InternalGuarantorAgreementId = guarantorAgreementTable.InternalGuarantorAgreementId,
                        Description = guarantorAgreementTable.Description
                    });
        }
        public IEnumerable<SelectItem<GuarantorAgreementTableItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<GuarantorAgreementTable>(search, SysParm.CompanyGUID);
                var guarantorAgreementTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<GuarantorAgreementTableItemViewMap, GuarantorAgreementTableItemView>().ToDropDownItem(search.ExcludeRowData);


                return guarantorAgreementTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #region GetListvw
        private IQueryable<GuarantorAgreementTableListViewMap> GetListQuery()
        {
            return (from guarantorAgreementTable in Entity
                    join documentStatus in db.Set<DocumentStatus>()
                    on guarantorAgreementTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljGuerantorAgreement
                    from documentStatus in ljGuerantorAgreement.DefaultIfEmpty()
                    join company in db.Set<Company>()
                    on guarantorAgreementTable.CompanyGUID equals company.CompanyGUID into ljdocumentStatus
                    from company in ljdocumentStatus.DefaultIfEmpty()
                    join mainAgreement in db.Set<MainAgreementTable>()
                    on guarantorAgreementTable.MainAgreementTableGUID equals mainAgreement.MainAgreementTableGUID
                    select new GuarantorAgreementTableListViewMap
                    {
                        CompanyGUID = guarantorAgreementTable.CompanyGUID,
                        Owner = guarantorAgreementTable.Owner,
                        OwnerBusinessUnitGUID = guarantorAgreementTable.OwnerBusinessUnitGUID,
                        GuarantorAgreementTableGUID = guarantorAgreementTable.GuarantorAgreementTableGUID,
                        InternalGuarantorAgreementId = guarantorAgreementTable.InternalGuarantorAgreementId,
                        GuarantorAgreementId = guarantorAgreementTable.GuarantorAgreementId,
                        AgreementDocType = guarantorAgreementTable.AgreementDocType,
                        Description = guarantorAgreementTable.Description,
                        AgreementDate = guarantorAgreementTable.AgreementDate,
                        ExpiryDate = guarantorAgreementTable.ExpiryDate,
                        DocumentStatusGUID = guarantorAgreementTable.DocumentStatusGUID,
                        Affiliate = guarantorAgreementTable.Affiliate,
                        ParentCompanyGUID = guarantorAgreementTable.ParentCompanyGUID,
                        DocumentStatus_Values = documentStatus.Description,
                        company_CompanyId = company.CompanyId,
                        MainAgreementTable_MainAgreementId = mainAgreement.MainAgreementId,
                        MainAgreementTable_MainAgreementGUID = mainAgreement.MainAgreementTableGUID.ToString(),
                        RefGuarantorAgreementTableGUID = guarantorAgreementTable.RefGuarantorAgreementTableGUID

                    });
        }
        public SearchResult<GuarantorAgreementTableListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<GuarantorAgreementTableListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<GuarantorAgreementTable>(search, SysParm.CompanyGUID);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<GuarantorAgreementTableListViewMap, GuarantorAgreementTableListView>();
                result = list.SetSearchResult<GuarantorAgreementTableListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<GuarantorAgreementTableItemViewMap> GetItemQuery()
        {
            return (from guarantorAgreementTable in Entity
                    join mainAgreement in db.Set<MainAgreementTable>()
                    on guarantorAgreementTable.MainAgreementTableGUID equals mainAgreement.MainAgreementTableGUID into ljmainAgreement

                    from mainAgreement in ljmainAgreement.DefaultIfEmpty()
                    join company in db.Set<Company>()
                    on guarantorAgreementTable.CompanyGUID equals company.CompanyGUID into ljcompany
                    from company in ljcompany.DefaultIfEmpty()

                    join customerTable in db.Set<CustomerTable>()
                    on mainAgreement.CustomerTableGUID equals customerTable.CustomerTableGUID into ljcustomerTable
                    from customerTable in ljcustomerTable.DefaultIfEmpty()

                    join parentCompany in db.Set<ParentCompany>()
                    on guarantorAgreementTable.ParentCompanyGUID equals parentCompany.ParentCompanyGUID into ljparentCompany


                    from parentCompany in ljparentCompany.DefaultIfEmpty()
                    join documentStatus in db.Set<DocumentStatus>()
                    on guarantorAgreementTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus

                    from documentStatus in ljdocumentStatus.DefaultIfEmpty()
                    join creditApp in db.Set<CreditAppTable>()
                    on mainAgreement.CreditAppTableGUID equals creditApp.CreditAppTableGUID into ljcreditApp

                    from creditApp in ljcreditApp.DefaultIfEmpty()
                    join documentReason in db.Set<DocumentReason>()
                    on guarantorAgreementTable.DocumentReasonGUID equals documentReason.DocumentReasonGUID into ljdocumentReason

                    from documentReason in ljdocumentReason.DefaultIfEmpty()
                    join ownerBU in db.Set<BusinessUnit>()
                    on guarantorAgreementTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljGuarantorAgreementTableOwnerBU

                    from ownerBU in ljGuarantorAgreementTableOwnerBU.DefaultIfEmpty()
                    join refGurantorAgreementTable in db.Set<GuarantorAgreementTable>()
                    on guarantorAgreementTable.RefGuarantorAgreementTableGUID equals refGurantorAgreementTable.GuarantorAgreementTableGUID into ljrefGurantorAgreementTable
                    from refGurantorAgreementTable in ljrefGurantorAgreementTable.DefaultIfEmpty()

                    join documentStatusMainAgreement in db.Set<DocumentStatus>()
                    on mainAgreement.DocumentStatusGUID equals documentStatusMainAgreement.DocumentStatusGUID into ljdocumentStatusMainAgreement
                    from documentStatusMainAgreement in ljdocumentStatusMainAgreement.DefaultIfEmpty()
                    
                    select new GuarantorAgreementTableItemViewMap
                    {
                        CompanyGUID = guarantorAgreementTable.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = guarantorAgreementTable.Owner,
                        OwnerBusinessUnitGUID = guarantorAgreementTable.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        CreatedBy = guarantorAgreementTable.CreatedBy,
                        CreatedDateTime = guarantorAgreementTable.CreatedDateTime,
                        ModifiedBy = guarantorAgreementTable.ModifiedBy,
                        ModifiedDateTime = guarantorAgreementTable.ModifiedDateTime,
                        GuarantorAgreementTableGUID = guarantorAgreementTable.GuarantorAgreementTableGUID,
                        Affiliate = guarantorAgreementTable.Affiliate,
                        AgreementDate = guarantorAgreementTable.AgreementDate,
                        AgreementDocType = guarantorAgreementTable.AgreementDocType,
                        AgreementExtension = guarantorAgreementTable.AgreementExtension,
                        AgreementYear = guarantorAgreementTable.AgreementYear,
                        Description = guarantorAgreementTable.Description,
                        DocumentReasonGUID = guarantorAgreementTable.DocumentReasonGUID,
                        DocumentStatusGUID = guarantorAgreementTable.DocumentStatusGUID,
                        ExpiryDate = guarantorAgreementTable.ExpiryDate,
                        GuarantorAgreementId = guarantorAgreementTable.GuarantorAgreementId,
                        InternalGuarantorAgreementId = guarantorAgreementTable.InternalGuarantorAgreementId,
                        MainAgreementTableGUID = guarantorAgreementTable.MainAgreementTableGUID,
                        ParentCompanyGUID = guarantorAgreementTable.ParentCompanyGUID,
                        RefGuarantorAgreementTableGUID = guarantorAgreementTable.RefGuarantorAgreementTableGUID,
                        SigningDate = guarantorAgreementTable.SigningDate,
                        WitnessCompany1GUID = guarantorAgreementTable.WitnessCompany1GUID,
                        WitnessCompany2GUID = guarantorAgreementTable.WitnessCompany2GUID,
                        WitnessCustomer1 = guarantorAgreementTable.WitnessCustomer1,
                        WitnessCustomer2 = guarantorAgreementTable.WitnessCustomer2,
                        MainAgreementTable_MainAgreementId = mainAgreement.MainAgreementId,
                        DocumentStatus_Values = documentStatus.Description,
                        creditApp_ProductType = creditApp.ProductType,
                        company_CompanyId = SmartAppUtil.GetDropDownLabel(parentCompany.ParentCompanyId, parentCompany.Description),
                        GuarantorAgreementTable_RefValue = refGurantorAgreementTable.GuarantorAgreementId,
                        GuarantorAgreementTable_RefInternalValue = SmartAppUtil.GetDropDownLabel(refGurantorAgreementTable.InternalGuarantorAgreementId, refGurantorAgreementTable.Description),
                        DocumentStatus_StatusId = documentStatus.StatusId,
                        MainAgreement_ConsortiumGUID = mainAgreement.ConsortiumTableGUID,
                        CustomerTable_CustomerTableGUID = customerTable.CustomerTableGUID,
                        CustomerTable_Value = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                        CreditAppTable_CreditAppTableGUID = mainAgreement.CreditAppTableGUID,
                        CreditAppRequestTable_CreditAppRequestTableGUID = mainAgreement.CreditAppRequestTableGUID,
                        Remark = guarantorAgreementTable.Remark,
                        DocumentReason_Values = (documentReason != null) ? SmartAppUtil.GetDropDownLabel(documentReason.ReasonId, documentReason.Description) : null,
                        MainAgreementTable_StatusId = documentStatusMainAgreement.StatusId,
                        MainAgreementTable_ProductType = mainAgreement.ProductType,
                        MainAgreementTable_CustomerName = mainAgreement.CustomerName,
                    
                        RowVersion = guarantorAgreementTable.RowVersion,
                    });
        }
        public GuarantorAgreementTableItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<GuarantorAgreementTableItemViewMap, GuarantorAgreementTableItemView>();
                item.HaveLine = GetGuarantorLineCount(guid);
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public GuarantorAgreementTable CreateGuarantorAgreementTable(GuarantorAgreementTable guarantorAgreementTable)
        {
            try
            {
                guarantorAgreementTable.GuarantorAgreementTableGUID = Guid.NewGuid();
                base.Add(guarantorAgreementTable);
                return guarantorAgreementTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateGuarantorAgreementTableVoid(GuarantorAgreementTable guarantorAgreementTable)
        {
            try
            {
                CreateGuarantorAgreementTable(guarantorAgreementTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GuarantorAgreementTable UpdateGuarantorAgreementTable(GuarantorAgreementTable dbGuarantorAgreementTable, GuarantorAgreementTable inputGuarantorAgreementTable, List<string> skipUpdateFields = null)
        {
            try
            {
                dbGuarantorAgreementTable = dbGuarantorAgreementTable.MapUpdateValues<GuarantorAgreementTable>(inputGuarantorAgreementTable);
                base.Update(dbGuarantorAgreementTable);
                return dbGuarantorAgreementTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateGuarantorAgreementTableVoid(GuarantorAgreementTable dbGuarantorAgreementTable, GuarantorAgreementTable inputGuarantorAgreementTable, List<string> skipUpdateFields = null)
        {
            try
            {
                dbGuarantorAgreementTable = dbGuarantorAgreementTable.MapUpdateValues<GuarantorAgreementTable>(inputGuarantorAgreementTable, skipUpdateFields);
                base.Update(dbGuarantorAgreementTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Create, Update, Delete
        public GuarantorAgreementTable GetGuarantorAgreementTableByIdNoTrackingByAccessLevel(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.GuarantorAgreementTableGUID == guid)
                    .FilterByAccessLevel(SysParm.AccessLevel)
                    .AsNoTracking()
                    .FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public GuarantorAgreementTableItemView GetGuarantorAgreementTableInitialData(Guid id)
        {
            try
            {
                var result = (from mainAgreement in db.Set<MainAgreementTable>()
                              join customer in db.Set<CustomerTable>()
                              on mainAgreement.CustomerTableGUID equals customer.CustomerTableGUID into ljcustomer
                              from customer in ljcustomer.DefaultIfEmpty()

                              join creditAppTable in db.Set<CreditAppTable>()
                              on mainAgreement.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljcreditAppTable
                              from creditAppTable in ljcreditAppTable.DefaultIfEmpty()

                              join parentCompany in db.Set<ParentCompany>()
                              on customer.ParentCompanyGUID equals parentCompany.ParentCompanyGUID into ljparentCompany
                              from parentCompany in ljparentCompany.DefaultIfEmpty()

                              join agreementTableInfo in db.Set<AgreementTableInfo>()
                              on mainAgreement.MainAgreementTableGUID equals agreementTableInfo.RefGUID into ljAgreementTableInfo
                              from agreementTableInfo in ljAgreementTableInfo.DefaultIfEmpty()

                              join productSubType in db.Set<ProductSubType>()
                              on creditAppTable.ProductSubTypeGUID equals productSubType.ProductSubTypeGUID into ljproductSubType
                              from productSubType in ljproductSubType.DefaultIfEmpty()

                              where mainAgreement.MainAgreementTableGUID == id
                              select new GuarantorAgreementTableItemView
                              {
                                  GuarantorAgreementTableGUID = new Guid().GuidNullToString(),
                                  MainAgreementTableGUID = mainAgreement.MainAgreementTableGUID.GuidNullToString(),
                                  MainAgreementTable_MainAgreementId = SmartAppUtil.GetDropDownLabel(mainAgreement.InternalMainAgreementId, mainAgreement.Description),
                                  AgreementDate = (DateTime.Now).DateToString(),
                                  AgreementDocType = (int)AgreementDocType.New,
                                  AgreementYear = productSubType.GuarantorAgreementYear,
                                  ExpiryDate = DateTime.Now.AddYears(productSubType.GuarantorAgreementYear).DateToString(),
                                  InternalGuarantorAgreementId = null,
                                  ParentCompanyGUID = customer.ParentCompanyGUID.GuidNullToString(),
                                  WitnessCompany1GUID = agreementTableInfo.WitnessCompany1GUID.GuidNullToString(),
                                  WitnessCompany2GUID = agreementTableInfo.WitnessCompany2GUID.GuidNullToString(),
                                  WitnessCustomer1 = agreementTableInfo.WitnessCustomer1,
                                  WitnessCustomer2 = agreementTableInfo.WitnessCustomer2,
                                  AgreementExtension = 0,
                                  creditApp_ProductType = creditAppTable.ProductType,
                                  company_CompanyId = SmartAppUtil.GetDropDownLabel(parentCompany.ParentCompanyId, parentCompany.Description),
                                  CustomerTable_Value = SmartAppUtil.GetDropDownLabel(customer.CustomerId, customer.Name)

                              }).FirstOrDefault();
                if (result != null)
                {
                    result.ExpiryDate = DateTime.Now.AddYears(result.AgreementYear).AddDays(-1).DateToString();
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private int GetGuarantorLineCount(Guid mainAgreement)
        {
            var result = (from guarantorAgreementLine in db.Set<GuarantorAgreementLine>()
                          where guarantorAgreementLine.GuarantorAgreementTableGUID == mainAgreement
                          select new GuarantorAgreementLine
                          {
                              GuarantorAgreementLineGUID = guarantorAgreementLine.GuarantorAgreementLineGUID
                          }).ToList();
            return result.Count();
        }
        public List<GuarantorAgreementTable> GetListByMainAgreementTable(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.MainAgreementTableGUID == guid).AsNoTracking().ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<GuarantorAgreementTable> GetListExtendGuarantorAgreementByGuarantorAgreementTableGUID(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.RefGuarantorAgreementTableGUID == guid
                                                  && item.AgreementDocType == (int)AgreementDocType.Extend).AsNoTracking().ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Validate
        public override void ValidateAdd(IEnumerable<GuarantorAgreementTable> items)
        {
            base.ValidateAdd(items);
            items.ToList().ForEach(f => CheckDuplicateGuarantorAgreementId(f));
        }
        public override void ValidateAdd(GuarantorAgreementTable item)
        {
            base.ValidateAdd(item);
            CheckDuplicateGuarantorAgreementId(item);
        }
        public override void ValidateUpdate(IEnumerable<GuarantorAgreementTable> items)
        {
            base.ValidateUpdate(items);
            items.ToList().ForEach(f => CheckDuplicateGuarantorAgreementId(f));
        }
        public override void ValidateUpdate(GuarantorAgreementTable item)
        {
            base.ValidateUpdate(item);
            CheckDuplicateGuarantorAgreementId(item);
        }
        public void CheckDuplicateGuarantorAgreementId(GuarantorAgreementTable item)
        {
            try
            {
                SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
                bool isDuplicateGuarantorAgreementId = IsDuplicateGuarantorAgreementId(item.GuarantorAgreementTableGUID, item.GuarantorAgreementId);
                if (isDuplicateGuarantorAgreementId)
                {
                    smartAppException.AddData("ERROR.DUPLICATE", new string[] { "LABEL.GUARANTOR_AGREEMENT_ID" });
                }
                if (smartAppException.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(smartAppException);
                }
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public bool IsDuplicateGuarantorAgreementId(Guid guid, string id)
        {
            try
            {
                bool isDuplicateGuarantorAgreementId = Entity.Any(a => a.GuarantorAgreementId == id
                                                                    && a.CompanyGUID == db.GetCompanyFilter().StringToGuid()
                                                                    && !string.IsNullOrEmpty(a.GuarantorAgreementId)
                                                                    && a.GuarantorAgreementTableGUID != guid);
                return isDuplicateGuarantorAgreementId;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion

        #region GenCrossGuarantorAgreementLineView
        public CrossGuarantorAgreementLineBookmarkView GetCrossGuarantorAgreementLineBookmarkView(Guid refGUID, int lineNum)
        {
            try
            {
                CrossGuarantorAgreementLineBookmarkView result = (from guarantorAgreementTable in Entity

                                                                      #region guarantorAgreementLine
                                                                  join guarantorAgreementLineNum in db.Set<GuarantorAgreementLine>().Where(w => w.Ordering == lineNum)
                                                                  on guarantorAgreementTable.GuarantorAgreementTableGUID equals guarantorAgreementLineNum.GuarantorAgreementTableGUID into ljAuarantorLine1
                                                                  from guarantorAgreementLineNum in ljAuarantorLine1.DefaultIfEmpty()

                                                                  join guarantorTrans in db.Set<GuarantorTrans>()
                                                                  on guarantorAgreementLineNum.GuarantorTransGUID equals guarantorTrans.GuarantorTransGUID into ljGuarantorTrans
                                                                  from guarantorTrans in ljGuarantorTrans.DefaultIfEmpty()

                                                                  join relatedPersonTable in db.Set<RelatedPersonTable>()
                                                                  on guarantorTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID into ljRelatedPersonTable
                                                                  from relatedPersonTable in ljRelatedPersonTable.DefaultIfEmpty()

                                                                  join race in db.Set<Race>()
                                                                      on guarantorAgreementLineNum.RaceGUID equals race.RaceGUID into ljRace
                                                                  from race in ljRace.DefaultIfEmpty()
                                                                  join nationality in db.Set<Nationality>()
                                                                      on guarantorAgreementLineNum.NationalityGUID equals nationality.NationalityGUID into ljNationality
                                                                  from nationality in ljNationality.DefaultIfEmpty()
                                                                  join guarantorAgreementLineAffiliate1 in db.Set<GuarantorAgreementLineAffiliate>().Where(w => w.Ordering == 1)
                                                                  on guarantorAgreementLineNum.GuarantorAgreementLineGUID equals guarantorAgreementLineAffiliate1.GuarantorAgreementLineGUID into ljAuarantorAgreementLineAffiliate
                                                                  from guarantorAgreementLineAffiliate1 in ljAuarantorAgreementLineAffiliate.DefaultIfEmpty()
                                                                  join customerTable in db.Set<CustomerTable>()
                                                                      on guarantorAgreementLineAffiliate1.CustomerTableGUID equals customerTable.CustomerTableGUID into ljCustomerLineAff
                                                                  from customerTable in ljCustomerLineAff.DefaultIfEmpty()
                                                                  join mainAgreementTable in db.Set<MainAgreementTable>()
                                                                      on guarantorAgreementLineAffiliate1.MainAgreementTableGUID equals mainAgreementTable.MainAgreementTableGUID into ljMainGuarantor
                                                                  from mainAgreementTable in ljMainGuarantor.DefaultIfEmpty()
                                                                  join agreementTableInfo1 in db.Set<AgreementTableInfo>()
                                                                      on mainAgreementTable.MainAgreementTableGUID equals agreementTableInfo1.RefGUID into ljMainAgree
                                                                  from agreementTableInfo1 in ljMainAgree.DefaultIfEmpty()



                                                                  join race2 in db.Set<Race>()
                                                                     on guarantorAgreementLineNum.RaceGUID equals race2.RaceGUID into ljRace2
                                                                  from race2 in ljRace2.DefaultIfEmpty()
                                                                  join nationality2 in db.Set<Nationality>()
                                                                      on guarantorAgreementLineNum.NationalityGUID equals nationality2.NationalityGUID into ljNationality2
                                                                  from nationality2 in ljNationality2.DefaultIfEmpty()
                                                                  join guarantorAgreementLineAffiliate2 in db.Set<GuarantorAgreementLineAffiliate>().Where(w => w.Ordering == 2)
                                                                      on guarantorAgreementLineNum.GuarantorAgreementLineGUID equals guarantorAgreementLineAffiliate2.GuarantorAgreementLineGUID into ljAuarantorAgreementLineAffiliate2
                                                                  from guarantorAgreementLineAffiliate2 in ljAuarantorAgreementLineAffiliate2.DefaultIfEmpty()
                                                                  join customerTable2 in db.Set<CustomerTable>()
                                                                      on guarantorAgreementLineAffiliate2.CustomerTableGUID equals customerTable2.CustomerTableGUID into ljCustomerLineAff2
                                                                  from customerTable2 in ljCustomerLineAff2.DefaultIfEmpty()
                                                                  join mainAgreementTable2 in db.Set<MainAgreementTable>()
                                                                      on guarantorAgreementLineAffiliate2.MainAgreementTableGUID equals mainAgreementTable2.MainAgreementTableGUID into ljMainGuarantor2
                                                                  from mainAgreementTable2 in ljMainGuarantor2.DefaultIfEmpty()
                                                                  join agreementTableInfo2 in db.Set<AgreementTableInfo>()
                                                                      on mainAgreementTable2.MainAgreementTableGUID equals agreementTableInfo2.RefGUID into ljMainAgree2
                                                                  from agreementTableInfo2 in ljMainAgree2.DefaultIfEmpty()



                                                                  join race3 in db.Set<Race>()
                                                                      on guarantorAgreementLineNum.RaceGUID equals race3.RaceGUID into ljRace3
                                                                  from race3 in ljRace3.DefaultIfEmpty()
                                                                  join nationality3 in db.Set<Nationality>()
                                                                      on guarantorAgreementLineNum.NationalityGUID equals nationality3.NationalityGUID into ljNationality3
                                                                  from nationality3 in ljNationality3.DefaultIfEmpty()
                                                                  join guarantorAgreementLineAffiliate3 in db.Set<GuarantorAgreementLineAffiliate>().Where(w => w.Ordering == 3)
                                                                      on guarantorAgreementLineNum.GuarantorAgreementLineGUID equals guarantorAgreementLineAffiliate3.GuarantorAgreementLineGUID into ljAuarantorAgreementLineAffiliate3
                                                                  from guarantorAgreementLineAffiliate3 in ljAuarantorAgreementLineAffiliate3.DefaultIfEmpty()
                                                                  join customerTable3 in db.Set<CustomerTable>()
                                                                      on guarantorAgreementLineAffiliate3.CustomerTableGUID equals customerTable3.CustomerTableGUID into ljCustomerLineAff3
                                                                  from customerTable3 in ljCustomerLineAff3.DefaultIfEmpty()
                                                                  join mainAgreementTable3 in db.Set<MainAgreementTable>()
                                                                      on guarantorAgreementLineAffiliate3.MainAgreementTableGUID equals mainAgreementTable3.MainAgreementTableGUID into ljMainGuarantor3
                                                                  from mainAgreementTable3 in ljMainGuarantor3.DefaultIfEmpty()
                                                                  join agreementTableInfo3 in db.Set<AgreementTableInfo>()
                                                                      on mainAgreementTable3.MainAgreementTableGUID equals agreementTableInfo3.RefGUID into ljMainAgree3
                                                                  from agreementTableInfo3 in ljMainAgree3.DefaultIfEmpty()



                                                                  join race4 in db.Set<Race>()
                                                                      on guarantorAgreementLineNum.RaceGUID equals race4.RaceGUID into ljRace4
                                                                  from race4 in ljRace4.DefaultIfEmpty()
                                                                  join nationality4 in db.Set<Nationality>()
                                                                      on guarantorAgreementLineNum.NationalityGUID equals nationality4.NationalityGUID into ljNationality4
                                                                  from nationality4 in ljNationality4.DefaultIfEmpty()
                                                                  join guarantorAgreementLineAffiliate4 in db.Set<GuarantorAgreementLineAffiliate>().Where(w => w.Ordering == 4)
                                                                      on guarantorAgreementLineNum.GuarantorAgreementLineGUID equals guarantorAgreementLineAffiliate4.GuarantorAgreementLineGUID into ljAuarantorAgreementLineAffiliate4
                                                                  from guarantorAgreementLineAffiliate4 in ljAuarantorAgreementLineAffiliate4.DefaultIfEmpty()
                                                                  join customerTable4 in db.Set<CustomerTable>()
                                                                      on guarantorAgreementLineAffiliate4.CustomerTableGUID equals customerTable4.CustomerTableGUID into ljCustomerLineAff4
                                                                  from customerTable4 in ljCustomerLineAff4.DefaultIfEmpty()
                                                                  join mainAgreementTable4 in db.Set<MainAgreementTable>()
                                                                      on guarantorAgreementLineAffiliate4.MainAgreementTableGUID equals mainAgreementTable4.MainAgreementTableGUID into ljMainGuarantor4
                                                                  from mainAgreementTable4 in ljMainGuarantor4.DefaultIfEmpty()
                                                                  join agreementTableInfo4 in db.Set<AgreementTableInfo>()
                                                                      on mainAgreementTable4.MainAgreementTableGUID equals agreementTableInfo4.RefGUID into ljMainAgree4
                                                                  from agreementTableInfo4 in ljMainAgree4.DefaultIfEmpty()



                                                                  join race5 in db.Set<Race>()
                                                                      on guarantorAgreementLineNum.RaceGUID equals race5.RaceGUID into ljRace5
                                                                  from race5 in ljRace5.DefaultIfEmpty()
                                                                  join nationality5 in db.Set<Nationality>()
                                                                      on guarantorAgreementLineNum.NationalityGUID equals nationality5.NationalityGUID into ljNationality5
                                                                  from nationality5 in ljNationality5.DefaultIfEmpty()
                                                                  join guarantorAgreementLineAffiliate5 in db.Set<GuarantorAgreementLineAffiliate>().Where(w => w.Ordering == 5)
                                                                      on guarantorAgreementLineNum.GuarantorAgreementLineGUID equals guarantorAgreementLineAffiliate5.GuarantorAgreementLineGUID into ljAuarantorAgreementLineAffiliate5
                                                                  from guarantorAgreementLineAffiliate5 in ljAuarantorAgreementLineAffiliate5.DefaultIfEmpty()
                                                                  join customerTable5 in db.Set<CustomerTable>()
                                                                      on guarantorAgreementLineAffiliate5.CustomerTableGUID equals customerTable5.CustomerTableGUID into ljCustomerLineAff5
                                                                  from customerTable5 in ljCustomerLineAff5.DefaultIfEmpty()
                                                                  join mainAgreementTable5 in db.Set<MainAgreementTable>()
                                                                      on guarantorAgreementLineAffiliate5.MainAgreementTableGUID equals mainAgreementTable5.MainAgreementTableGUID into ljMainGuarantor5
                                                                  from mainAgreementTable5 in ljMainGuarantor5.DefaultIfEmpty()
                                                                  join agreementTableInfo5 in db.Set<AgreementTableInfo>()
                                                                      on mainAgreementTable5.MainAgreementTableGUID equals agreementTableInfo5.RefGUID into ljMainAgree5
                                                                  from agreementTableInfo5 in ljMainAgree5.DefaultIfEmpty()



                                                                  join race6 in db.Set<Race>()
                                                                      on guarantorAgreementLineNum.RaceGUID equals race6.RaceGUID into ljRace6
                                                                  from race6 in ljRace6.DefaultIfEmpty()
                                                                  join nationality6 in db.Set<Nationality>()
                                                                      on guarantorAgreementLineNum.NationalityGUID equals nationality6.NationalityGUID into ljNationality6
                                                                  from nationality6 in ljNationality6.DefaultIfEmpty()
                                                                  join guarantorAgreementLineAffiliate6 in db.Set<GuarantorAgreementLineAffiliate>().Where(w => w.Ordering == 6)
                                                                      on guarantorAgreementLineNum.GuarantorAgreementLineGUID equals guarantorAgreementLineAffiliate6.GuarantorAgreementLineGUID into ljAuarantorAgreementLineAffiliate6
                                                                  from guarantorAgreementLineAffiliate6 in ljAuarantorAgreementLineAffiliate6.DefaultIfEmpty()
                                                                  join customerTable6 in db.Set<CustomerTable>()
                                                                      on guarantorAgreementLineAffiliate6.CustomerTableGUID equals customerTable6.CustomerTableGUID into ljCustomerLineAff6
                                                                  from customerTable6 in ljCustomerLineAff6.DefaultIfEmpty()
                                                                  join mainAgreementTable6 in db.Set<MainAgreementTable>()
                                                                      on guarantorAgreementLineAffiliate6.MainAgreementTableGUID equals mainAgreementTable6.MainAgreementTableGUID into ljMainGuarantor6
                                                                  from mainAgreementTable6 in ljMainGuarantor6.DefaultIfEmpty()
                                                                  join agreementTableInfo6 in db.Set<AgreementTableInfo>()
                                                                      on mainAgreementTable6.MainAgreementTableGUID equals agreementTableInfo6.RefGUID into ljMainAgree6
                                                                  from agreementTableInfo6 in ljMainAgree6.DefaultIfEmpty()


                                                                  join race7 in db.Set<Race>()
                                                                      on guarantorAgreementLineNum.RaceGUID equals race7.RaceGUID into ljRace7
                                                                  from race7 in ljRace7.DefaultIfEmpty()
                                                                  join nationality7 in db.Set<Nationality>()
                                                                      on guarantorAgreementLineNum.NationalityGUID equals nationality7.NationalityGUID into ljNationality7
                                                                  from nationality7 in ljNationality7.DefaultIfEmpty()
                                                                  join guarantorAgreementLineAffiliate7 in db.Set<GuarantorAgreementLineAffiliate>().Where(w => w.Ordering == 7)
                                                                      on guarantorAgreementLineNum.GuarantorAgreementLineGUID equals guarantorAgreementLineAffiliate7.GuarantorAgreementLineGUID into ljAuarantorAgreementLineAffiliate7
                                                                  from guarantorAgreementLineAffiliate7 in ljAuarantorAgreementLineAffiliate7.DefaultIfEmpty()
                                                                  join customerTable7 in db.Set<CustomerTable>()
                                                                      on guarantorAgreementLineAffiliate7.CustomerTableGUID equals customerTable7.CustomerTableGUID into ljCustomerLineAff7
                                                                  from customerTable7 in ljCustomerLineAff7.DefaultIfEmpty()
                                                                  join mainAgreementTable7 in db.Set<MainAgreementTable>()
                                                                      on guarantorAgreementLineAffiliate7.MainAgreementTableGUID equals mainAgreementTable7.MainAgreementTableGUID into ljMainGuarantor7
                                                                  from mainAgreementTable7 in ljMainGuarantor7.DefaultIfEmpty()
                                                                  join agreementTableInfo7 in db.Set<AgreementTableInfo>()
                                                                      on mainAgreementTable7.MainAgreementTableGUID equals agreementTableInfo7.RefGUID into ljMainAgree7
                                                                  from agreementTableInfo7 in ljMainAgree7.DefaultIfEmpty()



                                                                  join race8 in db.Set<Race>()
                                                                      on guarantorAgreementLineNum.RaceGUID equals race8.RaceGUID into ljRace8
                                                                  from race8 in ljRace8.DefaultIfEmpty()
                                                                  join nationality8 in db.Set<Nationality>()
                                                                      on guarantorAgreementLineNum.NationalityGUID equals nationality8.NationalityGUID into ljNationality8
                                                                  from nationality8 in ljNationality8.DefaultIfEmpty()
                                                                  join guarantorAgreementLineAffiliate8 in db.Set<GuarantorAgreementLineAffiliate>().Where(w => w.Ordering == 8)
                                                                      on guarantorAgreementLineNum.GuarantorAgreementLineGUID equals guarantorAgreementLineAffiliate8.GuarantorAgreementLineGUID into ljAuarantorAgreementLineAffiliate8
                                                                  from guarantorAgreementLineAffiliate8 in ljAuarantorAgreementLineAffiliate8.DefaultIfEmpty()
                                                                  join customerTable8 in db.Set<CustomerTable>()
                                                                      on guarantorAgreementLineAffiliate8.CustomerTableGUID equals customerTable8.CustomerTableGUID into ljCustomerLineAff8
                                                                  from customerTable8 in ljCustomerLineAff8.DefaultIfEmpty()
                                                                  join mainAgreementTable8 in db.Set<MainAgreementTable>()
                                                                      on guarantorAgreementLineAffiliate8.MainAgreementTableGUID equals mainAgreementTable8.MainAgreementTableGUID into ljMainGuarantor8
                                                                  from mainAgreementTable8 in ljMainGuarantor8.DefaultIfEmpty()
                                                                  join agreementTableInfo8 in db.Set<AgreementTableInfo>()
                                                                      on mainAgreementTable8.MainAgreementTableGUID equals agreementTableInfo8.RefGUID into ljMainAgree8
                                                                  from agreementTableInfo8 in ljMainAgree8.DefaultIfEmpty()
                                                                      #endregion

                                                                  where guarantorAgreementTable.GuarantorAgreementTableGUID == refGUID
                                                                  select new CrossGuarantorAgreementLineBookmarkView
                                                                  {

                                                                      #region GenCrossGuarantorAgreementLineView
                                                                      GuarantorName1st_1 = guarantorAgreementLineNum.Name,
                                                                      GuarantorName1st_2 = guarantorAgreementLineNum.Name,
                                                                      GuarantorName1st_3 = guarantorAgreementLineNum.Name,

                                                                      GuarantotOperatedBy = guarantorAgreementLineNum.OperatedBy,

                                                                      GuarantorAge1st_DateOfBirth = guarantorAgreementLineNum.DateOfBirth,
                                                                      GuarantorRace1st_1 = race != null ? race.Description : "",
                                                                      GuarantorNationality1st_1 = nationality != null ? nationality.Description : "",
                                                                      GuarantorAddress1st_1 = guarantorAgreementLineNum.PrimaryAddress1 + " " + guarantorAgreementLineNum.PrimaryAddress2,
                                                                      GuarantorTaxID1st_1 = guarantorAgreementLineNum.TaxId,


                                                                      GuarantorDateregis1st_1 = guarantorAgreementLineNum.DateOfIssue,
                                                                      RelatedName1st = customerTable != null ? customerTable.Name : "",
                                                                      RelatedAddress1st = agreementTableInfo1 != null ? (agreementTableInfo1.CustomerAddress1 + " " + agreementTableInfo1.CustomerAddress2) : "",
                                                                      RelatedContractNo1st = mainAgreementTable != null ? mainAgreementTable.MainAgreementId : "",
                                                                      RelatedContractDate1st = mainAgreementTable != null ? mainAgreementTable.AgreementDate : DateTime.MinValue,


                                                                      RelatedName2nd = customerTable2 != null ? customerTable2.Name : "",
                                                                      RelatedAddress2nd = agreementTableInfo2 != null ? (agreementTableInfo2.CustomerAddress1 + " " + agreementTableInfo2.CustomerAddress2) : "",
                                                                      RelatedContractNo2nd = mainAgreementTable2 != null ? mainAgreementTable2.MainAgreementId : "",
                                                                      RelatedContractDate2nd = mainAgreementTable2 != null ? mainAgreementTable2.AgreementDate : DateTime.MinValue,

                                                                      RelatedName3rd = customerTable3 != null ? customerTable3.Name : "",
                                                                      RelatedAddress3rd = agreementTableInfo3 != null ? (agreementTableInfo3.CustomerAddress1 + " " + agreementTableInfo3.CustomerAddress2) : "",
                                                                      RelatedContractNo3rd = mainAgreementTable3 != null ? mainAgreementTable3.MainAgreementId : "",
                                                                      RelatedContractDate3rd = mainAgreementTable3 != null ? mainAgreementTable3.AgreementDate : DateTime.MinValue,

                                                                      RelatedName4th = customerTable4 != null ? customerTable4.Name : "",
                                                                      RelatedAddress4th = agreementTableInfo4 != null ? (agreementTableInfo4.CustomerAddress1 + " " + agreementTableInfo4.CustomerAddress2) : "",
                                                                      RelatedContractNo4th = mainAgreementTable4 != null ? mainAgreementTable4.MainAgreementId : "",
                                                                      RelatedContractDate4th = mainAgreementTable4 != null ? mainAgreementTable4.AgreementDate : DateTime.MinValue,

                                                                      RelatedName5th = customerTable5 != null ? customerTable5.Name : "",
                                                                      RelatedAddress5th = agreementTableInfo5 != null ? (agreementTableInfo5.CustomerAddress1 + " " + agreementTableInfo5.CustomerAddress2) : "",
                                                                      RelatedContractNo5th = mainAgreementTable5 != null ? mainAgreementTable5.MainAgreementId : "",
                                                                      RelatedContractDate5th = mainAgreementTable5 != null ? mainAgreementTable5.AgreementDate : DateTime.MinValue,

                                                                      RelatedName6th = customerTable6 != null ? customerTable6.Name : "",
                                                                      RelatedAddress6th = agreementTableInfo6 != null ? (agreementTableInfo6.CustomerAddress1 + " " + agreementTableInfo6.CustomerAddress2) : "",
                                                                      RelatedContractNo6th = mainAgreementTable6 != null ? mainAgreementTable6.MainAgreementId : "",
                                                                      RelatedContractDate6th = mainAgreementTable6 != null ? mainAgreementTable6.AgreementDate : DateTime.MinValue,


                                                                      RelatedName7th = customerTable7 != null ? customerTable7.Name : "",
                                                                      RelatedAddress7th = agreementTableInfo7 != null ? (agreementTableInfo7.CustomerAddress1 + " " + agreementTableInfo7.CustomerAddress2) : "",
                                                                      RelatedContractNo7th = mainAgreementTable7 != null ? mainAgreementTable7.MainAgreementId : "",
                                                                      RelatedContractDate7th = mainAgreementTable2 != null ? mainAgreementTable7.AgreementDate : DateTime.MinValue,

                                                                      RelatedName8th = customerTable8 != null ? customerTable8.Name : "",
                                                                      RelatedAddress8th = agreementTableInfo8 != null ? (agreementTableInfo8.CustomerAddress1 + " " + agreementTableInfo8.CustomerAddress2) : "",
                                                                      RelatedContractNo8th = mainAgreementTable8 != null ? mainAgreementTable8.MainAgreementId : "",
                                                                      RelatedContractDate8th = mainAgreementTable8 != null ? mainAgreementTable8.AgreementDate : DateTime.MinValue,

                                                                      MaximumGuaranteeAmount = guarantorTrans.MaximumGuaranteeAmount,
                                                                      ApprovedCreditLimit = guarantorAgreementLineNum.ApprovedCreditLimit,

                                                                      AffiliateCustomerTaxId1st = customerTable != null ? customerTable.TaxID : "",
                                                                      AffiliateCustomerTaxId2nd = customerTable2 != null ? customerTable2.TaxID : "",
                                                                      AffiliateCustomerTaxId3rd = customerTable3 != null ? customerTable3.TaxID : "",
                                                                      AffiliateCustomerTaxId4th = customerTable4 != null ? customerTable4.TaxID : "",
                                                                      AffiliateCustomerTaxId5th = customerTable5 != null ? customerTable5.TaxID : "",
                                                                      AffiliateCustomerTaxId6th = customerTable6 != null ? customerTable6.TaxID : "",
                                                                      AffiliateCustomerTaxId7th = customerTable7 != null ? customerTable7.TaxID : "",
                                                                      AffiliateCustomerTaxId8th = customerTable8 != null ? customerTable8.TaxID : "",

                                                                      GuarantorName = relatedPersonTable != null ? relatedPersonTable.Name : "",
                                                                      GuarantorAgreementLineGUID = guarantorAgreementLineNum.GuarantorAgreementLineGUID
                                                                      #endregion

                                                                  }).AsNoTracking().FirstOrDefault();

                return result;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }



        public CrossGuarantorAgreementBookmarkView GetCrossGuarantorAgreementBookmarkView(Guid refGUID)
        {
            try
            {
                CrossGuarantorAgreementLineBookmarkView Qline1 = GetCrossGuarantorAgreementLineBookmarkView(refGUID, 1);
                CrossGuarantorAgreementLineBookmarkView Qline2 = GetCrossGuarantorAgreementLineBookmarkView(refGUID, 2);
                CrossGuarantorAgreementLineBookmarkView Qline3 = GetCrossGuarantorAgreementLineBookmarkView(refGUID, 3);
                CrossGuarantorAgreementLineBookmarkView Qline4 = GetCrossGuarantorAgreementLineBookmarkView(refGUID, 4);
                CrossGuarantorAgreementLineBookmarkView Qline5 = GetCrossGuarantorAgreementLineBookmarkView(refGUID, 5);
                CrossGuarantorAgreementLineBookmarkView Qline6 = GetCrossGuarantorAgreementLineBookmarkView(refGUID, 6);
                CrossGuarantorAgreementLineBookmarkView Qline7 = GetCrossGuarantorAgreementLineBookmarkView(refGUID, 7);
                CrossGuarantorAgreementLineBookmarkView Qline8 = GetCrossGuarantorAgreementLineBookmarkView(refGUID, 8);

                // R04
                IGuarantorAgreementLineAffiliateRepo guarantorAgreementLineAffiliateRepo = new GuarantorAgreementLineAffiliateRepo(db);
                int QLine1AffiliateSum = guarantorAgreementLineAffiliateRepo.GetListByGuarantorAgreementLine(Qline1.GuarantorAgreementLineGUID).Count();
                int QLine2AffiliateSum = guarantorAgreementLineAffiliateRepo.GetListByGuarantorAgreementLine(Qline2.GuarantorAgreementLineGUID).Count();
                int QLine3AffiliateSum = guarantorAgreementLineAffiliateRepo.GetListByGuarantorAgreementLine(Qline3.GuarantorAgreementLineGUID).Count();
                int QLine4AffiliateSum = guarantorAgreementLineAffiliateRepo.GetListByGuarantorAgreementLine(Qline4.GuarantorAgreementLineGUID).Count();
                int QLine5AffiliateSum = guarantorAgreementLineAffiliateRepo.GetListByGuarantorAgreementLine(Qline5.GuarantorAgreementLineGUID).Count();
                int QLine6AffiliateSum = guarantorAgreementLineAffiliateRepo.GetListByGuarantorAgreementLine(Qline6.GuarantorAgreementLineGUID).Count();
                int QLine7AffiliateSum = guarantorAgreementLineAffiliateRepo.GetListByGuarantorAgreementLine(Qline7.GuarantorAgreementLineGUID).Count();
                int QLine8AffiliateSum = guarantorAgreementLineAffiliateRepo.GetListByGuarantorAgreementLine(Qline8.GuarantorAgreementLineGUID).Count();

                var guarantorAgreementTableResult = (from guarantorAgreementTable in Entity

                                                     join agreementTableInfo in db.Set<AgreementTableInfo>()
                                                           .Where(w => w.RefType == (int)RefType.MainAgreement)
                                                       on guarantorAgreementTable.MainAgreementTableGUID equals agreementTableInfo.RefGUID
                                                       into ljAgreementInfo
                                                     from agreementTableInfo in ljAgreementInfo


                                                     join employeeTable1 in db.Set<EmployeeTable>()
                                                         on guarantorAgreementTable.WitnessCompany1GUID equals employeeTable1.EmployeeTableGUID into ljCompAgree1
                                                     from employeeTable1 in ljCompAgree1.DefaultIfEmpty()

                                                     join employeeTable2 in db.Set<EmployeeTable>()
                                                         on guarantorAgreementTable.WitnessCompany2GUID equals employeeTable2.EmployeeTableGUID into ljCompAgree2
                                                     from employeeTable2 in ljCompAgree2.DefaultIfEmpty()


                                                     where guarantorAgreementTable.GuarantorAgreementTableGUID == refGUID
                                                     select new CrossGuarantorAgreementBookmarkView
                                                     {
                                                         #region MappingGenCrossGuarantorAgreementView
                                                         GuarantorAgreementNo1 = guarantorAgreementTable.GuarantorAgreementId,
                                                         GuarantorAgreementNo2 = guarantorAgreementTable.GuarantorAgreementId,
                                                         GuarantorAgreementNo3 = guarantorAgreementTable.GuarantorAgreementId,
                                                         GuarantorAgreementNo4 = guarantorAgreementTable.GuarantorAgreementId,
                                                         GuarantorAgreementNo5 = guarantorAgreementTable.GuarantorAgreementId,
                                                         GuarantorAgreementNo6 = guarantorAgreementTable.GuarantorAgreementId,
                                                         GuarantorAgreementNo7 = guarantorAgreementTable.GuarantorAgreementId,
                                                         GuarantorAgreementNo8 = guarantorAgreementTable.GuarantorAgreementId,

                                                         qGuarantorAgreementDate1 = guarantorAgreementTable.AgreementDate,
                                                         qGuarantorAgreementDate2 = guarantorAgreementTable.AgreementDate,
                                                         qGuarantorAgreementDate3 = guarantorAgreementTable.AgreementDate,
                                                         qGuarantorAgreementDate4 = guarantorAgreementTable.AgreementDate,
                                                         qGuarantorAgreementDate5 = guarantorAgreementTable.AgreementDate,
                                                         qGuarantorAgreementDate6 = guarantorAgreementTable.AgreementDate,
                                                         qGuarantorAgreementDate7 = guarantorAgreementTable.AgreementDate,
                                                         qGuarantorAgreementDate8 = guarantorAgreementTable.AgreementDate,


                                                         GuarantorName1st_1 = Qline1 != null ? Qline1.GuarantorName1st_1 : "",
                                                         GuarantorName1st_2 = Qline1 != null ? Qline1.GuarantorName1st_2 : "",
                                                         GuarantorName1st_3 = Qline1 != null ? Qline1.GuarantorName1st_3 : "",

                                                         qGuarantorDateOfBirth1st_1 = Qline1.GuarantorAge1st_DateOfBirth,
                                                         GuarantorRace1st_1 = Qline1 != null ? Qline1.GuarantorRace1st_1 : "",
                                                         GuarantorNationality1st_1 = Qline1 != null ? Qline1.GuarantorNationality1st_1 : "",
                                                         GuarantorAddress1st_1 = Qline1 != null ? Qline1.GuarantorAddress1st_1 : "",
                                                         GuarantorTaxID1st_1 = Qline1 != null ? Qline1.GuarantorTaxID1st_1 : "",

                                                         qGuarantorDateregis1st_1 = Qline1 != null ? Qline1.GuarantorDateregis1st_1 : null,
                                                         RelatedName1st_Of1st_1 = Qline1 != null ? Qline1.RelatedName1st : "",
                                                         RelatedAddress1st_Of1st_1 = Qline1 != null ? Qline1.RelatedAddress1st : "",
                                                         RelatedContractNo1st_Of1st_1 = Qline1 != null ? Qline1.RelatedContractNo1st : "",
                                                         qRelatedContractDate1st_Of1st_1 = Qline1.RelatedContractDate1st != DateTime.MinValue ? Qline1.RelatedContractDate1st : null,
                                                         RelatedName2nd_Of1st_1 = Qline1 != null ? Qline1.RelatedName2nd : "",
                                                         RelatedAddress2nd_Of1st_1 = Qline1 != null ? Qline1.RelatedAddress2nd : "",
                                                         RelatedContractNo2nd_Of1st_1 = Qline1 != null ? Qline1.RelatedContractNo2nd : "",
                                                         qRelatedContractDate2nd_Of1st_1 = Qline1.RelatedContractDate2nd != DateTime.MinValue ? Qline1.RelatedContractDate2nd : null,
                                                         RelatedName3rd_Of1st_1 = Qline1 != null ? Qline1.RelatedName3rd : "",
                                                         RelatedAddress3rd_Of1st_1 = Qline1 != null ? Qline1.RelatedAddress3rd : "",
                                                         RelatedContractNo3rd_Of1st_1 = Qline1 != null ? Qline1.RelatedContractNo3rd : "",
                                                         qRelatedContractDate3rd_Of1st_1 = Qline1.RelatedContractDate3rd != DateTime.MinValue ? Qline1.RelatedContractDate3rd : null,
                                                         RelatedName4th_Of1st_1 = Qline1 != null ? Qline1.RelatedName4th : "",
                                                         RelatedAddress4th_Of1st_1 = Qline1 != null ? Qline1.RelatedAddress4th : "",
                                                         RelatedContractNo4th_Of1st_1 = Qline1 != null ? Qline1.RelatedContractNo4th : "",
                                                         qRelatedContractDate4th_Of1st_1 = Qline1.RelatedContractDate4th != DateTime.MinValue ? Qline1.RelatedContractDate4th : null,
                                                         RelatedName5th_Of1st_1 = Qline1 != null ? Qline1.RelatedName5th : "",
                                                         RelatedAddress5th_Of1st_1 = Qline1 != null ? Qline1.RelatedAddress5th : "",
                                                         RelatedContractNo5th_Of1st_1 = Qline1 != null ? Qline1.RelatedContractNo5th : "",
                                                         qRelatedContractDate5th_Of1st_1 = Qline1.RelatedContractDate5th != DateTime.MinValue ? Qline1.RelatedContractDate5th : null,
                                                         RelatedName6th_Of1st_1 = Qline1 != null ? Qline1.RelatedName6th : "",
                                                         RelatedAddress6th_Of1st_1 = Qline1 != null ? Qline1.RelatedAddress6th : "",
                                                         RelatedContractNo6th_Of1st_1 = Qline1 != null ? Qline1.RelatedContractNo6th : "",
                                                         qRelatedContractDate6th_Of1st_1 = Qline1.RelatedContractDate6th != DateTime.MinValue ? Qline1.RelatedContractDate6th : null,
                                                         RelatedName7th_Of1st_1 = Qline1 != null ? Qline1.RelatedName7th : "",
                                                         RelatedAddress7th_Of1st_1 = Qline1 != null ? Qline1.RelatedAddress7th : "",
                                                         RelatedContractNo7th_Of1st_1 = Qline1 != null ? Qline1.RelatedContractNo7th : "",
                                                         qRelatedContractDate7th_Of1st_1 = Qline1.RelatedContractDate7th != DateTime.MinValue ? Qline1.RelatedContractDate7th : null,
                                                         RelatedName8th_Of1st_1 = Qline1 != null ? Qline1.RelatedName8th : "",
                                                         RelatedAddress8th_Of1st_1 = Qline1 != null ? Qline1.RelatedAddress8th : "",
                                                         RelatedContractNo8th_Of1st_1 = Qline1 != null ? Qline1.RelatedContractNo8th : "",
                                                         qRelatedContractDate8th_Of1st_1 = Qline1.RelatedContractDate8th != DateTime.MinValue ? Qline1.RelatedContractDate8th : null,


                                                         GuarantorName2nd_1 = Qline2 != null ? Qline2.GuarantorName1st_1 : "",
                                                         GuarantorName2nd_2 = Qline2 != null ? Qline2.GuarantorName1st_2 : "",
                                                         qGuarantorDateOfBirth2nd_1 = Qline2.GuarantorAge1st_DateOfBirth,
                                                         GuarantorRace2nd_1 = Qline2 != null ? Qline2.GuarantorRace1st_1 : "",
                                                         GuarantorNationality2nd_1 = Qline2 != null ? Qline2.GuarantorNationality1st_1 : "",
                                                         GuarantorAddress2nd_1 = Qline2 != null ? Qline2.GuarantorAddress1st_1 : "",
                                                         GuarantorTaxID2nd_1 = Qline2 != null ? Qline2.GuarantorTaxID1st_1 : "",
                                                         qGuarantorDateregis2nd_1 = Qline2 != null ? Qline2.GuarantorDateregis1st_1 : null,
                                                         RelatedName1st_Of2nd_1 = Qline2 != null ? Qline2.RelatedName1st : "",
                                                         RelatedAddress1st_Of2nd_1 = Qline2 != null ? Qline2.RelatedAddress1st : "",
                                                         RelatedContractNo1st_Of2nd_1 = Qline2 != null ? Qline2.RelatedContractNo1st : "",
                                                         qRelatedContractDate1st_Of2nd_1 = Qline2.RelatedContractDate1st != DateTime.MinValue ? Qline2.RelatedContractDate1st : null,
                                                         RelatedName2nd_Of2nd_1 = Qline2 != null ? Qline2.RelatedName2nd : "",
                                                         RelatedAddress2nd_Of2nd_1 = Qline2 != null ? Qline2.RelatedAddress2nd : "",
                                                         RelatedContractNo2nd_Of2nd_1 = Qline2 != null ? Qline2.RelatedContractNo2nd : "",
                                                         qRelatedContractDate2nd_Of2nd_1 = Qline2.RelatedContractDate2nd != DateTime.MinValue ? Qline2.RelatedContractDate2nd : null,
                                                         RelatedName3rd_Of2nd_1 = Qline2 != null ? Qline2.RelatedName3rd : "",
                                                         RelatedAddress3rd_Of2nd_1 = Qline2 != null ? Qline2.RelatedAddress3rd : "",
                                                         RelatedContractNo3rd_Of2nd_1 = Qline2 != null ? Qline2.RelatedContractNo3rd : "",
                                                         qRelatedContractDate3rd_Of2nd_1 = Qline2.RelatedContractDate3rd != DateTime.MinValue ? Qline2.RelatedContractDate3rd : null,
                                                         RelatedName4th_Of2nd_1 = Qline2 != null ? Qline2.RelatedName4th : "",
                                                         RelatedAddress4th_Of2nd_1 = Qline2 != null ? Qline2.RelatedAddress4th : "",
                                                         RelatedContractNo4th_Of2nd_1 = Qline2 != null ? Qline2.RelatedContractNo4th : "",
                                                         qRelatedContractDate4th_Of2nd_1 = Qline2.RelatedContractDate4th != DateTime.MinValue ? Qline2.RelatedContractDate4th : null,
                                                         RelatedName5th_Of2nd_1 = Qline2 != null ? Qline2.RelatedName5th : "",
                                                         RelatedAddress5th_Of2nd_1 = Qline2 != null ? Qline2.RelatedAddress5th : "",
                                                         RelatedContractNo5th_Of2nd_1 = Qline2 != null ? Qline2.RelatedContractNo5th : "",
                                                         qRelatedContractDate5th_Of2nd_1 = Qline2.RelatedContractDate5th != DateTime.MinValue ? Qline2.RelatedContractDate5th : null,
                                                         RelatedName6th_Of2nd_1 = Qline2 != null ? Qline2.RelatedName6th : "",
                                                         RelatedAddress6th_Of2nd_1 = Qline2 != null ? Qline2.RelatedAddress6th : "",
                                                         RelatedContractNo6th_Of2nd_1 = Qline2 != null ? Qline2.RelatedContractNo6th : "",
                                                         qRelatedContractDate6th_Of2nd_1 = Qline2.RelatedContractDate6th != DateTime.MinValue ? Qline2.RelatedContractDate6th : null,
                                                         RelatedName7th_Of2nd_1 = Qline2 != null ? Qline2.RelatedName7th : "",
                                                         RelatedAddress7th_Of2nd_1 = Qline2 != null ? Qline2.RelatedAddress7th : "",
                                                         RelatedContractNo7th_Of2nd_1 = Qline2 != null ? Qline2.RelatedContractNo7th : "",
                                                         qRelatedContractDate7th_Of2nd_1 = Qline2.RelatedContractDate7th != DateTime.MinValue ? Qline2.RelatedContractDate7th : null,
                                                         RelatedName8th_Of2nd_1 = Qline2 != null ? Qline2.RelatedName8th : "",
                                                         RelatedAddress8th_Of2nd_1 = Qline2 != null ? Qline2.RelatedAddress8th : "",
                                                         RelatedContractNo8th_Of2nd_1 = Qline2 != null ? Qline2.RelatedContractNo8th : "",
                                                         qRelatedContractDate8th_Of2nd_1 = Qline2.RelatedContractDate8th != DateTime.MinValue ? Qline2.RelatedContractDate8th : null,


                                                         GuarantorName3rd_1 = Qline3 != null ? Qline3.GuarantorName1st_1 : "",
                                                         GuarantorName3rd_2 = Qline3 != null ? Qline3.GuarantorName1st_2 : "",
                                                         qGuarantorDateOfBirth3rd_1 = Qline3.GuarantorAge1st_DateOfBirth,
                                                         GuarantorRace3rd_1 = Qline3 != null ? Qline3.GuarantorRace1st_1 : "",
                                                         GuarantorNationality3rd_1 = Qline3 != null ? Qline3.GuarantorNationality1st_1 : "",
                                                         GuarantorAddress3rd_1 = Qline3 != null ? Qline3.GuarantorAddress1st_1 : "",
                                                         GuarantorTaxID3rd_1 = Qline3 != null ? Qline3.GuarantorTaxID1st_1 : "",
                                                         qGuarantorDateregis3rd_1 = Qline3 != null ? Qline3.GuarantorDateregis1st_1 : null,
                                                         RelatedName1st_Of3rd_1 = Qline3 != null ? Qline3.RelatedName1st : "",
                                                         RelatedAddress1st_Of3rd_1 = Qline3 != null ? Qline3.RelatedAddress1st : "",
                                                         RelatedContractNo1st_Of3rd_1 = Qline3 != null ? Qline3.RelatedContractNo1st : "",
                                                         qRelatedContractDate1st_Of3rd_1 = Qline3.RelatedContractDate1st != DateTime.MinValue ? Qline3.RelatedContractDate1st : null,
                                                         RelatedName2nd_Of3rd_1 = Qline3 != null ? Qline3.RelatedName2nd : "",
                                                         RelatedAddress2nd_Of3rd_1 = Qline3 != null ? Qline3.RelatedAddress2nd : "",
                                                         RelatedContractNo2nd_Of3rd_1 = Qline3 != null ? Qline3.RelatedContractNo2nd : "",
                                                         qRelatedContractDate2nd_Of3rd_1 = Qline3.RelatedContractDate2nd != DateTime.MinValue ? Qline3.RelatedContractDate2nd : null,
                                                         RelatedName3rd_Of3rd_1 = Qline3 != null ? Qline3.RelatedName3rd : "",
                                                         RelatedAddress3rd_Of3rd_1 = Qline3 != null ? Qline3.RelatedAddress3rd : "",
                                                         RelatedContractNo3rd_Of3rd_1 = Qline3 != null ? Qline3.RelatedContractNo3rd : "",
                                                         qRelatedContractDate3rd_Of3rd_1 = Qline3.RelatedContractDate3rd != DateTime.MinValue ? Qline3.RelatedContractDate3rd : null,
                                                         RelatedName4th_Of3rd_1 = Qline3 != null ? Qline3.RelatedName4th : "",
                                                         RelatedAddress4th_Of3rd_1 = Qline3 != null ? Qline3.RelatedAddress4th : "",
                                                         RelatedContractNo4th_Of3rd_1 = Qline3 != null ? Qline3.RelatedContractNo4th : "",
                                                         qRelatedContractDate4th_Of3rd_1 = Qline3.RelatedContractDate4th != DateTime.MinValue ? Qline3.RelatedContractDate4th : null,
                                                         RelatedName5th_Of3rd_1 = Qline3 != null ? Qline3.RelatedName5th : "",
                                                         RelatedAddress5th_Of3rd_1 = Qline3 != null ? Qline3.RelatedAddress5th : "",
                                                         RelatedContractNo5th_Of3rd_1 = Qline3 != null ? Qline3.RelatedContractNo5th : "",
                                                         qRelatedContractDate5th_Of3rd_1 = Qline3.RelatedContractDate5th != DateTime.MinValue ? Qline3.RelatedContractDate5th : null,
                                                         RelatedName6th_Of3rd_1 = Qline3 != null ? Qline3.RelatedName6th : "",
                                                         RelatedAddress6th_Of3rd_1 = Qline3 != null ? Qline3.RelatedAddress6th : "",
                                                         RelatedContractNo6th_Of3rd_1 = Qline3 != null ? Qline3.RelatedContractNo6th : "",
                                                         qRelatedContractDate6th_Of3rd_1 = Qline3.RelatedContractDate6th != DateTime.MinValue ? Qline3.RelatedContractDate6th : null,
                                                         RelatedName7th_Of3rd_1 = Qline3 != null ? Qline3.RelatedName7th : "",
                                                         RelatedAddress7th_Of3rd_1 = Qline3 != null ? Qline3.RelatedAddress7th : "",
                                                         RelatedContractNo7th_Of3rd_1 = Qline3 != null ? Qline3.RelatedContractNo7th : "",
                                                         qRelatedContractDate7th_Of3rd_1 = Qline3.RelatedContractDate7th != DateTime.MinValue ? Qline3.RelatedContractDate7th : null,
                                                         RelatedName8th_Of3rd_1 = Qline3 != null ? Qline3.RelatedName8th : "",
                                                         RelatedAddress8th_Of3rd_1 = Qline3 != null ? Qline3.RelatedAddress8th : "",
                                                         RelatedContractNo8th_Of3rd_1 = Qline3 != null ? Qline3.RelatedContractNo8th : "",
                                                         qRelatedContractDate8th_Of3rd_1 = Qline3.RelatedContractDate8th != DateTime.MinValue ? Qline3.RelatedContractDate8th : null,

                                                         GuarantorName4th_1 = Qline4 != null ? Qline4.GuarantorName1st_1 : "",
                                                         GuarantorName4th_2 = Qline4 != null ? Qline4.GuarantorName1st_2 : "",
                                                         qGuarantorDateOfBirth4th_1 = Qline4.GuarantorAge1st_DateOfBirth,
                                                         GuarantorRace4th_1 = Qline4 != null ? Qline4.GuarantorRace1st_1 : "",
                                                         GuarantorNationality4th_1 = Qline4 != null ? Qline4.GuarantorNationality1st_1 : "",
                                                         GuarantorAddress4th_1 = Qline4 != null ? Qline4.GuarantorAddress1st_1 : "",
                                                         GuarantorTaxID4th_1 = Qline4 != null ? Qline4.GuarantorTaxID1st_1 : "",
                                                         qGuarantorDateregis4th_1 = Qline4 != null ? Qline4.GuarantorDateregis1st_1 : null,
                                                         RelatedName1st_Of4th_1 = Qline4 != null ? Qline4.RelatedName1st : "",
                                                         RelatedAddress1st_Of4th_1 = Qline4 != null ? Qline4.RelatedAddress1st : "",
                                                         RelatedContractNo1st_Of4th_1 = Qline4 != null ? Qline4.RelatedContractNo1st : "",
                                                         qRelatedContractDate1st_Of4th_1 = Qline4.RelatedContractDate1st != DateTime.MinValue ? Qline4.RelatedContractDate1st : null,
                                                         RelatedName2nd_Of4th_1 = Qline4 != null ? Qline4.RelatedName2nd : "",
                                                         RelatedAddress2nd_Of4th_1 = Qline4 != null ? Qline4.RelatedAddress2nd : "",
                                                         RelatedContractNo2nd_Of4th_1 = Qline4 != null ? Qline4.RelatedContractNo2nd : "",
                                                         qRelatedContractDate2nd_Of4th_1 = Qline4.RelatedContractDate2nd != DateTime.MinValue ? Qline4.RelatedContractDate2nd : null,
                                                         RelatedName3rd_Of4th_1 = Qline4 != null ? Qline4.RelatedName3rd : "",
                                                         RelatedAddress3rd_Of4th_1 = Qline4 != null ? Qline4.RelatedAddress3rd : "",
                                                         RelatedContractNo3rd_Of4th_1 = Qline4 != null ? Qline4.RelatedContractNo3rd : "",
                                                         qRelatedContractDate3rd_Of4th_1 = Qline4.RelatedContractDate3rd != DateTime.MinValue ? Qline4.RelatedContractDate3rd : null,
                                                         RelatedName4th_Of4th_1 = Qline4 != null ? Qline4.RelatedName4th : "",
                                                         RelatedAddress4th_Of4th_1 = Qline4 != null ? Qline4.RelatedAddress4th : "",
                                                         RelatedContractNo4th_Of4th_1 = Qline4 != null ? Qline4.RelatedContractNo4th : "",
                                                         qRelatedContractDate4th_Of4th_1 = Qline4.RelatedContractDate4th != DateTime.MinValue ? Qline4.RelatedContractDate4th : null,
                                                         RelatedName5th_Of4th_1 = Qline4 != null ? Qline4.RelatedName5th : "",
                                                         RelatedAddress5th_Of4th_1 = Qline4 != null ? Qline4.RelatedAddress5th : "",
                                                         RelatedContractNo5th_Of4th_1 = Qline4 != null ? Qline4.RelatedContractNo5th : "",
                                                         qRelatedContractDate5th_Of4th_1 = Qline4.RelatedContractDate5th != DateTime.MinValue ? Qline4.RelatedContractDate5th : null,
                                                         RelatedName6th_Of4th_1 = Qline4 != null ? Qline4.RelatedName6th : "",
                                                         RelatedAddress6th_Of4th_1 = Qline4 != null ? Qline4.RelatedAddress6th : "",
                                                         RelatedContractNo6th_Of4th_1 = Qline4 != null ? Qline4.RelatedContractNo6th : "",
                                                         qRelatedContractDate6th_Of4th_1 = Qline4.RelatedContractDate6th != DateTime.MinValue ? Qline4.RelatedContractDate6th : null,
                                                         RelatedName7th_Of4th_1 = Qline4 != null ? Qline4.RelatedName7th : "",
                                                         RelatedAddress7th_Of4th_1 = Qline4 != null ? Qline4.RelatedAddress7th : "",
                                                         RelatedContractNo7th_Of4th_1 = Qline4 != null ? Qline4.RelatedContractNo7th : "",
                                                         qRelatedContractDate7th_Of4th_1 = Qline4.RelatedContractDate7th != DateTime.MinValue ? Qline4.RelatedContractDate7th : null,
                                                         RelatedName8th_Of4th_1 = Qline4 != null ? Qline4.RelatedName8th : "",
                                                         RelatedAddress8th_Of4th_1 = Qline4 != null ? Qline4.RelatedAddress8th : "",
                                                         RelatedContractNo8th_Of4th_1 = Qline4 != null ? Qline4.RelatedContractNo8th : "",
                                                         qRelatedContractDate8th_Of4th_1 = Qline4.RelatedContractDate8th != DateTime.MinValue ? Qline4.RelatedContractDate8th : null,

                                                         GuarantorName5th_1 = Qline5 != null ? Qline5.GuarantorName1st_1 : "",
                                                         GuarantorName5th_2 = Qline5 != null ? Qline5.GuarantorName1st_2 : "",
                                                         qGuarantorDateOfBirth5th_1 = Qline5.GuarantorAge1st_DateOfBirth,
                                                         GuarantorRace5th_1 = Qline5 != null ? Qline5.GuarantorRace1st_1 : "",
                                                         GuarantorNationality5th_1 = Qline5 != null ? Qline5.GuarantorNationality1st_1 : "",
                                                         GuarantorAddress5th_1 = Qline5 != null ? Qline5.GuarantorAddress1st_1 : "",
                                                         GuarantorTaxID5th_1 = Qline5 != null ? Qline5.GuarantorTaxID1st_1 : "",
                                                         qGuarantorDateregis5th_1 = Qline5 != null ? Qline5.GuarantorDateregis1st_1 : null,
                                                         RelatedName1st_Of5th_1 = Qline5 != null ? Qline5.RelatedName1st : "",
                                                         RelatedAddress1st_Of5th_1 = Qline5 != null ? Qline5.RelatedAddress1st : "",
                                                         RelatedContractNo1st_Of5th_1 = Qline5 != null ? Qline5.RelatedContractNo1st : "",
                                                         qRelatedContractDate1st_Of5th_1 = Qline5.RelatedContractDate1st != DateTime.MinValue ? Qline5.RelatedContractDate1st : null,
                                                         RelatedName2nd_Of5th_1 = Qline5 != null ? Qline5.RelatedName2nd : "",
                                                         RelatedAddress2nd_Of5th_1 = Qline5 != null ? Qline5.RelatedAddress2nd : "",
                                                         RelatedContractNo2nd_Of5th_1 = Qline5 != null ? Qline5.RelatedContractNo2nd : "",
                                                         qRelatedContractDate2nd_Of5th_1 = Qline5.RelatedContractDate2nd != DateTime.MinValue ? Qline5.RelatedContractDate2nd : null,
                                                         RelatedName3rd_Of5th_1 = Qline5 != null ? Qline5.RelatedName3rd : "",
                                                         RelatedAddress3rd_Of5th_1 = Qline5 != null ? Qline5.RelatedAddress3rd : "",
                                                         RelatedContractNo3rd_Of5th_1 = Qline5 != null ? Qline5.RelatedContractNo3rd : "",
                                                         qRelatedContractDate3rd_Of5th_1 = Qline5.RelatedContractDate3rd != DateTime.MinValue ? Qline5.RelatedContractDate3rd : null,
                                                         RelatedName4th_Of5th_1 = Qline5 != null ? Qline5.RelatedName4th : "",
                                                         RelatedAddress4th_Of5th_1 = Qline5 != null ? Qline5.RelatedAddress4th : "",
                                                         RelatedContractNo4th_Of5th_1 = Qline5 != null ? Qline5.RelatedContractNo4th : "",
                                                         qRelatedContractDate4th_Of5th_1 = Qline5.RelatedContractDate4th != DateTime.MinValue ? Qline5.RelatedContractDate4th : null,
                                                         RelatedName5th_Of5th_1 = Qline5 != null ? Qline5.RelatedName5th : "",
                                                         RelatedAddress5th_Of5th_1 = Qline5 != null ? Qline5.RelatedAddress5th : "",
                                                         RelatedContractNo5th_Of5th_1 = Qline5 != null ? Qline5.RelatedContractNo5th : "",
                                                         qRelatedContractDate5th_Of5th_1 = Qline5.RelatedContractDate5th != DateTime.MinValue ? Qline5.RelatedContractDate5th : null,
                                                         RelatedName6th_Of5th_1 = Qline5 != null ? Qline5.RelatedName6th : "",
                                                         RelatedAddress6th_Of5th_1 = Qline5 != null ? Qline5.RelatedAddress6th : "",
                                                         RelatedContractNo6th_Of5th_1 = Qline5 != null ? Qline5.RelatedContractNo6th : "",
                                                         qRelatedContractDate6th_Of5th_1 = Qline5.RelatedContractDate6th != DateTime.MinValue ? Qline5.RelatedContractDate6th : null,
                                                         RelatedName7th_Of5th_1 = Qline5 != null ? Qline5.RelatedName7th : "",
                                                         RelatedAddress7th_Of5th_1 = Qline5 != null ? Qline5.RelatedAddress7th : "",
                                                         RelatedContractNo7th_Of5th_1 = Qline5 != null ? Qline5.RelatedContractNo7th : "",
                                                         qRelatedContractDate7th_Of5th_1 = Qline5.RelatedContractDate7th != DateTime.MinValue ? Qline5.RelatedContractDate7th : null,
                                                         RelatedName8th_Of5th_1 = Qline5 != null ? Qline5.RelatedName8th : "",
                                                         RelatedAddress8th_Of5th_1 = Qline5 != null ? Qline5.RelatedAddress8th : "",
                                                         RelatedContractNo8th_Of5th_1 = Qline5 != null ? Qline5.RelatedContractNo8th : "",
                                                         qRelatedContractDate8th_Of5th_1 = Qline5.RelatedContractDate8th != DateTime.MinValue ? Qline5.RelatedContractDate8th : null,


                                                         GuarantorName6th_1 = Qline6 != null ? Qline6.GuarantorName1st_1 : "",
                                                         GuarantorName6th_2 = Qline6 != null ? Qline6.GuarantorName1st_2 : "",
                                                         qGuarantorDateOfBirth6th_1 = Qline6.GuarantorAge1st_DateOfBirth,
                                                         GuarantorRace6th_1 = Qline6 != null ? Qline6.GuarantorRace1st_1 : "",
                                                         GuarantorNationality6th_1 = Qline6 != null ? Qline6.GuarantorNationality1st_1 : "",
                                                         GuarantorAddress6th_1 = Qline6 != null ? Qline6.GuarantorAddress1st_1 : "",
                                                         GuarantorTaxID6th_1 = Qline6 != null ? Qline6.GuarantorTaxID1st_1 : "",
                                                         qGuarantorDateregis6th_1 = Qline6 != null ? Qline6.GuarantorDateregis1st_1 : null,
                                                         RelatedName1st_Of6th_1 = Qline6 != null ? Qline6.RelatedName1st : "",
                                                         RelatedAddress1st_Of6th_1 = Qline6 != null ? Qline6.RelatedAddress1st : "",
                                                         RelatedContractNo1st_Of6th_1 = Qline6 != null ? Qline6.RelatedContractNo1st : "",
                                                         qRelatedContractDate1st_Of6th_1 = Qline6.RelatedContractDate1st != DateTime.MinValue ? Qline6.RelatedContractDate1st : null,
                                                         RelatedName2nd_Of6th_1 = Qline6 != null ? Qline6.RelatedName2nd : "",
                                                         RelatedAddress2nd_Of6th_1 = Qline6 != null ? Qline6.RelatedAddress2nd : "",
                                                         RelatedContractNo2nd_Of6th_1 = Qline6 != null ? Qline6.RelatedContractNo2nd : "",
                                                         qRelatedContractDate2nd_Of6th_1 = Qline6.RelatedContractDate2nd != DateTime.MinValue ? Qline6.RelatedContractDate2nd : null,
                                                         RelatedName3rd_Of6th_1 = Qline6 != null ? Qline6.RelatedName3rd : "",
                                                         RelatedAddress3rd_Of6th_1 = Qline6 != null ? Qline6.RelatedAddress3rd : "",
                                                         RelatedContractNo3rd_Of6th_1 = Qline6 != null ? Qline6.RelatedContractNo3rd : "",
                                                         qRelatedContractDate3rd_Of6th_1 = Qline6.RelatedContractDate3rd != DateTime.MinValue ? Qline6.RelatedContractDate3rd : null,
                                                         RelatedName4th_Of6th_1 = Qline6 != null ? Qline6.RelatedName4th : "",
                                                         RelatedAddress4th_Of6th_1 = Qline6 != null ? Qline6.RelatedAddress4th : "",
                                                         RelatedContractNo4th_Of6th_1 = Qline6 != null ? Qline6.RelatedContractNo4th : "",
                                                         qRelatedContractDate4th_Of6th_1 = Qline6.RelatedContractDate4th != DateTime.MinValue ? Qline6.RelatedContractDate4th : null,
                                                         RelatedName5th_Of6th_1 = Qline6 != null ? Qline6.RelatedName5th : "",
                                                         RelatedAddress5th_Of6th_1 = Qline6 != null ? Qline6.RelatedAddress5th : "",
                                                         RelatedContractNo5th_Of6th_1 = Qline6 != null ? Qline6.RelatedContractNo5th : "",
                                                         qRelatedContractDate5th_Of6th_1 = Qline6.RelatedContractDate5th != DateTime.MinValue ? Qline6.RelatedContractDate5th : null,
                                                         RelatedName6th_Of6th_1 = Qline6 != null ? Qline6.RelatedName6th : "",
                                                         RelatedAddress6th_Of6th_1 = Qline6 != null ? Qline6.RelatedAddress6th : "",
                                                         RelatedContractNo6th_Of6th_1 = Qline6 != null ? Qline6.RelatedContractNo6th : "",
                                                         qRelatedContractDate6th_Of6th_1 = Qline6.RelatedContractDate6th != DateTime.MinValue ? Qline6.RelatedContractDate6th : null,
                                                         RelatedName7th_Of6th_1 = Qline6 != null ? Qline6.RelatedName7th : "",
                                                         RelatedAddress7th_Of6th_1 = Qline6 != null ? Qline6.RelatedAddress7th : "",
                                                         RelatedContractNo7th_Of6th_1 = Qline6 != null ? Qline6.RelatedContractNo7th : "",
                                                         qRelatedContractDate7th_Of6th_1 = Qline6.RelatedContractDate7th != DateTime.MinValue ? Qline6.RelatedContractDate7th : null,
                                                         RelatedName8th_Of6th_1 = Qline6 != null ? Qline6.RelatedName8th : "",
                                                         RelatedAddress8th_Of6th_1 = Qline6 != null ? Qline6.RelatedAddress8th : "",
                                                         RelatedContractNo8th_Of6th_1 = Qline6 != null ? Qline6.RelatedContractNo8th : "",
                                                         qRelatedContractDate8th_Of6th_1 = Qline6.RelatedContractDate8th != DateTime.MinValue ? Qline6.RelatedContractDate8th : null,

                                                         GuarantorName7th_1 = Qline7 != null ? Qline7.GuarantorName1st_1 : "",
                                                         GuarantorName7th_2 = Qline7 != null ? Qline7.GuarantorName1st_2 : "",
                                                         qGuarantorDateOfBirth7th_1 = Qline7.GuarantorAge1st_DateOfBirth,
                                                         GuarantorRace7th_1 = Qline7 != null ? Qline7.GuarantorRace1st_1 : "",
                                                         GuarantorNationality7th_1 = Qline7 != null ? Qline7.GuarantorNationality1st_1 : "",
                                                         GuarantorAddress7th_1 = Qline7 != null ? Qline7.GuarantorAddress1st_1 : "",
                                                         GuarantorTaxID7th_1 = Qline7 != null ? Qline7.GuarantorTaxID1st_1 : "",
                                                         qGuarantorDateregis7th_1 = Qline7 != null ? Qline7.GuarantorDateregis1st_1 : null,
                                                         RelatedName1st_Of7th_1 = Qline7 != null ? Qline7.RelatedName1st : "",
                                                         RelatedAddress1st_Of7th_1 = Qline7 != null ? Qline7.RelatedAddress1st : "",
                                                         RelatedContractNo1st_Of7th_1 = Qline7 != null ? Qline7.RelatedContractNo1st : "",
                                                         qRelatedContractDate1st_Of7th_1 = Qline7.RelatedContractDate1st != DateTime.MinValue ? Qline7.RelatedContractDate1st : null,
                                                         RelatedName2nd_Of7th_1 = Qline7 != null ? Qline7.RelatedName2nd : "",
                                                         RelatedAddress2nd_Of7th_1 = Qline7 != null ? Qline7.RelatedAddress2nd : "",
                                                         RelatedContractNo2nd_Of7th_1 = Qline7 != null ? Qline7.RelatedContractNo2nd : "",
                                                         qRelatedContractDate2nd_Of7th_1 = Qline7.RelatedContractDate2nd != DateTime.MinValue ? Qline7.RelatedContractDate2nd : null,
                                                         RelatedName3rd_Of7th_1 = Qline7 != null ? Qline7.RelatedName3rd : "",
                                                         RelatedAddress3rd_Of7th_1 = Qline7 != null ? Qline7.RelatedAddress3rd : "",
                                                         RelatedContractNo3rd_Of7th_1 = Qline7 != null ? Qline7.RelatedContractNo3rd : "",
                                                         qRelatedContractDate3rd_Of7th_1 = Qline7.RelatedContractDate3rd != DateTime.MinValue ? Qline7.RelatedContractDate3rd : null,
                                                         RelatedName4th_Of7th_1 = Qline7 != null ? Qline7.RelatedName4th : "",
                                                         RelatedAddress4th_Of7th_1 = Qline7 != null ? Qline7.RelatedAddress4th : "",
                                                         RelatedContractNo4th_Of7th_1 = Qline7 != null ? Qline7.RelatedContractNo4th : "",
                                                         qRelatedContractDate4th_Of7th_1 = Qline7.RelatedContractDate4th != DateTime.MinValue ? Qline7.RelatedContractDate4th : null,
                                                         RelatedName5th_Of7th_1 = Qline7 != null ? Qline7.RelatedName5th : "",
                                                         RelatedAddress5th_Of7th_1 = Qline7 != null ? Qline7.RelatedAddress5th : "",
                                                         RelatedContractNo5th_Of7th_1 = Qline7 != null ? Qline7.RelatedContractNo5th : "",
                                                         qRelatedContractDate5th_Of7th_1 = Qline7.RelatedContractDate5th != DateTime.MinValue ? Qline7.RelatedContractDate5th : null,
                                                         RelatedName6th_Of7th_1 = Qline7 != null ? Qline7.RelatedName6th : "",
                                                         RelatedAddress6th_Of7th_1 = Qline7 != null ? Qline7.RelatedAddress6th : "",
                                                         RelatedContractNo6th_Of7th_1 = Qline7 != null ? Qline7.RelatedContractNo6th : "",
                                                         qRelatedContractDate6th_Of7th_1 = Qline7.RelatedContractDate6th != DateTime.MinValue ? Qline7.RelatedContractDate6th : null,
                                                         RelatedName7th_Of7th_1 = Qline7 != null ? Qline7.RelatedName7th : "",
                                                         RelatedAddress7th_Of7th_1 = Qline7 != null ? Qline7.RelatedAddress7th : "",
                                                         RelatedContractNo7th_Of7th_1 = Qline7 != null ? Qline7.RelatedContractNo7th : "",
                                                         qRelatedContractDate7th_Of7th_1 = Qline7.RelatedContractDate7th != DateTime.MinValue ? Qline7.RelatedContractDate7th : null,
                                                         RelatedName8th_Of7th_1 = Qline7 != null ? Qline7.RelatedName8th : "",
                                                         RelatedAddress8th_Of7th_1 = Qline7 != null ? Qline7.RelatedAddress8th : "",
                                                         RelatedContractNo8th_Of7th_1 = Qline7 != null ? Qline7.RelatedContractNo8th : "",
                                                         qRelatedContractDate8th_Of7th_1 = Qline7.RelatedContractDate8th != DateTime.MinValue ? Qline7.RelatedContractDate8th : null,

                                                         GuarantorName8th_1 = Qline8 != null ? Qline8.GuarantorName1st_1 : "",
                                                         GuarantorName8th_2 = Qline8 != null ? Qline8.GuarantorName1st_2 : "",
                                                         qGuarantorDateOfBirth8th_1 = Qline8.GuarantorAge1st_DateOfBirth,
                                                         GuarantorRace8th_1 = Qline8 != null ? Qline8.GuarantorRace1st_1 : "",
                                                         GuarantorNationality8th_1 = Qline8 != null ? Qline8.GuarantorNationality1st_1 : "",
                                                         GuarantorAddress8th_1 = Qline8 != null ? Qline8.GuarantorAddress1st_1 : "",
                                                         GuarantorTaxID8th_1 = Qline8 != null ? Qline8.GuarantorTaxID1st_1 : "",
                                                         qGuarantorDateregis8th_1 = Qline8 != null ? Qline8.GuarantorDateregis1st_1 : null,
                                                         RelatedName1st_Of8th_1 = Qline8 != null ? Qline8.RelatedName1st : "",
                                                         RelatedAddress1st_Of8th_1 = Qline8 != null ? Qline8.RelatedAddress1st : "",
                                                         RelatedContractNo1st_Of8th_1 = Qline8 != null ? Qline8.RelatedContractNo1st : "",
                                                         qRelatedContractDate1st_Of8th_1 = Qline8.RelatedContractDate1st != DateTime.MinValue ? Qline8.RelatedContractDate1st : null,
                                                         RelatedName2nd_Of8th_1 = Qline8 != null ? Qline8.RelatedName2nd : "",
                                                         RelatedAddress2nd_Of8th_1 = Qline8 != null ? Qline8.RelatedAddress2nd : "",
                                                         RelatedContractNo2nd_Of8th_1 = Qline8 != null ? Qline8.RelatedContractNo2nd : "",
                                                         qRelatedContractDate2nd_Of8th_1 = Qline8.RelatedContractDate2nd != DateTime.MinValue ? Qline8.RelatedContractDate2nd : null,
                                                         RelatedName3rd_Of8th_1 = Qline8 != null ? Qline8.RelatedName3rd : "",
                                                         RelatedAddress3rd_Of8th_1 = Qline8 != null ? Qline8.RelatedAddress3rd : "",
                                                         RelatedContractNo3rd_Of8th_1 = Qline8 != null ? Qline8.RelatedContractNo3rd : "",
                                                         qRelatedContractDate3rd_Of8th_1 = Qline8.RelatedContractDate3rd != DateTime.MinValue ? Qline8.RelatedContractDate3rd : null,
                                                         RelatedName4th_Of8th_1 = Qline8 != null ? Qline8.RelatedName4th : "",
                                                         RelatedAddress4th_Of8th_1 = Qline8 != null ? Qline8.RelatedAddress4th : "",
                                                         RelatedContractNo4th_Of8th_1 = Qline8 != null ? Qline8.RelatedContractNo4th : "",
                                                         qRelatedContractDate4th_Of8th_1 = Qline8.RelatedContractDate4th != DateTime.MinValue ? Qline8.RelatedContractDate4th : null,
                                                         RelatedName5th_Of8th_1 = Qline8 != null ? Qline8.RelatedName5th : "",
                                                         RelatedAddress5th_Of8th_1 = Qline8 != null ? Qline8.RelatedAddress5th : "",
                                                         RelatedContractNo5th_Of8th_1 = Qline8 != null ? Qline8.RelatedContractNo5th : "",
                                                         qRelatedContractDate5th_Of8th_1 = Qline8.RelatedContractDate5th != DateTime.MinValue ? Qline8.RelatedContractDate5th : null,
                                                         RelatedName6th_Of8th_1 = Qline8 != null ? Qline8.RelatedName6th : "",
                                                         RelatedAddress6th_Of8th_1 = Qline8 != null ? Qline8.RelatedAddress6th : "",
                                                         RelatedContractNo6th_Of8th_1 = Qline8 != null ? Qline8.RelatedContractNo6th : "",
                                                         qRelatedContractDate6th_Of8th_1 = Qline8.RelatedContractDate6th != DateTime.MinValue ? Qline8.RelatedContractDate6th : null,
                                                         RelatedName7th_Of8th_1 = Qline8 != null ? Qline8.RelatedName7th : "",
                                                         RelatedAddress7th_Of8th_1 = Qline8 != null ? Qline8.RelatedAddress7th : "",
                                                         RelatedContractNo7th_Of8th_1 = Qline8 != null ? Qline8.RelatedContractNo7th : "",
                                                         qRelatedContractDate7th_Of8th_1 = Qline8.RelatedContractDate7th != DateTime.MinValue ? Qline8.RelatedContractDate7th : null,
                                                         RelatedName8th_Of8th_1 = Qline8 != null ? Qline8.RelatedName8th : "",
                                                         RelatedAddress8th_Of8th_1 = Qline8 != null ? Qline8.RelatedAddress8th : "",
                                                         RelatedContractNo8th_Of8th_1 = Qline8 != null ? Qline8.RelatedContractNo8th : "",
                                                         qRelatedContractDate8th_Of8th_1 = Qline8.RelatedContractDate8th != DateTime.MinValue ? Qline8.RelatedContractDate8th : null,
                                                         #endregion

                                                         GuarantorYear1 = guarantorAgreementTable.AgreementYear,
                                                         GuarantorYear2 = guarantorAgreementTable.AgreementYear,
                                                         GuarantorYear3 = guarantorAgreementTable.AgreementYear,
                                                         GuarantorYear4 = guarantorAgreementTable.AgreementYear,
                                                         GuarantorYear5 = guarantorAgreementTable.AgreementYear,
                                                         GuarantorYear6 = guarantorAgreementTable.AgreementYear,
                                                         GuarantorYear7 = guarantorAgreementTable.AgreementYear,
                                                         GuarantorYear8 = guarantorAgreementTable.AgreementYear,


                                                         WitnessFirst1 = employeeTable1 != null ? employeeTable1.Name : "",
                                                         WitnessFirst2 = employeeTable1 != null ? employeeTable1.Name : "",
                                                         WitnessFirst3 = employeeTable1 != null ? employeeTable1.Name : "",
                                                         WitnessFirst4 = employeeTable1 != null ? employeeTable1.Name : "",
                                                         WitnessFirst5 = employeeTable1 != null ? employeeTable1.Name : "",
                                                         WitnessFirst6 = employeeTable1 != null ? employeeTable1.Name : "",
                                                         WitnessFirst7 = employeeTable1 != null ? employeeTable1.Name : "",
                                                         WitnessFirst8 = employeeTable1 != null ? employeeTable1.Name : "",

                                                         WitnessSecond1 = employeeTable2 != null ? employeeTable2.Name : "",
                                                         WitnessSecond2 = employeeTable2 != null ? employeeTable2.Name : "",
                                                         WitnessSecond3 = employeeTable2 != null ? employeeTable2.Name : "",
                                                         WitnessSecond4 = employeeTable2 != null ? employeeTable2.Name : "",
                                                         WitnessSecond5 = employeeTable2 != null ? employeeTable2.Name : "",
                                                         WitnessSecond6 = employeeTable2 != null ? employeeTable2.Name : "",
                                                         WitnessSecond7 = employeeTable2 != null ? employeeTable2.Name : "",
                                                         WitnessSecond8 = employeeTable2 != null ? employeeTable2.Name : "",

                                                         MaxGuaranteeAmt1st_1 = Qline1 != null ? Qline1.MaximumGuaranteeAmount : 0,
                                                         MaxGuaranteeTxt1st_1 = null,
                                                         MaxGuaranteeAmt2nd_1 = Qline2 != null ? Qline2.MaximumGuaranteeAmount : 0,
                                                         MaxGuaranteeTxt2nd_1 = null,
                                                         MaxGuaranteeAmt3rd_1 = Qline3 != null ? Qline3.MaximumGuaranteeAmount : 0,
                                                         MaxGuaranteeTxt3rd_1 = null,
                                                         MaxGuaranteeAmt4th_1 = Qline4 != null ? Qline4.MaximumGuaranteeAmount : 0,
                                                         MaxGuaranteeTxt4th_1 = null,
                                                         MaxGuaranteeAmt5th_1 = Qline5 != null ? Qline5.MaximumGuaranteeAmount : 0,
                                                         MaxGuaranteeTxt5th_1 = null,
                                                         MaxGuaranteeAmt6th_1 = Qline6 != null ? Qline6.MaximumGuaranteeAmount : 0,
                                                         MaxGuaranteeTxt6th_1 = null,
                                                         MaxGuaranteeAmt7th_1 = Qline7 != null ? Qline7.MaximumGuaranteeAmount : 0,
                                                         MaxGuaranteeTxt7th_1 = null,
                                                         MaxGuaranteeAmt8th_1 = Qline8 != null ? Qline8.MaximumGuaranteeAmount : 0,
                                                         MaxGuaranteeTxt8th_1 = null,

                                                         ApprovedCreditLimitAmt1st_1 = Qline1 != null ? Qline1.ApprovedCreditLimit : 0,
                                                         ApprovedCreditLimitTxt1st_1 = null,
                                                         ApprovedCreditLimitAmt2nd_1 = Qline2 != null ? Qline2.ApprovedCreditLimit : 0,
                                                         ApprovedCreditLimitTxt2nd_1 = null,
                                                         ApprovedCreditLimitAmt3rd_1 = Qline3 != null ? Qline3.ApprovedCreditLimit : 0,
                                                         ApprovedCreditLimitTxt3rd_1 = null,
                                                         ApprovedCreditLimitAmt4th_1 = Qline4 != null ? Qline4.ApprovedCreditLimit : 0,
                                                         ApprovedCreditLimitTxt4th_1 = null,
                                                         ApprovedCreditLimitAmt5th_1 = Qline5 != null ? Qline5.ApprovedCreditLimit : 0,
                                                         ApprovedCreditLimitTxt5th_1 = null,
                                                         ApprovedCreditLimitAmt6th_1 = Qline6 != null ? Qline6.ApprovedCreditLimit : 0,
                                                         ApprovedCreditLimitTxt6th_1 = null,
                                                         ApprovedCreditLimitAmt7th_1 = Qline7 != null ? Qline7.ApprovedCreditLimit : 0,
                                                         ApprovedCreditLimitTxt7th_1 = null,
                                                         ApprovedCreditLimitAmt8th_1 = Qline8 != null ? Qline8.ApprovedCreditLimit : 0,
                                                         ApprovedCreditLimitTxt8th_1 = null,

                                                         GuarantorName2nd_3 = Qline2 != null ? Qline2.GuarantorName1st_3 : "",
                                                         GuarantorName3rd_3 = Qline3 != null ? Qline3.GuarantorName1st_3 : "",
                                                         GuarantorName4th_3 = Qline4 != null ? Qline4.GuarantorName1st_3 : "",
                                                         GuarantorName5th_3 = Qline5 != null ? Qline5.GuarantorName1st_3 : "",
                                                         GuarantorName6th_3 = Qline6 != null ? Qline6.GuarantorName1st_3 : "",
                                                         GuarantorName7th_3 = Qline7 != null ? Qline7.GuarantorName1st_3 : "",
                                                         GuarantorName8th_3 = Qline8 != null ? Qline8.GuarantorName1st_3 : "",

                                                         GuarantorCrossTaxID1st_Of1st_1 = Qline1 != null ? Qline1.AffiliateCustomerTaxId1st : "",
                                                         GuarantorCrossTaxID2nd_Of1st_1 = Qline1 != null ? Qline1.AffiliateCustomerTaxId2nd : "",
                                                         GuarantorCrossTaxID3rd_Of1st_1 = Qline1 != null ? Qline1.AffiliateCustomerTaxId3rd : "",
                                                         GuarantorCrossTaxID4th_Of1st_1 = Qline1 != null ? Qline1.AffiliateCustomerTaxId4th : "",
                                                         GuarantorCrossTaxID5th_Of1st_1 = Qline1 != null ? Qline1.AffiliateCustomerTaxId5th : "",
                                                         GuarantorCrossTaxID6th_Of1st_1 = Qline1 != null ? Qline1.AffiliateCustomerTaxId6th : "",
                                                         GuarantorCrossTaxID7th_Of1st_1 = Qline1 != null ? Qline1.AffiliateCustomerTaxId7th : "",
                                                         GuarantorCrossTaxID8th_Of1st_1 = Qline1 != null ? Qline1.AffiliateCustomerTaxId8th : "",

                                                         GuarantorCrossTaxID1st_Of2nd_1 = Qline2 != null ? Qline2.AffiliateCustomerTaxId1st : "",
                                                         GuarantorCrossTaxID2nd_Of2nd_1 = Qline2 != null ? Qline2.AffiliateCustomerTaxId2nd : "",
                                                         GuarantorCrossTaxID3rd_Of2nd_1 = Qline2 != null ? Qline2.AffiliateCustomerTaxId3rd : "",
                                                         GuarantorCrossTaxID4th_Of2nd_1 = Qline2 != null ? Qline2.AffiliateCustomerTaxId4th : "",
                                                         GuarantorCrossTaxID5th_Of2nd_1 = Qline2 != null ? Qline2.AffiliateCustomerTaxId5th : "",
                                                         GuarantorCrossTaxID6th_Of2nd_1 = Qline2 != null ? Qline2.AffiliateCustomerTaxId6th : "",
                                                         GuarantorCrossTaxID7th_Of2nd_1 = Qline2 != null ? Qline2.AffiliateCustomerTaxId7th : "",
                                                         GuarantorCrossTaxID8th_Of2nd_1 = Qline2 != null ? Qline2.AffiliateCustomerTaxId8th : "",

                                                         GuarantorCrossTaxID1st_Of3rd_1 = Qline3 != null ? Qline3.AffiliateCustomerTaxId1st : "",
                                                         GuarantorCrossTaxID2nd_Of3rd_1 = Qline3 != null ? Qline3.AffiliateCustomerTaxId2nd : "",
                                                         GuarantorCrossTaxID3rd_Of3rd_1 = Qline3 != null ? Qline3.AffiliateCustomerTaxId3rd : "",
                                                         GuarantorCrossTaxID4th_Of3rd_1 = Qline3 != null ? Qline3.AffiliateCustomerTaxId4th : "",
                                                         GuarantorCrossTaxID5th_Of3rd_1 = Qline3 != null ? Qline3.AffiliateCustomerTaxId5th : "",
                                                         GuarantorCrossTaxID6th_Of3rd_1 = Qline3 != null ? Qline3.AffiliateCustomerTaxId6th : "",
                                                         GuarantorCrossTaxID7th_Of3rd_1 = Qline3 != null ? Qline3.AffiliateCustomerTaxId7th : "",
                                                         GuarantorCrossTaxID8th_Of3rd_1 = Qline3 != null ? Qline3.AffiliateCustomerTaxId8th : "",

                                                         GuarantorCrossTaxID1st_Of4th_1 = Qline4 != null ? Qline4.AffiliateCustomerTaxId1st : "",
                                                         GuarantorCrossTaxID2nd_Of4th_1 = Qline4 != null ? Qline4.AffiliateCustomerTaxId2nd : "",
                                                         GuarantorCrossTaxID3rd_Of4th_1 = Qline4 != null ? Qline4.AffiliateCustomerTaxId3rd : "",
                                                         GuarantorCrossTaxID4th_Of4th_1 = Qline4 != null ? Qline4.AffiliateCustomerTaxId4th : "",
                                                         GuarantorCrossTaxID5th_Of4th_1 = Qline4 != null ? Qline4.AffiliateCustomerTaxId5th : "",
                                                         GuarantorCrossTaxID6th_Of4th_1 = Qline4 != null ? Qline4.AffiliateCustomerTaxId6th : "",
                                                         GuarantorCrossTaxID7th_Of4th_1 = Qline4 != null ? Qline4.AffiliateCustomerTaxId7th : "",
                                                         GuarantorCrossTaxID8th_Of4th_1 = Qline4 != null ? Qline4.AffiliateCustomerTaxId8th : "",

                                                         GuarantorCrossTaxID1st_Of5th_1 = Qline5 != null ? Qline5.AffiliateCustomerTaxId1st : "",
                                                         GuarantorCrossTaxID2nd_Of5th_1 = Qline5 != null ? Qline5.AffiliateCustomerTaxId2nd : "",
                                                         GuarantorCrossTaxID3rd_Of5th_1 = Qline5 != null ? Qline5.AffiliateCustomerTaxId3rd : "",
                                                         GuarantorCrossTaxID4th_Of5th_1 = Qline5 != null ? Qline5.AffiliateCustomerTaxId4th : "",
                                                         GuarantorCrossTaxID5th_Of5th_1 = Qline5 != null ? Qline5.AffiliateCustomerTaxId5th : "",
                                                         GuarantorCrossTaxID6th_Of5th_1 = Qline5 != null ? Qline5.AffiliateCustomerTaxId6th : "",
                                                         GuarantorCrossTaxID7th_Of5th_1 = Qline5 != null ? Qline5.AffiliateCustomerTaxId7th : "",
                                                         GuarantorCrossTaxID8th_Of5th_1 = Qline5 != null ? Qline5.AffiliateCustomerTaxId8th : "",

                                                         GuarantorCrossTaxID1st_Of6th_1 = Qline6 != null ? Qline6.AffiliateCustomerTaxId1st : "",
                                                         GuarantorCrossTaxID2nd_Of6th_1 = Qline6 != null ? Qline6.AffiliateCustomerTaxId2nd : "",
                                                         GuarantorCrossTaxID3rd_Of6th_1 = Qline6 != null ? Qline6.AffiliateCustomerTaxId3rd : "",
                                                         GuarantorCrossTaxID4th_Of6th_1 = Qline6 != null ? Qline6.AffiliateCustomerTaxId4th : "",
                                                         GuarantorCrossTaxID5th_Of6th_1 = Qline6 != null ? Qline6.AffiliateCustomerTaxId5th : "",
                                                         GuarantorCrossTaxID6th_Of6th_1 = Qline6 != null ? Qline6.AffiliateCustomerTaxId6th : "",
                                                         GuarantorCrossTaxID7th_Of6th_1 = Qline6 != null ? Qline6.AffiliateCustomerTaxId7th : "",
                                                         GuarantorCrossTaxID8th_Of6th_1 = Qline6 != null ? Qline6.AffiliateCustomerTaxId8th : "",

                                                         GuarantorCrossTaxID1st_Of7th_1 = Qline7 != null ? Qline7.AffiliateCustomerTaxId1st : "",
                                                         GuarantorCrossTaxID2nd_Of7th_1 = Qline7 != null ? Qline7.AffiliateCustomerTaxId2nd : "",
                                                         GuarantorCrossTaxID3rd_Of7th_1 = Qline7 != null ? Qline7.AffiliateCustomerTaxId3rd : "",
                                                         GuarantorCrossTaxID4th_Of7th_1 = Qline7 != null ? Qline7.AffiliateCustomerTaxId4th : "",
                                                         GuarantorCrossTaxID5th_Of7th_1 = Qline7 != null ? Qline7.AffiliateCustomerTaxId5th : "",
                                                         GuarantorCrossTaxID6th_Of7th_1 = Qline7 != null ? Qline7.AffiliateCustomerTaxId6th : "",
                                                         GuarantorCrossTaxID7th_Of7th_1 = Qline7 != null ? Qline7.AffiliateCustomerTaxId7th : "",
                                                         GuarantorCrossTaxID8th_Of7th_1 = Qline7 != null ? Qline7.AffiliateCustomerTaxId8th : "",

                                                         GuarantorCrossTaxID1st_Of8th_1 = Qline8 != null ? Qline8.AffiliateCustomerTaxId1st : "",
                                                         GuarantorCrossTaxID2nd_Of8th_1 = Qline8 != null ? Qline8.AffiliateCustomerTaxId2nd : "",
                                                         GuarantorCrossTaxID3rd_Of8th_1 = Qline8 != null ? Qline8.AffiliateCustomerTaxId3rd : "",
                                                         GuarantorCrossTaxID4th_Of8th_1 = Qline8 != null ? Qline8.AffiliateCustomerTaxId4th : "",
                                                         GuarantorCrossTaxID5th_Of8th_1 = Qline8 != null ? Qline8.AffiliateCustomerTaxId5th : "",
                                                         GuarantorCrossTaxID6th_Of8th_1 = Qline8 != null ? Qline8.AffiliateCustomerTaxId6th : "",
                                                         GuarantorCrossTaxID7th_Of8th_1 = Qline8 != null ? Qline8.AffiliateCustomerTaxId7th : "",
                                                         GuarantorCrossTaxID8th_Of8th_1 = Qline8 != null ? Qline8.AffiliateCustomerTaxId8th : "",
                                                         GuarantotOperatedBy1st_1 = Qline1 != null ? Qline1.GuarantotOperatedBy : "",
                                                         GuarantotOperatedBy1st_2 = Qline1 != null ? Qline1.GuarantotOperatedBy : "",
                                                         GuarantotOperatedBy2nd_1 = Qline2 != null ? Qline2.GuarantotOperatedBy : "",
                                                         GuarantotOperatedBy2nd_2 = Qline2 != null ? Qline2.GuarantotOperatedBy : "",
                                                         GuarantotOperatedBy3rd_1 = Qline3 != null ? Qline3.GuarantotOperatedBy : "",
                                                         GuarantotOperatedBy3rd_2 = Qline3 != null ? Qline3.GuarantotOperatedBy : "",
                                                         GuarantotOperatedBy4th_1 = Qline4 != null ? Qline4.GuarantotOperatedBy : "",
                                                         GuarantotOperatedBy4th_2 = Qline4 != null ? Qline4.GuarantotOperatedBy : "",
                                                         GuarantotOperatedBy5th_1 = Qline5 != null ? Qline5.GuarantotOperatedBy : "",
                                                         GuarantotOperatedBy5th_2 = Qline5 != null ? Qline5.GuarantotOperatedBy : "",
                                                         GuarantotOperatedBy6th_1 = Qline6 != null ? Qline6.GuarantotOperatedBy : "",
                                                         GuarantotOperatedBy6th_2 = Qline6 != null ? Qline6.GuarantotOperatedBy : "",
                                                         GuarantotOperatedBy7th_1 = Qline7 != null ? Qline7.GuarantotOperatedBy : "",
                                                         GuarantotOperatedBy7th_2 = Qline7 != null ? Qline7.GuarantotOperatedBy : "",
                                                         GuarantotOperatedBy8th_1 = Qline8 != null ? Qline8.GuarantotOperatedBy : "",
                                                         GuarantotOperatedBy8th_2 = Qline8 != null ? Qline8.GuarantotOperatedBy : "",

                                                         WritingAddress1th = Qline1 != null ? Qline1.GuarantorName : "",
                                                         WritingAddress2nd = Qline2 != null ? Qline2.GuarantorName : "",
                                                         WritingAddress3rd = Qline3 != null ? Qline3.GuarantorName : "",
                                                         WritingAddress4th = Qline4 != null ? Qline4.GuarantorName : "",
                                                         WritingAddress5th = Qline5 != null ? Qline5.GuarantorName : "",
                                                         WritingAddress6th = Qline6 != null ? Qline6.GuarantorName : "",
                                                         WritingAddress7th = Qline7 != null ? Qline7.GuarantorName : "",
                                                         WritingAddress8th = Qline8 != null ? Qline8.GuarantorName : "",

                                                         QSumLineAffliliate1st = QLine1AffiliateSum,
                                                         QSumLineAffliliate2nd = QLine2AffiliateSum,
                                                         QSumLineAffliliate3rd = QLine3AffiliateSum,
                                                         QSumLineAffliliate4th = QLine4AffiliateSum,
                                                         QSumLineAffliliate5th = QLine5AffiliateSum,
                                                         QSumLineAffliliate6th = QLine6AffiliateSum,
                                                         QSumLineAffliliate7th = QLine7AffiliateSum,
                                                         QSumLineAffliliate8th = QLine8AffiliateSum,

                                                     }).AsNoTracking().FirstOrDefault();


                return guarantorAgreementTableResult;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Bookmark document query 04GuarantorAgreement
        public QGuarantorAgreementTable GetqGuarantorAgreementTable(Guid refGUID)
        {
            try
            {
                QGuarantorAgreementInfo qGuarantorAgreementInfo1 = (from agreementTableInfo in Entity
                                                                   join employeeTable in db.Set<EmployeeTable>()
                                                                   on agreementTableInfo.WitnessCompany1GUID equals employeeTable.EmployeeTableGUID into ljagreementTableInfo
                                                                   from employeeTable in ljagreementTableInfo.DefaultIfEmpty()
                                                                   where agreementTableInfo.GuarantorAgreementTableGUID == refGUID
                                                                   select new QGuarantorAgreementInfo
                                                                   {
                                                                       QGuarantorAgreementInfo_CompanyWitness1 = employeeTable.Name
                                                                   }
                                                                   ).FirstOrDefault();
                QGuarantorAgreementInfo qGuarantorAgreementInfo2 = (from agreementTableInfo in Entity
                                                                    join employeeTable in db.Set<EmployeeTable>()
                                                                    on agreementTableInfo.WitnessCompany2GUID equals employeeTable.EmployeeTableGUID into ljagreementTableInfo
                                                                    from employeeTable in ljagreementTableInfo.DefaultIfEmpty()
                                                                    where agreementTableInfo.GuarantorAgreementTableGUID == refGUID
                                                                    select new QGuarantorAgreementInfo
                                                                    {
                                                                        QGuarantorAgreementInfo_CompanyWitness2 = employeeTable.Name
                                                                    }
                                                                   ).FirstOrDefault();

                ISharedService sharedService = new SharedService(db);
                QGuarantorAgreementTable result = (from guarantorAgreementTable in Entity
                                                       //QGuarantorAgreementInfo 
                                                   join agreementTableInfo in db.Set<AgreementTableInfo>()
                                                   on guarantorAgreementTable.MainAgreementTableGUID equals agreementTableInfo.RefGUID into ljagreementTableInfo
                                                   from agreementTableInfo in ljagreementTableInfo.DefaultIfEmpty()

                                                   join CompanyWitness1 in db.Set<EmployeeTable>()
                                                   on agreementTableInfo.WitnessCompany1GUID equals CompanyWitness1.EmployeeTableGUID into ljCompanyWitness1
                                                   from CompanyWitness1 in ljCompanyWitness1.DefaultIfEmpty()

                                                   join CompanyWitness2 in db.Set<EmployeeTable>()
                                                   on agreementTableInfo.WitnessCompany2GUID equals CompanyWitness2.EmployeeTableGUID into ljCompanyWitness2
                                                   from CompanyWitness2 in ljCompanyWitness2.DefaultIfEmpty()
                                                       //QCompany
                                                   join company in db.Set<Company>()
                                                   on guarantorAgreementTable.CompanyGUID equals company.CompanyGUID into ljcompany
                                                   from company in ljcompany.DefaultIfEmpty()

                                                   join addressTrans in db.Set<AddressTrans>().Where(s => s.RefType == (int)RefType.Branch && s.Primary == true)
                                                   on company.DefaultBranchGUID equals addressTrans.RefGUID into ljaddressTrans
                                                   from addressTrans in ljaddressTrans.DefaultIfEmpty()
                                                       //QMainAgreementTable
                                                   join mainagreementTable in db.Set<MainAgreementTable>()
                                                   on guarantorAgreementTable.MainAgreementTableGUID equals mainagreementTable.MainAgreementTableGUID into ljmainagreementTable
                                                   from mainagreementTable in ljmainagreementTable.DefaultIfEmpty()

                                                   join mainAgreementInfo in db.Set<AgreementTableInfo>()
                                                   on mainagreementTable.MainAgreementTableGUID equals mainAgreementInfo.RefGUID into ljmainAgreementInfo
                                                   from mainAgreementInfo in ljmainAgreementInfo.DefaultIfEmpty()

                                                   join consortiumTable in db.Set<ConsortiumTable>()
                                                   on mainagreementTable.ConsortiumTableGUID equals consortiumTable.ConsortiumTableGUID into ljconsortiumTable
                                                   from consortiumTable in ljconsortiumTable.DefaultIfEmpty()

                                                   where guarantorAgreementTable.GuarantorAgreementTableGUID == refGUID
                                                   select new QGuarantorAgreementTable
                                                   {
                                                       QGuarantorAgreementTable_GuarantorAgreementId = guarantorAgreementTable.GuarantorAgreementId,
                                                       QGuarantorAgreementTable_AgreementDate = sharedService.DateToWord(guarantorAgreementTable.AgreementDate, TextConstants.TH, TextConstants.DMY),
                                                       QGuarantorAgreementTable_AgreementYear = guarantorAgreementTable.AgreementYear.ToString(),
                                                       QGuarantorAgreementTable_MainAgreementTableGUID = guarantorAgreementTable.MainAgreementTableGUID.GuidNullToString(),
                                                       QGuarantorAgreementTable_CustomerAddress = agreementTableInfo.CustomerAddress1 + " " + agreementTableInfo.CustomerAddress2,
                                                       QGuarantorAgreementTable_CompanyWitness1 = qGuarantorAgreementInfo1.QGuarantorAgreementInfo_CompanyWitness1,
                                                       QGuarantorAgreementTable_CompanyWitness2 = qGuarantorAgreementInfo2.QGuarantorAgreementInfo_CompanyWitness2,
                                                       QGuarantorAgreementTable_CompanyGUID = company.Name,
                                                       QGuarantorAgreementTable_ApprovedCreditLimit = mainagreementTable.ApprovedCreditLimit,
                                                       QGuarantorAgreementTable_CompanyName = company.Name,
                                                       QGuarantorAgreementTable_CompanyAddress = addressTrans != null ? (addressTrans.Address1 + " " + addressTrans.Address2) : "",
                                                       QGuarantorAgreementTable_MainAgreementId = mainagreementTable.MainAgreementId,
                                                       QGuarantorAgreementTable_MainAgreementDate = mainagreementTable.AgreementDate,
                                                       QGuarantorAgreementTable_MainCustomerName = mainagreementTable.CustomerName,
                                                       QGuarantorAgreementTable_MainAgreementAddress = addressTrans != null ? (addressTrans.Address1 + " " + addressTrans.Address2) : "",
                                                       QGuarantorAgreementTable_Description = consortiumTable.Description,
                                                   }
                                                   ).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public QGuarantorAgreementLine1 GetqGuarantorAgreementLine1(Guid refGUID)
        {
            try
            {
                QGuarantorAgreementLine1 result = (from guarantorAgreementTable in Entity

                                                   join guarantorAgreementLineNum in db.Set<GuarantorAgreementLine>().Where(w => w.Ordering == 1)
                                                   on guarantorAgreementTable.GuarantorAgreementTableGUID equals guarantorAgreementLineNum.GuarantorAgreementTableGUID into ljAuarantorLine1
                                                   from guarantorAgreementLineNum in ljAuarantorLine1.DefaultIfEmpty()

                                                   join guarantorTrans in db.Set<GuarantorTrans>()
                                                   on guarantorAgreementLineNum.GuarantorTransGUID equals guarantorTrans.GuarantorTransGUID into ljGuarantorTrans
                                                   from guarantorTrans in ljGuarantorTrans.DefaultIfEmpty()

                                                   join race in db.Set<Race>()
                                                   on guarantorAgreementLineNum.RaceGUID equals race.RaceGUID into ljRace
                                                   from race in ljRace.DefaultIfEmpty()
                                                   join nationality in db.Set<Nationality>()
                                                   on guarantorAgreementLineNum.NationalityGUID equals nationality.NationalityGUID into ljNationality
                                                   from nationality in ljNationality.DefaultIfEmpty()

                                                   where guarantorAgreementLineNum.GuarantorAgreementTableGUID == refGUID
                                                   select new QGuarantorAgreementLine1
                                                   {
                                                       QGuarantorAgreementLine1_GuarantorAgreement_Name = guarantorAgreementLineNum.Name,
                                                       QGuarantorAgreementLine1_GuarantorAgreement_TaxId = guarantorAgreementLineNum.TaxId,
                                                       QGuarantorAgreementLine1_GuarantorAgreement_PassportId = guarantorAgreementLineNum.PassportId,
                                                       QGuarantorAgreementLine1_GuarantorAgreement_WorkPermitId = guarantorAgreementLineNum.WorkPermitId,
                                                       QGuarantorAgreementLine1_GuarantorAgreement_DateOfIssue = guarantorAgreementLineNum.DateOfIssue.DateNullToString(),
                                                       QGuarantorAgreementLine1_GuarantorAgreement_DOB = guarantorAgreementLineNum.DateOfBirth.DateNullToString(),
                                                       QGuarantorAgreementLine1_GuarantorAgreement_Age = guarantorAgreementLineNum.Age.ToString(),
                                                       QGuarantorAgreementLine1_GuarantorAgreement_Nationality = nationality != null ? nationality.Description : "",
                                                       QGuarantorAgreementLine1_GuarantorAgreement_Race = race != null ? race.Description : "",
                                                       QGuarantorAgreementLine1_GuarantorAgreement_Address = guarantorAgreementLineNum.PrimaryAddress1 + " " + guarantorAgreementLineNum.PrimaryAddress2

                                                   }).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public QGuarantorAgreementLine2 GetqGuarantorAgreementLine2(Guid refGUID)
        {
            try
            {
                QGuarantorAgreementLine2 result = (from guarantorAgreementTable in Entity

                                                   join guarantorAgreementLineNum in db.Set<GuarantorAgreementLine>().Where(w => w.Ordering == 2)
                                                   on guarantorAgreementTable.GuarantorAgreementTableGUID equals guarantorAgreementLineNum.GuarantorAgreementTableGUID into ljAuarantorLine2
                                                   from guarantorAgreementLineNum in ljAuarantorLine2.DefaultIfEmpty()

                                                   join guarantorTrans in db.Set<GuarantorTrans>()
                                                   on guarantorAgreementLineNum.GuarantorTransGUID equals guarantorTrans.GuarantorTransGUID into ljGuarantorTrans
                                                   from guarantorTrans in ljGuarantorTrans.DefaultIfEmpty()

                                                   join race in db.Set<Race>()
                                                   on guarantorAgreementLineNum.RaceGUID equals race.RaceGUID into ljRace
                                                   from race in ljRace.DefaultIfEmpty()
                                                   join nationality in db.Set<Nationality>()
                                                   on guarantorAgreementLineNum.NationalityGUID equals nationality.NationalityGUID into ljNationality
                                                   from nationality in ljNationality.DefaultIfEmpty()

                                                   where guarantorAgreementLineNum.GuarantorAgreementTableGUID == refGUID
                                                   select new QGuarantorAgreementLine2
                                                   {
                                                       QGuarantorAgreementLine2_GuarantorAgreement_Name = guarantorAgreementLineNum.Name,
                                                       QGuarantorAgreementLine2_GuarantorAgreement_TaxId = guarantorAgreementLineNum.TaxId,
                                                       QGuarantorAgreementLine2_GuarantorAgreement_PassportId = guarantorAgreementLineNum.PassportId,
                                                       QGuarantorAgreementLine2_GuarantorAgreement_WorkPermitId = guarantorAgreementLineNum.WorkPermitId,
                                                       QGuarantorAgreementLine2_GuarantorAgreement_DateOfIssue = guarantorAgreementLineNum.DateOfIssue.DateNullToString(),
                                                       QGuarantorAgreementLine2_GuarantorAgreement_DOB = guarantorAgreementLineNum.DateOfBirth.DateNullToString(),
                                                       QGuarantorAgreementLine2_GuarantorAgreement_Age = guarantorAgreementLineNum.Age.ToString(),
                                                       QGuarantorAgreementLine2_GuarantorAgreement_Nationality = nationality != null ? nationality.Description : "",
                                                       QGuarantorAgreementLine2_GuarantorAgreement_Race = race != null ? race.Description : "",
                                                       QGuarantorAgreementLine2_GuarantorAgreement_Address = guarantorAgreementLineNum.PrimaryAddress1 + " " + guarantorAgreementLineNum.PrimaryAddress2
                                                   }).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public QGuarantorAgreementLine3 GetqGuarantorAgreementLine3(Guid refGUID)
        {
            try
            {
                QGuarantorAgreementLine3 result = (from guarantorAgreementTable in Entity

                                                   join guarantorAgreementLineNum in db.Set<GuarantorAgreementLine>().Where(w => w.Ordering == 3)
                                                   on guarantorAgreementTable.GuarantorAgreementTableGUID equals guarantorAgreementLineNum.GuarantorAgreementTableGUID into ljAuarantorLine3
                                                   from guarantorAgreementLineNum in ljAuarantorLine3.DefaultIfEmpty()

                                                   join guarantorTrans in db.Set<GuarantorTrans>()
                                                   on guarantorAgreementLineNum.GuarantorTransGUID equals guarantorTrans.GuarantorTransGUID into ljGuarantorTrans
                                                   from guarantorTrans in ljGuarantorTrans.DefaultIfEmpty()

                                                   join race in db.Set<Race>()
                                                   on guarantorAgreementLineNum.RaceGUID equals race.RaceGUID into ljRace
                                                   from race in ljRace.DefaultIfEmpty()
                                                   join nationality in db.Set<Nationality>()
                                                   on guarantorAgreementLineNum.NationalityGUID equals nationality.NationalityGUID into ljNationality
                                                   from nationality in ljNationality.DefaultIfEmpty()

                                                   where guarantorAgreementLineNum.GuarantorAgreementTableGUID == refGUID
                                                   select new QGuarantorAgreementLine3
                                                   {
                                                       QGuarantorAgreementLine3_GuarantorAgreement_Name = guarantorAgreementLineNum.Name,
                                                       QGuarantorAgreementLine3_GuarantorAgreement_TaxId = guarantorAgreementLineNum.TaxId,
                                                       QGuarantorAgreementLine3_GuarantorAgreement_PassportId = guarantorAgreementLineNum.PassportId,
                                                       QGuarantorAgreementLine3_GuarantorAgreement_WorkPermitId = guarantorAgreementLineNum.WorkPermitId,
                                                       QGuarantorAgreementLine3_GuarantorAgreement_DateOfIssue = guarantorAgreementLineNum.DateOfIssue.DateNullToString(),
                                                       QGuarantorAgreementLine3_GuarantorAgreement_DOB = guarantorAgreementLineNum.DateOfBirth.DateNullToString(),
                                                       QGuarantorAgreementLine3_GuarantorAgreement_Age = guarantorAgreementLineNum.Age.ToString(),
                                                       QGuarantorAgreementLine3_GuarantorAgreement_Nationality = nationality != null ? nationality.Description : "",
                                                       QGuarantorAgreementLine3_GuarantorAgreement_Race = race != null ? race.Description : "",
                                                       QGuarantorAgreementLine3_GuarantorAgreement_Address = guarantorAgreementLineNum.PrimaryAddress1 + " " + guarantorAgreementLineNum.PrimaryAddress2
                                                   }).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public QGuarantorAgreementLine4 GetqGuarantorAgreementLine4(Guid refGUID)
        {
            try
            {
                QGuarantorAgreementLine4 result = (from guarantorAgreementTable in Entity

                                                   join guarantorAgreementLineNum in db.Set<GuarantorAgreementLine>().Where(w => w.Ordering == 4)
                                                   on guarantorAgreementTable.GuarantorAgreementTableGUID equals guarantorAgreementLineNum.GuarantorAgreementTableGUID into ljAuarantorLine4
                                                   from guarantorAgreementLineNum in ljAuarantorLine4.DefaultIfEmpty()

                                                   join guarantorTrans in db.Set<GuarantorTrans>()
                                                   on guarantorAgreementLineNum.GuarantorTransGUID equals guarantorTrans.GuarantorTransGUID into ljGuarantorTrans
                                                   from guarantorTrans in ljGuarantorTrans.DefaultIfEmpty()

                                                   join race in db.Set<Race>()
                                                   on guarantorAgreementLineNum.RaceGUID equals race.RaceGUID into ljRace
                                                   from race in ljRace.DefaultIfEmpty()
                                                   join nationality in db.Set<Nationality>()
                                                   on guarantorAgreementLineNum.NationalityGUID equals nationality.NationalityGUID into ljNationality
                                                   from nationality in ljNationality.DefaultIfEmpty()

                                                   where guarantorAgreementLineNum.GuarantorAgreementTableGUID == refGUID
                                                   select new QGuarantorAgreementLine4
                                                   {
                                                       QGuarantorAgreementLine4_GuarantorAgreement_Name = guarantorAgreementLineNum.Name,
                                                       QGuarantorAgreementLine4_GuarantorAgreement_TaxId = guarantorAgreementLineNum.TaxId,
                                                       QGuarantorAgreementLine4_GuarantorAgreement_PassportId = guarantorAgreementLineNum.PassportId,
                                                       QGuarantorAgreementLine4_GuarantorAgreement_WorkPermitId = guarantorAgreementLineNum.WorkPermitId,
                                                       QGuarantorAgreementLine4_GuarantorAgreement_DateOfIssue = guarantorAgreementLineNum.DateOfIssue.DateNullToString(),
                                                       QGuarantorAgreementLine4_GuarantorAgreement_DOB = guarantorAgreementLineNum.DateOfBirth.DateNullToString(),
                                                       QGuarantorAgreementLine4_GuarantorAgreement_Age = guarantorAgreementLineNum.Age.ToString(),
                                                       QGuarantorAgreementLine4_GuarantorAgreement_Nationality = nationality != null ? nationality.Description : "",
                                                       QGuarantorAgreementLine4_GuarantorAgreement_Race = race != null ? race.Description : "",
                                                       QGuarantorAgreementLine4_GuarantorAgreement_Address = guarantorAgreementLineNum.PrimaryAddress1 + " " + guarantorAgreementLineNum.PrimaryAddress2
                                                   }).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public QGuarantorAgreementLine5 GetqGuarantorAgreementLine5(Guid refGUID)
        {
            try
            {
                QGuarantorAgreementLine5 result = (from guarantorAgreementTable in Entity

                                                   join guarantorAgreementLineNum in db.Set<GuarantorAgreementLine>().Where(w => w.Ordering == 5)
                                                   on guarantorAgreementTable.GuarantorAgreementTableGUID equals guarantorAgreementLineNum.GuarantorAgreementTableGUID into ljAuarantorLine5
                                                   from guarantorAgreementLineNum in ljAuarantorLine5.DefaultIfEmpty()

                                                   join guarantorTrans in db.Set<GuarantorTrans>()
                                                   on guarantorAgreementLineNum.GuarantorTransGUID equals guarantorTrans.GuarantorTransGUID into ljGuarantorTrans
                                                   from guarantorTrans in ljGuarantorTrans.DefaultIfEmpty()

                                                   join race in db.Set<Race>()
                                                   on guarantorAgreementLineNum.RaceGUID equals race.RaceGUID into ljRace
                                                   from race in ljRace.DefaultIfEmpty()
                                                   join nationality in db.Set<Nationality>()
                                                   on guarantorAgreementLineNum.NationalityGUID equals nationality.NationalityGUID into ljNationality
                                                   from nationality in ljNationality.DefaultIfEmpty()

                                                   where guarantorAgreementLineNum.GuarantorAgreementTableGUID == refGUID
                                                   select new QGuarantorAgreementLine5
                                                   {
                                                       QGuarantorAgreementLine5_GuarantorAgreement_Name = guarantorAgreementLineNum.Name,
                                                       QGuarantorAgreementLine5_GuarantorAgreement_TaxId = guarantorAgreementLineNum.TaxId,
                                                       QGuarantorAgreementLine5_GuarantorAgreement_PassportId = guarantorAgreementLineNum.PassportId,
                                                       QGuarantorAgreementLine5_GuarantorAgreement_WorkPermitId = guarantorAgreementLineNum.WorkPermitId,
                                                       QGuarantorAgreementLine5_GuarantorAgreement_DateOfIssue = guarantorAgreementLineNum.DateOfIssue.DateNullToString(),
                                                       QGuarantorAgreementLine5_GuarantorAgreement_DOB = guarantorAgreementLineNum.DateOfBirth.DateNullToString(),
                                                       QGuarantorAgreementLine5_GuarantorAgreement_Age = guarantorAgreementLineNum.Age.ToString(),
                                                       QGuarantorAgreementLine5_GuarantorAgreement_Nationality = nationality != null ? nationality.Description : "",
                                                       QGuarantorAgreementLine5_GuarantorAgreement_Race = race != null ? race.Description : "",
                                                       QGuarantorAgreementLine5_GuarantorAgreement_Address = guarantorAgreementLineNum.PrimaryAddress1 + " " + guarantorAgreementLineNum.PrimaryAddress2
                                                   }).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public QGuarantorAgreementLine6 GetqGuarantorAgreementLine6(Guid refGUID)
        {
            try
            {
                QGuarantorAgreementLine6 result = (from guarantorAgreementTable in Entity

                                                   join guarantorAgreementLineNum in db.Set<GuarantorAgreementLine>().Where(w => w.Ordering == 6)
                                                   on guarantorAgreementTable.GuarantorAgreementTableGUID equals guarantorAgreementLineNum.GuarantorAgreementTableGUID into ljAuarantorLine6
                                                   from guarantorAgreementLineNum in ljAuarantorLine6.DefaultIfEmpty()

                                                   join guarantorTrans in db.Set<GuarantorTrans>()
                                                   on guarantorAgreementLineNum.GuarantorTransGUID equals guarantorTrans.GuarantorTransGUID into ljGuarantorTrans
                                                   from guarantorTrans in ljGuarantorTrans.DefaultIfEmpty()

                                                   join race in db.Set<Race>()
                                                   on guarantorAgreementLineNum.RaceGUID equals race.RaceGUID into ljRace
                                                   from race in ljRace.DefaultIfEmpty()
                                                   join nationality in db.Set<Nationality>()
                                                   on guarantorAgreementLineNum.NationalityGUID equals nationality.NationalityGUID into ljNationality
                                                   from nationality in ljNationality.DefaultIfEmpty()

                                                   where guarantorAgreementLineNum.GuarantorAgreementTableGUID == refGUID
                                                   select new QGuarantorAgreementLine6
                                                   {
                                                       QGuarantorAgreementLine6_GuarantorAgreement_Name = guarantorAgreementLineNum.Name,
                                                       QGuarantorAgreementLine6_GuarantorAgreement_TaxId = guarantorAgreementLineNum.TaxId,
                                                       QGuarantorAgreementLine6_GuarantorAgreement_PassportId = guarantorAgreementLineNum.PassportId,
                                                       QGuarantorAgreementLine6_GuarantorAgreement_WorkPermitId = guarantorAgreementLineNum.WorkPermitId,
                                                       QGuarantorAgreementLine6_GuarantorAgreement_DateOfIssue = guarantorAgreementLineNum.DateOfIssue.DateNullToString(),
                                                       QGuarantorAgreementLine6_GuarantorAgreement_DOB = guarantorAgreementLineNum.DateOfBirth.DateNullToString(),
                                                       QGuarantorAgreementLine6_GuarantorAgreement_Age = guarantorAgreementLineNum.Age.ToString(),
                                                       QGuarantorAgreementLine6_GuarantorAgreement_Nationality = nationality != null ? nationality.Description : "",
                                                       QGuarantorAgreementLine6_GuarantorAgreement_Race = race != null ? race.Description : "",
                                                       QGuarantorAgreementLine6_GuarantorAgreement_Address = guarantorAgreementLineNum.PrimaryAddress1 + " " + guarantorAgreementLineNum.PrimaryAddress2
                                                   }).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public QGuarantorAgreementLine7 GetqGuarantorAgreementLine7(Guid refGUID)
        {
            try
            {
                QGuarantorAgreementLine7 result = (from guarantorAgreementTable in Entity

                                                   join guarantorAgreementLineNum in db.Set<GuarantorAgreementLine>().Where(w => w.Ordering == 7)
                                                   on guarantorAgreementTable.GuarantorAgreementTableGUID equals guarantorAgreementLineNum.GuarantorAgreementTableGUID into ljAuarantorLine7
                                                   from guarantorAgreementLineNum in ljAuarantorLine7.DefaultIfEmpty()

                                                   join guarantorTrans in db.Set<GuarantorTrans>()
                                                   on guarantorAgreementLineNum.GuarantorTransGUID equals guarantorTrans.GuarantorTransGUID into ljGuarantorTrans
                                                   from guarantorTrans in ljGuarantorTrans.DefaultIfEmpty()

                                                   join race in db.Set<Race>()
                                                   on guarantorAgreementLineNum.RaceGUID equals race.RaceGUID into ljRace
                                                   from race in ljRace.DefaultIfEmpty()
                                                   join nationality in db.Set<Nationality>()
                                                   on guarantorAgreementLineNum.NationalityGUID equals nationality.NationalityGUID into ljNationality
                                                   from nationality in ljNationality.DefaultIfEmpty()

                                                   where guarantorAgreementLineNum.GuarantorAgreementTableGUID == refGUID
                                                   select new QGuarantorAgreementLine7
                                                   {
                                                       QGuarantorAgreementLine7_GuarantorAgreement_Name = guarantorAgreementLineNum.Name,
                                                       QGuarantorAgreementLine7_GuarantorAgreement_TaxId = guarantorAgreementLineNum.TaxId,
                                                       QGuarantorAgreementLine7_GuarantorAgreement_PassportId = guarantorAgreementLineNum.PassportId,
                                                       QGuarantorAgreementLine7_GuarantorAgreement_WorkPermitId = guarantorAgreementLineNum.WorkPermitId,
                                                       QGuarantorAgreementLine7_GuarantorAgreement_DateOfIssue = guarantorAgreementLineNum.DateOfIssue.DateNullToString(),
                                                       QGuarantorAgreementLine7_GuarantorAgreement_DOB = guarantorAgreementLineNum.DateOfBirth.DateNullToString(),
                                                       QGuarantorAgreementLine7_GuarantorAgreement_Age = guarantorAgreementLineNum.Age.ToString(),
                                                       QGuarantorAgreementLine7_GuarantorAgreement_Nationality = nationality != null ? nationality.Description : "",
                                                       QGuarantorAgreementLine7_GuarantorAgreement_Race = race != null ? race.Description : "",
                                                       QGuarantorAgreementLine7_GuarantorAgreement_Address = guarantorAgreementLineNum.PrimaryAddress1 + " " + guarantorAgreementLineNum.PrimaryAddress2
                                                   }).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public QGuarantorAgreementLine8 GetqGuarantorAgreementLine8(Guid refGUID)
        {
            try
            {
                QGuarantorAgreementLine8 result = (from guarantorAgreementTable in Entity

                                                   join guarantorAgreementLineNum in db.Set<GuarantorAgreementLine>().Where(w => w.Ordering == 8)
                                                   on guarantorAgreementTable.GuarantorAgreementTableGUID equals guarantorAgreementLineNum.GuarantorAgreementTableGUID into ljAuarantorLine8
                                                   from guarantorAgreementLineNum in ljAuarantorLine8.DefaultIfEmpty()

                                                   join guarantorTrans in db.Set<GuarantorTrans>()
                                                   on guarantorAgreementLineNum.GuarantorTransGUID equals guarantorTrans.GuarantorTransGUID into ljGuarantorTrans
                                                   from guarantorTrans in ljGuarantorTrans.DefaultIfEmpty()

                                                   join race in db.Set<Race>()
                                                   on guarantorAgreementLineNum.RaceGUID equals race.RaceGUID into ljRace
                                                   from race in ljRace.DefaultIfEmpty()
                                                   join nationality in db.Set<Nationality>()
                                                   on guarantorAgreementLineNum.NationalityGUID equals nationality.NationalityGUID into ljNationality
                                                   from nationality in ljNationality.DefaultIfEmpty()

                                                   where guarantorAgreementLineNum.GuarantorAgreementTableGUID == refGUID
                                                   select new QGuarantorAgreementLine8
                                                   {
                                                       QGuarantorAgreementLine8_GuarantorAgreement_Name = guarantorAgreementLineNum.Name,
                                                       QGuarantorAgreementLine8_GuarantorAgreement_TaxId = guarantorAgreementLineNum.TaxId,
                                                       QGuarantorAgreementLine8_GuarantorAgreement_PassportId = guarantorAgreementLineNum.PassportId,
                                                       QGuarantorAgreementLine8_GuarantorAgreement_WorkPermitId = guarantorAgreementLineNum.WorkPermitId,
                                                       QGuarantorAgreementLine8_GuarantorAgreement_DateOfIssue = guarantorAgreementLineNum.DateOfIssue.DateNullToString(),
                                                       QGuarantorAgreementLine8_GuarantorAgreement_DOB = guarantorAgreementLineNum.DateOfBirth.DateNullToString(),
                                                       QGuarantorAgreementLine8_GuarantorAgreement_Age = guarantorAgreementLineNum.Age.ToString(),
                                                       QGuarantorAgreementLine8_GuarantorAgreement_Nationality = nationality != null ? nationality.Description : "",
                                                       QGuarantorAgreementLine8_GuarantorAgreement_Race = race != null ? race.Description : "",
                                                       QGuarantorAgreementLine8_GuarantorAgreement_Address = guarantorAgreementLineNum.PrimaryAddress1 + " " + guarantorAgreementLineNum.PrimaryAddress2
                                                   }).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public QBookmarkDocumentTrans GetQBookmarkDocumentTrans(Guid bookmarkDocumentTransGUID)
        {
            try
            {
                QBookmarkDocumentTrans result = (from bookmarkDocumentTrans in db.Set<BookmarkDocumentTrans>()
                                                 join bookmarkDocument in db.Set<BookmarkDocument>()
                                                 on bookmarkDocumentTrans.BookmarkDocumentGUID equals bookmarkDocument.BookmarkDocumentGUID
                                                 where bookmarkDocumentTrans.BookmarkDocumentTransGUID == bookmarkDocumentTransGUID
                                                 select new QBookmarkDocumentTrans
                                                 {
                                                     EnumDocTemType = bookmarkDocument.DocumentTemplateType
                                                 }
                                                 ).FirstOrDefault();

                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<QGuarantorAgreementLine> GetQGuarantorAgreementLines(Guid refGUID , Guid bookmarkDocumentTransGUID)
        {
            try
            {
                int i = 1;
                int j = 0;
                QBookmarkDocumentTrans vQBookmark = GetQBookmarkDocumentTrans(bookmarkDocumentTransGUID);
                List<QGuarantorAgreementLine> result = new List<QGuarantorAgreementLine>(new QGuarantorAgreementLine[8]);

                switch ((DocumentTemplateType)vQBookmark.EnumDocTemType)
                {
                    case DocumentTemplateType.GuarantorAgreement_ThaiPerson: //GuarantorAgreement_ThaiPerson
                        List<QGuarantorAgreementLine> tmp_result = (from qGuarantorAgreementLine in db.Set<GuarantorAgreementLine>()
                                                                    join guarantorTrans in db.Set<GuarantorTrans>()
                                                                    on qGuarantorAgreementLine.GuarantorTransGUID equals guarantorTrans.GuarantorTransGUID into ljqGuarantorAgreementLine
                                                                    from guarantorTrans in ljqGuarantorAgreementLine.DefaultIfEmpty()
                                                                    join relatedPersonTable in db.Set< RelatedPersonTable >()
                                                                    on guarantorTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID into ljrelatedPersonTable
                                                                    from relatedPersonTable in ljrelatedPersonTable.DefaultIfEmpty()
                                                                    where qGuarantorAgreementLine.GuarantorAgreementTableGUID == refGUID && relatedPersonTable.RecordType == 0 && relatedPersonTable.IdentificationType == 0
                                                                    //orderby qGuarantorAgreementLine.Ordering
                                                                    select new QGuarantorAgreementLine
                                                                    {
                                                                        Name = qGuarantorAgreementLine.Name,
                                                                        TaxId = qGuarantorAgreementLine.TaxId,
                                                                        PassportId = qGuarantorAgreementLine.PassportId,
                                                                        WorkPermitId = qGuarantorAgreementLine.WorkPermitId,
                                                                        DateOfIssue = qGuarantorAgreementLine.DateOfIssue.DateNullToString(),
                                                                        DOB = qGuarantorAgreementLine.DateOfBirth,
                                                                        Age = qGuarantorAgreementLine.Age,
                                                                        NationalityGUID = qGuarantorAgreementLine.NationalityGUID.GuidNullToString(),
                                                                        Nationality = (from nationality in db.Set<Nationality>()
                                                                                       where nationality.NationalityGUID == qGuarantorAgreementLine.NationalityGUID
                                                                                       select nationality.Description
                                                                                       ).FirstOrDefault(),
                                                                        RaceGUID = qGuarantorAgreementLine.RaceGUID.GuidNullToString(),
                                                                        Race = (from race in db.Set<Race>()
                                                                                where race.RaceGUID == qGuarantorAgreementLine.RaceGUID
                                                                                select race.Description).FirstOrDefault(),
                                                                        Address = qGuarantorAgreementLine.PrimaryAddress1 +" "+ qGuarantorAgreementLine.PrimaryAddress2,
                                                                        IdentificationType = (from relatedPersonTable in db.Set<RelatedPersonTable>()
                                                                                              where relatedPersonTable.RelatedPersonTableGUID == guarantorTrans.RelatedPersonTableGUID
                                                                                              select relatedPersonTable.IdentificationType
                                                                                              ).FirstOrDefault(),
                                                                        RecordType = (from relatedPersonTable in db.Set<RelatedPersonTable>()
                                                                                        where relatedPersonTable.RelatedPersonTableGUID == guarantorTrans.RelatedPersonTableGUID
                                                                                        select relatedPersonTable.RecordType
                                                                                              ).FirstOrDefault(),
                                                                        GuaranteeLand = qGuarantorAgreementLine.GuaranteeLand,
                                                                        Ordering = qGuarantorAgreementLine.Ordering.ToString(),
                                                                        OperatedBy = qGuarantorAgreementLine.OperatedBy
                                                                    }
                                                                     ).OrderBy(o => o.Ordering).Take(8).AsNoTracking().ToList();
                        foreach (var item in tmp_result)
                        {
                            result[j] = item;
                            item.Row = i;
                            i++;
                            j++;
                        }
                        break;
                    case DocumentTemplateType.GuarantorAgreement_Foreigner: //GuarantorAgreement_Foreigner
                        List<QGuarantorAgreementLine> tmp_result1 = (from qGuarantorAgreementLine in db.Set<GuarantorAgreementLine>()
                                                                    join guarantorTrans in db.Set<GuarantorTrans>()
                                                                    on qGuarantorAgreementLine.GuarantorTransGUID equals guarantorTrans.GuarantorTransGUID into ljqGuarantorAgreementLine
                                                                    from guarantorTrans in ljqGuarantorAgreementLine.DefaultIfEmpty()
                                                                    join relatedPersonTable in db.Set<RelatedPersonTable>()
                                                                    on guarantorTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID into ljrelatedPersonTable
                                                                    from relatedPersonTable in ljrelatedPersonTable.DefaultIfEmpty()
                                                                    where qGuarantorAgreementLine.GuarantorAgreementTableGUID == refGUID && relatedPersonTable.RecordType == 0 && relatedPersonTable.IdentificationType == 1
                                                                    //orderby qGuarantorAgreementLine.Ordering
                                                                    select new QGuarantorAgreementLine
                                                                    {
                                                                        Name = qGuarantorAgreementLine.Name,
                                                                        TaxId = qGuarantorAgreementLine.TaxId,
                                                                        PassportId = qGuarantorAgreementLine.PassportId,
                                                                        WorkPermitId = qGuarantorAgreementLine.WorkPermitId,
                                                                        DateOfIssue = qGuarantorAgreementLine.DateOfIssue.DateNullToString(),
                                                                        DOB = qGuarantorAgreementLine.DateOfBirth,
                                                                        Age = qGuarantorAgreementLine.Age,
                                                                        NationalityGUID = qGuarantorAgreementLine.NationalityGUID.GuidNullToString(),
                                                                        Nationality = (from nationality in db.Set<Nationality>()
                                                                                       where nationality.NationalityGUID == qGuarantorAgreementLine.NationalityGUID
                                                                                       select nationality.Description
                                                                                       ).FirstOrDefault(),
                                                                        RaceGUID = qGuarantorAgreementLine.RaceGUID.GuidNullToString(),
                                                                        Race = (from race in db.Set<Race>()
                                                                                where race.RaceGUID == qGuarantorAgreementLine.RaceGUID
                                                                                select race.Description).FirstOrDefault(),
                                                                        Address = qGuarantorAgreementLine.PrimaryAddress1 + " " + qGuarantorAgreementLine.PrimaryAddress2,
                                                                        IdentificationType = (from relatedPersonTable in db.Set<RelatedPersonTable>()
                                                                                              where relatedPersonTable.RelatedPersonTableGUID == guarantorTrans.RelatedPersonTableGUID
                                                                                              select relatedPersonTable.IdentificationType
                                                                                              ).FirstOrDefault(),
                                                                        RecordType = (from relatedPersonTable in db.Set<RelatedPersonTable>()
                                                                                      where relatedPersonTable.RelatedPersonTableGUID == guarantorTrans.RelatedPersonTableGUID
                                                                                      select relatedPersonTable.RecordType
                                                                                              ).FirstOrDefault(),
                                                                        GuaranteeLand = qGuarantorAgreementLine.GuaranteeLand,
                                                                        Ordering = qGuarantorAgreementLine.Ordering.ToString(),
                                                                        OperatedBy = qGuarantorAgreementLine.OperatedBy
                                                                    }
                                                                     ).OrderBy(o => o.Ordering).Take(8).AsNoTracking().ToList();
                        foreach (var item in tmp_result1)
                        {
                            result[j] = item;
                            item.Row = i;
                            i++;
                            j++;
                        }
                        break;
                    case DocumentTemplateType.GuarantorAgreement_Organization: //GuarantorAgreement_Organization
                        List<QGuarantorAgreementLine> tmp_result2 = (from qGuarantorAgreementLine in db.Set<GuarantorAgreementLine>()
                                                                    join guarantorTrans in db.Set<GuarantorTrans>()
                                                                    on qGuarantorAgreementLine.GuarantorTransGUID equals guarantorTrans.GuarantorTransGUID into ljqGuarantorAgreementLine
                                                                    from guarantorTrans in ljqGuarantorAgreementLine.DefaultIfEmpty()
                                                                    join relatedPersonTable in db.Set<RelatedPersonTable>()
                                                                    on guarantorTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID into ljrelatedPersonTable
                                                                    from relatedPersonTable in ljrelatedPersonTable.DefaultIfEmpty()
                                                                    where qGuarantorAgreementLine.GuarantorAgreementTableGUID == refGUID && relatedPersonTable.RecordType == 1
                                                                    //orderby qGuarantorAgreementLine.Ordering
                                                                    select new QGuarantorAgreementLine
                                                                    {
                                                                        Name = qGuarantorAgreementLine.Name,
                                                                        TaxId = qGuarantorAgreementLine.TaxId,
                                                                        PassportId = qGuarantorAgreementLine.PassportId,
                                                                        WorkPermitId = qGuarantorAgreementLine.WorkPermitId,
                                                                        DateOfIssue = qGuarantorAgreementLine.DateOfIssue.DateNullToString(),
                                                                        DOB = qGuarantorAgreementLine.DateOfBirth,
                                                                        Age = qGuarantorAgreementLine.Age,
                                                                        NationalityGUID = qGuarantorAgreementLine.NationalityGUID.GuidNullToString(),
                                                                        Nationality = (from nationality in db.Set<Nationality>()
                                                                                       where nationality.NationalityGUID == qGuarantorAgreementLine.NationalityGUID
                                                                                       select nationality.Description
                                                                                       ).FirstOrDefault(),
                                                                        RaceGUID = qGuarantorAgreementLine.RaceGUID.GuidNullToString(),
                                                                        Race = (from race in db.Set<Race>()
                                                                                where race.RaceGUID == qGuarantorAgreementLine.RaceGUID
                                                                                select race.Description).FirstOrDefault(),
                                                                        Address = qGuarantorAgreementLine.PrimaryAddress1 + " " + qGuarantorAgreementLine.PrimaryAddress2,
                                                                        IdentificationType = (from relatedPersonTable in db.Set<RelatedPersonTable>()
                                                                                              where relatedPersonTable.RelatedPersonTableGUID == guarantorTrans.RelatedPersonTableGUID
                                                                                              select relatedPersonTable.IdentificationType
                                                                                              ).FirstOrDefault(),
                                                                        RecordType = (from relatedPersonTable in db.Set<RelatedPersonTable>()
                                                                                      where relatedPersonTable.RelatedPersonTableGUID == guarantorTrans.RelatedPersonTableGUID
                                                                                      select relatedPersonTable.RecordType
                                                                                              ).FirstOrDefault(),
                                                                        GuaranteeLand = qGuarantorAgreementLine.GuaranteeLand,
                                                                        Ordering = qGuarantorAgreementLine.Ordering.ToString(),
                                                                        OperatedBy = qGuarantorAgreementLine.OperatedBy
                                                                    }
                                                                     ).OrderBy(o => o.Ordering).Take(8).AsNoTracking().ToList();
                        foreach (var item in tmp_result2)
                        {
                            result[j] = item;
                            item.Row = i;
                            i++;
                            j++;
                        }
                        break;
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BookmaskDocumentGuarantorAgreement GetBookmaskDocumentGuarantorAgreement_ThaiPerson(Guid refGUID ,Guid bookmarkDocumentTransGuid)
        {
            try
            {
                QGuarantorAgreementTable qGuarantorAgreementTable = GetqGuarantorAgreementTable(refGUID);
                List<QGuarantorAgreementLine> qGuarantorAgreementLine = GetQGuarantorAgreementLines(refGUID, bookmarkDocumentTransGuid);
                IConsortiumTransRepo consortiumTransRepo = new ConsortiumTransRepo(db);
                //List<QConsortiumTrans1> qConsortiumTrans = consortiumTransRepo.GetQConsortiumTrans(refGUID);

                QConsortiumTrans1 qConsortiumTrans1 = consortiumTransRepo.GetQConsortiumTrans1(refGUID);
                QConsortiumTrans2 qConsortiumTrans2 = consortiumTransRepo.GetQConsortiumTrans2(refGUID);
                QConsortiumTrans3 qconsortiumTrans3 = consortiumTransRepo.GetQConsortiumTrans3(refGUID);
                IJointVentureTransRepo jointVentureTransRepo = new JointVentureTransRepo(db);
                QJointVentureTrans1 qJointVentureTrans1 = jointVentureTransRepo.GetQJointVentureTrans1(refGUID);
                QJointVentureTrans2 qJointVentureTrans2 = jointVentureTransRepo.GetQJointVentureTrans2(refGUID);
                QJointVentureTrans3 qjointVentureTrans3 = jointVentureTransRepo.GetQJointVentureTrans3(refGUID);
                ISharedService sharedService = new SharedService(db);
                int CountLine = (from guarantorAgreementLine in db.Set<GuarantorAgreementLine>()
                                 where guarantorAgreementLine.GuarantorAgreementTableGUID == refGUID
                                 select guarantorAgreementLine.GuarantorAgreementTableGUID
                                 ).Count();
                var NumberToWordsTH = sharedService.NumberToWordsTH(qGuarantorAgreementTable.QGuarantorAgreementTable_ApprovedCreditLimit);
                BookmaskDocumentGuarantorAgreement_Variable bookmaskDocumentGuarantorAgreement_Variable = new BookmaskDocumentGuarantorAgreement_Variable()
                {
                    Variable_GuarantorAgreementNo1 = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_GuarantorAgreementId + "(" + qGuarantorAgreementLine[0].Ordering.ToString() + "/" + CountLine + ")" : "",
                    Variable_GuarantorAgreementNo2 = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_GuarantorAgreementId + "(" + qGuarantorAgreementLine[1].Ordering.ToString() + "/" + CountLine + ")" : "",
                    Variable_GuarantorAgreementNo3 = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_GuarantorAgreementId + "(" + qGuarantorAgreementLine[2].Ordering.ToString() + "/" + CountLine + ")" : "",
                    Variable_GuarantorAgreementNo4 = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_GuarantorAgreementId + "(" + qGuarantorAgreementLine[3].Ordering.ToString() + "/" + CountLine + ")" : "",
                    Variable_GuarantorAgreementNo5 = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_GuarantorAgreementId + "(" + qGuarantorAgreementLine[4].Ordering.ToString() + "/" + CountLine + ")" : "",
                    Variable_GuarantorAgreementNo6 = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_GuarantorAgreementId + "(" + qGuarantorAgreementLine[5].Ordering.ToString() + "/" + CountLine + ")" : "",
                    Variable_GuarantorAgreementNo7 = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_GuarantorAgreementId + "(" + qGuarantorAgreementLine[6].Ordering.ToString() + "/" + CountLine + ")" : "",
                    Variable_GuarantorAgreementNo8 = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_GuarantorAgreementId + "(" + qGuarantorAgreementLine[7].Ordering.ToString() + "/" + CountLine + ")" : "",
                    Variable_ContractDate = qGuarantorAgreementTable.QGuarantorAgreementTable_MainAgreementDate != null ? sharedService.DateToWord(qGuarantorAgreementTable.QGuarantorAgreementTable_MainAgreementDate, TextConstants.TH, TextConstants.DMY) : "",
                };

                BookmaskDocumentGuarantorAgreement bookmaskDocumentGuarantorAgreement = new BookmaskDocumentGuarantorAgreement()
                {
                    //QGuarantorAgreementTable_GuarantorAgreementId = qGuarantorAgreementTable.QGuarantorAgreementTable_GuarantorAgreementId,
                    Variable_GuarantorAgreementNo1 = bookmaskDocumentGuarantorAgreement_Variable != null ? bookmaskDocumentGuarantorAgreement_Variable.Variable_GuarantorAgreementNo1 : "",
                    Variable_GuarantorAgreementNo2 = bookmaskDocumentGuarantorAgreement_Variable != null ? bookmaskDocumentGuarantorAgreement_Variable.Variable_GuarantorAgreementNo2 : "",
                    Variable_GuarantorAgreementNo3 = bookmaskDocumentGuarantorAgreement_Variable != null ? bookmaskDocumentGuarantorAgreement_Variable.Variable_GuarantorAgreementNo3 : "",
                    Variable_GuarantorAgreementNo4 = bookmaskDocumentGuarantorAgreement_Variable != null ? bookmaskDocumentGuarantorAgreement_Variable.Variable_GuarantorAgreementNo4 : "",
                    Variable_GuarantorAgreementNo5 = bookmaskDocumentGuarantorAgreement_Variable != null ? bookmaskDocumentGuarantorAgreement_Variable.Variable_GuarantorAgreementNo5 : "",
                    Variable_GuarantorAgreementNo6 = bookmaskDocumentGuarantorAgreement_Variable != null ? bookmaskDocumentGuarantorAgreement_Variable.Variable_GuarantorAgreementNo6 : "",
                    Variable_GuarantorAgreementNo7 = bookmaskDocumentGuarantorAgreement_Variable != null ? bookmaskDocumentGuarantorAgreement_Variable.Variable_GuarantorAgreementNo7 : "",
                    Variable_GuarantorAgreementNo8 = bookmaskDocumentGuarantorAgreement_Variable != null ? bookmaskDocumentGuarantorAgreement_Variable.Variable_GuarantorAgreementNo8 : "",

                    QCompany_Name = qGuarantorAgreementTable != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_CompanyName : "",
                    QCompany_Address = qGuarantorAgreementTable != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_CompanyAddress : "",
                    QGuarantorAgreementTable_AgreementDate = qGuarantorAgreementTable != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_AgreementDate : "",
                    QGuarantorAgreementTable_AgreementYear = qGuarantorAgreementTable != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_AgreementYear : "",
                    QMainAgreementTable_MainAgreementId = qGuarantorAgreementTable != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_MainAgreementId : "",
                    Variable_ContractDate = qGuarantorAgreementTable != null ? bookmaskDocumentGuarantorAgreement_Variable.Variable_ContractDate : "",
                    QMainAgreementTable_ApprovedCreditLimit = qGuarantorAgreementTable != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_ApprovedCreditLimit : 0,
                    Variable_MainApprovedCreditLimit = NumberToWordsTH,
                    THApprovedCreditLimit = NumberToWordsTH,
                    QGuarantorAgreementTable_CompanyWitness1 = qGuarantorAgreementTable.QGuarantorAgreementTable_CompanyWitness1,
                    QGuarantorAgreementTable_CompanyWitness2 = qGuarantorAgreementTable.QGuarantorAgreementTable_CompanyWitness2,
                    QGuarantorAgreementTable_MainCustomerName = qGuarantorAgreementTable.QGuarantorAgreementTable_MainCustomerName,
                    QGuarantorAgreementTable_CustomerAddress = qGuarantorAgreementTable.QGuarantorAgreementTable_CustomerAddress,
                    QGuarantorAgreementLine1_Name = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].Name : "",
                    QGuarantorAgreementLine1_Age = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].Age : 0,
                    QGuarantorAgreementLine1_Race = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].Race : "",
                    QGuarantorAgreementLine1_Nationality = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].Nationality : "",
                    QGuarantorAgreementLine1_Address = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].Address : "",
                    QGuarantorAgreementLine1_TaxId = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].TaxId : "",
                    Variable_GuarantorBirthday1 = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[0].DOB, TextConstants.TH, TextConstants.DMY) : "" : "",
                    QGuarantorAgreementLine1_PassportId = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].PassportId : "",
                    QGuarantorAgreementLine1_WorkPermitId = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].WorkPermitId : "",
                    QGuarantorAgreementLine1_GuaranteeLand = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].GuaranteeLand : "",
                    QConsortiumTrans1_ConsName = qGuarantorAgreementTable != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_Description : "",
                    QConsortiumTrans1_CustomerName = qConsortiumTrans1 != null ? qConsortiumTrans1.QConsortiumTrans1_GuarantorAgreement_CustomerName : "",
                    QConsortiumTrans1_Address = qConsortiumTrans1 != null ? qConsortiumTrans1.QConsortiumTrans1_GuarantorAgreement_Address : "",
                    QConsortiumTrans2_CustomerName = qConsortiumTrans2 != null ? qConsortiumTrans2.QConsortiumTrans2_GuarantorAgreement_CustomerName : "",
                    QConsortiumTrans2_Address = qConsortiumTrans2 != null ? qConsortiumTrans2.QConsortiumTrans2_GuarantorAgreement_Address : "",
                    QConsortiumTrans3_CustomerName = qconsortiumTrans3 != null ? qconsortiumTrans3.QConsortiumTrans3_GuarantorAgreement_CustomerName : "",
                    QConsortiumTrans3_Address = qconsortiumTrans3 != null ? qconsortiumTrans3.QConsortiumTrans3_GuarantorAgreement_Address : "",
                    QJointVentureTrans1_Name = qJointVentureTrans1 != null ? qJointVentureTrans1.QJointVentureTrans1_GuarantorAgreement_Name : "",
                    QJointVentureTrans1_Address = qJointVentureTrans1 != null ? qJointVentureTrans1.QJointVentureTrans1_GuarantorAgreement_Address : "",
                    QJointVentureTrans2_Name = qJointVentureTrans2 != null ? qJointVentureTrans2.QJointVentureTrans2_GuarantorAgreement_Name : "",
                    QJointVentureTrans2_Address = qJointVentureTrans2 != null ? qJointVentureTrans2.QJointVentureTrans2_GuarantorAgreement_Address : "",
                    QJointVentureTrans3_Name = qjointVentureTrans3 != null ? qjointVentureTrans3.QJointVentureTrans3_GuarantorAgreement_Name : "",
                    QJointVentureTrans3_Address = qjointVentureTrans3 != null ? qjointVentureTrans3.QJointVentureTrans3_GuarantorAgreement_Address : "",
                    //-----------------------------
                    QGuarantorAgreementLine2_Name = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].Name : "",
                    QGuarantorAgreementLine2_Age = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].Age : 0,
                    QGuarantorAgreementLine2_Race = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].Race : "",
                    QGuarantorAgreementLine2_Nationality = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].Nationality : "",
                    QGuarantorAgreementLine2_Address = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].Address : "",
                    QGuarantorAgreementLine2_TaxId = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].TaxId : "",
                    Variable_GuarantorBirthday2 = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[1].DOB, TextConstants.TH, TextConstants.DMY) : "" : "",
                    QGuarantorAgreementLine2_PassportId = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].PassportId : "",
                    QGuarantorAgreementLine2_WorkPermitId = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].WorkPermitId : "",
                    QGuarantorAgreementLine2_GuaranteeLand = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].GuaranteeLand : "",
                    //--------------------------------
                    QGuarantorAgreementLine3_Name = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].Name : "",
                    QGuarantorAgreementLine3_Age = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].Age : 0,
                    QGuarantorAgreementLine3_Race = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].Race : "",
                    QGuarantorAgreementLine3_Nationality = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].Nationality : "",
                    QGuarantorAgreementLine3_Address = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].Address : "",
                    QGuarantorAgreementLine3_TaxId = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].TaxId : "",
                    Variable_GuarantorBirthday3 = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[2].DOB, TextConstants.TH, TextConstants.DMY) : "" : "",
                    QGuarantorAgreementLine3_PassportId = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].PassportId : "",
                    QGuarantorAgreementLine3_WorkPermitId = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].WorkPermitId : "",
                    QGuarantorAgreementLine3_GuaranteeLand = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].GuaranteeLand : "",
                    //--------------------------------
                    QGuarantorAgreementLine4_Name = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].Name : "",
                    QGuarantorAgreementLine4_Age = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].Age : 0,
                    QGuarantorAgreementLine4_Race = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].Race : "",
                    QGuarantorAgreementLine4_Nationality = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].Nationality : "",
                    QGuarantorAgreementLine4_Address = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].Address : "",
                    QGuarantorAgreementLine4_TaxId = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].TaxId : "",
                    Variable_GuarantorBirthday4 = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[3].DOB, TextConstants.TH, TextConstants.DMY) : "" : "",
                    QGuarantorAgreementLine4_PassportId = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].PassportId : "",
                    QGuarantorAgreementLine4_WorkPermitId = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].WorkPermitId : "",
                    QGuarantorAgreementLine4_GuaranteeLand = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].GuaranteeLand : "",
                    //--------------------------------
                    QGuarantorAgreementLine5_Name = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].Name : "",
                    QGuarantorAgreementLine5_Age = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].Age: 0,
                    QGuarantorAgreementLine5_Race = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].Race : "",
                    QGuarantorAgreementLine5_Nationality = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].Nationality : "",
                    QGuarantorAgreementLine5_Address = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].Address : "",
                    QGuarantorAgreementLine5_TaxId = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].TaxId : "",
                    Variable_GuarantorBirthday5 = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[4].DOB, TextConstants.TH, TextConstants.DMY) : "" : "",
                    QGuarantorAgreementLine5_PassportId = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].PassportId : "",
                    QGuarantorAgreementLine5_WorkPermitId = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].WorkPermitId : "",
                    QGuarantorAgreementLine5_GuaranteeLand = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].GuaranteeLand : "",
                    //--------------------------------
                    QGuarantorAgreementLine6_Name = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].Name : "",
                    QGuarantorAgreementLine6_Age = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].Age : 0,
                    QGuarantorAgreementLine6_Race = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].Race : "",
                    QGuarantorAgreementLine6_Nationality = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].Nationality : "",
                    QGuarantorAgreementLine6_Address = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].Address : "",
                    QGuarantorAgreementLine6_TaxId = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].TaxId : "",
                    Variable_GuarantorBirthday6 = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[5].DOB, TextConstants.TH, TextConstants.DMY) : "" : "",
                    QGuarantorAgreementLine6_PassportId = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].PassportId : "",
                    QGuarantorAgreementLine6_WorkPermitId = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].WorkPermitId : "",
                    QGuarantorAgreementLine6_GuaranteeLand = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].GuaranteeLand : "",
                    //--------------------------------
                    QGuarantorAgreementLine7_Name = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].Name : "",
                    QGuarantorAgreementLine7_Age = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].Age : 0,
                    QGuarantorAgreementLine7_Race = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].Race : "",
                    QGuarantorAgreementLine7_Nationality = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].Nationality : "",
                    QGuarantorAgreementLine7_Address = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].Address : "",
                    QGuarantorAgreementLine7_TaxId = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].TaxId : "",
                    Variable_GuarantorBirthday7 = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[6].DOB, TextConstants.TH, TextConstants.DMY) : "" : "",
                    QGuarantorAgreementLine7_PassportId = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].PassportId : "",
                    QGuarantorAgreementLine7_WorkPermitId = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].WorkPermitId : "",
                    QGuarantorAgreementLine7_GuaranteeLand = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].GuaranteeLand : "",
                    //--------------------------------
                    QGuarantorAgreementLine8_Name = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].Name : "",
                    QGuarantorAgreementLine8_Age = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].Age : 0,
                    QGuarantorAgreementLine8_Race = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].Race : "",
                    QGuarantorAgreementLine8_Nationality = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].Nationality : "",
                    QGuarantorAgreementLine8_Address = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].Address : "",
                    QGuarantorAgreementLine8_TaxId = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].TaxId : "",
                    Variable_GuarantorBirthday8 = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[7].DOB, TextConstants.TH, TextConstants.DMY) : "" : "",
                    QGuarantorAgreementLine8_PassportId = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].PassportId : "",
                    QGuarantorAgreementLine8_WorkPermitId = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].WorkPermitId : "",
                    QGuarantorAgreementLine8_GuaranteeLand = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].GuaranteeLand : "",

                    QGuarantorAgreementLine1_OperatedBy = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].OperatedBy : "",
                    QGuarantorAgreementLine2_OperatedBy = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].OperatedBy : "",
                    QGuarantorAgreementLine3_OperatedBy = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].OperatedBy : "",
                    QGuarantorAgreementLine4_OperatedBy = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].OperatedBy : "",
                    QGuarantorAgreementLine5_OperatedBy = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].OperatedBy : "",
                    QGuarantorAgreementLine6_OperatedBy = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].OperatedBy : "",
                    QGuarantorAgreementLine7_OperatedBy = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].OperatedBy : "",
                    QGuarantorAgreementLine8_OperatedBy = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].OperatedBy : "",
                };


                return bookmaskDocumentGuarantorAgreement;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BookmaskDocumentGuarantorAgreement GetBookmaskDocumentGuarantorAgreement_Foreigner(Guid refGUID, Guid bookmarkDocumentTransGuid)
        {
            try
            {
                QGuarantorAgreementTable qGuarantorAgreementTable = GetqGuarantorAgreementTable(refGUID);
                List<QGuarantorAgreementLine> qGuarantorAgreementLine = GetQGuarantorAgreementLines(refGUID, bookmarkDocumentTransGuid);
                IConsortiumTransRepo consortiumTransRepo = new ConsortiumTransRepo(db);
                //List<QConsortiumTrans1> qConsortiumTrans = consortiumTransRepo.GetQConsortiumTrans(refGUID);
                QConsortiumTrans1 qConsortiumTrans1 = consortiumTransRepo.GetQConsortiumTrans1(refGUID);
                QConsortiumTrans2 qConsortiumTrans2 = consortiumTransRepo.GetQConsortiumTrans2(refGUID); ;
                QConsortiumTrans3 qconsortiumTrans3 = consortiumTransRepo.GetQConsortiumTrans3(refGUID); ;
                IJointVentureTransRepo jointVentureTransRepo = new JointVentureTransRepo(db);
                QJointVentureTrans1 qJointVentureTrans1 = jointVentureTransRepo.GetQJointVentureTrans1(refGUID);
                QJointVentureTrans2 qJointVentureTrans2 = jointVentureTransRepo.GetQJointVentureTrans2(refGUID);
                QJointVentureTrans3 qjointVentureTrans3 = jointVentureTransRepo.GetQJointVentureTrans3(refGUID);
                ISharedService sharedService = new SharedService(db);
                int CountLine = (from guarantorAgreementLine in db.Set<GuarantorAgreementLine>()
                                 where guarantorAgreementLine.GuarantorAgreementTableGUID == refGUID
                                 select guarantorAgreementLine.GuarantorAgreementTableGUID
                                 ).Count();
                var NumberToWordsTH = sharedService.NumberToWordsTH(qGuarantorAgreementTable.QGuarantorAgreementTable_ApprovedCreditLimit);
                BookmaskDocumentGuarantorAgreement_Variable bookmaskDocumentGuarantorAgreement_Variable = new BookmaskDocumentGuarantorAgreement_Variable()
                {
                    Variable_GuarantorAgreementNo1 = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_GuarantorAgreementId + "(" + qGuarantorAgreementLine[0].Ordering.ToString() + "/" + CountLine + ")" : "",
                    Variable_GuarantorAgreementNo2 = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_GuarantorAgreementId + "(" + qGuarantorAgreementLine[1].Ordering.ToString() + "/" + CountLine + ")" : "",
                    Variable_GuarantorAgreementNo3 = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_GuarantorAgreementId + "(" + qGuarantorAgreementLine[2].Ordering.ToString() + "/" + CountLine + ")" : "",
                    Variable_GuarantorAgreementNo4 = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_GuarantorAgreementId + "(" + qGuarantorAgreementLine[3].Ordering.ToString() + "/" + CountLine + ")" : "",
                    Variable_GuarantorAgreementNo5 = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_GuarantorAgreementId + "(" + qGuarantorAgreementLine[4].Ordering.ToString() + "/" + CountLine + ")" : "",
                    Variable_GuarantorAgreementNo6 = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_GuarantorAgreementId + "(" + qGuarantorAgreementLine[5].Ordering.ToString() + "/" + CountLine + ")" : "",
                    Variable_GuarantorAgreementNo7 = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_GuarantorAgreementId + "(" + qGuarantorAgreementLine[6].Ordering.ToString() + "/" + CountLine + ")" : "",
                    Variable_GuarantorAgreementNo8 = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_GuarantorAgreementId + "(" + qGuarantorAgreementLine[7].Ordering.ToString() + "/" + CountLine + ")" : "",
                    Variable_ContractDate = qGuarantorAgreementTable.QGuarantorAgreementTable_MainAgreementDate != null ? sharedService.DateToWord(qGuarantorAgreementTable.QGuarantorAgreementTable_MainAgreementDate, TextConstants.TH, TextConstants.DMY) : "",
                };

                BookmaskDocumentGuarantorAgreement bookmaskDocumentGuarantorAgreement = new BookmaskDocumentGuarantorAgreement()
                {
                    //QGuarantorAgreementTable_GuarantorAgreementId = qGuarantorAgreementTable.QGuarantorAgreementTable_GuarantorAgreementId,
                    Variable_GuarantorAgreementNo1 = bookmaskDocumentGuarantorAgreement_Variable != null ? bookmaskDocumentGuarantorAgreement_Variable.Variable_GuarantorAgreementNo1 : "",
                    Variable_GuarantorAgreementNo2 = bookmaskDocumentGuarantorAgreement_Variable != null ? bookmaskDocumentGuarantorAgreement_Variable.Variable_GuarantorAgreementNo2 : "",
                    Variable_GuarantorAgreementNo3 = bookmaskDocumentGuarantorAgreement_Variable != null ? bookmaskDocumentGuarantorAgreement_Variable.Variable_GuarantorAgreementNo3 : "",
                    Variable_GuarantorAgreementNo4 = bookmaskDocumentGuarantorAgreement_Variable != null ? bookmaskDocumentGuarantorAgreement_Variable.Variable_GuarantorAgreementNo4 : "",
                    Variable_GuarantorAgreementNo5 = bookmaskDocumentGuarantorAgreement_Variable != null ? bookmaskDocumentGuarantorAgreement_Variable.Variable_GuarantorAgreementNo5 : "",
                    Variable_GuarantorAgreementNo6 = bookmaskDocumentGuarantorAgreement_Variable != null ? bookmaskDocumentGuarantorAgreement_Variable.Variable_GuarantorAgreementNo6 : "",
                    Variable_GuarantorAgreementNo7 = bookmaskDocumentGuarantorAgreement_Variable != null ? bookmaskDocumentGuarantorAgreement_Variable.Variable_GuarantorAgreementNo7 : "",
                    Variable_GuarantorAgreementNo8 = bookmaskDocumentGuarantorAgreement_Variable != null ? bookmaskDocumentGuarantorAgreement_Variable.Variable_GuarantorAgreementNo8 : "",

                    QCompany_Name = qGuarantorAgreementTable != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_CompanyName : "",
                    QCompany_Address = qGuarantorAgreementTable != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_CompanyAddress : "",
                    QGuarantorAgreementTable_AgreementDate = qGuarantorAgreementTable != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_AgreementDate : "",
                    QGuarantorAgreementTable_AgreementYear = qGuarantorAgreementTable != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_AgreementYear : "",
                    QMainAgreementTable_MainAgreementId = qGuarantorAgreementTable != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_MainAgreementId : "",
                    Variable_ContractDate = qGuarantorAgreementTable != null ? bookmaskDocumentGuarantorAgreement_Variable.Variable_ContractDate : "",
                    QMainAgreementTable_ApprovedCreditLimit = qGuarantorAgreementTable != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_ApprovedCreditLimit : 0,
                    Variable_MainApprovedCreditLimit = NumberToWordsTH,
                    THApprovedCreditLimit = NumberToWordsTH,
                    QGuarantorAgreementTable_CompanyWitness1 = qGuarantorAgreementTable.QGuarantorAgreementTable_CompanyWitness1,
                    QGuarantorAgreementTable_CompanyWitness2 = qGuarantorAgreementTable.QGuarantorAgreementTable_CompanyWitness2,
                    QGuarantorAgreementTable_MainCustomerName = qGuarantorAgreementTable.QGuarantorAgreementTable_MainCustomerName,
                    QGuarantorAgreementTable_CustomerAddress = qGuarantorAgreementTable.QGuarantorAgreementTable_CustomerAddress,
                    QGuarantorAgreementLine1_Name = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].Name : "",
                    QGuarantorAgreementLine1_Age = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].Age : 0,
                    QGuarantorAgreementLine1_Race = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].Race : "",
                    QGuarantorAgreementLine1_Nationality = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].Nationality : "",
                    QGuarantorAgreementLine1_Address = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].Address : "",
                    QGuarantorAgreementLine1_TaxId = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].TaxId : "",
                    Variable_GuarantorBirthday1 = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[0].DOB, TextConstants.TH, TextConstants.DMY) : "" : "",
                    QGuarantorAgreementLine1_PassportId = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].PassportId : "",
                    QGuarantorAgreementLine1_WorkPermitId = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].WorkPermitId : "",
                    QGuarantorAgreementLine1_GuaranteeLand = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].GuaranteeLand : "",
                    QConsortiumTrans1_ConsName = qGuarantorAgreementTable != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_Description : "",
                    QConsortiumTrans1_CustomerName = qConsortiumTrans1 != null ? qConsortiumTrans1.QConsortiumTrans1_GuarantorAgreement_CustomerName : "",
                    QConsortiumTrans1_Address = qConsortiumTrans1 != null ? qConsortiumTrans1.QConsortiumTrans1_GuarantorAgreement_Address : "",
                    QConsortiumTrans2_CustomerName = qConsortiumTrans2 != null ? qConsortiumTrans2.QConsortiumTrans2_GuarantorAgreement_CustomerName : "",
                    QConsortiumTrans2_Address = qConsortiumTrans2 != null ? qConsortiumTrans2.QConsortiumTrans2_GuarantorAgreement_Address : "",
                    QConsortiumTrans3_CustomerName = qconsortiumTrans3 != null ? qconsortiumTrans3.QConsortiumTrans3_GuarantorAgreement_CustomerName : "",
                    QConsortiumTrans3_Address = qconsortiumTrans3 != null ? qconsortiumTrans3.QConsortiumTrans3_GuarantorAgreement_Address : "",
                    QJointVentureTrans1_Name = qJointVentureTrans1 != null ? qJointVentureTrans1.QJointVentureTrans1_GuarantorAgreement_Name : "",
                    QJointVentureTrans1_Address = qJointVentureTrans1 != null ? qJointVentureTrans1.QJointVentureTrans1_GuarantorAgreement_Address : "",
                    QJointVentureTrans2_Name = qJointVentureTrans2 != null ? qJointVentureTrans2.QJointVentureTrans2_GuarantorAgreement_Name : "",
                    QJointVentureTrans2_Address = qJointVentureTrans2 != null ? qJointVentureTrans2.QJointVentureTrans2_GuarantorAgreement_Address : "",
                    QJointVentureTrans3_Name = qjointVentureTrans3 != null ? qjointVentureTrans3.QJointVentureTrans3_GuarantorAgreement_Name : "",
                    QJointVentureTrans3_Address = qjointVentureTrans3 != null ? qjointVentureTrans3.QJointVentureTrans3_GuarantorAgreement_Address : "",
                    //-----------------------------
                    QGuarantorAgreementLine2_Name = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].Name : "",
                    QGuarantorAgreementLine2_Age = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].Age : 0,
                    QGuarantorAgreementLine2_Race = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].Race : "",
                    QGuarantorAgreementLine2_Nationality = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].Nationality : "",
                    QGuarantorAgreementLine2_Address = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].Address : "",
                    QGuarantorAgreementLine2_TaxId = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].TaxId : "",
                    Variable_GuarantorBirthday2 = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[1].DOB, TextConstants.TH, TextConstants.DMY) : "" : "",
                    QGuarantorAgreementLine2_PassportId = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].PassportId : "",
                    QGuarantorAgreementLine2_WorkPermitId = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].WorkPermitId : "",
                    QGuarantorAgreementLine2_GuaranteeLand = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].GuaranteeLand : "",
                    //--------------------------------
                    QGuarantorAgreementLine3_Name = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].Name : "",
                    QGuarantorAgreementLine3_Age = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].Age : 0,
                    QGuarantorAgreementLine3_Race = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].Race : "",
                    QGuarantorAgreementLine3_Nationality = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].Nationality : "",
                    QGuarantorAgreementLine3_Address = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].Address : "",
                    QGuarantorAgreementLine3_TaxId = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].TaxId : "",
                    Variable_GuarantorBirthday3 = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[2].DOB, TextConstants.TH, TextConstants.DMY) : "" : "",
                    QGuarantorAgreementLine3_PassportId = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].PassportId : "",
                    QGuarantorAgreementLine3_WorkPermitId = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].WorkPermitId : "",
                    QGuarantorAgreementLine3_GuaranteeLand = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].GuaranteeLand : "",
                    //--------------------------------
                    QGuarantorAgreementLine4_Name = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].Name : "",
                    QGuarantorAgreementLine4_Age = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].Age : 0,
                    QGuarantorAgreementLine4_Race = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].Race : "",
                    QGuarantorAgreementLine4_Nationality = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].Nationality : "",
                    QGuarantorAgreementLine4_Address = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].Address : "",
                    QGuarantorAgreementLine4_TaxId = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].TaxId : "",
                    Variable_GuarantorBirthday4 = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[3].DOB, TextConstants.TH, TextConstants.DMY) : "" : "",
                    QGuarantorAgreementLine4_PassportId = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].PassportId : "",
                    QGuarantorAgreementLine4_WorkPermitId = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].WorkPermitId : "",
                    QGuarantorAgreementLine4_GuaranteeLand = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].GuaranteeLand : "",
                    //--------------------------------
                    QGuarantorAgreementLine5_Name = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].Name : "",
                    QGuarantorAgreementLine5_Age = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].Age : 0,
                    QGuarantorAgreementLine5_Race = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].Race : "",
                    QGuarantorAgreementLine5_Nationality = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].Nationality : "",
                    QGuarantorAgreementLine5_Address = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].Address : "",
                    QGuarantorAgreementLine5_TaxId = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].TaxId : "",
                    Variable_GuarantorBirthday5 = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[4].DOB, TextConstants.TH, TextConstants.DMY) : "" : "",
                    QGuarantorAgreementLine5_PassportId = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].PassportId : "",
                    QGuarantorAgreementLine5_WorkPermitId = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].WorkPermitId : "",
                    QGuarantorAgreementLine5_GuaranteeLand = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].GuaranteeLand : "",
                    //--------------------------------
                    QGuarantorAgreementLine6_Name = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].Name : "",
                    QGuarantorAgreementLine6_Age = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].Age : 0,
                    QGuarantorAgreementLine6_Race = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].Race : "",
                    QGuarantorAgreementLine6_Nationality = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].Nationality : "",
                    QGuarantorAgreementLine6_Address = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].Address : "",
                    QGuarantorAgreementLine6_TaxId = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].TaxId : "",
                    Variable_GuarantorBirthday6 = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[5].DOB, TextConstants.TH, TextConstants.DMY) : "" : "",
                    QGuarantorAgreementLine6_PassportId = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].PassportId : "",
                    QGuarantorAgreementLine6_WorkPermitId = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].WorkPermitId : "",
                    QGuarantorAgreementLine6_GuaranteeLand = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].GuaranteeLand : "",
                    //--------------------------------
                    QGuarantorAgreementLine7_Name = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].Name : "",
                    QGuarantorAgreementLine7_Age = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].Age : 0,
                    QGuarantorAgreementLine7_Race = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].Race : "",
                    QGuarantorAgreementLine7_Nationality = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].Nationality : "",
                    QGuarantorAgreementLine7_Address = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].Address : "",
                    QGuarantorAgreementLine7_TaxId = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].TaxId : "",
                    Variable_GuarantorBirthday7 = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[6].DOB, TextConstants.TH, TextConstants.DMY) : "" : "",
                    QGuarantorAgreementLine7_PassportId = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].PassportId : "",
                    QGuarantorAgreementLine7_WorkPermitId = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].WorkPermitId : "",
                    QGuarantorAgreementLine7_GuaranteeLand = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].GuaranteeLand : "",
                    //--------------------------------
                    QGuarantorAgreementLine8_Name = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].Name : "",
                    QGuarantorAgreementLine8_Age = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].Age : 0,
                    QGuarantorAgreementLine8_Race = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].Race : "",
                    QGuarantorAgreementLine8_Nationality = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].Nationality : "",
                    QGuarantorAgreementLine8_Address = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].Address : "",
                    QGuarantorAgreementLine8_TaxId = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].TaxId : "",
                    Variable_GuarantorBirthday8 = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[7].DOB, TextConstants.TH, TextConstants.DMY) : "" : "",
                    QGuarantorAgreementLine8_PassportId = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].PassportId : "",
                    QGuarantorAgreementLine8_WorkPermitId = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].WorkPermitId : "",
                    QGuarantorAgreementLine8_GuaranteeLand = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].GuaranteeLand : "",

                    QGuarantorAgreementLine1_OperatedBy = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].OperatedBy : "",
                    QGuarantorAgreementLine2_OperatedBy = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].OperatedBy : "",
                    QGuarantorAgreementLine3_OperatedBy = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].OperatedBy : "",
                    QGuarantorAgreementLine4_OperatedBy = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].OperatedBy : "",
                    QGuarantorAgreementLine5_OperatedBy = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].OperatedBy : "",
                    QGuarantorAgreementLine6_OperatedBy = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].OperatedBy : "",
                    QGuarantorAgreementLine7_OperatedBy = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].OperatedBy : "",
                    QGuarantorAgreementLine8_OperatedBy = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].OperatedBy : "",
                };


                return bookmaskDocumentGuarantorAgreement;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BookmaskDocumentGuarantorAgreement GetBookmaskDocumentGuarantorAgreement_Organization(Guid refGUID, Guid bookmarkDocumentTransGuid)
        {
            try
            {
                QGuarantorAgreementTable qGuarantorAgreementTable = GetqGuarantorAgreementTable(refGUID);
                List<QGuarantorAgreementLine> qGuarantorAgreementLine = GetQGuarantorAgreementLines(refGUID, bookmarkDocumentTransGuid);
                IConsortiumTransRepo consortiumTransRepo = new ConsortiumTransRepo(db);
                //List<QConsortiumTrans1> qConsortiumTrans = consortiumTransRepo.GetQConsortiumTrans(refGUID);
                QConsortiumTrans1 qConsortiumTrans1 = consortiumTransRepo.GetQConsortiumTrans1(refGUID);
                QConsortiumTrans2 qConsortiumTrans2 = consortiumTransRepo.GetQConsortiumTrans2(refGUID);
                QConsortiumTrans3 qconsortiumTrans3 = consortiumTransRepo.GetQConsortiumTrans3(refGUID);
                IJointVentureTransRepo jointVentureTransRepo = new JointVentureTransRepo(db);
                QJointVentureTrans1 qJointVentureTrans1 = jointVentureTransRepo.GetQJointVentureTrans1(refGUID);
                QJointVentureTrans2 qJointVentureTrans2 = jointVentureTransRepo.GetQJointVentureTrans2(refGUID);
                QJointVentureTrans3 qjointVentureTrans3 = jointVentureTransRepo.GetQJointVentureTrans3(refGUID);
                ISharedService sharedService = new SharedService(db);
                int CountLine = (from guarantorAgreementLine in db.Set<GuarantorAgreementLine>()
                                 where guarantorAgreementLine.GuarantorAgreementTableGUID == refGUID
                                 select guarantorAgreementLine.GuarantorAgreementTableGUID
                                 ).Count();
                var NumberToWordsTH = sharedService.NumberToWordsTH(qGuarantorAgreementTable.QGuarantorAgreementTable_ApprovedCreditLimit);
                BookmaskDocumentGuarantorAgreement_Variable bookmaskDocumentGuarantorAgreement_Variable = new BookmaskDocumentGuarantorAgreement_Variable()
                {
                    Variable_GuarantorAgreementNo1 = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_GuarantorAgreementId + "(" + qGuarantorAgreementLine[0].Ordering.ToString() + "/" + CountLine + ")" : "",
                    Variable_GuarantorAgreementNo2 = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_GuarantorAgreementId + "(" + qGuarantorAgreementLine[1].Ordering.ToString() + "/" + CountLine + ")" : "",
                    Variable_GuarantorAgreementNo3 = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_GuarantorAgreementId + "(" + qGuarantorAgreementLine[2].Ordering.ToString() + "/" + CountLine + ")" : "",
                    Variable_GuarantorAgreementNo4 = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_GuarantorAgreementId + "(" + qGuarantorAgreementLine[3].Ordering.ToString() + "/" + CountLine + ")" : "",
                    Variable_GuarantorAgreementNo5 = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_GuarantorAgreementId + "(" + qGuarantorAgreementLine[4].Ordering.ToString() + "/" + CountLine + ")" : "",
                    Variable_GuarantorAgreementNo6 = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_GuarantorAgreementId + "(" + qGuarantorAgreementLine[5].Ordering.ToString() + "/" + CountLine + ")" : "",
                    Variable_GuarantorAgreementNo7 = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_GuarantorAgreementId + "(" + qGuarantorAgreementLine[6].Ordering.ToString() + "/" + CountLine + ")" : "",
                    Variable_GuarantorAgreementNo8 = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_GuarantorAgreementId + "(" + qGuarantorAgreementLine[7].Ordering.ToString() + "/" + CountLine + ")" : "",
                    Variable_ContractDate = qGuarantorAgreementTable.QGuarantorAgreementTable_MainAgreementDate != null ? sharedService.DateToWord(qGuarantorAgreementTable.QGuarantorAgreementTable_MainAgreementDate, TextConstants.TH, TextConstants.DMY) : "",
                };
                var Variable_GuarantorBirthday1 = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[0].DOB, TextConstants.TH, TextConstants.DMY) : "" : "";
                var Variable_GuarantorBirthday2 = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[1].DOB, TextConstants.TH, TextConstants.DMY) : "" : "";
                var Variable_GuarantorBirthday3 = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[2].DOB, TextConstants.TH, TextConstants.DMY) : "" : "";
                var Variable_GuarantorBirthday4 = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[3].DOB, TextConstants.TH, TextConstants.DMY) : "" : "";
                var Variable_GuarantorBirthday5 = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[4].DOB, TextConstants.TH, TextConstants.DMY) : "" : "";
                var Variable_GuarantorBirthday6 = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[5].DOB, TextConstants.TH, TextConstants.DMY) : "" : "";
                var Variable_GuarantorBirthday7 = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[6].DOB, TextConstants.TH, TextConstants.DMY) : "" : "";
                var Variable_GuarantorBirthday8 = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[7].DOB, TextConstants.TH, TextConstants.DMY) : "" : "";

                BookmaskDocumentGuarantorAgreement bookmaskDocumentGuarantorAgreement = new BookmaskDocumentGuarantorAgreement()
                {
                    Variable_GuarantorAgreementNo1 = bookmaskDocumentGuarantorAgreement_Variable != null ? bookmaskDocumentGuarantorAgreement_Variable.Variable_GuarantorAgreementNo1 : "",
                    Variable_GuarantorAgreementNo2 = bookmaskDocumentGuarantorAgreement_Variable != null ? bookmaskDocumentGuarantorAgreement_Variable.Variable_GuarantorAgreementNo2 : "",
                    Variable_GuarantorAgreementNo3 = bookmaskDocumentGuarantorAgreement_Variable != null ? bookmaskDocumentGuarantorAgreement_Variable.Variable_GuarantorAgreementNo3 : "",
                    Variable_GuarantorAgreementNo4 = bookmaskDocumentGuarantorAgreement_Variable != null ? bookmaskDocumentGuarantorAgreement_Variable.Variable_GuarantorAgreementNo4 : "",
                    Variable_GuarantorAgreementNo5 = bookmaskDocumentGuarantorAgreement_Variable != null ? bookmaskDocumentGuarantorAgreement_Variable.Variable_GuarantorAgreementNo5 : "",
                    Variable_GuarantorAgreementNo6 = bookmaskDocumentGuarantorAgreement_Variable != null ? bookmaskDocumentGuarantorAgreement_Variable.Variable_GuarantorAgreementNo6 : "",
                    Variable_GuarantorAgreementNo7 = bookmaskDocumentGuarantorAgreement_Variable != null ? bookmaskDocumentGuarantorAgreement_Variable.Variable_GuarantorAgreementNo7 : "",
                    Variable_GuarantorAgreementNo8 = bookmaskDocumentGuarantorAgreement_Variable != null ? bookmaskDocumentGuarantorAgreement_Variable.Variable_GuarantorAgreementNo8 : "",

                    QCompany_Name = qGuarantorAgreementTable != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_CompanyName : "",
                    QCompany_Address = qGuarantorAgreementTable != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_CompanyAddress : "",
                    QGuarantorAgreementTable_AgreementDate = qGuarantorAgreementTable != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_AgreementDate : "",
                    QGuarantorAgreementTable_AgreementYear = qGuarantorAgreementTable != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_AgreementYear : "",
                    QMainAgreementTable_MainAgreementId = qGuarantorAgreementTable != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_MainAgreementId : "",
                    Variable_ContractDate = qGuarantorAgreementTable != null ? bookmaskDocumentGuarantorAgreement_Variable.Variable_ContractDate : "",
                    QMainAgreementTable_ApprovedCreditLimit = qGuarantorAgreementTable != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_ApprovedCreditLimit : 0,
                    Variable_MainApprovedCreditLimit = NumberToWordsTH,
                    THApprovedCreditLimit = NumberToWordsTH,
                    QGuarantorAgreementTable_CompanyWitness1 = qGuarantorAgreementTable.QGuarantorAgreementTable_CompanyWitness1,
                    QGuarantorAgreementTable_CompanyWitness2 = qGuarantorAgreementTable.QGuarantorAgreementTable_CompanyWitness2,
                    QGuarantorAgreementTable_MainCustomerName = qGuarantorAgreementTable.QGuarantorAgreementTable_MainCustomerName,
                    QGuarantorAgreementTable_CustomerAddress = qGuarantorAgreementTable.QGuarantorAgreementTable_CustomerAddress,
                    QGuarantorAgreementLine1_Name = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].Name : "",
                    QGuarantorAgreementLine1_Age = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].Age : 0,
                    QGuarantorAgreementLine1_Race = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].Race : "",
                    QGuarantorAgreementLine1_Nationality = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].Nationality : "",
                    QGuarantorAgreementLine1_Address = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].Address : "",
                    QGuarantorAgreementLine1_TaxId = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].TaxId : "",
                    Variable_GuarantorBirthday1 = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[0].DOB, TextConstants.TH, TextConstants.DMY) : "" : "",
                    QGuarantorAgreementLine1_PassportId = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].PassportId : "",
                    QGuarantorAgreementLine1_WorkPermitId = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].WorkPermitId : "",
                    QGuarantorAgreementLine1_GuaranteeLand = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].GuaranteeLand : "",
                    QConsortiumTrans1_ConsName = qGuarantorAgreementTable != null ? qGuarantorAgreementTable.QGuarantorAgreementTable_Description : "",
                    QConsortiumTrans1_CustomerName = qConsortiumTrans1 != null ? qConsortiumTrans1.QConsortiumTrans1_GuarantorAgreement_CustomerName : "",
                    QConsortiumTrans1_Address = qConsortiumTrans1 != null ? qConsortiumTrans1.QConsortiumTrans1_GuarantorAgreement_Address : "",
                    QConsortiumTrans2_CustomerName = qConsortiumTrans2 != null ? qConsortiumTrans2.QConsortiumTrans2_GuarantorAgreement_CustomerName : "",
                    QConsortiumTrans2_Address = qConsortiumTrans2 != null ? qConsortiumTrans2.QConsortiumTrans2_GuarantorAgreement_Address : "",
                    QConsortiumTrans3_CustomerName = qconsortiumTrans3 != null ? qconsortiumTrans3.QConsortiumTrans3_GuarantorAgreement_CustomerName : "",
                    QConsortiumTrans3_Address = qconsortiumTrans3 != null ? qconsortiumTrans3.QConsortiumTrans3_GuarantorAgreement_Address : "",
                    QJointVentureTrans1_Name = qJointVentureTrans1 != null ? qJointVentureTrans1.QJointVentureTrans1_GuarantorAgreement_Name : "",
                    QJointVentureTrans1_Address = qJointVentureTrans1 != null ? qJointVentureTrans1.QJointVentureTrans1_GuarantorAgreement_Address : "",
                    QJointVentureTrans2_Name = qJointVentureTrans2 != null ? qJointVentureTrans2.QJointVentureTrans2_GuarantorAgreement_Name : "",
                    QJointVentureTrans2_Address = qJointVentureTrans2 != null ? qJointVentureTrans2.QJointVentureTrans2_GuarantorAgreement_Address : "",
                    QJointVentureTrans3_Name = qjointVentureTrans3 != null ? qjointVentureTrans3.QJointVentureTrans3_GuarantorAgreement_Name : "",
                    QJointVentureTrans3_Address = qjointVentureTrans3 != null ? qjointVentureTrans3.QJointVentureTrans3_GuarantorAgreement_Address : "",
                    //-----------------------------
                    QGuarantorAgreementLine2_Name = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].Name : "",
                    QGuarantorAgreementLine2_Age = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].Age : 0,
                    QGuarantorAgreementLine2_Race = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].Race : "",
                    QGuarantorAgreementLine2_Nationality = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].Nationality : "",
                    QGuarantorAgreementLine2_Address = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].Address : "",
                    QGuarantorAgreementLine2_TaxId = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].TaxId : "",
                    Variable_GuarantorBirthday2 = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[1].DOB, TextConstants.TH, TextConstants.DMY) : "" : "",
                    QGuarantorAgreementLine2_PassportId = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].PassportId : "",
                    QGuarantorAgreementLine2_WorkPermitId = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].WorkPermitId : "",
                    QGuarantorAgreementLine2_GuaranteeLand = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].GuaranteeLand : "",
                    //--------------------------------
                    QGuarantorAgreementLine3_Name = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].Name : "",
                    QGuarantorAgreementLine3_Age = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].Age : 0,
                    QGuarantorAgreementLine3_Race = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].Race : "",
                    QGuarantorAgreementLine3_Nationality = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].Nationality : "",
                    QGuarantorAgreementLine3_Address = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].Address : "",
                    QGuarantorAgreementLine3_TaxId = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].TaxId : "",
                    Variable_GuarantorBirthday3 = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[2].DOB, TextConstants.TH, TextConstants.DMY) : "" : "",
                    QGuarantorAgreementLine3_PassportId = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].PassportId : "",
                    QGuarantorAgreementLine3_WorkPermitId = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].WorkPermitId : "",
                    QGuarantorAgreementLine3_GuaranteeLand = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].GuaranteeLand : "",
                    //--------------------------------
                    QGuarantorAgreementLine4_Name = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].Name : "",
                    QGuarantorAgreementLine4_Age = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].Age : 0,
                    QGuarantorAgreementLine4_Race = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].Race : "",
                    QGuarantorAgreementLine4_Nationality = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].Nationality : "",
                    QGuarantorAgreementLine4_Address = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].Address : "",
                    QGuarantorAgreementLine4_TaxId = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].TaxId : "",
                    Variable_GuarantorBirthday4 = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[3].DOB, TextConstants.TH, TextConstants.DMY) : "" : "",
                    QGuarantorAgreementLine4_PassportId = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].PassportId : "",
                    QGuarantorAgreementLine4_WorkPermitId = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].WorkPermitId : "",
                    QGuarantorAgreementLine4_GuaranteeLand = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].GuaranteeLand : "",
                    //--------------------------------
                    QGuarantorAgreementLine5_Name = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].Name : "",
                    QGuarantorAgreementLine5_Age = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].Age : 0,
                    QGuarantorAgreementLine5_Race = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].Race : "",
                    QGuarantorAgreementLine5_Nationality = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].Nationality : "",
                    QGuarantorAgreementLine5_Address = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].Address : "",
                    QGuarantorAgreementLine5_TaxId = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].TaxId : "",
                    Variable_GuarantorBirthday5 = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[4].DOB, TextConstants.TH, TextConstants.DMY) : "" : "",
                    QGuarantorAgreementLine5_PassportId = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].PassportId : "",
                    QGuarantorAgreementLine5_WorkPermitId = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].WorkPermitId : "",
                    QGuarantorAgreementLine5_GuaranteeLand = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].GuaranteeLand : "",
                    //--------------------------------
                    QGuarantorAgreementLine6_Name = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].Name : "",
                    QGuarantorAgreementLine6_Age = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].Age : 0,
                    QGuarantorAgreementLine6_Race = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].Race : "",
                    QGuarantorAgreementLine6_Nationality = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].Nationality : "",
                    QGuarantorAgreementLine6_Address = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].Address : "",
                    QGuarantorAgreementLine6_TaxId = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].TaxId : "",
                    Variable_GuarantorBirthday6 = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[5].DOB, TextConstants.TH, TextConstants.DMY) : "" : "",
                    QGuarantorAgreementLine6_PassportId = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].PassportId : "",
                    QGuarantorAgreementLine6_WorkPermitId = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].WorkPermitId : "",
                    QGuarantorAgreementLine6_GuaranteeLand = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].GuaranteeLand : "",
                    //--------------------------------
                    QGuarantorAgreementLine7_Name = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].Name : "",
                    QGuarantorAgreementLine7_Age = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].Age : 0,
                    QGuarantorAgreementLine7_Race = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].Race : "",
                    QGuarantorAgreementLine7_Nationality = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].Nationality : "",
                    QGuarantorAgreementLine7_Address = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].Address : "",
                    QGuarantorAgreementLine7_TaxId = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].TaxId : "",
                    Variable_GuarantorBirthday7 = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[6].DOB, TextConstants.TH, TextConstants.DMY) : "" : "",
                    QGuarantorAgreementLine7_PassportId = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].PassportId : "",
                    QGuarantorAgreementLine7_WorkPermitId = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].WorkPermitId : "",
                    QGuarantorAgreementLine7_GuaranteeLand = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].GuaranteeLand : "",
                    //--------------------------------
                    QGuarantorAgreementLine8_Name = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].Name : "",
                    QGuarantorAgreementLine8_Age = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].Age : 0,
                    QGuarantorAgreementLine8_Race = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].Race : "",
                    QGuarantorAgreementLine8_Nationality = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].Nationality : "",
                    QGuarantorAgreementLine8_Address = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].Address : "",
                    QGuarantorAgreementLine8_TaxId = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].TaxId : "",
                    Variable_GuarantorBirthday8 = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].DOB != null ? sharedService.DateToWord((DateTime)qGuarantorAgreementLine[7].DOB, TextConstants.TH, TextConstants.DMY) : "" : "",
                    QGuarantorAgreementLine8_PassportId = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].PassportId : "",
                    QGuarantorAgreementLine8_WorkPermitId = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].WorkPermitId : "",
                    QGuarantorAgreementLine8_GuaranteeLand = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].GuaranteeLand : "",

                    QGuarantorAgreementLine1_OperatedBy = qGuarantorAgreementLine[0] != null ? qGuarantorAgreementLine[0].OperatedBy : "",
                    QGuarantorAgreementLine2_OperatedBy = qGuarantorAgreementLine[1] != null ? qGuarantorAgreementLine[1].OperatedBy : "",
                    QGuarantorAgreementLine3_OperatedBy = qGuarantorAgreementLine[2] != null ? qGuarantorAgreementLine[2].OperatedBy : "",
                    QGuarantorAgreementLine4_OperatedBy = qGuarantorAgreementLine[3] != null ? qGuarantorAgreementLine[3].OperatedBy : "",
                    QGuarantorAgreementLine5_OperatedBy = qGuarantorAgreementLine[4] != null ? qGuarantorAgreementLine[4].OperatedBy : "",
                    QGuarantorAgreementLine6_OperatedBy = qGuarantorAgreementLine[5] != null ? qGuarantorAgreementLine[5].OperatedBy : "",
                    QGuarantorAgreementLine7_OperatedBy = qGuarantorAgreementLine[6] != null ? qGuarantorAgreementLine[6].OperatedBy : "",
                    QGuarantorAgreementLine8_OperatedBy = qGuarantorAgreementLine[7] != null ? qGuarantorAgreementLine[7].OperatedBy : "",
                };


                return bookmaskDocumentGuarantorAgreement;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }
}

