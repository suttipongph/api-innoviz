using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IIntercompanyInvoiceAdjustmentRepo
	{
		#region DropDown
		IEnumerable<SelectItem<IntercompanyInvoiceAdjustmentItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<IntercompanyInvoiceAdjustmentListView> GetListvw(SearchParameter search);
		IntercompanyInvoiceAdjustmentItemView GetByIdvw(Guid id);
		IntercompanyInvoiceAdjustment Find(params object[] keyValues);
		IntercompanyInvoiceAdjustment GetIntercompanyInvoiceAdjustmentByIdNoTracking(Guid guid);
		IntercompanyInvoiceAdjustment CreateIntercompanyInvoiceAdjustment(IntercompanyInvoiceAdjustment intercompanyInvoiceAdjustment);
		void CreateIntercompanyInvoiceAdjustmentVoid(IntercompanyInvoiceAdjustment intercompanyInvoiceAdjustment);
		IntercompanyInvoiceAdjustment UpdateIntercompanyInvoiceAdjustment(IntercompanyInvoiceAdjustment dbIntercompanyInvoiceAdjustment, IntercompanyInvoiceAdjustment inputIntercompanyInvoiceAdjustment, List<string> skipUpdateFields = null);
		void UpdateIntercompanyInvoiceAdjustmentVoid(IntercompanyInvoiceAdjustment dbIntercompanyInvoiceAdjustment, IntercompanyInvoiceAdjustment inputIntercompanyInvoiceAdjustment, List<string> skipUpdateFields = null);
		void Remove(IntercompanyInvoiceAdjustment item);
		IntercompanyInvoiceAdjustment GetIntercompanyInvoiceAdjustmentByIdNoTrackingByAccessLevel(Guid guid);
	}
	public class IntercompanyInvoiceAdjustmentRepo : CompanyBaseRepository<IntercompanyInvoiceAdjustment>, IIntercompanyInvoiceAdjustmentRepo
	{
		public IntercompanyInvoiceAdjustmentRepo(SmartAppDbContext context) : base(context) { }
		public IntercompanyInvoiceAdjustmentRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public IntercompanyInvoiceAdjustment GetIntercompanyInvoiceAdjustmentByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.IntercompanyInvoiceAdjustmentGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<IntercompanyInvoiceAdjustmentItemViewMap> GetDropDownQuery()
		{
			return (from intercompanyInvoiceAdjustment in Entity
					select new IntercompanyInvoiceAdjustmentItemViewMap
					{
						CompanyGUID = intercompanyInvoiceAdjustment.CompanyGUID,
						Owner = intercompanyInvoiceAdjustment.Owner,
						OwnerBusinessUnitGUID = intercompanyInvoiceAdjustment.OwnerBusinessUnitGUID,
						IntercompanyInvoiceAdjustmentGUID = intercompanyInvoiceAdjustment.IntercompanyInvoiceAdjustmentGUID
					});
		}
		public IEnumerable<SelectItem<IntercompanyInvoiceAdjustmentItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<IntercompanyInvoiceAdjustment>(search, SysParm.CompanyGUID);
				var intercompanyInvoiceAdjustment = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<IntercompanyInvoiceAdjustmentItemViewMap, IntercompanyInvoiceAdjustmentItemView>().ToDropDownItem(search.ExcludeRowData);


				return intercompanyInvoiceAdjustment;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<IntercompanyInvoiceAdjustmentListViewMap> GetListQuery()
		{
			return (from intercompanyInvoiceAdjustment in Entity
					join documentReason in db.Set<DocumentReason>()
					on intercompanyInvoiceAdjustment.DocumentReasonGUID equals documentReason.DocumentReasonGUID into ljdocumentReason
					from documentReason in ljdocumentReason.DefaultIfEmpty()
					select new IntercompanyInvoiceAdjustmentListViewMap
				{
						CompanyGUID = intercompanyInvoiceAdjustment.CompanyGUID,
						Owner = intercompanyInvoiceAdjustment.Owner,
						OwnerBusinessUnitGUID = intercompanyInvoiceAdjustment.OwnerBusinessUnitGUID,
						IntercompanyInvoiceAdjustmentGUID = intercompanyInvoiceAdjustment.IntercompanyInvoiceAdjustmentGUID,
						OriginalAmount = intercompanyInvoiceAdjustment.OriginalAmount,
						Adjustment = intercompanyInvoiceAdjustment.Adjustment,
						DocumentReasonGUID = intercompanyInvoiceAdjustment.DocumentReasonGUID,
						DocumentReason_Values = (documentReason != null) ? SmartAppUtil.GetDropDownLabel(documentReason.ReasonId, documentReason.Description) : null,
						CreatedDateTime = intercompanyInvoiceAdjustment.CreatedDateTime,
						IntercompanyInvoiceTableGUID = intercompanyInvoiceAdjustment.IntercompanyInvoiceTableGUID,
						DocumentReason_DocumentReasonId = documentReason.ReasonId
					});
		}
		public SearchResult<IntercompanyInvoiceAdjustmentListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<IntercompanyInvoiceAdjustmentListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<IntercompanyInvoiceAdjustment>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<IntercompanyInvoiceAdjustmentListViewMap, IntercompanyInvoiceAdjustmentListView>();
				result = list.SetSearchResult<IntercompanyInvoiceAdjustmentListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<IntercompanyInvoiceAdjustmentItemViewMap> GetItemQuery()
		{
			return (from intercompanyInvoiceAdjustment in Entity
					join company in db.Set<Company>()
					on intercompanyInvoiceAdjustment.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on intercompanyInvoiceAdjustment.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljIntercompanyInvoiceAdjustmentOwnerBU
					from ownerBU in ljIntercompanyInvoiceAdjustmentOwnerBU.DefaultIfEmpty()
					join intercompanyInvoiceTable in db.Set<IntercompanyInvoiceTable>()
					on intercompanyInvoiceAdjustment.IntercompanyInvoiceTableGUID equals intercompanyInvoiceTable.IntercompanyInvoiceTableGUID into ljintercompanyInvoiceTable
					from intercompanyInvoiceTable in ljintercompanyInvoiceTable.DefaultIfEmpty()

					join documentReason in db.Set<DocumentReason>()
					on intercompanyInvoiceAdjustment.DocumentReasonGUID equals documentReason.DocumentReasonGUID into ljdocumentReason
					from documentReason in ljdocumentReason.DefaultIfEmpty()
					select new IntercompanyInvoiceAdjustmentItemViewMap
					{
						CompanyGUID = intercompanyInvoiceAdjustment.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = intercompanyInvoiceAdjustment.Owner,
						OwnerBusinessUnitGUID = intercompanyInvoiceAdjustment.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = intercompanyInvoiceAdjustment.CreatedBy,
						CreatedDateTime = intercompanyInvoiceAdjustment.CreatedDateTime,
						ModifiedBy = intercompanyInvoiceAdjustment.ModifiedBy,
						ModifiedDateTime = intercompanyInvoiceAdjustment.ModifiedDateTime,
						IntercompanyInvoiceAdjustmentGUID = intercompanyInvoiceAdjustment.IntercompanyInvoiceAdjustmentGUID,
						Adjustment = intercompanyInvoiceAdjustment.Adjustment,
						DocumentReasonGUID = intercompanyInvoiceAdjustment.DocumentReasonGUID,
						IntercompanyInvoiceTableGUID = intercompanyInvoiceAdjustment.IntercompanyInvoiceTableGUID,
						OriginalAmount = intercompanyInvoiceAdjustment.OriginalAmount,
						DocumentReason_Values = (documentReason != null) ? SmartAppUtil.GetDropDownLabel(documentReason.ReasonId, documentReason.Description) : null,
						IntercompanyInvoice_Values = SmartAppUtil.GetDropDownLabel(intercompanyInvoiceTable.IntercompanyInvoiceId, intercompanyInvoiceTable.IssuedDate.ToString()),
					
						RowVersion = intercompanyInvoiceAdjustment.RowVersion,
					});
		}
		public IntercompanyInvoiceAdjustmentItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<IntercompanyInvoiceAdjustmentItemViewMap, IntercompanyInvoiceAdjustmentItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public IntercompanyInvoiceAdjustment CreateIntercompanyInvoiceAdjustment(IntercompanyInvoiceAdjustment intercompanyInvoiceAdjustment)
		{
			try
			{
				intercompanyInvoiceAdjustment.IntercompanyInvoiceAdjustmentGUID = Guid.NewGuid();
				base.Add(intercompanyInvoiceAdjustment);
				return intercompanyInvoiceAdjustment;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateIntercompanyInvoiceAdjustmentVoid(IntercompanyInvoiceAdjustment intercompanyInvoiceAdjustment)
		{
			try
			{
				CreateIntercompanyInvoiceAdjustment(intercompanyInvoiceAdjustment);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IntercompanyInvoiceAdjustment UpdateIntercompanyInvoiceAdjustment(IntercompanyInvoiceAdjustment dbIntercompanyInvoiceAdjustment, IntercompanyInvoiceAdjustment inputIntercompanyInvoiceAdjustment, List<string> skipUpdateFields = null)
		{
			try
			{
				dbIntercompanyInvoiceAdjustment = dbIntercompanyInvoiceAdjustment.MapUpdateValues<IntercompanyInvoiceAdjustment>(inputIntercompanyInvoiceAdjustment);
				base.Update(dbIntercompanyInvoiceAdjustment);
				return dbIntercompanyInvoiceAdjustment;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateIntercompanyInvoiceAdjustmentVoid(IntercompanyInvoiceAdjustment dbIntercompanyInvoiceAdjustment, IntercompanyInvoiceAdjustment inputIntercompanyInvoiceAdjustment, List<string> skipUpdateFields = null)
		{
			try
			{
				dbIntercompanyInvoiceAdjustment = dbIntercompanyInvoiceAdjustment.MapUpdateValues<IntercompanyInvoiceAdjustment>(inputIntercompanyInvoiceAdjustment, skipUpdateFields);
				base.Update(dbIntercompanyInvoiceAdjustment);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public IntercompanyInvoiceAdjustment GetIntercompanyInvoiceAdjustmentByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.IntercompanyInvoiceAdjustmentGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

