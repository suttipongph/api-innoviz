using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IApplicationTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<ApplicationTableItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<ApplicationTableListView> GetListvw(SearchParameter search);
		ApplicationTableItemView GetByIdvw(Guid id);
		ApplicationTable Find(params object[] keyValues);
		ApplicationTable GetApplicationTableByIdNoTracking(Guid guid);
		ApplicationTable CreateApplicationTable(ApplicationTable applicationTable);
		void CreateApplicationTableVoid(ApplicationTable applicationTable);
		ApplicationTable UpdateApplicationTable(ApplicationTable dbApplicationTable, ApplicationTable inputApplicationTable, List<string> skipUpdateFields = null);
		void UpdateApplicationTableVoid(ApplicationTable dbApplicationTable, ApplicationTable inputApplicationTable, List<string> skipUpdateFields = null);
		void Remove(ApplicationTable item);
	}
	public class ApplicationTableRepo : CompanyBaseRepository<ApplicationTable>, IApplicationTableRepo
	{
		public ApplicationTableRepo(SmartAppDbContext context) : base(context) { }
		public ApplicationTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ApplicationTable GetApplicationTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ApplicationTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<ApplicationTableItemViewMap> GetDropDownQuery()
		{
			return (from applicationTable in Entity
					join documentStatus in db.Set<DocumentStatus>() on applicationTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
					select new ApplicationTableItemViewMap
					{
						CompanyGUID = applicationTable.CompanyGUID,
						Owner = applicationTable.Owner,
						OwnerBusinessUnitGUID = applicationTable.OwnerBusinessUnitGUID,
						ApplicationTableGUID = applicationTable.ApplicationTableGUID,
						CustomerTableGUID = applicationTable.CustomerTableGUID,
						DocumentStatusGUID = applicationTable.DocumentStatusGUID,
						ApplicationId = applicationTable.ApplicationId,
						DocumentStatus_StatusId = documentStatus.StatusId,
					});
		}
		public IEnumerable<SelectItem<ApplicationTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<ApplicationTable>(search, SysParm.CompanyGUID);
				var applicationTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<ApplicationTableItemViewMap, ApplicationTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return applicationTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<ApplicationTableListViewMap> GetListQuery()
		{
			return (from applicationTable in Entity
				select new ApplicationTableListViewMap
				{
						CompanyGUID = applicationTable.CompanyGUID,
						Owner = applicationTable.Owner,
						OwnerBusinessUnitGUID = applicationTable.OwnerBusinessUnitGUID,
						ApplicationTableGUID = applicationTable.ApplicationTableGUID,
				});
		}
		public SearchResult<ApplicationTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<ApplicationTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<ApplicationTable>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<ApplicationTableListViewMap, ApplicationTableListView>();
				result = list.SetSearchResult<ApplicationTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<ApplicationTableItemViewMap> GetItemQuery()
		{
			return (from applicationTable in Entity
					join company in db.Set<Company>()
					on applicationTable.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on applicationTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljApplicationTableOwnerBU
					from ownerBU in ljApplicationTableOwnerBU.DefaultIfEmpty()
					select new ApplicationTableItemViewMap
					{
						CompanyGUID = applicationTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = applicationTable.Owner,
						OwnerBusinessUnitGUID = applicationTable.OwnerBusinessUnitGUID,
						CreatedBy = applicationTable.CreatedBy,
						CreatedDateTime = applicationTable.CreatedDateTime,
						ModifiedBy = applicationTable.ModifiedBy,
						ModifiedDateTime = applicationTable.ModifiedDateTime,
						ApplicationTableGUID = applicationTable.ApplicationTableGUID,
						AgreementTypeGUID = applicationTable.AgreementTypeGUID,
						ApplicationDate = applicationTable.ApplicationDate,
						ApplicationId = applicationTable.ApplicationId,
						BillingAddressGUID = applicationTable.BillingAddressGUID,
						CalculationType = applicationTable.CalculationType,
						CreditResultGUID = applicationTable.CreditResultGUID,
						CreditResultNotes = applicationTable.CreditResultNotes,
						CurrencyGUID = applicationTable.CurrencyGUID,
						CustomerTableGUID = applicationTable.CustomerTableGUID,
						DeliveryAddressGUID = applicationTable.DeliveryAddressGUID,
						Dimension1GUID = applicationTable.Dimension1GUID,
						Dimension2GUID = applicationTable.Dimension2GUID,
						Dimension3GUID = applicationTable.Dimension3GUID,
						Dimension4GUID = applicationTable.Dimension4GUID,
						Dimension5GUID = applicationTable.Dimension5GUID,
						DocumentReasonGUID = applicationTable.DocumentReasonGUID,
						DocumentStatusGUID = applicationTable.DocumentStatusGUID,
						ExpectedExecuteDate = applicationTable.ExpectedExecuteDate,
						ExpirationDate = applicationTable.ExpirationDate,
						FlexInfo1 = applicationTable.FlexInfo1,
						FlexInfo2 = applicationTable.FlexInfo2,
						FlexSetupGUID1 = applicationTable.FlexSetupGUID1,
						FlexSetupGUID2 = applicationTable.FlexSetupGUID2,
						FollowupDate = applicationTable.FollowupDate,
						IntroducedByGUID = applicationTable.IntroducedByGUID,
						InvoiceAddressGUID = applicationTable.InvoiceAddressGUID,
						LanguageGUID = applicationTable.LanguageGUID,
						LeaseSubTypeGUID = applicationTable.LeaseSubTypeGUID,
						LeaseTypeGUID = applicationTable.LeaseTypeGUID,
						MailingInvoiceAddressGUID = applicationTable.MailingInvoiceAddressGUID,
						MailingReceiptAddressGUID = applicationTable.MailingReceiptAddressGUID,
						NCBAccountStatusGUID = applicationTable.NCBAccountStatusGUID,
						NCBNotes = applicationTable.NCBNotes,
						OriginalAgreementGUID = applicationTable.OriginalAgreementGUID,
						PaymentFrequencyGUID = applicationTable.PaymentFrequencyGUID,
						ProcessInstanceId = applicationTable.ProcessInstanceId,
						PropertyAddressGUID = applicationTable.PropertyAddressGUID,
						ProspectTableGUID = applicationTable.ProspectTableGUID,
						ReasonRemark = applicationTable.ReasonRemark,
						ReceiptAddressGUID = applicationTable.ReceiptAddressGUID,
						RefAgreementExtension = applicationTable.RefAgreementExtension,
						RegisterAddressGUID = applicationTable.RegisterAddressGUID,
						Remark = applicationTable.Remark,
						ResponsibleBy = applicationTable.ResponsibleBy,
						SalesLeaseBack = applicationTable.SalesLeaseBack,
						WorkingAddressGUID = applicationTable.WorkingAddressGUID,
					
						RowVersion = applicationTable.RowVersion,
					});
		}
		public ApplicationTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.ToMap<ApplicationTableItemViewMap, ApplicationTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public ApplicationTable CreateApplicationTable(ApplicationTable applicationTable)
		{
			try
			{
				applicationTable.ApplicationTableGUID = Guid.NewGuid();
				base.Add(applicationTable);
				return applicationTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateApplicationTableVoid(ApplicationTable applicationTable)
		{
			try
			{
				CreateApplicationTable(applicationTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ApplicationTable UpdateApplicationTable(ApplicationTable dbApplicationTable, ApplicationTable inputApplicationTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbApplicationTable = dbApplicationTable.MapUpdateValues<ApplicationTable>(inputApplicationTable);
				base.Update(dbApplicationTable);
				return dbApplicationTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateApplicationTableVoid(ApplicationTable dbApplicationTable, ApplicationTable inputApplicationTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbApplicationTable = dbApplicationTable.MapUpdateValues<ApplicationTable>(inputApplicationTable, skipUpdateFields);
				base.Update(dbApplicationTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

