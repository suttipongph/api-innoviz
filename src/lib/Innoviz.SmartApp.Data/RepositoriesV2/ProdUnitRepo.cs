using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IProdUnitRepo
	{
		#region DropDown
		IEnumerable<SelectItem<ProdUnitItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<ProdUnitListView> GetListvw(SearchParameter search);
		ProdUnitItemView GetByIdvw(Guid id);
		ProdUnit Find(params object[] keyValues);
		ProdUnit GetProdUnitByIdNoTracking(Guid guid);
		ProdUnit CreateProdUnit(ProdUnit prodUnit);
		void CreateProdUnitVoid(ProdUnit prodUnit);
		ProdUnit UpdateProdUnit(ProdUnit dbProdUnit, ProdUnit inputProdUnit, List<string> skipUpdateFields = null);
		void UpdateProdUnitVoid(ProdUnit dbProdUnit, ProdUnit inputProdUnit, List<string> skipUpdateFields = null);
		void Remove(ProdUnit item);
		ProdUnit GetProdUnitByIdNoTrackingByAccessLevel(Guid guid);
	}
	public class ProdUnitRepo : CompanyBaseRepository<ProdUnit>, IProdUnitRepo
	{
		public ProdUnitRepo(SmartAppDbContext context) : base(context) { }
		public ProdUnitRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ProdUnit GetProdUnitByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ProdUnitGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<ProdUnitItemViewMap> GetDropDownQuery()
		{
			return (from prodUnit in Entity
					select new ProdUnitItemViewMap
					{
						CompanyGUID = prodUnit.CompanyGUID,
						Owner = prodUnit.Owner,
						OwnerBusinessUnitGUID = prodUnit.OwnerBusinessUnitGUID,
						ProdUnitGUID = prodUnit.ProdUnitGUID
					});
		}
		public IEnumerable<SelectItem<ProdUnitItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<ProdUnit>(search, SysParm.CompanyGUID);
				var prodUnit = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<ProdUnitItemViewMap, ProdUnitItemView>().ToDropDownItem(search.ExcludeRowData);


				return prodUnit;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<ProdUnitListViewMap> GetListQuery()
		{
			return (from prodUnit in Entity
				select new ProdUnitListViewMap
				{
						CompanyGUID = prodUnit.CompanyGUID,
						Owner = prodUnit.Owner,
						OwnerBusinessUnitGUID = prodUnit.OwnerBusinessUnitGUID,
						ProdUnitGUID = prodUnit.ProdUnitGUID,
						UnitId = prodUnit.UnitId,
						Description = prodUnit.Description

						
				});
		}
		public SearchResult<ProdUnitListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<ProdUnitListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<ProdUnit>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<ProdUnitListViewMap, ProdUnitListView>();
				result = list.SetSearchResult<ProdUnitListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<ProdUnitItemViewMap> GetItemQuery()
		{
			return (from prodUnit in Entity
					join company in db.Set<Company>()
					on prodUnit.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on prodUnit.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljProdUnitOwnerBU
					from ownerBU in ljProdUnitOwnerBU.DefaultIfEmpty()
					select new ProdUnitItemViewMap
					{
						CompanyGUID = prodUnit.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = prodUnit.Owner,
						OwnerBusinessUnitGUID = prodUnit.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = prodUnit.CreatedBy,
						CreatedDateTime = prodUnit.CreatedDateTime,
						ModifiedBy = prodUnit.ModifiedBy,
						ModifiedDateTime = prodUnit.ModifiedDateTime,
						ProdUnitGUID = prodUnit.ProdUnitGUID,
						Description = prodUnit.Description,
						UnitId = prodUnit.UnitId,
					
						RowVersion = prodUnit.RowVersion,
					});
		}
		public ProdUnitItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<ProdUnitItemViewMap, ProdUnitItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public ProdUnit CreateProdUnit(ProdUnit prodUnit)
		{
			try
			{
				prodUnit.ProdUnitGUID = Guid.NewGuid();
				base.Add(prodUnit);
				return prodUnit;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateProdUnitVoid(ProdUnit prodUnit)
		{
			try
			{
				CreateProdUnit(prodUnit);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ProdUnit UpdateProdUnit(ProdUnit dbProdUnit, ProdUnit inputProdUnit, List<string> skipUpdateFields = null)
		{
			try
			{
				dbProdUnit = dbProdUnit.MapUpdateValues<ProdUnit>(inputProdUnit);
				base.Update(dbProdUnit);
				return dbProdUnit;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateProdUnitVoid(ProdUnit dbProdUnit, ProdUnit inputProdUnit, List<string> skipUpdateFields = null)
		{
			try
			{
				dbProdUnit = dbProdUnit.MapUpdateValues<ProdUnit>(inputProdUnit, skipUpdateFields);
				base.Update(dbProdUnit);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public ProdUnit GetProdUnitByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ProdUnitGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

