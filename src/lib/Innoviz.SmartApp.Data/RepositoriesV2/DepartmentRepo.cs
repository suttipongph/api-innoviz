using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IDepartmentRepo
	{
		#region DropDown
		IEnumerable<SelectItem<DepartmentItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<DepartmentListView> GetListvw(SearchParameter search);
		DepartmentItemView GetByIdvw(Guid id);
		Department Find(params object[] keyValues);
		Department GetDepartmentByIdNoTracking(Guid guid);
		Department CreateDepartment(Department department);
		void CreateDepartmentVoid(Department department);
		Department UpdateDepartment(Department dbDepartment, Department inputDepartment, List<string> skipUpdateFields = null);
		void UpdateDepartmentVoid(Department dbDepartment, Department inputDepartment, List<string> skipUpdateFields = null);
		void Remove(Department item);
	}
	public class DepartmentRepo : CompanyBaseRepository<Department>, IDepartmentRepo
	{
		public DepartmentRepo(SmartAppDbContext context) : base(context) { }
		public DepartmentRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public Department GetDepartmentByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.DepartmentGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<DepartmentItemViewMap> GetDropDownQuery()
		{
			return (from department in Entity
					select new DepartmentItemViewMap
					{
						CompanyGUID = department.CompanyGUID,
						Owner = department.Owner,
						OwnerBusinessUnitGUID = department.OwnerBusinessUnitGUID,
						DepartmentGUID = department.DepartmentGUID,
						DepartmentId = department.DepartmentId,
						Description = department.Description
					});
		}
		public IEnumerable<SelectItem<DepartmentItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<Department>(search, SysParm.CompanyGUID);
				var department = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<DepartmentItemViewMap, DepartmentItemView>().ToDropDownItem(search.ExcludeRowData);


				return department;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<DepartmentListViewMap> GetListQuery()
		{
			return (from department in Entity
				select new DepartmentListViewMap
				{
						CompanyGUID = department.CompanyGUID,
						Owner = department.Owner,
						OwnerBusinessUnitGUID = department.OwnerBusinessUnitGUID,
						DepartmentGUID = department.DepartmentGUID,
						DepartmentId = department.DepartmentId,
						Description = department.Description,
				});
		}
		public SearchResult<DepartmentListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<DepartmentListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<Department>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<DepartmentListViewMap, DepartmentListView>();
				result = list.SetSearchResult<DepartmentListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<DepartmentItemViewMap> GetItemQuery()
		{
			return (from department in Entity
					join ownerBU in db.Set<BusinessUnit>()
					on department.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljDepartmentOwnerBU
					from ownerBU in ljDepartmentOwnerBU.DefaultIfEmpty()
					join company in db.Set<Company>() on department.CompanyGUID equals company.CompanyGUID
					select new DepartmentItemViewMap
					{
						CompanyGUID = department.CompanyGUID,
						Owner = department.Owner,
						OwnerBusinessUnitGUID = department.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CompanyId = company.CompanyId,
						CreatedBy = department.CreatedBy,
						CreatedDateTime = department.CreatedDateTime,
						ModifiedBy = department.ModifiedBy,
						ModifiedDateTime = department.ModifiedDateTime,
						DepartmentGUID = department.DepartmentGUID,
						DepartmentId = department.DepartmentId,
						Description = department.Description,
					
						RowVersion = department.RowVersion,
					});
		}
		public DepartmentItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<DepartmentItemViewMap, DepartmentItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public Department CreateDepartment(Department department)
		{
			try
			{
				department.DepartmentGUID = Guid.NewGuid();
				base.Add(department);
				return department;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateDepartmentVoid(Department department)
		{
			try
			{
				CreateDepartment(department);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public Department UpdateDepartment(Department dbDepartment, Department inputDepartment, List<string> skipUpdateFields = null)
		{
			try
			{
				dbDepartment = dbDepartment.MapUpdateValues<Department>(inputDepartment);
				base.Update(dbDepartment);
				return dbDepartment;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateDepartmentVoid(Department dbDepartment, Department inputDepartment, List<string> skipUpdateFields = null)
		{
			try
			{
				dbDepartment = dbDepartment.MapUpdateValues<Department>(inputDepartment, skipUpdateFields);
				base.Update(dbDepartment);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

