using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface IBusinessCollateralAgmTableRepo
    {
        #region DropDown
        IEnumerable<SelectItem<BusinessCollateralAgmTableItemView>> GetDropDownItem(SearchParameter search);
        #endregion DropDown
        SearchResult<BusinessCollateralAgmTableListView> GetListvw(SearchParameter search);
        BusinessCollateralAgmTableItemView GetByIdvw(Guid id);
        BusinessCollateralAgmTable Find(params object[] keyValues);
        BusinessCollateralAgmTable GetBusinessCollateralAgmTableByIdNoTracking(Guid guid);
        BusinessCollateralAgmTable GetBusinessCollateralAgmTableByBusinessCollateralAgmId(string id);
        BusinessCollateralAgmTable CreateBusinessCollateralAgmTable(BusinessCollateralAgmTable businessCollateralAgmTable);
        void CreateBusinessCollateralAgmTableVoid(BusinessCollateralAgmTable businessCollateralAgmTable);
        BusinessCollateralAgmTable UpdateBusinessCollateralAgmTable(BusinessCollateralAgmTable dbBusinessCollateralAgmTable, BusinessCollateralAgmTable inputBusinessCollateralAgmTable, List<string> skipUpdateFields = null);
        void UpdateBusinessCollateralAgmTableVoid(BusinessCollateralAgmTable dbBusinessCollateralAgmTable, BusinessCollateralAgmTable inputBusinessCollateralAgmTable, List<string> skipUpdateFields = null);
        void Remove(BusinessCollateralAgmTable item);
        BusinessCollateralAgmTable GetBusinessCollateralAgmTableByIdNoTrackingByAccessLevel(Guid guid);
        AgreementTableInfo GenerateAgreementInfo(BusinessCollateralAgmTable input);
        List<BusinessCollateralAgmTable> GetListByRefBusinessCollateralAgmTableAndAgreementDocType(Guid guid, AgreementDocType agreementDocType);
        QBusinessCollateralAgreement GetBookmarkDocumentBusinessCollateral(Guid refGUID);
        BookmarkBusinessCollateralAgmTable GetBookmarkDocumentBusinessCollateralTable(Guid refGUID);
        BookmarkBusinessCollateralAgreementExpenceDetail GetBookmarkDocumentBusinessCollateralAgreementExpenceDetail(Guid refGUID);
        BookmarkCancelBusinessCollateralAgreement GetBookmarkDocumentCancelBusinessCollateralAgreement(Guid refGUID, Guid bookmarkDocumentTransGUID);
        void ValidateAdd(IEnumerable<BusinessCollateralAgmTable> items);
        IEnumerable<BusinessCollateralAgmTableItemViewMap> GetBusinessCollateralAgmTableNoTracking();
        List<BusinessCollateralAgmTable> GetBusinessCollateralAgmTableByRefBusinessCollateralAgmTableGUID(Guid guid);
        bool IsBusinessAgmLessthanSignedList(Guid businessCollateralAgmTableGUID , int agreementDoctype, int statusId);
        bool IsDuplicateBusinessCollateralAgmId(Guid guid, string id);
        void ValidateUpdate(IEnumerable<BusinessCollateralAgmTable> items);
        IEnumerable<BusinessCollateralAgmTable> GetBusinessCollateralAgmTableByIdNoTracking(IEnumerable<Guid> guids);
    }
    public class BusinessCollateralAgmTableRepo : CompanyBaseRepository<BusinessCollateralAgmTable>, IBusinessCollateralAgmTableRepo
    {
        public BusinessCollateralAgmTableRepo(SmartAppDbContext context) : base(context) { }
        public BusinessCollateralAgmTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public BusinessCollateralAgmTable GetBusinessCollateralAgmTableByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.BusinessCollateralAgmTableGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BookmarkCancelBusinessCollateralAgreement GetBookmarkDocumentCancelBusinessCollateralAgreement(Guid refGUID, Guid bookmarkDocumentTransGUID)
        {

            try
            {
                CBCANoticeOfCancellation cBCANoticeOfCancellation = (from businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
                                                                     join documentReson in db.Set<DocumentReason>()
                                                                     on businessCollateralAgmTable.DocumentReasonGUID equals documentReson.DocumentReasonGUID into ljdocumentReson
                                                                     from documentReson in ljdocumentReson.DefaultIfEmpty()
                                                                     join addressTrans in db.Set<AddressTrans>()
                                                                     on businessCollateralAgmTable.CustomerTableGUID equals addressTrans.RefGUID into ljaddressTrans
                                                                     from addressTrans in ljaddressTrans.DefaultIfEmpty()
                                                                     where businessCollateralAgmTable.BusinessCollateralAgmTableGUID == refGUID
                                                                     && businessCollateralAgmTable.AgreementDocType == (int)AgreementDocType.NoticeOfCancellation
                                                                     select new CBCANoticeOfCancellation
                                                                     {
                                                                         CBCANoticeOfCancellation_CustomerAddress = String.Concat(addressTrans.Address1," ",addressTrans.Address2),
                                                                         CBCANoticeOfCancellation_AgreementDate = businessCollateralAgmTable.AgreementDate,
                                                                         CBCANoticeOfCancellation_DocumentReasonId = documentReson.ReasonId,
                                                                         CBCANoticeOfCancellation_DocumentReasonDesc= documentReson.Description,
                                                                         CBCANoticeOfCancellation_Remark = businessCollateralAgmTable.Remark,
                                                                         CBCANoticeOfCancellation_DocumentReasonGUID = businessCollateralAgmTable.DocumentReasonGUID.ToString(),
                                                                         CBCANoticeOfCancellation_CustomerName = businessCollateralAgmTable.CustomerName,
                                                                         CBCANoticeOfCancellation_DBDRegistrationId = businessCollateralAgmTable.DBDRegistrationId,
                                                                         CBCANoticeOfCancellation_DBDRegistrationDate = businessCollateralAgmTable.DBDRegistrationDate
                                                                     }
                                                                     ).FirstOrDefault();
                if (cBCANoticeOfCancellation == null)
                {
                    cBCANoticeOfCancellation = new CBCANoticeOfCancellation();

                }




                CBCAAgreementTableInfo cBCAAgreementTableInfo = (from agreementTableInfo in db.Set<AgreementTableInfo>()



                                                                 join authorizedPersonCompany1 in db.Set<EmployeeTable>()
                                                                 on agreementTableInfo.AuthorizedPersonTransCompany1GUID equals authorizedPersonCompany1.EmployeeTableGUID into ljauthorizedPersonCompany1
                                                                 from authorizedPersonCompany1 in ljauthorizedPersonCompany1.DefaultIfEmpty()

                                                                 join authorizedPersonCompany2 in db.Set<EmployeeTable>()
                                                                on agreementTableInfo.AuthorizedPersonTransCompany2GUID equals authorizedPersonCompany2.EmployeeTableGUID into ljauthorizedPersonCompany2
                                                                 from authorizedPersonCompany2 in ljauthorizedPersonCompany2.DefaultIfEmpty()

                                                                 join authorizedPersonCompany3 in db.Set<EmployeeTable>()
                                                                on agreementTableInfo.AuthorizedPersonTransCompany3GUID equals authorizedPersonCompany3.EmployeeTableGUID into ljauthorizedPersonCompany3
                                                                 from authorizedPersonCompany3 in ljauthorizedPersonCompany3.DefaultIfEmpty()



                                                                 join witnessCompany1 in db.Set<EmployeeTable>()
                                                                 on agreementTableInfo.WitnessCompany1GUID equals witnessCompany1.EmployeeTableGUID into ljwitnessCompany1
                                                                 from witnessCompany1 in ljwitnessCompany1.DefaultIfEmpty()

                                                                 join witnessCompany2 in db.Set<EmployeeTable>()
                                                                 on agreementTableInfo.WitnessCompany2GUID equals witnessCompany2.EmployeeTableGUID into ljwitnessCompany2
                                                                 from witnessCompany2 in ljwitnessCompany2.DefaultIfEmpty()
                                                                where agreementTableInfo.RefGUID == refGUID
                                                                 && agreementTableInfo.RefType == (int)RefType.BusinessCollateralAgreement
                                                                 select new CBCAAgreementTableInfo
                                                                 {
                                                                     CBCAAgreementTableInfo_AuthorizedPersonCompany1 = authorizedPersonCompany1.Name,
                                                                     CBCAAgreementTableInfo_AuthorizedPersonCompany2 = authorizedPersonCompany2.Name,
                                                                     CBCAAgreementTableInfo_AuthorizedPersonCompany3 = authorizedPersonCompany3.Name,
                                                                     CBCAAgreementTableInfo_AuthorizedPersonTransCustomer1GUID = agreementTableInfo.AuthorizedPersonTransCustomer1GUID.ToString(),
                                                                     CBCAAgreementTableInfo_AuthorizedPersonTransCustomer2GUID = agreementTableInfo.AuthorizedPersonTransCustomer2GUID.ToString(),
                                                                     CBCAAgreementTableInfo_AuthorizedPersonTransCustomer3GUID = agreementTableInfo.AuthorizedPersonTransCustomer3GUID.ToString(),
                                                                     CBCAAgreementTableInfo_WitnessCompany1 = witnessCompany1.Name,
                                                                     CBCAAgreementTableInfo_WitnessCompany2 = witnessCompany2.Name,
                                                                     CBCAAgreementTableInfo_CompanyName = agreementTableInfo.CompanyName,
                                                                     CBCAAgreementTableInfo_CompanyAddress = agreementTableInfo.CompanyAddress1 + agreementTableInfo.CompanyAddress2,
                                                                 }
                                                                 ).FirstOrDefault();
                if (cBCAAgreementTableInfo == null)
                {
                    cBCAAgreementTableInfo = new CBCAAgreementTableInfo();

                }
                CBCAQBusinessCollateralAgm cBCAQBusinessCollateralAgm = (from businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
                                                                         where businessCollateralAgmTable.RefBusinessCollateralAgmTableGUID == refGUID
                                                                         select new CBCAQBusinessCollateralAgm
                                                                         {
                                                                             CBCAQBusinessCollateralAgm_DBDRegistrationId = businessCollateralAgmTable.DBDRegistrationId,
                                                                             CBCAQBusinessCollateralAgm_DBDRegistrationDate = businessCollateralAgmTable.DBDRegistrationDate.DateNullToString()
                                                                         }

                                                                         ).FirstOrDefault();

                if (cBCAQBusinessCollateralAgm == null)
                {
                    cBCAQBusinessCollateralAgm = new CBCAQBusinessCollateralAgm();

                }
                var authorizedPersonCustomer1_Name = (from relatedPersonTable in db.Set<RelatedPersonTable>()
                                                      join authorizedPersonTrans in db.Set<AuthorizedPersonTrans>()
                                                      on relatedPersonTable.RelatedPersonTableGUID equals authorizedPersonTrans.RelatedPersonTableGUID
                                                        where authorizedPersonTrans.AuthorizedPersonTransGUID.ToString() == cBCAAgreementTableInfo.CBCAAgreementTableInfo_AuthorizedPersonTransCustomer1GUID
                                                      select relatedPersonTable.Name
                                                      ).FirstOrDefault();
                var authorizedPersonCustomer2_Name = (from relatedPersonTable in db.Set<RelatedPersonTable>()
                                                      join authorizedPersonTrans in db.Set<AuthorizedPersonTrans>()
                                                      on relatedPersonTable.RelatedPersonTableGUID equals authorizedPersonTrans.RelatedPersonTableGUID
                                                      where authorizedPersonTrans.AuthorizedPersonTransGUID.ToString() == cBCAAgreementTableInfo.CBCAAgreementTableInfo_AuthorizedPersonTransCustomer2GUID
                                                      select relatedPersonTable.Name
                                                      ).FirstOrDefault();
                var authorizedPersonCustomer3_Name = (from relatedPersonTable in db.Set<RelatedPersonTable>()
                                                      join authorizedPersonTrans in db.Set<AuthorizedPersonTrans>()
                                                      on relatedPersonTable.RelatedPersonTableGUID equals authorizedPersonTrans.RelatedPersonTableGUID
                                                      where authorizedPersonTrans.AuthorizedPersonTransGUID.ToString() == cBCAAgreementTableInfo.CBCAAgreementTableInfo_AuthorizedPersonTransCustomer3GUID
                                                      select relatedPersonTable.Name
                                                      ).FirstOrDefault();
                CBCAQBookmarkDocumentTrans cBCAQBookmarkDocumentTrans = (from bookmarkDocumentTrans in db.Set<BookmarkDocumentTrans>()
                                                                         where bookmarkDocumentTrans.BookmarkDocumentTransGUID == bookmarkDocumentTransGUID
                                                                         select new CBCAQBookmarkDocumentTrans
                                                                         {
                                                                             CBCAQBookmarkDocumentTrans_ReferenceExternalId = bookmarkDocumentTrans.ReferenceExternalId,
                                                                             CBCAQBookmarkDocumentTrans_ReferenceExternalDate = bookmarkDocumentTrans.ReferenceExternalDate
                                                                         }
                                           ).FirstOrDefault();
                if (cBCAQBookmarkDocumentTrans == null)
                {
                    cBCAQBookmarkDocumentTrans = new CBCAQBookmarkDocumentTrans();
                }

                string bCCancelReasonFirst = null;
                string bCCancelReasonFSecond = null;
                string bCCancelReasonThird = null;
                string bCCancelReasonForth = null;
                string bCCancelReasonFifth = null;
                if (cBCANoticeOfCancellation.CBCANoticeOfCancellation_DocumentReasonId == "Cancel01")
                {
                    bCCancelReasonFirst = "X";
                }
                if (cBCANoticeOfCancellation.CBCANoticeOfCancellation_DocumentReasonId == "Cancel02")
                {
                    bCCancelReasonFSecond = "X";
                }
                if (cBCANoticeOfCancellation.CBCANoticeOfCancellation_DocumentReasonId == "Cancel03")
                {
                    bCCancelReasonThird = "X";
                }
                if (cBCANoticeOfCancellation.CBCANoticeOfCancellation_DocumentReasonId == "Cancel04")
                {
                    bCCancelReasonForth = "X";
                }
                if (cBCANoticeOfCancellation.CBCANoticeOfCancellation_DocumentReasonId == "Cancel05")
                {
                    bCCancelReasonFifth = "X";
                }

                List<CBCAQBusinessCollateralAgmLine> cBCAQBusinessCollateralAgmLineList = (from businessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                                                       join businesCollateralType in db.Set<BusinessCollateralType>()
                                                                                       on businessCollateralAgmLine.BusinessCollateralTypeGUID equals businesCollateralType.BusinessCollateralTypeGUID into ljbusinesCollateralType
                                                                                       from businesCollateralType in ljbusinesCollateralType.DefaultIfEmpty()

                                                                                       join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                                                                                       on businessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                                                                                       from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()

                                                                                       where businessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                                                                                       select new CBCAQBusinessCollateralAgmLine
                                                                                       {
                                                                                           CBCAQBusinessCollateralAgmLine_LineNum = businessCollateralAgmLine.LineNum,
                                                                                           CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeGUID = businessCollateralAgmLine.BusinessCollateralTypeGUID,
                                                                                           CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc = businesCollateralType.Description,
                                                                                           CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeGUID = businessCollateralAgmLine.BusinessCollateralSubTypeGUID,
                                                                                           CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc = businessCollateralSubType.Description,
                                                                                           CBCAQBusinessCollateralAgmLine_Description = businessCollateralAgmLine.Description,
                                                                                           CBCAQBusinessCollateralAgmLine_BusinessCollateralValue = businessCollateralAgmLine.BusinessCollateralValue
                                                                                       }
                                                                                       ).OrderBy(or=>or.CBCAQBusinessCollateralAgmLine_LineNum).Take(20).ToList();

                CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine1 = GetSingleCBCAQBusinessCollateralAgmLine(cBCAQBusinessCollateralAgmLineList, 0);
                CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine2 = GetSingleCBCAQBusinessCollateralAgmLine(cBCAQBusinessCollateralAgmLineList, 1);
                CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine3 = GetSingleCBCAQBusinessCollateralAgmLine(cBCAQBusinessCollateralAgmLineList, 2);
                CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine4 = GetSingleCBCAQBusinessCollateralAgmLine(cBCAQBusinessCollateralAgmLineList, 3);
                CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine5 = GetSingleCBCAQBusinessCollateralAgmLine(cBCAQBusinessCollateralAgmLineList, 4);
                CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine6 = GetSingleCBCAQBusinessCollateralAgmLine(cBCAQBusinessCollateralAgmLineList, 5);
                CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine7 = GetSingleCBCAQBusinessCollateralAgmLine(cBCAQBusinessCollateralAgmLineList, 6);
                CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine8 = GetSingleCBCAQBusinessCollateralAgmLine(cBCAQBusinessCollateralAgmLineList, 7);
                CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine9 = GetSingleCBCAQBusinessCollateralAgmLine(cBCAQBusinessCollateralAgmLineList, 8);
                CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine10 = GetSingleCBCAQBusinessCollateralAgmLine(cBCAQBusinessCollateralAgmLineList, 9);
                CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine11 = GetSingleCBCAQBusinessCollateralAgmLine(cBCAQBusinessCollateralAgmLineList, 10);
                CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine12 = GetSingleCBCAQBusinessCollateralAgmLine(cBCAQBusinessCollateralAgmLineList, 11);
                CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine13 = GetSingleCBCAQBusinessCollateralAgmLine(cBCAQBusinessCollateralAgmLineList, 12);
                CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine14 = GetSingleCBCAQBusinessCollateralAgmLine(cBCAQBusinessCollateralAgmLineList, 13);
                CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine15 = GetSingleCBCAQBusinessCollateralAgmLine(cBCAQBusinessCollateralAgmLineList, 14);
                CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine16 = GetSingleCBCAQBusinessCollateralAgmLine(cBCAQBusinessCollateralAgmLineList, 15);
                CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine17 = GetSingleCBCAQBusinessCollateralAgmLine(cBCAQBusinessCollateralAgmLineList, 16);
                CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine18 = GetSingleCBCAQBusinessCollateralAgmLine(cBCAQBusinessCollateralAgmLineList, 17);
                CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine19 = GetSingleCBCAQBusinessCollateralAgmLine(cBCAQBusinessCollateralAgmLineList, 18);
                CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine20 = GetSingleCBCAQBusinessCollateralAgmLine(cBCAQBusinessCollateralAgmLineList, 19);


                BookmarkCancelBusinessCollateralAgreement model = new BookmarkCancelBusinessCollateralAgreement();
                SetModelByCBCAQBusinessCollateralAgmLine1(model, cBCAQBusinessCollateralAgmLine1);
                SetModelByCBCAQBusinessCollateralAgmLine2(model, cBCAQBusinessCollateralAgmLine2);
                SetModelByCBCAQBusinessCollateralAgmLine3(model, cBCAQBusinessCollateralAgmLine3);
                SetModelByCBCAQBusinessCollateralAgmLine4(model, cBCAQBusinessCollateralAgmLine4);
                SetModelByCBCAQBusinessCollateralAgmLine5(model, cBCAQBusinessCollateralAgmLine5);
                SetModelByCBCAQBusinessCollateralAgmLine6(model, cBCAQBusinessCollateralAgmLine6);
                SetModelByCBCAQBusinessCollateralAgmLine7(model, cBCAQBusinessCollateralAgmLine7);
                SetModelByCBCAQBusinessCollateralAgmLine8(model, cBCAQBusinessCollateralAgmLine8);
                SetModelByCBCAQBusinessCollateralAgmLine9(model, cBCAQBusinessCollateralAgmLine9);
                SetModelByCBCAQBusinessCollateralAgmLine10(model, cBCAQBusinessCollateralAgmLine10);
                SetModelByCBCAQBusinessCollateralAgmLine11(model, cBCAQBusinessCollateralAgmLine11);
                SetModelByCBCAQBusinessCollateralAgmLine12(model, cBCAQBusinessCollateralAgmLine12);
                SetModelByCBCAQBusinessCollateralAgmLine13(model, cBCAQBusinessCollateralAgmLine13);
                SetModelByCBCAQBusinessCollateralAgmLine14(model, cBCAQBusinessCollateralAgmLine14);
                SetModelByCBCAQBusinessCollateralAgmLine15(model, cBCAQBusinessCollateralAgmLine15);
                SetModelByCBCAQBusinessCollateralAgmLine16(model, cBCAQBusinessCollateralAgmLine16);
                SetModelByCBCAQBusinessCollateralAgmLine17(model, cBCAQBusinessCollateralAgmLine17);
                SetModelByCBCAQBusinessCollateralAgmLine18(model, cBCAQBusinessCollateralAgmLine18);
                SetModelByCBCAQBusinessCollateralAgmLine19(model, cBCAQBusinessCollateralAgmLine19);
                SetModelByCBCAQBusinessCollateralAgmLine20(model, cBCAQBusinessCollateralAgmLine20);

                model.CBCANoticeOfCancellation_Remark = cBCANoticeOfCancellation != null ? cBCANoticeOfCancellation.CBCANoticeOfCancellation_Remark : "";
                model.CBCANoticeOfCancellation_DocumentReasonDesc = cBCANoticeOfCancellation != null ? cBCANoticeOfCancellation.CBCANoticeOfCancellation_DocumentReasonDesc: "";
                model.CBCANoticeOfCancellation_AgreementDate = DateToWordCustom(cBCANoticeOfCancellation.CBCANoticeOfCancellation_AgreementDate,TextConstants.TH, TextConstants.DMY);
                model.CBCANoticeOfCancellation_DocumentReasonGUID = cBCANoticeOfCancellation != null ? cBCANoticeOfCancellation.CBCANoticeOfCancellation_DocumentReasonGUID : "";
                model.CBCAAgreementTableInfo_AuthorizedPersonCompany1 = cBCAAgreementTableInfo != null ? cBCAAgreementTableInfo.CBCAAgreementTableInfo_AuthorizedPersonCompany1 : "";
                model.CBCAAgreementTableInfo_AuthorizedPersonCompany2 = cBCAAgreementTableInfo != null ? cBCAAgreementTableInfo.CBCAAgreementTableInfo_AuthorizedPersonCompany2 : "";
                model.CBCAAgreementTableInfo_AuthorizedPersonCompany3 = cBCAAgreementTableInfo != null ? cBCAAgreementTableInfo.CBCAAgreementTableInfo_AuthorizedPersonCompany3 : "";
                model.CBCAAgreementTableInfo_AuthorizedPersonTransCustomer1GUID = cBCAAgreementTableInfo != null ? cBCAAgreementTableInfo.CBCAAgreementTableInfo_AuthorizedPersonTransCustomer1GUID : "";
                model.CBCAAgreementTableInfo_AuthorizedPersonTransCustomer2GUID = cBCAAgreementTableInfo != null ? cBCAAgreementTableInfo.CBCAAgreementTableInfo_AuthorizedPersonTransCustomer2GUID : "";
                model.CBCAAgreementTableInfo_AuthorizedPersonTransCustomer3GUID = cBCAAgreementTableInfo != null ? cBCAAgreementTableInfo.CBCAAgreementTableInfo_AuthorizedPersonTransCustomer3GUID : "";
                model.CBCAAgreementTableInfo_WitnessCompany1 = cBCAAgreementTableInfo != null ? cBCAAgreementTableInfo.CBCAAgreementTableInfo_WitnessCompany1 : "";
                model.CBCAAgreementTableInfo_WitnessCompany2 = cBCAAgreementTableInfo != null ? cBCAAgreementTableInfo.CBCAAgreementTableInfo_WitnessCompany2 : "";


                model.CBCANoticeOfCancellation_CustomerName = cBCANoticeOfCancellation != null ? cBCANoticeOfCancellation.CBCANoticeOfCancellation_CustomerName : "";
                model.CBCANoticeOfCancellation_DBDRegistrationId = cBCANoticeOfCancellation != null ? cBCANoticeOfCancellation.CBCANoticeOfCancellation_DBDRegistrationId : "";
                model.CBCANoticeOfCancellation_DBDRegistrationDate =  DateToWordCustom(cBCANoticeOfCancellation.CBCANoticeOfCancellation_DBDRegistrationDate, TextConstants.TH, TextConstants.DMY);
                model.CBCANoticeOfCancellation_CustomerAddress = cBCANoticeOfCancellation != null ? cBCANoticeOfCancellation.CBCANoticeOfCancellation_CustomerAddress : "";


                model.BCCancelReasonFirst = bCCancelReasonFirst;
                model.BCCancelReasonFSecond = bCCancelReasonFSecond;
                model.BCCancelReasonThird = bCCancelReasonThird;
                model.BCCancelReasonForth = bCCancelReasonForth;
                model.BCCancelReasonFifth = bCCancelReasonFifth;
                model.ReferenceExternalId = cBCAQBookmarkDocumentTrans.CBCAQBookmarkDocumentTrans_ReferenceExternalId;
                model.CBCAQBookmarkDocumentTrans_ReferenceExternalDate = cBCAQBookmarkDocumentTrans.CBCAQBookmarkDocumentTrans_ReferenceExternalDate;
                model.AuthorizedPersonCustomer1_Name = authorizedPersonCustomer1_Name;
                model.AuthorizedPersonCustomer2_Name = authorizedPersonCustomer2_Name;
                model.AuthorizedPersonCustomer3_Name = authorizedPersonCustomer3_Name;
                return model;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region SetModelByCBCAQBusinessCollateralAgmLine
        private void SetModelByCBCAQBusinessCollateralAgmLine1(BookmarkCancelBusinessCollateralAgreement model, CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine)
        {
            try
            {
                model.CBCAQBusinessCollateralAgmLine_LineNum_1 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_LineNum;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_1 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_1 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_Description_1 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_Description;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_1 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelByCBCAQBusinessCollateralAgmLine2(BookmarkCancelBusinessCollateralAgreement model, CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine)
        {
            try
            {
                model.CBCAQBusinessCollateralAgmLine_LineNum_2 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_LineNum;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_2 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_2 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_Description_2 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_Description;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_2 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private void SetModelByCBCAQBusinessCollateralAgmLine3(BookmarkCancelBusinessCollateralAgreement model, CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine)
        {
            try
            {
                model.CBCAQBusinessCollateralAgmLine_LineNum_3 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_LineNum;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_3 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_3 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_Description_3 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_Description;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_3 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        private void SetModelByCBCAQBusinessCollateralAgmLine4(BookmarkCancelBusinessCollateralAgreement model, CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine)
        {
            try
            {
                model.CBCAQBusinessCollateralAgmLine_LineNum_4 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_LineNum;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_4 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_4 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_Description_4 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_Description;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_4 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        private void SetModelByCBCAQBusinessCollateralAgmLine5(BookmarkCancelBusinessCollateralAgreement model, CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine)
        {
            try
            {
                model.CBCAQBusinessCollateralAgmLine_LineNum_5 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_LineNum;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_5 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_5 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_Description_5 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_Description;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_5 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        private void SetModelByCBCAQBusinessCollateralAgmLine6(BookmarkCancelBusinessCollateralAgreement model, CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine)
        {
            try
            {
                model.CBCAQBusinessCollateralAgmLine_LineNum_6 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_LineNum;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_6 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_6 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_Description_6 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_Description;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_6 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        private void SetModelByCBCAQBusinessCollateralAgmLine7(BookmarkCancelBusinessCollateralAgreement model, CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine)
        {
            try
            {
                model.CBCAQBusinessCollateralAgmLine_LineNum_7 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_LineNum;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_7 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_7 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_Description_7 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_Description;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_7 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        private void SetModelByCBCAQBusinessCollateralAgmLine8(BookmarkCancelBusinessCollateralAgreement model, CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine)
        {
            try
            {
                model.CBCAQBusinessCollateralAgmLine_LineNum_8 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_LineNum;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_8 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_8 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_Description_8 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_Description;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_8 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        private void SetModelByCBCAQBusinessCollateralAgmLine9(BookmarkCancelBusinessCollateralAgreement model, CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine)
        {
            try
            {
                model.CBCAQBusinessCollateralAgmLine_LineNum_9 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_LineNum;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_9 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_9 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_Description_9 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_Description;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_9 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        private void SetModelByCBCAQBusinessCollateralAgmLine10(BookmarkCancelBusinessCollateralAgreement model, CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine)
        {
            try
            {
                model.CBCAQBusinessCollateralAgmLine_LineNum_10 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_LineNum;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_10 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_10 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_Description_10 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_Description;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_10 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        private void SetModelByCBCAQBusinessCollateralAgmLine11(BookmarkCancelBusinessCollateralAgreement model, CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine)
        {
            try
            {
                model.CBCAQBusinessCollateralAgmLine_LineNum_11 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_LineNum;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_11 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_11 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_Description_11 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_Description;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_11 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        private void SetModelByCBCAQBusinessCollateralAgmLine12(BookmarkCancelBusinessCollateralAgreement model, CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine)
        {
            try
            {
                model.CBCAQBusinessCollateralAgmLine_LineNum_12 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_LineNum;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_12 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_12 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_Description_12 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_Description;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_12 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        private void SetModelByCBCAQBusinessCollateralAgmLine13(BookmarkCancelBusinessCollateralAgreement model, CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine)
        {
            try
            {
                model.CBCAQBusinessCollateralAgmLine_LineNum_13 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_LineNum;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_13 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_13 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_Description_13 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_Description;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_13 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        private void SetModelByCBCAQBusinessCollateralAgmLine14(BookmarkCancelBusinessCollateralAgreement model, CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine)
        {
            try
            {
                model.CBCAQBusinessCollateralAgmLine_LineNum_14 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_LineNum;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_14 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_14 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_Description_14 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_Description;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_14 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        private void SetModelByCBCAQBusinessCollateralAgmLine15(BookmarkCancelBusinessCollateralAgreement model, CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine)
        {
            try
            {
                model.CBCAQBusinessCollateralAgmLine_LineNum_15 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_LineNum;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_15 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_15 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_Description_15 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_Description;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_15 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        private void SetModelByCBCAQBusinessCollateralAgmLine16(BookmarkCancelBusinessCollateralAgreement model, CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine)
        {
            try
            {
                model.CBCAQBusinessCollateralAgmLine_LineNum_16 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_LineNum;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_16 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_16 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_Description_16 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_Description;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_16 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        private void SetModelByCBCAQBusinessCollateralAgmLine17(BookmarkCancelBusinessCollateralAgreement model, CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine)
        {
            try
            {
                model.CBCAQBusinessCollateralAgmLine_LineNum_17 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_LineNum;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_17 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_17 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_Description_17 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_Description;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_17 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        private void SetModelByCBCAQBusinessCollateralAgmLine18(BookmarkCancelBusinessCollateralAgreement model, CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine)
        {
            try
            {
                model.CBCAQBusinessCollateralAgmLine_LineNum_18 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_LineNum;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_18 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_18 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_Description_18 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_Description;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_18 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        private void SetModelByCBCAQBusinessCollateralAgmLine19(BookmarkCancelBusinessCollateralAgreement model, CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine)
        {
            try
            {
                model.CBCAQBusinessCollateralAgmLine_LineNum_19 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_LineNum;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_19 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_19 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_Description_19 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_Description;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_19 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        private void SetModelByCBCAQBusinessCollateralAgmLine20(BookmarkCancelBusinessCollateralAgreement model, CBCAQBusinessCollateralAgmLine cBCAQBusinessCollateralAgmLine)
        {
            try
            {
                model.CBCAQBusinessCollateralAgmLine_LineNum_20 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_LineNum;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_20 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_20 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc;
                model.CBCAQBusinessCollateralAgmLine_Description_20 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_Description;
                model.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_20 = cBCAQBusinessCollateralAgmLine.CBCAQBusinessCollateralAgmLine_BusinessCollateralValue;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion SetModelByCBCAQBusinessCollateralAgmLine
        private CBCAQBusinessCollateralAgmLine GetSingleCBCAQBusinessCollateralAgmLine(List<CBCAQBusinessCollateralAgmLine> cBCAQBusinessCollateralAgmLineList, int index)
        {
            try
            {
                if(index< cBCAQBusinessCollateralAgmLineList.Count())
                {
                    return cBCAQBusinessCollateralAgmLineList[index];
                }
                else
                {
                    return new CBCAQBusinessCollateralAgmLine();
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public BookmarkBusinessCollateralAgreementExpenceDetail GetBookmarkDocumentBusinessCollateralAgreementExpenceDetail(Guid refGUID)
        {



            try
            {
                BCADTServiceFeeTrans QServiceFeeTrans10 = (from serviceFeeTrans in db.Set<ServiceFeeTrans>()
                                                           where serviceFeeTrans.RefType == (int)RefType.BusinessCollateralAgreement
                                                           && serviceFeeTrans.RefGUID == refGUID
                                                           && serviceFeeTrans.Ordering == 10
                                                           group new
                                                           {
                                                               serviceFeeTrans
                                                           }
                                                           by new
                                                           {
                                                               serviceFeeTrans.Ordering,
                                                               serviceFeeTrans.Description,
                                                               serviceFeeTrans.AmountBeforeTax,
                                                               serviceFeeTrans.TaxAmount,
                                                               serviceFeeTrans.AmountIncludeTax,
                                                               serviceFeeTrans.WHTAmount
                                                           }
                                                          into g
                                                           select new BCADTServiceFeeTrans
                                                           {
                                                               BCADTServiceFeeTrans_Ordering = g.Key.Ordering,
                                                               BCADTServiceFeeTrans_Description = g.Key.Description,
                                                               BCADTServiceFeeTrans_AmountBeforeTax = g.Key.AmountBeforeTax,
                                                               BCADTServiceFeeTrans_TaxAmount = g.Key.TaxAmount,
                                                               BCADTServiceFeeTrans_AmountIncludeTax = g.Key.AmountIncludeTax,
                                                               BCADTServiceFeeTrans_WHTAmount = g.Key.WHTAmount,
                                                               BCADTServiceFeeTrans_LineServiceFeeNet = g.Sum(su => (su.serviceFeeTrans.AmountIncludeTax - su.serviceFeeTrans.WHTAmount))
                                                           }
                                                          ).FirstOrDefault();
                BCADTServiceFeeTrans QServiceFeeTrans9 = (from serviceFeeTrans in db.Set<ServiceFeeTrans>()
                                                          where serviceFeeTrans.RefType == (int)RefType.BusinessCollateralAgreement
                                                          && serviceFeeTrans.RefGUID == refGUID
                                                          && serviceFeeTrans.Ordering == 9
                                                          group new
                                                          {
                                                              serviceFeeTrans
                                                          }
                                                          by new
                                                          {
                                                              serviceFeeTrans.Ordering,
                                                              serviceFeeTrans.Description,
                                                              serviceFeeTrans.AmountBeforeTax,
                                                              serviceFeeTrans.TaxAmount,
                                                              serviceFeeTrans.AmountIncludeTax,
                                                              serviceFeeTrans.WHTAmount
                                                          }
                                                          into g
                                                          select new BCADTServiceFeeTrans
                                                          {
                                                              BCADTServiceFeeTrans_Ordering = g.Key.Ordering,
                                                              BCADTServiceFeeTrans_Description = g.Key.Description,
                                                              BCADTServiceFeeTrans_AmountBeforeTax = g.Key.AmountBeforeTax,
                                                              BCADTServiceFeeTrans_TaxAmount = g.Key.TaxAmount,
                                                              BCADTServiceFeeTrans_AmountIncludeTax = g.Key.AmountIncludeTax,
                                                              BCADTServiceFeeTrans_WHTAmount = g.Key.WHTAmount,
                                                              BCADTServiceFeeTrans_LineServiceFeeNet = g.Sum(su => (su.serviceFeeTrans.AmountIncludeTax - su.serviceFeeTrans.WHTAmount))
                                                          }
                                                          ).FirstOrDefault();
                BCADTServiceFeeTrans QServiceFeeTrans8 = (from serviceFeeTrans in db.Set<ServiceFeeTrans>()
                                                          where serviceFeeTrans.RefType == (int)RefType.BusinessCollateralAgreement
                                                          && serviceFeeTrans.RefGUID == refGUID
                                                          && serviceFeeTrans.Ordering == 8
                                                          group new
                                                          {
                                                              serviceFeeTrans
                                                          }
                                                          by new
                                                          {
                                                              serviceFeeTrans.Ordering,
                                                              serviceFeeTrans.Description,
                                                              serviceFeeTrans.AmountBeforeTax,
                                                              serviceFeeTrans.TaxAmount,
                                                              serviceFeeTrans.AmountIncludeTax,
                                                              serviceFeeTrans.WHTAmount
                                                          }
                                                          into g
                                                          select new BCADTServiceFeeTrans
                                                          {
                                                              BCADTServiceFeeTrans_Ordering = g.Key.Ordering,
                                                              BCADTServiceFeeTrans_Description = g.Key.Description,
                                                              BCADTServiceFeeTrans_AmountBeforeTax = g.Key.AmountBeforeTax,
                                                              BCADTServiceFeeTrans_TaxAmount = g.Key.TaxAmount,
                                                              BCADTServiceFeeTrans_AmountIncludeTax = g.Key.AmountIncludeTax,
                                                              BCADTServiceFeeTrans_WHTAmount = g.Key.WHTAmount,
                                                              BCADTServiceFeeTrans_LineServiceFeeNet = g.Sum(su => (su.serviceFeeTrans.AmountIncludeTax - su.serviceFeeTrans.WHTAmount))
                                                          }
                                                          ).FirstOrDefault();
                BCADTServiceFeeTrans QServiceFeeTrans7 = (from serviceFeeTrans in db.Set<ServiceFeeTrans>()
                                                          where serviceFeeTrans.RefType == (int)RefType.BusinessCollateralAgreement
                                                          && serviceFeeTrans.RefGUID == refGUID
                                                          && serviceFeeTrans.Ordering == 7
                                                          group new
                                                          {
                                                              serviceFeeTrans
                                                          }
                                                          by new
                                                          {
                                                              serviceFeeTrans.Ordering,
                                                              serviceFeeTrans.Description,
                                                              serviceFeeTrans.AmountBeforeTax,
                                                              serviceFeeTrans.TaxAmount,
                                                              serviceFeeTrans.AmountIncludeTax,
                                                              serviceFeeTrans.WHTAmount
                                                          }
                                                          into g
                                                          select new BCADTServiceFeeTrans
                                                          {
                                                              BCADTServiceFeeTrans_Ordering = g.Key.Ordering,
                                                              BCADTServiceFeeTrans_Description = g.Key.Description,
                                                              BCADTServiceFeeTrans_AmountBeforeTax = g.Key.AmountBeforeTax,
                                                              BCADTServiceFeeTrans_TaxAmount = g.Key.TaxAmount,
                                                              BCADTServiceFeeTrans_AmountIncludeTax = g.Key.AmountIncludeTax,
                                                              BCADTServiceFeeTrans_WHTAmount = g.Key.WHTAmount,
                                                              BCADTServiceFeeTrans_LineServiceFeeNet = g.Sum(su => (su.serviceFeeTrans.AmountIncludeTax - su.serviceFeeTrans.WHTAmount))
                                                          }
                                                          ).FirstOrDefault();
                BCADTServiceFeeTrans QServiceFeeTrans6 = (from serviceFeeTrans in db.Set<ServiceFeeTrans>()
                                                          where serviceFeeTrans.RefType == (int)RefType.BusinessCollateralAgreement
                                                          && serviceFeeTrans.RefGUID == refGUID
                                                          && serviceFeeTrans.Ordering == 6
                                                          group new
                                                          {
                                                              serviceFeeTrans
                                                          }
                                                          by new
                                                          {
                                                              serviceFeeTrans.Ordering,
                                                              serviceFeeTrans.Description,
                                                              serviceFeeTrans.AmountBeforeTax,
                                                              serviceFeeTrans.TaxAmount,
                                                              serviceFeeTrans.AmountIncludeTax,
                                                              serviceFeeTrans.WHTAmount
                                                          }
                                                          into g
                                                          select new BCADTServiceFeeTrans
                                                          {
                                                              BCADTServiceFeeTrans_Ordering = g.Key.Ordering,
                                                              BCADTServiceFeeTrans_Description = g.Key.Description,
                                                              BCADTServiceFeeTrans_AmountBeforeTax = g.Key.AmountBeforeTax,
                                                              BCADTServiceFeeTrans_TaxAmount = g.Key.TaxAmount,
                                                              BCADTServiceFeeTrans_AmountIncludeTax = g.Key.AmountIncludeTax,
                                                              BCADTServiceFeeTrans_WHTAmount = g.Key.WHTAmount,
                                                              BCADTServiceFeeTrans_LineServiceFeeNet = g.Sum(su => (su.serviceFeeTrans.AmountIncludeTax - su.serviceFeeTrans.WHTAmount))
                                                          }
                                                          ).FirstOrDefault();
                BCADTServiceFeeTrans QServiceFeeTrans5 = (from serviceFeeTrans in db.Set<ServiceFeeTrans>()
                                                          where serviceFeeTrans.RefType == (int)RefType.BusinessCollateralAgreement
                                                          && serviceFeeTrans.RefGUID == refGUID
                                                          && serviceFeeTrans.Ordering == 5
                                                          group new
                                                          {
                                                              serviceFeeTrans
                                                          }
                                                          by new
                                                          {
                                                              serviceFeeTrans.Ordering,
                                                              serviceFeeTrans.Description,
                                                              serviceFeeTrans.AmountBeforeTax,
                                                              serviceFeeTrans.TaxAmount,
                                                              serviceFeeTrans.AmountIncludeTax,
                                                              serviceFeeTrans.WHTAmount
                                                          }
                                                          into g
                                                          select new BCADTServiceFeeTrans
                                                          {
                                                              BCADTServiceFeeTrans_Ordering = g.Key.Ordering,
                                                              BCADTServiceFeeTrans_Description = g.Key.Description,
                                                              BCADTServiceFeeTrans_AmountBeforeTax = g.Key.AmountBeforeTax,
                                                              BCADTServiceFeeTrans_TaxAmount = g.Key.TaxAmount,
                                                              BCADTServiceFeeTrans_AmountIncludeTax = g.Key.AmountIncludeTax,
                                                              BCADTServiceFeeTrans_WHTAmount = g.Key.WHTAmount,
                                                              BCADTServiceFeeTrans_LineServiceFeeNet = g.Sum(su => (su.serviceFeeTrans.AmountIncludeTax - su.serviceFeeTrans.WHTAmount))
                                                          }
                                                          ).FirstOrDefault();
                BCADTServiceFeeTrans QServiceFeeTrans4 = (from serviceFeeTrans in db.Set<ServiceFeeTrans>()
                                                          where serviceFeeTrans.RefType == (int)RefType.BusinessCollateralAgreement
                                                          && serviceFeeTrans.RefGUID == refGUID
                                                          && serviceFeeTrans.Ordering == 4
                                                          group new
                                                          {
                                                              serviceFeeTrans
                                                          }
                                                          by new
                                                          {
                                                              serviceFeeTrans.Ordering,
                                                              serviceFeeTrans.Description,
                                                              serviceFeeTrans.AmountBeforeTax,
                                                              serviceFeeTrans.TaxAmount,
                                                              serviceFeeTrans.AmountIncludeTax,
                                                              serviceFeeTrans.WHTAmount
                                                          }
                                                          into g
                                                          select new BCADTServiceFeeTrans
                                                          {
                                                              BCADTServiceFeeTrans_Ordering = g.Key.Ordering,
                                                              BCADTServiceFeeTrans_Description = g.Key.Description,
                                                              BCADTServiceFeeTrans_AmountBeforeTax = g.Key.AmountBeforeTax,
                                                              BCADTServiceFeeTrans_TaxAmount = g.Key.TaxAmount,
                                                              BCADTServiceFeeTrans_AmountIncludeTax = g.Key.AmountIncludeTax,
                                                              BCADTServiceFeeTrans_WHTAmount = g.Key.WHTAmount,
                                                              BCADTServiceFeeTrans_LineServiceFeeNet = g.Sum(su => (su.serviceFeeTrans.AmountIncludeTax - su.serviceFeeTrans.WHTAmount))
                                                          }
                                                          ).FirstOrDefault();
                BCADTServiceFeeTrans QServiceFeeTrans3 = (from serviceFeeTrans in db.Set<ServiceFeeTrans>()
                                                          where serviceFeeTrans.RefType == (int)RefType.BusinessCollateralAgreement
                                                          && serviceFeeTrans.RefGUID == refGUID
                                                          && serviceFeeTrans.Ordering == 3
                                                          group new
                                                          {
                                                              serviceFeeTrans
                                                          }
                                                          by new
                                                          {
                                                              serviceFeeTrans.Ordering,
                                                              serviceFeeTrans.Description,
                                                              serviceFeeTrans.AmountBeforeTax,
                                                              serviceFeeTrans.TaxAmount,
                                                              serviceFeeTrans.AmountIncludeTax,
                                                              serviceFeeTrans.WHTAmount
                                                          }
                                                          into g
                                                          select new BCADTServiceFeeTrans
                                                          {
                                                              BCADTServiceFeeTrans_Ordering = g.Key.Ordering,
                                                              BCADTServiceFeeTrans_Description = g.Key.Description,
                                                              BCADTServiceFeeTrans_AmountBeforeTax = g.Key.AmountBeforeTax,
                                                              BCADTServiceFeeTrans_TaxAmount = g.Key.TaxAmount,
                                                              BCADTServiceFeeTrans_AmountIncludeTax = g.Key.AmountIncludeTax,
                                                              BCADTServiceFeeTrans_WHTAmount = g.Key.WHTAmount,
                                                              BCADTServiceFeeTrans_LineServiceFeeNet = g.Sum(su => (su.serviceFeeTrans.AmountIncludeTax - su.serviceFeeTrans.WHTAmount))
                                                          }
                                                          ).FirstOrDefault();
                BCADTServiceFeeTrans QServiceFeeTrans2 = (from serviceFeeTrans in db.Set<ServiceFeeTrans>()
                                                          where serviceFeeTrans.RefType == (int)RefType.BusinessCollateralAgreement
                                                          && serviceFeeTrans.RefGUID == refGUID
                                                          && serviceFeeTrans.Ordering == 2
                                                          group new
                                                          {
                                                              serviceFeeTrans
                                                          }
                                                          by new
                                                          {
                                                              serviceFeeTrans.Ordering,
                                                              serviceFeeTrans.Description,
                                                              serviceFeeTrans.AmountBeforeTax,
                                                              serviceFeeTrans.TaxAmount,
                                                              serviceFeeTrans.AmountIncludeTax,
                                                              serviceFeeTrans.WHTAmount
                                                          }
                                                          into g
                                                          select new BCADTServiceFeeTrans
                                                          {
                                                              BCADTServiceFeeTrans_Ordering = g.Key.Ordering,
                                                              BCADTServiceFeeTrans_Description = g.Key.Description,
                                                              BCADTServiceFeeTrans_AmountBeforeTax = g.Key.AmountBeforeTax,
                                                              BCADTServiceFeeTrans_TaxAmount = g.Key.TaxAmount,
                                                              BCADTServiceFeeTrans_AmountIncludeTax = g.Key.AmountIncludeTax,
                                                              BCADTServiceFeeTrans_WHTAmount = g.Key.WHTAmount,
                                                              BCADTServiceFeeTrans_LineServiceFeeNet = g.Sum(su => (su.serviceFeeTrans.AmountIncludeTax - su.serviceFeeTrans.WHTAmount))
                                                          }
                                                          ).FirstOrDefault();
                BCADTServiceFeeTrans QServiceFeeTrans1 = (from serviceFeeTrans in db.Set<ServiceFeeTrans>()
                                                          where serviceFeeTrans.RefType == (int)RefType.BusinessCollateralAgreement
                                                          && serviceFeeTrans.RefGUID == refGUID
                                                          && serviceFeeTrans.Ordering == 1
                                                          group new
                                                          {
                                                              serviceFeeTrans
                                                          }
                                                          by new
                                                          {
                                                              serviceFeeTrans.Ordering,
                                                              serviceFeeTrans.Description,
                                                              serviceFeeTrans.AmountBeforeTax,
                                                              serviceFeeTrans.TaxAmount,
                                                              serviceFeeTrans.AmountIncludeTax,
                                                              serviceFeeTrans.WHTAmount
                                                          }
                                                          into g
                                                          select new BCADTServiceFeeTrans
                                                          {
                                                              BCADTServiceFeeTrans_Ordering = g.Key.Ordering,
                                                              BCADTServiceFeeTrans_Description = g.Key.Description,
                                                              BCADTServiceFeeTrans_AmountBeforeTax = g.Key.AmountBeforeTax,
                                                              BCADTServiceFeeTrans_TaxAmount = g.Key.TaxAmount,
                                                              BCADTServiceFeeTrans_AmountIncludeTax = g.Key.AmountIncludeTax,
                                                              BCADTServiceFeeTrans_WHTAmount = g.Key.WHTAmount,
                                                              BCADTServiceFeeTrans_LineServiceFeeNet = g.Sum(su => (su.serviceFeeTrans.AmountIncludeTax - su.serviceFeeTrans.WHTAmount))
                                                          }
                                                          ).FirstOrDefault();


                BCADTbusinessCollateralAgmTable qBusinessCollateralAgmTable = (from businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
                                                                               where businessCollateralAgmTable.BusinessCollateralAgmTableGUID == refGUID
                                                                               select new BCADTbusinessCollateralAgmTable
                                                                               {
                                                                                   BCADTbusinessCollateralAgmTable_AgreementDate = businessCollateralAgmTable.AgreementDate.DateToString(),
                                                                                   BCADTbusinessCollateralAgmTable_BusinessCollateralAgmId = businessCollateralAgmTable.BusinessCollateralAgmId,
                                                                                   BCADTbusinessCollateralAgmTable_CustomerName = businessCollateralAgmTable.CustomerName,
                                                                                   BCADTbusinessCollateralAgmTable_CreditAppTableGUID = businessCollateralAgmTable.CreditAppTableGUID != null ? businessCollateralAgmTable.CreditAppTableGUID.GuidNullToString() : "",
                                                                                   BCADTbusinessCollateralAgmTable_CompanyGUID = businessCollateralAgmTable.CompanyGUID != null ? businessCollateralAgmTable.CompanyGUID.GuidNullToString() : "",
                                                                                   BCADTConsortiumTrans_ConsortiumTableGUID = businessCollateralAgmTable.ConsortiumTableGUID != null ? businessCollateralAgmTable.ConsortiumTableGUID.GuidNullToString() : "",
                                                                                   BCADTConsortiumTrans_CreditAppRequestTableGUID = businessCollateralAgmTable.CreditAppRequestTableGUID.GuidNullToString()
                                                                               }
                                                                               ).FirstOrDefault();

                string QCompanyParameter_PFExtendCreditTerm = (from cp in db.Set<CompanyParameter>().Where(w => w.CompanyGUID == qBusinessCollateralAgmTable.BCADTbusinessCollateralAgmTable_CompanyGUID.StringToGuid())
                                                               join ct in db.Set<CreditTerm>()
                                                               on cp.PFExtendCreditTermGUID equals ct.CreditTermGUID
                                                               select ct.Description
                                                                ).FirstOrDefault();

                BCADTCreditAppTable qCreditAppTable = (from creditAppTable in db.Set<CreditAppTable>()
                                                       // Remove R02
                                                       //join creditAppRequestTable in db.Set<CreditAppRequestTable>()
                                                       //on creditAppTable.CreditAppTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID
                                                       //into ljcreditAppRequestTable
                                                       //from creditAppRequestTable in ljcreditAppRequestTable.DefaultIfEmpty()
                                                       //join creditAppRequestLine in db.Set<CreditAppRequestLine>()
                                                       //on creditAppRequestTable.CreditAppRequestTableGUID equals creditAppRequestLine.CreditAppRequestTableGUID
                                                       //into ljcreditAppRequestLine
                                                       //from creditAppRequestLine in ljcreditAppRequestLine.DefaultIfEmpty()
                                                       where creditAppTable.CreditAppTableGUID.ToString() == qBusinessCollateralAgmTable.BCADTbusinessCollateralAgmTable_CreditAppTableGUID
                                                       group new
                                                       {
                                                           creditAppTable,
                                                       }
                                                       by new
                                                       {
                                                           creditAppTable.ApprovedCreditLimit
                                                       }
                                                        into g
                                                       select new BCADTCreditAppTable
                                                       {
                                                           BCADTCreditAppTable_ApprovedCreditLimit = g.Key.ApprovedCreditLimit,
                                                       }
                                                       ).FirstOrDefault();

             BCADTCreditAppRequestLine qCreditAppRequestLine = (from creditAppRequestLine in db.Set<CreditAppRequestLine>()
                                                       where creditAppRequestLine.CreditAppRequestTableGUID.ToString() == qBusinessCollateralAgmTable.BCADTConsortiumTrans_CreditAppRequestTableGUID && creditAppRequestLine.ApprovalDecision == (int)ApprovalDecision.Approved
                                                                group creditAppRequestLine
                                                       by creditAppRequestLine.CreditAppRequestTableGUID into g
                                                       select new BCADTCreditAppRequestLine
                                                       {
                                                           BCADTCreditAppRequestLine_SumApprovedCreditLimitLineRequest = g.Sum(t => t.ApprovedCreditLimitLineRequest),
                                                       }
                                       ).FirstOrDefault();


                var qCompany_Name = (from company in db.Set<Company>()
                                     where company.CompanyGUID.ToString() == qBusinessCollateralAgmTable.BCADTbusinessCollateralAgmTable_CompanyGUID
                                     select company.Name
                                     ).FirstOrDefault();

                BCADTConsortiumTrans qConsortiumTrans1 = (from consortiumTrans in db.Set<ConsortiumTrans>()
                                                          where consortiumTrans.RefGUID == refGUID && consortiumTrans.Ordering == 1
                                                          select new BCADTConsortiumTrans
                                                          {
                                                              BCADTConsortiumTrans_OperatedBy = consortiumTrans.OperatedBy,
                                                              BCADTConsortiumTrans_CustomerName = consortiumTrans.CustomerName
                                                          }

                                                          ).FirstOrDefault();
                BCADTConsortiumTrans qConsortiumTrans2 = (from consortiumTrans in db.Set<ConsortiumTrans>()
                                                          where consortiumTrans.RefGUID == refGUID && consortiumTrans.Ordering == 2
                                                          select new BCADTConsortiumTrans
                                                          {
                                                              BCADTConsortiumTrans_OperatedBy = consortiumTrans.OperatedBy,
                                                              BCADTConsortiumTrans_CustomerName = consortiumTrans.CustomerName
                                                          }

                                                          ).FirstOrDefault();
                BCADTConsortiumTrans qConsortiumTrans3 = (from consortiumTrans in db.Set<ConsortiumTrans>()
                                                          where consortiumTrans.RefGUID == refGUID && consortiumTrans.Ordering == 3
                                                          select new BCADTConsortiumTrans
                                                          {
                                                              BCADTConsortiumTrans_OperatedBy = consortiumTrans.OperatedBy,
                                                              BCADTConsortiumTrans_CustomerName = consortiumTrans.CustomerName
                                                          }
                                                          ).FirstOrDefault();
                var qConsortium1_Description = (from consortiumTable in db.Set<ConsortiumTable>()
                                                where consortiumTable.ConsortiumTableGUID.ToString() == qBusinessCollateralAgmTable.BCADTConsortiumTrans_ConsortiumTableGUID
                                                select consortiumTable.Description
                                                ).FirstOrDefault();
                BCADTAgreementTableInfo qAgreementTableInfo = (from agreementTableInfo in db.Set<AgreementTableInfo>()
                                                               where agreementTableInfo.RefType == (int)RefType.BusinessCollateralAgreement
                                                               && agreementTableInfo.RefGUID == refGUID
                                                               select new BCADTAgreementTableInfo
                                                               {
                                                                   BCADTAgreementTableInfo_AuthorizedPersonTransCustomer1GUID = agreementTableInfo.AuthorizedPersonTransCustomer1GUID != null ? agreementTableInfo.AuthorizedPersonTransCustomer1GUID.ToString() : "",
                                                                   BCADTAgreementTableInfo_AuthorizedPersonTransCustomer2GUID = agreementTableInfo.AuthorizedPersonTransCustomer2GUID != null ? agreementTableInfo.AuthorizedPersonTransCustomer2GUID.ToString() : "",
                                                                   BCADTAgreementTableInfo_AuthorizedPersonTransCustomer3GUID = agreementTableInfo.AuthorizedPersonTransCustomer3GUID != null ? agreementTableInfo.AuthorizedPersonTransCustomer3GUID.ToString() : ""
                                                               }
                                                                ).FirstOrDefault();

                var qCustomerAuthorizedPerson1_Name = (from relatedPersonTable in db.Set<RelatedPersonTable>()
                                                       join authorizedPersonTrans in db.Set<AuthorizedPersonTrans>()
                                                       on relatedPersonTable.RelatedPersonTableGUID equals authorizedPersonTrans.RelatedPersonTableGUID
                                                       where authorizedPersonTrans.AuthorizedPersonTransGUID.ToString() == qAgreementTableInfo.BCADTAgreementTableInfo_AuthorizedPersonTransCustomer1GUID
                                                       select relatedPersonTable.Name
                                                       ).FirstOrDefault();
                var qCustomerAuthorizedPerson2_Name = (from relatedPersonTable in db.Set<RelatedPersonTable>()
                                                       join authorizedPersonTrans in db.Set<AuthorizedPersonTrans>()
                                                       on relatedPersonTable.RelatedPersonTableGUID equals authorizedPersonTrans.RelatedPersonTableGUID
                                                       where authorizedPersonTrans.AuthorizedPersonTransGUID.ToString() == qAgreementTableInfo.BCADTAgreementTableInfo_AuthorizedPersonTransCustomer2GUID
                                                       select relatedPersonTable.Name
                                                       ).FirstOrDefault();
                var qCustomerAuthorizedPerson3_Name = (from relatedPersonTable in db.Set<RelatedPersonTable>()
                                                       join authorizedPersonTrans in db.Set<AuthorizedPersonTrans>()
                                                       on relatedPersonTable.RelatedPersonTableGUID equals authorizedPersonTrans.RelatedPersonTableGUID
                                                       where authorizedPersonTrans.AuthorizedPersonTransGUID.ToString() == qAgreementTableInfo.BCADTAgreementTableInfo_AuthorizedPersonTransCustomer3GUID
                                                       select relatedPersonTable.Name
                                                       ).FirstOrDefault();



                BookmarkBusinessCollateralAgreementExpenceDetail model = new BookmarkBusinessCollateralAgreementExpenceDetail();
                model.BCADTbusinessCollateralAgmTable_BusinessCollateralAgmId = qBusinessCollateralAgmTable != null ? qBusinessCollateralAgmTable.BCADTbusinessCollateralAgmTable_BusinessCollateralAgmId : "";
                model.BCADTbusinessCollateralAgmTable_CustomerName = qBusinessCollateralAgmTable != null ? qBusinessCollateralAgmTable.BCADTbusinessCollateralAgmTable_CustomerName : "";
                model.BCADTbusinessCollateralAgmTable_CreditAppTableGUID = qBusinessCollateralAgmTable != null ? qBusinessCollateralAgmTable.BCADTbusinessCollateralAgmTable_CreditAppTableGUID : "";
                model.BCADTAgreementTableInfo_AuthorizedPersonTransCustomer1GUID = qAgreementTableInfo != null ? qAgreementTableInfo.BCADTAgreementTableInfo_AuthorizedPersonTransCustomer1GUID : "";
                model.BCADTAgreementTableInfo_AuthorizedPersonTransCustomer2GUID = qAgreementTableInfo != null ? qAgreementTableInfo.BCADTAgreementTableInfo_AuthorizedPersonTransCustomer2GUID : "";
                model.BCADTAgreementTableInfo_AuthorizedPersonTransCustomer3GUID = qAgreementTableInfo != null ? qAgreementTableInfo.BCADTAgreementTableInfo_AuthorizedPersonTransCustomer3GUID : "";

                model.BCADTCustomerAuthorizedPerson1_Name = qCustomerAuthorizedPerson1_Name;
                model.BCADTCustomerAuthorizedPerson2_Name = qCustomerAuthorizedPerson2_Name;
                model.BCADTCustomerAuthorizedPerson3_Name = qCustomerAuthorizedPerson3_Name;



                model.BCADTConsortium1_Description = qConsortium1_Description;

                model.BCADTConsortiumTrans1_OperatedBy = qConsortiumTrans1 != null ? qConsortiumTrans1.BCADTConsortiumTrans_OperatedBy : "";
                model.BCADTConsortiumTrans1_CustomerName = qConsortiumTrans1 != null ? qConsortiumTrans1.BCADTConsortiumTrans_CustomerName : "";
                model.BCADTConsortiumTrans2_OperatedBy = qConsortiumTrans2 != null ? qConsortiumTrans2.BCADTConsortiumTrans_OperatedBy : "";
                model.BCADTConsortiumTrans2_CustomerName = qConsortiumTrans2 != null ? qConsortiumTrans2.BCADTConsortiumTrans_CustomerName : "";
                model.BCADTConsortiumTrans3_OperatedBy = qConsortiumTrans3 != null ? qConsortiumTrans3.BCADTConsortiumTrans_OperatedBy : "";
                model.BCADTConsortiumTrans3_CustomerName = qConsortiumTrans3 != null ? qConsortiumTrans3.BCADTConsortiumTrans_CustomerName : "";
                model.BCADTCompany_ApprovedCreditLimit = qCreditAppTable != null ? qCreditAppTable.BCADTCreditAppTable_ApprovedCreditLimit : 0;
                model.BCADTCompany_SumApprovedCreditLimitLineRequest = qCreditAppRequestLine != null ? qCreditAppRequestLine.BCADTCreditAppRequestLine_SumApprovedCreditLimitLineRequest : 0;
                model.BCADTCompanyParameter_PFExtendCreditTermGUID = QCompanyParameter_PFExtendCreditTerm != null ? QCompanyParameter_PFExtendCreditTerm : "";

                model.BCADTServiceFeeTrans1_Ordering = QServiceFeeTrans1 != null ? QServiceFeeTrans1.BCADTServiceFeeTrans_Ordering : 0;
                model.BCADTServiceFeeTrans1_Description = QServiceFeeTrans1 != null ? QServiceFeeTrans1.BCADTServiceFeeTrans_Description : "";
                model.BCADTServiceFeeTrans1_AmountBeforeTax = QServiceFeeTrans1 != null ? QServiceFeeTrans1.BCADTServiceFeeTrans_AmountBeforeTax : 0;
                model.BCADTServiceFeeTrans1_TaxAmount = QServiceFeeTrans1 != null ? QServiceFeeTrans1.BCADTServiceFeeTrans_TaxAmount : 0;
                model.BCADTServiceFeeTrans1_AmountIncludeTax = QServiceFeeTrans1 != null ? QServiceFeeTrans1.BCADTServiceFeeTrans_AmountIncludeTax : 0;
                model.BCADTServiceFeeTrans1_WHTAmount = QServiceFeeTrans1 != null ? QServiceFeeTrans1.BCADTServiceFeeTrans_WHTAmount : 0;
                model.BCADTServiceFeeTrans1_LineServiceFeeNet = QServiceFeeTrans1 != null ? QServiceFeeTrans1.BCADTServiceFeeTrans_LineServiceFeeNet : 0;

                model.BCADTServiceFeeTrans2_Ordering = QServiceFeeTrans2 != null ? QServiceFeeTrans2.BCADTServiceFeeTrans_Ordering : 0;
                model.BCADTServiceFeeTrans2_Description = QServiceFeeTrans2 != null ? QServiceFeeTrans2.BCADTServiceFeeTrans_Description : "";
                model.BCADTServiceFeeTrans2_AmountBeforeTax = QServiceFeeTrans2 != null ? QServiceFeeTrans2.BCADTServiceFeeTrans_AmountBeforeTax : 0;
                model.BCADTServiceFeeTrans2_TaxAmount = QServiceFeeTrans2 != null ? QServiceFeeTrans2.BCADTServiceFeeTrans_TaxAmount : 0;
                model.BCADTServiceFeeTrans2_AmountIncludeTax = QServiceFeeTrans2 != null ? QServiceFeeTrans2.BCADTServiceFeeTrans_AmountIncludeTax : 0;
                model.BCADTServiceFeeTrans2_WHTAmount = QServiceFeeTrans2 != null ? QServiceFeeTrans2.BCADTServiceFeeTrans_WHTAmount : 0;
                model.BCADTServiceFeeTrans2_LineServiceFeeNet = QServiceFeeTrans2 != null ? QServiceFeeTrans2.BCADTServiceFeeTrans_LineServiceFeeNet : 0;

                model.BCADTServiceFeeTrans3_Ordering = QServiceFeeTrans3 != null ? QServiceFeeTrans3.BCADTServiceFeeTrans_Ordering : 0;
                model.BCADTServiceFeeTrans3_Description = QServiceFeeTrans3 != null ? QServiceFeeTrans3.BCADTServiceFeeTrans_Description : "";
                model.BCADTServiceFeeTrans3_AmountBeforeTax = QServiceFeeTrans3 != null ? QServiceFeeTrans3.BCADTServiceFeeTrans_AmountBeforeTax : 0;
                model.BCADTServiceFeeTrans3_TaxAmount = QServiceFeeTrans3 != null ? QServiceFeeTrans3.BCADTServiceFeeTrans_TaxAmount : 0;
                model.BCADTServiceFeeTrans3_AmountIncludeTax = QServiceFeeTrans3 != null ? QServiceFeeTrans3.BCADTServiceFeeTrans_AmountIncludeTax : 0;
                model.BCADTServiceFeeTrans3_WHTAmount = QServiceFeeTrans3 != null ? QServiceFeeTrans3.BCADTServiceFeeTrans_WHTAmount : 0;
                model.BCADTServiceFeeTrans3_LineServiceFeeNet = QServiceFeeTrans3 != null ? QServiceFeeTrans3.BCADTServiceFeeTrans_LineServiceFeeNet : 0;

                model.BCADTServiceFeeTrans4_Ordering = QServiceFeeTrans4 != null ? QServiceFeeTrans4.BCADTServiceFeeTrans_Ordering : 0;
                model.BCADTServiceFeeTrans4_Description = QServiceFeeTrans4 != null ? QServiceFeeTrans4.BCADTServiceFeeTrans_Description : "";
                model.BCADTServiceFeeTrans4_AmountBeforeTax = QServiceFeeTrans4 != null ? QServiceFeeTrans4.BCADTServiceFeeTrans_AmountBeforeTax : 0;
                model.BCADTServiceFeeTrans4_TaxAmount = QServiceFeeTrans4 != null ? QServiceFeeTrans4.BCADTServiceFeeTrans_TaxAmount : 0;
                model.BCADTServiceFeeTrans4_AmountIncludeTax = QServiceFeeTrans4 != null ? QServiceFeeTrans4.BCADTServiceFeeTrans_AmountIncludeTax : 0;
                model.BCADTServiceFeeTrans4_WHTAmount = QServiceFeeTrans4 != null ? QServiceFeeTrans4.BCADTServiceFeeTrans_WHTAmount : 0;
                model.BCADTServiceFeeTrans4_LineServiceFeeNet = QServiceFeeTrans4 != null ? QServiceFeeTrans4.BCADTServiceFeeTrans_LineServiceFeeNet : 0;

                model.BCADTServiceFeeTrans5_Ordering = QServiceFeeTrans5 != null ? QServiceFeeTrans5.BCADTServiceFeeTrans_Ordering : 0;
                model.BCADTServiceFeeTrans5_Description = QServiceFeeTrans5 != null ? QServiceFeeTrans5.BCADTServiceFeeTrans_Description : "";
                model.BCADTServiceFeeTrans5_AmountBeforeTax = QServiceFeeTrans5 != null ? QServiceFeeTrans5.BCADTServiceFeeTrans_AmountBeforeTax : 0;
                model.BCADTServiceFeeTrans5_TaxAmount = QServiceFeeTrans5 != null ? QServiceFeeTrans5.BCADTServiceFeeTrans_TaxAmount : 0;
                model.BCADTServiceFeeTrans5_AmountIncludeTax = QServiceFeeTrans5 != null ? QServiceFeeTrans5.BCADTServiceFeeTrans_AmountIncludeTax : 0;
                model.BCADTServiceFeeTrans5_WHTAmount = QServiceFeeTrans5 != null ? QServiceFeeTrans5.BCADTServiceFeeTrans_WHTAmount : 0;
                model.BCADTServiceFeeTrans5_LineServiceFeeNet = QServiceFeeTrans5 != null ? QServiceFeeTrans5.BCADTServiceFeeTrans_LineServiceFeeNet : 0;

                model.BCADTServiceFeeTrans6_Ordering = QServiceFeeTrans6 != null ? QServiceFeeTrans6.BCADTServiceFeeTrans_Ordering : 0;
                model.BCADTServiceFeeTrans6_Description = QServiceFeeTrans6 != null ? QServiceFeeTrans6.BCADTServiceFeeTrans_Description : "";
                model.BCADTServiceFeeTrans6_AmountBeforeTax = QServiceFeeTrans6 != null ? QServiceFeeTrans6.BCADTServiceFeeTrans_AmountBeforeTax : 0;
                model.BCADTServiceFeeTrans6_TaxAmount = QServiceFeeTrans6 != null ? QServiceFeeTrans6.BCADTServiceFeeTrans_TaxAmount : 0;
                model.BCADTServiceFeeTrans6_AmountIncludeTax = QServiceFeeTrans6 != null ? QServiceFeeTrans6.BCADTServiceFeeTrans_AmountIncludeTax : 0;
                model.BCADTServiceFeeTrans6_WHTAmount = QServiceFeeTrans6 != null ? QServiceFeeTrans6.BCADTServiceFeeTrans_WHTAmount : 0;
                model.BCADTServiceFeeTrans6_LineServiceFeeNet = QServiceFeeTrans6 != null ? QServiceFeeTrans6.BCADTServiceFeeTrans_LineServiceFeeNet : 0;

                model.BCADTServiceFeeTrans7_Ordering = QServiceFeeTrans7 != null ? QServiceFeeTrans7.BCADTServiceFeeTrans_Ordering : 0;
                model.BCADTServiceFeeTrans7_Description = QServiceFeeTrans7 != null ? QServiceFeeTrans7.BCADTServiceFeeTrans_Description : "";
                model.BCADTServiceFeeTrans7_AmountBeforeTax = QServiceFeeTrans7 != null ? QServiceFeeTrans7.BCADTServiceFeeTrans_AmountBeforeTax : 0;
                model.BCADTServiceFeeTrans7_TaxAmount = QServiceFeeTrans7 != null ? QServiceFeeTrans7.BCADTServiceFeeTrans_TaxAmount : 0;
                model.BCADTServiceFeeTrans7_AmountIncludeTax = QServiceFeeTrans7 != null ? QServiceFeeTrans7.BCADTServiceFeeTrans_AmountIncludeTax : 0;
                model.BCADTServiceFeeTrans7_WHTAmount = QServiceFeeTrans7 != null ? QServiceFeeTrans7.BCADTServiceFeeTrans_WHTAmount : 0;
                model.BCADTServiceFeeTrans7_LineServiceFeeNet = QServiceFeeTrans7 != null ? QServiceFeeTrans7.BCADTServiceFeeTrans_LineServiceFeeNet : 0;

                model.BCADTServiceFeeTrans8_Ordering = QServiceFeeTrans8 != null ? QServiceFeeTrans8.BCADTServiceFeeTrans_Ordering : 0;
                model.BCADTServiceFeeTrans8_Description = QServiceFeeTrans8 != null ? QServiceFeeTrans8.BCADTServiceFeeTrans_Description : "";
                model.BCADTServiceFeeTrans8_AmountBeforeTax = QServiceFeeTrans8 != null ? QServiceFeeTrans8.BCADTServiceFeeTrans_AmountBeforeTax : 0;
                model.BCADTServiceFeeTrans8_TaxAmount = QServiceFeeTrans8 != null ? QServiceFeeTrans8.BCADTServiceFeeTrans_TaxAmount : 0;
                model.BCADTServiceFeeTrans8_AmountIncludeTax = QServiceFeeTrans8 != null ? QServiceFeeTrans8.BCADTServiceFeeTrans_AmountIncludeTax : 0;
                model.BCADTServiceFeeTrans8_WHTAmount = QServiceFeeTrans8 != null ? QServiceFeeTrans8.BCADTServiceFeeTrans_WHTAmount : 0;
                model.BCADTServiceFeeTrans8_LineServiceFeeNet = QServiceFeeTrans8 != null ? QServiceFeeTrans8.BCADTServiceFeeTrans_LineServiceFeeNet : 0;

                model.BCADTServiceFeeTrans9_Ordering = QServiceFeeTrans9 != null ? QServiceFeeTrans9.BCADTServiceFeeTrans_Ordering : 0;
                model.BCADTServiceFeeTrans9_Description = QServiceFeeTrans9 != null ? QServiceFeeTrans9.BCADTServiceFeeTrans_Description : "";
                model.BCADTServiceFeeTrans9_AmountBeforeTax = QServiceFeeTrans9 != null ? QServiceFeeTrans9.BCADTServiceFeeTrans_AmountBeforeTax : 0;
                model.BCADTServiceFeeTrans9_TaxAmount = QServiceFeeTrans9 != null ? QServiceFeeTrans9.BCADTServiceFeeTrans_TaxAmount : 0;
                model.BCADTServiceFeeTrans9_AmountIncludeTax = QServiceFeeTrans9 != null ? QServiceFeeTrans9.BCADTServiceFeeTrans_AmountIncludeTax : 0;
                model.BCADTServiceFeeTrans9_WHTAmount = QServiceFeeTrans9 != null ? QServiceFeeTrans9.BCADTServiceFeeTrans_WHTAmount : 0;
                model.BCADTServiceFeeTrans9_LineServiceFeeNet = QServiceFeeTrans9 != null ? QServiceFeeTrans9.BCADTServiceFeeTrans_LineServiceFeeNet : 0;

                model.BCADTServiceFeeTrans10_Ordering = QServiceFeeTrans10 != null ? QServiceFeeTrans10.BCADTServiceFeeTrans_Ordering : 0;
                model.BCADTServiceFeeTrans10_Description = QServiceFeeTrans10 != null ? QServiceFeeTrans10.BCADTServiceFeeTrans_Description : "";
                model.BCADTServiceFeeTrans10_AmountBeforeTax = QServiceFeeTrans10 != null ? QServiceFeeTrans10.BCADTServiceFeeTrans_AmountBeforeTax : 0;
                model.BCADTServiceFeeTrans10_TaxAmount = QServiceFeeTrans10 != null ? QServiceFeeTrans10.BCADTServiceFeeTrans_TaxAmount : 0;
                model.BCADTServiceFeeTrans10_AmountIncludeTax = QServiceFeeTrans10 != null ? QServiceFeeTrans10.BCADTServiceFeeTrans_AmountIncludeTax : 0;
                model.BCADTServiceFeeTrans10_WHTAmount = QServiceFeeTrans10 != null ? QServiceFeeTrans10.BCADTServiceFeeTrans_WHTAmount : 0;
                model.BCADTServiceFeeTrans10_LineServiceFeeNet = QServiceFeeTrans10 != null ? QServiceFeeTrans10.BCADTServiceFeeTrans_LineServiceFeeNet : 0;
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                model.TotalServiceFee = serviceFeeTransService.GetTotalAmountByReference(RefType.BusinessCollateralAgreement, refGUID.ToString());
                model.UpcountryChequeFee = serviceFeeTransService.GetUpcountryChequeFee(model.TotalServiceFee);
                model.ExpTransTotalCheck = (model.TotalServiceFee + model.UpcountryChequeFee);
                model.FixValue = 0;

                // R02
                model.AgreementDate = qBusinessCollateralAgmTable != null ? DateToWordCustom(qBusinessCollateralAgmTable.BCADTbusinessCollateralAgmTable_AgreementDate.StringNullToDateNull(), TextConstants.TH, TextConstants.DMY) : "";
                model.TotalLineServiceFeeNet = ((QServiceFeeTrans1 != null ? QServiceFeeTrans1.BCADTServiceFeeTrans_LineServiceFeeNet : 0) +
                                         (QServiceFeeTrans2 != null ? QServiceFeeTrans2.BCADTServiceFeeTrans_LineServiceFeeNet : 0) +
                                         (QServiceFeeTrans3 != null ? QServiceFeeTrans3.BCADTServiceFeeTrans_LineServiceFeeNet : 0) +
                                         (QServiceFeeTrans4 != null ? QServiceFeeTrans4.BCADTServiceFeeTrans_LineServiceFeeNet : 0) +
                                         (QServiceFeeTrans5 != null ? QServiceFeeTrans5.BCADTServiceFeeTrans_LineServiceFeeNet : 0) +
                                         (QServiceFeeTrans6 != null ? QServiceFeeTrans6.BCADTServiceFeeTrans_LineServiceFeeNet : 0) +
                                         (QServiceFeeTrans7 != null ? QServiceFeeTrans7.BCADTServiceFeeTrans_LineServiceFeeNet : 0) +
                                         (QServiceFeeTrans8 != null ? QServiceFeeTrans8.BCADTServiceFeeTrans_LineServiceFeeNet : 0) +
                                         (QServiceFeeTrans9 != null ? QServiceFeeTrans9.BCADTServiceFeeTrans_LineServiceFeeNet : 0) +
                                         (QServiceFeeTrans10 != null ? QServiceFeeTrans10.BCADTServiceFeeTrans_LineServiceFeeNet : 0));

                model.BCADTbusinessCollateralAgmTable_SumLineServiceFeeNet = model.TotalLineServiceFeeNet;
                model.BCADTbusinessCollateralAgmTable_AgreementDate = model.AgreementDate;
                return model;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }

        public BookmarkBusinessCollateralAgmTable GetBookmarkDocumentBusinessCollateralTable(Guid refGUID)
        {
            try
            {
                QBusinessCollateralAgmTable qBusinessCollateralAgmTable = GetQBusinessCollateralAgmTable(refGUID);

                QCustomerTableBusinessCollateralAgreement qCustomerTableBusinessCollateralAgreement = GetQCustomerTableBusinessCollateralAgreement(qBusinessCollateralAgmTable);

                var customerAuthorizedPerson1 = GEtcustomerAuthorizedPerson1(qBusinessCollateralAgmTable);
                var customerAuthorizedPerson2 = GEtcustomerAuthorizedPerson2(qBusinessCollateralAgmTable);
                var customerAuthorizedPerson3 = GEtcustomerAuthorizedPerson3(qBusinessCollateralAgmTable);

                List<QBusinessCollateralAgmLineT1S1> qBusinessCollateralAgmLineT1S1 = GetQBusinessCollateralAgmLineT1S1(refGUID, qBusinessCollateralAgmTable);
                List<QBusinessCollateralAgmLineT1S2> qBusinessCollateralAgmLineT1S2 = GetQBusinessCollateralAgmLineT1S2(refGUID, qBusinessCollateralAgmTable); 
                List<QBusinessCollateralAgmLineT1S3> qBusinessCollateralAgmLineT1S3 = GetQBusinessCollateralAgmLineT1S3(refGUID, qBusinessCollateralAgmTable); 
                List<QBusinessCollateralAgmLineT2S1> qBusinessCollateralAgmLineT2S1 = GetQBusinessCollateralAgmLineT2S1(refGUID, qBusinessCollateralAgmTable);
                List<QBusinessCollateralAgmLineT2S2> qBusinessCollateralAgmLineT2S2 = GetQBusinessCollateralAgmLineT2S2(refGUID, qBusinessCollateralAgmTable); 
                List<QBusinessCollateralAgmLineT3S1> qBusinessCollateralAgmLineT3S1 = GetQBusinessCollateralAgmLineT3S1(refGUID, qBusinessCollateralAgmTable);
                List<QBusinessCollateralAgmLineT3S2> qBusinessCollateralAgmLineT3S2 = GetQBusinessCollateralAgmLineT3S2(refGUID, qBusinessCollateralAgmTable); 
                List<QBusinessCollateralAgmLineT3S3> qBusinessCollateralAgmLineT3S3 = GetQBusinessCollateralAgmLineT3S3(refGUID, qBusinessCollateralAgmTable); 
                List<QBusinessCollateralAgmLineT3S4> qBusinessCollateralAgmLineT3S4 = GetQBusinessCollateralAgmLineT3S4(refGUID, qBusinessCollateralAgmTable);
                List<QBusinessCollateralAgmLineT3S5> qBusinessCollateralAgmLineT3S5 = GetQBusinessCollateralAgmLineT3S5(refGUID, qBusinessCollateralAgmTable); 
                List<QBusinessCollateralAgmLineT4S1> qBusinessCollateralAgmLineT4S1 = GetQBusinessCollateralAgmLineT4S1(refGUID, qBusinessCollateralAgmTable); 
                List<QBusinessCollateralAgmLineT4S2> qBusinessCollateralAgmLineT4S2 = GetQBusinessCollateralAgmLineT4S2(refGUID, qBusinessCollateralAgmTable); 
                List<QBusinessCollateralAgmLineT4S3> qBusinessCollateralAgmLineT4S3 = GetQBusinessCollateralAgmLineT4S3(refGUID, qBusinessCollateralAgmTable); 
                List<QBusinessCollateralAgmLineT5S1> qBusinessCollateralAgmLineT5S1 = GetQBusinessCollateralAgmLineT5S1(refGUID, qBusinessCollateralAgmTable);
                List<QBusinessCollateralAgmLineT5S2> qBusinessCollateralAgmLineT5S2 = GetQBusinessCollateralAgmLineT5S2(refGUID, qBusinessCollateralAgmTable); 
                List<QBusinessCollateralAgmLineT5S3> qBusinessCollateralAgmLineT5S3 = GetQBusinessCollateralAgmLineT5S3(refGUID, qBusinessCollateralAgmTable);
                List<QBusinessCollateralAgmLineT5S4> qBusinessCollateralAgmLineT5S4 = GetQBusinessCollateralAgmLineT5S4(refGUID, qBusinessCollateralAgmTable);
                #region qBCLType_Mark
                var qBCLType1_Mark1 = (from businessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                       join businessCollateralType in db.Set<BusinessCollateralType>()
                                       on businessCollateralAgmLine.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID into ljbusinessCollateralType
                                       from businessCollateralType in ljbusinessCollateralType.DefaultIfEmpty()
                                       where businessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID && businessCollateralType.AgreementOrdering == 1
                                       select businessCollateralAgmLine.BusinessCollateralAgmLineGUID
                                       ).FirstOrDefault();
                var qBCLType2_Mark1 = (from businessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                       join businessCollateralType in db.Set<BusinessCollateralType>()
                                       on businessCollateralAgmLine.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID into ljbusinessCollateralType
                                       from businessCollateralType in ljbusinessCollateralType.DefaultIfEmpty()
                                       where businessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID && businessCollateralType.AgreementOrdering == 2
                                       select businessCollateralAgmLine.BusinessCollateralAgmLineGUID
                                       ).FirstOrDefault();
                var qBCLType3_Mark1 = (from businessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                       join businessCollateralType in db.Set<BusinessCollateralType>()
                                       on businessCollateralAgmLine.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID into ljbusinessCollateralType
                                       from businessCollateralType in ljbusinessCollateralType.DefaultIfEmpty()
                                       where businessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID && businessCollateralType.AgreementOrdering == 3
                                       select businessCollateralAgmLine.BusinessCollateralAgmLineGUID
                                       ).FirstOrDefault();
                var qBCLType4_Mark1 = (from businessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                       join businessCollateralType in db.Set<BusinessCollateralType>()
                                       on businessCollateralAgmLine.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID into ljbusinessCollateralType
                                       from businessCollateralType in ljbusinessCollateralType.DefaultIfEmpty()
                                       where businessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID && businessCollateralType.AgreementOrdering == 4
                                       select businessCollateralAgmLine.BusinessCollateralAgmLineGUID
                                       ).FirstOrDefault();
                var qBCLType5_Mark1 = (from businessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                       join businessCollateralType in db.Set<BusinessCollateralType>()
                                       on businessCollateralAgmLine.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID into ljbusinessCollateralType
                                       from businessCollateralType in ljbusinessCollateralType.DefaultIfEmpty()
                                       where businessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID && businessCollateralType.AgreementOrdering == 5
                                       select businessCollateralAgmLine.BusinessCollateralAgmLineGUID
                                       ).FirstOrDefault();
               
                QCompanyBusinessCollateralAgreement qCompanyBusinessCollateralAgreement = new QCompanyBusinessCollateralAgreement();
                qCompanyBusinessCollateralAgreement = (from company in db.Set<Company>()
                                                       join addressTrans in db.Set<AddressTrans>()
                                                       on company.DefaultBranchGUID equals addressTrans.RefGUID
                                                       where company.CompanyGUID.ToString() == qBusinessCollateralAgmTable.QBusinessCollateralAgmTable_CompanyGUID
                                                       select new QCompanyBusinessCollateralAgreement
                                                       {
                                                           QCompanyBusinessCollateralAgreement_Company_Name = addressTrans.Name,
                                                           QCompanyBusinessCollateralAgreement_Company_Address = addressTrans.Address1 + " " + addressTrans.Address2,
                                                       }
                                                                                          ).FirstOrDefault();
                var companyParameter = (from companyParam in db.Set<CompanyParameter>()
                                        where companyParam.CompanyGUID.ToString() == qBusinessCollateralAgmTable.QBusinessCollateralAgmTable_CompanyGUID
                                        select companyParam.MaxInterestPct
                                        ).FirstOrDefault();
                #endregion qBCLType_Mark
                #region assign to model
                ISharedService sharedService = new SharedService();
                BookmarkBusinessCollateralAgmTable model = new BookmarkBusinessCollateralAgmTable();

                GetGeneralCompany(model, qCompanyBusinessCollateralAgreement);


                GetGeneralBusinesCalateral(model, qBusinessCollateralAgmTable);


                GetGeneralBusinesCalateralAgmLineT1S1(model, qBusinessCollateralAgmLineT1S1);

                GetQCustomerTableBusinessCollateralAgreement(model, qCustomerTableBusinessCollateralAgreement);

                model.QBusinessCollateralAgmLineT1S1_AccountNumber = GetQBusinessCollateralAgmLineT1S1_AccountNumber(qBusinessCollateralAgmLineT1S1, 1);
                model.QBusinessCollateralAgmLineT1S1_AccountNumber_Row_2 = GetQBusinessCollateralAgmLineT1S1_AccountNumber(qBusinessCollateralAgmLineT1S1, 2);
                model.QBusinessCollateralAgmLineT1S1_AccountNumber_Row_3 = GetQBusinessCollateralAgmLineT1S1_AccountNumber(qBusinessCollateralAgmLineT1S1, 3);
                model.QBusinessCollateralAgmLineT1S1_AccountNumber_Row_4 = GetQBusinessCollateralAgmLineT1S1_AccountNumber(qBusinessCollateralAgmLineT1S1, 4);
                model.QBusinessCollateralAgmLineT1S1_AccountNumber_Row_5 = GetQBusinessCollateralAgmLineT1S1_AccountNumber(qBusinessCollateralAgmLineT1S1, 5);

                model.QBusinessCollateralAgmLineT1S1_BankGroup = GetQBusinessCollateralAgmLineT1S1_BankGroup(qBusinessCollateralAgmLineT1S1, 1);
                model.QBusinessCollateralAgmLineT1S1_BankGroup_Row_2 = GetQBusinessCollateralAgmLineT1S1_BankGroup(qBusinessCollateralAgmLineT1S1, 2);
                model.QBusinessCollateralAgmLineT1S1_BankGroup_Row_3 = GetQBusinessCollateralAgmLineT1S1_BankGroup(qBusinessCollateralAgmLineT1S1, 3);
                model.QBusinessCollateralAgmLineT1S1_BankGroup_Row_4 = GetQBusinessCollateralAgmLineT1S1_BankGroup(qBusinessCollateralAgmLineT1S1, 4);
                model.QBusinessCollateralAgmLineT1S1_BankGroup_Row_5 = GetQBusinessCollateralAgmLineT1S1_BankGroup(qBusinessCollateralAgmLineT1S1, 5);

                model.QBusinessCollateralAgmLineT1S1_BankType = GetQBusinessCollateralAgmLineT1S1_BankType(qBusinessCollateralAgmLineT1S1, 1);
                model.QBusinessCollateralAgmLineT1S1_BankType_Row_2 = GetQBusinessCollateralAgmLineT1S1_BankType(qBusinessCollateralAgmLineT1S1, 2);
                model.QBusinessCollateralAgmLineT1S1_BankType_Row_3 = GetQBusinessCollateralAgmLineT1S1_BankType(qBusinessCollateralAgmLineT1S1, 3);
                model.QBusinessCollateralAgmLineT1S1_BankType_Row_4 = GetQBusinessCollateralAgmLineT1S1_BankType(qBusinessCollateralAgmLineT1S1, 4);
                model.QBusinessCollateralAgmLineT1S1_BankType_Row_5 = GetQBusinessCollateralAgmLineT1S1_BankType(qBusinessCollateralAgmLineT1S1, 5);



                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                GetGeneralBusinesCalateralAgmLineT1S2(model, qBusinessCollateralAgmLineT1S2);

                model.QBusinessCollateralAgmLineT1S2_RefAgreementId = GetQBusinessCollateralAgmLineT1S2_RefAgreementId(qBusinessCollateralAgmLineT1S2, 1);
                model.QBusinessCollateralAgmLineT1S2_RefAgreementId_Row_2 = GetQBusinessCollateralAgmLineT1S2_RefAgreementId(qBusinessCollateralAgmLineT1S2,2);
                model.QBusinessCollateralAgmLineT1S2_RefAgreementId_Row_3 = GetQBusinessCollateralAgmLineT1S2_RefAgreementId(qBusinessCollateralAgmLineT1S2, 3);
                model.QBusinessCollateralAgmLineT1S2_RefAgreementId_Row_4 = GetQBusinessCollateralAgmLineT1S2_RefAgreementId(qBusinessCollateralAgmLineT1S2, 4);
                model.QBusinessCollateralAgmLineT1S2_RefAgreementId_Row_5 = GetQBusinessCollateralAgmLineT1S2_RefAgreementId(qBusinessCollateralAgmLineT1S2, 5);
                model.QBusinessCollateralAgmLineT1S2_RefAgreementId_Row_6 = GetQBusinessCollateralAgmLineT1S2_RefAgreementId(qBusinessCollateralAgmLineT1S2, 6);
                model.QBusinessCollateralAgmLineT1S2_RefAgreementId_Row_7 = GetQBusinessCollateralAgmLineT1S2_RefAgreementId(qBusinessCollateralAgmLineT1S2, 7);
                model.QBusinessCollateralAgmLineT1S2_RefAgreementId_Row_8 = GetQBusinessCollateralAgmLineT1S2_RefAgreementId(qBusinessCollateralAgmLineT1S2, 8);
                model.QBusinessCollateralAgmLineT1S2_RefAgreementId_Row_9 = GetQBusinessCollateralAgmLineT1S2_RefAgreementId(qBusinessCollateralAgmLineT1S2, 9);
                model.QBusinessCollateralAgmLineT1S2_RefAgreementId_Row_10 = GetQBusinessCollateralAgmLineT1S2_RefAgreementId(qBusinessCollateralAgmLineT1S2, 10);



                model.QBusinessCollateralAgmLineT1S2_RefAgreementDate = GetQBusinessCollateralAgmLineT1S2_RefAgreementDate(qBusinessCollateralAgmLineT1S2, 1);
                model.QBusinessCollateralAgmLineT1S2_RefAgreementDate_Row_2 = GetQBusinessCollateralAgmLineT1S2_RefAgreementDate(qBusinessCollateralAgmLineT1S2, 2);
                model.QBusinessCollateralAgmLineT1S2_RefAgreementDate_Row_3 = GetQBusinessCollateralAgmLineT1S2_RefAgreementDate(qBusinessCollateralAgmLineT1S2, 3);
                model.QBusinessCollateralAgmLineT1S2_RefAgreementDate_Row_4 = GetQBusinessCollateralAgmLineT1S2_RefAgreementDate(qBusinessCollateralAgmLineT1S2, 4);
                model.QBusinessCollateralAgmLineT1S2_RefAgreementDate_Row_5 = GetQBusinessCollateralAgmLineT1S2_RefAgreementDate(qBusinessCollateralAgmLineT1S2, 5);
                model.QBusinessCollateralAgmLineT1S2_RefAgreementDate_Row_6 = GetQBusinessCollateralAgmLineT1S2_RefAgreementDate(qBusinessCollateralAgmLineT1S2, 6);
                model.QBusinessCollateralAgmLineT1S2_RefAgreementDate_Row_7 = GetQBusinessCollateralAgmLineT1S2_RefAgreementDate(qBusinessCollateralAgmLineT1S2, 7);
                model.QBusinessCollateralAgmLineT1S2_RefAgreementDate_Row_8 = GetQBusinessCollateralAgmLineT1S2_RefAgreementDate(qBusinessCollateralAgmLineT1S2, 8);
                model.QBusinessCollateralAgmLineT1S2_RefAgreementDate_Row_9 = GetQBusinessCollateralAgmLineT1S2_RefAgreementDate(qBusinessCollateralAgmLineT1S2, 9);
                model.QBusinessCollateralAgmLineT1S2_RefAgreementDate_Row_10 = GetQBusinessCollateralAgmLineT1S2_RefAgreementDate(qBusinessCollateralAgmLineT1S2, 10);



                model.QBusinessCollateralAgmLineT1S2_Lessee = GetQBusinessCollateralAgmLineT1S2_Lessee(qBusinessCollateralAgmLineT1S2, 1);
                model.QBusinessCollateralAgmLineT1S2_Lessee_Row_2 = GetQBusinessCollateralAgmLineT1S2_Lessee(qBusinessCollateralAgmLineT1S2, 2);
                model.QBusinessCollateralAgmLineT1S2_Lessee_Row_3 = GetQBusinessCollateralAgmLineT1S2_Lessee(qBusinessCollateralAgmLineT1S2, 3);
                model.QBusinessCollateralAgmLineT1S2_Lessee_Row_4 = GetQBusinessCollateralAgmLineT1S2_Lessee(qBusinessCollateralAgmLineT1S2, 4);
                model.QBusinessCollateralAgmLineT1S2_Lessee_Row_5 = GetQBusinessCollateralAgmLineT1S2_Lessee(qBusinessCollateralAgmLineT1S2, 5);
                model.QBusinessCollateralAgmLineT1S2_Lessee_Row_6 = GetQBusinessCollateralAgmLineT1S2_Lessee(qBusinessCollateralAgmLineT1S2, 6);
                model.QBusinessCollateralAgmLineT1S2_Lessee_Row_7 = GetQBusinessCollateralAgmLineT1S2_Lessee(qBusinessCollateralAgmLineT1S2, 7);
                model.QBusinessCollateralAgmLineT1S2_Lessee_Row_8 = GetQBusinessCollateralAgmLineT1S2_Lessee(qBusinessCollateralAgmLineT1S2, 8);
                model.QBusinessCollateralAgmLineT1S2_Lessee_Row_9 = GetQBusinessCollateralAgmLineT1S2_Lessee(qBusinessCollateralAgmLineT1S2, 9);
                model.QBusinessCollateralAgmLineT1S2_Lessee_Row_10 = GetQBusinessCollateralAgmLineT1S2_Lessee(qBusinessCollateralAgmLineT1S2, 10);



                model.QBusinessCollateralAgmLineT1S2_Lessor = GetQBusinessCollateralAgmLineT1S2_Lessor(qBusinessCollateralAgmLineT1S2, 1);
                model.QBusinessCollateralAgmLineT1S2_Lessor_Row_2 = GetQBusinessCollateralAgmLineT1S2_Lessor(qBusinessCollateralAgmLineT1S2, 2);
                model.QBusinessCollateralAgmLineT1S2_Lessor_Row_3 = GetQBusinessCollateralAgmLineT1S2_Lessor(qBusinessCollateralAgmLineT1S2, 3);
                model.QBusinessCollateralAgmLineT1S2_Lessor_Row_4 = GetQBusinessCollateralAgmLineT1S2_Lessor(qBusinessCollateralAgmLineT1S2, 4);
                model.QBusinessCollateralAgmLineT1S2_Lessor_Row_5 = GetQBusinessCollateralAgmLineT1S2_Lessor(qBusinessCollateralAgmLineT1S2, 5);
                model.QBusinessCollateralAgmLineT1S2_Lessor_Row_6 = GetQBusinessCollateralAgmLineT1S2_Lessor(qBusinessCollateralAgmLineT1S2, 6);
                model.QBusinessCollateralAgmLineT1S2_Lessor_Row_7 = GetQBusinessCollateralAgmLineT1S2_Lessor(qBusinessCollateralAgmLineT1S2, 7);
                model.QBusinessCollateralAgmLineT1S2_Lessor_Row_8 = GetQBusinessCollateralAgmLineT1S2_Lessor(qBusinessCollateralAgmLineT1S2, 8);
                model.QBusinessCollateralAgmLineT1S2_Lessor_Row_9 = GetQBusinessCollateralAgmLineT1S2_Lessor(qBusinessCollateralAgmLineT1S2, 9);
                model.QBusinessCollateralAgmLineT1S2_Lessor_Row_10 = GetQBusinessCollateralAgmLineT1S2_Lessor(qBusinessCollateralAgmLineT1S2, 10);




                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                GetGeneralBusinesCalateralAgmLineT1S3(model, qBusinessCollateralAgmLineT1S3);

                model.QBusinessCollateralAgmLineT1S3_Description = GetQBusinessCollateralAgmLineT1S3_Description(qBusinessCollateralAgmLineT1S3, 1);
                model.QBusinessCollateralAgmLineT1S3_Description_Row_2 = GetQBusinessCollateralAgmLineT1S3_Description(qBusinessCollateralAgmLineT1S3,2);
                model.QBusinessCollateralAgmLineT1S3_Description_Row_3 = GetQBusinessCollateralAgmLineT1S3_Description(qBusinessCollateralAgmLineT1S3, 3);
                model.QBusinessCollateralAgmLineT1S3_Description_Row_4 = GetQBusinessCollateralAgmLineT1S3_Description(qBusinessCollateralAgmLineT1S3, 4);
                model.QBusinessCollateralAgmLineT1S3_Description_Row_5 = GetQBusinessCollateralAgmLineT1S3_Description(qBusinessCollateralAgmLineT1S3, 5);
                model.QBusinessCollateralAgmLineT1S3_Description_Row_6 = GetQBusinessCollateralAgmLineT1S3_Description(qBusinessCollateralAgmLineT1S3, 6);
                model.QBusinessCollateralAgmLineT1S3_Description_Row_7 = GetQBusinessCollateralAgmLineT1S3_Description(qBusinessCollateralAgmLineT1S3, 7);
                model.QBusinessCollateralAgmLineT1S3_Description_Row_8 = GetQBusinessCollateralAgmLineT1S3_Description(qBusinessCollateralAgmLineT1S3, 8);
                model.QBusinessCollateralAgmLineT1S3_Description_Row_9 = GetQBusinessCollateralAgmLineT1S3_Description(qBusinessCollateralAgmLineT1S3, 9);
                model.QBusinessCollateralAgmLineT1S3_Description_Row_10 = GetQBusinessCollateralAgmLineT1S3_Description(qBusinessCollateralAgmLineT1S3, 10);


                model.QBusinessCollateralAgmLineT1S3_RefAgreementId = GetQBusinessCollateralAgmLineT1S3_RefAgreementId(qBusinessCollateralAgmLineT1S3, 1);
                model.QBusinessCollateralAgmLineT1S3_RefAgreementId_Row_2 = GetQBusinessCollateralAgmLineT1S3_RefAgreementId(qBusinessCollateralAgmLineT1S3, 2);
                model.QBusinessCollateralAgmLineT1S3_RefAgreementId_Row_3 = GetQBusinessCollateralAgmLineT1S3_RefAgreementId(qBusinessCollateralAgmLineT1S3, 3);
                model.QBusinessCollateralAgmLineT1S3_RefAgreementId_Row_4 = GetQBusinessCollateralAgmLineT1S3_RefAgreementId(qBusinessCollateralAgmLineT1S3, 4);
                model.QBusinessCollateralAgmLineT1S3_RefAgreementId_Row_5 = GetQBusinessCollateralAgmLineT1S3_RefAgreementId(qBusinessCollateralAgmLineT1S3, 5);
                model.QBusinessCollateralAgmLineT1S3_RefAgreementId_Row_6 = GetQBusinessCollateralAgmLineT1S3_RefAgreementId(qBusinessCollateralAgmLineT1S3, 6);
                model.QBusinessCollateralAgmLineT1S3_RefAgreementId_Row_7 = GetQBusinessCollateralAgmLineT1S3_RefAgreementId(qBusinessCollateralAgmLineT1S3, 7);
                model.QBusinessCollateralAgmLineT1S3_RefAgreementId_Row_8 = GetQBusinessCollateralAgmLineT1S3_RefAgreementId(qBusinessCollateralAgmLineT1S3, 8);
                model.QBusinessCollateralAgmLineT1S3_RefAgreementId_Row_9 = GetQBusinessCollateralAgmLineT1S3_RefAgreementId(qBusinessCollateralAgmLineT1S3, 9);
                model.QBusinessCollateralAgmLineT1S3_RefAgreementId_Row_10 = GetQBusinessCollateralAgmLineT1S3_RefAgreementId(qBusinessCollateralAgmLineT1S3, 10);



                model.QBusinessCollateralAgmLineT1S3_RefAgreementDate = GetQBusinessCollateralAgmLineT1S3_RefAgreementDate(qBusinessCollateralAgmLineT1S3, 1);
                model.QBusinessCollateralAgmLineT1S3_RefAgreementDate_Row_2 = GetQBusinessCollateralAgmLineT1S3_RefAgreementDate(qBusinessCollateralAgmLineT1S3, 2);
                model.QBusinessCollateralAgmLineT1S3_RefAgreementDate_Row_3 = GetQBusinessCollateralAgmLineT1S3_RefAgreementDate(qBusinessCollateralAgmLineT1S3, 3);
                model.QBusinessCollateralAgmLineT1S3_RefAgreementDate_Row_4 = GetQBusinessCollateralAgmLineT1S3_RefAgreementDate(qBusinessCollateralAgmLineT1S3, 4);
                model.QBusinessCollateralAgmLineT1S3_RefAgreementDate_Row_5 = GetQBusinessCollateralAgmLineT1S3_RefAgreementDate(qBusinessCollateralAgmLineT1S3, 5);
                model.QBusinessCollateralAgmLineT1S3_RefAgreementDate_Row_6 = GetQBusinessCollateralAgmLineT1S3_RefAgreementDate(qBusinessCollateralAgmLineT1S3, 6);
                model.QBusinessCollateralAgmLineT1S3_RefAgreementDate_Row_7 = GetQBusinessCollateralAgmLineT1S3_RefAgreementDate(qBusinessCollateralAgmLineT1S3, 7);
                model.QBusinessCollateralAgmLineT1S3_RefAgreementDate_Row_8 = GetQBusinessCollateralAgmLineT1S3_RefAgreementDate(qBusinessCollateralAgmLineT1S3, 8);
                model.QBusinessCollateralAgmLineT1S3_RefAgreementDate_Row_9 = GetQBusinessCollateralAgmLineT1S3_RefAgreementDate(qBusinessCollateralAgmLineT1S3, 9);
                model.QBusinessCollateralAgmLineT1S3_RefAgreementDate_Row_10 = GetQBusinessCollateralAgmLineT1S3_RefAgreementDate(qBusinessCollateralAgmLineT1S3, 10);


                model.QBusinessCollateralAgmLineT1S3_Lessee = GetQBusinessCollateralAgmLineT1S3_Lessee(qBusinessCollateralAgmLineT1S3, 1);
                model.QBusinessCollateralAgmLineT1S3_Lessee_Row_2 = GetQBusinessCollateralAgmLineT1S3_Lessee(qBusinessCollateralAgmLineT1S3, 2);
                model.QBusinessCollateralAgmLineT1S3_Lessee_Row_3 = GetQBusinessCollateralAgmLineT1S3_Lessee(qBusinessCollateralAgmLineT1S3, 3);
                model.QBusinessCollateralAgmLineT1S3_Lessee_Row_4 = GetQBusinessCollateralAgmLineT1S3_Lessee(qBusinessCollateralAgmLineT1S3, 4);
                model.QBusinessCollateralAgmLineT1S3_Lessee_Row_5 = GetQBusinessCollateralAgmLineT1S3_Lessee(qBusinessCollateralAgmLineT1S3, 5);
                model.QBusinessCollateralAgmLineT1S3_Lessee_Row_6 = GetQBusinessCollateralAgmLineT1S3_Lessee(qBusinessCollateralAgmLineT1S3, 6);
                model.QBusinessCollateralAgmLineT1S3_Lessee_Row_7 = GetQBusinessCollateralAgmLineT1S3_Lessee(qBusinessCollateralAgmLineT1S3, 7);
                model.QBusinessCollateralAgmLineT1S3_Lessee_Row_8 = GetQBusinessCollateralAgmLineT1S3_Lessee(qBusinessCollateralAgmLineT1S3, 8);
                model.QBusinessCollateralAgmLineT1S3_Lessee_Row_9 = GetQBusinessCollateralAgmLineT1S3_Lessee(qBusinessCollateralAgmLineT1S3, 9);
                model.QBusinessCollateralAgmLineT1S3_Lessee_Row_10 = GetQBusinessCollateralAgmLineT1S3_Lessee(qBusinessCollateralAgmLineT1S3, 10);



                model.QBusinessCollateralAgmLineT1S3_Lessor = GetQBusinessCollateralAgmLineT1S3_Lessor(qBusinessCollateralAgmLineT1S3, 1);
                model.QBusinessCollateralAgmLineT1S3_Lessor_Row_2 = GetQBusinessCollateralAgmLineT1S3_Lessor(qBusinessCollateralAgmLineT1S3, 2);
                model.QBusinessCollateralAgmLineT1S3_Lessor_Row_3 = GetQBusinessCollateralAgmLineT1S3_Lessor(qBusinessCollateralAgmLineT1S3, 3);
                model.QBusinessCollateralAgmLineT1S3_Lessor_Row_4 = GetQBusinessCollateralAgmLineT1S3_Lessor(qBusinessCollateralAgmLineT1S3, 4);
                model.QBusinessCollateralAgmLineT1S3_Lessor_Row_5 = GetQBusinessCollateralAgmLineT1S3_Lessor(qBusinessCollateralAgmLineT1S3, 5);
                model.QBusinessCollateralAgmLineT1S3_Lessor_Row_6 = GetQBusinessCollateralAgmLineT1S3_Lessor(qBusinessCollateralAgmLineT1S3, 6);
                model.QBusinessCollateralAgmLineT1S3_Lessor_Row_7 = GetQBusinessCollateralAgmLineT1S3_Lessor(qBusinessCollateralAgmLineT1S3, 7);
                model.QBusinessCollateralAgmLineT1S3_Lessor_Row_8 = GetQBusinessCollateralAgmLineT1S3_Lessor(qBusinessCollateralAgmLineT1S3, 8);
                model.QBusinessCollateralAgmLineT1S3_Lessor_Row_9 = GetQBusinessCollateralAgmLineT1S3_Lessor(qBusinessCollateralAgmLineT1S3, 9);
                model.QBusinessCollateralAgmLineT1S3_Lessor_Row_10 = GetQBusinessCollateralAgmLineT1S3_Lessor(qBusinessCollateralAgmLineT1S3, 10);

                model.QBusinessCollateralAgmLineT1S3_Product = GetQBusinessCollateralAgmLineT1S3_Product(qBusinessCollateralAgmLineT1S3, 1);
                model.QBusinessCollateralAgmLineT1S3_Product_Row_2 = GetQBusinessCollateralAgmLineT1S3_Product(qBusinessCollateralAgmLineT1S3, 2);
                model.QBusinessCollateralAgmLineT1S3_Product_Row_3 = GetQBusinessCollateralAgmLineT1S3_Product(qBusinessCollateralAgmLineT1S3, 3);
                model.QBusinessCollateralAgmLineT1S3_Product_Row_4 = GetQBusinessCollateralAgmLineT1S3_Product(qBusinessCollateralAgmLineT1S3, 4);
                model.QBusinessCollateralAgmLineT1S3_Product_Row_5 = GetQBusinessCollateralAgmLineT1S3_Product(qBusinessCollateralAgmLineT1S3, 5);
                model.QBusinessCollateralAgmLineT1S3_Product_Row_6 = GetQBusinessCollateralAgmLineT1S3_Product(qBusinessCollateralAgmLineT1S3, 6);
                model.QBusinessCollateralAgmLineT1S3_Product_Row_7 = GetQBusinessCollateralAgmLineT1S3_Product(qBusinessCollateralAgmLineT1S3, 7);
                model.QBusinessCollateralAgmLineT1S3_Product_Row_8 = GetQBusinessCollateralAgmLineT1S3_Product(qBusinessCollateralAgmLineT1S3, 8);
                model.QBusinessCollateralAgmLineT1S3_Product_Row_9 = GetQBusinessCollateralAgmLineT1S3_Product(qBusinessCollateralAgmLineT1S3, 9);
                model.QBusinessCollateralAgmLineT1S3_Product_Row_10 = GetQBusinessCollateralAgmLineT1S3_Product(qBusinessCollateralAgmLineT1S3, 10);



                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                GetGeneralBusinesCalateralAgmLineT2S1(model, qBusinessCollateralAgmLineT2S1);

                model.QBusinessCollateralAgmLineT2S1_BuyerName_Row_1 = GetQBusinessCollateralAgmLineT2S1_BuyerName(qBusinessCollateralAgmLineT2S1, 1);
                model.QBusinessCollateralAgmLineT2S1_BuyerName_Row_2 = GetQBusinessCollateralAgmLineT2S1_BuyerName(qBusinessCollateralAgmLineT2S1, 2);
                model.QBusinessCollateralAgmLineT2S1_BuyerName_Row_3 = GetQBusinessCollateralAgmLineT2S1_BuyerName(qBusinessCollateralAgmLineT2S1, 3);
                model.QBusinessCollateralAgmLineT2S1_BuyerName_Row_4 = GetQBusinessCollateralAgmLineT2S1_BuyerName(qBusinessCollateralAgmLineT2S1, 4);
                model.QBusinessCollateralAgmLineT2S1_BuyerName_Row_5 = GetQBusinessCollateralAgmLineT2S1_BuyerName(qBusinessCollateralAgmLineT2S1, 5);
                model.QBusinessCollateralAgmLineT2S1_BuyerName_Row_6 = GetQBusinessCollateralAgmLineT2S1_BuyerName(qBusinessCollateralAgmLineT2S1, 6);
                model.QBusinessCollateralAgmLineT2S1_BuyerName_Row_7 = GetQBusinessCollateralAgmLineT2S1_BuyerName(qBusinessCollateralAgmLineT2S1, 7);
                model.QBusinessCollateralAgmLineT2S1_BuyerName_Row_8 = GetQBusinessCollateralAgmLineT2S1_BuyerName(qBusinessCollateralAgmLineT2S1, 8);
                model.QBusinessCollateralAgmLineT2S1_BuyerName_Row_9 = GetQBusinessCollateralAgmLineT2S1_BuyerName(qBusinessCollateralAgmLineT2S1, 9);
                model.QBusinessCollateralAgmLineT2S1_BuyerName_Row_10 = GetQBusinessCollateralAgmLineT2S1_BuyerName(qBusinessCollateralAgmLineT2S1, 10);

                model.QBusinessCollateralAgmLineT2S1_IdentificationType_Row_1 = GetQBusinessCollateralAgmLineT2S1_IdentificationType(qBusinessCollateralAgmLineT2S1, 1);
                model.QBusinessCollateralAgmLineT2S1_IdentificationType_Row_2 = GetQBusinessCollateralAgmLineT2S1_IdentificationType(qBusinessCollateralAgmLineT2S1, 2);
                model.QBusinessCollateralAgmLineT2S1_IdentificationType_Row_3 = GetQBusinessCollateralAgmLineT2S1_IdentificationType(qBusinessCollateralAgmLineT2S1, 3);
                model.QBusinessCollateralAgmLineT2S1_IdentificationType_Row_4 = GetQBusinessCollateralAgmLineT2S1_IdentificationType(qBusinessCollateralAgmLineT2S1, 4);
                model.QBusinessCollateralAgmLineT2S1_IdentificationType_Row_5 = GetQBusinessCollateralAgmLineT2S1_IdentificationType(qBusinessCollateralAgmLineT2S1, 5);
                model.QBusinessCollateralAgmLineT2S1_IdentificationType_Row_6 = GetQBusinessCollateralAgmLineT2S1_IdentificationType(qBusinessCollateralAgmLineT2S1, 6);
                model.QBusinessCollateralAgmLineT2S1_IdentificationType_Row_7 = GetQBusinessCollateralAgmLineT2S1_IdentificationType(qBusinessCollateralAgmLineT2S1, 7);
                model.QBusinessCollateralAgmLineT2S1_IdentificationType_Row_8 = GetQBusinessCollateralAgmLineT2S1_IdentificationType(qBusinessCollateralAgmLineT2S1, 8);
                model.QBusinessCollateralAgmLineT2S1_IdentificationType_Row_9 = GetQBusinessCollateralAgmLineT2S1_IdentificationType(qBusinessCollateralAgmLineT2S1, 9);
                model.QBusinessCollateralAgmLineT2S1_IdentificationType_Row_10 = GetQBusinessCollateralAgmLineT2S1_IdentificationType(qBusinessCollateralAgmLineT2S1, 10);


                model.QBusinessCollateralAgmLineT2S1_TaxId_Row_1 = GetQBusinessCollateralAgmLineT2S1_TaxId(qBusinessCollateralAgmLineT2S1, 1);
                model.QBusinessCollateralAgmLineT2S1_TaxId_Row_2 = GetQBusinessCollateralAgmLineT2S1_TaxId(qBusinessCollateralAgmLineT2S1, 2);
                model.QBusinessCollateralAgmLineT2S1_TaxId_Row_3 = GetQBusinessCollateralAgmLineT2S1_TaxId(qBusinessCollateralAgmLineT2S1, 3);
                model.QBusinessCollateralAgmLineT2S1_TaxId_Row_4 = GetQBusinessCollateralAgmLineT2S1_TaxId(qBusinessCollateralAgmLineT2S1, 4);
                model.QBusinessCollateralAgmLineT2S1_TaxId_Row_5 = GetQBusinessCollateralAgmLineT2S1_TaxId(qBusinessCollateralAgmLineT2S1, 5);
                model.QBusinessCollateralAgmLineT2S1_TaxId_Row_6 = GetQBusinessCollateralAgmLineT2S1_TaxId(qBusinessCollateralAgmLineT2S1, 6);
                model.QBusinessCollateralAgmLineT2S1_TaxId_Row_7 = GetQBusinessCollateralAgmLineT2S1_TaxId(qBusinessCollateralAgmLineT2S1, 7);
                model.QBusinessCollateralAgmLineT2S1_TaxId_Row_8 = GetQBusinessCollateralAgmLineT2S1_TaxId(qBusinessCollateralAgmLineT2S1, 8);
                model.QBusinessCollateralAgmLineT2S1_TaxId_Row_9 = GetQBusinessCollateralAgmLineT2S1_TaxId(qBusinessCollateralAgmLineT2S1, 9);
                model.QBusinessCollateralAgmLineT2S1_TaxId_Row_10 = GetQBusinessCollateralAgmLineT2S1_TaxId(qBusinessCollateralAgmLineT2S1, 10);

                model.QBusinessCollateralAgmLineT2S1_PassportId_Row_1 = GetQBusinessCollateralAgmLineT2S1_PassportId(qBusinessCollateralAgmLineT2S1, 1);
                model.QBusinessCollateralAgmLineT2S1_PassportId_Row_2 = GetQBusinessCollateralAgmLineT2S1_PassportId(qBusinessCollateralAgmLineT2S1, 2);
                model.QBusinessCollateralAgmLineT2S1_PassportId_Row_3 = GetQBusinessCollateralAgmLineT2S1_PassportId(qBusinessCollateralAgmLineT2S1, 3);
                model.QBusinessCollateralAgmLineT2S1_PassportId_Row_4 = GetQBusinessCollateralAgmLineT2S1_PassportId(qBusinessCollateralAgmLineT2S1, 4);
                model.QBusinessCollateralAgmLineT2S1_PassportId_Row_5 = GetQBusinessCollateralAgmLineT2S1_PassportId(qBusinessCollateralAgmLineT2S1, 5);
                model.QBusinessCollateralAgmLineT2S1_PassportId_Row_6 = GetQBusinessCollateralAgmLineT2S1_PassportId(qBusinessCollateralAgmLineT2S1, 6);
                model.QBusinessCollateralAgmLineT2S1_PassportId_Row_7 = GetQBusinessCollateralAgmLineT2S1_PassportId(qBusinessCollateralAgmLineT2S1, 7);
                model.QBusinessCollateralAgmLineT2S1_PassportId_Row_8 = GetQBusinessCollateralAgmLineT2S1_PassportId(qBusinessCollateralAgmLineT2S1, 8);
                model.QBusinessCollateralAgmLineT2S1_PassportId_Row_9 = GetQBusinessCollateralAgmLineT2S1_PassportId(qBusinessCollateralAgmLineT2S1, 9);
                model.QBusinessCollateralAgmLineT2S1_PassportId_Row_10 = GetQBusinessCollateralAgmLineT2S1_PassportId(qBusinessCollateralAgmLineT2S1, 10);

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                GetGeneralBusinesCalateralAgmLineT2S2(model, qBusinessCollateralAgmLineT2S2);


                model.QBusinessCollateralAgmLineT2S2_BuyerName_Row_1 = GetQBusinessCollateralAgmLineT2S2_BuyerName(qBusinessCollateralAgmLineT2S2, 1);
                model.QBusinessCollateralAgmLineT2S2_BuyerName_Row_2 = GetQBusinessCollateralAgmLineT2S2_BuyerName(qBusinessCollateralAgmLineT2S2, 2);
                model.QBusinessCollateralAgmLineT2S2_BuyerName_Row_3 = GetQBusinessCollateralAgmLineT2S2_BuyerName(qBusinessCollateralAgmLineT2S2, 3);
                model.QBusinessCollateralAgmLineT2S2_BuyerName_Row_4 = GetQBusinessCollateralAgmLineT2S2_BuyerName(qBusinessCollateralAgmLineT2S2, 4);
                model.QBusinessCollateralAgmLineT2S2_BuyerName_Row_5 = GetQBusinessCollateralAgmLineT2S2_BuyerName(qBusinessCollateralAgmLineT2S2, 5);
                model.QBusinessCollateralAgmLineT2S2_BuyerName_Row_6 = GetQBusinessCollateralAgmLineT2S2_BuyerName(qBusinessCollateralAgmLineT2S2, 6);
                model.QBusinessCollateralAgmLineT2S2_BuyerName_Row_7 = GetQBusinessCollateralAgmLineT2S2_BuyerName(qBusinessCollateralAgmLineT2S2, 7);
                model.QBusinessCollateralAgmLineT2S2_BuyerName_Row_8 = GetQBusinessCollateralAgmLineT2S2_BuyerName(qBusinessCollateralAgmLineT2S2, 8);
                model.QBusinessCollateralAgmLineT2S2_BuyerName_Row_9 = GetQBusinessCollateralAgmLineT2S2_BuyerName(qBusinessCollateralAgmLineT2S2, 9);
                model.QBusinessCollateralAgmLineT2S2_BuyerName_Row_10 = GetQBusinessCollateralAgmLineT2S2_BuyerName(qBusinessCollateralAgmLineT2S2, 10);

                model.QBusinessCollateralAgmLineT2S2_IdentificationType_Row_1 = GetQBusinessCollateralAgmLineT2S2_IdentificationType(qBusinessCollateralAgmLineT2S2, 1);
                model.QBusinessCollateralAgmLineT2S2_IdentificationType_Row_2 = GetQBusinessCollateralAgmLineT2S2_IdentificationType(qBusinessCollateralAgmLineT2S2, 2);
                model.QBusinessCollateralAgmLineT2S2_IdentificationType_Row_3 = GetQBusinessCollateralAgmLineT2S2_IdentificationType(qBusinessCollateralAgmLineT2S2, 3);
                model.QBusinessCollateralAgmLineT2S2_IdentificationType_Row_4 = GetQBusinessCollateralAgmLineT2S2_IdentificationType(qBusinessCollateralAgmLineT2S2, 4);
                model.QBusinessCollateralAgmLineT2S2_IdentificationType_Row_5 = GetQBusinessCollateralAgmLineT2S2_IdentificationType(qBusinessCollateralAgmLineT2S2, 5);
                model.QBusinessCollateralAgmLineT2S2_IdentificationType_Row_6 = GetQBusinessCollateralAgmLineT2S2_IdentificationType(qBusinessCollateralAgmLineT2S2, 6);
                model.QBusinessCollateralAgmLineT2S2_IdentificationType_Row_7 = GetQBusinessCollateralAgmLineT2S2_IdentificationType(qBusinessCollateralAgmLineT2S2, 7);
                model.QBusinessCollateralAgmLineT2S2_IdentificationType_Row_8 = GetQBusinessCollateralAgmLineT2S2_IdentificationType(qBusinessCollateralAgmLineT2S2, 8);
                model.QBusinessCollateralAgmLineT2S2_IdentificationType_Row_9 = GetQBusinessCollateralAgmLineT2S2_IdentificationType(qBusinessCollateralAgmLineT2S2, 9);
                model.QBusinessCollateralAgmLineT2S2_IdentificationType_Row_10 = GetQBusinessCollateralAgmLineT2S2_IdentificationType(qBusinessCollateralAgmLineT2S2, 10);


                model.QBusinessCollateralAgmLineT2S2_TaxId_Row_1 = GetQBusinessCollateralAgmLineT2S2_TaxId(qBusinessCollateralAgmLineT2S2, 1);
                model.QBusinessCollateralAgmLineT2S2_TaxId_Row_2 = GetQBusinessCollateralAgmLineT2S2_TaxId(qBusinessCollateralAgmLineT2S2, 2);
                model.QBusinessCollateralAgmLineT2S2_TaxId_Row_3 = GetQBusinessCollateralAgmLineT2S2_TaxId(qBusinessCollateralAgmLineT2S2, 3);
                model.QBusinessCollateralAgmLineT2S2_TaxId_Row_4 = GetQBusinessCollateralAgmLineT2S2_TaxId(qBusinessCollateralAgmLineT2S2, 4);
                model.QBusinessCollateralAgmLineT2S2_TaxId_Row_5 = GetQBusinessCollateralAgmLineT2S2_TaxId(qBusinessCollateralAgmLineT2S2, 5);
                model.QBusinessCollateralAgmLineT2S2_TaxId_Row_6 = GetQBusinessCollateralAgmLineT2S2_TaxId(qBusinessCollateralAgmLineT2S2, 6);
                model.QBusinessCollateralAgmLineT2S2_TaxId_Row_7 = GetQBusinessCollateralAgmLineT2S2_TaxId(qBusinessCollateralAgmLineT2S2, 7);
                model.QBusinessCollateralAgmLineT2S2_TaxId_Row_8 = GetQBusinessCollateralAgmLineT2S2_TaxId(qBusinessCollateralAgmLineT2S2, 8);
                model.QBusinessCollateralAgmLineT2S2_TaxId_Row_9 = GetQBusinessCollateralAgmLineT2S2_TaxId(qBusinessCollateralAgmLineT2S2, 9);
                model.QBusinessCollateralAgmLineT2S2_TaxId_Row_10 = GetQBusinessCollateralAgmLineT2S2_TaxId(qBusinessCollateralAgmLineT2S2, 10);

                model.QBusinessCollateralAgmLineT2S2_PassportId_Row_1 = GetQBusinessCollateralAgmLineT2S2_PassportId(qBusinessCollateralAgmLineT2S2, 1);
                model.QBusinessCollateralAgmLineT2S2_PassportId_Row_2 = GetQBusinessCollateralAgmLineT2S2_PassportId(qBusinessCollateralAgmLineT2S2, 2);
                model.QBusinessCollateralAgmLineT2S2_PassportId_Row_3 = GetQBusinessCollateralAgmLineT2S2_PassportId(qBusinessCollateralAgmLineT2S2, 3);
                model.QBusinessCollateralAgmLineT2S2_PassportId_Row_4 = GetQBusinessCollateralAgmLineT2S2_PassportId(qBusinessCollateralAgmLineT2S2, 4);
                model.QBusinessCollateralAgmLineT2S2_PassportId_Row_5 = GetQBusinessCollateralAgmLineT2S2_PassportId(qBusinessCollateralAgmLineT2S2, 5);
                model.QBusinessCollateralAgmLineT2S2_PassportId_Row_6 = GetQBusinessCollateralAgmLineT2S2_PassportId(qBusinessCollateralAgmLineT2S2, 6);
                model.QBusinessCollateralAgmLineT2S2_PassportId_Row_7 = GetQBusinessCollateralAgmLineT2S2_PassportId(qBusinessCollateralAgmLineT2S2, 7);
                model.QBusinessCollateralAgmLineT2S2_PassportId_Row_8 = GetQBusinessCollateralAgmLineT2S2_PassportId(qBusinessCollateralAgmLineT2S2, 8);
                model.QBusinessCollateralAgmLineT2S2_PassportId_Row_9 = GetQBusinessCollateralAgmLineT2S2_PassportId(qBusinessCollateralAgmLineT2S2, 9);
                model.QBusinessCollateralAgmLineT2S2_PassportId_Row_10 = GetQBusinessCollateralAgmLineT2S2_PassportId(qBusinessCollateralAgmLineT2S2, 10);


                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                GetGeneralBusinesCalateralAgmLineT3S1(model, qBusinessCollateralAgmLineT3S1);

                model.QBusinessCollateralAgmLineT3S1_Product = GetQBusinessCollateralAgmLineT3S1_Product(qBusinessCollateralAgmLineT3S1, 1);
                model.QBusinessCollateralAgmLineT3S1_Product_Row_2 = GetQBusinessCollateralAgmLineT3S1_Product(qBusinessCollateralAgmLineT3S1, 2);
                model.QBusinessCollateralAgmLineT3S1_Product_Row_3 = GetQBusinessCollateralAgmLineT3S1_Product(qBusinessCollateralAgmLineT3S1, 3);
                model.QBusinessCollateralAgmLineT3S1_Product_Row_4 = GetQBusinessCollateralAgmLineT3S1_Product(qBusinessCollateralAgmLineT3S1, 4);
                model.QBusinessCollateralAgmLineT3S1_Product_Row_5 = GetQBusinessCollateralAgmLineT3S1_Product(qBusinessCollateralAgmLineT3S1, 5);

                model.QBusinessCollateralAgmLineT3S1_RegistrationPlateNumber = GetQBusinessCollateralAgmLineT3S1_RegistrationPlateNumber(qBusinessCollateralAgmLineT3S1, 1);
                model.QBusinessCollateralAgmLineT3S1_RegistrationPlateNumber_Row_2 = GetQBusinessCollateralAgmLineT3S1_RegistrationPlateNumber(qBusinessCollateralAgmLineT3S1, 2);
                model.QBusinessCollateralAgmLineT3S1_RegistrationPlateNumber_Row_3 = GetQBusinessCollateralAgmLineT3S1_RegistrationPlateNumber(qBusinessCollateralAgmLineT3S1, 3);
                model.QBusinessCollateralAgmLineT3S1_RegistrationPlateNumber_Row_4 = GetQBusinessCollateralAgmLineT3S1_RegistrationPlateNumber(qBusinessCollateralAgmLineT3S1, 4);
                model.QBusinessCollateralAgmLineT3S1_RegistrationPlateNumber_Row_5 = GetQBusinessCollateralAgmLineT3S1_RegistrationPlateNumber(qBusinessCollateralAgmLineT3S1, 5);

                model.QBusinessCollateralAgmLineT3S1_MachineNumber = GetQBusinessCollateralAgmLineT3S1_MachineNumber(qBusinessCollateralAgmLineT3S1, 1);
                model.QBusinessCollateralAgmLineT3S1_MachineNumber_Row_2 = GetQBusinessCollateralAgmLineT3S1_MachineNumber(qBusinessCollateralAgmLineT3S1, 2);
                model.QBusinessCollateralAgmLineT3S1_MachineNumber_Row_3 = GetQBusinessCollateralAgmLineT3S1_MachineNumber(qBusinessCollateralAgmLineT3S1, 3);
                model.QBusinessCollateralAgmLineT3S1_MachineNumber_Row_4 = GetQBusinessCollateralAgmLineT3S1_MachineNumber(qBusinessCollateralAgmLineT3S1, 4);
                model.QBusinessCollateralAgmLineT3S1_MachineNumber_Row_5 = GetQBusinessCollateralAgmLineT3S1_MachineNumber(qBusinessCollateralAgmLineT3S1, 5);

                model.QBusinessCollateralAgmLineT3S1_MachineRegisteredStatus = GetQBusinessCollateralAgmLineT3S1_MachineRegisteredStatus(qBusinessCollateralAgmLineT3S1, 1);
                model.QBusinessCollateralAgmLineT3S1_MachineRegisteredStatus_Row_2 = GetQBusinessCollateralAgmLineT3S1_MachineRegisteredStatus(qBusinessCollateralAgmLineT3S1, 2);
                model.QBusinessCollateralAgmLineT3S1_MachineRegisteredStatus_Row_3 = GetQBusinessCollateralAgmLineT3S1_MachineRegisteredStatus(qBusinessCollateralAgmLineT3S1, 3);
                model.QBusinessCollateralAgmLineT3S1_MachineRegisteredStatus_Row_4= GetQBusinessCollateralAgmLineT3S1_MachineRegisteredStatus(qBusinessCollateralAgmLineT3S1, 4);
                model.QBusinessCollateralAgmLineT3S1_MachineRegisteredStatus_Row_5 = GetQBusinessCollateralAgmLineT3S1_MachineRegisteredStatus(qBusinessCollateralAgmLineT3S1, 5);

                model.QBusinessCollateralAgmLineT3S1_RegisteredPlace = GetQBusinessCollateralAgmLineT3S1_RegisteredPlace(qBusinessCollateralAgmLineT3S1, 1);
                model.QBusinessCollateralAgmLineT3S1_RegisteredPlace_Row_2 = GetQBusinessCollateralAgmLineT3S1_RegisteredPlace(qBusinessCollateralAgmLineT3S1, 2);
                model.QBusinessCollateralAgmLineT3S1_RegisteredPlace_Row_3 = GetQBusinessCollateralAgmLineT3S1_RegisteredPlace(qBusinessCollateralAgmLineT3S1, 3);
                model.QBusinessCollateralAgmLineT3S1_RegisteredPlace_Row_4 = GetQBusinessCollateralAgmLineT3S1_RegisteredPlace(qBusinessCollateralAgmLineT3S1, 4);
                model.QBusinessCollateralAgmLineT3S1_RegisteredPlace_Row_5 = GetQBusinessCollateralAgmLineT3S1_RegisteredPlace(qBusinessCollateralAgmLineT3S1, 5);

                model.QBusinessCollateralAgmLineT3S1_BusinessCollateralValue = GetQBusinessCollateralAgmLineT3S1_BusinessCollateralValue(qBusinessCollateralAgmLineT3S1, 1);
                model.QBusinessCollateralAgmLineT3S1_BusinessCollateralValue_Row_2 = GetQBusinessCollateralAgmLineT3S1_BusinessCollateralValue(qBusinessCollateralAgmLineT3S1, 2);
                model.QBusinessCollateralAgmLineT3S1_BusinessCollateralValue_Row_3 = GetQBusinessCollateralAgmLineT3S1_BusinessCollateralValue(qBusinessCollateralAgmLineT3S1, 3);
                model.QBusinessCollateralAgmLineT3S1_BusinessCollateralValue_Row_4 = GetQBusinessCollateralAgmLineT3S1_BusinessCollateralValue(qBusinessCollateralAgmLineT3S1, 4);
                model.QBusinessCollateralAgmLineT3S1_BusinessCollateralValue_Row_5 = GetQBusinessCollateralAgmLineT3S1_BusinessCollateralValue(qBusinessCollateralAgmLineT3S1, 5);

                model.QBusinessCollateralAgmLineT3S1_Unit = GetQBusinessCollateralAgmLineT3S1_Unit(qBusinessCollateralAgmLineT3S1, 1);
                model.QBusinessCollateralAgmLineT3S1_Unit_Row_2 = GetQBusinessCollateralAgmLineT3S1_Unit(qBusinessCollateralAgmLineT3S1, 2);
                model.QBusinessCollateralAgmLineT3S1_Unit_Row_3 = GetQBusinessCollateralAgmLineT3S1_Unit(qBusinessCollateralAgmLineT3S1, 3);
                model.QBusinessCollateralAgmLineT3S1_Unit_Row_4 = GetQBusinessCollateralAgmLineT3S1_Unit(qBusinessCollateralAgmLineT3S1, 4);
                model.QBusinessCollateralAgmLineT3S1_Unit_Row_5 = GetQBusinessCollateralAgmLineT3S1_Unit(qBusinessCollateralAgmLineT3S1, 5);

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                GetGeneralBusinesCalateralAgmLineT3S2(model, qBusinessCollateralAgmLineT3S2);

                model.QBusinessCollateralAgmLineT3S2_Product = GetQBusinessCollateralAgmLineT3S2_Product(qBusinessCollateralAgmLineT3S2,1);
                model.QBusinessCollateralAgmLineT3S2_Product_Row_2 = GetQBusinessCollateralAgmLineT3S2_Product(qBusinessCollateralAgmLineT3S2, 2);
                model.QBusinessCollateralAgmLineT3S2_Product_Row_3 = GetQBusinessCollateralAgmLineT3S2_Product(qBusinessCollateralAgmLineT3S2, 3);
                model.QBusinessCollateralAgmLineT3S2_Product_Row_4 = GetQBusinessCollateralAgmLineT3S2_Product(qBusinessCollateralAgmLineT3S2, 4);
                model.QBusinessCollateralAgmLineT3S2_Product_Row_5 = GetQBusinessCollateralAgmLineT3S2_Product(qBusinessCollateralAgmLineT3S2, 5);

                model.QBusinessCollateralAgmLineT3S2_Quantity = GetQBusinessCollateralAgmLineT3S2_Quantity(qBusinessCollateralAgmLineT3S2, 1);
                model.QBusinessCollateralAgmLineT3S2_Quantity_Row_2 = GetQBusinessCollateralAgmLineT3S2_Quantity(qBusinessCollateralAgmLineT3S2, 2);
                model.QBusinessCollateralAgmLineT3S2_Quantity_Row_3 = GetQBusinessCollateralAgmLineT3S2_Quantity(qBusinessCollateralAgmLineT3S2, 3);
                model.QBusinessCollateralAgmLineT3S2_Quantity_Row_4 = GetQBusinessCollateralAgmLineT3S2_Quantity(qBusinessCollateralAgmLineT3S2, 4);
                model.QBusinessCollateralAgmLineT3S2_Quantity_Row_5 = GetQBusinessCollateralAgmLineT3S2_Quantity(qBusinessCollateralAgmLineT3S2, 5);


                model.QBusinessCollateralAgmLineT3S2_BusinessCollateralValue = QBusinessCollateralAgmLineT3S2_BusinessCollateralValue(qBusinessCollateralAgmLineT3S2, 1);
                model.QBusinessCollateralAgmLineT3S2_BusinessCollateralValue_Row_2 = QBusinessCollateralAgmLineT3S2_BusinessCollateralValue(qBusinessCollateralAgmLineT3S2, 2);
                model.QBusinessCollateralAgmLineT3S2_BusinessCollateralValue_Row_3 = QBusinessCollateralAgmLineT3S2_BusinessCollateralValue(qBusinessCollateralAgmLineT3S2, 3);
                model.QBusinessCollateralAgmLineT3S2_BusinessCollateralValue_Row_4 = QBusinessCollateralAgmLineT3S2_BusinessCollateralValue(qBusinessCollateralAgmLineT3S2, 4);
                model.QBusinessCollateralAgmLineT3S2_BusinessCollateralValue_Row_5 = QBusinessCollateralAgmLineT3S2_BusinessCollateralValue(qBusinessCollateralAgmLineT3S2, 5);

                model.QBusinessCollateralAgmLineT3S2_Unit = QBusinessCollateralAgmLineT3S2_Unit(qBusinessCollateralAgmLineT3S2, 1);
                model.QBusinessCollateralAgmLineT3S2_Unit_Row_2 = QBusinessCollateralAgmLineT3S2_Unit(qBusinessCollateralAgmLineT3S2, 2);
                model.QBusinessCollateralAgmLineT3S2_Unit_Row_3 = QBusinessCollateralAgmLineT3S2_Unit(qBusinessCollateralAgmLineT3S2, 3);
                model.QBusinessCollateralAgmLineT3S2_Unit_Row_4 = QBusinessCollateralAgmLineT3S2_Unit(qBusinessCollateralAgmLineT3S2, 4);
                model.QBusinessCollateralAgmLineT3S2_Unit_Row_5 = QBusinessCollateralAgmLineT3S2_Unit(qBusinessCollateralAgmLineT3S2, 5);



                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                GetGeneralBusinesCalateralAgmLineT3S3(model, qBusinessCollateralAgmLineT3S3);



                model.QBusinessCollateralAgmLineT3S3_Quantity = GetQBusinessCollateralAgmLineT3S3_Quantity(qBusinessCollateralAgmLineT3S3, 1);
                model.QBusinessCollateralAgmLineT3S3_Quantity_Row_2 = GetQBusinessCollateralAgmLineT3S3_Quantity(qBusinessCollateralAgmLineT3S3, 2);
                model.QBusinessCollateralAgmLineT3S3_Quantity_Row_3 = GetQBusinessCollateralAgmLineT3S3_Quantity(qBusinessCollateralAgmLineT3S3, 3);
                model.QBusinessCollateralAgmLineT3S3_Quantity_Row_4 = GetQBusinessCollateralAgmLineT3S3_Quantity(qBusinessCollateralAgmLineT3S3, 4);
                model.QBusinessCollateralAgmLineT3S3_Quantity_Row_5 = GetQBusinessCollateralAgmLineT3S3_Quantity(qBusinessCollateralAgmLineT3S3, 5);

                model.QBusinessCollateralAgmLineT3S3_Product = GetQBusinessCollateralAgmLineT3S3_Product(qBusinessCollateralAgmLineT3S3, 1);
                model.QBusinessCollateralAgmLineT3S3_Product_Row_2 = GetQBusinessCollateralAgmLineT3S3_Product(qBusinessCollateralAgmLineT3S3, 2);
                model.QBusinessCollateralAgmLineT3S3_Product_Row_3 = GetQBusinessCollateralAgmLineT3S3_Product(qBusinessCollateralAgmLineT3S3, 3);
                model.QBusinessCollateralAgmLineT3S3_Product_Row_4 = GetQBusinessCollateralAgmLineT3S3_Product(qBusinessCollateralAgmLineT3S3, 4);
                model.QBusinessCollateralAgmLineT3S3_Product_Row_5 = GetQBusinessCollateralAgmLineT3S3_Product(qBusinessCollateralAgmLineT3S3, 5);


                model.QBusinessCollateralAgmLineT3S3_BusinessCollateralValue = GetQBusinessCollateralAgmLineT3S3_BusinessCollateralValue(qBusinessCollateralAgmLineT3S3, 1);
                model.QBusinessCollateralAgmLineT3S3_BusinessCollateralValue_Row_2 = GetQBusinessCollateralAgmLineT3S3_BusinessCollateralValue(qBusinessCollateralAgmLineT3S3, 2);
                model.QBusinessCollateralAgmLineT3S3_BusinessCollateralValue_Row_3 = GetQBusinessCollateralAgmLineT3S3_BusinessCollateralValue(qBusinessCollateralAgmLineT3S3, 3);
                model.QBusinessCollateralAgmLineT3S3_BusinessCollateralValue_Row_4 = GetQBusinessCollateralAgmLineT3S3_BusinessCollateralValue(qBusinessCollateralAgmLineT3S3, 4);
                model.QBusinessCollateralAgmLineT3S3_BusinessCollateralValue_Row_5 = GetQBusinessCollateralAgmLineT3S3_BusinessCollateralValue(qBusinessCollateralAgmLineT3S3, 5);

                model.QBusinessCollateralAgmLineT3S3_Unit = GetQBusinessCollateralAgmLineT3S3_Unit(qBusinessCollateralAgmLineT3S3, 1);
                model.QBusinessCollateralAgmLineT3S3_Unit_Row_2 = GetQBusinessCollateralAgmLineT3S3_Unit(qBusinessCollateralAgmLineT3S3, 2);
                model.QBusinessCollateralAgmLineT3S3_Unit_Row_3 = GetQBusinessCollateralAgmLineT3S3_Unit(qBusinessCollateralAgmLineT3S3, 3); 
                model.QBusinessCollateralAgmLineT3S3_Unit_Row_4 = GetQBusinessCollateralAgmLineT3S3_Unit(qBusinessCollateralAgmLineT3S3, 4); 
                model.QBusinessCollateralAgmLineT3S3_Unit_Row_5 = GetQBusinessCollateralAgmLineT3S3_Unit(qBusinessCollateralAgmLineT3S3, 5);

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                GetGeneralBusinesCalateralAgmLineT3S4(model, qBusinessCollateralAgmLineT3S4);


                model.QBusinessCollateralAgmLineT3S4_Product = GetQBusinessCollateralAgmLineT3S4_Product(qBusinessCollateralAgmLineT3S4,1);
                model.QBusinessCollateralAgmLineT3S4_Product_Row_2 = GetQBusinessCollateralAgmLineT3S4_Product(qBusinessCollateralAgmLineT3S4, 2);
                model.QBusinessCollateralAgmLineT3S4_Product_Row_3 = GetQBusinessCollateralAgmLineT3S4_Product(qBusinessCollateralAgmLineT3S4, 3);
                model.QBusinessCollateralAgmLineT3S4_Product_Row_4 = GetQBusinessCollateralAgmLineT3S4_Product(qBusinessCollateralAgmLineT3S4, 4);
                model.QBusinessCollateralAgmLineT3S4_Product_Row_5 = GetQBusinessCollateralAgmLineT3S4_Product(qBusinessCollateralAgmLineT3S4, 5);

                model.QBusinessCollateralAgmLineT3S4_MachineNumber = GetQBusinessCollateralAgmLineT3S4_MachineNumber(qBusinessCollateralAgmLineT3S4, 1);
                model.QBusinessCollateralAgmLineT3S4_MachineNumber_Row_2 = GetQBusinessCollateralAgmLineT3S4_MachineNumber(qBusinessCollateralAgmLineT3S4, 2);
                model.QBusinessCollateralAgmLineT3S4_MachineNumber_Row_3 = GetQBusinessCollateralAgmLineT3S4_MachineNumber(qBusinessCollateralAgmLineT3S4, 3);
                model.QBusinessCollateralAgmLineT3S4_MachineNumber_Row_4 = GetQBusinessCollateralAgmLineT3S4_MachineNumber(qBusinessCollateralAgmLineT3S4, 4);
                model.QBusinessCollateralAgmLineT3S4_MachineNumber_Row_5 = GetQBusinessCollateralAgmLineT3S4_MachineNumber(qBusinessCollateralAgmLineT3S4, 5);


                model.QBusinessCollateralAgmLineT3S4_ChassisNumber = GetQBusinessCollateralAgmLineT3S4_ChassisNumber(qBusinessCollateralAgmLineT3S4, 1);
                model.QBusinessCollateralAgmLineT3S4_ChassisNumber_Row_2 = GetQBusinessCollateralAgmLineT3S4_ChassisNumber(qBusinessCollateralAgmLineT3S4, 2);
                model.QBusinessCollateralAgmLineT3S4_ChassisNumber_Row_3 = GetQBusinessCollateralAgmLineT3S4_ChassisNumber(qBusinessCollateralAgmLineT3S4, 3);
                model.QBusinessCollateralAgmLineT3S4_ChassisNumber_Row_4 = GetQBusinessCollateralAgmLineT3S4_ChassisNumber(qBusinessCollateralAgmLineT3S4, 4);
                model.QBusinessCollateralAgmLineT3S4_ChassisNumber_Row_5 = GetQBusinessCollateralAgmLineT3S4_ChassisNumber(qBusinessCollateralAgmLineT3S4, 5);

                model.QBusinessCollateralAgmLineT3S4_RegistrationPlateNumber = GetQBusinessCollateralAgmLineT3S4_RegistrationPlateNumber(qBusinessCollateralAgmLineT3S4, 1);
                model.QBusinessCollateralAgmLineT3S4_RegistrationPlateNumber_Row_2 = GetQBusinessCollateralAgmLineT3S4_RegistrationPlateNumber(qBusinessCollateralAgmLineT3S4, 2);
                model.QBusinessCollateralAgmLineT3S4_RegistrationPlateNumber_Row_3 = GetQBusinessCollateralAgmLineT3S4_RegistrationPlateNumber(qBusinessCollateralAgmLineT3S4, 3);
                model.QBusinessCollateralAgmLineT3S4_RegistrationPlateNumber_Row_4 = GetQBusinessCollateralAgmLineT3S4_RegistrationPlateNumber(qBusinessCollateralAgmLineT3S4, 4);
                model.QBusinessCollateralAgmLineT3S4_RegistrationPlateNumber_Row_5 = GetQBusinessCollateralAgmLineT3S4_RegistrationPlateNumber(qBusinessCollateralAgmLineT3S4, 5);


                model.QBusinessCollateralAgmLineT3S4_Quantity = GetQBusinessCollateralAgmLineT3S4_Quantity(qBusinessCollateralAgmLineT3S4, 1);
                model.QBusinessCollateralAgmLineT3S4_Quantity_Row_2 = GetQBusinessCollateralAgmLineT3S4_Quantity(qBusinessCollateralAgmLineT3S4, 2);
                model.QBusinessCollateralAgmLineT3S4_Quantity_Row_3 = GetQBusinessCollateralAgmLineT3S4_Quantity(qBusinessCollateralAgmLineT3S4, 3);
                model.QBusinessCollateralAgmLineT3S4_Quantity_Row_4 = GetQBusinessCollateralAgmLineT3S4_Quantity(qBusinessCollateralAgmLineT3S4, 4);
                model.QBusinessCollateralAgmLineT3S4_Quantity_Row_5 = GetQBusinessCollateralAgmLineT3S4_Quantity(qBusinessCollateralAgmLineT3S4, 5);

        model.QBusinessCollateralAgmLineT3S4_BusinessCollateralValue = GetQBusinessCollateralAgmLineT3S4_BusinessCollateralValue(qBusinessCollateralAgmLineT3S4, 1);
                model.QBusinessCollateralAgmLineT3S4_BusinessCollateralValue_Row_2 = GetQBusinessCollateralAgmLineT3S4_BusinessCollateralValue(qBusinessCollateralAgmLineT3S4, 2);
                model.QBusinessCollateralAgmLineT3S4_BusinessCollateralValue_Row_3 = GetQBusinessCollateralAgmLineT3S4_BusinessCollateralValue(qBusinessCollateralAgmLineT3S4, 3);
                model.QBusinessCollateralAgmLineT3S4_BusinessCollateralValue_Row_4 = GetQBusinessCollateralAgmLineT3S4_BusinessCollateralValue(qBusinessCollateralAgmLineT3S4, 4);
                model.QBusinessCollateralAgmLineT3S4_BusinessCollateralValue_Row_5 = GetQBusinessCollateralAgmLineT3S4_BusinessCollateralValue(qBusinessCollateralAgmLineT3S4, 5);

                model.QBusinessCollateralAgmLineT3S4_Unit = GetQBusinessCollateralAgmLineT3S4_Unit(qBusinessCollateralAgmLineT3S4, 1);
                model.QBusinessCollateralAgmLineT3S4_Unit_Row_2 = GetQBusinessCollateralAgmLineT3S4_Unit(qBusinessCollateralAgmLineT3S4, 2);
                model.QBusinessCollateralAgmLineT3S4_Unit_Row_3 = GetQBusinessCollateralAgmLineT3S4_Unit(qBusinessCollateralAgmLineT3S4, 3);
                model.QBusinessCollateralAgmLineT3S4_Unit_Row_4 = GetQBusinessCollateralAgmLineT3S4_Unit(qBusinessCollateralAgmLineT3S4, 4);
                model.QBusinessCollateralAgmLineT3S4_Unit_Row_5 = GetQBusinessCollateralAgmLineT3S4_Unit(qBusinessCollateralAgmLineT3S4, 5);

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                GetGeneralBusinesCalateralAgmLineT3S5(model, qBusinessCollateralAgmLineT3S5);

                model.QBusinessCollateralAgmLineT3S5_Product = GetQBusinessCollateralAgmLineT3S5_Product(qBusinessCollateralAgmLineT3S5,1);
                model.QBusinessCollateralAgmLineT3S5_Product_Row_2 = GetQBusinessCollateralAgmLineT3S5_Product(qBusinessCollateralAgmLineT3S5, 2);
                model.QBusinessCollateralAgmLineT3S5_Product_Row_3 = GetQBusinessCollateralAgmLineT3S5_Product(qBusinessCollateralAgmLineT3S5, 3);
                model.QBusinessCollateralAgmLineT3S5_Product_Row_4 = GetQBusinessCollateralAgmLineT3S5_Product(qBusinessCollateralAgmLineT3S5, 4);
                model.QBusinessCollateralAgmLineT3S5_Product_Row_5 = GetQBusinessCollateralAgmLineT3S5_Product(qBusinessCollateralAgmLineT3S5, 5);

                model.QBusinessCollateralAgmLineT3S5_Quantity = GetQBusinessCollateralAgmLineT3S5_Quantity(qBusinessCollateralAgmLineT3S5, 1);
                model.QBusinessCollateralAgmLineT3S5_Quantity_Row_2 = GetQBusinessCollateralAgmLineT3S5_Quantity(qBusinessCollateralAgmLineT3S5, 2);
                model.QBusinessCollateralAgmLineT3S5_Quantity_Row_3 = GetQBusinessCollateralAgmLineT3S5_Quantity(qBusinessCollateralAgmLineT3S5, 3);
                model.QBusinessCollateralAgmLineT3S5_Quantity_Row_4 = GetQBusinessCollateralAgmLineT3S5_Quantity(qBusinessCollateralAgmLineT3S5, 4);
                model.QBusinessCollateralAgmLineT3S5_Quantity_Row_5 = GetQBusinessCollateralAgmLineT3S5_Quantity(qBusinessCollateralAgmLineT3S5, 5);

                model.QBusinessCollateralAgmLineT3S5_BusinessCollateralValue = GetQBusinessCollateralAgmLineT3S5_BusinessCollateralValue(qBusinessCollateralAgmLineT3S5, 1);
                model.QBusinessCollateralAgmLineT3S5_BusinessCollateralValue_Row_2 = GetQBusinessCollateralAgmLineT3S5_BusinessCollateralValue(qBusinessCollateralAgmLineT3S5, 2);
                model.QBusinessCollateralAgmLineT3S5_BusinessCollateralValue_Row_3 = GetQBusinessCollateralAgmLineT3S5_BusinessCollateralValue(qBusinessCollateralAgmLineT3S5, 3);
                model.QBusinessCollateralAgmLineT3S5_BusinessCollateralValue_Row_4 = GetQBusinessCollateralAgmLineT3S5_BusinessCollateralValue(qBusinessCollateralAgmLineT3S5, 4);
                model.QBusinessCollateralAgmLineT3S5_BusinessCollateralValue_Row_5 = GetQBusinessCollateralAgmLineT3S5_BusinessCollateralValue(qBusinessCollateralAgmLineT3S5, 5);
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                GetGeneralBusinesCalateralAgmLineT4S1(model, qBusinessCollateralAgmLineT4S1);



                model.QBusinessCollateralAgmLineT4S1_ProjectName = GetQBusinessCollateralAgmLineT4S1_ProjectName(qBusinessCollateralAgmLineT4S1, 1);
                model.QBusinessCollateralAgmLineT4S1_ProjectName_Row_2 = GetQBusinessCollateralAgmLineT4S1_ProjectName(qBusinessCollateralAgmLineT4S1, 2);
                model.QBusinessCollateralAgmLineT4S1_ProjectName_Row_3 = GetQBusinessCollateralAgmLineT4S1_ProjectName(qBusinessCollateralAgmLineT4S1, 3);
                model.QBusinessCollateralAgmLineT4S1_ProjectName_Row_4 = GetQBusinessCollateralAgmLineT4S1_ProjectName(qBusinessCollateralAgmLineT4S1, 4);
                model.QBusinessCollateralAgmLineT4S1_ProjectName_Row_5 = GetQBusinessCollateralAgmLineT4S1_ProjectName(qBusinessCollateralAgmLineT4S1, 5);

                model.QBusinessCollateralAgmLineT4S1_TitleDeedNumber = GetQBusinessCollateralAgmLineT4S1_TitleDeedNumber(qBusinessCollateralAgmLineT4S1, 1);
                model.QBusinessCollateralAgmLineT4S1_TitleDeedNumber_Row_2 = GetQBusinessCollateralAgmLineT4S1_TitleDeedNumber(qBusinessCollateralAgmLineT4S1, 2);
                model.QBusinessCollateralAgmLineT4S1_TitleDeedNumber_Row_3 = GetQBusinessCollateralAgmLineT4S1_TitleDeedNumber(qBusinessCollateralAgmLineT4S1, 3);
                model.QBusinessCollateralAgmLineT4S1_TitleDeedNumber_Row_4 = GetQBusinessCollateralAgmLineT4S1_TitleDeedNumber(qBusinessCollateralAgmLineT4S1, 4);
                model.QBusinessCollateralAgmLineT4S1_TitleDeedNumber_Row_5 = GetQBusinessCollateralAgmLineT4S1_TitleDeedNumber(qBusinessCollateralAgmLineT4S1, 5);

                model.QBusinessCollateralAgmLineT4S1_TitleDeedSubDistrict = GetQBusinessCollateralAgmLineT4S1_TitleDeedSubDistrict(qBusinessCollateralAgmLineT4S1, 1);
                model.QBusinessCollateralAgmLineT4S1_TitleDeedSubDistrict_Row_2 = GetQBusinessCollateralAgmLineT4S1_TitleDeedSubDistrict(qBusinessCollateralAgmLineT4S1, 2);
                model.QBusinessCollateralAgmLineT4S1_TitleDeedSubDistrict_Row_3 = GetQBusinessCollateralAgmLineT4S1_TitleDeedSubDistrict(qBusinessCollateralAgmLineT4S1, 3);
                model.QBusinessCollateralAgmLineT4S1_TitleDeedSubDistrict_Row_4 = GetQBusinessCollateralAgmLineT4S1_TitleDeedSubDistrict(qBusinessCollateralAgmLineT4S1, 4);
                model.QBusinessCollateralAgmLineT4S1_TitleDeedSubDistrict_Row_5 = GetQBusinessCollateralAgmLineT4S1_TitleDeedSubDistrict(qBusinessCollateralAgmLineT4S1, 5);

                model.QBusinessCollateralAgmLineT4S1_TitleDeedDistrict = GetQBusinessCollateralAgmLineT4S1_TitleDeedDistrict(qBusinessCollateralAgmLineT4S1, 1);
                model.QBusinessCollateralAgmLineT4S1_TitleDeedDistrict_Row_2 = GetQBusinessCollateralAgmLineT4S1_TitleDeedDistrict(qBusinessCollateralAgmLineT4S1, 2);
                model.QBusinessCollateralAgmLineT4S1_TitleDeedDistrict_Row_3 = GetQBusinessCollateralAgmLineT4S1_TitleDeedDistrict(qBusinessCollateralAgmLineT4S1, 3);
                model.QBusinessCollateralAgmLineT4S1_TitleDeedDistrict_Row_4 = GetQBusinessCollateralAgmLineT4S1_TitleDeedDistrict(qBusinessCollateralAgmLineT4S1, 4);
                model.QBusinessCollateralAgmLineT4S1_TitleDeedDistrict_Row_5 = GetQBusinessCollateralAgmLineT4S1_TitleDeedDistrict(qBusinessCollateralAgmLineT4S1, 5);

                model.QBusinessCollateralAgmLineT4S1_TitleDeedProvince = GetQBusinessCollateralAgmLineT4S1_TitleDeedProvince(qBusinessCollateralAgmLineT4S1, 1);
                model.QBusinessCollateralAgmLineT4S1_TitleDeedProvince_Row_2 = GetQBusinessCollateralAgmLineT4S1_TitleDeedProvince(qBusinessCollateralAgmLineT4S1, 2);
                model.QBusinessCollateralAgmLineT4S1_TitleDeedProvince_Row_3 = GetQBusinessCollateralAgmLineT4S1_TitleDeedProvince(qBusinessCollateralAgmLineT4S1, 3);
                model.QBusinessCollateralAgmLineT4S1_TitleDeedProvince_Row_4 = GetQBusinessCollateralAgmLineT4S1_TitleDeedProvince(qBusinessCollateralAgmLineT4S1, 4);
                model.QBusinessCollateralAgmLineT4S1_TitleDeedProvince_Row_5 = GetQBusinessCollateralAgmLineT4S1_TitleDeedProvince(qBusinessCollateralAgmLineT4S1, 5);

                model.QBusinessCollateralAgmLineT4S1_Unit = GetQBusinessCollateralAgmLineT4S1_Unit(qBusinessCollateralAgmLineT4S1, 1);
                model.QBusinessCollateralAgmLineT4S1_Unit_Row_2 = GetQBusinessCollateralAgmLineT4S1_Unit(qBusinessCollateralAgmLineT4S1, 2);
                model.QBusinessCollateralAgmLineT4S1_Unit_Row_3 = GetQBusinessCollateralAgmLineT4S1_Unit(qBusinessCollateralAgmLineT4S1, 3);
                model.QBusinessCollateralAgmLineT4S1_Unit_Row_4 = GetQBusinessCollateralAgmLineT4S1_Unit(qBusinessCollateralAgmLineT4S1, 4);
                model.QBusinessCollateralAgmLineT4S1_Unit_Row_5 = GetQBusinessCollateralAgmLineT4S1_Unit(qBusinessCollateralAgmLineT4S1, 5);


                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ///



                GetGeneralBusinesCalateralAgmLineT4S2(model, qBusinessCollateralAgmLineT4S2);
                model.QBusinessCollateralAgmLineT4S2_Unit = GetQBusinessCollateralAgmLineT4S2_Unit(qBusinessCollateralAgmLineT4S2, 1);
                model.QBusinessCollateralAgmLineT4S2_Unit_2 = GetQBusinessCollateralAgmLineT4S2_Unit(qBusinessCollateralAgmLineT4S2, 2);
                model.QBusinessCollateralAgmLineT4S2_Unit_3 = GetQBusinessCollateralAgmLineT4S2_Unit(qBusinessCollateralAgmLineT4S2, 3);
                model.QBusinessCollateralAgmLineT4S2_Unit_4 = GetQBusinessCollateralAgmLineT4S2_Unit(qBusinessCollateralAgmLineT4S2, 4);
                model.QBusinessCollateralAgmLineT4S2_Unit_5 = GetQBusinessCollateralAgmLineT4S2_Unit(qBusinessCollateralAgmLineT4S2, 5);
                GetGeneralBusinesCalateralAgmLineT4S3(model, qBusinessCollateralAgmLineT4S3);
    

                GetGeneralBusinesCalateralAgmLineT5S1(model, qBusinessCollateralAgmLineT5S1);
                model.QBusinessCollateralAgmLineT5S1_BusinessCollateralValue =  GetQBusinessCollateralAgmLineT5S1_BusinessCollateralValue(qBusinessCollateralAgmLineT5S1, 0);
                model.QBusinessCollateralAgmLineT5S1_Product = GetQBusinessCollateralAgmLineT5S1_Product(qBusinessCollateralAgmLineT5S1, 0);
                model.QBusinessCollateralAgmLineT5S1_BusinessCollateralAgmLineDesc = GetQBusinessCollateralAgmLineT5S1_BusinessCollateralAgmLineDesc(qBusinessCollateralAgmLineT5S1, 0);



                model.QBusinessCollateralAgmLineT5S1_BusinessCollateralValue_2 = GetQBusinessCollateralAgmLineT5S1_BusinessCollateralValue(qBusinessCollateralAgmLineT5S1, 1);
                model.QBusinessCollateralAgmLineT5S1_Product_2 = GetQBusinessCollateralAgmLineT5S1_Product(qBusinessCollateralAgmLineT5S1, 1);
                model.QBusinessCollateralAgmLineT5S1_BusinessCollateralAgmLineDesc_2 = GetQBusinessCollateralAgmLineT5S1_BusinessCollateralAgmLineDesc(qBusinessCollateralAgmLineT5S1, 1);



                model.QBusinessCollateralAgmLineT5S1_BusinessCollateralValue_3 = GetQBusinessCollateralAgmLineT5S1_BusinessCollateralValue(qBusinessCollateralAgmLineT5S1, 2);
                model.QBusinessCollateralAgmLineT5S1_Product_3 = GetQBusinessCollateralAgmLineT5S1_Product(qBusinessCollateralAgmLineT5S1,2);
                model.QBusinessCollateralAgmLineT5S1_BusinessCollateralAgmLineDesc_3 = GetQBusinessCollateralAgmLineT5S1_BusinessCollateralAgmLineDesc(qBusinessCollateralAgmLineT5S1, 2);


                model.QBusinessCollateralAgmLineT5S1_BusinessCollateralValue_4 = GetQBusinessCollateralAgmLineT5S1_BusinessCollateralValue(qBusinessCollateralAgmLineT5S1, 3);
                model.QBusinessCollateralAgmLineT5S1_Product_4 = GetQBusinessCollateralAgmLineT5S1_Product(qBusinessCollateralAgmLineT5S1, 3);
                model.QBusinessCollateralAgmLineT5S1_BusinessCollateralAgmLineDesc_4 = GetQBusinessCollateralAgmLineT5S1_BusinessCollateralAgmLineDesc(qBusinessCollateralAgmLineT5S1, 3);

                model.QBusinessCollateralAgmLineT5S1_Unit = GetQBusinessCollateralAgmLineT5S1_Unit(qBusinessCollateralAgmLineT5S1, 1);
                model.QBusinessCollateralAgmLineT5S1_Unit_2 = GetQBusinessCollateralAgmLineT5S1_Unit(qBusinessCollateralAgmLineT5S1, 2);
                model.QBusinessCollateralAgmLineT5S1_Unit_3 = GetQBusinessCollateralAgmLineT5S1_Unit(qBusinessCollateralAgmLineT5S1, 3);
                model.QBusinessCollateralAgmLineT5S1_Unit_4 = GetQBusinessCollateralAgmLineT5S1_Unit(qBusinessCollateralAgmLineT5S1, 4);
                model.QBusinessCollateralAgmLineT5S1_Unit_5 = GetQBusinessCollateralAgmLineT5S1_Unit(qBusinessCollateralAgmLineT5S1, 5);
                model.QBusinessCollateralAgmLineT5S1_Unit_6 = GetQBusinessCollateralAgmLineT5S1_Unit(qBusinessCollateralAgmLineT5S1, 6);
                model.QBusinessCollateralAgmLineT5S1_Unit_7 = GetQBusinessCollateralAgmLineT5S1_Unit(qBusinessCollateralAgmLineT5S1, 7);
                model.QBusinessCollateralAgmLineT5S1_Unit_8 = GetQBusinessCollateralAgmLineT5S1_Unit(qBusinessCollateralAgmLineT5S1, 8);
                model.QBusinessCollateralAgmLineT5S1_Unit_9 = GetQBusinessCollateralAgmLineT5S1_Unit(qBusinessCollateralAgmLineT5S1, 9);
                model.QBusinessCollateralAgmLineT5S1_Unit_10 = GetQBusinessCollateralAgmLineT5S1_Unit(qBusinessCollateralAgmLineT5S1, 10);
                model.QBusinessCollateralAgmLineT5S1_Unit_11= GetQBusinessCollateralAgmLineT5S1_Unit(qBusinessCollateralAgmLineT5S1, 11);
                model.QBusinessCollateralAgmLineT5S1_Unit_12 = GetQBusinessCollateralAgmLineT5S1_Unit(qBusinessCollateralAgmLineT5S1, 12);
                model.QBusinessCollateralAgmLineT5S1_Unit_13 = GetQBusinessCollateralAgmLineT5S1_Unit(qBusinessCollateralAgmLineT5S1, 13);
                model.QBusinessCollateralAgmLineT5S1_Unit_14 = GetQBusinessCollateralAgmLineT5S1_Unit(qBusinessCollateralAgmLineT5S1, 14);
                model.QBusinessCollateralAgmLineT5S1_Unit_15 = GetQBusinessCollateralAgmLineT5S1_Unit(qBusinessCollateralAgmLineT5S1, 15);
                model.QBusinessCollateralAgmLineT5S1_Unit_16 = GetQBusinessCollateralAgmLineT5S1_Unit(qBusinessCollateralAgmLineT5S1, 16);
                model.QBusinessCollateralAgmLineT5S1_Unit_17 = GetQBusinessCollateralAgmLineT5S1_Unit(qBusinessCollateralAgmLineT5S1, 17);
                model.QBusinessCollateralAgmLineT5S1_Unit_18 = GetQBusinessCollateralAgmLineT5S1_Unit(qBusinessCollateralAgmLineT5S1, 18);
                model.QBusinessCollateralAgmLineT5S1_Unit_19 = GetQBusinessCollateralAgmLineT5S1_Unit(qBusinessCollateralAgmLineT5S1, 19);
                model.QBusinessCollateralAgmLineT5S1_Unit_20 = GetQBusinessCollateralAgmLineT5S1_Unit(qBusinessCollateralAgmLineT5S1, 20);

                GetGeneralBusinesCalateralAgmLineT5S2(model, qBusinessCollateralAgmLineT5S2);


                GetGeneralBusinesCalateralAgmLineT5S3(model, qBusinessCollateralAgmLineT5S3);

                GetGeneralBusinesCalateralAgmLineT5S4(model, qBusinessCollateralAgmLineT5S4);

                model.MaxInterestPct = companyParameter;
                model.BCLType1 = qBCLType1_Mark1 != null ? "X" : "";
                model.BCLType2 = qBCLType2_Mark1 != null ? "X" : "";
                model.BCLType3 = qBCLType3_Mark1 != null ? "X" : "";
                model.BCLType4 = qBCLType4_Mark1 != null ? "X" : "";
                model.BCLType5 = qBCLType5_Mark1 != null ? "X" : "";

                model.DBDRegistrationAmountText = sharedService.NumberToWordsTH(qBusinessCollateralAgmTable.QBusinessCollateralAgmTable_DBDRegistrationAmount);
                model.SumBusinessCollateralValueText = sharedService.NumberToWordsTH(qBusinessCollateralAgmTable.QBusinessCollateralAgmTable_SumBusinessCollateralValue);
                model.SumBusinessCollateralValueTextT1S1 = sharedService.NumberToWordsTH(qBusinessCollateralAgmLineT1S1.Select(sl => sl.QBusinessCollateralAgmLineT1S1_SumBusinessCollateralValue).FirstOrDefault());
                model.SumBusinessCollateralValueTextT1S2 = sharedService.NumberToWordsTH(qBusinessCollateralAgmLineT1S2.Select(sl => sl.QBusinessCollateralAgmLineT1S2_SumBusinessCollateralValue).FirstOrDefault());
                model.SumBusinessCollateralValueTextT1S3 = sharedService.NumberToWordsTH(qBusinessCollateralAgmLineT1S3.Select(sl => sl.QBusinessCollateralAgmLineT1S3_SumBusinessCollateralValue).FirstOrDefault());
                model.SumBusinessCollateralValueTextT2S1 = sharedService.NumberToWordsTH(qBusinessCollateralAgmLineT2S1.Select(sl => sl.QBusinessCollateralAgmLineT2S1_SumBusinessCollateralValue).FirstOrDefault());
                model.SumBusinessCollateralValueTextT2S2 = sharedService.NumberToWordsTH(qBusinessCollateralAgmLineT2S2.Select(sl => sl.QBusinessCollateralAgmLineT2S2_SumBusinessCollateralValue).FirstOrDefault());
                model.SumBusinessCollateralValueTextT3S1 = sharedService.NumberToWordsTH(qBusinessCollateralAgmLineT3S1.Select(sl => sl.QBusinessCollateralAgmLineT3S1_SumBusinessCollateralValue).FirstOrDefault());
                model.SumBusinessCollateralValueTextT3S2 = sharedService.NumberToWordsTH(qBusinessCollateralAgmLineT3S2.Select(sl => sl.QBusinessCollateralAgmLineT3S2_SumBusinessCollateralValue).FirstOrDefault());
                model.SumBusinessCollateralValueTextT3S3 = sharedService.NumberToWordsTH(qBusinessCollateralAgmLineT3S3.Select(sl => sl.QBusinessCollateralAgmLineT3S3_SumBusinessCollateralValue).FirstOrDefault());
                model.SumBusinessCollateralValueTextT3S4 = sharedService.NumberToWordsTH(qBusinessCollateralAgmLineT3S4.Select(sl => sl.QBusinessCollateralAgmLineT3S4_SumBusinessCollateralValue).FirstOrDefault());
                model.SumBusinessCollateralValueTextT3S5 = sharedService.NumberToWordsTH(qBusinessCollateralAgmLineT3S5.Select(sl => sl.QBusinessCollateralAgmLineT3S5_SumBusinessCollateralValue).FirstOrDefault());
                model.SumBusinessCollateralValueTextT4S1 = sharedService.NumberToWordsTH(qBusinessCollateralAgmLineT4S1.Select(sl => sl.QBusinessCollateralAgmLineT4S1_SumBusinessCollateralValue).FirstOrDefault());
                model.SumBusinessCollateralValueTextT4S2 = sharedService.NumberToWordsTH(qBusinessCollateralAgmLineT4S2.Select(sl => sl.QBusinessCollateralAgmLineT4S2_SumBusinessCollateralValue).FirstOrDefault());
                model.SumBusinessCollateralValueTextT4S3 = sharedService.NumberToWordsTH(qBusinessCollateralAgmLineT4S3.Select(sl => sl.QBusinessCollateralAgmLineT4S3_SumBusinessCollateralValue).FirstOrDefault());
                model.SumBusinessCollateralValueTextT5S1 = sharedService.NumberToWordsTH(qBusinessCollateralAgmLineT5S1.Select(sl => sl.QBusinessCollateralAgmLineT5S1_SumBusinessCollateralValue).FirstOrDefault());
                model.SumBusinessCollateralValueTextT5S2 = sharedService.NumberToWordsTH(qBusinessCollateralAgmLineT5S2.Select(sl => sl.QBusinessCollateralAgmLineT5S2_SumBusinessCollateralValue).FirstOrDefault());
                model.SumBusinessCollateralValueTextT5S3 = sharedService.NumberToWordsTH(qBusinessCollateralAgmLineT5S3.Select(sl => sl.QBusinessCollateralAgmLineT5S3_SumBusinessCollateralValue).FirstOrDefault());
                model.QCustomerAuthorizedPerson1_Name = customerAuthorizedPerson1;
                model.QCustomerAuthorizedPerson2_Name = customerAuthorizedPerson2;
                model.QCustomerAuthorizedPerson3_Name = customerAuthorizedPerson3;
                model.QBusinessCollateralAgmLineT3S1_BCCheckAlreadyRegis = qBusinessCollateralAgmLineT3S1.Where(wh => wh.QBusinessCollateralAgmLineT3S1_Row == 1).Select(sl => sl.QBusinessCollateralAgmLineT3S1_MachineRegisteredStatus) != null ? "Yes" : "X";
                model.QBusinessCollateralAgmLineT3S1_BCCheckNotRegis = qBusinessCollateralAgmLineT3S1.Where(wh => wh.QBusinessCollateralAgmLineT3S1_Row == 1).Select(sl => sl.QBusinessCollateralAgmLineT3S1_MachineRegisteredStatus) != null ? "No" : "X";
                #endregion assign to model
                SetVariable(model);

                model.Variable_BCMachineAmountTextFirst1 = GetNumberToWordsTHCustomT3S1(1, qBusinessCollateralAgmLineT3S1);
                model.Variable_BCMachineAmountTextSecond1 = GetNumberToWordsTHCustomT3S1(2, qBusinessCollateralAgmLineT3S1);
                model.Variable_BCMachineAmountTextThird1 = GetNumberToWordsTHCustomT3S1(3, qBusinessCollateralAgmLineT3S1);
                model.Variable_BCMachineAmountTextFourth1 = GetNumberToWordsTHCustomT3S1(4, qBusinessCollateralAgmLineT3S1);
                model.Variable_BCMachineAmountTextFifth1 = GetNumberToWordsTHCustomT3S1(5, qBusinessCollateralAgmLineT3S1);

                model.Variable_BCInvenAmountTextFirst1 = GetNumberToWordsTHCustomT3S2(1, qBusinessCollateralAgmLineT3S2);
                model.Variable_BCInvenAmountTextSecond1 = GetNumberToWordsTHCustomT3S2(2, qBusinessCollateralAgmLineT3S2);
                model.Variable_BCInvenAmountTextThird1 = GetNumberToWordsTHCustomT3S2(3, qBusinessCollateralAgmLineT3S2);
                model.Variable_BCInvenAmountTextFourth1 = GetNumberToWordsTHCustomT3S2(4, qBusinessCollateralAgmLineT3S2);
                model.Variable_BCInvenAmountTextFifth1 = GetNumberToWordsTHCustomT3S2(5, qBusinessCollateralAgmLineT3S2);

                model.Variable_BCMaterialAmountTextFirst1 = GetNumberToWordsTHCustomT3S3(1, qBusinessCollateralAgmLineT3S3);
                model.Variable_BCMaterialAmountTextSecond1 = GetNumberToWordsTHCustomT3S3(2, qBusinessCollateralAgmLineT3S3);
                model.Variable_BCMaterialAmountTextThird1 = GetNumberToWordsTHCustomT3S3(3, qBusinessCollateralAgmLineT3S3);
                model.Variable_BCMaterialAmountTextFourth1 = GetNumberToWordsTHCustomT3S3(4, qBusinessCollateralAgmLineT3S3);
                model.Variable_BCMaterialAmountTextFifth1 = GetNumberToWordsTHCustomT3S3(5, qBusinessCollateralAgmLineT3S3);

                model.Variable_BCCarAmountTextFirst1 = GetNumberToWordsTHCustomT3S4(1, qBusinessCollateralAgmLineT3S4);
                model.Variable_BCCarAmountTextSecond1 = GetNumberToWordsTHCustomT3S4(2, qBusinessCollateralAgmLineT3S4);
                model.Variable_BCCarAmountTextThird1 = GetNumberToWordsTHCustomT3S4(3, qBusinessCollateralAgmLineT3S4);
                model.Variable_BCCarAmountTextFourth1 = GetNumberToWordsTHCustomT3S4(4, qBusinessCollateralAgmLineT3S4);
                model.Variable_BCCarAmountTextFifth1 = GetNumberToWordsTHCustomT3S4(5, qBusinessCollateralAgmLineT3S4);

                model.Variable_BCOtherAmountTextFirst1 = GetNumberToWordsTHCustomT3S5(1, qBusinessCollateralAgmLineT3S5);
                model.Variable_BCOtherAmountTextSecond1 = GetNumberToWordsTHCustomT3S5(2, qBusinessCollateralAgmLineT3S5);
                model.Variable_BCOtherAmountTextThird1 = GetNumberToWordsTHCustomT3S5(3, qBusinessCollateralAgmLineT3S5);
                model.Variable_BCOtherAmountTextFourth1 = GetNumberToWordsTHCustomT3S5(4, qBusinessCollateralAgmLineT3S5);
                model.Variable_BCOtherAmountTextFifth1 = GetNumberToWordsTHCustomT3S5(5, qBusinessCollateralAgmLineT3S5);

                model.Variable_BCIntellAssetsAmountTextFirst1 = GetNumberToWordsTHCustomT5S1(1, qBusinessCollateralAgmLineT5S1);
                model.Variable_BCIntellAssetsAmountTextSecond1 = GetNumberToWordsTHCustomT5S1(2, qBusinessCollateralAgmLineT5S1);
                model.Variable_BCIntellAssetsAmountTextThird1 = GetNumberToWordsTHCustomT5S1(3, qBusinessCollateralAgmLineT5S1);
                model.Variable_BCIntellAssetsAmountTextFour1 = GetNumberToWordsTHCustomT5S1(4, qBusinessCollateralAgmLineT5S1);

                model.Variable_NotRegisFirst = GetValueByMachineRegisteredStatus(1,qBusinessCollateralAgmLineT3S1,"No");
                    model.Variable_AlreadyRegisFirst = GetValueByMachineRegisteredStatus(1, qBusinessCollateralAgmLineT3S1, "Yes");
                model.Variable_NotRegisSecond = GetValueByMachineRegisteredStatus(2, qBusinessCollateralAgmLineT3S1, "No");
                model.Variable_AlreadyRegisSecond = GetValueByMachineRegisteredStatus(2, qBusinessCollateralAgmLineT3S1, "Yes");
                model.Variable_NotRegisThird = GetValueByMachineRegisteredStatus(3, qBusinessCollateralAgmLineT3S1, "No");
                model.Variable_AlreadyRegisThird = GetValueByMachineRegisteredStatus(3, qBusinessCollateralAgmLineT3S1, "Yes");
                model.Variable_NotRegisFourth = GetValueByMachineRegisteredStatus(4, qBusinessCollateralAgmLineT3S1, "No");
                model.Variable_AlreadyRegisFourth = GetValueByMachineRegisteredStatus(4, qBusinessCollateralAgmLineT3S1, "Yes");
                model.Variable_NotRegisFifth = GetValueByMachineRegisteredStatus(5, qBusinessCollateralAgmLineT3S1, "No");
                model.Variable_AlreadyRegisFifth = GetValueByMachineRegisteredStatus(5, qBusinessCollateralAgmLineT3S1, "Yes");
                return model;


            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
       private string GetQBusinessCollateralAgmLineT5S1_Unit(List<QBusinessCollateralAgmLineT5S1> qBusinessCollateralAgmLineT5S1, int index)
        {
            if (index < qBusinessCollateralAgmLineT5S1.Count())
            {
                return qBusinessCollateralAgmLineT5S1[index].QBusinessCollateralAgmLineT5S1_Unit;
            }
            else
            {
                return null;
            }
        }
        private string GetQBusinessCollateralAgmLineT5S1_BusinessCollateralAgmLineDesc(List<QBusinessCollateralAgmLineT5S1> qBusinessCollateralAgmLineT5S1, int index)
        {
            if (index < qBusinessCollateralAgmLineT5S1.Count())
            {
                return qBusinessCollateralAgmLineT5S1[index].QBusinessCollateralAgmLineT5S1_BusinessCollateralAgmLineDesc;
            }
            else
            {
                return null;
            }
        }

        private string GetQBusinessCollateralAgmLineT5S1_Product(List<QBusinessCollateralAgmLineT5S1> qBusinessCollateralAgmLineT5S1, int index)
        {
            if (index < qBusinessCollateralAgmLineT5S1.Count())
            {
                return qBusinessCollateralAgmLineT5S1[index].QBusinessCollateralAgmLineT5S1_Product;
            }
            else
            {
                return null;
            }
        }

        private decimal GetQBusinessCollateralAgmLineT5S1_BusinessCollateralValue(List<QBusinessCollateralAgmLineT5S1> qBusinessCollateralAgmLineT5S1, int index)
        {
            try
            {
                if(index< qBusinessCollateralAgmLineT5S1.Count())
                {
                    return qBusinessCollateralAgmLineT5S1[index].QBusinessCollateralAgmLineT5S1_BusinessCollateralValue;
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string GetValueByMachineRegisteredStatus(int index, List<QBusinessCollateralAgmLineT3S1> qBusinessCollateralAgmLineT3S1, string condition)
        {
            try
            {
                ISharedService sharedService = new SharedService();
                var machineRegisteredStatus = qBusinessCollateralAgmLineT3S1.Where(wh => wh.QBusinessCollateralAgmLineT3S1_Row == index).Select(sl => sl.QBusinessCollateralAgmLineT3S1_MachineRegisteredStatus).FirstOrDefault();
                return machineRegisteredStatus== condition? "X":null;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string GetNumberToWordsTHCustomT5S1(int index, List<QBusinessCollateralAgmLineT5S1> qBusinessCollateralAgmLineT5S1)
        {
            try
            {
                ISharedService sharedService = new SharedService();
                var businessCollateralValue = qBusinessCollateralAgmLineT5S1.Where(wh => wh.QBusinessCollateralAgmLineT5S1_Row == index).Select(sl => sl.QBusinessCollateralAgmLineT5S1_BusinessCollateralValue).FirstOrDefault();
                return sharedService.NumberToWordsTH(businessCollateralValue);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string GetNumberToWordsTHCustomT3S5(int index, List<QBusinessCollateralAgmLineT3S5> qBusinessCollateralAgmLineT3S5)
        {
            try
            {
                ISharedService sharedService = new SharedService();
                var businessCollateralValue = qBusinessCollateralAgmLineT3S5.Where(wh => wh.QBusinessCollateralAgmLineT3S5_Row == index).Select(sl => sl.QBusinessCollateralAgmLineT3S5_BusinessCollateralValue).FirstOrDefault();
                return sharedService.NumberToWordsTH(businessCollateralValue);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string GetNumberToWordsTHCustomT3S4(int index, List<QBusinessCollateralAgmLineT3S4> qBusinessCollateralAgmLineT3S4)
        {
            try
            {
                ISharedService sharedService = new SharedService();
                var businessCollateralValue = qBusinessCollateralAgmLineT3S4.Where(wh => wh.QBusinessCollateralAgmLineT3S4_Row == index).Select(sl => sl.QBusinessCollateralAgmLineT3S4_BusinessCollateralValue).FirstOrDefault();
                return sharedService.NumberToWordsTH(businessCollateralValue);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string GetNumberToWordsTHCustomT3S3(int index, List<QBusinessCollateralAgmLineT3S3> qBusinessCollateralAgmLineT3S3)
        {
            try
            {
                ISharedService sharedService = new SharedService();
                var businessCollateralValue = qBusinessCollateralAgmLineT3S3.Where(wh => wh.QBusinessCollateralAgmLineT3S3_Row == index).Select(sl => sl.QBusinessCollateralAgmLineT3S3_BusinessCollateralValue).FirstOrDefault();
                return sharedService.NumberToWordsTH(businessCollateralValue);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string GetNumberToWordsTHCustomT3S2(int index, List<QBusinessCollateralAgmLineT3S2> qBusinessCollateralAgmLineT3S2)
        {
            try
            {
                ISharedService sharedService = new SharedService();
                var businessCollateralValue = qBusinessCollateralAgmLineT3S2.Where(wh => wh.QBusinessCollateralAgmLineT3S2_Row == index).Select(sl => sl.QBusinessCollateralAgmLineT3S2_BusinessCollateralValue).FirstOrDefault();
                return sharedService.NumberToWordsTH(businessCollateralValue);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string GetNumberToWordsTHCustomT3S1(int index, List<QBusinessCollateralAgmLineT3S1> qBusinessCollateralAgmLineT3S1)
        {
            

            try
            {
                ISharedService sharedService = new SharedService();
                var businessCollateralValue = qBusinessCollateralAgmLineT3S1.Where(wh => wh.QBusinessCollateralAgmLineT3S1_Row == index).Select(sl => sl.QBusinessCollateralAgmLineT3S1_BusinessCollateralValue).FirstOrDefault();
                return sharedService.NumberToWordsTH(businessCollateralValue);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private List<QBusinessCollateralAgmLineT5S4> GetQBusinessCollateralAgmLineT5S4(Guid refGUID, QBusinessCollateralAgmTable qBusinessCollateralAgmTable)
        {
            try
            {
                var qBusinessCollateralAgmLineT5S4 = (from BusinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                      join businessCollateralType in db.Set<BusinessCollateralType>()
                                                      on BusinessCollateralAgmLine.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID into ljbusinessCollateralType
                                                      from businessCollateralType in ljbusinessCollateralType.DefaultIfEmpty()
                                                      join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                                                       on BusinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                                                      from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()
                                                      where BusinessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                                                         && businessCollateralType.AgreementOrdering == 5
                                                         && businessCollateralSubType.AgreementOrdering == 4
                                                      group new
                                                      {
                                                          BusinessCollateralAgmLine.BusinessCollateralSubTypeGUID,
                                                      }
                                                     by new
                                                     {
                                                         BusinessCollateralAgmLine.BusinessCollateralAgmLineGUID,
                                                         BusinessCollateralAgmLine.LineNum,
                                                         BusinessCollateralAgmLine.AttachmentText,
                                                         BusinessCollateralAgmLine.Product

                                                     } into g
                                                      orderby g.Key.LineNum
                                                      select new QBusinessCollateralAgmLineT5S4
                                                      {
                                                          QBusinessCollateralAgmLineT5S4_Row = g.Key.LineNum,
                                                          QBusinessCollateralAgmLineT5S4_AttachmentText = g.Key.AttachmentText,
                                                          QBusinessCollateralAgmLineT5S4_Product = g.Key.Product
                                                      }).ToList();
              
                int iqBusinessCollateralAgmLineT5S4 = 1;
                foreach (var item in qBusinessCollateralAgmLineT5S4)
                {
                    item.QBusinessCollateralAgmLineT5S4_Row = iqBusinessCollateralAgmLineT5S4;
                    item.QBusinessCollateralAgmLineT5S4_ConutLine = qBusinessCollateralAgmLineT5S4.Count();

                    ++iqBusinessCollateralAgmLineT5S4;
                }
                return qBusinessCollateralAgmLineT5S4;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private List<QBusinessCollateralAgmLineT5S3> GetQBusinessCollateralAgmLineT5S3(Guid refGUID, QBusinessCollateralAgmTable qBusinessCollateralAgmTable)
        {
            try
            {
               var qBusinessCollateralAgmLineT5S3 =(from BusinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                 join businessCollateralType in db.Set<BusinessCollateralType>()
                 on BusinessCollateralAgmLine.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID into ljbusinessCollateralType
                 from businessCollateralType in ljbusinessCollateralType.DefaultIfEmpty()
                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                  on BusinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()
                 where BusinessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                    && businessCollateralType.AgreementOrdering == 5
                    && businessCollateralSubType.AgreementOrdering == 3
                 group new
                 {
                     BusinessCollateralAgmLine.BusinessCollateralSubTypeGUID,
                 }
                by new
                {
                    BusinessCollateralAgmLine.BusinessCollateralAgmLineGUID,

                    BusinessCollateralAgmLine.LineNum,
                    businessCollateralSubType.Description,
                    BusinessCollateralAgmLine.PreferentialCreditorNumber,
                    BusinessCollateralAgmLine.AttachmentText,
                    BusinessCollateralAgmLine.Product

                } into g
                orderby g.Key.LineNum
                 select new QBusinessCollateralAgmLineT5S3
                 {
                     QBusinessCollateralAgmLineT5S3_SubTypeId = g.Key.Description,
                     
                     QBusinessCollateralAgmLineT5S3_PreferentialCreditorNumber = g.Key.PreferentialCreditorNumber,
                     QBusinessCollateralAgmLineT5S3_Row = g.Key.LineNum,
                     QBusinessCollateralAgmLineT5S3_AttachmentText = g.Key.AttachmentText,
                     QBusinessCollateralAgmLineT5S3_Product = g.Key.Product
                 }).ToList();
                var sumBusinesCollateralValue = (from bussinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                 join bussinessCollateralType in db.Set<BusinessCollateralType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralTypeGUID equals bussinessCollateralType.BusinessCollateralTypeGUID into ljbussinessCollateralType
                                                 from bussinessCollateralType in ljbussinessCollateralType.DefaultIfEmpty()

                                                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                                                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()

                                                 where bussinessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                                                     && bussinessCollateralType.AgreementOrdering == 5
                                                     && businessCollateralSubType.AgreementOrdering == 3
                                                 select bussinessCollateralAgmLine.BusinessCollateralValue
                                ).Sum();
                int iqBusinessCollateralAgmLineT5S3 = 1;
                foreach (var item in qBusinessCollateralAgmLineT5S3)
                {
                    item.QBusinessCollateralAgmLineT5S3_Row = iqBusinessCollateralAgmLineT5S3;
                    item.QBusinessCollateralAgmLineT5S3_SumBusinessCollateralValue = sumBusinesCollateralValue;
                    item.QBusinessCollateralAgmLineT5S3_ConutLine = qBusinessCollateralAgmLineT5S3.Count();

                    ++iqBusinessCollateralAgmLineT5S3;
                }
                return qBusinessCollateralAgmLineT5S3;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private List<QBusinessCollateralAgmLineT5S2> GetQBusinessCollateralAgmLineT5S2(Guid refGUID, QBusinessCollateralAgmTable qBusinessCollateralAgmTable)
        {
            try
            {
                var qBusinessCollateralAgmLineT5S2 = (from BusinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                 join businessCollateralType in db.Set<BusinessCollateralType>()
                 on BusinessCollateralAgmLine.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID into ljbusinessCollateralType
                 from businessCollateralType in ljbusinessCollateralType.DefaultIfEmpty()
                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                  on BusinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()
                 where BusinessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                    && businessCollateralType.AgreementOrdering == 5
                    && businessCollateralSubType.AgreementOrdering == 2
                 group new
                 {
                     BusinessCollateralAgmLine.BusinessCollateralSubTypeGUID,
                 }
                by new
                {
                    BusinessCollateralAgmLine.BusinessCollateralAgmLineGUID,

                    BusinessCollateralAgmLine.LineNum,
                    businessCollateralSubType.Description,
                    BusinessCollateralAgmLine.PreferentialCreditorNumber,
                    BusinessCollateralAgmLine.AttachmentText,
                    BusinessCollateralAgmLine.Product

                } into g
                                                      orderby g.Key.LineNum
                                                      select new QBusinessCollateralAgmLineT5S2
                 {
                     QBusinessCollateralAgmLineT5S2_SubTypeId = g.Key.Description,
                     QBusinessCollateralAgmLineT5S2_PreferentialCreditorNumber = g.Key.PreferentialCreditorNumber,
                     QBusinessCollateralAgmLineT5S2_Row = g.Key.LineNum,
                     QBusinessCollateralAgmLineT5S2_AttachmentText = g.Key.AttachmentText,
                     QBusinessCollateralAgmLineT5S2_Product = g.Key.Product
                                                      }).ToList();
                var sumBusinesCollateralValue = (from bussinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                 join bussinessCollateralType in db.Set<BusinessCollateralType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralTypeGUID equals bussinessCollateralType.BusinessCollateralTypeGUID into ljbussinessCollateralType
                                                 from bussinessCollateralType in ljbussinessCollateralType.DefaultIfEmpty()

                                                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                                                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()

                                                 where bussinessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                                                     && bussinessCollateralType.AgreementOrdering == 5
                                                     && businessCollateralSubType.AgreementOrdering == 2
                                                 select bussinessCollateralAgmLine.BusinessCollateralValue
                                ).Sum();
                int iqBusinessCollateralAgmLineT5S2 = 1;
                foreach (var item in qBusinessCollateralAgmLineT5S2)
                {
                    item.QBusinessCollateralAgmLineT5S2_Row = iqBusinessCollateralAgmLineT5S2;
                    item.QBusinessCollateralAgmLineT5S2_SumBusinessCollateralValue = sumBusinesCollateralValue;
                    item.QBusinessCollateralAgmLineT5S2_ConutLine = qBusinessCollateralAgmLineT5S2.Count();


                    ++iqBusinessCollateralAgmLineT5S2;
                }
                return qBusinessCollateralAgmLineT5S2;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private List<QBusinessCollateralAgmLineT5S1> GetQBusinessCollateralAgmLineT5S1(Guid refGUID, QBusinessCollateralAgmTable qBusinessCollateralAgmTable)
        {
            try
            {
                var qBusinessCollateralAgmLineT5S1 =(from BusinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                 join businessCollateralType in db.Set<BusinessCollateralType>()
                 on BusinessCollateralAgmLine.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID into ljbusinessCollateralType
                 from businessCollateralType in ljbusinessCollateralType.DefaultIfEmpty()
                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                  on BusinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()
                 where BusinessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                    && businessCollateralType.AgreementOrdering == 5
                    && businessCollateralSubType.AgreementOrdering == 1
                 group new
                 {
                     BusinessCollateralAgmLine.BusinessCollateralSubTypeGUID,
                 }
                by new
                {
                    BusinessCollateralAgmLine.BusinessCollateralAgmLineGUID,

                    BusinessCollateralAgmLine.LineNum,
                    businessCollateralSubType.Description,
                    BusinessCollateralAgmLine.PreferentialCreditorNumber,
                    BusinessCollateralAgmLine.Product,
                    BusinessCollateralAgmLine.BusinessCollateralValue,
                    BusinessCollateralAgmLineDesc = BusinessCollateralAgmLine.Description,
                    BusinessCollateralAgmLine.AttachmentText,
                    BusinessCollateralAgmLine.Unit

                } into g
                                                     orderby g.Key.LineNum
                                                     select new QBusinessCollateralAgmLineT5S1
                 {
                     QBusinessCollateralAgmLineT5S1_SubTypeId = g.Key.Description,
                     QBusinessCollateralAgmLineT5S1_PreferentialCreditorNumber = g.Key.PreferentialCreditorNumber,
                     QBusinessCollateralAgmLineT5S1_Row = g.Key.LineNum,
                     QBusinessCollateralAgmLineT5S1_AttachmentText = g.Key.AttachmentText,
                     QBusinessCollateralAgmLineT5S1_Unit = g.Key.Unit,
                     QBusinessCollateralAgmLineT5S1_BusinessCollateralAgmLineDesc = g.Key.BusinessCollateralAgmLineDesc,
                     QBusinessCollateralAgmLineT5S1_BusinessCollateralValue = g.Key.BusinessCollateralValue,
                     QBusinessCollateralAgmLineT5S1_Product = g.Key.Product
                 }).ToList();
                var sumBusinesCollateralValue = (from bussinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                 join bussinessCollateralType in db.Set<BusinessCollateralType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralTypeGUID equals bussinessCollateralType.BusinessCollateralTypeGUID into ljbussinessCollateralType
                                                 from bussinessCollateralType in ljbussinessCollateralType.DefaultIfEmpty()

                                                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                                                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()

                                                 where bussinessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                                                     && bussinessCollateralType.AgreementOrdering == 5
                                                     && businessCollateralSubType.AgreementOrdering == 1
                                                 select bussinessCollateralAgmLine.BusinessCollateralValue
                                ).Sum();
                int iqBusinessCollateralAgmLineT5S1 = 1;
                foreach (var item in qBusinessCollateralAgmLineT5S1)
                {
                    item.QBusinessCollateralAgmLineT5S1_Row = iqBusinessCollateralAgmLineT5S1;
                    item.QBusinessCollateralAgmLineT5S1_SumBusinessCollateralValue = sumBusinesCollateralValue;
                    item.QBusinessCollateralAgmLineT5S1_ConutLine = qBusinessCollateralAgmLineT5S1.Count();


                    ++iqBusinessCollateralAgmLineT5S1;
                }
                return qBusinessCollateralAgmLineT5S1;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private List<QBusinessCollateralAgmLineT4S3> GetQBusinessCollateralAgmLineT4S3(Guid refGUID, QBusinessCollateralAgmTable qBusinessCollateralAgmTable)
        {
            try
            {
                var qBusinessCollateralAgmLineT4S3 = (from BusinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                 join businessCollateralType in db.Set<BusinessCollateralType>()
                 on BusinessCollateralAgmLine.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID into ljbusinessCollateralType
                 from businessCollateralType in ljbusinessCollateralType.DefaultIfEmpty()
                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                  on BusinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()
                 where BusinessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                    && businessCollateralType.AgreementOrdering == 4
                    && businessCollateralSubType.AgreementOrdering == 3
                 group new
                 {
                     BusinessCollateralAgmLine.BusinessCollateralSubTypeGUID,
                 }
                by new
                {
                    BusinessCollateralAgmLine.BusinessCollateralAgmLineGUID,
                    BusinessCollateralAgmLine.LineNum,
                    businessCollateralSubType.Description,
                    BusinessCollateralAgmLine.PreferentialCreditorNumber,
                    BusinessCollateralAgmLine.Product,
                    BusinessCollateralAgmLine.AttachmentText

                } into g
                                                      orderby g.Key.LineNum
                                                      select new QBusinessCollateralAgmLineT4S3
                 {


                     QBusinessCollateralAgmLineT4S3_SubTypeId = g.Key.Description,
                     QBusinessCollateralAgmLineT4S3_PreferentialCreditorNumber = g.Key.PreferentialCreditorNumber,
                     QBusinessCollateralAgmLineT4S3_Row = g.Key.LineNum,
                     QBusinessCollateralAgmLineT4S3_AttachmentText = g.Key.AttachmentText,
                     QBusinessCollateralAgmLineT4S3_Product = g.Key.Product
                                                      }).ToList();
                var sumBusinesCollateralValue = (from bussinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                 join bussinessCollateralType in db.Set<BusinessCollateralType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralTypeGUID equals bussinessCollateralType.BusinessCollateralTypeGUID into ljbussinessCollateralType
                                                 from bussinessCollateralType in ljbussinessCollateralType.DefaultIfEmpty()

                                                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                                                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()

                                                 where bussinessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                                                     && bussinessCollateralType.AgreementOrdering == 4
                                                     && businessCollateralSubType.AgreementOrdering == 3
                                                 select bussinessCollateralAgmLine.BusinessCollateralValue
                                ).Sum();
                int iqBusinessCollateralAgmLineT4S3 = 1;
                foreach (var item in qBusinessCollateralAgmLineT4S3)
                {
                    item.QBusinessCollateralAgmLineT4S3_Row = iqBusinessCollateralAgmLineT4S3;
                    item.QBusinessCollateralAgmLineT4S3_SumBusinessCollateralValue = sumBusinesCollateralValue;
                    item.QBusinessCollateralAgmLineT4S3_ConutLine = qBusinessCollateralAgmLineT4S3.Count();

                    ++iqBusinessCollateralAgmLineT4S3;
                }
                return qBusinessCollateralAgmLineT4S3;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private List<QBusinessCollateralAgmLineT4S2> GetQBusinessCollateralAgmLineT4S2(Guid refGUID, QBusinessCollateralAgmTable qBusinessCollateralAgmTable)
        {
            try
            {
                var qBusinessCollateralAgmLineT4S2 = (from BusinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                 join businessCollateralType in db.Set<BusinessCollateralType>()
                 on BusinessCollateralAgmLine.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID into ljbusinessCollateralType
                 from businessCollateralType in ljbusinessCollateralType.DefaultIfEmpty()
                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                  on BusinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()
                 where BusinessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                    && businessCollateralType.AgreementOrdering == 4
                    && businessCollateralSubType.AgreementOrdering == 2
                 group new
                 {
                     BusinessCollateralAgmLine.BusinessCollateralSubTypeGUID,
                 }
                by new
                {
                    BusinessCollateralAgmLine.BusinessCollateralAgmLineGUID,
                    BusinessCollateralAgmLine.LineNum,
                    businessCollateralSubType.Description,
                    BusinessCollateralAgmLine.PreferentialCreditorNumber,
                    BusinessCollateralAgmLine.Product,
                    BusinessCollateralAgmLine.AttachmentText,
                    BusinessCollateralAgmLine.Unit

                } into g
                                                      orderby g.Key.LineNum
                                                      select new QBusinessCollateralAgmLineT4S2
                 {
                     QBusinessCollateralAgmLineT4S2_SubTypeId = g.Key.Description,
                     QBusinessCollateralAgmLineT4S2_PreferentialCreditorNumber = g.Key.PreferentialCreditorNumber,
                     QBusinessCollateralAgmLineT4S2_Row = g.Key.LineNum,
                     QBusinessCollateralAgmLineT4S2_AttachmentText = g.Key.AttachmentText,
                     QBusinessCollateralAgmLineT4S2_Product = g.Key.Product,
                     QBusinessCollateralAgmLineT4S2_Unit = g.Key.Unit
                                                      }).ToList();
                var sumBusinesCollateralValue = (from bussinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                 join bussinessCollateralType in db.Set<BusinessCollateralType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralTypeGUID equals bussinessCollateralType.BusinessCollateralTypeGUID into ljbussinessCollateralType
                                                 from bussinessCollateralType in ljbussinessCollateralType.DefaultIfEmpty()

                                                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                                                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()

                                                 where bussinessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                                                     && bussinessCollateralType.AgreementOrdering == 4
                                                     && businessCollateralSubType.AgreementOrdering == 2
                                                 select bussinessCollateralAgmLine.BusinessCollateralValue
                                ).Sum();
                int iqBusinessCollateralAgmLineT4S2 = 1;
                foreach (var item in qBusinessCollateralAgmLineT4S2)
                {
                    item.QBusinessCollateralAgmLineT4S2_Row = iqBusinessCollateralAgmLineT4S2;
                    item.QBusinessCollateralAgmLineT4S2_SumBusinessCollateralValue = sumBusinesCollateralValue;
                    item.QBusinessCollateralAgmLineT4S2_ConutLine = qBusinessCollateralAgmLineT4S2.Count();

                    ++iqBusinessCollateralAgmLineT4S2;
                }
                return qBusinessCollateralAgmLineT4S2;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private List<QBusinessCollateralAgmLineT4S1> GetQBusinessCollateralAgmLineT4S1(Guid refGUID, QBusinessCollateralAgmTable qBusinessCollateralAgmTable)
        {
            try
            {
                var qBusinessCollateralAgmLineT4S1 =(from BusinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                 join businessCollateralType in db.Set<BusinessCollateralType>()
                 on BusinessCollateralAgmLine.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID into ljbusinessCollateralType
                 from businessCollateralType in ljbusinessCollateralType.DefaultIfEmpty()
                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                  on BusinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()
                 where BusinessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                    && businessCollateralType.AgreementOrdering == 4
                    && businessCollateralSubType.AgreementOrdering == 1
                 group new
                 {
                     BusinessCollateralAgmLine.BusinessCollateralSubTypeGUID,


                 }
                by new
                {
                    BusinessCollateralAgmLine.LineNum,
                    businessCollateralSubType.Description,
                    BusinessCollateralAgmLine.PreferentialCreditorNumber,
                    BusinessCollateralAgmLine.BusinessCollateralAgmLineGUID,
                    BusinessCollateralAgmLine.ProjectName,
                    BusinessCollateralAgmLine.TitleDeedNumber,
                    BusinessCollateralAgmLine.TitleDeedSubDistrict,
                    BusinessCollateralAgmLine.TitleDeedDistrict,
                    BusinessCollateralAgmLine.TitleDeedProvince,
                    BusinessCollateralAgmLine.Product,
                    BusinessCollateralAgmLine.AttachmentText,
                    BusinessCollateralAgmLine.Unit

                } into g
                                                     orderby g.Key.LineNum
                                                     select new QBusinessCollateralAgmLineT4S1
                 {
                     QBusinessCollateralAgmLineT4S1_SubTypeId = g.Key.Description,
                     QBusinessCollateralAgmLineT4S1_PreferentialCreditorNumber = g.Key.PreferentialCreditorNumber,
                     QBusinessCollateralAgmLineT4S1_Row = g.Key.LineNum,
                     QBusinessCollateralAgmLineT4S1_ProjectName = g.Key.ProjectName,
                     QBusinessCollateralAgmLineT4S1_TitleDeedNumber = g.Key.TitleDeedNumber,
                     QBusinessCollateralAgmLineT4S1_TitleDeedSubDistrict = g.Key.TitleDeedSubDistrict,
                     QBusinessCollateralAgmLineT4S1_TitleDeedDistrict = g.Key.TitleDeedDistrict,
                     QBusinessCollateralAgmLineT4S1_TitleDeedProvince = g.Key.TitleDeedProvince,
                     QBusinessCollateralAgmLineT4S1_AttachmentText = g.Key.AttachmentText,
                     QBusinessCollateralAgmLineT4S1_Product = g.Key.Product,
                     QBusinessCollateralAgmLineT4S1_Unit = g.Key.Unit

                                                     }
                                                                                 ).ToList();
                var sumBusinesCollateralValue = (from bussinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                 join bussinessCollateralType in db.Set<BusinessCollateralType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralTypeGUID equals bussinessCollateralType.BusinessCollateralTypeGUID into ljbussinessCollateralType
                                                 from bussinessCollateralType in ljbussinessCollateralType.DefaultIfEmpty()

                                                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                                                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()

                                                 where bussinessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                                                     && bussinessCollateralType.AgreementOrdering == 4
                                                     && businessCollateralSubType.AgreementOrdering == 1
                                                 select bussinessCollateralAgmLine.BusinessCollateralValue
                                ).Sum();
                int iqBusinessCollateralAgmLineT4S1 = 1;
                foreach (var item in qBusinessCollateralAgmLineT4S1)
                {
                    item.QBusinessCollateralAgmLineT4S1_Row = iqBusinessCollateralAgmLineT4S1;
                    item.QBusinessCollateralAgmLineT4S1_SumBusinessCollateralValue = sumBusinesCollateralValue;
                    item.QBusinessCollateralAgmLineT4S1_ConutLine = qBusinessCollateralAgmLineT4S1.Count();

                    ++iqBusinessCollateralAgmLineT4S1;
                }
                return qBusinessCollateralAgmLineT4S1;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private List<QBusinessCollateralAgmLineT3S5> GetQBusinessCollateralAgmLineT3S5(Guid refGUID, QBusinessCollateralAgmTable qBusinessCollateralAgmTable)
        {
            try
            {
                var qBusinessCollateralAgmLineT3S5 = (from BusinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                 join businessCollateralType in db.Set<BusinessCollateralType>()
                 on BusinessCollateralAgmLine.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID into ljbusinessCollateralType
                 from businessCollateralType in ljbusinessCollateralType.DefaultIfEmpty()
                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                  on BusinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()
                 where BusinessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                    && businessCollateralType.AgreementOrdering == 3
                    && businessCollateralSubType.AgreementOrdering == 5
                 group new
                 {
                     BusinessCollateralAgmLine.BusinessCollateralSubTypeGUID,


                 }
                by new
                {
                    BusinessCollateralAgmLine.BusinessCollateralAgmLineGUID,
                    BusinessCollateralAgmLine.LineNum,
                    businessCollateralSubType.Description,
                    BusinessCollateralAgmLine.PreferentialCreditorNumber,
                    BusinessCollateralAgmLine.BusinessCollateralValue,
                    BusinessCollateralAgmLine.Quantity,
                    BusinessCollateralAgmLine.Product,
                    BusinessCollateralAgmLine.AttachmentText

                } into g
                                                      orderby g.Key.LineNum
                                                      select new QBusinessCollateralAgmLineT3S5
                 {
                     QBusinessCollateralAgmLineT3S5_SubTypeId = g.Key.Description,
                     QBusinessCollateralAgmLineT3S5_AttachmentText = g.Key.AttachmentText,
                     QBusinessCollateralAgmLineT3S5_PreferentialCreditorNumber = g.Key.PreferentialCreditorNumber,
                     QBusinessCollateralAgmLineT3S5_Row = g.Key.LineNum,
                     QBusinessCollateralAgmLineT3S5_Quantity = g.Key.Quantity,
                     QBusinessCollateralAgmLineT3S5_BusinessCollateralValue = g.Key.BusinessCollateralValue,
                     QBusinessCollateralAgmLineT3S5_Product = g.Key.Product

                 }
                                                                                 ).ToList();
                var sumBusinesCollateralValue = (from bussinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                 join bussinessCollateralType in db.Set<BusinessCollateralType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralTypeGUID equals bussinessCollateralType.BusinessCollateralTypeGUID into ljbussinessCollateralType
                                                 from bussinessCollateralType in ljbussinessCollateralType.DefaultIfEmpty()

                                                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                                                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()

                                                 where bussinessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                                                     && bussinessCollateralType.AgreementOrdering == 3
                                                     && businessCollateralSubType.AgreementOrdering == 5
                                                 select bussinessCollateralAgmLine.BusinessCollateralValue
                                ).Sum();
                int iqBusinessCollateralAgmLineT3S5 = 1;
                foreach (var item in qBusinessCollateralAgmLineT3S5)
                {
                    item.QBusinessCollateralAgmLineT3S5_Row = iqBusinessCollateralAgmLineT3S5;
                    item.QBusinessCollateralAgmLineT3S5_SumBusinessCollateralValue = sumBusinesCollateralValue;
                    item.QBusinessCollateralAgmLineT3S5_ConutLine = qBusinessCollateralAgmLineT3S5.Count();

                    ++iqBusinessCollateralAgmLineT3S5;
                }
                return qBusinessCollateralAgmLineT3S5;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private List<QBusinessCollateralAgmLineT3S4> GetQBusinessCollateralAgmLineT3S4(Guid refGUID, QBusinessCollateralAgmTable qBusinessCollateralAgmTable)
        {
            try
            {
                var qBusinessCollateralAgmLineT3S4 =(from BusinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                 join businessCollateralType in db.Set<BusinessCollateralType>()
                 on BusinessCollateralAgmLine.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID into ljbusinessCollateralType
                 from businessCollateralType in ljbusinessCollateralType.DefaultIfEmpty()
                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                  on BusinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()
                 where BusinessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                    && businessCollateralType.AgreementOrdering == 3
                    && businessCollateralSubType.AgreementOrdering == 4
                 group new
                 {
                     BusinessCollateralAgmLine.BusinessCollateralSubTypeGUID,


                 }
                by new
                {
                    BusinessCollateralAgmLine.BusinessCollateralAgmLineGUID,
                    BusinessCollateralAgmLine.LineNum,
                    businessCollateralSubType.Description,
                    BusinessCollateralAgmLine.PreferentialCreditorNumber,
                    BusinessCollateralAgmLine.BusinessCollateralValue,
                    BusinessCollateralAgmLine.Quantity,
                    BusinessCollateralAgmLine.MachineNumber,
                    BusinessCollateralAgmLine.ChassisNumber,
                    BusinessCollateralAgmLine.RegistrationPlateNumber,
                    BusinessCollateralAgmLine.Product,
                    BusinessCollateralAgmLine.Unit,
                    BusinessCollateralAgmLine.AttachmentText

                } into g
                                                     orderby g.Key.LineNum
                                                     select new QBusinessCollateralAgmLineT3S4
                 {
                     QBusinessCollateralAgmLineT3S4_SubTypeId = g.Key.Description,
                                                         QBusinessCollateralAgmLineT3S4_AttachmentText = g.Key.AttachmentText,
                                                         QBusinessCollateralAgmLineT3S4_Unit = g.Key.Unit,
                     QBusinessCollateralAgmLineT3S4_PreferentialCreditorNumber = g.Key.PreferentialCreditorNumber,
                     QBusinessCollateralAgmLineT3S4_Row = g.Key.LineNum,
                     QBusinessCollateralAgmLineT3S4_Quantity = g.Key.Quantity,
                     QBusinessCollateralAgmLineT3S4_BusinessCollateralValue = g.Key.BusinessCollateralValue,
                     QBusinessCollateralAgmLineT3S4_MachineNumber = g.Key.MachineNumber,
                     QBusinessCollateralAgmLineT3S4_ChassisNumber = g.Key.ChassisNumber,
                     QBusinessCollateralAgmLineT3S4_RegistrationPlateNumber = g.Key.RegistrationPlateNumber,
                     QBusinessCollateralAgmLineT3S4_Product = g.Key.Product


                 }
                                                                                ).ToList();
                var sumBusinesCollateralValue = (from bussinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                 join bussinessCollateralType in db.Set<BusinessCollateralType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralTypeGUID equals bussinessCollateralType.BusinessCollateralTypeGUID into ljbussinessCollateralType
                                                 from bussinessCollateralType in ljbussinessCollateralType.DefaultIfEmpty()

                                                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                                                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()

                                                 where bussinessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                                                     && bussinessCollateralType.AgreementOrdering == 3
                                                     && businessCollateralSubType.AgreementOrdering == 4
                                                 select bussinessCollateralAgmLine.BusinessCollateralValue
                                ).Sum();
                int iqBusinessCollateralAgmLineT3S4 = 1;
                foreach (var item in qBusinessCollateralAgmLineT3S4)
                {
                    item.QBusinessCollateralAgmLineT3S4_Row = iqBusinessCollateralAgmLineT3S4;
                    item.QBusinessCollateralAgmLineT3S4_SumBusinessCollateralValue = sumBusinesCollateralValue;
                    item.QBusinessCollateralAgmLineT3S4_ConutLine = qBusinessCollateralAgmLineT3S4.Count();

                    ++iqBusinessCollateralAgmLineT3S4;
                }
                return qBusinessCollateralAgmLineT3S4;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private List<QBusinessCollateralAgmLineT3S3> GetQBusinessCollateralAgmLineT3S3(Guid refGUID, QBusinessCollateralAgmTable qBusinessCollateralAgmTable)
        {
            try
            {
                var qBusinessCollateralAgmLineT3S3 =(from BusinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                 join businessCollateralType in db.Set<BusinessCollateralType>()
                 on BusinessCollateralAgmLine.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID into ljbusinessCollateralType
                 from businessCollateralType in ljbusinessCollateralType.DefaultIfEmpty()
                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                  on BusinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()
                 where BusinessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                    && businessCollateralType.AgreementOrdering == 3
                    && businessCollateralSubType.AgreementOrdering == 3
                 group new
                 {
                     BusinessCollateralAgmLine.BusinessCollateralSubTypeGUID,

                 }
                by new
                {
                    BusinessCollateralAgmLine.BusinessCollateralAgmLineGUID,
                    BusinessCollateralAgmLine.BusinessCollateralValue,
                    BusinessCollateralAgmLine.LineNum,
                    businessCollateralSubType.Description,
                    BusinessCollateralAgmLine.PreferentialCreditorNumber,
                    BusinessCollateralAgmLine.Product,
                    BusinessCollateralAgmLine.Quantity,
                    BusinessCollateralAgmLine.AttachmentText,
                    BusinessCollateralAgmLine.Unit

                } into g
                                                     orderby g.Key.LineNum
                                                     select new QBusinessCollateralAgmLineT3S3
                 {
                     QBusinessCollateralAgmLineT3S3_SubTypeId = g.Key.Description,
                     QBusinessCollateralAgmLineT3S3_Product = g.Key.Product,
                                                         QBusinessCollateralAgmLineT3S3_AttachmentText = g.Key.AttachmentText,
                                                         QBusinessCollateralAgmLineT3S3_Unit = g.Key.Unit,
                     QBusinessCollateralAgmLineT3S3_PreferentialCreditorNumber = g.Key.PreferentialCreditorNumber,
                     QBusinessCollateralAgmLineT3S3_Row = g.Key.LineNum,
                     QBusinessCollateralAgmLineT3S3_Quantity = g.Key.Quantity,
                     QBusinessCollateralAgmLineT3S3_BusinessCollateralValue = g.Key.BusinessCollateralValue
                 }
                                                                                 ).ToList();
                var sumBusinesCollateralValue = (from bussinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                 join bussinessCollateralType in db.Set<BusinessCollateralType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralTypeGUID equals bussinessCollateralType.BusinessCollateralTypeGUID into ljbussinessCollateralType
                                                 from bussinessCollateralType in ljbussinessCollateralType.DefaultIfEmpty()

                                                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                                                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()

                                                 where bussinessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                                                     && bussinessCollateralType.AgreementOrdering == 3
                                                     && businessCollateralSubType.AgreementOrdering == 3
                                                 select bussinessCollateralAgmLine.BusinessCollateralValue
                                ).Sum();
                int iqBusinessCollateralAgmLineT3S3 = 1;
                foreach (var item in qBusinessCollateralAgmLineT3S3)
                {
                    item.QBusinessCollateralAgmLineT3S3_Row = iqBusinessCollateralAgmLineT3S3;
                    item.QBusinessCollateralAgmLineT3S3_SumBusinessCollateralValue = sumBusinesCollateralValue;
                    item.QBusinessCollateralAgmLineT3S3_ConutLine = qBusinessCollateralAgmLineT3S3.Count();

                    ++iqBusinessCollateralAgmLineT3S3;
                }
                return qBusinessCollateralAgmLineT3S3;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private List<QBusinessCollateralAgmLineT3S2> GetQBusinessCollateralAgmLineT3S2(Guid refGUID, QBusinessCollateralAgmTable qBusinessCollateralAgmTable)
        {
            try
            {
                var qBusinessCollateralAgmLineT3S2 = (from BusinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                 join businessCollateralType in db.Set<BusinessCollateralType>()
                 on BusinessCollateralAgmLine.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID into ljbusinessCollateralType
                 from businessCollateralType in ljbusinessCollateralType.DefaultIfEmpty()
                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                  on BusinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()
                 where BusinessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                    && businessCollateralType.AgreementOrdering == 3
                    && businessCollateralSubType.AgreementOrdering == 2
                 
                 group new
                 {
                     BusinessCollateralAgmLine.BusinessCollateralSubTypeGUID,


                 }
                by new
                {
                    BusinessCollateralAgmLine.BusinessCollateralAgmLineGUID,
                    BusinessCollateralAgmLine.LineNum,
                    businessCollateralSubType.Description,
                    BusinessCollateralAgmLine.PreferentialCreditorNumber,
                    BusinessCollateralAgmLine.BusinessCollateralValue,
                    BusinessCollateralAgmLine.Product,
                    BusinessCollateralAgmLine.Quantity,
                    BusinessCollateralAgmLine.Unit,
                    BusinessCollateralAgmLine.AttachmentText

                } into g
                                                      orderby g.Key.LineNum
                                                      select new QBusinessCollateralAgmLineT3S2
                 {
                     QBusinessCollateralAgmLineT3S2_SubTypeId = g.Key.Description,
                                                          QBusinessCollateralAgmLineT3S2_AttachmentText = g.Key.AttachmentText,
                                                          QBusinessCollateralAgmLineT3S2_Unit = g.Key.Unit,
                     QBusinessCollateralAgmLineT3S2_PreferentialCreditorNumber = g.Key.PreferentialCreditorNumber,
                     QBusinessCollateralAgmLineT3S2_Row = g.Key.LineNum,
                     QBusinessCollateralAgmLineT3S2_Quantity = g.Key.Quantity,
                     QBusinessCollateralAgmLineT3S2_BusinessCollateralValue = g.Key.BusinessCollateralValue,
                     QBusinessCollateralAgmLineT3S2_Product = g.Key.Product

                 }
                                                                                 ).ToList();
                var sumBusinesCollateralValue = (from bussinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                 join bussinessCollateralType in db.Set<BusinessCollateralType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralTypeGUID equals bussinessCollateralType.BusinessCollateralTypeGUID into ljbussinessCollateralType
                                                 from bussinessCollateralType in ljbussinessCollateralType.DefaultIfEmpty()

                                                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                                                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()

                                                 where bussinessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                                                     && bussinessCollateralType.AgreementOrdering == 3
                                                     && businessCollateralSubType.AgreementOrdering == 2
                                                 select bussinessCollateralAgmLine.BusinessCollateralValue
                                ).Sum();
                int iqBusinessCollateralAgmLineT3S2 = 1;
                foreach (var item in qBusinessCollateralAgmLineT3S2)
                {
                    item.QBusinessCollateralAgmLineT3S2_Row = iqBusinessCollateralAgmLineT3S2;
                    item.QBusinessCollateralAgmLineT3S2_SumBusinessCollateralValue = sumBusinesCollateralValue;
                    item.QBusinessCollateralAgmLineT3S2_ConutLine = qBusinessCollateralAgmLineT3S2.Count();

                    ++iqBusinessCollateralAgmLineT3S2;
                }
                return qBusinessCollateralAgmLineT3S2;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private List<QBusinessCollateralAgmLineT3S1> GetQBusinessCollateralAgmLineT3S1(Guid refGUID, QBusinessCollateralAgmTable qBusinessCollateralAgmTable)
        {
            try
            {
                var qBusinessCollateralAgmLineT3S1 = (from BusinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                 join businessCollateralType in db.Set<BusinessCollateralType>()
                 on BusinessCollateralAgmLine.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID into ljbusinessCollateralType
                 from businessCollateralType in ljbusinessCollateralType.DefaultIfEmpty()
                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                  on BusinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()
                 where BusinessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                    && businessCollateralType.AgreementOrdering == 3
                    && businessCollateralSubType.AgreementOrdering == 1
    
                 group new
                 {
                     BusinessCollateralAgmLine.BusinessCollateralSubTypeGUID

                 }
                by new
                {
                    BusinessCollateralAgmLine.LineNum,
                    BusinessCollateralAgmLine.BusinessCollateralAgmLineGUID,
                    businessCollateralSubType.Description,
                    BusinessCollateralAgmLine.PreferentialCreditorNumber,
                    BusinessCollateralAgmLine.RegistrationPlateNumber,
                    BusinessCollateralAgmLine.MachineNumber,
                    BusinessCollateralAgmLine.MachineRegisteredStatus,
                    BusinessCollateralAgmLine.RegisteredPlace,
                    BusinessCollateralAgmLine.BusinessCollateralValue,
                    BusinessCollateralAgmLine.Product,
                    BusinessCollateralAgmLine.AttachmentText,
                    BusinessCollateralAgmLine.Unit

                } into g
                                                      orderby g.Key.LineNum
                                                      select new QBusinessCollateralAgmLineT3S1
                 {
                     QBusinessCollateralAgmLineT3S1_SubTypeId = g.Key.Description,
                                                          QBusinessCollateralAgmLineT3S1_AttachmentText = g.Key.AttachmentText,
                                                          QBusinessCollateralAgmLineT3S1_Unit = g.Key.Unit,
                     QBusinessCollateralAgmLineT3S1_PreferentialCreditorNumber = g.Key.PreferentialCreditorNumber,
                     QBusinessCollateralAgmLineT3S1_Row = g.Key.LineNum,
                     QBusinessCollateralAgmLineT3S1_RegistrationPlateNumber = g.Key.RegistrationPlateNumber,
                     QBusinessCollateralAgmLineT3S1_MachineNumber = g.Key.MachineNumber,
                     QBusinessCollateralAgmLineT3S1_MachineRegisteredStatus = g.Key.MachineRegisteredStatus,
                     QBusinessCollateralAgmLineT3S1_RegisteredPlace = g.Key.RegisteredPlace,
                     QBusinessCollateralAgmLineT3S1_BusinessCollateralValue = g.Key.BusinessCollateralValue,
                     QBusinessCollateralAgmLineT3S1_Product = g.Key.Product


                 }
                                                                                ).ToList();
                var sumBusinesCollateralValue = (from bussinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                 join bussinessCollateralType in db.Set<BusinessCollateralType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralTypeGUID equals bussinessCollateralType.BusinessCollateralTypeGUID into ljbussinessCollateralType
                                                 from bussinessCollateralType in ljbussinessCollateralType.DefaultIfEmpty()

                                                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                                                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()

                                                 where bussinessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                                                     && bussinessCollateralType.AgreementOrdering == 3
                                                     && businessCollateralSubType.AgreementOrdering == 1
                                                 select bussinessCollateralAgmLine.BusinessCollateralValue
                                ).Sum();
                int iqBusinessCollateralAgmLineT3S1 = 1;
                foreach (var item in qBusinessCollateralAgmLineT3S1)
                {
                    item.QBusinessCollateralAgmLineT3S1_Row = iqBusinessCollateralAgmLineT3S1;
                    item.QBusinessCollateralAgmLineT3S1_SumBusinessCollateralValue = sumBusinesCollateralValue;
                    item.QBusinessCollateralAgmLineT3S1_ConutLine = qBusinessCollateralAgmLineT3S1.Count();

                    ++iqBusinessCollateralAgmLineT3S1;
                }
                return qBusinessCollateralAgmLineT3S1;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private List<QBusinessCollateralAgmLineT2S2> GetQBusinessCollateralAgmLineT2S2(Guid refGUID, QBusinessCollateralAgmTable qBusinessCollateralAgmTable)
        {
            try
            {
               var qBusinessCollateralAgmLineT2S2 = (from businessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                 join businessCollateralType in db.Set<BusinessCollateralType>()
                 on businessCollateralAgmLine.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID into ljbusinessCollateralType
                 from businessCollateralType in ljbusinessCollateralType.DefaultIfEmpty()
                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                  on businessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()
                 join buyerTable in db.Set<BuyerTable>()
                  on businessCollateralAgmLine.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
                 from buyerTable in ljbuyerTable.DefaultIfEmpty()
                 where businessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                    && businessCollateralType.AgreementOrdering == 2
                    && businessCollateralSubType.AgreementOrdering == 2
                
                 group new
                 {
                     businessCollateralAgmLine.BusinessCollateralSubTypeGUID,


                 }
                by new
                {
                    businessCollateralAgmLine.BusinessCollateralAgmLineGUID,
                    businessCollateralAgmLine.LineNum,
                    businessCollateralAgmLine.AccountNumber,
                    businessCollateralSubType.Description,
                    businessCollateralAgmLine.PreferentialCreditorNumber,
                    buyerTable.BuyerName,
                    buyerTable.IdentificationType,
                    buyerTable.TaxId,
                    buyerTable.PassportId,
                    businessCollateralAgmLine.BuyerTaxIdentificationId,
                    businessCollateralAgmLine.Product,
                    businessCollateralAgmLine.AttachmentText
                } into g
                                                     orderby g.Key.LineNum
                                                     select new QBusinessCollateralAgmLineT2S2
                 {
                     QBusinessCollateralAgmLineT2S2_SubTypeId = g.Key.Description,
                                                         QBusinessCollateralAgmLineT2S2_AttachmentText = g.Key.AttachmentText,
                                                         QBusinessCollateralAgmLineT2S2_Product = g.Key.Product,
                     QBusinessCollateralAgmLineT2S2_PreferentialCreditorNumber = g.Key.PreferentialCreditorNumber,
                     QBusinessCollateralAgmLineT2S2_Row = g.Key.LineNum,
                     QBusinessCollateralAgmLineT2S2_BuyerName = g.Key.BuyerName,
                     QBusinessCollateralAgmLineT2S2_IdentificationType = g.Key.IdentificationType,
                     QBusinessCollateralAgmLineT2S2_TaxId = g.Key.BuyerTaxIdentificationId,
                     QBusinessCollateralAgmLineT2S2_PassportId = g.Key.PassportId,

                 }
                                                                                 ).ToList();
                var sumBusinesCollateralValue = (from bussinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                 join bussinessCollateralType in db.Set<BusinessCollateralType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralTypeGUID equals bussinessCollateralType.BusinessCollateralTypeGUID into ljbussinessCollateralType
                                                 from bussinessCollateralType in ljbussinessCollateralType.DefaultIfEmpty()

                                                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                                                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()

                                                 where bussinessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                                                     && bussinessCollateralType.AgreementOrdering == 2
                                                     && businessCollateralSubType.AgreementOrdering == 2
                                                 select bussinessCollateralAgmLine.BusinessCollateralValue
                                ).Sum();
                int iqBusinessCollateralAgmLineT2S2 = 1;
                foreach (var item in qBusinessCollateralAgmLineT2S2)
                {
                    item.QBusinessCollateralAgmLineT2S2_Row = iqBusinessCollateralAgmLineT2S2;
                    item.QBusinessCollateralAgmLineT2S2_SumBusinessCollateralValue = sumBusinesCollateralValue;
                    item.QBusinessCollateralAgmLineT2S2_ConutLine = qBusinessCollateralAgmLineT2S2.Count();


                    ++iqBusinessCollateralAgmLineT2S2;
                }
                return qBusinessCollateralAgmLineT2S2;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private List<QBusinessCollateralAgmLineT2S1> GetQBusinessCollateralAgmLineT2S1(Guid refGUID, QBusinessCollateralAgmTable qBusinessCollateralAgmTable)
        {
            try
            {
               var qBusinessCollateralAgmLineT2S1 = (from businessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                 join businessCollateralType in db.Set<BusinessCollateralType>()
                 on businessCollateralAgmLine.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID into ljbusinessCollateralType
                 from businessCollateralType in ljbusinessCollateralType.DefaultIfEmpty()
                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                  on businessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()
                 join buyerTable in db.Set<BuyerTable>()
                 on businessCollateralAgmLine.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
                 from buyerTable in ljbuyerTable.DefaultIfEmpty()
                 where businessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                    && businessCollateralType.AgreementOrdering == 2
                    && businessCollateralSubType.AgreementOrdering == 1
                 
                 group new
                 {
                     businessCollateralAgmLine.BusinessCollateralSubTypeGUID,

                 }
                by new
                {
                    businessCollateralAgmLine.BusinessCollateralAgmLineGUID,
                    businessCollateralAgmLine.LineNum,
                    businessCollateralAgmLine.AccountNumber,
                    businessCollateralSubType.Description,
                    businessCollateralAgmLine.PreferentialCreditorNumber,
                    businessCollateralAgmLine.BuyerTaxIdentificationId,
                    buyerTable.BuyerName,
                    buyerTable.IdentificationType,
                    buyerTable.TaxId,
                    buyerTable.PassportId,
                    businessCollateralAgmLine.Product,
                    businessCollateralAgmLine.AttachmentText
                } into g
                                                     orderby g.Key.LineNum
                                                     select new QBusinessCollateralAgmLineT2S1
                 {
                     QBusinessCollateralAgmLineT2S1_SubTypeId = g.Key.Description,
                                                         QBusinessCollateralAgmLineT2S1_AttachmentText = g.Key.AttachmentText,
                                                         QBusinessCollateralAgmLineT2S1_Product = g.Key.Product,
                     QBusinessCollateralAgmLineT2S1_PreferentialCreditorNumber = g.Key.PreferentialCreditorNumber,
                     QBusinessCollateralAgmLineT2S1_BuyerName = g.Key.BuyerName,
                     QBusinessCollateralAgmLineT2S1_IdentificationType = g.Key.IdentificationType,
                     QBusinessCollateralAgmLineT2S1_TaxId = g.Key.BuyerTaxIdentificationId,
                     QBusinessCollateralAgmLineT2S1_PassportId = g.Key.PassportId,
                     QBusinessCollateralAgmLineT2S1_Row = g.Key.LineNum


                 }
                                                                                ).ToList();
                var sumBusinesCollateralValue = (from bussinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                 join bussinessCollateralType in db.Set<BusinessCollateralType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralTypeGUID equals bussinessCollateralType.BusinessCollateralTypeGUID into ljbussinessCollateralType
                                                 from bussinessCollateralType in ljbussinessCollateralType.DefaultIfEmpty()

                                                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                                                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()

                                                 where bussinessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                                                     && bussinessCollateralType.AgreementOrdering == 2
                                                     && businessCollateralSubType.AgreementOrdering == 1
                                                 select bussinessCollateralAgmLine.BusinessCollateralValue
                                ).Sum();
                int iqBusinessCollateralAgmLineT2S1 = 1;
                foreach (var item in qBusinessCollateralAgmLineT2S1)
                {
                    item.QBusinessCollateralAgmLineT2S1_Row = iqBusinessCollateralAgmLineT2S1;
                    item.QBusinessCollateralAgmLineT2S1_SumBusinessCollateralValue = sumBusinesCollateralValue;
                    item.QBusinessCollateralAgmLineT2S1_ConutLine = qBusinessCollateralAgmLineT2S1.Count();

                    ++iqBusinessCollateralAgmLineT2S1;
                }
                return qBusinessCollateralAgmLineT2S1;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private List<QBusinessCollateralAgmLineT1S3> GetQBusinessCollateralAgmLineT1S3(Guid refGUID, QBusinessCollateralAgmTable qBusinessCollateralAgmTable)
        {
            try
            {
                var qBusinessCollateralAgmLineT1S3 = (from businessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                 join businessCollateralType in db.Set<BusinessCollateralType>()
                 on businessCollateralAgmLine.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID into ljbusinessCollateralType
                 from businessCollateralType in ljbusinessCollateralType.DefaultIfEmpty()
                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                 on businessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()
                 where businessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                 && businessCollateralType.AgreementOrdering == 1
                 && businessCollateralSubType.AgreementOrdering == 3
                 
                 group new
                 {
                     businessCollateralAgmLine.BusinessCollateralSubTypeGUID,

                 }
                by new
                {
                    businessCollateralAgmLine.BusinessCollateralAgmLineGUID,
                    businessCollateralAgmLine.RefAgreementId,
                    businessCollateralAgmLine.Description,
                    businessCollateralAgmLine.RefAgreementDate,
                    businessCollateralAgmLine.Lessee,
                    businessCollateralAgmLine.Lessor,
                    businessCollateralAgmLine.LineNum,
                    SubTypeDescription  = businessCollateralSubType.Description,
                    businessCollateralAgmLine.PreferentialCreditorNumber,
                    businessCollateralAgmLine.Product,
                    businessCollateralAgmLine.AttachmentText
                } into g
                                                      orderby g.Key.LineNum
                                                      select new QBusinessCollateralAgmLineT1S3
                 {
                     QBusinessCollateralAgmLineT1S3_SubTypeId = g.Key.SubTypeDescription,
                                                          QBusinessCollateralAgmLineT1S3_AttachmentText = g.Key.AttachmentText,
                                                          QBusinessCollateralAgmLineT1S3_Product =  g.Key.Product,
                     QBusinessCollateralAgmLineT1S3_PreferentialCreditorNumber = g.Key.PreferentialCreditorNumber,
                     QBusinessCollateralAgmLineT1S3_Row = g.Key.LineNum,
                     QBusinessCollateralAgmLineT1S3_Lessee = g.Key.Lessee,
                     QBusinessCollateralAgmLineT1S3_Lessor = g.Key.Lessor,
                     QBusinessCollateralAgmLineT1S3_RefAgreementId = g.Key.RefAgreementId,
                     QBusinessCollateralAgmLineT1S3_RefAgreementDate = g.Key.RefAgreementDate,
                     QBusinessCollateralAgmLineT1S3_Description = g.Key.Description,

                 }
                                                                                 ).ToList();
                var sumBusinesCollateralValue = (from bussinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                 join bussinessCollateralType in db.Set<BusinessCollateralType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralTypeGUID equals bussinessCollateralType.BusinessCollateralTypeGUID into ljbussinessCollateralType
                                                 from bussinessCollateralType in ljbussinessCollateralType.DefaultIfEmpty()

                                                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                                                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()

                                                 where bussinessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                                                     && bussinessCollateralType.AgreementOrdering == 1
                                                     && businessCollateralSubType.AgreementOrdering == 3
                                                 select bussinessCollateralAgmLine.BusinessCollateralValue
                                ).Sum();

                int iqBusinessCollateralAgmLineT1S3 = 1;
                foreach (var item in qBusinessCollateralAgmLineT1S3)
                {
                    item.QBusinessCollateralAgmLineT1S3_Row = iqBusinessCollateralAgmLineT1S3;
                    item.QBusinessCollateralAgmLineT1S3_SumBusinessCollateralValue = sumBusinesCollateralValue;
                    item.QBusinessCollateralAgmLineT1S3_ConutLine = qBusinessCollateralAgmLineT1S3.Count();
                    ++iqBusinessCollateralAgmLineT1S3;
                }
                return qBusinessCollateralAgmLineT1S3;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private List<QBusinessCollateralAgmLineT1S2> GetQBusinessCollateralAgmLineT1S2(Guid refGUID, QBusinessCollateralAgmTable qBusinessCollateralAgmTable)
        {
            try
            {
                var qBusinessCollateralAgmLineT1S2 = (from businessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                 join businessCollateralType in db.Set<BusinessCollateralType>()
                 on businessCollateralAgmLine.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID into ljbusinessCollateralType
                 from businessCollateralType in ljbusinessCollateralType.DefaultIfEmpty()
                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                 on businessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()
                 where businessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                 && businessCollateralType.AgreementOrdering == 1
                 && businessCollateralSubType.AgreementOrdering == 2
                
                 group new
                 {
                     businessCollateralAgmLine.BusinessCollateralSubTypeGUID,

                 }
                by new
                {
                    businessCollateralAgmLine.BusinessCollateralAgmLineGUID,
                    businessCollateralAgmLine.RefAgreementId,
                    businessCollateralAgmLine.RefAgreementDate,
                    businessCollateralAgmLine.Lessee,
                    businessCollateralAgmLine.Lessor,
                    businessCollateralAgmLine.LineNum,
                    businessCollateralAgmLine.BankGroupGUID,
                    businessCollateralAgmLine.BankTypeGUID,
                    businessCollateralAgmLine.AccountNumber,
                    businessCollateralSubType.Description,
                    businessCollateralAgmLine.PreferentialCreditorNumber,
                    businessCollateralAgmLine.Product,
                    businessCollateralAgmLine.AttachmentText,
                } into g
                                                      orderby g.Key.LineNum
                                                      select new QBusinessCollateralAgmLineT1S2
                 {
                     QBusinessCollateralAgmLineT1S2_SubTypeId = g.Key.Description,
                                                          QBusinessCollateralAgmLineT1S2_AttachmentText = g.Key.AttachmentText,
                                                          QBusinessCollateralAgmLineT1S2_Product = g.Key.Product,
                     QBusinessCollateralAgmLineT1S2_PreferentialCreditorNumber = g.Key.PreferentialCreditorNumber,
                     QBusinessCollateralAgmLineT1S2_Row = g.Key.LineNum,
                     QBusinessCollateralAgmLineT1S2_Lessee = g.Key.Lessee,
                     QBusinessCollateralAgmLineT1S2_Lessor = g.Key.Lessor,
                     QBusinessCollateralAgmLineT1S2_RefAgreementId = g.Key.RefAgreementId,
                     QBusinessCollateralAgmLineT1S2_RefAgreementDate = g.Key.RefAgreementDate,

                 }
                                                                                 ).ToList();
                var sumBusinesCollateralValue = (from bussinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                 join bussinessCollateralType in db.Set<BusinessCollateralType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralTypeGUID equals bussinessCollateralType.BusinessCollateralTypeGUID into ljbussinessCollateralType
                                                 from bussinessCollateralType in ljbussinessCollateralType.DefaultIfEmpty()

                                                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                                                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()

                                                 where bussinessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                                                     && bussinessCollateralType.AgreementOrdering == 1
                                                     && businessCollateralSubType.AgreementOrdering == 2
                                                 select bussinessCollateralAgmLine.BusinessCollateralValue
                                                ).Sum();
                int iqBusinessCollateralAgmLineT1S2 = 1;
                foreach (var item in qBusinessCollateralAgmLineT1S2)
                {
                    item.QBusinessCollateralAgmLineT1S2_Row = iqBusinessCollateralAgmLineT1S2;
                    item.QBusinessCollateralAgmLineT1S2_SumBusinessCollateralValue = sumBusinesCollateralValue;
                    item.QBusinessCollateralAgmLineT1S2_ConutLine = qBusinessCollateralAgmLineT1S2.Count();

                    ++iqBusinessCollateralAgmLineT1S2;
                }
                return qBusinessCollateralAgmLineT1S2;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private List<QBusinessCollateralAgmLineT1S1> GetQBusinessCollateralAgmLineT1S1(Guid refGUID, QBusinessCollateralAgmTable qBusinessCollateralAgmTable)
        {
            try
            {
               var qBusinessCollateralAgmLineT1S1 = (from bussinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                        join bussinessCollateralType in db.Set<BusinessCollateralType>()
                        on bussinessCollateralAgmLine.BusinessCollateralTypeGUID equals bussinessCollateralType.BusinessCollateralTypeGUID into ljbussinessCollateralType
                        from bussinessCollateralType in ljbussinessCollateralType.DefaultIfEmpty()

                        join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                        on bussinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                        from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()

                        join bankGroup in db.Set<BankGroup>()
                        on bussinessCollateralAgmLine.BankGroupGUID equals bankGroup.BankGroupGUID into ljbankGroup
                        from bankGroup in ljbankGroup.DefaultIfEmpty()

                        join bankType in db.Set<BankType>()
                        on bussinessCollateralAgmLine.BankTypeGUID equals bankType.BankTypeGUID into ljbankType
                        from bankType in ljbankType.DefaultIfEmpty()



                        group new
                        {
                            bussinessCollateralAgmLine.BusinessCollateralSubTypeGUID,
                        }
                        by new
                        {
                            bussinessCollateralAgmLine.BusinessCollateralAgmLineGUID,
                            bussinessCollateralAgmLine.BusinessCollateralAgmTableGUID,
                            bussinessCollateralAgmLine.LineNum,
                            bussinessCollateralAgmLine.BankGroupGUID,
                            bussinessCollateralAgmLine.BankTypeGUID,
                            bussinessCollateralAgmLine.AccountNumber,
                            bankGroup_Description = bankGroup.Description,
                            bankType_Description = bankType.Description,
                            businessCollateralSubType.Description,
                            bussinessCollateralAgmLine.PreferentialCreditorNumber,
                            bussinessCollateralAgmLine.BusinessCollateralValue,
                            bussinessCollateralType.AgreementOrdering,
                            subTypeOrdering = businessCollateralSubType.AgreementOrdering,
                            bussinessCollateralAgmLine.AttachmentText,
                            bussinessCollateralAgmLine.Product
                        } into g
                                                     where g.Key.BusinessCollateralAgmTableGUID == refGUID
                             && g.Key.AgreementOrdering == 1
                             && g.Key.subTypeOrdering == 1
                                                     orderby g.Key.LineNum
                                                     select new QBusinessCollateralAgmLineT1S1
                        {
                            QBusinessCollateralAgmLineT1S1_BankTypeGUID = g.Key.BankTypeGUID != null ? g.Key.BankTypeGUID.ToString() : new Guid().GuidNullOrEmptyToString(),
                            QBusinessCollateralAgmLineT1S1_AccountNumber = g.Key.AccountNumber.ToString(),
                            QBusinessCollateralAgmLineT1S1_BankGroup = g.Key.bankGroup_Description,
                            QBusinessCollateralAgmLineT1S1_BankType = g.Key.bankType_Description,
                            QBusinessCollateralAgmLineT1S1_SubTypeId = g.Key.Description,
                            QBusinessCollateralAgmLineT1S1_AttachmentText = g.Key.AttachmentText,
                                                         QBusinessCollateralAgmLineT1S1_Product = g.Key.Product,
                            QBusinessCollateralAgmLineT1S1_PreferentialCreditorNumber = g.Key.PreferentialCreditorNumber,
                            QBusinessCollateralAgmLineT1S1_Row = g.Key.LineNum
                        }
                        
                                                                                 ).ToList();
                var sumBusinesCollateralValue = (from bussinessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                 join bussinessCollateralType in db.Set<BusinessCollateralType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralTypeGUID equals bussinessCollateralType.BusinessCollateralTypeGUID into ljbussinessCollateralType
                                                 from bussinessCollateralType in ljbussinessCollateralType.DefaultIfEmpty()

                                                 join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
                                                 on bussinessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID into ljbusinessCollateralSubType
                                                 from businessCollateralSubType in ljbusinessCollateralSubType.DefaultIfEmpty()

                                                 where bussinessCollateralAgmLine.BusinessCollateralAgmTableGUID == refGUID
                                                     && bussinessCollateralType.AgreementOrdering == 1
                                                     && businessCollateralSubType.AgreementOrdering == 1
                                                 select bussinessCollateralAgmLine.BusinessCollateralValue
                                                ).Sum();
                int iqBusinessCollateralAgmLineT1S1 = 1;
                foreach (var item in qBusinessCollateralAgmLineT1S1)
                {
                    item.QBusinessCollateralAgmLineT1S1_Row = iqBusinessCollateralAgmLineT1S1;
                    item.QBusinessCollateralAgmLineT1S1_SumBusinessCollateralValue = sumBusinesCollateralValue;
                    item.QBusinessCollateralAgmLineT1S1_ConutLine = qBusinessCollateralAgmLineT1S1.Count();
                    ++iqBusinessCollateralAgmLineT1S1;
                }
                return qBusinessCollateralAgmLineT1S1;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        #region child method

        private void SetVariable(BookmarkBusinessCollateralAgmTable model)
        {
            try
            {
                
                model.Variable_BCLeaseholdsRightDateFirst1 = DateToWordCustom(model.QBusinessCollateralAgmLineT1S2_RefAgreementDate, TextConstants.TH, TextConstants.DMY);
                model.Variable_BCLeaseholdsRightDateSecond1 = DateToWordCustom(model.QBusinessCollateralAgmLineT1S2_RefAgreementDate_Row_2, TextConstants.TH, TextConstants.DMY);
                model.Variable_BCLeaseholdsRightDateThird1 = DateToWordCustom(model.QBusinessCollateralAgmLineT1S2_RefAgreementDate_Row_3, TextConstants.TH, TextConstants.DMY);
                model.Variable_BCLeaseholdsRightDateFourth1 = DateToWordCustom(model.QBusinessCollateralAgmLineT1S2_RefAgreementDate_Row_4, TextConstants.TH, TextConstants.DMY);
                model.Variable_BCLeaseholdsRightDateFifth1 = DateToWordCustom(model.QBusinessCollateralAgmLineT1S2_RefAgreementDate_Row_5, TextConstants.TH, TextConstants.DMY);
                model.Variable_BCLeaseholdsRightDateSixth1 = DateToWordCustom(model.QBusinessCollateralAgmLineT1S2_RefAgreementDate_Row_6, TextConstants.TH, TextConstants.DMY);
                model.Variable_BCLeaseholdsRightDateSeven1 = DateToWordCustom(model.QBusinessCollateralAgmLineT1S2_RefAgreementDate_Row_7, TextConstants.TH, TextConstants.DMY);
                model.Variable_BCLeaseholdsRightDateEighth1 = DateToWordCustom(model.QBusinessCollateralAgmLineT1S2_RefAgreementDate_Row_8, TextConstants.TH, TextConstants.DMY);
                model.Variable_BCLeaseholdsRightDateNineth1 = DateToWordCustom(model.QBusinessCollateralAgmLineT1S2_RefAgreementDate_Row_9, TextConstants.TH, TextConstants.DMY);
                model.Variable_BCLeaseholdsRightDateTenth1 = DateToWordCustom(model.QBusinessCollateralAgmLineT1S2_RefAgreementDate_Row_10, TextConstants.TH, TextConstants.DMY);

                model.Variable_BCRightofClaimDateFirst1 = DateToWordCustom(model.QBusinessCollateralAgmLineT1S3_RefAgreementDate, TextConstants.TH, TextConstants.DMY);
                model.Variable_BCRightofClaimDateSecond1 = DateToWordCustom(model.QBusinessCollateralAgmLineT1S3_RefAgreementDate_Row_2, TextConstants.TH, TextConstants.DMY);
                model.Variable_BCRightofClaimDateThird1 = DateToWordCustom(model.QBusinessCollateralAgmLineT1S3_RefAgreementDate_Row_3, TextConstants.TH, TextConstants.DMY);
                model.Variable_BCRightofClaimDateFourth1 = DateToWordCustom(model.QBusinessCollateralAgmLineT1S3_RefAgreementDate_Row_4, TextConstants.TH, TextConstants.DMY);
                model.Variable_BCRightofClaimDateFifth1 = DateToWordCustom(model.QBusinessCollateralAgmLineT1S3_RefAgreementDate_Row_5, TextConstants.TH, TextConstants.DMY);
                model.Variable_BCRightofClaimDateSixth1 = DateToWordCustom(model.QBusinessCollateralAgmLineT1S3_RefAgreementDate_Row_6, TextConstants.TH, TextConstants.DMY);
                model.Variable_BCRightofClaimDateSeventh1 = DateToWordCustom(model.QBusinessCollateralAgmLineT1S3_RefAgreementDate_Row_7, TextConstants.TH, TextConstants.DMY);
                model.Variable_BCRightofClaimDateEighth1 = DateToWordCustom(model.QBusinessCollateralAgmLineT1S3_RefAgreementDate_Row_8, TextConstants.TH, TextConstants.DMY);
                model.Variable_BCRightofClaimDateNineth1 = DateToWordCustom(model.QBusinessCollateralAgmLineT1S3_RefAgreementDate_Row_9, TextConstants.TH, TextConstants.DMY);
                model.Variable_BCRightofClaimDateTenth1 = DateToWordCustom(model.QBusinessCollateralAgmLineT1S3_RefAgreementDate_Row_10, TextConstants.TH, TextConstants.DMY);
                model.Variable_CollateralDate = DateToWordCustom(model.QBusinessCollateralAgmTable_AgreementDate, TextConstants.TH, TextConstants.DMY);

                model.Variable_BCBuyerTaxIDFirst1 = GetTaxIdOrPassport(model.QBusinessCollateralAgmLineT2S1_IdentificationType_Row_1,model.QBusinessCollateralAgmLineT2S1_TaxId_Row_1, model.QBusinessCollateralAgmLineT2S1_PassportId_Row_1);
                model.Variable_BCBuyerTaxIDSecond1 = GetTaxIdOrPassport(model.QBusinessCollateralAgmLineT2S1_IdentificationType_Row_2, model.QBusinessCollateralAgmLineT2S1_TaxId_Row_2, model.QBusinessCollateralAgmLineT2S1_PassportId_Row_2);
                model.Variable_BCBuyerTaxIDThird1 = GetTaxIdOrPassport(model.QBusinessCollateralAgmLineT2S1_IdentificationType_Row_3, model.QBusinessCollateralAgmLineT2S1_TaxId_Row_3, model.QBusinessCollateralAgmLineT2S1_PassportId_Row_3);
                model.Variable_BCBuyerTaxIDFourth1 = GetTaxIdOrPassport(model.QBusinessCollateralAgmLineT2S1_IdentificationType_Row_4, model.QBusinessCollateralAgmLineT2S1_TaxId_Row_4, model.QBusinessCollateralAgmLineT2S1_PassportId_Row_4);
                model.Variable_BCBuyerTaxIDFifth1 = GetTaxIdOrPassport(model.QBusinessCollateralAgmLineT2S1_IdentificationType_Row_5, model.QBusinessCollateralAgmLineT2S1_TaxId_Row_5, model.QBusinessCollateralAgmLineT2S1_PassportId_Row_5);
                model.Variable_BCBuyerTaxIDSixth1 = GetTaxIdOrPassport(model.QBusinessCollateralAgmLineT2S1_IdentificationType_Row_6, model.QBusinessCollateralAgmLineT2S1_TaxId_Row_6, model.QBusinessCollateralAgmLineT2S1_PassportId_Row_6);
                model.Variable_BCBuyerTaxIDSeventh1 = GetTaxIdOrPassport(model.QBusinessCollateralAgmLineT2S1_IdentificationType_Row_7, model.QBusinessCollateralAgmLineT2S1_TaxId_Row_7, model.QBusinessCollateralAgmLineT2S1_PassportId_Row_7);
                model.Variable_BCBuyerTaxIDEighth1 = GetTaxIdOrPassport(model.QBusinessCollateralAgmLineT2S1_IdentificationType_Row_8, model.QBusinessCollateralAgmLineT2S1_TaxId_Row_8, model.QBusinessCollateralAgmLineT2S1_PassportId_Row_8);
                model.Variable_BCBuyerTaxIDNineth1 = GetTaxIdOrPassport(model.QBusinessCollateralAgmLineT2S1_IdentificationType_Row_9, model.QBusinessCollateralAgmLineT2S1_TaxId_Row_9, model.QBusinessCollateralAgmLineT2S1_PassportId_Row_9);
                model.Variable_BCBuyerTaxIDTenth1 = GetTaxIdOrPassport(model.QBusinessCollateralAgmLineT2S1_IdentificationType_Row_10, model.QBusinessCollateralAgmLineT2S1_TaxId_Row_10, model.QBusinessCollateralAgmLineT2S1_PassportId_Row_10);

                model.Variable_BCTaxIdARFirst1 = GetTaxIdOrPassport(model.QBusinessCollateralAgmLineT2S2_IdentificationType_Row_1, model.QBusinessCollateralAgmLineT2S2_TaxId_Row_1, model.QBusinessCollateralAgmLineT2S2_PassportId_Row_1);
                model.Variable_BCTaxIdARSecond1 = GetTaxIdOrPassport(model.QBusinessCollateralAgmLineT2S2_IdentificationType_Row_2, model.QBusinessCollateralAgmLineT2S2_TaxId_Row_2, model.QBusinessCollateralAgmLineT2S2_PassportId_Row_2);
                model.Variable_BCTaxIdARThird1 = GetTaxIdOrPassport(model.QBusinessCollateralAgmLineT2S2_IdentificationType_Row_3, model.QBusinessCollateralAgmLineT2S2_TaxId_Row_3, model.QBusinessCollateralAgmLineT2S2_PassportId_Row_3);
                model.Variable_BCTaxIdARFourth1 = GetTaxIdOrPassport(model.QBusinessCollateralAgmLineT2S2_IdentificationType_Row_4, model.QBusinessCollateralAgmLineT2S2_TaxId_Row_4, model.QBusinessCollateralAgmLineT2S2_PassportId_Row_4);
                model.Variable_BCTaxIdARFifth1 = GetTaxIdOrPassport(model.QBusinessCollateralAgmLineT2S2_IdentificationType_Row_5, model.QBusinessCollateralAgmLineT2S2_TaxId_Row_5, model.QBusinessCollateralAgmLineT2S2_PassportId_Row_5);
                model.Variable_BCTaxIdARSixth1 = GetTaxIdOrPassport(model.QBusinessCollateralAgmLineT2S2_IdentificationType_Row_6, model.QBusinessCollateralAgmLineT2S2_TaxId_Row_6, model.QBusinessCollateralAgmLineT2S2_PassportId_Row_6);
                model.Variable_BCTaxIdARSeventh1 = GetTaxIdOrPassport(model.QBusinessCollateralAgmLineT2S2_IdentificationType_Row_7, model.QBusinessCollateralAgmLineT2S2_TaxId_Row_7, model.QBusinessCollateralAgmLineT2S2_PassportId_Row_7);
                model.Variable_BCTaxIdAREighth1 = GetTaxIdOrPassport(model.QBusinessCollateralAgmLineT2S2_IdentificationType_Row_8, model.QBusinessCollateralAgmLineT2S2_TaxId_Row_8, model.QBusinessCollateralAgmLineT2S2_PassportId_Row_8);
                model.Variable_BCTaxIdARNineth1 = GetTaxIdOrPassport(model.QBusinessCollateralAgmLineT2S2_IdentificationType_Row_9, model.QBusinessCollateralAgmLineT2S2_TaxId_Row_9, model.QBusinessCollateralAgmLineT2S2_PassportId_Row_9);
                model.Variable_BCTaxIdARTenth1 = GetTaxIdOrPassport(model.QBusinessCollateralAgmLineT2S2_IdentificationType_Row_10, model.QBusinessCollateralAgmLineT2S2_TaxId_Row_10, model.QBusinessCollateralAgmLineT2S2_PassportId_Row_10);

                
      


            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string DateToWordCustom(DateTime? dattime, string lang , string format)
        {
            try
            {
                if (dattime != null)
                {
                    ISharedService sharedService = new SharedService(db);
                    return sharedService.DateToWord((DateTime)dattime, lang, format);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
   
        }
        private string GetTaxIdOrPassport(int identificationType, string taxId, string passportId)
        {
            try
            {
                if (identificationType == 0)
                {
                    return taxId;
                }
                else
                {
                    return passportId;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string GEtcustomerAuthorizedPerson3(QBusinessCollateralAgmTable qBusinessCollateralAgmTable)
        {
            try
            {
                var result = (from relatedPersonTable in db.Set<RelatedPersonTable>()
                        join authorizedPersonTrans in db.Set<AuthorizedPersonTrans>()
                        on relatedPersonTable.RelatedPersonTableGUID equals authorizedPersonTrans.RelatedPersonTableGUID 
                        where authorizedPersonTrans.AuthorizedPersonTransGUID.ToString() == qBusinessCollateralAgmTable.QBusinessCollateralAgmTable_AuthorizedPersonTransCustomer3GUID
                        select relatedPersonTable.Name).FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
            
        }

        private string GEtcustomerAuthorizedPerson2(QBusinessCollateralAgmTable qBusinessCollateralAgmTable)
        {
            try
            {
                return (from relatedPersonTable in db.Set<RelatedPersonTable>()
                        join authorizedPersonTrans in db.Set<AuthorizedPersonTrans>()
                        on relatedPersonTable.RelatedPersonTableGUID equals authorizedPersonTrans.RelatedPersonTableGUID 
                        where authorizedPersonTrans.AuthorizedPersonTransGUID.ToString() == qBusinessCollateralAgmTable.QBusinessCollateralAgmTable_AuthorizedPersonTransCustomer2GUID
                        select relatedPersonTable.Name
                                                ).FirstOrDefault();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
           
        }

        private string GEtcustomerAuthorizedPerson1(QBusinessCollateralAgmTable qBusinessCollateralAgmTable)
        {
            try
            {
                return (from relatedPersonTable in db.Set<RelatedPersonTable>()
                        join authorizedPersonTrans in db.Set<AuthorizedPersonTrans>()
                        on relatedPersonTable.RelatedPersonTableGUID equals authorizedPersonTrans.RelatedPersonTableGUID 
                        where authorizedPersonTrans.AuthorizedPersonTransGUID.ToString() == qBusinessCollateralAgmTable.QBusinessCollateralAgmTable_AuthorizedPersonTransCustomer1GUID
                        select relatedPersonTable.Name
                                                 ).FirstOrDefault();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

           
        }

        private QCustomerTableBusinessCollateralAgreement GetQCustomerTableBusinessCollateralAgreement(QBusinessCollateralAgmTable qBusinessCollateralAgmTable)
        {
            try
            {
                return (from customerTable in db.Set<CustomerTable>()
                                                             where customerTable.CustomerTableGUID == qBusinessCollateralAgmTable.QBusinessCollateralAgmTable_CustomerTableGUID
                                                             select new QCustomerTableBusinessCollateralAgreement
                                                             {
                                                                 QCustomerTableBusinessCollateralAgreement_Name = customerTable.Name,
                                                                 QCustomerTableBusinessCollateralAgreement_TaxID = customerTable.TaxID
                                                             }
                                             ).FirstOrDefault();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }

        private QBusinessCollateralAgmTable GetQBusinessCollateralAgmTable( Guid refGUID)
        {
            try
            {
                return  (from businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
                                               join agreementTableInfo in db.Set<AgreementTableInfo>()
                                               on businessCollateralAgmTable.BusinessCollateralAgmTableGUID equals agreementTableInfo.RefGUID into ljagreementTableInfo
                                               from agreementTableInfo in ljagreementTableInfo.DefaultIfEmpty()

                                               join companyWitness1 in db.Set<EmployeeTable>()
                                               on agreementTableInfo.WitnessCompany1GUID equals companyWitness1.EmployeeTableGUID into ljcompanyWitness1
                                               from companyWitness1 in ljcompanyWitness1.DefaultIfEmpty()

                                               join companyWitness2 in db.Set<EmployeeTable>()
                                                on agreementTableInfo.WitnessCompany2GUID equals companyWitness2.EmployeeTableGUID into ljcompanyWitness2
                                               from companyWitness2 in ljcompanyWitness2.DefaultIfEmpty()

                                               join authorizedPersonCompany1 in db.Set<EmployeeTable>()
                                                on agreementTableInfo.AuthorizedPersonTransCompany1GUID equals authorizedPersonCompany1.EmployeeTableGUID into ljauthorizedPersonCompany1
                                               from authorizedPersonCompany1 in ljauthorizedPersonCompany1.DefaultIfEmpty()

                                               join authorizedPersonCompany2 in db.Set<EmployeeTable>()
                                               on agreementTableInfo.AuthorizedPersonTransCompany2GUID equals authorizedPersonCompany2.EmployeeTableGUID into ljauthorizedPersonCompany2
                                               from authorizedPersonCompany2 in ljauthorizedPersonCompany2.DefaultIfEmpty()

                                               join authorizedPersonCompany3 in db.Set<EmployeeTable>()
                                               on agreementTableInfo.AuthorizedPersonTransCompany3GUID equals authorizedPersonCompany3.EmployeeTableGUID into ljauthorizedPersonCompany3
                                               from authorizedPersonCompany3 in ljauthorizedPersonCompany3.DefaultIfEmpty()

                                               join businessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                               on businessCollateralAgmTable.BusinessCollateralAgmTableGUID equals businessCollateralAgmLine.BusinessCollateralAgmTableGUID into ljbusinessCollateralAgmLine
                                               from businessCollateralAgmLine in ljbusinessCollateralAgmLine.DefaultIfEmpty()

                                               where businessCollateralAgmTable.BusinessCollateralAgmTableGUID == refGUID && agreementTableInfo.RefType == (int)RefType.BusinessCollateralAgreement
                                               group new
                                               {
                                                   businessCollateralAgmTable,
                                                   agreementTableInfo,
                                                   companyWitness1,
                                                   companyWitness2,
                                                   authorizedPersonCompany1,
                                                   authorizedPersonCompany2,
                                                   authorizedPersonCompany3,
                                                   businessCollateralAgmLine
                                               }
                                               by new
                                               {
                                                   businessCollateralAgmTable.BusinessCollateralAgmId,
                                                   businessCollateralAgmTable.AgreementDate,
                                                   companyWitness1_name = companyWitness1.Name,
                                                   companyWitness2_name = companyWitness2.Name,
                                                   agreementTableInfo.CustomerAddress1,
                                                   agreementTableInfo.CustomerAddress2,
                                                   authorizedPersonCompany1_name = authorizedPersonCompany1.Name,
                                                   authorizedPersonCompany2_name = authorizedPersonCompany2.Name,
                                                   authorizedPersonCompany3_name = authorizedPersonCompany3.Name,
                                                   agreementTableInfo.AuthorizedPersonTransCustomer1GUID,
                                                   agreementTableInfo.AuthorizedPersonTransCustomer2GUID,
                                                   agreementTableInfo.AuthorizedPersonTransCustomer3GUID,
                                                   businessCollateralAgmTable.DBDRegistrationId,
                                                   businessCollateralAgmTable.DBDRegistrationDescription,
                                                   businessCollateralAgmTable.DBDRegistrationAmount,
                                                   businessCollateralAgmTable.AttachmentRemark,
                                                   businessCollateralAgmTable.CompanyGUID,
                                                   businessCollateralAgmTable.CustomerTableGUID
                                               }
                                                                             into g
                                               select new QBusinessCollateralAgmTable
                                               {
                                                   QBusinessCollateralAgmTable_BusinessCollateralAgmId = g.Key.BusinessCollateralAgmId,
                                                   QBusinessCollateralAgmTable_AgreementDate = g.Key.AgreementDate,
                                                   QBusinessCollateralAgmTable_CustomerAddress = g.Key.CustomerAddress1 + " " + g.Key.CustomerAddress2,
                                                   QBusinessCollateralAgmTable_CompanyWitness1 = g.Key.companyWitness1_name,
                                                   QBusinessCollateralAgmTable_CompanyWitness2 = g.Key.companyWitness2_name,
                                                   QBusinessCollateralAgmTable_CompanyAuthorizedPerson1 = g.Key.authorizedPersonCompany1_name,
                                                   QBusinessCollateralAgmTable_CompanyAuthorizedPerson2 = g.Key.authorizedPersonCompany2_name,
                                                   QBusinessCollateralAgmTable_CompanyAuthorizedPerson3 = g.Key.authorizedPersonCompany3_name,
                                                   QBusinessCollateralAgmTable_AuthorizedPersonTransCustomer1GUID =  g.Key.AuthorizedPersonTransCustomer1GUID.GuidNullOrEmptyToString(),
                                                   QBusinessCollateralAgmTable_AuthorizedPersonTransCustomer2GUID = g.Key.AuthorizedPersonTransCustomer2GUID.GuidNullOrEmptyToString(),
                                                   QBusinessCollateralAgmTable_AuthorizedPersonTransCustomer3GUID =  g.Key.AuthorizedPersonTransCustomer3GUID.GuidNullOrEmptyToString(),
                                                   QBusinessCollateralAgmTable_SumBusinessCollateralValue = g.Sum(su => su.businessCollateralAgmLine.BusinessCollateralValue),
                                                   QBusinessCollateralAgmTable_DBDRegistrationId = g.Key.DBDRegistrationId,
                                                   QBusinessCollateralAgmTable_DBDRegistrationDescription = g.Key.DBDRegistrationDescription,
                                                   QBusinessCollateralAgmTable_DBDRegistrationAmount = g.Key.DBDRegistrationAmount,
                                                   QBusinessCollateralAgmTable_AttachmentText = g.Key.AttachmentRemark,
                                                   QBusinessCollateralAgmTable_CompanyGUID = g.Key.CompanyGUID.ToString(),
                                                   QBusinessCollateralAgmTable_CustomerTableGUID = g.Key.CustomerTableGUID
                                               }).FirstOrDefault();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
            
        }

        private void GetQCustomerTableBusinessCollateralAgreement(BookmarkBusinessCollateralAgmTable model, QCustomerTableBusinessCollateralAgreement qCustomerTableBusinessCollateralAgreement)
        {
            try
            {
                if (qCustomerTableBusinessCollateralAgreement != null)
                {
                    model.QCustomerTableBusinessCollateralAgreement_Name = qCustomerTableBusinessCollateralAgreement.QCustomerTableBusinessCollateralAgreement_Name ;
                    model.QCustomerTableBusinessCollateralAgreement_TaxID = qCustomerTableBusinessCollateralAgreement.QCustomerTableBusinessCollateralAgreement_TaxID ;
                }
                else
                {
                    model.QCustomerTableBusinessCollateralAgreement_Name = "";
                    model.QCustomerTableBusinessCollateralAgreement_TaxID = "";
                }



            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }

        private void GetGeneralCompany(BookmarkBusinessCollateralAgmTable model, QCompanyBusinessCollateralAgreement qCompanyBusinessCollateralAgreement)
        {
            try
            {
                if (qCompanyBusinessCollateralAgreement != null)
                {
                    model.QCompanyBusinessCollateralAgreement_Company_Name = qCompanyBusinessCollateralAgreement.QCompanyBusinessCollateralAgreement_Company_Name ;
                    model.QCompanyBusinessCollateralAgreement_Company_Address = qCompanyBusinessCollateralAgreement.QCompanyBusinessCollateralAgreement_Company_Address;
                }
                else
                {
                    model.QCompanyBusinessCollateralAgreement_Company_Name = "";
                    model.QCompanyBusinessCollateralAgreement_Company_Address ="";
                }



            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }

        private void GetGeneralBusinesCalateral(BookmarkBusinessCollateralAgmTable model, QBusinessCollateralAgmTable qBusinessCollateralAgmTable)
        {
            try
            {
                if (model != null)
                {
                    model.QBusinessCollateralAgmTable_BusinessCollateralAgmId = qBusinessCollateralAgmTable.QBusinessCollateralAgmTable_BusinessCollateralAgmId;
                    model.QBusinessCollateralAgmTable_AgreementDate = qBusinessCollateralAgmTable.QBusinessCollateralAgmTable_AgreementDate;
                    model.QBusinessCollateralAgmTable_CustomerAddress = qBusinessCollateralAgmTable.QBusinessCollateralAgmTable_CustomerAddress;
                    model.QBusinessCollateralAgmTable_CompanyWitness1 = qBusinessCollateralAgmTable.QBusinessCollateralAgmTable_CompanyWitness1;
                    model.QBusinessCollateralAgmTable_CompanyWitness2 = qBusinessCollateralAgmTable.QBusinessCollateralAgmTable_CompanyWitness2;
                    model.QBusinessCollateralAgmTable_CompanyAuthorizedPerson1 = qBusinessCollateralAgmTable.QBusinessCollateralAgmTable_CompanyAuthorizedPerson1;
                    model.QBusinessCollateralAgmTable_CompanyAuthorizedPerson2 = qBusinessCollateralAgmTable.QBusinessCollateralAgmTable_CompanyAuthorizedPerson2;
                    model.QBusinessCollateralAgmTable_CompanyAuthorizedPerson3 = qBusinessCollateralAgmTable.QBusinessCollateralAgmTable_CompanyAuthorizedPerson3;
                    model.QBusinessCollateralAgmTable_AuthorizedPersonTransCustomer1GUID = qBusinessCollateralAgmTable.QBusinessCollateralAgmTable_AuthorizedPersonTransCustomer1GUID;
                    model.QBusinessCollateralAgmTable_AuthorizedPersonTransCustomer2GUID = qBusinessCollateralAgmTable.QBusinessCollateralAgmTable_AuthorizedPersonTransCustomer2GUID;
                    model.QBusinessCollateralAgmTable_AuthorizedPersonTransCustomer3GUID = qBusinessCollateralAgmTable.QBusinessCollateralAgmTable_AuthorizedPersonTransCustomer3GUID;
                    model.QBusinessCollateralAgmTable_SumBusinessCollateralValue = qBusinessCollateralAgmTable.QBusinessCollateralAgmTable_SumBusinessCollateralValue;
                    model.QBusinessCollateralAgmTable_DBDRegistrationId = qBusinessCollateralAgmTable.QBusinessCollateralAgmTable_DBDRegistrationId;
                    model.QBusinessCollateralAgmTable_DBDRegistrationDescription = qBusinessCollateralAgmTable.QBusinessCollateralAgmTable_DBDRegistrationDescription;
                    model.QBusinessCollateralAgmTable_DBDRegistrationAmount = qBusinessCollateralAgmTable.QBusinessCollateralAgmTable_DBDRegistrationAmount;
                }


            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }


        }

        private void GetGeneralBusinesCalateralAgmLineT1S1(BookmarkBusinessCollateralAgmTable model, List<QBusinessCollateralAgmLineT1S1> qBusinessCollateralAgmLineT1S1)

        {
            try
            {
                var result = qBusinessCollateralAgmLineT1S1.FirstOrDefault();
                if (result != null)
                {
                    model.QBusinessCollateralAgmLineT1S1_SubTypeId = result.QBusinessCollateralAgmLineT1S1_SubTypeId;
                    model.QBusinessCollateralAgmLineT1S1_SumBusinessCollateralValue = result.QBusinessCollateralAgmLineT1S1_SumBusinessCollateralValue;
                    model.QBusinessCollateralAgmLineT1S1_AttachmentText = result.QBusinessCollateralAgmLineT1S1_AttachmentText;
                    model.QBusinessCollateralAgmLineT1S1_ConutLine = result.QBusinessCollateralAgmLineT1S1_ConutLine;
                    model.QBusinessCollateralAgmLineT1S1_PreferentialCreditorNumber = result.QBusinessCollateralAgmLineT1S1_PreferentialCreditorNumber;
                    model.QBusinessCollateralAgmLineT1S1_Row = result.QBusinessCollateralAgmLineT1S1_Row;
                }



            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }
        private void GetGeneralBusinesCalateralAgmLineT1S2(BookmarkBusinessCollateralAgmTable model, List<QBusinessCollateralAgmLineT1S2> qBusinessCollateralAgmLineT1S2)

        {
            try
            {
                var result = qBusinessCollateralAgmLineT1S2.FirstOrDefault();
                if (result != null)
                {
                    model.QBusinessCollateralAgmLineT1S2_SubTypeId = result.QBusinessCollateralAgmLineT1S2_SubTypeId;
                    model.QBusinessCollateralAgmLineT1S2_SumBusinessCollateralValue = result.QBusinessCollateralAgmLineT1S2_SumBusinessCollateralValue;
                    model.QBusinessCollateralAgmLineT1S2_AttachmentText = result.QBusinessCollateralAgmLineT1S2_AttachmentText;
                    model.QBusinessCollateralAgmLineT1S2_ConutLine = result.QBusinessCollateralAgmLineT1S2_ConutLine;
                    model.QBusinessCollateralAgmLineT1S2_PreferentialCreditorNumber = result.QBusinessCollateralAgmLineT1S2_PreferentialCreditorNumber;
                    model.QBusinessCollateralAgmLineT1S2_Row = result.QBusinessCollateralAgmLineT1S2_Row;
                }



            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }
        private void GetGeneralBusinesCalateralAgmLineT1S3(BookmarkBusinessCollateralAgmTable model, List<QBusinessCollateralAgmLineT1S3> qBusinessCollateralAgmLineT1S3)

        {
            try
            {
                var result = qBusinessCollateralAgmLineT1S3.FirstOrDefault();
                if (result != null)
                {
                    model.QBusinessCollateralAgmLineT1S3_SubTypeId = result.QBusinessCollateralAgmLineT1S3_SubTypeId;
                    model.QBusinessCollateralAgmLineT1S3_SumBusinessCollateralValue = result.QBusinessCollateralAgmLineT1S3_SumBusinessCollateralValue;
                    model.QBusinessCollateralAgmLineT1S3_AttachmentText = result.QBusinessCollateralAgmLineT1S3_AttachmentText;
                    model.QBusinessCollateralAgmLineT1S3_ConutLine = result.QBusinessCollateralAgmLineT1S3_ConutLine;
                    model.QBusinessCollateralAgmLineT1S3_PreferentialCreditorNumber = result.QBusinessCollateralAgmLineT1S3_PreferentialCreditorNumber;
                    model.QBusinessCollateralAgmLineT1S3_Row = result.QBusinessCollateralAgmLineT1S3_Row;
                }



            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }
        private void GetGeneralBusinesCalateralAgmLineT2S1(BookmarkBusinessCollateralAgmTable model, List<QBusinessCollateralAgmLineT2S1> qBusinessCollateralAgmLineT2S1)

        {
            try
            {
                var result = qBusinessCollateralAgmLineT2S1.FirstOrDefault();
                if (result != null)
                {
                    model.QBusinessCollateralAgmLineT2S1_SubTypeId = result.QBusinessCollateralAgmLineT2S1_SubTypeId;
                    model.QBusinessCollateralAgmLineT2S1_SumBusinessCollateralValue = result.QBusinessCollateralAgmLineT2S1_SumBusinessCollateralValue;
                    model.QBusinessCollateralAgmLineT2S1_AttachmentText = result.QBusinessCollateralAgmLineT2S1_AttachmentText;
                    model.QBusinessCollateralAgmLineT2S1_ConutLine = result.QBusinessCollateralAgmLineT2S1_ConutLine;
                    model.QBusinessCollateralAgmLineT2S1_PreferentialCreditorNumber = result.QBusinessCollateralAgmLineT2S1_PreferentialCreditorNumber;
                    model.QBusinessCollateralAgmLineT2S1_Row = result.QBusinessCollateralAgmLineT2S1_Row;
                }



            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }
        private void GetGeneralBusinesCalateralAgmLineT2S2(BookmarkBusinessCollateralAgmTable model, List<QBusinessCollateralAgmLineT2S2> qBusinessCollateralAgmLineT2S2)

        {
            try
            {
                var result = qBusinessCollateralAgmLineT2S2.FirstOrDefault();
                if (result != null)
                {
                    model.QBusinessCollateralAgmLineT2S2_SubTypeId = result.QBusinessCollateralAgmLineT2S2_SubTypeId;
                    model.QBusinessCollateralAgmLineT2S2_SumBusinessCollateralValue = result.QBusinessCollateralAgmLineT2S2_SumBusinessCollateralValue;
                    model.QBusinessCollateralAgmLineT2S2_AttachmentText = result.QBusinessCollateralAgmLineT2S2_AttachmentText;
                    model.QBusinessCollateralAgmLineT2S2_ConutLine = result.QBusinessCollateralAgmLineT2S2_ConutLine;
                    model.QBusinessCollateralAgmLineT2S2_PreferentialCreditorNumber = result.QBusinessCollateralAgmLineT2S2_PreferentialCreditorNumber;
                    model.QBusinessCollateralAgmLineT2S2_Row = result.QBusinessCollateralAgmLineT2S2_Row;
                }



            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }
        private void GetGeneralBusinesCalateralAgmLineT3S1(BookmarkBusinessCollateralAgmTable model, List<QBusinessCollateralAgmLineT3S1> qBusinessCollateralAgmLineT3S1)

        {
            try
            {
                var result = qBusinessCollateralAgmLineT3S1.FirstOrDefault();
                if (result != null)
                {
                    model.QBusinessCollateralAgmLineT3S1_SubTypeId = result.QBusinessCollateralAgmLineT3S1_SubTypeId;
                    model.QBusinessCollateralAgmLineT3S1_SumBusinessCollateralValue = result.QBusinessCollateralAgmLineT3S1_SumBusinessCollateralValue;
                    model.QBusinessCollateralAgmLineT3S1_AttachmentText = result.QBusinessCollateralAgmLineT3S1_AttachmentText;
                    model.QBusinessCollateralAgmLineT3S1_ConutLine = result.QBusinessCollateralAgmLineT3S1_ConutLine;
                    model.QBusinessCollateralAgmLineT3S1_PreferentialCreditorNumber = result.QBusinessCollateralAgmLineT3S1_PreferentialCreditorNumber;
                    model.QBusinessCollateralAgmLineT3S1_Row = result.QBusinessCollateralAgmLineT3S1_Row;
                }



            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }
        private void GetGeneralBusinesCalateralAgmLineT3S2(BookmarkBusinessCollateralAgmTable model, List<QBusinessCollateralAgmLineT3S2> qBusinessCollateralAgmLineT3S2)

        {
            try
            {
                var result = qBusinessCollateralAgmLineT3S2.FirstOrDefault();
                if (result != null)
                {
                    model.QBusinessCollateralAgmLineT3S2_SubTypeId = result.QBusinessCollateralAgmLineT3S2_SubTypeId;
                    model.QBusinessCollateralAgmLineT3S2_SumBusinessCollateralValue = result.QBusinessCollateralAgmLineT3S2_SumBusinessCollateralValue;
                    model.QBusinessCollateralAgmLineT3S2_AttachmentText = result.QBusinessCollateralAgmLineT3S2_AttachmentText;
                    model.QBusinessCollateralAgmLineT3S2_ConutLine = result.QBusinessCollateralAgmLineT3S2_ConutLine;
                    model.QBusinessCollateralAgmLineT3S2_PreferentialCreditorNumber = result.QBusinessCollateralAgmLineT3S2_PreferentialCreditorNumber;
                    model.QBusinessCollateralAgmLineT3S2_Row = result.QBusinessCollateralAgmLineT3S2_Row;
                }



            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }
        private void GetGeneralBusinesCalateralAgmLineT3S3(BookmarkBusinessCollateralAgmTable model, List<QBusinessCollateralAgmLineT3S3> qBusinessCollateralAgmLineT3S3)

        {
            try
            {
                var result = qBusinessCollateralAgmLineT3S3.FirstOrDefault();
                if (result != null)
                {
                    model.QBusinessCollateralAgmLineT3S3_SubTypeId = result.QBusinessCollateralAgmLineT3S3_SubTypeId;
                    model.QBusinessCollateralAgmLineT3S3_SumBusinessCollateralValue = result.QBusinessCollateralAgmLineT3S3_SumBusinessCollateralValue;
                    model.QBusinessCollateralAgmLineT3S3_AttachmentText = result.QBusinessCollateralAgmLineT3S3_AttachmentText;
                    model.QBusinessCollateralAgmLineT3S3_ConutLine = result.QBusinessCollateralAgmLineT3S3_ConutLine;
                    model.QBusinessCollateralAgmLineT3S3_PreferentialCreditorNumber = result.QBusinessCollateralAgmLineT3S3_PreferentialCreditorNumber;
                    model.QBusinessCollateralAgmLineT3S3_Row = result.QBusinessCollateralAgmLineT3S3_Row;
                }



            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }
        private void GetGeneralBusinesCalateralAgmLineT3S4(BookmarkBusinessCollateralAgmTable model, List<QBusinessCollateralAgmLineT3S4> qBusinessCollateralAgmLineT3S4)

        {
            try
            {
                var result = qBusinessCollateralAgmLineT3S4.FirstOrDefault();
                if (result != null)
                {
                    model.QBusinessCollateralAgmLineT3S4_SubTypeId = result.QBusinessCollateralAgmLineT3S4_SubTypeId;
                    model.QBusinessCollateralAgmLineT3S4_SumBusinessCollateralValue = result.QBusinessCollateralAgmLineT3S4_SumBusinessCollateralValue;
                    model.QBusinessCollateralAgmLineT3S4_AttachmentText = result.QBusinessCollateralAgmLineT3S4_AttachmentText;
                    model.QBusinessCollateralAgmLineT3S4_ConutLine = result.QBusinessCollateralAgmLineT3S4_ConutLine;
                    model.QBusinessCollateralAgmLineT3S4_PreferentialCreditorNumber = result.QBusinessCollateralAgmLineT3S4_PreferentialCreditorNumber;
                    model.QBusinessCollateralAgmLineT3S4_Row = result.QBusinessCollateralAgmLineT3S4_Row;
                }



            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }
        private void GetGeneralBusinesCalateralAgmLineT3S5(BookmarkBusinessCollateralAgmTable model, List<QBusinessCollateralAgmLineT3S5> qBusinessCollateralAgmLineT3S5)

        {
            try
            {
                var result = qBusinessCollateralAgmLineT3S5.FirstOrDefault();
                if (result != null)
                {
                    model.QBusinessCollateralAgmLineT3S5_SubTypeId = result.QBusinessCollateralAgmLineT3S5_SubTypeId;
                    model.QBusinessCollateralAgmLineT3S5_SumBusinessCollateralValue = result.QBusinessCollateralAgmLineT3S5_SumBusinessCollateralValue;
                    model.QBusinessCollateralAgmLineT3S5_AttachmentText = result.QBusinessCollateralAgmLineT3S5_AttachmentText;
                    model.QBusinessCollateralAgmLineT3S5_ConutLine = result.QBusinessCollateralAgmLineT3S5_ConutLine;
                    model.QBusinessCollateralAgmLineT3S5_PreferentialCreditorNumber = result.QBusinessCollateralAgmLineT3S5_PreferentialCreditorNumber;
                    model.QBusinessCollateralAgmLineT3S5_Row = result.QBusinessCollateralAgmLineT3S5_Row;
                }



            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }
        private void GetGeneralBusinesCalateralAgmLineT4S1(BookmarkBusinessCollateralAgmTable model, List<QBusinessCollateralAgmLineT4S1> qBusinessCollateralAgmLineT4S1)

        {
            try
            {
                var result = qBusinessCollateralAgmLineT4S1.FirstOrDefault();
                if (result != null)
                {
                    model.QBusinessCollateralAgmLineT4S1_SubTypeId = result.QBusinessCollateralAgmLineT4S1_SubTypeId;
                    model.QBusinessCollateralAgmLineT4S1_SumBusinessCollateralValue = result.QBusinessCollateralAgmLineT4S1_SumBusinessCollateralValue;
                    model.QBusinessCollateralAgmLineT4S1_AttachmentText = result.QBusinessCollateralAgmLineT4S1_AttachmentText;
                    model.QBusinessCollateralAgmLineT4S1_ConutLine = result.QBusinessCollateralAgmLineT4S1_ConutLine;
                    model.QBusinessCollateralAgmLineT4S1_PreferentialCreditorNumber = result.QBusinessCollateralAgmLineT4S1_PreferentialCreditorNumber;
                    model.QBusinessCollateralAgmLineT4S1_Row = result.QBusinessCollateralAgmLineT4S1_Row;
                }



            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }
        private void GetGeneralBusinesCalateralAgmLineT4S2(BookmarkBusinessCollateralAgmTable model, List<QBusinessCollateralAgmLineT4S2> qBusinessCollateralAgmLineT4S2)

        {
            try
            {
                var result = qBusinessCollateralAgmLineT4S2.FirstOrDefault();
                if (result != null)
                {
                    model.QBusinessCollateralAgmLineT4S2_SubTypeId = result.QBusinessCollateralAgmLineT4S2_SubTypeId;
                    model.QBusinessCollateralAgmLineT4S2_SumBusinessCollateralValue = result.QBusinessCollateralAgmLineT4S2_SumBusinessCollateralValue;
                    model.QBusinessCollateralAgmLineT4S2_AttachmentText = result.QBusinessCollateralAgmLineT4S2_AttachmentText;
                    model.QBusinessCollateralAgmLineT4S2_ConutLine = result.QBusinessCollateralAgmLineT4S2_ConutLine;
                    model.QBusinessCollateralAgmLineT4S2_PreferentialCreditorNumber = result.QBusinessCollateralAgmLineT4S2_PreferentialCreditorNumber;
                    model.QBusinessCollateralAgmLineT4S2_Row = result.QBusinessCollateralAgmLineT4S2_Row;

                }



            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }
        private void GetGeneralBusinesCalateralAgmLineT4S3(BookmarkBusinessCollateralAgmTable model, List<QBusinessCollateralAgmLineT4S3> qBusinessCollateralAgmLineT4S3)

        {
            try
            {
                var result = qBusinessCollateralAgmLineT4S3.FirstOrDefault();
                if (result != null)
                {
                    model.QBusinessCollateralAgmLineT4S3_SubTypeId = result.QBusinessCollateralAgmLineT4S3_SubTypeId;
                    model.QBusinessCollateralAgmLineT4S3_SumBusinessCollateralValue = result.QBusinessCollateralAgmLineT4S3_SumBusinessCollateralValue;
                    model.QBusinessCollateralAgmLineT4S3_AttachmentText = result.QBusinessCollateralAgmLineT4S3_AttachmentText;
                    model.QBusinessCollateralAgmLineT4S3_ConutLine = result.QBusinessCollateralAgmLineT4S3_ConutLine;
                    model.QBusinessCollateralAgmLineT4S3_PreferentialCreditorNumber = result.QBusinessCollateralAgmLineT4S3_PreferentialCreditorNumber;
                    model.QBusinessCollateralAgmLineT4S3_Row = result.QBusinessCollateralAgmLineT4S3_Row;
                }



            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }
        private void GetGeneralBusinesCalateralAgmLineT5S1(BookmarkBusinessCollateralAgmTable model, List<QBusinessCollateralAgmLineT5S1> qBusinessCollateralAgmLineT5S1)

        {
            try
            {
                var result = qBusinessCollateralAgmLineT5S1.FirstOrDefault();
                if (result != null)
                {
                    model.QBusinessCollateralAgmLineT5S1_SubTypeId = result.QBusinessCollateralAgmLineT5S1_SubTypeId;
                    model.QBusinessCollateralAgmLineT5S1_SumBusinessCollateralValue = result.QBusinessCollateralAgmLineT5S1_SumBusinessCollateralValue;
                    model.QBusinessCollateralAgmLineT5S1_AttachmentText = result.QBusinessCollateralAgmLineT5S1_AttachmentText;
                    model.QBusinessCollateralAgmLineT5S1_ConutLine = result.QBusinessCollateralAgmLineT5S1_ConutLine;
                    model.QBusinessCollateralAgmLineT5S1_PreferentialCreditorNumber = result.QBusinessCollateralAgmLineT5S1_PreferentialCreditorNumber;
                }



            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }
        private void GetGeneralBusinesCalateralAgmLineT5S2(BookmarkBusinessCollateralAgmTable model, List<QBusinessCollateralAgmLineT5S2> qBusinessCollateralAgmLineT5S2)

        {
            try
            {
                var result = qBusinessCollateralAgmLineT5S2.FirstOrDefault();
                if (result != null)
                {
                    model.QBusinessCollateralAgmLineT5S2_SubTypeId = result.QBusinessCollateralAgmLineT5S2_SubTypeId;
                    model.QBusinessCollateralAgmLineT5S2_SumBusinessCollateralValue = result.QBusinessCollateralAgmLineT5S2_SumBusinessCollateralValue;
                    model.QBusinessCollateralAgmLineT5S2_AttachmentText = result.QBusinessCollateralAgmLineT5S2_AttachmentText;
                    model.QBusinessCollateralAgmLineT5S2_ConutLine = result.QBusinessCollateralAgmLineT5S2_ConutLine;
                    model.QBusinessCollateralAgmLineT5S2_PreferentialCreditorNumber = result.QBusinessCollateralAgmLineT5S2_PreferentialCreditorNumber;
                    model.QBusinessCollateralAgmLineT5S2_Row = result.QBusinessCollateralAgmLineT5S2_Row;
                }



            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }
        
        private void GetGeneralBusinesCalateralAgmLineT5S4(BookmarkBusinessCollateralAgmTable model, List<QBusinessCollateralAgmLineT5S4> qBusinessCollateralAgmLineT5S4)

        {
            try
            {
                var result = qBusinessCollateralAgmLineT5S4.FirstOrDefault();
                if (result != null)
                {
                    model.QBusinessCollateralAgmLineT5S4_AttachmentText = result.QBusinessCollateralAgmLineT5S4_AttachmentText;
                    model.QBusinessCollateralAgmLineT5S4_Product = result.QBusinessCollateralAgmLineT5S4_Product;
                    model.QBusinessCollateralAgmLineT5S4_ConutLine = result.QBusinessCollateralAgmLineT5S4_ConutLine;
                    model.QBusinessCollateralAgmLineT5S4_PreferentialCreditorNumber = result.QBusinessCollateralAgmLineT5S4_PreferentialCreditorNumber;
                }



            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }
        private void GetGeneralBusinesCalateralAgmLineT5S3(BookmarkBusinessCollateralAgmTable model, List<QBusinessCollateralAgmLineT5S3> qBusinessCollateralAgmLineT5S3)

        {
            try
            {
                var result = qBusinessCollateralAgmLineT5S3.FirstOrDefault();
                if (result != null)
                {
                    model.QBusinessCollateralAgmLineT5S3_SubTypeId = result.QBusinessCollateralAgmLineT5S3_SubTypeId;
                    model.QBusinessCollateralAgmLineT5S3_SumBusinessCollateralValue = result.QBusinessCollateralAgmLineT5S3_SumBusinessCollateralValue;
                    model.QBusinessCollateralAgmLineT5S3_AttachmentText = result.QBusinessCollateralAgmLineT5S3_AttachmentText;
                    model.QBusinessCollateralAgmLineT5S3_ConutLine = result.QBusinessCollateralAgmLineT5S3_ConutLine;
                    model.QBusinessCollateralAgmLineT5S3_PreferentialCreditorNumber = result.QBusinessCollateralAgmLineT5S3_PreferentialCreditorNumber;
                    model.QBusinessCollateralAgmLineT5S3_Row = result.QBusinessCollateralAgmLineT5S3_Row;
                }

    

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }
        
            private string GetQBusinessCollateralAgmLineT4S2_Unit(List<QBusinessCollateralAgmLineT4S2> qBusinessCollateralAgmLineT4S2, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT4S2.Where(wh => wh.QBusinessCollateralAgmLineT4S2_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT4S2_Unit;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private string GetQBusinessCollateralAgmLineT4S1_Unit(List<QBusinessCollateralAgmLineT4S1> qBusinessCollateralAgmLineT4S1, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT4S1.Where(wh => wh.QBusinessCollateralAgmLineT4S1_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT4S1_Unit;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private string GetQBusinessCollateralAgmLineT4S1_TitleDeedProvince(List<QBusinessCollateralAgmLineT4S1> qBusinessCollateralAgmLineT4S1, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT4S1.Where(wh => wh.QBusinessCollateralAgmLineT4S1_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT4S1_TitleDeedProvince;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string GetQBusinessCollateralAgmLineT4S1_TitleDeedDistrict(List<QBusinessCollateralAgmLineT4S1> qBusinessCollateralAgmLineT4S1, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT4S1.Where(wh => wh.QBusinessCollateralAgmLineT4S1_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT4S1_TitleDeedDistrict;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string GetQBusinessCollateralAgmLineT4S1_TitleDeedSubDistrict(List<QBusinessCollateralAgmLineT4S1> qBusinessCollateralAgmLineT4S1, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT4S1.Where(wh => wh.QBusinessCollateralAgmLineT4S1_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT4S1_TitleDeedSubDistrict;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string GetQBusinessCollateralAgmLineT4S1_TitleDeedNumber(List<QBusinessCollateralAgmLineT4S1> qBusinessCollateralAgmLineT4S1, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT4S1.Where(wh => wh.QBusinessCollateralAgmLineT4S1_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT4S1_TitleDeedNumber;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string GetQBusinessCollateralAgmLineT4S1_ProjectName(List<QBusinessCollateralAgmLineT4S1> qBusinessCollateralAgmLineT4S1, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT4S1.Where(wh => wh.QBusinessCollateralAgmLineT4S1_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT4S1_ProjectName;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private decimal GetQBusinessCollateralAgmLineT3S5_BusinessCollateralValue(List<QBusinessCollateralAgmLineT3S5> qBusinessCollateralAgmLineT3S5, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT3S5.Where(wh => wh.QBusinessCollateralAgmLineT3S5_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT3S5_BusinessCollateralValue;
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private decimal GetQBusinessCollateralAgmLineT3S5_Quantity(List<QBusinessCollateralAgmLineT3S5> qBusinessCollateralAgmLineT3S5, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT3S5.Where(wh => wh.QBusinessCollateralAgmLineT3S5_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT3S5_Quantity;
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string GetQBusinessCollateralAgmLineT3S5_Product(List<QBusinessCollateralAgmLineT3S5> qBusinessCollateralAgmLineT3S5, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT3S5.Where(wh => wh.QBusinessCollateralAgmLineT3S5_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT3S5_Product;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string GetQBusinessCollateralAgmLineT3S4_Unit(List<QBusinessCollateralAgmLineT3S4> qBusinessCollateralAgmLineT3S4, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT3S4.Where(wh => wh.QBusinessCollateralAgmLineT3S4_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT3S4_Unit;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private decimal GetQBusinessCollateralAgmLineT3S4_BusinessCollateralValue(List<QBusinessCollateralAgmLineT3S4> qBusinessCollateralAgmLineT3S4, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT3S4.Where(wh => wh.QBusinessCollateralAgmLineT3S4_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT3S4_BusinessCollateralValue;
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private decimal GetQBusinessCollateralAgmLineT3S4_Quantity(List<QBusinessCollateralAgmLineT3S4> qBusinessCollateralAgmLineT3S4, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT3S4.Where(wh => wh.QBusinessCollateralAgmLineT3S4_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT3S4_Quantity;
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string GetQBusinessCollateralAgmLineT3S4_RegistrationPlateNumber(List<QBusinessCollateralAgmLineT3S4> qBusinessCollateralAgmLineT3S4, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT3S4.Where(wh => wh.QBusinessCollateralAgmLineT3S4_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT3S4_RegistrationPlateNumber;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string GetQBusinessCollateralAgmLineT3S4_ChassisNumber(List<QBusinessCollateralAgmLineT3S4> qBusinessCollateralAgmLineT3S4, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT3S4.Where(wh => wh.QBusinessCollateralAgmLineT3S4_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT3S4_ChassisNumber;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string GetQBusinessCollateralAgmLineT3S4_MachineNumber(List<QBusinessCollateralAgmLineT3S4> qBusinessCollateralAgmLineT3S4, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT3S4.Where(wh => wh.QBusinessCollateralAgmLineT3S4_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT3S4_MachineNumber;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string GetQBusinessCollateralAgmLineT3S4_Product(List<QBusinessCollateralAgmLineT3S4> qBusinessCollateralAgmLineT3S4, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT3S4.Where(wh => wh.QBusinessCollateralAgmLineT3S4_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT3S4_Product;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string GetQBusinessCollateralAgmLineT3S3_Unit(List<QBusinessCollateralAgmLineT3S3> qBusinessCollateralAgmLineT3S3, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT3S3.Where(wh => wh.QBusinessCollateralAgmLineT3S3_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT3S3_Unit;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private decimal GetQBusinessCollateralAgmLineT3S3_BusinessCollateralValue(List<QBusinessCollateralAgmLineT3S3> qBusinessCollateralAgmLineT3S3, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT3S3.Where(wh => wh.QBusinessCollateralAgmLineT3S3_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT3S3_BusinessCollateralValue;
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string GetQBusinessCollateralAgmLineT3S3_Product(List<QBusinessCollateralAgmLineT3S3> qBusinessCollateralAgmLineT3S3, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT3S3.Where(wh => wh.QBusinessCollateralAgmLineT3S3_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT3S3_Product;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private decimal GetQBusinessCollateralAgmLineT3S3_Quantity(List<QBusinessCollateralAgmLineT3S3> qBusinessCollateralAgmLineT3S3, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT3S3.Where(wh => wh.QBusinessCollateralAgmLineT3S3_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT3S3_Quantity;
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private decimal QBusinessCollateralAgmLineT3S2_BusinessCollateralValue(List<QBusinessCollateralAgmLineT3S2> qBusinessCollateralAgmLineT3S2, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT3S2.Where(wh => wh.QBusinessCollateralAgmLineT3S2_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT3S2_BusinessCollateralValue;
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
             private string QBusinessCollateralAgmLineT3S2_Unit(List<QBusinessCollateralAgmLineT3S2> qBusinessCollateralAgmLineT3S2, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT3S2.Where(wh => wh.QBusinessCollateralAgmLineT3S2_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT3S2_Unit;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private decimal GetQBusinessCollateralAgmLineT3S2_Quantity(List<QBusinessCollateralAgmLineT3S2> qBusinessCollateralAgmLineT3S2, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT3S2.Where(wh => wh.QBusinessCollateralAgmLineT3S2_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT3S2_Quantity;
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string GetQBusinessCollateralAgmLineT3S2_Product(List<QBusinessCollateralAgmLineT3S2> qBusinessCollateralAgmLineT3S2, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT3S2.Where(wh => wh.QBusinessCollateralAgmLineT3S2_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT3S2_Product;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string GetQBusinessCollateralAgmLineT1S1_BankType(List<QBusinessCollateralAgmLineT1S1> qBusinessCollateralAgmLineT1S1, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT1S1.Where(wh => wh.QBusinessCollateralAgmLineT1S1_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT1S1_BankType;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string GetQBusinessCollateralAgmLineT1S1_BankGroup(List<QBusinessCollateralAgmLineT1S1> qBusinessCollateralAgmLineT1S1, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT1S1.Where(wh => wh.QBusinessCollateralAgmLineT1S1_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT1S1_BankGroup;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string GetQBusinessCollateralAgmLineT1S1_AccountNumber(List<QBusinessCollateralAgmLineT1S1> qBusinessCollateralAgmLineT1S1, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT1S1.Where(wh => wh.QBusinessCollateralAgmLineT1S1_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT1S1_AccountNumber;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string GetQBusinessCollateralAgmLineT3S1_Product(List<QBusinessCollateralAgmLineT3S1> qBusinessCollateralAgmLineT3S1, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT3S1.Where(wh => wh.QBusinessCollateralAgmLineT3S1_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT3S1_Product;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private string GetQBusinessCollateralAgmLineT3S1_RegistrationPlateNumber(List<QBusinessCollateralAgmLineT3S1> qBusinessCollateralAgmLineT3S1, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT3S1.Where(wh => wh.QBusinessCollateralAgmLineT3S1_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT3S1_RegistrationPlateNumber;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private string GetQBusinessCollateralAgmLineT3S1_MachineNumber(List<QBusinessCollateralAgmLineT3S1> qBusinessCollateralAgmLineT3S1, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT3S1.Where(wh => wh.QBusinessCollateralAgmLineT3S1_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT3S1_MachineNumber;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private string GetQBusinessCollateralAgmLineT3S1_MachineRegisteredStatus(List<QBusinessCollateralAgmLineT3S1> qBusinessCollateralAgmLineT3S1, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT3S1.Where(wh => wh.QBusinessCollateralAgmLineT3S1_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT3S1_MachineRegisteredStatus;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private string GetQBusinessCollateralAgmLineT3S1_RegisteredPlace(List<QBusinessCollateralAgmLineT3S1> qBusinessCollateralAgmLineT3S1, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT3S1.Where(wh => wh.QBusinessCollateralAgmLineT3S1_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT3S1_RegisteredPlace;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private decimal GetQBusinessCollateralAgmLineT3S1_BusinessCollateralValue(List<QBusinessCollateralAgmLineT3S1> qBusinessCollateralAgmLineT3S1, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT3S1.Where(wh => wh.QBusinessCollateralAgmLineT3S1_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT3S1_BusinessCollateralValue;
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private string GetQBusinessCollateralAgmLineT3S1_Unit(List<QBusinessCollateralAgmLineT3S1> qBusinessCollateralAgmLineT3S1, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT3S1.Where(wh => wh.QBusinessCollateralAgmLineT3S1_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT3S1_Unit;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string GetQBusinessCollateralAgmLineT1S2_RefAgreementId(List<QBusinessCollateralAgmLineT1S2> qBusinessCollateralAgmLineT1S2, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT1S2.Where(wh => wh.QBusinessCollateralAgmLineT1S2_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT1S2_RefAgreementId;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        private DateTime? GetQBusinessCollateralAgmLineT1S2_RefAgreementDate(List<QBusinessCollateralAgmLineT1S2> qBusinessCollateralAgmLineT1S2, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT1S2.Where(wh => wh.QBusinessCollateralAgmLineT1S2_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT1S2_RefAgreementDate;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        private string GetQBusinessCollateralAgmLineT1S2_Lessee(List<QBusinessCollateralAgmLineT1S2> qBusinessCollateralAgmLineT1S2, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT1S2.Where(wh => wh.QBusinessCollateralAgmLineT1S2_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT1S2_Lessee;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        private string GetQBusinessCollateralAgmLineT1S2_Lessor(List<QBusinessCollateralAgmLineT1S2> qBusinessCollateralAgmLineT1S2, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT1S2.Where(wh => wh.QBusinessCollateralAgmLineT1S2_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT1S2_Lessor;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }



        private string GetQBusinessCollateralAgmLineT1S3_Description(List<QBusinessCollateralAgmLineT1S3> qBusinessCollateralAgmLineT1S3, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT1S3.Where(wh => wh.QBusinessCollateralAgmLineT1S3_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT1S3_Description;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private string GetQBusinessCollateralAgmLineT1S3_RefAgreementId(List<QBusinessCollateralAgmLineT1S3> qBusinessCollateralAgmLineT1S3, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT1S3.Where(wh => wh.QBusinessCollateralAgmLineT1S3_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT1S3_RefAgreementId;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private DateTime? GetQBusinessCollateralAgmLineT1S3_RefAgreementDate(List<QBusinessCollateralAgmLineT1S3> qBusinessCollateralAgmLineT1S3, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT1S3.Where(wh => wh.QBusinessCollateralAgmLineT1S3_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT1S3_RefAgreementDate;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private string GetQBusinessCollateralAgmLineT1S3_Lessee(List<QBusinessCollateralAgmLineT1S3> qBusinessCollateralAgmLineT1S3, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT1S3.Where(wh => wh.QBusinessCollateralAgmLineT1S3_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT1S3_Lessee;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private string GetQBusinessCollateralAgmLineT1S3_Lessor(List<QBusinessCollateralAgmLineT1S3> qBusinessCollateralAgmLineT1S3, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT1S3.Where(wh => wh.QBusinessCollateralAgmLineT1S3_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT1S3_Lessor;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private string GetQBusinessCollateralAgmLineT1S3_Product(List<QBusinessCollateralAgmLineT1S3> qBusinessCollateralAgmLineT1S3, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT1S3.Where(wh => wh.QBusinessCollateralAgmLineT1S3_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT1S3_Product;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }



        private string GetQBusinessCollateralAgmLineT2S2_BuyerName(List<QBusinessCollateralAgmLineT2S2> qBusinessCollateralAgmLineT2S2, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT2S2.Where(wh => wh.QBusinessCollateralAgmLineT2S2_Row == index).FirstOrDefault();
                if(model != null)
                {
                    return model.QBusinessCollateralAgmLineT2S2_BuyerName;
                }
                else
                {
                    return null;
                }
          
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private int GetQBusinessCollateralAgmLineT2S2_IdentificationType(List<QBusinessCollateralAgmLineT2S2> qBusinessCollateralAgmLineT2S2, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT2S2.Where(wh => wh.QBusinessCollateralAgmLineT2S2_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT2S2_IdentificationType;
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string GetQBusinessCollateralAgmLineT2S2_TaxId(List<QBusinessCollateralAgmLineT2S2> qBusinessCollateralAgmLineT2S2, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT2S2.Where(wh => wh.QBusinessCollateralAgmLineT2S2_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT2S2_TaxId;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        private string GetQBusinessCollateralAgmLineT2S2_PassportId(List<QBusinessCollateralAgmLineT2S2> qBusinessCollateralAgmLineT2S2, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT2S2.Where(wh => wh.QBusinessCollateralAgmLineT2S2_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT2S2_PassportId;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }




        private string GetQBusinessCollateralAgmLineT2S1_BuyerName(List<QBusinessCollateralAgmLineT2S1> qBusinessCollateralAgmLineT2S1, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT2S1.Where(wh => wh.QBusinessCollateralAgmLineT2S1_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT2S1_BuyerName;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private int GetQBusinessCollateralAgmLineT2S1_IdentificationType(List<QBusinessCollateralAgmLineT2S1> qBusinessCollateralAgmLineT2S1, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT2S1.Where(wh => wh.QBusinessCollateralAgmLineT2S1_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT2S1_IdentificationType;
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string GetQBusinessCollateralAgmLineT2S1_TaxId(List<QBusinessCollateralAgmLineT2S1> qBusinessCollateralAgmLineT2S1, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT2S1.Where(wh => wh.QBusinessCollateralAgmLineT2S1_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT2S1_TaxId;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        private string GetQBusinessCollateralAgmLineT2S1_PassportId(List<QBusinessCollateralAgmLineT2S1> qBusinessCollateralAgmLineT2S1, int index)
        {
            try
            {
                var model = qBusinessCollateralAgmLineT2S1.Where(wh => wh.QBusinessCollateralAgmLineT2S1_Row == index).FirstOrDefault();
                if (model != null)
                {
                    return model.QBusinessCollateralAgmLineT2S1_PassportId;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion child method

        public QBusinessCollateralAgreement GetBookmarkDocumentBusinessCollateral(Guid refGUID)
        {
            try
            {
                var businessCollateralAgmExtend = (from businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
                                                   join customerTable in db.Set<CustomerTable>()
                                                   on businessCollateralAgmTable.CustomerTableGUID equals customerTable.CustomerTableGUID into ljcustomerTable
                                                   from customerTable in ljcustomerTable.DefaultIfEmpty()
                                                   where businessCollateralAgmTable.BusinessCollateralAgmTableGUID == refGUID && businessCollateralAgmTable.AgreementDocType == (int)AgreementDocType.Addendum
                                                   select new QBusinessCollateralAgmExtend
                                                   {
                                                       QBusinessCollateralAgmExtend_BusinessCollateralAgmId = businessCollateralAgmTable.BusinessCollateralAgmId,
                                                       QBusinessCollateralAgmExtend_RefBusinessCollateralAgmTableGUID = businessCollateralAgmTable.RefBusinessCollateralAgmTableGUID,
                                                       QBusinessCollateralAgmExtend_Name = customerTable.Name,
                                                       QBusinessCollateralAgmExtend_TaxID = customerTable.TaxID,
                                                       QBusinessCollateralAgmExtend_CompanyGUID = businessCollateralAgmTable.CompanyGUID.ToString()
                                                   }
                                                   ).FirstOrDefault();
                if (businessCollateralAgmExtend == null)
                {
                    businessCollateralAgmExtend = new QBusinessCollateralAgmExtend();

                }
                var agreementTableinfo = (from agreementTable in db.Set<AgreementTableInfo>()
                                          join companyWitness1 in db.Set<EmployeeTable>()
                                          on agreementTable.WitnessCompany1GUID equals companyWitness1.EmployeeTableGUID into ljcompanyWitness1
                                          from companyWitness1 in ljcompanyWitness1.DefaultIfEmpty()
                                          join companyWitness2 in db.Set<EmployeeTable>()
                                          on agreementTable.WitnessCompany2GUID equals companyWitness2.EmployeeTableGUID into ljcompanyWitness2
                                          from companyWitness2 in ljcompanyWitness2.DefaultIfEmpty()

                                          join authorizedPersonCompany1 in db.Set<EmployeeTable>()
                                          on agreementTable.AuthorizedPersonTransCompany1GUID equals authorizedPersonCompany1.EmployeeTableGUID into ljauthorizedPersonCompany1
                                          from authorizedPersonCompany1 in ljauthorizedPersonCompany1.DefaultIfEmpty()

                                          join authorizedPersonCompany2 in db.Set<EmployeeTable>()
                                          on agreementTable.AuthorizedPersonTransCompany2GUID equals authorizedPersonCompany2.EmployeeTableGUID into ljauthorizedPersonCompany2
                                          from authorizedPersonCompany2 in ljauthorizedPersonCompany2.DefaultIfEmpty()

                                          join authorizedPersonCompany3 in db.Set<EmployeeTable>()
                                          on agreementTable.AuthorizedPersonTransCompany3GUID equals authorizedPersonCompany3.EmployeeTableGUID into ljauthorizedPersonCompany3
                                          from authorizedPersonCompany3 in ljauthorizedPersonCompany3.DefaultIfEmpty()

                                          where agreementTable.RefType == (int)RefType.BusinessCollateralAgreement && agreementTable.RefGUID == refGUID

                                          select new QAgreementTableInfo
                                          {
                                              QAgreementTableInfo_CustomerAddress = agreementTable.CustomerAddress1 + " " + agreementTable.CustomerAddress2,
                                              QAgreementTableInfo_CompanyWitness1 = companyWitness1.Name,
                                              QAgreementTableInfo_CompanyWitness2 = companyWitness2.Name,
                                              QAgreementTableInfo_CompanyAuthorizedPerson1 = authorizedPersonCompany1.Name,
                                              QAgreementTableInfo_CompanyAuthorizedPerson2 = authorizedPersonCompany2.Name,
                                              QAgreementTableInfo_CompanyAuthorizedPerson3 = authorizedPersonCompany3.Name,
                                              QAgreementTableInfo_AuthorizedPersonTransCustomer1GUID = agreementTable.AuthorizedPersonTransCustomer1GUID.ToString(),
                                              QAgreementTableInfo_AuthorizedPersonTransCustomer2GUID = agreementTable.AuthorizedPersonTransCustomer2GUID.ToString(),
                                              QAgreementTableInfo_AuthorizedPersonTransCustomer3GUID = agreementTable.AuthorizedPersonTransCustomer3GUID.ToString()
                                          }
                                          ).FirstOrDefault();
                if (agreementTableinfo == null)
                {
                    agreementTableinfo = new QAgreementTableInfo();

                }
                var customerAuthorizedPerson1 = (from customerAutherizedPerson in db.Set<RelatedPersonTable>()
                                                 join authorizedPersonTrans in db.Set<AuthorizedPersonTrans>()
                                                 on customerAutherizedPerson.RelatedPersonTableGUID equals authorizedPersonTrans.RelatedPersonTableGUID into ljauthorizedPersonTrans
                                                 from authorizedPersonTrans in ljauthorizedPersonTrans.DefaultIfEmpty()
                                                 where authorizedPersonTrans.AuthorizedPersonTransGUID.ToString() == agreementTableinfo.QAgreementTableInfo_AuthorizedPersonTransCustomer1GUID
                                                 select customerAutherizedPerson.Name
                                                 ).FirstOrDefault();
                var customerAuthorizedPerson2 = (from customerAutherizedPerson in db.Set<RelatedPersonTable>()
                                                 join authorizedPersonTrans in db.Set<AuthorizedPersonTrans>()
                                                 on customerAutherizedPerson.RelatedPersonTableGUID equals authorizedPersonTrans.RelatedPersonTableGUID into ljauthorizedPersonTrans
                                                 from authorizedPersonTrans in ljauthorizedPersonTrans.DefaultIfEmpty()
                                                 where authorizedPersonTrans.AuthorizedPersonTransGUID.ToString() == agreementTableinfo.QAgreementTableInfo_AuthorizedPersonTransCustomer2GUID
                                                 select customerAutherizedPerson.Name
                                                 ).FirstOrDefault();
                var customerAuthorizedPerson3 = (from customerAutherizedPerson in db.Set<RelatedPersonTable>()
                                                 join authorizedPersonTrans in db.Set<AuthorizedPersonTrans>()
                                                 on customerAutherizedPerson.RelatedPersonTableGUID equals authorizedPersonTrans.RelatedPersonTableGUID into ljauthorizedPersonTrans
                                                 from authorizedPersonTrans in ljauthorizedPersonTrans.DefaultIfEmpty()
                                                 where authorizedPersonTrans.AuthorizedPersonTransGUID.ToString() == agreementTableinfo.QAgreementTableInfo_AuthorizedPersonTransCustomer3GUID
                                                 select customerAutherizedPerson.Name
                                                 ).FirstOrDefault();
                var businessCollateralAgmOriginal = (from businessCollateralAgmOriginalTable in db.Set<BusinessCollateralAgmTable>()
                                                     join businessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                     on businessCollateralAgmOriginalTable.BusinessCollateralAgmTableGUID equals businessCollateralAgmLine.BusinessCollateralAgmTableGUID into ljbusinessCollateralAgmLine
                                                     from businessCollateralAgmLine in ljbusinessCollateralAgmLine.DefaultIfEmpty()
                                                     where businessCollateralAgmOriginalTable.BusinessCollateralAgmTableGUID == businessCollateralAgmExtend.QBusinessCollateralAgmExtend_RefBusinessCollateralAgmTableGUID && businessCollateralAgmOriginalTable.AgreementDocType == (int)AgreementDocType.New

                                                     group new
                                                     {
                                                         businessCollateralAgmOriginalTable.BusinessCollateralAgmTableGUID,
                                                         
                                                     }
                                                     by new
                                                     {
                                                         businessCollateralAgmOriginalTable.BusinessCollateralAgmId,
                                                         businessCollateralAgmOriginalTable.AgreementDate,
                                                         businessCollateralAgmLine.BusinessCollateralValue
                                                     } into g

                                                     select new QBusinessCollateralAgmOriginal
                                                     {
                                                         QBusinessCollateralAgmOriginal_TotalBusinessCollateralValue = g.Key.BusinessCollateralValue,
                                                         QBusinessCollateralAgmOriginal_BusinessCollateralAgmId = g.Key.BusinessCollateralAgmId,
                                                         QBusinessCollateralAgmOriginal_AgreementDate = g.Key.AgreementDate
                                                     }
                                                     ).FirstOrDefault();
                var sumBusinessCollateralValue = (from businessCollateralAgmOriginalTable in db.Set<BusinessCollateralAgmTable>()
                                                  join businessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                  on businessCollateralAgmOriginalTable.BusinessCollateralAgmTableGUID equals businessCollateralAgmLine.BusinessCollateralAgmTableGUID into ljbusinessCollateralAgmLine
                                                  from businessCollateralAgmLine in ljbusinessCollateralAgmLine.DefaultIfEmpty()
                                                  where businessCollateralAgmOriginalTable.BusinessCollateralAgmTableGUID == businessCollateralAgmExtend.QBusinessCollateralAgmExtend_RefBusinessCollateralAgmTableGUID && businessCollateralAgmOriginalTable.AgreementDocType == (int)AgreementDocType.New

                                                  select businessCollateralAgmLine.BusinessCollateralValue
                                                     ).Sum();
                if (businessCollateralAgmOriginal == null)
                {
                    businessCollateralAgmOriginal = new QBusinessCollateralAgmOriginal();

                }
                businessCollateralAgmOriginal.QBusinessCollateralAgmOriginal_TotalBusinessCollateralValue = sumBusinessCollateralValue;
 
                var businessCollateralAgmExtend1 = (from businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
                                                    join businessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                    on businessCollateralAgmTable.BusinessCollateralAgmTableGUID equals businessCollateralAgmLine.BusinessCollateralAgmTableGUID into ljbusinessCollateralAgmLine
                                                    from businessCollateralAgmLine in ljbusinessCollateralAgmLine.DefaultIfEmpty()
                                                    where businessCollateralAgmTable.RefBusinessCollateralAgmTableGUID == businessCollateralAgmExtend.QBusinessCollateralAgmExtend_RefBusinessCollateralAgmTableGUID
                                                    && businessCollateralAgmTable.AgreementDocType == (int)AgreementDocType.Addendum
                                                    && businessCollateralAgmTable.AgreementExtension == 1
                                                    group new
                                                    {
                                                        businessCollateralAgmTable.BusinessCollateralAgmTableGUID
                                                    } by new
                                                    {
                                                        businessCollateralAgmTable.AgreementDate,
                                                        businessCollateralAgmTable.AgreementExtension,
                                                        businessCollateralAgmLine.BusinessCollateralValue
                                                    } into g

                                                    select new QBusinessCollateralAgmExtendREF
                                                    {
                                                        QBusinessCollateralAgmExten_AgreementDate = g.Key.AgreementDate,
                                                        QBusinessCollateralAgmExtend_AgreementExtension = g.Key.AgreementExtension,
                                                        QBusinessCollateralAgmExtend_TotalBusinessCollateralValue = g.Key.BusinessCollateralValue
                                                    }).FirstOrDefault();
                if (businessCollateralAgmExtend1 == null)
                {
                    businessCollateralAgmExtend1 = new QBusinessCollateralAgmExtendREF();

                }
                var businessCollateralAgmExtend2 = (from businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
                                                    join businessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                    on businessCollateralAgmTable.BusinessCollateralAgmTableGUID equals businessCollateralAgmLine.BusinessCollateralAgmTableGUID into ljbusinessCollateralAgmLine
                                                    from businessCollateralAgmLine in ljbusinessCollateralAgmLine.DefaultIfEmpty()
                                                    where businessCollateralAgmTable.RefBusinessCollateralAgmTableGUID == businessCollateralAgmExtend.QBusinessCollateralAgmExtend_RefBusinessCollateralAgmTableGUID
                                                    && businessCollateralAgmTable.AgreementDocType == (int)AgreementDocType.Addendum
                                                    && businessCollateralAgmTable.AgreementExtension == 2
                                                    group new
                                                    {
                                                        businessCollateralAgmTable.BusinessCollateralAgmTableGUID
                                                    } by new
                                                    {
                                                        businessCollateralAgmTable.AgreementDate,
                                                        businessCollateralAgmTable.AgreementExtension,
                                                        businessCollateralAgmLine.BusinessCollateralValue

                                                    } into g
                                                    select new QBusinessCollateralAgmExtendREF
                                                    {
                                                        QBusinessCollateralAgmExten_AgreementDate = g.Key.AgreementDate,
                                                        QBusinessCollateralAgmExtend_AgreementExtension = g.Key.AgreementExtension,
                                                        QBusinessCollateralAgmExtend_TotalBusinessCollateralValue = g.Key.BusinessCollateralValue
                                                    }).FirstOrDefault();
                if (businessCollateralAgmExtend2 == null)
                {
                    businessCollateralAgmExtend2 = new QBusinessCollateralAgmExtendREF();

                }
                var businessCollateralAgmExtend3 = (from businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
                                                    join businessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                    on businessCollateralAgmTable.BusinessCollateralAgmTableGUID equals businessCollateralAgmLine.BusinessCollateralAgmTableGUID into ljbusinessCollateralAgmLine
                                                    from businessCollateralAgmLine in ljbusinessCollateralAgmLine.DefaultIfEmpty()
                                                    where businessCollateralAgmTable.RefBusinessCollateralAgmTableGUID == businessCollateralAgmExtend.QBusinessCollateralAgmExtend_RefBusinessCollateralAgmTableGUID
                                                    && businessCollateralAgmTable.AgreementDocType == (int)AgreementDocType.Addendum
                                                    && businessCollateralAgmTable.AgreementExtension == 3
                                                    group new
                                                    {
                                                        businessCollateralAgmTable.BusinessCollateralAgmTableGUID
                                                    } by new
                                                    {
                                                        businessCollateralAgmTable.AgreementDate,
                                                        businessCollateralAgmTable.AgreementExtension,
                                                        businessCollateralAgmLine.BusinessCollateralValue

                                                    } into g
                                                    select new QBusinessCollateralAgmExtendREF
                                                    {
                                                        QBusinessCollateralAgmExten_AgreementDate = g.Key.AgreementDate,
                                                        QBusinessCollateralAgmExtend_AgreementExtension = g.Key.AgreementExtension,
                                                        QBusinessCollateralAgmExtend_TotalBusinessCollateralValue = g.Key.BusinessCollateralValue
                                                    }).FirstOrDefault();
                if (businessCollateralAgmExtend3 == null)
                {
                    businessCollateralAgmExtend3 = new QBusinessCollateralAgmExtendREF();

                }
                var businessCollateralAgmExtend4 = (from businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
                                                    join businessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                    on businessCollateralAgmTable.BusinessCollateralAgmTableGUID equals businessCollateralAgmLine.BusinessCollateralAgmTableGUID into ljbusinessCollateralAgmLine
                                                    from businessCollateralAgmLine in ljbusinessCollateralAgmLine.DefaultIfEmpty()
                                                    where businessCollateralAgmTable.RefBusinessCollateralAgmTableGUID == businessCollateralAgmExtend.QBusinessCollateralAgmExtend_RefBusinessCollateralAgmTableGUID
                                                    && businessCollateralAgmTable.AgreementDocType == (int)AgreementDocType.Addendum
                                                    && businessCollateralAgmTable.AgreementExtension == 4
                                                    group new
                                                    {
                                                        businessCollateralAgmTable.BusinessCollateralAgmTableGUID
                                                    } by new
                                                    {
                                                        businessCollateralAgmTable.AgreementDate,
                                                        businessCollateralAgmTable.AgreementExtension,
                                                        businessCollateralAgmLine.BusinessCollateralValue

                                                    } into g
                                                    select new QBusinessCollateralAgmExtendREF
                                                    {
                                                        QBusinessCollateralAgmExten_AgreementDate = g.Key.AgreementDate,
                                                        QBusinessCollateralAgmExtend_AgreementExtension = g.Key.AgreementExtension,
                                                        QBusinessCollateralAgmExtend_TotalBusinessCollateralValue = g.Key.BusinessCollateralValue
                                                    }).FirstOrDefault();
                if (businessCollateralAgmExtend4 == null)
                {
                    businessCollateralAgmExtend4 = new QBusinessCollateralAgmExtendREF();

                }
                var businessCollateralAgmExtend5 = (from businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
                                                    join businessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                    on businessCollateralAgmTable.BusinessCollateralAgmTableGUID equals businessCollateralAgmLine.BusinessCollateralAgmTableGUID into ljbusinessCollateralAgmLine
                                                    from businessCollateralAgmLine in ljbusinessCollateralAgmLine.DefaultIfEmpty()
                                                    where businessCollateralAgmTable.RefBusinessCollateralAgmTableGUID == businessCollateralAgmExtend.QBusinessCollateralAgmExtend_RefBusinessCollateralAgmTableGUID
                                                    && businessCollateralAgmTable.AgreementDocType == (int)AgreementDocType.Addendum
                                                    && businessCollateralAgmTable.AgreementExtension == 5
                                                    group new
                                                    {
                                                        businessCollateralAgmTable.BusinessCollateralAgmTableGUID
                                                    } by new
                                                    {
                                                        businessCollateralAgmTable.AgreementDate,
                                                        businessCollateralAgmTable.AgreementExtension,
                                                        businessCollateralAgmLine.BusinessCollateralValue

                                                    } into g
                                                    select new QBusinessCollateralAgmExtendREF
                                                    {
                                                        QBusinessCollateralAgmExten_AgreementDate = g.Key.AgreementDate,
                                                        QBusinessCollateralAgmExtend_AgreementExtension = g.Key.AgreementExtension,
                                                        QBusinessCollateralAgmExtend_TotalBusinessCollateralValue = g.Key.BusinessCollateralValue
                                                    }).FirstOrDefault();
                if (businessCollateralAgmExtend5 == null)
                {
                    businessCollateralAgmExtend5 = new QBusinessCollateralAgmExtendREF();

                }
                var businessCollateralAgmExtend6 = (from businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
                                                    join businessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                    on businessCollateralAgmTable.BusinessCollateralAgmTableGUID equals businessCollateralAgmLine.BusinessCollateralAgmTableGUID into ljbusinessCollateralAgmLine
                                                    from businessCollateralAgmLine in ljbusinessCollateralAgmLine.DefaultIfEmpty()
                                                    where businessCollateralAgmTable.RefBusinessCollateralAgmTableGUID == businessCollateralAgmExtend.QBusinessCollateralAgmExtend_RefBusinessCollateralAgmTableGUID
                                                    && businessCollateralAgmTable.AgreementDocType == (int)AgreementDocType.Addendum
                                                    && businessCollateralAgmTable.AgreementExtension == 6
                                                    group new
                                                    {
                                                        businessCollateralAgmTable.BusinessCollateralAgmTableGUID
                                                    } by new
                                                    {
                                                        businessCollateralAgmTable.AgreementDate,
                                                        businessCollateralAgmTable.AgreementExtension,
                                                        businessCollateralAgmLine.BusinessCollateralValue

                                                    } into g
                                                    select new QBusinessCollateralAgmExtendREF
                                                    {
                                                        QBusinessCollateralAgmExten_AgreementDate = g.Key.AgreementDate,
                                                        QBusinessCollateralAgmExtend_AgreementExtension = g.Key.AgreementExtension,
                                                        QBusinessCollateralAgmExtend_TotalBusinessCollateralValue = g.Key.BusinessCollateralValue
                                                    }).FirstOrDefault();
                if (businessCollateralAgmExtend6 == null)
                {
                    businessCollateralAgmExtend6 = new QBusinessCollateralAgmExtendREF();

                }
                var businessCollateralAgmExtend7 = (from businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
                                                    join businessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                    on businessCollateralAgmTable.BusinessCollateralAgmTableGUID equals businessCollateralAgmLine.BusinessCollateralAgmTableGUID into ljbusinessCollateralAgmLine
                                                    from businessCollateralAgmLine in ljbusinessCollateralAgmLine.DefaultIfEmpty()
                                                    where businessCollateralAgmTable.RefBusinessCollateralAgmTableGUID == businessCollateralAgmExtend.QBusinessCollateralAgmExtend_RefBusinessCollateralAgmTableGUID
                                                    && businessCollateralAgmTable.AgreementDocType == (int)AgreementDocType.Addendum
                                                    && businessCollateralAgmTable.AgreementExtension == 7
                                                    group new
                                                    {
                                                        businessCollateralAgmTable.BusinessCollateralAgmTableGUID
                                                    } by new
                                                    {
                                                        businessCollateralAgmTable.AgreementDate,
                                                        businessCollateralAgmTable.AgreementExtension,
                                                        businessCollateralAgmLine.BusinessCollateralValue

                                                    } into g
                                                    select new QBusinessCollateralAgmExtendREF
                                                    {
                                                        QBusinessCollateralAgmExten_AgreementDate = g.Key.AgreementDate,
                                                        QBusinessCollateralAgmExtend_AgreementExtension = g.Key.AgreementExtension,
                                                        QBusinessCollateralAgmExtend_TotalBusinessCollateralValue = g.Key.BusinessCollateralValue
                                                    }).FirstOrDefault();
                if (businessCollateralAgmExtend7 == null)
                {
                    businessCollateralAgmExtend7 = new QBusinessCollateralAgmExtendREF();

                }
                var businessCollateralAgmExtend8 = (from businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
                                                    join businessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                    on businessCollateralAgmTable.BusinessCollateralAgmTableGUID equals businessCollateralAgmLine.BusinessCollateralAgmTableGUID into ljbusinessCollateralAgmLine
                                                    from businessCollateralAgmLine in ljbusinessCollateralAgmLine.DefaultIfEmpty()
                                                    where businessCollateralAgmTable.RefBusinessCollateralAgmTableGUID == businessCollateralAgmExtend.QBusinessCollateralAgmExtend_RefBusinessCollateralAgmTableGUID
                                                    && businessCollateralAgmTable.AgreementDocType == (int)AgreementDocType.Addendum
                                                    && businessCollateralAgmTable.AgreementExtension == 8
                                                    group new
                                                    {
                                                        businessCollateralAgmTable.BusinessCollateralAgmTableGUID
                                                    } by new
                                                    {
                                                        businessCollateralAgmTable.AgreementDate,
                                                        businessCollateralAgmTable.AgreementExtension,
                                                        businessCollateralAgmLine.BusinessCollateralValue

                                                    } into g
                                                    select new QBusinessCollateralAgmExtendREF
                                                    {
                                                        QBusinessCollateralAgmExten_AgreementDate = g.Key.AgreementDate,
                                                        QBusinessCollateralAgmExtend_AgreementExtension = g.Key.AgreementExtension,
                                                        QBusinessCollateralAgmExtend_TotalBusinessCollateralValue = g.Key.BusinessCollateralValue
                                                    }).FirstOrDefault();
                if (businessCollateralAgmExtend8 == null)
                {
                    businessCollateralAgmExtend8 = new QBusinessCollateralAgmExtendREF();

                }
                var businessCollateralAgmExtend9 = (from businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
                                                    join businessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                    on businessCollateralAgmTable.BusinessCollateralAgmTableGUID equals businessCollateralAgmLine.BusinessCollateralAgmTableGUID into ljbusinessCollateralAgmLine
                                                    from businessCollateralAgmLine in ljbusinessCollateralAgmLine.DefaultIfEmpty()
                                                    where businessCollateralAgmTable.RefBusinessCollateralAgmTableGUID == businessCollateralAgmExtend.QBusinessCollateralAgmExtend_RefBusinessCollateralAgmTableGUID
                                                    && businessCollateralAgmTable.AgreementDocType == (int)AgreementDocType.Addendum
                                                    && businessCollateralAgmTable.AgreementExtension == 9
                                                    group new
                                                    {
                                                        businessCollateralAgmTable.BusinessCollateralAgmTableGUID
                                                    } by new
                                                    {
                                                        businessCollateralAgmTable.AgreementDate,
                                                        businessCollateralAgmTable.AgreementExtension,
                                                        businessCollateralAgmLine.BusinessCollateralValue

                                                    } into g
                                                    select new QBusinessCollateralAgmExtendREF
                                                    {
                                                        QBusinessCollateralAgmExten_AgreementDate = g.Key.AgreementDate,
                                                        QBusinessCollateralAgmExtend_AgreementExtension = g.Key.AgreementExtension,
                                                        QBusinessCollateralAgmExtend_TotalBusinessCollateralValue = g.Key.BusinessCollateralValue
                                                    }).FirstOrDefault();
                if (businessCollateralAgmExtend9 == null)
                {
                    businessCollateralAgmExtend9 = new QBusinessCollateralAgmExtendREF();

                }
                var businessCollateralAgmExtend10 = (from businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
                                                     join businessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                                                    on businessCollateralAgmTable.BusinessCollateralAgmTableGUID equals businessCollateralAgmLine.BusinessCollateralAgmTableGUID into ljbusinessCollateralAgmLine
                                                     from businessCollateralAgmLine in ljbusinessCollateralAgmLine.DefaultIfEmpty()
                                                     where businessCollateralAgmTable.RefBusinessCollateralAgmTableGUID == businessCollateralAgmExtend.QBusinessCollateralAgmExtend_RefBusinessCollateralAgmTableGUID
                                                     && businessCollateralAgmTable.AgreementDocType == (int)AgreementDocType.Addendum
                                                     && businessCollateralAgmTable.AgreementExtension == 10
                                                     group new
                                                     {
                                                         businessCollateralAgmTable.BusinessCollateralAgmTableGUID
                                                     } by new
                                                     {
                                                         businessCollateralAgmTable.AgreementDate,
                                                         businessCollateralAgmTable.AgreementExtension,
                                                         businessCollateralAgmLine.BusinessCollateralValue

                                                     } into g
                                                     select new QBusinessCollateralAgmExtendREF
                                                     {
                                                         QBusinessCollateralAgmExten_AgreementDate = g.Key.AgreementDate,
                                                         QBusinessCollateralAgmExtend_AgreementExtension = g.Key.AgreementExtension,
                                                         QBusinessCollateralAgmExtend_TotalBusinessCollateralValue = g.Key.BusinessCollateralValue
                                                     }).FirstOrDefault();
                if (businessCollateralAgmExtend10 == null)
                {
                    businessCollateralAgmExtend10 = new QBusinessCollateralAgmExtendREF();

                }
                QCompany company = (from companyTable in db.Set<Company>()
                                    join addressTrans in db.Set<AddressTrans>()
                                    on companyTable.DefaultBranchGUID equals addressTrans.RefGUID into ljaddressTrans
                                    from addressTrans in ljaddressTrans.DefaultIfEmpty()
                                    where companyTable.CompanyGUID.ToString() == businessCollateralAgmExtend.QBusinessCollateralAgmExtend_CompanyGUID
                                    select new QCompany
                                    {
                                        QCompany_Name = companyTable.Name,
                                        QCompany_Address = addressTrans.Address1 + "" + addressTrans.Address2
                                    }
                               ).FirstOrDefault();
                if (company == null)
                {
                    company = new QCompany();

                }
                ISharedService sharedService = new SharedService();
                QBusinessCollateralAgreement model = new QBusinessCollateralAgreement();

                model.QBusinessCollateralAgmExtend_BusinessCollateralAgmId = businessCollateralAgmExtend != null ? businessCollateralAgmExtend.QBusinessCollateralAgmExtend_BusinessCollateralAgmId : "";
                model.QBusinessCollateralAgmExtend_Name = businessCollateralAgmExtend != null ? businessCollateralAgmExtend.QBusinessCollateralAgmExtend_Name : "";
                model.QBusinessCollateralAgmExtend_TaxID = businessCollateralAgmExtend != null ? businessCollateralAgmExtend.QBusinessCollateralAgmExtend_TaxID : "";
                model.QAgreementTableInfo_CustomerAddress = agreementTableinfo != null ? agreementTableinfo.QAgreementTableInfo_CustomerAddress : "";
                model.QAgreementTableInfo_CompanyWitness1 = agreementTableinfo != null ? agreementTableinfo.QAgreementTableInfo_CompanyWitness1 : "";
                model.QAgreementTableInfo_CompanyWitness2 = agreementTableinfo != null ? agreementTableinfo.QAgreementTableInfo_CompanyWitness2 : "";
                model.QAgreementTableInfo_CompanyAuthorizedPerson1 = agreementTableinfo != null ? agreementTableinfo.QAgreementTableInfo_CompanyAuthorizedPerson1 : "";
                model.QAgreementTableInfo_CompanyAuthorizedPerson2 = agreementTableinfo != null ? agreementTableinfo.QAgreementTableInfo_CompanyAuthorizedPerson2 : "";
                model.QAgreementTableInfo_CompanyAuthorizedPerson3 = agreementTableinfo != null ? agreementTableinfo.QAgreementTableInfo_CompanyAuthorizedPerson3 : "";
                model.QAgreementTableInfo_AuthorizedPersonTransCustomer1GUID = agreementTableinfo != null ? agreementTableinfo.QAgreementTableInfo_AuthorizedPersonTransCustomer1GUID : "";
                model.QAgreementTableInfo_AuthorizedPersonTransCustomer2GUID = agreementTableinfo != null ? agreementTableinfo.QAgreementTableInfo_AuthorizedPersonTransCustomer2GUID : "";
                model.QAgreementTableInfo_AuthorizedPersonTransCustomer3GUID = agreementTableinfo != null ? agreementTableinfo.QAgreementTableInfo_AuthorizedPersonTransCustomer3GUID : "";
                model.QCustomerAuthorizedPerson1_Name = customerAuthorizedPerson1 != null ? customerAuthorizedPerson1 : "";
                model.QCustomerAuthorizedPerson2_Name = customerAuthorizedPerson2 != null ? customerAuthorizedPerson2 : "";
                model.QCustomerAuthorizedPerson3_Name = customerAuthorizedPerson3 != null ? customerAuthorizedPerson3 : "";
                model.QBusinessCollateralAgmOriginal_TotalBusinessCollateralValue = businessCollateralAgmOriginal.QBusinessCollateralAgmOriginal_TotalBusinessCollateralValue;
                model.QBusinessCollateralAgmOriginal_BusinessCollateralAgmId = businessCollateralAgmOriginal != null ? businessCollateralAgmOriginal.QBusinessCollateralAgmOriginal_BusinessCollateralAgmId: "";
                model.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue = businessCollateralAgmOriginal != null ? businessCollateralAgmOriginal.QBusinessCollateralAgmOriginal_TotalBusinessCollateralValue : 0;
                model.QBusinessCollateralAgmExtend1_TotalBusinessCollateralValue = businessCollateralAgmExtend1 != null ? businessCollateralAgmExtend1.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue : 0;
                model.QBusinessCollateralAgmExtend2_TotalBusinessCollateralValue = businessCollateralAgmExtend2 != null ? businessCollateralAgmExtend2.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue : 0;
                model.QBusinessCollateralAgmExtend3_TotalBusinessCollateralValue = businessCollateralAgmExtend3 != null ? businessCollateralAgmExtend3.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue : 0;
                model.QBusinessCollateralAgmExtend4_TotalBusinessCollateralValue = businessCollateralAgmExtend4 != null ? businessCollateralAgmExtend4.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue : 0;
                model.QBusinessCollateralAgmExtend5_TotalBusinessCollateralValue = businessCollateralAgmExtend5 != null ? businessCollateralAgmExtend5.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue : 0;
                model.QBusinessCollateralAgmExtend6_TotalBusinessCollateralValue = businessCollateralAgmExtend6 != null ? businessCollateralAgmExtend6.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue : 0;
                model.QBusinessCollateralAgmExtend7_TotalBusinessCollateralValue = businessCollateralAgmExtend7 != null ? businessCollateralAgmExtend7.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue : 0;
                model.QBusinessCollateralAgmExtend8_TotalBusinessCollateralValue = businessCollateralAgmExtend8 != null ? businessCollateralAgmExtend8.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue : 0;
                model.QBusinessCollateralAgmExtend9_TotalBusinessCollateralValue = businessCollateralAgmExtend9 != null ? businessCollateralAgmExtend9.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue : 0;
                model.QBusinessCollateralAgmExtend10_TotalBusinessCollateralValue = businessCollateralAgmExtend10 != null ? businessCollateralAgmExtend10.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue : 0;
                
                model.QBusinessCollateralAgmExtend1_AgreementExtension =  businessCollateralAgmExtend1.QBusinessCollateralAgmExtend_AgreementExtension ;
                model.QBusinessCollateralAgmExtend2_AgreementExtension =  businessCollateralAgmExtend2.QBusinessCollateralAgmExtend_AgreementExtension ;
                model.QBusinessCollateralAgmExtend3_AgreementExtension =  businessCollateralAgmExtend3.QBusinessCollateralAgmExtend_AgreementExtension ;
                model.QBusinessCollateralAgmExtend4_AgreementExtension = businessCollateralAgmExtend4.QBusinessCollateralAgmExtend_AgreementExtension ;
                model.QBusinessCollateralAgmExtend5_AgreementExtension =  businessCollateralAgmExtend5.QBusinessCollateralAgmExtend_AgreementExtension;
                model.QBusinessCollateralAgmExtend6_AgreementExtension = businessCollateralAgmExtend6.QBusinessCollateralAgmExtend_AgreementExtension;
                model.QBusinessCollateralAgmExtend7_AgreementExtension = businessCollateralAgmExtend7.QBusinessCollateralAgmExtend_AgreementExtension ;
                model.QBusinessCollateralAgmExtend8_AgreementExtension =  businessCollateralAgmExtend8.QBusinessCollateralAgmExtend_AgreementExtension;
                model.QBusinessCollateralAgmExtend9_AgreementExtension =  businessCollateralAgmExtend9.QBusinessCollateralAgmExtend_AgreementExtension ;
                model.QBusinessCollateralAgmExtend10_AgreementExtension =  businessCollateralAgmExtend10.QBusinessCollateralAgmExtend_AgreementExtension ;
                model.Variable_OriginalText = NumberToWordsTHCustom(businessCollateralAgmOriginal.QBusinessCollateralAgmOriginal_TotalBusinessCollateralValue);


                model.Variable_AmountText1 = NumberToWordsTHCustom(businessCollateralAgmExtend1.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue);
                model.Variable_AmountText2 = NumberToWordsTHCustom(businessCollateralAgmExtend2.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue);
                model.Variable_AmountText3 = NumberToWordsTHCustom(businessCollateralAgmExtend3.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue);
                model.Variable_AmountText4 = NumberToWordsTHCustom(businessCollateralAgmExtend4.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue);
                model.Variable_AmountText5 = NumberToWordsTHCustom(businessCollateralAgmExtend5.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue);
                model.Variable_AmountText6 = NumberToWordsTHCustom(businessCollateralAgmExtend6.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue);
                model.Variable_AmountText7 = NumberToWordsTHCustom(businessCollateralAgmExtend7.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue);
                model.Variable_AmountText8 = NumberToWordsTHCustom(businessCollateralAgmExtend8.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue);
                model.Variable_AmountText9 = NumberToWordsTHCustom(businessCollateralAgmExtend9.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue);
                model.Variable_AmountText10 = NumberToWordsTHCustom(businessCollateralAgmExtend10.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue);

                model.Variable_AccumExt1 = businessCollateralAgmOriginal.QBusinessCollateralAgmOriginal_TotalBusinessCollateralValue + businessCollateralAgmExtend1.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue;
                model.Variable_AccumExt2 = model.Variable_AccumExt1 + businessCollateralAgmExtend2.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue;
                model.Variable_AccumExt3 = model.Variable_AccumExt2 + businessCollateralAgmExtend3.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue;
                model.Variable_AccumExt4 = model.Variable_AccumExt3 + businessCollateralAgmExtend4.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue;
                model.Variable_AccumExt5 = model.Variable_AccumExt4 + businessCollateralAgmExtend5.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue;
                model.Variable_AccumExt6 = model.Variable_AccumExt5 + businessCollateralAgmExtend6.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue;
                model.Variable_AccumExt7 = model.Variable_AccumExt6 + businessCollateralAgmExtend7.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue;
                model.Variable_AccumExt8 = model.Variable_AccumExt7 + businessCollateralAgmExtend8.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue;
                model.Variable_AccumExt9 = model.Variable_AccumExt8 + businessCollateralAgmExtend9.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue;
                model.Variable_AccumExt10 = model.Variable_AccumExt9 + businessCollateralAgmExtend10.QBusinessCollateralAgmExtend_TotalBusinessCollateralValue;

                model.Variable_AccumExtText1 = NumberToWordsTHCustom(model.Variable_AccumExt1);
                model.Variable_AccumExtText2 = NumberToWordsTHCustom(model.Variable_AccumExt2);
                model.Variable_AccumExtText3 = NumberToWordsTHCustom(model.Variable_AccumExt3);
                model.Variable_AccumExtText4 = NumberToWordsTHCustom(model.Variable_AccumExt4);
                model.Variable_AccumExtText5 = NumberToWordsTHCustom(model.Variable_AccumExt5);
                model.Variable_AccumExtText6 = NumberToWordsTHCustom(model.Variable_AccumExt6);
                model.Variable_AccumExtText7 = NumberToWordsTHCustom(model.Variable_AccumExt7);
                model.Variable_AccumExtText8 = NumberToWordsTHCustom(model.Variable_AccumExt8);
                model.Variable_AccumExtText9 = NumberToWordsTHCustom(model.Variable_AccumExt9);
                model.Variable_AccumExtText10 = NumberToWordsTHCustom(model.Variable_AccumExt10);

                model.Variable_AgmDate = DateToWordCustom(businessCollateralAgmOriginal.QBusinessCollateralAgmOriginal_AgreementDate, TextConstants.TH, TextConstants.DMY);
                model.Variable_AgmDateExt1 = DateToWordCustom(businessCollateralAgmExtend1.QBusinessCollateralAgmExten_AgreementDate, TextConstants.TH, TextConstants.DMY);
                model.Variable_AgmDateExt2 = DateToWordCustom(businessCollateralAgmExtend2.QBusinessCollateralAgmExten_AgreementDate, TextConstants.TH, TextConstants.DMY);
                model.Variable_AgmDateExt3 = DateToWordCustom(businessCollateralAgmExtend3.QBusinessCollateralAgmExten_AgreementDate, TextConstants.TH, TextConstants.DMY);
                model.Variable_AgmDateExt4 = DateToWordCustom(businessCollateralAgmExtend4.QBusinessCollateralAgmExten_AgreementDate, TextConstants.TH, TextConstants.DMY);
                model.Variable_AgmDateExt5 = DateToWordCustom(businessCollateralAgmExtend5.QBusinessCollateralAgmExten_AgreementDate, TextConstants.TH, TextConstants.DMY);
                model.Variable_AgmDateExt6 = DateToWordCustom(businessCollateralAgmExtend6.QBusinessCollateralAgmExten_AgreementDate, TextConstants.TH, TextConstants.DMY);
                model.Variable_AgmDateExt7 = DateToWordCustom(businessCollateralAgmExtend7.QBusinessCollateralAgmExten_AgreementDate, TextConstants.TH, TextConstants.DMY);
                model.Variable_AgmDateExt8 = DateToWordCustom(businessCollateralAgmExtend8.QBusinessCollateralAgmExten_AgreementDate, TextConstants.TH, TextConstants.DMY);
                model.Variable_AgmDateExt9 = DateToWordCustom(businessCollateralAgmExtend9.QBusinessCollateralAgmExten_AgreementDate, TextConstants.TH, TextConstants.DMY);
                model.Variable_AgmDateExt10 = DateToWordCustom(businessCollateralAgmExtend10.QBusinessCollateralAgmExten_AgreementDate, TextConstants.TH, TextConstants.DMY);

                model.QCompany_Name = company != null ? company.QCompany_Name : "";
                model.QCompany_Address = company != null ? company.QCompany_Address : "";
                return model;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private string NumberToWordsTHCustom(decimal totalBusinessCollateralValue)
        {
            try
            {
                ISharedService sharedService = new SharedService();
                return sharedService.NumberToWordsTH(totalBusinessCollateralValue);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public BusinessCollateralAgmTable GetBusinessCollateralAgmTableByBusinessCollateralAgmId(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return null;
                }
                else
                {
                    var result = Entity.Where(item => item.BusinessCollateralAgmId == id && item.CompanyGUID == db.GetCompanyFilter().StringToGuid()).AsNoTracking().FirstOrDefault();
                    return result;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        private IQueryable<BusinessCollateralAgmTableItemViewMap> GetDropDownQuery()
        {
            return (from businessCollateralAgmTable in Entity
                    select new BusinessCollateralAgmTableItemViewMap
                    {
                        CompanyGUID = businessCollateralAgmTable.CompanyGUID,
                        Owner = businessCollateralAgmTable.Owner,
                        OwnerBusinessUnitGUID = businessCollateralAgmTable.OwnerBusinessUnitGUID,
                        BusinessCollateralAgmTableGUID = businessCollateralAgmTable.BusinessCollateralAgmTableGUID,
                        BusinessCollateralAgmId = businessCollateralAgmTable.BusinessCollateralAgmId,
                        Description = businessCollateralAgmTable.Description,
                        RefBusinessCollateralAgmTableGUID = businessCollateralAgmTable.RefBusinessCollateralAgmTableGUID,
                        DocumentStatusGUID = businessCollateralAgmTable.DocumentStatusGUID,
                        AgreementDocType = businessCollateralAgmTable.AgreementDocType,
                        InternalBusinessCollateralAgmId = businessCollateralAgmTable.InternalBusinessCollateralAgmId                 
                    });
        }
        public IEnumerable<SelectItem<BusinessCollateralAgmTableItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = GetFilterLevelPredicate<BusinessCollateralAgmTable>(search, SysParm.CompanyGUID);
                var businessCollateralAgmTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
                    .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                    .Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
                    .ToMaps<BusinessCollateralAgmTableItemViewMap, BusinessCollateralAgmTableItemView>().ToDropDownItem(search.ExcludeRowData);
                return businessCollateralAgmTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #region GetListvw
        private IQueryable<BusinessCollateralAgmTableListViewMap> GetListQuery()
        {
            return (from businessCollateralAgmTable in Entity
                    join customerTable in db.Set<CustomerTable>()
                    on businessCollateralAgmTable.CustomerTableGUID equals customerTable.CustomerTableGUID
                    join documentStatus in db.Set<DocumentStatus>()
                    on businessCollateralAgmTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
                    join creditAppRequestTable in db.Set<CreditAppRequestTable>()
                    on businessCollateralAgmTable.CreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID
                    join creditAppTable in db.Set<CreditAppTable>()
                    on businessCollateralAgmTable.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljCreditAppTable
                    from creditAppTable in ljCreditAppTable.DefaultIfEmpty()
                    select new BusinessCollateralAgmTableListViewMap
                    {
                        CompanyGUID = businessCollateralAgmTable.CompanyGUID,
                        Owner = businessCollateralAgmTable.Owner,
                        OwnerBusinessUnitGUID = businessCollateralAgmTable.OwnerBusinessUnitGUID,
                        BusinessCollateralAgmTableGUID = businessCollateralAgmTable.BusinessCollateralAgmTableGUID,
                        InternalBusinessCollateralAgmId = businessCollateralAgmTable.InternalBusinessCollateralAgmId,
                        BusinessCollateralAgmId = businessCollateralAgmTable.BusinessCollateralAgmId,
                        Description = businessCollateralAgmTable.Description,
                        AgreementDocType = businessCollateralAgmTable.AgreementDocType,
                        CustomerTableGUID = businessCollateralAgmTable.CustomerTableGUID,
                        SigningDate = businessCollateralAgmTable.SigningDate,
                        DocumentStatusGUID = businessCollateralAgmTable.DocumentStatusGUID,
                        CreditAppRequestTableGUID = businessCollateralAgmTable.CreditAppRequestTableGUID,
                        DocumentStatus_StatusId = documentStatus.StatusId,
                        DocumentStatus_Description = documentStatus.Description,
                        ProductType = businessCollateralAgmTable.ProductType,
                        RefBusinessCollateralAgmTableGUID = businessCollateralAgmTable.RefBusinessCollateralAgmTableGUID,
                        CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                        DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
                        CreditAppRequestTable_Values = SmartAppUtil.GetDropDownLabel(creditAppRequestTable.CreditAppRequestId, creditAppRequestTable.Description),
                        CreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
                        CreditAppTable_CreditAppId = creditAppTable.CreditAppId,
                        CreditAppRequestTable_CreditAppRequestId = creditAppRequestTable.CreditAppRequestId,
                        CustomerTable_CustomerId = customerTable.CustomerId
                    });
        }
        public SearchResult<BusinessCollateralAgmTableListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<BusinessCollateralAgmTableListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<BusinessCollateralAgmTable>(search, SysParm.CompanyGUID);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<BusinessCollateralAgmTableListViewMap, BusinessCollateralAgmTableListView>();
                result = list.SetSearchResult<BusinessCollateralAgmTableListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<BusinessCollateralAgmTableItemViewMap> GetItemQuery()
        {
            return (from businessCollateralAgmTable in Entity
                    join company in db.Set<Company>()
                    on businessCollateralAgmTable.CompanyGUID equals company.CompanyGUID
                    join documentStatus in db.Set<DocumentStatus>()
                    on businessCollateralAgmTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID

                    join ownerBU in db.Set<BusinessUnit>()
                    on businessCollateralAgmTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljBusinessCollateralAgmTableOwnerBU
                    from ownerBU in ljBusinessCollateralAgmTableOwnerBU.DefaultIfEmpty()

                    join creditAppTable in db.Set<CreditAppTable>()
                    on businessCollateralAgmTable.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljCreditAppTable
                    from creditAppTable in ljCreditAppTable.DefaultIfEmpty()

                    join customerTable in db.Set<CustomerTable>()
                    on businessCollateralAgmTable.CustomerTableGUID equals customerTable.CustomerTableGUID into ljCustomerTable
                    from customerTable in ljCustomerTable.DefaultIfEmpty()

                    join creditLimitType in db.Set<CreditLimitType>()
                    on businessCollateralAgmTable.CreditLimitTypeGUID equals creditLimitType.CreditLimitTypeGUID into ljCreditLimitType
                    from creditLimitType in ljCreditLimitType.DefaultIfEmpty()

                    join consortiumTable in db.Set<ConsortiumTable>()
                    on businessCollateralAgmTable.ConsortiumTableGUID equals consortiumTable.ConsortiumTableGUID into ljConsortiumTable
                    from consortiumTable in ljConsortiumTable.DefaultIfEmpty()

                    join refBusinessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
                    on businessCollateralAgmTable.RefBusinessCollateralAgmTableGUID equals refBusinessCollateralAgmTable.BusinessCollateralAgmTableGUID into ljRefBusinessCollateralAgmTable
                    from refBusinessCollateralAgmTable in ljRefBusinessCollateralAgmTable.DefaultIfEmpty()

                    join documentReason in db.Set<DocumentReason>()
                    on businessCollateralAgmTable.DocumentReasonGUID equals documentReason.DocumentReasonGUID into ljdocumentReason
                    from documentReason in ljdocumentReason.DefaultIfEmpty()

                    join businessCollateralAgmLine in db.Set<BusinessCollateralAgmLine>()
                    .GroupBy(g => g.BusinessCollateralAgmTableGUID)
                    .Select(s => new { BusinessCollateralAgmTableGUID = s.Key, SumBusinessCollateralValue = s.Sum(su => su.BusinessCollateralValue) })
                    on businessCollateralAgmTable.BusinessCollateralAgmTableGUID equals businessCollateralAgmLine.BusinessCollateralAgmTableGUID into ljbusinessCollateralAgmLine
                    from businessCollateralAgmLine in ljbusinessCollateralAgmLine.DefaultIfEmpty()

                    select new BusinessCollateralAgmTableItemViewMap
                    {
                        CompanyGUID = businessCollateralAgmTable.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = businessCollateralAgmTable.Owner,
                        OwnerBusinessUnitGUID = businessCollateralAgmTable.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        CreatedBy = businessCollateralAgmTable.CreatedBy,
                        CreatedDateTime = businessCollateralAgmTable.CreatedDateTime,
                        ModifiedBy = businessCollateralAgmTable.ModifiedBy,
                        ModifiedDateTime = businessCollateralAgmTable.ModifiedDateTime,
                        BusinessCollateralAgmTableGUID = businessCollateralAgmTable.BusinessCollateralAgmTableGUID,
                        AgreementDate = businessCollateralAgmTable.AgreementDate,
                        AgreementDocType = businessCollateralAgmTable.AgreementDocType,
                        AgreementExtension = businessCollateralAgmTable.AgreementExtension,
                        BusinessCollateralAgmId = businessCollateralAgmTable.BusinessCollateralAgmId,
                        CreditAppRequestTableGUID = businessCollateralAgmTable.CreditAppRequestTableGUID,
                        CreditAppTableGUID = businessCollateralAgmTable.CreditAppTableGUID,
                        CreditLimitTypeGUID = businessCollateralAgmTable.CreditLimitTypeGUID,
                        CustomerAltName = customerTable.AltName,
                        CustomerName = customerTable.Name,
                        CustomerTableGUID = businessCollateralAgmTable.CustomerTableGUID,
                        CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                        Description = businessCollateralAgmTable.Description,
                        DocumentReasonGUID = businessCollateralAgmTable.DocumentReasonGUID,
                        DocumentStatusGUID = businessCollateralAgmTable.DocumentStatusGUID,
                        InternalBusinessCollateralAgmId = businessCollateralAgmTable.InternalBusinessCollateralAgmId,
                        ProductType = businessCollateralAgmTable.ProductType,
                        RefBusinessCollateralAgmTableGUID = businessCollateralAgmTable.RefBusinessCollateralAgmTableGUID,
                        SigningDate = businessCollateralAgmTable.SigningDate,
                        CreditAppTable_CreditAppId = (creditAppTable != null) ? creditAppTable.CreditAppId : null,
                        CreditLimitType_CreditLimitTypeId = (creditLimitType != null) ? creditLimitType.CreditLimitTypeId : null,
                        DocumentStatus_StatusId = documentStatus.StatusId,
                        DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
                        ConsortiumTableGUID = businessCollateralAgmTable.ConsortiumTableGUID,
                        ConsortiumTable_ConsortiumId = (consortiumTable != null) ? consortiumTable.ConsortiumId : null,
                        BusinessCollateralAgmTable_Values = (refBusinessCollateralAgmTable != null) ? SmartAppUtil.GetDropDownLabel(refBusinessCollateralAgmTable.InternalBusinessCollateralAgmId, refBusinessCollateralAgmTable.Description) : null,
                        DBDRegistrationAmount = businessCollateralAgmTable.DBDRegistrationAmount,
                        DBDRegistrationDate = businessCollateralAgmTable.DBDRegistrationDate,
                        DBDRegistrationDescription = businessCollateralAgmTable.DBDRegistrationDescription,
                        DBDRegistrationId = businessCollateralAgmTable.DBDRegistrationId,
                        AttachmentRemark = businessCollateralAgmTable.AttachmentRemark,

                        CreditLimitType_Values = (creditLimitType != null) ? SmartAppUtil.GetDropDownLabel(creditLimitType.CreditLimitTypeId, creditLimitType.Description)  : null,
                        ConsortiumTable_Values = (consortiumTable != null) ? SmartAppUtil.GetDropDownLabel(consortiumTable.ConsortiumId, consortiumTable.Description) : null,
                        CreditAppTable_Values = (creditAppTable != null) ? SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description) : null,
                        Remark = businessCollateralAgmTable.Remark,
                        DocumentReason_Values = (documentReason != null) ? SmartAppUtil.GetDropDownLabel(documentReason.ReasonId, documentReason.Description) : null,
                        TotalBusinessCollateralValue = (businessCollateralAgmLine != null) ? businessCollateralAgmLine.SumBusinessCollateralValue : 0,
                    
                        RowVersion = businessCollateralAgmTable.RowVersion,
                    });
        }
        public BusinessCollateralAgmTableItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<BusinessCollateralAgmTableItemViewMap, BusinessCollateralAgmTableItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public BusinessCollateralAgmTable CreateBusinessCollateralAgmTable(BusinessCollateralAgmTable businessCollateralAgmTable)
        {
            try
            {
                businessCollateralAgmTable.BusinessCollateralAgmTableGUID = Guid.NewGuid();
                base.Add(businessCollateralAgmTable);
                return businessCollateralAgmTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateBusinessCollateralAgmTableVoid(BusinessCollateralAgmTable businessCollateralAgmTable)
        {
            try
            {
                CreateBusinessCollateralAgmTable(businessCollateralAgmTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BusinessCollateralAgmTable UpdateBusinessCollateralAgmTable(BusinessCollateralAgmTable dbBusinessCollateralAgmTable, BusinessCollateralAgmTable inputBusinessCollateralAgmTable, List<string> skipUpdateFields = null)
        {
            try
            {
                dbBusinessCollateralAgmTable = dbBusinessCollateralAgmTable.MapUpdateValues<BusinessCollateralAgmTable>(inputBusinessCollateralAgmTable);
                base.Update(dbBusinessCollateralAgmTable);
                return dbBusinessCollateralAgmTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateBusinessCollateralAgmTableVoid(BusinessCollateralAgmTable dbBusinessCollateralAgmTable, BusinessCollateralAgmTable inputBusinessCollateralAgmTable, List<string> skipUpdateFields = null)
        {
            try
            {
                dbBusinessCollateralAgmTable = dbBusinessCollateralAgmTable.MapUpdateValues<BusinessCollateralAgmTable>(inputBusinessCollateralAgmTable, skipUpdateFields);
                base.Update(dbBusinessCollateralAgmTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Create, Update, Delete
        public BusinessCollateralAgmTable GetBusinessCollateralAgmTableByIdNoTrackingByAccessLevel(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.BusinessCollateralAgmTableGUID == guid)
                    .FilterByAccessLevel(SysParm.AccessLevel)
                    .AsNoTracking()
                    .FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AgreementTableInfo GenerateAgreementInfo(BusinessCollateralAgmTable input)
        {
            try
            {
                List<BusinessCollateralAgmTable> businessCollateralAgmTables = new List<BusinessCollateralAgmTable>();
                businessCollateralAgmTables.Add(input);
                var qGuarantorTransName = (from creditAppTable in db.Set<CreditAppTable>().Where(t => t.CreditAppTableGUID == input.CreditAppTableGUID).ToList()
                                          join guarantorTrans in db.Set<GuarantorTrans>()
                                          on creditAppTable.CreditAppTableGUID equals guarantorTrans.RefGUID
                                          join relatedPersonTable in db.Set<RelatedPersonTable>()
                                          on guarantorTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID
                                          where guarantorTrans.InActive == false
                                          group new { creditAppTable, relatedPersonTable } by creditAppTable.CreditAppTableGUID into g
                                          select new
                                          {
                                              CreditAppTableGUID = g.Key,
                                              GuarantorName = string.Join(", ", g.Select(y => y.relatedPersonTable.Name))
                                          }).FirstOrDefault();
                string guarantorTransName = "";

                if(qGuarantorTransName != null)
                {
                    guarantorTransName = qGuarantorTransName.GuarantorName.Substring(0, Math.Min(qGuarantorTransName.GuarantorName.Length, 500));
                }

                return (from businessCollateralAgmTable in businessCollateralAgmTables

                        join company in db.Set<Company>().Where(t => t.CompanyGUID == input.CompanyGUID).ToList()
                        on businessCollateralAgmTable.CompanyGUID equals company.CompanyGUID

                        join customerTable in db.Set<CustomerTable>()
                        on businessCollateralAgmTable.CustomerTableGUID equals customerTable.CustomerTableGUID

                        join branch in db.Set<Branch>()
                        on company.DefaultBranchGUID equals branch.BranchGUID into ljBranch
                        from branch in ljBranch.DefaultIfEmpty()

                        join creditAppTable in db.Set<CreditAppTable>().Where(t => t.CreditAppTableGUID == input.CreditAppTableGUID).ToList()
                        on businessCollateralAgmTable.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljCreditAppTable
                        from creditAppTable in ljCreditAppTable.DefaultIfEmpty()

                        join addressTransGuarantor in db.Set<AddressTrans>()
                        on (creditAppTable != null) ? creditAppTable.RegisteredAddressGUID : Guid.Empty equals addressTransGuarantor.AddressTransGUID into ljAddressTransGuarantor
                        from addressTransGuarantor in ljAddressTransGuarantor.DefaultIfEmpty()

                        join addressTransBranch in db.Set<AddressTrans>()
                        on (branch != null) ? branch.BranchGUID : Guid.Empty equals addressTransBranch.RefGUID into ljAddressTransBranch
                        from addressTransBranch in ljAddressTransBranch.DefaultIfEmpty()

                        join contactTransPhone in db.Set<ContactTrans>().Where(t => t.PrimaryContact == true &&
                                                                                    t.ContactType == (int)ContactType.Phone).ToList()
                        on company.DefaultBranchGUID equals contactTransPhone.RefGUID into ljContactTransPhone
                        from contactTransPhone in ljContactTransPhone.DefaultIfEmpty()

                        join contactTransFax in db.Set<ContactTrans>().Where(t => t.PrimaryContact == true &&
                                                                                    t.ContactType == (int)ContactType.Fax).ToList()
                        on company.DefaultBranchGUID equals contactTransFax.RefGUID into ljContactTransFax
                        from contactTransFax in ljContactTransFax.DefaultIfEmpty()

                        join companyBank in db.Set<CompanyBank>().Where(t => t.Primary == true).ToList()
                        on company.CompanyGUID equals companyBank.CompanyGUID into ljCompanyBank
                        from companyBank in ljCompanyBank.DefaultIfEmpty()

                        join employeeTable in db.Set<EmployeeTable>()
                        on businessCollateralAgmTable.CreatedBy equals employeeTable.Name into ljEmployeeTable
                        from employeeTable in ljEmployeeTable.DefaultIfEmpty()
                        select new AgreementTableInfo
                        {
                            RefGUID = businessCollateralAgmTable.BusinessCollateralAgmTableGUID,
                            RefType = (int)RefType.BusinessCollateralAgreement,
                            GuarantorName = guarantorTransName,
                            CustomerAddress1 = (addressTransGuarantor != null) ? addressTransGuarantor.Address1 : null,
                            CustomerAddress2 = (addressTransGuarantor != null) ? addressTransGuarantor.Address2 : null,
                            CompanyName = company.Name,
                            CompanyAltName = company.AltName,
                            CompanyGUID = company.CompanyGUID,
                            CompanyAddress1 = (addressTransBranch != null) ? addressTransBranch.Address1 : null,
                            CompanyAddress2 = (addressTransBranch != null) ? addressTransBranch.Address2 : null,
                            CompanyPhone = (contactTransPhone != null) ? contactTransPhone.Description : null,
                            CompanyFax = (contactTransFax != null) ? contactTransFax.Description : null,
                            CompanyBankGUID = companyBank?.CompanyBankGUID,
                            Owner = businessCollateralAgmTable.Owner,
                            OwnerBusinessUnitGUID = businessCollateralAgmTable.OwnerBusinessUnitGUID
                        }).ToList().FirstOrDefault();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<BusinessCollateralAgmTable> GetListByRefBusinessCollateralAgmTableAndAgreementDocType(Guid guid, AgreementDocType agreementDocType)
        {
            try
            {
                var result = Entity.Where(item => item.RefBusinessCollateralAgmTableGUID == guid && item.AgreementDocType == (int)agreementDocType).AsNoTracking().ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<BusinessCollateralAgmTable> GetBusinessCollateralAgmTableByRefBusinessCollateralAgmTableGUID(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.RefBusinessCollateralAgmTableGUID == guid).AsNoTracking().ToList() ;
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<BusinessCollateralAgmTable> GetBusinessCollateralAgmTableByIdNoTracking(IEnumerable<Guid> guids)
        {
            try
            {
                var result = Entity.Where(item => guids.Contains(item.BusinessCollateralAgmTableGUID)).AsNoTracking();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Validate
        public override void ValidateAdd(IEnumerable<BusinessCollateralAgmTable> items)
        {
            base.ValidateAdd(items);
            items.ToList().ForEach(f => ValidateAdd(f));
        }
        public override void ValidateAdd(BusinessCollateralAgmTable item)
        {
            base.ValidateAdd(item);
            CheckDuplicateBusinessCollateralAgmId(item);
        }
        public override void ValidateUpdate(BusinessCollateralAgmTable item)
        {
            base.ValidateUpdate(item);
            CheckDuplicateBusinessCollateralAgmId(item);
        }
        public override void ValidateUpdate(IEnumerable<BusinessCollateralAgmTable> items)
        {
            base.ValidateUpdate(items);
            items.ToList().ForEach(f => CheckDuplicateBusinessCollateralAgmId(f));
        }
        public void CheckDuplicateBusinessCollateralAgmId(BusinessCollateralAgmTable item)
        {
            try
            {
                SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
                bool isDuplicateBusinessCollateralAgmId = IsDuplicateBusinessCollateralAgmId(item.BusinessCollateralAgmTableGUID, item.BusinessCollateralAgmId);
                if (isDuplicateBusinessCollateralAgmId)
                {
                    smartAppException.AddData("ERROR.DUPLICATE", new string[] { "LABEL.BUSINESS_COLLATERAL_AGREEMENT_ID" });
                }
                if (smartAppException.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(smartAppException);
                }
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public bool IsDuplicateBusinessCollateralAgmId(Guid guid, string id)
        {
            try
            {
                bool isDuplicateBusinessCollateralAgmId = Entity.Any(a => a.BusinessCollateralAgmId == id
                                                                    && a.CompanyGUID == db.GetCompanyFilter().StringToGuid()
                                                                    && !string.IsNullOrEmpty(a.BusinessCollateralAgmId)
                                                                    && a.BusinessCollateralAgmTableGUID != guid);
                return isDuplicateBusinessCollateralAgmId;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        public IEnumerable<BusinessCollateralAgmTableItemViewMap> GetBusinessCollateralAgmTableNoTracking()
        {
            try
            {
                return (from businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
                        join documentStatus in db.Set<DocumentStatus>()
                        on businessCollateralAgmTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus
                        from documentStatus in ljdocumentStatus.DefaultIfEmpty()
                        select new BusinessCollateralAgmTableItemViewMap
                        {
                            CompanyGUID = businessCollateralAgmTable.CompanyGUID,
                            Owner = businessCollateralAgmTable.Owner,
                            OwnerBusinessUnitGUID = businessCollateralAgmTable.OwnerBusinessUnitGUID,
                            CreatedBy = businessCollateralAgmTable.CreatedBy,
                            CreatedDateTime = businessCollateralAgmTable.CreatedDateTime,
                            ModifiedBy = businessCollateralAgmTable.ModifiedBy,
                            ModifiedDateTime = businessCollateralAgmTable.ModifiedDateTime,
                            BusinessCollateralAgmTableGUID = businessCollateralAgmTable.BusinessCollateralAgmTableGUID,
                            AgreementDate = businessCollateralAgmTable.AgreementDate,
                            AgreementDocType = businessCollateralAgmTable.AgreementDocType,
                            AgreementExtension = businessCollateralAgmTable.AgreementExtension,
                            BusinessCollateralAgmId = businessCollateralAgmTable.BusinessCollateralAgmId,
                            CreditAppRequestTableGUID = businessCollateralAgmTable.CreditAppRequestTableGUID,
                            CreditAppTableGUID = businessCollateralAgmTable.CreditAppTableGUID,
                            CreditLimitTypeGUID = businessCollateralAgmTable.CreditLimitTypeGUID,
                            CustomerTableGUID = businessCollateralAgmTable.CustomerTableGUID,
                            Description = businessCollateralAgmTable.Description,
                            DocumentReasonGUID = businessCollateralAgmTable.DocumentReasonGUID,
                            DocumentStatusGUID = businessCollateralAgmTable.DocumentStatusGUID,
                            InternalBusinessCollateralAgmId = businessCollateralAgmTable.InternalBusinessCollateralAgmId,
                            ProductType = businessCollateralAgmTable.ProductType,
                            RefBusinessCollateralAgmTableGUID = businessCollateralAgmTable.RefBusinessCollateralAgmTableGUID,
                            SigningDate = businessCollateralAgmTable.SigningDate,
                            DocumentStatus_StatusId = documentStatus.StatusId,
                            ConsortiumTableGUID = businessCollateralAgmTable.ConsortiumTableGUID,
                            DBDRegistrationAmount = businessCollateralAgmTable.DBDRegistrationAmount,
                            DBDRegistrationDate = businessCollateralAgmTable.DBDRegistrationDate,
                            DBDRegistrationDescription = businessCollateralAgmTable.DBDRegistrationDescription,
                            DBDRegistrationId = businessCollateralAgmTable.DBDRegistrationId,
                            AttachmentRemark = businessCollateralAgmTable.AttachmentRemark,
                            Remark = businessCollateralAgmTable.Remark,
                        }
                        ).AsNoTracking();
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        public bool IsBusinessAgmLessthanSignedList(Guid businessCollateralAgmTableGUID , int agreementDoctype, int statusId)
        {
            try
            {
                var  result = (from businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
                        join documentStatus in db.Set<DocumentStatus>()
                        on businessCollateralAgmTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus
                        from documentStatus in ljdocumentStatus.DefaultIfEmpty()
                        where businessCollateralAgmTable.AgreementDocType == agreementDoctype
                        && businessCollateralAgmTable.RefBusinessCollateralAgmTableGUID == businessCollateralAgmTableGUID
                        && Convert.ToInt32(documentStatus.StatusId) < statusId
                        select new BusinessCollateralAgmTable
                        {
                            BusinessCollateralAgmTableGUID = businessCollateralAgmTable.BusinessCollateralAgmTableGUID
                        }


                         );
                return result.Any();
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion

    }
}

