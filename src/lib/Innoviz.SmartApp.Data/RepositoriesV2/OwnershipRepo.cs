using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IOwnershipRepo
	{
		#region DropDown
		IEnumerable<SelectItem<OwnershipItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<OwnershipListView> GetListvw(SearchParameter search);
		OwnershipItemView GetByIdvw(Guid id);
		Ownership Find(params object[] keyValues);
		Ownership GetOwnershipByIdNoTracking(Guid guid);
		Ownership CreateOwnership(Ownership ownership);
		void CreateOwnershipVoid(Ownership ownership);
		Ownership UpdateOwnership(Ownership dbOwnership, Ownership inputOwnership, List<string> skipUpdateFields = null);
		void UpdateOwnershipVoid(Ownership dbOwnership, Ownership inputOwnership, List<string> skipUpdateFields = null);
		void Remove(Ownership item);
	}
	public class OwnershipRepo : CompanyBaseRepository<Ownership>, IOwnershipRepo
	{
		public OwnershipRepo(SmartAppDbContext context) : base(context) { }
		public OwnershipRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public Ownership GetOwnershipByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.OwnershipGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<OwnershipItemViewMap> GetDropDownQuery()
		{
			return (from ownership in Entity
					select new OwnershipItemViewMap
					{
						CompanyGUID = ownership.CompanyGUID,
						Owner = ownership.Owner,
						OwnerBusinessUnitGUID = ownership.OwnerBusinessUnitGUID,
						OwnershipGUID = ownership.OwnershipGUID,
						OwnershipId = ownership.OwnershipId,
						Description = ownership.Description
					});
		}
		public IEnumerable<SelectItem<OwnershipItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<Ownership>(search, SysParm.CompanyGUID);
				var ownership = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<OwnershipItemViewMap, OwnershipItemView>().ToDropDownItem(search.ExcludeRowData);


				return ownership;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<OwnershipListViewMap> GetListQuery()
		{
			return (from ownership in Entity
				select new OwnershipListViewMap
				{
						CompanyGUID = ownership.CompanyGUID,
						Owner = ownership.Owner,
						OwnerBusinessUnitGUID = ownership.OwnerBusinessUnitGUID,
						OwnershipGUID = ownership.OwnershipGUID,
						OwnershipId = ownership.OwnershipId,
						Description = ownership.Description
				});
		}
		public SearchResult<OwnershipListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<OwnershipListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<Ownership>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<OwnershipListViewMap, OwnershipListView>();
				result = list.SetSearchResult<OwnershipListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<OwnershipItemViewMap> GetItemQuery()
		{
			return (from ownership in Entity
					join company in db.Set<Company>()
					on ownership.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on ownership.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljOwnershipOwnerBU
					from ownerBU in ljOwnershipOwnerBU.DefaultIfEmpty()
					select new OwnershipItemViewMap
					{
						CompanyGUID = ownership.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = ownership.Owner,
						OwnerBusinessUnitGUID = ownership.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = ownership.CreatedBy,
						CreatedDateTime = ownership.CreatedDateTime,
						ModifiedBy = ownership.ModifiedBy,
						ModifiedDateTime = ownership.ModifiedDateTime,
						OwnershipGUID = ownership.OwnershipGUID,
						Description = ownership.Description,
						OwnershipId = ownership.OwnershipId,
					
						RowVersion = ownership.RowVersion,
					});
		}
		public OwnershipItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<OwnershipItemViewMap, OwnershipItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public Ownership CreateOwnership(Ownership ownership)
		{
			try
			{
				ownership.OwnershipGUID = Guid.NewGuid();
				base.Add(ownership);
				return ownership;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateOwnershipVoid(Ownership ownership)
		{
			try
			{
				CreateOwnership(ownership);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public Ownership UpdateOwnership(Ownership dbOwnership, Ownership inputOwnership, List<string> skipUpdateFields = null)
		{
			try
			{
				dbOwnership = dbOwnership.MapUpdateValues<Ownership>(inputOwnership);
				base.Update(dbOwnership);
				return dbOwnership;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateOwnershipVoid(Ownership dbOwnership, Ownership inputOwnership, List<string> skipUpdateFields = null)
		{
			try
			{
				dbOwnership = dbOwnership.MapUpdateValues<Ownership>(inputOwnership, skipUpdateFields);
				base.Update(dbOwnership);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

