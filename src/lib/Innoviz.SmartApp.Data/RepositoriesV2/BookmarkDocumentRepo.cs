using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IBookmarkDocumentRepo
	{
		#region DropDown
		IEnumerable<SelectItem<BookmarkDocumentItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<BookmarkDocumentListView> GetListvw(SearchParameter search);
		BookmarkDocumentItemView GetByIdvw(Guid id);
		BookmarkDocument Find(params object[] keyValues);
		BookmarkDocument GetBookmarkDocumentByIdNoTracking(Guid guid);
		BookmarkDocument CreateBookmarkDocument(BookmarkDocument bookmarkDocument);
		void CreateBookmarkDocumentVoid(BookmarkDocument bookmarkDocument);
		BookmarkDocument UpdateBookmarkDocument(BookmarkDocument dbBookmarkDocument, BookmarkDocument inputBookmarkDocument, List<string> skipUpdateFields = null);
		void UpdateBookmarkDocumentVoid(BookmarkDocument dbBookmarkDocument, BookmarkDocument inputBookmarkDocument, List<string> skipUpdateFields = null);
		void Remove(BookmarkDocument item);
	}
	public class BookmarkDocumentRepo : CompanyBaseRepository<BookmarkDocument>, IBookmarkDocumentRepo
	{
		public BookmarkDocumentRepo(SmartAppDbContext context) : base(context) { }
		public BookmarkDocumentRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BookmarkDocument GetBookmarkDocumentByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BookmarkDocumentGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<BookmarkDocumentItemViewMap> GetDropDownQuery()
		{
			return (from bookmarkDocument in Entity
					select new BookmarkDocumentItemViewMap
					{
						CompanyGUID = bookmarkDocument.CompanyGUID,
						Owner = bookmarkDocument.Owner,
						OwnerBusinessUnitGUID = bookmarkDocument.OwnerBusinessUnitGUID,
						BookmarkDocumentGUID = bookmarkDocument.BookmarkDocumentGUID,
						BookmarkDocumentId = bookmarkDocument.BookmarkDocumentId,
						Description = bookmarkDocument.Description,
						BookmarkDocumentRefType = bookmarkDocument.BookmarkDocumentRefType,
						DocumentTemplateType = bookmarkDocument.DocumentTemplateType
					});
		}
		public IEnumerable<SelectItem<BookmarkDocumentItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<BookmarkDocument>(search, SysParm.CompanyGUID);
				var bookmarkDocument = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<BookmarkDocumentItemViewMap, BookmarkDocumentItemView>().ToDropDownItem(search.ExcludeRowData);


				return bookmarkDocument;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<BookmarkDocumentListViewMap> GetListQuery()
		{
			return (from bookmarkDocument in Entity
				select new BookmarkDocumentListViewMap
				{
						CompanyGUID = bookmarkDocument.CompanyGUID,
						Owner = bookmarkDocument.Owner,
						OwnerBusinessUnitGUID = bookmarkDocument.OwnerBusinessUnitGUID,
						BookmarkDocumentGUID = bookmarkDocument.BookmarkDocumentGUID,
						BookmarkDocumentId = bookmarkDocument.BookmarkDocumentId,
						Description = bookmarkDocument.Description,
						DocumentTemplateType = bookmarkDocument.DocumentTemplateType,
						BookmarkDocumentRefType = bookmarkDocument.BookmarkDocumentRefType,
				});
		}
		public SearchResult<BookmarkDocumentListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<BookmarkDocumentListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<BookmarkDocument>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<BookmarkDocumentListViewMap, BookmarkDocumentListView>();
				result = list.SetSearchResult<BookmarkDocumentListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<BookmarkDocumentItemViewMap> GetItemQuery()
		{
			return (from bookmarkDocument in Entity
					join company in db.Set<Company>()
					on bookmarkDocument.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on bookmarkDocument.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljBookmarkDocumentOwnerBU
					from ownerBU in ljBookmarkDocumentOwnerBU.DefaultIfEmpty()
					select new BookmarkDocumentItemViewMap
					{
						CompanyGUID = bookmarkDocument.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = bookmarkDocument.Owner,
						OwnerBusinessUnitGUID = bookmarkDocument.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = bookmarkDocument.CreatedBy,
						CreatedDateTime = bookmarkDocument.CreatedDateTime,
						ModifiedBy = bookmarkDocument.ModifiedBy,
						ModifiedDateTime = bookmarkDocument.ModifiedDateTime,
						BookmarkDocumentGUID = bookmarkDocument.BookmarkDocumentGUID,
						BookmarkDocumentId = bookmarkDocument.BookmarkDocumentId,
						BookmarkDocumentRefType = bookmarkDocument.BookmarkDocumentRefType,
						Description = bookmarkDocument.Description,
						DocumentTemplateType = bookmarkDocument.DocumentTemplateType,
					
						RowVersion = bookmarkDocument.RowVersion,
					});
		}
		public BookmarkDocumentItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<BookmarkDocumentItemViewMap, BookmarkDocumentItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public BookmarkDocument CreateBookmarkDocument(BookmarkDocument bookmarkDocument)
		{
			try
			{
				bookmarkDocument.BookmarkDocumentGUID = Guid.NewGuid();
				base.Add(bookmarkDocument);
				return bookmarkDocument;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateBookmarkDocumentVoid(BookmarkDocument bookmarkDocument)
		{
			try
			{
				CreateBookmarkDocument(bookmarkDocument);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BookmarkDocument UpdateBookmarkDocument(BookmarkDocument dbBookmarkDocument, BookmarkDocument inputBookmarkDocument, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBookmarkDocument = dbBookmarkDocument.MapUpdateValues<BookmarkDocument>(inputBookmarkDocument);
				base.Update(dbBookmarkDocument);
				return dbBookmarkDocument;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateBookmarkDocumentVoid(BookmarkDocument dbBookmarkDocument, BookmarkDocument inputBookmarkDocument, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBookmarkDocument = dbBookmarkDocument.MapUpdateValues<BookmarkDocument>(inputBookmarkDocument, skipUpdateFields);
				base.Update(dbBookmarkDocument);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

