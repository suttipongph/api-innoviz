using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICreditTermRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CreditTermItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<CreditTermListView> GetListvw(SearchParameter search);
		CreditTermItemView GetByIdvw(Guid id);
		CreditTerm Find(params object[] keyValues);
		CreditTerm GetCreditTermByIdNoTracking(Guid guid);
		CreditTerm CreateCreditTerm(CreditTerm creditTerm);
		void CreateCreditTermVoid(CreditTerm creditTerm);
		CreditTerm UpdateCreditTerm(CreditTerm dbCreditTerm, CreditTerm inputCreditTerm, List<string> skipUpdateFields = null);
		void UpdateCreditTermVoid(CreditTerm dbCreditTerm, CreditTerm inputCreditTerm, List<string> skipUpdateFields = null);
		void Remove(CreditTerm item);
	}
	public class CreditTermRepo : CompanyBaseRepository<CreditTerm>, ICreditTermRepo
	{
		public CreditTermRepo(SmartAppDbContext context) : base(context) { }
		public CreditTermRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CreditTerm GetCreditTermByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CreditTermGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CreditTermItemViewMap> GetDropDownQuery()
		{
			return (from creditTerm in Entity
					select new CreditTermItemViewMap
					{
						CompanyGUID = creditTerm.CompanyGUID,
						Owner = creditTerm.Owner,
						OwnerBusinessUnitGUID = creditTerm.OwnerBusinessUnitGUID,
						CreditTermGUID = creditTerm.CreditTermGUID,
						CreditTermId = creditTerm.CreditTermId,
						Description = creditTerm.Description
					});
		}
		public IEnumerable<SelectItem<CreditTermItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditTerm>(search, SysParm.CompanyGUID);
				var creditTerm = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CreditTermItemViewMap, CreditTermItemView>().ToDropDownItem(search.ExcludeRowData);


				return creditTerm;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CreditTermListViewMap> GetListQuery()
		{
			return (from creditTerm in Entity
				select new CreditTermListViewMap
				{
						CompanyGUID = creditTerm.CompanyGUID,
						Owner = creditTerm.Owner,
						OwnerBusinessUnitGUID = creditTerm.OwnerBusinessUnitGUID,
						CreditTermGUID = creditTerm.CreditTermGUID,
						CreditTermId = creditTerm.CreditTermId,
						Description = creditTerm.Description,
						NumberOfDays = creditTerm.NumberOfDays,
				});
		}
		public SearchResult<CreditTermListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CreditTermListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditTerm>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CreditTermListViewMap, CreditTermListView>();
				result = list.SetSearchResult<CreditTermListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CreditTermItemViewMap> GetItemQuery()
		{
			return (from creditTerm in Entity
					join company in db.Set<Company>()
					on creditTerm.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on creditTerm.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCreditTermOwnerBU
					from ownerBU in ljCreditTermOwnerBU.DefaultIfEmpty()
					select new CreditTermItemViewMap
					{
						CompanyGUID = creditTerm.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = creditTerm.Owner,
						OwnerBusinessUnitGUID = creditTerm.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = creditTerm.CreatedBy,
						CreatedDateTime = creditTerm.CreatedDateTime,
						ModifiedBy = creditTerm.ModifiedBy,
						ModifiedDateTime = creditTerm.ModifiedDateTime,
						CreditTermGUID = creditTerm.CreditTermGUID,
						CreditTermId = creditTerm.CreditTermId,
						Description = creditTerm.Description,
						NumberOfDays = creditTerm.NumberOfDays,
					
						RowVersion = creditTerm.RowVersion,
					});
		}
		public CreditTermItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<CreditTermItemViewMap, CreditTermItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public CreditTerm CreateCreditTerm(CreditTerm creditTerm)
		{
			try
			{
				creditTerm.CreditTermGUID = Guid.NewGuid();
				base.Add(creditTerm);
				return creditTerm;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCreditTermVoid(CreditTerm creditTerm)
		{
			try
			{
				CreateCreditTerm(creditTerm);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditTerm UpdateCreditTerm(CreditTerm dbCreditTerm, CreditTerm inputCreditTerm, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCreditTerm = dbCreditTerm.MapUpdateValues<CreditTerm>(inputCreditTerm);
				base.Update(dbCreditTerm);
				return dbCreditTerm;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCreditTermVoid(CreditTerm dbCreditTerm, CreditTerm inputCreditTerm, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCreditTerm = dbCreditTerm.MapUpdateValues<CreditTerm>(inputCreditTerm, skipUpdateFields);
				base.Update(dbCreditTerm);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

