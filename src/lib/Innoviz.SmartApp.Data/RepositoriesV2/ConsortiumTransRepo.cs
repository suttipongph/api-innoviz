using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface IConsortiumTransRepo
    {
        #region DropDown
        IEnumerable<SelectItem<ConsortiumTransItemView>> GetDropDownItem(SearchParameter search);
        #endregion DropDown
        SearchResult<ConsortiumTransListView> GetListvw(SearchParameter search);
        ConsortiumTransItemView GetByIdvw(Guid id);
        ConsortiumTrans Find(params object[] keyValues);
        ConsortiumTrans GetConsortiumTransByIdNoTracking(Guid guid);
        ConsortiumTrans CreateConsortiumTrans(ConsortiumTrans consortiumTrans);
        void CreateConsortiumTransVoid(ConsortiumTrans consortiumTrans);
        ConsortiumTrans UpdateConsortiumTrans(ConsortiumTrans dbConsortiumTrans, ConsortiumTrans inputConsortiumTrans, List<string> skipUpdateFields = null);
        void UpdateConsortiumTransVoid(ConsortiumTrans dbConsortiumTrans, ConsortiumTrans inputConsortiumTrans, List<string> skipUpdateFields = null);
        void Remove(ConsortiumTrans item);
        ConsortiumTrans GetConsortiumTransByIdNoTrackingByAccessLevel(Guid guid);
        IEnumerable<ConsortiumTrans> GeConsortiumTransByReferenceTableNoTracking(Guid refGUID, int refType);
        MainAgreementConsortiumBookmarkView GetMainAgreementConsortiumBookmarkValue(Guid refGuid);
        void ValidateAdd(ConsortiumTrans item);
        void ValidateAdd(IEnumerable<ConsortiumTrans> items);
        #region bookmark
        QConsortiumTrans1 GetQConsortiumTrans1(Guid refGUID);
        QConsortiumTrans2 GetQConsortiumTrans2(Guid refGUID);
        QConsortiumTrans3 GetQConsortiumTrans3(Guid refGUID);
        List<QConsortiumTrans1> GetQConsortiumTrans(Guid refGUID);
        #endregion
    }
    public class ConsortiumTransRepo : CompanyBaseRepository<ConsortiumTrans>, IConsortiumTransRepo
    {
        public ConsortiumTransRepo(SmartAppDbContext context) : base(context) { }
        public ConsortiumTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public ConsortiumTrans GetConsortiumTransByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.ConsortiumTransGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        private IQueryable<ConsortiumTransItemViewMap> GetDropDownQuery()
        {
            return (from consortiumTrans in Entity
                    select new ConsortiumTransItemViewMap
                    {
                        CompanyGUID = consortiumTrans.CompanyGUID,
                        Owner = consortiumTrans.Owner,
                        OwnerBusinessUnitGUID = consortiumTrans.OwnerBusinessUnitGUID,
                        ConsortiumTransGUID = consortiumTrans.ConsortiumTransGUID,
                        CustomerName = consortiumTrans.CustomerName
                    });
        }
        public IEnumerable<SelectItem<ConsortiumTransItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<ConsortiumTrans>(search, SysParm.CompanyGUID);
                var consortiumTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<ConsortiumTransItemViewMap, ConsortiumTransItemView>().ToDropDownItem(search.ExcludeRowData);


                return consortiumTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #region GetListvw
        private IQueryable<ConsortiumTransListViewMap> GetListQuery()
        {
            return (from consortiumTrans in Entity
                    join authorizedPersonType in db.Set<AuthorizedPersonType>()
                    on consortiumTrans.AuthorizedPersonTypeGUID equals authorizedPersonType.AuthorizedPersonTypeGUID into ljConsortiumTransAuthorizedPersonType
                    from authorizedPersonType in ljConsortiumTransAuthorizedPersonType.DefaultIfEmpty()

                    join businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
                    on consortiumTrans.RefGUID equals businessCollateralAgmTable.BusinessCollateralAgmTableGUID into ljbusinessCollateralAgmTable
                    from businessCollateralAgmTable in ljbusinessCollateralAgmTable.DefaultIfEmpty()

                    select new ConsortiumTransListViewMap
                    {
                        CompanyGUID = consortiumTrans.CompanyGUID,
                        Owner = consortiumTrans.Owner,
                        OwnerBusinessUnitGUID = consortiumTrans.OwnerBusinessUnitGUID,
                        ConsortiumTransGUID = consortiumTrans.ConsortiumTransGUID,
                        Ordering = consortiumTrans.Ordering,
                        AuthorizedPersonTypeGUID = consortiumTrans.AuthorizedPersonTypeGUID,
                        CustomerName = consortiumTrans.CustomerName,
                        OperatedBy = consortiumTrans.OperatedBy,
                        RefGUID = consortiumTrans.RefGUID,
                        AuthorizedPersonType_AuthorizedPersonTypeId = (authorizedPersonType != null) ? authorizedPersonType.AuthorizedPersonTypeId : null,
                        AuthorizedPersonType_Values = (authorizedPersonType != null) ? SmartAppUtil.GetDropDownLabel(authorizedPersonType.AuthorizedPersonTypeId, authorizedPersonType.Description) : null,
                    });
        }
        public SearchResult<ConsortiumTransListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<ConsortiumTransListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<ConsortiumTrans>(search, SysParm.CompanyGUID);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<ConsortiumTransListViewMap, ConsortiumTransListView>();
                result = list.SetSearchResult<ConsortiumTransListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<ConsortiumTransItemViewMap> GetItemQuery()
        {
            return (from consortiumTrans in Entity
                    join company in db.Set<Company>()
                    on consortiumTrans.CompanyGUID equals company.CompanyGUID
                    join ownerBU in db.Set<BusinessUnit>()
                    on consortiumTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljConsortiumTransOwnerBU
                    from ownerBU in ljConsortiumTransOwnerBU.DefaultIfEmpty()

                    join mainAgreementTable in db.Set<MainAgreementTable>()
                    on consortiumTrans.RefGUID equals mainAgreementTable.MainAgreementTableGUID into ljMainAgreementTable
                    from mainAgreementTable in ljMainAgreementTable.DefaultIfEmpty()

                    join businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
                    on consortiumTrans.RefGUID equals businessCollateralAgmTable.BusinessCollateralAgmTableGUID into ljBusinessCollateralAgmTable
                    from businessCollateralAgmTable in ljBusinessCollateralAgmTable.DefaultIfEmpty()

                    join consortiumTable in db.Set<ConsortiumTable>()
                    on businessCollateralAgmTable.ConsortiumTableGUID equals consortiumTable.ConsortiumTableGUID into ljConsortiumTable
                    from consortiumTable in ljConsortiumTable.DefaultIfEmpty()

                    join consortiumLine in db.Set<ConsortiumLine>()
                    on consortiumTable.ConsortiumTableGUID equals consortiumLine.ConsortiumTableGUID into ljConsortiumLine
                    from consortiumLine in ljConsortiumLine.DefaultIfEmpty()

                    join businesscollateral in db.Set<BusinessCollateralAgmTable>()
                    on consortiumTrans.RefGUID equals businesscollateral.BusinessCollateralAgmTableGUID into ljBusinewwCollateral
                    from businesscollateral in ljBusinewwCollateral.DefaultIfEmpty()

                    join assignmentAgreementTable in db.Set<AssignmentAgreementTable>()
                    on consortiumTrans.RefGUID equals assignmentAgreementTable.AssignmentAgreementTableGUID into ljAssignmentAgreementTable
                    from assignmentAgreementTable in ljAssignmentAgreementTable.DefaultIfEmpty()

                    select new ConsortiumTransItemViewMap
                    {
                        CompanyGUID = consortiumTrans.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = consortiumTrans.Owner,
                        OwnerBusinessUnitGUID = consortiumTrans.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        CreatedBy = consortiumTrans.CreatedBy,
                        CreatedDateTime = consortiumTrans.CreatedDateTime,
                        ModifiedBy = consortiumTrans.ModifiedBy,
                        ModifiedDateTime = consortiumTrans.ModifiedDateTime,
                        ConsortiumTransGUID = consortiumTrans.ConsortiumTransGUID,
                        Address = consortiumTrans.Address,
                        AuthorizedPersonTypeGUID = consortiumTrans.AuthorizedPersonTypeGUID,
                        CustomerName = consortiumTrans.CustomerName,
                        OperatedBy = consortiumTrans.OperatedBy,
                        Ordering = consortiumTrans.Ordering,
                        RefGUID = consortiumTrans.RefGUID,
                        RefType = consortiumTrans.RefType,
                        RefId = (mainAgreementTable != null && consortiumTrans.RefType == (int)RefType.MainAgreement) ? mainAgreementTable.InternalMainAgreementId :
                        (businesscollateral != null && consortiumTrans.RefType == (int)RefType.BusinessCollateralAgreement) ? businesscollateral.InternalBusinessCollateralAgmId :
                        (assignmentAgreementTable != null && consortiumTrans.RefType == (int)RefType.AssignmentAgreement) ? assignmentAgreementTable.InternalAssignmentAgreementId :
                        null,
                        Remark = consortiumTrans.Remark,
                        MainAgreementTable_ConsortiumTableGUID = (mainAgreementTable != null) ? (Guid?)mainAgreementTable.ConsortiumTableGUID :
                                                                 (businessCollateralAgmTable != null) ? (Guid?)businessCollateralAgmTable.ConsortiumTableGUID : null,
                        ConsortiumLineGUID = consortiumLine.ConsortiumLineGUID,
                    
                        RowVersion = consortiumTrans.RowVersion,
                    });
        }
        public ConsortiumTransItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<ConsortiumTransItemViewMap, ConsortiumTransItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public ConsortiumTrans CreateConsortiumTrans(ConsortiumTrans consortiumTrans)
        {
            try
            {
                consortiumTrans.ConsortiumTransGUID = Guid.NewGuid();
                base.Add(consortiumTrans);
                return consortiumTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateConsortiumTransVoid(ConsortiumTrans consortiumTrans)
        {
            try
            {
                CreateConsortiumTrans(consortiumTrans);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ConsortiumTrans UpdateConsortiumTrans(ConsortiumTrans dbConsortiumTrans, ConsortiumTrans inputConsortiumTrans, List<string> skipUpdateFields = null)
        {
            try
            {
                dbConsortiumTrans = dbConsortiumTrans.MapUpdateValues<ConsortiumTrans>(inputConsortiumTrans);
                base.Update(dbConsortiumTrans);
                return dbConsortiumTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateConsortiumTransVoid(ConsortiumTrans dbConsortiumTrans, ConsortiumTrans inputConsortiumTrans, List<string> skipUpdateFields = null)
        {
            try
            {
                dbConsortiumTrans = dbConsortiumTrans.MapUpdateValues<ConsortiumTrans>(inputConsortiumTrans, skipUpdateFields);
                base.Update(dbConsortiumTrans);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Create, Update, Delete
        public ConsortiumTrans GetConsortiumTransByIdNoTrackingByAccessLevel(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.ConsortiumTransGUID == guid)
                    .FilterByAccessLevel(SysParm.AccessLevel)
                    .AsNoTracking()
                    .FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<ConsortiumTrans> GeConsortiumTransByReferenceTableNoTracking(Guid refGUID, int refType)
        {
            try
            {
                var result = Entity.Where(item => item.RefGUID == refGUID && item.RefType == refType).AsNoTracking();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region MainAgreementConsortiumBookmark
        public MainAgreementConsortiumBookmarkView GetMainAgreementConsortiumBookmarkValue(Guid refGuid)
        {
            try
            {
                var qMainAgreementTable = (from consortium in db.Set<ConsortiumTable>()
                                          join mainAgreement in db.Set<MainAgreementTable>()
                                          on consortium.ConsortiumTableGUID equals mainAgreement.ConsortiumTableGUID
                                          where mainAgreement.MainAgreementTableGUID == refGuid
                                          select consortium).FirstOrDefault();
                var consortiumTrans = Entity.Where(w => w.RefGUID == refGuid);

                return (from consortiumTrans1 in consortiumTrans.Where(w => w.Ordering == 1)
                        join authorizedPersonType1 in db.Set<AuthorizedPersonType>()
                        on consortiumTrans1.AuthorizedPersonTypeGUID equals authorizedPersonType1.AuthorizedPersonTypeGUID
                        into ljauthType1
                        from authorizedPersonType1 in ljauthType1.DefaultIfEmpty()

                        join consortiumTrans2 in consortiumTrans.Where(w => w.Ordering == 2)
                        on refGuid equals consortiumTrans2.RefGUID
                        into ljconsortiumTrans2
                        from consortiumTrans2 in ljconsortiumTrans2.DefaultIfEmpty()
                        join authorizedPersonType2 in db.Set<AuthorizedPersonType>()
                        on consortiumTrans2.AuthorizedPersonTypeGUID equals authorizedPersonType2.AuthorizedPersonTypeGUID
                        into ljauthType2
                        from authorizedPersonType2 in ljauthType2.DefaultIfEmpty()

                        join consortiumTrans3 in consortiumTrans.Where(w => w.Ordering == 3)
                        on refGuid equals consortiumTrans3.RefGUID
                        into ljconsortiumTrans3
                        from consortiumTrans3 in ljconsortiumTrans3.DefaultIfEmpty()
                        join authorizedPersonType3 in db.Set<AuthorizedPersonType>()
                        on consortiumTrans3.AuthorizedPersonTypeGUID equals authorizedPersonType3.AuthorizedPersonTypeGUID
                        into ljauthType3
                        from authorizedPersonType3 in ljauthType3.DefaultIfEmpty()

                        join consortiumTrans4 in consortiumTrans.Where(w => w.Ordering == 4)
                        on refGuid equals consortiumTrans4.RefGUID
                        into ljconsortiumTrans4
                        from consortiumTrans4 in ljconsortiumTrans4.DefaultIfEmpty()
                        join authorizedPersonType4 in db.Set<AuthorizedPersonType>()
                        on consortiumTrans4.AuthorizedPersonTypeGUID equals authorizedPersonType4.AuthorizedPersonTypeGUID
                        into ljauthType4
                        from authorizedPersonType4 in ljauthType4.DefaultIfEmpty()
                        select new MainAgreementConsortiumBookmarkView
                        {
                            ConsName1 = qMainAgreementTable.Description,
                            ConsName2 = qMainAgreementTable.Description,
                            ConsName3 = qMainAgreementTable.Description,
                            ConsName4 = qMainAgreementTable.Description,
                            ConsName5 = qMainAgreementTable.Description,
                            ConsName6 = qMainAgreementTable.Description,
                            ConsName7 = qMainAgreementTable.Description,

                            ConsNameCustFirst1 = consortiumTrans1 != null ? consortiumTrans1.CustomerName : "",
                            ConsNameCustFirst2 = consortiumTrans1 != null ? consortiumTrans1.CustomerName : "",
                            ConsNameCustFirst3 = consortiumTrans1 != null ? consortiumTrans1.CustomerName : "",
                            ConsNameCustFirst4 = consortiumTrans1 != null ? consortiumTrans1.CustomerName : "",

                            ConsNameCustSecond1 = consortiumTrans2 != null ? consortiumTrans2.CustomerName : "",
                            ConsNameCustSecond2 = consortiumTrans2 != null ? consortiumTrans2.CustomerName : "",
                            ConsNameCustSecond3 = consortiumTrans2 != null ? consortiumTrans2.CustomerName : "",
                            ConsNameCustSecond4 = consortiumTrans2 != null ? consortiumTrans2.CustomerName : "",

                            ConsNameCustThird1 = consortiumTrans3 != null ? consortiumTrans3.CustomerName : "",
                            ConsNameCustThird2 = consortiumTrans3 != null ? consortiumTrans3.CustomerName : "",
                            ConsNameCustThird3 = consortiumTrans3 != null ? consortiumTrans3.CustomerName : "",
                            ConsNameCustThird4 = consortiumTrans3 != null ? consortiumTrans3.CustomerName : "",

                            ConsNameCustForth1 = consortiumTrans4 != null ? consortiumTrans4.CustomerName : "",
                            ConsNameCustForth2 = consortiumTrans4 != null ? consortiumTrans4.CustomerName : "",
                            ConsNameCustForth3 = consortiumTrans4 != null ? consortiumTrans4.CustomerName : "",
                            ConsNameCustForth4 = consortiumTrans4 != null ? consortiumTrans4.CustomerName : "",

                            ConsAddressCustFirst1 = consortiumTrans1 != null ? consortiumTrans1.Address : "",
                            ConsAddressCustFirst2 = consortiumTrans1 != null ? consortiumTrans1.Address : "",
                            ConsAddressCustFirst3 = consortiumTrans1 != null ? consortiumTrans1.Address : "",
                            ConsAddressCustFirst4 = consortiumTrans1 != null ? consortiumTrans1.Address : "",

                            ConsAddressCustSecond1 = consortiumTrans2 != null ? consortiumTrans2.Address : "",
                            ConsAddressCustSecond2 = consortiumTrans2 != null ? consortiumTrans2.Address : "",
                            ConsAddressCustSecond3 = consortiumTrans2 != null ? consortiumTrans2.Address : "",
                            ConsAddressCustSecond4 = consortiumTrans2 != null ? consortiumTrans2.Address : "",

                            ConsAddressCustThird1 = consortiumTrans3 != null ? consortiumTrans3.Address : "",
                            ConsAddressCustThird2 = consortiumTrans3 != null ? consortiumTrans3.Address : "",
                            ConsAddressCustThird3 = consortiumTrans3 != null ? consortiumTrans3.Address : "",
                            ConsAddressCustThird4 = consortiumTrans3 != null ? consortiumTrans3.Address : "",

                            ConsAddressCustForth1 = consortiumTrans4 != null ? consortiumTrans4.Address : "",
                            ConsAddressCustForth2 = consortiumTrans4 != null ? consortiumTrans4.Address : "",
                            ConsAddressCustForth3 = consortiumTrans4 != null ? consortiumTrans4.Address : "",
                            ConsAddressCustForth4 = consortiumTrans4 != null ? consortiumTrans4.Address : "",

                            ConOperatedbyFirst1 = consortiumTrans1 != null ? consortiumTrans1.OperatedBy : "",
                            ConOperatedbyFirst2 = consortiumTrans1 != null ? consortiumTrans1.OperatedBy : "",
                            ConOperatedbyFirst3 = consortiumTrans1 != null ? consortiumTrans1.OperatedBy : "",
                            ConOperatedbyFirst4 = consortiumTrans1 != null ? consortiumTrans1.OperatedBy : "",

                            ConOperatedbySecond1 = consortiumTrans2 != null ? consortiumTrans2.OperatedBy : "",
                            ConOperatedbySecond2 = consortiumTrans2 != null ? consortiumTrans2.OperatedBy : "",
                            ConOperatedbySecond3 = consortiumTrans2 != null ? consortiumTrans2.OperatedBy : "",
                            ConOperatedbySecond4 = consortiumTrans2 != null ? consortiumTrans2.OperatedBy : "",

                            ConOperatedbyThird1 = consortiumTrans3 != null ? consortiumTrans3.OperatedBy : "",
                            ConOperatedbyThird2 = consortiumTrans3 != null ? consortiumTrans3.OperatedBy : "",
                            ConOperatedbyThird3 = consortiumTrans3 != null ? consortiumTrans3.OperatedBy : "",
                            ConOperatedbyThird4 = consortiumTrans3 != null ? consortiumTrans3.OperatedBy : "",

                            ConOperatedbyForth1 = consortiumTrans4 != null ? consortiumTrans4.OperatedBy : "",
                            ConOperatedbyForth2 = consortiumTrans4 != null ? consortiumTrans4.OperatedBy : "",
                            ConOperatedbyForth3 = consortiumTrans4 != null ? consortiumTrans4.OperatedBy : "",
                            ConOperatedbyForth4 = consortiumTrans4 != null ? consortiumTrans4.OperatedBy : "",

                            PositionConsCustFirst1 = authorizedPersonType1 != null ? authorizedPersonType1.Description : "",
                            PositionConsCustFirst2 = authorizedPersonType1 != null ? authorizedPersonType1.Description : "",
                            PositionConsCustFirst3 = authorizedPersonType1 != null ? authorizedPersonType1.Description : "",
                            PositionConsCustFirst4 = authorizedPersonType1 != null ? authorizedPersonType1.Description : "",

                            PositionConsCustSecond1 = authorizedPersonType2 != null ? authorizedPersonType2.Description : "",
                            PositionConsCustSecond2 = authorizedPersonType2 != null ? authorizedPersonType2.Description : "",
                            PositionConsCustSecond3 = authorizedPersonType2 != null ? authorizedPersonType2.Description : "",
                            PositionConsCustSecond4 = authorizedPersonType2 != null ? authorizedPersonType2.Description : "",

                            PositionConsCustThird1 = authorizedPersonType3 != null ? authorizedPersonType3.Description : "",
                            PositionConsCustThird2 = authorizedPersonType3 != null ? authorizedPersonType3.Description : "",
                            PositionConsCustThird3 = authorizedPersonType3 != null ? authorizedPersonType3.Description : "",
                            PositionConsCustThird4 = authorizedPersonType3 != null ? authorizedPersonType3.Description : "",

                            PositionConsCustForth1 = authorizedPersonType4 != null ? authorizedPersonType4.Description : "",
                            PositionConsCustForth2 = authorizedPersonType4 != null ? authorizedPersonType4.Description : "",
                            PositionConsCustForth3 = authorizedPersonType4 != null ? authorizedPersonType4.Description : "",
                            PositionConsCustForth4 = authorizedPersonType4 != null ? authorizedPersonType4.Description : "",
                        }).AsNoTracking().FirstOrDefault();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Bookmark document query 04GuarantorAgreement
        public List<QConsortiumTrans1> GetQConsortiumTrans(Guid refGUID)
        {
            try
            {
                int i = 1;
                int j = 0;
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                var g_result = guarantorAgreementTableRepo.GetGuarantorAgreementTableByIdNoTracking(refGUID);

                List<QConsortiumTrans1> result = new List<QConsortiumTrans1>(new QConsortiumTrans1[3]);
                List<QConsortiumTrans1> tmp_result = (from consortiumTrans in Entity
                                            where consortiumTrans.RefGUID == g_result.MainAgreementTableGUID
                                            select new QConsortiumTrans1
                                            {
                                                QConsortiumTrans1_GuarantorAgreement_CustomerName = consortiumTrans.CustomerName,
                                                QConsortiumTrans1_GuarantorAgreement_Address = consortiumTrans.Address,
                                                Ordering = consortiumTrans.Ordering,

                                            }).OrderBy(o => o.Ordering).ToList();
                foreach (var item in tmp_result)
                {
                    result[j] = item;
                    i++;
                    j++;
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public QConsortiumTrans1 GetQConsortiumTrans1(Guid refGUID)
        {
            try
            {
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                var g_result = guarantorAgreementTableRepo.GetGuarantorAgreementTableByIdNoTracking(refGUID);

                QConsortiumTrans1 result = (from consortiumTrans in Entity

                                            where consortiumTrans.RefGUID == g_result.MainAgreementTableGUID && consortiumTrans.Ordering == 1
                                            select new QConsortiumTrans1
                                            {
                                                QConsortiumTrans1_GuarantorAgreement_CustomerName = consortiumTrans.CustomerName,
                                                QConsortiumTrans1_GuarantorAgreement_Address = consortiumTrans.Address,

                                            }).FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public QConsortiumTrans2 GetQConsortiumTrans2(Guid refGUID)
        {
            try
            {
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                var g_result = guarantorAgreementTableRepo.GetGuarantorAgreementTableByIdNoTracking(refGUID);

                QConsortiumTrans2 result = (from consortiumTrans in Entity

                                            where consortiumTrans.RefGUID == g_result.MainAgreementTableGUID && consortiumTrans.Ordering == 2
                                            select new QConsortiumTrans2
                                            {
                                                QConsortiumTrans2_GuarantorAgreement_CustomerName = consortiumTrans.CustomerName,
                                                QConsortiumTrans2_GuarantorAgreement_Address = consortiumTrans.Address,

                                            }).FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public QConsortiumTrans3 GetQConsortiumTrans3(Guid refGUID)
        {
            try
            {
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                var g_result = guarantorAgreementTableRepo.GetGuarantorAgreementTableByIdNoTracking(refGUID);

                QConsortiumTrans3 result = (from consortiumTrans in Entity

                                            where consortiumTrans.RefGUID == g_result.MainAgreementTableGUID && consortiumTrans.Ordering == 3
                                            select new QConsortiumTrans3
                                            {
                                                QConsortiumTrans3_GuarantorAgreement_CustomerName = consortiumTrans.CustomerName,
                                                QConsortiumTrans3_GuarantorAgreement_Address = consortiumTrans.Address,

                                            }).FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }
}

