using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IDocumentTemplateTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<DocumentTemplateTableItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<DocumentTemplateTableListView> GetListvw(SearchParameter search);
		Task<DocumentTemplateTableItemView> GetByIdvw(Guid id);
		DocumentTemplateTable Find(params object[] keyValues);
		DocumentTemplateTable GetDocumentTemplateTableByIdNoTracking(Guid guid);
		DocumentTemplateTable CreateDocumentTemplateTable(DocumentTemplateTable documentTemplateTable);
		void CreateDocumentTemplateTableVoid(DocumentTemplateTable documentTemplateTable);
		DocumentTemplateTable UpdateDocumentTemplateTable(DocumentTemplateTable dbDocumentTemplateTable, DocumentTemplateTable inputDocumentTemplateTable, List<string> skipUpdateFields = null);
		void UpdateDocumentTemplateTableVoid(DocumentTemplateTable dbDocumentTemplateTable, DocumentTemplateTable inputDocumentTemplateTable, List<string> skipUpdateFields = null);
		void Remove(DocumentTemplateTable item);
		DocumentTemplateTable GetDocumentTemplateTableByIdNoTrackingByAccessLevel(Guid guid);
		#region Method103, 104
		Task<DocumentTemplateTableItemViewMap> GetDocumentTemplateTableItemViewMapByDocumentTemplateTableGUID(Guid guid);
		Task<List<DocumentTemplateTableItemViewMap>> GetDocumentTemplateTableItemViewMapByDocumentTemplateTableGUID(List<Guid> guids);
		#endregion
	}
    public class DocumentTemplateTableRepo : CompanyBaseRepository<DocumentTemplateTable>, IDocumentTemplateTableRepo
	{
		public DocumentTemplateTableRepo(SmartAppDbContext context) : base(context) { }
		public DocumentTemplateTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public DocumentTemplateTable GetDocumentTemplateTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.DocumentTemplateTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<DocumentTemplateTableItemViewMap> GetDropDownQuery()
		{
			return (from documentTemplateTable in Entity
					select new DocumentTemplateTableItemViewMap
					{
                        CompanyGUID = documentTemplateTable.CompanyGUID,
                        Owner = documentTemplateTable.Owner,
                        OwnerBusinessUnitGUID = documentTemplateTable.OwnerBusinessUnitGUID,
                        DocumentTemplateTableGUID = documentTemplateTable.DocumentTemplateTableGUID,
						TemplateId = documentTemplateTable.TemplateId,
						Description = documentTemplateTable.Description,
						DocumentTemplateType = documentTemplateTable.DocumentTemplateType
					});
		}
		public IEnumerable<SelectItem<DocumentTemplateTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<DocumentTemplateTable>(search, SysParm.CompanyGUID);
				var documentTemplateTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<DocumentTemplateTableItemViewMap, DocumentTemplateTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return documentTemplateTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<DocumentTemplateTableListViewMap> GetListQuery()
		{
			return (from documentTemplateTable in Entity
				select new DocumentTemplateTableListViewMap
				{
						CompanyGUID = documentTemplateTable.CompanyGUID,
						Owner = documentTemplateTable.Owner,
						OwnerBusinessUnitGUID = documentTemplateTable.OwnerBusinessUnitGUID,
						DocumentTemplateTableGUID = documentTemplateTable.DocumentTemplateTableGUID,
						TemplateId = documentTemplateTable.TemplateId,
						Description = documentTemplateTable.Description,
						DocumentTemplateType = documentTemplateTable.DocumentTemplateType
				});
		}
		public SearchResult<DocumentTemplateTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<DocumentTemplateTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<DocumentTemplateTable>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<DocumentTemplateTableListViewMap, DocumentTemplateTableListView>();
				result = list.SetSearchResult<DocumentTemplateTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<DocumentTemplateTableItemViewMap> GetItemQuery()
		{
			return (from documentTemplateTable in Entity
					join company in db.Set<Company>()
					on documentTemplateTable.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on documentTemplateTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljDocumentTemplateTableOwnerBU
					from ownerBU in ljDocumentTemplateTableOwnerBU.DefaultIfEmpty()
					select new DocumentTemplateTableItemViewMap
					{
						CompanyGUID = documentTemplateTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = documentTemplateTable.Owner,
						OwnerBusinessUnitGUID = documentTemplateTable.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = documentTemplateTable.CreatedBy,
						CreatedDateTime = documentTemplateTable.CreatedDateTime,
						ModifiedBy = documentTemplateTable.ModifiedBy,
						ModifiedDateTime = documentTemplateTable.ModifiedDateTime,
						DocumentTemplateTableGUID = documentTemplateTable.DocumentTemplateTableGUID,
						Base64Data = documentTemplateTable.Base64Data,
						Description = documentTemplateTable.Description,
						DocumentTemplateType = documentTemplateTable.DocumentTemplateType,
						FileName = documentTemplateTable.FileName,
						FilePath = documentTemplateTable.FilePath,
						TemplateId = documentTemplateTable.TemplateId,
					
						RowVersion = documentTemplateTable.RowVersion,
					});
		}
		public async Task<DocumentTemplateTableItemView> GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<DocumentTemplateTableItemViewMap, DocumentTemplateTableItemView>();
				item = await item.GetFileContent();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public DocumentTemplateTable CreateDocumentTemplateTable(DocumentTemplateTable documentTemplateTable)
		{
			try
			{
				Guid newGuid = Guid.NewGuid();
				documentTemplateTable.DocumentTemplateTableGUID = newGuid;
				documentTemplateTable.FilePath = newGuid.GuidNullToString() + FileEXT.DOCX_EXTENSION;
				ValidateAdd(documentTemplateTable);
				documentTemplateTable = documentTemplateTable.EnsureFileCreated();
				base.Add(documentTemplateTable);
				return documentTemplateTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateDocumentTemplateTableVoid(DocumentTemplateTable documentTemplateTable)
		{
			try
			{
				CreateDocumentTemplateTable(documentTemplateTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentTemplateTable UpdateDocumentTemplateTable(DocumentTemplateTable dbDocumentTemplateTable, DocumentTemplateTable inputDocumentTemplateTable, List<string> skipUpdateFields = null)
		{
			try
			{
				ValidateUpdate(inputDocumentTemplateTable);
				inputDocumentTemplateTable = inputDocumentTemplateTable.EnsureFileUpdated();
				dbDocumentTemplateTable = dbDocumentTemplateTable.MapUpdateValues<DocumentTemplateTable>(inputDocumentTemplateTable, skipUpdateFields);
				base.Update(dbDocumentTemplateTable);
				return dbDocumentTemplateTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateDocumentTemplateTableVoid(DocumentTemplateTable dbDocumentTemplateTable, DocumentTemplateTable inputDocumentTemplateTable, List<string> skipUpdateFields = null)
		{
			try
			{
				ValidateUpdate(inputDocumentTemplateTable);
				inputDocumentTemplateTable = inputDocumentTemplateTable.EnsureFileUpdated();
				dbDocumentTemplateTable = dbDocumentTemplateTable.MapUpdateValues<DocumentTemplateTable>(inputDocumentTemplateTable, skipUpdateFields);
				base.Update(dbDocumentTemplateTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public DocumentTemplateTable GetDocumentTemplateTableByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.DocumentTemplateTableGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Method103, 104
		public async Task<DocumentTemplateTableItemViewMap> GetDocumentTemplateTableItemViewMapByDocumentTemplateTableGUID(Guid guid)
		{
            try
            {
				List<Guid> guids = new List<Guid>() { guid };
				var result = (await GetDocumentTemplateTableItemViewMapByDocumentTemplateTableGUID(guids)).FirstOrDefault();
				return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
		}
		public async Task<List<DocumentTemplateTableItemViewMap>> GetDocumentTemplateTableItemViewMapByDocumentTemplateTableGUID(List<Guid> guids)
        {
            try
            {
				var result = GetItemQuery()
								.Where(w => guids.Contains(w.DocumentTemplateTableGUID))
								.ToList();
                if (FileHandler.GetFileRootPath(typeof(DocumentTemplateTable)) != null)
                {
                    var taskGetFile = result.Select(async s =>
					{
						s = await s.GetFileContent();
						return s;
					});
					var getFileResult = await Task.WhenAll(taskGetFile);
					return getFileResult.ToList();
                }
                else
                {
                    return result;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }
}

