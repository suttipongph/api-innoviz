using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IOwnerTransRepo
	{
		#region DropDown
		IEnumerable<SelectItem<OwnerTransItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<OwnerTransListView> GetListvw(SearchParameter search);
		OwnerTransItemView GetByIdvw(Guid id);
		OwnerTrans Find(params object[] keyValues);
		OwnerTrans GetOwnerTransByIdNoTracking(Guid guid);
		OwnerTrans GetOwnerTransByIdNoTrackingByAccessLevel(Guid guid);
		OwnerTrans CreateOwnerTrans(OwnerTrans ownerTrans);
		void CreateOwnerTransVoid(OwnerTrans ownerTrans);
		OwnerTrans UpdateOwnerTrans(OwnerTrans dbOwnerTrans, OwnerTrans inputOwnerTrans, List<string> skipUpdateFields = null);
		void UpdateOwnerTransVoid(OwnerTrans dbOwnerTrans, OwnerTrans inputOwnerTrans, List<string> skipUpdateFields = null);
		void Remove(OwnerTrans item);
		IEnumerable<OwnerTrans> GetOwnerTransByReference(Guid refId, int refType);
		void ValidateAdd(OwnerTrans item);
		void ValidateAdd(IEnumerable<OwnerTrans> items);
	}
	public class OwnerTransRepo : CompanyBaseRepository<OwnerTrans>, IOwnerTransRepo
	{
		public OwnerTransRepo(SmartAppDbContext context) : base(context) { }
		public OwnerTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public OwnerTrans GetOwnerTransByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.OwnerTransGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public OwnerTrans GetOwnerTransByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.OwnerTransGUID == guid)
									.FilterByAccessLevel(SysParm.AccessLevel)
									.AsNoTracking()
									.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<OwnerTransItemViewMap> GetDropDownQuery()
		{
			return (from ownerTrans in Entity
					join relatedPersonTable in db.Set<RelatedPersonTable>()
					on ownerTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID into ljRelatedPersonTable
					from relatedPersonTable in ljRelatedPersonTable.DefaultIfEmpty()
					join customerTable in db.Set<CustomerTable>()
					on ownerTrans.RefGUID equals customerTable.CustomerTableGUID into ljCustomerTable
					from customerTable in ljCustomerTable.DefaultIfEmpty()
					select new OwnerTransItemViewMap
					{
						CompanyGUID = ownerTrans.CompanyGUID,
						Owner = ownerTrans.Owner,
						OwnerBusinessUnitGUID = ownerTrans.OwnerBusinessUnitGUID,
						OwnerTransGUID = ownerTrans.OwnerTransGUID,
						RelatedPersonTableGUID = ownerTrans.RelatedPersonTableGUID,
						RefGUID = ownerTrans.RefGUID,
						RelatedPersonTable_BackgroundSummary = relatedPersonTable.BackgroundSummary,
						RelatedPersonTable_DateOfBirth = relatedPersonTable.DateOfBirth,
						RelatedPersonTable_Email = relatedPersonTable.Email,
						RelatedPersonTable_Extension = relatedPersonTable.Extension,
						RelatedPersonTable_Fax = relatedPersonTable.Fax,
						RelatedPersonTable_IdentificationType = relatedPersonTable.IdentificationType,
						RelatedPersonTable_LineId = relatedPersonTable.LineId,
						RelatedPersonTable_Mobile = relatedPersonTable.Mobile,
						RelatedPersonTable_Name = relatedPersonTable.Name,
						RelatedPersonTable_PassportId = relatedPersonTable.PassportId,
						RelatedPersonTable_Phone = relatedPersonTable.Phone,
						RelatedPersonTable_TaxId = relatedPersonTable.TaxId,
						RelatedPersonTable_WorkPermitId = relatedPersonTable.WorkPermitId,
						RefId = (customerTable != null && ownerTrans.RefType == (int)RefType.Customer) ? customerTable.CustomerId : null,
					});
		}
		public IEnumerable<SelectItem<OwnerTransItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<OwnerTrans>(search, SysParm.CompanyGUID);
				var ownerTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<OwnerTransItemViewMap, OwnerTransItemView>().ToDropDownItem(search.ExcludeRowData);


				return ownerTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<OwnerTransListViewMap> GetListQuery()
		{
			return (from ownerTrans in Entity
					join relatedPersonTable in db.Set<RelatedPersonTable>()
					on ownerTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID into ljRelatedPersonTable
					from relatedPersonTable in ljRelatedPersonTable.DefaultIfEmpty()
					select new OwnerTransListViewMap
				{
						CompanyGUID = ownerTrans.CompanyGUID,
						Owner = ownerTrans.Owner,
						OwnerBusinessUnitGUID = ownerTrans.OwnerBusinessUnitGUID,
						OwnerTransGUID = ownerTrans.OwnerTransGUID,
						Ordering = ownerTrans.Ordering,
						InActive = ownerTrans.InActive,
						RefGUID = ownerTrans.RefGUID,
						RelatedPersonTable_Name = relatedPersonTable.Name,
						RelatedPersonTable_PassportId = relatedPersonTable.PassportId,
						RelatedPersonTable_TaxId = relatedPersonTable.TaxId,
					});
		}
		public SearchResult<OwnerTransListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<OwnerTransListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<OwnerTrans>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<OwnerTransListViewMap, OwnerTransListView>();
				result = list.SetSearchResult<OwnerTransListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<OwnerTransItemViewMap> GetItemQuery()
		{
			IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService();

			return (from ownerTrans in Entity
					join company in db.Set<Company>()
					on ownerTrans.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on ownerTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljOwnerTransOwnerBU
					from ownerBU in ljOwnerTransOwnerBU.DefaultIfEmpty()
					join relatedPersonTable in db.Set<RelatedPersonTable>()
					on ownerTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID into ljRelatedPersonTable
					from relatedPersonTable in ljRelatedPersonTable.DefaultIfEmpty()
					join customerTable in db.Set<CustomerTable>()
					on ownerTrans.RefGUID equals customerTable.CustomerTableGUID into ljCustomerTable
					from customerTable in ljCustomerTable.DefaultIfEmpty()
					join creditAppRequestTable in db.Set<CreditAppRequestTable>()
					on ownerTrans.RefGUID equals creditAppRequestTable.CreditAppRequestTableGUID into ljCreditAppRequestTable
					from creditAppRequestTable in ljCreditAppRequestTable.DefaultIfEmpty()
					join creditAppTable in db.Set<CreditAppTable>()
					on ownerTrans.RefGUID equals creditAppTable.CreditAppTableGUID into ljCreditAppTable
					from creditAppTable in ljCreditAppTable.DefaultIfEmpty()

					select new OwnerTransItemViewMap
					{
						CompanyGUID = ownerTrans.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = ownerTrans.Owner,
						OwnerBusinessUnitGUID = ownerTrans.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = ownerTrans.CreatedBy,
						CreatedDateTime = ownerTrans.CreatedDateTime,
						ModifiedBy = ownerTrans.ModifiedBy,
						ModifiedDateTime = ownerTrans.ModifiedDateTime,
						OwnerTransGUID = ownerTrans.OwnerTransGUID,
						InActive = ownerTrans.InActive,
						Ordering = ownerTrans.Ordering,
						PropotionOfShareholderPct = ownerTrans.PropotionOfShareholderPct,
						RefGUID = ownerTrans.RefGUID,
						RefType = ownerTrans.RefType,
						RelatedPersonTableGUID = ownerTrans.RelatedPersonTableGUID,
						RelatedPersonTable_BackgroundSummary = relatedPersonTable.BackgroundSummary,
						RelatedPersonTable_DateOfBirth = relatedPersonTable.DateOfBirth,
						RelatedPersonTable_Email = relatedPersonTable.Email,
						RelatedPersonTable_Extension = relatedPersonTable.Extension,
						RelatedPersonTable_Fax = relatedPersonTable.Fax,
						RelatedPersonTable_IdentificationType = relatedPersonTable.IdentificationType,
						RelatedPersonTable_LineId = relatedPersonTable.LineId,
						RelatedPersonTable_Mobile = relatedPersonTable.Mobile,
						RelatedPersonTable_Name = relatedPersonTable.Name,
						RelatedPersonTable_PassportId = relatedPersonTable.PassportId,
						RelatedPersonTable_Phone = relatedPersonTable.Phone,
						RelatedPersonTable_TaxId = relatedPersonTable.TaxId,
						RelatedPersonTable_WorkPermitId = relatedPersonTable.WorkPermitId,
						RefId = (customerTable != null && ownerTrans.RefType == (int)RefType.Customer) ? customerTable.CustomerId :
								(creditAppRequestTable != null && ownerTrans.RefType == (int)RefType.CreditAppRequestTable) ? creditAppRequestTable.CreditAppRequestId :
								(creditAppTable != null && ownerTrans.RefType == (int)RefType.CreditAppTable) ? creditAppTable.CreditAppId :
								null,
						Age = relatedPersonTableService.CalcAge(relatedPersonTable.DateOfBirth),
					
						RowVersion = ownerTrans.RowVersion,
					});
		}
		public OwnerTransItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<OwnerTransItemViewMap, OwnerTransItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public OwnerTrans CreateOwnerTrans(OwnerTrans ownerTrans)
		{
			try
			{
				ownerTrans.OwnerTransGUID = Guid.NewGuid();
				base.Add(ownerTrans);
				return ownerTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateOwnerTransVoid(OwnerTrans ownerTrans)
		{
			try
			{
				CreateOwnerTrans(ownerTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public OwnerTrans UpdateOwnerTrans(OwnerTrans dbOwnerTrans, OwnerTrans inputOwnerTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbOwnerTrans = dbOwnerTrans.MapUpdateValues<OwnerTrans>(inputOwnerTrans);
				base.Update(dbOwnerTrans);
				return dbOwnerTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateOwnerTransVoid(OwnerTrans dbOwnerTrans, OwnerTrans inputOwnerTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbOwnerTrans = dbOwnerTrans.MapUpdateValues<OwnerTrans>(inputOwnerTrans, skipUpdateFields);
				base.Update(dbOwnerTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete

		public override void ValidateAdd(OwnerTrans item)
		{
			base.ValidateAdd(item);
			CheckDuplicateActive(item);
		}
		public override void ValidateAdd(IEnumerable<OwnerTrans> items)
		{
			base.ValidateAdd(items);
			items.ToList().ForEach(f => CheckDuplicateActive(f));
		}
		public override void ValidateUpdate(OwnerTrans item)
		{
			base.ValidateUpdate(item);
			CheckDuplicateActive(item);
		}
		public IEnumerable<OwnerTrans> GetOwnerTransByReference(Guid refId, int refType)
		{
			try
			{
				Guid companyGUID = SysParm.CompanyGUID.StringToGuid();
				return Entity.Where(t => t.CompanyGUID == companyGUID && t.RefGUID == refId && t.RefType == refType).AsNoTracking();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#region Validate
		public void CheckDuplicateActive(OwnerTrans item)
		{
			try
			{
				SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
				IEnumerable<OwnerTrans> ownerTrans = GetOwnerTransByReference(item.RefGUID, item.RefType);
				bool isDupplicateOrdering = ownerTrans.Any(a => (a.InActive == item.InActive) 
																	 && item.InActive == false
																	 && (a.Ordering == item.Ordering)
																	 && (a.OwnerTransGUID != item.OwnerTransGUID));
				bool isDupplicateRelatedPerson = ownerTrans.Any(a => (a.InActive == item.InActive)
													 && item.InActive == false
													 && (a.RelatedPersonTableGUID == item.RelatedPersonTableGUID)
													 && (a.OwnerTransGUID != item.OwnerTransGUID));
				if (isDupplicateOrdering)
				{
					smartAppException.AddData("ERROR.DUPLICATE", new string[] { "LABEL.ORDERING", "LABEL.REF_ID", "LABEL.ACTIVE" });
				}

				if (isDupplicateRelatedPerson)
				{
					smartAppException.AddData("ERROR.DUPLICATE", new string[] { "LABEL.RELATED_PERSON_ID", "LABEL.REF_ID", "LABEL.ACTIVE" });
				}

				if (smartAppException.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(smartAppException);
				}
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion
	}
}

