using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface IPurchaseLineRepo
    {
        #region DropDown
        IEnumerable<SelectItem<PurchaseLineItemView>> GetDropDownItem(SearchParameter search);
        #endregion DropDown
        SearchResult<PurchaseLineListView> GetListvw(SearchParameter search);
        SearchResult<InquiryRollbillPurchaseLineListView> GetInquiryRollBillListvw(SearchParameter search);
        PurchaseLineItemView GetByIdvw(Guid id);
        InquiryRollbillPurchaseLineItemView GetInquiryRollBillPurchaseLineByIdvw(Guid id);
        PurchaseLine Find(params object[] keyValues);
        PurchaseLine GetPurchaseLineByIdNoTracking(Guid guid);
        List<PurchaseLine> GetPurchaseLineByIdNoTracking(IEnumerable<Guid> purchaseLineGuids);
        PurchaseLine CreatePurchaseLine(PurchaseLine purchaseLine);
        void CreatePurchaseLineVoid(PurchaseLine purchaseLine);
        PurchaseLine UpdatePurchaseLine(PurchaseLine dbPurchaseLine, PurchaseLine inputPurchaseLine, List<string> skipUpdateFields = null);
        void UpdatePurchaseLineVoid(PurchaseLine dbPurchaseLine, PurchaseLine inputPurchaseLine, List<string> skipUpdateFields = null);
        void Remove(PurchaseLine item);
        #region function
        PurchaseLine GetPurchaseLineByIdNoTrackingByAccessLevel(Guid guid);
        IQueryable<PurchaseLine> GetPurchaseLineByBuyerInvoiceTableNoTracking(Guid guid);
        PurchaseLine GetPurchaseLineByBuyerInvoiceTableNoTracking1(Guid guid);
        IEnumerable<PurchaseLine> GetPurchaseLineByPurchaseTableNoTracking(Guid purchaseTableGUID);
        IEnumerable<PurchaseLine> GetPurchaseLineByCompanyIdNoTracking(Guid guid);
        PurchaseLine GetPurchaseLineByMaxPurchaseDate(Guid buyerInvoiceTableGUID);
        #region Inquiry	Purchase line outstanding
        SearchResult<InquiryPurchaseLineOutstandingListView> GetInquiryPurchaseLineOutstandingListvw(SearchParameter search);
        InquiryPurchaseLineOutstandingItemView GetInquiryPurchaseLineOutstandingByIdvw(Guid id);
        PurchaseLine GetPurchaseLineByPurchaseInvoiceTableNoTracking(Guid guid);
        #endregion
        #region addtional purchase
        bool AnyNotRejectedOrCancelledAdditionalPurchase(Guid purchaseLineGUID);
        #endregion
        #endregion
        void ValidateAdd(PurchaseLine item);
        SearchResult<ListPurchaseLineOutstandingListView> GetListPurchaseLineOutstandingListvw(SearchParameter search);
        IEnumerable<PurchaseLine> GetPurchaseLineForMaxRollBillNoTracking(Guid guid);
        List<PurchaseLine> GetPurchaseLineByInvoice(Guid guid);
        IEnumerable<PurchaseLine> GetPurchaseLineByBuyerCreditLimitBuyer(Guid purchaseTableGUID, int productType);
        void ValidateAdd(IEnumerable<PurchaseLine> items);
        IEnumerable<PurchaseLine> GetPurchaseLineByValidatePurchaseAssignmentAmount(Guid purchaseTableGUID);
    }
    public class PurchaseLineRepo : CompanyBaseRepository<PurchaseLine>, IPurchaseLineRepo
    {
        public PurchaseLineRepo(SmartAppDbContext context) : base(context) { }
        public PurchaseLineRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public PurchaseLine GetPurchaseLineByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.PurchaseLineGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<PurchaseLine> GetPurchaseLineByIdNoTracking(IEnumerable<Guid> purchaseLineGuids)
        {
            try
            {
                var result = Entity.Where(w => purchaseLineGuids.Contains(w.PurchaseLineGUID))
                                    .AsNoTracking()
                                    .ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        private IQueryable<PurchaseLineItemViewMap> GetDropDownQuery()
        {
            return (from purchaseLine in Entity
                    join purchaseTable in db.Set<PurchaseTable>()
                    on purchaseLine.PurchaseTableGUID equals purchaseTable.PurchaseTableGUID
                    select new PurchaseLineItemViewMap
                    {
                        CompanyGUID = purchaseLine.CompanyGUID,
                        Owner = purchaseLine.Owner,
                        OwnerBusinessUnitGUID = purchaseLine.OwnerBusinessUnitGUID,
                        PurchaseLineGUID = purchaseLine.PurchaseLineGUID,
                        PurchaseTableGUID = purchaseLine.PurchaseTableGUID,
                        LineNum = purchaseLine.LineNum,
                        PurchaseTable_PurchaseId = purchaseTable.PurchaseId
                    });
        }
        public IEnumerable<SelectItem<PurchaseLineItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<PurchaseLine>(search, SysParm.CompanyGUID);
                var purchaseLine = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
                    .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                    .Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
                    .ToMaps<PurchaseLineItemViewMap, PurchaseLineItemView>().ToDropDownItem(search.ExcludeRowData);
                return purchaseLine;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #region GetListvw
        private IQueryable<PurchaseLineListViewMap> GetListQuery()
        {
            return (from purchaseLine in Entity
                    join buyerTable in db.Set<BuyerTable>()
                    on purchaseLine.BuyerTableGUID equals buyerTable.BuyerTableGUID

                    join buyerInvoiceTable in db.Set<BuyerInvoiceTable>()
                    on purchaseLine.BuyerInvoiceTableGUID equals buyerInvoiceTable.BuyerInvoiceTableGUID

                    join methodOfPayment in db.Set<MethodOfPayment>()
                    on purchaseLine.MethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID into ljMethodOfPayment
                    from methodOfPayment in ljMethodOfPayment.DefaultIfEmpty()

                    select new PurchaseLineListViewMap
                    {
                        CompanyGUID = purchaseLine.CompanyGUID,
                        Owner = purchaseLine.Owner,
                        OwnerBusinessUnitGUID = purchaseLine.OwnerBusinessUnitGUID,
                        PurchaseLineGUID = purchaseLine.PurchaseLineGUID,
                        PurchaseTableGUID = purchaseLine.PurchaseTableGUID,
                        LineNum = purchaseLine.LineNum,
                        BuyerTableGUID = purchaseLine.BuyerTableGUID,
                        BuyerInvoiceTableGUID = purchaseLine.BuyerInvoiceTableGUID,
                        DueDate = purchaseLine.DueDate,
                        BillingDate = purchaseLine.BillingDate,
                        CollectionDate = purchaseLine.CollectionDate,
                        BuyerInvoiceAmount = purchaseLine.BuyerInvoiceAmount,
                        PurchaseAmount = purchaseLine.PurchaseAmount,
                        MethodOfPaymentGUID = purchaseLine.MethodOfPaymentGUID,
                        BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
                        MethodOfPayment_Values = (methodOfPayment != null) ? SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName) : null,
                        BuyerInvoiceTable_Values = SmartAppUtil.GetDropDownLabel(buyerInvoiceTable.BuyerInvoiceId),
                        BuyerInvoiceTable_BuyerInvoiceId = buyerInvoiceTable.BuyerInvoiceId,
                        MethodOfPayment_MethodOfPaymentId = (methodOfPayment != null) ? methodOfPayment.MethodOfPaymentId : null,
                        BuyerTable_BuyerId = buyerTable.BuyerId,
                    });
        }
        private IQueryable<InquiryRollbillPurchaseLineListViewMap> GetInquiryRollBillPurchaseLineListQuery()
        {
            return (from purchaseLine in Entity
                    join purchaseTable in db.Set<PurchaseTable>()
                    on purchaseLine.PurchaseTableGUID equals purchaseTable.PurchaseTableGUID
                    join documentStatus in db.Set<DocumentStatus>()
                    on purchaseTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID

                    select new InquiryRollbillPurchaseLineListViewMap
                    {
                        CompanyGUID = purchaseLine.CompanyGUID,
                        Owner = purchaseLine.Owner,
                        OwnerBusinessUnitGUID = purchaseLine.OwnerBusinessUnitGUID,
                        PurchaseLineGUID = purchaseLine.PurchaseLineGUID,
                        LineNum = purchaseLine.LineNum,
                        DueDate = purchaseLine.DueDate,
                        NumberOfRollbill = purchaseLine.NumberOfRollbill,
                        InterestDate = purchaseLine.InterestDate,
                        OutstandingBuyerInvoiceAmount = purchaseLine.OutstandingBuyerInvoiceAmount,
                        LinePurchaseAmount = purchaseLine.LinePurchaseAmount,
                        PurchaseTable_PurchaseDate = purchaseTable.PurchaseDate,
                        PurchaseTable_PurchaseId = purchaseTable.PurchaseId,
                        DocumentStatus_Description = documentStatus.Description,
                    });
        }
        private IQueryable<InquiryRollbillPurchaseLineItemViewMap> GetInquiryRollBillPurchaseLineItemQuery()
        {
            return (from purchaseLine in Entity

                    join company in db.Set<Company>()
                    on purchaseLine.CompanyGUID equals company.CompanyGUID

                    join purchaseTable in db.Set<PurchaseTable>()
                    on purchaseLine.PurchaseTableGUID equals purchaseTable.PurchaseTableGUID
                    join documentStatus in db.Set<DocumentStatus>()
                    on purchaseTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID

                    join ownerBU in db.Set<BusinessUnit>()
                                    on purchaseLine.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljPurchaseLineOwnerBU
                    from ownerBU in ljPurchaseLineOwnerBU.DefaultIfEmpty()

                    select new InquiryRollbillPurchaseLineItemViewMap
                    {
                        CompanyGUID = purchaseLine.CompanyGUID,
                        Owner = purchaseLine.Owner,
                        OwnerBusinessUnitGUID = purchaseLine.OwnerBusinessUnitGUID,
                        PurchaseLineGUID = purchaseLine.PurchaseLineGUID,
                        LineNum = purchaseLine.LineNum,
                        DueDate = purchaseLine.DueDate,
                        NumberOfRollbill = purchaseLine.NumberOfRollbill,
                        InterestDate = purchaseLine.InterestDate,
                        OutstandingBuyerInvoiceAmount = purchaseLine.OutstandingBuyerInvoiceAmount,
                        LinePurchaseAmount = purchaseLine.LinePurchaseAmount,
                        PurchaseTable_PurchaseDate = purchaseTable.PurchaseDate,
                        PurchaseTable_PurchaseId = purchaseTable.PurchaseId,
                        DocumentStatus_Description = documentStatus.Description,
                        CompanyId = company.CompanyId,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                    });
        }
        public SearchResult<PurchaseLineListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<PurchaseLineListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<PurchaseLine>(search, SysParm.CompanyGUID);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();

                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<PurchaseLineListViewMap, PurchaseLineListView>();
                result = list.SetSearchResult<PurchaseLineListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public SearchResult<InquiryRollbillPurchaseLineListView> GetInquiryRollBillListvw(SearchParameter search)
        {
            var result = new SearchResult<InquiryRollbillPurchaseLineListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<PurchaseLine>(search, SysParm.CompanyGUID);
                var total = GetInquiryRollBillPurchaseLineListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();

                var list = GetInquiryRollBillPurchaseLineListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<InquiryRollbillPurchaseLineListViewMap, InquiryRollbillPurchaseLineListView>();
                result = list.SetSearchResult<InquiryRollbillPurchaseLineListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<PurchaseLineItemViewMap> GetItemQuery()
        {
            return (from purchaseLine in Entity
                    join company in db.Set<Company>()
                    on purchaseLine.CompanyGUID equals company.CompanyGUID
                    join purchaseTable in db.Set<PurchaseTable>()
                    on purchaseLine.PurchaseTableGUID equals purchaseTable.PurchaseTableGUID
                    join documentStatus in db.Set<DocumentStatus>()
                    on purchaseTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
                    join creditAppLine in db.Set<CreditAppLine>()
                    on purchaseLine.CreditAppLineGUID equals creditAppLine.CreditAppLineGUID
                    join creditAppTable in db.Set<CreditAppTable>()
                    on creditAppLine.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID
                    join creditLimitType in db.Set<CreditLimitType>()
                    on creditAppTable.CreditLimitTypeGUID equals creditLimitType.CreditLimitTypeGUID

                    join ownerBU in db.Set<BusinessUnit>()
                    on purchaseLine.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljPurchaseLineOwnerBU
                    from ownerBU in ljPurchaseLineOwnerBU.DefaultIfEmpty()

                    join methodOfPayment in db.Set<MethodOfPayment>()
                    on purchaseLine.MethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID into ljMethodOfPayment
                    from methodOfPayment in ljMethodOfPayment.DefaultIfEmpty()

                    join invoiceTable in db.Set<InvoiceTable>()
                    on purchaseLine.PurchaseLineInvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljInvoiceTable
                    from invoiceTable in ljInvoiceTable.DefaultIfEmpty()

                    join refPurchaseLine in db.Set<PurchaseLine>()
                    on purchaseLine.RefPurchaseLineGUID equals refPurchaseLine.PurchaseLineGUID into ljRefPurchaseLine
                    from refPurchaseLine in ljRefPurchaseLine.DefaultIfEmpty()

                    join chequeTable in db.Set<ChequeTable>()
                    on purchaseLine.CustomerPDCTableGUID equals chequeTable.ChequeTableGUID into ljChequeTable
                    from chequeTable in ljChequeTable.DefaultIfEmpty()

                    join assignmentMethod in db.Set<AssignmentMethod>()
                    on creditAppLine.AssignmentMethodGUID equals assignmentMethod.AssignmentMethodGUID into ljAssignmentMethod
                    from assignmentMethod in ljAssignmentMethod.DefaultIfEmpty()
                    select new PurchaseLineItemViewMap
                    {
                        CompanyGUID = purchaseLine.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = purchaseLine.Owner,
                        OwnerBusinessUnitGUID = purchaseLine.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        CreatedBy = purchaseLine.CreatedBy,
                        CreatedDateTime = purchaseLine.CreatedDateTime,
                        ModifiedBy = purchaseLine.ModifiedBy,
                        ModifiedDateTime = purchaseLine.ModifiedDateTime,
                        PurchaseLineGUID = purchaseLine.PurchaseLineGUID,
                        AssignmentAgreementTableGUID = purchaseLine.AssignmentAgreementTableGUID,
                        AssignmentAmount = purchaseLine.AssignmentAmount,
                        BillingDate = purchaseLine.BillingDate,
                        BillingFeeAmount = purchaseLine.BillingFeeAmount,
                        BuyerAgreementTableGUID = purchaseLine.BuyerAgreementTableGUID,
                        BuyerInvoiceAmount = purchaseLine.BuyerInvoiceAmount,
                        BuyerInvoiceTableGUID = purchaseLine.BuyerInvoiceTableGUID,
                        BuyerPDCTableGUID = purchaseLine.BuyerPDCTableGUID,
                        BuyerTableGUID = purchaseLine.BuyerTableGUID,
                        ClosedForAdditionalPurchase = purchaseLine.ClosedForAdditionalPurchase,
                        CollectionDate = purchaseLine.CollectionDate,
                        CreditAppLineGUID = purchaseLine.CreditAppLineGUID,
                        CustomerPDCTableGUID = purchaseLine.CustomerPDCTableGUID,
                        Dimension1GUID = purchaseLine.Dimension1GUID,
                        Dimension2GUID = purchaseLine.Dimension2GUID,
                        Dimension3GUID = purchaseLine.Dimension3GUID,
                        Dimension4GUID = purchaseLine.Dimension4GUID,
                        Dimension5GUID = purchaseLine.Dimension5GUID,
                        DueDate = purchaseLine.DueDate,
                        InterestAmount = purchaseLine.InterestAmount,
                        InterestDate = purchaseLine.InterestDate,
                        InterestDay = purchaseLine.InterestDay,
                        InterestRefundAmount = purchaseLine.InterestRefundAmount,
                        LineNum = purchaseLine.LineNum,
                        LinePurchaseAmount = purchaseLine.LinePurchaseAmount,
                        LinePurchasePct = purchaseLine.LinePurchasePct,
                        MethodOfPaymentGUID = purchaseLine.MethodOfPaymentGUID,
                        NetPurchaseAmount = purchaseLine.NetPurchaseAmount,
                        NumberOfRollbill = purchaseLine.NumberOfRollbill,
                        OriginalInterestDate = purchaseLine.OriginalInterestDate,
                        OriginalPurchaseLineGUID = purchaseLine.OriginalPurchaseLineGUID,
                        OutstandingBuyerInvoiceAmount = purchaseLine.OutstandingBuyerInvoiceAmount,
                        PurchaseAmount = purchaseLine.PurchaseAmount,
                        PurchaseFeeAmount = purchaseLine.PurchaseFeeAmount,
                        PurchaseFeeCalculateBase = purchaseLine.PurchaseFeeCalculateBase,
                        PurchaseFeePct = purchaseLine.PurchaseFeePct,
                        PurchaseLineInvoiceTableGUID = purchaseLine.PurchaseLineInvoiceTableGUID,
                        PurchasePct = purchaseLine.PurchasePct,
                        PurchaseTableGUID = purchaseLine.PurchaseTableGUID,
                        ReceiptFeeAmount = purchaseLine.ReceiptFeeAmount,
                        RefPurchaseLineGUID = purchaseLine.RefPurchaseLineGUID,
                        ReserveAmount = purchaseLine.ReserveAmount,
                        RetentionAmount = purchaseLine.RetentionAmount,
                        RollbillInterestAmount = purchaseLine.RollbillInterestAmount,
                        RollbillInterestDay = purchaseLine.RollbillInterestDay,
                        RollbillInterestPct = purchaseLine.RollbillInterestPct,
                        RollbillPurchaseLineGUID = purchaseLine.RollbillPurchaseLineGUID,
                        SettleBillingFeeAmount = purchaseLine.SettleBillingFeeAmount,
                        SettleInterestAmount = purchaseLine.SettleInterestAmount,
                        SettlePurchaseFeeAmount = purchaseLine.SettlePurchaseFeeAmount,
                        SettleReceiptFeeAmount = purchaseLine.SettleReceiptFeeAmount,
                        SettleRollBillInterestAmount = purchaseLine.SettleRollBillInterestAmount,
                        PurchaseTable_CustomerTableGUID = purchaseTable.CustomerTableGUID,
                        PurchaseTable_CreditAppTableGUID = purchaseTable.CreditAppTableGUID,
                        PurchaseTable_PurchaseDate = purchaseTable.PurchaseDate,
                        PurchaseTable_Values = SmartAppUtil.GetDropDownLabel(purchaseTable.PurchaseId, purchaseTable.Description),
                        PurchaseTable_PurchaseId = purchaseTable.PurchaseId,
                        DocumentStatus_StatusId = documentStatus.StatusId,
                        MethodOfPayment_Values = SmartAppUtil.GetDropDownLabel(methodOfPayment.MethodOfPaymentId, methodOfPayment.Description),
                        CreditAppLine_Values = SmartAppUtil.GetDropDownLabel(creditAppLine.LineNum.ToString(), creditAppLine.ApprovedCreditLimitLine.ToString()),
                        PurchaseLineInvoiceTable_Values = (invoiceTable != null) ? SmartAppUtil.GetDropDownLabel(invoiceTable.InvoiceId, invoiceTable.DueDate.DateToString()) : null,
                        CreditAppLine_MaxPurchasePct = creditAppLine.MaxPurchasePct,
                        CreditLitmitType_ValidateBuyerAgreement = creditLimitType.ValidateBuyerAgreement,
                        RefPurchaseLine_Values = (refPurchaseLine != null) ? SmartAppUtil.GetDropDownLabel(refPurchaseLine.LineNum.ToString()) : null,
                        PurchaseTable_AdditionalPurchase = purchaseTable.AdditionalPurchase,
                        RollBill = purchaseTable.Rollbill,
                        RetentionCalculateBase = purchaseTable.RetentionCalculateBase,
                        RetentionPct = purchaseTable.RetentionPct,
                        CustomerPDCDate = (chequeTable != null) ? (DateTime?)chequeTable.ChequeDate : null,

                        AssignmentMethod_ValidateAssignmentBalance = (assignmentMethod != null) ? assignmentMethod.ValidateAssignmentBalance : false,
                        AssignmentMethod_AssignmentMethodGUID = (assignmentMethod != null) ? assignmentMethod.AssignmentMethodGUID.GuidNullToString() : null,

                        RowVersion = purchaseLine.RowVersion,
                    });
        }
        public PurchaseLineItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<PurchaseLineItemViewMap, PurchaseLineItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public InquiryRollbillPurchaseLineItemView GetInquiryRollBillPurchaseLineByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetInquiryRollBillPurchaseLineItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<InquiryRollbillPurchaseLineItemViewMap, InquiryRollbillPurchaseLineItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public PurchaseLine CreatePurchaseLine(PurchaseLine purchaseLine)
        {
            try
            {
                purchaseLine.PurchaseLineGUID = Guid.NewGuid();
                base.Add(purchaseLine);
                return purchaseLine;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreatePurchaseLineVoid(PurchaseLine purchaseLine)
        {
            try
            {
                CreatePurchaseLine(purchaseLine);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public PurchaseLine UpdatePurchaseLine(PurchaseLine dbPurchaseLine, PurchaseLine inputPurchaseLine, List<string> skipUpdateFields = null)
        {
            try
            {
                dbPurchaseLine = dbPurchaseLine.MapUpdateValues<PurchaseLine>(inputPurchaseLine);
                base.Update(dbPurchaseLine);
                return dbPurchaseLine;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdatePurchaseLineVoid(PurchaseLine dbPurchaseLine, PurchaseLine inputPurchaseLine, List<string> skipUpdateFields = null)
        {
            try
            {
                dbPurchaseLine = dbPurchaseLine.MapUpdateValues<PurchaseLine>(inputPurchaseLine, skipUpdateFields);
                base.Update(dbPurchaseLine);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public override void ValidateAdd(PurchaseLine item)
        {
            ValidateShared(item);
            IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
            PurchaseTable purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTracking(item.PurchaseTableGUID);
            if (purchaseTable != null)
            {
                if (purchaseTable.Rollbill == false)
                {
                    ValidatePurchase(item);
                }
            }
            base.ValidateAdd(item);
        }
        public override void ValidateUpdate(PurchaseLine item)
        {
            ValidateShared(item);
            IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
            PurchaseTable purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTracking(item.PurchaseTableGUID);
            if (purchaseTable.Rollbill == false)
            {
                ValidatePurchase(item);
            }
            base.ValidateUpdate(item);
        }

        public void ValidateShared(PurchaseLine item)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");

                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);

                CreditAppLine creditAppLine = creditAppLineRepo.GetCreditAppLineByIdNoTracking(item.CreditAppLineGUID);

                PurchaseLine purchaseLine = GetPurchaseLineByMaxPurchaseDate(item.BuyerInvoiceTableGUID);
                PurchaseTable purchaseTableCurrent = purchaseTableRepo.GetPurchaseTableByIdNoTracking(item.PurchaseTableGUID);

                if (purchaseLine != null)
                {
                    PurchaseTable purchaseTableMaxPurchaseDate = purchaseTableRepo.GetPurchaseTableByIdNoTracking(purchaseLine.PurchaseTableGUID);

                    if (purchaseTableCurrent != null && purchaseTableMaxPurchaseDate != null)
                    {
                        if (purchaseTableCurrent.PurchaseDate < purchaseTableMaxPurchaseDate.PurchaseDate)
                        {
                            ex.AddData("ERROR.90079", new string[] { "LABEL.PURCHASE_DATE","LABEL.PURCHASE_DATE", "LABEL.BUYER_INVOICE_ID" });
                        }
                    }
                }

                if (purchaseTableCurrent != null)
                {
                    if (purchaseTableCurrent.PurchaseDate >= creditAppLine.ExpiryDate)
                    {
                        ex.AddData("ERROR.90153", new string[] { "LABEL.CREDIT_APPLICATION_LINE", "LAEL.PURCHASE_DATE" });
                    }
                }

                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public void ValidatePurchase(PurchaseLine item)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
                IBuyerInvoiceTableRepo buyerInvoiceTableRepo = new BuyerInvoiceTableRepo(db);

                BuyerInvoiceTable buyerInvoiceTable = buyerInvoiceTableRepo.GetBuyerInvoiceTableByIdNoTracking(item.BuyerInvoiceTableGUID);
                InvoiceTable invoiceTable = invoiceTableRepo.GetInvoiceTableByBuyerInvoiceTableNoTracking((Guid?)buyerInvoiceTable.BuyerInvoiceTableGUID).FirstOrDefault();

                if (item.RefPurchaseLineGUID == null && item.PurchaseLineGUID != Guid.Empty)
                {
                    // condition NO.1
                    if (invoiceTable != null)
                    {
                        ICustTransRepo custVisitingTransRepo = new CustTransRepo(db);
                        CustTrans custTrans = custVisitingTransRepo.GetCustTransByInvoiceTableNoTracking(invoiceTable.InvoiceTableGUID);

                        if (custTrans.CustTransStatus == (int)CustTransStatus.Open)
                        {
                            ex.AddData("ERROR.90116", new string[] { invoiceTable.InvoiceId, buyerInvoiceTable.BuyerInvoiceId });
                        }
                    }
                }

                // condition NO.2
                ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                CreditAppLine creditAppLine = creditAppLineRepo.GetCreditAppLineByIdNoTracking(item.CreditAppLineGUID);
                if (item.PurchasePct > creditAppLine.MaxPurchasePct)
                {
                    ex.AddData("ERROR.90031", new string[] { "LABEL.PURCHASE_PCT", "LABEL.MAXIMUM_PURCHASE_PCT" });
                }

                // condition NO.3
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                int postedPurchaseTableStatus = Convert.ToInt32(documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(Convert.ToInt32(PurchaseStatus.Posted).ToString()).StatusId);

                PurchaseTable purchaseTableResult = (from purchaseLine in Entity
                                                     join purchaseTable in db.Set<PurchaseTable>()
                                                     on purchaseLine.PurchaseTableGUID equals purchaseTable.PurchaseTableGUID
                                                     join documentstatus in db.Set<DocumentStatus>()
                                                     on purchaseTable.DocumentStatusGUID equals documentstatus.DocumentStatusGUID
                                                     where Convert.ToInt32(documentstatus.StatusId) < postedPurchaseTableStatus
                                                     && purchaseLine.BuyerInvoiceTableGUID == item.BuyerInvoiceTableGUID
                                                     && purchaseLine.CompanyGUID == item.CompanyGUID
                                                     && purchaseLine.PurchaseLineGUID != item.PurchaseLineGUID
                                                     select purchaseTable
                                       ).FirstOrDefault();
                if (purchaseTableResult != null)
                {
                    ex.AddData("ERROR.90078", new string[] { buyerInvoiceTable.BuyerInvoiceId, "LABEL.BUYER_INVOICE" });
                }

                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion Create, Update, Delete
        #region function
        public PurchaseLine GetPurchaseLineByIdNoTrackingByAccessLevel(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.PurchaseLineGUID == guid)
                    .FilterByAccessLevel(SysParm.AccessLevel)
                    .AsNoTracking()
                    .FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IQueryable<PurchaseLine> GetPurchaseLineByBuyerInvoiceTableNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.BuyerInvoiceTableGUID == guid).AsNoTracking();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public PurchaseLine GetPurchaseLineByBuyerInvoiceTableNoTracking1(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.BuyerInvoiceTableGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public IEnumerable<PurchaseLine> GetPurchaseLineByPurchaseTableNoTracking(Guid purchaseTableGUID)
        {
            try
            {
                var result = Entity.Where(item => item.PurchaseTableGUID == purchaseTableGUID).AsNoTracking();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<PurchaseLine> GetPurchaseLineByCompanyIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.CompanyGUID == guid).AsNoTracking();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public PurchaseLine GetPurchaseLineByMaxPurchaseDate(Guid buyerInvoiceTableGUID)
        {
            try
            {
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                Guid postedPurchaseTableStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(Convert.ToInt32(PurchaseStatus.Posted).ToString()).DocumentStatusGUID;
                PurchaseLine purchaseLine = (from purchaseLines in db.Set<PurchaseLine>()
                                             join purchaseTable in db.Set<PurchaseTable>()
                                             on purchaseLines.PurchaseTableGUID equals purchaseTable.PurchaseTableGUID
                                             where purchaseTable.DocumentStatusGUID == postedPurchaseTableStatus && purchaseLines.ClosedForAdditionalPurchase == false
                                             && purchaseLines.BuyerInvoiceTableGUID == buyerInvoiceTableGUID
                                             orderby purchaseTable.PurchaseDate descending
                                             select purchaseLines
                                                   ).AsNoTracking().FirstOrDefault();
                return purchaseLine;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public PurchaseLine GetPurchaseLineByPurchaseInvoiceTableNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.PurchaseLineInvoiceTableGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<PurchaseLine> GetPurchaseLineForMaxRollBillNoTracking(Guid guid)
        {
            try
            {
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                Guid postStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(Convert.ToInt32(PurchaseStatus.Posted).ToString()).DocumentStatusGUID;
                return (from purchaseLine in Entity
                        join purchaseTable in db.Set<PurchaseTable>()
                        on purchaseLine.PurchaseTableGUID equals purchaseTable.PurchaseTableGUID
                        where purchaseLine.OriginalPurchaseLineGUID == guid && purchaseTable.DocumentStatusGUID == postStatus
                        select purchaseLine);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region GetPurchaseLineOutstandingListvw
        private IQueryable<InquiryPurchaseLineOutstandingListViewMap> GetInquiryPurchaseLineOutstandingListQuery()
        {
            return (from purchaseLine in Entity
                    join purchaseTable in db.Set<PurchaseTable>()
                    on purchaseLine.PurchaseTableGUID equals purchaseTable.PurchaseTableGUID
                    join customerTable in db.Set<CustomerTable>()
                    on purchaseTable.CustomerTableGUID equals customerTable.CustomerTableGUID
                    join buyerTable in db.Set<BuyerTable>()
                    on purchaseLine.BuyerTableGUID equals buyerTable.BuyerTableGUID
                    join methodOfPayment in db.Set<MethodOfPayment>()
                    on purchaseLine.MethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID into ljPurchaseLineMethodOfPayment
                    from methodOfPayment in ljPurchaseLineMethodOfPayment.DefaultIfEmpty()
                    join invoiceTable in db.Set<InvoiceTable>()
                    on purchaseLine.PurchaseLineInvoiceTableGUID equals invoiceTable.InvoiceTableGUID
                    join custTrans in db.Set<CustTrans>()
                    on invoiceTable.InvoiceTableGUID equals custTrans.InvoiceTableGUID
                    where custTrans.CustTransStatus == (int)CustTransStatus.Open
                    select new InquiryPurchaseLineOutstandingListViewMap
                    {
                        CompanyGUID = purchaseLine.CompanyGUID,
                        Owner = purchaseLine.Owner,
                        OwnerBusinessUnitGUID = purchaseLine.OwnerBusinessUnitGUID,
                        PurchaseLineGUID = purchaseLine.PurchaseLineGUID,
                        PurchaseTable_Values = SmartAppUtil.GetDropDownLabel(purchaseTable.PurchaseId, purchaseTable.Description),
                        PurchaseTableGUID = purchaseLine.PurchaseTableGUID,
                        PurchaseTable_PurchaseId = purchaseTable.PurchaseId,
                        CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                        CustomerTableGUID = purchaseTable.CustomerTableGUID,
                        CustomerTable_CustomerId = customerTable.CustomerId,
                        BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
                        BuyerTableGUID = purchaseLine.BuyerTableGUID,
                        BuyerTable_BuyerId = buyerTable.BuyerId,
                        InvoiceAmount = purchaseLine.BuyerInvoiceAmount,
                        PurchaseAmount = purchaseLine.PurchaseAmount,
                        Outstanding = (custTrans != null) ? custTrans.TransAmount - custTrans.SettleAmount : 0,
                        DueDate = purchaseLine.DueDate,
                        BillingDate = purchaseLine.BillingDate,
                        CollectionDate = purchaseLine.CollectionDate,
                        MethodOfPayment_Values = (methodOfPayment != null) ? SmartAppUtil.GetDropDownLabel(methodOfPayment.MethodOfPaymentId, methodOfPayment.Description) : null,
                        MethodOfPaymentGUID = purchaseLine.MethodOfPaymentGUID,
                        MethodOfPayment_MethodOfPaymentId = (methodOfPayment != null) ? methodOfPayment.MethodOfPaymentId : null,
                    });
        }
        public SearchResult<InquiryPurchaseLineOutstandingListView> GetInquiryPurchaseLineOutstandingListvw(SearchParameter search)
        {
            var result = new SearchResult<InquiryPurchaseLineOutstandingListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<PurchaseLine>(search, SysParm.CompanyGUID);
                var total = GetInquiryPurchaseLineOutstandingListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetInquiryPurchaseLineOutstandingListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<InquiryPurchaseLineOutstandingListViewMap, InquiryPurchaseLineOutstandingListView>();
                result = list.SetSearchResult<InquiryPurchaseLineOutstandingListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetPurchaseLineOutstandingListvw
        #region GetPurchaseLineOutstandingByIdvw
        private IQueryable<InquiryPurchaseLineOutstandingItemViewMap> GetInquiryPurchaseLineOutstandingItemQuery()
        {
            return (from purchaseLine in Entity
                    join company in db.Set<Company>()
                    on purchaseLine.CompanyGUID equals company.CompanyGUID
                    join ownerBU in db.Set<BusinessUnit>()
                    on purchaseLine.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljPurchaseLineOwnerBU
                    from ownerBU in ljPurchaseLineOwnerBU.DefaultIfEmpty()
                    join purchaseTable in db.Set<PurchaseTable>()
                    on purchaseLine.PurchaseTableGUID equals purchaseTable.PurchaseTableGUID
                    join customerTable in db.Set<CustomerTable>()
                    on purchaseTable.CustomerTableGUID equals customerTable.CustomerTableGUID
                    join buyerTable in db.Set<BuyerTable>()
                    on purchaseLine.BuyerTableGUID equals buyerTable.BuyerTableGUID
                    join buyerInvoiceTable in db.Set<BuyerInvoiceTable>()
                    on purchaseLine.BuyerInvoiceTableGUID equals buyerInvoiceTable.BuyerInvoiceTableGUID
                    join methodOfPayment in db.Set<MethodOfPayment>()
                    on purchaseLine.MethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID into ljPurchaseLineMethodOfPayment
                    from methodOfPayment in ljPurchaseLineMethodOfPayment.DefaultIfEmpty()
                    join invoiceTable in db.Set<InvoiceTable>()
                    on purchaseLine.PurchaseLineInvoiceTableGUID equals invoiceTable.InvoiceTableGUID
                    join custTrans in db.Set<CustTrans>()
                    on invoiceTable.InvoiceTableGUID equals custTrans.InvoiceTableGUID
                    join chequeTable in db.Set<ChequeTable>()
                    on purchaseLine.CustomerPDCTableGUID equals chequeTable.ChequeTableGUID into ljPurchaseLineChequeTable
                    from chequeTable in ljPurchaseLineChequeTable.DefaultIfEmpty()
                    join creditAppTable in db.Set<CreditAppTable>()
                    on purchaseTable.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID
                    join creditAppTableBankAccountControl_CustBank in db.Set<CustBank>()
                    on creditAppTable.BankAccountControlGUID equals creditAppTableBankAccountControl_CustBank.CustBankGUID into ljCreditAppTableBankAccountControlCustBank
                    from creditAppTableBankAccountControl_CustBank in ljCreditAppTableBankAccountControlCustBank.DefaultIfEmpty()
                    join creditAppTablePDCBank_CustBank in db.Set<CustBank>()
                    on creditAppTable.BankAccountControlGUID equals creditAppTablePDCBank_CustBank.CustBankGUID into ljCreditAppTablePDCBankCustBank
                    from creditAppTablePDCBank_CustBank in ljCreditAppTablePDCBankCustBank.DefaultIfEmpty()
                    join creditAppLine in db.Set<CreditAppLine>()
                    on purchaseLine.CreditAppLineGUID equals creditAppLine.CreditAppLineGUID
                    where custTrans.CustTransStatus == (int)CustTransStatus.Open
                    select new InquiryPurchaseLineOutstandingItemViewMap
                    {
                        CompanyGUID = purchaseLine.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = purchaseLine.Owner,
                        OwnerBusinessUnitGUID = purchaseLine.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        CreatedBy = purchaseLine.CreatedBy,
                        CreatedDateTime = purchaseLine.CreatedDateTime,
                        ModifiedBy = purchaseLine.ModifiedBy,
                        ModifiedDateTime = purchaseLine.ModifiedDateTime,
                        PurchaseLineGUID = purchaseLine.PurchaseLineGUID,
                        PurchaseTable_Values = SmartAppUtil.GetDropDownLabel(purchaseTable.PurchaseId, purchaseTable.Description),
                        CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                        BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
                        BuyerInvoiceTable_BuyerInvoiceId = buyerInvoiceTable.BuyerInvoiceId,
                        InvoiceAmount = purchaseLine.BuyerInvoiceAmount,
                        PurchaseAmount = purchaseLine.PurchaseAmount,
                        Outstanding = (custTrans != null) ? custTrans.TransAmount - custTrans.SettleAmount : 0,
                        MethodOfPayment_Values = (methodOfPayment != null) ? SmartAppUtil.GetDropDownLabel(methodOfPayment.MethodOfPaymentId, methodOfPayment.Description) : null,
                        DueDate = purchaseLine.DueDate,
                        ChequeTable_ChequeDate = (chequeTable != null) ? chequeTable.ChequeDate : (DateTime?)null,
                        BillingDate = purchaseLine.BillingDate,
                        CollectionDate = purchaseLine.CollectionDate,
                        CreditAppTableBankAccountControl_BankAccountName = (creditAppTableBankAccountControl_CustBank != null) ? creditAppTableBankAccountControl_CustBank.BankAccountName : null,
                        CreditAppTablePDCBank_BankAccountName = (creditAppTablePDCBank_CustBank != null) ? creditAppTablePDCBank_CustBank.BankAccountName : null,
                        LineNum = purchaseLine.LineNum,
                        CreditAppLine_Values = SmartAppUtil.GetDropDownLabel(creditAppLine.LineNum.ToString(), creditAppLine.ApprovedCreditLimitLine.ToString()),
                        AssignmentAgreementTableGUID = purchaseLine.AssignmentAgreementTableGUID,
                        BuyerAgreementTableGUID = purchaseLine.BuyerAgreementTableGUID,
                        MethodOfPaymentGUID = purchaseLine.MethodOfPaymentGUID,
                    });
        }
        public InquiryPurchaseLineOutstandingItemView GetInquiryPurchaseLineOutstandingByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetInquiryPurchaseLineOutstandingItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<InquiryPurchaseLineOutstandingItemViewMap, InquiryPurchaseLineOutstandingItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetPurchaseLineOutstandingByIdvw
        #region Addtional purchase
        public bool AnyNotRejectedOrCancelledAdditionalPurchase(Guid purchaseLineGUID)
        {
            try
            {
                string rejectStatus = PurchaseStatus.Rejected.ToString();
                string cancelledStatus = PurchaseStatus.Cancelled.ToString();

                var result =
                    (from purchaseLine in Entity.Where(w => w.RefPurchaseLineGUID == purchaseLineGUID)
                     join purchaseTable in db.Set<PurchaseTable>().Where(w => w.AdditionalPurchase == true)
                     on purchaseLine.PurchaseTableGUID equals purchaseTable.PurchaseTableGUID
                     join documentStatus in db.Set<DocumentStatus>().Where(w => w.StatusId != rejectStatus && w.StatusId != cancelledStatus)
                     on purchaseTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
                     select purchaseLine).AsNoTracking().Any();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region listOutStandingPurchaseLine 
        private IQueryable<ListPurchaseLineOutstandingListViewMap> GetListPurchaseLineOutstandingListQuery(PurchaseTable inputPurchaseTable)
        {
            return (from purchaseLine in Entity
                    join purchaseTable in db.Set<PurchaseTable>()
                    on purchaseLine.PurchaseTableGUID equals purchaseTable.PurchaseTableGUID
                    join customerTable in db.Set<CustomerTable>()
                    on purchaseTable.CustomerTableGUID equals customerTable.CustomerTableGUID
                    join buyerTable in db.Set<BuyerTable>()
                    on purchaseLine.BuyerTableGUID equals buyerTable.BuyerTableGUID
                    join invoiceTable in db.Set<InvoiceTable>()
                    on purchaseLine.PurchaseLineInvoiceTableGUID equals invoiceTable.InvoiceTableGUID
                    join custTrans in db.Set<CustTrans>()
                    on invoiceTable.InvoiceTableGUID equals custTrans.InvoiceTableGUID
                    join methodOfPayment in db.Set<MethodOfPayment>()
                    on purchaseLine.MethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID into ljPurchaseLineMethodOfPayment
                    from methodOfPayment in ljPurchaseLineMethodOfPayment.DefaultIfEmpty()
                    where custTrans.CustTransStatus == (int)CustTransStatus.Open && 
                          purchaseTable.CreditAppTableGUID == inputPurchaseTable.CreditAppTableGUID &&
                          purchaseLine.ClosedForAdditionalPurchase == false
                    select new ListPurchaseLineOutstandingListViewMap
                    {
                        ListPurchaseLineOutstandingGUID = purchaseLine.PurchaseLineGUID,
                        PurchaseTable_Values = SmartAppUtil.GetDropDownLabel(purchaseTable.PurchaseId, purchaseTable.Description),
                        CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                        BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
                        DueDate = purchaseLine.DueDate,
                        BillingDate = purchaseLine.BillingDate,
                        CollectionDate = purchaseLine.CollectionDate,
                        InvoiceAmount = purchaseLine.BuyerInvoiceAmount,
                        PurchaseAmount = purchaseLine.PurchaseAmount,
                        Outstanding = (custTrans != null) ? custTrans.TransAmount - custTrans.SettleAmount : 0,
                        MethodOfPayment_Values = (methodOfPayment != null) ? SmartAppUtil.GetDropDownLabel(methodOfPayment.MethodOfPaymentId, methodOfPayment.Description) : null,
                        PurchaseTableGUID = purchaseTable.PurchaseTableGUID,
                        CustomerTableGUID = customerTable.CustomerTableGUID,
                        BuyerTableGUID = buyerTable.BuyerTableGUID,
                        MethodOfPaymentGUID = (methodOfPayment != null) ? (Guid?)methodOfPayment.MethodOfPaymentGUID : null,
                        PurchaseId = purchaseTable.PurchaseId,
                        CustomerId = customerTable.CustomerId,
                        BuyerId = buyerTable.BuyerId,
                        MethodOfPaymentId = methodOfPayment.MethodOfPaymentId,
                        InterestDate = purchaseLine.InterestDate,
                        ClosedForAdditionalPurchase = purchaseLine.ClosedForAdditionalPurchase
                    });
        }
        public SearchResult<ListPurchaseLineOutstandingListView> GetListPurchaseLineOutstandingListvw(SearchParameter search)
        {
            var result = new SearchResult<ListPurchaseLineOutstandingListView>();
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                PurchaseTable purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTracking(search.RefTable.StringToGuid());
                var predicate = base.GetFilterLevelPredicate<PurchaseLine>(search, SysParm.CompanyGUID);
                List<Guid?> purchaseLines = GetPurchaseLineByPurchaseTableNoTracking(purchaseTable.PurchaseTableGUID).Select(s => s.RollbillPurchaseLineGUID).ToList();
                var q = GetListPurchaseLineOutstandingListQuery(purchaseTable).Where(e => !purchaseLines.Contains(e.ListPurchaseLineOutstandingGUID));
                var total = q.Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = q.Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<ListPurchaseLineOutstandingListViewMap, ListPurchaseLineOutstandingListView>();
                result = list.SetSearchResult<ListPurchaseLineOutstandingListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        public List<PurchaseLine> GetPurchaseLineByInvoice(Guid guid)
        {
            try
            {
                return Entity.Where(w => w.PurchaseLineInvoiceTableGUID == guid).AsNoTracking().ToList();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<PurchaseLine> GetPurchaseLineByBuyerCreditLimitBuyer(Guid purchaseTableGUID, int productType)
        {
            try
            {
                var result = (from purchaseLine in Entity

                              join buyerCreditLimitByProduct in db.Set<BuyerCreditLimitByProduct>()
                              on purchaseLine.BuyerTableGUID equals buyerCreditLimitByProduct.BuyerTableGUID

                              where buyerCreditLimitByProduct.ProductType == productType && purchaseLine.PurchaseTableGUID == purchaseTableGUID
                              select purchaseLine)
                .AsNoTracking();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<PurchaseLine> GetPurchaseLineByValidatePurchaseAssignmentAmount(Guid purchaseTableGUID)
        {
            try
            {
                var result = (from purchaseLine in Entity

                              join creditAppLine in db.Set<CreditAppLine>()
                              on purchaseLine.CreditAppLineGUID equals creditAppLine.CreditAppLineGUID

                              join assignmentMethod in db.Set<AssignmentMethod>()
                              on creditAppLine.AssignmentMethodGUID equals assignmentMethod.AssignmentMethodGUID

                              where assignmentMethod.ValidateAssignmentBalance == true && purchaseLine.PurchaseTableGUID == purchaseTableGUID
                              select purchaseLine)
                .AsNoTracking();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Validate
        public override void ValidateAdd(IEnumerable<PurchaseLine> items)
        {
            base.ValidateAdd(items);
        }
        #endregion 
    }
}

