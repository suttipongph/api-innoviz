using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface IAssignmentAgreementLineRepo
    {
        #region DropDown
        IEnumerable<SelectItem<AssignmentAgreementLineItemView>> GetDropDownItem(SearchParameter search);
        #endregion DropDown
        SearchResult<AssignmentAgreementLineListView> GetListvw(SearchParameter search);
        AssignmentAgreementLineItemView GetByIdvw(Guid id);
        AssignmentAgreementLine Find(params object[] keyValues);
        AssignmentAgreementLine GetAssignmentAgreementLineByIdNoTracking(Guid guid);
        AssignmentAgreementLine CreateAssignmentAgreementLine(AssignmentAgreementLine assignmentAgreementLine);
        void CreateAssignmentAgreementLineVoid(AssignmentAgreementLine assignmentAgreementLine);
        AssignmentAgreementLine UpdateAssignmentAgreementLine(AssignmentAgreementLine dbAssignmentAgreementLine, AssignmentAgreementLine inputAssignmentAgreementLine, List<string> skipUpdateFields = null);
        void UpdateAssignmentAgreementLineVoid(AssignmentAgreementLine dbAssignmentAgreementLine, AssignmentAgreementLine inputAssignmentAgreementLine, List<string> skipUpdateFields = null);
        void Remove(AssignmentAgreementLine item);
        AssignmentAgreementLine GetAssignmentAgreementLineByIdNoTrackingByAccessLevel(Guid guid);
        #region function
        List<AssignmentAgreementLine> GetAssignmentAgreementLineByAssignmentAgreementTableGuidNoTracking(Guid guid);
        #endregion
        IEnumerable<AssignmentAgreementLine> GetAssignmentAgreementLineByAssignmentAgreementTableGuid(Guid guid);
        void ValidateAdd(IEnumerable<AssignmentAgreementLine> items);
    }
    public class AssignmentAgreementLineRepo : CompanyBaseRepository<AssignmentAgreementLine>, IAssignmentAgreementLineRepo
    {
        public AssignmentAgreementLineRepo(SmartAppDbContext context) : base(context) { }
        public AssignmentAgreementLineRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public AssignmentAgreementLine GetAssignmentAgreementLineByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.AssignmentAgreementLineGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        private IQueryable<AssignmentAgreementLineItemViewMap> GetDropDownQuery()
        {
            return (from assignmentAgreementLine in Entity
                    select new AssignmentAgreementLineItemViewMap
                    {
                        CompanyGUID = assignmentAgreementLine.CompanyGUID,
                        Owner = assignmentAgreementLine.Owner,
                        OwnerBusinessUnitGUID = assignmentAgreementLine.OwnerBusinessUnitGUID,
                        AssignmentAgreementLineGUID = assignmentAgreementLine.AssignmentAgreementLineGUID
                    });
        }
        public IEnumerable<SelectItem<AssignmentAgreementLineItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<AssignmentAgreementLine>(search, SysParm.CompanyGUID);
                var assignmentAgreementLine = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<AssignmentAgreementLineItemViewMap, AssignmentAgreementLineItemView>().ToDropDownItem(search.ExcludeRowData);


                return assignmentAgreementLine;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #region GetListvw
        private IQueryable<AssignmentAgreementLineListViewMap> GetListQuery()
        {
            return (from assignmentAgreementLine in Entity
                    join buyerAgreementTable in db.Set<BuyerAgreementTable>()
                    on assignmentAgreementLine.BuyerAgreementTableGUID equals buyerAgreementTable.BuyerAgreementTableGUID into ljAssignBuyer
                    from buyerAgreementTable in ljAssignBuyer.DefaultIfEmpty()
                    join company in db.Set<Company>()
                    on assignmentAgreementLine.CompanyGUID equals company.CompanyGUID
                    join ownerBU in db.Set<BusinessUnit>()
                    on assignmentAgreementLine.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljAssignmentAgreementTableOwnerBU
                    from ownerBU in ljAssignmentAgreementTableOwnerBU.DefaultIfEmpty()
                    select new AssignmentAgreementLineListViewMap
                    {
                        CompanyGUID = assignmentAgreementLine.CompanyGUID,
                        Owner = assignmentAgreementLine.Owner,
                        OwnerBusinessUnitGUID = assignmentAgreementLine.OwnerBusinessUnitGUID,
                        AssignmentAgreementLineGUID = assignmentAgreementLine.AssignmentAgreementLineGUID,
                        BuyerAgreementTableGUID = assignmentAgreementLine.BuyerAgreementTableGUID,
                        BuyerAgreementTable_Values = SmartAppUtil.GetDropDownLabel(buyerAgreementTable.BuyerAgreementId, buyerAgreementTable.Description),
                        ReferenceAgreementID = buyerAgreementTable.ReferenceAgreementID,
                        LineNum = assignmentAgreementLine.LineNum,
                        BuyerAgreementTable_BuyerAgreementId = buyerAgreementTable.BuyerAgreementId,
                        AssignmentAgreementTableGUID = assignmentAgreementLine.AssignmentAgreementTableGUID
                    });
        }
        public SearchResult<AssignmentAgreementLineListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<AssignmentAgreementLineListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<AssignmentAgreementLine>(search, SysParm.CompanyGUID);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<AssignmentAgreementLineListViewMap, AssignmentAgreementLineListView>();
                result = list.SetSearchResult<AssignmentAgreementLineListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<AssignmentAgreementLineItemViewMap> GetItemQuery()
        {
            return (from assignmentAgreementLine in Entity
                    join buyerAgreementTable in db.Set<BuyerAgreementTable>()
                    on assignmentAgreementLine.BuyerAgreementTableGUID equals buyerAgreementTable.BuyerAgreementTableGUID into ljAssignBuyer
                    from buyerAgreementTable in ljAssignBuyer.DefaultIfEmpty()
                    join buyerAgreementLine in db.Set<BuyerAgreementLine>()
                    on assignmentAgreementLine.BuyerAgreementTableGUID equals buyerAgreementLine.BuyerAgreementTableGUID into ljAssignBuyerLine
                    from buyerAgreementLine in ljAssignBuyerLine.DefaultIfEmpty()
                    join assignmentAgreementTable in db.Set<AssignmentAgreementTable>()
                    on assignmentAgreementLine.AssignmentAgreementTableGUID equals assignmentAgreementTable.AssignmentAgreementTableGUID
                    join documentStatus in db.Set<DocumentStatus>()
                    on assignmentAgreementTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljAssignDocStatus
                    from documentStatus in ljAssignDocStatus.DefaultIfEmpty()
                    join company in db.Set<Company>()
                    on assignmentAgreementLine.CompanyGUID equals company.CompanyGUID
                    join ownerBU in db.Set<BusinessUnit>()
                    on assignmentAgreementLine.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljAssignmentAgreementLineOwnerBU
                    from ownerBU in ljAssignmentAgreementLineOwnerBU.DefaultIfEmpty()
                    group new { assignmentAgreementLine, buyerAgreementTable, assignmentAgreementTable, company, ownerBU, buyerAgreementLine, documentStatus }
                    by new
                    {
                        assignmentAgreementLine.CompanyGUID,
                        company.CompanyId,
                        assignmentAgreementLine.Owner,
                        ownerBU.BusinessUnitId,
                        assignmentAgreementLine.CreatedBy,
                        assignmentAgreementLine.CreatedDateTime,
                        assignmentAgreementLine.ModifiedBy,
                        assignmentAgreementLine.ModifiedDateTime,
                        assignmentAgreementLine.AssignmentAgreementLineGUID,
                        assignmentAgreementTable.AssignmentAgreementTableGUID,
                        assignmentAgreementLine.BuyerAgreementTableGUID,
                        assignmentAgreementLine.LineNum,
                        assignmentAgreementLine.OwnerBusinessUnitGUID,
                        buyerAgreementTable.ReferenceAgreementID,
                        buyerAgreementTable.StartDate,
                        buyerAgreementTable.EndDate,
                        documentStatus.StatusId,
                        assignmentAgreementTable.InternalAssignmentAgreementId,
                        assignmentAgreementTable.AssignmentAgreementId,
                        assignmentAgreementTable.CustomerTableGUID,
                        assignmentAgreementTable.BuyerTableGUID,
                        assignmentAgreementTable.Description,
                        assignmentAgreementLine.RowVersion,
                    } into g
                    select new AssignmentAgreementLineItemViewMap
                    {
                        CompanyGUID = g.Key.CompanyGUID,
                        CompanyId = g.Key.CompanyId,
                        Owner = g.Key.Owner,
                        OwnerBusinessUnitGUID = g.Key.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = g.Key.BusinessUnitId,
                        CreatedBy = g.Key.CreatedBy,
                        CreatedDateTime = g.Key.CreatedDateTime,
                        ModifiedBy = g.Key.ModifiedBy,
                        ModifiedDateTime = g.Key.ModifiedDateTime,
                        AssignmentAgreementLineGUID = g.Key.AssignmentAgreementLineGUID,
                        AssignmentAgreementTableGUID = g.Key.AssignmentAgreementTableGUID,
                        BuyerAgreementTableGUID = g.Key.BuyerAgreementTableGUID,
                        LineNum = g.Key.LineNum,
                        ReferenceAgreementID = g.Key.ReferenceAgreementID,
                        StartDate = g.Key.StartDate,
                        EndDate = g.Key.EndDate,
                        BuyerAgreementAmount = g.Sum(s => s.buyerAgreementLine.Amount),
                        AssignmentAgreementTable_StatusId = g.Key.StatusId,
                        AssignmentAgreementTable_InternalAssignmentAgreementId = g.Key.InternalAssignmentAgreementId,
                        AssignmentAgreementTable_AssignmentAgreementId = g.Key.AssignmentAgreementId,
                        AssignmentAgreementTable_CustomerTableGUID = g.Key.CustomerTableGUID,
                        AssignmentAgreementTable_BuyerTableGUID = g.Key.BuyerTableGUID,
                        AssignmentAgreementTable_Description = g.Key.Description,
                    
                        RowVersion = g.Key.RowVersion,
                    });
        }
        public AssignmentAgreementLineItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<AssignmentAgreementLineItemViewMap, AssignmentAgreementLineItemView>();
                item.AssignmentAgreementTable_Values = SmartAppUtil.GetDropDownLabel(item.AssignmentAgreementTable_AssignmentAgreementId, item.AssignmentAgreementTable_Description);
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public AssignmentAgreementLine CreateAssignmentAgreementLine(AssignmentAgreementLine assignmentAgreementLine)
        {
            try
            {
                assignmentAgreementLine.AssignmentAgreementLineGUID = Guid.NewGuid();
                base.Add(assignmentAgreementLine);
                return assignmentAgreementLine;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateAssignmentAgreementLineVoid(AssignmentAgreementLine assignmentAgreementLine)
        {
            try
            {
                CreateAssignmentAgreementLine(assignmentAgreementLine);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AssignmentAgreementLine UpdateAssignmentAgreementLine(AssignmentAgreementLine dbAssignmentAgreementLine, AssignmentAgreementLine inputAssignmentAgreementLine, List<string> skipUpdateFields = null)
        {
            try
            {
                dbAssignmentAgreementLine = dbAssignmentAgreementLine.MapUpdateValues<AssignmentAgreementLine>(inputAssignmentAgreementLine);
                base.Update(dbAssignmentAgreementLine);
                return dbAssignmentAgreementLine;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateAssignmentAgreementLineVoid(AssignmentAgreementLine dbAssignmentAgreementLine, AssignmentAgreementLine inputAssignmentAgreementLine, List<string> skipUpdateFields = null)
        {
            try
            {
                dbAssignmentAgreementLine = dbAssignmentAgreementLine.MapUpdateValues<AssignmentAgreementLine>(inputAssignmentAgreementLine, skipUpdateFields);
                base.Update(dbAssignmentAgreementLine);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Create, Update, Delete
        public AssignmentAgreementLine GetAssignmentAgreementLineByIdNoTrackingByAccessLevel(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.AssignmentAgreementLineGUID == guid)
                    .FilterByAccessLevel(SysParm.AccessLevel)
                    .AsNoTracking()
                    .FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region function
        public List<AssignmentAgreementLine> GetAssignmentAgreementLineByAssignmentAgreementTableGuidNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.AssignmentAgreementTableGUID == guid).AsNoTracking().ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        public IEnumerable<AssignmentAgreementLine> GetAssignmentAgreementLineByAssignmentAgreementTableGuid(Guid guid)
        {
            try
            {
                IEnumerable<AssignmentAgreementLine> assignmentAgreementLines = Entity.Where(w => w.AssignmentAgreementTableGUID == guid)
                    .AsNoTracking().ToList();
                return assignmentAgreementLines;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Validate
        public override void ValidateAdd(IEnumerable<AssignmentAgreementLine> items)
        {
            base.ValidateAdd(items);
        }
        #endregion Validate
    }
}

