using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICustGroupRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CustGroupItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<CustGroupListView> GetListvw(SearchParameter search);
		CustGroupItemView GetByIdvw(Guid id);
		CustGroup Find(params object[] keyValues);
		CustGroup GetCustGroupByIdNoTracking(Guid guid);
		CustGroup CreateCustGroup(CustGroup custGroup);
		void CreateCustGroupVoid(CustGroup custGroup);
		CustGroup UpdateCustGroup(CustGroup dbCustGroup, CustGroup inputCustGroup, List<string> skipUpdateFields = null);
		void UpdateCustGroupVoid(CustGroup dbCustGroup, CustGroup inputCustGroup, List<string> skipUpdateFields = null);
		void Remove(CustGroup item);
		Guid GetNumberSeqTableByCustGroupGUID(Guid custGroupGUID);
	}
	public class CustGroupRepo : CompanyBaseRepository<CustGroup>, ICustGroupRepo
	{
		public CustGroupRepo(SmartAppDbContext context) : base(context) { }
		public CustGroupRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CustGroup GetCustGroupByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CustGroupGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CustGroupItemViewMap> GetDropDownQuery()
		{
			return (from custGroup in Entity
					select new CustGroupItemViewMap
					{
						CompanyGUID = custGroup.CompanyGUID,
						Owner = custGroup.Owner,
						OwnerBusinessUnitGUID = custGroup.OwnerBusinessUnitGUID,
						CustGroupGUID = custGroup.CustGroupGUID,
						CustGroupId = custGroup.CustGroupId,
						Description = custGroup.Description
					});
		}
		public IEnumerable<SelectItem<CustGroupItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CustGroup>(search, SysParm.CompanyGUID);
				var custGroup = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CustGroupItemViewMap, CustGroupItemView>().ToDropDownItem(search.ExcludeRowData);


				return custGroup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CustGroupListViewMap> GetListQuery()
		{
			return (from custGroup in Entity
					join numberSeqTable in db.Set<NumberSeqTable>()
					on custGroup.NumberSeqTableGUID equals numberSeqTable.NumberSeqTableGUID into ljNumberSeqTable
					from numberSeqTable in ljNumberSeqTable.DefaultIfEmpty()
					select new CustGroupListViewMap
				{
					CompanyGUID = custGroup.CompanyGUID,
					Owner = custGroup.Owner,
					OwnerBusinessUnitGUID = custGroup.OwnerBusinessUnitGUID,
					CustGroupGUID = custGroup.CustGroupGUID,
					CustGroupId = custGroup.CustGroupId,
					Description = custGroup.Description,
					NumberSeqTableGUID = custGroup.NumberSeqTableGUID,
					NumberSeqCode = numberSeqTable.NumberSeqCode,
					NumberSeqTable_Description = numberSeqTable.Description,
					NumberSeqTable_Value = SmartAppUtil.GetDropDownLabel(numberSeqTable.NumberSeqCode, numberSeqTable.Description)
				}); 
		}
		public SearchResult<CustGroupListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CustGroupListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CustGroup>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CustGroupListViewMap, CustGroupListView>();
				result = list.SetSearchResult<CustGroupListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CustGroupItemViewMap> GetItemQuery()
		{
			return (from custGroup in Entity
					join company in db.Set<Company>()
					on custGroup.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on custGroup.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCustGroupOwnerBU
					from ownerBU in ljCustGroupOwnerBU.DefaultIfEmpty()
					select new CustGroupItemViewMap
					{
						CompanyGUID = custGroup.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = custGroup.Owner,
						OwnerBusinessUnitGUID = custGroup.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = custGroup.CreatedBy,
						CreatedDateTime = custGroup.CreatedDateTime,
						ModifiedBy = custGroup.ModifiedBy,
						ModifiedDateTime = custGroup.ModifiedDateTime,
						CustGroupGUID = custGroup.CustGroupGUID,
						CustGroupId = custGroup.CustGroupId,
						Description = custGroup.Description,
						NumberSeqTableGUID = custGroup.NumberSeqTableGUID,
					
						RowVersion = custGroup.RowVersion,
					});
		}
		public CustGroupItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<CustGroupItemViewMap, CustGroupItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public CustGroup CreateCustGroup(CustGroup custGroup)
		{
			try
			{
				custGroup.CustGroupGUID = Guid.NewGuid();
				base.Add(custGroup);
				return custGroup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCustGroupVoid(CustGroup custGroup)
		{
			try
			{
				CreateCustGroup(custGroup);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CustGroup UpdateCustGroup(CustGroup dbCustGroup, CustGroup inputCustGroup, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCustGroup = dbCustGroup.MapUpdateValues<CustGroup>(inputCustGroup);
				base.Update(dbCustGroup);
				return dbCustGroup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCustGroupVoid(CustGroup dbCustGroup, CustGroup inputCustGroup, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCustGroup = dbCustGroup.MapUpdateValues<CustGroup>(inputCustGroup, skipUpdateFields);
				base.Update(dbCustGroup);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public Guid GetNumberSeqTableByCustGroupGUID(Guid custGroupGUID)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				CustGroup custGroup = GetCustGroupByIdNoTracking(custGroupGUID);
				if(custGroup.NumberSeqTableGUID == null)
                {
					ex.AddData("ERROR.90024");
					throw SmartAppUtil.AddStackTrace(ex);

				}
				return custGroup.NumberSeqTableGUID;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}

	}
}

