using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IMethodOfPaymentRepo
	{
		#region DropDown
		IEnumerable<SelectItem<MethodOfPaymentItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<MethodOfPaymentListView> GetListvw(SearchParameter search);
		MethodOfPaymentItemView GetByIdvw(Guid id);
		MethodOfPayment Find(params object[] keyValues);
		MethodOfPayment GetMethodOfPaymentByIdNoTracking(Guid guid);
		List<MethodOfPayment> GetMethodOfPaymentByIdNoTracking(IEnumerable<Guid> methodOfPaymentGuids);
		MethodOfPayment CreateMethodOfPayment(MethodOfPayment methodOfPayment);
		void CreateMethodOfPaymentVoid(MethodOfPayment methodOfPayment);
		MethodOfPayment UpdateMethodOfPayment(MethodOfPayment dbMethodOfPayment, MethodOfPayment inputMethodOfPayment, List<string> skipUpdateFields = null);
		void UpdateMethodOfPaymentVoid(MethodOfPayment dbMethodOfPayment, MethodOfPayment inputMethodOfPayment, List<string> skipUpdateFields = null);
		void Remove(MethodOfPayment item);
		List<MethodOfPayment> GetMethodOfPaymentByCompanyNoTracking(Guid companyGUID);
	}
	public class MethodOfPaymentRepo : CompanyBaseRepository<MethodOfPayment>, IMethodOfPaymentRepo
	{
		public MethodOfPaymentRepo(SmartAppDbContext context) : base(context) { }
		public MethodOfPaymentRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public MethodOfPayment GetMethodOfPaymentByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.MethodOfPaymentGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<MethodOfPayment> GetMethodOfPaymentByIdNoTracking(IEnumerable<Guid> methodOfPaymentGuids)
        {
            try
            {
				var result = Entity.Where(w => methodOfPaymentGuids.Contains(w.MethodOfPaymentGUID))
									.AsNoTracking()
									.ToList();
				return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		#region DropDown
		private IQueryable<MethodOfPaymentItemViewMap> GetDropDownQuery()
		{
			return (from methodOfPayment in Entity
					select new MethodOfPaymentItemViewMap
					{
						CompanyGUID = methodOfPayment.CompanyGUID,
						Owner = methodOfPayment.Owner,
						OwnerBusinessUnitGUID = methodOfPayment.OwnerBusinessUnitGUID,
						MethodOfPaymentGUID = methodOfPayment.MethodOfPaymentGUID,
						MethodOfPaymentId = methodOfPayment.MethodOfPaymentId,
						Description = methodOfPayment.Description,
						PaymentRemark = methodOfPayment.PaymentRemark,
						PaymentType = methodOfPayment.PaymentType
					});
		}
		public IEnumerable<SelectItem<MethodOfPaymentItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<MethodOfPayment>(search, SysParm.CompanyGUID);
				var methodOfPayment = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<MethodOfPaymentItemViewMap, MethodOfPaymentItemView>().ToDropDownItem(search.ExcludeRowData);


				return methodOfPayment;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<MethodOfPaymentListViewMap> GetListQuery()
		{
			return (from methodOfPayment in Entity
					join companyBank in db.Set<CompanyBank>()
					on methodOfPayment.CompanyBankGUID equals companyBank.CompanyBankGUID into ljMethodOfPaymentCompanyBank
					from companyBank in ljMethodOfPaymentCompanyBank.DefaultIfEmpty()
					join bankGroup in db.Set<BankGroup>()
					on companyBank.BankGroupGUID equals bankGroup.BankGroupGUID into ljMethodOfPaymentBankGroup
					from bankGroup in ljMethodOfPaymentBankGroup.DefaultIfEmpty()
					select new MethodOfPaymentListViewMap
					{
						CompanyGUID = methodOfPayment.CompanyGUID,
						Owner = methodOfPayment.Owner,
						OwnerBusinessUnitGUID = methodOfPayment.OwnerBusinessUnitGUID,
						MethodOfPaymentGUID = methodOfPayment.MethodOfPaymentGUID,
						MethodOfPaymentId = methodOfPayment.MethodOfPaymentId,
						Description = methodOfPayment.Description,
						CompanyBankGUID = methodOfPayment.CompanyBankGUID,
						PaymentType = methodOfPayment.PaymentType,
						PaymentRemark = methodOfPayment.PaymentRemark,
						CompanyBank_AccountNumber = companyBank.AccountNumber,
						CompanyBank_Values = SmartAppUtil.GetDropDownLabel(bankGroup.BankGroupId, companyBank.BankAccountName),
						AccountType = methodOfPayment.AccountType,
						AccountNum = methodOfPayment.AccountNum,

		});
		}
		public SearchResult<MethodOfPaymentListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<MethodOfPaymentListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<MethodOfPayment>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<MethodOfPaymentListViewMap, MethodOfPaymentListView>();
				result = list.SetSearchResult<MethodOfPaymentListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<MethodOfPaymentItemViewMap> GetItemQuery()
		{
			return (from methodOfPayment in Entity
					join company in db.Set<Company>()
					on methodOfPayment.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on methodOfPayment.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljMethodOfPaymentOwnerBU
					from ownerBU in ljMethodOfPaymentOwnerBU.DefaultIfEmpty()
					select new MethodOfPaymentItemViewMap
					{
						CompanyGUID = methodOfPayment.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = methodOfPayment.Owner,
						OwnerBusinessUnitGUID = methodOfPayment.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = methodOfPayment.CreatedBy,
						CreatedDateTime = methodOfPayment.CreatedDateTime,
						ModifiedBy = methodOfPayment.ModifiedBy,
						ModifiedDateTime = methodOfPayment.ModifiedDateTime,
						MethodOfPaymentGUID = methodOfPayment.MethodOfPaymentGUID,
						CompanyBankGUID = methodOfPayment.CompanyBankGUID,
						Description = methodOfPayment.Description,
						MethodOfPaymentId = methodOfPayment.MethodOfPaymentId,
						PaymentRemark = methodOfPayment.PaymentRemark,
						PaymentType = methodOfPayment.PaymentType,
						AccountType = methodOfPayment.AccountType,
						AccountNum = methodOfPayment.AccountNum,
					
						RowVersion = methodOfPayment.RowVersion,
					});
		}
		public MethodOfPaymentItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<MethodOfPaymentItemViewMap, MethodOfPaymentItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public MethodOfPayment CreateMethodOfPayment(MethodOfPayment methodOfPayment)
		{
			try
			{
				methodOfPayment.MethodOfPaymentGUID = Guid.NewGuid();
				base.Add(methodOfPayment);
				return methodOfPayment;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateMethodOfPaymentVoid(MethodOfPayment methodOfPayment)
		{
			try
			{
				CreateMethodOfPayment(methodOfPayment);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public MethodOfPayment UpdateMethodOfPayment(MethodOfPayment dbMethodOfPayment, MethodOfPayment inputMethodOfPayment, List<string> skipUpdateFields = null)
		{
			try
			{
				dbMethodOfPayment = dbMethodOfPayment.MapUpdateValues<MethodOfPayment>(inputMethodOfPayment);
				base.Update(dbMethodOfPayment);
				return dbMethodOfPayment;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateMethodOfPaymentVoid(MethodOfPayment dbMethodOfPayment, MethodOfPayment inputMethodOfPayment, List<string> skipUpdateFields = null)
		{
			try
			{
				dbMethodOfPayment = dbMethodOfPayment.MapUpdateValues<MethodOfPayment>(inputMethodOfPayment, skipUpdateFields);
				base.Update(dbMethodOfPayment);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public List<MethodOfPayment> GetMethodOfPaymentByCompanyNoTracking(Guid companyGUID)
		{
			try
			{
				List<MethodOfPayment> methodOfPayments = Entity.Where(w => w.CompanyGUID == companyGUID).AsNoTracking().ToList();
				return methodOfPayments;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public override void ValidateAdd(MethodOfPayment item)
		{
			base.ValidateAdd(item);
			ValidateMethodOfPayment(item);
		}
		public override void ValidateUpdate(MethodOfPayment item)
		{
			base.ValidateUpdate(item);
			ValidateMethodOfPayment(item);
		}
		public void ValidateMethodOfPayment(MethodOfPayment methodOfPayment)
		{
			try
			{
				ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				//Validate 1 & 2
				CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(SysParm.CompanyGUID.StringToGuid());
				//Validate 3
				MethodOfPayment dbMethodOfPayment = GetMethodOfPaymentByIdNoTracking(methodOfPayment.MethodOfPaymentGUID);
				List<ReceiptTempTable> receiptTempTables = (from receiptTempPaymDetail in db.Set<ReceiptTempPaymDetail>()
															join receiptTempTable in db.Set<ReceiptTempTable>()
															on receiptTempPaymDetail.ReceiptTempTableGUID equals receiptTempTable.ReceiptTempTableGUID
															join documentStatus in db.Set<DocumentStatus>()
															on receiptTempTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
															where receiptTempPaymDetail.MethodOfPaymentGUID == methodOfPayment.MethodOfPaymentGUID && documentStatus.StatusId != TemporaryReceiptStatus.Cancelled.GetAttrValue().ToString()
															select receiptTempTable
															).ToList();
				bool isReceiptTempTableStatusNotCancelled = ConditionService.IsNotZero(receiptTempTables.Count());

				if (methodOfPayment.PaymentType != (int)PaymentType.SuspenseAccount && companyParameter.SuspenseMethodOfPaymentGUID == methodOfPayment.MethodOfPaymentGUID) //Validate 1
				{
					ex.AddData("ERROR.90173", new string[] { "LABEL.METHOD_OF_PAYMENT", "LABEL.PAYMENT_TYPE", "ENUM.SUSPENSE_ACCOUNT", "LABEL.SUSPENSE_METHOD_OF_PAYMENT_ID", "LABEL.COMPANY_PARAMETER" });
				}
				if (methodOfPayment.PaymentType != (int)PaymentType.Ledger && companyParameter.SettlementMethodOfPaymentGUID == methodOfPayment.MethodOfPaymentGUID) //Validate 2
				{
					ex.AddData("ERROR.90173", new string[] { "LABEL.METHOD_OF_PAYMENT", "LABEL.PAYMENT_TYPE", "ENUM.LEDGER", "LABEL.SETTLEMENT_METHOD_OF_PAYMENT_ID", "LABEL.COMPANY_PARAMETER" });
				}
				if (isReceiptTempTableStatusNotCancelled && dbMethodOfPayment.PaymentType != methodOfPayment.PaymentType) //Validate 3
				{
					ex.AddData("ERROR.90171", new string[] { "LABEL.PAYMENT_TYPE", "LABEL.COLLECTION_PAYMENT_DETAIL", "LABEL.METHOD_OF_PAYMENT_ID", methodOfPayment.MethodOfPaymentId, "LABEL.COLLECTION" });
				}
				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

