using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface IInvoiceTableRepo
    {
        #region DropDown
        IEnumerable<SelectItem<InvoiceTableItemView>> GetDropDownItem(SearchParameter search);
        IEnumerable<SelectItem<InvoiceTableItemView>> GetRefInvoiceTableDropDownItem(SearchParameter search);
        IEnumerable<SelectItem<InvoiceTableItemView>> GetOriginalInvoiceDropDownItem(SearchParameter search);
        #endregion DropDown
        SearchResult<InvoiceTableListView> GetListvw(SearchParameter search);
        InvoiceTableItemView GetByIdvw(Guid id);
        InvoiceTable Find(params object[] keyValues);
        InvoiceTable GetInvoiceTableByIdNoTracking(Guid guid);
        List<InvoiceTable> GetInvoiceTableByIdNoTracking(IEnumerable<Guid> invoiceTableGuids);
        InvoiceTable CreateInvoiceTable(InvoiceTable invoiceTable);
        void CreateInvoiceTableVoid(InvoiceTable invoiceTable);
        InvoiceTable UpdateInvoiceTable(InvoiceTable dbInvoiceTable, InvoiceTable inputInvoiceTable, List<string> skipUpdateFields = null);
        void UpdateInvoiceTableVoid(InvoiceTable dbInvoiceTable, InvoiceTable inputInvoiceTable, List<string> skipUpdateFields = null);
        void Remove(InvoiceTable item);
        InvoiceTable GetInvoiceTableByIdNoTrackingByAccessLevel(Guid guid);
        IEnumerable<InvoiceTable> GetInvoiceTableByBuyerInvoiceTableNoTracking(Guid? guid);
        InvoiceTable GetInvoiceTableByInvoiceIdNoTracking(string id);
        List<InvoiceTable> GetInvoiceTableByInvoiceIdNoTracking(IEnumerable<string> invoiceIds);
        InvoiceTableItemViewMap GetInvoiceTableItemViewMapByInvoiceTableGuid(Guid invoiceTableGuid);
        IEnumerable<InvoiceTable> GetInvoiceTableByCustTransOpenStatus(Guid companyGUID);
        InvoiceTable GetInvoiceTableByRefType(int refType, Guid refGuid);
        void ValidateAdd(InvoiceTable item);
        void ValidateAdd(IEnumerable<InvoiceTable> items);
        #region InvoiceOutstanding
        InvoiceOutstandingListViewMap GetInvoiceSettlementDetailInitialData(string id);
        InvoiceOutstandingListViewMap GetItemSuspenseOutstandingQuery(string id);
        SearchResult<InvoiceOutstandingListView> GetListOutstandingvw(SearchParameter search);
        SearchResult<InvoiceOutstandingListView> GetListSuspenseOutstandingvw(SearchParameter search);
        #endregion InvoiceOutstanding
        List<InvoiceOutstandingListView> GetListOutstandingvw(SearchPredicate predicate);
        List<CustomerAgingViewMap> GetListForCustomerAging(ReportAgingView parameter);
    }
    public class InvoiceTableRepo : BranchCompanyBaseRepository<InvoiceTable>, IInvoiceTableRepo
    {
        public InvoiceTableRepo(SmartAppDbContext context) : base(context) { }
        public InvoiceTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public InvoiceTable GetInvoiceTableByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.InvoiceTableGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<InvoiceTable> GetInvoiceTableByIdNoTracking(IEnumerable<Guid> invoiceTableGuids)
        {
            try
            {
                var result = Entity.Where(w => invoiceTableGuids.Contains(w.InvoiceTableGUID))
                                    .AsNoTracking()
                                    .ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        private IQueryable<InvoiceTableItemViewMap> GetDropDownQuery()
        {
            return (from invoiceTable in Entity
                    select new InvoiceTableItemViewMap
                    {
                        CompanyGUID = invoiceTable.CompanyGUID,
                        Owner = invoiceTable.Owner,
                        OwnerBusinessUnitGUID = invoiceTable.OwnerBusinessUnitGUID,
                        InvoiceTableGUID = invoiceTable.InvoiceTableGUID,
                        InvoiceId = invoiceTable.InvoiceId,
                        IssuedDate = invoiceTable.IssuedDate,
                        InvoiceAmount = invoiceTable.InvoiceAmount,
                        CustomerTableGUID = invoiceTable.CustomerTableGUID,
                        SuspenseInvoiceType = invoiceTable.SuspenseInvoiceType,
                        ProductInvoice = invoiceTable.ProductInvoice,
                        ProductType = invoiceTable.ProductType
                    });
        }
        private IQueryable<InvoiceTableItemViewMap> GetRefInvoiceTableDropDownQuery()
        {
            return (from invoiceTable in Entity
                    select new InvoiceTableItemViewMap
                    {
                        CompanyGUID = invoiceTable.CompanyGUID,
                        Owner = invoiceTable.Owner,
                        OwnerBusinessUnitGUID = invoiceTable.OwnerBusinessUnitGUID,
                        RefInvoiceGUID = invoiceTable.RefInvoiceGUID,
                        InvoiceId = invoiceTable.InvoiceId,
                        IssuedDate = invoiceTable.IssuedDate
                    });
        }
        private IQueryable<InvoiceTableItemViewMap> GetOriginalInvoiceDropDownQuery()
        {
            string posted = ((int)InvoiceStatus.Posted).ToString();
            return (from invoiceTable in Entity
                    join companyParameter  in db.Set<CompanyParameter>() on invoiceTable.CompanyGUID equals companyParameter.CompanyGUID
                    join creditAppRequestTable in db.Set<CreditAppRequestTable>() on invoiceTable.CreditAppTableGUID equals creditAppRequestTable.RefCreditAppTableGUID
                    join invoiceLine in db.Set<InvoiceLine>() on invoiceTable.InvoiceTableGUID equals invoiceLine.InvoiceTableGUID
                    join documentStatus in db.Set<DocumentStatus>() on invoiceTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
                    where invoiceTable.InvoiceTypeGUID == companyParameter.ServiceFeeInvTypeGUID && 
                          invoiceLine.InvoiceRevenueTypeGUID == companyParameter.CreditReqInvRevenueTypeGUID &&
                          documentStatus.StatusId == posted
                    select new InvoiceTableItemViewMap
                    {
                        CompanyGUID = invoiceTable.CompanyGUID,
                        Owner = invoiceTable.Owner,
                        OwnerBusinessUnitGUID = invoiceTable.OwnerBusinessUnitGUID,
                        RefInvoiceGUID = invoiceTable.RefInvoiceGUID,
                        InvoiceId = invoiceTable.InvoiceId,
                        IssuedDate = invoiceTable.IssuedDate,
                        InvoiceAmount = invoiceTable.InvoiceAmount,
                        InvoiceTableGUID = invoiceTable.InvoiceTableGUID,
                        CreditAppRequestTable_CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
                    });
        }
        public IEnumerable<SelectItem<InvoiceTableItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<InvoiceTable>(search, db.GetAvailableBranch(), SysParm.CompanyGUID, SysParm.BranchGUID);
                var invoiceTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
                    .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                    .Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
                    .ToMaps<InvoiceTableItemViewMap, InvoiceTableItemView>().ToDropDownItem(search.ExcludeRowData);
                return invoiceTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<InvoiceTableItemView>> GetRefInvoiceTableDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<InvoiceTable>(search, db.GetAvailableBranch(), SysParm.CompanyGUID, SysParm.BranchGUID);
                var invoiceTable = GetRefInvoiceTableDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<InvoiceTableItemViewMap, InvoiceTableItemView>().ToDropDownItem(search.ExcludeRowData);


                return invoiceTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<InvoiceTableItemView>> GetOriginalInvoiceDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<InvoiceTable>(search, db.GetAvailableBranch(), SysParm.CompanyGUID, SysParm.BranchGUID);
                var invoiceTable = GetOriginalInvoiceDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<InvoiceTableItemViewMap, InvoiceTableItemView>().ToDropDownItem(search.ExcludeRowData);


                return invoiceTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #region GetListvw
        private IQueryable<InvoiceTableListViewMap> GetListQuery()
        {
            return (from invoiceTable in db.Set<InvoiceTable>()

                    join customerTable in db.Set<CustomerTable>() 
                    on invoiceTable.CustomerTableGUID equals customerTable.CustomerTableGUID into ljcustomerTable
                    from customerTable in ljcustomerTable.DefaultIfEmpty()

                    join creditAppTable in db.Set<CreditAppTable>() 
                    on invoiceTable.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljCreditAppTable
                    from creditAppTable in ljCreditAppTable.DefaultIfEmpty()

                    join documentStatus in db.Set<DocumentStatus>() 
                    on invoiceTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus
                    from documentStatus in ljdocumentStatus.DefaultIfEmpty()

                    join invoiceType in db.Set<InvoiceType>() 
                    on invoiceTable.InvoiceTypeGUID equals invoiceType.InvoiceTypeGUID into ljinvoiceType
                    from invoiceType in ljinvoiceType.DefaultIfEmpty()

                    join buyerTable in db.Set<BuyerTable>() 
                    on invoiceTable.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljBuyerTable
                    from buyerTable in ljBuyerTable.DefaultIfEmpty()

                    join buyerInvoiceTable in db.Set<BuyerInvoiceTable>() 
                    on invoiceTable.BuyerInvoiceTableGUID equals buyerInvoiceTable.BuyerInvoiceTableGUID into ljBuyerInvoiceTable
                    from buyerInvoiceTable in ljBuyerInvoiceTable.DefaultIfEmpty()

                    join custtrans in db.Set<CustTrans>()
                    on invoiceTable.InvoiceTableGUID equals custtrans.InvoiceTableGUID into ljcusttrans
                    from custtrans in ljcusttrans.DefaultIfEmpty()

                    select new InvoiceTableListViewMap
                    {
                        CompanyGUID = invoiceTable.CompanyGUID,
                        BranchGUID = invoiceTable.BranchGUID,
                        Owner = invoiceTable.Owner,
                        OwnerBusinessUnitGUID = invoiceTable.OwnerBusinessUnitGUID,
                        InvoiceTableGUID = invoiceTable.InvoiceTableGUID,
                        InvoiceId = invoiceTable.InvoiceId,
                        CustomerTableGUID = invoiceTable.CustomerTableGUID,
                        CreditAppTableGUID = invoiceTable.CreditAppTableGUID,
                        IssuedDate = invoiceTable.IssuedDate,
                        InvoiceTypeGUID = invoiceTable.InvoiceTypeGUID,
                        DocumentStatusGUID = invoiceTable.DocumentStatusGUID,
                        DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
                        DocumentStatus_DocumentId = documentStatus.StatusId,
                        BuyerTableGUID = invoiceTable.BuyerTableGUID,
                        BuyerInvoiceTableGUID = invoiceTable.BuyerInvoiceTableGUID,
                        CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                        CustomerTable_CustomerId = customerTable.CustomerId,
                        InvoiceType_Values = SmartAppUtil.GetDropDownLabel(invoiceType.InvoiceTypeId, invoiceType.Description),
                        InvoiceType_InvoiceTypeId = invoiceType.InvoiceTypeId,
                        CreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
                        CreditAppTable_CreditAppId = creditAppTable.CreditAppId,
                        BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
                        BuyerInvoiceTable_Values = SmartAppUtil.GetDropDownLabel(buyerInvoiceTable.BuyerInvoiceId, buyerInvoiceTable.Remark),
                        BuyerInvoiceTable_BuyerInvoiceId = buyerInvoiceTable.BuyerInvoiceId,
                        BuyerTable_BuyerId = buyerTable.BuyerId,
                        DueDate = invoiceTable.DueDate,
                        RefGUID = invoiceTable.RefGUID,
                        CustTransStatus = custtrans.CustTransStatus,
                        DocumentId = invoiceTable.DocumentId,
                        RefType = invoiceTable.RefType
                    });
       
        }
        public SearchResult<InvoiceTableListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<InvoiceTableListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<InvoiceTable>(search, db.GetAvailableBranch(), SysParm.CompanyGUID, SysParm.BranchGUID);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<InvoiceTableListViewMap, InvoiceTableListView>();
                result = list.SetSearchResult<InvoiceTableListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<InvoiceTableItemViewMap> GetItemQuery()
        {
            return (from invoiceTable in Entity
                    join company in db.Set<Company>()
                    on invoiceTable.CompanyGUID equals company.CompanyGUID
                    join branch in db.Set<Branch>()
                    on invoiceTable.BranchGUID equals branch.BranchGUID
                    join ownerBU in db.Set<BusinessUnit>()
                    on invoiceTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljInvoiceTableOwnerBU
                    from ownerBU in ljInvoiceTableOwnerBU.DefaultIfEmpty()
                    join documentStatus in db.Set<DocumentStatus>()
                    on invoiceTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
                    join custtrans in db.Set<CustTrans>() on invoiceTable.InvoiceTableGUID equals custtrans.InvoiceTableGUID into ljcusttrans
                    from custtrans in ljcusttrans.DefaultIfEmpty()

                    join receiptTempTable in db.Set<ReceiptTempTable>()
                    on invoiceTable.RefGUID equals receiptTempTable.ReceiptTempTableGUID into ljreceiptTempTable
                    from receiptTempTable in ljreceiptTempTable.DefaultIfEmpty()

                    join purchaseLine in db.Set<PurchaseLine>()
                    on invoiceTable.RefGUID equals purchaseLine.PurchaseLineGUID into ljPurchaseLine
                    from purchaseLine in ljPurchaseLine.DefaultIfEmpty()

                    join purchaseTable in db.Set<PurchaseTable>()
                    on purchaseLine.PurchaseTableGUID equals purchaseTable.PurchaseTableGUID into ljPurchaseTable
                    from purchaseTable in ljPurchaseTable.DefaultIfEmpty()

                    join withdrawalLine in db.Set<WithdrawalLine>()
                    on invoiceTable.RefGUID equals withdrawalLine.WithdrawalTableGUID into ljWithdrawalLine
                    from withdrawalLine in ljWithdrawalLine.DefaultIfEmpty()

                    join withdrawalTable in db.Set<WithdrawalTable>()
                    on withdrawalLine.WithdrawalTableGUID equals withdrawalTable.WithdrawalTableGUID into ljWithdrawalTable
                    from withdrawalTable in ljWithdrawalTable.DefaultIfEmpty()

                    join serviceFeeTrans in db.Set<ServiceFeeTrans>()
                    on invoiceTable.RefGUID equals serviceFeeTrans.ServiceFeeTransGUID into ljServiceFeeTrans
                    from serviceFeeTrans in ljServiceFeeTrans.DefaultIfEmpty()

                    select new InvoiceTableItemViewMap
                    {
                        CompanyGUID = invoiceTable.CompanyGUID,
                        CompanyId = company.CompanyId,
                        BranchGUID = invoiceTable.BranchGUID,
                        BranchId = branch.BranchId,
                        Owner = invoiceTable.Owner,
                        OwnerBusinessUnitGUID = invoiceTable.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        CreatedBy = invoiceTable.CreatedBy,
                        CreatedDateTime = invoiceTable.CreatedDateTime,
                        ModifiedBy = invoiceTable.ModifiedBy,
                        ModifiedDateTime = invoiceTable.ModifiedDateTime,
                        InvoiceTableGUID = invoiceTable.InvoiceTableGUID,
                        AccountingPeriod = invoiceTable.AccountingPeriod,
                        BuyerAgreementTableGUID = invoiceTable.BuyerAgreementTableGUID,
                        BuyerInvoiceTableGUID = invoiceTable.BuyerInvoiceTableGUID,
                        BuyerTableGUID = invoiceTable.BuyerTableGUID,
                        CNReasonGUID = invoiceTable.CNReasonGUID,
                        CreditAppTableGUID = invoiceTable.CreditAppTableGUID,
                        CurrencyGUID = invoiceTable.CurrencyGUID,
                        CustomerName = invoiceTable.CustomerName,
                        CustomerTableGUID = invoiceTable.CustomerTableGUID,
                        Dimension1GUID = invoiceTable.Dimension1GUID,
                        Dimension2GUID = invoiceTable.Dimension2GUID,
                        Dimension3GUID = invoiceTable.Dimension3GUID,
                        Dimension4GUID = invoiceTable.Dimension4GUID,
                        Dimension5GUID = invoiceTable.Dimension5GUID,
                        DocumentId = invoiceTable.DocumentId,
                        DocumentStatusGUID = invoiceTable.DocumentStatusGUID,
                        DueDate = invoiceTable.DueDate,
                        ExchangeRate = invoiceTable.ExchangeRate,
                        InvoiceAddress1 = invoiceTable.InvoiceAddress1,
                        InvoiceAddress2 = invoiceTable.InvoiceAddress2,
                        InvoiceAmount = invoiceTable.InvoiceAmount,
                        InvoiceAmountBeforeTax = invoiceTable.InvoiceAmountBeforeTax,
                        InvoiceAmountBeforeTaxMST = invoiceTable.InvoiceAmountBeforeTaxMST,
                        InvoiceAmountMST = invoiceTable.InvoiceAmountMST,
                        InvoiceId = invoiceTable.InvoiceId,
                        InvoiceTypeGUID = invoiceTable.InvoiceTypeGUID,
                        IssuedDate = invoiceTable.IssuedDate,
                        MailingInvoiceAddress1 = invoiceTable.MailingInvoiceAddress1,
                        MailingInvoiceAddress2 = invoiceTable.MailingInvoiceAddress2,
                        MarketingPeriod = invoiceTable.MarketingPeriod,
                        MethodOfPaymentGUID = invoiceTable.MethodOfPaymentGUID,
                        OrigInvoiceAmount = invoiceTable.OrigInvoiceAmount,
                        OrigInvoiceId = invoiceTable.OrigInvoiceId,
                        OrigTaxInvoiceAmount = invoiceTable.OrigTaxInvoiceAmount,
                        OrigTaxInvoiceId = invoiceTable.OrigTaxInvoiceId,
                        ProductInvoice = invoiceTable.ProductInvoice,
                        ProductType = invoiceTable.ProductType,
                        ReceiptTempTableGUID = invoiceTable.ReceiptTempTableGUID,
                        RefGUID = invoiceTable.RefGUID,
                        RefInvoiceGUID = invoiceTable.RefInvoiceGUID,
                        RefTaxInvoiceGUID = invoiceTable.RefTaxInvoiceGUID,
                        RefType = invoiceTable.RefType,
                        Remark = invoiceTable.Remark,
                        SuspenseInvoiceType = invoiceTable.SuspenseInvoiceType,
                        TaxAmount = invoiceTable.TaxAmount,
                        TaxAmountMST = invoiceTable.TaxAmountMST,
                        TaxBranchId = invoiceTable.TaxBranchId,
                        TaxBranchName = invoiceTable.TaxBranchName,
                        WHTAmount = invoiceTable.WHTAmount,
                        WHTBaseAmount = invoiceTable.WHTBaseAmount,
                        UnboundInvoiceAddress = invoiceTable.InvoiceAddress1 + " " + invoiceTable.InvoiceAddress2,
                        UnboundMailingAddress = invoiceTable.MailingInvoiceAddress1 + " " + invoiceTable.MailingInvoiceAddress2,
                        CustTransStatus = custtrans.CustTransStatus,
                        ReferenceID = (receiptTempTable != null && invoiceTable.RefType == (int)RefType.ReceiptTemp) ? receiptTempTable.ReceiptTempId :
                                      (purchaseLine != null && invoiceTable.RefType == (int)RefType.PurchaseLine) ? purchaseTable.PurchaseId :
                                      (withdrawalLine != null && invoiceTable.RefType == (int)RefType.WithdrawalLine) ? withdrawalTable.WithdrawalId :
                                      (serviceFeeTrans != null && invoiceTable.RefType == (int)RefType.ServiceFeeTrans) ? serviceFeeTrans.Description :
                                      null,
                    
                        RowVersion = invoiceTable.RowVersion,
                    });
        }
        public InvoiceTableItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<InvoiceTableItemViewMap, InvoiceTableItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public InvoiceTable CreateInvoiceTable(InvoiceTable invoiceTable)
        {
            try
            {
                invoiceTable.InvoiceTableGUID = invoiceTable.InvoiceTableGUID == Guid.Empty ? Guid.NewGuid(): invoiceTable.InvoiceTableGUID;
                base.Add(invoiceTable);
                return invoiceTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateInvoiceTableVoid(InvoiceTable invoiceTable)
        {
            try
            {
                CreateInvoiceTable(invoiceTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public InvoiceTable UpdateInvoiceTable(InvoiceTable dbInvoiceTable, InvoiceTable inputInvoiceTable, List<string> skipUpdateFields = null)
        {
            try
            {
                dbInvoiceTable = dbInvoiceTable.MapUpdateValues<InvoiceTable>(inputInvoiceTable);
                base.Update(dbInvoiceTable);
                return dbInvoiceTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateInvoiceTableVoid(InvoiceTable dbInvoiceTable, InvoiceTable inputInvoiceTable, List<string> skipUpdateFields = null)
        {
            try
            {
                dbInvoiceTable = dbInvoiceTable.MapUpdateValues<InvoiceTable>(inputInvoiceTable, skipUpdateFields);
                base.Update(dbInvoiceTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Create, Update, Delete
        public InvoiceTable GetInvoiceTableByIdNoTrackingByAccessLevel(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.InvoiceTableGUID == guid)
                    .FilterByAccessLevel(SysParm.AccessLevel)
                    .AsNoTracking()
                    .FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public IEnumerable<InvoiceTable> GetInvoiceTableByBuyerInvoiceTableNoTracking(Guid? guid)
        {
            try
            {
                if (guid != null)
                {
                    var result = Entity.Where(item => item.BuyerInvoiceTableGUID == guid)
                        .AsNoTracking();
                    return result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public InvoiceTable GetInvoiceTableByInvoiceIdNoTracking(string id)
        {
            try
            {
                var result = Entity.Where(item => item.InvoiceId == id)
                    .AsNoTracking()
                    .FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<InvoiceTable> GetInvoiceTableByInvoiceIdNoTracking(IEnumerable<string> invoiceIds)
        {
            try
            {
                var result = Entity.Where(w => invoiceIds.Contains(w.InvoiceId))
                                .AsNoTracking()
                                .ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public InvoiceTableItemViewMap GetInvoiceTableItemViewMapByInvoiceTableGuid(Guid invoiceTableGuid)
        {
            try
            {
                InvoiceTableItemViewMap invoiceTableItemViewMap = (from invoiceTable in Entity
                                                                   join custTans in db.Set<CustTrans>() on invoiceTable.InvoiceTableGUID equals custTans.InvoiceTableGUID
                                                                   where invoiceTable.InvoiceTableGUID == invoiceTableGuid
                                                                   select new InvoiceTableItemViewMap
                                                                   {
                                                                       InvoiceTableGUID = invoiceTable.InvoiceTableGUID,
                                                                       WHTAmount = invoiceTable.WHTAmount,
                                                                       CustTrans_SettleAmount = custTans.SettleAmount,
                                                                       CustTrans_TransAmount = custTans.TransAmount
                                                                   }).AsNoTracking().FirstOrDefault();
                return invoiceTableItemViewMap;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public IEnumerable<InvoiceTable> GetInvoiceTableByCustTransOpenStatus(Guid companyGUID)
        {
            try
            {
                return (from invoiceTable in Entity
                        join custTans in db.Set<CustTrans>() on invoiceTable.InvoiceTableGUID equals custTans.InvoiceTableGUID
                        where custTans.CustTransStatus == (int)CustTransStatus.Open && invoiceTable.CompanyGUID == companyGUID
                        select new InvoiceTable
                        {
                            InvoiceTableGUID = invoiceTable.InvoiceTableGUID,
                        }).AsNoTracking();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public InvoiceTable GetInvoiceTableByRefType(int refType, Guid refGuid)
        {
            try
            {
                InvoiceTable item = Entity.Where(w => w.RefType == refType && w.RefGUID == refGuid)
                    .AsNoTracking().FirstOrDefault();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public override void ValidateAdd(IEnumerable<InvoiceTable> items)
        {
            base.ValidateAdd(items);
        }
        public override void ValidateAdd(InvoiceTable item)
        {
            base.ValidateAdd(item);
        }
        #region InvoiceOutstanding
        private IQueryable<InvoiceOutstandingListViewMap> GetListOutstandingQuery(string refTypeString = null, string refGUIDString = null)
        {
            int refType = refTypeString != null ? Convert.ToInt32(refTypeString): -1;
            Guid refGUID = refGUIDString != null ? new Guid(refGUIDString) : Guid.Empty;
            IQueryable<InvoiceOutstandingListViewMap> exitsInvoice =
                                         (from invoiceTables in Entity
                                          join invoiceSettlementDetails in db.Set<InvoiceSettlementDetail>()
                                          on invoiceTables.InvoiceTableGUID equals invoiceSettlementDetails.InvoiceTableGUID
                                          where ((refType != -1) ? invoiceSettlementDetails.RefType == refType : invoiceSettlementDetails.RefType == invoiceSettlementDetails.RefType)
                                          && ((refGUID != Guid.Empty) ? invoiceSettlementDetails.RefGUID == refGUID : invoiceSettlementDetails.RefGUID == invoiceSettlementDetails.RefGUID)
                                          select new InvoiceOutstandingListViewMap
                                          {
                                              InvoiceTableGUID = invoiceTables.InvoiceTableGUID,
                                              RefGUID = invoiceSettlementDetails.RefGUID,
                                          });
            IQueryable<InvoiceOutstandingListViewMap> invoiceOutstandingListViewMaps = 
                                                 (from invoiceTables in Entity
                                                  join custTrans in db.Set<CustTrans>()
                                                  on invoiceTables.InvoiceTableGUID equals custTrans.InvoiceTableGUID
                                                  join invoiceType in db.Set<InvoiceType>()
                                                  on invoiceTables.InvoiceTypeGUID equals invoiceType.InvoiceTypeGUID
                                                  join currency in db.Set<Currency>() on invoiceTables.CurrencyGUID equals currency.CurrencyGUID
                                                  join creditAppTable in db.Set<CreditAppTable>()
                                                  on invoiceTables.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljCreditAppTable
                                                  from creditAppTable in ljCreditAppTable.DefaultIfEmpty()
                                                  join buyerAgreementTable in db.Set<BuyerAgreementTable>() 
                                                  on invoiceTables.BuyerAgreementTableGUID equals buyerAgreementTable.BuyerAgreementTableGUID into ljBuyerAgreementTable
                                                  from buyerAgreementTable in ljBuyerAgreementTable.DefaultIfEmpty()
                                                  join buyerInvoiceTable in db.Set<BuyerInvoiceTable>()
                                                  on invoiceTables.BuyerInvoiceTableGUID equals buyerInvoiceTable.BuyerInvoiceTableGUID into ljBuyerInvoiceTable
                                                  from buyerInvoiceTable in ljBuyerInvoiceTable.DefaultIfEmpty()
                                                  join buyerTable in db.Set<BuyerTable>()
                                                  on invoiceTables.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljBuyerTable
                                                  from buyerTable in ljBuyerTable.DefaultIfEmpty()
                                                  where custTrans.CustTransStatus == (int)CustTransStatus.Open
                                                  select new InvoiceOutstandingListViewMap
                                                  {
                                                      #region data for view and search
                                                      CompanyGUID = invoiceTables.CompanyGUID,
                                                      BranchGUID = invoiceTables.BranchGUID,
                                                      Owner = invoiceTables.Owner,
                                                      OwnerBusinessUnitGUID = invoiceTables.OwnerBusinessUnitGUID,
                                                      InvoiceTableGUID = invoiceTables.InvoiceTableGUID,
                                                      ProductType = invoiceTables.ProductType,
                                                      CreatedDateTime = invoiceTables.CreatedDateTime,
                                                      DocumentId = invoiceTables.DocumentId,
                                                      BuyerAgreementTableGUID = invoiceTables.BuyerAgreementTableGUID,
                                                      BuyerInvoiceTableGUID = invoiceTables.BuyerInvoiceTableGUID,
                                                      BuyerTableGUID = invoiceTables.BuyerTableGUID,
                                                      DueDate = invoiceTables.DueDate,
                                                      InvoiceAmount = invoiceTables.InvoiceAmount,
                                                      InvoiceTable_CustomerTableGUID = invoiceTables.CustomerTableGUID,
                                                      SuspenseInvoiceType = invoiceTables.SuspenseInvoiceType,
                                                      ProductInvoice = invoiceTables.ProductInvoice,
                                                      BalanceAmount = custTrans.TransAmount - custTrans.SettleAmount,
                                                      InvoiceTypeGUID = invoiceTables.InvoiceTypeGUID,
                                                      #endregion data for view and search
                                                      #region for lookup
                                                      // lookup display
                                                      InvoiceTable_Values = SmartAppUtil.GetDropDownLabel(invoiceTables.InvoiceId, invoiceTables.IssuedDate.DateToString()),
                                                      InvoiceType_Values = SmartAppUtil.GetDropDownLabel(invoiceType.InvoiceTypeId, invoiceType.Description),
                                                      Currency_Values = SmartAppUtil.GetDropDownLabel(invoiceType.InvoiceTypeId, invoiceType.Description),
                                                      CreditAppTable_Values = (creditAppTable != null) ? SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description): string.Empty,
                                                      BuyerAgreementTable_Values = (buyerAgreementTable != null) ? SmartAppUtil.GetDropDownLabel(buyerAgreementTable.BuyerAgreementId, buyerAgreementTable.Description) : string.Empty,
                                                      BuyerInvoiceTable_Values = (buyerInvoiceTable != null) ? SmartAppUtil.GetDropDownLabel(buyerInvoiceTable.BuyerInvoiceId) : string.Empty,
                                                      BuyerTable_Values = (buyerTable != null) ? SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName) : string.Empty,
                                                      // lookup sorting
                                                      InvoiceTable_InvoiceId = invoiceTables.InvoiceId,
                                                      InvoiceType_InvoiceTypeId = invoiceType.InvoiceTypeId,
                                                      BuyerAgreementTable_BuyerAgreementId = (buyerAgreementTable != null) ? buyerAgreementTable.BuyerAgreementId : string.Empty,
                                                      BuyerInvoiceTable_BuyerInvoiceId = (buyerInvoiceTable != null) ? buyerInvoiceTable.BuyerInvoiceId : string.Empty,
                                                      BuyerTable_BuyerId = (buyerTable != null) ? buyerTable.BuyerId : string.Empty,
                                                      RefType = invoiceTables.RefType,
                                                      ExitsInvoice =  exitsInvoice.Any(a => a.InvoiceTableGUID == invoiceTables.InvoiceTableGUID),
                                                      #endregion for lookup
                                                  });
            return invoiceOutstandingListViewMaps;
        }
        public SearchResult<InvoiceOutstandingListView> GetListOutstandingvw(SearchParameter search)
        {
            var result = new SearchResult<InvoiceOutstandingListView>();
            try
            {
                var refType = search.Conditions.Where(w => w.ColumnName == "refType").FirstOrDefault();
                var refGUID = search.Conditions.Where(w => w.ColumnName == "refGUID").FirstOrDefault();
                search.Conditions.Remove(refType);
                search.Conditions.Remove(refGUID);
                var predicate = base.GetFilterLevelPredicate<InvoiceTable>(search, db.GetAvailableBranch(), SysParm.CompanyGUID, SysParm.BranchGUID);
                var total = GetListOutstandingQuery(refType.Value, refGUID.Value).Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .DistinctBy(b => b.InvoiceTableGUID)
                                            .Count();
                var list = GetListOutstandingQuery(refType.Value, refGUID.Value).Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<InvoiceOutstandingListViewMap, InvoiceOutstandingListView>();
                list = list.DistinctBy(b => b.InvoiceTableGUID).ToList();
                result = list.SetSearchResult<InvoiceOutstandingListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        public InvoiceOutstandingListViewMap GetInvoiceSettlementDetailInitialData(string id)
        {
            Guid guid = new Guid(id);
            InvoiceOutstandingListViewMap invoiceOutstandingListViewMaps = GetListOutstandingQuery().Where(w => w.InvoiceTableGUID == guid).FirstOrDefault();
            return invoiceOutstandingListViewMaps;
        }
        #endregion InvoiceOutstanding
        #region SuspenseOutstanding
        private IQueryable<InvoiceOutstandingListViewMap> GetListSuspenseOutstandingQuery()
        {
            return (from invoiceTable in db.Set<InvoiceTable>()
                    select new InvoiceOutstandingListViewMap
                    {
                        CompanyGUID = invoiceTable.CompanyGUID,
                        BranchGUID = invoiceTable.BranchGUID,
                        Owner = invoiceTable.Owner,
                        OwnerBusinessUnitGUID = invoiceTable.OwnerBusinessUnitGUID,
                        InvoiceTableGUID = invoiceTable.InvoiceTableGUID,
                        InvoiceTable_Values = SmartAppUtil.GetDropDownLabel(invoiceTable.InvoiceId),
                        ProductType = invoiceTable.ProductType,
                        CreatedDateTime = invoiceTable.CreatedDateTime,
                        DocumentId = invoiceTable.DocumentId,
                    });
        }
        public SearchResult<InvoiceOutstandingListView> GetListSuspenseOutstandingvw(SearchParameter search)
        {
            var result = new SearchResult<InvoiceOutstandingListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<InvoiceTable>(search, db.GetAvailableBranch(), SysParm.CompanyGUID, SysParm.BranchGUID);
                var total = GetListSuspenseOutstandingQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListSuspenseOutstandingQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<InvoiceOutstandingListViewMap, InvoiceOutstandingListView>();
                result = list.SetSearchResult<InvoiceOutstandingListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public InvoiceOutstandingListViewMap GetItemSuspenseOutstandingQuery(string id)
        {
            try
            {
                return GetListOutstandingQuery().Where(w => w.InvoiceTableGUID == id.StringToGuid()).FirstOrDefault();
                //return GetListSuspenseOutstandingQuery().Where(w => w.InvoiceTableGUID == id.StringToGuid()).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        #endregion SuspenseOutstanding


        public List<InvoiceOutstandingListView> GetListOutstandingvw(SearchPredicate predicate)
        {
            try
            {
                List<InvoiceOutstandingListView> result = GetListOutstandingQuery().Where(predicate.Predicates, predicate.Values)
                    .ToMaps<InvoiceOutstandingListViewMap, InvoiceOutstandingListView>();
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        #region Invoice for customer aging
        public List<CustomerAgingViewMap> GetListForCustomerAging(ReportAgingView parameter)
        {
            try
            {
                List<Guid> parmCustomerTableGUIDs = parameter.CustomerTableGUID.Select(Guid.Parse).ToList();
                List<Guid> parmBuyerTableGUIDs = parameter.BuyerTableGUID.Select(Guid.Parse).ToList();
                DateTime asOfDate = parameter.AsOfDate.StringToDate();
                IQueryable<CustomerAgingViewMap> query = 
                    (from invoiceTable in Entity
                        join custTrans in db.Set<CustTrans>() on invoiceTable.InvoiceTableGUID equals custTrans.InvoiceTableGUID
                        join customerTable in db.Set<CustomerTable>() on invoiceTable.CustomerTableGUID equals customerTable.CustomerTableGUID
                        join employeeTable in db.Set<EmployeeTable>() on customerTable.ResponsibleByGUID equals employeeTable.EmployeeTableGUID
                        join invoiceType in db.Set<InvoiceType>() on invoiceTable.InvoiceTypeGUID equals invoiceType.InvoiceTypeGUID
                        join invoiceStatus in db.Set<DocumentStatus>().Where(w => w.StatusId == ((int)InvoiceStatus.Posted).ToString()) 
                        on invoiceTable.DocumentStatusGUID equals invoiceStatus.DocumentStatusGUID

                        join buyerTable in db.Set<BuyerTable>()
                        on invoiceTable.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljBuyerTable
                        from buyerTable in ljBuyerTable.DefaultIfEmpty()
                        join creditAppTable in db.Set<CreditAppTable>()
                        on invoiceTable.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljCreditAppTable
                        from creditAppTable in ljCreditAppTable.DefaultIfEmpty()
                        join businessUnit in db.Set<BusinessUnit>() 
                        on employeeTable.BusinessUnitGUID equals businessUnit.BusinessUnitGUID into ljBusinessUnit
                        from businessUnit in ljBusinessUnit.DefaultIfEmpty()
                        join businessType in db.Set<BusinessType>()
                        on customerTable.BusinessTypeGUID equals businessType.BusinessTypeGUID into ljBusinessType
                        from businessType in ljBusinessType.DefaultIfEmpty()
                        
                        join purchaseTable in db.Set<PurchaseTable>()
                        on invoiceTable.RefGUID equals purchaseTable.PurchaseTableGUID into ljPurchaseTable
                        from purchaseTable in ljPurchaseTable.DefaultIfEmpty()
                        join purchaseLine in db.Set<PurchaseLine>()
                        on invoiceTable.RefGUID equals purchaseLine.PurchaseLineGUID into ljPurchaseLine
                        from purchaseLine in ljPurchaseLine.DefaultIfEmpty()
                        join purchaseTableLine in db.Set<PurchaseTable>()
                        on purchaseLine.PurchaseTableGUID equals purchaseTableLine.PurchaseTableGUID into ljPurchaseTableLine
                        from purchaseTableLine in ljPurchaseTableLine.DefaultIfEmpty()
                        join buyerInvoiceTable in db.Set<BuyerInvoiceTable>()
                        on purchaseLine.BuyerInvoiceTableGUID equals buyerInvoiceTable.BuyerInvoiceTableGUID into ljBuyerInvoiceTable
                        from buyerInvoiceTable in ljBuyerInvoiceTable.DefaultIfEmpty()

                        join withdrawalTable in db.Set<WithdrawalTable>()
                        on invoiceTable.RefGUID equals withdrawalTable.WithdrawalTableGUID into ljWithdrawalTable
                        from withdrawalTable in ljWithdrawalTable.DefaultIfEmpty()
                        join withdrawalLine in db.Set<WithdrawalLine>()
                        on invoiceTable.RefGUID equals withdrawalLine.WithdrawalLineGUID into ljWithdrawalLine
                        from withdrawalLine in ljWithdrawalLine.DefaultIfEmpty()
                        join withdrawalTableLine in db.Set<WithdrawalTable>()
                        on withdrawalLine.WithdrawalTableGUID equals withdrawalTableLine.WithdrawalTableGUID into ljWithdrawalTableLine
                        from withdrawalTableLine in ljWithdrawalTableLine.DefaultIfEmpty()
                     where invoiceTable.CompanyGUID == parameter.CompanyGUID.StringToGuid()
                        && invoiceTable.IssuedDate <= asOfDate
                        && invoiceTable.ProductType == parameter.ProductType
                        && invoiceTable.SuspenseInvoiceType == parameter.SuspenseInvoiceType
                        && parameter.RefType.Contains(invoiceTable.RefType)
                        && (parmCustomerTableGUIDs.Contains(Guid.Empty) ||
                            parmCustomerTableGUIDs.Contains(invoiceTable.CustomerTableGUID))
                        && (parmBuyerTableGUIDs.Contains(Guid.Empty) ||
                             parmBuyerTableGUIDs.Contains(purchaseLine.BuyerTableGUID) ||
                             parmBuyerTableGUIDs.Contains(withdrawalTable.BuyerTableGUID.Value) ||
                             parmBuyerTableGUIDs.Contains(withdrawalTableLine.BuyerTableGUID.Value))
                        && (custTrans.CustTransStatus == (int)CustTransStatus.Open ||
                            (custTrans.CustTransStatus == (int)CustTransStatus.Closed &&
                             asOfDate < (from paymHistory in db.Set<PaymentHistory>().Where(w => w.Cancel == false)
                                         where invoiceTable.InvoiceTableGUID == paymHistory.InvoiceTableGUID
                                         select paymHistory).OrderByDescending(o => o.PaymentDate).FirstOrDefault().PaymentDate))
                     select new CustomerAgingViewMap
                     {
                         CompanyGUID = invoiceTable.CompanyGUID,
                         InvoiceTableGUID = invoiceTable.InvoiceTableGUID,
                         InvoiceTable_RefGUID = invoiceTable.RefGUID,
                         ProductInvoice = invoiceTable.ProductInvoice,
                         CustTrans_TransAmount = custTrans.TransAmount,
                         CustTrans_SettledAmount = custTrans.SettleAmount,

                         CustomerId = customerTable.CustomerId,
                         CustomerName = customerTable.Name,
                         BuyerId = buyerTable != null ? buyerTable.BuyerId : "",
                         BuyerName = buyerTable != null ? buyerTable.BuyerName : "",
                         ProductType = invoiceTable.ProductType,
                         InvoiceTypeId = invoiceType.InvoiceTypeId,
                         InvoiceId = invoiceTable.InvoiceId,
                         DocumentId = invoiceTable.DocumentId,
                         BuyerInvoiceId = buyerInvoiceTable != null ? buyerInvoiceTable.BuyerInvoiceId : "",
                         BuyerInvoiceDate = buyerInvoiceTable.InvoiceDate,
                         PurchaseDate = purchaseTable != null ?
                                         purchaseTable.PurchaseDate : purchaseTableLine.PurchaseDate,
                         WithdrawalDate = withdrawalTable.WithdrawalDate != null ?
                                             withdrawalTable.WithdrawalDate : withdrawalTableLine.WithdrawalDate,
                         InterestDate = (invoiceTable.ProductInvoice == true && invoiceTable.ProductType == (int)ProductType.Factoring) ?
                                         purchaseLine.InterestDate :
                                         (invoiceTable.ProductInvoice == true && invoiceTable.ProductType == (int)ProductType.ProjectFinance) ? 
                                          withdrawalLine.InterestDate : 
                                            invoiceTable.DueDate,
                         AsOfDate = asOfDate,
                         InvoiceAmount = (invoiceTable.ProductInvoice == true && invoiceTable.ProductType == (int)ProductType.Factoring) ?
                                            purchaseLine.BuyerInvoiceAmount :
                                            (invoiceTable.ProductInvoice == true && invoiceTable.ProductType == (int)ProductType.ProjectFinance) ? 
                                            invoiceTable.InvoiceAmount : 
                                            0,
                         PurchaseAmount = (invoiceTable.ProductInvoice == true && invoiceTable.ProductType == (int)ProductType.Factoring && purchaseLine.RefPurchaseLineGUID == null ) ?
                                            purchaseLine.LinePurchaseAmount :
                                            (invoiceTable.ProductInvoice == true && invoiceTable.ProductType == (int)ProductType.Factoring && purchaseLine.RefPurchaseLineGUID != null) ? 
                                            purchaseLine.PurchaseAmount :
                                            0,
                         ReserveAmount = purchaseLine != null ? purchaseLine.ReserveAmount : 0,
                         FeeBalance = (invoiceTable.ProductInvoice == true && invoiceTable.ProductType == (int)ProductType.Factoring) ? 
                                        (purchaseLine.BillingFeeAmount + purchaseLine.ReceiptFeeAmount) : 0,
                         ResponsibleName = invoiceTable.ProductInvoice == true ? employeeTable.Name : "",
                         BusinessUnitId = invoiceTable.ProductInvoice == true ? businessUnit.BusinessUnitId : "",
                         BusinessTypeDesc = invoiceTable.ProductInvoice == true ? businessType.Description : "",
                         CreditAppId = invoiceTable.ProductInvoice == true ? creditAppTable.CreditAppId : "",
                         MainAgreementId = invoiceTable.ProductInvoice == true ? 
                                           (from mainAgreementTable in db.Set<MainAgreementTable>()
                                            join mainAgmStatus in db.Set<DocumentStatus>().Where(w => w.StatusId == ((int)MainAgreementStatus.Signed).ToString())
                                            on mainAgreementTable.DocumentStatusGUID equals mainAgmStatus.DocumentStatusGUID
                                            where mainAgreementTable.CreditAppTableGUID == invoiceTable.CreditAppTableGUID
                                            select mainAgreementTable.MainAgreementId).FirstOrDefault() : "",
                     }).AsNoTracking();

                var queryPaymHistory = (from paymHistory in db.Set<PaymentHistory>()
                                        join queryInvoice in query
                                        on paymHistory.InvoiceTableGUID equals queryInvoice.InvoiceTableGUID
                                        join invoiceSettlementDetail in db.Set<InvoiceSettlementDetail>()
                                        on paymHistory.InvoiceSettlementDetailGUID equals invoiceSettlementDetail.InvoiceSettlementDetailGUID into ljInvSettlemetDetail
                                        from invoiceSettlementDetail in ljInvSettlemetDetail.DefaultIfEmpty()
                                        where paymHistory.Cancel == false
                                           && paymHistory.PaymentDate <= asOfDate
                                        select new { paymHistory, invoiceSettlementDetail });

                List<CustomerAgingViewMap> view = query.ToList();
                view = view.Select(s =>
                        {
                            s.Days = Convert.ToInt32((asOfDate - s.InterestDate).TotalDays);
                            s.ReserveAmount = s.PurchaseAmount != 0 ? s.ReserveAmount : 0;
                            s.AROutstanding = s.CustTrans_TransAmount - queryPaymHistory.Where(w => w.paymHistory.InvoiceTableGUID == s.InvoiceTableGUID)
                                                                        .Sum(s => s.paymHistory.PaymentAmount);
                            s.PurchaseOutstanding = s.PurchaseAmount - queryPaymHistory.Where(w => w.invoiceSettlementDetail.InvoiceTableGUID == s.InvoiceTableGUID)
                                                                        .Sum(s => s.invoiceSettlementDetail.SettlePurchaseAmount);
                            s.UnearnedInterestBalance = (from intRealizeTrans in db.Set<InterestRealizedTrans>()
                                                            where intRealizeTrans.RefGUID == s.InvoiceTable_RefGUID
                                                            && intRealizeTrans.AccountingDate > asOfDate
                                                            select intRealizeTrans.AccInterestAmount).Sum();
                            //s.AccruedIntBalance = 0;
                            return s;
                        }).ToList();

                view = view.Select(s =>
                {
                    s.NetAR = (s.ProductInvoice == true && s.ProductType == (int)ProductType.Factoring) ?
                                (s.PurchaseOutstanding - s.UnearnedInterestBalance - s.FeeBalance + s.AccruedIntBalance) :
                                (s.ProductInvoice == true && s.ProductType == (int)ProductType.ProjectFinance) ?
                                (s.AROutstanding - s.UnearnedInterestBalance - s.FeeBalance + s.AccruedIntBalance) :
                                0;
                    return s;
                }).ToList();

                return view;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Invoice for customer aging
    }
}

