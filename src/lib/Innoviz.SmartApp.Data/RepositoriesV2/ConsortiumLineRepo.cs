using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IConsortiumLineRepo
	{
		#region DropDown
		IEnumerable<SelectItem<ConsortiumLineItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<ConsortiumLineListView> GetListvw(SearchParameter search);
		ConsortiumLineItemView GetByIdvw(Guid id);
		ConsortiumLine Find(params object[] keyValues);
		ConsortiumLine GetConsortiumLineByIdNoTracking(Guid guid);
		ConsortiumLine CreateConsortiumLine(ConsortiumLine consortiumLine);
		void CreateConsortiumLineVoid(ConsortiumLine consortiumLine);
		ConsortiumLine UpdateConsortiumLine(ConsortiumLine dbConsortiumLine, ConsortiumLine inputConsortiumLine, List<string> skipUpdateFields = null);
		void UpdateConsortiumLineVoid(ConsortiumLine dbConsortiumLine, ConsortiumLine inputConsortiumLine, List<string> skipUpdateFields = null);
		void Remove(ConsortiumLine item);
		IEnumerable<ConsortiumLine> GetConsortiumLineByConsortiumTableNoTracking(Guid ConsortiumTableGUID);
		void ValidateAdd(ConsortiumLine item);
		void ValidateAdd(IEnumerable<ConsortiumLine> items);
	}
	public class ConsortiumLineRepo : CompanyBaseRepository<ConsortiumLine>, IConsortiumLineRepo
	{
		public ConsortiumLineRepo(SmartAppDbContext context) : base(context) { }
		public ConsortiumLineRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ConsortiumLine GetConsortiumLineByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ConsortiumLineGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<ConsortiumLineItemViewMap> GetDropDownQuery()
		{
			return (from consortiumLine in Entity
					select new ConsortiumLineItemViewMap
					{
						CompanyGUID = consortiumLine.CompanyGUID,
						Owner = consortiumLine.Owner,
						OwnerBusinessUnitGUID = consortiumLine.OwnerBusinessUnitGUID,
						ConsortiumLineGUID = consortiumLine.ConsortiumLineGUID,
						Ordering = consortiumLine.Ordering,
						CustomerName = consortiumLine.CustomerName,
						Remark = consortiumLine.Remark,
						OperatedBy = consortiumLine.OperatedBy,
						Address = consortiumLine.Address,
						AuthorizedPersonTypeGUID = consortiumLine.AuthorizedPersonTypeGUID,
						ConsortiumTableGUID = consortiumLine.ConsortiumTableGUID
					});
		}
		public IEnumerable<SelectItem<ConsortiumLineItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<ConsortiumLine>(search, SysParm.CompanyGUID);
				var consortiumLine = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<ConsortiumLineItemViewMap, ConsortiumLineItemView>().ToDropDownItem(search.ExcludeRowData);


				return consortiumLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<ConsortiumLineListViewMap> GetListQuery()
		{
			return (from consortiumLine in Entity
				select new ConsortiumLineListViewMap
				{
						CompanyGUID = consortiumLine.CompanyGUID,
						Owner = consortiumLine.Owner,
						OwnerBusinessUnitGUID = consortiumLine.OwnerBusinessUnitGUID,
						ConsortiumLineGUID = consortiumLine.ConsortiumLineGUID,
						CustomerName = consortiumLine.CustomerName,
						OperatedBy = consortiumLine.OperatedBy,
						Position = consortiumLine.Position,
						ProportionOfShareholderPct = consortiumLine.ProportionOfShareholderPct,
						IsMain = consortiumLine.IsMain,
						ConsortiumTableGUID = consortiumLine.ConsortiumTableGUID,
						Ordering = consortiumLine.Ordering
				});
		}
		public SearchResult<ConsortiumLineListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<ConsortiumLineListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<ConsortiumLine>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<ConsortiumLineListViewMap, ConsortiumLineListView>();
				result = list.SetSearchResult<ConsortiumLineListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<ConsortiumLineItemViewMap> GetItemQuery()
		{
			return (from consortiumLine in Entity
					join company in db.Set<Company>()
					on consortiumLine.CompanyGUID equals company.CompanyGUID
					join consortiumTable in db.Set<ConsortiumTable>()
					on consortiumLine.ConsortiumTableGUID equals consortiumTable.ConsortiumTableGUID
					join ownerBU in db.Set<BusinessUnit>()
					on consortiumLine.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljConsortiumLineOwnerBU
					from ownerBU in ljConsortiumLineOwnerBU.DefaultIfEmpty()
					select new ConsortiumLineItemViewMap
					{
						CompanyGUID = consortiumLine.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = consortiumLine.Owner,
						OwnerBusinessUnitGUID = consortiumLine.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = consortiumLine.CreatedBy,
						CreatedDateTime = consortiumLine.CreatedDateTime,
						ModifiedBy = consortiumLine.ModifiedBy,
						ModifiedDateTime = consortiumLine.ModifiedDateTime,
						ConsortiumLineGUID = consortiumLine.ConsortiumLineGUID,
						Address = consortiumLine.Address,
						AuthorizedPersonTypeGUID = consortiumLine.AuthorizedPersonTypeGUID,
						ConsortiumTableGUID = consortiumLine.ConsortiumTableGUID,
						CustomerName = consortiumLine.CustomerName,
						IsMain = consortiumLine.IsMain,
						OperatedBy = consortiumLine.OperatedBy,
						Position = consortiumLine.Position,
						ProportionOfShareholderPct = consortiumLine.ProportionOfShareholderPct,
						Remark = consortiumLine.Remark,
						ConsortiumTable_Values = SmartAppUtil.GetDropDownLabel(consortiumTable.ConsortiumId, consortiumTable.Description),
						Ordering = consortiumLine.Ordering,
					
						RowVersion = consortiumLine.RowVersion,
					});
		}
		public ConsortiumLineItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<ConsortiumLineItemViewMap, ConsortiumLineItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public ConsortiumLine CreateConsortiumLine(ConsortiumLine consortiumLine)
		{
			try
			{
				consortiumLine.ConsortiumLineGUID = Guid.NewGuid();
				base.Add(consortiumLine);
				return consortiumLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateConsortiumLineVoid(ConsortiumLine consortiumLine)
		{
			try
			{
				CreateConsortiumLine(consortiumLine);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ConsortiumLine UpdateConsortiumLine(ConsortiumLine dbConsortiumLine, ConsortiumLine inputConsortiumLine, List<string> skipUpdateFields = null)
		{
			try
			{
				dbConsortiumLine = dbConsortiumLine.MapUpdateValues<ConsortiumLine>(inputConsortiumLine);
				base.Update(dbConsortiumLine);
				return dbConsortiumLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateConsortiumLineVoid(ConsortiumLine dbConsortiumLine, ConsortiumLine inputConsortiumLine, List<string> skipUpdateFields = null)
		{
			try
			{
				dbConsortiumLine = dbConsortiumLine.MapUpdateValues<ConsortiumLine>(inputConsortiumLine, skipUpdateFields);
				base.Update(dbConsortiumLine);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete

		#region Validate
		public void CheckDuplicateIsMain(ConsortiumLine item)
		{
			try
			{
				SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
				bool isDupplicate = Entity.Any(a => (a.CompanyGUID == item.CompanyGUID)
											     && (a.IsMain == true)
												 && (a.IsMain == item.IsMain)
												 && (a.ConsortiumTableGUID == item.ConsortiumTableGUID)
												 && (a.ConsortiumLineGUID != item.ConsortiumLineGUID));
				if (isDupplicate)
				{
					smartAppException.AddData("ERROR.DUPLICATE", new string[] { "LABEL.CONSORTIUM_ID", "LABEL.IS_MAIN" });
				}

				if (smartAppException.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(smartAppException);
				}
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public override void ValidateAdd(ConsortiumLine item)
		{
			base.ValidateAdd(item);
			CheckDuplicateIsMain(item);
		}
		public override void ValidateUpdate(ConsortiumLine item)
		{
			base.ValidateUpdate(item);
			CheckDuplicateIsMain(item);
		}
		#endregion
		public IEnumerable<ConsortiumLine> GetConsortiumLineByConsortiumTableNoTracking(Guid ConsortiumTableGUID)
		{
			try
			{
				var result = Entity.Where(item => item.ConsortiumTableGUID == ConsortiumTableGUID).AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

