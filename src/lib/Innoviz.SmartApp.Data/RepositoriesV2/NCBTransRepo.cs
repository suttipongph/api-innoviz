using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface INCBTransRepo
	{
		#region DropDown
		IEnumerable<SelectItem<NCBTransItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<NCBTransListView> GetListvw(SearchParameter search);
		NCBTransItemView GetByIdvw(Guid id);
		NCBTrans Find(params object[] keyValues);
		NCBTrans GetNCBTransByIdNoTracking(Guid guid);
		NCBTrans CreateNCBTrans(NCBTrans ncbTrans);
		void CreateNCBTransVoid(NCBTrans ncbTrans);
		NCBTrans UpdateNCBTrans(NCBTrans dbNCBTrans, NCBTrans inputNCBTrans, List<string> skipUpdateFields = null);
		void UpdateNCBTransVoid(NCBTrans dbNCBTrans, NCBTrans inputNCBTrans, List<string> skipUpdateFields = null);
		void Remove(NCBTrans item);
		NCBTrans GetNCBTransByIdNoTrackingByAccessLevel(Guid guid);
		void ValidateAdd(NCBTrans item);
		void ValidateAdd(IEnumerable<NCBTrans> items);
	}
	public class NCBTransRepo : CompanyBaseRepository<NCBTrans>, INCBTransRepo
	{
		public NCBTransRepo(SmartAppDbContext context) : base(context) { }
		public NCBTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public NCBTrans GetNCBTransByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.NCBTransGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<NCBTransItemViewMap> GetDropDownQuery()
		{
			return (from ncbTrans in Entity
					select new NCBTransItemViewMap
					{
						CompanyGUID = ncbTrans.CompanyGUID,
						Owner = ncbTrans.Owner,
						OwnerBusinessUnitGUID = ncbTrans.OwnerBusinessUnitGUID,
						NCBTransGUID = ncbTrans.NCBTransGUID,
						CreditTypeGUID = ncbTrans.CreditTypeGUID,
						CreditLimit = ncbTrans.CreditLimit
					});
		}
		public IEnumerable<SelectItem<NCBTransItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<NCBTrans>(search, SysParm.CompanyGUID);
				var ncbTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<NCBTransItemViewMap, NCBTransItemView>().ToDropDownItem(search.ExcludeRowData);


				return ncbTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<NCBTransListViewMap> GetListQuery()
		{
			return (from ncbTrans in Entity
					join creditType in db.Set<CreditType>()
					on ncbTrans.CreditTypeGUID equals creditType.CreditTypeGUID
					join bankGroup in db.Set<BankGroup>()
					on ncbTrans.BankGroupGUID equals bankGroup.BankGroupGUID
					join ncbAccountStatus in db.Set<NCBAccountStatus>()
					on ncbTrans.NCBAccountStatusGUID equals ncbAccountStatus.NCBAccountStatusGUID
					select new NCBTransListViewMap
				{
						CompanyGUID = ncbTrans.CompanyGUID,
						Owner = ncbTrans.Owner,
						OwnerBusinessUnitGUID = ncbTrans.OwnerBusinessUnitGUID,
						NCBTransGUID = ncbTrans.NCBTransGUID,
						CreditTypeGUID = ncbTrans.CreditTypeGUID,
						CreditLimit = ncbTrans.CreditLimit,
						OutstandingAR = ncbTrans.OutstandingAR,
						MonthlyRepayment = ncbTrans.MonthlyRepayment,
						EndDate = ncbTrans.EndDate,
						BankGroupGUID = ncbTrans.BankGroupGUID,
						NCBAccountStatusGUID = ncbTrans.NCBAccountStatusGUID,
						CreditType_Values = SmartAppUtil.GetDropDownLabel(creditType.CreditTypeId, creditType.Description),
						BankGroup_Values = SmartAppUtil.GetDropDownLabel(bankGroup.BankGroupId, bankGroup.Description),
						NCBAccountStatus_Values = SmartAppUtil.GetDropDownLabel(ncbAccountStatus.NCBAccStatusId, ncbAccountStatus.Description),
						CreditType_CreditTypeId = creditType.CreditTypeId,
						BankGroup_BankGroupId = bankGroup.BankGroupId,
						NcbAccountStatus_NCBAccStatusId = ncbAccountStatus.NCBAccStatusId,
						RefGUID = ncbTrans.RefGUID,
						CreatedDateTime = ncbTrans.CreatedDateTime
					});
		}
		public SearchResult<NCBTransListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<NCBTransListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<NCBTrans>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<NCBTransListViewMap, NCBTransListView>();
				result = list.SetSearchResult<NCBTransListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<NCBTransItemViewMap> GetItemQuery()
		{
			return (from ncbTrans in Entity
					join company in db.Set<Company>()
					on ncbTrans.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on ncbTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljNCBTransOwnerBU
					from ownerBU in ljNCBTransOwnerBU.DefaultIfEmpty()
					join creditAppRequestTable in db.Set<CreditAppRequestTable>()
					on ncbTrans.RefGUID equals creditAppRequestTable.CreditAppRequestTableGUID into ljCreditAppRequestTable
					from creditAppRequestTable in ljCreditAppRequestTable.DefaultIfEmpty()
					join authorizedPersonTrans in db.Set<AuthorizedPersonTrans>()
					on ncbTrans.RefGUID equals authorizedPersonTrans.AuthorizedPersonTransGUID into ljAuthorizedPersonTrans
					from authorizedPersonTrans in ljAuthorizedPersonTrans.DefaultIfEmpty()
					join relatedPersonTable in db.Set<RelatedPersonTable>()
					on authorizedPersonTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID into ljRelatedPersonTable
					from relatedPersonTable in ljRelatedPersonTable.DefaultIfEmpty()
					select new NCBTransItemViewMap
					{
						CompanyGUID = ncbTrans.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = ncbTrans.Owner,
						OwnerBusinessUnitGUID = ncbTrans.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = ncbTrans.CreatedBy,
						CreatedDateTime = ncbTrans.CreatedDateTime,
						ModifiedBy = ncbTrans.ModifiedBy,
						ModifiedDateTime = ncbTrans.ModifiedDateTime,
						NCBTransGUID = ncbTrans.NCBTransGUID,
						BankGroupGUID = ncbTrans.BankGroupGUID,
						CreditLimit = ncbTrans.CreditLimit,
						CreditTypeGUID = ncbTrans.CreditTypeGUID,
						EndDate = ncbTrans.EndDate,
						MonthlyRepayment = ncbTrans.MonthlyRepayment,
						NCBAccountStatusGUID = ncbTrans.NCBAccountStatusGUID,
						OutstandingAR = ncbTrans.OutstandingAR,
						RefGUID = ncbTrans.RefGUID,
						RefType = ncbTrans.RefType,
						RefId = (authorizedPersonTrans != null && ncbTrans.RefType == (int)RefType.AuthorizedPersonTrans) ? relatedPersonTable.RelatedPersonId :
								(creditAppRequestTable != null && ncbTrans.RefType == (int)RefType.CreditAppRequestTable) ? creditAppRequestTable.CreditAppRequestId :
								null,
					
						RowVersion = ncbTrans.RowVersion,
					});
		}
		public NCBTransItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<NCBTransItemViewMap, NCBTransItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public NCBTrans CreateNCBTrans(NCBTrans ncbTrans)
		{
			try
			{
				ncbTrans.NCBTransGUID = Guid.NewGuid();
				base.Add(ncbTrans);
				return ncbTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateNCBTransVoid(NCBTrans ncbTrans)
		{
			try
			{
				CreateNCBTrans(ncbTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public NCBTrans UpdateNCBTrans(NCBTrans dbNCBTrans, NCBTrans inputNCBTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbNCBTrans = dbNCBTrans.MapUpdateValues<NCBTrans>(inputNCBTrans);
				base.Update(dbNCBTrans);
				return dbNCBTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateNCBTransVoid(NCBTrans dbNCBTrans, NCBTrans inputNCBTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbNCBTrans = dbNCBTrans.MapUpdateValues<NCBTrans>(inputNCBTrans, skipUpdateFields);
				base.Update(dbNCBTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public NCBTrans GetNCBTransByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.NCBTransGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public override void ValidateAdd(IEnumerable<NCBTrans> items)
		{
			ValidateCreditLimit(items);
			base.ValidateAdd(items);
		}
		public override void ValidateAdd(NCBTrans item)
		{
			ValidateCreditLimit(item);
			base.ValidateAdd(item);
		}
		public override void ValidateUpdate(IEnumerable<NCBTrans> items)
		{
			ValidateCreditLimit(items);
			ValidateUpdate(items);
		}
		public override void ValidateUpdate(NCBTrans item)
		{
			ValidateCreditLimit(item);
			base.ValidateUpdate(item);
		}

		public void ValidateCreditLimit(NCBTrans item)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				if (item.CreditLimit <= 0)
				{
					ex.AddData("ERROR.GREATER_THAN_ZERO", new string[] { "LABEL.CREDIT_LIMIT"});
				}
				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public void ValidateCreditLimit(IEnumerable<NCBTrans> items)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				if (items.Any(t => t.CreditLimit <= 0))
				{
					ex.AddData("ERROR.GREATER_THAN_ZERO", new string[] { "LABEL.CREDIT_LIMIT" });
				}
				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
	}
}

