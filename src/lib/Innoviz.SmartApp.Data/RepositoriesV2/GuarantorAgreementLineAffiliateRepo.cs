using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IGuarantorAgreementLineAffiliateRepo
	{
		#region DropDown
		IEnumerable<SelectItem<GuarantorAgreementLineAffiliateItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<GuarantorAgreementLineAffiliateListView> GetListvw(SearchParameter search);
		bool GetByIdvwByGuarator(string id);
		GuarantorAgreementLineAffiliateItemView GetByIdvw(Guid id);
		GuarantorAgreementLineAffiliate Find(params object[] keyValues);
		GuarantorAgreementLineAffiliate GetGuarantorAgreementLineAffiliateByIdNoTracking(Guid guid);
		GuarantorAgreementLineAffiliate CreateGuarantorAgreementLineAffiliate(GuarantorAgreementLineAffiliate guarantorAgreementLineAffiliate);
		void CreateGuarantorAgreementLineAffiliateVoid(GuarantorAgreementLineAffiliate guarantorAgreementLineAffiliate);
		GuarantorAgreementLineAffiliate UpdateGuarantorAgreementLineAffiliate(GuarantorAgreementLineAffiliate dbGuarantorAgreementLineAffiliate, GuarantorAgreementLineAffiliate inputGuarantorAgreementLineAffiliate, List<string> skipUpdateFields = null);
		void UpdateGuarantorAgreementLineAffiliateVoid(GuarantorAgreementLineAffiliate dbGuarantorAgreementLineAffiliate, GuarantorAgreementLineAffiliate inputGuarantorAgreementLineAffiliate, List<string> skipUpdateFields = null);
		void Remove(GuarantorAgreementLineAffiliate item);
		GuarantorAgreementLineAffiliate GetGuarantorAgreementLineAffiliateByIdNoTrackingByAccessLevel(Guid guid);
		GuarantorAgreementLineAffiliateItemView GetGuarantorAgreementLineAffiliateInitialData(Guid id);
		void ValidateAdd(IEnumerable<GuarantorAgreementLineAffiliate> items);
		GuarantorAgreementLineAffiliate GetOrderingFromGuaratorAgreement(Guid id);
		List<GuarantorAgreementLineAffiliate> GetListByGuarantorAgreementLine(List<Guid> guarantorAgreementLineGUIDs);
		List<GuarantorAgreementLineAffiliate> GetListByGuarantorAgreementLine(Guid guarantorAgreementLineGUID);
	}
	public class GuarantorAgreementLineAffiliateRepo : CompanyBaseRepository<GuarantorAgreementLineAffiliate>, IGuarantorAgreementLineAffiliateRepo
	{
		public GuarantorAgreementLineAffiliateRepo(SmartAppDbContext context) : base(context) { }
		public GuarantorAgreementLineAffiliateRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public GuarantorAgreementLineAffiliate GetGuarantorAgreementLineAffiliateByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.GuarantorAgreementLineAffiliateGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<GuarantorAgreementLineAffiliateItemViewMap> GetDropDownQuery()
		{
			return (from guarantorAgreementLineAffiliate in Entity
					select new GuarantorAgreementLineAffiliateItemViewMap
					{
						CompanyGUID = guarantorAgreementLineAffiliate.CompanyGUID,
						Owner = guarantorAgreementLineAffiliate.Owner,
						OwnerBusinessUnitGUID = guarantorAgreementLineAffiliate.OwnerBusinessUnitGUID,
						GuarantorAgreementLineAffiliateGUID = guarantorAgreementLineAffiliate.GuarantorAgreementLineAffiliateGUID,
						Ordering = guarantorAgreementLineAffiliate.Ordering,
						CustomerTableGUID = guarantorAgreementLineAffiliate.CustomerTableGUID
					});
		}
		public IEnumerable<SelectItem<GuarantorAgreementLineAffiliateItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<GuarantorAgreementLineAffiliate>(search, SysParm.CompanyGUID);
				var guarantorAgreementLineAffiliate = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<GuarantorAgreementLineAffiliateItemViewMap, GuarantorAgreementLineAffiliateItemView>().ToDropDownItem(search.ExcludeRowData);


				return guarantorAgreementLineAffiliate;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		public bool GetByIdvwByGuarator(string id)
        {
			var result = (from guarantorAgreementLineAffiliate in Entity
							
						  where guarantorAgreementLineAffiliate.GuarantorAgreementLineGUID.ToString() == id

						  select new GuarantorAgreementLineAffiliateListViewMap
						  {
							  CompanyGUID = guarantorAgreementLineAffiliate.CompanyGUID,
							  Owner = guarantorAgreementLineAffiliate.Owner,
							  OwnerBusinessUnitGUID = guarantorAgreementLineAffiliate.OwnerBusinessUnitGUID,
							  GuarantorAgreementLineAffiliateGUID = guarantorAgreementLineAffiliate.GuarantorAgreementLineAffiliateGUID,
							  Ordering = guarantorAgreementLineAffiliate.Ordering,
							  CustomerTableGUID = guarantorAgreementLineAffiliate.CustomerTableGUID,
							  GuarantorAgreementLineGUID = guarantorAgreementLineAffiliate.GuarantorAgreementLineGUID
						  }).ToList().Count();
			return !(result>0);
        }
		private IQueryable<GuarantorAgreementLineAffiliateListViewMap> GetListQuery()
		{
			return (from guarantorAgreementLineAffiliate in Entity
					join customerTable in db.Set<CustomerTable>()
					on guarantorAgreementLineAffiliate.CustomerTableGUID equals customerTable.CustomerTableGUID into ljcustomerTable
					from customerTable in ljcustomerTable.DefaultIfEmpty()

					select new GuarantorAgreementLineAffiliateListViewMap
				{
						CompanyGUID = guarantorAgreementLineAffiliate.CompanyGUID,
						Owner = guarantorAgreementLineAffiliate.Owner,
						OwnerBusinessUnitGUID = guarantorAgreementLineAffiliate.OwnerBusinessUnitGUID,
						GuarantorAgreementLineAffiliateGUID = guarantorAgreementLineAffiliate.GuarantorAgreementLineAffiliateGUID,
						Ordering = guarantorAgreementLineAffiliate.Ordering,
						CustomerTableGUID = guarantorAgreementLineAffiliate.CustomerTableGUID,
						Customer_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
						GuarantorAgreementLineGUID  = guarantorAgreementLineAffiliate.GuarantorAgreementLineGUID
					});
		}
		public SearchResult<GuarantorAgreementLineAffiliateListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<GuarantorAgreementLineAffiliateListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<GuarantorAgreementLineAffiliate>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<GuarantorAgreementLineAffiliateListViewMap, GuarantorAgreementLineAffiliateListView>();
				result = list.SetSearchResult<GuarantorAgreementLineAffiliateListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<GuarantorAgreementLineAffiliateItemViewMap> GetItemQuery()
		{
			var result = (from guarantorAgreementLineAffiliate in Entity
					join company in db.Set<Company>()
					on guarantorAgreementLineAffiliate.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on guarantorAgreementLineAffiliate.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljGuarantorAgreementLineAffiliateOwnerBU
					from ownerBU in ljGuarantorAgreementLineAffiliateOwnerBU.DefaultIfEmpty()
					join guarantorAgreementLine in db.Set<GuarantorAgreementLine>()
					on guarantorAgreementLineAffiliate.GuarantorAgreementLineGUID equals guarantorAgreementLine.GuarantorAgreementLineGUID into ljguarantorAgreementLine
					from guarantorAgreementLine in ljguarantorAgreementLine.DefaultIfEmpty()
					join guarantorAgreementTable in db.Set<GuarantorAgreementTable>()
					on guarantorAgreementLine.GuarantorAgreementTableGUID equals guarantorAgreementTable.GuarantorAgreementTableGUID into ljguarantorAgreementTable
					from guarantorAgreementTable in ljguarantorAgreementTable.DefaultIfEmpty()
					join documentStatus in db.Set<DocumentStatus>()
					on guarantorAgreementTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus
				    from documentStatus in ljdocumentStatus.DefaultIfEmpty()
					join mainAgreement in db.Set<MainAgreementTable>()
					on guarantorAgreementTable.MainAgreementTableGUID equals mainAgreement.MainAgreementTableGUID into ljmainAgreement
					from mainAgreement in ljmainAgreement.DefaultIfEmpty()
					select new GuarantorAgreementLineAffiliateItemViewMap
					{
						CompanyGUID = guarantorAgreementLineAffiliate.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = guarantorAgreementLineAffiliate.Owner,
						OwnerBusinessUnitGUID = guarantorAgreementLineAffiliate.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = guarantorAgreementLineAffiliate.CreatedBy,
						CreatedDateTime = guarantorAgreementLineAffiliate.CreatedDateTime,
						ModifiedBy = guarantorAgreementLineAffiliate.ModifiedBy,
						ModifiedDateTime = guarantorAgreementLineAffiliate.ModifiedDateTime,
						GuarantorAgreementLineAffiliateGUID = guarantorAgreementLineAffiliate.GuarantorAgreementLineAffiliateGUID,
						CustomerTableGUID = guarantorAgreementLineAffiliate.CustomerTableGUID,
						GuarantorAgreementLineGUID = guarantorAgreementLineAffiliate.GuarantorAgreementLineGUID,
						MainAgreementTableGUID = guarantorAgreementLineAffiliate.MainAgreementTableGUID,
						Ordering = guarantorAgreementLineAffiliate.Ordering,
						DocumentStatus_StatusId = documentStatus.StatusId,
						Customer_CustomerGUID = mainAgreement.CustomerTableGUID.ToString(),
						MainAgreement_ProductType = mainAgreement.ProductType.ToString(),
						GuarantorAgreementLine_Values = SmartAppUtil.GetDropDownLabel(guarantorAgreementTable.InternalGuarantorAgreementId, guarantorAgreementLine.Name),
						GuarantorAgreementTable_ParentCompanyGUID = guarantorAgreementTable.ParentCompanyGUID.ToString(),

						RowVersion = guarantorAgreementLineAffiliate.RowVersion,
					});
			return result;
		}
		public GuarantorAgreementLineAffiliateItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<GuarantorAgreementLineAffiliateItemViewMap, GuarantorAgreementLineAffiliateItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public GuarantorAgreementLineAffiliate CreateGuarantorAgreementLineAffiliate(GuarantorAgreementLineAffiliate guarantorAgreementLineAffiliate)
		{
			try
			{
				guarantorAgreementLineAffiliate.GuarantorAgreementLineAffiliateGUID = Guid.NewGuid();
				base.Add(guarantorAgreementLineAffiliate);
				return guarantorAgreementLineAffiliate;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateGuarantorAgreementLineAffiliateVoid(GuarantorAgreementLineAffiliate guarantorAgreementLineAffiliate)
		{
			try
			{
				CreateGuarantorAgreementLineAffiliate(guarantorAgreementLineAffiliate);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public GuarantorAgreementLineAffiliate UpdateGuarantorAgreementLineAffiliate(GuarantorAgreementLineAffiliate dbGuarantorAgreementLineAffiliate, GuarantorAgreementLineAffiliate inputGuarantorAgreementLineAffiliate, List<string> skipUpdateFields = null)
		{
			try
			{
				dbGuarantorAgreementLineAffiliate = dbGuarantorAgreementLineAffiliate.MapUpdateValues<GuarantorAgreementLineAffiliate>(inputGuarantorAgreementLineAffiliate);
				base.Update(dbGuarantorAgreementLineAffiliate);
				return dbGuarantorAgreementLineAffiliate;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateGuarantorAgreementLineAffiliateVoid(GuarantorAgreementLineAffiliate dbGuarantorAgreementLineAffiliate, GuarantorAgreementLineAffiliate inputGuarantorAgreementLineAffiliate, List<string> skipUpdateFields = null)
		{
			try
			{
				dbGuarantorAgreementLineAffiliate = dbGuarantorAgreementLineAffiliate.MapUpdateValues<GuarantorAgreementLineAffiliate>(inputGuarantorAgreementLineAffiliate, skipUpdateFields);
				base.Update(dbGuarantorAgreementLineAffiliate);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public GuarantorAgreementLineAffiliate GetGuarantorAgreementLineAffiliateByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.GuarantorAgreementLineAffiliateGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region InitialData
		public GuarantorAgreementLineAffiliateItemView GetGuarantorAgreementLineAffiliateInitialData(Guid id)
		{
			var result = (from guarantorAgreementTable in db.Set<GuarantorAgreementTable>()
						  join guarantorAgreementLine in db.Set<GuarantorAgreementLine>()
						  on guarantorAgreementTable.GuarantorAgreementTableGUID equals guarantorAgreementLine.GuarantorAgreementTableGUID into ljguarantorAgreementLine
						  from guarantorAgreementLine in ljguarantorAgreementLine.DefaultIfEmpty()
						  join mainAgreement in db.Set<MainAgreementTable>()
						  on guarantorAgreementTable.MainAgreementTableGUID equals mainAgreement.MainAgreementTableGUID into ljmainAgreement
						  from mainAgreement in ljmainAgreement.DefaultIfEmpty()
						  where guarantorAgreementLine.GuarantorAgreementLineGUID == id


						  //from guarantorAgreementLineTable in db.Set<GuarantorAgreementLine>()
						  //join guarantorAgreementTable in db.Set<GuarantorAgreementTable>()
						  //on guarantorAgreementLineTable.GuarantorAgreementTableGUID equals guarantorAgreementTable.GuarantorAgreementTableGUID into ljguarantorAgreementTable
						  //from guarantorAgreementTable in ljguarantorAgreementTable.DefaultIfEmpty()
						  //where guarantorAgreementLineTable.GuarantorAgreementLineGUID == id
						  select new GuarantorAgreementLineAffiliateItemView
						  {
							  GuarantorAgreementLineGUID = guarantorAgreementLine.GuarantorAgreementLineGUID.GuidNullToString(),
							  Customer_CustomerGUID = mainAgreement.CustomerTableGUID.GuidNullToString(),
							  MainAgreement_ProductType = mainAgreement.ProductType.GuidNullToString(),
								GuarantorAgreementLine_Values = SmartAppUtil.GetDropDownLabel(guarantorAgreementTable.InternalGuarantorAgreementId, guarantorAgreementLine.Name),
							  GuarantorAgreementTable_ParentCompanyGUID = guarantorAgreementTable.ParentCompanyGUID.GuidNullToString()
						  }).FirstOrDefault();
			return result;
		}

        #endregion GetByIdvw
        #region Validate
        public override void ValidateAdd(IEnumerable<GuarantorAgreementLineAffiliate> items)
        {
            base.ValidateAdd(items);	
        }

		public GuarantorAgreementLineAffiliate GetOrderingFromGuaratorAgreement(Guid id)
        {
			try
			{
				var result = Entity.Where(item => item.GuarantorAgreementLineGUID == id)
					.OrderByDescending(or=>or.Ordering).AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Validate
		public List<GuarantorAgreementLineAffiliate> GetListByGuarantorAgreementLine(List<Guid> guarantorAgreementLineGUIDs)
		{
			try
			{
				var result = Entity.Where(w => guarantorAgreementLineGUIDs.Contains(w.GuarantorAgreementLineGUID))
									.AsNoTracking()
									.ToList();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<GuarantorAgreementLineAffiliate> GetListByGuarantorAgreementLine(Guid guarantorAgreementLineGUID)
		{
			try
			{
				var result = Entity.Where(w => w.GuarantorAgreementLineGUID == guarantorAgreementLineGUID)
									.AsNoTracking()
									.ToList();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}
