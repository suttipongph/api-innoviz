using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IAddressDistrictRepo
	{
		#region DropDown
		IEnumerable<SelectItem<AddressDistrictItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<AddressDistrictListView> GetListvw(SearchParameter search);
		AddressDistrictItemView GetByIdvw(Guid id);
		AddressDistrict Find(params object[] keyValues);
		AddressDistrict GetAddressDistrictByIdNoTracking(Guid guid);
		AddressDistrict CreateAddressDistrict(AddressDistrict addressDistrict);
		void CreateAddressDistrictVoid(AddressDistrict addressDistrict);
		AddressDistrict UpdateAddressDistrict(AddressDistrict dbAddressDistrict, AddressDistrict inputAddressDistrict, List<string> skipUpdateFields = null);
		void UpdateAddressDistrictVoid(AddressDistrict dbAddressDistrict, AddressDistrict inputAddressDistrict, List<string> skipUpdateFields = null);
		void Remove(AddressDistrict item);
		List<AddressDistrict> GetAddressDistrictByCompanyNoTracking(Guid companyGUID);
	}
	public class AddressDistrictRepo : CompanyBaseRepository<AddressDistrict>, IAddressDistrictRepo
	{
		public AddressDistrictRepo(SmartAppDbContext context) : base(context) { }
		public AddressDistrictRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public AddressDistrict GetAddressDistrictByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.AddressDistrictGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<AddressDistrictItemViewMap> GetDropDownQuery()
		{
			return (from addressDistrict in Entity
					select new AddressDistrictItemViewMap
					{
						CompanyGUID = addressDistrict.CompanyGUID,
						Owner = addressDistrict.Owner,
						OwnerBusinessUnitGUID = addressDistrict.OwnerBusinessUnitGUID,
						AddressDistrictGUID = addressDistrict.AddressDistrictGUID,
						DistrictId = addressDistrict.DistrictId,
						Name = addressDistrict.Name,
						AddressProvinceGUID = addressDistrict.AddressProvinceGUID
					});
		}
		public IEnumerable<SelectItem<AddressDistrictItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<AddressDistrict>(search, SysParm.CompanyGUID);
				var addressDistrict = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<AddressDistrictItemViewMap, AddressDistrictItemView>().ToDropDownItem(search.ExcludeRowData);


				return addressDistrict;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<AddressDistrictListViewMap> GetListQuery()
		{
			return (from addressDistrict in Entity
					join addressProvince in db.Set<AddressProvince>()
					on addressDistrict.AddressProvinceGUID equals addressProvince.AddressProvinceGUID into ljAddressProvince
					from addressProvince in ljAddressProvince.DefaultIfEmpty()
					select new AddressDistrictListViewMap
				{
						CompanyGUID = addressDistrict.CompanyGUID,
						Owner = addressDistrict.Owner,
						OwnerBusinessUnitGUID = addressDistrict.OwnerBusinessUnitGUID,
						AddressDistrictGUID = addressDistrict.AddressDistrictGUID,
						AddressProvinceGUID = addressDistrict.AddressProvinceGUID,
						DistrictId = addressDistrict.DistrictId,
						ProvinceId = addressProvince.ProvinceId,
						AddressProvince_Values = SmartAppUtil.GetDropDownLabel(addressProvince.ProvinceId ,addressProvince.Name),
						Name = addressDistrict.Name
				});
		}
		public SearchResult<AddressDistrictListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<AddressDistrictListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<AddressDistrict>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<AddressDistrictListViewMap, AddressDistrictListView>();
				result = list.SetSearchResult<AddressDistrictListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<AddressDistrictItemViewMap> GetItemQuery()
		{
			return (from addressDistrict in Entity
					join company in db.Set<Company>()
					on addressDistrict.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on addressDistrict.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljAddressDistrictOwnerBU
					from ownerBU in ljAddressDistrictOwnerBU.DefaultIfEmpty()
					select new AddressDistrictItemViewMap
					{
						CompanyGUID = addressDistrict.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = addressDistrict.Owner,
						OwnerBusinessUnitGUID = addressDistrict.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = addressDistrict.CreatedBy,
						CreatedDateTime = addressDistrict.CreatedDateTime,
						ModifiedBy = addressDistrict.ModifiedBy,
						ModifiedDateTime = addressDistrict.ModifiedDateTime,
						AddressDistrictGUID = addressDistrict.AddressDistrictGUID,
						AddressProvinceGUID = addressDistrict.AddressProvinceGUID,
						DistrictId = addressDistrict.DistrictId,
						Name = addressDistrict.Name,
					
						RowVersion = addressDistrict.RowVersion,
					});
		}
		public AddressDistrictItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<AddressDistrictItemViewMap, AddressDistrictItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public AddressDistrict CreateAddressDistrict(AddressDistrict addressDistrict)
		{
			try
			{
				addressDistrict.AddressDistrictGUID = Guid.NewGuid();
				base.Add(addressDistrict);
				return addressDistrict;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateAddressDistrictVoid(AddressDistrict addressDistrict)
		{
			try
			{
				CreateAddressDistrict(addressDistrict);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AddressDistrict UpdateAddressDistrict(AddressDistrict dbAddressDistrict, AddressDistrict inputAddressDistrict, List<string> skipUpdateFields = null)
		{
			try
			{
				dbAddressDistrict = dbAddressDistrict.MapUpdateValues<AddressDistrict>(inputAddressDistrict);
				base.Update(dbAddressDistrict);
				return dbAddressDistrict;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateAddressDistrictVoid(AddressDistrict dbAddressDistrict, AddressDistrict inputAddressDistrict, List<string> skipUpdateFields = null)
		{
			try
			{
				dbAddressDistrict = dbAddressDistrict.MapUpdateValues<AddressDistrict>(inputAddressDistrict, skipUpdateFields);
				base.Update(dbAddressDistrict);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public List<AddressDistrict> GetAddressDistrictByCompanyNoTracking(Guid companyGUID)
		{
			try
			{
				List<AddressDistrict> addressDistricts = Entity.Where(w => w.CompanyGUID == companyGUID).AsNoTracking().ToList();
				return addressDistricts;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

