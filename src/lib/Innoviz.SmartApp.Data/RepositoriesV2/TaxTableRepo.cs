using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ITaxTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<TaxTableItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<TaxTableListView> GetListvw(SearchParameter search);
		TaxTableItemView GetByIdvw(Guid id);
		TaxTable Find(params object[] keyValues);
		TaxTable GetTaxTableByIdNoTracking(Guid guid);
		List<TaxTable> GetTaxTableByIdNoTracking(List<Guid> guids);
		TaxTable CreateTaxTable(TaxTable taxTable);
		void CreateTaxTableVoid(TaxTable taxTable);
		TaxTable UpdateTaxTable(TaxTable dbTaxTable, TaxTable inputTaxTable, List<string> skipUpdateFields = null);
		void UpdateTaxTableVoid(TaxTable dbTaxTable, TaxTable inputTaxTable, List<string> skipUpdateFields = null);
		void Remove(TaxTable item);
		List<TaxTable> GetTaxTableByCompanyNoTracking(Guid companyGUID);
	}
	public class TaxTableRepo : CompanyBaseRepository<TaxTable>, ITaxTableRepo
	{
		public TaxTableRepo(SmartAppDbContext context) : base(context) { }
		public TaxTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public TaxTable GetTaxTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.TaxTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<TaxTable> GetTaxTableByIdNoTracking(List<Guid> guids)
		{
			try
			{
				var results = Entity.Where(w => guids.Contains(w.TaxTableGUID)).AsNoTracking().ToList();
				return results;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<TaxTableItemViewMap> GetDropDownQuery()
		{
			return (from taxTable in Entity
					select new TaxTableItemViewMap
					{
						CompanyGUID = taxTable.CompanyGUID,
						Owner = taxTable.Owner,
						OwnerBusinessUnitGUID = taxTable.OwnerBusinessUnitGUID,
						TaxTableGUID = taxTable.TaxTableGUID,
						TaxCode = taxTable.TaxCode,
						Description = taxTable.Description
					});
		}
		public IEnumerable<SelectItem<TaxTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<TaxTable>(search, SysParm.CompanyGUID);
				var taxTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<TaxTableItemViewMap, TaxTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return taxTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<TaxTableListViewMap> GetListQuery()
		{
			return (from taxTable in Entity
				select new TaxTableListViewMap
				{
						CompanyGUID = taxTable.CompanyGUID,
						Owner = taxTable.Owner,
						OwnerBusinessUnitGUID = taxTable.OwnerBusinessUnitGUID,
						TaxTableGUID = taxTable.TaxTableGUID,
						TaxCode = taxTable.TaxCode,
						Description = taxTable.Description
				});
		}
		public SearchResult<TaxTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<TaxTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<TaxTable>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<TaxTableListViewMap, TaxTableListView>();
				result = list.SetSearchResult<TaxTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<TaxTableItemViewMap> GetItemQuery()
		{
			return (from taxTable in Entity
					join company in db.Set<Company>()
					on taxTable.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on taxTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljTaxTableOwnerBU
					from ownerBU in ljTaxTableOwnerBU.DefaultIfEmpty()
					select new TaxTableItemViewMap
					{
						CompanyGUID = taxTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = taxTable.Owner,
						OwnerBusinessUnitGUID = taxTable.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = taxTable.CreatedBy,
						CreatedDateTime = taxTable.CreatedDateTime,
						ModifiedBy = taxTable.ModifiedBy,
						ModifiedDateTime = taxTable.ModifiedDateTime,
						TaxTableGUID = taxTable.TaxTableGUID,
						Description = taxTable.Description,
						InputTaxLedgerAccount = taxTable.InputTaxLedgerAccount,
						OutputTaxLedgerAccount = taxTable.OutputTaxLedgerAccount,
						PaymentTaxTableGUID = taxTable.PaymentTaxTableGUID,
						TaxCode = taxTable.TaxCode,
					
						RowVersion = taxTable.RowVersion,
					});
		}
		public TaxTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<TaxTableItemViewMap, TaxTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public TaxTable CreateTaxTable(TaxTable taxTable)
		{
			try
			{
				taxTable.TaxTableGUID = Guid.NewGuid();
				base.Add(taxTable);
				return taxTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateTaxTableVoid(TaxTable taxTable)
		{
			try
			{
				CreateTaxTable(taxTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public TaxTable UpdateTaxTable(TaxTable dbTaxTable, TaxTable inputTaxTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbTaxTable = dbTaxTable.MapUpdateValues<TaxTable>(inputTaxTable);
				base.Update(dbTaxTable);
				return dbTaxTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateTaxTableVoid(TaxTable dbTaxTable, TaxTable inputTaxTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbTaxTable = dbTaxTable.MapUpdateValues<TaxTable>(inputTaxTable, skipUpdateFields);
				base.Update(dbTaxTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public List<TaxTable> GetTaxTableByCompanyNoTracking(Guid companyGUID)
		{
			try
			{
				List<TaxTable> taxTables = Entity.Where(w => w.CompanyGUID == companyGUID).AsNoTracking().ToList();
				return taxTables;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

