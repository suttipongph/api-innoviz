using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IDocumentConditionTemplateLineRepo
	{
		#region DropDown
		IEnumerable<SelectItem<DocumentConditionTemplateLineItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<DocumentConditionTemplateLineListView> GetListvw(SearchParameter search);
		DocumentConditionTemplateLineItemView GetByIdvw(Guid id);
		DocumentConditionTemplateLine Find(params object[] keyValues);
		DocumentConditionTemplateLine GetDocumentConditionTemplateLineByIdNoTracking(Guid guid);
		DocumentConditionTemplateLine CreateDocumentConditionTemplateLine(DocumentConditionTemplateLine documentConditionTemplateLine);
		void CreateDocumentConditionTemplateLineVoid(DocumentConditionTemplateLine documentConditionTemplateLine);
		DocumentConditionTemplateLine UpdateDocumentConditionTemplateLine(DocumentConditionTemplateLine dbDocumentConditionTemplateLine, DocumentConditionTemplateLine inputDocumentConditionTemplateLine, List<string> skipUpdateFields = null);
		void UpdateDocumentConditionTemplateLineVoid(DocumentConditionTemplateLine dbDocumentConditionTemplateLine, DocumentConditionTemplateLine inputDocumentConditionTemplateLine, List<string> skipUpdateFields = null);
		void Remove(DocumentConditionTemplateLine item);
		#region function
		IEnumerable<DocumentConditionTemplateLine> GetDocumentConditionTemplateLineByDocumentConditionTemplateTableGUID(Guid guid);
		#endregion
	}
    public class DocumentConditionTemplateLineRepo : CompanyBaseRepository<DocumentConditionTemplateLine>, IDocumentConditionTemplateLineRepo
	{
		public DocumentConditionTemplateLineRepo(SmartAppDbContext context) : base(context) { }
		public DocumentConditionTemplateLineRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public DocumentConditionTemplateLine GetDocumentConditionTemplateLineByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.DocumentConditionTemplateLineGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<DocumentConditionTemplateLineItemViewMap> GetDropDownQuery()
		{
			return (from documentConditionTemplateLine in Entity
					select new DocumentConditionTemplateLineItemViewMap
					{
						CompanyGUID = documentConditionTemplateLine.CompanyGUID,
						Owner = documentConditionTemplateLine.Owner,
						OwnerBusinessUnitGUID = documentConditionTemplateLine.OwnerBusinessUnitGUID,
						DocumentConditionTemplateLineGUID = documentConditionTemplateLine.DocumentConditionTemplateLineGUID,
						DocumentTypeGUID = documentConditionTemplateLine.DocumentTypeGUID
					});
		}
		public IEnumerable<SelectItem<DocumentConditionTemplateLineItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<DocumentConditionTemplateLine>(search, SysParm.CompanyGUID);
				var documentConditionTemplateLine = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<DocumentConditionTemplateLineItemViewMap, DocumentConditionTemplateLineItemView>().ToDropDownItem(search.ExcludeRowData);


				return documentConditionTemplateLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<DocumentConditionTemplateLineListViewMap> GetListQuery()
		{
			return (from documentConditionTemplateLine in Entity
					join documentType in db.Set<DocumentType>()
					on documentConditionTemplateLine.DocumentTypeGUID equals documentType.DocumentTypeGUID into ljDocumentConditionTemplateLineDocumentType
					from documentType in ljDocumentConditionTemplateLineDocumentType.DefaultIfEmpty()
					select new DocumentConditionTemplateLineListViewMap
				{
						CompanyGUID = documentConditionTemplateLine.CompanyGUID,
						Owner = documentConditionTemplateLine.Owner,
						OwnerBusinessUnitGUID = documentConditionTemplateLine.OwnerBusinessUnitGUID,
						DocumentConditionTemplateLineGUID = documentConditionTemplateLine.DocumentConditionTemplateLineGUID,
						DocumentConditionTemplateTableGUID = documentConditionTemplateLine.DocumentConditionTemplateTableGUID,
						DocumentTypeGUID = documentConditionTemplateLine.DocumentTypeGUID,
						Mandatory = documentConditionTemplateLine.Mandatory,
						DocumentTypeId = documentType.DocumentTypeId,
						DocumentType_Values = SmartAppUtil.GetDropDownLabel(documentType.DocumentTypeId,documentType.Description),
				});
		}
		public SearchResult<DocumentConditionTemplateLineListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<DocumentConditionTemplateLineListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<DocumentConditionTemplateLine>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<DocumentConditionTemplateLineListViewMap, DocumentConditionTemplateLineListView>();
				result = list.SetSearchResult<DocumentConditionTemplateLineListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<DocumentConditionTemplateLineItemViewMap> GetItemQuery()
		{
			return (from documentConditionTemplateLine in Entity
					join company in db.Set<Company>()
					on documentConditionTemplateLine.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on documentConditionTemplateLine.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljDocumentConditionTemplateLineOwnerBU
					from ownerBU in ljDocumentConditionTemplateLineOwnerBU.DefaultIfEmpty()
					select new DocumentConditionTemplateLineItemViewMap
					{
						CompanyGUID = documentConditionTemplateLine.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = documentConditionTemplateLine.Owner,
						OwnerBusinessUnitGUID = documentConditionTemplateLine.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = documentConditionTemplateLine.CreatedBy,
						CreatedDateTime = documentConditionTemplateLine.CreatedDateTime,
						ModifiedBy = documentConditionTemplateLine.ModifiedBy,
						ModifiedDateTime = documentConditionTemplateLine.ModifiedDateTime,
						DocumentConditionTemplateLineGUID = documentConditionTemplateLine.DocumentConditionTemplateLineGUID,
						DocumentConditionTemplateTableGUID = documentConditionTemplateLine.DocumentConditionTemplateTableGUID,
						DocumentTypeGUID = documentConditionTemplateLine.DocumentTypeGUID,
						Mandatory = documentConditionTemplateLine.Mandatory,
					
						RowVersion = documentConditionTemplateLine.RowVersion,
					});
		}
		public DocumentConditionTemplateLineItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.ToMap<DocumentConditionTemplateLineItemViewMap, DocumentConditionTemplateLineItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public DocumentConditionTemplateLine CreateDocumentConditionTemplateLine(DocumentConditionTemplateLine documentConditionTemplateLine)
		{
			try
			{
				documentConditionTemplateLine.DocumentConditionTemplateLineGUID = Guid.NewGuid();
				base.Add(documentConditionTemplateLine);
				return documentConditionTemplateLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateDocumentConditionTemplateLineVoid(DocumentConditionTemplateLine documentConditionTemplateLine)
		{
			try
			{
				CreateDocumentConditionTemplateLine(documentConditionTemplateLine);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentConditionTemplateLine UpdateDocumentConditionTemplateLine(DocumentConditionTemplateLine dbDocumentConditionTemplateLine, DocumentConditionTemplateLine inputDocumentConditionTemplateLine, List<string> skipUpdateFields = null)
		{
			try
			{
				dbDocumentConditionTemplateLine = dbDocumentConditionTemplateLine.MapUpdateValues<DocumentConditionTemplateLine>(inputDocumentConditionTemplateLine);
				base.Update(dbDocumentConditionTemplateLine);
				return dbDocumentConditionTemplateLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateDocumentConditionTemplateLineVoid(DocumentConditionTemplateLine dbDocumentConditionTemplateLine, DocumentConditionTemplateLine inputDocumentConditionTemplateLine, List<string> skipUpdateFields = null)
		{
			try
			{
				dbDocumentConditionTemplateLine = dbDocumentConditionTemplateLine.MapUpdateValues<DocumentConditionTemplateLine>(inputDocumentConditionTemplateLine, skipUpdateFields);
				base.Update(dbDocumentConditionTemplateLine);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #endregion Create, Update, Delete
        #region function
		public IEnumerable<DocumentConditionTemplateLine> GetDocumentConditionTemplateLineByDocumentConditionTemplateTableGUID(Guid guid)
        {
			try
			{
				var result = Entity.Where(item => item.DocumentConditionTemplateTableGUID == guid).AsNoTracking().ToList();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}

