using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ILanguageRepo
	{
		#region DropDown
		IEnumerable<SelectItem<LanguageItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<LanguageListView> GetListvw(SearchParameter search);
		LanguageItemView GetByIdvw(Guid id);
		Language Find(params object[] keyValues);
		Language GetLanguageByIdNoTracking(Guid guid);
		Language CreateLanguage(Language language);
		void CreateLanguageVoid(Language language);
		Language UpdateLanguage(Language dbLanguage, Language inputLanguage, List<string> skipUpdateFields = null);
		void UpdateLanguageVoid(Language dbLanguage, Language inputLanguage, List<string> skipUpdateFields = null);
		void Remove(Language item);
	}
	public class LanguageRepo : CompanyBaseRepository<Language>, ILanguageRepo
	{
		public LanguageRepo(SmartAppDbContext context) : base(context) { }
		public LanguageRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public Language GetLanguageByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.LanguageGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<LanguageItemViewMap> GetDropDownQuery()
		{
			return (from language in Entity
					select new LanguageItemViewMap
					{
						CompanyGUID = language.CompanyGUID,
						Owner = language.Owner,
						OwnerBusinessUnitGUID = language.OwnerBusinessUnitGUID,
						LanguageGUID = language.LanguageGUID,
						LanguageId = language.LanguageId,
						Name = language.Name,
					});
		}
		public IEnumerable<SelectItem<LanguageItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<Language>(search, SysParm.CompanyGUID);
				var language = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<LanguageItemViewMap, LanguageItemView>().ToDropDownItem(search.ExcludeRowData);


				return language;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<LanguageListViewMap> GetListQuery()
		{
			return (from language in Entity
				select new LanguageListViewMap
				{
						CompanyGUID = language.CompanyGUID,
						Owner = language.Owner,
						OwnerBusinessUnitGUID = language.OwnerBusinessUnitGUID,
						LanguageGUID = language.LanguageGUID,
				});
		}
		public SearchResult<LanguageListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<LanguageListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<Language>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<LanguageListViewMap, LanguageListView>();
				result = list.SetSearchResult<LanguageListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<LanguageItemViewMap> GetItemQuery()
		{
			return (from language in Entity
					join company in db.Set<Company>()
					on language.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on language.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljLanguageOwnerBU
					from ownerBU in ljLanguageOwnerBU.DefaultIfEmpty()
					select new LanguageItemViewMap
					{
						CompanyGUID = language.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = language.Owner,
						OwnerBusinessUnitGUID = language.OwnerBusinessUnitGUID,
						CreatedBy = language.CreatedBy,
						CreatedDateTime = language.CreatedDateTime,
						ModifiedBy = language.ModifiedBy,
						ModifiedDateTime = language.ModifiedDateTime,
						LanguageGUID = language.LanguageGUID,
						LanguageId = language.LanguageId,
						Name = language.Name,
					
						RowVersion = language.RowVersion,
					});
		}
		public LanguageItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<LanguageItemViewMap, LanguageItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public Language CreateLanguage(Language language)
		{
			try
			{
				language.LanguageGUID = Guid.NewGuid();
				base.Add(language);
				return language;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateLanguageVoid(Language language)
		{
			try
			{
				CreateLanguage(language);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public Language UpdateLanguage(Language dbLanguage, Language inputLanguage, List<string> skipUpdateFields = null)
		{
			try
			{
				dbLanguage = dbLanguage.MapUpdateValues<Language>(inputLanguage);
				base.Update(dbLanguage);
				return dbLanguage;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateLanguageVoid(Language dbLanguage, Language inputLanguage, List<string> skipUpdateFields = null)
		{
			try
			{
				dbLanguage = dbLanguage.MapUpdateValues<Language>(inputLanguage, skipUpdateFields);
				base.Update(dbLanguage);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

