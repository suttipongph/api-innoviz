using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface IRetentionTransRepo
    {
        #region DropDown
        IEnumerable<SelectItem<RetentionTransItemView>> GetDropDownItem(SearchParameter search);
        #endregion DropDown
        SearchResult<RetentionTransListView> GetListvw(SearchParameter search);
        RetentionTransItemView GetByIdvw(Guid id);
        RetentionTrans Find(params object[] keyValues);
        RetentionTrans GetRetentionTransByIdNoTracking(Guid guid);
        RetentionTrans CreateRetentionTrans(RetentionTrans retentionTrans);
        void CreateRetentionTransVoid(RetentionTrans retentionTrans);
        RetentionTrans UpdateRetentionTrans(RetentionTrans dbRetentionTrans, RetentionTrans inputRetentionTrans, List<string> skipUpdateFields = null);
        void UpdateRetentionTransVoid(RetentionTrans dbRetentionTrans, RetentionTrans inputRetentionTrans, List<string> skipUpdateFields = null);
        void Remove(RetentionTrans item);
        RetentionTrans GetRetentionTransByIdNoTrackingByAccessLevel(Guid guid);
        List<RetentionTrans> GetRetentionTransByCreditAppTableGUIDNoTracking(Guid creditAppTableGUID);
        SearchResult<RetentionOutstandingView> GetRetentionOutstandingByCustomer(SearchParameter search);
        List<RetentionOutstandingView> GetRetentionOutstandingByCustomer(Guid customerTableGUID);
    }
    public class RetentionTransRepo : CompanyBaseRepository<RetentionTrans>, IRetentionTransRepo
    {
        public RetentionTransRepo(SmartAppDbContext context) : base(context) { }
        public RetentionTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public RetentionTrans GetRetentionTransByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.RetentionTransGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        private IQueryable<RetentionTransItemViewMap> GetDropDownQuery()
        {
            return (from retentionTrans in Entity
                    select new RetentionTransItemViewMap
                    {
                        CompanyGUID = retentionTrans.CompanyGUID,
                        Owner = retentionTrans.Owner,
                        OwnerBusinessUnitGUID = retentionTrans.OwnerBusinessUnitGUID,
                        RetentionTransGUID = retentionTrans.RetentionTransGUID
                    });
        }
        public IEnumerable<SelectItem<RetentionTransItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<RetentionTrans>(search, SysParm.CompanyGUID);
                var retentionTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<RetentionTransItemViewMap, RetentionTransItemView>().ToDropDownItem(search.ExcludeRowData);


                return retentionTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #region GetListvw
        private IQueryable<RetentionTransListViewMap> GetListQuery()
        {
            return (from retentionTrans in Entity

                    join creditappTable in db.Set<CreditAppTable>()
                    on retentionTrans.CreditAppTableGUID equals creditappTable.CreditAppTableGUID into ljRetention
                    from creditappTable in ljRetention.DefaultIfEmpty()

                    join buyerTable in db.Set<BuyerTable>()
                    on retentionTrans.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljBuyer
                    from buyerTable in ljBuyer.DefaultIfEmpty()

                    join buyerAgreementTable in db.Set<BuyerAgreementTable>()
                    on retentionTrans.BuyerAgreementTableGUID equals buyerAgreementTable.BuyerAgreementTableGUID into ljBuyerAgm
                    from buyerAgreementTable in ljBuyerAgm.DefaultIfEmpty()


                    select new RetentionTransListViewMap
                    {
                        CustomerTableGUID = retentionTrans.CustomerTableGUID,
                        CompanyGUID = retentionTrans.CompanyGUID,
                        Owner = retentionTrans.Owner,
                        OwnerBusinessUnitGUID = retentionTrans.OwnerBusinessUnitGUID,
                        RetentionTransGUID = retentionTrans.RetentionTransGUID,
                        CreditAppTableGUID = retentionTrans.CreditAppTableGUID,
                        ProductType = retentionTrans.ProductType,
                        TransDate = retentionTrans.TransDate,
                        Amount = retentionTrans.Amount,
                        BuyerTableGUID = retentionTrans.BuyerTableGUID,
                        BuyerAgreementTableGUID = retentionTrans.BuyerAgreementTableGUID,
                        RefType = retentionTrans.RefType,
                        DocumentId = retentionTrans.DocumentId,
                        CreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditappTable.CreditAppId, creditappTable.Description),
                        BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
                        BuyerAgreementTable_Values = SmartAppUtil.GetDropDownLabel(buyerAgreementTable.BuyerAgreementId, buyerAgreementTable.Description),
                        RefGUID = retentionTrans.RefGUID,
                        CreditAppTable_CreditAppId = creditappTable.CreditAppId,
                        BuyerTable_BuyerId = buyerTable.BuyerId,
                        BuyerAgreementTable_ReferenceAgreementID = buyerAgreementTable.ReferenceAgreementID,
                    });
        }
        public SearchResult<RetentionTransListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<RetentionTransListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<RetentionTrans>(search, SysParm.CompanyGUID);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<RetentionTransListViewMap, RetentionTransListView>();
                result = list.SetSearchResult<RetentionTransListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<RetentionTransItemViewMap> GetItemQuery()
        {
            return (from retentionTrans in Entity
                    join company in db.Set<Company>()
                    on retentionTrans.CompanyGUID equals company.CompanyGUID into ljCompany
                    from company in ljCompany.DefaultIfEmpty()

                    join ownerBU in db.Set<BusinessUnit>()
                    on retentionTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljRetentionTransOwnerBU
                    from ownerBU in ljRetentionTransOwnerBU.DefaultIfEmpty()


                    join creditappTable in db.Set<CreditAppTable>()
                    on retentionTrans.CreditAppTableGUID equals creditappTable.CreditAppTableGUID into ljRetention
                    from creditappTable in ljRetention.DefaultIfEmpty()

                    join buyerTable in db.Set<BuyerTable>()
                    on retentionTrans.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljBuyer
                    from buyerTable in ljBuyer.DefaultIfEmpty()

                    join buyerAgreementTable in db.Set<BuyerAgreementTable>()
                    on retentionTrans.BuyerAgreementTableGUID equals buyerAgreementTable.BuyerAgreementTableGUID into ljBuyerAgm
                    from buyerAgreementTable in ljBuyerAgm.DefaultIfEmpty()

                    join customerTable in db.Set<CustomerTable>()
                    on retentionTrans.CustomerTableGUID equals customerTable.CustomerTableGUID into ljcustomer
                    from customerTable in ljcustomer.DefaultIfEmpty()

                    join creditApprequestTable in db.Set<CreditAppRequestTable>()
                    on retentionTrans.RefGUID equals creditApprequestTable.CreditAppRequestTableGUID into ljcreditApprequestTable
                    from creditApprequestTable in ljcreditApprequestTable.DefaultIfEmpty()
                    select new RetentionTransItemViewMap
                    {
                        CompanyGUID = retentionTrans.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = retentionTrans.Owner,
                        OwnerBusinessUnitGUID = retentionTrans.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        CreatedBy = retentionTrans.CreatedBy,
                        CreatedDateTime = retentionTrans.CreatedDateTime,
                        ModifiedBy = retentionTrans.ModifiedBy,
                        ModifiedDateTime = retentionTrans.ModifiedDateTime,
                        RetentionTransGUID = retentionTrans.RetentionTransGUID,
                        Amount = retentionTrans.Amount,
                        BuyerAgreementTableGUID = retentionTrans.BuyerAgreementTableGUID,
                        BuyerTableGUID = retentionTrans.BuyerTableGUID,
                        CreditAppTableGUID = retentionTrans.CreditAppTableGUID,
                        CustomerTableGUID = retentionTrans.CustomerTableGUID,
                        DocumentId = retentionTrans.DocumentId,
                        ProductType = retentionTrans.ProductType,
                        RefGUID = retentionTrans.RefGUID,
                        RefType = retentionTrans.RefType,
                        TransDate = retentionTrans.TransDate,
                        CreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditappTable.CreditAppId, creditappTable.Description),
                        BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
                        BuyerAgreementTable_Values = SmartAppUtil.GetDropDownLabel(buyerAgreementTable.BuyerAgreementId, buyerAgreementTable.Description),
                        CustomerTable_Value = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                        RefID = creditApprequestTable != null ? SmartAppUtil.GetDropDownLabel(creditApprequestTable.CreditAppRequestId, creditApprequestTable.Description) : null,

                        RowVersion = retentionTrans.RowVersion,
                    });
        }
        public RetentionTransItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<RetentionTransItemViewMap, RetentionTransItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public RetentionTrans CreateRetentionTrans(RetentionTrans retentionTrans)
        {
            try
            {
                retentionTrans.RetentionTransGUID = retentionTrans.RetentionTransGUID == Guid.Empty ? Guid.NewGuid() : retentionTrans.RetentionTransGUID;
                base.Add(retentionTrans);
                return retentionTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateRetentionTransVoid(RetentionTrans retentionTrans)
        {
            try
            {
                CreateRetentionTrans(retentionTrans);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public RetentionTrans UpdateRetentionTrans(RetentionTrans dbRetentionTrans, RetentionTrans inputRetentionTrans, List<string> skipUpdateFields = null)
        {
            try
            {
                dbRetentionTrans = dbRetentionTrans.MapUpdateValues<RetentionTrans>(inputRetentionTrans);
                base.Update(dbRetentionTrans);
                return dbRetentionTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateRetentionTransVoid(RetentionTrans dbRetentionTrans, RetentionTrans inputRetentionTrans, List<string> skipUpdateFields = null)
        {
            try
            {
                dbRetentionTrans = dbRetentionTrans.MapUpdateValues<RetentionTrans>(inputRetentionTrans, skipUpdateFields);
                base.Update(dbRetentionTrans);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Create, Update, Delete
        public RetentionTrans GetRetentionTransByIdNoTrackingByAccessLevel(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.RetentionTransGUID == guid)
                    .FilterByAccessLevel(SysParm.AccessLevel)
                    .AsNoTracking()
                    .FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<RetentionTrans> GetRetentionTransByCreditAppTableGUIDNoTracking(Guid creditAppTableGUID)
        {
            try
            {
                var result = Entity.Where(item => item.CreditAppTableGUID == creditAppTableGUID)
                                .AsNoTracking()
                                .ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }



        public SearchResult<RetentionOutstandingView> GetRetentionOutstandingByCustomer(SearchParameter search)
        {
            var result = new SearchResult<RetentionOutstandingView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<RetentionOutstandingView>(search, SysParm.CompanyGUID);
                var total = GetRetentionOutstanding().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                List<RetentionOutstandingView> list = GetRetentionOutstanding().Where(predicate.Predicates, predicate.Values)
                                             .OrderBy(predicate.Sorting)
                                             .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                             .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                             .ToList();
                result = list.SetSearchResult<RetentionOutstandingView>(total, search);

                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        private IQueryable<RetentionOutstandingView> GetRetentionOutstanding()
        {
            var result = new SearchResult<RetentionTransListView>();
            try
            {
                return (from retentionTrans in Entity
                        join customerTable in db.Set<CustomerTable>() on retentionTrans.CustomerTableGUID equals customerTable.CustomerTableGUID
                        join creditAppTable in db.Set<CreditAppTable>()
                        on retentionTrans.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID
                        group new
                        {
                            retentionTrans,
                            customerTable,
                            creditAppTable
                        }
                        by new
                        {
                            retentionTrans.CompanyGUID,
                            retentionTrans.CustomerTableGUID,
                            retentionTrans.CreditAppTableGUID,
                            creditAppTable.CreditAppId,
                            retentionTrans.ProductType,
                            creditAppTable.MaxRetentionAmount,
                        }
                        into g
                        select new RetentionOutstandingView
                        {



                            CompanyGUID = g.Key.CompanyGUID,
                            CustomerTableGUID = g.Key.CustomerTableGUID,
                            ProductType = g.Key.ProductType,
                            AccumRetentionAmount = g.Sum(su => su.retentionTrans.Amount),
                            MaximumRetention = g.Key.MaxRetentionAmount,
                            RemainingAmount = (g.Key.MaxRetentionAmount > 0) ? (g.Key.MaxRetentionAmount - g.Sum(su => su.retentionTrans.Amount)) : 0,
                            CreditAppTableGUID = g.Key.CreditAppTableGUID,
                            CreditAppId = g.Key.CreditAppId
                        }
                );
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<RetentionOutstandingView> GetRetentionOutstandingByCustomer(Guid customerTableGUID)
        {
            try
            {
                return GetRetentionOutstanding().Where(w => w.CustomerTableGUID == customerTableGUID).ToList();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
