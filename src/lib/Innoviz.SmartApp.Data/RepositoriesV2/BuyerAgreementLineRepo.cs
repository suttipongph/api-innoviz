using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IBuyerAgreementLineRepo
	{
		#region DropDown
		IEnumerable<SelectItem<BuyerAgreementLineItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<BuyerAgreementLineListView> GetListvw(SearchParameter search);
		BuyerAgreementLineItemView GetByIdvw(Guid id);
		BuyerAgreementLine Find(params object[] keyValues);
		BuyerAgreementLine GetBuyerAgreementLineByIdNoTracking(Guid guid);
		IQueryable<BuyerAgreementLine> GetBuyerAgreementLineByBuyerAgreementTableIdNoTracking(Guid guid);
		BuyerAgreementLine CreateBuyerAgreementLine(BuyerAgreementLine buyerAgreementLine);
		void CreateBuyerAgreementLineVoid(BuyerAgreementLine buyerAgreementLine);
		BuyerAgreementLine UpdateBuyerAgreementLine(BuyerAgreementLine dbBuyerAgreementLine, BuyerAgreementLine inputBuyerAgreementLine, List<string> skipUpdateFields = null);
		void UpdateBuyerAgreementLineVoid(BuyerAgreementLine dbBuyerAgreementLine, BuyerAgreementLine inputBuyerAgreementLine, List<string> skipUpdateFields = null);
		void Remove(BuyerAgreementLine item);
		void ValidateAdd(BuyerAgreementLine item);
		void ValidateAdd(IEnumerable<BuyerAgreementLine> items);
	}
	public class BuyerAgreementLineRepo : CompanyBaseRepository<BuyerAgreementLine>, IBuyerAgreementLineRepo
	{
		public BuyerAgreementLineRepo(SmartAppDbContext context) : base(context) { }
		public BuyerAgreementLineRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BuyerAgreementLine GetBuyerAgreementLineByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BuyerAgreementLineGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IQueryable<BuyerAgreementLine> GetBuyerAgreementLineByBuyerAgreementTableIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BuyerAgreementTableGUID == guid).AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<BuyerAgreementLineItemViewMap> GetDropDownQuery()
		{
			return (from buyerAgreementLine in Entity
					select new BuyerAgreementLineItemViewMap
					{
						CompanyGUID = buyerAgreementLine.CompanyGUID,
						Owner = buyerAgreementLine.Owner,
						OwnerBusinessUnitGUID = buyerAgreementLine.OwnerBusinessUnitGUID,
						BuyerAgreementLineGUID = buyerAgreementLine.BuyerAgreementLineGUID,
						Period = buyerAgreementLine.Period,
						Description = buyerAgreementLine.Description,
						BuyerAgreementTableGUID = buyerAgreementLine.BuyerAgreementTableGUID,
						DueDate = buyerAgreementLine.DueDate,
						Amount = buyerAgreementLine.Amount
					});
		}
		public IEnumerable<SelectItem<BuyerAgreementLineItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<BuyerAgreementLine>(search, SysParm.CompanyGUID);
				var buyerAgreementLine = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<BuyerAgreementLineItemViewMap, BuyerAgreementLineItemView>().ToDropDownItem(search.ExcludeRowData);


				return buyerAgreementLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<BuyerAgreementLineListViewMap> GetListQuery()
		{
			return (from buyerAgreementLine in Entity
				select new BuyerAgreementLineListViewMap
				{
						CompanyGUID = buyerAgreementLine.CompanyGUID,
						Owner = buyerAgreementLine.Owner,
						OwnerBusinessUnitGUID = buyerAgreementLine.OwnerBusinessUnitGUID,
						BuyerAgreementLineGUID = buyerAgreementLine.BuyerAgreementLineGUID,
						Period = buyerAgreementLine.Period,
						Description = buyerAgreementLine.Description,
						Amount = buyerAgreementLine.Amount,
					    BuyerAgreementTableGUID = buyerAgreementLine.BuyerAgreementTableGUID,
				});
		}
		public SearchResult<BuyerAgreementLineListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<BuyerAgreementLineListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<BuyerAgreementLine>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<BuyerAgreementLineListViewMap, BuyerAgreementLineListView>();
				result = list.SetSearchResult<BuyerAgreementLineListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<BuyerAgreementLineItemViewMap> GetItemQuery()
		{
			return (from buyerAgreementLine in Entity
					join company in db.Set<Company>()
					on buyerAgreementLine.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on buyerAgreementLine.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljBuyerAgreementLineOwnerBU
					from ownerBU in ljBuyerAgreementLineOwnerBU.DefaultIfEmpty()
					join buyerAgreementTable in db.Set<BuyerAgreementTable>() on buyerAgreementLine.BuyerAgreementTableGUID equals buyerAgreementTable.BuyerAgreementTableGUID into ljBuyerAgreementLineBuyerAgreementTable
					from buyerAgreementTable in ljBuyerAgreementLineBuyerAgreementTable.DefaultIfEmpty()
					select new BuyerAgreementLineItemViewMap
					{
						CompanyGUID = buyerAgreementLine.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = buyerAgreementLine.Owner,
						OwnerBusinessUnitGUID = buyerAgreementLine.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = buyerAgreementLine.CreatedBy,
						CreatedDateTime = buyerAgreementLine.CreatedDateTime,
						ModifiedBy = buyerAgreementLine.ModifiedBy,
						ModifiedDateTime = buyerAgreementLine.ModifiedDateTime,
						BuyerAgreementLineGUID = buyerAgreementLine.BuyerAgreementLineGUID,
						AlreadyInvoiced = buyerAgreementLine.AlreadyInvoiced,
						Amount = buyerAgreementLine.Amount,
						BuyerAgreementTableGUID = buyerAgreementLine.BuyerAgreementTableGUID,
						Description = buyerAgreementLine.Description,
						DueDate = buyerAgreementLine.DueDate,
						Period = buyerAgreementLine.Period,
						Remark = buyerAgreementLine.Remark,
						BuyerAgreementTable_Values = SmartAppUtil.GetDropDownLabel(buyerAgreementTable.BuyerAgreementId, buyerAgreementTable.Description),
					
						RowVersion = buyerAgreementLine.RowVersion,
					});
		}
		public BuyerAgreementLineItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<BuyerAgreementLineItemViewMap, BuyerAgreementLineItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public BuyerAgreementLine CreateBuyerAgreementLine(BuyerAgreementLine buyerAgreementLine)
		{
			try
			{
				buyerAgreementLine.BuyerAgreementLineGUID = Guid.NewGuid();
				base.Add(buyerAgreementLine);
				return buyerAgreementLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateBuyerAgreementLineVoid(BuyerAgreementLine buyerAgreementLine)
		{
			try
			{
				CreateBuyerAgreementLine(buyerAgreementLine);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BuyerAgreementLine UpdateBuyerAgreementLine(BuyerAgreementLine dbBuyerAgreementLine, BuyerAgreementLine inputBuyerAgreementLine, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBuyerAgreementLine = dbBuyerAgreementLine.MapUpdateValues<BuyerAgreementLine>(inputBuyerAgreementLine);
				base.Update(dbBuyerAgreementLine);
				return dbBuyerAgreementLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateBuyerAgreementLineVoid(BuyerAgreementLine dbBuyerAgreementLine, BuyerAgreementLine inputBuyerAgreementLine, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBuyerAgreementLine = dbBuyerAgreementLine.MapUpdateValues<BuyerAgreementLine>(inputBuyerAgreementLine, skipUpdateFields);
				base.Update(dbBuyerAgreementLine);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

