using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IBusinessCollateralSubTypeRepo
	{
		#region DropDown
		IEnumerable<SelectItem<BusinessCollateralSubTypeItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<BusinessCollateralSubTypeListView> GetListvw(SearchParameter search);
		BusinessCollateralSubTypeItemView GetByIdvw(Guid id);
		BusinessCollateralSubType Find(params object[] keyValues);
		BusinessCollateralSubType GetBusinessCollateralSubTypeByIdNoTracking(Guid guid);
		BusinessCollateralSubType CreateBusinessCollateralSubType(BusinessCollateralSubType businessCollateralSubType);
		void CreateBusinessCollateralSubTypeVoid(BusinessCollateralSubType businessCollateralSubType);
		BusinessCollateralSubType UpdateBusinessCollateralSubType(BusinessCollateralSubType dbBusinessCollateralSubType, BusinessCollateralSubType inputBusinessCollateralSubType, List<string> skipUpdateFields = null);
		void UpdateBusinessCollateralSubTypeVoid(BusinessCollateralSubType dbBusinessCollateralSubType, BusinessCollateralSubType inputBusinessCollateralSubType, List<string> skipUpdateFields = null);
		void Remove(BusinessCollateralSubType item);
	}
	public class BusinessCollateralSubTypeRepo : CompanyBaseRepository<BusinessCollateralSubType>, IBusinessCollateralSubTypeRepo
	{
		public BusinessCollateralSubTypeRepo(SmartAppDbContext context) : base(context) { }
		public BusinessCollateralSubTypeRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BusinessCollateralSubType GetBusinessCollateralSubTypeByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BusinessCollateralSubTypeGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<BusinessCollateralSubTypeItemViewMap> GetDropDownQuery()
		{
			return (from businessCollateralSubType in Entity
					select new BusinessCollateralSubTypeItemViewMap
					{
						CompanyGUID = businessCollateralSubType.CompanyGUID,
						Owner = businessCollateralSubType.Owner,
						OwnerBusinessUnitGUID = businessCollateralSubType.OwnerBusinessUnitGUID,
						BusinessCollateralSubTypeGUID = businessCollateralSubType.BusinessCollateralSubTypeGUID,
						BusinessCollateralSubTypeId = businessCollateralSubType.BusinessCollateralSubTypeId,
						Description = businessCollateralSubType.Description,
						BusinessCollateralTypeGUID = businessCollateralSubType.BusinessCollateralTypeGUID,
					});
		}
		public IEnumerable<SelectItem<BusinessCollateralSubTypeItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<BusinessCollateralSubType>(search, SysParm.CompanyGUID);
				var businessCollateralSubType = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<BusinessCollateralSubTypeItemViewMap, BusinessCollateralSubTypeItemView>().ToDropDownItem(search.ExcludeRowData);


				return businessCollateralSubType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<BusinessCollateralSubTypeListViewMap> GetListQuery()
		{
			return (from businessCollateralSubType in Entity
					join businessCollateralType in db.Set<BusinessCollateralType>()
					on businessCollateralSubType.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID into ljBusinessCollateralType
					from businessCollateralType in ljBusinessCollateralType.DefaultIfEmpty()
					select new BusinessCollateralSubTypeListViewMap
				{
						CompanyGUID = businessCollateralSubType.CompanyGUID,
						Owner = businessCollateralSubType.Owner,
						OwnerBusinessUnitGUID = businessCollateralSubType.OwnerBusinessUnitGUID,
						BusinessCollateralSubTypeGUID = businessCollateralSubType.BusinessCollateralSubTypeGUID,
						BusinessCollateralTypeGUID = businessCollateralSubType.BusinessCollateralTypeGUID,
						BusinessCollateralSubTypeId = businessCollateralSubType.BusinessCollateralSubTypeId,
						Description = businessCollateralSubType.Description,
						AgreementOrdering = businessCollateralSubType.AgreementOrdering,
						BusinessCollateralType_Values = SmartAppUtil.GetDropDownLabel(businessCollateralType.BusinessCollateralTypeId, businessCollateralType.Description),
						BusinessCollateralTypeId = businessCollateralType.BusinessCollateralTypeId,
						DebtorClaims = businessCollateralSubType.DebtorClaims
				});
		}
		public SearchResult<BusinessCollateralSubTypeListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<BusinessCollateralSubTypeListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<BusinessCollateralSubType>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<BusinessCollateralSubTypeListViewMap, BusinessCollateralSubTypeListView>();
				result = list.SetSearchResult<BusinessCollateralSubTypeListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<BusinessCollateralSubTypeItemViewMap> GetItemQuery()
		{
			return (from businessCollateralSubType in Entity
					join company in db.Set<Company>()
					on businessCollateralSubType.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on businessCollateralSubType.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljBusinessCollateralSubTypeOwnerBU
					from ownerBU in ljBusinessCollateralSubTypeOwnerBU.DefaultIfEmpty()
					join businessCollateralType in db.Set<BusinessCollateralType>()
					on businessCollateralSubType.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID into ljBusinessCollateralType
					from businessCollateralType in ljBusinessCollateralType.DefaultIfEmpty()
					select new BusinessCollateralSubTypeItemViewMap
					{
						CompanyGUID = businessCollateralSubType.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = businessCollateralSubType.Owner,
						OwnerBusinessUnitGUID = businessCollateralSubType.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = businessCollateralSubType.CreatedBy,
						CreatedDateTime = businessCollateralSubType.CreatedDateTime,
						ModifiedBy = businessCollateralSubType.ModifiedBy,
						ModifiedDateTime = businessCollateralSubType.ModifiedDateTime,
						BusinessCollateralSubTypeGUID = businessCollateralSubType.BusinessCollateralSubTypeGUID,
						AgreementOrdering = businessCollateralSubType.AgreementOrdering,
						AgreementRefText = businessCollateralSubType.AgreementRefText,
						BusinessCollateralSubTypeId = businessCollateralSubType.BusinessCollateralSubTypeId,
						BusinessCollateralTypeGUID = businessCollateralSubType.BusinessCollateralTypeGUID,
						BusinessCollateralTypeId = businessCollateralType.BusinessCollateralTypeId,
						Description = businessCollateralSubType.Description,
						BusinessCollateralType_Values = SmartAppUtil.GetDropDownLabel(businessCollateralType.BusinessCollateralTypeId, businessCollateralType.Description),
						DebtorClaims = businessCollateralSubType.DebtorClaims,
					
						RowVersion = businessCollateralSubType.RowVersion,
					});
		}
		public BusinessCollateralSubTypeItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<BusinessCollateralSubTypeItemViewMap, BusinessCollateralSubTypeItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public BusinessCollateralSubType CreateBusinessCollateralSubType(BusinessCollateralSubType businessCollateralSubType)
		{
			try
			{
				businessCollateralSubType.BusinessCollateralSubTypeGUID = Guid.NewGuid();
				base.Add(businessCollateralSubType);
				return businessCollateralSubType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateBusinessCollateralSubTypeVoid(BusinessCollateralSubType businessCollateralSubType)
		{
			try
			{
				CreateBusinessCollateralSubType(businessCollateralSubType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BusinessCollateralSubType UpdateBusinessCollateralSubType(BusinessCollateralSubType dbBusinessCollateralSubType, BusinessCollateralSubType inputBusinessCollateralSubType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBusinessCollateralSubType = dbBusinessCollateralSubType.MapUpdateValues<BusinessCollateralSubType>(inputBusinessCollateralSubType);
				base.Update(dbBusinessCollateralSubType);
				return dbBusinessCollateralSubType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateBusinessCollateralSubTypeVoid(BusinessCollateralSubType dbBusinessCollateralSubType, BusinessCollateralSubType inputBusinessCollateralSubType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBusinessCollateralSubType = dbBusinessCollateralSubType.MapUpdateValues<BusinessCollateralSubType>(inputBusinessCollateralSubType, skipUpdateFields);
				base.Update(dbBusinessCollateralSubType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

