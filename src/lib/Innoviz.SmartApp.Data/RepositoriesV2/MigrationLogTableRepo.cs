﻿using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface IMigrationLogTableRepo
    {
        SearchResult<MigrationLogTableView> GetListvw(SearchParameter search, MigrationLogParm input);
    }
    public class MigrationLogTableRepo : BaseRepository<MigrationLogTable>, IMigrationLogTableRepo
    {
        public MigrationLogTableRepo(SmartAppDbContext context) : base(context) { }
        public MigrationLogTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public IQueryable<MigrationLogTableViewMap> GetListQuery(MigrationLogParm input)
        {
            try
            {
                DateTime migrationFromDate = input.MigrationFromDate.StringToDate();
                DateTime migrationToDate = input.MigrationToDate.StringToDate().AddDays(1).AddSeconds(-1);
                IMigrationTableRepo migrationTableRepo = new MigrationTableRepo(db);
                string migrationTable = migrationTableRepo.Find(input.MigrationTable.StringToGuid()).MigrateName;
                string pkgName = migrationTable + "Pkg";
                string functionName = migrationTable + "_";

                var result = (from migration in Entity
                              where migration.StartDateTime >= migrationFromDate &&
                                    migration.EndDateTime <= migrationToDate &&
                                    (migration.Source.StartsWith(pkgName) || migration.Source.StartsWith(functionName))
                              select new MigrationLogTableViewMap
                              {
                                  Id = migration.Id,
                                  MigrationName = migration.Source,
                                  LogStartDateTime = migration.StartDateTime,
                                  LogEndDateTime = migration.EndDateTime,
                                  LogMessage = migration.Message
                              });
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SearchResult<MigrationLogTableView> GetListvw(SearchParameter search, MigrationLogParm input)
        {
            var result = new SearchResult<MigrationLogTableView>();
            try
            {
                var predicate = search.GetPredicate_entity<MigrationLogTable>();
                var total = GetListQuery(input).Where(predicate.Predicates, predicate.Values)
                                                .AsNoTracking()
                                                .Count();
                var list = GetListQuery(input).Where(predicate.Predicates, predicate.Values)
                                                .OrderBy(predicate.Sorting)
                                                .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                                .Take(search.Paginator.Rows.GetPaginatorRows(total))
                                                .AsNoTracking()
                                                .ToMaps<MigrationLogTableViewMap, MigrationLogTableView>();
                result = list.SetSearchResult<MigrationLogTableView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
    }
}
