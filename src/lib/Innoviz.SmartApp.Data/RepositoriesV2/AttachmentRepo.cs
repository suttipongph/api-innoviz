using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IAttachmentRepo
	{
		#region DropDown
		IEnumerable<SelectItem<AttachmentItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<AttachmentListView> GetListvw(SearchParameter search);
		Task<AttachmentItemView> GetByIdvw(Guid id);
		Attachment Find(params object[] keyValues);
		Attachment GetAttachmentByIdNoTracking(Guid guid);
		Attachment CreateAttachment(Attachment attachment);
		void CreateAttachmentVoid(Attachment attachment);
		Attachment UpdateAttachment(Attachment dbAttachment, Attachment inputAttachment, List<string> skipUpdateFields = null);
		void UpdateAttachmentVoid(Attachment dbAttachment, Attachment inputAttachment, List<string> skipUpdateFields = null);
		void Remove(Attachment item);
		Attachment GetAttachmentByIdNoTrackingByAccessLevel(Guid guid);
		Attachment GetByReferenceAndFileName(int refType, Guid refGUID, string fileName);
		IEnumerable<Attachment> GetAttachmentByRefTypeNoTracking(Guid refGUID, int refType);
	}
	public class AttachmentRepo : CompanyBaseRepository<Attachment>, IAttachmentRepo
	{
		public AttachmentRepo(SmartAppDbContext context) : base(context) { }
		public AttachmentRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public Attachment GetAttachmentByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.AttachmentGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<AttachmentItemViewMap> GetDropDownQuery()
		{
			return (from attachment in Entity
					select new AttachmentItemViewMap
					{
                        AttachmentGUID = attachment.AttachmentGUID,
						FileName = attachment.FileName,
						FileDescription = attachment.FileDescription,
						RefType = attachment.RefType,
						RefGUID = attachment.RefGUID
					});
		}
		public IEnumerable<SelectItem<AttachmentItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<Attachment>(search);
				var attachment = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<AttachmentItemViewMap, AttachmentItemView>().ToDropDownItem(search.ExcludeRowData);


				return attachment;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<AttachmentListViewMap> GetListQuery()
		{
			return (from attachment in Entity
				select new AttachmentListViewMap
				{
						AttachmentGUID = attachment.AttachmentGUID,
						RefType = attachment.RefType,
						RefGUID = attachment.RefGUID,
						FileName = attachment.FileName,
						FileDescription = attachment.FileDescription
				});
		}
		public SearchResult<AttachmentListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<AttachmentListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<Attachment>(search);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<AttachmentListViewMap, AttachmentListView>();
				result = list.SetSearchResult<AttachmentListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<AttachmentItemViewMap> GetItemQuery()
		{
			return (from attachment in Entity
					join buyerTable in db.Set<BuyerTable>()
					on attachment.RefGUID equals buyerTable.BuyerTableGUID into ljBuyerTable
					from buyerTable in ljBuyerTable.DefaultIfEmpty()

					join customerTable in db.Set<CustomerTable>()
					on attachment.RefGUID equals customerTable.CustomerTableGUID into ljCustomerTable
					from customerTable in ljCustomerTable.DefaultIfEmpty()

					join creditAppRequestTable in db.Set<CreditAppRequestTable>()
					on attachment.RefGUID equals creditAppRequestTable.CreditAppRequestTableGUID into ljCreditAppRequestTable
					from creditAppRequestTable in ljCreditAppRequestTable.DefaultIfEmpty()

					join creditAppRequestLine in db.Set<CreditAppRequestLine>()
					on attachment.RefGUID equals creditAppRequestLine.CreditAppRequestLineGUID into ljCreditAppRequestLine
					from creditAppRequestLine in ljCreditAppRequestLine.DefaultIfEmpty()

					join creditAppTable in db.Set<CreditAppTable>()
					on attachment.RefGUID equals creditAppTable.CreditAppTableGUID into ljCreditAppTable
					from creditAppTable in ljCreditAppTable.DefaultIfEmpty()

					join creditAppLine in db.Set<CreditAppLine>()
					on attachment.RefGUID equals creditAppLine.CreditAppLineGUID into ljCreditAppLine
					from creditAppLine in ljCreditAppLine.DefaultIfEmpty()

					join assignmentAgreementTable in db.Set<AssignmentAgreementTable>()
					on attachment.RefGUID equals assignmentAgreementTable.AssignmentAgreementTableGUID into ljAssignmentAgreementTable
					from assignmentAgreementTable in ljAssignmentAgreementTable.DefaultIfEmpty()

					join messengerJobTable in db.Set<MessengerJobTable>()
					on attachment.RefGUID equals messengerJobTable.MessengerJobTableGUID into ljMessengerJobTable
					from messengerJobTable in ljMessengerJobTable.DefaultIfEmpty()

					join mainAgreementTable in db.Set<MainAgreementTable>()
					on attachment.RefGUID equals mainAgreementTable.MainAgreementTableGUID into ljMainAgreementTable
					from mainAgreementTable in ljMainAgreementTable.DefaultIfEmpty()

					join guarantorAgreementTable in db.Set<GuarantorAgreementTable>()
					on attachment.RefGUID equals guarantorAgreementTable.GuarantorAgreementTableGUID into ljGuarantorAgreementTable
					from guarantorAgreementTable in ljGuarantorAgreementTable.DefaultIfEmpty()

					join businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
					on attachment.RefGUID equals businessCollateralAgmTable.BusinessCollateralAgmTableGUID into ljBusinessCollateralAgmTable
					from businessCollateralAgmTable in ljBusinessCollateralAgmTable.DefaultIfEmpty()

                    join collectionFollowUp in db.Set<CollectionFollowUp>()
                    on attachment.RefGUID equals collectionFollowUp.CollectionFollowUpGUID into ljCollectionFollowUp
                    from collectionFollowUp in ljCollectionFollowUp.DefaultIfEmpty()

                    join projectProgressTable in db.Set<ProjectProgressTable>()
                    on attachment.RefGUID equals projectProgressTable.ProjectProgressTableGUID into ljProjectProgressTable
                    from projectProgressTable in ljProjectProgressTable.DefaultIfEmpty()

                    join customerRefundTable in db.Set<CustomerRefundTable>()
                    on attachment.RefGUID equals customerRefundTable.CustomerRefundTableGUID into ljCustomerRefundTable
                    from customerRefundTable in ljCustomerRefundTable.DefaultIfEmpty()

                    join purchaseTable in db.Set<PurchaseTable>()
					on attachment.RefGUID equals purchaseTable.PurchaseTableGUID into ljPurchaseTable
					from purchaseTable in ljPurchaseTable.DefaultIfEmpty()

					join receiptTempTable in db.Set<ReceiptTempTable>()
					on attachment.RefGUID equals receiptTempTable.ReceiptTempTableGUID into ljReceiptTempTable
					from receiptTempTable in ljReceiptTempTable.DefaultIfEmpty()

					join verificationTable in db.Set<VerificationTable>()
					on attachment.RefGUID equals verificationTable.VerificationTableGUID into ljVerificationTable
					from verificationTable in ljVerificationTable.DefaultIfEmpty()

					join withdrawalTable in db.Set<WithdrawalTable>()
					on attachment.RefGUID equals withdrawalTable.WithdrawalTableGUID into ljWithdrawalTable
					from withdrawalTable in ljWithdrawalTable.DefaultIfEmpty()

					join documentReturnTable in db.Set<DocumentReturnTable>()
					on attachment.RefGUID equals documentReturnTable.DocumentReturnTableGUID into ljDocumentReturnTable
					from documentReturnTable in ljDocumentReturnTable.DefaultIfEmpty()

					join freetextInvoice in db.Set<FreeTextInvoiceTable>()
					on attachment.RefGUID equals freetextInvoice.FreeTextInvoiceTableGUID into ljfreetextInvoice
					from freetextInvoice in ljfreetextInvoice.DefaultIfEmpty()
					select new AttachmentItemViewMap
					{
						CreatedBy = attachment.CreatedBy,
						CreatedDateTime = attachment.CreatedDateTime,
						ModifiedBy = attachment.ModifiedBy,
						ModifiedDateTime = attachment.ModifiedDateTime,
						AttachmentGUID = attachment.AttachmentGUID,
						Base64Data = attachment.Base64Data,
						FileDescription = attachment.FileDescription,
						FileName = attachment.FileName,
						FilePath = attachment.FilePath,
						ContentType = attachment.ContentType,
						RefGUID = attachment.RefGUID,
						RefType = attachment.RefType,
						RefId =
							(buyerTable != null && attachment.RefType == (int)RefType.Buyer) ? buyerTable.BuyerId :
							(customerTable != null && attachment.RefType == (int)RefType.Customer) ? customerTable.CustomerId :
							(creditAppRequestTable != null && attachment.RefType == (int)RefType.CreditAppRequestTable) ? creditAppRequestTable.CreditAppRequestId :
							(creditAppTable != null && attachment.RefType == (int)RefType.CreditAppTable) ? creditAppTable.CreditAppId :
							(assignmentAgreementTable != null && attachment.RefType == (int)RefType.AssignmentAgreement) ? assignmentAgreementTable.InternalAssignmentAgreementId :
							(messengerJobTable != null && attachment.RefType == (int)RefType.MessengerJob) ? messengerJobTable.JobId :
							(mainAgreementTable != null && attachment.RefType == (int)RefType.MainAgreement) ? mainAgreementTable.InternalMainAgreementId :
							(guarantorAgreementTable != null && attachment.RefType == (int)RefType.GuarantorAgreement) ? guarantorAgreementTable.InternalGuarantorAgreementId :
							(businessCollateralAgmTable != null && attachment.RefType == (int)RefType.BusinessCollateralAgreement) ? businessCollateralAgmTable.InternalBusinessCollateralAgmId :
							(projectProgressTable != null && attachment.RefType == (int)RefType.ProjectProgress) ? projectProgressTable.ProjectProgressId :
                            (customerRefundTable != null && attachment.RefType == (int)RefType.CustomerRefund) ? customerRefundTable.CustomerRefundId :
                            (purchaseTable != null && attachment.RefType == (int)RefType.PurchaseTable) ? purchaseTable.PurchaseId :
							(receiptTempTable != null && attachment.RefType == (int)RefType.ReceiptTemp) ? receiptTempTable.ReceiptTempId :
							(verificationTable != null && attachment.RefType == (int)RefType.Verification) ? verificationTable.VerificationId :
							(withdrawalTable != null && attachment.RefType == (int)RefType.WithdrawalTable) ? withdrawalTable.WithdrawalId : 
							 (documentReturnTable != null && attachment.RefType == (int)RefType.DocumentReturn) ? documentReturnTable.DocumentReturnId: 
							 (freetextInvoice != null && attachment.RefType == (int)RefType.FreeTextInvoice) ? freetextInvoice.FreeTextInvoiceId :
							 (collectionFollowUp != null && attachment.RefType == (int)RefType.CollectionFollowUp) ? collectionFollowUp.CollectionDate.DateToString() : null, 
						RefId_Int =
							(creditAppRequestLine != null && attachment.RefType == (int)RefType.CreditAppRequestLine) ? creditAppRequestLine.LineNum :
							(creditAppLine != null && attachment.RefType == (int)RefType.CreditAppLine) ? creditAppLine.LineNum : -1,

					
						RowVersion = attachment.RowVersion,
					});
		}
		public async Task<AttachmentItemView> GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<AttachmentItemViewMap, AttachmentItemView>();
				item.RefId = item.RefId != null ? item.RefId : item.RefId_Int != -1 ? item.RefId_Int.ToString() : null;
				item = await item.GetFileContent();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public Attachment CreateAttachment(Attachment attachment)
		{
			try
			{
				Guid newGuid = Guid.NewGuid();
				attachment.AttachmentGUID = newGuid;
				attachment.FilePath = newGuid.GuidNullToString() + FileEXT.DOCX_EXTENSION;
				ValidateNotNullContentAndFileName(attachment);
				attachment = attachment.EnsureFileCreated();
				base.Add(attachment);
				return attachment;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateAttachmentVoid(Attachment attachment)
		{
			try
			{
				CreateAttachment(attachment);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public Attachment UpdateAttachment(Attachment dbAttachment, Attachment inputAttachment, List<string> skipUpdateFields = null)
		{
			try
			{
				inputAttachment = inputAttachment.EnsureFileUpdated();
				dbAttachment = dbAttachment.MapUpdateValues<Attachment>(inputAttachment, skipUpdateFields);
				base.Update(dbAttachment);
				return dbAttachment;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateAttachmentVoid(Attachment dbAttachment, Attachment inputAttachment, List<string> skipUpdateFields = null)
		{
			try
			{
				inputAttachment = inputAttachment.EnsureFileUpdated();
				dbAttachment = dbAttachment.MapUpdateValues<Attachment>(inputAttachment, skipUpdateFields);
				base.Update(dbAttachment);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public Attachment GetAttachmentByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.AttachmentGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void ValidateNotNullContentAndFileName(Attachment model)
		{
			try
			{
				if (model.Base64Data == null || model.FileName == null)
				{
					SmartAppException ex = new SmartAppException("ERROR.00159");
					ex.AddData("ERROR.00160");
					throw SmartAppUtil.AddStackTrace(ex);
				}
				bool fileNameExisted = Entity.Any(item => item.FileName == model.FileName);
				if (fileNameExisted)
				{
					SmartAppException ex = new SmartAppException("ERROR.00159");
					ex.AddData("ERROR.00485", model.FileName);
					throw SmartAppUtil.AddStackTrace(ex);
				}

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public Attachment GetByReferenceAndFileName(int refType, Guid refGUID, string fileName)
        {
            try
            {
				return Entity.Where(w => w.RefGUID == refGUID && w.RefType == refType && w.FileName == fileName).AsNoTracking().FirstOrDefault();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<Attachment> GetAttachmentByRefTypeNoTracking(Guid refGUID,int refType)
		{
			try
			{
				var result = Entity.Where(item => item.RefGUID == refGUID && item.RefType == refType).AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

