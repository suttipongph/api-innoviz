using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IAssignmentAgreementSettleRepo
	{
		#region DropDown
		IEnumerable<SelectItem<AssignmentAgreementSettleItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<AssignmentAgreementSettleListView> GetListvw(SearchParameter search);
		AssignmentAgreementSettleItemView GetByIdvw(Guid id);
		AssignmentAgreementSettle Find(params object[] keyValues);
		AssignmentAgreementSettle GetAssignmentAgreementSettleByIdNoTracking(Guid guid);
		AssignmentAgreementSettle CreateAssignmentAgreementSettle(AssignmentAgreementSettle assignmentAgreementSettle);
		void CreateAssignmentAgreementSettleVoid(AssignmentAgreementSettle assignmentAgreementSettle);
		AssignmentAgreementSettle UpdateAssignmentAgreementSettle(AssignmentAgreementSettle dbAssignmentAgreementSettle, AssignmentAgreementSettle inputAssignmentAgreementSettle, List<string> skipUpdateFields = null);
		void UpdateAssignmentAgreementSettleVoid(AssignmentAgreementSettle dbAssignmentAgreementSettle, AssignmentAgreementSettle inputAssignmentAgreementSettle, List<string> skipUpdateFields = null);
		void Remove(AssignmentAgreementSettle item);
		AssignmentAgreementSettle GetAssignmentAgreementSettleByIdNoTrackingByAccessLevel(Guid guid);
		List<AssignmentAgreementSettle> GetAssignmentAgreementSettleByAssignmentAgreementTableGUIDNoTracking(Guid assignmentAgreementTableGUID);
		void ValidateAdd(IEnumerable<AssignmentAgreementSettle> items);
	}
	public class AssignmentAgreementSettleRepo : CompanyBaseRepository<AssignmentAgreementSettle>, IAssignmentAgreementSettleRepo
	{
		public AssignmentAgreementSettleRepo(SmartAppDbContext context) : base(context) { }
		public AssignmentAgreementSettleRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public AssignmentAgreementSettle GetAssignmentAgreementSettleByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.AssignmentAgreementSettleGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<AssignmentAgreementSettleItemViewMap> GetDropDownQuery()
		{
			return (from assignmentAgreementSettle in Entity
					select new AssignmentAgreementSettleItemViewMap
					{
						CompanyGUID = assignmentAgreementSettle.CompanyGUID,
						Owner = assignmentAgreementSettle.Owner,
						OwnerBusinessUnitGUID = assignmentAgreementSettle.OwnerBusinessUnitGUID,
						AssignmentAgreementSettleGUID = assignmentAgreementSettle.AssignmentAgreementSettleGUID
					});
		}
		public IEnumerable<SelectItem<AssignmentAgreementSettleItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<AssignmentAgreementSettle>(search, SysParm.CompanyGUID);
				var assignmentAgreementSettle = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<AssignmentAgreementSettleItemViewMap, AssignmentAgreementSettleItemView>().ToDropDownItem(search.ExcludeRowData);


				return assignmentAgreementSettle;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<AssignmentAgreementSettleListViewMap> GetListQuery()
		{
			return (from assignmentAgreementSettle in Entity
					join buyeragreement in db.Set<BuyerAgreementTable>()
					on assignmentAgreementSettle.BuyerAgreementTableGUID equals buyeragreement.BuyerAgreementTableGUID into ljbuyeragreement
					from buyeragreement in ljbuyeragreement.DefaultIfEmpty()
					select new AssignmentAgreementSettleListViewMap
				{
						CompanyGUID = assignmentAgreementSettle.CompanyGUID,
						Owner = assignmentAgreementSettle.Owner,
						OwnerBusinessUnitGUID = assignmentAgreementSettle.OwnerBusinessUnitGUID,
						AssignmentAgreementSettleGUID = assignmentAgreementSettle.AssignmentAgreementSettleGUID,
						SettledDate = assignmentAgreementSettle.SettledDate,
						SettledAmount = assignmentAgreementSettle.SettledAmount,
						BuyerAgreementTableGUID = assignmentAgreementSettle.BuyerAgreementTableGUID,
						BuyerAgreementTable_Values = SmartAppUtil.GetDropDownLabel(buyeragreement.BuyerAgreementId, buyeragreement.Description),
						RefGUID = assignmentAgreementSettle.RefGUID,
						AssignmentAgreementTableGUID = assignmentAgreementSettle.AssignmentAgreementTableGUID
				});
		}
		public SearchResult<AssignmentAgreementSettleListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<AssignmentAgreementSettleListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<AssignmentAgreementSettle>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<AssignmentAgreementSettleListViewMap, AssignmentAgreementSettleListView>();
				result = list.SetSearchResult<AssignmentAgreementSettleListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<AssignmentAgreementSettleItemViewMap> GetItemQuery()
		{
			var result = (from assignmentAgreementSettle in Entity

						  join company in db.Set<Company>()
							on assignmentAgreementSettle.CompanyGUID equals company.CompanyGUID into ljcompany
							from company in ljcompany.DefaultIfEmpty()
							join buyeragreement in db.Set<BuyerAgreementTable>()
							on assignmentAgreementSettle.BuyerAgreementTableGUID equals buyeragreement.BuyerAgreementTableGUID into ljbuyeragreement
							from buyeragreement in ljbuyeragreement.DefaultIfEmpty()

							join buyer in db.Set<BuyerTable>()
							on buyeragreement.BuyerTableGUID equals buyer.BuyerTableGUID into ljbuyer
							from buyer in ljbuyer.DefaultIfEmpty()

							join assigntable in db.Set<AssignmentAgreementTable>()
							on assignmentAgreementSettle.AssignmentAgreementTableGUID equals assigntable.AssignmentAgreementTableGUID into ljassigntable
							from assigntable in ljassigntable.DefaultIfEmpty()


							join ownerBU in db.Set<BusinessUnit>()
							on assignmentAgreementSettle.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljAssignmentAgreementSettleOwnerBU
							from ownerBU in ljAssignmentAgreementSettleOwnerBU.DefaultIfEmpty()

							join assignmentagreementtable in db.Set<AssignmentAgreementTable>() 
							on assignmentAgreementSettle.AssignmentAgreementTableGUID equals assignmentagreementtable.AssignmentAgreementTableGUID
							into ljAssignmentAgreementSeetleAssignmentAgreementTable 
							from assignmentagreementtable in ljAssignmentAgreementSeetleAssignmentAgreementTable.DefaultIfEmpty()

							join reasonTable in db.Set<DocumentReason>()
							on assignmentAgreementSettle.DocumentReasonGUID equals reasonTable.DocumentReasonGUID into ljreasonTable
							from reasonTable in ljreasonTable.DefaultIfEmpty()

							join refassignmenttable in db.Set<AssignmentAgreementSettle>()
							on assignmentAgreementSettle.RefAssignmentAgreementSettleGUID equals refassignmenttable.AssignmentAgreementSettleGUID into ljrefassignmenttable
							from refassignmenttable in ljrefassignmenttable.DefaultIfEmpty()

							join receiptTempTable in db.Set<ReceiptTempTable>()
							on assignmentAgreementSettle.RefGUID equals receiptTempTable.ReceiptTempTableGUID into ljreceiptTempTable
							from receiptTempTable in ljreceiptTempTable.DefaultIfEmpty()
							join invoiceTable in db.Set<InvoiceTable>()
							on assignmentAgreementSettle.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljinvoiceTable
							from invoiceTable in ljinvoiceTable.DefaultIfEmpty()
							select new AssignmentAgreementSettleItemViewMap
							{
								CompanyGUID = assignmentAgreementSettle.CompanyGUID,
								CompanyId = company.CompanyId,
								Owner = assignmentAgreementSettle.Owner,
								OwnerBusinessUnitGUID = assignmentAgreementSettle.OwnerBusinessUnitGUID,
								OwnerBusinessUnitId = ownerBU.BusinessUnitId,
								CreatedBy = assignmentAgreementSettle.CreatedBy,
								CreatedDateTime = assignmentAgreementSettle.CreatedDateTime,
								ModifiedBy = assignmentAgreementSettle.ModifiedBy,
								ModifiedDateTime = assignmentAgreementSettle.ModifiedDateTime,
								AssignmentAgreementSettleGUID = assignmentAgreementSettle.AssignmentAgreementSettleGUID,
								AssignmentAgreementTableGUID = assignmentAgreementSettle.AssignmentAgreementTableGUID,
								BuyerAgreementTableGUID = assignmentAgreementSettle.BuyerAgreementTableGUID,
								DocumentReasonGUID = assignmentAgreementSettle.DocumentReasonGUID,
								RefAssignmentAgreementSettleGUID = assignmentAgreementSettle.RefAssignmentAgreementSettleGUID,
								RefGUID = assignmentAgreementSettle.RefGUID,
								RefType = assignmentAgreementSettle.RefType,
								SettledAmount = assignmentAgreementSettle.SettledAmount,
								SettledDate = assignmentAgreementSettle.SettledDate,

								AssignmentAgreementTable_Value = assignmentagreementtable != null ? SmartAppUtil.GetDropDownLabel(assignmentagreementtable.InternalAssignmentAgreementId, assignmentagreementtable.Description) : null,
								AssignmentAgreementTable_AssignmentAgreementId = assigntable.AssignmentAgreementId,
								BuyerTable_Value = SmartAppUtil.GetDropDownLabel(buyeragreement.BuyerAgreementId, buyeragreement.Description),
								DocumentReason_ReasonId = reasonTable != null ? reasonTable.ReasonId : null,
								RefId = (receiptTempTable != null && assignmentAgreementSettle.RefType == (int)RefType.ReceiptTemp) ? receiptTempTable.ReceiptTempId : null,
								DocumentId = assignmentAgreementSettle.DocumentId,
								InvoiceTableGUID = assignmentAgreementSettle.InvoiceTableGUID,
								InvoiceTable_Values = invoiceTable != null ? SmartAppUtil.GetDropDownLabel(invoiceTable.InvoiceId, invoiceTable.IssuedDate.DateToString()) : null,

								RowVersion = assignmentAgreementSettle.RowVersion,
							});
			return result;
		}
		public AssignmentAgreementSettleItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<AssignmentAgreementSettleItemViewMap, AssignmentAgreementSettleItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public AssignmentAgreementSettle CreateAssignmentAgreementSettle(AssignmentAgreementSettle assignmentAgreementSettle)
		{
			try
			{
				assignmentAgreementSettle.AssignmentAgreementSettleGUID = Guid.NewGuid();
				base.Add(assignmentAgreementSettle);
				return assignmentAgreementSettle;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateAssignmentAgreementSettleVoid(AssignmentAgreementSettle assignmentAgreementSettle)
		{
			try
			{
				CreateAssignmentAgreementSettle(assignmentAgreementSettle);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AssignmentAgreementSettle UpdateAssignmentAgreementSettle(AssignmentAgreementSettle dbAssignmentAgreementSettle, AssignmentAgreementSettle inputAssignmentAgreementSettle, List<string> skipUpdateFields = null)
		{
			try
			{
				dbAssignmentAgreementSettle = dbAssignmentAgreementSettle.MapUpdateValues<AssignmentAgreementSettle>(inputAssignmentAgreementSettle);
				base.Update(dbAssignmentAgreementSettle);
				return dbAssignmentAgreementSettle;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateAssignmentAgreementSettleVoid(AssignmentAgreementSettle dbAssignmentAgreementSettle, AssignmentAgreementSettle inputAssignmentAgreementSettle, List<string> skipUpdateFields = null)
		{
			try
			{
				dbAssignmentAgreementSettle = dbAssignmentAgreementSettle.MapUpdateValues<AssignmentAgreementSettle>(inputAssignmentAgreementSettle, skipUpdateFields);
				base.Update(dbAssignmentAgreementSettle);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public AssignmentAgreementSettle GetAssignmentAgreementSettleByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.AssignmentAgreementSettleGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<AssignmentAgreementSettle> GetAssignmentAgreementSettleByAssignmentAgreementTableGUIDNoTracking(Guid assignmentAgreementTableGUID)
        {
            try
            {
				var result = Entity.Where(item => item.AssignmentAgreementTableGUID == assignmentAgreementTableGUID)
									.AsNoTracking()
									.ToList();
				return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Validate
        public override void ValidateAdd(IEnumerable<AssignmentAgreementSettle> items)
        {
            base.ValidateAdd(items);
        }
        #endregion
    }
}
