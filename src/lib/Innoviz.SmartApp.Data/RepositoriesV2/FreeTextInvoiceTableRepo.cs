using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IFreeTextInvoiceTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<FreeTextInvoiceTableItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<FreeTextInvoiceTableListView> GetListvw(SearchParameter search);
		FreeTextInvoiceTableItemView GetByIdvw(Guid id);
		FreeTextInvoiceTable Find(params object[] keyValues);
		FreeTextInvoiceTable GetFreeTextInvoiceTableByIdNoTracking(Guid guid);
		FreeTextInvoiceTable CreateFreeTextInvoiceTable(FreeTextInvoiceTable freeTextInvoiceTable);
		void CreateFreeTextInvoiceTableVoid(FreeTextInvoiceTable freeTextInvoiceTable);
		FreeTextInvoiceTable UpdateFreeTextInvoiceTable(FreeTextInvoiceTable dbFreeTextInvoiceTable, FreeTextInvoiceTable inputFreeTextInvoiceTable, List<string> skipUpdateFields = null);
		void UpdateFreeTextInvoiceTableVoid(FreeTextInvoiceTable dbFreeTextInvoiceTable, FreeTextInvoiceTable inputFreeTextInvoiceTable, List<string> skipUpdateFields = null);
		void Remove(FreeTextInvoiceTable item);
		FreeTextInvoiceTable GetFreeTextInvoiceTableByIdNoTrackingByAccessLevel(Guid guid);
		#region Function
		PostFreeTextInvoiceView GetPostFreeTextInvoiceById(Guid freeTextInvoiceTableGUID);
        #endregion Function
    }
    public class FreeTextInvoiceTableRepo : CompanyBaseRepository<FreeTextInvoiceTable>, IFreeTextInvoiceTableRepo
	{
		public FreeTextInvoiceTableRepo(SmartAppDbContext context) : base(context) { }
		public FreeTextInvoiceTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public FreeTextInvoiceTable GetFreeTextInvoiceTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.FreeTextInvoiceTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<FreeTextInvoiceTableItemViewMap> GetDropDownQuery()
		{
			return (from freeTextInvoiceTable in Entity
					select new FreeTextInvoiceTableItemViewMap
					{
						CompanyGUID = freeTextInvoiceTable.CompanyGUID,
						Owner = freeTextInvoiceTable.Owner,
						OwnerBusinessUnitGUID = freeTextInvoiceTable.OwnerBusinessUnitGUID,
						FreeTextInvoiceTableGUID = freeTextInvoiceTable.FreeTextInvoiceTableGUID,
						FreeTextInvoiceId = freeTextInvoiceTable.FreeTextInvoiceId,
						IssuedDate = freeTextInvoiceTable.IssuedDate
					});
		}
		public IEnumerable<SelectItem<FreeTextInvoiceTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<FreeTextInvoiceTable>(search, SysParm.CompanyGUID);
				var freeTextInvoiceTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<FreeTextInvoiceTableItemViewMap, FreeTextInvoiceTableItemView>().ToDropDownItem(search.ExcludeRowData);
				return freeTextInvoiceTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<FreeTextInvoiceTableListViewMap> GetListQuery()
		{
			return (from freeTextInvoiceTable in Entity
					join customerTable in db.Set<CustomerTable>()
					on freeTextInvoiceTable.CustomerTableGUID equals customerTable.CustomerTableGUID into ljcustomerTable
					from customerTable in ljcustomerTable.DefaultIfEmpty()
					join invoiceTypeTable in  db.Set<InvoiceType>()
					on freeTextInvoiceTable.InvoiceTypeGUID equals invoiceTypeTable.InvoiceTypeGUID into ljinvoiceTypeTable
					from invoiceTypeTable in ljinvoiceTypeTable.DefaultIfEmpty()
					join creditAppTable in db.Set<CreditAppTable>()
					on freeTextInvoiceTable.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljcrediAppTable
					from creditAppTable in ljcrediAppTable.DefaultIfEmpty()
					join documentStatus in db.Set<DocumentStatus>()
					on freeTextInvoiceTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatustable
					from documentStatus in ljdocumentStatustable.DefaultIfEmpty()
					select new FreeTextInvoiceTableListViewMap
				{
						CompanyGUID = freeTextInvoiceTable.CompanyGUID,
						Owner = freeTextInvoiceTable.Owner,
						OwnerBusinessUnitGUID = freeTextInvoiceTable.OwnerBusinessUnitGUID,
						FreeTextInvoiceTableGUID = freeTextInvoiceTable.FreeTextInvoiceTableGUID,
						FreeTextInvoiceId = freeTextInvoiceTable.FreeTextInvoiceId,
						CustomerTableGUID = freeTextInvoiceTable.CustomerTableGUID,
						CreditAppTableGUID = freeTextInvoiceTable.CreditAppTableGUID,
						IssuedDate = freeTextInvoiceTable.IssuedDate,
						InvoiceTypeGUID = freeTextInvoiceTable.InvoiceTypeGUID,
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
						InvoiceType_Values = SmartAppUtil.GetDropDownLabel(invoiceTypeTable.InvoiceTypeId, invoiceTypeTable.Description),
						CreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
						CustomerTable_CustomerId = customerTable.CustomerId,
						InvoiceType_InvoiceTypeId = invoiceTypeTable.InvoiceTypeId,
						CreditAppTable_CreditAppId = creditAppTable.CreditAppId,
						DocumentStatus_StatusId = (documentStatus != null) ? documentStatus.StatusId : null,
						DocumentStatus_Values = (documentStatus != null) ? documentStatus.Description : null,
						DocumentStatusGUID = freeTextInvoiceTable.DocumentStatusGUID,
						InvoiceAmount = freeTextInvoiceTable.InvoiceAmount
					});
		}
		public SearchResult<FreeTextInvoiceTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<FreeTextInvoiceTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<FreeTextInvoiceTable>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<FreeTextInvoiceTableListViewMap, FreeTextInvoiceTableListView>();
				result = list.SetSearchResult<FreeTextInvoiceTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<FreeTextInvoiceTableItemViewMap> GetItemQuery()
		{
			return (from freeTextInvoiceTable in Entity
					join company in db.Set<Company>()
					on freeTextInvoiceTable.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on freeTextInvoiceTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljFreeTextInvoiceTableOwnerBU
					from ownerBU in ljFreeTextInvoiceTableOwnerBU.DefaultIfEmpty()
					join documentStatus in db.Set<DocumentStatus>()
					on freeTextInvoiceTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljDocumentStatus
					from documentStatus in ljDocumentStatus.DefaultIfEmpty()
					join invoiceTable in db.Set<InvoiceTable>()
					on freeTextInvoiceTable.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljinvoiceTable
					from invoiceTable in ljinvoiceTable.DefaultIfEmpty()
					select new FreeTextInvoiceTableItemViewMap
					{
						CompanyGUID = freeTextInvoiceTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = freeTextInvoiceTable.Owner,
						OwnerBusinessUnitGUID = freeTextInvoiceTable.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = freeTextInvoiceTable.CreatedBy,
						CreatedDateTime = freeTextInvoiceTable.CreatedDateTime,
						ModifiedBy = freeTextInvoiceTable.ModifiedBy,
						ModifiedDateTime = freeTextInvoiceTable.ModifiedDateTime,
						FreeTextInvoiceTableGUID = freeTextInvoiceTable.FreeTextInvoiceTableGUID,
						CNReasonGUID = freeTextInvoiceTable.CNReasonGUID,
						CreditAppTableGUID = freeTextInvoiceTable.CreditAppTableGUID,
						CurrencyGUID = freeTextInvoiceTable.CurrencyGUID,
						CustomerName = freeTextInvoiceTable.CustomerName,
						CustomerTableGUID = freeTextInvoiceTable.CustomerTableGUID,
						Dimension1GUID = freeTextInvoiceTable.Dimension1GUID,
						Dimension2GUID = freeTextInvoiceTable.Dimension2GUID,
						Dimension3GUID = freeTextInvoiceTable.Dimension3GUID,
						Dimension4GUID = freeTextInvoiceTable.Dimension4GUID,
						Dimension5GUID = freeTextInvoiceTable.Dimension5GUID,
						DocumentStatusGUID = freeTextInvoiceTable.DocumentStatusGUID,
						DueDate = freeTextInvoiceTable.DueDate,
						ExchangeRate = freeTextInvoiceTable.ExchangeRate,
						FreeTextInvoiceId = freeTextInvoiceTable.FreeTextInvoiceId,
						InvoiceAddress1 = freeTextInvoiceTable.InvoiceAddress1,
						InvoiceAddress2 = freeTextInvoiceTable.InvoiceAddress2,
						InvoiceAddressGUID = freeTextInvoiceTable.InvoiceAddressGUID,
						InvoiceAmount = freeTextInvoiceTable.InvoiceAmount,
						InvoiceAmountBeforeTax = freeTextInvoiceTable.InvoiceAmountBeforeTax,
						InvoiceRevenueTypeGUID = freeTextInvoiceTable.InvoiceRevenueTypeGUID,
						InvoiceTableGUID = freeTextInvoiceTable.InvoiceTableGUID,
						InvoiceText = freeTextInvoiceTable.InvoiceText,
						InvoiceTypeGUID = freeTextInvoiceTable.InvoiceTypeGUID,
						IssuedDate = freeTextInvoiceTable.IssuedDate,
						MailingInvoiceAddress1 = freeTextInvoiceTable.MailingInvoiceAddress1,
						MailingInvoiceAddress2 = freeTextInvoiceTable.MailingInvoiceAddress2,
						MailingInvoiceAddressGUID = freeTextInvoiceTable.MailingInvoiceAddressGUID,
						OrigInvoiceAmount = freeTextInvoiceTable.OrigInvoiceAmount,
						OrigInvoiceId = freeTextInvoiceTable.OrigInvoiceId,
						OrigTaxInvoiceAmount = freeTextInvoiceTable.OrigTaxInvoiceAmount,
						OrigTaxInvoiceId = freeTextInvoiceTable.OrigTaxInvoiceId,
						ProductType = freeTextInvoiceTable.ProductType,
						Remark = freeTextInvoiceTable.Remark,
						TaxAmount = freeTextInvoiceTable.TaxAmount,
						TaxBranchId = freeTextInvoiceTable.TaxBranchId,
						TaxBranchName = freeTextInvoiceTable.TaxBranchName,
						TaxTableGUID = freeTextInvoiceTable.TaxTableGUID,
						WHTAmount = freeTextInvoiceTable.WHTAmount,
						WithholdingTaxTableGUID = freeTextInvoiceTable.WithholdingTaxTableGUID,
						DocumentStatus_StatusId = documentStatus.StatusId,
						DocumentStatus_Description = documentStatus.Description,
						InvoiceTable_Values = (invoiceTable != null) ? SmartAppUtil.GetDropDownLabel(invoiceTable.InvoiceId, invoiceTable.IssuedDate.DateToString()) : null,
					
						RowVersion = freeTextInvoiceTable.RowVersion,
					});
		}
		public FreeTextInvoiceTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<FreeTextInvoiceTableItemViewMap, FreeTextInvoiceTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public FreeTextInvoiceTable CreateFreeTextInvoiceTable(FreeTextInvoiceTable freeTextInvoiceTable)
		{
			try
			{
				freeTextInvoiceTable.FreeTextInvoiceTableGUID = Guid.NewGuid();
				base.Add(freeTextInvoiceTable);
				return freeTextInvoiceTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateFreeTextInvoiceTableVoid(FreeTextInvoiceTable freeTextInvoiceTable)
		{
			try
			{
				CreateFreeTextInvoiceTable(freeTextInvoiceTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public FreeTextInvoiceTable UpdateFreeTextInvoiceTable(FreeTextInvoiceTable dbFreeTextInvoiceTable, FreeTextInvoiceTable inputFreeTextInvoiceTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbFreeTextInvoiceTable = dbFreeTextInvoiceTable.MapUpdateValues<FreeTextInvoiceTable>(inputFreeTextInvoiceTable);
				base.Update(dbFreeTextInvoiceTable);
				return dbFreeTextInvoiceTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateFreeTextInvoiceTableVoid(FreeTextInvoiceTable dbFreeTextInvoiceTable, FreeTextInvoiceTable inputFreeTextInvoiceTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbFreeTextInvoiceTable = dbFreeTextInvoiceTable.MapUpdateValues<FreeTextInvoiceTable>(inputFreeTextInvoiceTable, skipUpdateFields);
				base.Update(dbFreeTextInvoiceTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public FreeTextInvoiceTable GetFreeTextInvoiceTableByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.FreeTextInvoiceTableGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region function
		public PostFreeTextInvoiceView GetPostFreeTextInvoiceById(Guid freeTextInvoiceTableGUID)
		{
			try
			{
				PostFreeTextInvoiceView postFreeTextInvoiceView = (from freeTextInvoiceTable in Entity
																   join currency in db.Set<Currency>() on freeTextInvoiceTable.CurrencyGUID equals currency.CurrencyGUID
																   join customer in db.Set<CustomerTable>() on freeTextInvoiceTable.CustomerTableGUID equals customer.CustomerTableGUID
																   join invoiceRevenueType in db.Set<InvoiceRevenueType>() on freeTextInvoiceTable.InvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID
																   join invoiceType in db.Set<InvoiceType>() on freeTextInvoiceTable.InvoiceTypeGUID equals invoiceType.InvoiceTypeGUID
																   where freeTextInvoiceTable.FreeTextInvoiceTableGUID == freeTextInvoiceTableGUID
																   select new PostFreeTextInvoiceView
																   {
																	   FreeTextInvoiceTableGUID = freeTextInvoiceTable.FreeTextInvoiceTableGUID.ToString(),
																	   Currency_Values = SmartAppUtil.GetDropDownLabel(currency.CurrencyId, currency.Name),
																	   CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customer.CustomerId, customer.Name),
																	   CustomerName = freeTextInvoiceTable.CustomerName,
																	   DueDate = freeTextInvoiceTable.DueDate.DateToString(),
																	   FreeTextInvoiceId = freeTextInvoiceTable.FreeTextInvoiceId,
																	   InvoiceAmount = freeTextInvoiceTable.InvoiceAmount,
																	   InvoiceAmountBeforeTax = freeTextInvoiceTable.InvoiceAmountBeforeTax,
																	   InvoiceRevenueType_Values = SmartAppUtil.GetDropDownLabel(invoiceRevenueType.RevenueTypeId, invoiceRevenueType.Description),
																	   InvoiceType_Values = SmartAppUtil.GetDropDownLabel(invoiceType.InvoiceTypeId, invoiceType.Description),
																	   IssuedDate = freeTextInvoiceTable.IssuedDate.DateToString(),
																	   TaxAmount = freeTextInvoiceTable.TaxAmount,
																	   WHTAmount = freeTextInvoiceTable.WHTAmount
																   }).AsNoTracking().FirstOrDefault();
				return postFreeTextInvoiceView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion function
	}
}

