using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IDocumentReturnTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<DocumentReturnTableItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<DocumentReturnTableListView> GetListvw(SearchParameter search);
		DocumentReturnTableItemView GetByIdvw(Guid id);
		DocumentReturnTable Find(params object[] keyValues);
		DocumentReturnTable GetDocumentReturnTableByIdNoTracking(Guid guid);
		DocumentReturnTable CreateDocumentReturnTable(DocumentReturnTable documentReturnTable);
		void CreateDocumentReturnTableVoid(DocumentReturnTable documentReturnTable);
		DocumentReturnTable UpdateDocumentReturnTable(DocumentReturnTable dbDocumentReturnTable, DocumentReturnTable inputDocumentReturnTable, List<string> skipUpdateFields = null);
		void UpdateDocumentReturnTableVoid(DocumentReturnTable dbDocumentReturnTable, DocumentReturnTable inputDocumentReturnTable, List<string> skipUpdateFields = null);
		void Remove(DocumentReturnTable item);
		DocumentReturnTable GetDocumentReturnTableByIdNoTrackingByAccessLevel(Guid guid);
	}
	public class DocumentReturnTableRepo : CompanyBaseRepository<DocumentReturnTable>, IDocumentReturnTableRepo
	{
		public DocumentReturnTableRepo(SmartAppDbContext context) : base(context) { }
		public DocumentReturnTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public DocumentReturnTable GetDocumentReturnTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.DocumentReturnTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<DocumentReturnTableItemViewMap> GetDropDownQuery()
		{
			return (from documentReturnTable in Entity
					select new DocumentReturnTableItemViewMap
					{
						CompanyGUID = documentReturnTable.CompanyGUID,
						Owner = documentReturnTable.Owner,
						OwnerBusinessUnitGUID = documentReturnTable.OwnerBusinessUnitGUID,
						DocumentReturnTableGUID = documentReturnTable.DocumentReturnTableGUID,
						DocumentReturnId = documentReturnTable.DocumentReturnId,
						ContactPersonName = documentReturnTable.ContactPersonName
					});
		}
		public IEnumerable<SelectItem<DocumentReturnTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<DocumentReturnTable>(search, SysParm.CompanyGUID);
				var documentReturnTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<DocumentReturnTableItemViewMap, DocumentReturnTableItemView>().ToDropDownItem(search.ExcludeRowData);
				return documentReturnTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<DocumentReturnTableListViewMap> GetListQuery()
		{
			return (from documentReturnTable in Entity
					join documentStatus in db.Set<DocumentStatus>()
					on documentReturnTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus
					from documentStatus in ljdocumentStatus.DefaultIfEmpty()

					join documentReturnMetnod in db.Set<DocumentReturnMethod>()
					on documentReturnTable.DocumentReturnMethodGUID equals documentReturnMetnod.DocumentReturnMethodGUID into ljdocumentReturnMetnod
					from documentReturnMetnod in ljdocumentReturnMetnod.DefaultIfEmpty()

					join employeeTable in db.Set<EmployeeTable>()
					on documentReturnTable.RequestorGUID equals employeeTable.EmployeeTableGUID into ljemployeeTable
					from employeeTable in ljemployeeTable.DefaultIfEmpty()
					select new DocumentReturnTableListViewMap
				{
						CompanyGUID = documentReturnTable.CompanyGUID,
						Owner = documentReturnTable.Owner,
						OwnerBusinessUnitGUID = documentReturnTable.OwnerBusinessUnitGUID,
						DocumentReturnTableGUID = documentReturnTable.DocumentReturnTableGUID,
						DocumentReturnId = documentReturnTable.DocumentReturnId,
						RequestorGUID = documentReturnTable.RequestorGUID,
						ContactTo = documentReturnTable.ContactTo,
						ContactPersonName = documentReturnTable.ContactPersonName,
						DocumentStatusGUID = documentReturnTable.DocumentStatusGUID,
						DocumentReturnMethodGUID = documentReturnTable.DocumentReturnMethodGUID,
						ExpectedReturnDate = documentReturnTable.ExpectedReturnDate,
						EmployeeTable_Values = SmartAppUtil.GetDropDownLabel(employeeTable.EmployeeId,employeeTable.Name),
						DocumentStatus_Values = documentStatus.Description,
						DocumentReturnMethod_Values = SmartAppUtil.GetDropDownLabel(documentReturnMetnod.DocumentReturnMethodId,documentReturnMetnod.Description),
						DocumentStatus_StatusId = documentStatus.StatusId,
						DocumentReturnMethod_DocumentReturnMethodId = documentReturnMetnod.DocumentReturnMethodId,
						EmployeeTable_EmployeeTableId = employeeTable.EmployeeId,

					});
		}
		public SearchResult<DocumentReturnTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<DocumentReturnTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<DocumentReturnTable>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<DocumentReturnTableListViewMap, DocumentReturnTableListView>();
				result = list.SetSearchResult<DocumentReturnTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<DocumentReturnTableItemViewMap> GetItemQuery()
		{
			return (from documentReturnTable in Entity
					join company in db.Set<Company>()
					on documentReturnTable.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on documentReturnTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljDocumentReturnTableOwnerBU
					from ownerBU in ljDocumentReturnTableOwnerBU.DefaultIfEmpty()

					join documentStatus in db.Set<DocumentStatus>()
					on documentReturnTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus
					from documentStatus in ljdocumentStatus.DefaultIfEmpty()
					select new DocumentReturnTableItemViewMap
					{
						CompanyGUID = documentReturnTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = documentReturnTable.Owner,
						OwnerBusinessUnitGUID = documentReturnTable.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = documentReturnTable.CreatedBy,
						CreatedDateTime = documentReturnTable.CreatedDateTime,
						ModifiedBy = documentReturnTable.ModifiedBy,
						ModifiedDateTime = documentReturnTable.ModifiedDateTime,
						DocumentReturnTableGUID = documentReturnTable.DocumentReturnTableGUID,
						ActualReturnDate = documentReturnTable.ActualReturnDate,
						Address = documentReturnTable.Address,
						BuyerTableGUID = documentReturnTable.BuyerTableGUID,
						ContactPersonName = documentReturnTable.ContactPersonName,
						ContactTo = documentReturnTable.ContactTo,
						CustomerTableGUID = documentReturnTable.CustomerTableGUID,
						DocumentReturnId = documentReturnTable.DocumentReturnId,
						DocumentReturnMethodGUID = documentReturnTable.DocumentReturnMethodGUID,
						DocumentStatusGUID = documentReturnTable.DocumentStatusGUID,
						ExpectedReturnDate = documentReturnTable.ExpectedReturnDate,
						PostAcknowledgementNo = documentReturnTable.PostAcknowledgementNo,
						PostNumber = documentReturnTable.PostNumber,
						ReceivedBy = documentReturnTable.ReceivedBy,
						Remark = documentReturnTable.Remark,
						RequestorGUID = documentReturnTable.RequestorGUID,
						ReturnByGUID = documentReturnTable.ReturnByGUID,
						DocumentStatus_StatusId = documentStatus.StatusId,
						DocumentStatus_Values = documentStatus.Description,
						
					
						RowVersion = documentReturnTable.RowVersion,
					});
		}
		public DocumentReturnTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<DocumentReturnTableItemViewMap, DocumentReturnTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public DocumentReturnTable CreateDocumentReturnTable(DocumentReturnTable documentReturnTable)
		{
			try
			{
				documentReturnTable.DocumentReturnTableGUID = Guid.NewGuid();
				base.Add(documentReturnTable);
				return documentReturnTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateDocumentReturnTableVoid(DocumentReturnTable documentReturnTable)
		{
			try
			{
				CreateDocumentReturnTable(documentReturnTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentReturnTable UpdateDocumentReturnTable(DocumentReturnTable dbDocumentReturnTable, DocumentReturnTable inputDocumentReturnTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbDocumentReturnTable = dbDocumentReturnTable.MapUpdateValues<DocumentReturnTable>(inputDocumentReturnTable);
				base.Update(dbDocumentReturnTable);
				return dbDocumentReturnTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateDocumentReturnTableVoid(DocumentReturnTable dbDocumentReturnTable, DocumentReturnTable inputDocumentReturnTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbDocumentReturnTable = dbDocumentReturnTable.MapUpdateValues<DocumentReturnTable>(inputDocumentReturnTable, skipUpdateFields);
				base.Update(dbDocumentReturnTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public DocumentReturnTable GetDocumentReturnTableByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.DocumentReturnTableGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

