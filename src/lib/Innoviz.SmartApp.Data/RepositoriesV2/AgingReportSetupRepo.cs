using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IAgingReportSetupRepo
	{
		AgingReportSetup Find(params object[] keyValues);
		AgingReportSetup GetAgingReportSetupByIdNoTracking(Guid guid);
		AgingReportSetup GetAgingReportSetupByIdNoTrackingByAccessLevel(Guid guid);
		AgingReportSetupView GetAgingReportSetupView(Guid companyGUID);
		AgingReportSetupView GetAgingReportSetupDescription(Guid companyGUID);
	}
	public class AgingReportSetupRepo : CompanyBaseRepository<AgingReportSetup>, IAgingReportSetupRepo
	{
		public AgingReportSetupRepo(SmartAppDbContext context) : base(context) { }
		public AgingReportSetupRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public AgingReportSetup GetAgingReportSetupByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.AgingReportSetupGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AgingReportSetup GetAgingReportSetupByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.AgingReportSetupGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<AgingReportSetup> GetAgingReportSetupByCompanyNoTracking(Guid companyGUID)
		{
			try
			{
				List<AgingReportSetup> agingReportSetups = Entity.Where(w => w.CompanyGUID == companyGUID).AsNoTracking().ToList();
				return agingReportSetups;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AgingReportSetupView GetAgingReportSetupView(Guid companyGUID)
		{
			try
			{
				IEnumerable<AgingReportSetup> agingReportSetups = GetAgingReportSetupByCompanyNoTracking(companyGUID).OrderBy(o => o.LineNum);
				
				var queryFrom = from t1 in agingReportSetups
								join t2 in agingReportSetups
								on t1.LineNum equals t2.LineNum + 1 into lj
								from t2 in lj.DefaultIfEmpty()
								select new
								{
									col = "From" + t1.LineNum.ToString(),
									maxDay = t1.LineNum == 1 ? -999999 : t2.MaximumDays + 1
								};
				var queryTo = from t1 in agingReportSetups
								select new
								{
									col = "To" + t1.LineNum.ToString(),
									maxDay = t1.MaximumDays
								};
				var query = queryFrom.Union(queryTo);

				AgingReportSetupView view = new AgingReportSetupView {
					From1 = query.Where(w => w.col == "From1").Select(s => s.maxDay).FirstOrDefault(),
					To1 = query.Where(w => w.col == "To1").Select(s => s.maxDay).FirstOrDefault(),
					From2 = query.Where(w => w.col == "From2").Select(s => s.maxDay).FirstOrDefault(),
					To2 = query.Where(w => w.col == "To2").Select(s => s.maxDay).FirstOrDefault(),
					From3 = query.Where(w => w.col == "From3").Select(s => s.maxDay).FirstOrDefault(),
					To3 = query.Where(w => w.col == "To3").Select(s => s.maxDay).FirstOrDefault(),
					From4 = query.Where(w => w.col == "From4").Select(s => s.maxDay).FirstOrDefault(),
					To4 = query.Where(w => w.col == "To4").Select(s => s.maxDay).FirstOrDefault(),
					From5 = query.Where(w => w.col == "From5").Select(s => s.maxDay).FirstOrDefault(),
					To5 = query.Where(w => w.col == "To5").Select(s => s.maxDay).FirstOrDefault(),
					From6 = query.Where(w => w.col == "From6").Select(s => s.maxDay).FirstOrDefault(),
					To6 = query.Where(w => w.col == "To6").Select(s => s.maxDay).FirstOrDefault()
				};
				return view;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AgingReportSetupView GetAgingReportSetupDescription(Guid companyGUID)
		{
			try
			{
				IEnumerable<AgingReportSetup> agingReportSetups = GetAgingReportSetupByCompanyNoTracking(companyGUID).OrderBy(o => o.LineNum);
				AgingReportSetupView view = new AgingReportSetupView
				{
					Description1 = agingReportSetups.Where(w => w.LineNum == 1).Select(s => s.Description).FirstOrDefault(),
					Description2 = agingReportSetups.Where(w => w.LineNum == 2).Select(s => s.Description).FirstOrDefault(),
					Description3 = agingReportSetups.Where(w => w.LineNum == 3).Select(s => s.Description).FirstOrDefault(),
					Description4 = agingReportSetups.Where(w => w.LineNum == 4).Select(s => s.Description).FirstOrDefault(),
					Description5 = agingReportSetups.Where(w => w.LineNum == 5).Select(s => s.Description).FirstOrDefault(),
					Description6 = agingReportSetups.Where(w => w.LineNum == 6).Select(s => s.Description).FirstOrDefault()
				};
				return view;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}
