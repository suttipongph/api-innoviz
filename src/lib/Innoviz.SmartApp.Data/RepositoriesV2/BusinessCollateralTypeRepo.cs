using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IBusinessCollateralTypeRepo
	{
		#region DropDown
		IEnumerable<SelectItem<BusinessCollateralTypeItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<BusinessCollateralTypeListView> GetListvw(SearchParameter search);
		BusinessCollateralTypeItemView GetByIdvw(Guid id);
		BusinessCollateralType Find(params object[] keyValues);
		BusinessCollateralType GetBusinessCollateralTypeByIdNoTracking(Guid guid);
		BusinessCollateralType CreateBusinessCollateralType(BusinessCollateralType businessCollateralType);
		void CreateBusinessCollateralTypeVoid(BusinessCollateralType businessCollateralType);
		BusinessCollateralType UpdateBusinessCollateralType(BusinessCollateralType dbBusinessCollateralType, BusinessCollateralType inputBusinessCollateralType, List<string> skipUpdateFields = null);
		void UpdateBusinessCollateralTypeVoid(BusinessCollateralType dbBusinessCollateralType, BusinessCollateralType inputBusinessCollateralType, List<string> skipUpdateFields = null);
		void Remove(BusinessCollateralType item);
		BusinessCollateralType GetBusinessCollateralTypeNoTrackingByAccessLevel(Guid guid);
	}
	public class BusinessCollateralTypeRepo : CompanyBaseRepository<BusinessCollateralType>, IBusinessCollateralTypeRepo
	{
		public BusinessCollateralTypeRepo(SmartAppDbContext context) : base(context) { }
		public BusinessCollateralTypeRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BusinessCollateralType GetBusinessCollateralTypeByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BusinessCollateralTypeGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BusinessCollateralType GetBusinessCollateralTypeNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BusinessCollateralTypeGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<BusinessCollateralTypeItemViewMap> GetDropDownQuery()
		{
			return (from businessCollateralType in Entity
					select new BusinessCollateralTypeItemViewMap
					{
						CompanyGUID = businessCollateralType.CompanyGUID,
						Owner = businessCollateralType.Owner,
						OwnerBusinessUnitGUID = businessCollateralType.OwnerBusinessUnitGUID,
						BusinessCollateralTypeGUID = businessCollateralType.BusinessCollateralTypeGUID,
						BusinessCollateralTypeId = businessCollateralType.BusinessCollateralTypeId,
						Description = businessCollateralType.Description
					});
		}
		public IEnumerable<SelectItem<BusinessCollateralTypeItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<BusinessCollateralType>(search, SysParm.CompanyGUID);
				var businessCollateralType = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<BusinessCollateralTypeItemViewMap, BusinessCollateralTypeItemView>().ToDropDownItem(search.ExcludeRowData);


				return businessCollateralType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<BusinessCollateralTypeListViewMap> GetListQuery()
		{
			return (from businessCollateralType in Entity
				select new BusinessCollateralTypeListViewMap
				{
						CompanyGUID = businessCollateralType.CompanyGUID,
						Owner = businessCollateralType.Owner,
						OwnerBusinessUnitGUID = businessCollateralType.OwnerBusinessUnitGUID,
						BusinessCollateralTypeGUID = businessCollateralType.BusinessCollateralTypeGUID,
						BusinessCollateralTypeId = businessCollateralType.BusinessCollateralTypeId,
						Description = businessCollateralType.Description,
						AgreementOrdering = businessCollateralType.AgreementOrdering,
				});
		}
		public SearchResult<BusinessCollateralTypeListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<BusinessCollateralTypeListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<BusinessCollateralType>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<BusinessCollateralTypeListViewMap, BusinessCollateralTypeListView>();
				result = list.SetSearchResult<BusinessCollateralTypeListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<BusinessCollateralTypeItemViewMap> GetItemQuery()
		{
			return (from businessCollateralType in Entity
					join company in db.Set<Company>()
					on businessCollateralType.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on businessCollateralType.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljBusinessCollateralTypeOwnerBU
					from ownerBU in ljBusinessCollateralTypeOwnerBU.DefaultIfEmpty()
					select new BusinessCollateralTypeItemViewMap
					{
						CompanyGUID = businessCollateralType.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = businessCollateralType.Owner,
						OwnerBusinessUnitGUID = businessCollateralType.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = businessCollateralType.CreatedBy,
						CreatedDateTime = businessCollateralType.CreatedDateTime,
						ModifiedBy = businessCollateralType.ModifiedBy,
						ModifiedDateTime = businessCollateralType.ModifiedDateTime,
						BusinessCollateralTypeGUID = businessCollateralType.BusinessCollateralTypeGUID,
						AgreementOrdering = businessCollateralType.AgreementOrdering,
						BusinessCollateralTypeId = businessCollateralType.BusinessCollateralTypeId,
						Description = businessCollateralType.Description,
					
						RowVersion = businessCollateralType.RowVersion,
					});
		}
		public BusinessCollateralTypeItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<BusinessCollateralTypeItemViewMap, BusinessCollateralTypeItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public BusinessCollateralType CreateBusinessCollateralType(BusinessCollateralType businessCollateralType)
		{
			try
			{
				businessCollateralType.BusinessCollateralTypeGUID = Guid.NewGuid();
				base.Add(businessCollateralType);
				return businessCollateralType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateBusinessCollateralTypeVoid(BusinessCollateralType businessCollateralType)
		{
			try
			{
				CreateBusinessCollateralType(businessCollateralType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BusinessCollateralType UpdateBusinessCollateralType(BusinessCollateralType dbBusinessCollateralType, BusinessCollateralType inputBusinessCollateralType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBusinessCollateralType = dbBusinessCollateralType.MapUpdateValues<BusinessCollateralType>(inputBusinessCollateralType);
				base.Update(dbBusinessCollateralType);
				return dbBusinessCollateralType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateBusinessCollateralTypeVoid(BusinessCollateralType dbBusinessCollateralType, BusinessCollateralType inputBusinessCollateralType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBusinessCollateralType = dbBusinessCollateralType.MapUpdateValues<BusinessCollateralType>(inputBusinessCollateralType, skipUpdateFields);
				base.Update(dbBusinessCollateralType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

