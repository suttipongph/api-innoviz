using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IIntercompanyInvoiceTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<IntercompanyInvoiceTableItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<IntercompanyInvoiceTableListView> GetListvw(SearchParameter search);
		IntercompanyInvoiceTableItemView GetByIdvw(Guid id);
		IntercompanyInvoiceTable Find(params object[] keyValues);
		IntercompanyInvoiceTable GetIntercompanyInvoiceTableByIdNoTracking(Guid guid);
		IntercompanyInvoiceTable CreateIntercompanyInvoiceTable(IntercompanyInvoiceTable intercompanyInvoiceTable);
		void CreateIntercompanyInvoiceTableVoid(IntercompanyInvoiceTable intercompanyInvoiceTable);
		IntercompanyInvoiceTable UpdateIntercompanyInvoiceTable(IntercompanyInvoiceTable dbIntercompanyInvoiceTable, IntercompanyInvoiceTable inputIntercompanyInvoiceTable, List<string> skipUpdateFields = null);
		void UpdateIntercompanyInvoiceTableVoid(IntercompanyInvoiceTable dbIntercompanyInvoiceTable, IntercompanyInvoiceTable inputIntercompanyInvoiceTable, List<string> skipUpdateFields = null);
		void Remove(IntercompanyInvoiceTable item);
		IntercompanyInvoiceTable GetIntercompanyInvoiceTableByIdNoTrackingByAccessLevel(Guid guid);
		void ValidateAdd(IntercompanyInvoiceTable item);
		void ValidateAdd(IEnumerable<IntercompanyInvoiceTable> items);
		List<IntercompanyInvoiceTable> GetIntercompanyInvoiceTableByIntercompanyInvoiceIdNoTracking(IEnumerable<string> intercompanyInvoiceIds);
		IntercompanyInvoiceTableItemViewMap GetIntercompanyInvoiceTableItemViewMapById(Guid intercompanyInvoiceTableGUID);
		decimal GetSumAmountByIntercompanyInvoiceId(Guid intercompanyInvoiceTableGUID, string statusId);
	}
	public class IntercompanyInvoiceTableRepo : CompanyBaseRepository<IntercompanyInvoiceTable>, IIntercompanyInvoiceTableRepo
	{
		public IntercompanyInvoiceTableRepo(SmartAppDbContext context) : base(context) { }
		public IntercompanyInvoiceTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public IntercompanyInvoiceTable GetIntercompanyInvoiceTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.IntercompanyInvoiceTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<IntercompanyInvoiceTableItemViewMap> GetDropDownQuery()
		{
			return (from intercompanyInvoiceTable in Entity
					select new IntercompanyInvoiceTableItemViewMap
					{
						CompanyGUID = intercompanyInvoiceTable.CompanyGUID,
						Owner = intercompanyInvoiceTable.Owner,
						OwnerBusinessUnitGUID = intercompanyInvoiceTable.OwnerBusinessUnitGUID,
						IntercompanyInvoiceTableGUID = intercompanyInvoiceTable.IntercompanyInvoiceTableGUID,
						IntercompanyInvoiceId = intercompanyInvoiceTable.IntercompanyInvoiceId,
						IssuedDate = intercompanyInvoiceTable.IssuedDate
					});
		}
		public IEnumerable<SelectItem<IntercompanyInvoiceTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<IntercompanyInvoiceTable>(search, SysParm.CompanyGUID);
				var intercompanyInvoiceTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<IntercompanyInvoiceTableItemViewMap, IntercompanyInvoiceTableItemView>().ToDropDownItem(search.ExcludeRowData);
				return intercompanyInvoiceTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<IntercompanyInvoiceTableListViewMap> GetListQuery()
		{

			DocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
			DocumentStatus postStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(Convert.ToInt32(IntercompanyInvoiceSettlementStatus.Posted).ToString());
			var sumSettleAmountList = (from intercompanyInvoiceTable in Entity
																					 join intercompanyInvoiceSettlement in db.Set<IntercompanyInvoiceSettlement>()
																					 on intercompanyInvoiceTable.IntercompanyInvoiceTableGUID equals intercompanyInvoiceSettlement.IntercompanyInvoiceTableGUID into ljintercompanyInvoiceSettlement
																					 from intercompanyInvoiceSettlement in ljintercompanyInvoiceSettlement.DefaultIfEmpty()
																					 where intercompanyInvoiceSettlement.DocumentStatusGUID == postStatus.DocumentStatusGUID 
																					 group new
                                                                                     {
																						 intercompanyInvoiceTable,
																						 intercompanyInvoiceSettlement
																					 }
																					 by new
                                                                                     {
																						 intercompanyInvoiceTable.IntercompanyInvoiceTableGUID
																					 } into g 
																					 select new IntercompanyInvoiceTableSettleInvoiceAmount
																					 {
																						 SumSettleInvoiceAmount = g.Sum(su=>su.intercompanyInvoiceSettlement.SettleInvoiceAmount),
																						 IntercompanyInvoiceTableGUID = g.Key.IntercompanyInvoiceTableGUID
																					 });
			
			return (from intercompanyInvoiceTable in Entity
					join customerTable in db.Set<CustomerTable>()
					on intercompanyInvoiceTable.CustomerTableGUID equals customerTable.CustomerTableGUID into ljcustomerTable
					from customerTable in ljcustomerTable.DefaultIfEmpty()
					join creditAppTable in db.Set<CreditAppTable>()
					on intercompanyInvoiceTable.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljcreditappTable
					from creditAppTable in ljcreditappTable.DefaultIfEmpty()
					join invoiceRevenueType in db.Set<InvoiceRevenueType>()
					on intercompanyInvoiceTable.InvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID into ljinvoiceRevenueType
					from invoiceRevenueType in ljinvoiceRevenueType.DefaultIfEmpty()
					join settleAmountList in sumSettleAmountList
					on intercompanyInvoiceTable.IntercompanyInvoiceTableGUID equals settleAmountList.IntercompanyInvoiceTableGUID into ljsettleAmountList
					from settleAmountList in ljsettleAmountList.DefaultIfEmpty()
					select new IntercompanyInvoiceTableListViewMap
					{
						CompanyGUID = intercompanyInvoiceTable.CompanyGUID,
						Owner = intercompanyInvoiceTable.Owner,
						OwnerBusinessUnitGUID = intercompanyInvoiceTable.OwnerBusinessUnitGUID,
						IntercompanyInvoiceTableGUID = intercompanyInvoiceTable.IntercompanyInvoiceTableGUID,
						IntercompanyInvoiceId = intercompanyInvoiceTable.IntercompanyInvoiceId,
						CustomerTableGUID = intercompanyInvoiceTable.CustomerTableGUID,
						ProductType = intercompanyInvoiceTable.ProductType,
						CreditAppTableGUID = intercompanyInvoiceTable.CreditAppTableGUID,
						DocumentId = intercompanyInvoiceTable.DocumentId,
						IssuedDate = intercompanyInvoiceTable.IssuedDate,
						DueDate = intercompanyInvoiceTable.DueDate,
						InvoiceRevenueTypeGUID = intercompanyInvoiceTable.InvoiceRevenueTypeGUID,
						InvoiceAmount = intercompanyInvoiceTable.InvoiceAmount,
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
						CreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
						InvoiceRevenueType_Values = SmartAppUtil.GetDropDownLabel(invoiceRevenueType.RevenueTypeId, invoiceRevenueType.Description),
						SettleAmount = settleAmountList.SumSettleInvoiceAmount,
						Balance = (settleAmountList.IntercompanyInvoiceTableGUID != null ) ? (intercompanyInvoiceTable.InvoiceAmount - settleAmountList.SumSettleInvoiceAmount) : (intercompanyInvoiceTable.InvoiceAmount - 0),
						CustomerTable_CustomerId = customerTable.CustomerId,
						CreditAppTable_CreditAppId = creditAppTable.CreditAppId,
						InvoiceRevenueType_InvoiceRevenueTypeId = invoiceRevenueType.RevenueTypeId,

					});
		}
		public SearchResult<IntercompanyInvoiceTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<IntercompanyInvoiceTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<IntercompanyInvoiceTable>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<IntercompanyInvoiceTableListViewMap, IntercompanyInvoiceTableListView>();
				result = list.SetSearchResult<IntercompanyInvoiceTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<IntercompanyInvoiceTableItemViewMap> GetItemQuery()
		{

			return  (from intercompanyInvoiceTable in Entity
					join company in db.Set<Company>()
					on intercompanyInvoiceTable.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on intercompanyInvoiceTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljIntercompanyInvoiceTableOwnerBU
					from ownerBU in ljIntercompanyInvoiceTableOwnerBU.DefaultIfEmpty()
					join customerTable in db.Set<CustomerTable>()
					on intercompanyInvoiceTable.CustomerTableGUID equals customerTable.CustomerTableGUID into ljcustomerTable
					from customerTable in ljcustomerTable.DefaultIfEmpty()

					join invoiceType in db.Set<InvoiceType>()
					on intercompanyInvoiceTable.InvoiceTypeGUID equals invoiceType.InvoiceTypeGUID into ljinvoiceType
					from invoiceType in ljinvoiceType.DefaultIfEmpty()

					join documentStatus in db.Set<DocumentStatus>()
					on intercompanyInvoiceTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus
					from documentStatus in ljdocumentStatus.DefaultIfEmpty()

					join intercompany in db.Set<Intercompany>()
					on intercompanyInvoiceTable.IntercompanyGUID equals intercompany.IntercompanyGUID into ljintercompany
					from intercompany in ljintercompany.DefaultIfEmpty()

					join creditAppTable in db.Set<CreditAppTable>()
					on intercompanyInvoiceTable.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljcreditappTable
					from creditAppTable in ljcreditappTable.DefaultIfEmpty()

					join currency in db.Set<Currency>()
					on intercompanyInvoiceTable.CurrencyGUID equals currency.CurrencyGUID into ljcurrency
					from currency in ljcurrency.DefaultIfEmpty()

					join invoiceRevenueType in db.Set<InvoiceRevenueType>()
					on intercompanyInvoiceTable.InvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID into ljinvoiceRevenueType
					from invoiceRevenueType in ljinvoiceRevenueType.DefaultIfEmpty()

					join taxTable in db.Set<TaxTable>()
					on intercompanyInvoiceTable.TaxTableGUID equals taxTable.TaxTableGUID into ljtaxtable
					from taxTable in ljtaxtable.DefaultIfEmpty()

					join withholdingTaxTable in db.Set<WithholdingTaxTable>()
					on intercompanyInvoiceTable.WithholdingTaxTableGUID equals withholdingTaxTable.WithholdingTaxTableGUID into ljwithholdingTaxTable
					from withholdingTaxTable in ljwithholdingTaxTable.DefaultIfEmpty()

					join cnReason in db.Set<DocumentReason>()
					on intercompanyInvoiceTable.CNReasonGUID equals cnReason.DocumentReasonGUID into ljcnReason
					from cnReason in ljcnReason.DefaultIfEmpty()

					join dimesion1 in db.Set<LedgerDimension>()
					on intercompanyInvoiceTable.Dimension1GUID equals dimesion1.LedgerDimensionGUID into ljdimesion1
					from dimesion1 in ljdimesion1.DefaultIfEmpty()

					join dimesion2 in db.Set<LedgerDimension>()
					on intercompanyInvoiceTable.Dimension2GUID equals dimesion2.LedgerDimensionGUID into ljdimesion2
					from dimesion2 in ljdimesion2.DefaultIfEmpty()

					join dimesion3 in db.Set<LedgerDimension>()
					on intercompanyInvoiceTable.Dimension3GUID equals dimesion3.LedgerDimensionGUID into ljdimesion3
					from dimesion3 in ljdimesion3.DefaultIfEmpty()

					join dimesion4 in db.Set<LedgerDimension>()
					on intercompanyInvoiceTable.Dimension4GUID equals dimesion4.LedgerDimensionGUID into ljdimesion4
					from dimesion4 in ljdimesion4.DefaultIfEmpty()

					join dimesion5 in db.Set<LedgerDimension>()
					on intercompanyInvoiceTable.Dimension5GUID equals dimesion5.LedgerDimensionGUID into ljdimesion5
					from dimesion5 in ljdimesion5.DefaultIfEmpty()

					join servicefeeTrans in db.Set<ServiceFeeTrans>()
					on intercompanyInvoiceTable.RefGUID equals servicefeeTrans.ServiceFeeTransGUID into ljservicefeeTrans
					from servicefeeTrans in ljservicefeeTrans.DefaultIfEmpty()

					join revenueType in db.Set<InvoiceRevenueType>()
					on servicefeeTrans.InvoiceRevenueTypeGUID equals revenueType.InvoiceRevenueTypeGUID into ljrevenueType
					from revenueType in ljrevenueType.DefaultIfEmpty()

					join addressTrans in db.Set<AddressTrans>()
					on intercompanyInvoiceTable.InvoiceAddressTransGUID equals addressTrans.AddressTransGUID into ljaddressTrans
					from addressTrans in ljaddressTrans.DefaultIfEmpty()
					select new IntercompanyInvoiceTableItemViewMap
					{
						CompanyGUID = intercompanyInvoiceTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = intercompanyInvoiceTable.Owner,
						OwnerBusinessUnitGUID = intercompanyInvoiceTable.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = intercompanyInvoiceTable.CreatedBy,
						CreatedDateTime = intercompanyInvoiceTable.CreatedDateTime,
						ModifiedBy = intercompanyInvoiceTable.ModifiedBy,
						ModifiedDateTime = intercompanyInvoiceTable.ModifiedDateTime,
						IntercompanyInvoiceTableGUID = intercompanyInvoiceTable.IntercompanyInvoiceTableGUID,
						CNReasonGUID = intercompanyInvoiceTable.CNReasonGUID,
						CreditAppTableGUID = intercompanyInvoiceTable.CreditAppTableGUID,
						CurrencyGUID = intercompanyInvoiceTable.CurrencyGUID,
						CustomerName = intercompanyInvoiceTable.CustomerName,
						CustomerTableGUID = intercompanyInvoiceTable.CustomerTableGUID,
						Dimension1GUID = intercompanyInvoiceTable.Dimension1GUID,
						Dimension2GUID = intercompanyInvoiceTable.Dimension2GUID,
						Dimension3GUID = intercompanyInvoiceTable.Dimension3GUID,
						Dimension4GUID = intercompanyInvoiceTable.Dimension4GUID,
						Dimension5GUID = intercompanyInvoiceTable.Dimension5GUID,
						DocumentId = intercompanyInvoiceTable.DocumentId,
						DocumentStatusGUID = intercompanyInvoiceTable.DocumentStatusGUID,
						DueDate = intercompanyInvoiceTable.DueDate,
						ExchangeRate = intercompanyInvoiceTable.ExchangeRate,
						IntercompanyGUID = intercompanyInvoiceTable.IntercompanyGUID,
						IntercompanyInvoiceId = intercompanyInvoiceTable.IntercompanyInvoiceId,
						InvoiceAddressTransGUID = intercompanyInvoiceTable.InvoiceAddressTransGUID,
						InvoiceAmount = intercompanyInvoiceTable.InvoiceAmount,
						InvoiceRevenueTypeGUID = intercompanyInvoiceTable.InvoiceRevenueTypeGUID,
						InvoiceText = intercompanyInvoiceTable.InvoiceText,
						InvoiceTypeGUID = intercompanyInvoiceTable.InvoiceTypeGUID,
						IssuedDate = intercompanyInvoiceTable.IssuedDate,
						OrigInvoiceAmount = intercompanyInvoiceTable.OrigInvoiceAmount,
						OrigInvoiceId = intercompanyInvoiceTable.OrigInvoiceId,
						OrigTaxInvoiceAmount = intercompanyInvoiceTable.OrigTaxInvoiceAmount,
						OrigTaxInvoiceId = intercompanyInvoiceTable.OrigTaxInvoiceId,
						ProductType = intercompanyInvoiceTable.ProductType,
						RefGUID = intercompanyInvoiceTable.RefGUID,
						RefType = intercompanyInvoiceTable.RefType,
						TaxBranchId = intercompanyInvoiceTable.TaxBranchId,
						TaxBranchName = intercompanyInvoiceTable.TaxBranchName,
						TaxTableGUID = intercompanyInvoiceTable.TaxTableGUID,
						WithholdingTaxTableGUID = intercompanyInvoiceTable.WithholdingTaxTableGUID,
						Customer_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId,customerTable.Name),
						InvoiceType_Values = SmartAppUtil.GetDropDownLabel(invoiceType.InvoiceTypeId,invoiceType.Description),
						InterCompany_Values = SmartAppUtil.GetDropDownLabel(intercompany.IntercompanyId,intercompany.Description),
						CreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId,creditAppTable.Description),
						Currency_Values = SmartAppUtil.GetDropDownLabel(currency.CurrencyId,currency.Name),
						InvoiceRevenueType_Values = SmartAppUtil.GetDropDownLabel(invoiceRevenueType.RevenueTypeId,invoiceRevenueType.Description),
						TaxTable_Values = SmartAppUtil.GetDropDownLabel(taxTable.TaxCode,taxTable.Description),
						WithholdingTaxTable_Values = SmartAppUtil.GetDropDownLabel(withholdingTaxTable.WHTCode,withholdingTaxTable.Description),
						CNReason_Values =SmartAppUtil.GetDropDownLabel(cnReason.ReasonId,cnReason.Description),
						Dimension1_Values = SmartAppUtil.GetDropDownLabel(dimesion1.DimensionCode,dimesion1.Description),
						Dimension2_Values = SmartAppUtil.GetDropDownLabel(dimesion2.DimensionCode, dimesion2.Description),
						Dimension3_Values = SmartAppUtil.GetDropDownLabel(dimesion3.DimensionCode, dimesion3.Description),
						Dimension4_Values = SmartAppUtil.GetDropDownLabel(dimesion4.DimensionCode, dimesion4.Description),
						Dimension5_Values = SmartAppUtil.GetDropDownLabel(dimesion5.DimensionCode, dimesion5.Description),
						DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
						RefId = (servicefeeTrans !=null && intercompanyInvoiceTable.RefType == (int)RefType.ServiceFeeTrans)? revenueType.RevenueTypeId:null,
						UnboundInvoiceAddress = addressTrans.Address1+" "+addressTrans.Address2,
						InvoiceAddressTrans_Values  = SmartAppUtil.GetDropDownLabel(addressTrans.Name,addressTrans.Address1),
						FeeLedgerAccount = intercompanyInvoiceTable.FeeLedgerAccount,

						RowVersion = intercompanyInvoiceTable.RowVersion,
					});
		}
		public IntercompanyInvoiceTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<IntercompanyInvoiceTableItemViewMap, IntercompanyInvoiceTableItemView>();
				decimal sumSettleAmount = GetSumAmountByIntercompanyInvoiceId(item.IntercompanyInvoiceTableGUID.StringToGuid(), Convert.ToInt32(IntercompanyInvoiceSettlementStatus.Posted).ToString());
				item.SettleAmount = sumSettleAmount;
				item.Balance = (item.InvoiceAmount - sumSettleAmount);
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public IntercompanyInvoiceTable CreateIntercompanyInvoiceTable(IntercompanyInvoiceTable intercompanyInvoiceTable)
		{
			try
			{
				intercompanyInvoiceTable.IntercompanyInvoiceTableGUID = Guid.NewGuid();
				base.Add(intercompanyInvoiceTable);
				return intercompanyInvoiceTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateIntercompanyInvoiceTableVoid(IntercompanyInvoiceTable intercompanyInvoiceTable)
		{
			try
			{
				CreateIntercompanyInvoiceTable(intercompanyInvoiceTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IntercompanyInvoiceTable UpdateIntercompanyInvoiceTable(IntercompanyInvoiceTable dbIntercompanyInvoiceTable, IntercompanyInvoiceTable inputIntercompanyInvoiceTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbIntercompanyInvoiceTable = dbIntercompanyInvoiceTable.MapUpdateValues<IntercompanyInvoiceTable>(inputIntercompanyInvoiceTable);
				base.Update(dbIntercompanyInvoiceTable);
				return dbIntercompanyInvoiceTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateIntercompanyInvoiceTableVoid(IntercompanyInvoiceTable dbIntercompanyInvoiceTable, IntercompanyInvoiceTable inputIntercompanyInvoiceTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbIntercompanyInvoiceTable = dbIntercompanyInvoiceTable.MapUpdateValues<IntercompanyInvoiceTable>(inputIntercompanyInvoiceTable, skipUpdateFields);
				base.Update(dbIntercompanyInvoiceTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public IntercompanyInvoiceTable GetIntercompanyInvoiceTableByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.IntercompanyInvoiceTableGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public override void ValidateAdd(IEnumerable<IntercompanyInvoiceTable> items)
		{
			base.ValidateAdd(items);
		}
		public override void ValidateAdd(IntercompanyInvoiceTable item)
		{
			base.ValidateAdd(item);
		}
		public List<IntercompanyInvoiceTable> GetIntercompanyInvoiceTableByIntercompanyInvoiceIdNoTracking(IEnumerable<string> intercompanyInvoiceIds)
		{
			try
			{
				var result = Entity.Where(w => intercompanyInvoiceIds.Contains(w.IntercompanyInvoiceId))
								.AsNoTracking()
								.ToList();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IntercompanyInvoiceTableItemViewMap GetIntercompanyInvoiceTableItemViewMapById(Guid intercompanyInvoiceTableGUID)
		{
            try 
			{
				return (from intercompanyInvoiceTable in Entity
						join company in db.Set<Company>()
						on intercompanyInvoiceTable.CompanyGUID equals company.CompanyGUID
						join ownerBU in db.Set<BusinessUnit>()
						on intercompanyInvoiceTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljIntercompanyInvoiceTableOwnerBU
						from ownerBU in ljIntercompanyInvoiceTableOwnerBU.DefaultIfEmpty()

						join intercompany in db.Set<Intercompany>()
						on intercompanyInvoiceTable.IntercompanyGUID equals intercompany.IntercompanyGUID into ljintercompany
						from intercompany in ljintercompany.DefaultIfEmpty()

						join currency in db.Set<Currency>()
						on intercompanyInvoiceTable.CurrencyGUID equals currency.CurrencyGUID into ljcurrency
						from currency in ljcurrency.DefaultIfEmpty()

						join taxTable in db.Set<TaxTable>()
						on intercompanyInvoiceTable.TaxTableGUID equals taxTable.TaxTableGUID into ljtaxtable
						from taxTable in ljtaxtable.DefaultIfEmpty()

						join cnReason in db.Set<DocumentReason>()
						on intercompanyInvoiceTable.CNReasonGUID equals cnReason.DocumentReasonGUID into ljcnReason
						from cnReason in ljcnReason.DefaultIfEmpty()

						join dimesion1 in db.Set<LedgerDimension>()
						on intercompanyInvoiceTable.Dimension1GUID equals dimesion1.LedgerDimensionGUID into ljdimesion1
						from dimesion1 in ljdimesion1.DefaultIfEmpty()

						join dimesion2 in db.Set<LedgerDimension>()
						on intercompanyInvoiceTable.Dimension2GUID equals dimesion2.LedgerDimensionGUID into ljdimesion2
						from dimesion2 in ljdimesion2.DefaultIfEmpty()

						join dimesion3 in db.Set<LedgerDimension>()
						on intercompanyInvoiceTable.Dimension3GUID equals dimesion3.LedgerDimensionGUID into ljdimesion3
						from dimesion3 in ljdimesion3.DefaultIfEmpty()

						join dimesion4 in db.Set<LedgerDimension>()
						on intercompanyInvoiceTable.Dimension4GUID equals dimesion4.LedgerDimensionGUID into ljdimesion4
						from dimesion4 in ljdimesion4.DefaultIfEmpty()

						join dimesion5 in db.Set<LedgerDimension>()
						on intercompanyInvoiceTable.Dimension5GUID equals dimesion5.LedgerDimensionGUID into ljdimesion5
						from dimesion5 in ljdimesion5.DefaultIfEmpty()
						where intercompanyInvoiceTable.IntercompanyInvoiceTableGUID == intercompanyInvoiceTableGUID
						select new IntercompanyInvoiceTableItemViewMap
						{
							CompanyGUID = intercompanyInvoiceTable.CompanyGUID,
							CompanyId = company.CompanyId,
							Owner = intercompanyInvoiceTable.Owner,
							OwnerBusinessUnitGUID = intercompanyInvoiceTable.OwnerBusinessUnitGUID,
							OwnerBusinessUnitId = ownerBU.BusinessUnitId,
							CreatedBy = intercompanyInvoiceTable.CreatedBy,
							CreatedDateTime = intercompanyInvoiceTable.CreatedDateTime,
							ModifiedBy = intercompanyInvoiceTable.ModifiedBy,
							ModifiedDateTime = intercompanyInvoiceTable.ModifiedDateTime,
							IntercompanyInvoiceTableGUID = intercompanyInvoiceTable.IntercompanyInvoiceTableGUID,
							CNReasonGUID = intercompanyInvoiceTable.CNReasonGUID,
							CreditAppTableGUID = intercompanyInvoiceTable.CreditAppTableGUID,
							CurrencyGUID = intercompanyInvoiceTable.CurrencyGUID,
							CustomerName = intercompanyInvoiceTable.CustomerName,
							CustomerTableGUID = intercompanyInvoiceTable.CustomerTableGUID,
							Dimension1GUID = intercompanyInvoiceTable.Dimension1GUID,
							Dimension2GUID = intercompanyInvoiceTable.Dimension2GUID,
							Dimension3GUID = intercompanyInvoiceTable.Dimension3GUID,
							Dimension4GUID = intercompanyInvoiceTable.Dimension4GUID,
							Dimension5GUID = intercompanyInvoiceTable.Dimension5GUID,
							DocumentId = intercompanyInvoiceTable.DocumentId,
							DocumentStatusGUID = intercompanyInvoiceTable.DocumentStatusGUID,
							DueDate = intercompanyInvoiceTable.DueDate,
							ExchangeRate = intercompanyInvoiceTable.ExchangeRate,
							IntercompanyGUID = intercompanyInvoiceTable.IntercompanyGUID,
							IntercompanyInvoiceId = intercompanyInvoiceTable.IntercompanyInvoiceId,
							InvoiceAddressTransGUID = intercompanyInvoiceTable.InvoiceAddressTransGUID,
							InvoiceAmount = intercompanyInvoiceTable.InvoiceAmount,
							InvoiceRevenueTypeGUID = intercompanyInvoiceTable.InvoiceRevenueTypeGUID,
							InvoiceText = intercompanyInvoiceTable.InvoiceText,
							InvoiceTypeGUID = intercompanyInvoiceTable.InvoiceTypeGUID,
							IssuedDate = intercompanyInvoiceTable.IssuedDate,
							OrigInvoiceAmount = intercompanyInvoiceTable.OrigInvoiceAmount,
							OrigInvoiceId = intercompanyInvoiceTable.OrigInvoiceId,
							OrigTaxInvoiceAmount = intercompanyInvoiceTable.OrigTaxInvoiceAmount,
							OrigTaxInvoiceId = intercompanyInvoiceTable.OrigTaxInvoiceId,
							ProductType = intercompanyInvoiceTable.ProductType,
							RefGUID = intercompanyInvoiceTable.RefGUID,
							RefType = intercompanyInvoiceTable.RefType,
							TaxBranchId = intercompanyInvoiceTable.TaxBranchId,
							TaxBranchName = intercompanyInvoiceTable.TaxBranchName,
							TaxTableGUID = intercompanyInvoiceTable.TaxTableGUID,
							WithholdingTaxTableGUID = intercompanyInvoiceTable.WithholdingTaxTableGUID,
							FeeLedgerAccount = intercompanyInvoiceTable.FeeLedgerAccount,
							TaxTable_TaxCode = taxTable != null ? taxTable.TaxCode : null,
							CNReason_ReasonId = cnReason != null ? cnReason.ReasonId : null,
							InterCompany_InterCompanyId = intercompany.IntercompanyId,
							Currency_CurrencyId = currency.CurrencyId,
							Dimension1_DimensionCode = dimesion1.DimensionCode,
							Dimension2_DimensionCode = dimesion2.DimensionCode,
							Dimension3_DimensionCode = dimesion3.DimensionCode,
							Dimension4_DimensionCode = dimesion4.DimensionCode,
							Dimension5_DimensionCode = dimesion5.DimensionCode,
						}).AsNoTracking()
						  .FirstOrDefault();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

}

        public decimal GetSumAmountByIntercompanyInvoiceId(Guid intercompanyInvoiceTableGUID, string statusId)
        {
			try
			{
				return (from intercompanyInvoiceSettlement in db.Set<IntercompanyInvoiceSettlement>()
					
										   join documentStatus in db.Set<DocumentStatus>()
										   on intercompanyInvoiceSettlement.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus
										   from documentStatus in ljdocumentStatus.DefaultIfEmpty()
										   where documentStatus.StatusId == statusId && intercompanyInvoiceSettlement.IntercompanyInvoiceTableGUID == intercompanyInvoiceTableGUID
										   select intercompanyInvoiceSettlement.SettleInvoiceAmount).Sum();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
    }
}
