using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IActionHistoryRepo
	{
		#region DropDown
		IEnumerable<SelectItem<ActionHistoryItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<ActionHistoryListView> GetListvw(SearchParameter search);
		ActionHistoryItemView GetByIdvw(Guid id);
		ActionHistory Find(params object[] keyValues);
		ActionHistory GetActionHistoryByIdNoTracking(Guid guid);
		ActionHistory CreateActionHistory(ActionHistory actionHistory);
		void CreateActionHistoryVoid(ActionHistory actionHistory);
		ActionHistory UpdateActionHistory(ActionHistory dbActionHistory, ActionHistory inputActionHistory, List<string> skipUpdateFields = null);
		void UpdateActionHistoryVoid(ActionHistory dbActionHistory, ActionHistory inputActionHistory, List<string> skipUpdateFields = null);
		void Remove(ActionHistory item);
		IEnumerable<ActionHistoryListView> GetActionHistoryByRefGUID(Guid refGUID);
		ActionHistoryItemView GetActionHistoryForK2(ActionHistoryK2Parm parm);
	}
	public class ActionHistoryRepo : CompanyBaseRepository<ActionHistory>, IActionHistoryRepo
	{
		public ActionHistoryRepo(SmartAppDbContext context) : base(context) { }
		public ActionHistoryRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ActionHistory GetActionHistoryByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ActionHistoryGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<ActionHistoryItemViewMap> GetDropDownQuery()
		{
			return (from actionHistory in Entity
					select new ActionHistoryItemViewMap
					{
						CompanyGUID = actionHistory.CompanyGUID,
						Owner = actionHistory.Owner,
						OwnerBusinessUnitGUID = actionHistory.OwnerBusinessUnitGUID,
						ActionHistoryGUID = actionHistory.ActionHistoryGUID,
						ActivityName = actionHistory.ActivityName,
						ActionName = actionHistory.ActionName,
						Comment = actionHistory.Comment,
						RefGUID = actionHistory.RefGUID
					});
		}
		public IEnumerable<SelectItem<ActionHistoryItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<ActionHistory>(search, SysParm.CompanyGUID);
				var actionHistory = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<ActionHistoryItemViewMap, ActionHistoryItemView>().ToDropDownItem(search.ExcludeRowData);


				return actionHistory;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<ActionHistoryListViewMap> GetListQuery()
		{
			return (from actionHistory in Entity
					select new ActionHistoryListViewMap
					{
						CompanyGUID = actionHistory.CompanyGUID,
						Owner = actionHistory.Owner,
						OwnerBusinessUnitGUID = actionHistory.OwnerBusinessUnitGUID,
						ActionHistoryGUID = actionHistory.ActionHistoryGUID,
						ActivityName = actionHistory.ActivityName,
						ActionName = actionHistory.ActionName,
						Comment = actionHistory.Comment,
						CreatedBy = actionHistory.CreatedBy,
						CreatedDateTime = actionHistory.CreatedDateTime,
						RefGUID = actionHistory.RefGUID
					});
		}
		public SearchResult<ActionHistoryListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<ActionHistoryListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<ActionHistory>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<ActionHistoryListViewMap, ActionHistoryListView>();
				result = list.SetSearchResult<ActionHistoryListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<ActionHistoryItemViewMap> GetItemQuery()
		{
			return (from actionHistory in Entity
					join company in db.Set<Company>()
					on actionHistory.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on actionHistory.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljActionHistoryOwnerBU
					from ownerBU in ljActionHistoryOwnerBU.DefaultIfEmpty()
					select new ActionHistoryItemViewMap
					{
						CompanyGUID = actionHistory.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = actionHistory.Owner,
						OwnerBusinessUnitGUID = actionHistory.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = actionHistory.CreatedBy,
						CreatedDateTime = actionHistory.CreatedDateTime,
						ModifiedBy = actionHistory.ModifiedBy,
						ModifiedDateTime = actionHistory.ModifiedDateTime,
						ActionHistoryGUID = actionHistory.ActionHistoryGUID,
						ActivityName = actionHistory.ActivityName,
						ActionName = actionHistory.ActionName,
						Comment = actionHistory.Comment,
						WorkflowName = actionHistory.WorkflowName,
						SerialNo = actionHistory.SerialNo,
						RefGUID = actionHistory.RefGUID,
					
						RowVersion = actionHistory.RowVersion,
					});
		}
		public ActionHistoryItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<ActionHistoryItemViewMap, ActionHistoryItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public ActionHistory CreateActionHistory(ActionHistory actionHistory)
		{
			try
			{
				actionHistory.ActionHistoryGUID = Guid.NewGuid();
				base.Add(actionHistory);
				return actionHistory;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateActionHistoryVoid(ActionHistory actionHistory)
		{
			try
			{
				CreateActionHistory(actionHistory);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ActionHistory UpdateActionHistory(ActionHistory dbActionHistory, ActionHistory inputActionHistory, List<string> skipUpdateFields = null)
		{
			try
			{
				dbActionHistory = dbActionHistory.MapUpdateValues<ActionHistory>(inputActionHistory);
				base.Update(dbActionHistory);
				return dbActionHistory;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateActionHistoryVoid(ActionHistory dbActionHistory, ActionHistory inputActionHistory, List<string> skipUpdateFields = null)
		{
			try
			{
				dbActionHistory = dbActionHistory.MapUpdateValues<ActionHistory>(inputActionHistory, skipUpdateFields);
				base.Update(dbActionHistory);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public IEnumerable<ActionHistoryListView> GetActionHistoryByRefGUID(Guid refGUID)
        {
            try
            {
				SearchCondition cond = SearchConditionService.GetActionHistoryRefGUIDCondition(refGUID);
				SearchParameter search = new SearchParameter
				{
					Conditions = new List<SearchCondition>() { cond }
				};
				var predicate = GetFilterLevelPredicate<ActionHistory>(search, SysParm.CompanyGUID);
				var result = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderByDescending(o => o.CreatedDateTime)
											.AsNoTracking()
											.ToMaps<ActionHistoryListViewMap, ActionHistoryListView>();
				return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public ActionHistoryItemView GetActionHistoryForK2(ActionHistoryK2Parm parm)
        {
            try
            {
				Guid refGUID = parm.RefGUID.StringToGuid();
				List<SearchCondition> conds = SearchConditionService.GetActionHistoryForK2Condition(refGUID, parm.ActionName, parm.ActivityName);
				SearchParameter search = new SearchParameter
				{
					Conditions = conds
				};
				var predicate = GetFilterLevelPredicate<ActionHistory>(search, SysParm.CompanyGUID);
				var result = GetItemQuery().Where(predicate.Predicates, predicate.Values)
											.OrderByDescending(o => o.CreatedDateTime)
											.AsNoTracking()
											.FirstOrDefault()
											.ToMap<ActionHistoryItemViewMap, ActionHistoryItemView>();
				return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
	}
}

