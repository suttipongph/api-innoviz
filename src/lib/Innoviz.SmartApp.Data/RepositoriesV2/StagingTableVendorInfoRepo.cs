using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IStagingTableVendorInfoRepo
	{
		#region DropDown
		IEnumerable<SelectItem<StagingTableVendorInfoItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<StagingTableVendorInfoListView> GetListvw(SearchParameter search);
		StagingTableVendorInfoItemView GetByIdvw(Guid id);
		StagingTableVendorInfo Find(params object[] keyValues);
		StagingTableVendorInfo GetStagingTableVendorInfoByIdNoTracking(Guid guid);
		StagingTableVendorInfo CreateStagingTableVendorInfo(StagingTableVendorInfo stagingTableVendorInfo);
		void CreateStagingTableVendorInfoVoid(StagingTableVendorInfo stagingTableVendorInfo);
		StagingTableVendorInfo UpdateStagingTableVendorInfo(StagingTableVendorInfo dbStagingTableVendorInfo, StagingTableVendorInfo inputStagingTableVendorInfo, List<string> skipUpdateFields = null);
		void UpdateStagingTableVendorInfoVoid(StagingTableVendorInfo dbStagingTableVendorInfo, StagingTableVendorInfo inputStagingTableVendorInfo, List<string> skipUpdateFields = null);
		void Remove(StagingTableVendorInfo item);
		StagingTableVendorInfo GetStagingTableVendorInfoByIdNoTrackingByAccessLevel(Guid guid);
		List<StagingTableVendorInfo> GetStagingTableVendorInfoByProcessTransGUIDNoTracking(IEnumerable<Guid> processTransGuids);
	}
	public class StagingTableVendorInfoRepo : CompanyBaseRepository<StagingTableVendorInfo>, IStagingTableVendorInfoRepo
	{
		public StagingTableVendorInfoRepo(SmartAppDbContext context) : base(context) { }
		public StagingTableVendorInfoRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public StagingTableVendorInfo GetStagingTableVendorInfoByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.StagingTableVendorInfoGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<StagingTableVendorInfoItemViewMap> GetDropDownQuery()
		{
			return (from stagingTableVendorInfo in Entity
					select new StagingTableVendorInfoItemViewMap
					{
						CompanyGUID = stagingTableVendorInfo.CompanyGUID,
						Owner = stagingTableVendorInfo.Owner,
						OwnerBusinessUnitGUID = stagingTableVendorInfo.OwnerBusinessUnitGUID,
						StagingTableVendorInfoGUID = stagingTableVendorInfo.StagingTableVendorInfoGUID
					});
		}
		public IEnumerable<SelectItem<StagingTableVendorInfoItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<StagingTableVendorInfo>(search, SysParm.CompanyGUID);
				var stagingTableVendorInfo = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<StagingTableVendorInfoItemViewMap, StagingTableVendorInfoItemView>().ToDropDownItem(search.ExcludeRowData);


				return stagingTableVendorInfo;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<StagingTableVendorInfoListViewMap> GetListQuery()
		{
			return (from stagingTableVendorInfo in Entity
				select new StagingTableVendorInfoListViewMap
				{
					CompanyGUID = stagingTableVendorInfo.CompanyGUID,
					Owner = stagingTableVendorInfo.Owner,
					OwnerBusinessUnitGUID = stagingTableVendorInfo.OwnerBusinessUnitGUID,
					StagingTableVendorInfoGUID = stagingTableVendorInfo.StagingTableVendorInfoGUID,
					RecordType = stagingTableVendorInfo.RecordType,
					VendorId = stagingTableVendorInfo.VendorId,
					Name = stagingTableVendorInfo.Name,
					CurrencyId = stagingTableVendorInfo.CurrencyId,
					VendGroupId = stagingTableVendorInfo.VendGroupId,
					BankAccountName = stagingTableVendorInfo.BankAccountName,
					BankAccount = stagingTableVendorInfo.BankAccount,
					BankGroupId = stagingTableVendorInfo.BankGroupId,
					ProcessTransGUID = stagingTableVendorInfo.ProcessTransGUID
				});
		}
		public SearchResult<StagingTableVendorInfoListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<StagingTableVendorInfoListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<StagingTableVendorInfo>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<StagingTableVendorInfoListViewMap, StagingTableVendorInfoListView>();
				result = list.SetSearchResult<StagingTableVendorInfoListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<StagingTableVendorInfoItemViewMap> GetItemQuery()
		{
			return (from stagingTableVendorInfo in Entity
					join company in db.Set<Company>()
					on stagingTableVendorInfo.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on stagingTableVendorInfo.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljStagingTableVendorInfoOwnerBU
					from ownerBU in ljStagingTableVendorInfoOwnerBU.DefaultIfEmpty()
					select new StagingTableVendorInfoItemViewMap
					{
						CompanyGUID = stagingTableVendorInfo.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = stagingTableVendorInfo.Owner,
						OwnerBusinessUnitGUID = stagingTableVendorInfo.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = stagingTableVendorInfo.CreatedBy,
						CreatedDateTime = stagingTableVendorInfo.CreatedDateTime,
						ModifiedBy = stagingTableVendorInfo.ModifiedBy,
						ModifiedDateTime = stagingTableVendorInfo.ModifiedDateTime,
						StagingTableVendorInfoGUID = stagingTableVendorInfo.StagingTableVendorInfoGUID,
						Address = stagingTableVendorInfo.Address,
						AddressName = stagingTableVendorInfo.AddressName,
						AltName = stagingTableVendorInfo.AltName,
						BankAccount = stagingTableVendorInfo.BankAccount,
						BankAccountName = stagingTableVendorInfo.BankAccountName,
						BankBranch = stagingTableVendorInfo.BankBranch,
						BankGroupId = stagingTableVendorInfo.BankGroupId,
						CountryId = stagingTableVendorInfo.CountryId,
						CurrencyId = stagingTableVendorInfo.CurrencyId,
						DistrictId = stagingTableVendorInfo.DistrictId,
						Name = stagingTableVendorInfo.Name,
						PostalCode = stagingTableVendorInfo.PostalCode,
						ProcessTransGUID = stagingTableVendorInfo.ProcessTransGUID,
						ProvinceId = stagingTableVendorInfo.ProvinceId,
						RecordType = stagingTableVendorInfo.RecordType,
						SubDistrictId = stagingTableVendorInfo.SubDistrictId,
						TaxBranchId = stagingTableVendorInfo.TaxBranchId,
						VendGroupId = stagingTableVendorInfo.VendGroupId,
						VendorId = stagingTableVendorInfo.VendorId,
						VendorPaymentTransGUID = stagingTableVendorInfo.VendorPaymentTransGUID,
						VendorTaxId = stagingTableVendorInfo.VendorTaxId,
						StagingTableCompanyId = stagingTableVendorInfo.CompanyId,
					
						RowVersion = stagingTableVendorInfo.RowVersion,
					});
		}
		public StagingTableVendorInfoItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<StagingTableVendorInfoItemViewMap, StagingTableVendorInfoItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public StagingTableVendorInfo CreateStagingTableVendorInfo(StagingTableVendorInfo stagingTableVendorInfo)
		{
			try
			{
				stagingTableVendorInfo.StagingTableVendorInfoGUID = Guid.NewGuid();
				base.Add(stagingTableVendorInfo);
				return stagingTableVendorInfo;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateStagingTableVendorInfoVoid(StagingTableVendorInfo stagingTableVendorInfo)
		{
			try
			{
				CreateStagingTableVendorInfo(stagingTableVendorInfo);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public StagingTableVendorInfo UpdateStagingTableVendorInfo(StagingTableVendorInfo dbStagingTableVendorInfo, StagingTableVendorInfo inputStagingTableVendorInfo, List<string> skipUpdateFields = null)
		{
			try
			{
				dbStagingTableVendorInfo = dbStagingTableVendorInfo.MapUpdateValues<StagingTableVendorInfo>(inputStagingTableVendorInfo);
				base.Update(dbStagingTableVendorInfo);
				return dbStagingTableVendorInfo;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateStagingTableVendorInfoVoid(StagingTableVendorInfo dbStagingTableVendorInfo, StagingTableVendorInfo inputStagingTableVendorInfo, List<string> skipUpdateFields = null)
		{
			try
			{
				dbStagingTableVendorInfo = dbStagingTableVendorInfo.MapUpdateValues<StagingTableVendorInfo>(inputStagingTableVendorInfo, skipUpdateFields);
				base.Update(dbStagingTableVendorInfo);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public StagingTableVendorInfo GetStagingTableVendorInfoByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.StagingTableVendorInfoGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<StagingTableVendorInfo> GetStagingTableVendorInfoByProcessTransGUIDNoTracking(IEnumerable<Guid> processTransGuids)
        {
            try
            {
				var result = Entity.Where(w => w.ProcessTransGUID.HasValue && processTransGuids.Contains(w.ProcessTransGUID.Value))
												.AsNoTracking()
												.ToList();
				return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
	}
}

