using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IProjectReferenceTransRepo
	{
		#region DropDown
		IEnumerable<SelectItem<ProjectReferenceTransItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<ProjectReferenceTransListView> GetListvw(SearchParameter search);
		ProjectReferenceTransItemView GetByIdvw(Guid id);
		ProjectReferenceTrans Find(params object[] keyValues);
		ProjectReferenceTrans GetProjectReferenceTransByIdNoTracking(Guid guid);
		ProjectReferenceTrans CreateProjectReferenceTrans(ProjectReferenceTrans projectReferenceTrans);
		void CreateProjectReferenceTransVoid(ProjectReferenceTrans projectReferenceTrans);
		ProjectReferenceTrans UpdateProjectReferenceTrans(ProjectReferenceTrans dbProjectReferenceTrans, ProjectReferenceTrans inputProjectReferenceTrans, List<string> skipUpdateFields = null);
		void UpdateProjectReferenceTransVoid(ProjectReferenceTrans dbProjectReferenceTrans, ProjectReferenceTrans inputProjectReferenceTrans, List<string> skipUpdateFields = null);
		void Remove(ProjectReferenceTrans item);
		void ValidateAdd(ProjectReferenceTrans item);
		void ValidateAdd(IEnumerable<ProjectReferenceTrans> items);
	}
	public class ProjectReferenceTransRepo : CompanyBaseRepository<ProjectReferenceTrans>, IProjectReferenceTransRepo
	{
		public ProjectReferenceTransRepo(SmartAppDbContext context) : base(context) { }
		public ProjectReferenceTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ProjectReferenceTrans GetProjectReferenceTransByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ProjectReferenceTransGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<ProjectReferenceTransItemViewMap> GetDropDownQuery()
		{
			return (from projectReferenceTrans in Entity
					select new ProjectReferenceTransItemViewMap
					{
						CompanyGUID = projectReferenceTrans.CompanyGUID,
						Owner = projectReferenceTrans.Owner,
						OwnerBusinessUnitGUID = projectReferenceTrans.OwnerBusinessUnitGUID,
						ProjectReferenceTransGUID = projectReferenceTrans.ProjectReferenceTransGUID
					});
		}
		public IEnumerable<SelectItem<ProjectReferenceTransItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<ProjectReferenceTrans>(search, SysParm.CompanyGUID);
				var projectReferenceTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<ProjectReferenceTransItemViewMap, ProjectReferenceTransItemView>().ToDropDownItem(search.ExcludeRowData);


				return projectReferenceTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<ProjectReferenceTransListViewMap> GetListQuery()
		{
			return (from projectReferenceTrans in Entity
				select new ProjectReferenceTransListViewMap
				{
						CompanyGUID = projectReferenceTrans.CompanyGUID,
						Owner = projectReferenceTrans.Owner,
						OwnerBusinessUnitGUID = projectReferenceTrans.OwnerBusinessUnitGUID,
						ProjectReferenceTransGUID = projectReferenceTrans.ProjectReferenceTransGUID,
						ProjectCompanyName = projectReferenceTrans.ProjectCompanyName,
						ProjectName = projectReferenceTrans.ProjectName,
						ProjectValue = projectReferenceTrans.ProjectValue,
						ProjectCompletion = projectReferenceTrans.ProjectCompletion,
						ProjectStatus = projectReferenceTrans.ProjectStatus,
					    RefGUID = projectReferenceTrans.RefGUID,
					    RefType = projectReferenceTrans.RefType,
				});
		}
		public SearchResult<ProjectReferenceTransListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<ProjectReferenceTransListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<ProjectReferenceTrans>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<ProjectReferenceTransListViewMap, ProjectReferenceTransListView>();
				result = list.SetSearchResult<ProjectReferenceTransListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<ProjectReferenceTransItemViewMap> GetItemQuery()
		{
			return (from projectReferenceTrans in Entity
					join company in db.Set<Company>()
					on projectReferenceTrans.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on projectReferenceTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljProjectReferenceTransOwnerBU
					from ownerBU in ljProjectReferenceTransOwnerBU.DefaultIfEmpty()
					join customer in db.Set<CustomerTable>() on projectReferenceTrans.RefGUID equals customer.CustomerTableGUID into ljljProjectReferenceTransCustomer
					from customer in ljljProjectReferenceTransCustomer.DefaultIfEmpty()
					select new ProjectReferenceTransItemViewMap
					{
						CompanyGUID = projectReferenceTrans.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = projectReferenceTrans.Owner,
						OwnerBusinessUnitGUID = projectReferenceTrans.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = projectReferenceTrans.CreatedBy,
						CreatedDateTime = projectReferenceTrans.CreatedDateTime,
						ModifiedBy = projectReferenceTrans.ModifiedBy,
						ModifiedDateTime = projectReferenceTrans.ModifiedDateTime,
						ProjectReferenceTransGUID = projectReferenceTrans.ProjectReferenceTransGUID,
						ProjectCompanyName = projectReferenceTrans.ProjectCompanyName,
						ProjectCompletion = projectReferenceTrans.ProjectCompletion,
						ProjectName = projectReferenceTrans.ProjectName,
						ProjectStatus = projectReferenceTrans.ProjectStatus,
						ProjectValue = projectReferenceTrans.ProjectValue,
						RefGUID = projectReferenceTrans.RefGUID,
						RefType = projectReferenceTrans.RefType,
						Remark = projectReferenceTrans.Remark,
						RefId = (customer != null && projectReferenceTrans.RefType == (int)RefType.Customer) ? customer.CustomerId: null,
					
						RowVersion = projectReferenceTrans.RowVersion,
					});
		}
		public ProjectReferenceTransItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<ProjectReferenceTransItemViewMap, ProjectReferenceTransItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public ProjectReferenceTrans CreateProjectReferenceTrans(ProjectReferenceTrans projectReferenceTrans)
		{
			try
			{
				projectReferenceTrans.ProjectReferenceTransGUID = Guid.NewGuid();
				base.Add(projectReferenceTrans);
				return projectReferenceTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateProjectReferenceTransVoid(ProjectReferenceTrans projectReferenceTrans)
		{
			try
			{
				CreateProjectReferenceTrans(projectReferenceTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ProjectReferenceTrans UpdateProjectReferenceTrans(ProjectReferenceTrans dbProjectReferenceTrans, ProjectReferenceTrans inputProjectReferenceTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbProjectReferenceTrans = dbProjectReferenceTrans.MapUpdateValues<ProjectReferenceTrans>(inputProjectReferenceTrans);
				base.Update(dbProjectReferenceTrans);
				return dbProjectReferenceTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateProjectReferenceTransVoid(ProjectReferenceTrans dbProjectReferenceTrans, ProjectReferenceTrans inputProjectReferenceTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbProjectReferenceTrans = dbProjectReferenceTrans.MapUpdateValues<ProjectReferenceTrans>(inputProjectReferenceTrans, skipUpdateFields);
				base.Update(dbProjectReferenceTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

