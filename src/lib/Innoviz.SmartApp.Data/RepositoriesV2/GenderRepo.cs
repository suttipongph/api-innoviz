using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IGenderRepo
	{
		#region DropDown
		IEnumerable<SelectItem<GenderItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<GenderListView> GetListvw(SearchParameter search);
		GenderItemView GetByIdvw(Guid id);
		Gender Find(params object[] keyValues);
		Gender GetGenderByIdNoTracking(Guid guid);
		Gender CreateGender(Gender gender);
		void CreateGenderVoid(Gender gender);
		Gender UpdateGender(Gender dbGender, Gender inputGender, List<string> skipUpdateFields = null);
		void UpdateGenderVoid(Gender dbGender, Gender inputGender, List<string> skipUpdateFields = null);
		void Remove(Gender item);
	}
	public class GenderRepo : CompanyBaseRepository<Gender>, IGenderRepo
	{
		public GenderRepo(SmartAppDbContext context) : base(context) { }
		public GenderRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public Gender GetGenderByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.GenderGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<GenderItemViewMap> GetDropDownQuery()
		{
			return (from gender in Entity
					select new GenderItemViewMap
					{
						CompanyGUID = gender.CompanyGUID,
						Owner = gender.Owner,
						OwnerBusinessUnitGUID = gender.OwnerBusinessUnitGUID,
						GenderGUID = gender.GenderGUID,
						GenderId = gender.GenderId,
						Description = gender.Description
					});
		}
		public IEnumerable<SelectItem<GenderItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<Gender>(search, SysParm.CompanyGUID);
				var gender = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<GenderItemViewMap, GenderItemView>().ToDropDownItem(search.ExcludeRowData);


				return gender;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<GenderListViewMap> GetListQuery()
		{
			return (from gender in Entity
				select new GenderListViewMap
				{
						CompanyGUID = gender.CompanyGUID,
						Owner = gender.Owner,
						OwnerBusinessUnitGUID = gender.OwnerBusinessUnitGUID,
						GenderGUID = gender.GenderGUID,
						GenderId = gender.GenderId,
						Description = gender.Description
				});
		}
		public SearchResult<GenderListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<GenderListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<Gender>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<GenderListViewMap, GenderListView>();
				result = list.SetSearchResult<GenderListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<GenderItemViewMap> GetItemQuery()
		{
			return (from gender in Entity
					join company in db.Set<Company>()
					on gender.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on gender.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljGenderOwnerBU
					from ownerBU in ljGenderOwnerBU.DefaultIfEmpty()
					select new GenderItemViewMap
					{
						CompanyGUID = gender.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = gender.Owner,
						OwnerBusinessUnitGUID = gender.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = gender.CreatedBy,
						CreatedDateTime = gender.CreatedDateTime,
						ModifiedBy = gender.ModifiedBy,
						ModifiedDateTime = gender.ModifiedDateTime,
						GenderGUID = gender.GenderGUID,
						Description = gender.Description,
						GenderId = gender.GenderId,
					
						RowVersion = gender.RowVersion,
					});
		}
		public GenderItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<GenderItemViewMap, GenderItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public Gender CreateGender(Gender gender)
		{
			try
			{
				gender.GenderGUID = Guid.NewGuid();
				base.Add(gender);
				return gender;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateGenderVoid(Gender gender)
		{
			try
			{
				CreateGender(gender);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public Gender UpdateGender(Gender dbGender, Gender inputGender, List<string> skipUpdateFields = null)
		{
			try
			{
				dbGender = dbGender.MapUpdateValues<Gender>(inputGender);
				base.Update(dbGender);
				return dbGender;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateGenderVoid(Gender dbGender, Gender inputGender, List<string> skipUpdateFields = null)
		{
			try
			{
				dbGender = dbGender.MapUpdateValues<Gender>(inputGender, skipUpdateFields);
				base.Update(dbGender);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

