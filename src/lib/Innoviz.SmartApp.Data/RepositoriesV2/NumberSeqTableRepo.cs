using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface INumberSeqTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<NumberSeqTableItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		NumberSeqTable GetNumberSeqTableIdNoTrackingByAccessLevel(Guid guid);
		SearchResult<NumberSeqTableListView> GetListvw(SearchParameter search);
		NumberSeqTableItemView GetByIdvw(Guid id);
		NumberSeqTable Find(params object[] keyValues);
		NumberSeqTable GetByNumberSeqTableGUID(Guid? guid);
		NumberSeqTable CreateNumberSeqTable(NumberSeqTable numberSeqTable);
		void CreateNumberSeqTableVoid(NumberSeqTable numberSeqTable);
		NumberSeqTable UpdateNumberSeqTable(NumberSeqTable dbNumberSeqTable, NumberSeqTable inputNumberSeqTable, List<string> skipUpdateFields = null);
		void UpdateNumberSeqTableVoid(NumberSeqTable dbNumberSeqTable, NumberSeqTable inputNumberSeqTable, List<string> skipUpdateFields = null);
		void Remove(NumberSeqTable item);
		void CheckAccessLevel(NumberSeqTable item);
		#region function
		IEnumerable<NumberSeqTableItemView> GetNumberSeqTableByRefId(List<string> refIds);
		#endregion
		NumberSeqTable GetNumberSeqTableByRefIdAndCompanyGUIDNoTracking(Guid companyGUID, string refId);
		List<NumberSeqTable> GetNumberSeqTableByIdNoTracking(IEnumerable<Guid> numberSeqTableGuids);
		NumberSeqTable GetNumberSeqTableByInternalMainAgreementNumberSeqAndCompanyAndProductTypeNoTracking(Guid companyGUID, int productType);
	}
	public class NumberSeqTableRepo : CompanyBaseRepository<NumberSeqTable>, INumberSeqTableRepo
	{
		public NumberSeqTableRepo(SmartAppDbContext context) : base(context) { }
		public NumberSeqTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public NumberSeqTable GetByNumberSeqTableGUID(Guid? guid)
		{
			try
			{
				NumberSeqTable numberSeqTable = Entity.Where(item => item.NumberSeqTableGUID == guid).AsNoTracking().FirstOrDefault();
				if (numberSeqTable == null)
				{
					SmartAppException ex = new SmartAppException("ERROR.ERROR");
					ex.AddData("ERROR.90009");
					throw ex;
				}
				else
					return numberSeqTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public NumberSeqTable GetNumberSeqTableIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.NumberSeqTableGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<NumberSeqTable> GetNumberSeqTableByIdNoTracking(IEnumerable<Guid> numberSeqTableGuids)
        {
            try
            {
				var result = Entity.Where(w => numberSeqTableGuids.Contains(w.NumberSeqTableGUID))
									.AsNoTracking()
									.ToList();
				return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		#region DropDown
		private IQueryable<NumberSeqTableItemViewMap> GetDropDownQuery()
		{
			return (from numberSeqTable in Entity
					select new NumberSeqTableItemViewMap
					{
						CompanyGUID = numberSeqTable.CompanyGUID,
						Owner = numberSeqTable.Owner,
						OwnerBusinessUnitGUID = numberSeqTable.OwnerBusinessUnitGUID,
						NumberSeqTableGUID = numberSeqTable.NumberSeqTableGUID,
						NumberSeqCode = numberSeqTable.NumberSeqCode,
						Description = numberSeqTable.Description,
						NumberSeqTable_Values = SmartAppUtil.GetDropDownLabel(numberSeqTable.NumberSeqCode,numberSeqTable.Description),
					});
		}
		public IEnumerable<SelectItem<NumberSeqTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<NumberSeqTable>(search, SysParm.CompanyGUID);
				var numberSeqTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<NumberSeqTableItemViewMap, NumberSeqTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return numberSeqTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<NumberSeqTableListViewMap> GetListQuery()
		{
			return (from numberSeqTable in Entity
				select new NumberSeqTableListViewMap
				{
						CompanyGUID = numberSeqTable.CompanyGUID,
						Owner = numberSeqTable.Owner,
						OwnerBusinessUnitGUID = numberSeqTable.OwnerBusinessUnitGUID,
						NumberSeqTableGUID = numberSeqTable.NumberSeqTableGUID,
						Description = numberSeqTable.Description,
						Largest = numberSeqTable.Largest,
						Manual = numberSeqTable.Manual,
						Next = numberSeqTable.Next,
						NumberSeqCode = numberSeqTable.NumberSeqCode,
						RowVersion = numberSeqTable.RowVersion,
						Smallest = numberSeqTable.Smallest,
				});
		}
		public SearchResult<NumberSeqTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<NumberSeqTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<NumberSeqTable>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<NumberSeqTableListViewMap, NumberSeqTableListView>();
				result = list.SetSearchResult<NumberSeqTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<NumberSeqTableItemViewMap> GetItemQuery()
		{
			return (from numberSeqTable in Entity
					join ownerBU in db.Set<BusinessUnit>()
					on numberSeqTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljNumberSeqTableOwnerBU
					from ownerBU in ljNumberSeqTableOwnerBU.DefaultIfEmpty()
					join company in db.Set<Company>()
					on numberSeqTable.CompanyGUID equals company.CompanyGUID into ljNumberSeqTableCompany
					from company in ljNumberSeqTableCompany.DefaultIfEmpty()
					select new NumberSeqTableItemViewMap
					{
						CompanyGUID = numberSeqTable.CompanyGUID,
						Owner = numberSeqTable.Owner,
						OwnerBusinessUnitGUID = numberSeqTable.OwnerBusinessUnitGUID,
						CreatedBy = numberSeqTable.CreatedBy,
						CreatedDateTime = numberSeqTable.CreatedDateTime,
						CompanyId = company.CompanyId,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						ModifiedBy = numberSeqTable.ModifiedBy,
						ModifiedDateTime = numberSeqTable.ModifiedDateTime,
						NumberSeqTableGUID = numberSeqTable.NumberSeqTableGUID,
						Description = numberSeqTable.Description,
						Largest = numberSeqTable.Largest,
						Manual = numberSeqTable.Manual,
						Next = numberSeqTable.Next,
						NumberSeqCode = numberSeqTable.NumberSeqCode,
						RowVersion = numberSeqTable.RowVersion,
						Smallest = numberSeqTable.Smallest,
					});
		}
		public NumberSeqTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<NumberSeqTableItemViewMap, NumberSeqTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public NumberSeqTable CreateNumberSeqTable(NumberSeqTable numberSeqTable)
		{
			try
			{
				numberSeqTable.NumberSeqTableGUID = Guid.NewGuid();
				base.Add(numberSeqTable);
				return numberSeqTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateNumberSeqTableVoid(NumberSeqTable numberSeqTable)
		{
			try
			{
				CreateNumberSeqTable(numberSeqTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public NumberSeqTable UpdateNumberSeqTable(NumberSeqTable dbNumberSeqTable, NumberSeqTable inputNumberSeqTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbNumberSeqTable = dbNumberSeqTable.MapUpdateValues<NumberSeqTable>(inputNumberSeqTable);
				base.Update(dbNumberSeqTable);
				return dbNumberSeqTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateNumberSeqTableVoid(NumberSeqTable dbNumberSeqTable, NumberSeqTable inputNumberSeqTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbNumberSeqTable = dbNumberSeqTable.MapUpdateValues<NumberSeqTable>(inputNumberSeqTable, skipUpdateFields);
				base.Update(dbNumberSeqTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #endregion Create, Update, Delete

        #region function
		public IEnumerable<NumberSeqTableItemView> GetNumberSeqTableByRefId(List<string> refIds)
        {
			try
			{
				var result = (from numberSeqTable in Entity
							  join numberSeqParameter in db.Set<NumberSeqParameter>()
							  on numberSeqTable.NumberSeqTableGUID equals numberSeqParameter.NumberSeqTableGUID into ljNumberTableNumberParam
							  from numberSeqParameter in ljNumberTableNumberParam.DefaultIfEmpty()
							  where refIds.Any(a => a == numberSeqParameter.ReferenceId) && numberSeqTable.CompanyGUID == db.GetCompanyFilter().StringToGuid()
							  select new NumberSeqTableItemView
							  {
								  NumberSeqTableGUID = numberSeqTable.NumberSeqTableGUID.ToString(),
								  Manual = numberSeqTable.Manual,
								  NumberSeqParameter_ReferenceId = numberSeqParameter.ReferenceId
							  }).AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		public NumberSeqTable GetNumberSeqTableByRefIdAndCompanyGUIDNoTracking(Guid companyGUID, string refId)
        {
            try
            {
				var result =
					(from numberSeqTable in Entity
					 join numberSeqParameter in db.Set<NumberSeqParameter>().Where(w => w.ReferenceId == refId && w.CompanyGUID == companyGUID)
					 on numberSeqTable.NumberSeqTableGUID equals numberSeqParameter.NumberSeqTableGUID
					 select numberSeqTable)
					 .AsNoTracking()
					 .FirstOrDefault();

				if (result == null)
				{
					SmartAppException ex = new SmartAppException("ERROR.ERROR");
					ex.AddData("ERROR.90009");
					throw ex;
				}
				return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public NumberSeqTable GetNumberSeqTableByInternalMainAgreementNumberSeqAndCompanyAndProductTypeNoTracking(Guid companyGUID, int productType)
        {
            try
            {
				var result = (from numberSeqTable in Entity
							  join numberSeqSetupByProductType in db.Set<NumberSeqSetupByProductType>().Where(w => w.ProductType == productType)
							  on numberSeqTable.NumberSeqTableGUID equals numberSeqSetupByProductType.InternalMainAgreementNumberSeqGUID
							  where numberSeqTable.CompanyGUID == companyGUID 
							  select numberSeqTable)
					 .AsNoTracking()
					 .FirstOrDefault();

				return result;
            }
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}
