using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICurrencyRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CurrencyItemView>> GetDropDownItem(SearchParameter search);
		IEnumerable<SelectItem<CurrencyItemView>> GetDropDownItemPlusExchangeRate(SearchParameter search);
		
		#endregion DropDown
		SearchResult<CurrencyListView> GetListvw(SearchParameter search);
		CurrencyItemView GetByIdvw(Guid id);
		Currency Find(params object[] keyValues);
		Currency GetCurrencyByIdNoTracking(Guid guid);
		Currency GetCurrencyByIdNoTrackingByAccessLevel(Guid guid);
		Currency CreateCurrency(Currency currency);
		void CreateCurrencyVoid(Currency currency);
		Currency UpdateCurrency(Currency dbCurrency, Currency inputCurrency, List<string> skipUpdateFields = null);
		void UpdateCurrencyVoid(Currency dbCurrency, Currency inputCurrency, List<string> skipUpdateFields = null);
		void Remove(Currency item);
		Currency GetHomeCurrencyByCompanyIdNoTrackingByAccessLevel(Guid guid);
		List<Currency> GetCurrencyByCompanyNoTracking(Guid companyGUID);

	}
	public class CurrencyRepo : CompanyBaseRepository<Currency>, ICurrencyRepo
	{
		public CurrencyRepo(SmartAppDbContext context) : base(context) { }
		public CurrencyRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public Currency GetCurrencyByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CurrencyGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public Currency GetCurrencyByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CurrencyGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public Currency GetHomeCurrencyByCompanyIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = (from currency in Entity
							  join companyParameter in db.Set<CompanyParameter>()
							  on currency.CurrencyGUID equals companyParameter.HomeCurrencyGUID
							  where companyParameter.CompanyGUID == guid
							  select currency)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CurrencyItemViewMap> GetDropDownQuery()
		{
			return (from currency in Entity
					select new CurrencyItemViewMap
					{
						CompanyGUID = currency.CompanyGUID,
						Owner = currency.Owner,
						OwnerBusinessUnitGUID = currency.OwnerBusinessUnitGUID,
						CurrencyGUID = currency.CurrencyGUID,
						CurrencyId = currency.CurrencyId,
						Name = currency.Name
					});
		}
		public IEnumerable<SelectItem<CurrencyItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<Currency>(search, SysParm.CompanyGUID);
				var currency = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CurrencyItemViewMap, CurrencyItemView>().ToDropDownItem(search.ExcludeRowData);


				return currency;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private IQueryable<CurrencyItemViewMap> GetDropDownQueryPlusExchangeRate()
		{
			return (from currency in Entity
					join exchangeRate in db.Set<ExchangeRate>()
					on currency.CurrencyGUID equals exchangeRate.CurrencyGUID into ljexchangeRate
					from exchangeRate in ljexchangeRate.DefaultIfEmpty()
					select new CurrencyItemViewMap
					{
						CompanyGUID = currency.CompanyGUID,
						Owner = currency.Owner,
						OwnerBusinessUnitGUID = currency.OwnerBusinessUnitGUID,
						CurrencyGUID = currency.CurrencyGUID,
						CurrencyId = currency.CurrencyId,
						Name = currency.Name,
						ExchangeRate_Rate = exchangeRate.Rate
					});
		}
		public IEnumerable<SelectItem<CurrencyItemView>> GetDropDownItemPlusExchangeRate(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<Currency>(search, SysParm.CompanyGUID);
				var currency = GetDropDownQueryPlusExchangeRate().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CurrencyItemViewMap, CurrencyItemView>().ToDropDownItem(search.ExcludeRowData);


				return currency;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CurrencyListViewMap> GetListQuery()
		{
			return (from currency in Entity
				select new CurrencyListViewMap
				{
						CompanyGUID = currency.CompanyGUID,
						Owner = currency.Owner,
						OwnerBusinessUnitGUID = currency.OwnerBusinessUnitGUID,
						CurrencyGUID = currency.CurrencyGUID,
						CurrencyId = currency.CurrencyId,
						Name = currency.Name
				});
		}
		public SearchResult<CurrencyListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CurrencyListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<Currency>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CurrencyListViewMap, CurrencyListView>();
				result = list.SetSearchResult<CurrencyListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CurrencyItemViewMap> GetItemQuery()
		{
			return (from currency in Entity
					join company in db.Set<Company>()
					on currency.CompanyGUID equals company.CompanyGUID into ljCurrencyCompany
					from company in ljCurrencyCompany.DefaultIfEmpty()
					join ownerBU in db.Set<BusinessUnit>()
					on currency.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCurrencyOwnerBU
					from ownerBU in ljCurrencyOwnerBU.DefaultIfEmpty()
					select new CurrencyItemViewMap
					{
						CompanyGUID = currency.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = currency.Owner,
						OwnerBusinessUnitGUID = currency.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = currency.CreatedBy,
						CreatedDateTime = currency.CreatedDateTime,
						ModifiedBy = currency.ModifiedBy,
						ModifiedDateTime = currency.ModifiedDateTime,
						CurrencyGUID = currency.CurrencyGUID,
						CurrencyId = currency.CurrencyId,
						Name = currency.Name,
					
						RowVersion = currency.RowVersion,
					});
		}
		public CurrencyItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<CurrencyItemViewMap, CurrencyItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public Currency CreateCurrency(Currency currency)
		{
			try
			{
				currency.CurrencyGUID = Guid.NewGuid();
				base.Add(currency);
				return currency;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCurrencyVoid(Currency currency)
		{
			try
			{
				CreateCurrency(currency);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public Currency UpdateCurrency(Currency dbCurrency, Currency inputCurrency, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCurrency = dbCurrency.MapUpdateValues<Currency>(inputCurrency);
				base.Update(dbCurrency);
				return dbCurrency;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCurrencyVoid(Currency dbCurrency, Currency inputCurrency, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCurrency = dbCurrency.MapUpdateValues<Currency>(inputCurrency, skipUpdateFields);
				base.Update(dbCurrency);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public List<Currency> GetCurrencyByCompanyNoTracking(Guid companyGUID)
		{
			try
			{
				List<Currency> currency = Entity.Where(w => w.CompanyGUID == companyGUID).AsNoTracking().ToList();
				return currency;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

