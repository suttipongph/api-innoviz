using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICreditTypeRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CreditTypeItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<CreditTypeListView> GetListvw(SearchParameter search);
		CreditTypeItemView GetByIdvw(Guid id);
		CreditType Find(params object[] keyValues);
		CreditType GetCreditTypeByIdNoTracking(Guid guid);
		CreditType CreateCreditType(CreditType creditType);
		void CreateCreditTypeVoid(CreditType creditType);
		CreditType UpdateCreditType(CreditType dbCreditType, CreditType inputCreditType, List<string> skipUpdateFields = null);
		void UpdateCreditTypeVoid(CreditType dbCreditType, CreditType inputCreditType, List<string> skipUpdateFields = null);
		void Remove(CreditType item);
	}
	public class CreditTypeRepo : CompanyBaseRepository<CreditType>, ICreditTypeRepo
	{
		public CreditTypeRepo(SmartAppDbContext context) : base(context) { }
		public CreditTypeRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CreditType GetCreditTypeByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CreditTypeGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CreditTypeItemViewMap> GetDropDownQuery()
		{
			return (from creditType in Entity
					select new CreditTypeItemViewMap
					{
						CompanyGUID = creditType.CompanyGUID,
						Owner = creditType.Owner,
						OwnerBusinessUnitGUID = creditType.OwnerBusinessUnitGUID,
						CreditTypeGUID = creditType.CreditTypeGUID,
						CreditTypeId = creditType.CreditTypeId,
						Description = creditType.Description
					});
		}
		public IEnumerable<SelectItem<CreditTypeItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditType>(search, SysParm.CompanyGUID);
				var creditType = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CreditTypeItemViewMap, CreditTypeItemView>().ToDropDownItem(search.ExcludeRowData);


				return creditType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CreditTypeListViewMap> GetListQuery()
		{
			return (from creditType in Entity
				select new CreditTypeListViewMap
				{
						CompanyGUID = creditType.CompanyGUID,
						Owner = creditType.Owner,
						OwnerBusinessUnitGUID = creditType.OwnerBusinessUnitGUID,
						CreditTypeGUID = creditType.CreditTypeGUID,
						CreditTypeId = creditType.CreditTypeId,
						Description = creditType.Description,
				});
		}
		public SearchResult<CreditTypeListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CreditTypeListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditType>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CreditTypeListViewMap, CreditTypeListView>();
				result = list.SetSearchResult<CreditTypeListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CreditTypeItemViewMap> GetItemQuery()
		{
			return (from creditType in Entity
					join company in db.Set<Company>()
					on creditType.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on creditType.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCreditTypeOwnerBU
					from ownerBU in ljCreditTypeOwnerBU.DefaultIfEmpty()
					select new CreditTypeItemViewMap
					{
						CompanyGUID = creditType.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = creditType.Owner,
						OwnerBusinessUnitGUID = creditType.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = creditType.CreatedBy,
						CreatedDateTime = creditType.CreatedDateTime,
						ModifiedBy = creditType.ModifiedBy,
						ModifiedDateTime = creditType.ModifiedDateTime,
						CreditTypeGUID = creditType.CreditTypeGUID,
						CreditTypeId = creditType.CreditTypeId,
						Description = creditType.Description,
					
						RowVersion = creditType.RowVersion,
					});
		}
		public CreditTypeItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<CreditTypeItemViewMap, CreditTypeItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public CreditType CreateCreditType(CreditType creditType)
		{
			try
			{
				creditType.CreditTypeGUID = Guid.NewGuid();
				base.Add(creditType);
				return creditType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCreditTypeVoid(CreditType creditType)
		{
			try
			{
				CreateCreditType(creditType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditType UpdateCreditType(CreditType dbCreditType, CreditType inputCreditType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCreditType = dbCreditType.MapUpdateValues<CreditType>(inputCreditType);
				base.Update(dbCreditType);
				return dbCreditType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCreditTypeVoid(CreditType dbCreditType, CreditType inputCreditType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCreditType = dbCreditType.MapUpdateValues<CreditType>(inputCreditType, skipUpdateFields);
				base.Update(dbCreditType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

