using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICollectionFollowUpRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CollectionFollowUpItemView>> GetDropDownItem(SearchParameter search);
		IEnumerable<SelectItem<CollectionFollowUpItemView>> GetDropDownItemBy(SearchParameter search);
		#endregion DropDown
		SearchResult<CollectionFollowUpListView> GetListvw(SearchParameter search);
		CollectionFollowUpItemView GetByIdvw(Guid id);
		CollectionFollowUp Find(params object[] keyValues);
		CollectionFollowUp GetCollectionFollowUpByIdNoTracking(Guid guid);
		CollectionFollowUp CreateCollectionFollowUp(CollectionFollowUp collectionFollowUp);
		void CreateCollectionFollowUpVoid(CollectionFollowUp collectionFollowUp);
		CollectionFollowUp UpdateCollectionFollowUp(CollectionFollowUp dbCollectionFollowUp, CollectionFollowUp inputCollectionFollowUp, List<string> skipUpdateFields = null);
		void UpdateCollectionFollowUpVoid(CollectionFollowUp dbCollectionFollowUp, CollectionFollowUp inputCollectionFollowUp, List<string> skipUpdateFields = null);
		void Remove(CollectionFollowUp item);
		CollectionFollowUp GetCollectionFollowUpByIdNoTrackingByAccessLevel(Guid guid);
		CollectionFollowUpItemView GetCollectionFollowUpInitialDataByInquiryPurcaseLineOutstanding(Guid guid); 
		CollectionFollowUpItemView GetCollectionFollowUpInitialDataByInquiryWithdrawalLineOutstand(Guid guid);
	}
	public class CollectionFollowUpRepo : CompanyBaseRepository<CollectionFollowUp>, ICollectionFollowUpRepo
	{
		public CollectionFollowUpRepo(SmartAppDbContext context) : base(context) { }
		public CollectionFollowUpRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CollectionFollowUp GetCollectionFollowUpByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CollectionFollowUpGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CollectionFollowUpItemViewMap> GetDropDownQueryBy()
		{
			var result = (from collectionFollowUp in Entity
                          join methodOfPayment in db.Set<MethodOfPayment>()
                          on collectionFollowUp.MethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID into ljmethodOfPayment
                          from methodOfPayment in ljmethodOfPayment.DefaultIfEmpty()
                          select new CollectionFollowUpItemViewMap
					{
						CompanyGUID = collectionFollowUp.CompanyGUID,
						Owner = collectionFollowUp.Owner,
						OwnerBusinessUnitGUID = collectionFollowUp.OwnerBusinessUnitGUID,
						CollectionFollowUpGUID = collectionFollowUp.CollectionFollowUpGUID,
						CollectionDate = collectionFollowUp.CollectionDate,
						Description = collectionFollowUp.Description,
						ChequeBankGroupGUID = collectionFollowUp.ChequeBankGroupGUID,
						ChequeBranch = collectionFollowUp.ChequeBranch,
						ChequeDate = collectionFollowUp.ChequeDate,
						ChequeNo = collectionFollowUp.ChequeNo,
						ChequeBankAccNo = collectionFollowUp.ChequeBankAccNo,
						RecipientName = collectionFollowUp.RecipientName,
						ChequeAmount = collectionFollowUp.ChequeAmount,
						CollectionFollowUpResult = collectionFollowUp.CollectionFollowUpResult,
						RefGUID = collectionFollowUp.RefGUID,
						MethodOfPayment_PaymentType = methodOfPayment.PaymentType.ToString()
					}) ;
			return result;
		}
		public IEnumerable<SelectItem<CollectionFollowUpItemView>> GetDropDownItemBy(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CollectionFollowUp>(search, SysParm.CompanyGUID);
				var collectionFollowUp = GetDropDownQueryBy().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CollectionFollowUpItemViewMap, CollectionFollowUpItemView>().ToDropDownItem(search.ExcludeRowData);


				return collectionFollowUp;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private IQueryable<CollectionFollowUpItemViewMap> GetDropDownQuery()
		{
			return (from collectionFollowUp in Entity
					select new CollectionFollowUpItemViewMap
					{
						CompanyGUID = collectionFollowUp.CompanyGUID,
						Owner = collectionFollowUp.Owner,
						OwnerBusinessUnitGUID = collectionFollowUp.OwnerBusinessUnitGUID,
						CollectionFollowUpGUID = collectionFollowUp.CollectionFollowUpGUID,
						CollectionDate = collectionFollowUp.CollectionDate,
						Description = collectionFollowUp.Description
					});
		}
		public IEnumerable<SelectItem<CollectionFollowUpItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CollectionFollowUp>(search, SysParm.CompanyGUID);
				var collectionFollowUp = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CollectionFollowUpItemViewMap, CollectionFollowUpItemView>().ToDropDownItem(search.ExcludeRowData);


				return collectionFollowUp;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CollectionFollowUpListViewMap> GetListQuery()
		{
			return (from collectionFollowUp in Entity
					join buyerTable in db.Set<BuyerTable>() on collectionFollowUp.BuyerTableGUID equals buyerTable.BuyerTableGUID
					join customerTable in db.Set<CustomerTable>() on collectionFollowUp.CustomerTableGUID equals customerTable.CustomerTableGUID
					join methodOfPayment in db.Set<MethodOfPayment>() on collectionFollowUp.MethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID
					select new CollectionFollowUpListViewMap
				{
						CompanyGUID = collectionFollowUp.CompanyGUID,
						Owner = collectionFollowUp.Owner,
						OwnerBusinessUnitGUID = collectionFollowUp.OwnerBusinessUnitGUID,
						CollectionFollowUpGUID = collectionFollowUp.CollectionFollowUpGUID,
						CollectionDate = collectionFollowUp.CollectionDate,
						Description = collectionFollowUp.Description,
						ReceivedFrom = collectionFollowUp.ReceivedFrom,
						MethodOfPaymentGUID = collectionFollowUp.MethodOfPaymentGUID,
						CollectionAmount = collectionFollowUp.CollectionAmount,
						CollectionFollowUpResult = collectionFollowUp.CollectionFollowUpResult,
						NewCollectionDate = collectionFollowUp.NewCollectionDate,
						CustomerTableGUID = collectionFollowUp.CustomerTableGUID,
						BuyerTableGUID = collectionFollowUp.BuyerTableGUID,
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
						BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
						MethodOfPayment_Values = SmartAppUtil.GetDropDownLabel(methodOfPayment.MethodOfPaymentId, methodOfPayment.Description),
						CustomerTable_CustomerId = customerTable.CustomerId,
						BuyerTable_BuyerId = buyerTable.BuyerId,
						MethodOfPayment_MethodOfPaymentId = methodOfPayment.MethodOfPaymentId,
						RefGUID = collectionFollowUp.RefGUID
					});
		}
		public SearchResult<CollectionFollowUpListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CollectionFollowUpListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CollectionFollowUp>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CollectionFollowUpListViewMap, CollectionFollowUpListView>();
				result = list.SetSearchResult<CollectionFollowUpListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CollectionFollowUpItemViewMap> GetItemQuery()
		{
			return (from collectionFollowUp in Entity
					join company in db.Set<Company>()
					on collectionFollowUp.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on collectionFollowUp.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCollectionFollowUpOwnerBU
					from ownerBU in ljCollectionFollowUpOwnerBU.DefaultIfEmpty()
					join buyerTable in db.Set<BuyerTable>() on collectionFollowUp.BuyerTableGUID equals buyerTable.BuyerTableGUID
					join customerTable in db.Set<CustomerTable>() on collectionFollowUp.CustomerTableGUID equals customerTable.CustomerTableGUID

					join purchaseLine in db.Set<PurchaseLine>()
					on collectionFollowUp.RefGUID equals purchaseLine.PurchaseLineGUID into ljPurchaseLine
					from purchaseLine in ljPurchaseLine.DefaultIfEmpty()
					join creditAppLineByPurchaseLine in db.Set<CreditAppLine>()
					on purchaseLine.CreditAppLineGUID equals creditAppLineByPurchaseLine.CreditAppLineGUID into ljCreditAppLineByPurchaseLine
					from creditAppLineByPurchaseLine in ljCreditAppLineByPurchaseLine.DefaultIfEmpty()

					join withdrawalLine in db.Set<WithdrawalLine>()
					on collectionFollowUp.RefGUID equals withdrawalLine.WithdrawalLineGUID into ljWithdrawalLine
					from withdrawalLine in ljWithdrawalLine.DefaultIfEmpty()
					join creditAppLineByWithdrawalLine in db.Set<CreditAppLine>()
					on purchaseLine.CreditAppLineGUID equals creditAppLineByWithdrawalLine.CreditAppLineGUID into ljCreditAppLineByWithdrawalLine
					from creditAppLineByWithdrawalLine in ljCreditAppLineByWithdrawalLine.DefaultIfEmpty()
					select new CollectionFollowUpItemViewMap
					{
						CompanyGUID = collectionFollowUp.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = collectionFollowUp.Owner,
						OwnerBusinessUnitGUID = collectionFollowUp.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = collectionFollowUp.CreatedBy,
						CreatedDateTime = collectionFollowUp.CreatedDateTime,
						ModifiedBy = collectionFollowUp.ModifiedBy,
						ModifiedDateTime = collectionFollowUp.ModifiedDateTime,
						CollectionFollowUpGUID = collectionFollowUp.CollectionFollowUpGUID,
						BuyerTableGUID = collectionFollowUp.BuyerTableGUID,
						ChequeAmount = collectionFollowUp.ChequeAmount,
						ChequeBankAccNo = collectionFollowUp.ChequeBankAccNo,
						ChequeBankGroupGUID = collectionFollowUp.ChequeBankGroupGUID,
						ChequeBranch = collectionFollowUp.ChequeBranch,
						ChequeCollectionResult = collectionFollowUp.ChequeCollectionResult,
						ChequeDate = collectionFollowUp.ChequeDate,
						ChequeNo = collectionFollowUp.ChequeNo,
						CollectionAmount = collectionFollowUp.CollectionAmount,
						CollectionDate = collectionFollowUp.CollectionDate,
						CollectionFollowUpResult = collectionFollowUp.CollectionFollowUpResult,
						CreditAppLineGUID = collectionFollowUp.CreditAppLineGUID,
						CustomerTableGUID = collectionFollowUp.CustomerTableGUID,
						Description = collectionFollowUp.Description,
						DocumentId = collectionFollowUp.DocumentId,
						MethodOfPaymentGUID = collectionFollowUp.MethodOfPaymentGUID,
						NewCollectionDate = collectionFollowUp.NewCollectionDate,
						ReceivedFrom = collectionFollowUp.ReceivedFrom,
						RecipientName = collectionFollowUp.RecipientName,
						RefGUID = collectionFollowUp.RefGUID,
						RefType = collectionFollowUp.RefType,
						Remark = collectionFollowUp.Remark,
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
						BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
						RefId = (purchaseLine != null && collectionFollowUp.RefType == (int)RefType.PurchaseLine) ? purchaseLine.LineNum.ToString() :
								(withdrawalLine != null && collectionFollowUp.RefType == (int)RefType.WithdrawalLine) ? withdrawalLine.WithdrawalLineType.ToString() : null,
						CreditAppLine_Values = (creditAppLineByPurchaseLine != null && collectionFollowUp.RefType == (int)RefType.PurchaseLine) ? SmartAppUtil.GetDropDownLabel(creditAppLineByPurchaseLine.LineNum.ToString(), creditAppLineByPurchaseLine.ApprovedCreditLimitLine.ToString()) :
											   (creditAppLineByWithdrawalLine != null && collectionFollowUp.RefType == (int)RefType.WithdrawalLine) ? SmartAppUtil.GetDropDownLabel(creditAppLineByWithdrawalLine.LineNum.ToString(), creditAppLineByWithdrawalLine.ApprovedCreditLimitLine.ToString()) : null,
					
						RowVersion = collectionFollowUp.RowVersion,
					});
		}
		public CollectionFollowUpItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<CollectionFollowUpItemViewMap, CollectionFollowUpItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public CollectionFollowUp CreateCollectionFollowUp(CollectionFollowUp collectionFollowUp)
		{
			try
			{
				collectionFollowUp.CollectionFollowUpGUID = Guid.NewGuid();
				base.Add(collectionFollowUp);
				return collectionFollowUp;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCollectionFollowUpVoid(CollectionFollowUp collectionFollowUp)
		{
			try
			{
				CreateCollectionFollowUp(collectionFollowUp);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CollectionFollowUp UpdateCollectionFollowUp(CollectionFollowUp dbCollectionFollowUp, CollectionFollowUp inputCollectionFollowUp, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCollectionFollowUp = dbCollectionFollowUp.MapUpdateValues<CollectionFollowUp>(inputCollectionFollowUp);
				base.Update(dbCollectionFollowUp);
				return dbCollectionFollowUp;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCollectionFollowUpVoid(CollectionFollowUp dbCollectionFollowUp, CollectionFollowUp inputCollectionFollowUp, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCollectionFollowUp = dbCollectionFollowUp.MapUpdateValues<CollectionFollowUp>(inputCollectionFollowUp, skipUpdateFields);
				base.Update(dbCollectionFollowUp);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public CollectionFollowUp GetCollectionFollowUpByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CollectionFollowUpGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CollectionFollowUpItemView GetCollectionFollowUpInitialDataByInquiryPurcaseLineOutstanding(Guid guid)
		{
			return (from purchaseLine in db.Set<PurchaseLine>().Where(w => w.PurchaseLineGUID == guid)
					join purchaseTable in db.Set<PurchaseTable>()
					on purchaseLine.PurchaseTableGUID equals purchaseTable.PurchaseTableGUID
					join customerTable in db.Set<CustomerTable>()
					on purchaseTable.CustomerTableGUID equals customerTable.CustomerTableGUID
					join buyerTable in db.Set<BuyerTable>()
					on purchaseLine.BuyerTableGUID equals buyerTable.BuyerTableGUID
					join invoiceTable in db.Set<InvoiceTable>()
					on purchaseLine.PurchaseLineInvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljPurchaseLineInvoiceTable
					from invoiceTable in ljPurchaseLineInvoiceTable.DefaultIfEmpty()
					join custTrans in db.Set<CustTrans>()
					on invoiceTable.InvoiceTableGUID equals custTrans.InvoiceTableGUID into ljInvoiceTableCusttrans
					from custTrans in ljInvoiceTableCusttrans.DefaultIfEmpty()
					join creditAppLine in db.Set<CreditAppLine>()
					on purchaseLine.CreditAppLineGUID equals creditAppLine.CreditAppLineGUID
					select new CollectionFollowUpItemView
					{
						CollectionFollowUpGUID = new Guid().GuidNullToString(),
						RefGUID = guid.ToString(),
						RefType = (int)RefType.PurchaseLine,
						RefId = purchaseLine.LineNum.ToString(),
						CustomerTableGUID = purchaseTable.CustomerTableGUID.GuidNullToString(),
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
						BuyerTableGUID = purchaseLine.BuyerTableGUID.GuidNullToString(),
						BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
						CollectionDate = purchaseLine.CollectionDate.DateToString(),
						CreditAppLineGUID = purchaseLine.CreditAppLineGUID.GuidNullToString(),
						CreditAppLine_Values = SmartAppUtil.GetDropDownLabel(creditAppLine.LineNum.ToString(), creditAppLine.ApprovedCreditLimitLine.ToString()),
						DocumentId = purchaseTable.PurchaseId,
						MethodOfPaymentGUID = purchaseLine.MethodOfPaymentGUID.GuidNullToString(),
						CollectionAmount = custTrans != null ? custTrans.TransAmount - custTrans.SettleAmount : 0,
						CollectionFollowUpResult = (int)CollectionFollowUpResult.None,
						ChequeCollectionResult = (int)Result.None
					}).FirstOrDefault();
		}
		public CollectionFollowUpItemView GetCollectionFollowUpInitialDataByInquiryWithdrawalLineOutstand(Guid guid)
		{
			return (from withdrawalLine in db.Set<WithdrawalLine>().Where(w => w.WithdrawalLineGUID == guid)
					join withdrawalTable in db.Set<WithdrawalTable>()
					on withdrawalLine.WithdrawalTableGUID equals withdrawalTable.WithdrawalTableGUID
					join customerTable in db.Set<CustomerTable>()
					on withdrawalTable.CustomerTableGUID equals customerTable.CustomerTableGUID
					join buyerTable in db.Set<BuyerTable>()
					on withdrawalTable.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljWithdrawalLineBuyerTable
					from buyerTable in ljWithdrawalLineBuyerTable.DefaultIfEmpty()
					join invoiceTable in db.Set<InvoiceTable>()
					on withdrawalLine.WithdrawalLineInvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljWithdrawalLineInvoiceTable
					from invoiceTable in ljWithdrawalLineInvoiceTable.DefaultIfEmpty()
					join custTrans in db.Set<CustTrans>()
					on invoiceTable.InvoiceTableGUID equals custTrans.InvoiceTableGUID into ljInvoiceTableCusttrans
					from custTrans in ljInvoiceTableCusttrans.DefaultIfEmpty()
					join creditAppLine in db.Set<CreditAppLine>()
					on withdrawalTable.CreditAppLineGUID equals creditAppLine.CreditAppLineGUID
					select new CollectionFollowUpItemView
					{
						CollectionFollowUpGUID = new Guid().GuidNullToString(),
						RefGUID = guid.ToString(),
						RefType = (int)RefType.WithdrawalLine,
						RefId = withdrawalLine.WithdrawalLineType.ToString(),
						CustomerTableGUID = withdrawalTable.CustomerTableGUID.GuidNullToString(),
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
						BuyerTableGUID = withdrawalTable.BuyerTableGUID.GuidNullToString(),
						BuyerTable_Values = (buyerTable != null ) ? SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName) : null,
						CollectionDate = withdrawalLine.CollectionDate.DateToString(),
						CreditAppLineGUID = withdrawalTable.CreditAppLineGUID.GuidNullToString(),
						CreditAppLine_Values = SmartAppUtil.GetDropDownLabel(creditAppLine.LineNum.ToString(), creditAppLine.ApprovedCreditLimitLine.ToString()),
						DocumentId = withdrawalTable.WithdrawalId,
						MethodOfPaymentGUID = withdrawalTable.MethodOfPaymentGUID.GuidNullToString(),
						CollectionAmount = custTrans != null ? custTrans.TransAmount - custTrans.SettleAmount : 0,
						CollectionFollowUpResult = (int)CollectionFollowUpResult.None,
						ChequeCollectionResult = (int)Result.None
					}).FirstOrDefault();
		}
	}
}

