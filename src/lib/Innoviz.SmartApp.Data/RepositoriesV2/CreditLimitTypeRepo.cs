using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICreditLimitTypeRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CreditLimitTypeItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<CreditLimitTypeListView> GetListvw(SearchParameter search);
		CreditLimitTypeItemView GetByIdvw(Guid id);
		CreditLimitType Find(params object[] keyValues);
		CreditLimitType GetCreditLimitTypeByIdNoTracking(Guid guid);
		CreditLimitType CreateCreditLimitType(CreditLimitType creditLimitType);
		void CreateCreditLimitTypeVoid(CreditLimitType creditLimitType);
		CreditLimitType UpdateCreditLimitType(CreditLimitType dbCreditLimitType, CreditLimitType inputCreditLimitType, List<string> skipUpdateFields = null);
		void UpdateCreditLimitTypeVoid(CreditLimitType dbCreditLimitType, CreditLimitType inputCreditLimitType, List<string> skipUpdateFields = null);
		void Remove(CreditLimitType item);
		CreditLimitType GetCreditLimitTypeByCreditAppRequestTable(Guid CreditAppRequestTableId);
		IEnumerable<CreditLimitType> GetCreditLimitTypeByCompanyNoTracking(Guid guid);
		IEnumerable<CreditLimitType> GetCreditLimitTypeByCreditLImitConditionTypeNoTracking(Guid companyGUID, int creditLimitConditionType);
	}
	public class CreditLimitTypeRepo : CompanyBaseRepository<CreditLimitType>, ICreditLimitTypeRepo
	{
		public CreditLimitTypeRepo(SmartAppDbContext context) : base(context) { }
		public CreditLimitTypeRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CreditLimitType GetCreditLimitTypeByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CreditLimitTypeGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<CreditLimitType> GetCreditLimitTypeByCompanyNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CompanyGUID == guid).AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<CreditLimitType> GetCreditLimitTypeByCreditLImitConditionTypeNoTracking(Guid companyGUID,int creditLimitConditionType)
		{
			try
			{
				var result = Entity.Where(item => item.CompanyGUID == companyGUID && item.CreditLimitConditionType == creditLimitConditionType).AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CreditLimitTypeItemViewMap> GetDropDownQuery()
		{
			return (from creditLimitType in Entity
					select new CreditLimitTypeItemViewMap
					{
						CompanyGUID = creditLimitType.CompanyGUID,
						Owner = creditLimitType.Owner,
						OwnerBusinessUnitGUID = creditLimitType.OwnerBusinessUnitGUID,
						CreditLimitTypeGUID = creditLimitType.CreditLimitTypeGUID,
						CreditLimitTypeId = creditLimitType.CreditLimitTypeId,
						Description = creditLimitType.Description,
						ProductType = creditLimitType.ProductType,
						CreditLimitExpiration = creditLimitType.CreditLimitExpiration,
						CreditLimitRemark = creditLimitType.CreditLimitRemark,
						ParentCreditLimitTypeGUID = creditLimitType.ParentCreditLimitTypeGUID,
					});
		}
		public IEnumerable<SelectItem<CreditLimitTypeItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditLimitType>(search, SysParm.CompanyGUID);
				var creditLimitType = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CreditLimitTypeItemViewMap, CreditLimitTypeItemView>().ToDropDownItem(search.ExcludeRowData);


				return creditLimitType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CreditLimitTypeListViewMap> GetListQuery()
		{
			return (from creditLimitType in Entity
				select new CreditLimitTypeListViewMap
				{
						CompanyGUID = creditLimitType.CompanyGUID,
						Owner = creditLimitType.Owner,
						OwnerBusinessUnitGUID = creditLimitType.OwnerBusinessUnitGUID,
						CreditLimitTypeGUID = creditLimitType.CreditLimitTypeGUID,
						CreditLimitTypeId = creditLimitType.CreditLimitTypeId,
						Description = creditLimitType.Description,
						ProductType = creditLimitType.ProductType,
						Revolving = creditLimitType.Revolving,
						CreditLimitExpiration = creditLimitType.CreditLimitExpiration,
						CreditLimitExpiryDay = creditLimitType.CreditLimitExpiryDay,
						InactiveDay = creditLimitType.InactiveDay,
						CloseDay = creditLimitType.CloseDay,
						BookmarkOrdering = creditLimitType.BookmarkOrdering,	
				});
		}
		public SearchResult<CreditLimitTypeListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CreditLimitTypeListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditLimitType>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CreditLimitTypeListViewMap, CreditLimitTypeListView>();
				result = list.SetSearchResult<CreditLimitTypeListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CreditLimitTypeItemViewMap> GetItemQuery()
		{
			return (from creditLimitType in Entity
					join company in db.Set<Company>()
					on creditLimitType.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on creditLimitType.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCreditLimitTypeOwnerBU
					from ownerBU in ljCreditLimitTypeOwnerBU.DefaultIfEmpty()
					select new CreditLimitTypeItemViewMap
					{
						CompanyGUID = creditLimitType.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = creditLimitType.Owner,
						OwnerBusinessUnitGUID = creditLimitType.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = creditLimitType.CreatedBy,
						CreatedDateTime = creditLimitType.CreatedDateTime,
						ModifiedBy = creditLimitType.ModifiedBy,
						ModifiedDateTime = creditLimitType.ModifiedDateTime,
						CreditLimitTypeGUID = creditLimitType.CreditLimitTypeGUID,
						AllowAddLine = creditLimitType.AllowAddLine,
						BuyerMatchingAndLoanRequest = creditLimitType.BuyerMatchingAndLoanRequest,
						CloseDay = creditLimitType.CloseDay,
						CreditLimitConditionType = creditLimitType.CreditLimitConditionType,
						CreditLimitExpiration = creditLimitType.CreditLimitExpiration,
						CreditLimitExpiryDay = creditLimitType.CreditLimitExpiryDay,
						CreditLimitRemark = creditLimitType.CreditLimitRemark,
						CreditLimitTypeId = creditLimitType.CreditLimitTypeId,
						Description = creditLimitType.Description,
						InactiveDay = creditLimitType.InactiveDay,
						ParentCreditLimitTypeGUID = creditLimitType.ParentCreditLimitTypeGUID,
						ProductType = creditLimitType.ProductType,
						Revolving = creditLimitType.Revolving,
						ValidateBuyerAgreement = creditLimitType.ValidateBuyerAgreement,
						ValidateCreditLimitType = creditLimitType.ValidateCreditLimitType,
						BookmarkOrdering = creditLimitType.BookmarkOrdering,
					
						RowVersion = creditLimitType.RowVersion,
					});
		}
		public CreditLimitTypeItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<CreditLimitTypeItemViewMap, CreditLimitTypeItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public CreditLimitType CreateCreditLimitType(CreditLimitType creditLimitType)
		{
			try
			{
				creditLimitType.CreditLimitTypeGUID = Guid.NewGuid();
				base.Add(creditLimitType);
				return creditLimitType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCreditLimitTypeVoid(CreditLimitType creditLimitType)
		{
			try
			{
				CreateCreditLimitType(creditLimitType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditLimitType UpdateCreditLimitType(CreditLimitType dbCreditLimitType, CreditLimitType inputCreditLimitType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCreditLimitType = dbCreditLimitType.MapUpdateValues<CreditLimitType>(inputCreditLimitType);
				base.Update(dbCreditLimitType);
				return dbCreditLimitType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCreditLimitTypeVoid(CreditLimitType dbCreditLimitType, CreditLimitType inputCreditLimitType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCreditLimitType = dbCreditLimitType.MapUpdateValues<CreditLimitType>(inputCreditLimitType, skipUpdateFields);
				base.Update(dbCreditLimitType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public CreditLimitType GetCreditLimitTypeByCreditAppRequestTable(Guid CreditAppRequestTableId)
		{

			try
			{
				CreditLimitType creditLimitType = (from _creditLimitType in db.Set<CreditLimitType>()
												   join creditAppRequestTable in db.Set<CreditAppRequestTable>() on _creditLimitType.CreditLimitTypeGUID equals creditAppRequestTable.CreditLimitTypeGUID
												   where creditAppRequestTable.CreditAppRequestTableGUID == CreditAppRequestTableId
												   select _creditLimitType).AsNoTracking().FirstOrDefault();
				return creditLimitType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

