using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ILedgerDimensionRepo
	{
		#region DropDown
		IEnumerable<SelectItem<LedgerDimensionItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<LedgerDimensionListView> GetListvw(SearchParameter search);
		LedgerDimensionItemView GetByIdvw(Guid id);
		LedgerDimension Find(params object[] keyValues);
		LedgerDimension GetLedgerDimensionByIdNoTracking(Guid guid);
		LedgerDimension CreateLedgerDimension(LedgerDimension ledgerDimension);
		void CreateLedgerDimensionVoid(LedgerDimension ledgerDimension);
		LedgerDimension UpdateLedgerDimension(LedgerDimension dbLedgerDimension, LedgerDimension inputLedgerDimension, List<string> skipUpdateFields = null);
		void UpdateLedgerDimensionVoid(LedgerDimension dbLedgerDimension, LedgerDimension inputLedgerDimension, List<string> skipUpdateFields = null);
		void Remove(LedgerDimension item);
		List<LedgerDimension> GetLedgerDimensionByCompanyNoTracking(Guid companyGUID);
	}
	public class LedgerDimensionRepo : CompanyBaseRepository<LedgerDimension>, ILedgerDimensionRepo
	{
		public LedgerDimensionRepo(SmartAppDbContext context) : base(context) { }
		public LedgerDimensionRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public LedgerDimension GetLedgerDimensionByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.LedgerDimensionGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<LedgerDimensionItemViewMap> GetDropDownQuery()
		{
			return (from ledgerDimension in Entity
					select new LedgerDimensionItemViewMap
					{
						CompanyGUID = ledgerDimension.CompanyGUID,
						Owner = ledgerDimension.Owner,
						OwnerBusinessUnitGUID = ledgerDimension.OwnerBusinessUnitGUID,
						LedgerDimensionGUID = ledgerDimension.LedgerDimensionGUID,
						DimensionCode = ledgerDimension.DimensionCode,
						Description = ledgerDimension.Description,
						Dimension = ledgerDimension.Dimension
					});
		}
		public IEnumerable<SelectItem<LedgerDimensionItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<LedgerDimension>(search, SysParm.CompanyGUID);
				var ledgerDimension = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<LedgerDimensionItemViewMap, LedgerDimensionItemView>().ToDropDownItem(search.ExcludeRowData);


				return ledgerDimension;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<LedgerDimensionListViewMap> GetListQuery()
		{
			return (from ledgerDimension in Entity
				select new LedgerDimensionListViewMap
				{
						CompanyGUID = ledgerDimension.CompanyGUID,
						Owner = ledgerDimension.Owner,
						OwnerBusinessUnitGUID = ledgerDimension.OwnerBusinessUnitGUID,
						LedgerDimensionGUID = ledgerDimension.LedgerDimensionGUID,
						DimensionCode = ledgerDimension.DimensionCode,
						Dimension = ledgerDimension.Dimension,
						Description = ledgerDimension.Description
						
				});
		}
		public SearchResult<LedgerDimensionListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<LedgerDimensionListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<LedgerDimension>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<LedgerDimensionListViewMap, LedgerDimensionListView>();
				result = list.SetSearchResult<LedgerDimensionListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<LedgerDimensionItemViewMap> GetItemQuery()
		{
			return (from ledgerDimension in Entity
					join company in db.Set<Company>()
					on ledgerDimension.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on ledgerDimension.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljLedgerDimensionOwnerBU
					from ownerBU in ljLedgerDimensionOwnerBU.DefaultIfEmpty()
					select new LedgerDimensionItemViewMap
					{
						CompanyGUID = ledgerDimension.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = ledgerDimension.Owner,
						OwnerBusinessUnitGUID = ledgerDimension.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = ledgerDimension.CreatedBy,
						CreatedDateTime = ledgerDimension.CreatedDateTime,
						ModifiedBy = ledgerDimension.ModifiedBy,
						ModifiedDateTime = ledgerDimension.ModifiedDateTime,
						LedgerDimensionGUID = ledgerDimension.LedgerDimensionGUID,
						Description = ledgerDimension.Description,
						Dimension = ledgerDimension.Dimension,
						DimensionCode = ledgerDimension.DimensionCode,
					
						RowVersion = ledgerDimension.RowVersion,
					});
		}
		public LedgerDimensionItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<LedgerDimensionItemViewMap, LedgerDimensionItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public LedgerDimension CreateLedgerDimension(LedgerDimension ledgerDimension)
		{
			try
			{
				ledgerDimension.LedgerDimensionGUID = Guid.NewGuid();
				base.Add(ledgerDimension);
				return ledgerDimension;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateLedgerDimensionVoid(LedgerDimension ledgerDimension)
		{
			try
			{
				CreateLedgerDimension(ledgerDimension);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public LedgerDimension UpdateLedgerDimension(LedgerDimension dbLedgerDimension, LedgerDimension inputLedgerDimension, List<string> skipUpdateFields = null)
		{
			try
			{
				dbLedgerDimension = dbLedgerDimension.MapUpdateValues<LedgerDimension>(inputLedgerDimension);
				base.Update(dbLedgerDimension);
				return dbLedgerDimension;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateLedgerDimensionVoid(LedgerDimension dbLedgerDimension, LedgerDimension inputLedgerDimension, List<string> skipUpdateFields = null)
		{
			try
			{
				dbLedgerDimension = dbLedgerDimension.MapUpdateValues<LedgerDimension>(inputLedgerDimension, skipUpdateFields);
				base.Update(dbLedgerDimension);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete

		public List<LedgerDimension> GetLedgerDimensionByCompanyNoTracking(Guid companyGUID)
        {
            try
            {
				List<LedgerDimension> ledgerDimensions = Entity.Where(w => w.CompanyGUID == companyGUID).AsNoTracking().ToList();
				return ledgerDimensions;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

