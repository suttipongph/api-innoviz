using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICreditAppReqAssignmentRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CreditAppReqAssignmentItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<CreditAppReqAssignmentListView> GetListvw(SearchParameter search);
		CreditAppReqAssignmentItemView GetByIdvw(Guid id);
		CreditAppReqAssignment Find(params object[] keyValues);
		CreditAppReqAssignment GetCreditAppReqAssignmentByIdNoTracking(Guid guid);
		CreditAppReqAssignment CreateCreditAppReqAssignment(CreditAppReqAssignment creditAppReqAssignment);
		void CreateCreditAppReqAssignmentVoid(CreditAppReqAssignment creditAppReqAssignment);
		CreditAppReqAssignment UpdateCreditAppReqAssignment(CreditAppReqAssignment dbCreditAppReqAssignment, CreditAppReqAssignment inputCreditAppReqAssignment, List<string> skipUpdateFields = null);
		void UpdateCreditAppReqAssignmentVoid(CreditAppReqAssignment dbCreditAppReqAssignment, CreditAppReqAssignment inputCreditAppReqAssignment, List<string> skipUpdateFields = null);
		void Remove(CreditAppReqAssignment item);
		CreditAppReqAssignment GetCreditAppReqAssignmentByIdNoTrackingByAccessLevel(Guid guid);
		List<CreditAppReqAssignment> GetCreditAppReqAssignmentByCreditAppReqNoTracking(Guid creditAppRequestTableGUID);
	}
	public class CreditAppReqAssignmentRepo : CompanyBaseRepository<CreditAppReqAssignment>, ICreditAppReqAssignmentRepo
	{
		public CreditAppReqAssignmentRepo(SmartAppDbContext context) : base(context) { }
		public CreditAppReqAssignmentRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CreditAppReqAssignment GetCreditAppReqAssignmentByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CreditAppReqAssignmentGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CreditAppReqAssignmentItemViewMap> GetDropDownQuery()
		{
			return (from creditAppReqAssignment in Entity
					select new CreditAppReqAssignmentItemViewMap
					{
						CompanyGUID = creditAppReqAssignment.CompanyGUID,
						Owner = creditAppReqAssignment.Owner,
						OwnerBusinessUnitGUID = creditAppReqAssignment.OwnerBusinessUnitGUID,
						CreditAppReqAssignmentGUID = creditAppReqAssignment.CreditAppReqAssignmentGUID,
						CreditAppRequestTableGUID = creditAppReqAssignment.CreditAppRequestTableGUID,
						IsNew = creditAppReqAssignment.IsNew,
						Description = creditAppReqAssignment.Description,
						ReferenceAgreementId = creditAppReqAssignment.ReferenceAgreementId,
						AssignmentMethodGUID = creditAppReqAssignment.AssignmentMethodGUID,
						AssignmentAgreementAmount = creditAppReqAssignment.AssignmentAgreementAmount,
						BuyerTableGUID = creditAppReqAssignment.BuyerTableGUID
					});
		}
		public IEnumerable<SelectItem<CreditAppReqAssignmentItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditAppReqAssignment>(search, SysParm.CompanyGUID);
				var creditAppReqAssignment = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CreditAppReqAssignmentItemViewMap, CreditAppReqAssignmentItemView>().ToDropDownItem(search.ExcludeRowData);


				return creditAppReqAssignment;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CreditAppReqAssignmentListViewMap> GetListQuery()
		{
			return (from creditAppReqAssignment in Entity
					join assignmentAgreementTable in db.Set<AssignmentAgreementTable>()
					on creditAppReqAssignment.AssignmentAgreementTableGUID equals assignmentAgreementTable.AssignmentAgreementTableGUID
					into ljAssignmentAgreementTable 
					from assignmentAgreementTable in ljAssignmentAgreementTable.DefaultIfEmpty()
					join buyerTable in db.Set<BuyerTable>() on creditAppReqAssignment.BuyerTableGUID equals buyerTable.BuyerTableGUID
					join assignmentMethod in db.Set<AssignmentMethod>() on creditAppReqAssignment.AssignmentMethodGUID equals assignmentMethod.AssignmentMethodGUID
					join buyerAgreementTable in db.Set<BuyerAgreementTable>()
					on creditAppReqAssignment.BuyerAgreementTableGUID equals buyerAgreementTable.BuyerAgreementTableGUID into ljBuyerAgreementTable
					from buyerAgreementTable in ljBuyerAgreementTable.DefaultIfEmpty()
					select new CreditAppReqAssignmentListViewMap
					{
                        #region data
                        CompanyGUID = creditAppReqAssignment.CompanyGUID,
						Owner = creditAppReqAssignment.Owner,
						OwnerBusinessUnitGUID = creditAppReqAssignment.OwnerBusinessUnitGUID,
						CreditAppReqAssignmentGUID = creditAppReqAssignment.CreditAppReqAssignmentGUID,
						IsNew = creditAppReqAssignment.IsNew,
						AssignmentAgreementTableGUID = creditAppReqAssignment.AssignmentAgreementTableGUID,
						BuyerTableGUID = creditAppReqAssignment.BuyerTableGUID,
						AssignmentAgreementAmount = creditAppReqAssignment.AssignmentAgreementAmount,
						AssignmentMethodGUID = creditAppReqAssignment.AssignmentMethodGUID,
						BuyerAgreementTableGUID = creditAppReqAssignment.BuyerAgreementTableGUID,
						BuyerAgreementAmount = creditAppReqAssignment.BuyerAgreementAmount,
						RemainingAmount = creditAppReqAssignment.RemainingAmount,
						CreditAppRequestTableGUID = creditAppReqAssignment.CreditAppRequestTableGUID,
						CreatedDateTime = creditAppReqAssignment.CreatedDateTime,
						#endregion data
						#region for lookup
						#region display
						AssignmentAgreementTable_Values = assignmentAgreementTable != null 
						? SmartAppUtil.GetDropDownLabel(assignmentAgreementTable.InternalAssignmentAgreementId, assignmentAgreementTable.Description)
						: string.Empty,
						BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
						AssignmentMethod_Values = SmartAppUtil.GetDropDownLabel(assignmentMethod.AssignmentMethodId, assignmentMethod.Description),
						BuyerAgreementTable_Values = buyerAgreementTable != null 
						? SmartAppUtil.GetDropDownLabel(buyerAgreementTable.BuyerAgreementId, buyerAgreementTable.Description): string.Empty,
                        #endregion display
                        #region sorting
						AssignmentAgreementTable_InternalAssignmentAgreementId = assignmentAgreementTable != null 
						? assignmentAgreementTable.InternalAssignmentAgreementId : string.Empty,
						BuyerTable_BuyerId = buyerTable.BuyerId,
						BuyerAgreementTable_BuyerAgreementId = buyerAgreementTable != null ? buyerAgreementTable.BuyerAgreementId : string.Empty,
						AssignmentMethod_AssignmentMethodId = assignmentMethod.AssignmentMethodId,
						#endregion sorting
						#endregion for lookup
					});
		}
		public SearchResult<CreditAppReqAssignmentListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CreditAppReqAssignmentListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditAppReqAssignment>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CreditAppReqAssignmentListViewMap, CreditAppReqAssignmentListView>();
				result = list.SetSearchResult<CreditAppReqAssignmentListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CreditAppReqAssignmentItemViewMap> GetItemQuery()
		{
			return (from creditAppReqAssignment in Entity
					join company in db.Set<Company>()
					on creditAppReqAssignment.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on creditAppReqAssignment.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCreditAppReqAssignmentOwnerBU
					from ownerBU in ljCreditAppReqAssignmentOwnerBU.DefaultIfEmpty()
					join customerTable in db.Set<CustomerTable>()
					on creditAppReqAssignment.CustomerTableGUID equals customerTable.CustomerTableGUID
					join creditAppRequestTable in db.Set<CreditAppRequestTable>() 
					on creditAppReqAssignment.CreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID
					select new CreditAppReqAssignmentItemViewMap
					{
						CompanyGUID = creditAppReqAssignment.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = creditAppReqAssignment.Owner,
						OwnerBusinessUnitGUID = creditAppReqAssignment.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = creditAppReqAssignment.CreatedBy,
						CreatedDateTime = creditAppReqAssignment.CreatedDateTime,
						ModifiedBy = creditAppReqAssignment.ModifiedBy,
						ModifiedDateTime = creditAppReqAssignment.ModifiedDateTime,
						CreditAppReqAssignmentGUID = creditAppReqAssignment.CreditAppReqAssignmentGUID,
						AssignmentAgreementAmount = creditAppReqAssignment.AssignmentAgreementAmount,
						AssignmentAgreementTableGUID = creditAppReqAssignment.AssignmentAgreementTableGUID,
						AssignmentMethodGUID = creditAppReqAssignment.AssignmentMethodGUID,
						BuyerAgreementAmount = creditAppReqAssignment.BuyerAgreementAmount,
						BuyerAgreementTableGUID = creditAppReqAssignment.BuyerAgreementTableGUID,
						BuyerTableGUID = creditAppReqAssignment.BuyerTableGUID,
						CreditAppRequestTableGUID = creditAppReqAssignment.CreditAppRequestTableGUID,
						CustomerTableGUID = creditAppReqAssignment.CustomerTableGUID,
						Description = creditAppReqAssignment.Description,
						IsNew = creditAppReqAssignment.IsNew,
						ReferenceAgreementId = creditAppReqAssignment.ReferenceAgreementId,
						RemainingAmount = creditAppReqAssignment.RemainingAmount,
						CreditAppRequestTable_Values = SmartAppUtil.GetDropDownLabel(creditAppRequestTable.CreditAppRequestId, creditAppRequestTable.Description),
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
					
						RowVersion = creditAppReqAssignment.RowVersion,
					});
		}
		public CreditAppReqAssignmentItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<CreditAppReqAssignmentItemViewMap, CreditAppReqAssignmentItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public CreditAppReqAssignment CreateCreditAppReqAssignment(CreditAppReqAssignment creditAppReqAssignment)
		{
			try
			{
				creditAppReqAssignment.CreditAppReqAssignmentGUID = Guid.NewGuid();
				base.Add(creditAppReqAssignment);
				return creditAppReqAssignment;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCreditAppReqAssignmentVoid(CreditAppReqAssignment creditAppReqAssignment)
		{
			try
			{
				CreateCreditAppReqAssignment(creditAppReqAssignment);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditAppReqAssignment UpdateCreditAppReqAssignment(CreditAppReqAssignment dbCreditAppReqAssignment, CreditAppReqAssignment inputCreditAppReqAssignment, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCreditAppReqAssignment = dbCreditAppReqAssignment.MapUpdateValues<CreditAppReqAssignment>(inputCreditAppReqAssignment);
				base.Update(dbCreditAppReqAssignment);
				return dbCreditAppReqAssignment;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCreditAppReqAssignmentVoid(CreditAppReqAssignment dbCreditAppReqAssignment, CreditAppReqAssignment inputCreditAppReqAssignment, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCreditAppReqAssignment = dbCreditAppReqAssignment.MapUpdateValues<CreditAppReqAssignment>(inputCreditAppReqAssignment, skipUpdateFields);
				base.Update(dbCreditAppReqAssignment);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public CreditAppReqAssignment GetCreditAppReqAssignmentByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CreditAppReqAssignmentGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #region Validate
        public override void ValidateAdd(CreditAppReqAssignment item)
        {
            base.ValidateAdd(item);
			ValidateCreateAndUpdate(item);
		}
        public override void ValidateUpdate(CreditAppReqAssignment item)
        {
            base.ValidateUpdate(item);
			ValidateCreateAndUpdate(item);
		}
		public void ValidateCreateAndUpdate(CreditAppReqAssignment item)
        {
            try
            {
				SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
				if (item.AssignmentAgreementTableGUID.HasValue)
				{
					bool hasDuplicate = Entity.Any(a => a.AssignmentAgreementTableGUID == item.AssignmentAgreementTableGUID
													 && a.CreditAppRequestTableGUID == item.CreditAppRequestTableGUID
													 && a.CreditAppReqAssignmentGUID != item.CreditAppReqAssignmentGUID);
					if (hasDuplicate)
					{
						smartAppException.AddData("ERROR.DUPLICATE", new string[] { "LABEL.ASSIGNMENT_AGREEMENT_ID", "LABEL.CREDIT_APPLICATION_REQUEST_ID" });
					}
				}
				if (item.AssignmentAgreementAmount == 0)
				{
					smartAppException.AddData("ERROR.GREATER_THAN_ZERO", new string[] { "LABEL.ASSIGNMENT_AGREEMENT_AMOUNT" });
				}
				if (smartAppException.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(smartAppException);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Validate

		public List<CreditAppReqAssignment> GetCreditAppReqAssignmentByCreditAppReqNoTracking(Guid creditAppRequestTableGUID)
		{
			try
			{
				var result = Entity.Where(item => item.CreditAppRequestTableGUID == creditAppRequestTableGUID).AsNoTracking().ToList();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

