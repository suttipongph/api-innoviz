using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICreditAppReqBusinessCollateralRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CreditAppReqBusinessCollateralItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<CreditAppReqBusinessCollateralListView> GetListvw(SearchParameter search);
		CreditAppReqBusinessCollateralItemView GetByIdvw(Guid id);
		CreditAppReqBusinessCollateral Find(params object[] keyValues);
		CreditAppReqBusinessCollateral GetCreditAppReqBusinessCollateralByIdNoTracking(Guid guid);
		CreditAppReqBusinessCollateral CreateCreditAppReqBusinessCollateral(CreditAppReqBusinessCollateral creditAppReqBusinessCollateral);
		void CreateCreditAppReqBusinessCollateralVoid(CreditAppReqBusinessCollateral creditAppReqBusinessCollateral);
		CreditAppReqBusinessCollateral UpdateCreditAppReqBusinessCollateral(CreditAppReqBusinessCollateral dbCreditAppReqBusinessCollateral, CreditAppReqBusinessCollateral inputCreditAppReqBusinessCollateral, List<string> skipUpdateFields = null);
		void UpdateCreditAppReqBusinessCollateralVoid(CreditAppReqBusinessCollateral dbCreditAppReqBusinessCollateral, CreditAppReqBusinessCollateral inputCreditAppReqBusinessCollateral, List<string> skipUpdateFields = null);
		void Remove(CreditAppReqBusinessCollateral item);
		CreditAppReqBusinessCollateral GetCreditAppReqBusinessCollateralByIdNoTrackingByAccessLevel(Guid guid);
		void ValidateAdd(CreditAppReqBusinessCollateral item);
		void ValidateAdd(IEnumerable<CreditAppReqBusinessCollateral> items);
		List<CreditAppReqBusinessCollateral> GetCreditAppReqBusinessCollateralByCreditAppReqNoTracking(Guid creditAppRequestTableGUID);
	}
	public class CreditAppReqBusinessCollateralRepo : CompanyBaseRepository<CreditAppReqBusinessCollateral>, ICreditAppReqBusinessCollateralRepo
	{
		public CreditAppReqBusinessCollateralRepo(SmartAppDbContext context) : base(context) { }
		public CreditAppReqBusinessCollateralRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CreditAppReqBusinessCollateral GetCreditAppReqBusinessCollateralByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CreditAppReqBusinessCollateralGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CreditAppReqBusinessCollateralItemViewMap> GetDropDownQuery()
		{
			return (from creditAppReqBusinessCollateral in Entity
					select new CreditAppReqBusinessCollateralItemViewMap
					{
						CompanyGUID = creditAppReqBusinessCollateral.CompanyGUID,
						Owner = creditAppReqBusinessCollateral.Owner,
						OwnerBusinessUnitGUID = creditAppReqBusinessCollateral.OwnerBusinessUnitGUID,
						CreditAppReqBusinessCollateralGUID = creditAppReqBusinessCollateral.CreditAppReqBusinessCollateralGUID,
						CreditAppRequestTableGUID = creditAppReqBusinessCollateral.CreditAppRequestTableGUID,
						IsNew = creditAppReqBusinessCollateral.IsNew,
						Description = creditAppReqBusinessCollateral.Description,
						BusinessCollateralTypeGUID = creditAppReqBusinessCollateral.BusinessCollateralTypeGUID,
						BusinessCollateralSubTypeGUID = creditAppReqBusinessCollateral.BusinessCollateralSubTypeGUID,
						BuyerTableGUID = creditAppReqBusinessCollateral.BuyerTableGUID,
						BuyerName = creditAppReqBusinessCollateral.BuyerName,
						BuyerTaxIdentificationId = creditAppReqBusinessCollateral.BuyerTaxIdentificationId,
						BankGroupGUID = creditAppReqBusinessCollateral.BankGroupGUID,
						BankTypeGUID = creditAppReqBusinessCollateral.BankTypeGUID,
						AccountNumber = creditAppReqBusinessCollateral.AccountNumber,
						RefAgreementId = creditAppReqBusinessCollateral.RefAgreementId,
						RefAgreementDate = creditAppReqBusinessCollateral.RefAgreementDate,
						PreferentialCreditorNumber = creditAppReqBusinessCollateral.PreferentialCreditorNumber,
						Lessee = creditAppReqBusinessCollateral.Lessee,
						Lessor = creditAppReqBusinessCollateral.Lessor,
						RegistrationPlateNumber = creditAppReqBusinessCollateral.RegistrationPlateNumber,
						MachineNumber = creditAppReqBusinessCollateral.MachineNumber,
						MachineRegisteredStatus = creditAppReqBusinessCollateral.MachineRegisteredStatus,
						ChassisNumber = creditAppReqBusinessCollateral.ChassisNumber,
						RegisteredPlace = creditAppReqBusinessCollateral.RegisteredPlace,
						GuaranteeAmount = creditAppReqBusinessCollateral.GuaranteeAmount,
						Quantity = creditAppReqBusinessCollateral.Quantity,
						Unit = creditAppReqBusinessCollateral.Unit,
						ProjectName = creditAppReqBusinessCollateral.ProjectName,
						TitleDeedNumber = creditAppReqBusinessCollateral.TitleDeedNumber,
						TitleDeedSubDistrict = creditAppReqBusinessCollateral.TitleDeedSubDistrict,
						TitleDeedDistrict = creditAppReqBusinessCollateral.TitleDeedDistrict,
						TitleDeedProvince = creditAppReqBusinessCollateral.TitleDeedProvince,
						AttachmentRemark = creditAppReqBusinessCollateral.AttachmentRemark,
						DBDRegistrationId = creditAppReqBusinessCollateral.DBDRegistrationId,
						DBDRegistrationDescription = creditAppReqBusinessCollateral.DBDRegistrationDescription,
						DBDRegistrationDate = creditAppReqBusinessCollateral.DBDRegistrationDate,
						DBDRegistrationAmount = creditAppReqBusinessCollateral.DBDRegistrationAmount,
						CapitalValuation = creditAppReqBusinessCollateral.CapitalValuation,
						ValuationCommittee = creditAppReqBusinessCollateral.ValuationCommittee,
						DateOfValuation = creditAppReqBusinessCollateral.DateOfValuation,
						Ownership = creditAppReqBusinessCollateral.Ownership,
						BusinessCollateralValue = creditAppReqBusinessCollateral.BusinessCollateralValue,
					});
		}
		public IEnumerable<SelectItem<CreditAppReqBusinessCollateralItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditAppReqBusinessCollateral>(search, SysParm.CompanyGUID);
				var creditAppReqBusinessCollateral = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CreditAppReqBusinessCollateralItemViewMap, CreditAppReqBusinessCollateralItemView>().ToDropDownItem(search.ExcludeRowData);


				return creditAppReqBusinessCollateral;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CreditAppReqBusinessCollateralListViewMap> GetListQuery()
		{
			return (from creditAppReqBusinessCollateral in Entity
					join customerTable in db.Set<CustomerTable>() on creditAppReqBusinessCollateral.CustomerTableGUID equals customerTable.CustomerTableGUID
					join creditAppRequestTable in db.Set<CreditAppRequestTable>() on creditAppReqBusinessCollateral.CreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID
					join custBusinessCollateral in db.Set<CustBusinessCollateral>() on creditAppReqBusinessCollateral.CustBusinessCollateralGUID equals custBusinessCollateral.CustBusinessCollateralGUID into ljCustBusinessCollateral
					from custBusinessCollateral in ljCustBusinessCollateral.DefaultIfEmpty()
					join businessCollateralType in db.Set<BusinessCollateralType>() on creditAppReqBusinessCollateral.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID
					join businessCollateralSubType in db.Set<BusinessCollateralSubType>() on creditAppReqBusinessCollateral.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID
					select new CreditAppReqBusinessCollateralListViewMap
				{
						CompanyGUID = creditAppReqBusinessCollateral.CompanyGUID,
						Owner = creditAppReqBusinessCollateral.Owner,
						OwnerBusinessUnitGUID = creditAppReqBusinessCollateral.OwnerBusinessUnitGUID,
						CreditAppReqBusinessCollateralGUID = creditAppReqBusinessCollateral.CreditAppReqBusinessCollateralGUID,
						IsNew = creditAppReqBusinessCollateral.IsNew,
						BusinessCollateralValue = creditAppReqBusinessCollateral.BusinessCollateralValue,
						CapitalValuation = creditAppReqBusinessCollateral.CapitalValuation,
						CustBusinessCollateralGUID = creditAppReqBusinessCollateral.CustBusinessCollateralGUID,
						CustomerTableGUID = creditAppReqBusinessCollateral.CustomerTableGUID,
						CreditAppRequestTableGUID = creditAppReqBusinessCollateral.CreditAppRequestTableGUID,
						BusinessCollateralTypeGUID = creditAppReqBusinessCollateral.BusinessCollateralTypeGUID,
						BusinessCollateralSubTypeGUID = creditAppReqBusinessCollateral.BusinessCollateralSubTypeGUID,
						Description = creditAppReqBusinessCollateral.Description,
						RefAgreementId = creditAppReqBusinessCollateral.RefAgreementId,
						RefAgreementDate = creditAppReqBusinessCollateral.RefAgreementDate,
						CreditAppRequestTable_CreditAppRequestId = creditAppRequestTable.CreditAppRequestId,
						CreditAppRequestTable_Values = SmartAppUtil.GetDropDownLabel(creditAppRequestTable.CreditAppRequestId, creditAppRequestTable.Description),
						CustomerTable_CustomerId = customerTable.CustomerId,
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
						CustBusinessCollateral_CustBusinessCollateralId = custBusinessCollateral.CustBusinessCollateralId,
						CustBusinessCollateral_Values = (custBusinessCollateral != null) ? SmartAppUtil.GetDropDownLabel(custBusinessCollateral.CustBusinessCollateralId, custBusinessCollateral.Description) : null,
						BusinessCollateralType_BusinessCollateralTypeId = businessCollateralType.BusinessCollateralTypeId,
						BusinessCollateralType_Values = (businessCollateralType != null) ? SmartAppUtil.GetDropDownLabel(businessCollateralType.BusinessCollateralTypeId, businessCollateralType.Description) : null,
						BusinessCollateralSubType_BusinessCollateralSubTypeId = businessCollateralSubType.BusinessCollateralSubTypeId,
						BusinessCollateralSubType_Values = (businessCollateralSubType != null) ? SmartAppUtil.GetDropDownLabel(businessCollateralSubType.BusinessCollateralSubTypeId, businessCollateralSubType.Description) : null,
						CreatedDateTime = creditAppReqBusinessCollateral.CreatedDateTime
					});
		}
		public SearchResult<CreditAppReqBusinessCollateralListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CreditAppReqBusinessCollateralListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditAppReqBusinessCollateral>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CreditAppReqBusinessCollateralListViewMap, CreditAppReqBusinessCollateralListView>();
				result = list.SetSearchResult<CreditAppReqBusinessCollateralListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CreditAppReqBusinessCollateralItemViewMap> GetItemQuery()
		{
			return (from creditAppReqBusinessCollateral in Entity
					join company in db.Set<Company>()
					on creditAppReqBusinessCollateral.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on creditAppReqBusinessCollateral.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCreditAppReqBusinessCollateralOwnerBU
					from ownerBU in ljCreditAppReqBusinessCollateralOwnerBU.DefaultIfEmpty()
					join customerTable in db.Set<CustomerTable>() on creditAppReqBusinessCollateral.CustomerTableGUID equals customerTable.CustomerTableGUID
					join creditAppRequestTable in db.Set<CreditAppRequestTable>() on creditAppReqBusinessCollateral.CreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID
					select new CreditAppReqBusinessCollateralItemViewMap
					{
						CompanyGUID = creditAppReqBusinessCollateral.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = creditAppReqBusinessCollateral.Owner,
						OwnerBusinessUnitGUID = creditAppReqBusinessCollateral.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = creditAppReqBusinessCollateral.CreatedBy,
						CreatedDateTime = creditAppReqBusinessCollateral.CreatedDateTime,
						ModifiedBy = creditAppReqBusinessCollateral.ModifiedBy,
						ModifiedDateTime = creditAppReqBusinessCollateral.ModifiedDateTime,
						CreditAppReqBusinessCollateralGUID = creditAppReqBusinessCollateral.CreditAppReqBusinessCollateralGUID,
						AccountNumber = creditAppReqBusinessCollateral.AccountNumber,
						AttachmentRemark = creditAppReqBusinessCollateral.AttachmentRemark,
						BankGroupGUID = creditAppReqBusinessCollateral.BankGroupGUID,
						BankTypeGUID = creditAppReqBusinessCollateral.BankTypeGUID,
						BusinessCollateralSubTypeGUID = creditAppReqBusinessCollateral.BusinessCollateralSubTypeGUID,
						BusinessCollateralTypeGUID = creditAppReqBusinessCollateral.BusinessCollateralTypeGUID,
						BusinessCollateralValue = creditAppReqBusinessCollateral.BusinessCollateralValue,
						BuyerName = creditAppReqBusinessCollateral.BuyerName,
						BuyerTableGUID = creditAppReqBusinessCollateral.BuyerTableGUID,
						BuyerTaxIdentificationId = creditAppReqBusinessCollateral.BuyerTaxIdentificationId,
						CapitalValuation = creditAppReqBusinessCollateral.CapitalValuation,
						ChassisNumber = creditAppReqBusinessCollateral.ChassisNumber,
						CreditAppRequestTableGUID = creditAppReqBusinessCollateral.CreditAppRequestTableGUID,
						CustBusinessCollateralGUID = creditAppReqBusinessCollateral.CustBusinessCollateralGUID,
						CustomerTableGUID = creditAppReqBusinessCollateral.CustomerTableGUID,
						DateOfValuation = creditAppReqBusinessCollateral.DateOfValuation,
						DBDRegistrationAmount = creditAppReqBusinessCollateral.DBDRegistrationAmount,
						DBDRegistrationDate = creditAppReqBusinessCollateral.DBDRegistrationDate,
						DBDRegistrationDescription = creditAppReqBusinessCollateral.DBDRegistrationDescription,
						DBDRegistrationId = creditAppReqBusinessCollateral.DBDRegistrationId,
						Description = creditAppReqBusinessCollateral.Description,
						GuaranteeAmount = creditAppReqBusinessCollateral.GuaranteeAmount,
						IsNew = creditAppReqBusinessCollateral.IsNew,
						Lessee = creditAppReqBusinessCollateral.Lessee,
						Lessor = creditAppReqBusinessCollateral.Lessor,
						MachineNumber = creditAppReqBusinessCollateral.MachineNumber,
						MachineRegisteredStatus = creditAppReqBusinessCollateral.MachineRegisteredStatus,
						Ownership = creditAppReqBusinessCollateral.Ownership,
						PreferentialCreditorNumber = creditAppReqBusinessCollateral.PreferentialCreditorNumber,
						ProjectName = creditAppReqBusinessCollateral.ProjectName,
						Quantity = creditAppReqBusinessCollateral.Quantity,
						RefAgreementDate = creditAppReqBusinessCollateral.RefAgreementDate,
						RefAgreementId = creditAppReqBusinessCollateral.RefAgreementId,
						RegisteredPlace = creditAppReqBusinessCollateral.RegisteredPlace,
						RegistrationPlateNumber = creditAppReqBusinessCollateral.RegistrationPlateNumber,
						TitleDeedDistrict = creditAppReqBusinessCollateral.TitleDeedDistrict,
						TitleDeedNumber = creditAppReqBusinessCollateral.TitleDeedNumber,
						TitleDeedProvince = creditAppReqBusinessCollateral.TitleDeedProvince,
						TitleDeedSubDistrict = creditAppReqBusinessCollateral.TitleDeedSubDistrict,
						Unit = creditAppReqBusinessCollateral.Unit,
						ValuationCommittee = creditAppReqBusinessCollateral.ValuationCommittee,
						CreditAppRequestTable_Values = SmartAppUtil.GetDropDownLabel(creditAppRequestTable.CreditAppRequestId, creditAppRequestTable.Description),
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
					
						RowVersion = creditAppReqBusinessCollateral.RowVersion,
					});
		}
		public CreditAppReqBusinessCollateralItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<CreditAppReqBusinessCollateralItemViewMap, CreditAppReqBusinessCollateralItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public CreditAppReqBusinessCollateral CreateCreditAppReqBusinessCollateral(CreditAppReqBusinessCollateral creditAppReqBusinessCollateral)
		{
			try
			{
				creditAppReqBusinessCollateral.CreditAppReqBusinessCollateralGUID = Guid.NewGuid();
				base.Add(creditAppReqBusinessCollateral);
				return creditAppReqBusinessCollateral;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCreditAppReqBusinessCollateralVoid(CreditAppReqBusinessCollateral creditAppReqBusinessCollateral)
		{
			try
			{
				CreateCreditAppReqBusinessCollateral(creditAppReqBusinessCollateral);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditAppReqBusinessCollateral UpdateCreditAppReqBusinessCollateral(CreditAppReqBusinessCollateral dbCreditAppReqBusinessCollateral, CreditAppReqBusinessCollateral inputCreditAppReqBusinessCollateral, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCreditAppReqBusinessCollateral = dbCreditAppReqBusinessCollateral.MapUpdateValues<CreditAppReqBusinessCollateral>(inputCreditAppReqBusinessCollateral);
				base.Update(dbCreditAppReqBusinessCollateral);
				return dbCreditAppReqBusinessCollateral;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCreditAppReqBusinessCollateralVoid(CreditAppReqBusinessCollateral dbCreditAppReqBusinessCollateral, CreditAppReqBusinessCollateral inputCreditAppReqBusinessCollateral, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCreditAppReqBusinessCollateral = dbCreditAppReqBusinessCollateral.MapUpdateValues<CreditAppReqBusinessCollateral>(inputCreditAppReqBusinessCollateral, skipUpdateFields);
				base.Update(dbCreditAppReqBusinessCollateral);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public CreditAppReqBusinessCollateral GetCreditAppReqBusinessCollateralByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CreditAppReqBusinessCollateralGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Validate
		public override void ValidateAdd(IEnumerable<CreditAppReqBusinessCollateral> items)
		{
			base.ValidateAdd(items);
			items.ToList().ForEach(f => CheckDuplicateWhenCustBusinessCollateralNotNull(f));
		}
		public override void ValidateAdd(CreditAppReqBusinessCollateral item)
		{
			base.ValidateAdd(item);
			CheckDuplicateWhenCustBusinessCollateralNotNull(item);
		}
		public override void ValidateUpdate(CreditAppReqBusinessCollateral item)
		{
			base.ValidateUpdate(item);
			CheckDuplicateWhenCustBusinessCollateralNotNull(item);
		}
		public void CheckDuplicateWhenCustBusinessCollateralNotNull(CreditAppReqBusinessCollateral item)
		{
			try
			{
				if (ConditionService.IsNotNullAndNotEmptyGUID(item.CustBusinessCollateralGUID))
				{

					bool isDuplicate = Entity.Any(w => w.CompanyGUID == item.CompanyGUID
					&& w.CreditAppRequestTableGUID == item.CreditAppRequestTableGUID
					&& w.CustBusinessCollateralGUID == item.CustBusinessCollateralGUID
					&& w.CreditAppReqBusinessCollateralGUID != item.CreditAppReqBusinessCollateralGUID);
					if (isDuplicate)
					{
						SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
						smartAppException.AddData("ERROR.DUPLICATE", new string[] { "LABEL.CREDIT_APPLICATION_REQUEST_ID", "LABEL.CUSTOMER_BUSINESS_COLLATERAL_ID" });
						throw SmartAppUtil.AddStackTrace(smartAppException);
					}
				}
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion

		public List<CreditAppReqBusinessCollateral> GetCreditAppReqBusinessCollateralByCreditAppReqNoTracking(Guid creditAppRequestTableGUID)
		{
			try
			{
				var result = Entity.Where(item => item.CreditAppRequestTableGUID == creditAppRequestTableGUID).AsNoTracking().ToList();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

