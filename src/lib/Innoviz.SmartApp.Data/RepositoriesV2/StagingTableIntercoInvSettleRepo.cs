using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.InterfaceWithOtherSystems.Models;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IStagingTableIntercoInvSettleRepo
	{
		#region DropDown
		IEnumerable<SelectItem<StagingTableIntercoInvSettleItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<StagingTableIntercoInvSettleListView> GetListvw(SearchParameter search);
		StagingTableIntercoInvSettleItemView GetByIdvw(Guid id);
		StagingTableIntercoInvSettle Find(params object[] keyValues);
		StagingTableIntercoInvSettle GetStagingTableIntercoInvSettleByIdNoTracking(Guid guid);
		StagingTableIntercoInvSettle CreateStagingTableIntercoInvSettle(StagingTableIntercoInvSettle stagingTableIntercoInvSettle);
		void CreateStagingTableIntercoInvSettleVoid(StagingTableIntercoInvSettle stagingTableIntercoInvSettle);
		StagingTableIntercoInvSettle UpdateStagingTableIntercoInvSettle(StagingTableIntercoInvSettle dbStagingTableIntercoInvSettle, StagingTableIntercoInvSettle inputStagingTableIntercoInvSettle, List<string> skipUpdateFields = null);
		void UpdateStagingTableIntercoInvSettleVoid(StagingTableIntercoInvSettle dbStagingTableIntercoInvSettle, StagingTableIntercoInvSettle inputStagingTableIntercoInvSettle, List<string> skipUpdateFields = null);
		void Remove(StagingTableIntercoInvSettle item);
		StagingTableIntercoInvSettle GetStagingTableIntercoInvSettleByIdNoTrackingByAccessLevel(Guid guid);
		#region interface
		List<InterfaceAXStagingAR> InterfaceIntercoInvSettlementData(List<int> interfaceStatuses);
		IEnumerable<StagingTableIntercoInvSettle> GetByInterfaceStatuses(List<int> interfaceStatuses);
		#endregion interface
	}
	public class StagingTableIntercoInvSettleRepo : CompanyBaseRepository<StagingTableIntercoInvSettle>, IStagingTableIntercoInvSettleRepo
	{
		public StagingTableIntercoInvSettleRepo(SmartAppDbContext context) : base(context) { }
		public StagingTableIntercoInvSettleRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public StagingTableIntercoInvSettle GetStagingTableIntercoInvSettleByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.StagingTableIntercoInvSettleGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<StagingTableIntercoInvSettleItemViewMap> GetDropDownQuery()
		{
			return (from stagingTableIntercoInvSettle in Entity
					select new StagingTableIntercoInvSettleItemViewMap
					{
						CompanyGUID = stagingTableIntercoInvSettle.CompanyGUID,
						Owner = stagingTableIntercoInvSettle.Owner,
						OwnerBusinessUnitGUID = stagingTableIntercoInvSettle.OwnerBusinessUnitGUID,
						StagingTableIntercoInvSettleGUID = stagingTableIntercoInvSettle.StagingTableIntercoInvSettleGUID
					});
		}
		public IEnumerable<SelectItem<StagingTableIntercoInvSettleItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<StagingTableIntercoInvSettle>(search, SysParm.CompanyGUID);
				var stagingTableIntercoInvSettle = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<StagingTableIntercoInvSettleItemViewMap, StagingTableIntercoInvSettleItemView>().ToDropDownItem(search.ExcludeRowData);


				return stagingTableIntercoInvSettle;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<StagingTableIntercoInvSettleListViewMap> GetListQuery()
		{
			return (from stagingTableIntercoInvSettle in Entity
				select new StagingTableIntercoInvSettleListViewMap
				{
						CompanyGUID = stagingTableIntercoInvSettle.CompanyGUID,
						Owner = stagingTableIntercoInvSettle.Owner,
						OwnerBusinessUnitGUID = stagingTableIntercoInvSettle.OwnerBusinessUnitGUID,
						StagingTableIntercoInvSettleGUID = stagingTableIntercoInvSettle.StagingTableIntercoInvSettleGUID,
						IntercompanyInvoiceSettlementId = stagingTableIntercoInvSettle.IntercompanyInvoiceSettlementId,
						InterfaceStatus = stagingTableIntercoInvSettle.InterfaceStatus,
						InvoiceDate = stagingTableIntercoInvSettle.InvoiceDate,
						DueDate = stagingTableIntercoInvSettle.DueDate,
						MethodOfPaymentId = stagingTableIntercoInvSettle.MethodOfPaymentId,
						SettleInvoiceAmount = stagingTableIntercoInvSettle.SettleInvoiceAmount,
						FeeLedgerAccount = stagingTableIntercoInvSettle.FeeLedgerAccount,
						CustomerId = stagingTableIntercoInvSettle.CustomerId,
						Name = stagingTableIntercoInvSettle.Name,
						InterfaceStagingBatchId = stagingTableIntercoInvSettle.InterfaceStagingBatchId,
					IntercompanyInvoiceSettlementGUID = stagingTableIntercoInvSettle.IntercompanyInvoiceSettlementGUID,

				});
		}
		public SearchResult<StagingTableIntercoInvSettleListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<StagingTableIntercoInvSettleListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<StagingTableIntercoInvSettle>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<StagingTableIntercoInvSettleListViewMap, StagingTableIntercoInvSettleListView>();
				result = list.SetSearchResult<StagingTableIntercoInvSettleListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<StagingTableIntercoInvSettleItemViewMap> GetItemQuery()
		{
			return (from stagingTableIntercoInvSettle in Entity
					join company in db.Set<Company>()
					on stagingTableIntercoInvSettle.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on stagingTableIntercoInvSettle.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljStagingTableIntercoInvSettleOwnerBU
					from ownerBU in ljStagingTableIntercoInvSettleOwnerBU.DefaultIfEmpty()
					select new StagingTableIntercoInvSettleItemViewMap
					{
						CompanyGUID = stagingTableIntercoInvSettle.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = stagingTableIntercoInvSettle.Owner,
						OwnerBusinessUnitGUID = stagingTableIntercoInvSettle.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = stagingTableIntercoInvSettle.CreatedBy,
						CreatedDateTime = stagingTableIntercoInvSettle.CreatedDateTime,
						ModifiedBy = stagingTableIntercoInvSettle.ModifiedBy,
						ModifiedDateTime = stagingTableIntercoInvSettle.ModifiedDateTime,
						StagingTableIntercoInvSettleGUID = stagingTableIntercoInvSettle.StagingTableIntercoInvSettleGUID,
						Address1 = stagingTableIntercoInvSettle.Address1,
						AltName = stagingTableIntercoInvSettle.AltName,
						CNReasonId = stagingTableIntercoInvSettle.CNReasonId,
						StagingTableIntercoInvSettleCompanyId = stagingTableIntercoInvSettle.CompanyId,
						CountryId = stagingTableIntercoInvSettle.CountryId,
						CurrencyId = stagingTableIntercoInvSettle.CurrencyId,
						CustFaxValue = stagingTableIntercoInvSettle.CustFaxValue,
						CustGroupId = stagingTableIntercoInvSettle.CustGroupId,
						CustomerId = stagingTableIntercoInvSettle.CustomerId,
						CustPhoneValue = stagingTableIntercoInvSettle.CustPhoneValue,
						DimensionCode1 = stagingTableIntercoInvSettle.DimensionCode1,
						DimensionCode2 = stagingTableIntercoInvSettle.DimensionCode2,
						DimensionCode3 = stagingTableIntercoInvSettle.DimensionCode3,
						DimensionCode4 = stagingTableIntercoInvSettle.DimensionCode4,
						DimensionCode5 = stagingTableIntercoInvSettle.DimensionCode5,
						DistrictId = stagingTableIntercoInvSettle.DistrictId,
						DueDate = stagingTableIntercoInvSettle.DueDate,
						FeeLedgerAccount = stagingTableIntercoInvSettle.FeeLedgerAccount,
						IntercompanyInvoiceSettlementGUID = stagingTableIntercoInvSettle.IntercompanyInvoiceSettlementGUID,
						IntercompanyInvoiceSettlementId = stagingTableIntercoInvSettle.IntercompanyInvoiceSettlementId,
						InterfaceStagingBatchId = stagingTableIntercoInvSettle.InterfaceStagingBatchId,
						InterfaceStatus = stagingTableIntercoInvSettle.InterfaceStatus,
						InvoiceDate = stagingTableIntercoInvSettle.InvoiceDate,
						InvoiceText = stagingTableIntercoInvSettle.InvoiceText,
						MethodOfPaymentId = stagingTableIntercoInvSettle.MethodOfPaymentId,
						Name = stagingTableIntercoInvSettle.Name,
						OrigInvoiceAmount = stagingTableIntercoInvSettle.OrigInvoiceAmount,
						OrigInvoiceId = stagingTableIntercoInvSettle.OrigInvoiceId,
						PostalCode = stagingTableIntercoInvSettle.PostalCode,
						ProvinceId = stagingTableIntercoInvSettle.ProvinceId,
						RecordType = stagingTableIntercoInvSettle.RecordType,
						SettleInvoiceAmount = stagingTableIntercoInvSettle.SettleInvoiceAmount,
						SubDistrictId = stagingTableIntercoInvSettle.SubDistrictId,
						TaxBranchId = stagingTableIntercoInvSettle.TaxBranchId,
						TaxCode = stagingTableIntercoInvSettle.TaxCode,
						TaxId = stagingTableIntercoInvSettle.TaxId,
						CompanyTaxBranchId = stagingTableIntercoInvSettle.CompanyTaxBranchId,
					
						RowVersion = stagingTableIntercoInvSettle.RowVersion,
					});
		}
		public StagingTableIntercoInvSettleItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<StagingTableIntercoInvSettleItemViewMap, StagingTableIntercoInvSettleItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public StagingTableIntercoInvSettle CreateStagingTableIntercoInvSettle(StagingTableIntercoInvSettle stagingTableIntercoInvSettle)
		{
			try
			{
				stagingTableIntercoInvSettle.StagingTableIntercoInvSettleGUID = Guid.NewGuid();
				base.Add(stagingTableIntercoInvSettle);
				return stagingTableIntercoInvSettle;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateStagingTableIntercoInvSettleVoid(StagingTableIntercoInvSettle stagingTableIntercoInvSettle)
		{
			try
			{
				CreateStagingTableIntercoInvSettle(stagingTableIntercoInvSettle);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public StagingTableIntercoInvSettle UpdateStagingTableIntercoInvSettle(StagingTableIntercoInvSettle dbStagingTableIntercoInvSettle, StagingTableIntercoInvSettle inputStagingTableIntercoInvSettle, List<string> skipUpdateFields = null)
		{
			try
			{
				dbStagingTableIntercoInvSettle = dbStagingTableIntercoInvSettle.MapUpdateValues<StagingTableIntercoInvSettle>(inputStagingTableIntercoInvSettle);
				base.Update(dbStagingTableIntercoInvSettle);
				return dbStagingTableIntercoInvSettle;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateStagingTableIntercoInvSettleVoid(StagingTableIntercoInvSettle dbStagingTableIntercoInvSettle, StagingTableIntercoInvSettle inputStagingTableIntercoInvSettle, List<string> skipUpdateFields = null)
		{
			try
			{
				dbStagingTableIntercoInvSettle = dbStagingTableIntercoInvSettle.MapUpdateValues<StagingTableIntercoInvSettle>(inputStagingTableIntercoInvSettle, skipUpdateFields);
				base.Update(dbStagingTableIntercoInvSettle);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public StagingTableIntercoInvSettle GetStagingTableIntercoInvSettleByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.StagingTableIntercoInvSettleGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region interface
		public IEnumerable<StagingTableIntercoInvSettle> GetByInterfaceStatuses(List<int> interfaceStatuses)
		{
            try
            {
				Guid companyGUID = SysParm.CompanyGUID.StringToGuid();
				interfaceStatuses = interfaceStatuses != null && interfaceStatuses.Count > 0 ?
					interfaceStatuses : new List<int> { (int)InterfaceStatus.None, (int)InterfaceStatus.Fail };

				var result = Entity.Where(w => w.CompanyGUID == companyGUID &&
								interfaceStatuses.Contains(w.InterfaceStatus))
					.AsNoTracking();
				return result; 
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<InterfaceAXStagingAR> InterfaceIntercoInvSettlementData(List<int> interfaceStatuses)
		{
			try
			{
				Guid companyGUID = SysParm.CompanyGUID.StringToGuid();
				interfaceStatuses = interfaceStatuses != null && interfaceStatuses.Count > 0 ?
					interfaceStatuses : new List<int> { (int)InterfaceStatus.None, (int)InterfaceStatus.Fail };

				var result = GetByInterfaceStatuses(interfaceStatuses)
									.Select((stagingTableIntercoInvSettle, idx) => new InterfaceAXStagingAR
									{
										CompanyId = SmartAppUtil.HandleInterfaceStringValue(stagingTableIntercoInvSettle.CompanyId, 4),
										DataAreaId = SmartAppUtil.HandleInterfaceStringValue(stagingTableIntercoInvSettle.CompanyId, 4),
										CustRecordType = stagingTableIntercoInvSettle.RecordType,
										CustAccount = SmartAppUtil.HandleInterfaceStringValue(stagingTableIntercoInvSettle.CustomerId, 20),
										CustName = SmartAppUtil.HandleInterfaceStringValue(stagingTableIntercoInvSettle.Name, 100),
										CustNameAlias = SmartAppUtil.HandleInterfaceStringValue(stagingTableIntercoInvSettle.AltName, 20),
										CustCurrency = SmartAppUtil.HandleInterfaceStringValue(stagingTableIntercoInvSettle.CurrencyId, 3),
										CustGroup = SmartAppUtil.HandleInterfaceStringValue(stagingTableIntercoInvSettle.CustGroupId, 10),
										CustTaxExemptNum = SmartAppUtil.HandleInterfaceStringValue(stagingTableIntercoInvSettle.TaxId, 20),
										CustPhoneNo = SmartAppUtil.HandleInterfaceStringValue(stagingTableIntercoInvSettle.CustPhoneValue, 255),
										CustFaxNo = SmartAppUtil.HandleInterfaceStringValue(stagingTableIntercoInvSettle.CustFaxValue, 255),
										CustAddressCountry = SmartAppUtil.HandleInterfaceStringValue(stagingTableIntercoInvSettle.CountryId, 10),
										CustAddressPostalCode = SmartAppUtil.HandleInterfaceStringValue(stagingTableIntercoInvSettle.PostalCode, 10),
										CustAddressStreet = SmartAppUtil.HandleInterfaceStringValue(stagingTableIntercoInvSettle.Address1, 250),
										CustAddressCity = SmartAppUtil.HandleInterfaceStringValue(stagingTableIntercoInvSettle.DistrictId, 60),
										CustAddressDistrict = SmartAppUtil.HandleInterfaceStringValue(stagingTableIntercoInvSettle.SubDistrictId, 60),
										CustAddressState = SmartAppUtil.HandleInterfaceStringValue(stagingTableIntercoInvSettle.ProvinceId, 10),
										CustTaxInfoBranchNum = SmartAppUtil.HandleInterfaceStringValue(stagingTableIntercoInvSettle.TaxBranchId, 15),
										InvoiceDate = stagingTableIntercoInvSettle.InvoiceDate,
										DueDate = stagingTableIntercoInvSettle.DueDate,
										ReferenceInvoice = SmartAppUtil.HandleInterfaceStringValue(stagingTableIntercoInvSettle.OrigInvoiceId, 20),
										ReferenceInvoiceValue = stagingTableIntercoInvSettle.OrigInvoiceAmount,
										ReasonCode = SmartAppUtil.HandleInterfaceStringValue(stagingTableIntercoInvSettle.CNReasonId, 10),
										PaymMode = SmartAppUtil.HandleInterfaceStringValue(stagingTableIntercoInvSettle.MethodOfPaymentId, 10),
										CustomerReq = SmartAppUtil.HandleInterfaceStringValue(stagingTableIntercoInvSettle.IntercompanyInvoiceSettlementId, 20),
										AccountNum = SmartAppUtil.HandleInterfaceStringValue(stagingTableIntercoInvSettle.FeeLedgerAccount, 20),
										TaxCode = SmartAppUtil.HandleInterfaceStringValue(stagingTableIntercoInvSettle.TaxCode, 10),
										Amount = stagingTableIntercoInvSettle.SettleInvoiceAmount,
										Description = stagingTableIntercoInvSettle.InvoiceText,
										DimensionCode1 = SmartAppUtil.HandleInterfaceStringValue(stagingTableIntercoInvSettle.DimensionCode1, 30),
										DimensionCode2 = SmartAppUtil.HandleInterfaceStringValue(stagingTableIntercoInvSettle.DimensionCode2, 30),
										DimensionCode3 = SmartAppUtil.HandleInterfaceStringValue(stagingTableIntercoInvSettle.DimensionCode3, 30),
										DimensionCode4 = SmartAppUtil.HandleInterfaceStringValue(stagingTableIntercoInvSettle.DimensionCode4, 30),
										DimensionCode5 = SmartAppUtil.HandleInterfaceStringValue(stagingTableIntercoInvSettle.DimensionCode5, 30),
										RecId = SmartAppUtil.GetRecIdStagingTable(idx),
										CompanyTaxBranchId = stagingTableIntercoInvSettle.CompanyTaxBranchId
									})
									.ToList();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion interface
	}
}

