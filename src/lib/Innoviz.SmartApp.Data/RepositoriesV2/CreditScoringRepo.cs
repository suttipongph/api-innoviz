using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICreditScoringRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CreditScoringItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<CreditScoringListView> GetListvw(SearchParameter search);
		CreditScoringItemView GetByIdvw(Guid id);
		CreditScoring Find(params object[] keyValues);
		CreditScoring GetCreditScoringByIdNoTracking(Guid guid);
		CreditScoring CreateCreditScoring(CreditScoring creditScoring);
		void CreateCreditScoringVoid(CreditScoring creditScoring);
		CreditScoring UpdateCreditScoring(CreditScoring dbCreditScoring, CreditScoring inputCreditScoring, List<string> skipUpdateFields = null);
		void UpdateCreditScoringVoid(CreditScoring dbCreditScoring, CreditScoring inputCreditScoring, List<string> skipUpdateFields = null);
		void Remove(CreditScoring item);
	}
	public class CreditScoringRepo : CompanyBaseRepository<CreditScoring>, ICreditScoringRepo
	{
		public CreditScoringRepo(SmartAppDbContext context) : base(context) { }
		public CreditScoringRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CreditScoring GetCreditScoringByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CreditScoringGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CreditScoringItemViewMap> GetDropDownQuery()
		{
			return (from creditScoring in Entity
					select new CreditScoringItemViewMap
					{
						CompanyGUID = creditScoring.CompanyGUID,
						Owner = creditScoring.Owner,
						OwnerBusinessUnitGUID = creditScoring.OwnerBusinessUnitGUID,
						CreditScoringGUID = creditScoring.CreditScoringGUID,
						CreditScoringId = creditScoring.CreditScoringId,
						Description = creditScoring.Description
					});
		}
		public IEnumerable<SelectItem<CreditScoringItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditScoring>(search, SysParm.CompanyGUID);
				var creditScoring = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CreditScoringItemViewMap, CreditScoringItemView>().ToDropDownItem(search.ExcludeRowData);


				return creditScoring;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CreditScoringListViewMap> GetListQuery()
		{
			return (from creditScoring in Entity
				select new CreditScoringListViewMap
				{
						CompanyGUID = creditScoring.CompanyGUID,
						Owner = creditScoring.Owner,
						OwnerBusinessUnitGUID = creditScoring.OwnerBusinessUnitGUID,
						CreditScoringGUID = creditScoring.CreditScoringGUID,
						CreditScoringId = creditScoring.CreditScoringId,
						Description = creditScoring.Description,
				});
		}
		public SearchResult<CreditScoringListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CreditScoringListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditScoring>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CreditScoringListViewMap, CreditScoringListView>();
				result = list.SetSearchResult<CreditScoringListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CreditScoringItemViewMap> GetItemQuery()
		{
			return (from creditScoring in Entity
					join company in db.Set<Company>()
					on creditScoring.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on creditScoring.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCreditScoringOwnerBU
					from ownerBU in ljCreditScoringOwnerBU.DefaultIfEmpty()
					select new CreditScoringItemViewMap
					{
						CompanyGUID = creditScoring.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = creditScoring.Owner,
						OwnerBusinessUnitGUID = creditScoring.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = creditScoring.CreatedBy,
						CreatedDateTime = creditScoring.CreatedDateTime,
						ModifiedBy = creditScoring.ModifiedBy,
						ModifiedDateTime = creditScoring.ModifiedDateTime,
						CreditScoringGUID = creditScoring.CreditScoringGUID,
						CreditScoringId = creditScoring.CreditScoringId,
						Description = creditScoring.Description,
					
						RowVersion = creditScoring.RowVersion,
					});
		}
		public CreditScoringItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<CreditScoringItemViewMap, CreditScoringItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public CreditScoring CreateCreditScoring(CreditScoring creditScoring)
		{
			try
			{
				creditScoring.CreditScoringGUID = Guid.NewGuid();
				base.Add(creditScoring);
				return creditScoring;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCreditScoringVoid(CreditScoring creditScoring)
		{
			try
			{
				CreateCreditScoring(creditScoring);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditScoring UpdateCreditScoring(CreditScoring dbCreditScoring, CreditScoring inputCreditScoring, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCreditScoring = dbCreditScoring.MapUpdateValues<CreditScoring>(inputCreditScoring);
				base.Update(dbCreditScoring);
				return dbCreditScoring;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCreditScoringVoid(CreditScoring dbCreditScoring, CreditScoring inputCreditScoring, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCreditScoring = dbCreditScoring.MapUpdateValues<CreditScoring>(inputCreditScoring, skipUpdateFields);
				base.Update(dbCreditScoring);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

