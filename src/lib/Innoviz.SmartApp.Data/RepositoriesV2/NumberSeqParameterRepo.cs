using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface INumberSeqParameterRepo
	{
		#region DropDown
		IEnumerable<SelectItem<NumberSeqParameterItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<NumberSeqParameterListView> GetListvw(SearchParameter search);
		NumberSeqParameterItemView GetByIdvw(Guid id);
		NumberSeqParameter Find(params object[] keyValues);
		NumberSeqParameter GetNumberSeqParameterByIdNoTracking(Guid guid);
		NumberSeqParameter CreateNumberSeqParameter(NumberSeqParameter numberSeqParameter);
		void CreateNumberSeqParameterVoid(NumberSeqParameter numberSeqParameter);
		NumberSeqParameter UpdateNumberSeqParameter(NumberSeqParameter dbNumberSeqParameter, NumberSeqParameter inputNumberSeqParameter, List<string> skipUpdateFields = null);
		void UpdateNumberSeqParameterVoid(NumberSeqParameter dbNumberSeqParameter, NumberSeqParameter inputNumberSeqParameter, List<string> skipUpdateFields = null);
		void Remove(NumberSeqParameter item);
		NumberSeqParameter GetByReferenceId(Guid companyGUID, string referenceId);
		public NumberSeqParameter GetNumberSeqParameterByIdNoTrackingByAccessLevel(Guid guid);
	}
	public class NumberSeqParameterRepo : CompanyBaseRepository<NumberSeqParameter>, INumberSeqParameterRepo
	{
		public NumberSeqParameterRepo(SmartAppDbContext context) : base(context) { }
		public NumberSeqParameterRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public NumberSeqParameter GetNumberSeqParameterByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.NumberSeqParameterGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<NumberSeqParameterItemViewMap> GetDropDownQuery()
		{
			return (from numberSeqParameter in Entity
					select new NumberSeqParameterItemViewMap
					{
						CompanyGUID = numberSeqParameter.CompanyGUID,
						Owner = numberSeqParameter.Owner,
						OwnerBusinessUnitGUID = numberSeqParameter.OwnerBusinessUnitGUID,
						NumberSeqParameterGUID = numberSeqParameter.NumberSeqParameterGUID
					});
		}
		public IEnumerable<SelectItem<NumberSeqParameterItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<NumberSeqParameter>(search, SysParm.CompanyGUID);
				var numberSeqParameter = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<NumberSeqParameterItemViewMap, NumberSeqParameterItemView>().ToDropDownItem(search.ExcludeRowData);


				return numberSeqParameter;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<NumberSeqParameterListViewMap> GetListQuery()
		{
			return (from numberSeqParameter in Entity
					join numberSeqTable in db.Set<NumberSeqTable>()
					on numberSeqParameter.NumberSeqTableGUID equals numberSeqTable.NumberSeqTableGUID into ljnumberSeqParameterNumbeNumberSeqTable
					from numberSeqTable in ljnumberSeqParameterNumbeNumberSeqTable.DefaultIfEmpty()
					select new NumberSeqParameterListViewMap
					{
						CompanyGUID = numberSeqParameter.CompanyGUID,
						Owner = numberSeqParameter.Owner,
						OwnerBusinessUnitGUID = numberSeqParameter.OwnerBusinessUnitGUID,
						NumberSeqParameterGUID = numberSeqParameter.NumberSeqParameterGUID,
						NumberSeqTableGUID = numberSeqParameter.NumberSeqTableGUID,
						NumberSeqTable_Values = SmartAppUtil.GetDropDownLabel(numberSeqTable.NumberSeqCode,numberSeqTable.Description),
						ReferenceId = numberSeqParameter.ReferenceId,
						Numberseqtable_numberseqcode = numberSeqTable.NumberSeqCode
					});
		}
		public SearchResult<NumberSeqParameterListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<NumberSeqParameterListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<NumberSeqParameter>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<NumberSeqParameterListViewMap, NumberSeqParameterListView>();
				result = list.SetSearchResult<NumberSeqParameterListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<NumberSeqParameterItemViewMap> GetItemQuery()
		{
			return (from numberSeqParameter in Entity
					join company in db.Set<Company>()
					on numberSeqParameter.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on numberSeqParameter.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljNumberSeqParameterOwnerBU
					from ownerBU in ljNumberSeqParameterOwnerBU.DefaultIfEmpty()
					select new NumberSeqParameterItemViewMap
					{
						CompanyGUID = numberSeqParameter.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = numberSeqParameter.Owner,
						OwnerBusinessUnitGUID = numberSeqParameter.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = numberSeqParameter.CreatedBy,
						CreatedDateTime = numberSeqParameter.CreatedDateTime,
						ModifiedBy = numberSeqParameter.ModifiedBy,
						ModifiedDateTime = numberSeqParameter.ModifiedDateTime,
						NumberSeqParameterGUID = numberSeqParameter.NumberSeqParameterGUID,
						NumberSeqTableGUID = numberSeqParameter.NumberSeqTableGUID,
						ReferenceId = numberSeqParameter.ReferenceId,
					
						RowVersion = numberSeqParameter.RowVersion,
					});
		}
		public NumberSeqParameterItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<NumberSeqParameterItemViewMap, NumberSeqParameterItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public NumberSeqParameter CreateNumberSeqParameter(NumberSeqParameter numberSeqParameter)
		{
			try
			{
				numberSeqParameter.NumberSeqParameterGUID = Guid.NewGuid();
				base.Add(numberSeqParameter);
				return numberSeqParameter;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateNumberSeqParameterVoid(NumberSeqParameter numberSeqParameter)
		{
			try
			{
				CreateNumberSeqParameter(numberSeqParameter);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public NumberSeqParameter UpdateNumberSeqParameter(NumberSeqParameter dbNumberSeqParameter, NumberSeqParameter inputNumberSeqParameter, List<string> skipUpdateFields = null)
		{
			try
			{
				dbNumberSeqParameter = dbNumberSeqParameter.MapUpdateValues<NumberSeqParameter>(inputNumberSeqParameter);
				base.Update(dbNumberSeqParameter);
				return dbNumberSeqParameter;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateNumberSeqParameterVoid(NumberSeqParameter dbNumberSeqParameter, NumberSeqParameter inputNumberSeqParameter, List<string> skipUpdateFields = null)
		{
			try
			{
				dbNumberSeqParameter = dbNumberSeqParameter.MapUpdateValues<NumberSeqParameter>(inputNumberSeqParameter, skipUpdateFields);
				base.Update(dbNumberSeqParameter);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete

		public NumberSeqParameter GetByReferenceId(Guid companyGUID, string referenceId)
        {
			try
			{
				NumberSeqParameter numberSeqParameter = Entity.Where(w => w.CompanyGUID == companyGUID && w.ReferenceId == referenceId).FirstOrDefault();
				if (numberSeqParameter == null)
                {
					SmartAppException ex = new SmartAppException("ERROR.ERROR");
					ex.AddData("ERROR.90010");
					throw ex;
				}
					return numberSeqParameter;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public NumberSeqParameter GetNumberSeqParameterByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.NumberSeqParameterGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

