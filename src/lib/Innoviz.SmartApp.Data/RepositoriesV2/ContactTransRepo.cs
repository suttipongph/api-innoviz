using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IContactTransRepo
	{
		#region DropDown
		IEnumerable<SelectItem<ContactTransItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<ContactTransListView> GetListvw(SearchParameter search);
		ContactTransItemView GetByIdvw(Guid id);
		ContactTrans Find(params object[] keyValues);
		ContactTrans GetContactTransByIdNoTracking(Guid guid);
		ContactTrans CreateContactTrans(ContactTrans contactTrans);
		void CreateContactTransVoid(ContactTrans contactTrans);
		ContactTrans UpdateContactTrans(ContactTrans dbContactTrans, ContactTrans inputContactTrans, List<string> skipUpdateFields = null);
		void UpdateContactTransVoid(ContactTrans dbContactTrans, ContactTrans inputContactTrans, List<string> skipUpdateFields = null);
		void Remove(ContactTrans item);
		void ValidateAdd(ContactTrans item);
		void ValidateAdd(IEnumerable<ContactTrans> items);
		#region function
		ContactTrans GetPrimaryContactTransByRefType(Guid refGuid, int refType);
		IEnumerable<ContactTrans> GetPrimaryContactTransByEmailType(Guid refGuid, int refType);
		IEnumerable<ContactTrans> GetPrimaryContactTransListByRefType(Guid refGuid, int refType);
		ContactTrans GetPrimaryContactTransByRefTypeAndContactType(Guid refGuid, int refType, int contactType);
		#endregion
	}
    public class ContactTransRepo : CompanyBaseRepository<ContactTrans>, IContactTransRepo
	{
		public ContactTransRepo(SmartAppDbContext context) : base(context) { }
		public ContactTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ContactTrans GetContactTransByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ContactTransGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<ContactTransItemViewMap> GetDropDownQuery()
		{
			return (from contactTrans in Entity
					select new ContactTransItemViewMap
					{
						CompanyGUID = contactTrans.CompanyGUID,
						Owner = contactTrans.Owner,
						OwnerBusinessUnitGUID = contactTrans.OwnerBusinessUnitGUID,
						ContactTransGUID = contactTrans.ContactTransGUID,
						ContactType = contactTrans.ContactType,
						ContactValue = contactTrans.ContactValue
					});
		}
		public IEnumerable<SelectItem<ContactTransItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<ContactTrans>(search, SysParm.CompanyGUID);
				var contactTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<ContactTransItemViewMap, ContactTransItemView>().ToDropDownItem(search.ExcludeRowData);


				return contactTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<ContactTransListViewMap> GetListQuery()
		{
			return (from contactTrans in Entity
				select new ContactTransListViewMap
				{
						CompanyGUID = contactTrans.CompanyGUID,
						Owner = contactTrans.Owner,
						OwnerBusinessUnitGUID = contactTrans.OwnerBusinessUnitGUID,
						ContactTransGUID = contactTrans.ContactTransGUID,
						ContactType = contactTrans.ContactType,
						PrimaryContact = contactTrans.PrimaryContact,
						Description = contactTrans.Description,
						ContactValue = contactTrans.ContactValue,
						RefGUID = contactTrans.RefGUID
				});
		}
		public SearchResult<ContactTransListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<ContactTransListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<ContactTrans>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<ContactTransListViewMap, ContactTransListView>();
				result = list.SetSearchResult<ContactTransListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<ContactTransItemViewMap> GetItemQuery()
		{
			return (from contactTrans in Entity
					join company in db.Set<Company>()
					on contactTrans.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on contactTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljContactTransOwnerBU
					from ownerBU in ljContactTransOwnerBU.DefaultIfEmpty()
					join buyer in db.Set<BuyerTable>() on contactTrans.RefGUID equals buyer.BuyerTableGUID into ljContactTransBuyer
					from buyer in ljContactTransBuyer.DefaultIfEmpty()
					join customer in db.Set<CustomerTable>() on contactTrans.RefGUID equals customer.CustomerTableGUID into ljContactTransCustomer
					from customer in ljContactTransCustomer.DefaultIfEmpty()
					join vendor in db.Set<VendorTable>() on contactTrans.RefGUID equals vendor.VendorTableGUID into ljContactTransVendor
					from vendor in ljContactTransVendor.DefaultIfEmpty()
					join employeeTable in db.Set<EmployeeTable>() on contactTrans.RefGUID equals employeeTable.EmployeeTableGUID into ljContactTransEmployeeTable
					from employeeTable in ljContactTransEmployeeTable.DefaultIfEmpty()
					select new ContactTransItemViewMap
					{
						CompanyGUID = contactTrans.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = contactTrans.Owner,
						OwnerBusinessUnitGUID = contactTrans.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = contactTrans.CreatedBy,
						CreatedDateTime = contactTrans.CreatedDateTime,
						ModifiedBy = contactTrans.ModifiedBy,
						ModifiedDateTime = contactTrans.ModifiedDateTime,
						ContactTransGUID = contactTrans.ContactTransGUID,
						ContactType = contactTrans.ContactType,
						ContactValue = contactTrans.ContactValue,
						Description = contactTrans.Description,
						Extension = contactTrans.Extension,
						PrimaryContact = contactTrans.PrimaryContact,
						RefGUID = contactTrans.RefGUID,
						RefType = contactTrans.RefType,
						RefId = (buyer != null && contactTrans.RefType == (int)RefType.Buyer) ? buyer.BuyerId :
								(customer != null && contactTrans.RefType == (int)RefType.Customer) ? customer.CustomerId :
								(vendor != null && contactTrans.RefType == (int)RefType.Vendor) ? vendor.VendorId :
								(vendor != null && contactTrans.RefType == (int)RefType.Employee) ? employeeTable.EmployeeId : null,
					
						RowVersion = contactTrans.RowVersion,
					});
		}
		public ContactTransItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<ContactTransItemViewMap, ContactTransItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public ContactTrans CreateContactTrans(ContactTrans contactTrans)
		{
			try
			{
				contactTrans.ContactTransGUID = Guid.NewGuid();
				base.Add(contactTrans);
				return contactTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateContactTransVoid(ContactTrans contactTrans)
		{
			try
			{
				CreateContactTrans(contactTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ContactTrans UpdateContactTrans(ContactTrans dbContactTrans, ContactTrans inputContactTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbContactTrans = dbContactTrans.MapUpdateValues<ContactTrans>(inputContactTrans);
				base.Update(dbContactTrans);
				return dbContactTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateContactTransVoid(ContactTrans dbContactTrans, ContactTrans inputContactTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbContactTrans = dbContactTrans.MapUpdateValues<ContactTrans>(inputContactTrans, skipUpdateFields);
				base.Update(dbContactTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public IEnumerable<ContactTrans> GetContactTransByReferance(Guid companyGUID, Guid refId,int refType)
		{
			try
			{
				return Entity.Where(t => t.CompanyGUID == companyGUID && t.RefGUID == refId && t.RefType == refType).AsNoTracking();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}

		#region Validate
		public void CheckDuplicatePrimaryContact(ContactTrans item)
		{
			try
			{
				IEnumerable<ContactTrans> contactTrans = GetContactTransByReferance(item.CompanyGUID, item.RefGUID, item.RefType);
				SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
				bool isDupplicateOrdering = contactTrans.Any(a => (a.PrimaryContact == true)
															   && (item.PrimaryContact == true)
															   && item.ContactType == a.ContactType
															   && (a.ContactTransGUID != item.ContactTransGUID));
				if (isDupplicateOrdering)
				{
					smartAppException.AddData("ERROR.DUPLICATE", new string[] { "LABEL.REFERENCE_TYPE", "LABEL.REFERENCE_ID", "LABEL.CONTACT_TYPE", "LABEL.PRIMARY_CONTACT" });
				}



				if (smartAppException.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(smartAppException);
				}
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
        public override void ValidateAdd(ContactTrans item)
        {
            base.ValidateAdd(item);
			CheckDuplicatePrimaryContact(item);
		}
        public override void ValidateUpdate(ContactTrans item)
        {
            base.ValidateUpdate(item);
			CheckDuplicatePrimaryContact(item);
		}
		#endregion
		#region function
		public ContactTrans GetPrimaryContactTransByRefType(Guid refGuid,int refType)
		{
			try
			{
				var result = Entity.Where(item => item.RefGUID == refGuid && item.RefType == refType && item.PrimaryContact).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public IEnumerable<ContactTrans> GetPrimaryContactTransByEmailType(Guid refGuid, int refType)
		{
			try
			{
				var result = Entity.Where(item => item.RefGUID == refGuid && 
												  item.RefType == refType && 
												  item.ContactType == (int)ContactType.Email &&
												  item.PrimaryContact).AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<ContactTrans> GetPrimaryContactTransListByRefType(Guid refGuid, int refType)
		{
			try
			{
				var result = Entity.Where(item => item.RefGUID == refGuid && item.RefType == refType && item.PrimaryContact).AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ContactTrans GetPrimaryContactTransByRefTypeAndContactType(Guid refGuid, int refType, int contactType)
		{
			try
			{
				var result = Entity.Where(item => item.RefGUID == refGuid && item.RefType == refType && item.ContactType == contactType && item.PrimaryContact).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}

