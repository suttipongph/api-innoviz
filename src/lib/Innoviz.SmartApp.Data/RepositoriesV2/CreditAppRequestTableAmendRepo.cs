using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICreditAppRequestTableAmendRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CreditAppRequestTableAmendItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<CreditAppRequestTableAmendListView> GetListvw(SearchParameter search);
		CreditAppRequestTableAmendItemView GetByIdvw(Guid id);
		CreditAppRequestTableAmend Find(params object[] keyValues);
		CreditAppRequestTableAmend GetCreditAppRequestTableAmendByIdNoTracking(Guid guid);
		CreditAppRequestTableAmend CreateCreditAppRequestTableAmend(CreditAppRequestTableAmend creditAppRequestTableAmend);
		void CreateCreditAppRequestTableAmendVoid(CreditAppRequestTableAmend creditAppRequestTableAmend);
		CreditAppRequestTableAmend UpdateCreditAppRequestTableAmend(CreditAppRequestTableAmend dbCreditAppRequestTableAmend, CreditAppRequestTableAmend inputCreditAppRequestTableAmend, List<string> skipUpdateFields = null);
		void UpdateCreditAppRequestTableAmendVoid(CreditAppRequestTableAmend dbCreditAppRequestTableAmend, CreditAppRequestTableAmend inputCreditAppRequestTableAmend, List<string> skipUpdateFields = null);
		void Remove(CreditAppRequestTableAmend item);
		CreditAppRequestTableAmend GetCreditAppRequestTableAmendByIdNoTrackingByAccessLevel(Guid guid);
		CreditAppRequestTableAmend GetCreditAppRequestTableAmendByCreditAppRequestTable(Guid guid);
	}
    public class CreditAppRequestTableAmendRepo : CompanyBaseRepository<CreditAppRequestTableAmend>, ICreditAppRequestTableAmendRepo
	{
		public CreditAppRequestTableAmendRepo(SmartAppDbContext context) : base(context) { }
		public CreditAppRequestTableAmendRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CreditAppRequestTableAmend GetCreditAppRequestTableAmendByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CreditAppRequestTableAmendGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CreditAppRequestTableAmendItemViewMap> GetDropDownQuery()
		{
			return (from creditAppRequestTableAmend in Entity
					select new CreditAppRequestTableAmendItemViewMap
					{
						CompanyGUID = creditAppRequestTableAmend.CompanyGUID,
						Owner = creditAppRequestTableAmend.Owner,
						OwnerBusinessUnitGUID = creditAppRequestTableAmend.OwnerBusinessUnitGUID,
						CreditAppRequestTableAmendGUID = creditAppRequestTableAmend.CreditAppRequestTableAmendGUID,
					});
		}
		public IEnumerable<SelectItem<CreditAppRequestTableAmendItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditAppRequestTableAmend>(search, SysParm.CompanyGUID);
				var creditAppRequestTableAmend = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CreditAppRequestTableAmendItemViewMap, CreditAppRequestTableAmendItemView>().ToDropDownItem(search.ExcludeRowData);


				return creditAppRequestTableAmend;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CreditAppRequestTableAmendListViewMap> GetListQuery()
		{
			return (from creditAppRequestTableAmend in Entity
					join creditAppRequestTable in db.Set<CreditAppRequestTable>() on creditAppRequestTableAmend.CreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID
					join customerTable in db.Set<CustomerTable>() on creditAppRequestTable.CustomerTableGUID equals customerTable.CustomerTableGUID
					join documentStatus in db.Set<DocumentStatus>() on creditAppRequestTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID 
					select new CreditAppRequestTableAmendListViewMap
				{
						CompanyGUID = creditAppRequestTableAmend.CompanyGUID,
						Owner = creditAppRequestTableAmend.Owner,
						OwnerBusinessUnitGUID = creditAppRequestTableAmend.OwnerBusinessUnitGUID,
						CreditAppRequestTable_Description = creditAppRequestTable.Description,
						CreditAppRequestTableAmendGUID = creditAppRequestTableAmend.CreditAppRequestTableAmendGUID,
						CreditAppRequestTable_CreditAppRequestId = creditAppRequestTable.CreditAppRequestId,
						CustomerTable_CustomerId = customerTable.CustomerId,
						DocumentStatus_Description = documentStatus.Description,
						CreditAppRequestTable_RequestDate = creditAppRequestTable.RequestDate,
						CreditAppRequestTable_CreditAppRequestType = creditAppRequestTable.CreditAppRequestType,
						CreditAppRequestTable_RefCreditAppTableGUID = creditAppRequestTable.RefCreditAppTableGUID,
					});
		}
		public SearchResult<CreditAppRequestTableAmendListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CreditAppRequestTableAmendListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditAppRequestTableAmend>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CreditAppRequestTableAmendListViewMap, CreditAppRequestTableAmendListView>();
				result = list.SetSearchResult<CreditAppRequestTableAmendListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CreditAppRequestTableAmendItemViewMap> GetItemQuery(Guid guid)
		{
			var newTotalInterestPct = (from creditAppRequestTableAmend in Entity
					 join creditAppRequestTable in db.Set<CreditAppRequestTable>() on creditAppRequestTableAmend.CreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID
					 join interestType in db.Set<InterestType>() on creditAppRequestTable.InterestTypeGUID equals interestType.InterestTypeGUID into ljCreditAppRequestTableAmendInterestType
					 from interestType in ljCreditAppRequestTableAmendInterestType.DefaultIfEmpty()
					 join interestTypeValue in db.Set<InterestTypeValue>() on interestType.InterestTypeGUID equals interestTypeValue.InterestTypeGUID into ljInterestTypeInterestTypeValue
					 from interestTypeValue in ljInterestTypeInterestTypeValue.DefaultIfEmpty()
					 where (creditAppRequestTableAmend.CreditAppRequestTableAmendGUID == guid)
					 && (interestTypeValue.EffectiveFrom <= creditAppRequestTable.RequestDate && (interestTypeValue.EffectiveTo >= creditAppRequestTable.RequestDate || interestTypeValue.EffectiveTo == (DateTime?)null))
					 select new CreditAppRequestTableAmendItemViewMap
					 {
						 CreditAppRequestTableAmendGUID = creditAppRequestTableAmend.CreditAppRequestTableAmendGUID,
						 CreditAppRequestTable_NewTotalInterestPct = (interestTypeValue != null) ? creditAppRequestTable.InterestAdjustment + (interestTypeValue.Value): creditAppRequestTable.InterestAdjustment,
					 });

			return (from creditAppRequestTableAmend in Entity
					join company in db.Set<Company>()
					on creditAppRequestTableAmend.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on creditAppRequestTableAmend.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCreditAppRequestTableAmendOwnerBU
					from ownerBU in ljCreditAppRequestTableAmendOwnerBU.DefaultIfEmpty()
					join creditAppRequestTable in db.Set<CreditAppRequestTable>() on creditAppRequestTableAmend.CreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID
					join customerTable in db.Set<CustomerTable>() on creditAppRequestTable.CustomerTableGUID equals customerTable.CustomerTableGUID
					join documentStatus in db.Set<DocumentStatus>() on creditAppRequestTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
					join interestType in db.Set<InterestType>() on creditAppRequestTableAmend.OriginalInterestTypeGUID equals interestType.InterestTypeGUID into ljCreditAppRequestTableAmendInterestType
					from interestType in ljCreditAppRequestTableAmendInterestType.DefaultIfEmpty()
					join _newTotalInterestPct in newTotalInterestPct on creditAppRequestTableAmend.CreditAppRequestTableAmendGUID equals _newTotalInterestPct.CreditAppRequestTableAmendGUID into ljCreditAppRequestTableAmendNewTotalInterestPct
					from _newTotalInterestPct in ljCreditAppRequestTableAmendNewTotalInterestPct.DefaultIfEmpty()
					join creditAppTable in db.Set<CreditAppTable>() on creditAppRequestTable.RefCreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljCreditAppTable
					from creditAppTable in ljCreditAppTable.DefaultIfEmpty()
					select new CreditAppRequestTableAmendItemViewMap 
					{
						CompanyGUID = creditAppRequestTableAmend.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = creditAppRequestTableAmend.Owner,
						OwnerBusinessUnitGUID = creditAppRequestTableAmend.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = creditAppRequestTableAmend.CreatedBy,
						CreatedDateTime = creditAppRequestTableAmend.CreatedDateTime,
						ModifiedBy = creditAppRequestTableAmend.ModifiedBy,
						ModifiedDateTime = creditAppRequestTableAmend.ModifiedDateTime,
						CreditAppRequestTableAmendGUID = creditAppRequestTableAmend.CreditAppRequestTableAmendGUID,
						AmendAuthorizedPerson = creditAppRequestTableAmend.AmendAuthorizedPerson,
						AmendGuarantor = creditAppRequestTableAmend.AmendGuarantor,
						AmendRate = creditAppRequestTableAmend.AmendRate,
						CreditAppRequestTableGUID = creditAppRequestTableAmend.CreditAppRequestTableGUID,
						OriginalCreditLimit = creditAppRequestTableAmend.OriginalCreditLimit,
						OriginalCreditLimitRequestFeeAmount = creditAppRequestTableAmend.OriginalCreditLimitRequestFeeAmount,
						OriginalInterestAdjustmentPct = creditAppRequestTableAmend.OriginalInterestAdjustmentPct,
						OriginalInterestTypeGUID = creditAppRequestTableAmend.OriginalInterestTypeGUID,
						OriginalMaxPurchasePct = creditAppRequestTableAmend.OriginalMaxPurchasePct,
						OriginalMaxRetentionAmount = creditAppRequestTableAmend.OriginalMaxRetentionAmount,
						OriginalMaxRetentionPct = creditAppRequestTableAmend.OriginalMaxRetentionPct,
						OriginalPurchaseFeeCalculateBase = creditAppRequestTableAmend.OriginalPurchaseFeeCalculateBase,
						OriginalPurchaseFeePct = creditAppRequestTableAmend.OriginalPurchaseFeePct,
						OriginalTotalInterestPct = creditAppRequestTableAmend.OriginalTotalInterestPct,
						CNReasonGUID = creditAppRequestTableAmend.CNReasonGUID,
						OrigInvoiceAmount = creditAppRequestTableAmend.OrigInvoiceAmount,
						OrigInvoiceId = creditAppRequestTableAmend.OrigInvoiceId,
						OrigTaxInvoiceAmount = creditAppRequestTableAmend.OrigTaxInvoiceAmount,
						OrigTaxInvoiceId = creditAppRequestTableAmend.OrigTaxInvoiceId,

						CreditAppRequestTable_CreditAppRequestId = creditAppRequestTable.CreditAppRequestId,
						CreditAppRequestTable_Description = creditAppRequestTable.Description,
						CreditAppRequestTable_CreditAppRequestType = creditAppRequestTable.CreditAppRequestType,
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
						CreditAppRequestTable_CustomerTableGUID = customerTable.CustomerTableGUID,
						DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
						DocumentStatus_StatusId = documentStatus.StatusId,
						CreditAppRequestTable_StartDate = creditAppRequestTable.StartDate,
						CreditAppRequestTable_ExpiryDate = creditAppRequestTable.ExpiryDate,
						CreditAppRequestTable_RequestDate = creditAppRequestTable.RequestDate,
						CreditAppRequestTable_CustomerCreditLimit = creditAppRequestTable.CustomerCreditLimit,
						RefCreditAppTableGUID = creditAppRequestTable.RefCreditAppTableGUID,
						RefCreditAppTable_Values = (creditAppTable != null) ? SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description): string.Empty,
						CreditAppRequestTable_Remark = creditAppRequestTable.Remark,
						CreditAppRequestTable_NewCreditLimit =  creditAppRequestTable.CreditLimitRequest,
						CreditAppRequestTable_CreditLimitRequestFeePct = creditAppRequestTable.CreditRequestFeePct,
						CreditAppRequestTable_NewCreditLimitRequestFeeAmount = creditAppRequestTable.CreditRequestFeeAmount,
						CreditAppRequestTable_CreditLimitRequestFeeAmount = creditAppRequestTable.CreditRequestFeeAmount,
						CreditAppRequestTable_ApprovedCreditLimitRequest = creditAppRequestTable.ApprovedCreditLimitRequest,
                        CreditAppRequestTable_ApprovedDate = creditAppRequestTable.ApprovedDate,
                        CreditAppRequestTable_MarketingComment = creditAppRequestTable.MarketingComment,
						CreditAppRequestTable_CreditComment = creditAppRequestTable.CreditComment,
						CreditAppRequestTable_ApproverComment = creditAppRequestTable.ApproverComment,
						OriginalInterestType_Values = (interestType != null) ? SmartAppUtil.GetDropDownLabel(interestType.InterestTypeId, interestType.Description): string.Empty,

						#region Amend Rate / ActiveAmendCustomerCreditLimit
						CreditAppRequestTable_NewCreditLimitRequestFeePct = creditAppRequestTable.CreditRequestFeePct,
						CreditAppRequestTable_NewMaxRetentionPct = creditAppRequestTable.MaxRetentionPct,
						CreditAppRequestTable_NewMaxRetentionAmount = creditAppRequestTable.MaxRetentionAmount,
						CreditAppRequestTable_NewMaxPurchasePct = creditAppRequestTable.MaxPurchasePct,
						CreditAppRequestTable_NewPurchaseFeePct = creditAppRequestTable.PurchaseFeePct,
						CreditAppRequestTable_NewPurchaseFeeCalculateBase = creditAppRequestTable.PurchaseFeeCalculateBase,
                        CreditAppRequestTable_NewInterestTypeGUID = creditAppRequestTable.InterestTypeGUID,
                        CreditAppRequestTable_NewInterestAdjustmentPct = creditAppRequestTable.InterestAdjustment,
						CreditAppRequestTable_NewTotalInterestPct = (_newTotalInterestPct != null) ? _newTotalInterestPct.CreditAppRequestTable_NewTotalInterestPct: creditAppRequestTable.InterestAdjustment,
						CreditAppRequestTable_MaxRetentionAmount = creditAppRequestTable.MaxRetentionAmount,
						CreditAppRequestTable_MaxRetentionPct = creditAppRequestTableAmend.OriginalMaxRetentionPct,
						#endregion Amend Rate / ActiveAmendCustomerCreditLimit

						ProcessInstanceId = creditAppRequestTable.ProcessInstanceId,
						CreditAppRequestTable_CACondition = creditAppRequestTable.CACondition,
						CreditAppRequestTable_SigningCondition = creditAppRequestTable.SigningCondition,
						CreditAppRequestTable_FinancialCreditCheckedDate = creditAppRequestTable.FinancialCreditCheckedDate,
						CreditAppRequestTable_FinancialCheckedBy = creditAppRequestTable.FinancialCheckedBy,
						CreditAppRequestTable_NCBCheckedDate = creditAppRequestTable.NCBCheckedDate,
						CreditAppRequestTable_NCBCheckedBy = creditAppRequestTable.NCBCheckedBy,
						CreditAppRequestTable_BlacklistStatusGUID = creditAppRequestTable.BlacklistStatusGUID,
						CreditAppRequestTable_KYCGUID = creditAppRequestTable.KYCGUID,
						CreditAppRequestTable_CreditScoringGUID = creditAppRequestTable.CreditScoringGUID,
						CreditAppTable_InactiveDate = creditAppTable.InactiveDate,
					
						RowVersion = creditAppRequestTableAmend.RowVersion,
					});
		}
		public CreditAppRequestTableAmendItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery(guid)
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<CreditAppRequestTableAmendItemViewMap, CreditAppRequestTableAmendItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public CreditAppRequestTableAmend CreateCreditAppRequestTableAmend(CreditAppRequestTableAmend creditAppRequestTableAmend)
		{
			try
			{
				creditAppRequestTableAmend.CreditAppRequestTableAmendGUID = Guid.NewGuid();
				base.Add(creditAppRequestTableAmend);
				return creditAppRequestTableAmend;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCreditAppRequestTableAmendVoid(CreditAppRequestTableAmend creditAppRequestTableAmend)
		{
			try
			{
				CreateCreditAppRequestTableAmend(creditAppRequestTableAmend);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditAppRequestTableAmend UpdateCreditAppRequestTableAmend(CreditAppRequestTableAmend dbCreditAppRequestTableAmend, CreditAppRequestTableAmend inputCreditAppRequestTableAmend, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCreditAppRequestTableAmend = dbCreditAppRequestTableAmend.MapUpdateValues<CreditAppRequestTableAmend>(inputCreditAppRequestTableAmend);
				base.Update(dbCreditAppRequestTableAmend);
				return dbCreditAppRequestTableAmend;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCreditAppRequestTableAmendVoid(CreditAppRequestTableAmend dbCreditAppRequestTableAmend, CreditAppRequestTableAmend inputCreditAppRequestTableAmend, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCreditAppRequestTableAmend = dbCreditAppRequestTableAmend.MapUpdateValues<CreditAppRequestTableAmend>(inputCreditAppRequestTableAmend, skipUpdateFields);
				base.Update(dbCreditAppRequestTableAmend);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public CreditAppRequestTableAmend GetCreditAppRequestTableAmendByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CreditAppRequestTableAmendGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditAppRequestTableAmend GetCreditAppRequestTableAmendByCreditAppRequestTable(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CreditAppRequestTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

