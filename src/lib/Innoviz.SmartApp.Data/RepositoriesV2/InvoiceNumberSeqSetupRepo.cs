using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IInvoiceNumberSeqSetupRepo
	{
		#region DropDown
		IEnumerable<SelectItem<InvoiceNumberSeqSetupItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<InvoiceNumberSeqSetupListView> GetListvw(SearchParameter search);
		InvoiceNumberSeqSetupItemView GetByIdvw(Guid id);
		InvoiceNumberSeqSetup Find(params object[] keyValues);
		InvoiceNumberSeqSetup GetInvoiceNumberSeqSetupByIdNoTracking(Guid guid);
		InvoiceNumberSeqSetup CreateInvoiceNumberSeqSetup(InvoiceNumberSeqSetup invoiceNumberSeqSetup);
		void CreateInvoiceNumberSeqSetupVoid(InvoiceNumberSeqSetup invoiceNumberSeqSetup);
		InvoiceNumberSeqSetup UpdateInvoiceNumberSeqSetup(InvoiceNumberSeqSetup dbInvoiceNumberSeqSetup, InvoiceNumberSeqSetup inputInvoiceNumberSeqSetup, List<string> skipUpdateFields = null);
		void UpdateInvoiceNumberSeqSetupVoid(InvoiceNumberSeqSetup dbInvoiceNumberSeqSetup, InvoiceNumberSeqSetup inputInvoiceNumberSeqSetup, List<string> skipUpdateFields = null);
		void Remove(InvoiceNumberSeqSetup item);
		InvoiceNumberSeqSetup GetInvoiceNumberSeqSetupByIdNoTrackingByAccessLevel(Guid guid);
		#region function
		InvoiceNumberSeqSetup GetInvoiceNumberSeqSetupByBranchAndProductTypeAndInvoiceType(Guid branchGuid, int productType, Guid invoiceTypeGuid);
		#endregion
	}
    public class InvoiceNumberSeqSetupRepo : CompanyBaseRepository<InvoiceNumberSeqSetup>, IInvoiceNumberSeqSetupRepo
	{
		public InvoiceNumberSeqSetupRepo(SmartAppDbContext context) : base(context) { }
		public InvoiceNumberSeqSetupRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public InvoiceNumberSeqSetup GetInvoiceNumberSeqSetupByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.InvoiceNumberSeqSetupGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<InvoiceNumberSeqSetupItemViewMap> GetDropDownQuery()
		{
			return (from invoiceNumberSeqSetup in Entity
					select new InvoiceNumberSeqSetupItemViewMap
					{
						CompanyGUID = invoiceNumberSeqSetup.CompanyGUID,
						Owner = invoiceNumberSeqSetup.Owner,
						OwnerBusinessUnitGUID = invoiceNumberSeqSetup.OwnerBusinessUnitGUID,
						InvoiceNumberSeqSetupGUID = invoiceNumberSeqSetup.InvoiceNumberSeqSetupGUID
					});
		}
		public IEnumerable<SelectItem<InvoiceNumberSeqSetupItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<InvoiceNumberSeqSetup>(search, db.GetAvailableBranch(), SysParm.CompanyGUID, SysParm.BranchGUID);
				var invoiceNumberSeqSetup = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<InvoiceNumberSeqSetupItemViewMap, InvoiceNumberSeqSetupItemView>().ToDropDownItem(search.ExcludeRowData);


				return invoiceNumberSeqSetup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<InvoiceNumberSeqSetupListViewMap> GetListQuery()
		{
			return (from invoiceNumberSeqSetup in Entity
					join branch in db.Set<Branch>()
					on invoiceNumberSeqSetup.BranchGUID equals branch.BranchGUID into ljInvoiceNumberSeqSetupBranch
					from branch in ljInvoiceNumberSeqSetupBranch.DefaultIfEmpty()
					join invoiceType in db.Set<InvoiceType>()
					on invoiceNumberSeqSetup.InvoiceTypeGUID equals invoiceType.InvoiceTypeGUID into ljInvoiceNumberSeqSetupInvoiceType
					from invoiceType in ljInvoiceNumberSeqSetupInvoiceType.DefaultIfEmpty()
					join numberSeqTable in db.Set<NumberSeqTable>()
					on invoiceNumberSeqSetup.NumberSeqTableGUID equals numberSeqTable.NumberSeqTableGUID into ljInvoiceNumberSeqSetupNumberSeqTable
					from numberSeqTable in ljInvoiceNumberSeqSetupNumberSeqTable.DefaultIfEmpty()
					select new InvoiceNumberSeqSetupListViewMap
				{
						CompanyGUID = invoiceNumberSeqSetup.CompanyGUID,
						Owner = invoiceNumberSeqSetup.Owner,
						BranchGUID = invoiceNumberSeqSetup.BranchGUID,
						Branch_BranchId = branch.BranchId,
						Branch_Values = SmartAppUtil.GetDropDownLabel(branch.BranchId,branch.Name),
						OwnerBusinessUnitGUID = invoiceNumberSeqSetup.OwnerBusinessUnitGUID,
						InvoiceNumberSeqSetupGUID = invoiceNumberSeqSetup.InvoiceNumberSeqSetupGUID,
						InvoiceType_InvoiceTypeId = invoiceType.InvoiceTypeId,
						InvoiceType_Values = SmartAppUtil.GetDropDownLabel(invoiceType.InvoiceTypeId,invoiceType.Description),
						NumberSeqTableGUID = invoiceNumberSeqSetup.NumberSeqTableGUID,
						NumberSeqTable_NumberSeqCode = numberSeqTable.NumberSeqCode,
						NumberSeqTable_Values = SmartAppUtil.GetDropDownLabel(numberSeqTable.NumberSeqCode,numberSeqTable.Description),
						ProductType = invoiceNumberSeqSetup.ProductType,
						InvoiceTypeGUID = invoiceNumberSeqSetup.InvoiceTypeGUID,
					});
		}
		public SearchResult<InvoiceNumberSeqSetupListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<InvoiceNumberSeqSetupListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<InvoiceNumberSeqSetup>(search, db.GetAvailableBranch(), SysParm.CompanyGUID, SysParm.BranchGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<InvoiceNumberSeqSetupListViewMap, InvoiceNumberSeqSetupListView>();
				result = list.SetSearchResult<InvoiceNumberSeqSetupListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<InvoiceNumberSeqSetupItemViewMap> GetItemQuery()
		{
			return (from invoiceNumberSeqSetup in Entity
					join company in db.Set<Company>()
					on invoiceNumberSeqSetup.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on invoiceNumberSeqSetup.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljInvoiceNumberSeqSetupOwnerBU
					from ownerBU in ljInvoiceNumberSeqSetupOwnerBU.DefaultIfEmpty()
					select new InvoiceNumberSeqSetupItemViewMap
					{
						CompanyGUID = invoiceNumberSeqSetup.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = invoiceNumberSeqSetup.Owner,
						OwnerBusinessUnitGUID = invoiceNumberSeqSetup.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = invoiceNumberSeqSetup.CreatedBy,
						CreatedDateTime = invoiceNumberSeqSetup.CreatedDateTime,
						ModifiedBy = invoiceNumberSeqSetup.ModifiedBy,
						ModifiedDateTime = invoiceNumberSeqSetup.ModifiedDateTime,
						InvoiceNumberSeqSetupGUID = invoiceNumberSeqSetup.InvoiceNumberSeqSetupGUID,
						InvoiceTypeGUID = invoiceNumberSeqSetup.InvoiceTypeGUID,
						ProductType = invoiceNumberSeqSetup.ProductType,
						NumberSeqTableGUID = invoiceNumberSeqSetup.NumberSeqTableGUID,
						BranchGUID = invoiceNumberSeqSetup.BranchGUID,
					
						RowVersion = invoiceNumberSeqSetup.RowVersion,
					});
		}
		public InvoiceNumberSeqSetupItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<InvoiceNumberSeqSetupItemViewMap, InvoiceNumberSeqSetupItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public InvoiceNumberSeqSetup CreateInvoiceNumberSeqSetup(InvoiceNumberSeqSetup invoiceNumberSeqSetup)
		{
			try
			{
				invoiceNumberSeqSetup.InvoiceNumberSeqSetupGUID = Guid.NewGuid();
				base.Add(invoiceNumberSeqSetup);
				return invoiceNumberSeqSetup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateInvoiceNumberSeqSetupVoid(InvoiceNumberSeqSetup invoiceNumberSeqSetup)
		{
			try
			{
				CreateInvoiceNumberSeqSetup(invoiceNumberSeqSetup);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public InvoiceNumberSeqSetup UpdateInvoiceNumberSeqSetup(InvoiceNumberSeqSetup dbInvoiceNumberSeqSetup, InvoiceNumberSeqSetup inputInvoiceNumberSeqSetup, List<string> skipUpdateFields = null)
		{
			try
			{
				dbInvoiceNumberSeqSetup = dbInvoiceNumberSeqSetup.MapUpdateValues<InvoiceNumberSeqSetup>(inputInvoiceNumberSeqSetup);
				base.Update(dbInvoiceNumberSeqSetup);
				return dbInvoiceNumberSeqSetup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateInvoiceNumberSeqSetupVoid(InvoiceNumberSeqSetup dbInvoiceNumberSeqSetup, InvoiceNumberSeqSetup inputInvoiceNumberSeqSetup, List<string> skipUpdateFields = null)
		{
			try
			{
				dbInvoiceNumberSeqSetup = dbInvoiceNumberSeqSetup.MapUpdateValues<InvoiceNumberSeqSetup>(inputInvoiceNumberSeqSetup, skipUpdateFields);
				base.Update(dbInvoiceNumberSeqSetup);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public InvoiceNumberSeqSetup GetInvoiceNumberSeqSetupByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.InvoiceNumberSeqSetupGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        #region function
		public InvoiceNumberSeqSetup GetInvoiceNumberSeqSetupByBranchAndProductTypeAndInvoiceType(Guid branchGuid,int productType,Guid invoiceTypeGuid)
        {
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				IBranchRepo branchRepo = new BranchRepo(db);
				IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
				var result = Entity.Where(w => w.BranchGUID == branchGuid &&w.ProductType == productType && w.InvoiceTypeGUID == invoiceTypeGuid)
					.AsNoTracking()
					.FirstOrDefault();
				if(result == null)
                {
					Branch branch = branchRepo.GetBranchByIdNoTracking(branchGuid);
					InvoiceType invoiceType = invoiceTypeRepo.GetInvoiceTypeByIdNoTracking(invoiceTypeGuid);
					ex.AddData("ERROR.90056",new string[] { "LABEL.INVOICE_NUMBER_SEQ_SETUP", branch.BranchId,((ProductType)productType).ToString(),invoiceType.InvoiceTypeId });
					throw SmartAppUtil.AddStackTrace(ex);
				}
                else
                {
					return result;
				}				
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}

