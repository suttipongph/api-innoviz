using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICalendarGroupRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CalendarGroupItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<CalendarGroupListView> GetListvw(SearchParameter search);
		CalendarGroupItemView GetByIdvw(Guid id);
		CalendarGroup Find(params object[] keyValues);
		CalendarGroup GetCalendarGroupByIdNoTracking(Guid guid);
		CalendarGroup CreateCalendarGroup(CalendarGroup calendarGroup);
		void CreateCalendarGroupVoid(CalendarGroup calendarGroup);
		CalendarGroup UpdateCalendarGroup(CalendarGroup dbCalendarGroup, CalendarGroup inputCalendarGroup, List<string> skipUpdateFields = null);
		void UpdateCalendarGroupVoid(CalendarGroup dbCalendarGroup, CalendarGroup inputCalendarGroup, List<string> skipUpdateFields = null);
		void Remove(CalendarGroup item);
		CalendarGroup GetCalendarGroupByIdNoTrackingByAccessLevel(Guid guid);
	}
	public class CalendarGroupRepo : CompanyBaseRepository<CalendarGroup>, ICalendarGroupRepo
	{
		public CalendarGroupRepo(SmartAppDbContext context) : base(context) { }
		public CalendarGroupRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CalendarGroup GetCalendarGroupByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CalendarGroupGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CalendarGroupItemViewMap> GetDropDownQuery()
		{
			return (from calendarGroup in Entity
					select new CalendarGroupItemViewMap
					{
						CompanyGUID = calendarGroup.CompanyGUID,
						Owner = calendarGroup.Owner,
						OwnerBusinessUnitGUID = calendarGroup.OwnerBusinessUnitGUID,
						CalendarGroupGUID = calendarGroup.CalendarGroupGUID,
						CalendarGroupId = calendarGroup.CalendarGroupId,
						Description = calendarGroup.Description,
						WorkingDay = calendarGroup.WorkingDay
					});
		}
		public IEnumerable<SelectItem<CalendarGroupItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CalendarGroup>(search, SysParm.CompanyGUID);
				var calendarGroup = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CalendarGroupItemViewMap, CalendarGroupItemView>().ToDropDownItem(search.ExcludeRowData);


				return calendarGroup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CalendarGroupListViewMap> GetListQuery()
		{
			return (from calendarGroup in Entity
				select new CalendarGroupListViewMap
				{
						CompanyGUID = calendarGroup.CompanyGUID,
						Owner = calendarGroup.Owner,
						OwnerBusinessUnitGUID = calendarGroup.OwnerBusinessUnitGUID,
						CalendarGroupGUID = calendarGroup.CalendarGroupGUID,
						CalendarGroupId = calendarGroup.CalendarGroupId,
						Description = calendarGroup.Description,
						WorkingDay = calendarGroup.WorkingDay,
				});
		}
		public SearchResult<CalendarGroupListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CalendarGroupListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CalendarGroup>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CalendarGroupListViewMap, CalendarGroupListView>();
				result = list.SetSearchResult<CalendarGroupListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CalendarGroupItemViewMap> GetItemQuery()
		{
			return (from calendarGroup in Entity
					join company in db.Set<Company>()
					on calendarGroup.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on calendarGroup.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCalendarGroupOwnerBU
					from ownerBU in ljCalendarGroupOwnerBU.DefaultIfEmpty()
					select new CalendarGroupItemViewMap
					{
						CompanyGUID = calendarGroup.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = calendarGroup.Owner,
						OwnerBusinessUnitGUID = calendarGroup.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = calendarGroup.CreatedBy,
						CreatedDateTime = calendarGroup.CreatedDateTime,
						ModifiedBy = calendarGroup.ModifiedBy,
						ModifiedDateTime = calendarGroup.ModifiedDateTime,
						CalendarGroupGUID = calendarGroup.CalendarGroupGUID,
						CalendarGroupId = calendarGroup.CalendarGroupId,
						Description = calendarGroup.Description,
						WorkingDay = calendarGroup.WorkingDay,
					
						RowVersion = calendarGroup.RowVersion,
					});
		}
		public CalendarGroupItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<CalendarGroupItemViewMap, CalendarGroupItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public CalendarGroup CreateCalendarGroup(CalendarGroup calendarGroup)
		{
			try
			{
				calendarGroup.CalendarGroupGUID = Guid.NewGuid();
				base.Add(calendarGroup);
				return calendarGroup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCalendarGroupVoid(CalendarGroup calendarGroup)
		{
			try
			{
				CreateCalendarGroup(calendarGroup);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CalendarGroup UpdateCalendarGroup(CalendarGroup dbCalendarGroup, CalendarGroup inputCalendarGroup, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCalendarGroup = dbCalendarGroup.MapUpdateValues<CalendarGroup>(inputCalendarGroup);
				base.Update(dbCalendarGroup);
				return dbCalendarGroup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCalendarGroupVoid(CalendarGroup dbCalendarGroup, CalendarGroup inputCalendarGroup, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCalendarGroup = dbCalendarGroup.MapUpdateValues<CalendarGroup>(inputCalendarGroup, skipUpdateFields);
				base.Update(dbCalendarGroup);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public CalendarGroup GetCalendarGroupByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CalendarGroupGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

