using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IServiceFeeCondTemplateLineRepo
	{
		#region DropDown
		IEnumerable<SelectItem<ServiceFeeCondTemplateLineItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<ServiceFeeCondTemplateLineListView> GetListvw(SearchParameter search);
		ServiceFeeCondTemplateLineItemView GetByIdvw(Guid id);
		ServiceFeeCondTemplateLine Find(params object[] keyValues);
		ServiceFeeCondTemplateLine GetServiceFeeCondTemplateLineByIdNoTracking(Guid guid);
		ServiceFeeCondTemplateLine CreateServiceFeeCondTemplateLine(ServiceFeeCondTemplateLine serviceFeeCondTemplateLine);
		void CreateServiceFeeCondTemplateLineVoid(ServiceFeeCondTemplateLine serviceFeeCondTemplateLine);
		ServiceFeeCondTemplateLine UpdateServiceFeeCondTemplateLine(ServiceFeeCondTemplateLine dbServiceFeeCondTemplateLine, ServiceFeeCondTemplateLine inputServiceFeeCondTemplateLine, List<string> skipUpdateFields = null);
		void UpdateServiceFeeCondTemplateLineVoid(ServiceFeeCondTemplateLine dbServiceFeeCondTemplateLine, ServiceFeeCondTemplateLine inputServiceFeeCondTemplateLine, List<string> skipUpdateFields = null);
		void Remove(ServiceFeeCondTemplateLine item);
		List<ServiceFeeCondTemplateLine> GetServiceFeeCondTemplateLineByServiceFeeCondTemplateTable(Guid serviceFeeCondTemplateTableGUID);
	}
	public class ServiceFeeCondTemplateLineRepo : CompanyBaseRepository<ServiceFeeCondTemplateLine>, IServiceFeeCondTemplateLineRepo
	{
		public ServiceFeeCondTemplateLineRepo(SmartAppDbContext context) : base(context) { }
		public ServiceFeeCondTemplateLineRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ServiceFeeCondTemplateLine GetServiceFeeCondTemplateLineByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ServiceFeeCondTemplateLineGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<ServiceFeeCondTemplateLineItemViewMap> GetDropDownQuery()
		{
			return (from serviceFeeCondTemplateLine in Entity
					select new ServiceFeeCondTemplateLineItemViewMap
					{
						CompanyGUID = serviceFeeCondTemplateLine.CompanyGUID,
						Owner = serviceFeeCondTemplateLine.Owner,
						OwnerBusinessUnitGUID = serviceFeeCondTemplateLine.OwnerBusinessUnitGUID,
						ServiceFeeCondTemplateLineGUID = serviceFeeCondTemplateLine.ServiceFeeCondTemplateLineGUID
					});
		}
		public IEnumerable<SelectItem<ServiceFeeCondTemplateLineItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<ServiceFeeCondTemplateLine>(search, SysParm.CompanyGUID);
				var serviceFeeCondTemplateLine = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<ServiceFeeCondTemplateLineItemViewMap, ServiceFeeCondTemplateLineItemView>().ToDropDownItem(search.ExcludeRowData);


				return serviceFeeCondTemplateLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<ServiceFeeCondTemplateLineListViewMap> GetListQuery()
		{
			return (from serviceFeeCondTemplateLine in Entity
					join invoiceRevenueType in db.Set<InvoiceRevenueType>()
					on serviceFeeCondTemplateLine.InvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID into ljServiceFeeCondTemplateinvoiceRevenueType
					from invoiceRevenueType in ljServiceFeeCondTemplateinvoiceRevenueType.DefaultIfEmpty()
					select new ServiceFeeCondTemplateLineListViewMap
				{
						CompanyGUID = serviceFeeCondTemplateLine.CompanyGUID,
						Owner = serviceFeeCondTemplateLine.Owner,
						OwnerBusinessUnitGUID = serviceFeeCondTemplateLine.OwnerBusinessUnitGUID,
						ServiceFeeCondTemplateLineGUID = serviceFeeCondTemplateLine.ServiceFeeCondTemplateLineGUID,
						Ordering = serviceFeeCondTemplateLine.Ordering,
						InvoiceRevenueTypeGUID = serviceFeeCondTemplateLine.InvoiceRevenueTypeGUID,
						Description = serviceFeeCondTemplateLine.Description,
						AmountBeforeTax = serviceFeeCondTemplateLine.AmountBeforeTax,
						InvoiceRevenueType_Values = SmartAppUtil.GetDropDownLabel(invoiceRevenueType.RevenueTypeId, invoiceRevenueType.Description),
						ServiceFeeCondTemplateTableGUID = serviceFeeCondTemplateLine.ServiceFeeCondTemplateTableGUID
					});
		}
		public SearchResult<ServiceFeeCondTemplateLineListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<ServiceFeeCondTemplateLineListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<ServiceFeeCondTemplateLine>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<ServiceFeeCondTemplateLineListViewMap, ServiceFeeCondTemplateLineListView>();
				result = list.SetSearchResult<ServiceFeeCondTemplateLineListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<ServiceFeeCondTemplateLineItemViewMap> GetItemQuery()
		{
			return (from serviceFeeCondTemplateLine in Entity
					join company in db.Set<Company>()
					on serviceFeeCondTemplateLine.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on serviceFeeCondTemplateLine.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljServiceFeeCondTemplateLineOwnerBU
					from ownerBU in ljServiceFeeCondTemplateLineOwnerBU.DefaultIfEmpty()
					join serviceFeeCondTemplateTable in db.Set<ServiceFeeCondTemplateTable>()
					on serviceFeeCondTemplateLine.ServiceFeeCondTemplateTableGUID equals serviceFeeCondTemplateTable.ServiceFeeCondTemplateTableGUID into ljServiceFeeCondTemplateTable
					from serviceFeeCondTemplateTable in ljServiceFeeCondTemplateTable.DefaultIfEmpty()
					select new ServiceFeeCondTemplateLineItemViewMap
					{
						CompanyGUID = serviceFeeCondTemplateLine.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = serviceFeeCondTemplateLine.Owner,
						OwnerBusinessUnitGUID = serviceFeeCondTemplateLine.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = serviceFeeCondTemplateLine.CreatedBy,
						CreatedDateTime = serviceFeeCondTemplateLine.CreatedDateTime,
						ModifiedBy = serviceFeeCondTemplateLine.ModifiedBy,
						ModifiedDateTime = serviceFeeCondTemplateLine.ModifiedDateTime,
						ServiceFeeCondTemplateLineGUID = serviceFeeCondTemplateLine.ServiceFeeCondTemplateLineGUID,
						AmountBeforeTax = serviceFeeCondTemplateLine.AmountBeforeTax,
						Description = serviceFeeCondTemplateLine.Description,
						InvoiceRevenueTypeGUID = serviceFeeCondTemplateLine.InvoiceRevenueTypeGUID,
						Ordering = serviceFeeCondTemplateLine.Ordering,
						ServiceFeeCondTemplateTableGUID = serviceFeeCondTemplateLine.ServiceFeeCondTemplateTableGUID,
						ProductType = serviceFeeCondTemplateTable.ProductType,
					
						RowVersion = serviceFeeCondTemplateLine.RowVersion,
					});
		}
		public ServiceFeeCondTemplateLineItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<ServiceFeeCondTemplateLineItemViewMap, ServiceFeeCondTemplateLineItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public ServiceFeeCondTemplateLine CreateServiceFeeCondTemplateLine(ServiceFeeCondTemplateLine serviceFeeCondTemplateLine)
		{
			try
			{
				serviceFeeCondTemplateLine.ServiceFeeCondTemplateLineGUID = Guid.NewGuid();
				base.Add(serviceFeeCondTemplateLine);
				return serviceFeeCondTemplateLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateServiceFeeCondTemplateLineVoid(ServiceFeeCondTemplateLine serviceFeeCondTemplateLine)
		{
			try
			{
				CreateServiceFeeCondTemplateLine(serviceFeeCondTemplateLine);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ServiceFeeCondTemplateLine UpdateServiceFeeCondTemplateLine(ServiceFeeCondTemplateLine dbServiceFeeCondTemplateLine, ServiceFeeCondTemplateLine inputServiceFeeCondTemplateLine, List<string> skipUpdateFields = null)
		{
			try
			{
				dbServiceFeeCondTemplateLine = dbServiceFeeCondTemplateLine.MapUpdateValues<ServiceFeeCondTemplateLine>(inputServiceFeeCondTemplateLine);
				base.Update(dbServiceFeeCondTemplateLine);
				return dbServiceFeeCondTemplateLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateServiceFeeCondTemplateLineVoid(ServiceFeeCondTemplateLine dbServiceFeeCondTemplateLine, ServiceFeeCondTemplateLine inputServiceFeeCondTemplateLine, List<string> skipUpdateFields = null)
		{
			try
			{
				dbServiceFeeCondTemplateLine = dbServiceFeeCondTemplateLine.MapUpdateValues<ServiceFeeCondTemplateLine>(inputServiceFeeCondTemplateLine, skipUpdateFields);
				base.Update(dbServiceFeeCondTemplateLine);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public List<ServiceFeeCondTemplateLine> GetServiceFeeCondTemplateLineByServiceFeeCondTemplateTable(Guid serviceFeeCondTemplateTableGUID)
		{
			try
			{
				var result = Entity.Where(item => item.ServiceFeeCondTemplateTableGUID == serviceFeeCondTemplateTableGUID).AsNoTracking().ToList();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

