using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICollectionGroupRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CollectionGroupItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<CollectionGroupListView> GetListvw(SearchParameter search);
		CollectionGroupItemView GetByIdvw(Guid id);
		CollectionGroup Find(params object[] keyValues);
		CollectionGroup GetCollectionGroupByIdNoTracking(Guid guid);
		CollectionGroup CreateCollectionGroup(CollectionGroup collectionGroup);
		void CreateCollectionGroupVoid(CollectionGroup collectionGroup);
		CollectionGroup UpdateCollectionGroup(CollectionGroup dbCollectionGroup, CollectionGroup inputCollectionGroup, List<string> skipUpdateFields = null);
		void UpdateCollectionGroupVoid(CollectionGroup dbCollectionGroup, CollectionGroup inputCollectionGroup, List<string> skipUpdateFields = null);
		void Remove(CollectionGroup item);
	}
	public class CollectionGroupRepo : CompanyBaseRepository<CollectionGroup>, ICollectionGroupRepo
	{
		public CollectionGroupRepo(SmartAppDbContext context) : base(context) { }
		public CollectionGroupRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CollectionGroup GetCollectionGroupByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CollectionGroupGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CollectionGroupItemViewMap> GetDropDownQuery()
		{
			return (from collectionGroup in Entity
					select new CollectionGroupItemViewMap
					{
						CompanyGUID = collectionGroup.CompanyGUID,
						Owner = collectionGroup.Owner,
						OwnerBusinessUnitGUID = collectionGroup.OwnerBusinessUnitGUID,
						CollectionGroupGUID = collectionGroup.CollectionGroupGUID,
						CollectionGroupId = collectionGroup.CollectionGroupId,
						Description = collectionGroup.Description
					});
		}
		public IEnumerable<SelectItem<CollectionGroupItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CollectionGroup>(search, SysParm.CompanyGUID);
				var collectionGroup = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CollectionGroupItemViewMap, CollectionGroupItemView>().ToDropDownItem(search.ExcludeRowData);


				return collectionGroup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CollectionGroupListViewMap> GetListQuery()
		{
			return (from collectionGroup in Entity
				select new CollectionGroupListViewMap
				{
						CompanyGUID = collectionGroup.CompanyGUID,
						Owner = collectionGroup.Owner,
						OwnerBusinessUnitGUID = collectionGroup.OwnerBusinessUnitGUID,
						CollectionGroupGUID = collectionGroup.CollectionGroupGUID,
				});
		}
		public SearchResult<CollectionGroupListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CollectionGroupListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CollectionGroup>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CollectionGroupListViewMap, CollectionGroupListView>();
				result = list.SetSearchResult<CollectionGroupListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CollectionGroupItemViewMap> GetItemQuery()
		{
			return (from collectionGroup in Entity
					join company in db.Set<Company>()
					on collectionGroup.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on collectionGroup.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCollectionGroupOwnerBU
					from ownerBU in ljCollectionGroupOwnerBU.DefaultIfEmpty()
					select new CollectionGroupItemViewMap
					{
						CompanyGUID = collectionGroup.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = collectionGroup.Owner,
						OwnerBusinessUnitGUID = collectionGroup.OwnerBusinessUnitGUID,
						CreatedBy = collectionGroup.CreatedBy,
						CreatedDateTime = collectionGroup.CreatedDateTime,
						ModifiedBy = collectionGroup.ModifiedBy,
						ModifiedDateTime = collectionGroup.ModifiedDateTime,
						CollectionGroupGUID = collectionGroup.CollectionGroupGUID,
						AgreementBranchId = collectionGroup.AgreementBranchId,
						AgreementFromOverdueDay = collectionGroup.AgreementFromOverdueDay,
						AgreementLeaseTypeId = collectionGroup.AgreementLeaseTypeId,
						AgreementToOverdueDay = collectionGroup.AgreementToOverdueDay,
						CollectionGroupId = collectionGroup.CollectionGroupId,
						CustGroupId = collectionGroup.CustGroupId,
						CustTerritoryId = collectionGroup.CustTerritoryId,
						Description = collectionGroup.Description,
					
						RowVersion = collectionGroup.RowVersion,
					});
		}
		public CollectionGroupItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<CollectionGroupItemViewMap, CollectionGroupItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public CollectionGroup CreateCollectionGroup(CollectionGroup collectionGroup)
		{
			try
			{
				collectionGroup.CollectionGroupGUID = Guid.NewGuid();
				base.Add(collectionGroup);
				return collectionGroup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCollectionGroupVoid(CollectionGroup collectionGroup)
		{
			try
			{
				CreateCollectionGroup(collectionGroup);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CollectionGroup UpdateCollectionGroup(CollectionGroup dbCollectionGroup, CollectionGroup inputCollectionGroup, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCollectionGroup = dbCollectionGroup.MapUpdateValues<CollectionGroup>(inputCollectionGroup);
				base.Update(dbCollectionGroup);
				return dbCollectionGroup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCollectionGroupVoid(CollectionGroup dbCollectionGroup, CollectionGroup inputCollectionGroup, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCollectionGroup = dbCollectionGroup.MapUpdateValues<CollectionGroup>(inputCollectionGroup, skipUpdateFields);
				base.Update(dbCollectionGroup);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

