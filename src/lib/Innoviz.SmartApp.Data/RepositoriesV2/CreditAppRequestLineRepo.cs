using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface ICreditAppRequestLineRepo
    {
        #region DropDown
        IEnumerable<SelectItem<CreditAppRequestLineItemView>> GetDropDownItem(SearchParameter search);
        #endregion DropDown
        SearchResult<CreditAppRequestLineListView> GetListvw(SearchParameter search);
        CreditAppRequestLineItemView GetByIdvw(Guid id);
        CreditAppRequestLine Find(params object[] keyValues);
        CreditAppRequestLine GetCreditAppRequestLineByIdNoTracking(Guid guid);
        CreditAppRequestLine CreateCreditAppRequestLine(CreditAppRequestLine creditAppRequestLine);
        void CreateCreditAppRequestLineVoid(CreditAppRequestLine creditAppRequestLine);
        CreditAppRequestLine UpdateCreditAppRequestLine(CreditAppRequestLine dbCreditAppRequestLine, CreditAppRequestLine inputCreditAppRequestLine, List<string> skipUpdateFields = null);
        void UpdateCreditAppRequestLineVoid(CreditAppRequestLine dbCreditAppRequestLine, CreditAppRequestLine inputCreditAppRequestLine, List<string> skipUpdateFields = null);
        void Remove(CreditAppRequestLine item);
        IEnumerable<CreditAppRequestLine> GetCreditAppRequestLineByCreditAppRequestTable(Guid guid);
        CreditAppRequestLine GetCreditAppRequestLineByIdNoTrackingByAccessLevel(Guid guid);
        void ValidateAdd(CreditAppRequestLine item);
        void ValidateAdd(IEnumerable<CreditAppRequestLine> items);
        public QueryBuyerAmendCreditLimit GetBookmarkDocumentQueryBuyerAmendCreditLimit(Guid refGUID, Guid bookmarkDocumentTransGUID);
        #region Amend CA Line
        public CreditAppRequestLine CopyCreditAppRequestLineForCreateCreditAppRequestLineAmend(CreditAppRequestLineAmendItemView creditAppRequestLineAmendView, CreditAppRequestTable newCreditAppRequestTable);
        void ValidateAllowAddLine(CreditLimitType creditLimitType);
        #endregion Amend CA Line
    }
    public class CreditAppRequestLineRepo : CompanyBaseRepository<CreditAppRequestLine>, ICreditAppRequestLineRepo
    {
        public CreditAppRequestLineRepo(SmartAppDbContext context) : base(context) { }
        public CreditAppRequestLineRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public CreditAppRequestLine GetCreditAppRequestLineByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.CreditAppRequestLineGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppRequestLine GetCreditAppRequestTableByIdNoTrackingByAccessLevel(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.CreditAppRequestLineGUID == guid)
                                    .FilterByAccessLevel(SysParm.AccessLevel)
                                    .AsNoTracking()
                                    .FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        private IQueryable<CreditAppRequestLineItemViewMap> GetDropDownQuery()
        {
            return (from creditAppRequestLine in Entity
                    select new CreditAppRequestLineItemViewMap
                    {
                        CompanyGUID = creditAppRequestLine.CompanyGUID,
                        Owner = creditAppRequestLine.Owner,
                        OwnerBusinessUnitGUID = creditAppRequestLine.OwnerBusinessUnitGUID,
                        CreditAppRequestLineGUID = creditAppRequestLine.CreditAppRequestLineGUID,
                        LineNum = creditAppRequestLine.LineNum
                    });
        }
        public IEnumerable<SelectItem<CreditAppRequestLineItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<CreditAppRequestLine>(search, SysParm.CompanyGUID);
                var creditAppRequestLine = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CreditAppRequestLineItemViewMap, CreditAppRequestLineItemView>().ToDropDownItem(search.ExcludeRowData);


                return creditAppRequestLine;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #region GetListvw
        private IQueryable<CreditAppRequestLineListViewMap> GetListQuery()
        {
            return (from creditAppRequestLine in Entity
                    join buyerTable in db.Set<BuyerTable>() on creditAppRequestLine.BuyerTableGUID equals buyerTable.BuyerTableGUID
                    join businessSegment in db.Set<BusinessSegment>() on buyerTable.BusinessSegmentGUID equals businessSegment.BusinessSegmentGUID
                    select new CreditAppRequestLineListViewMap
                    {
                        CompanyGUID = creditAppRequestLine.CompanyGUID,
                        Owner = creditAppRequestLine.Owner,
                        OwnerBusinessUnitGUID = creditAppRequestLine.OwnerBusinessUnitGUID,
                        CreditAppRequestLineGUID = creditAppRequestLine.CreditAppRequestLineGUID,
                        LineNum = creditAppRequestLine.LineNum,
                        CreditLimitLineRequest = creditAppRequestLine.CreditLimitLineRequest,
                        ApprovedCreditLimitLineRequest = creditAppRequestLine.ApprovedCreditLimitLineRequest,
                        ApprovalDecision = creditAppRequestLine.ApprovalDecision,
                        CreditapprequesttableGUID = creditAppRequestLine.CreditAppRequestTableGUID,
                        BuyerTableGUID = creditAppRequestLine.BuyerTableGUID,
                        BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
                        BuyerTable_BuyerId = buyerTable.BuyerId,
                        BusinessSegmentGUID = businessSegment.BusinessSegmentGUID,
                        BusinessSegment_Values = SmartAppUtil.GetDropDownLabel(businessSegment.BusinessSegmentId, businessSegment.Description),
                        BusinessSegment_BusinessSegmentId = businessSegment.BusinessSegmentId,
                    });
        }
        public SearchResult<CreditAppRequestLineListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<CreditAppRequestLineListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<CreditAppRequestLine>(search, SysParm.CompanyGUID);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<CreditAppRequestLineListViewMap, CreditAppRequestLineListView>();
                result = list.SetSearchResult<CreditAppRequestLineListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<CreditAppRequestLineItemViewMap> GetItemQuery()
        {
            var address = db.Set<AddressTrans>().Where(w => w.RefType == (int)RefType.Buyer);
            var addressLabel = (from creditAppRequestLine in Entity
                                join billingAddress in address on creditAppRequestLine.BillingAddressGUID equals billingAddress.AddressTransGUID into ljCreditAppRequestLineBillingAddress
                                from billingAddress in ljCreditAppRequestLineBillingAddress.DefaultIfEmpty()
                                join invoiceAddress in address on creditAppRequestLine.InvoiceAddressGUID equals invoiceAddress.AddressTransGUID into ljCreditAppRequestLineInvoiceAddress
                                from invoiceAddress in ljCreditAppRequestLineInvoiceAddress.DefaultIfEmpty()
                                join mailingReceiptAddress in address on creditAppRequestLine.MailingReceiptAddressGUID equals mailingReceiptAddress.AddressTransGUID into ljCreditAppRequestLineMailingReceiptAddress
                                from mailingReceiptAddress in ljCreditAppRequestLineMailingReceiptAddress.DefaultIfEmpty()
                                join receiptAddress in address on creditAppRequestLine.ReceiptAddressGUID equals receiptAddress.AddressTransGUID into ljCreditAppRequestLineReceiptAddressGUID
                                from receiptAddress in ljCreditAppRequestLineReceiptAddressGUID.DefaultIfEmpty()

                                select new CreditAppRequestLineItemViewMap
                                {
                                    CreditAppRequestLineGUID = creditAppRequestLine.CreditAppRequestLineGUID,
                                    BuyerTableGUID = creditAppRequestLine.BuyerTableGUID,
                                    UnboundBillingAddress = (billingAddress != null) ? SmartAppUtil.GetAddressLabel(billingAddress.Address1, billingAddress.Address2) : null,
                                    UnboundInvoiceAddress = (invoiceAddress != null) ? SmartAppUtil.GetAddressLabel(invoiceAddress.Address1, invoiceAddress.Address2) : null,
                                    UnboundMailingReceiptAddress = (mailingReceiptAddress != null) ? SmartAppUtil.GetAddressLabel(mailingReceiptAddress.Address1, mailingReceiptAddress.Address2) : null,
                                    UnboundReceiptAddress = (receiptAddress != null) ? SmartAppUtil.GetAddressLabel(receiptAddress.Address1, receiptAddress.Address2) : null,
                                });
            return (from creditAppRequestLine in Entity
                    join company in db.Set<Company>()
                    on creditAppRequestLine.CompanyGUID equals company.CompanyGUID
                    join ownerBU in db.Set<BusinessUnit>()
                    on creditAppRequestLine.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCreditAppRequestLineOwnerBU
                    from ownerBU in ljCreditAppRequestLineOwnerBU.DefaultIfEmpty()
                    join creditAppRequestTable in db.Set<CreditAppRequestTable>() on creditAppRequestLine.CreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID
                    join documentStatus in db.Set<DocumentStatus>() on creditAppRequestTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
                    join buyerTable in db.Set<BuyerTable>() on creditAppRequestLine.BuyerTableGUID equals buyerTable.BuyerTableGUID
                    join businessSegment in db.Set<BusinessSegment>() on buyerTable.BusinessSegmentGUID equals businessSegment.BusinessSegmentGUID
                    join businessType in db.Set<BusinessType>() on buyerTable.BusinessTypeGUID equals businessType.BusinessTypeGUID into ljCreditAppRequestLineBusinessTypes
                    from businessType in ljCreditAppRequestLineBusinessTypes.DefaultIfEmpty()
                    join lineOfBusiness in db.Set<LineOfBusiness>() on buyerTable.LineOfBusinessGUID equals lineOfBusiness.LineOfBusinessGUID into ljCreditAppRequestLineLineOfBusiness
                    from lineOfBusiness in ljCreditAppRequestLineLineOfBusiness.DefaultIfEmpty()
                    join blacklistStatus in db.Set<BlacklistStatus>() on creditAppRequestLine.BlacklistStatusGUID equals blacklistStatus.BlacklistStatusGUID into ljCreditAppRequestLineBlacklistStatus
                    from blacklistStatus in ljCreditAppRequestLineBlacklistStatus.DefaultIfEmpty()
                    join addLabel in addressLabel on creditAppRequestLine.CreditAppRequestLineGUID equals addLabel.CreditAppRequestLineGUID into ljCreditAppRequestLineAddLabel
                    from addLabel in ljCreditAppRequestLineAddLabel.DefaultIfEmpty()
                    join creditAppTable in db.Set<CreditAppTable>() on creditAppRequestTable.RefCreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljCreditAppRequestLineCreditAppTable
                    from creditAppTable in ljCreditAppRequestLineCreditAppTable.DefaultIfEmpty()
                    join refCreditAppLine in db.Set<CreditAppLine>() on creditAppRequestLine.RefCreditAppLineGUID equals refCreditAppLine.CreditAppLineGUID into ljCreditAppRequestLineRefCreditAppLine
                    from refCreditAppLine in ljCreditAppRequestLineRefCreditAppLine.DefaultIfEmpty()
                    select new CreditAppRequestLineItemViewMap
                    {
                        CompanyGUID = creditAppRequestLine.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = creditAppRequestLine.Owner,
                        OwnerBusinessUnitGUID = creditAppRequestLine.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        CreatedBy = creditAppRequestLine.CreatedBy,
                        CreatedDateTime = creditAppRequestLine.CreatedDateTime,
                        ModifiedBy = creditAppRequestLine.ModifiedBy,
                        ModifiedDateTime = creditAppRequestLine.ModifiedDateTime,
                        CreditAppRequestLineGUID = creditAppRequestLine.CreditAppRequestLineGUID,
                        AcceptanceDocument = creditAppRequestLine.AcceptanceDocument,
                        AcceptanceDocumentDescription = creditAppRequestLine.AcceptanceDocumentDescription,
                        ApprovalDecision = creditAppRequestLine.ApprovalDecision,
                        ApprovedCreditLimitLineRequest = creditAppRequestLine.ApprovedCreditLimitLineRequest,
                        ApproverComment = creditAppRequestLine.ApproverComment,
                        AssignmentAgreementTableGUID = creditAppRequestLine.AssignmentAgreementTableGUID,
                        AssignmentMethodGUID = creditAppRequestLine.AssignmentMethodGUID,
                        AssignmentMethodRemark = creditAppRequestLine.AssignmentMethodRemark,
                        BillingAddressGUID = creditAppRequestLine.BillingAddressGUID,
                        BillingContactPersonGUID = creditAppRequestLine.BillingContactPersonGUID,
                        BillingDay = creditAppRequestLine.BillingDay,
                        BillingDescription = creditAppRequestLine.BillingDescription,
                        BillingRemark = creditAppRequestLine.BillingRemark,
                        BillingResponsibleByGUID = creditAppRequestLine.BillingResponsibleByGUID,
                        BlacklistStatusGUID = creditAppRequestLine.BlacklistStatusGUID,
                        BuyerCreditLimit = creditAppRequestLine.BuyerCreditLimit,
                        BuyerCreditTermGUID = creditAppRequestLine.BuyerCreditTermGUID,
                        BuyerProduct = creditAppRequestLine.BuyerProduct,
                        BuyerTableGUID = creditAppRequestLine.BuyerTableGUID,
                        CreditAppRequestTableGUID = creditAppRequestLine.CreditAppRequestTableGUID,
                        CreditComment = creditAppRequestLine.CreditComment,
                        CreditLimitLineRequest = creditAppRequestLine.CreditLimitLineRequest,
                        CreditScoringGUID = creditAppRequestLine.CreditScoringGUID,
                        CreditTermDescription = creditAppRequestLine.CreditTermDescription,
                        CustomerContactBuyerPeriod = creditAppRequestLine.CustomerContactBuyerPeriod,
                        CustomerRequest = creditAppRequestLine.CustomerRequest,
                        InsuranceCreditLimit = creditAppRequestLine.InsuranceCreditLimit,
                        InvoiceAddressGUID = creditAppRequestLine.InvoiceAddressGUID,
                        KYCSetupGUID = creditAppRequestLine.KYCSetupGUID,
                        LineNum = creditAppRequestLine.LineNum,
                        MailingReceiptAddressGUID = creditAppRequestLine.MailingReceiptAddressGUID,
                        MarketingComment = creditAppRequestLine.MarketingComment,
                        MaxPurchasePct = creditAppRequestLine.MaxPurchasePct,
                        MethodOfBilling = creditAppRequestLine.MethodOfBilling,
                        MethodOfPaymentGUID = creditAppRequestLine.MethodOfPaymentGUID,
                        PaymentCondition = creditAppRequestLine.PaymentCondition,
                        PurchaseFeeCalculateBase = creditAppRequestLine.PurchaseFeeCalculateBase,
                        PurchaseFeePct = creditAppRequestLine.PurchaseFeePct,
                        ReceiptAddressGUID = creditAppRequestLine.ReceiptAddressGUID,
                        ReceiptContactPersonGUID = creditAppRequestLine.ReceiptContactPersonGUID,
                        ReceiptDay = creditAppRequestLine.ReceiptDay,
                        ReceiptDescription = creditAppRequestLine.ReceiptDescription,
                        ReceiptRemark = creditAppRequestLine.ReceiptRemark,
                        UnboundBillingAddress = (addLabel != null) ? addLabel.UnboundBillingAddress : null,
                        UnboundInvoiceAddress = (addLabel != null) ? addLabel.UnboundInvoiceAddress : null,
                        UnboundMailingReceiptAddress = (addLabel != null) ? addLabel.UnboundMailingReceiptAddress : null,
                        UnboundReceiptAddress = (addLabel != null) ? addLabel.UnboundReceiptAddress : null,
                        CreditAppRequestTable_Values = SmartAppUtil.GetDropDownLabel(creditAppRequestTable.CreditAppRequestId, creditAppRequestTable.Description),
                        CreditAppRequestTable_StatusId = documentStatus.StatusId,
                        BlacklistStatus_Values = (blacklistStatus != null) ? SmartAppUtil.GetDropDownLabel(blacklistStatus.BlacklistStatusId, blacklistStatus.Description) : null,
                        BusinessSegment_Values = SmartAppUtil.GetDropDownLabel(businessSegment.BusinessSegmentId, businessSegment.Description),
                        BusinessType_Values = (businessType != null) ? SmartAppUtil.GetDropDownLabel(businessType.BusinessTypeId, businessType.Description) : null,
                        LineOfBusiness_Values = (lineOfBusiness != null) ? SmartAppUtil.GetDropDownLabel(lineOfBusiness.LineOfBusinessId, lineOfBusiness.Description) : null,
                        DateOfEstablish = buyerTable.DateOfEstablish,
                        CreditAppRequestTable_ProductType = creditAppRequestTable.ProductType,
                        //R02
                        RemainingCreditLoanRequest = creditAppRequestLine.RemainingCreditLoanRequest,
                        AllCustomerBuyerOutstanding = creditAppRequestLine.AllCustomerBuyerOutstanding,
                        CustomerBuyerOutstanding = creditAppRequestLine.CustomerBuyerOutstanding,
                        RefCreditAppLineGUID = creditAppRequestLine.RefCreditAppLineGUID,
                        RefCreditAppLine_Values = (refCreditAppLine != null) ? SmartAppUtil.GetDropDownLabel(refCreditAppLine.LineNum.ToString(), refCreditAppLine.ApprovedCreditLimitLine.ToString()) : null,
                        RefCreditAppTable_Values = (creditAppTable != null) ? SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description) : null,
                        LineCondition = creditAppRequestLine.LineCondition,
                    
                        RowVersion = creditAppRequestLine.RowVersion,
                    }); ;
        }
        public CreditAppRequestLineItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .ToMap<CreditAppRequestLineItemViewMap, CreditAppRequestLineItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public CreditAppRequestLine CreateCreditAppRequestLine(CreditAppRequestLine creditAppRequestLine)
        {
            try
            {
                creditAppRequestLine.CreditAppRequestLineGUID = Guid.NewGuid();
                base.Add(creditAppRequestLine);
                return creditAppRequestLine;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateCreditAppRequestLineVoid(CreditAppRequestLine creditAppRequestLine)
        {
            try
            {
                CreateCreditAppRequestLine(creditAppRequestLine);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppRequestLine UpdateCreditAppRequestLine(CreditAppRequestLine dbCreditAppRequestLine, CreditAppRequestLine inputCreditAppRequestLine, List<string> skipUpdateFields = null)
        {
            try
            {
                dbCreditAppRequestLine = dbCreditAppRequestLine.MapUpdateValues<CreditAppRequestLine>(inputCreditAppRequestLine);
                base.Update(dbCreditAppRequestLine);
                return dbCreditAppRequestLine;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateCreditAppRequestLineVoid(CreditAppRequestLine dbCreditAppRequestLine, CreditAppRequestLine inputCreditAppRequestLine, List<string> skipUpdateFields = null)
        {
            try
            {
                dbCreditAppRequestLine = dbCreditAppRequestLine.MapUpdateValues<CreditAppRequestLine>(inputCreditAppRequestLine, skipUpdateFields);
                base.Update(dbCreditAppRequestLine);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Create, Update, Delete
        #region Validate
        public override void ValidateAdd(IEnumerable<CreditAppRequestLine> items)
        {
            base.ValidateAdd(items);
            items.ToList().ForEach(f => ValidateAllowAddLine(f));
        }
        public override void ValidateAdd(CreditAppRequestLine item)
        {
            base.ValidateAdd(item);
            ValidateAllowAddLine(item);
        }
        public void ValidateAllowAddLine(CreditAppRequestLine item)
        {
            try
            {
                ICreditLimitTypeRepo creditLimitTypeRepo = new CreditLimitTypeRepo(db);
                CreditLimitType creditLimitType = creditLimitTypeRepo.GetCreditLimitTypeByCreditAppRequestTable(item.CreditAppRequestTableGUID);
                ValidateAllowAddLine(creditLimitType);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void ValidateAllowAddLine(CreditLimitType creditLimitType)
        {
            try
            {
                if (!creditLimitType.AllowAddLine)
                {
                    SmartAppException smartAppException = new SmartAppException("ERROR.ERORR");
                    smartAppException.AddData("ERROR.90011", new string[] { SmartAppUtil.GetDropDownLabel(creditLimitType.CreditLimitTypeId, creditLimitType.Description) });
                    throw smartAppException;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        public IEnumerable<CreditAppRequestLine> GetCreditAppRequestLineByCreditAppRequestTable(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.CreditAppRequestTableGUID == guid).AsNoTracking();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppRequestLine GetCreditAppRequestLineByIdNoTrackingByAccessLevel(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.CreditAppRequestLineGUID == guid)
                                    .FilterByAccessLevel(SysParm.AccessLevel)
                                    .AsNoTracking()
                                    .FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Amend CA Line
        public CreditAppRequestLine CopyCreditAppRequestLineForCreateCreditAppRequestLineAmend(CreditAppRequestLineAmendItemView creditAppRequestLineAmendView, CreditAppRequestTable newCreditAppRequestTable)
        {
            try
            {
                var result =
                        (from creditAppLine in db.Set<CreditAppLine>()
                         join creditAppTable in db.Set<CreditAppTable>() on creditAppLine.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID
                         join creditAppRequestTable in db.Set<CreditAppRequestTable>() on creditAppTable.RefCreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID
                         join creditAppRequestLine in db.Set<CreditAppRequestLine>()
                         on creditAppLine.RefCreditAppRequestLineGUID equals creditAppRequestLine.CreditAppRequestLineGUID into ljCreditAppLineCreditAppRequestLine
                         from creditAppRequestLine in ljCreditAppLineCreditAppRequestLine.DefaultIfEmpty()
                         join buyerTable in db.Set<BuyerTable>() on creditAppLine.BuyerTableGUID equals buyerTable.BuyerTableGUID
                         join buyerCreditLimitByProduct in db.Set<BuyerCreditLimitByProduct>().Where(w => w.ProductType == newCreditAppRequestTable.ProductType)
                         on buyerTable.BuyerTableGUID equals buyerCreditLimitByProduct.BuyerTableGUID into ljBuyerCreditLimitByProduct
                         from buyerCreditLimitByProduct in ljBuyerCreditLimitByProduct.DefaultIfEmpty()
                         where creditAppTable.CreditAppTableGUID == newCreditAppRequestTable.RefCreditAppTableGUID
                         select new CreditAppRequestLine
                         {
                             CreditAppRequestLineGUID = Guid.NewGuid(),
                             CreditAppRequestTableGUID = newCreditAppRequestTable.CreditAppRequestTableGUID,
                             LineNum = 1,
                             ApprovalDecision = (int)ApprovalDecision.None,
                             RefCreditAppLineGUID = creditAppRequestLineAmendView.CreditAppRequestLine_RefCreditAppLineGUID.StringToGuid(),
                             BuyerTableGUID = creditAppLine.BuyerTableGUID,
                             BuyerCreditTermGUID = creditAppLine.CreditTermGUID,
                             CreditTermDescription = creditAppLine.CreditTermDescription,
                             BillingDay = creditAppLine.BillingDay,
                             BillingDescription = creditAppLine.BillingDescription,
                             BillingAddressGUID = creditAppLine.BillingAddressGUID,
                             BillingContactPersonGUID = creditAppLine.BillingContactPersonGUID,
                             BillingResponsibleByGUID = creditAppLine.BillingResponsibleByGUID,
                             MethodOfBilling = creditAppLine.MethodOfBilling,
                             BillingRemark = creditAppLine.BillingRemark,
                             ReceiptDay = creditAppLine.ReceiptDay,
                             ReceiptDescription = creditAppLine.ReceiptDescription,
                             ReceiptAddressGUID = creditAppLine.ReceiptAddressGUID,
                             ReceiptContactPersonGUID = creditAppLine.ReceiptContactPersonGUID,
                             MethodOfPaymentGUID = creditAppLine.MethodOfPaymentGUID,
                             ReceiptRemark = creditAppLine.ReceiptRemark,
                             InvoiceAddressGUID = creditAppLine.InvoiceAddressGUID,
                             MailingReceiptAddressGUID = creditAppLine.MailingReceiptAddressGUID,
                             CreditLimitLineRequest = creditAppLine.ApprovedCreditLimitLine,
                             AssignmentMethodGUID = creditAppLine.AssignmentMethodGUID,
                             AcceptanceDocument = creditAppLine.AcceptanceDocument,
                             AcceptanceDocumentDescription = creditAppLine.AcceptanceDocumentDescription,
                             AssignmentMethodRemark = creditAppLine.AssignmentMethodRemark,
                             PaymentCondition = creditAppLine.PaymentCondition,
                             InsuranceCreditLimit = creditAppLine.InsuranceCreditLimit,
                             MaxPurchasePct = creditAppLine.MaxPurchasePct,
                             PurchaseFeePct = creditAppLine.PurchaseFeePct,
                             AssignmentAgreementTableGUID = creditAppLine.AssignmentAgreementTableGUID,
                             BlacklistStatusGUID = buyerTable.BlacklistStatusGUID,
                             KYCSetupGUID = buyerTable.KYCSetupGUID,
                             CreditScoringGUID = buyerTable.CreditScoringGUID,
                             CustomerContactBuyerPeriod = (creditAppRequestLine != null) ? creditAppRequestLine.CustomerContactBuyerPeriod : 0,
                             BuyerProduct = (creditAppRequestLine != null) ? creditAppRequestLine.BuyerProduct : string.Empty,
                             CustomerRequest = (creditAppRequestLine != null) ? creditAppRequestLine.CustomerRequest : 0,
                             BuyerCreditLimit = (buyerCreditLimitByProduct != null) ? buyerCreditLimitByProduct.CreditLimit : 0,
                             CompanyGUID = creditAppLine.CompanyGUID,
                         }).FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion Amend Ca Line
        #region BookmarkDocumentQueryBuyerAmendCreditLimit
        public QueryBuyerAmendCreditLimit GetBookmarkDocumentQueryBuyerAmendCreditLimit(Guid refGUID, Guid bookmarkDocumentTransGUID)
        {
            try
            {
                QueryBuyerAmendCreditLimit model = new QueryBuyerAmendCreditLimit();

                QCAReqTableBuyerAmendCL qCAReqTableBuyerAmendCL = GetQCAReqTableBuyerAmendCL(refGUID);
                SetModelByQCAReqTableBuyerAmendCL(model, qCAReqTableBuyerAmendCL);

                QCAReqLineBuyerAmendCL qCAReqLineBuyerAmendCL = GetQCAReqLineBuyerAmendCL(qCAReqTableBuyerAmendCL.QCAReqTableBuyerAmendCL_CrediAppReqGUID);
                SetModelByQCAReqLineBuyerAmendCL(model, qCAReqLineBuyerAmendCL);

                QCAReqLineAmendBuyerAmendCL qCAReqLineAmendBuyerAmendCL = GetQCAReqLineAmendBuyerAmendCL(qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_CreditAppRequestLineGUID);
                SetModelByQCAReqLineAmendBuyerAmendCL(model, qCAReqLineAmendBuyerAmendCL);

                QBillingContactPersonBuyerAmendCL qBillingContactPersonBuyerAmendCL = GetQBillingContactPersonBuyerAmendCL(qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_BillingContactPersonGUID);
                SetModelByQBillingContactPersonBuyerAmendCL(model, qBillingContactPersonBuyerAmendCL);

                QReceiptContactPersonBuyerAmendCL qReceiptContactPersonBuyerAmendCL = GetQReceiptContactPersonBuyerAmendCL(qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_ReceiptContactPersonGUID);
                SetModelByQReceiptContactPersonBuyerAmendCL(model, qReceiptContactPersonBuyerAmendCL);

                SetModelQCreditAppReqLineFinBuyerAmendCL(model, qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_CreditAppRequestLineGUID);

                QOriginalCreditAppRequestLine qOriginalCreditAppRequestLine = GetQOriginalCreditAppRequestLine(qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_BuyerTableGUID, qCAReqTableBuyerAmendCL.QCAReqTableBuyerAmendCL_RefCreditAppRequestTableGUID);
                SetModelByQOriginalCreditAppRequestLine(model, qOriginalCreditAppRequestLine);


                // get list
                List<QBillingDocumentBuyerAmendCL> qBillingDocumentBuyerAmendCL = GetQBillingDocumentBuyerAmendCL(qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_RefCreditAppLineGUID);
                // get list
                QBillingDocumentBuyerAmendCL qBillingDocumentBuyerAmendCL_1 = GetQBillingDocumentBuyerAmendCLEachRow(qBillingDocumentBuyerAmendCL, 1);
                SetModelByQBillingDocumentBuyerAmendCL_1(model, qBillingDocumentBuyerAmendCL_1);

                QBillingDocumentBuyerAmendCL qBillingDocumentBuyerAmendCL_2 = GetQBillingDocumentBuyerAmendCLEachRow(qBillingDocumentBuyerAmendCL, 2);
                SetModelByQBillingDocumentBuyerAmendCL_2(model, qBillingDocumentBuyerAmendCL_2);

                QBillingDocumentBuyerAmendCL qBillingDocumentBuyerAmendCL_3 = GetQBillingDocumentBuyerAmendCLEachRow(qBillingDocumentBuyerAmendCL, 3);
                SetModelByQBillingDocumentBuyerAmendCL_3(model, qBillingDocumentBuyerAmendCL_3);

                QBillingDocumentBuyerAmendCL qBillingDocumentBuyerAmendCL_4 = GetQBillingDocumentBuyerAmendCLEachRow(qBillingDocumentBuyerAmendCL, 4);
                SetModelByQBillingDocumentBuyerAmendCL_4(model, qBillingDocumentBuyerAmendCL_4);

                QBillingDocumentBuyerAmendCL qBillingDocumentBuyerAmendCL_5 = GetQBillingDocumentBuyerAmendCLEachRow(qBillingDocumentBuyerAmendCL, 5);
                SetModelByQBillingDocumentBuyerAmendCL_5(model, qBillingDocumentBuyerAmendCL_5);

                QBillingDocumentBuyerAmendCL qBillingDocumentBuyerAmendCL_6 = GetQBillingDocumentBuyerAmendCLEachRow(qBillingDocumentBuyerAmendCL, 6);
                SetModelByQBillingDocumentBuyerAmendCL_6(model, qBillingDocumentBuyerAmendCL_6);

                QBillingDocumentBuyerAmendCL qBillingDocumentBuyerAmendCL_7 = GetQBillingDocumentBuyerAmendCLEachRow(qBillingDocumentBuyerAmendCL, 7);
                SetModelByQBillingDocumentBuyerAmendCL_7(model, qBillingDocumentBuyerAmendCL_7);

                QBillingDocumentBuyerAmendCL qBillingDocumentBuyerAmendCL_8 = GetQBillingDocumentBuyerAmendCLEachRow(qBillingDocumentBuyerAmendCL, 8);
                SetModelByQBillingDocumentBuyerAmendCL_8(model, qBillingDocumentBuyerAmendCL_8);

                QBillingDocumentBuyerAmendCL qBillingDocumentBuyerAmendCL_9 = GetQBillingDocumentBuyerAmendCLEachRow(qBillingDocumentBuyerAmendCL, 9);
                SetModelByQBillingDocumentBuyerAmendCL_9(model, qBillingDocumentBuyerAmendCL_9);

                QBillingDocumentBuyerAmendCL qBillingDocumentBuyerAmendCL_10 = GetQBillingDocumentBuyerAmendCLEachRow(qBillingDocumentBuyerAmendCL, 10);
                SetModelByQBillingDocumentBuyerAmendCL_10(model, qBillingDocumentBuyerAmendCL_10);
                // get list
                List<QReceiptDocumentConditionBuyerAmendCL> qReceiptDocumentConditionBuyerAmendCL = GetQReceiptDocumentConditionBuyerAmendCL(qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_RefCreditAppLineGUID);
                // get list
                QReceiptDocumentConditionBuyerAmendCL QReceiptDocumentConditionBuyerAmendCL_1 = GetQReceiptDocumentConditionBuyerAmendCLEachRow(qReceiptDocumentConditionBuyerAmendCL, 1);
                SetModelByQReceiptDocumentConditionBuyerAmendCL_1(model, QReceiptDocumentConditionBuyerAmendCL_1);

                QReceiptDocumentConditionBuyerAmendCL QReceiptDocumentConditionBuyerAmendCL_2 = GetQReceiptDocumentConditionBuyerAmendCLEachRow(qReceiptDocumentConditionBuyerAmendCL, 2);
                SetModelByQReceiptDocumentConditionBuyerAmendCL_2(model, QReceiptDocumentConditionBuyerAmendCL_2);

                QReceiptDocumentConditionBuyerAmendCL QReceiptDocumentConditionBuyerAmendCL_3 = GetQReceiptDocumentConditionBuyerAmendCLEachRow(qReceiptDocumentConditionBuyerAmendCL, 3);
                SetModelByQReceiptDocumentConditionBuyerAmendCL_3(model, QReceiptDocumentConditionBuyerAmendCL_3);

                QReceiptDocumentConditionBuyerAmendCL QReceiptDocumentConditionBuyerAmendCL_4 = GetQReceiptDocumentConditionBuyerAmendCLEachRow(qReceiptDocumentConditionBuyerAmendCL, 4);
                SetModelByQReceiptDocumentConditionBuyerAmendCL_4(model, QReceiptDocumentConditionBuyerAmendCL_4);

                QReceiptDocumentConditionBuyerAmendCL QReceiptDocumentConditionBuyerAmendCL_5 = GetQReceiptDocumentConditionBuyerAmendCLEachRow(qReceiptDocumentConditionBuyerAmendCL, 5);
                SetModelByQReceiptDocumentConditionBuyerAmendCL_5(model, QReceiptDocumentConditionBuyerAmendCL_5);

                QReceiptDocumentConditionBuyerAmendCL QReceiptDocumentConditionBuyerAmendCL_6 = GetQReceiptDocumentConditionBuyerAmendCLEachRow(qReceiptDocumentConditionBuyerAmendCL, 6);
                SetModelByQReceiptDocumentConditionBuyerAmendCL_6(model, QReceiptDocumentConditionBuyerAmendCL_6);

                QReceiptDocumentConditionBuyerAmendCL QReceiptDocumentConditionBuyerAmendCL_7 = GetQReceiptDocumentConditionBuyerAmendCLEachRow(qReceiptDocumentConditionBuyerAmendCL, 7);
                SetModelByQReceiptDocumentConditionBuyerAmendCL_7(model, QReceiptDocumentConditionBuyerAmendCL_7);

                QReceiptDocumentConditionBuyerAmendCL QReceiptDocumentConditionBuyerAmendCL_8 = GetQReceiptDocumentConditionBuyerAmendCLEachRow(qReceiptDocumentConditionBuyerAmendCL, 8);
                SetModelByQReceiptDocumentConditionBuyerAmendCL_8(model, QReceiptDocumentConditionBuyerAmendCL_8);

                QReceiptDocumentConditionBuyerAmendCL QReceiptDocumentConditionBuyerAmendCL_9 = GetQReceiptDocumentConditionBuyerAmendCLEachRow(qReceiptDocumentConditionBuyerAmendCL, 9);
                SetModelByQReceiptDocumentConditionBuyerAmendCL_9(model, QReceiptDocumentConditionBuyerAmendCL_9);

                QReceiptDocumentConditionBuyerAmendCL QReceiptDocumentConditionBuyerAmendCL_10 = GetQReceiptDocumentConditionBuyerAmendCLEachRow(qReceiptDocumentConditionBuyerAmendCL, 10);
                SetModelByQReceiptDocumentConditionBuyerAmendCL_10(model, QReceiptDocumentConditionBuyerAmendCL_10);

                List<QServiceFeeConditionLineBuyerAmendCL> qServiceFeeConditionLineBuyerAmendCL = GetQServiceFeeConditionLineBuyerAmendCL(qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_CreditAppRequestLineGUID);
                QServiceFeeConditionLineBuyerAmendCL qServiceFeeConditionLineBuyerAmendCL_1 = GetQServiceFeeConditionLineBuyerAmendCLEachRow(qServiceFeeConditionLineBuyerAmendCL, 1);
                SetModelQServiceFeeConditionLineBuyerAmendCL_1(model, qServiceFeeConditionLineBuyerAmendCL_1);

                QServiceFeeConditionLineBuyerAmendCL qServiceFeeConditionLineBuyerAmendCL_2 = GetQServiceFeeConditionLineBuyerAmendCLEachRow(qServiceFeeConditionLineBuyerAmendCL, 2);
                SetModelQServiceFeeConditionLineBuyerAmendCL_2(model, qServiceFeeConditionLineBuyerAmendCL_2);

                QServiceFeeConditionLineBuyerAmendCL qServiceFeeConditionLineBuyerAmendCL_3 = GetQServiceFeeConditionLineBuyerAmendCLEachRow(qServiceFeeConditionLineBuyerAmendCL, 3);
                SetModelQServiceFeeConditionLineBuyerAmendCL_3(model, qServiceFeeConditionLineBuyerAmendCL_3);

                QServiceFeeConditionLineBuyerAmendCL qServiceFeeConditionLineBuyerAmendCL_4 = GetQServiceFeeConditionLineBuyerAmendCLEachRow(qServiceFeeConditionLineBuyerAmendCL, 4);
                SetModelQServiceFeeConditionLineBuyerAmendCL_4(model, qServiceFeeConditionLineBuyerAmendCL_4);

                QServiceFeeConditionLineBuyerAmendCL qServiceFeeConditionLineBuyerAmendCL_5 = GetQServiceFeeConditionLineBuyerAmendCLEachRow(qServiceFeeConditionLineBuyerAmendCL, 5);
                SetModelQServiceFeeConditionLineBuyerAmendCL_5(model, qServiceFeeConditionLineBuyerAmendCL_5);

                QServiceFeeConditionLineBuyerAmendCL qServiceFeeConditionLineBuyerAmendCL_6 = GetQServiceFeeConditionLineBuyerAmendCLEachRow(qServiceFeeConditionLineBuyerAmendCL, 6);
                SetModelQServiceFeeConditionLineBuyerAmendCL_6(model, qServiceFeeConditionLineBuyerAmendCL_6);

                QServiceFeeConditionLineBuyerAmendCL qServiceFeeConditionLineBuyerAmendCL_7 = GetQServiceFeeConditionLineBuyerAmendCLEachRow(qServiceFeeConditionLineBuyerAmendCL, 7);
                SetModelQServiceFeeConditionLineBuyerAmendCL_7(model, qServiceFeeConditionLineBuyerAmendCL_7);

                QServiceFeeConditionLineBuyerAmendCL qServiceFeeConditionLineBuyerAmendCL_8 = GetQServiceFeeConditionLineBuyerAmendCLEachRow(qServiceFeeConditionLineBuyerAmendCL, 8);
                SetModelQServiceFeeConditionLineBuyerAmendCL_8(model, qServiceFeeConditionLineBuyerAmendCL_8);

                QServiceFeeConditionLineBuyerAmendCL qServiceFeeConditionLineBuyerAmendCL_9 = GetQServiceFeeConditionLineBuyerAmendCLEachRow(qServiceFeeConditionLineBuyerAmendCL, 9);
                SetModelQServiceFeeConditionLineBuyerAmendCL_9(model, qServiceFeeConditionLineBuyerAmendCL_9);

                QServiceFeeConditionLineBuyerAmendCL qServiceFeeConditionLineBuyerAmendCL_10 = GetQServiceFeeConditionLineBuyerAmendCLEachRow(qServiceFeeConditionLineBuyerAmendCL, 10);
                SetModelQServiceFeeConditionLineBuyerAmendCL_10(model, qServiceFeeConditionLineBuyerAmendCL_10);

                decimal sumQSumServiceFeeConditionLineBuyerAmendCL = GetSunQSumServiceFeeConditionLineBuyerAmendCL(qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_CreditAppRequestLineGUID);
                SetModelsumQSumServiceFeeConditionLineBuyerAmendCL(model, sumQSumServiceFeeConditionLineBuyerAmendCL);

                List<QActionHistoryBuyerAmendCL> qActionHistoryBuyerAmendCL = GettQActionHistoryBuyerAmendCL(qCAReqTableBuyerAmendCL.QCAReqTableBuyerAmendCL_CrediAppReqGUID);
                QActionHistoryBuyerAmendCL qActionHistoryBuyerAmendCL_1 = GetQActionHistoryBuyerAmendCLEachRow(qActionHistoryBuyerAmendCL, 1);
                SetModelByQActionHistoryBuyerAmendCL_1(model, qActionHistoryBuyerAmendCL_1);

                QActionHistoryBuyerAmendCL qActionHistoryBuyerAmendCL_2 = GetQActionHistoryBuyerAmendCLEachRow(qActionHistoryBuyerAmendCL, 2);
                SetModelByQActionHistoryBuyerAmendCL_2(model, qActionHistoryBuyerAmendCL_2);

                QActionHistoryBuyerAmendCL qActionHistoryBuyerAmendCL_3 = GetQActionHistoryBuyerAmendCLEachRow(qActionHistoryBuyerAmendCL, 3);
                SetModelByQActionHistoryBuyerAmendCL_3(model, qActionHistoryBuyerAmendCL_3);

                QActionHistoryBuyerAmendCL qActionHistoryBuyerAmendCL_4 = GetQActionHistoryBuyerAmendCLEachRow(qActionHistoryBuyerAmendCL, 4);
                SetModelByQActionHistoryBuyerAmendCL_4(model, qActionHistoryBuyerAmendCL_4);

                QActionHistoryBuyerAmendCL qActionHistoryBuyerAmendCL_5 = GetQActionHistoryBuyerAmendCLEachRow(qActionHistoryBuyerAmendCL, 5);
                SetModelByQActionHistoryBuyerAmendCL_5(model, qActionHistoryBuyerAmendCL_5);

                SetModelByVariableBuyerCreditLimitRequest(model, qCAReqLineBuyerAmendCL, qCAReqLineAmendBuyerAmendCL);
                SetModelByVariableAcceptanceDocument(model, qCAReqLineBuyerAmendCL);


                return model;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }



        private void SetModelByQOriginalCreditAppRequestLine(QueryBuyerAmendCreditLimit model, QOriginalCreditAppRequestLine qOriginalCreditAppRequestLine)
        {
            try
            {
                model.QOriginalCreditAppRequestLine_BillingResponsibleBy = qOriginalCreditAppRequestLine.QOriginalCreditAppRequestLine_BillingResponsibleBy;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private QOriginalCreditAppRequestLine GetQOriginalCreditAppRequestLine(Guid? qCAReqLineBuyerAmendCL_BuyerTableGUID, Guid? qCAReqTableBuyerAmendCL_RefCreditAppTableGUID)
        {
            try
            {
                var result = (from orgCreditAppRequestLine in db.Set<CreditAppRequestLine>().Where(wh => wh.CreditAppRequestTableGUID == qCAReqTableBuyerAmendCL_RefCreditAppTableGUID && wh.BuyerTableGUID == qCAReqLineBuyerAmendCL_BuyerTableGUID)
                              join billingResponsibleBy in db.Set<BillingResponsibleBy>()
                              on orgCreditAppRequestLine.BillingResponsibleByGUID equals billingResponsibleBy.BillingResponsibleByGUID into ljbillingResponsibleBy
                              from billingResponsibleBy in ljbillingResponsibleBy.DefaultIfEmpty()
                              select new QOriginalCreditAppRequestLine
                              {
                                  QOriginalCreditAppRequestLine_RefCreditLineGUID = orgCreditAppRequestLine.RefCreditAppLineGUID,
                                  QOriginalCreditAppRequestLine_BillingResponsibleByGUID = orgCreditAppRequestLine.BillingResponsibleByGUID,
                                  QOriginalCreditAppRequestLine_BillingResponsibleBy = billingResponsibleBy != null ? billingResponsibleBy.Description : "",
                                  QOriginalCreditAppRequestLine_CreditAppRequestLineGUID = orgCreditAppRequestLine.CreditAppRequestTableGUID
                              }
                               ).FirstOrDefault();
                if (result != null)
                {
                    return result;
                }
                else
                {
                    return new QOriginalCreditAppRequestLine();
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private void SetModelByVariableAcceptanceDocument(QueryBuyerAmendCreditLimit model, QCAReqLineBuyerAmendCL qCAReqLineBuyerAmendCL)
        {
            try
            {
                if (qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_AcceptanceDocument)
                {
                    model.QueryBuyerAmendCreditLimit_AcceptanceDocument = "Yes";
                }
                else
                {
                    model.QueryBuyerAmendCreditLimit_AcceptanceDocument = "No";
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private void SetModelByVariableBuyerCreditLimitRequest(QueryBuyerAmendCreditLimit model, QCAReqLineBuyerAmendCL qCAReqLineBuyerAmendCL, QCAReqLineAmendBuyerAmendCL qCAReqLineAmendBuyerAmendCL)
        {
            try
            {
                model.QueryBuyerAmendCreditLimit_BuyerCreditLimitRequest = (qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_CreditLimitLineRequest - qCAReqLineAmendBuyerAmendCL.QCAReqLineAmendBuyerAmendCL_OriginalCreditLimitLineRequest);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private void SetModelByQActionHistoryBuyerAmendCL_8(QueryBuyerAmendCreditLimit model, QActionHistoryBuyerAmendCL qActionHistoryBuyerAmendCL_8)
        {
            try
            {
                model.QActionHistoryBuyerAmendCL_Comment_8 = qActionHistoryBuyerAmendCL_8.QActionHistoryBuyerAmendCL_Comment;
                model.QActionHistoryBuyerAmendCL_CreatedDateTime_8 = qActionHistoryBuyerAmendCL_8.QActionHistoryBuyerAmendCL_CreatedDateTime;
                model.QActionHistoryBuyerAmendCL_ActionName_8 = qActionHistoryBuyerAmendCL_8.QActionHistoryBuyerAmendCL_ActionName;
                model.QActionHistoryBuyerAmendCL_ApproverName_8 = qActionHistoryBuyerAmendCL_8.QActionHistoryBuyerAmendCL_ApproverName;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelByQActionHistoryBuyerAmendCL_7(QueryBuyerAmendCreditLimit model, QActionHistoryBuyerAmendCL qActionHistoryBuyerAmendCL_7)
        {
            try
            {
                model.QActionHistoryBuyerAmendCL_Comment_7 = qActionHistoryBuyerAmendCL_7.QActionHistoryBuyerAmendCL_Comment;
                model.QActionHistoryBuyerAmendCL_CreatedDateTime_7 = qActionHistoryBuyerAmendCL_7.QActionHistoryBuyerAmendCL_CreatedDateTime;
                model.QActionHistoryBuyerAmendCL_ActionName_7 = qActionHistoryBuyerAmendCL_7.QActionHistoryBuyerAmendCL_ActionName;
                model.QActionHistoryBuyerAmendCL_ApproverName_7 = qActionHistoryBuyerAmendCL_7.QActionHistoryBuyerAmendCL_ApproverName;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelByQActionHistoryBuyerAmendCL_6(QueryBuyerAmendCreditLimit model, QActionHistoryBuyerAmendCL qActionHistoryBuyerAmendCL_6)
        {
            try
            {
                model.QActionHistoryBuyerAmendCL_Comment_6 = qActionHistoryBuyerAmendCL_6.QActionHistoryBuyerAmendCL_Comment;
                model.QActionHistoryBuyerAmendCL_CreatedDateTime_6 = qActionHistoryBuyerAmendCL_6.QActionHistoryBuyerAmendCL_CreatedDateTime;
                model.QActionHistoryBuyerAmendCL_ActionName_6 = qActionHistoryBuyerAmendCL_6.QActionHistoryBuyerAmendCL_ActionName;
                model.QActionHistoryBuyerAmendCL_ApproverName_6 = qActionHistoryBuyerAmendCL_6.QActionHistoryBuyerAmendCL_ApproverName;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelByQActionHistoryBuyerAmendCL_5(QueryBuyerAmendCreditLimit model, QActionHistoryBuyerAmendCL qActionHistoryBuyerAmendCL_5)
        {
            try
            {
                model.QActionHistoryBuyerAmendCL_Comment_5 = qActionHistoryBuyerAmendCL_5.QActionHistoryBuyerAmendCL_Comment;
                model.QActionHistoryBuyerAmendCL_CreatedDateTime_5 = qActionHistoryBuyerAmendCL_5.QActionHistoryBuyerAmendCL_CreatedDateTime;
                model.QActionHistoryBuyerAmendCL_ActionName_5 = qActionHistoryBuyerAmendCL_5.QActionHistoryBuyerAmendCL_ActionName;
                model.QActionHistoryBuyerAmendCL_ApproverName_5 = qActionHistoryBuyerAmendCL_5.QActionHistoryBuyerAmendCL_ApproverName;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelByQActionHistoryBuyerAmendCL_4(QueryBuyerAmendCreditLimit model, QActionHistoryBuyerAmendCL qActionHistoryBuyerAmendCL_4)
        {
            try
            {
                model.QActionHistoryBuyerAmendCL_Comment_4 = qActionHistoryBuyerAmendCL_4.QActionHistoryBuyerAmendCL_Comment;
                model.QActionHistoryBuyerAmendCL_CreatedDateTime_4 = qActionHistoryBuyerAmendCL_4.QActionHistoryBuyerAmendCL_CreatedDateTime;
                model.QActionHistoryBuyerAmendCL_ActionName_4 = qActionHistoryBuyerAmendCL_4.QActionHistoryBuyerAmendCL_ActionName;
                model.QActionHistoryBuyerAmendCL_ApproverName_4 = qActionHistoryBuyerAmendCL_4.QActionHistoryBuyerAmendCL_ApproverName;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelByQActionHistoryBuyerAmendCL_3(QueryBuyerAmendCreditLimit model, QActionHistoryBuyerAmendCL qActionHistoryBuyerAmendCL_3)
        {
            try
            {
                model.QActionHistoryBuyerAmendCL_Comment_3 = qActionHistoryBuyerAmendCL_3.QActionHistoryBuyerAmendCL_Comment;
                model.QActionHistoryBuyerAmendCL_CreatedDateTime_3 = qActionHistoryBuyerAmendCL_3.QActionHistoryBuyerAmendCL_CreatedDateTime;
                model.QActionHistoryBuyerAmendCL_ActionName_3 = qActionHistoryBuyerAmendCL_3.QActionHistoryBuyerAmendCL_ActionName;
                model.QActionHistoryBuyerAmendCL_ApproverName_3 = qActionHistoryBuyerAmendCL_3.QActionHistoryBuyerAmendCL_ApproverName;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelByQActionHistoryBuyerAmendCL_2(QueryBuyerAmendCreditLimit model, QActionHistoryBuyerAmendCL qActionHistoryBuyerAmendCL_2)
        {
            try
            {
                model.QActionHistoryBuyerAmendCL_Comment_2 = qActionHistoryBuyerAmendCL_2.QActionHistoryBuyerAmendCL_Comment;
                model.QActionHistoryBuyerAmendCL_CreatedDateTime_2 = qActionHistoryBuyerAmendCL_2.QActionHistoryBuyerAmendCL_CreatedDateTime;
                model.QActionHistoryBuyerAmendCL_ActionName_2 = qActionHistoryBuyerAmendCL_2.QActionHistoryBuyerAmendCL_ActionName;
                model.QActionHistoryBuyerAmendCL_ApproverName_2 = qActionHistoryBuyerAmendCL_2.QActionHistoryBuyerAmendCL_ApproverName;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelByQActionHistoryBuyerAmendCL_1(QueryBuyerAmendCreditLimit model, QActionHistoryBuyerAmendCL qActionHistoryBuyerAmendCL_1)
        {
            try
            {
                model.QActionHistoryBuyerAmendCL_Comment_1 = qActionHistoryBuyerAmendCL_1.QActionHistoryBuyerAmendCL_Comment;
                model.QActionHistoryBuyerAmendCL_CreatedDateTime_1 = qActionHistoryBuyerAmendCL_1.QActionHistoryBuyerAmendCL_CreatedDateTime;
                model.QActionHistoryBuyerAmendCL_ActionName_1 = qActionHistoryBuyerAmendCL_1.QActionHistoryBuyerAmendCL_ActionName;
                model.QActionHistoryBuyerAmendCL_ApproverName_1 = qActionHistoryBuyerAmendCL_1.QActionHistoryBuyerAmendCL_ApproverName;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private void SetModelsumQSumServiceFeeConditionLineBuyerAmendCL(QueryBuyerAmendCreditLimit model, decimal sumQSumServiceFeeConditionLineBuyerAmendCL)
        {
            try
            {
                model.QSumServiceFeeConditionLineBuyerAmendCL_SumServiceFeeAmt = sumQSumServiceFeeConditionLineBuyerAmendCL;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private void SetModelQServiceFeeConditionLineBuyerAmendCL_10(QueryBuyerAmendCreditLimit model, QServiceFeeConditionLineBuyerAmendCL qServiceFeeConditionLineBuyerAmendCL_10)
        {
            try
            {
                model.QServiceFeeConditionLineBuyerAmendCL_Ordering_10 = qServiceFeeConditionLineBuyerAmendCL_10.QServiceFeeConditionLineBuyerAmendCL_Ordering;
                model.QServiceFeeConditionLineBuyerAmendCL_Description_10 = qServiceFeeConditionLineBuyerAmendCL_10.QServiceFeeConditionLineBuyerAmendCL_Description;
                model.QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax_10 = qServiceFeeConditionLineBuyerAmendCL_10.QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax;
                model.QServiceFeeConditionLineBuyerAmendCL_ServicefeeType_10 = qServiceFeeConditionLineBuyerAmendCL_10.QServiceFeeConditionLineBuyerAmendCL_ServicefeeType;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelQServiceFeeConditionLineBuyerAmendCL_9(QueryBuyerAmendCreditLimit model, QServiceFeeConditionLineBuyerAmendCL qServiceFeeConditionLineBuyerAmendCL_9)
        {
            try
            {
                model.QServiceFeeConditionLineBuyerAmendCL_Ordering_9 = qServiceFeeConditionLineBuyerAmendCL_9.QServiceFeeConditionLineBuyerAmendCL_Ordering;
                model.QServiceFeeConditionLineBuyerAmendCL_Description_9 = qServiceFeeConditionLineBuyerAmendCL_9.QServiceFeeConditionLineBuyerAmendCL_Description;
                model.QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax_9 = qServiceFeeConditionLineBuyerAmendCL_9.QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax;
                model.QServiceFeeConditionLineBuyerAmendCL_ServicefeeType_9 = qServiceFeeConditionLineBuyerAmendCL_9.QServiceFeeConditionLineBuyerAmendCL_ServicefeeType;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelQServiceFeeConditionLineBuyerAmendCL_8(QueryBuyerAmendCreditLimit model, QServiceFeeConditionLineBuyerAmendCL qServiceFeeConditionLineBuyerAmendCL_8)
        {
            try
            {
                model.QServiceFeeConditionLineBuyerAmendCL_Ordering_8 = qServiceFeeConditionLineBuyerAmendCL_8.QServiceFeeConditionLineBuyerAmendCL_Ordering;
                model.QServiceFeeConditionLineBuyerAmendCL_Description_8 = qServiceFeeConditionLineBuyerAmendCL_8.QServiceFeeConditionLineBuyerAmendCL_Description;
                model.QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax_8 = qServiceFeeConditionLineBuyerAmendCL_8.QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax;
                model.QServiceFeeConditionLineBuyerAmendCL_ServicefeeType_8 = qServiceFeeConditionLineBuyerAmendCL_8.QServiceFeeConditionLineBuyerAmendCL_ServicefeeType;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelQServiceFeeConditionLineBuyerAmendCL_7(QueryBuyerAmendCreditLimit model, QServiceFeeConditionLineBuyerAmendCL qServiceFeeConditionLineBuyerAmendCL_7)
        {
            try
            {
                model.QServiceFeeConditionLineBuyerAmendCL_Ordering_7 = qServiceFeeConditionLineBuyerAmendCL_7.QServiceFeeConditionLineBuyerAmendCL_Ordering;
                model.QServiceFeeConditionLineBuyerAmendCL_Description_7 = qServiceFeeConditionLineBuyerAmendCL_7.QServiceFeeConditionLineBuyerAmendCL_Description;
                model.QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax_7 = qServiceFeeConditionLineBuyerAmendCL_7.QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax;
                model.QServiceFeeConditionLineBuyerAmendCL_ServicefeeType_7 = qServiceFeeConditionLineBuyerAmendCL_7.QServiceFeeConditionLineBuyerAmendCL_ServicefeeType;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelQServiceFeeConditionLineBuyerAmendCL_6(QueryBuyerAmendCreditLimit model, QServiceFeeConditionLineBuyerAmendCL qServiceFeeConditionLineBuyerAmendCL_6)
        {
            try
            {
                model.QServiceFeeConditionLineBuyerAmendCL_Ordering_6 = qServiceFeeConditionLineBuyerAmendCL_6.QServiceFeeConditionLineBuyerAmendCL_Ordering;
                model.QServiceFeeConditionLineBuyerAmendCL_Description_6 = qServiceFeeConditionLineBuyerAmendCL_6.QServiceFeeConditionLineBuyerAmendCL_Description;
                model.QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax_6 = qServiceFeeConditionLineBuyerAmendCL_6.QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax;
                model.QServiceFeeConditionLineBuyerAmendCL_ServicefeeType_6 = qServiceFeeConditionLineBuyerAmendCL_6.QServiceFeeConditionLineBuyerAmendCL_ServicefeeType;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelQServiceFeeConditionLineBuyerAmendCL_5(QueryBuyerAmendCreditLimit model, QServiceFeeConditionLineBuyerAmendCL qServiceFeeConditionLineBuyerAmendCL_5)
        {
            try
            {
                model.QServiceFeeConditionLineBuyerAmendCL_Ordering_5 = qServiceFeeConditionLineBuyerAmendCL_5.QServiceFeeConditionLineBuyerAmendCL_Ordering;
                model.QServiceFeeConditionLineBuyerAmendCL_Description_5 = qServiceFeeConditionLineBuyerAmendCL_5.QServiceFeeConditionLineBuyerAmendCL_Description;
                model.QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax_5 = qServiceFeeConditionLineBuyerAmendCL_5.QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax;
                model.QServiceFeeConditionLineBuyerAmendCL_ServicefeeType_5 = qServiceFeeConditionLineBuyerAmendCL_5.QServiceFeeConditionLineBuyerAmendCL_ServicefeeType;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelQServiceFeeConditionLineBuyerAmendCL_4(QueryBuyerAmendCreditLimit model, QServiceFeeConditionLineBuyerAmendCL qServiceFeeConditionLineBuyerAmendCL_4)
        {
            try
            {
                model.QServiceFeeConditionLineBuyerAmendCL_Ordering_4 = qServiceFeeConditionLineBuyerAmendCL_4.QServiceFeeConditionLineBuyerAmendCL_Ordering;
                model.QServiceFeeConditionLineBuyerAmendCL_Description_4 = qServiceFeeConditionLineBuyerAmendCL_4.QServiceFeeConditionLineBuyerAmendCL_Description;
                model.QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax_4 = qServiceFeeConditionLineBuyerAmendCL_4.QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax;
                model.QServiceFeeConditionLineBuyerAmendCL_ServicefeeType_4 = qServiceFeeConditionLineBuyerAmendCL_4.QServiceFeeConditionLineBuyerAmendCL_ServicefeeType;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelQServiceFeeConditionLineBuyerAmendCL_3(QueryBuyerAmendCreditLimit model, QServiceFeeConditionLineBuyerAmendCL qServiceFeeConditionLineBuyerAmendCL_3)
        {
            try
            {
                model.QServiceFeeConditionLineBuyerAmendCL_Ordering_3 = qServiceFeeConditionLineBuyerAmendCL_3.QServiceFeeConditionLineBuyerAmendCL_Ordering;
                model.QServiceFeeConditionLineBuyerAmendCL_Description_3 = qServiceFeeConditionLineBuyerAmendCL_3.QServiceFeeConditionLineBuyerAmendCL_Description;
                model.QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax_3 = qServiceFeeConditionLineBuyerAmendCL_3.QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax;
                model.QServiceFeeConditionLineBuyerAmendCL_ServicefeeType_3 = qServiceFeeConditionLineBuyerAmendCL_3.QServiceFeeConditionLineBuyerAmendCL_ServicefeeType;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelQServiceFeeConditionLineBuyerAmendCL_2(QueryBuyerAmendCreditLimit model, QServiceFeeConditionLineBuyerAmendCL qServiceFeeConditionLineBuyerAmendCL_2)
        {
            try
            {
                model.QServiceFeeConditionLineBuyerAmendCL_Ordering_2 = qServiceFeeConditionLineBuyerAmendCL_2.QServiceFeeConditionLineBuyerAmendCL_Ordering;
                model.QServiceFeeConditionLineBuyerAmendCL_Description_2 = qServiceFeeConditionLineBuyerAmendCL_2.QServiceFeeConditionLineBuyerAmendCL_Description;
                model.QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax_2 = qServiceFeeConditionLineBuyerAmendCL_2.QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax;
                model.QServiceFeeConditionLineBuyerAmendCL_ServicefeeType_2 = qServiceFeeConditionLineBuyerAmendCL_2.QServiceFeeConditionLineBuyerAmendCL_ServicefeeType;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelQServiceFeeConditionLineBuyerAmendCL_1(QueryBuyerAmendCreditLimit model, QServiceFeeConditionLineBuyerAmendCL qServiceFeeConditionLineBuyerAmendCL_1)
        {
            try
            {
                model.QServiceFeeConditionLineBuyerAmendCL_Ordering_1 = qServiceFeeConditionLineBuyerAmendCL_1.QServiceFeeConditionLineBuyerAmendCL_Ordering;
                model.QServiceFeeConditionLineBuyerAmendCL_Description_1 = qServiceFeeConditionLineBuyerAmendCL_1.QServiceFeeConditionLineBuyerAmendCL_Description;
                model.QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax_1 = qServiceFeeConditionLineBuyerAmendCL_1.QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax;
                model.QServiceFeeConditionLineBuyerAmendCL_ServicefeeType_1 = qServiceFeeConditionLineBuyerAmendCL_1.QServiceFeeConditionLineBuyerAmendCL_ServicefeeType;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private void SetModelByQReceiptDocumentConditionBuyerAmendCL_10(QueryBuyerAmendCreditLimit model, QReceiptDocumentConditionBuyerAmendCL qReceiptDocumentConditionBuyerAmendCL_10)
        {
            try
            {
                model.QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription_10 = qReceiptDocumentConditionBuyerAmendCL_10.QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelByQReceiptDocumentConditionBuyerAmendCL_9(QueryBuyerAmendCreditLimit model, QReceiptDocumentConditionBuyerAmendCL qReceiptDocumentConditionBuyerAmendCL_9)
        {
            try
            {
                model.QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription_9 = qReceiptDocumentConditionBuyerAmendCL_9.QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelByQReceiptDocumentConditionBuyerAmendCL_8(QueryBuyerAmendCreditLimit model, QReceiptDocumentConditionBuyerAmendCL qReceiptDocumentConditionBuyerAmendCL_8)
        {
            try
            {
                model.QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription_8 = qReceiptDocumentConditionBuyerAmendCL_8.QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelByQReceiptDocumentConditionBuyerAmendCL_7(QueryBuyerAmendCreditLimit model, QReceiptDocumentConditionBuyerAmendCL qReceiptDocumentConditionBuyerAmendCL_7)
        {
            try
            {
                model.QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription_7 = qReceiptDocumentConditionBuyerAmendCL_7.QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelByQReceiptDocumentConditionBuyerAmendCL_6(QueryBuyerAmendCreditLimit model, QReceiptDocumentConditionBuyerAmendCL qReceiptDocumentConditionBuyerAmendCL_6)
        {
            try
            {
                model.QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription_6 = qReceiptDocumentConditionBuyerAmendCL_6.QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelByQReceiptDocumentConditionBuyerAmendCL_5(QueryBuyerAmendCreditLimit model, QReceiptDocumentConditionBuyerAmendCL qReceiptDocumentConditionBuyerAmendCL_5)
        {
            try
            {
                model.QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription_5 = qReceiptDocumentConditionBuyerAmendCL_5.QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelByQReceiptDocumentConditionBuyerAmendCL_4(QueryBuyerAmendCreditLimit model, QReceiptDocumentConditionBuyerAmendCL qReceiptDocumentConditionBuyerAmendCL_4)
        {
            try
            {
                model.QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription_4 = qReceiptDocumentConditionBuyerAmendCL_4.QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelByQReceiptDocumentConditionBuyerAmendCL_3(QueryBuyerAmendCreditLimit model, QReceiptDocumentConditionBuyerAmendCL qReceiptDocumentConditionBuyerAmendCL_3)
        {
            try
            {
                model.QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription_3 = qReceiptDocumentConditionBuyerAmendCL_3.QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelByQReceiptDocumentConditionBuyerAmendCL_2(QueryBuyerAmendCreditLimit model, QReceiptDocumentConditionBuyerAmendCL qReceiptDocumentConditionBuyerAmendCL_2)
        {
            try
            {
                model.QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription_2 = qReceiptDocumentConditionBuyerAmendCL_2.QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelByQReceiptDocumentConditionBuyerAmendCL_1(QueryBuyerAmendCreditLimit model, QReceiptDocumentConditionBuyerAmendCL qReceiptDocumentConditionBuyerAmendCL_1)
        {
            try
            {
                model.QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription_1 = qReceiptDocumentConditionBuyerAmendCL_1.QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private void SetModelByQBillingDocumentBuyerAmendCL_10(QueryBuyerAmendCreditLimit model, QBillingDocumentBuyerAmendCL qBillingDocumentBuyerAmendCL_10)
        {
            try
            {
                model.QBillingDocumentBuyerAmendCL_DocumentTypeDescription_10 = qBillingDocumentBuyerAmendCL_10.QBillingDocumentBuyerAmendCL_DocumentTypeDescription;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelByQBillingDocumentBuyerAmendCL_9(QueryBuyerAmendCreditLimit model, QBillingDocumentBuyerAmendCL qBillingDocumentBuyerAmendCL_9)
        {
            try
            {
                model.QBillingDocumentBuyerAmendCL_DocumentTypeDescription_9 = qBillingDocumentBuyerAmendCL_9.QBillingDocumentBuyerAmendCL_DocumentTypeDescription;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelByQBillingDocumentBuyerAmendCL_8(QueryBuyerAmendCreditLimit model, QBillingDocumentBuyerAmendCL qBillingDocumentBuyerAmendCL_8)
        {
            try
            {
                model.QBillingDocumentBuyerAmendCL_DocumentTypeDescription_8 = qBillingDocumentBuyerAmendCL_8.QBillingDocumentBuyerAmendCL_DocumentTypeDescription;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelByQBillingDocumentBuyerAmendCL_7(QueryBuyerAmendCreditLimit model, QBillingDocumentBuyerAmendCL qBillingDocumentBuyerAmendCL_7)
        {
            try
            {
                model.QBillingDocumentBuyerAmendCL_DocumentTypeDescription_7 = qBillingDocumentBuyerAmendCL_7.QBillingDocumentBuyerAmendCL_DocumentTypeDescription;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelByQBillingDocumentBuyerAmendCL_6(QueryBuyerAmendCreditLimit model, QBillingDocumentBuyerAmendCL qBillingDocumentBuyerAmendCL_6)
        {
            try
            {
                model.QBillingDocumentBuyerAmendCL_DocumentTypeDescription_6 = qBillingDocumentBuyerAmendCL_6.QBillingDocumentBuyerAmendCL_DocumentTypeDescription;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelByQBillingDocumentBuyerAmendCL_5(QueryBuyerAmendCreditLimit model, QBillingDocumentBuyerAmendCL qBillingDocumentBuyerAmendCL_5)
        {
            try
            {
                model.QBillingDocumentBuyerAmendCL_DocumentTypeDescription_5 = qBillingDocumentBuyerAmendCL_5.QBillingDocumentBuyerAmendCL_DocumentTypeDescription;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelByQBillingDocumentBuyerAmendCL_4(QueryBuyerAmendCreditLimit model, QBillingDocumentBuyerAmendCL qBillingDocumentBuyerAmendCL_4)
        {
            try
            {
                model.QBillingDocumentBuyerAmendCL_DocumentTypeDescription_4 = qBillingDocumentBuyerAmendCL_4.QBillingDocumentBuyerAmendCL_DocumentTypeDescription;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelByQBillingDocumentBuyerAmendCL_3(QueryBuyerAmendCreditLimit model, QBillingDocumentBuyerAmendCL qBillingDocumentBuyerAmendCL_3)
        {
            try
            {
                model.QBillingDocumentBuyerAmendCL_DocumentTypeDescription_3 = qBillingDocumentBuyerAmendCL_3.QBillingDocumentBuyerAmendCL_DocumentTypeDescription;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelByQBillingDocumentBuyerAmendCL_2(QueryBuyerAmendCreditLimit model, QBillingDocumentBuyerAmendCL qBillingDocumentBuyerAmendCL_2)
        {
            try
            {
                model.QBillingDocumentBuyerAmendCL_DocumentTypeDescription_2 = qBillingDocumentBuyerAmendCL_2.QBillingDocumentBuyerAmendCL_DocumentTypeDescription;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelByQBillingDocumentBuyerAmendCL_1(QueryBuyerAmendCreditLimit model, QBillingDocumentBuyerAmendCL qBillingDocumentBuyerAmendCL_1)
        {
            try
            {
                model.QBillingDocumentBuyerAmendCL_DocumentTypeDescription_1 = qBillingDocumentBuyerAmendCL_1.QBillingDocumentBuyerAmendCL_DocumentTypeDescription;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private void SetModelByQCreditAppReqLineFinBuyerAmendCL_3(QueryBuyerAmendCreditLimit model, QCreditAppReqLineFinBuyerAmendCL qCreditAppReqLineFinBuyerAmendCL_3)
        {
            try
            {
                model.QCreditAppReqLineFinBuyerAmendCL_Year_3 = qCreditAppReqLineFinBuyerAmendCL_3.QCreditAppReqLineFinBuyerAmendCL_Year;
                model.QCreditAppReqLineFinBuyerAmendCL_RegisteredCapital_3 = qCreditAppReqLineFinBuyerAmendCL_3.QCreditAppReqLineFinBuyerAmendCL_RegisteredCapital;
                model.QCreditAppReqLineFinBuyerAmendCL_PaidCapital_3 = qCreditAppReqLineFinBuyerAmendCL_3.QCreditAppReqLineFinBuyerAmendCL_PaidCapital;
                model.QCreditAppReqLineFinBuyerAmendCL_TotalAsset_3 = qCreditAppReqLineFinBuyerAmendCL_3.QCreditAppReqLineFinBuyerAmendCL_TotalAsset;
                model.QCreditAppReqLineFinBuyerAmendCL_TotalLiability_3 = qCreditAppReqLineFinBuyerAmendCL_3.QCreditAppReqLineFinBuyerAmendCL_TotalLiability;
                model.QCreditAppReqLineFinBuyerAmendCL_TotalEquity_3 = qCreditAppReqLineFinBuyerAmendCL_3.QCreditAppReqLineFinBuyerAmendCL_TotalEquity;
                model.QCreditAppReqLineFinBuyerAmendCL_TotalRevenue_3 = qCreditAppReqLineFinBuyerAmendCL_3.QCreditAppReqLineFinBuyerAmendCL_TotalRevenue;
                model.QCreditAppReqLineFinBuyerAmendCL_TotalCOGS_3 = qCreditAppReqLineFinBuyerAmendCL_3.QCreditAppReqLineFinBuyerAmendCL_TotalCOGS;
                model.QCreditAppReqLineFinBuyerAmendCL_TotalGrossProfit_3 = qCreditAppReqLineFinBuyerAmendCL_3.QCreditAppReqLineFinBuyerAmendCL_TotalGrossProfit;
                model.QCreditAppReqLineFinBuyerAmendCL_TotalOperExpFirst_3 = qCreditAppReqLineFinBuyerAmendCL_3.QCreditAppReqLineFinBuyerAmendCL_TotalOperExpFirst;
                model.QCreditAppReqLineFinBuyerAmendCL_TotalNetProfitFirst_3 = qCreditAppReqLineFinBuyerAmendCL_3.QCreditAppReqLineFinBuyerAmendCL_TotalNetProfitFirst;
                model.QCreditAppReqLineFinBuyerAmendCL_NetProfitPercent_3 = qCreditAppReqLineFinBuyerAmendCL_3.QCreditAppReqLineFinBuyerAmendCL_NetProfitPercent;
                model.QCreditAppReqLineFinBuyerAmendCL_lDE_3 = qCreditAppReqLineFinBuyerAmendCL_3.QCreditAppReqLineFinBuyerAmendCL_lDE;
                model.QCreditAppReqLineFinBuyerAmendCL_QuickRatio_3 = qCreditAppReqLineFinBuyerAmendCL_3.QCreditAppReqLineFinBuyerAmendCL_QuickRatio;
                model.QCreditAppReqLineFinBuyerAmendCL_IntCoverageRatio_3 = qCreditAppReqLineFinBuyerAmendCL_3.QCreditAppReqLineFinBuyerAmendCL_IntCoverageRatio;
            }
            catch (Exception e)
            {

                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelByQCreditAppReqLineFinBuyerAmendCL_2(QueryBuyerAmendCreditLimit model, QCreditAppReqLineFinBuyerAmendCL qCreditAppReqLineFinBuyerAmendCL_2)
        {
            try
            {
                model.QCreditAppReqLineFinBuyerAmendCL_Year_2 = qCreditAppReqLineFinBuyerAmendCL_2.QCreditAppReqLineFinBuyerAmendCL_Year;
                model.QCreditAppReqLineFinBuyerAmendCL_RegisteredCapital_2 = qCreditAppReqLineFinBuyerAmendCL_2.QCreditAppReqLineFinBuyerAmendCL_RegisteredCapital;
                model.QCreditAppReqLineFinBuyerAmendCL_PaidCapital_2 = qCreditAppReqLineFinBuyerAmendCL_2.QCreditAppReqLineFinBuyerAmendCL_PaidCapital;
                model.QCreditAppReqLineFinBuyerAmendCL_TotalAsset_2 = qCreditAppReqLineFinBuyerAmendCL_2.QCreditAppReqLineFinBuyerAmendCL_TotalAsset;
                model.QCreditAppReqLineFinBuyerAmendCL_TotalLiability_2 = qCreditAppReqLineFinBuyerAmendCL_2.QCreditAppReqLineFinBuyerAmendCL_TotalLiability;
                model.QCreditAppReqLineFinBuyerAmendCL_TotalEquity_2 = qCreditAppReqLineFinBuyerAmendCL_2.QCreditAppReqLineFinBuyerAmendCL_TotalEquity;
                model.QCreditAppReqLineFinBuyerAmendCL_TotalRevenue_2 = qCreditAppReqLineFinBuyerAmendCL_2.QCreditAppReqLineFinBuyerAmendCL_TotalRevenue;
                model.QCreditAppReqLineFinBuyerAmendCL_TotalCOGS_2 = qCreditAppReqLineFinBuyerAmendCL_2.QCreditAppReqLineFinBuyerAmendCL_TotalCOGS;
                model.QCreditAppReqLineFinBuyerAmendCL_TotalGrossProfit_2 = qCreditAppReqLineFinBuyerAmendCL_2.QCreditAppReqLineFinBuyerAmendCL_TotalGrossProfit;
                model.QCreditAppReqLineFinBuyerAmendCL_TotalOperExpFirst_2 = qCreditAppReqLineFinBuyerAmendCL_2.QCreditAppReqLineFinBuyerAmendCL_TotalOperExpFirst;
                model.QCreditAppReqLineFinBuyerAmendCL_TotalNetProfitFirst_2 = qCreditAppReqLineFinBuyerAmendCL_2.QCreditAppReqLineFinBuyerAmendCL_TotalNetProfitFirst;
                model.QCreditAppReqLineFinBuyerAmendCL_NetProfitPercent_2 = qCreditAppReqLineFinBuyerAmendCL_2.QCreditAppReqLineFinBuyerAmendCL_NetProfitPercent;
                model.QCreditAppReqLineFinBuyerAmendCL_lDE_2 = qCreditAppReqLineFinBuyerAmendCL_2.QCreditAppReqLineFinBuyerAmendCL_lDE;
                model.QCreditAppReqLineFinBuyerAmendCL_QuickRatio_2 = qCreditAppReqLineFinBuyerAmendCL_2.QCreditAppReqLineFinBuyerAmendCL_QuickRatio;
                model.QCreditAppReqLineFinBuyerAmendCL_IntCoverageRatio_2 = qCreditAppReqLineFinBuyerAmendCL_2.QCreditAppReqLineFinBuyerAmendCL_IntCoverageRatio;
            }
            catch (Exception e)
            {

                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetModelByQCreditAppReqLineFinBuyerAmendCL_1(QueryBuyerAmendCreditLimit model, QCreditAppReqLineFinBuyerAmendCL qCreditAppReqLineFinBuyerAmendCL_1)
        {
            try
            {
                model.QCreditAppReqLineFinBuyerAmendCL_Year_1 = qCreditAppReqLineFinBuyerAmendCL_1.QCreditAppReqLineFinBuyerAmendCL_Year;
                model.QCreditAppReqLineFinBuyerAmendCL_RegisteredCapital_1 = qCreditAppReqLineFinBuyerAmendCL_1.QCreditAppReqLineFinBuyerAmendCL_RegisteredCapital;
                model.QCreditAppReqLineFinBuyerAmendCL_PaidCapital_1 = qCreditAppReqLineFinBuyerAmendCL_1.QCreditAppReqLineFinBuyerAmendCL_PaidCapital;
                model.QCreditAppReqLineFinBuyerAmendCL_TotalAsset_1 = qCreditAppReqLineFinBuyerAmendCL_1.QCreditAppReqLineFinBuyerAmendCL_TotalAsset;
                model.QCreditAppReqLineFinBuyerAmendCL_TotalLiability_1 = qCreditAppReqLineFinBuyerAmendCL_1.QCreditAppReqLineFinBuyerAmendCL_TotalLiability;
                model.QCreditAppReqLineFinBuyerAmendCL_TotalEquity_1 = qCreditAppReqLineFinBuyerAmendCL_1.QCreditAppReqLineFinBuyerAmendCL_TotalEquity;
                model.QCreditAppReqLineFinBuyerAmendCL_TotalRevenue_1 = qCreditAppReqLineFinBuyerAmendCL_1.QCreditAppReqLineFinBuyerAmendCL_TotalRevenue;
                model.QCreditAppReqLineFinBuyerAmendCL_TotalCOGS_1 = qCreditAppReqLineFinBuyerAmendCL_1.QCreditAppReqLineFinBuyerAmendCL_TotalCOGS;
                model.QCreditAppReqLineFinBuyerAmendCL_TotalGrossProfit_1 = qCreditAppReqLineFinBuyerAmendCL_1.QCreditAppReqLineFinBuyerAmendCL_TotalGrossProfit;
                model.QCreditAppReqLineFinBuyerAmendCL_TotalOperExpFirst_1 = qCreditAppReqLineFinBuyerAmendCL_1.QCreditAppReqLineFinBuyerAmendCL_TotalOperExpFirst;
                model.QCreditAppReqLineFinBuyerAmendCL_TotalNetProfitFirst_1 = qCreditAppReqLineFinBuyerAmendCL_1.QCreditAppReqLineFinBuyerAmendCL_TotalNetProfitFirst;
                model.QCreditAppReqLineFinBuyerAmendCL_NetProfitPercent_1 = qCreditAppReqLineFinBuyerAmendCL_1.QCreditAppReqLineFinBuyerAmendCL_NetProfitPercent;
                model.QCreditAppReqLineFinBuyerAmendCL_lDE_1 = qCreditAppReqLineFinBuyerAmendCL_1.QCreditAppReqLineFinBuyerAmendCL_lDE;
                model.QCreditAppReqLineFinBuyerAmendCL_QuickRatio_1 = qCreditAppReqLineFinBuyerAmendCL_1.QCreditAppReqLineFinBuyerAmendCL_QuickRatio;
                model.QCreditAppReqLineFinBuyerAmendCL_IntCoverageRatio_1 = qCreditAppReqLineFinBuyerAmendCL_1.QCreditAppReqLineFinBuyerAmendCL_IntCoverageRatio;
            }
            catch (Exception e)
            {

                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private void SetModelByQReceiptContactPersonBuyerAmendCL(QueryBuyerAmendCreditLimit model, QReceiptContactPersonBuyerAmendCL qReceiptContactPersonBuyerAmendCL)
        {
            try
            {
                model.QReceiptContactPersonBuyerAmendCL_Name = qReceiptContactPersonBuyerAmendCL.QReceiptContactPersonBuyerAmendCL_Name;
                model.QReceiptContactPersonBuyerAmendCL_Phone = qReceiptContactPersonBuyerAmendCL.QReceiptContactPersonBuyerAmendCL_Phone;
                model.QReceiptContactPersonBuyerAmendCL_Position = qReceiptContactPersonBuyerAmendCL.QReceiptContactPersonBuyerAmendCL_Position;
                model.QReceiptContactPersonBuyerAmendCL_ContactPersonTrans = qReceiptContactPersonBuyerAmendCL.QReceiptContactPersonBuyerAmendCL_ContactPersonTrans;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private void SetModelByQBillingContactPersonBuyerAmendCL(QueryBuyerAmendCreditLimit model, QBillingContactPersonBuyerAmendCL qBillingContactPersonBuyerAmendCL)
        {
            try
            {
                model.QBillingContactPersonBuyerAmendCL_Name = qBillingContactPersonBuyerAmendCL.QBillingContactPersonBuyerAmendCL_Name;
                model.QBillingContactPersonBuyerAmendCL_Phone = qBillingContactPersonBuyerAmendCL.QBillingContactPersonBuyerAmendCL_Phone;
                model.QBillingContactPersonBuyerAmendCL_Position = qBillingContactPersonBuyerAmendCL.QBillingContactPersonBuyerAmendCL_Position;
                model.QBillingContactPersonBuyerAmendCL_ContactPersonTrans = qBillingContactPersonBuyerAmendCL.QBillingContactPersonBuyerAmendCL_ContactPersonTrans;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private void SetModelByQCAReqLineAmendBuyerAmendCL(QueryBuyerAmendCreditLimit model, QCAReqLineAmendBuyerAmendCL qCAReqLineAmendBuyerAmendCL)
        {
            try
            {
                model.QCAReqLineAmendBuyerAmendCL_OriginalCreditLimitLineRequest = qCAReqLineAmendBuyerAmendCL.QCAReqLineAmendBuyerAmendCL_OriginalCreditLimitLineRequest;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private void SetModelByQCAReqTableBuyerAmendCL(QueryBuyerAmendCreditLimit model, QCAReqTableBuyerAmendCL qCAReqTableBuyerAmendCL)
        {
            try
            {
                model.QCAReqTableBuyerAmendCL_PurchaseFeeCalculateBase = SystemStaticData.GetTranslatedMessage(((PurchaseFeeCalculateBase)qCAReqTableBuyerAmendCL.QCAReqTableBuyerAmendCL_PurchaseFeeCalculateBase).GetAttrCode());
                model.QCAReqTableBuyerAmendCL_CreditAppRequestId = qCAReqTableBuyerAmendCL.QCAReqTableBuyerAmendCL_CreditAppRequestId;
                model.QCAReqTableBuyerAmendCL_RefCreditAppTableGUID = qCAReqTableBuyerAmendCL.QCAReqTableBuyerAmendCL_RefCreditAppTableGUID;
                model.QCAReqTableBuyerAmendCL_OriginalCreditAppTableId = qCAReqTableBuyerAmendCL.QCAReqTableBuyerAmendCL_OriginalCreditAppTableId;
                model.QCAReqTableBuyerAmendCL_DocumentStatusGUID = qCAReqTableBuyerAmendCL.QCAReqTableBuyerAmendCL_DocumentStatusGUID;
                model.QCAReqTableBuyerAmendCL_Status = qCAReqTableBuyerAmendCL.QCAReqTableBuyerAmendCL_Status;
                model.QCAReqTableBuyerAmendCL_RequestDate = qCAReqTableBuyerAmendCL.QCAReqTableBuyerAmendCL_RequestDate;
                model.QCAReqTableBuyerAmendCL_Description = qCAReqTableBuyerAmendCL.QCAReqTableBuyerAmendCL_Description;
                model.QCAReqTableBuyerAmendCL_Remark = qCAReqTableBuyerAmendCL.QCAReqTableBuyerAmendCL_Remark;
                model.QCAReqLineBuyerAmendCL_CustomerId = qCAReqTableBuyerAmendCL.QCAReqLineBuyerAmendCL_CustomerId;
                model.QCAReqLineBuyerAmendCL_CustomerName = qCAReqTableBuyerAmendCL.QCAReqLineBuyerAmendCL_CustomerName;
                model.QCAReqLineBuyerAmendCL_ResponsibleBy = qCAReqTableBuyerAmendCL.QCAReqLineBuyerAmendCL_ResponsibleBy;
                model.QCAReqTableBuyerAmendCL_CreditScoring = qCAReqTableBuyerAmendCL.QCAReqTableBuyerAmendCL_CreditScoring;
                model.QCAReqLineAmendBuyerAmendCL_CACondition = qCAReqTableBuyerAmendCL.QCAReqLineAmendBuyerAmendCL_CACondition;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private void SetModelByQCAReqLineBuyerAmendCL(QueryBuyerAmendCreditLimit model, QCAReqLineBuyerAmendCL qCAReqLineBuyerAmendCL)
        {
            try
            {
                model.QCAReqLineBuyerAmendCL_CreditAppRequestTableGUID = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_CreditAppRequestTableGUID;
                model.QCAReqLineBuyerAmendCL_BuyerTableGUID = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_BuyerTableGUID;
                model.QCAReqLineBuyerAmendCL_BuyerName = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_BuyerName;
                model.QCAReqLineBuyerAmendCL_BusinessSegmentGUID = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_BusinessSegmentGUID;
                model.QCAReqLineBuyerAmendCL_BusinessSegment = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_BusinessSegment;
                model.QCAReqLineBuyerAmendCL_TaxIdPassportId = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_IdentificationType == 0 ? qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_TaxId : qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_PassportId;
                model.QCAReqLineBuyerAmendCL_InvoiceAddressGUID = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_InvoiceAddressGUID;
                model.QCAReqLineBuyerAmendCL_InvoiceAddress = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_InvoiceAddress;
                model.QCAReqLineBuyerAmendCL_DateOfEstablish = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_DateOfEstablish;
                model.QCAReqLineBuyerAmendCL_LineOfBusinessGUID = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_LineOfBusinessGUID;
                model.QCAReqLineBuyerAmendCL_LineOfBusiness = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_LineOfBusiness;
                model.QCAReqLineBuyerAmendCL_CreditScoringGUID = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_CreditScoringGUID;
                model.QCAReqLineBuyerAmendCL_CreditScoring = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_CreditScoring;
                model.QCAReqLineBuyerAmendCL_BuyerCreditLimit = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_BuyerCreditLimit;
                model.QCAReqLineBuyerAmendCL_CustomerBuyerOutstanding = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_CustomerBuyerOutstanding;
                model.QCAReqLineBuyerAmendCL_AllCustomerBuyerOutstanding = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_AllCustomerBuyerOutstanding;
                model.QCAReqLineBuyerAmendCL_PurchaseFeePct = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_PurchaseFeePct;
                //model.QCAReqLineBuyerAmendCL_PurchaseFeeCalculateBase = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_PurchaseFeeCalculateBase;
                model.QCAReqLineBuyerAmendCL_BillingResponsibleByGUID = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_BillingResponsibleByGUID;
                //model.QCAReqLineBuyerAmendCL_BillingResponsibleBy = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_BillingResponsibleBy;
                model.QCAReqLineBuyerAmendCL_AcceptanceDocument = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_AcceptanceDocument;
                model.QCAReqLineBuyerAmendCL_BillingContactPersonGUID = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_BillingContactPersonGUID;
                model.QCAReqLineBuyerAmendCL_BillingAddressGUID = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_BillingAddressGUID;
                model.QCAReqLineBuyerAmendCL_BillingAddress = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_BillingAddress;
                model.QCAReqLineBuyerAmendCL_MethodOfPaymentGUID = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_MethodOfPaymentGUID;
                model.QCAReqLineBuyerAmendCL_MethodOfPayment = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_MethodOfPayment;
                model.QCAReqLineBuyerAmendCL_ReceiptDescription = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_ReceiptDescription;
                model.QCAReqLineBuyerAmendCL_ReceiptContactPersonGUID = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_ReceiptContactPersonGUID;
                model.QCAReqLineBuyerAmendCL_ReceiptAddressGUID = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_ReceiptAddressGUID;
                model.QCAReqLineBuyerAmendCL_ReceiptAddress = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_ReceiptAddress;
                model.QCAReqLineBuyerAmendCL_MarketingComment = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_MarketingComment;
                model.QCAReqLineBuyerAmendCL_CreditComment = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_CreditComment;
                model.QCAReqLineBuyerAmendCL_ApproverComment = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_ApproverComment;
                model.QCAReqLineBuyerAmendCL_CreditLimitLineRequest = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_CreditLimitLineRequest;
                model.QCAReqLineBuyerAmendCL_BillingDescription = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_BillingDescription;
                model.QCAReqLineBuyerAmendCL_BlacklistStatus = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_BlacklistStatus;
                model.QCAReqLineBuyerAmendCL_KYCSetup = qCAReqLineBuyerAmendCL.QCAReqLineBuyerAmendCL_KYCSetup;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private QActionHistoryBuyerAmendCL GetQActionHistoryBuyerAmendCLEachRow(List<QActionHistoryBuyerAmendCL> qActionHistoryBuyerAmendCL, int row_number)
        {
            try
            {
                var result = qActionHistoryBuyerAmendCL.Where(w => w.QActionHistoryBuyerAmendCL_Row == row_number).FirstOrDefault();
                if (result != null)
                {
                    return result;
                }
                else
                {
                    return new QActionHistoryBuyerAmendCL();

                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private List<QActionHistoryBuyerAmendCL> GettQActionHistoryBuyerAmendCL(Guid qCAReqTableBuyerAmendCL_CrediAppReqGUID)
        {
            try
            {
                var result = (from actionhis in db.Set<ActionHistory>()
                              where (actionhis.ActivityName == "Assist MD approve" || actionhis.ActivityName == "Board 2 in 3 approve" || actionhis.ActivityName == "Board 3 in 4 approve")
                              && actionhis.ActionName == "Approve" && actionhis.RefGUID == qCAReqTableBuyerAmendCL_CrediAppReqGUID
                              select new QActionHistoryBuyerAmendCL
                              {
                                  QActionHistoryBuyerAmendCL_Comment = actionhis.Comment,
                                  QActionHistoryBuyerAmendCL_CreatedDateTime = actionhis.CreatedDateTime,
                                  QActionHistoryBuyerAmendCL_ActionName = actionhis.ActionName,
                                  QActionHistoryBuyerAmendCL_ApproverName = actionhis.CreatedBy,
                                  QActionHistoryBuyerAmendCL_Row = 0
                              }).Take(5).ToList();
                var row_number = 0;
                foreach (var item in result)
                {
                    item.QActionHistoryBuyerAmendCL_Row = row_number + 1;
                    row_number = row_number + 1;
                }
                if (result != null)
                {
                    return result;
                }
                else
                {
                    return new List<QActionHistoryBuyerAmendCL>();

                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private decimal GetSunQSumServiceFeeConditionLineBuyerAmendCL(Guid creditAppReqLineGUID)
        {
            try
            {
                var result = (from serviceFeeTrans in db.Set<ServiceFeeConditionTrans>()
                              where serviceFeeTrans.RefGUID == creditAppReqLineGUID && serviceFeeTrans.Inactive == false && serviceFeeTrans.RefType == (int)RefType.CreditAppRequestLine
                              select new QSumServiceFeeConditionLineBuyerAmendCL
                              {
                                  QSumServiceFeeConditionLineBuyerAmendCL_SumServiceFeeAmt = serviceFeeTrans.AmountBeforeTax
                              }).Sum(su => su.QSumServiceFeeConditionLineBuyerAmendCL_SumServiceFeeAmt);
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private QServiceFeeConditionLineBuyerAmendCL GetQServiceFeeConditionLineBuyerAmendCLEachRow(List<QServiceFeeConditionLineBuyerAmendCL> qServiceFeeConditionLineBuyerAmendCL, int row_number)
        {
            try
            {

                var result = qServiceFeeConditionLineBuyerAmendCL.Where(w => w.QServiceFeeConditionLineBuyerAmendCL_Row == row_number).FirstOrDefault();
                if (result != null)
                {
                    return result;
                }
                else
                {
                    return new QServiceFeeConditionLineBuyerAmendCL();

                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private List<QServiceFeeConditionLineBuyerAmendCL> GetQServiceFeeConditionLineBuyerAmendCL(Guid creditAppReqLineGUID)
        {
            try
            {
                var result = (from serviceFeeConTrans in db.Set<ServiceFeeConditionTrans>()
                              join invoiceRevenueType in db.Set<InvoiceRevenueType>()
                              on serviceFeeConTrans.InvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID into ljinvoiceRevenueType
                              from invoiceRevenueType in ljinvoiceRevenueType.DefaultIfEmpty()
                              where serviceFeeConTrans.RefGUID == creditAppReqLineGUID && serviceFeeConTrans.Inactive == false
                              && serviceFeeConTrans.RefType == (int)RefType.CreditAppRequestLine
                              orderby serviceFeeConTrans.Ordering
                              select new QServiceFeeConditionLineBuyerAmendCL
                              {
                                  QServiceFeeConditionLineBuyerAmendCL_Ordering = serviceFeeConTrans.Ordering,
                                  QServiceFeeConditionLineBuyerAmendCL_Description = serviceFeeConTrans.Description,
                                  QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax = serviceFeeConTrans.AmountBeforeTax,
                                  QServiceFeeConditionLineBuyerAmendCL_ServicefeeType = invoiceRevenueType.Description,
                                  QServiceFeeConditionLineBuyerAmendCL_Row = 0
                              }).Take(10).ToList();
                var row_number = 0;
                foreach (var item in result)
                {
                    item.QServiceFeeConditionLineBuyerAmendCL_Row = row_number + 1;
                    row_number = row_number + 1;
                }
                if (result != null)
                {
                    return result;
                }
                else
                {
                    return new List<QServiceFeeConditionLineBuyerAmendCL>();
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private QReceiptDocumentConditionBuyerAmendCL GetQReceiptDocumentConditionBuyerAmendCLEachRow(List<QReceiptDocumentConditionBuyerAmendCL> qReceiptDocumentConditionBuyerAmendCL, int row_number)
        {
            try
            {
                var result = qReceiptDocumentConditionBuyerAmendCL.Where(w => w.QReceiptDocumentConditionBuyerAmendCL_Row == row_number).FirstOrDefault();
                if (result != null)
                {
                    return result;
                }
                else
                {
                    return new QReceiptDocumentConditionBuyerAmendCL();

                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private List<QReceiptDocumentConditionBuyerAmendCL> GetQReceiptDocumentConditionBuyerAmendCL(Guid? creditAppReqLineGUID)
        {
            try
            {
                var result = (from docConTrans in db.Set<DocumentConditionTrans>()
                              join docType in db.Set<DocumentType>()
                              on docConTrans.DocumentTypeGUID equals docType.DocumentTypeGUID into ljdocType
                              from docType in ljdocType.DefaultIfEmpty()
                              where docConTrans.RefType == (int)RefType.CreditAppLine && docConTrans.RefGUID == creditAppReqLineGUID
                              && docConTrans.DocConVerifyType == (int)DocConVerifyType.Receipt
                              && docConTrans.Inactive == false
                              select new QReceiptDocumentConditionBuyerAmendCL
                              {
                                  QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription = docType.Description,
                                  QReceiptDocumentConditionBuyerAmendCL_Row = 0
                              }).Take(10).ToList();
                var row_number = 0;
                foreach (var item in result)
                {
                    item.QReceiptDocumentConditionBuyerAmendCL_Row = row_number + 1;
                    row_number = row_number + 1;
                }
                if (result != null)
                {
                    return result;
                }
                else
                {
                    return new List<QReceiptDocumentConditionBuyerAmendCL>();
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private QBillingDocumentBuyerAmendCL GetQBillingDocumentBuyerAmendCLEachRow(List<QBillingDocumentBuyerAmendCL> qBillingDocumentBuyerAmendCL, int row_number)
        {
            try
            {
                var result = qBillingDocumentBuyerAmendCL.Where(w => w.QBillingDocumentBuyerAmendCL_Row == row_number).FirstOrDefault();
                if (result != null)
                {
                    return result;
                }
                else
                {
                    return new QBillingDocumentBuyerAmendCL();

                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private List<QBillingDocumentBuyerAmendCL> GetQBillingDocumentBuyerAmendCL(Guid? creditAppReqLineGUID)
        {
            try
            {
                var result = (from docConTrans in db.Set<DocumentConditionTrans>()
                              join docType in db.Set<DocumentType>()
                              on docConTrans.DocumentTypeGUID equals docType.DocumentTypeGUID into ljdocType
                              from docType in ljdocType.DefaultIfEmpty()
                              where docConTrans.RefGUID == creditAppReqLineGUID

                              && docConTrans.RefType == (int)RefType.CreditAppLine
                              && docConTrans.Inactive == false
                              && docConTrans.DocConVerifyType == (int)DocConVerifyType.Billing
                              select new QBillingDocumentBuyerAmendCL
                              {
                                  QBillingDocumentBuyerAmendCL_DocumentTypeDescription = docType.Description,
                                  QBillingDocumentBuyerAmendCL_Row = 0
                              }).Take(10).ToList();
                var row_number = 0;
                foreach (var item in result)
                {
                    item.QBillingDocumentBuyerAmendCL_Row = row_number + 1;
                    row_number = row_number + 1;
                }
                if (result != null)
                {
                    return result;
                }
                else
                {
                    return new List<QBillingDocumentBuyerAmendCL>();

                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private QCreditAppReqLineFinBuyerAmendCL GetQCreditAppReqLineFinBuyerAmendCLEachYear(List<FinancialStatementTrans> financialStatementTrans, int year)
        {
            try
            {
                List<FinancialStatementTrans> financialStatementTransByYear = financialStatementTrans.Where(w => w.Year == year).ToList();
                QCreditAppReqLineFinBuyerAmendCL model = new QCreditAppReqLineFinBuyerAmendCL();
                {
                    model.QCreditAppReqLineFinBuyerAmendCL_Year = year;
                    model.QCreditAppReqLineFinBuyerAmendCL_RegisteredCapital = GetFinanceStatementAmount(1, financialStatementTransByYear);
                    model.QCreditAppReqLineFinBuyerAmendCL_PaidCapital = GetFinanceStatementAmount(2, financialStatementTransByYear);
                    model.QCreditAppReqLineFinBuyerAmendCL_TotalAsset = GetFinanceStatementAmount(3, financialStatementTransByYear);
                    model.QCreditAppReqLineFinBuyerAmendCL_TotalLiability = GetFinanceStatementAmount(4, financialStatementTransByYear);
                    model.QCreditAppReqLineFinBuyerAmendCL_TotalEquity = GetFinanceStatementAmount(5, financialStatementTransByYear);
                    model.QCreditAppReqLineFinBuyerAmendCL_TotalRevenue = GetFinanceStatementAmount(6, financialStatementTransByYear);
                    model.QCreditAppReqLineFinBuyerAmendCL_TotalCOGS = GetFinanceStatementAmount(7, financialStatementTransByYear);
                    model.QCreditAppReqLineFinBuyerAmendCL_TotalGrossProfit = GetFinanceStatementAmount(8, financialStatementTransByYear);
                    model.QCreditAppReqLineFinBuyerAmendCL_TotalOperExpFirst = GetFinanceStatementAmount(9, financialStatementTransByYear);
                    model.QCreditAppReqLineFinBuyerAmendCL_TotalNetProfitFirst = GetFinanceStatementAmount(10, financialStatementTransByYear);
                    model.QCreditAppReqLineFinBuyerAmendCL_NetProfitPercent = GetFinanceStatementAmount(11, financialStatementTransByYear);
                    model.QCreditAppReqLineFinBuyerAmendCL_lDE = GetFinanceStatementAmount(12, financialStatementTransByYear);
                    model.QCreditAppReqLineFinBuyerAmendCL_QuickRatio = GetFinanceStatementAmount(13, financialStatementTransByYear);
                    model.QCreditAppReqLineFinBuyerAmendCL_IntCoverageRatio = GetFinanceStatementAmount(14, financialStatementTransByYear);
                };

                return model;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private decimal GetFinanceStatementAmount(int ordering, List<FinancialStatementTrans> financialStatementTrans)
        {
            try
            {
                var result = financialStatementTrans.Where(w => w.Ordering == ordering).Select(s => s.Amount).FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private void SetModelQCreditAppReqLineFinBuyerAmendCL(QueryBuyerAmendCreditLimit model, Guid creditAppReqLineGUID)
        {
            try
            {
                IFinancialStatementTransRepo financialStatmentTransRepo = new FinancialStatementTransRepo(db);
                List<FinancialStatementTrans> financialStatmentTrans = financialStatmentTransRepo.GetFinancialStatementTransByRefernece(creditAppReqLineGUID, (int)RefType.CreditAppRequestLine)
                                                                                                     .OrderByDescending(o => o.Year).ThenBy(o => o.Ordering).ToList();
                List<DistinctYear> yearDistinctList = financialStatmentTrans.GroupBy(g => g.Year).Select(s => new DistinctYear
                {
                    Year = s.Key
                }).Take(3).ToList();

                if (yearDistinctList.ElementAtOrDefault(0) != null)
                {
                    QCreditAppReqLineFinBuyerAmendCL qCreditAppReqLineFinBuyerAmendCL_1 = GetQCreditAppReqLineFinBuyerAmendCLEachYear(financialStatmentTrans, yearDistinctList[0].Year);
                    SetModelByQCreditAppReqLineFinBuyerAmendCL_1(model, qCreditAppReqLineFinBuyerAmendCL_1);
                }
                if (yearDistinctList.ElementAtOrDefault(1) != null)
                {
                    QCreditAppReqLineFinBuyerAmendCL qCreditAppReqLineFinBuyerAmendCL_2 = GetQCreditAppReqLineFinBuyerAmendCLEachYear(financialStatmentTrans, yearDistinctList[1].Year);
                    SetModelByQCreditAppReqLineFinBuyerAmendCL_2(model, qCreditAppReqLineFinBuyerAmendCL_2);
                }
                if (yearDistinctList.ElementAtOrDefault(2) != null)
                {
                    QCreditAppReqLineFinBuyerAmendCL qCreditAppReqLineFinBuyerAmendCL_3 = GetQCreditAppReqLineFinBuyerAmendCLEachYear(financialStatmentTrans, yearDistinctList[2].Year);
                    SetModelByQCreditAppReqLineFinBuyerAmendCL_3(model, qCreditAppReqLineFinBuyerAmendCL_3);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private QReceiptContactPersonBuyerAmendCL GetQReceiptContactPersonBuyerAmendCL(Guid? qCAReqLineBuyerAmendCL_ReceiptContactPersonGUID)
        {
            try
            {
                var result = (from contactPSTrans in db.Set<ContactPersonTrans>()
                              join realatedPSTable in db.Set<RelatedPersonTable>()
                              on contactPSTrans.RelatedPersonTableGUID equals realatedPSTable.RelatedPersonTableGUID into ljrealatedPSTable
                              from realatedPSTable in ljrealatedPSTable.DefaultIfEmpty()
                              where contactPSTrans.ContactPersonTransGUID == qCAReqLineBuyerAmendCL_ReceiptContactPersonGUID
                              select new QReceiptContactPersonBuyerAmendCL
                              {
                                  QReceiptContactPersonBuyerAmendCL_Name = realatedPSTable.Name,
                                  QReceiptContactPersonBuyerAmendCL_Phone = realatedPSTable.Phone,
                                  QReceiptContactPersonBuyerAmendCL_Position = realatedPSTable.Position,


                              }).FirstOrDefault();
                if (result != null)
                {
                    return result;
                }
                else
                {
                    return new QReceiptContactPersonBuyerAmendCL();
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private QBillingContactPersonBuyerAmendCL GetQBillingContactPersonBuyerAmendCL(Guid? qCAReqLineBuyerAmendCL_BillingContactPersonGUID)
        {
            try
            {
                var result = (from contactPersonTrans in db.Set<ContactPersonTrans>()
                              join relatedPSTable in db.Set<RelatedPersonTable>()
                              on contactPersonTrans.RelatedPersonTableGUID equals relatedPSTable.RelatedPersonTableGUID into ljrelatedPSTable
                              from relatedPSTable in ljrelatedPSTable.DefaultIfEmpty()
                              where contactPersonTrans.ContactPersonTransGUID == qCAReqLineBuyerAmendCL_BillingContactPersonGUID
                              select new QBillingContactPersonBuyerAmendCL
                              {
                                  QBillingContactPersonBuyerAmendCL_Name = relatedPSTable.Name,
                                  QBillingContactPersonBuyerAmendCL_Phone = relatedPSTable.Phone,
                                  QBillingContactPersonBuyerAmendCL_Position = relatedPSTable.Position,

                              }).FirstOrDefault();

                if (result != null)
                {
                    return result;
                }
                else
                {
                    return new QBillingContactPersonBuyerAmendCL();
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private QCAReqLineAmendBuyerAmendCL GetQCAReqLineAmendBuyerAmendCL(Guid creditAppReqLineGUID)
        {
            try
            {
                var result = (from creditappReqLineAmend in db.Set<CreditAppRequestLineAmend>()
                              where creditappReqLineAmend.CreditAppRequestLineGUID == creditAppReqLineGUID
                              select new QCAReqLineAmendBuyerAmendCL
                              {
                                  QCAReqLineAmendBuyerAmendCL_OriginalCreditLimitLineRequest = creditappReqLineAmend.OriginalCreditLimitLineRequest,
                              }).FirstOrDefault();
                if (result != null)
                {
                    return result;
                }
                else
                {
                    return new QCAReqLineAmendBuyerAmendCL();
                }


            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private QCAReqTableBuyerAmendCL GetQCAReqTableBuyerAmendCL(Guid refGUID)
        {
            try
            {
                var result = (from creditappReq in db.Set<CreditAppRequestTable>().Where(w => w.CreditAppRequestTableGUID == refGUID)

                              join creditAppTable in db.Set<CreditAppTable>()
                              on creditappReq.RefCreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljcreditAppTable
                              from creditAppTable in ljcreditAppTable.DefaultIfEmpty()

                              join documentStatus in db.Set<DocumentStatus>()
                              on creditappReq.DocumentStatusGUID equals documentStatus.DocumentStatusGUID

                              join customerTable in db.Set<CustomerTable>()
                              on creditappReq.CustomerTableGUID equals customerTable.CustomerTableGUID into ljcustomerTable
                              from customerTable in ljcustomerTable.DefaultIfEmpty()

                              join employee in db.Set<EmployeeTable>()
                              on customerTable.ResponsibleByGUID equals employee.EmployeeTableGUID into ljemployee
                              from employee in ljemployee.DefaultIfEmpty()

                              join creditScoringTable in db.Set<CreditScoring>()
                              on creditappReq.CreditScoringGUID equals creditScoringTable.CreditScoringGUID into ljcreditScoringTable
                              from creditScoringTable in ljcreditScoringTable.DefaultIfEmpty()
                              select new QCAReqTableBuyerAmendCL

                              {
                                  QCAReqTableBuyerAmendCL_CrediAppReqGUID = creditappReq.CreditAppRequestTableGUID,
                                  QCAReqTableBuyerAmendCL_CreditAppRequestId = creditappReq.CreditAppRequestId,
                                  QCAReqTableBuyerAmendCL_RefCreditAppTableGUID = creditappReq.RefCreditAppTableGUID,
                                  QCAReqTableBuyerAmendCL_RefCreditAppRequestTableGUID = creditAppTable.RefCreditAppRequestTableGUID,
                                  QCAReqTableBuyerAmendCL_OriginalCreditAppTableId = creditAppTable.CreditAppId,
                                  QCAReqTableBuyerAmendCL_DocumentStatusGUID = creditappReq.DocumentStatusGUID,
                                  QCAReqTableBuyerAmendCL_Status = documentStatus.Description,
                                  QCAReqTableBuyerAmendCL_RequestDate = creditappReq.RequestDate,
                                  QCAReqTableBuyerAmendCL_Description = creditappReq.Description,
                                  QCAReqTableBuyerAmendCL_Remark = creditappReq.Remark,
                                  QCAReqLineBuyerAmendCL_CustomerTableGUID = creditappReq.CustomerTableGUID,
                                  QCAReqLineBuyerAmendCL_CustomerId = customerTable.CustomerId,
                                  QCAReqLineBuyerAmendCL_CustomerName = customerTable.Name,
                                  QCAReqLineBuyerAmendCL_ResponsibleByGUID = customerTable.ResponsibleByGUID,
                                  QCAReqLineBuyerAmendCL_ResponsibleBy = employee != null ? employee.Name : "",
                                  QCAReqTableBuyerAmendCL_PurchaseFeeCalculateBase = creditAppTable.PurchaseFeeCalculateBase,
                                  QCAReqTableBuyerAmendCL_CreditScoring = creditScoringTable != null ? creditScoringTable.Description : "",
                                  QCAReqLineAmendBuyerAmendCL_CACondition = creditAppTable.CACondition


                              }).AsNoTracking().FirstOrDefault();


                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private QCAReqLineBuyerAmendCL GetQCAReqLineBuyerAmendCL(Guid creditAppReqTableGUID)
        {
            try
            {
                var result = (from creditAppReqLine in db.Set<CreditAppRequestLine>().Where(w => w.CreditAppRequestTableGUID == creditAppReqTableGUID)
                              join buyerTable in db.Set<BuyerTable>()
                              on creditAppReqLine.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
                              from buyerTable in ljbuyerTable.DefaultIfEmpty()

                              join businessSegment in db.Set<BusinessSegment>()
                              on buyerTable.BusinessSegmentGUID equals businessSegment.BusinessSegmentGUID into ljbusinessSegment
                              from businessSegment in ljbusinessSegment.DefaultIfEmpty()

                              join addressTransInvoice in db.Set<AddressTrans>()
                              on creditAppReqLine.InvoiceAddressGUID equals addressTransInvoice.AddressTransGUID into ljaddressTransInvoice
                              from addressTransInvoice in ljaddressTransInvoice.DefaultIfEmpty()

                              join lineOfBus in db.Set<LineOfBusiness>()
                              on buyerTable.LineOfBusinessGUID equals lineOfBus.LineOfBusinessGUID into ljlineOfBus
                              from lineOfBus in ljlineOfBus.DefaultIfEmpty()

                              join creditScoring in db.Set<CreditScoring>()
                              on creditAppReqLine.CreditScoringGUID equals creditScoring.CreditScoringGUID into ljcreditScoring
                              from creditScoring in ljcreditScoring.DefaultIfEmpty()

                              join addresstransBilling in db.Set<AddressTrans>()
                              on creditAppReqLine.BillingAddressGUID equals addresstransBilling.AddressTransGUID into ljaddresstransBilling
                              from addresstransBilling in ljaddresstransBilling.DefaultIfEmpty()

                              join methodOfPayment in db.Set<MethodOfPayment>()
                              on creditAppReqLine.MethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID into ljmethodOfPayment
                              from methodOfPayment in ljmethodOfPayment.DefaultIfEmpty()

                              join addresstransReceipt in db.Set<AddressTrans>()
                              on creditAppReqLine.ReceiptAddressGUID equals addresstransReceipt.AddressTransGUID into ljaddresstransReceipt
                              from addresstransReceipt in ljaddresstransReceipt.DefaultIfEmpty()

                              join blacklistStatus in db.Set<BlacklistStatus>()
                              on creditAppReqLine.BlacklistStatusGUID equals blacklistStatus.BlacklistStatusGUID into ljblacklistStatus
                              from blacklistStatus in ljblacklistStatus.DefaultIfEmpty()

                              join kycSetup in db.Set<KYCSetup>()
                              on creditAppReqLine.KYCSetupGUID equals kycSetup.KYCSetupGUID into ljkycsetup
                              from kycSetup in ljkycsetup.DefaultIfEmpty()
                              select new QCAReqLineBuyerAmendCL
                              {
                                  QCAReqLineBuyerAmendCL_RefCreditAppLineGUID = creditAppReqLine.RefCreditAppLineGUID,
                                  QCAReqLineBuyerAmendCL_CreditAppRequestLineGUID = creditAppReqLine.CreditAppRequestLineGUID,
                                  QCAReqLineBuyerAmendCL_CreditAppRequestTableGUID = creditAppReqLine.CreditAppRequestTableGUID,
                                  QCAReqLineBuyerAmendCL_BuyerTableGUID = creditAppReqLine.BuyerTableGUID,
                                  QCAReqLineBuyerAmendCL_BuyerName = buyerTable.BuyerName,
                                  QCAReqLineBuyerAmendCL_BusinessSegmentGUID = buyerTable.BusinessSegmentGUID,
                                  QCAReqLineBuyerAmendCL_BusinessSegment = businessSegment.Description,
                                  QCAReqLineBuyerAmendCL_IdentificationType = buyerTable.IdentificationType,
                                  QCAReqLineBuyerAmendCL_TaxId = buyerTable.TaxId,
                                  QCAReqLineBuyerAmendCL_PassportId = buyerTable.PassportId,
                                  QCAReqLineBuyerAmendCL_InvoiceAddressGUID = creditAppReqLine.InvoiceAddressGUID,
                                  QCAReqLineBuyerAmendCL_InvoiceAddress = addressTransInvoice != null ? (addressTransInvoice.Address1 + " " + addressTransInvoice.Address2) : "",
                                  QCAReqLineBuyerAmendCL_DateOfEstablish = buyerTable.DateOfEstablish,
                                  QCAReqLineBuyerAmendCL_LineOfBusinessGUID = buyerTable.LineOfBusinessGUID,
                                  QCAReqLineBuyerAmendCL_LineOfBusiness = lineOfBus != null ? lineOfBus.Description : "",
                                  QCAReqLineBuyerAmendCL_CreditScoringGUID = creditAppReqLine.CreditScoringGUID,
                                  QCAReqLineBuyerAmendCL_CreditScoring = creditScoring != null ? creditScoring.Description : "",
                                  QCAReqLineBuyerAmendCL_BuyerCreditLimit = creditAppReqLine.BuyerCreditLimit,
                                  QCAReqLineBuyerAmendCL_CustomerBuyerOutstanding = creditAppReqLine.CustomerBuyerOutstanding,
                                  QCAReqLineBuyerAmendCL_AllCustomerBuyerOutstanding = creditAppReqLine.AllCustomerBuyerOutstanding,
                                  QCAReqLineBuyerAmendCL_PurchaseFeePct = creditAppReqLine.PurchaseFeePct,
                                  QCAReqLineBuyerAmendCL_PurchaseFeeCalculateBase = creditAppReqLine.PurchaseFeeCalculateBase,
                                  QCAReqLineBuyerAmendCL_AcceptanceDocument = creditAppReqLine.AcceptanceDocument,
                                  QCAReqLineBuyerAmendCL_BillingContactPersonGUID = creditAppReqLine.BillingContactPersonGUID,
                                  QCAReqLineBuyerAmendCL_BillingAddressGUID = creditAppReqLine.BillingAddressGUID,
                                  QCAReqLineBuyerAmendCL_BillingAddress = addresstransBilling != null ? (addresstransBilling.Address1 + " " + addresstransBilling.Address2) : "",
                                  QCAReqLineBuyerAmendCL_MethodOfPaymentGUID = creditAppReqLine.MethodOfPaymentGUID,
                                  QCAReqLineBuyerAmendCL_MethodOfPayment = methodOfPayment != null ? methodOfPayment.Description : "",
                                  QCAReqLineBuyerAmendCL_ReceiptDescription = creditAppReqLine.ReceiptDescription,
                                  QCAReqLineBuyerAmendCL_ReceiptContactPersonGUID = creditAppReqLine.ReceiptContactPersonGUID,
                                  QCAReqLineBuyerAmendCL_ReceiptAddressGUID = creditAppReqLine.ReceiptAddressGUID,
                                  QCAReqLineBuyerAmendCL_ReceiptAddress = addresstransReceipt != null ? (addresstransReceipt.Address1 + " " + addresstransReceipt.Address2) : "",
                                  QCAReqLineBuyerAmendCL_MarketingComment = creditAppReqLine.MarketingComment,
                                  QCAReqLineBuyerAmendCL_CreditComment = creditAppReqLine.CreditComment,
                                  QCAReqLineBuyerAmendCL_ApproverComment = creditAppReqLine.ApproverComment,
                                  QCAReqLineBuyerAmendCL_CreditLimitLineRequest = creditAppReqLine.CreditLimitLineRequest,
                                  QCAReqLineBuyerAmendCL_BillingDescription = creditAppReqLine.BillingDescription,
                                  QCAReqLineBuyerAmendCL_BlacklistStatus = blacklistStatus != null ? blacklistStatus.Description : "",
                                  QCAReqLineBuyerAmendCL_KYCSetup = creditAppReqLine.KYCSetupGUID.ToString()
                              }).FirstOrDefault(); ;
                if (result != null)
                {
                    return result;
                }
                else
                {
                    return new QCAReqLineBuyerAmendCL();
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }
        #endregion
    }
}

