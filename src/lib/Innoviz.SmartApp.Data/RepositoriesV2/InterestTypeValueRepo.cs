using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IInterestTypeValueRepo
	{
		#region DropDown
		IEnumerable<SelectItem<InterestTypeValueItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<InterestTypeValueListView> GetListvw(SearchParameter search);
		InterestTypeValueItemView GetByIdvw(Guid id);
		InterestTypeValue Find(params object[] keyValues);
		InterestTypeValue GetInterestTypeValueByIdNoTracking(Guid guid);
		InterestTypeValue CreateInterestTypeValue(InterestTypeValue interestTypeValue);
		void CreateInterestTypeValueVoid(InterestTypeValue interestTypeValue);
		InterestTypeValue UpdateInterestTypeValue(InterestTypeValue dbInterestTypeValue, InterestTypeValue inputInterestTypeValue, List<string> skipUpdateFields = null);
		void UpdateInterestTypeValueVoid(InterestTypeValue dbInterestTypeValue, InterestTypeValue inputInterestTypeValue, List<string> skipUpdateFields = null);
		void Remove(InterestTypeValue item);
		InterestTypeValue GetByInterestTypeDateNoTracking(Guid interestTypeGuid, DateTime asOfDate, bool isThrow);
	}
	public class InterestTypeValueRepo : CompanyBaseRepository<InterestTypeValue>, IInterestTypeValueRepo
	{
		public InterestTypeValueRepo(SmartAppDbContext context) : base(context) { }
		public InterestTypeValueRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public InterestTypeValue GetInterestTypeValueByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.InterestTypeValueGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<InterestTypeValueItemViewMap> GetDropDownQuery()
		{
			return (from interestTypeValue in Entity
					select new InterestTypeValueItemViewMap
					{
						CompanyGUID = interestTypeValue.CompanyGUID,
						Owner = interestTypeValue.Owner,
						OwnerBusinessUnitGUID = interestTypeValue.OwnerBusinessUnitGUID,
						InterestTypeValueGUID = interestTypeValue.InterestTypeValueGUID,
						Value = interestTypeValue.Value
					});
		}
		public IEnumerable<SelectItem<InterestTypeValueItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<InterestTypeValue>(search, SysParm.CompanyGUID, DateTime.MinValue);
				var interestTypeValue = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<InterestTypeValueItemViewMap, InterestTypeValueItemView>().ToDropDownItem(search.ExcludeRowData);


				return interestTypeValue;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<InterestTypeValueListViewMap> GetListQuery()
		{
			return (from interestTypeValue in Entity
				select new InterestTypeValueListViewMap
				{
						CompanyGUID = interestTypeValue.CompanyGUID,
						Owner = interestTypeValue.Owner,
						OwnerBusinessUnitGUID = interestTypeValue.OwnerBusinessUnitGUID,
						InterestTypeValueGUID = interestTypeValue.InterestTypeValueGUID,
						EffectiveFrom = interestTypeValue.EffectiveFrom,
						EffectiveTo = interestTypeValue.EffectiveTo,
						Value = interestTypeValue.Value,
						InterestTypeGUID = interestTypeValue.InterestTypeGUID,
				});
		}
		public SearchResult<InterestTypeValueListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<InterestTypeValueListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<InterestTypeValue>(search, SysParm.CompanyGUID, DateTime.MinValue);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<InterestTypeValueListViewMap, InterestTypeValueListView>();
				result = list.SetSearchResult<InterestTypeValueListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<InterestTypeValueItemViewMap> GetItemQuery()
		{
			return (from interestTypeValue in Entity
					join company in db.Set<Company>()
					on interestTypeValue.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on interestTypeValue.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljInterestTypeValueOwnerBU
					from ownerBU in ljInterestTypeValueOwnerBU.DefaultIfEmpty()
					join interestType in db.Set<InterestType>() on interestTypeValue.InterestTypeGUID equals interestType.InterestTypeGUID
					select new InterestTypeValueItemViewMap
					{
						CompanyGUID = interestTypeValue.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = interestTypeValue.Owner,
						OwnerBusinessUnitGUID = interestTypeValue.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = interestTypeValue.CreatedBy,
						CreatedDateTime = interestTypeValue.CreatedDateTime,
						ModifiedBy = interestTypeValue.ModifiedBy,
						ModifiedDateTime = interestTypeValue.ModifiedDateTime,
						InterestTypeValueGUID = interestTypeValue.InterestTypeValueGUID,
						EffectiveFrom = interestTypeValue.EffectiveFrom,
						EffectiveTo = interestTypeValue.EffectiveTo,
						InterestTypeGUID = interestTypeValue.InterestTypeGUID,
						Value = interestTypeValue.Value,
						InterestType_Values = SmartAppUtil.GetDropDownLabel(interestType.InterestTypeId, interestType.Description),
					
						RowVersion = interestTypeValue.RowVersion,
					});
		}
		public InterestTypeValueItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<InterestTypeValueItemViewMap, InterestTypeValueItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public InterestTypeValue CreateInterestTypeValue(InterestTypeValue interestTypeValue)
		{
			try
			{
				interestTypeValue.InterestTypeValueGUID = Guid.NewGuid();
				base.Add(interestTypeValue);
				return interestTypeValue;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateInterestTypeValueVoid(InterestTypeValue interestTypeValue)
		{
			try
			{
				CreateInterestTypeValue(interestTypeValue);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public InterestTypeValue UpdateInterestTypeValue(InterestTypeValue dbInterestTypeValue, InterestTypeValue inputInterestTypeValue, List<string> skipUpdateFields = null)
		{
			try
			{
				dbInterestTypeValue = dbInterestTypeValue.MapUpdateValues<InterestTypeValue>(inputInterestTypeValue);
				base.Update(dbInterestTypeValue);
				return dbInterestTypeValue;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateInterestTypeValueVoid(InterestTypeValue dbInterestTypeValue, InterestTypeValue inputInterestTypeValue, List<string> skipUpdateFields = null)
		{
			try
			{
				dbInterestTypeValue = dbInterestTypeValue.MapUpdateValues<InterestTypeValue>(inputInterestTypeValue, skipUpdateFields);
				base.Update(dbInterestTypeValue);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #endregion Create, Update, Delete

        #region Validate
        public override void ValidateAdd(InterestTypeValue item)
		{
			base.ValidateAdd(item);
			ValidateEffective(item);
		}
		public override void ValidateUpdate(InterestTypeValue item)
		{
			base.ValidateUpdate(item);
			ValidateEffective(item);
		}
		private void ValidateEffective(InterestTypeValue item)
		{
			try
			{
				DateTime? effectiveTo = (item.EffectiveTo != null) ? item.EffectiveTo : DateTime.MaxValue;

				if (!(item.Value >= 0))
				{
					SmartAppException ex = new SmartAppException("ERROR.ERROR");
					ex.AddData("ERROR.GREATER_THAN_EQUAL_OR_ZERO", new string[] { "LABEL.VALUE"});
					throw SmartAppUtil.AddStackTrace(ex);
				}

				if (!(item.EffectiveFrom <= effectiveTo))
				{
					SmartAppException ex = new SmartAppException("ERROR.ERROR");
					ex.AddData("ERROR.00037");
					throw SmartAppUtil.AddStackTrace(ex);
				}

                if (BetweenEffectiveDate(item))
                {
                    SmartAppException ex = new SmartAppException("ERROR.ERROR");
                    ex.AddData("ERROR.00651", new string[] { item.EffectiveFrom.DateToString(), effectiveTo.DateToString() });
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        public bool BetweenEffectiveDate(InterestTypeValue item)
        {
            try
            {
                DateTime? maxEffectiveTo = MaxEffectiveTo(item.InterestTypeGUID);
                DateTime? DBeffectiveTo = (maxEffectiveTo != null) ? maxEffectiveTo : DateTime.MaxValue.AddDays(-1);

                DateTime? effectiveTo = (item.EffectiveTo != null) ? item.EffectiveTo : DateTime.MaxValue;

                if (!Entity.Any(o => o.InterestTypeGUID == item.InterestTypeGUID))
                {
                    return false;
                }

                var notBetween = Entity.Any(o =>
                                (item.EffectiveFrom >= o.EffectiveFrom && item.EffectiveFrom <= o.EffectiveTo && item.InterestTypeValueGUID != o.InterestTypeValueGUID &&
                                o.InterestTypeGUID == item.InterestTypeGUID && o.CompanyGUID == item.CompanyGUID)

                            || (item.EffectiveFrom <= o.EffectiveFrom && item.EffectiveTo >= o.EffectiveTo && item.InterestTypeValueGUID != o.InterestTypeValueGUID &&
                                o.InterestTypeGUID == item.InterestTypeGUID && o.CompanyGUID == item.CompanyGUID));

                if (notBetween)
                {
                    return notBetween;
                }

                if (maxEffectiveTo == null && DBeffectiveTo != DateTime.MaxValue.AddDays(-1))
                {
                    return notBetween;
                }

                var maxRow = Entity.Where(o => o.EffectiveTo == maxEffectiveTo &&
                                               o.InterestTypeGUID == item.InterestTypeGUID &&
                                               o.CompanyGUID == item.CompanyGUID).AsNoTracking().FirstOrDefault();

                if (item.InterestTypeValueGUID == maxRow.InterestTypeValueGUID)
                {
                    return notBetween;
                }
                if (maxEffectiveTo == null && effectiveTo < DBeffectiveTo && effectiveTo >= maxRow.EffectiveFrom && item.InterestTypeValueGUID != maxRow.InterestTypeValueGUID)
                {
                    return true;
                }

                var maxDB = Entity.Any(o => o.InterestTypeGUID == item.InterestTypeGUID &&
                        o.CompanyGUID == item.CompanyGUID &&
                        (effectiveTo >= DBeffectiveTo && item.EffectiveFrom <= DBeffectiveTo)
                        && item.InterestTypeValueGUID != o.InterestTypeValueGUID
                       );

                if (maxDB)
                {
                    return maxDB;
                }
                var intervene = Entity.Any(o => item.EffectiveTo < o.EffectiveTo && item.EffectiveFrom < o.EffectiveFrom && item.EffectiveTo >= o.EffectiveFrom
                                             && item.InterestTypeValueGUID != o.InterestTypeValueGUID && o.InterestTypeGUID == item.InterestTypeGUID && o.CompanyGUID == item.CompanyGUID);

                return intervene;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public DateTime? MaxEffectiveTo(Guid? interestTypeGUID)
		{
			try
			{
				List<InterestTypeValue> interestTypeValues = Entity.Where(o => o.InterestTypeGUID == interestTypeGUID).AsNoTracking().ToList();
				if (interestTypeValues.Count() == 0)
				{
					return null;
				}
				else if (interestTypeValues.Any(o => o.EffectiveTo == null))
				{
					return null;
				}
				else
				{
					return interestTypeValues.Max(o => o.EffectiveTo);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Validate
		public InterestTypeValue GetByInterestTypeDateNoTracking(Guid interestTypeGuid, DateTime asOfDate, bool isThrow)
		{
			try
			{
				var result = Entity.Where(w => w.InterestTypeGUID == interestTypeGuid
										  && w.EffectiveFrom <= asOfDate
										  && (w.EffectiveTo >= asOfDate || w.EffectiveTo == (DateTime?)null)).AsNoTracking().FirstOrDefault();
				if(isThrow && result == null)
                {
					IInterestTypeRepo interestTypeRepo = new InterestTypeRepo(db);
					InterestType interestType = interestTypeRepo.GetInterestTypeByIdNoTracking(interestTypeGuid);
					SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
					smartAppException.AddData("ERROR.90007", new string[] {"LABEL.ORIGINAL_INTEREST_TYPE_ID", SmartAppUtil.GetDropDownLabel(interestType.InterestTypeId, interestType.Description) });
					throw smartAppException;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

