using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IWithholdingTaxTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<WithholdingTaxTableItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<WithholdingTaxTableListView> GetListvw(SearchParameter search);
		WithholdingTaxTableItemView GetByIdvw(Guid id);
		WithholdingTaxTable Find(params object[] keyValues);
		WithholdingTaxTable GetWithholdingTaxTableByIdNoTracking(Guid guid);
		WithholdingTaxTable CreateWithholdingTaxTable(WithholdingTaxTable withholdingTaxTable);
		void CreateWithholdingTaxTableVoid(WithholdingTaxTable withholdingTaxTable);
		WithholdingTaxTable UpdateWithholdingTaxTable(WithholdingTaxTable dbWithholdingTaxTable, WithholdingTaxTable inputWithholdingTaxTable, List<string> skipUpdateFields = null);
		void UpdateWithholdingTaxTableVoid(WithholdingTaxTable dbWithholdingTaxTable, WithholdingTaxTable inputWithholdingTaxTable, List<string> skipUpdateFields = null);
		void Remove(WithholdingTaxTable item);
		List<WithholdingTaxTable> GetWithholdingTaxTableByIdNoTracking(List<Guid> withholdingTaxTableGUID);
	}
	public class WithholdingTaxTableRepo : CompanyBaseRepository<WithholdingTaxTable>, IWithholdingTaxTableRepo
	{
		public WithholdingTaxTableRepo(SmartAppDbContext context) : base(context) { }
		public WithholdingTaxTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public WithholdingTaxTable GetWithholdingTaxTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.WithholdingTaxTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<WithholdingTaxTableItemViewMap> GetDropDownQuery()
		{
			return (from withholdingTaxTable in Entity
					select new WithholdingTaxTableItemViewMap
					{
						CompanyGUID = withholdingTaxTable.CompanyGUID,
						Owner = withholdingTaxTable.Owner,
						OwnerBusinessUnitGUID = withholdingTaxTable.OwnerBusinessUnitGUID,
						WithholdingTaxTableGUID = withholdingTaxTable.WithholdingTaxTableGUID,
						WHTCode = withholdingTaxTable.WHTCode,
						Description = withholdingTaxTable.Description
					});
		}
		public IEnumerable<SelectItem<WithholdingTaxTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<WithholdingTaxTable>(search, SysParm.CompanyGUID);
				var withholdingTaxTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<WithholdingTaxTableItemViewMap, WithholdingTaxTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return withholdingTaxTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<WithholdingTaxTableListViewMap> GetListQuery()
		{
			return (from withholdingTaxTable in Entity
				select new WithholdingTaxTableListViewMap
				{
						CompanyGUID = withholdingTaxTable.CompanyGUID,
						Owner = withholdingTaxTable.Owner,
						OwnerBusinessUnitGUID = withholdingTaxTable.OwnerBusinessUnitGUID,
						WithholdingTaxTableGUID = withholdingTaxTable.WithholdingTaxTableGUID,
						WHTCode = withholdingTaxTable.WHTCode,
						Description = withholdingTaxTable.Description
				});
		}
		public SearchResult<WithholdingTaxTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<WithholdingTaxTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<WithholdingTaxTable>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<WithholdingTaxTableListViewMap, WithholdingTaxTableListView>();
				result = list.SetSearchResult<WithholdingTaxTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<WithholdingTaxTableItemViewMap> GetItemQuery()
		{
			return (from withholdingTaxTable in Entity
					join company in db.Set<Company>()
					on withholdingTaxTable.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on withholdingTaxTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljWithholdingTaxTableOwnerBU
					from ownerBU in ljWithholdingTaxTableOwnerBU.DefaultIfEmpty()
					select new WithholdingTaxTableItemViewMap
					{
						CompanyGUID = withholdingTaxTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = withholdingTaxTable.Owner,
						OwnerBusinessUnitGUID = withholdingTaxTable.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = withholdingTaxTable.CreatedBy,
						CreatedDateTime = withholdingTaxTable.CreatedDateTime,
						ModifiedBy = withholdingTaxTable.ModifiedBy,
						ModifiedDateTime = withholdingTaxTable.ModifiedDateTime,
						WithholdingTaxTableGUID = withholdingTaxTable.WithholdingTaxTableGUID,
						Description = withholdingTaxTable.Description,
						LedgerAccount = withholdingTaxTable.LedgerAccount,
						WHTCode = withholdingTaxTable.WHTCode,
					
						RowVersion = withholdingTaxTable.RowVersion,
					});
		}
		public WithholdingTaxTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<WithholdingTaxTableItemViewMap, WithholdingTaxTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public WithholdingTaxTable CreateWithholdingTaxTable(WithholdingTaxTable withholdingTaxTable)
		{
			try
			{
				withholdingTaxTable.WithholdingTaxTableGUID = Guid.NewGuid();
				base.Add(withholdingTaxTable);
				return withholdingTaxTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateWithholdingTaxTableVoid(WithholdingTaxTable withholdingTaxTable)
		{
			try
			{
				CreateWithholdingTaxTable(withholdingTaxTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public WithholdingTaxTable UpdateWithholdingTaxTable(WithholdingTaxTable dbWithholdingTaxTable, WithholdingTaxTable inputWithholdingTaxTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbWithholdingTaxTable = dbWithholdingTaxTable.MapUpdateValues<WithholdingTaxTable>(inputWithholdingTaxTable);
				base.Update(dbWithholdingTaxTable);
				return dbWithholdingTaxTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateWithholdingTaxTableVoid(WithholdingTaxTable dbWithholdingTaxTable, WithholdingTaxTable inputWithholdingTaxTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbWithholdingTaxTable = dbWithholdingTaxTable.MapUpdateValues<WithholdingTaxTable>(inputWithholdingTaxTable, skipUpdateFields);
				base.Update(dbWithholdingTaxTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete

		public List<WithholdingTaxTable> GetWithholdingTaxTableByIdNoTracking(List<Guid> withholdingTaxTableGUID)
        {
            try
            {
				List<WithholdingTaxTable> withholdingTaxTables = Entity.Where(w => withholdingTaxTableGUID.Contains(w.WithholdingTaxTableGUID)).ToList();
				return withholdingTaxTables;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

