using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IAuthorizedPersonTypeRepo
	{
		#region DropDown
		IEnumerable<SelectItem<AuthorizedPersonTypeItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<AuthorizedPersonTypeListView> GetListvw(SearchParameter search);
		AuthorizedPersonTypeItemView GetByIdvw(Guid id);
		AuthorizedPersonType Find(params object[] keyValues);
		AuthorizedPersonType GetAuthorizedPersonTypeByIdNoTracking(Guid guid);
		AuthorizedPersonType CreateAuthorizedPersonType(AuthorizedPersonType authorizedPersonType);
		void CreateAuthorizedPersonTypeVoid(AuthorizedPersonType authorizedPersonType);
		AuthorizedPersonType UpdateAuthorizedPersonType(AuthorizedPersonType dbAuthorizedPersonType, AuthorizedPersonType inputAuthorizedPersonType, List<string> skipUpdateFields = null);
		void UpdateAuthorizedPersonTypeVoid(AuthorizedPersonType dbAuthorizedPersonType, AuthorizedPersonType inputAuthorizedPersonType, List<string> skipUpdateFields = null);
		void Remove(AuthorizedPersonType item);
	}
	public class AuthorizedPersonTypeRepo : CompanyBaseRepository<AuthorizedPersonType>, IAuthorizedPersonTypeRepo
	{
		public AuthorizedPersonTypeRepo(SmartAppDbContext context) : base(context) { }
		public AuthorizedPersonTypeRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public AuthorizedPersonType GetAuthorizedPersonTypeByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.AuthorizedPersonTypeGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<AuthorizedPersonTypeItemViewMap> GetDropDownQuery()
		{
			return (from authorizedPersonType in Entity
					select new AuthorizedPersonTypeItemViewMap
					{
						CompanyGUID = authorizedPersonType.CompanyGUID,
						Owner = authorizedPersonType.Owner,
						OwnerBusinessUnitGUID = authorizedPersonType.OwnerBusinessUnitGUID,
						AuthorizedPersonTypeGUID = authorizedPersonType.AuthorizedPersonTypeGUID,
						AuthorizedPersonTypeId = authorizedPersonType.AuthorizedPersonTypeId,
						Description = authorizedPersonType.Description
					});
		}
		public IEnumerable<SelectItem<AuthorizedPersonTypeItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<AuthorizedPersonType>(search, SysParm.CompanyGUID);
				var authorizedPersonType = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<AuthorizedPersonTypeItemViewMap, AuthorizedPersonTypeItemView>().ToDropDownItem(search.ExcludeRowData);


				return authorizedPersonType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<AuthorizedPersonTypeListViewMap> GetListQuery()
		{
			return (from authorizedPersonType in Entity
				select new AuthorizedPersonTypeListViewMap
				{
						CompanyGUID = authorizedPersonType.CompanyGUID,
						Owner = authorizedPersonType.Owner,
						OwnerBusinessUnitGUID = authorizedPersonType.OwnerBusinessUnitGUID,
						AuthorizedPersonTypeGUID = authorizedPersonType.AuthorizedPersonTypeGUID,
						AuthorizedPersonTypeId = authorizedPersonType.AuthorizedPersonTypeId,
						Description = authorizedPersonType.Description,
				});
		}
		public SearchResult<AuthorizedPersonTypeListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<AuthorizedPersonTypeListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<AuthorizedPersonType>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<AuthorizedPersonTypeListViewMap, AuthorizedPersonTypeListView>();
				result = list.SetSearchResult<AuthorizedPersonTypeListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<AuthorizedPersonTypeItemViewMap> GetItemQuery()
		{
			return (from authorizedPersonType in Entity
					join company in db.Set<Company>()
					on authorizedPersonType.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on authorizedPersonType.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljAuthorizedPersonTypeOwnerBU
					from ownerBU in ljAuthorizedPersonTypeOwnerBU.DefaultIfEmpty()
					select new AuthorizedPersonTypeItemViewMap
					{
						CompanyGUID = authorizedPersonType.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = authorizedPersonType.Owner,
						OwnerBusinessUnitGUID = authorizedPersonType.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = authorizedPersonType.CreatedBy,
						CreatedDateTime = authorizedPersonType.CreatedDateTime,
						ModifiedBy = authorizedPersonType.ModifiedBy,
						ModifiedDateTime = authorizedPersonType.ModifiedDateTime,
						AuthorizedPersonTypeGUID = authorizedPersonType.AuthorizedPersonTypeGUID,
						AuthorizedPersonTypeId = authorizedPersonType.AuthorizedPersonTypeId,
						Description = authorizedPersonType.Description,
					
						RowVersion = authorizedPersonType.RowVersion,
					});
		}
		public AuthorizedPersonTypeItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<AuthorizedPersonTypeItemViewMap, AuthorizedPersonTypeItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public AuthorizedPersonType CreateAuthorizedPersonType(AuthorizedPersonType authorizedPersonType)
		{
			try
			{
				authorizedPersonType.AuthorizedPersonTypeGUID = Guid.NewGuid();
				base.Add(authorizedPersonType);
				return authorizedPersonType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateAuthorizedPersonTypeVoid(AuthorizedPersonType authorizedPersonType)
		{
			try
			{
				CreateAuthorizedPersonType(authorizedPersonType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AuthorizedPersonType UpdateAuthorizedPersonType(AuthorizedPersonType dbAuthorizedPersonType, AuthorizedPersonType inputAuthorizedPersonType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbAuthorizedPersonType = dbAuthorizedPersonType.MapUpdateValues<AuthorizedPersonType>(inputAuthorizedPersonType);
				base.Update(dbAuthorizedPersonType);
				return dbAuthorizedPersonType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateAuthorizedPersonTypeVoid(AuthorizedPersonType dbAuthorizedPersonType, AuthorizedPersonType inputAuthorizedPersonType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbAuthorizedPersonType = dbAuthorizedPersonType.MapUpdateValues<AuthorizedPersonType>(inputAuthorizedPersonType, skipUpdateFields);
				base.Update(dbAuthorizedPersonType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

