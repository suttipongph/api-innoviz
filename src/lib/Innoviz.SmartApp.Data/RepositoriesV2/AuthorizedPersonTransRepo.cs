using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface IAuthorizedPersonTransRepo
    {
        #region DropDown
        IEnumerable<SelectItem<AuthorizedPersonTransItemView>> GetDropDownItem(SearchParameter search);
        #endregion DropDown
        void ValidateAdd(IEnumerable<AuthorizedPersonTrans> items);
        void ValidateRemove(IEnumerable<AuthorizedPersonTrans> items);
		SearchResult<AuthorizedPersonTransListView> GetListvw(SearchParameter search);
		AuthorizedPersonTransItemView GetByIdvw(Guid id);
		AuthorizedPersonTrans Find(params object[] keyValues);
		AuthorizedPersonTrans GetAuthorizedPersonTransByIdNoTracking(Guid guid);
		AuthorizedPersonTrans CreateAuthorizedPersonTrans(AuthorizedPersonTrans authorizedPersonTrans);
		void CreateAuthorizedPersonTransVoid(AuthorizedPersonTrans authorizedPersonTrans);
		AuthorizedPersonTrans UpdateAuthorizedPersonTrans(AuthorizedPersonTrans dbAuthorizedPersonTrans, AuthorizedPersonTrans inputAuthorizedPersonTrans, List<string> skipUpdateFields = null);
		void UpdateAuthorizedPersonTransVoid(AuthorizedPersonTrans dbAuthorizedPersonTrans, AuthorizedPersonTrans inputAuthorizedPersonTrans, List<string> skipUpdateFields = null);
		void Remove(AuthorizedPersonTrans item);
		void Remove(IEnumerable<AuthorizedPersonTrans> items);
		IEnumerable<AuthorizedPersonTrans> GetAuthorizedPersonTransByReference(Guid refId, int refType);
		AuthorizedPersonTrans GetAuthorizedPersonTransByIdNoTrackingByAccessLevel(Guid guid);
		IEnumerable<AuthorizedPersonTrans> GetAuthorizedPersonTransByRefAuthorizedPersonTrans(List<Guid> refAuthorizedPersonTrans);
	}
	public class AuthorizedPersonTransRepo : CompanyBaseRepository<AuthorizedPersonTrans>, IAuthorizedPersonTransRepo
	{
		public AuthorizedPersonTransRepo(SmartAppDbContext context) : base(context) { }
		public AuthorizedPersonTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public AuthorizedPersonTrans GetAuthorizedPersonTransByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.AuthorizedPersonTransGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<AuthorizedPersonTransItemViewMap> GetDropDownQuery()
		{
			return (from authorizedPersonTrans in Entity
					join relatedPersonTable in db.Set<RelatedPersonTable>()
					on authorizedPersonTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID
					select new AuthorizedPersonTransItemViewMap
					{
						CompanyGUID = authorizedPersonTrans.CompanyGUID,
						Owner = authorizedPersonTrans.Owner,
						OwnerBusinessUnitGUID = authorizedPersonTrans.OwnerBusinessUnitGUID,
						AuthorizedPersonTransGUID = authorizedPersonTrans.AuthorizedPersonTransGUID,
						RelatedPersonTableGUID = authorizedPersonTrans.RelatedPersonTableGUID,
						RelatedPersonTable_Name = relatedPersonTable.Name,
						RelatedPersonTable_RelatedPersonId = relatedPersonTable.RelatedPersonId,
						RefGUID = authorizedPersonTrans.RefGUID,
						RefType = authorizedPersonTrans.RefType
					});
		}
		public IEnumerable<SelectItem<AuthorizedPersonTransItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<AuthorizedPersonTrans>(search, SysParm.CompanyGUID);
				var authorizedPersonTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<AuthorizedPersonTransItemViewMap, AuthorizedPersonTransItemView>().ToDropDownItem(search.ExcludeRowData);


				return authorizedPersonTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<AuthorizedPersonTransListViewMap> GetListQuery()
		{
			return (from authorizedPersonTrans in Entity
					join relatedPersonTable in db.Set<RelatedPersonTable>() on authorizedPersonTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID
					join authorizedPersonType in db.Set<AuthorizedPersonType>() on authorizedPersonTrans.AuthorizedPersonTypeGUID equals authorizedPersonType.AuthorizedPersonTypeGUID into relatedPersonTableAuthorizedPersonType
					from authorizedPersonType in relatedPersonTableAuthorizedPersonType.DefaultIfEmpty()
					select new AuthorizedPersonTransListViewMap
					{
						CompanyGUID = authorizedPersonTrans.CompanyGUID,
						Owner = authorizedPersonTrans.Owner,
						OwnerBusinessUnitGUID = authorizedPersonTrans.OwnerBusinessUnitGUID,
						AuthorizedPersonTransGUID = authorizedPersonTrans.AuthorizedPersonTransGUID,
						Ordering = authorizedPersonTrans.Ordering,
						AuthorizedPersonTypeGUID = authorizedPersonType.AuthorizedPersonTypeGUID,
						InActive = authorizedPersonTrans.InActive,
						RefGUID = authorizedPersonTrans.RefGUID,
						RefType = authorizedPersonTrans.RefType,
						RefAuthorizedPersonTransGUID = authorizedPersonTrans.RefAuthorizedPersonTransGUID,
						AuthorizedPersonType_Values = (authorizedPersonType != null) ? SmartAppUtil.GetDropDownLabel(authorizedPersonType.AuthorizedPersonTypeId.ToString(), authorizedPersonType.Description) : null,
						#region RelatedPersonTable
						RelatedPersonTableGUID = authorizedPersonTrans.RelatedPersonTableGUID,
						RelatedPersonTable_Name = relatedPersonTable.Name,
						RelatedPersonTable_PassportId = relatedPersonTable.PassportId,
						RelatedPersonTable_TaxId = relatedPersonTable.TaxId,
						#endregion
						AuthorizedPersonType_AuthorizedPersonTypeId = authorizedPersonType.AuthorizedPersonTypeId,
					});
		}
		public SearchResult<AuthorizedPersonTransListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<AuthorizedPersonTransListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<AuthorizedPersonTrans>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<AuthorizedPersonTransListViewMap, AuthorizedPersonTransListView>();
				result = list.SetSearchResult<AuthorizedPersonTransListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<AuthorizedPersonTransItemViewMap> GetItemQuery()
		{
			IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService();
			return (from authorizedPersonTrans in Entity
					join company in db.Set<Company>()
					on authorizedPersonTrans.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on authorizedPersonTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljAuthorizedPersonTransOwnerBU
					from ownerBU in ljAuthorizedPersonTransOwnerBU.DefaultIfEmpty()
					join relatedPersonTable in db.Set<RelatedPersonTable>() on authorizedPersonTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID
					join buyer in db.Set<BuyerTable>() on authorizedPersonTrans.RefGUID equals buyer.BuyerTableGUID into ljAuthorizedPersonTransBuyer
					from buyer in ljAuthorizedPersonTransBuyer.DefaultIfEmpty()
					join customer in db.Set<CustomerTable>() on authorizedPersonTrans.RefGUID equals customer.CustomerTableGUID into ljAuthorizedPersonTransCustomer
					from customer in ljAuthorizedPersonTransCustomer.DefaultIfEmpty()
					join creditAppRequestTable in db.Set<CreditAppRequestTable>() on authorizedPersonTrans.RefGUID equals creditAppRequestTable.CreditAppRequestTableGUID into ljAuthorizedPersonTransCreditAppRequestTable
					from creditAppRequestTable in ljAuthorizedPersonTransCreditAppRequestTable.DefaultIfEmpty()
					join creditAppTable in db.Set<CreditAppTable>() on authorizedPersonTrans.RefGUID equals creditAppTable.CreditAppTableGUID into ljAuthorizedPersonTransCreditAppTable
					from creditAppTable in ljAuthorizedPersonTransCreditAppTable.DefaultIfEmpty()
					join nationality in db.Set<Nationality>()
					on relatedPersonTable.NationalityGUID equals nationality.NationalityGUID into ljNationality
					from nationality in ljNationality.DefaultIfEmpty()
					join race in db.Set<Race>()
					on relatedPersonTable.RaceGUID equals race.RaceGUID into ljRace
					from race in ljRace.DefaultIfEmpty()
					select new AuthorizedPersonTransItemViewMap
					{
						CompanyGUID = authorizedPersonTrans.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = authorizedPersonTrans.Owner,
						OwnerBusinessUnitGUID = authorizedPersonTrans.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = authorizedPersonTrans.CreatedBy,
						CreatedDateTime = authorizedPersonTrans.CreatedDateTime,
						ModifiedBy = authorizedPersonTrans.ModifiedBy,
						ModifiedDateTime = authorizedPersonTrans.ModifiedDateTime,
						AuthorizedPersonTransGUID = authorizedPersonTrans.AuthorizedPersonTransGUID,
						AuthorizedPersonTypeGUID = authorizedPersonTrans.AuthorizedPersonTypeGUID,
						InActive = authorizedPersonTrans.InActive,
						NCBCheckedBy = authorizedPersonTrans.NCBCheckedBy,
						NCBCheckedDate = authorizedPersonTrans.NCBCheckedDate,
						Ordering = authorizedPersonTrans.Ordering,
						RefAuthorizedPersonTransGUID = authorizedPersonTrans.RefAuthorizedPersonTransGUID,
						RefGUID = authorizedPersonTrans.RefGUID,
						RefType = authorizedPersonTrans.RefType,
						#region RelatedPersonTable
						RelatedPersonTableGUID = authorizedPersonTrans.RelatedPersonTableGUID,
						RelatedPersonTable_Name = relatedPersonTable.Name,
						RelatedPersonTable_IdentificationType = relatedPersonTable.IdentificationType,
						RelatedPersonTable_PassportId = relatedPersonTable.PassportId,
						RelatedPersonTable_WorkPermitId = relatedPersonTable.PassportId,
						RelatedPersonTable_DateOfBirth = relatedPersonTable.DateOfBirth,
						RelatedPersonTable_Phone = relatedPersonTable.Phone,
						RelatedPersonTable_Extension = relatedPersonTable.Extension,
						RelatedPersonTable_Fax = relatedPersonTable.Fax,
						RelatedPersonTable_Mobile = relatedPersonTable.Mobile,
						RelatedPersonTable_LineId = relatedPersonTable.LineId,
						RelatedPersonTable_Email = relatedPersonTable.Email,
						RelatedPersonTable_NationalityId = (nationality != null) ? SmartAppUtil.GetDropDownLabel(nationality.NationalityId, nationality.Description) : string.Empty,
						RelatedPersonTable_RaceId = (nationality != null) ? SmartAppUtil.GetDropDownLabel(race.RaceId, race.Description) : string.Empty,
						RelatedPersonTable_TaxId = relatedPersonTable.TaxId,
						RelatedPersonTable_BackgroundSummary = relatedPersonTable.BackgroundSummary,
						RelatedPersonTable_Age = relatedPersonTableService.CalcAge(relatedPersonTable.DateOfBirth),
						#endregion
						RefId = (buyer != null && authorizedPersonTrans.RefType == (int)RefType.Buyer) ? buyer.BuyerId :
						        (customer != null && authorizedPersonTrans.RefType == (int)RefType.Customer) ? customer.CustomerId :
								(creditAppRequestTable != null && authorizedPersonTrans.RefType == (int)RefType.CreditAppRequestTable) ? creditAppRequestTable.CreditAppRequestId :
								(creditAppTable != null && authorizedPersonTrans.RefType == (int)RefType.CreditAppTable) ? creditAppTable.CreditAppId : null,
						Replace = authorizedPersonTrans.Replace,
						ReplacedAuthorizedPersonTransGUID = authorizedPersonTrans.ReplacedAuthorizedPersonTransGUID,
					
						RowVersion = authorizedPersonTrans.RowVersion,
					});
		}
		public AuthorizedPersonTransItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<AuthorizedPersonTransItemViewMap, AuthorizedPersonTransItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public AuthorizedPersonTrans CreateAuthorizedPersonTrans(AuthorizedPersonTrans authorizedPersonTrans)
		{
			try
			{
				authorizedPersonTrans.AuthorizedPersonTransGUID = Guid.NewGuid();
				base.Add(authorizedPersonTrans);
				return authorizedPersonTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateAuthorizedPersonTransVoid(AuthorizedPersonTrans authorizedPersonTrans)
		{
			try
			{
				CreateAuthorizedPersonTrans(authorizedPersonTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AuthorizedPersonTrans UpdateAuthorizedPersonTrans(AuthorizedPersonTrans dbAuthorizedPersonTrans, AuthorizedPersonTrans inputAuthorizedPersonTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbAuthorizedPersonTrans = dbAuthorizedPersonTrans.MapUpdateValues<AuthorizedPersonTrans>(inputAuthorizedPersonTrans);
				base.Update(dbAuthorizedPersonTrans);
				return dbAuthorizedPersonTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateAuthorizedPersonTransVoid(AuthorizedPersonTrans dbAuthorizedPersonTrans, AuthorizedPersonTrans inputAuthorizedPersonTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbAuthorizedPersonTrans = dbAuthorizedPersonTrans.MapUpdateValues<AuthorizedPersonTrans>(inputAuthorizedPersonTrans, skipUpdateFields);
				base.Update(dbAuthorizedPersonTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public IEnumerable<AuthorizedPersonTrans> GetAuthorizedPersonTransByReference(Guid refId, int refType)
		{
			try
			{
				Guid companyGUID = SysParm.CompanyGUID.StringToGuid();
				return Entity.Where(t => t.CompanyGUID == companyGUID && t.RefGUID == refId && t.RefType == refType).AsNoTracking();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
        #region Validate

        public override void ValidateAdd(IEnumerable<AuthorizedPersonTrans> items)
        {
            foreach (var item in items)
            {
                ValidateAdd(item);
            }
        }
        public override void ValidateAdd(AuthorizedPersonTrans item)
        {
            base.ValidateAdd(item);
            CheckDuplicateActive(item);
        }
        public override void ValidateUpdate(AuthorizedPersonTrans item)
        {
            base.ValidateUpdate(item);
            CheckDuplicateActive(item);
        }
        public void CheckDuplicateActive(AuthorizedPersonTrans item)
        {
            try
            {
                IEnumerable<AuthorizedPersonTrans> authorizedPersonTrans = GetAuthorizedPersonTransByReference(item.RefGUID, item.RefType);
                SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
                if (authorizedPersonTrans.Any())
                {
                    authorizedPersonTrans = authorizedPersonTrans.Where(w => w.InActive == false);

                }
                bool isDupplicateActive = authorizedPersonTrans.Any(a => (a.RelatedPersonTableGUID == item.RelatedPersonTableGUID)
                                                                     && (a.AuthorizedPersonTransGUID != item.AuthorizedPersonTransGUID));
                bool isDupplicateOrdering = authorizedPersonTrans.Any(a => (a.Ordering == item.Ordering)
                                                                     && (a.AuthorizedPersonTransGUID != item.AuthorizedPersonTransGUID));
                if (isDupplicateActive)
                {
                    smartAppException.AddData("ERROR.DUPLICATE", new string[] { "LABEL.RELATED_PERSON_ID", "LABEL.REF_ID", "LABEL.ACTIVE" });
                }
                if (isDupplicateOrdering)
                {
                    smartAppException.AddData("ERROR.DUPLICATE", new string[] { "LABEL.ORDERING", "LABEL.REF_ID", "LABEL.ACTIVE" });
                }
                if (smartAppException.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(smartAppException);
                }
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion

        public AuthorizedPersonTrans GetAuthorizedPersonTransByIdNoTrackingByAccessLevel(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.AuthorizedPersonTransGUID == guid)
                                    .FilterByAccessLevel(SysParm.AccessLevel)
                                    .AsNoTracking()
                                    .FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public IEnumerable<AuthorizedPersonTrans> GetAuthorizedPersonTransByRefAuthorizedPersonTrans(List<Guid> refAuthorizedPersonTrans)
		{
			try
			{
				return Entity.Where(w => refAuthorizedPersonTrans.Any(a => a == w.AuthorizedPersonTransGUID)).AsNoTracking();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
