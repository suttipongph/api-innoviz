using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICreditAppLineRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CreditAppLineItemView>> GetDropDownItem(SearchParameter search);
		IEnumerable<SelectItem<CreditAppLineItemView>> GetDropDownItemByWithdrawalTable(SearchParameter search, DateTime withdrawalDate);
		#endregion DropDown
		SearchResult<CreditAppLineListView> GetListvw(SearchParameter search);
		CreditAppLineItemView GetByIdvw(Guid id);
		CreditAppLine Find(params object[] keyValues);
		CreditAppLine GetCreditAppLineByIdNoTracking(Guid guid);
		CreditAppLine GetCreditAppLineByIdNoTrackingByAccessLevel(Guid guid);
		CreditAppLine CreateCreditAppLine(CreditAppLine creditAppLine);
		void CreateCreditAppLineVoid(CreditAppLine creditAppLine);
		CreditAppLine UpdateCreditAppLine(CreditAppLine dbCreditAppLine, CreditAppLine inputCreditAppLine, List<string> skipUpdateFields = null);
		void UpdateCreditAppLineVoid(CreditAppLine dbCreditAppLine, CreditAppLine inputCreditAppLine, List<string> skipUpdateFields = null);
		void Remove(CreditAppLine item);
		IEnumerable<CreditAppLine> GetCreditAppLineByCompanyIdNoTracking(Guid guid);
		IEnumerable<CreditAppLine> GetCreditAppLineByCreditAppIdNoTracking(Guid guid);
		IQueryable<CreditAppLineItemView> GetQueryForApproveCreditApplicationRequestK2(Guid guid);
		IEnumerable<CreditAppLine> GetCreditAppLineByRefCreditAppLine(List<Guid> refCreditAppLineGUID);
		IEnumerable<CreditAppLine> GetCreditAppLineByBuyerIdNoTracking(Guid guid);
		SearchResult<CALineOutstandingView> GetCALineOutstandingByCA(SearchParameter search);
		SearchResult<CALineOutstandingView> GetCALineOutstandingByCA2(SearchParameter search);
		void ValidateAdd(CreditAppLine item);
		void ValidateAdd(IEnumerable<CreditAppLine> items);
		#region Shared
		List<CALineOutstandingViewMap> GetCALineOutstandingByCA(int refType, Guid refGUID, bool active , DateTime? asOfDate);
		#endregion
	}
	public class CreditAppLineRepo : CompanyBaseRepository<CreditAppLine>, ICreditAppLineRepo
	{
		public CreditAppLineRepo(SmartAppDbContext context) : base(context) { }
		public CreditAppLineRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CreditAppLine GetCreditAppLineByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CreditAppLineGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditAppLine GetCreditAppLineByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CreditAppLineGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CreditAppLineItemViewMap> GetDropDownQuery()
		{
			return (from creditAppLine in Entity
					join creditAppTable in db.Set<CreditAppTable>()
					on creditAppLine.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljcreditAppTable
					from creditAppTable in ljcreditAppTable.DefaultIfEmpty()
					join buyerTable in db.Set<BuyerTable>() on creditAppLine.BuyerTableGUID equals buyerTable.BuyerTableGUID
					join methodOfPayment in db.Set<MethodOfPayment>() on creditAppLine.MethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID into ljCreditAppLineMethodOfPayment
					from methodOfPayment in ljCreditAppLineMethodOfPayment.DefaultIfEmpty()
					select new CreditAppLineItemViewMap
					{
						CompanyGUID = creditAppLine.CompanyGUID,
						Owner = creditAppLine.Owner,
						OwnerBusinessUnitGUID = creditAppLine.OwnerBusinessUnitGUID,
						CreditAppLineGUID = creditAppLine.CreditAppLineGUID,
						LineNum = creditAppLine.LineNum,
						ApprovedCreditLimitLine = creditAppLine.ApprovedCreditLimitLine,
						MaxPurchasePct = creditAppLine.MaxPurchasePct,
						BuyerTableGUID = creditAppLine.BuyerTableGUID,
						CreditAppTable_ProductType = creditAppTable.ProductType.ToString(),
						CreditAppTableGUID = creditAppLine.CreditAppTableGUID,
						ExpiryDate = creditAppLine.ExpiryDate,
						BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
						MethodOfPaymentGUID = creditAppLine.MethodOfPaymentGUID,
						MethodOfPayment_Values = (methodOfPayment != null) ? SmartAppUtil.GetDropDownLabel(methodOfPayment.MethodOfPaymentId, methodOfPayment.Description) : null
					});
		}
		public IEnumerable<SelectItem<CreditAppLineItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditAppLine>(search, SysParm.CompanyGUID);
				var creditAppLine = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CreditAppLineItemViewMap, CreditAppLineItemView>().ToDropDownItem(search.ExcludeRowData);


				return creditAppLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<CreditAppLineItemView>> GetDropDownItemByWithdrawalTable(SearchParameter search, DateTime withdrawalDate)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditAppLine>(search, SysParm.CompanyGUID);
				var creditAppLine = GetDropDownQuery().Where(t => t.ExpiryDate > withdrawalDate).Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CreditAppLineItemViewMap, CreditAppLineItemView>().ToDropDownItem(search.ExcludeRowData);

				return creditAppLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CreditAppLineListViewMap> GetListQuery()
		{
			return (from creditAppLine in Entity
					join buyerTable in db.Set<BuyerTable>() on creditAppLine.BuyerTableGUID equals buyerTable.BuyerTableGUID
					join businessSegment in db.Set<BusinessSegment>() on buyerTable.BusinessSegmentGUID equals businessSegment.BusinessSegmentGUID into ljCreditAppLineBusinessSegment
					from businessSegment in ljCreditAppLineBusinessSegment.DefaultIfEmpty()
					join creditAppTable in db.Set<CreditAppTable>() on creditAppLine.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID
					join customerTable in db.Set<CustomerTable>() on creditAppTable.CustomerTableGUID equals customerTable.CustomerTableGUID
					select new CreditAppLineListViewMap
					{
						CompanyGUID = creditAppLine.CompanyGUID,
						Owner = creditAppLine.Owner,
						OwnerBusinessUnitGUID = creditAppLine.OwnerBusinessUnitGUID,
						CreditAppLineGUID = creditAppLine.CreditAppLineGUID,
						CreditAppTableGUID = creditAppLine.CreditAppTableGUID,
						LineNum = creditAppLine.LineNum,
						ReviewDate = creditAppLine.ReviewDate,
						BuyerTableGUID = creditAppLine.BuyerTableGUID,
						BusinessSegmentGUID = buyerTable.BusinessSegmentGUID,
						ApprovedCreditLimitLine = creditAppLine.ApprovedCreditLimitLine,
						BusinessSegment_Values = (businessSegment != null) ? SmartAppUtil.GetDropDownLabel(businessSegment.BusinessSegmentId, businessSegment.Description) : null,
						BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
						BuyerTable_BuyerId = buyerTable.BuyerId,
						BusinessSegment_BusinessSegmentId = businessSegment.BusinessSegmentId,
						CreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
						CreditAppTable_CreditAppId = creditAppTable.CreditAppId,
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
						CustomerTable_CustomerId = customerTable.CustomerId,
						CustomerTable_CustomerTableGUID = customerTable.CustomerTableGUID,
						ExpiryDate = creditAppLine.ExpiryDate,
						ProductType = creditAppTable.ProductType
					});

		}
		public SearchResult<CreditAppLineListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CreditAppLineListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditAppLine>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CreditAppLineListViewMap, CreditAppLineListView>();
				result = list.SetSearchResult<CreditAppLineListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CreditAppLineItemViewMap> GetItemQuery()
		{
			var address = db.Set<AddressTrans>();
			var addressLabel = (from creditAppLine in Entity
								join billingAddress in address on creditAppLine.BillingAddressGUID equals billingAddress.AddressTransGUID into ljCreditAppRequestLineBillingAddress
								from billingAddress in ljCreditAppRequestLineBillingAddress.DefaultIfEmpty()
								join invoiceAddress in address on creditAppLine.InvoiceAddressGUID equals invoiceAddress.AddressTransGUID into ljCreditAppRequestLineInvoiceAddress
								from invoiceAddress in ljCreditAppRequestLineInvoiceAddress.DefaultIfEmpty()
								join mailingReceiptAddress in address on creditAppLine.MailingReceiptAddressGUID equals mailingReceiptAddress.AddressTransGUID into ljCreditAppRequestLineMailingReceiptAddress
								from mailingReceiptAddress in ljCreditAppRequestLineMailingReceiptAddress.DefaultIfEmpty()
								join receiptAddress in address on creditAppLine.ReceiptAddressGUID equals receiptAddress.AddressTransGUID into ljCreditAppRequestLineReceiptAddressGUID
								from receiptAddress in ljCreditAppRequestLineReceiptAddressGUID.DefaultIfEmpty()
								select new CreditAppLineItemViewMap
								{
									CreditAppLineGUID = creditAppLine.CreditAppLineGUID,
									UnboundBillingAddress = (billingAddress != null) ? SmartAppUtil.GetAddressLabel(billingAddress.Address1, billingAddress.Address2) : null,
									UnboundInvoiceAddress = (invoiceAddress != null) ? SmartAppUtil.GetAddressLabel(invoiceAddress.Address1, invoiceAddress.Address2) : null,
									UnboundMailingReceiptAddress = (mailingReceiptAddress != null) ? SmartAppUtil.GetAddressLabel(mailingReceiptAddress.Address1, mailingReceiptAddress.Address2) : null,
									UnboundReceiptAddress = (receiptAddress != null) ? SmartAppUtil.GetAddressLabel(receiptAddress.Address1, receiptAddress.Address2) : null,
									BillingAddress_Values = (billingAddress != null) ? SmartAppUtil.GetDropDownLabel(billingAddress.Name, billingAddress.Address1) : null,
									InvoiceAddress_Values = (invoiceAddress != null) ? SmartAppUtil.GetDropDownLabel(invoiceAddress.Name, invoiceAddress.Address1) : null,
									MailingReceiptAddress_Values = (mailingReceiptAddress != null) ? SmartAppUtil.GetDropDownLabel(mailingReceiptAddress.Name, mailingReceiptAddress.Address1) : null,
									ReceiptAddress_Values = (receiptAddress != null) ? SmartAppUtil.GetDropDownLabel(receiptAddress.Name, receiptAddress.Address1) : null,
								});
			var contactPersonLabel = (from creditAppLine in Entity
									  join billingContactPerson in db.Set<ContactPersonTrans>() on creditAppLine.BillingContactPersonGUID equals billingContactPerson.ContactPersonTransGUID into ljCreditAppTableBillingContactPerson
									  from billingContactPerson in ljCreditAppTableBillingContactPerson.DefaultIfEmpty()
									  join receiptContactPerson in db.Set<ContactPersonTrans>() on creditAppLine.ReceiptContactPersonGUID equals receiptContactPerson.ContactPersonTransGUID into ljCreditAppTableReceiptContactPerson
									  from receiptContactPerson in ljCreditAppTableReceiptContactPerson.DefaultIfEmpty()
									  join billingRelatedPersonTable in db.Set<RelatedPersonTable>() on billingContactPerson.RelatedPersonTableGUID equals billingRelatedPersonTable.RelatedPersonTableGUID into ljCreditAppTableReceiptBillingRelatedPersonTable
									  from billingRelatedPersonTable in ljCreditAppTableReceiptBillingRelatedPersonTable.DefaultIfEmpty()
									  join receiptRelatedPersonTable in db.Set<RelatedPersonTable>() on receiptContactPerson.RelatedPersonTableGUID equals receiptRelatedPersonTable.RelatedPersonTableGUID into ljCreditAppTableReceiptReceiptRelatedPersonTable
									  from receiptRelatedPersonTable in ljCreditAppTableReceiptReceiptRelatedPersonTable.DefaultIfEmpty()
									  select new CreditAppLineItemViewMap
									  {
										  CreditAppLineGUID = creditAppLine.CreditAppLineGUID,
										  BillingContactPerson_Values = (billingRelatedPersonTable != null) ? SmartAppUtil.GetDropDownLabel(billingRelatedPersonTable.RelatedPersonId, billingRelatedPersonTable.Name) : null,
										  ReceiptContactPerson_Values = (receiptRelatedPersonTable != null) ? SmartAppUtil.GetDropDownLabel(receiptRelatedPersonTable.RelatedPersonId, receiptRelatedPersonTable.Name) : null,
									  });
			var otherLabel = (from creditAppLine in Entity
							  join creditAppTable in db.Set<CreditAppTable>() on creditAppLine.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID

							  join creditAppRequestLine in db.Set<CreditAppRequestLine>() on creditAppLine.RefCreditAppRequestLineGUID equals creditAppRequestLine.CreditAppRequestLineGUID into ljCreditAppLineCreditAppRequestLine
							  from creditAppRequestLine in ljCreditAppLineCreditAppRequestLine.DefaultIfEmpty()

							  join creditAppRequestTable in db.Set<CreditAppRequestTable>() on creditAppRequestLine.CreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID

							  join creditTerm in db.Set<CreditTerm>() on creditAppLine.CreditTermGUID equals creditTerm.CreditTermGUID into ljCreditAppLineCreditAppTable
							  from creditTerm in ljCreditAppLineCreditAppTable.DefaultIfEmpty()

							  join billingResponsibleBy in db.Set<BillingResponsibleBy>() on creditAppLine.BillingResponsibleByGUID equals billingResponsibleBy.BillingResponsibleByGUID into ljCreditAppLineBillingResponsibleBy
							  from billingResponsibleBy in ljCreditAppLineBillingResponsibleBy.DefaultIfEmpty()

							  join assignmentMethod in db.Set<AssignmentMethod>() on creditAppLine.AssignmentMethodGUID equals assignmentMethod.AssignmentMethodGUID into ljCreditAppLineAssignmentMethod
							  from assignmentMethod in ljCreditAppLineAssignmentMethod.DefaultIfEmpty()

							  join assignmentAgreementTable in db.Set<AssignmentAgreementTable>() on creditAppLine.AssignmentAgreementTableGUID equals assignmentAgreementTable.AssignmentAgreementTableGUID into ljCreditAppLineAssignmentAgreementTable
							  from assignmentAgreementTable in ljCreditAppLineAssignmentAgreementTable.DefaultIfEmpty()

							  join methodOfPayment in db.Set<MethodOfPayment>() on creditAppLine.MethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID into ljCreditAppLineMethodOfPayment
							  from methodOfPayment in ljCreditAppLineMethodOfPayment.DefaultIfEmpty()

							  select new CreditAppLineItemViewMap
							  {
								  CreditAppLineGUID = creditAppLine.CreditAppLineGUID,
								  CreditAppTable_Values = (creditAppTable != null) ? SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description) : null,
								  RefCreditAppRequestLine_Values = (creditAppRequestLine != null) ? SmartAppUtil.GetDropDownLabel(creditAppRequestLine.LineNum.ToString()) : null,
								  RefCreditAppRequestTable_Values = SmartAppUtil.GetDropDownLabel(creditAppRequestTable.CreditAppRequestId, creditAppRequestTable.Description),
								  BillingResponsibleBy_Values = (billingResponsibleBy != null) ? SmartAppUtil.GetDropDownLabel(billingResponsibleBy.BillingResponsibleById, billingResponsibleBy.Description) : null,
								  AssignmentMethod_Values = (assignmentMethod != null) ? SmartAppUtil.GetDropDownLabel(assignmentMethod.AssignmentMethodId, assignmentMethod.Description) : null,
								  AssignmentAgreementTable_Values = (assignmentAgreementTable != null) ? SmartAppUtil.GetDropDownLabel(assignmentAgreementTable.AssignmentAgreementId, assignmentAgreementTable.Description) : null,
								  CreditTerm_Values = (creditTerm != null) ? SmartAppUtil.GetDropDownLabel(creditTerm.CreditTermId, creditTerm.Description) : null,
								  MethodOfPayment_Values = (methodOfPayment != null) ? SmartAppUtil.GetDropDownLabel(methodOfPayment.MethodOfPaymentId, methodOfPayment.Description) : null,
							  });
			return (from creditAppLine in Entity
					join company in db.Set<Company>()
					on creditAppLine.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on creditAppLine.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCreditAppLineOwnerBU
					from ownerBU in ljCreditAppLineOwnerBU.DefaultIfEmpty()
					join buyerTable in db.Set<BuyerTable>() on creditAppLine.BuyerTableGUID equals buyerTable.BuyerTableGUID
					join businessType in db.Set<BusinessType>() on buyerTable.BusinessTypeGUID equals businessType.BusinessTypeGUID into ljCreditAppLineBusinessTypes
					from businessType in ljCreditAppLineBusinessTypes.DefaultIfEmpty()
					join lineOfBusiness in db.Set<LineOfBusiness>() on buyerTable.LineOfBusinessGUID equals lineOfBusiness.LineOfBusinessGUID into ljCreditAppLineLineOfBusiness
					from lineOfBusiness in ljCreditAppLineLineOfBusiness.DefaultIfEmpty()
					join businessSegment in db.Set<BusinessSegment>() on buyerTable.BusinessSegmentGUID equals businessSegment.BusinessSegmentGUID into ljCreditAppLineBusinessSegment
					from businessSegment in ljCreditAppLineBusinessSegment.DefaultIfEmpty()
					join addLabel in addressLabel on creditAppLine.CreditAppLineGUID equals addLabel.CreditAppLineGUID into ljCreditAppRequestTableAddLabel
					from addLabel in ljCreditAppRequestTableAddLabel.DefaultIfEmpty()
					join contactLabel in contactPersonLabel on creditAppLine.CreditAppLineGUID equals contactLabel.CreditAppLineGUID into ljCreditAppRequestTableContactPersonLabel
					from contactLabel in ljCreditAppRequestTableContactPersonLabel.DefaultIfEmpty()
					join _otherLabel in otherLabel on creditAppLine.CreditAppLineGUID equals _otherLabel.CreditAppLineGUID into ljCreditAppRequestTableOtherLabel
					from _otherLabel in ljCreditAppRequestTableOtherLabel.DefaultIfEmpty()
					select new CreditAppLineItemViewMap
					{
						CompanyGUID = creditAppLine.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = creditAppLine.Owner,
						OwnerBusinessUnitGUID = creditAppLine.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = creditAppLine.CreatedBy,
						CreatedDateTime = creditAppLine.CreatedDateTime,
						ModifiedBy = creditAppLine.ModifiedBy,
						ModifiedDateTime = creditAppLine.ModifiedDateTime,
						CreditAppLineGUID = creditAppLine.CreditAppLineGUID,
						AcceptanceDocument = creditAppLine.AcceptanceDocument,
						AcceptanceDocumentDescription = creditAppLine.AcceptanceDocumentDescription,
						ApprovedCreditLimitLine = creditAppLine.ApprovedCreditLimitLine,
						AssignmentAgreementTableGUID = creditAppLine.AssignmentAgreementTableGUID,
						AssignmentMethodGUID = creditAppLine.AssignmentMethodGUID,
						AssignmentMethodRemark = creditAppLine.AssignmentMethodRemark,
						BillingAddressGUID = creditAppLine.BillingAddressGUID,
						BillingContactPersonGUID = creditAppLine.BillingContactPersonGUID,
						BillingDay = creditAppLine.BillingDay,
						BillingDescription = creditAppLine.BillingDescription,
						BillingRemark = creditAppLine.BillingRemark,
						BillingResponsibleByGUID = creditAppLine.BillingResponsibleByGUID,
						BuyerTableGUID = creditAppLine.BuyerTableGUID,
						CreditAppTableGUID = creditAppLine.CreditAppTableGUID,
						CreditTermDescription = creditAppLine.CreditTermDescription,
						CreditTermGUID = creditAppLine.CreditTermGUID,
						ExpiryDate = creditAppLine.ExpiryDate,
						InsuranceCreditLimit = creditAppLine.InsuranceCreditLimit,
						InvoiceAddressGUID = creditAppLine.InvoiceAddressGUID,
						LineNum = creditAppLine.LineNum,
						MailingReceiptAddressGUID = creditAppLine.MailingReceiptAddressGUID,
						MaxPurchasePct = creditAppLine.MaxPurchasePct,
						MethodOfBilling = creditAppLine.MethodOfBilling,
						MethodOfPaymentGUID = creditAppLine.MethodOfPaymentGUID,
						PaymentCondition = creditAppLine.PaymentCondition,
						PurchaseFeeCalculateBase = creditAppLine.PurchaseFeeCalculateBase,
						PurchaseFeePct = creditAppLine.PurchaseFeePct,
						ReceiptAddressGUID = creditAppLine.ReceiptAddressGUID,
						ReceiptContactPersonGUID = creditAppLine.ReceiptContactPersonGUID,
						ReceiptDay = creditAppLine.ReceiptDay,
						ReceiptDescription = creditAppLine.ReceiptDescription,
						ReceiptRemark = creditAppLine.ReceiptRemark,
						ReviewDate = creditAppLine.ReviewDate,

						UnboundBillingAddress = (addLabel != null) ? addLabel.UnboundBillingAddress : null,
						UnboundInvoiceAddress = (addLabel != null) ? addLabel.UnboundInvoiceAddress : null,
						UnboundMailingReceiptAddress = (addLabel != null) ? addLabel.UnboundMailingReceiptAddress : null,
						UnboundReceiptAddress = (addLabel != null) ? addLabel.UnboundReceiptAddress : null,
						BillingAddress_Values = (addLabel != null) ? addLabel.BillingAddress_Values : null,
						InvoiceAddress_Values = (addLabel != null) ? addLabel.InvoiceAddress_Values : null,
						MailingReceiptAddress_Values = (addLabel != null) ? addLabel.MailingReceiptAddress_Values : null,
						ReceiptAddress_Values = (addLabel != null) ? addLabel.ReceiptAddress_Values : null,
						BusinessSegment_Values = (businessSegment != null) ? SmartAppUtil.GetDropDownLabel(businessSegment.BusinessSegmentId, businessSegment.Description) : null,
						BusinessType_Values = (businessType != null) ? SmartAppUtil.GetDropDownLabel(businessType.BusinessTypeId, businessType.Description) : null,
						LineOfBusiness_Values = (lineOfBusiness != null) ? SmartAppUtil.GetDropDownLabel(lineOfBusiness.LineOfBusinessId, lineOfBusiness.Description) : null,
						DateOfEstablish = buyerTable.DateOfEstablish,

						BillingContactPerson_Values = (contactLabel != null) ? contactLabel.BillingContactPerson_Values : null,
						ReceiptContactPerson_Values = (contactLabel != null) ? contactLabel.ReceiptContactPerson_Values : null,
						CreditAppTable_Values = (_otherLabel != null) ? _otherLabel.CreditAppTable_Values : null,
						RefCreditAppRequestLine_Values = (_otherLabel != null) ? _otherLabel.RefCreditAppRequestLine_Values : null,
						RefCreditAppRequestTable_Values = (_otherLabel != null) ? _otherLabel.RefCreditAppRequestTable_Values : null,
						CreditTerm_Values = (_otherLabel != null) ? _otherLabel.CreditTerm_Values : null,
						MethodOfPayment_Values = (_otherLabel != null) ? _otherLabel.MethodOfPayment_Values : null,
						BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
						BillingResponsibleBy_Values = (_otherLabel != null) ? _otherLabel.BillingResponsibleBy_Values : null,
						AssignmentMethod_Values = (_otherLabel != null) ? _otherLabel.AssignmentMethod_Values : null,
						AssignmentAgreementTable_Values = (_otherLabel != null) ? _otherLabel.AssignmentAgreementTable_Values : null,
						LineCondition = creditAppLine.LineCondition,
					
						RowVersion = creditAppLine.RowVersion,
					});
		}
		public CreditAppLineItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<CreditAppLineItemViewMap, CreditAppLineItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public CreditAppLine CreateCreditAppLine(CreditAppLine creditAppLine)
		{
			try
			{
				creditAppLine.CreditAppLineGUID = Guid.NewGuid();
				base.Add(creditAppLine);
				return creditAppLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCreditAppLineVoid(CreditAppLine creditAppLine)
		{
			try
			{
				CreateCreditAppLine(creditAppLine);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditAppLine UpdateCreditAppLine(CreditAppLine dbCreditAppLine, CreditAppLine inputCreditAppLine, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCreditAppLine = dbCreditAppLine.MapUpdateValues<CreditAppLine>(inputCreditAppLine);
				base.Update(dbCreditAppLine);
				return dbCreditAppLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCreditAppLineVoid(CreditAppLine dbCreditAppLine, CreditAppLine inputCreditAppLine, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCreditAppLine = dbCreditAppLine.MapUpdateValues<CreditAppLine>(inputCreditAppLine, skipUpdateFields);
				base.Update(dbCreditAppLine);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public IEnumerable<CreditAppLine> GetCreditAppLineByCompanyIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CompanyGUID == guid);
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<CreditAppLine> GetCreditAppLineByCreditAppIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CreditAppTableGUID == guid);
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IQueryable<CreditAppLineItemView> GetQueryForApproveCreditApplicationRequestK2(Guid guid)
		{
			return (from creditAppLine in Entity
					join buyerTable in db.Set<BuyerTable>() on creditAppLine.BuyerTableGUID equals buyerTable.BuyerTableGUID
					where creditAppLine.CreditAppTableGUID == guid
					select new CreditAppLineItemView
					{
						CompanyGUID = creditAppLine.CompanyGUID.GuidNullToString(),
						CreditAppLineGUID = creditAppLine.CreditAppLineGUID.GuidNullToString(),
						CreditAppTableGUID = creditAppLine.CreditAppTableGUID.GuidNullToString(),
						ExpiryDate = creditAppLine.ExpiryDate.DateToString(),
						BuyerTableGUID = creditAppLine.BuyerTableGUID.GuidNullToString(),
						BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
					});
		}
		public IEnumerable<CreditAppLine> GetCreditAppLineByRefCreditAppLine(List<Guid> refCreditAppLineGUID)
		{
			try
			{
				return Entity.Where(w => refCreditAppLineGUID.Any(a => a == w.CreditAppLineGUID)).AsNoTracking();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<CreditAppLine> GetCreditAppLineByBuyerIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(w => w.BuyerTableGUID == guid).AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IQueryable<CALineOutstandingViewMap> GetCALineOutstandingByCA()
		{
			try
			{
				var getCreditDeductAmountByCreditAppLineList = (from creditAppTrans in db.Set<CreditAppTrans>()
																join creditAppLine in db.Set<CreditAppLine>()
																on creditAppTrans.CreditAppLineGUID equals creditAppLine.CreditAppLineGUID
																group new { creditAppTrans.CreditDeductAmount } by new { creditAppLine.CreditAppLineGUID } into gcal
																select new CALineOutstandingViewMap
																{
																	CreditAppLineGUID = gcal.Key.CreditAppLineGUID,
																	CreditDeductAmount = gcal.Sum(s => s.CreditDeductAmount)
																});
				var getArBalanceByCreditAppLineList = (from custTrans in db.Set<CustTrans>().Where(w => w.CustTransStatus == (int)CustTransStatus.Open)
													   join invoiceTable in db.Set<InvoiceTable>().Where(w => w.ProductInvoice == true)
													   on custTrans.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID
													   join creditAppLine in db.Set<CreditAppLine>()
													   on custTrans.CreditAppLineGUID equals creditAppLine.CreditAppLineGUID
													   group new { ARBalance = custTrans.TransAmount - custTrans.SettleAmount } by new { creditAppLine.CreditAppLineGUID } into gcal
													   select new CALineOutstandingViewMap
													   {
														   CreditAppLineGUID = gcal.Key.CreditAppLineGUID,
														   ARBalance = gcal.Sum(s => s.ARBalance)
													   });
				IQueryable<CALineOutstandingViewMap> caLineOutstandingViews = (from creditAppTable in db.Set<CreditAppTable>()
																			   join creditAppLine in db.Set<CreditAppLine>()
																			   on creditAppTable.CreditAppTableGUID equals creditAppLine.CreditAppTableGUID
																			   join creditAppRequestLine in db.Set<CreditAppRequestLine>()
																			   on creditAppLine.RefCreditAppRequestLineGUID equals creditAppRequestLine.CreditAppRequestLineGUID into ljCreditAppLineCreditAppRequestLine
																			   from creditAppRequestLine in ljCreditAppLineCreditAppRequestLine.DefaultIfEmpty()
                                                                               join creditAppRequestTable in db.Set<CreditAppRequestTable>()
                                                                               on creditAppRequestLine.CreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID into ljCreditAppLineCreditAppRequestTable
                                                                               from creditAppRequestTable in ljCreditAppLineCreditAppRequestTable.DefaultIfEmpty()
                                                                               join getCreditDeductAmountByCreditAppLine in getCreditDeductAmountByCreditAppLineList
																			   on creditAppLine.CreditAppLineGUID equals getCreditDeductAmountByCreditAppLine.CreditAppLineGUID into ljCreditAppLineGetCreditDeductAmountByCreditAppLineList
																			   from sumCreditDeductAmountByCreditAppLine in ljCreditAppLineGetCreditDeductAmountByCreditAppLineList.DefaultIfEmpty()
																			   join getArBalanceByCreditAppLine in getArBalanceByCreditAppLineList
																			   on creditAppLine.CreditAppLineGUID equals getArBalanceByCreditAppLine.CreditAppLineGUID into ljCreditAppLineGetArBalanceByCreditAppLineList
																			   from getArBalanceByCreditAppLine in ljCreditAppLineGetArBalanceByCreditAppLineList.DefaultIfEmpty()
																			   join creditLimitType in db.Set<CreditLimitType>()
																			   on creditAppTable.CreditLimitTypeGUID equals creditLimitType.CreditLimitTypeGUID into ljcreditLimitType
																			   from creditLimitType in ljcreditLimitType.DefaultIfEmpty()
																			   join customerTable in db.Set<CustomerTable>()
																			   on creditAppTable.CustomerTableGUID equals customerTable.CustomerTableGUID into ljcustomerTable
																			   from customerTable in ljcustomerTable.DefaultIfEmpty()
																			   join buyerTable in db.Set<BuyerTable>()
																			   on creditAppLine.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
																			   from buyerTable in ljbuyerTable.DefaultIfEmpty()
																			   select new CALineOutstandingViewMap
																			   {
																				   CreditAppTableGUID = creditAppLine.CreditAppTableGUID,
																				   CreditAppId = creditAppTable.CreditAppId,
																				   CreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
																				   ProductType = creditAppTable.ProductType,
																				   CreditLimitTypeGUID = creditAppTable.CreditLimitTypeGUID,
																				   CreditLimitTypeId = creditLimitType.CreditLimitTypeId,
																				   CreditLimitType_Values = SmartAppUtil.GetDropDownLabel(creditLimitType.CreditLimitTypeId, creditLimitType.Description),
																				   CustomerTableGUID = creditAppTable.CustomerTableGUID,
																				   CustomerId = customerTable.CustomerId,
																				   CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
																				   BuyerTableGUID = creditAppLine.BuyerTableGUID,
																				   BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
																				   BuyerId = buyerTable.BuyerId,
																				   ApprovedCreditLimitLine = creditAppLine.ApprovedCreditLimitLine,
																				   ExpiryDate = creditAppLine.ExpiryDate,
																				   ExpiryDate_Values = creditAppLine.ExpiryDate.DateToString(),
																				   LineNum = creditAppLine.LineNum,
																				   StartDate = (creditAppRequestTable != null) ? creditAppRequestTable.ApprovedDate : null,
																				   StartDate_Values = (creditAppRequestTable != null) ? creditAppRequestTable.ApprovedDate.DateNullToString() : null,
																				   CreditLimitBalance = (sumCreditDeductAmountByCreditAppLine != null) ? creditAppLine.ApprovedCreditLimitLine - sumCreditDeductAmountByCreditAppLine.CreditDeductAmount : creditAppLine.ApprovedCreditLimitLine,
																				   ARBalance = (getArBalanceByCreditAppLine != null) ? getArBalanceByCreditAppLine.ARBalance : 0,
																				   CompanyGUID = creditAppLine.CompanyGUID,
																				   MaxPurchasePct = creditAppLine.MaxPurchasePct,
																				   AssignmentMethodGUID = creditAppLine.AssignmentMethodGUID,
																				   BillingResponsibleByGUID = creditAppLine.BillingResponsibleByGUID,
																				   MethodOfPaymentGUID = creditAppLine.MethodOfPaymentGUID,
																				   CreditAppLineGUID = creditAppLine.CreditAppLineGUID,
																				   CreditAppRequestTableGUID = (creditAppRequestTable != null) ? creditAppRequestTable.CreditAppRequestTableGUID : (Guid?)null,
																				   RefType = 0,
																			   });
				return caLineOutstandingViews;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public SearchResult<CALineOutstandingView> GetCALineOutstandingByCA(SearchParameter search)
		{
			var result = new SearchResult<CALineOutstandingView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CreditAppLine>(search, SysParm.CompanyGUID);
				var total = GetCALineOutstandingByCA().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetCALineOutstandingByCA().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CALineOutstandingViewMap, CALineOutstandingView>();
				result = list.SetSearchResult<CALineOutstandingView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public SearchResult<CALineOutstandingView> GetCALineOutstandingByCA2(SearchParameter search)
		{
			var result = new SearchResult<CALineOutstandingView>();
			try
			{
				search.Conditions[12].Value= null;
				var predicate = base.GetFilterLevelPredicate<CreditAppLine>(search, SysParm.CompanyGUID);
				var total = GetCALineOutstandingByCA().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetCALineOutstandingByCA().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CALineOutstandingViewMap, CALineOutstandingView>();
				result = list.SetSearchResult<CALineOutstandingView>(total, search);

				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public override void ValidateAdd(IEnumerable<CreditAppLine> items)
		{
			base.ValidateAdd(items);
		}
		public override void ValidateAdd(CreditAppLine item)
		{
			base.ValidateAdd(item);
		}
		public List<CALineOutstandingViewMap> GetCALineOutstandingByCA(int refType, Guid refGUID, bool active, DateTime? asOfDate)
		{
			try
			{
				DateTime minimumExpiryDate = (asOfDate.HasValue && active) ? asOfDate.Value : DateTime.MinValue;
				List<CALineOutstandingViewMap> caLineOutstandingViewMap =
					(refType == (int)RefType.Buyer)
					? GetCALineOutstandingByCA().Where(w => w.BuyerTableGUID == refGUID && w.ExpiryDate > minimumExpiryDate).ToList()
					: (refType == (int)RefType.CreditAppTable)
					  ? GetCALineOutstandingByCA().Where(w => w.CreditAppTableGUID == refGUID && w.ExpiryDate > minimumExpiryDate).ToList()
					  : null;
                if (caLineOutstandingViewMap.Any() && active)
                {
					caLineOutstandingViewMap.ForEach(f => f.Status = "Active");
				}
				return caLineOutstandingViewMap;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);

			}
		}
	}
}

