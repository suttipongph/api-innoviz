using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IInterestTypeRepo
	{
		#region DropDown
		IEnumerable<SelectItem<InterestTypeItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<InterestTypeListView> GetListvw(SearchParameter search);
		InterestTypeItemView GetByIdvw(Guid id);
		InterestType Find(params object[] keyValues);
		InterestType GetInterestTypeByIdNoTracking(Guid guid);
		InterestType CreateInterestType(InterestType interestType);
		void CreateInterestTypeVoid(InterestType interestType);
		InterestType UpdateInterestType(InterestType dbInterestType, InterestType inputInterestType, List<string> skipUpdateFields = null);
		void UpdateInterestTypeVoid(InterestType dbInterestType, InterestType inputInterestType, List<string> skipUpdateFields = null);
		void Remove(InterestType item);
	}
	public class InterestTypeRepo : CompanyBaseRepository<InterestType>, IInterestTypeRepo
	{
		public InterestTypeRepo(SmartAppDbContext context) : base(context) { }
		public InterestTypeRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public InterestType GetInterestTypeByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.InterestTypeGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<InterestTypeItemViewMap> GetDropDownQuery()
		{
			return (from interestType in Entity
					select new InterestTypeItemViewMap
					{
						CompanyGUID = interestType.CompanyGUID,
						Owner = interestType.Owner,
						OwnerBusinessUnitGUID = interestType.OwnerBusinessUnitGUID,
						InterestTypeGUID = interestType.InterestTypeGUID,
						InterestTypeId = interestType.InterestTypeId,
						Description = interestType.Description
					});
		}
		public IEnumerable<SelectItem<InterestTypeItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<InterestType>(search, SysParm.CompanyGUID);
				var interestType = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<InterestTypeItemViewMap, InterestTypeItemView>().ToDropDownItem(search.ExcludeRowData);


				return interestType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<InterestTypeListViewMap> GetListQuery()
		{
			return (from interestType in Entity
				select new InterestTypeListViewMap
				{
						CompanyGUID = interestType.CompanyGUID,
						Owner = interestType.Owner,
						OwnerBusinessUnitGUID = interestType.OwnerBusinessUnitGUID,
						InterestTypeGUID = interestType.InterestTypeGUID,
						InterestTypeId = interestType.InterestTypeId,
						Description = interestType.Description,
				});
		}
		public SearchResult<InterestTypeListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<InterestTypeListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<InterestType>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<InterestTypeListViewMap, InterestTypeListView>();
				result = list.SetSearchResult<InterestTypeListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<InterestTypeItemViewMap> GetItemQuery()
		{
			return (from interestType in Entity
					join company in db.Set<Company>()
					on interestType.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on interestType.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljInterestTypeOwnerBU
					from ownerBU in ljInterestTypeOwnerBU.DefaultIfEmpty()
					select new InterestTypeItemViewMap
					{
						CompanyGUID = interestType.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = interestType.Owner,
						OwnerBusinessUnitGUID = interestType.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = interestType.CreatedBy,
						CreatedDateTime = interestType.CreatedDateTime,
						ModifiedBy = interestType.ModifiedBy,
						ModifiedDateTime = interestType.ModifiedDateTime,
						InterestTypeGUID = interestType.InterestTypeGUID,
						Description = interestType.Description,
						InterestTypeId = interestType.InterestTypeId,
					
						RowVersion = interestType.RowVersion,
					});
		}
		public InterestTypeItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<InterestTypeItemViewMap, InterestTypeItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public InterestType CreateInterestType(InterestType interestType)
		{
			try
			{
				interestType.InterestTypeGUID = Guid.NewGuid();
				base.Add(interestType);
				return interestType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateInterestTypeVoid(InterestType interestType)
		{
			try
			{
				CreateInterestType(interestType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public InterestType UpdateInterestType(InterestType dbInterestType, InterestType inputInterestType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbInterestType = dbInterestType.MapUpdateValues<InterestType>(inputInterestType);
				base.Update(dbInterestType);
				return dbInterestType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateInterestTypeVoid(InterestType dbInterestType, InterestType inputInterestType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbInterestType = dbInterestType.MapUpdateValues<InterestType>(inputInterestType, skipUpdateFields);
				base.Update(dbInterestType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

