using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface INumberSeqSegmentRepo
	{
		#region DropDown
		IEnumerable<SelectItem<NumberSeqSegmentItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<NumberSeqSegmentListView> GetListvw(SearchParameter search);
		NumberSeqSegmentItemView GetByIdvw(Guid id);
		NumberSeqSegment Find(params object[] keyValues);
		NumberSeqSegment GetNumberSeqSegmentByIdNoTracking(Guid guid);
		NumberSeqSegment CreateNumberSeqSegment(NumberSeqSegment numberSeqSegment);
		void CreateNumberSeqSegmentVoid(NumberSeqSegment numberSeqSegment);
		NumberSeqSegment UpdateNumberSeqSegment(NumberSeqSegment dbNumberSeqSegment, NumberSeqSegment inputNumberSeqSegment, List<string> skipUpdateFields = null);
		void UpdateNumberSeqSegmentVoid(NumberSeqSegment dbNumberSeqSegment, NumberSeqSegment inputNumberSeqSegment, List<string> skipUpdateFields = null);
		void Remove(NumberSeqSegment item);
		IQueryable<NumberSeqSegment> GetNumberSeqSegmentByNumberSeqTableIdNoTracking(Guid guid);
		int GetLastOrdering(string numberSeqTableGUID);
	}
	public class NumberSeqSegmentRepo : CompanyBaseRepository<NumberSeqSegment>, INumberSeqSegmentRepo
	{
		public NumberSeqSegmentRepo(SmartAppDbContext context) : base(context) { }
		public NumberSeqSegmentRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public NumberSeqSegment GetNumberSeqSegmentByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.NumberSeqSegmentGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IQueryable<NumberSeqSegment> GetNumberSeqSegmentByNumberSeqTableIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.NumberSeqTableGUID == guid).AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<NumberSeqSegmentItemViewMap> GetDropDownQuery()
		{
			return (from numberSeqSegment in Entity
					select new NumberSeqSegmentItemViewMap
					{
						CompanyGUID = numberSeqSegment.CompanyGUID,
						Owner = numberSeqSegment.Owner,
						OwnerBusinessUnitGUID = numberSeqSegment.OwnerBusinessUnitGUID,
						NumberSeqSegmentGUID = numberSeqSegment.NumberSeqSegmentGUID
					});
		}
		public IEnumerable<SelectItem<NumberSeqSegmentItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<NumberSeqSegment>(search, SysParm.CompanyGUID);
				var numberSeqSegment = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<NumberSeqSegmentItemViewMap, NumberSeqSegmentItemView>().ToDropDownItem(search.ExcludeRowData);


				return numberSeqSegment;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<NumberSeqSegmentListViewMap> GetListQuery()
		{
			return (from numberSeqSegment in Entity
				select new NumberSeqSegmentListViewMap
				{
						CompanyGUID = numberSeqSegment.CompanyGUID,
						Owner = numberSeqSegment.Owner,
						OwnerBusinessUnitGUID = numberSeqSegment.OwnerBusinessUnitGUID,
						NumberSeqSegmentGUID = numberSeqSegment.NumberSeqSegmentGUID,
						NumberSeqTableGUID = numberSeqSegment.NumberSeqTableGUID,
						Ordering = numberSeqSegment.Ordering,
						SegmentType = numberSeqSegment.SegmentType,
						SegmentValue = numberSeqSegment.SegmentValue
				});
		}
		public SearchResult<NumberSeqSegmentListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<NumberSeqSegmentListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<NumberSeqSegment>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<NumberSeqSegmentListViewMap, NumberSeqSegmentListView>();
				result = list.SetSearchResult<NumberSeqSegmentListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<NumberSeqSegmentItemViewMap> GetItemQuery()
		{
			return (from numberSeqSegment in Entity
					join company in db.Set<Company>()
					on numberSeqSegment.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on numberSeqSegment.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljNumberSeqSegmentOwnerBU
					from ownerBU in ljNumberSeqSegmentOwnerBU.DefaultIfEmpty()
					select new NumberSeqSegmentItemViewMap
					{
						CompanyGUID = numberSeqSegment.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = numberSeqSegment.Owner,
						OwnerBusinessUnitGUID = numberSeqSegment.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = numberSeqSegment.CreatedBy,
						CreatedDateTime = numberSeqSegment.CreatedDateTime,
						ModifiedBy = numberSeqSegment.ModifiedBy,
						ModifiedDateTime = numberSeqSegment.ModifiedDateTime,
						NumberSeqSegmentGUID = numberSeqSegment.NumberSeqSegmentGUID,
						NumberSeqTableGUID = numberSeqSegment.NumberSeqTableGUID,
						Ordering = numberSeqSegment.Ordering,
						SegmentType = numberSeqSegment.SegmentType,
						SegmentValue = numberSeqSegment.SegmentValue,
					
						RowVersion = numberSeqSegment.RowVersion,
					});
		}
		public NumberSeqSegmentItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<NumberSeqSegmentItemViewMap, NumberSeqSegmentItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public NumberSeqSegment CreateNumberSeqSegment(NumberSeqSegment numberSeqSegment)
		{
			try
			{
				numberSeqSegment.NumberSeqSegmentGUID = Guid.NewGuid();
				base.Add(numberSeqSegment);
				return numberSeqSegment;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateNumberSeqSegmentVoid(NumberSeqSegment numberSeqSegment)
		{
			try
			{
				CreateNumberSeqSegment(numberSeqSegment);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public NumberSeqSegment UpdateNumberSeqSegment(NumberSeqSegment dbNumberSeqSegment, NumberSeqSegment inputNumberSeqSegment, List<string> skipUpdateFields = null)
		{
			try
			{
				dbNumberSeqSegment = dbNumberSeqSegment.MapUpdateValues<NumberSeqSegment>(inputNumberSeqSegment);
				base.Update(dbNumberSeqSegment);
				return dbNumberSeqSegment;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateNumberSeqSegmentVoid(NumberSeqSegment dbNumberSeqSegment, NumberSeqSegment inputNumberSeqSegment, List<string> skipUpdateFields = null)
		{
			try
			{
				dbNumberSeqSegment = dbNumberSeqSegment.MapUpdateValues<NumberSeqSegment>(inputNumberSeqSegment, skipUpdateFields);
				base.Update(dbNumberSeqSegment);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public override void ValidateRemove(NumberSeqSegment item)
		{
			int lastOrdering = GetLastOrdering(item.NumberSeqTableGUID.ToString());
			if (item.Ordering != lastOrdering)
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				ex.AddData("ERROR.00318");
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public int GetLastOrdering(string numberSeqTableGUID)
		{
			try
			{
				Guid guid = new Guid(numberSeqTableGUID);
				return Entity.Where(a => a.NumberSeqTableGUID == guid).DefaultIfEmpty().Max(m => m == null ? 0 : m.Ordering);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

