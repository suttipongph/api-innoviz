using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IBillingResponsibleByRepo
	{
		#region DropDown
		IEnumerable<SelectItem<BillingResponsibleByItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<BillingResponsibleByListView> GetListvw(SearchParameter search);
		BillingResponsibleByItemView GetByIdvw(Guid id);
		BillingResponsibleBy Find(params object[] keyValues);
		BillingResponsibleBy GetBillingResponsibleByByIdNoTracking(Guid guid);
		BillingResponsibleBy CreateBillingResponsibleBy(BillingResponsibleBy billingResponsibleBy);
		void CreateBillingResponsibleByVoid(BillingResponsibleBy billingResponsibleBy);
		BillingResponsibleBy UpdateBillingResponsibleBy(BillingResponsibleBy dbBillingResponsibleBy, BillingResponsibleBy inputBillingResponsibleBy, List<string> skipUpdateFields = null);
		void UpdateBillingResponsibleByVoid(BillingResponsibleBy dbBillingResponsibleBy, BillingResponsibleBy inputBillingResponsibleBy, List<string> skipUpdateFields = null);
		void Remove(BillingResponsibleBy item);
	}
	public class BillingResponsibleByRepo : CompanyBaseRepository<BillingResponsibleBy>, IBillingResponsibleByRepo
	{
		public BillingResponsibleByRepo(SmartAppDbContext context) : base(context) { }
		public BillingResponsibleByRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BillingResponsibleBy GetBillingResponsibleByByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BillingResponsibleByGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<BillingResponsibleByItemViewMap> GetDropDownQuery()
		{
			return (from billingResponsibleBy in Entity
					select new BillingResponsibleByItemViewMap
					{
						CompanyGUID = billingResponsibleBy.CompanyGUID,
						Owner = billingResponsibleBy.Owner,
						OwnerBusinessUnitGUID = billingResponsibleBy.OwnerBusinessUnitGUID,
						BillingResponsibleByGUID = billingResponsibleBy.BillingResponsibleByGUID,
						BillingResponsibleById = billingResponsibleBy.BillingResponsibleById,
						Description = billingResponsibleBy.Description
					});
		}
		public IEnumerable<SelectItem<BillingResponsibleByItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<BillingResponsibleBy>(search, SysParm.CompanyGUID);
				var billingResponsibleBy = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<BillingResponsibleByItemViewMap, BillingResponsibleByItemView>().ToDropDownItem(search.ExcludeRowData);


				return billingResponsibleBy;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<BillingResponsibleByListViewMap> GetListQuery()
		{
			return (from billingResponsibleBy in Entity
				select new BillingResponsibleByListViewMap
				{
						CompanyGUID = billingResponsibleBy.CompanyGUID,
						Owner = billingResponsibleBy.Owner,
						OwnerBusinessUnitGUID = billingResponsibleBy.OwnerBusinessUnitGUID,
						BillingResponsibleByGUID = billingResponsibleBy.BillingResponsibleByGUID,
						BillingResponsibleById = billingResponsibleBy.BillingResponsibleById,
						Description = billingResponsibleBy.Description,
						BillingBy = billingResponsibleBy.BillingBy,
				});
		}
		public SearchResult<BillingResponsibleByListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<BillingResponsibleByListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<BillingResponsibleBy>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<BillingResponsibleByListViewMap, BillingResponsibleByListView>();
				result = list.SetSearchResult<BillingResponsibleByListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<BillingResponsibleByItemViewMap> GetItemQuery()
		{
			return (from billingResponsibleBy in Entity
					join ownerBU in db.Set<BusinessUnit>()
					on billingResponsibleBy.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljBillingResponsibleByOwnerBU
					from ownerBU in ljBillingResponsibleByOwnerBU.DefaultIfEmpty()
					join company in db.Set<Company>() on billingResponsibleBy.CompanyGUID equals company.CompanyGUID
					select new BillingResponsibleByItemViewMap
					{
						CompanyGUID = billingResponsibleBy.CompanyGUID,
						Owner = billingResponsibleBy.Owner,
						OwnerBusinessUnitGUID = billingResponsibleBy.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CompanyId = company.CompanyId,
						CreatedBy = billingResponsibleBy.CreatedBy,
						CreatedDateTime = billingResponsibleBy.CreatedDateTime,
						ModifiedBy = billingResponsibleBy.ModifiedBy,
						ModifiedDateTime = billingResponsibleBy.ModifiedDateTime,
						BillingResponsibleByGUID = billingResponsibleBy.BillingResponsibleByGUID,
						BillingBy = billingResponsibleBy.BillingBy,
						BillingResponsibleById = billingResponsibleBy.BillingResponsibleById,
						Description = billingResponsibleBy.Description,
					
						RowVersion = billingResponsibleBy.RowVersion,
					});
		}
		public BillingResponsibleByItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<BillingResponsibleByItemViewMap, BillingResponsibleByItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public BillingResponsibleBy CreateBillingResponsibleBy(BillingResponsibleBy billingResponsibleBy)
		{
			try
			{
				billingResponsibleBy.BillingResponsibleByGUID = Guid.NewGuid();
				base.Add(billingResponsibleBy);
				return billingResponsibleBy;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateBillingResponsibleByVoid(BillingResponsibleBy billingResponsibleBy)
		{
			try
			{
				CreateBillingResponsibleBy(billingResponsibleBy);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BillingResponsibleBy UpdateBillingResponsibleBy(BillingResponsibleBy dbBillingResponsibleBy, BillingResponsibleBy inputBillingResponsibleBy, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBillingResponsibleBy = dbBillingResponsibleBy.MapUpdateValues<BillingResponsibleBy>(inputBillingResponsibleBy);
				base.Update(dbBillingResponsibleBy);
				return dbBillingResponsibleBy;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateBillingResponsibleByVoid(BillingResponsibleBy dbBillingResponsibleBy, BillingResponsibleBy inputBillingResponsibleBy, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBillingResponsibleBy = dbBillingResponsibleBy.MapUpdateValues<BillingResponsibleBy>(inputBillingResponsibleBy, skipUpdateFields);
				base.Update(dbBillingResponsibleBy);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

