using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IVendorTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<VendorTableItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<VendorTableListView> GetListvw(SearchParameter search);
		VendorTableItemView GetByIdvw(Guid id);
		VendorTable Find(params object[] keyValues);
		VendorTable GetVendorTableByIdNoTracking(Guid guid);
		VendorTable GetVendorTableByIdNoTrackingByAccessLevel(Guid guid);
		VendorTable CreateVendorTable(VendorTable vendorTable);
		void CreateVendorTableVoid(VendorTable vendorTable);
		VendorTable UpdateVendorTable(VendorTable dbVendorTable, VendorTable inputVendorTable, List<string> skipUpdateFields = null);
		void UpdateVendorTableVoid(VendorTable dbVendorTable, VendorTable inputVendorTable, List<string> skipUpdateFields = null);
		void Remove(VendorTable item);
		List<VendorTable> GetVendorTableByIdNoTracking(List<Guid> vendorTableGuids);
		void ValidateAdd(VendorTable item);
		void ValidateAdd(IEnumerable<VendorTable> items);
	}
	public class VendorTableRepo : CompanyBaseRepository<VendorTable>, IVendorTableRepo
	{
		public VendorTableRepo(SmartAppDbContext context) : base(context) { }
		public VendorTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public VendorTable GetVendorTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.VendorTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public VendorTable GetVendorTableByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.VendorTableGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<VendorTableItemViewMap> GetDropDownQuery()
		{
			return (from vendorTable in Entity
					select new VendorTableItemViewMap
					{
						CompanyGUID = vendorTable.CompanyGUID,
						Owner = vendorTable.Owner,
						OwnerBusinessUnitGUID = vendorTable.OwnerBusinessUnitGUID,
						VendorTableGUID = vendorTable.VendorTableGUID,
						VendorId = vendorTable.VendorId,
						Name = vendorTable.Name,
					});
		}
		public IEnumerable<SelectItem<VendorTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<VendorTable>(search, SysParm.CompanyGUID);
				var vendorTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<VendorTableItemViewMap, VendorTableItemView>().ToDropDownItem(search.ExcludeRowData);
				return vendorTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<VendorTableListViewMap> GetListQuery()
		{
			return (from vendorTable in Entity
					join currency in db.Set<Currency>() on vendorTable.CurrencyGUID equals currency.CurrencyGUID
					join vendGroup in db.Set<VendGroup>() on vendorTable.VendGroupGUID equals vendGroup.VendGroupGUID
				select new VendorTableListViewMap
				{
						CompanyGUID = vendorTable.CompanyGUID,
						Owner = vendorTable.Owner,
						OwnerBusinessUnitGUID = vendorTable.OwnerBusinessUnitGUID,
						VendorTableGUID = vendorTable.VendorTableGUID,
						VendorId = vendorTable.VendorId,
						Name = vendorTable.Name,
						AltName = vendorTable.AltName,
						ExternalCode = vendorTable.ExternalCode,
						RecordType = vendorTable.RecordType,
						CurrencyGUID = vendorTable.CurrencyGUID,
						VendGroupGUID = vendorTable.VendGroupGUID,
						Currency_CurrencyId = currency.CurrencyId,
						VendGroup_VendGroupId = vendGroup.VendGroupId,
						TaxId = vendorTable.TaxId,
						VendGroup_Values = SmartAppUtil.GetDropDownLabel(vendGroup.VendGroupId, vendGroup.Description),
						Currency_Values = SmartAppUtil.GetDropDownLabel(currency.CurrencyId, currency.Name),
				});
		}
		public SearchResult<VendorTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<VendorTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<VendorTable>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<VendorTableListViewMap, VendorTableListView>();
				result = list.SetSearchResult<VendorTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<VendorTableItemViewMap> GetItemQuery()
		{
			return (from vendorTable in Entity
					join company in db.Set<Company>()
					on vendorTable.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on vendorTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljVendorTableOwnerBU
					from ownerBU in ljVendorTableOwnerBU.DefaultIfEmpty()
					select new VendorTableItemViewMap
					{
						CompanyGUID = vendorTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = vendorTable.Owner,
						OwnerBusinessUnitGUID = vendorTable.OwnerBusinessUnitGUID,
						CreatedBy = vendorTable.CreatedBy,
						CreatedDateTime = vendorTable.CreatedDateTime,
						ModifiedBy = vendorTable.ModifiedBy,
						ModifiedDateTime = vendorTable.ModifiedDateTime,
						VendorTableGUID = vendorTable.VendorTableGUID,
						AltName = vendorTable.AltName,
						CurrencyGUID = vendorTable.CurrencyGUID,
						ExternalCode = vendorTable.ExternalCode,
						Name = vendorTable.Name,
						RecordType = vendorTable.RecordType,
						SentToOtherSystem = vendorTable.SentToOtherSystem,
						TaxId = vendorTable.TaxId,
						VendGroupGUID = vendorTable.VendGroupGUID,
						VendorId = vendorTable.VendorId,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,


					
						RowVersion = vendorTable.RowVersion,
					});
		}
		public VendorTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<VendorTableItemViewMap, VendorTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public VendorTable CreateVendorTable(VendorTable vendorTable)
		{
			try
			{
				vendorTable.VendorTableGUID = Guid.NewGuid();
				base.Add(vendorTable);
				return vendorTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateVendorTableVoid(VendorTable vendorTable)
		{
			try
			{
				CreateVendorTable(vendorTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public VendorTable UpdateVendorTable(VendorTable dbVendorTable, VendorTable inputVendorTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbVendorTable = dbVendorTable.MapUpdateValues<VendorTable>(inputVendorTable);
				base.Update(dbVendorTable);
				return dbVendorTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateVendorTableVoid(VendorTable dbVendorTable, VendorTable inputVendorTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbVendorTable = dbVendorTable.MapUpdateValues<VendorTable>(inputVendorTable, skipUpdateFields);
				base.Update(dbVendorTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public List<VendorTable> GetVendorTableByIdNoTracking(List<Guid> vendorTableGuids)
        {
            try
            {
				var result = Entity.Where(w => vendorTableGuids.Contains(w.VendorTableGUID))
									.AsNoTracking()
									.ToList();
				return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
	}
}

