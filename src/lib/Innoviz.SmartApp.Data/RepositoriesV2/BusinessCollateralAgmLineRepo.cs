using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IBusinessCollateralAgmLineRepo
	{
		#region DropDown
		IEnumerable<SelectItem<BusinessCollateralAgmLineItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<BusinessCollateralAgmLineListView> GetListvw(SearchParameter search);
		BusinessCollateralAgmLineItemView GetByIdvw(Guid id);
		BusinessCollateralAgmLine Find(params object[] keyValues);
		BusinessCollateralAgmLine GetBusinessCollateralAgmLineByIdNoTracking(Guid guid);
		BusinessCollateralAgmLine CreateBusinessCollateralAgmLine(BusinessCollateralAgmLine businessCollateralAgmLine);
		void CreateBusinessCollateralAgmLineVoid(BusinessCollateralAgmLine businessCollateralAgmLine);
		BusinessCollateralAgmLine UpdateBusinessCollateralAgmLine(BusinessCollateralAgmLine dbBusinessCollateralAgmLine, BusinessCollateralAgmLine inputBusinessCollateralAgmLine, List<string> skipUpdateFields = null);
		void UpdateBusinessCollateralAgmLineVoid(BusinessCollateralAgmLine dbBusinessCollateralAgmLine, BusinessCollateralAgmLine inputBusinessCollateralAgmLine, List<string> skipUpdateFields = null);
		void Remove(BusinessCollateralAgmLine item);
		BusinessCollateralAgmLine GetBusinessCollateralAgmLineByIdNoTrackingByAccessLevel(Guid guid);
		IEnumerable<BusinessCollateralAgmLine> GetBusinessCollateralAgmLineByBusinessCollatralAgmTableNoTracking(Guid guid);
		IEnumerable<BusinessCollateralAgmLine> GetBusinessCollateralAgmLineByCancelledNoTracking(bool cancelled);
		void ValidateAdd(IEnumerable<BusinessCollateralAgmLine> items);
		BusinessCollateralAgmLine GetOriginalBusinessCollateralAgmLine(Guid OriginalBusinessCollateralAgmLineGUID);
		IEnumerable<BusinessCollateralAgmLine> GetBusinessCollateralAgmLineByIdNoTracking(IEnumerable<Guid> guids);
	}
	public class BusinessCollateralAgmLineRepo : CompanyBaseRepository<BusinessCollateralAgmLine>, IBusinessCollateralAgmLineRepo
	{
		public BusinessCollateralAgmLineRepo(SmartAppDbContext context) : base(context) { }
		public BusinessCollateralAgmLineRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BusinessCollateralAgmLine GetBusinessCollateralAgmLineByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BusinessCollateralAgmLineGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<BusinessCollateralAgmLineItemViewMap> GetDropDownQuery()
		{
			return (from businessCollateralAgmLine in Entity
					select new BusinessCollateralAgmLineItemViewMap
					{
						CompanyGUID = businessCollateralAgmLine.CompanyGUID,
						Owner = businessCollateralAgmLine.Owner,
						OwnerBusinessUnitGUID = businessCollateralAgmLine.OwnerBusinessUnitGUID,
						BusinessCollateralAgmLineGUID = businessCollateralAgmLine.BusinessCollateralAgmLineGUID,
						OriginalBusinessCollateralAgreementTableGUID = businessCollateralAgmLine.OriginalBusinessCollateralAgreementTableGUID,
						LineNum = businessCollateralAgmLine.LineNum,
						Description = businessCollateralAgmLine.Description,
						BusinessCollateralAgmTableGUID = businessCollateralAgmLine.BusinessCollateralAgmTableGUID
					});
		}
		public IEnumerable<SelectItem<BusinessCollateralAgmLineItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<BusinessCollateralAgmLine>(search, SysParm.CompanyGUID);
				var businessCollateralAgmLine = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<BusinessCollateralAgmLineItemViewMap, BusinessCollateralAgmLineItemView>().ToDropDownItem(search.ExcludeRowData);


				return businessCollateralAgmLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<BusinessCollateralAgmLineListViewMap> GetListQuery()
		{
			return (from businessCollateralAgmLine in Entity
					join businessCollateralType in db.Set<BusinessCollateralType>()
					on businessCollateralAgmLine.BusinessCollateralTypeGUID equals businessCollateralType.BusinessCollateralTypeGUID
					join businessCollateralSubType in db.Set<BusinessCollateralSubType>()
					on businessCollateralAgmLine.BusinessCollateralSubTypeGUID equals businessCollateralSubType.BusinessCollateralSubTypeGUID
					join buyerTable in db.Set<BuyerTable>()
					on businessCollateralAgmLine.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljBuyerTable
					from buyerTable in ljBuyerTable.DefaultIfEmpty()
					select new BusinessCollateralAgmLineListViewMap
				{
						CompanyGUID = businessCollateralAgmLine.CompanyGUID,
						Owner = businessCollateralAgmLine.Owner,
						OwnerBusinessUnitGUID = businessCollateralAgmLine.OwnerBusinessUnitGUID,
						BusinessCollateralAgmLineGUID = businessCollateralAgmLine.BusinessCollateralAgmLineGUID,
						LineNum = businessCollateralAgmLine.LineNum,
						BusinessCollateralTypeGUID = businessCollateralAgmLine.BusinessCollateralTypeGUID,
						BusinessCollateralSubTypeGUID = businessCollateralAgmLine.BusinessCollateralSubTypeGUID,
						Description = businessCollateralAgmLine.Description,
						BusinessCollateralValue = businessCollateralAgmLine.BusinessCollateralValue,
						BuyerTableGUID = businessCollateralAgmLine.BuyerTableGUID,
						BusinessCollateralSubType_BusinessCollateralSubTypeId = businessCollateralSubType.BusinessCollateralSubTypeId,
						BusinessCollateralType_BusinessCollateralTypeId = businessCollateralType.BusinessCollateralTypeId,
						BuyerTable_BuyerId = (buyerTable != null) ? buyerTable.BuyerId : null,
						BusinessCollateralAgmTableGUID = businessCollateralAgmLine.BusinessCollateralAgmTableGUID,
						BusinessCollateralType_Values = SmartAppUtil.GetDropDownLabel(businessCollateralType.BusinessCollateralTypeId, businessCollateralType.Description),
						BusinessCollateralSubType_Values = SmartAppUtil.GetDropDownLabel(businessCollateralSubType.BusinessCollateralSubTypeId, businessCollateralSubType.Description),
						BuyerTable_Values = (buyerTable != null) ? SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName) : null,
						Cancelled = businessCollateralAgmLine.Cancelled
				});
		}
		public SearchResult<BusinessCollateralAgmLineListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<BusinessCollateralAgmLineListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<BusinessCollateralAgmLine>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<BusinessCollateralAgmLineListViewMap, BusinessCollateralAgmLineListView>();
				result = list.SetSearchResult<BusinessCollateralAgmLineListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<BusinessCollateralAgmLineItemViewMap> GetItemQuery()
		{
			return (from businessCollateralAgmLine in Entity
					join company in db.Set<Company>()
					on businessCollateralAgmLine.CompanyGUID equals company.CompanyGUID
					join businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
					on businessCollateralAgmLine.BusinessCollateralAgmTableGUID equals businessCollateralAgmTable.BusinessCollateralAgmTableGUID
					join documentStatus in db.Set<DocumentStatus>()
					on businessCollateralAgmTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
					join ownerBU in db.Set<BusinessUnit>()
					on businessCollateralAgmLine.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljBusinessCollateralAgmLineOwnerBU
					from ownerBU in ljBusinessCollateralAgmLineOwnerBU.DefaultIfEmpty()
					join businessCollateralSatatus in db.Set<BusinessCollateralStatus>()
					on businessCollateralAgmLine.BusinessCollateralStatusGUID equals businessCollateralSatatus.BusinessCollateralStatusGUID into ljbusinessCollateralSatatus
					from businessCollateralSatatus in ljbusinessCollateralSatatus.DefaultIfEmpty()
					select new BusinessCollateralAgmLineItemViewMap
					{
						CompanyGUID = businessCollateralAgmLine.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = businessCollateralAgmLine.Owner,
						OwnerBusinessUnitGUID = businessCollateralAgmLine.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = businessCollateralAgmLine.CreatedBy,
						CreatedDateTime = businessCollateralAgmLine.CreatedDateTime,
						ModifiedBy = businessCollateralAgmLine.ModifiedBy,
						ModifiedDateTime = businessCollateralAgmLine.ModifiedDateTime,
						BusinessCollateralAgmLineGUID = businessCollateralAgmLine.BusinessCollateralAgmLineGUID,
						AccountNumber = businessCollateralAgmLine.AccountNumber,
						Product = businessCollateralAgmLine.Product,
						BankGroupGUID = businessCollateralAgmLine.BankGroupGUID,
						BankTypeGUID = businessCollateralAgmLine.BankTypeGUID,
						BusinessCollateralAgmTableGUID = businessCollateralAgmLine.BusinessCollateralAgmTableGUID,
						BusinessCollateralSubTypeGUID = businessCollateralAgmLine.BusinessCollateralSubTypeGUID,
						BusinessCollateralTypeGUID = businessCollateralAgmLine.BusinessCollateralTypeGUID,
						BusinessCollateralValue = businessCollateralAgmLine.BusinessCollateralValue,
						BuyerName = businessCollateralAgmLine.BuyerName,
						BuyerTableGUID = businessCollateralAgmLine.BuyerTableGUID,
						BuyerTaxIdentificationId = businessCollateralAgmLine.BuyerTaxIdentificationId,
						CapitalValuation = businessCollateralAgmLine.CapitalValuation,
						ChassisNumber = businessCollateralAgmLine.ChassisNumber,
						CreditAppReqBusinessCollateralGUID = businessCollateralAgmLine.CreditAppReqBusinessCollateralGUID,
						DateOfValuation = businessCollateralAgmLine.DateOfValuation,
						Description = businessCollateralAgmLine.Description,
						GuaranteeAmount = businessCollateralAgmLine.GuaranteeAmount,
						Lessee = businessCollateralAgmLine.Lessee,
						Lessor = businessCollateralAgmLine.Lessor,
						LineNum = businessCollateralAgmLine.LineNum,
						MachineNumber = businessCollateralAgmLine.MachineNumber,
						MachineRegisteredStatus = businessCollateralAgmLine.MachineRegisteredStatus,
						Ownership = businessCollateralAgmLine.Ownership,
						PreferentialCreditorNumber = businessCollateralAgmLine.PreferentialCreditorNumber,
						ProjectName = businessCollateralAgmLine.ProjectName,
						Quantity = businessCollateralAgmLine.Quantity,
						RefAgreementDate = businessCollateralAgmLine.RefAgreementDate,
						RefAgreementId = businessCollateralAgmLine.RefAgreementId,
						RegisteredPlace = businessCollateralAgmLine.RegisteredPlace,
						RegistrationPlateNumber = businessCollateralAgmLine.RegistrationPlateNumber,
						TitleDeedDistrict = businessCollateralAgmLine.TitleDeedDistrict,
						TitleDeedNumber = businessCollateralAgmLine.TitleDeedNumber,
						TitleDeedProvince = businessCollateralAgmLine.TitleDeedProvince,
						TitleDeedSubDistrict = businessCollateralAgmLine.TitleDeedSubDistrict,
						Unit = businessCollateralAgmLine.Unit,
						ValuationCommittee = businessCollateralAgmLine.ValuationCommittee,
						BusinessCollateralAgmTable_BusinessCollateralAgmId = businessCollateralAgmTable.BusinessCollateralAgmId,
						BusinessCollateralAgmTable_InternalBusinessCollateralAgmId = businessCollateralAgmTable.InternalBusinessCollateralAgmId,
						BusinessCollateralAgmTable_CreditAppRequestTableGUID = businessCollateralAgmTable.CreditAppRequestTableGUID,
						DocumentStatus_StatusId = documentStatus.StatusId,
						BusinessCollateralAgmTable_AgreementDocType = businessCollateralAgmTable.AgreementDocType,
						Cancelled = businessCollateralAgmLine.Cancelled,
						OriginalBusinessCollateralAgreementLineGUID = businessCollateralAgmLine.OriginalBusinessCollateralAgreementLineGUID,
						OriginalBusinessCollateralAgreementTableGUID = businessCollateralAgmLine.OriginalBusinessCollateralAgreementTableGUID,
						BusinessCollateralStatusGUID = businessCollateralAgmLine.BusinessCollateralStatusGUID,
						BusinessCollateralStatus_Values = SmartAppUtil.GetDropDownLabel(businessCollateralSatatus.BusinessCollateralStatusId,businessCollateralSatatus.Description),
						AttachmentText = businessCollateralAgmLine.AttachmentText,
						BusinessCollateralAgmTable_Values = SmartAppUtil.GetDropDownLabel(businessCollateralAgmTable.InternalBusinessCollateralAgmId, businessCollateralAgmTable.Description),
					
						RowVersion = businessCollateralAgmLine.RowVersion,
					});
		}
		public BusinessCollateralAgmLineItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<BusinessCollateralAgmLineItemViewMap, BusinessCollateralAgmLineItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public BusinessCollateralAgmLine GetOriginalBusinessCollateralAgmLine(Guid OriginalBusinessCollateralAgmLineGUID)
        {
            try
            {
				BusinessCollateralAgmLine tmp_businessCollateralAgmLine = (from businessCollateralAgmLine in Entity
																		   where businessCollateralAgmLine.BusinessCollateralAgmLineGUID == OriginalBusinessCollateralAgmLineGUID
																		   select new BusinessCollateralAgmLine() { 
																			   CreditAppReqBusinessCollateralGUID = businessCollateralAgmLine.CreditAppReqBusinessCollateralGUID,
																			   BusinessCollateralTypeGUID = businessCollateralAgmLine.BusinessCollateralTypeGUID,
																			   BusinessCollateralSubTypeGUID = businessCollateralAgmLine.BusinessCollateralSubTypeGUID,
																			   Product = businessCollateralAgmLine.Product,
																			   BusinessCollateralValue = businessCollateralAgmLine.BusinessCollateralValue,
																			   Description = businessCollateralAgmLine.Description,
																			   BuyerTableGUID = businessCollateralAgmLine.BuyerTableGUID,
																			   BuyerName = businessCollateralAgmLine.BuyerName,
																			   BuyerTaxIdentificationId = businessCollateralAgmLine.BuyerTaxIdentificationId,
																			   BankGroupGUID = businessCollateralAgmLine.BankGroupGUID,
																			   BankTypeGUID = businessCollateralAgmLine.BankTypeGUID,
																			   AccountNumber = businessCollateralAgmLine.AccountNumber,
																			   RefAgreementId = businessCollateralAgmLine.RefAgreementId,
																			   RefAgreementDate = businessCollateralAgmLine.RefAgreementDate,
																			   PreferentialCreditorNumber = businessCollateralAgmLine.PreferentialCreditorNumber,
																			   Lessee = businessCollateralAgmLine.Lessee,
																			   Lessor = businessCollateralAgmLine.Lessor,
																			   RegistrationPlateNumber = businessCollateralAgmLine.RegistrationPlateNumber,
																			   ChassisNumber = businessCollateralAgmLine.ChassisNumber,
																			   MachineNumber = businessCollateralAgmLine.MachineNumber,
																			   MachineRegisteredStatus = businessCollateralAgmLine.MachineRegisteredStatus,
																			   RegisteredPlace = businessCollateralAgmLine.RegisteredPlace,
																			   Quantity = businessCollateralAgmLine.Quantity,
																			   Unit = businessCollateralAgmLine.Unit,
																			   GuaranteeAmount = businessCollateralAgmLine.GuaranteeAmount,
																			   TitleDeedNumber = businessCollateralAgmLine.TitleDeedNumber,
																			   TitleDeedSubDistrict = businessCollateralAgmLine.TitleDeedSubDistrict,
																			   TitleDeedDistrict = businessCollateralAgmLine.TitleDeedDistrict,
																			   TitleDeedProvince = businessCollateralAgmLine.TitleDeedProvince,
																			   DateOfValuation = businessCollateralAgmLine.DateOfValuation,
																			   CapitalValuation = businessCollateralAgmLine.CapitalValuation,
																			   ValuationCommittee = businessCollateralAgmLine.ValuationCommittee,
																			   Ownership = businessCollateralAgmLine.Ownership,
																			   CompanyGUID = businessCollateralAgmLine.CompanyGUID
																		   }).FirstOrDefault();
				return tmp_businessCollateralAgmLine;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public BusinessCollateralAgmLine CreateBusinessCollateralAgmLine(BusinessCollateralAgmLine businessCollateralAgmLine)
		{
			try
			{
				businessCollateralAgmLine.BusinessCollateralAgmLineGUID = Guid.NewGuid();
				base.Add(businessCollateralAgmLine);
				return businessCollateralAgmLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateBusinessCollateralAgmLineVoid(BusinessCollateralAgmLine businessCollateralAgmLine)
		{
			try
			{
				CreateBusinessCollateralAgmLine(businessCollateralAgmLine);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BusinessCollateralAgmLine UpdateBusinessCollateralAgmLine(BusinessCollateralAgmLine dbBusinessCollateralAgmLine, BusinessCollateralAgmLine inputBusinessCollateralAgmLine, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBusinessCollateralAgmLine = dbBusinessCollateralAgmLine.MapUpdateValues<BusinessCollateralAgmLine>(inputBusinessCollateralAgmLine);
				base.Update(dbBusinessCollateralAgmLine);
				return dbBusinessCollateralAgmLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateBusinessCollateralAgmLineVoid(BusinessCollateralAgmLine dbBusinessCollateralAgmLine, BusinessCollateralAgmLine inputBusinessCollateralAgmLine, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBusinessCollateralAgmLine = dbBusinessCollateralAgmLine.MapUpdateValues<BusinessCollateralAgmLine>(inputBusinessCollateralAgmLine, skipUpdateFields);
				base.Update(dbBusinessCollateralAgmLine);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public BusinessCollateralAgmLine GetBusinessCollateralAgmLineByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BusinessCollateralAgmLineGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		
			public IEnumerable<BusinessCollateralAgmLine> GetBusinessCollateralAgmLineByCancelledNoTracking(bool cancelled)
		{
			try
			{
				var result = Entity.Where(item => item.Cancelled == cancelled).AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<BusinessCollateralAgmLine> GetBusinessCollateralAgmLineByBusinessCollatralAgmTableNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BusinessCollateralAgmTableGUID == guid).AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<BusinessCollateralAgmLine> GetBusinessCollateralAgmLineByIdNoTracking(IEnumerable<Guid> guids)
		{
			try
			{
				var result = Entity.Where(item => guids.Contains(item.BusinessCollateralAgmLineGUID)).AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Validate
		public override void ValidateAdd(IEnumerable<BusinessCollateralAgmLine> items)
        {
            base.ValidateAdd(items);
        }
        #endregion Validate
    }
}

