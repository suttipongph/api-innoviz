using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IRaceRepo
	{
		#region DropDown
		IEnumerable<SelectItem<RaceItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<RaceListView> GetListvw(SearchParameter search);
		RaceItemView GetByIdvw(Guid id);
		Race Find(params object[] keyValues);
		Race GetRaceByIdNoTracking(Guid guid);
		Race CreateRace(Race race);
		void CreateRaceVoid(Race race);
		Race UpdateRace(Race dbRace, Race inputRace, List<string> skipUpdateFields = null);
		void UpdateRaceVoid(Race dbRace, Race inputRace, List<string> skipUpdateFields = null);
		void Remove(Race item);
	}
	public class RaceRepo : CompanyBaseRepository<Race>, IRaceRepo
	{
		public RaceRepo(SmartAppDbContext context) : base(context) { }
		public RaceRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public Race GetRaceByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.RaceGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<RaceItemViewMap> GetDropDownQuery()
		{
			return (from race in Entity
					select new RaceItemViewMap
					{
						CompanyGUID = race.CompanyGUID,
						Owner = race.Owner,
						OwnerBusinessUnitGUID = race.OwnerBusinessUnitGUID,
						RaceGUID = race.RaceGUID,
						RaceId = race.RaceId,
						Description = race.Description
					});
		}
		public IEnumerable<SelectItem<RaceItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<Race>(search, SysParm.CompanyGUID);
				var race = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<RaceItemViewMap, RaceItemView>().ToDropDownItem(search.ExcludeRowData);


				return race;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<RaceListViewMap> GetListQuery()
		{
			return (from race in Entity
				select new RaceListViewMap
				{
						CompanyGUID = race.CompanyGUID,
						Owner = race.Owner,
						OwnerBusinessUnitGUID = race.OwnerBusinessUnitGUID,
						RaceGUID = race.RaceGUID,
						RaceId = race.RaceId,
						Description = race.Description,
				});
		}
		public SearchResult<RaceListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<RaceListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<Race>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<RaceListViewMap, RaceListView>();
				result = list.SetSearchResult<RaceListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<RaceItemViewMap> GetItemQuery()
		{
			return (from race in Entity
					join ownerBU in db.Set<BusinessUnit>()
					on race.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljRaceOwnerBU
					from ownerBU in ljRaceOwnerBU.DefaultIfEmpty()
					join company in db.Set<Company>() on race.CompanyGUID equals company.CompanyGUID
					select new RaceItemViewMap
					{
						CompanyGUID = race.CompanyGUID,
						Owner = race.Owner,
						OwnerBusinessUnitGUID = race.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CompanyId = company.CompanyId,
						CreatedBy = race.CreatedBy,
						CreatedDateTime = race.CreatedDateTime,
						ModifiedBy = race.ModifiedBy,
						ModifiedDateTime = race.ModifiedDateTime,
						RaceGUID = race.RaceGUID,
						Description = race.Description,
						RaceId = race.RaceId,
					
						RowVersion = race.RowVersion,
					});
		}
		public RaceItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<RaceItemViewMap, RaceItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public Race CreateRace(Race race)
		{
			try
			{
				race.RaceGUID = Guid.NewGuid();
				base.Add(race);
				return race;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateRaceVoid(Race race)
		{
			try
			{
				CreateRace(race);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public Race UpdateRace(Race dbRace, Race inputRace, List<string> skipUpdateFields = null)
		{
			try
			{
				dbRace = dbRace.MapUpdateValues<Race>(inputRace);
				base.Update(dbRace);
				return dbRace;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateRaceVoid(Race dbRace, Race inputRace, List<string> skipUpdateFields = null)
		{
			try
			{
				dbRace = dbRace.MapUpdateValues<Race>(inputRace, skipUpdateFields);
				base.Update(dbRace);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

