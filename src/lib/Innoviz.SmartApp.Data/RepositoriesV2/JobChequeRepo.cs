using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IJobChequeRepo
	{
		#region DropDown
		IEnumerable<SelectItem<JobChequeItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<JobChequeListView> GetListvw(SearchParameter search);
		JobChequeItemView GetByIdvw(Guid id);
		JobCheque Find(params object[] keyValues);
		JobCheque GetJobChequeByIdNoTracking(Guid guid);
		JobCheque CreateJobCheque(JobCheque jobCheque);
		void CreateJobChequeVoid(JobCheque jobCheque);
		JobCheque UpdateJobCheque(JobCheque dbJobCheque, JobCheque inputJobCheque, List<string> skipUpdateFields = null);
		void UpdateJobChequeVoid(JobCheque dbJobCheque, JobCheque inputJobCheque, List<string> skipUpdateFields = null);
		void Remove(JobCheque item);
		JobCheque GetJobChequeByIdNoTrackingByAccessLevel(Guid guid);
	}
	public class JobChequeRepo : CompanyBaseRepository<JobCheque>, IJobChequeRepo
	{
		public JobChequeRepo(SmartAppDbContext context) : base(context) { }
		public JobChequeRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public JobCheque GetJobChequeByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.JobChequeGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<JobChequeItemViewMap> GetDropDownQuery()
		{
			return (from jobCheque in Entity
					select new JobChequeItemViewMap
					{
						CompanyGUID = jobCheque.CompanyGUID,
						Owner = jobCheque.Owner,
						OwnerBusinessUnitGUID = jobCheque.OwnerBusinessUnitGUID,
						JobChequeGUID = jobCheque.JobChequeGUID,
						ChequeNo = jobCheque.ChequeNo
					});
		}
		public IEnumerable<SelectItem<JobChequeItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<JobCheque>(search, SysParm.CompanyGUID);
				var jobCheque = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<JobChequeItemViewMap, JobChequeItemView>().ToDropDownItem(search.ExcludeRowData);


				return jobCheque;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<JobChequeListViewMap> GetListQuery()
		{
			return (from jobCheque in Entity
					join chequeTable in db.Set<ChequeTable>()
					on jobCheque.ChequeTableGUID equals chequeTable.ChequeTableGUID into ljchequeTable
					from chequeTable in ljchequeTable.DefaultIfEmpty()

					join collectionFollowUp in db.Set<CollectionFollowUp>()
					on jobCheque.CollectionFollowUpGUID equals collectionFollowUp.CollectionFollowUpGUID into ljcollectionFollowUp
					from collectionFollowUp in ljcollectionFollowUp.DefaultIfEmpty()

					join bankGroupTable in db.Set<BankGroup>()
					on jobCheque.ChequeBankGroupGUID equals bankGroupTable.BankGroupGUID into ljbankGroupTable
					from bankGroupTable in ljbankGroupTable.DefaultIfEmpty()
					select new JobChequeListViewMap
				{
						CompanyGUID = jobCheque.CompanyGUID,
						Owner = jobCheque.Owner,
						OwnerBusinessUnitGUID = jobCheque.OwnerBusinessUnitGUID,
						JobChequeGUID = jobCheque.JobChequeGUID,
						ChequeSource = jobCheque.ChequeSource,
						CollectionFollowUpGUID = jobCheque.CollectionFollowUpGUID,
						ChequeTableGUID = jobCheque.ChequeTableGUID,
						ChequeBankGroupGUID = jobCheque.ChequeBankGroupGUID,
						ChequeBranch = jobCheque.ChequeBranch,
						ChequeDate = jobCheque.ChequeDate,
						ChequeNo = jobCheque.ChequeNo,
						Amount = jobCheque.Amount,
						CollectionFollowUp_Values = collectionFollowUp != null ? SmartAppUtil.GetDropDownLabel(collectionFollowUp.CollectionDate.DateToString(), collectionFollowUp.Description) : null,
						ChequeTable_Values = SmartAppUtil.GetDropDownLabel(chequeTable.ChequeNo, chequeTable.ChequeDate.DateToString()),
						BankGroup_Values = SmartAppUtil.GetDropDownLabel(bankGroupTable.BankGroupId, bankGroupTable.Description),
						MessengerJobTableGUID = jobCheque.MessengerJobTableGUID
				});
		}
		public SearchResult<JobChequeListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<JobChequeListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<JobCheque>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<JobChequeListViewMap, JobChequeListView>();
				result = list.SetSearchResult<JobChequeListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<JobChequeItemViewMap> GetItemQuery()
		{
			return (from jobCheque in Entity
					join company in db.Set<Company>()
					on jobCheque.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on jobCheque.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljJobChequeOwnerBU
					from ownerBU in ljJobChequeOwnerBU.DefaultIfEmpty()
					join messengerJobTable in db.Set<MessengerJobTable>()
					on jobCheque.MessengerJobTableGUID equals messengerJobTable.MessengerJobTableGUID into ljmessengerJobTable
					from messengerJobTable in ljmessengerJobTable.DefaultIfEmpty()
					select new JobChequeItemViewMap
					{
						CompanyGUID = jobCheque.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = jobCheque.Owner,
						OwnerBusinessUnitGUID = jobCheque.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = jobCheque.CreatedBy,
						CreatedDateTime = jobCheque.CreatedDateTime,
						ModifiedBy = jobCheque.ModifiedBy,
						ModifiedDateTime = jobCheque.ModifiedDateTime,
						JobChequeGUID = jobCheque.JobChequeGUID,
						Amount = jobCheque.Amount,
						ChequeBankAccNo = jobCheque.ChequeBankAccNo,
						ChequeBankGroupGUID = jobCheque.ChequeBankGroupGUID,
						ChequeBranch = jobCheque.ChequeBranch,
						ChequeDate = jobCheque.ChequeDate,
						ChequeNo = jobCheque.ChequeNo,
						ChequeSource = jobCheque.ChequeSource,
						ChequeTableGUID = jobCheque.ChequeTableGUID,
						CollectionFollowUpGUID = jobCheque.CollectionFollowUpGUID,
						MessengerJobTableGUID = jobCheque.MessengerJobTableGUID,
						RecipientName = jobCheque.RecipientName,
						MessengerJobTable_CustomerTableGUID = messengerJobTable.CustomerTableGUID,
						MessengerJobTable_BuyerTableGUID = messengerJobTable.BuyerTableGUID,
						MessengerJobTable_RefGUID = messengerJobTable.RefGUID,
					
						RowVersion = jobCheque.RowVersion,
					});
		}
		public JobChequeItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<JobChequeItemViewMap, JobChequeItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public JobCheque CreateJobCheque(JobCheque jobCheque)
		{
			try
			{
				jobCheque.JobChequeGUID = Guid.NewGuid();
				base.Add(jobCheque);
				return jobCheque;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateJobChequeVoid(JobCheque jobCheque)
		{
			try
			{
				CreateJobCheque(jobCheque);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public JobCheque UpdateJobCheque(JobCheque dbJobCheque, JobCheque inputJobCheque, List<string> skipUpdateFields = null)
		{
			try
			{
				dbJobCheque = dbJobCheque.MapUpdateValues<JobCheque>(inputJobCheque);
				base.Update(dbJobCheque);
				return dbJobCheque;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateJobChequeVoid(JobCheque dbJobCheque, JobCheque inputJobCheque, List<string> skipUpdateFields = null)
		{
			try
			{
				dbJobCheque = dbJobCheque.MapUpdateValues<JobCheque>(inputJobCheque, skipUpdateFields);
				base.Update(dbJobCheque);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public JobCheque GetJobChequeByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.JobChequeGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

