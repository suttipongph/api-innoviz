using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IWithholdingTaxGroupRepo
	{
		#region DropDown
		IEnumerable<SelectItem<WithholdingTaxGroupItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<WithholdingTaxGroupListView> GetListvw(SearchParameter search);
		WithholdingTaxGroupItemView GetByIdvw(Guid id);
		WithholdingTaxGroup Find(params object[] keyValues);
		WithholdingTaxGroup GetWithholdingTaxGroupByIdNoTracking(Guid guid);
		WithholdingTaxGroup CreateWithholdingTaxGroup(WithholdingTaxGroup withholdingTaxGroup);
		void CreateWithholdingTaxGroupVoid(WithholdingTaxGroup withholdingTaxGroup);
		WithholdingTaxGroup UpdateWithholdingTaxGroup(WithholdingTaxGroup dbWithholdingTaxGroup, WithholdingTaxGroup inputWithholdingTaxGroup, List<string> skipUpdateFields = null);
		void UpdateWithholdingTaxGroupVoid(WithholdingTaxGroup dbWithholdingTaxGroup, WithholdingTaxGroup inputWithholdingTaxGroup, List<string> skipUpdateFields = null);
		void Remove(WithholdingTaxGroup item);
	}
	public class WithholdingTaxGroupRepo : CompanyBaseRepository<WithholdingTaxGroup>, IWithholdingTaxGroupRepo
	{
		public WithholdingTaxGroupRepo(SmartAppDbContext context) : base(context) { }
		public WithholdingTaxGroupRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public WithholdingTaxGroup GetWithholdingTaxGroupByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.WithholdingTaxGroupGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<WithholdingTaxGroupItemViewMap> GetDropDownQuery()
		{
			return (from withholdingTaxGroup in Entity
					select new WithholdingTaxGroupItemViewMap
					{
						CompanyGUID = withholdingTaxGroup.CompanyGUID,
						Owner = withholdingTaxGroup.Owner,
						OwnerBusinessUnitGUID = withholdingTaxGroup.OwnerBusinessUnitGUID,
						WithholdingTaxGroupGUID = withholdingTaxGroup.WithholdingTaxGroupGUID,
						WHTGroupId = withholdingTaxGroup.WHTGroupId,
						Description = withholdingTaxGroup.Description
					});
		}
		public IEnumerable<SelectItem<WithholdingTaxGroupItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<WithholdingTaxGroup>(search, SysParm.CompanyGUID);
				var withholdingTaxGroup = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<WithholdingTaxGroupItemViewMap, WithholdingTaxGroupItemView>().ToDropDownItem(search.ExcludeRowData);


				return withholdingTaxGroup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<WithholdingTaxGroupListViewMap> GetListQuery()
		{
			return (from withholdingTaxGroup in Entity
				select new WithholdingTaxGroupListViewMap
				{
						CompanyGUID = withholdingTaxGroup.CompanyGUID,
						Owner = withholdingTaxGroup.Owner,
						OwnerBusinessUnitGUID = withholdingTaxGroup.OwnerBusinessUnitGUID,
						WithholdingTaxGroupGUID = withholdingTaxGroup.WithholdingTaxGroupGUID,
						WHTGroupId = withholdingTaxGroup.WHTGroupId,
						Description = withholdingTaxGroup.Description
				});
		}
		public SearchResult<WithholdingTaxGroupListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<WithholdingTaxGroupListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<WithholdingTaxGroup>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<WithholdingTaxGroupListViewMap, WithholdingTaxGroupListView>();
				result = list.SetSearchResult<WithholdingTaxGroupListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<WithholdingTaxGroupItemViewMap> GetItemQuery()
		{
			return (from withholdingTaxGroup in Entity
					join company in db.Set<Company>()
					on withholdingTaxGroup.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on withholdingTaxGroup.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljWithholdingTaxGroupOwnerBU
					from ownerBU in ljWithholdingTaxGroupOwnerBU.DefaultIfEmpty()
					select new WithholdingTaxGroupItemViewMap
					{
						CompanyGUID = withholdingTaxGroup.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = withholdingTaxGroup.Owner,
						OwnerBusinessUnitGUID = withholdingTaxGroup.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = withholdingTaxGroup.CreatedBy,
						CreatedDateTime = withholdingTaxGroup.CreatedDateTime,
						ModifiedBy = withholdingTaxGroup.ModifiedBy,
						ModifiedDateTime = withholdingTaxGroup.ModifiedDateTime,
						WithholdingTaxGroupGUID = withholdingTaxGroup.WithholdingTaxGroupGUID,
						Description = withholdingTaxGroup.Description,
						WHTGroupId = withholdingTaxGroup.WHTGroupId,
					
						RowVersion = withholdingTaxGroup.RowVersion,
					});
		}
		public WithholdingTaxGroupItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<WithholdingTaxGroupItemViewMap, WithholdingTaxGroupItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public WithholdingTaxGroup CreateWithholdingTaxGroup(WithholdingTaxGroup withholdingTaxGroup)
		{
			try
			{
				withholdingTaxGroup.WithholdingTaxGroupGUID = Guid.NewGuid();
				base.Add(withholdingTaxGroup);
				return withholdingTaxGroup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateWithholdingTaxGroupVoid(WithholdingTaxGroup withholdingTaxGroup)
		{
			try
			{
				CreateWithholdingTaxGroup(withholdingTaxGroup);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public WithholdingTaxGroup UpdateWithholdingTaxGroup(WithholdingTaxGroup dbWithholdingTaxGroup, WithholdingTaxGroup inputWithholdingTaxGroup, List<string> skipUpdateFields = null)
		{
			try
			{
				dbWithholdingTaxGroup = dbWithholdingTaxGroup.MapUpdateValues<WithholdingTaxGroup>(inputWithholdingTaxGroup);
				base.Update(dbWithholdingTaxGroup);
				return dbWithholdingTaxGroup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateWithholdingTaxGroupVoid(WithholdingTaxGroup dbWithholdingTaxGroup, WithholdingTaxGroup inputWithholdingTaxGroup, List<string> skipUpdateFields = null)
		{
			try
			{
				dbWithholdingTaxGroup = dbWithholdingTaxGroup.MapUpdateValues<WithholdingTaxGroup>(inputWithholdingTaxGroup, skipUpdateFields);
				base.Update(dbWithholdingTaxGroup);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

