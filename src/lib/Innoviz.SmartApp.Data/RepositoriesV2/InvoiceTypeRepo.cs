using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IInvoiceTypeRepo
	{
		#region DropDown
		IEnumerable<SelectItem<InvoiceTypeItemView>>  GetDropDownItemByAutoGenInvoiceRevenueTypeNull(SearchParameter search);
		IEnumerable<SelectItem<InvoiceTypeItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<InvoiceTypeListView> GetListvw(SearchParameter search);
		InvoiceTypeItemView GetByIdvw(Guid id);
		InvoiceType Find(params object[] keyValues);
		InvoiceType GetInvoiceTypeByIdNoTracking(Guid guid);
		InvoiceType CreateInvoiceType(InvoiceType invoiceType);
		void CreateInvoiceTypeVoid(InvoiceType invoiceType);
		InvoiceType UpdateInvoiceType(InvoiceType dbInvoiceType, InvoiceType inputInvoiceType, List<string> skipUpdateFields = null);
		void UpdateInvoiceTypeVoid(InvoiceType dbInvoiceType, InvoiceType inputInvoiceType, List<string> skipUpdateFields = null);
		void Remove(InvoiceType item);
		InvoiceType GetInvoiceTypeByIdNoTrackingByAccessLevel(Guid guid);
		void ValidateUpdate(InvoiceType item);
		List<InvoiceType> GetInvoiceTypeByIdNoTracking(IEnumerable<Guid> invoiceTypeGuids);
		InvoiceType GetInvoiceTypeByInvoiceRevenueTypeNoTracking(Guid guid);
		List<InvoiceType> GetInvoiceTypeByCompanyNoTracking(Guid companyGUID);
	}
	public class InvoiceTypeRepo : CompanyBaseRepository<InvoiceType>, IInvoiceTypeRepo
	{
		public InvoiceTypeRepo(SmartAppDbContext context) : base(context) { }
		public InvoiceTypeRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public InvoiceType GetInvoiceTypeByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.InvoiceTypeGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<InvoiceType> GetInvoiceTypeByIdNoTracking(IEnumerable<Guid> invoiceTypeGuids)
        {
            try
            {
				var result = Entity.Where(w => invoiceTypeGuids.Contains(w.InvoiceTypeGUID))
								.AsNoTracking()
								.ToList();
				return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public InvoiceType GetInvoiceTypeByInvoiceRevenueTypeNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.AutoGenInvoiceRevenueTypeGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		
		
		private IQueryable<InvoiceTypeItemViewMap> GetDropDownQuery()
		{
			return (from invoiceType in Entity
					join invoiceRevenueType in db.Set<InvoiceRevenueType>()
					on invoiceType.AutoGenInvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID into ljinvoiceRevenueType
					from invoiceRevenueType in ljinvoiceRevenueType.DefaultIfEmpty()
					select new InvoiceTypeItemViewMap
					{
						CompanyGUID = invoiceType.CompanyGUID,
						Owner = invoiceType.Owner,
						OwnerBusinessUnitGUID = invoiceType.OwnerBusinessUnitGUID,
						InvoiceTypeGUID = invoiceType.InvoiceTypeGUID,
						InvoiceTypeId = invoiceType.InvoiceTypeId,
						Description = invoiceType.Description,
						AutoGen = invoiceType.AutoGen,
						SuspenseInvoiceType = invoiceType.SuspenseInvoiceType,
						AutoGenInvoiceRevenueTypeGUID = invoiceType.AutoGenInvoiceRevenueTypeGUID,
						ProductType = invoiceType.ProductType,
						AutoGenInvoiceRevenueType_values = SmartAppUtil.GetDropDownLabel(invoiceRevenueType.RevenueTypeId, invoiceRevenueType.Description),
						DirectReceipt = invoiceType.DirectReceipt,
						ProductInvoice = invoiceType.ProductInvoice,
						ValidateDirectReceiveByCA = invoiceType.ValidateDirectReceiveByCA
					});
		}
		private IQueryable<InvoiceTypeItemViewMap> GetDropDownQueryByAutoGenInvoiceRevenueTypeNull()
		{
			return (from invoiceType in Entity
					join invoiceRevenueType in db.Set<InvoiceRevenueType>()
					on invoiceType.AutoGenInvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID into ljinvoiceRevenueType
					from invoiceRevenueType in ljinvoiceRevenueType.DefaultIfEmpty()
					where invoiceType.AutoGenInvoiceRevenueTypeGUID == null
					select new InvoiceTypeItemViewMap
					{
						CompanyGUID = invoiceType.CompanyGUID,
						Owner = invoiceType.Owner,
						OwnerBusinessUnitGUID = invoiceType.OwnerBusinessUnitGUID,
						InvoiceTypeGUID = invoiceType.InvoiceTypeGUID,
						InvoiceTypeId = invoiceType.InvoiceTypeId,
						Description = invoiceType.Description,
						AutoGen = invoiceType.AutoGen,
						SuspenseInvoiceType = invoiceType.SuspenseInvoiceType,
						AutoGenInvoiceRevenueTypeGUID = invoiceType.AutoGenInvoiceRevenueTypeGUID,
						ProductType = invoiceType.ProductType,
						AutoGenInvoiceRevenueType_values = SmartAppUtil.GetDropDownLabel(invoiceRevenueType.RevenueTypeId, invoiceRevenueType.Description),
						DirectReceipt = invoiceType.DirectReceipt,
						ProductInvoice = invoiceType.ProductInvoice,
						ValidateDirectReceiveByCA = invoiceType.ValidateDirectReceiveByCA
					});
		}
		public IEnumerable<SelectItem<InvoiceTypeItemView>> GetDropDownItemByAutoGenInvoiceRevenueTypeNull(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<InvoiceType>(search, SysParm.CompanyGUID);
				var invoiceType = GetDropDownQueryByAutoGenInvoiceRevenueTypeNull().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<InvoiceTypeItemViewMap, InvoiceTypeItemView>().ToDropDownItem(search.ExcludeRowData);


				return invoiceType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<InvoiceTypeItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<InvoiceType>(search, SysParm.CompanyGUID);
				var invoiceType = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<InvoiceTypeItemViewMap, InvoiceTypeItemView>().ToDropDownItem(search.ExcludeRowData);


				return invoiceType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<InvoiceTypeListViewMap> GetListQuery()
		{
			return (from invoiceType in Entity
					join invoiceRevenueType in db.Set<InvoiceRevenueType>()
					on invoiceType.AutoGenInvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID into ljinvoiceRevenueType
					from invoiceRevenueType in ljinvoiceRevenueType.DefaultIfEmpty()
					select new InvoiceTypeListViewMap
					{
							CompanyGUID = invoiceType.CompanyGUID,
							Owner = invoiceType.Owner,
							OwnerBusinessUnitGUID = invoiceType.OwnerBusinessUnitGUID,
							InvoiceTypeGUID = invoiceType.InvoiceTypeGUID,
							InvoiceTypeId = invoiceType.InvoiceTypeId,
							Description = invoiceType.Description,
							ProductType = invoiceType.ProductType,
							DirectReceipt = invoiceType.DirectReceipt,
							ValidateDirectReceiveByCA = invoiceType.ValidateDirectReceiveByCA,
							SuspenseInvoiceType = invoiceType.SuspenseInvoiceType,
							ProductInvoice = invoiceType.ProductInvoice,
							AutoGenInvoiceRevenueTypeGUID = invoiceType.AutoGenInvoiceRevenueTypeGUID,
							AutoGenInvoiceRevenueType_values = SmartAppUtil.GetDropDownLabel(invoiceRevenueType.RevenueTypeId, invoiceRevenueType.Description)
					});
		}
		public SearchResult<InvoiceTypeListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<InvoiceTypeListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<InvoiceType>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<InvoiceTypeListViewMap, InvoiceTypeListView>();
				result = list.SetSearchResult<InvoiceTypeListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<InvoiceTypeItemViewMap> GetItemQuery()
		{
			return (from invoiceType in Entity
					join company in db.Set<Company>()
					on invoiceType.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on invoiceType.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljInvoiceTypeOwnerBU
					from ownerBU in ljInvoiceTypeOwnerBU.DefaultIfEmpty()
					join invoiceRevenueType in db.Set<InvoiceRevenueType>()
					on invoiceType.AutoGenInvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID into ljinvoiceRevenueType
					from invoiceRevenueType in ljinvoiceRevenueType.DefaultIfEmpty()
					select new InvoiceTypeItemViewMap
					{
						CompanyGUID = invoiceType.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = invoiceType.Owner,
						OwnerBusinessUnitGUID = invoiceType.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = invoiceType.CreatedBy,
						CreatedDateTime = invoiceType.CreatedDateTime,
						ModifiedBy = invoiceType.ModifiedBy,
						ModifiedDateTime = invoiceType.ModifiedDateTime,
						InvoiceTypeGUID = invoiceType.InvoiceTypeGUID,
						ARLedgerAccount = invoiceType.ARLedgerAccount,
						Description = invoiceType.Description,
						InvoiceTypeId = invoiceType.InvoiceTypeId,
						SuspenseInvoiceType = invoiceType.SuspenseInvoiceType,
						ProductInvoice = invoiceType.ProductInvoice,
						ProductType = invoiceType.ProductType,
						ValidateDirectReceiveByCA = invoiceType.ValidateDirectReceiveByCA,
						DirectReceipt = invoiceType.DirectReceipt,
						AutoGenInvoiceRevenueTypeGUID = invoiceType.AutoGenInvoiceRevenueTypeGUID,
						AutoGenInvoiceRevenueType_values = SmartAppUtil.GetDropDownLabel(invoiceRevenueType.RevenueTypeId, invoiceRevenueType.Description),
					
						RowVersion = invoiceType.RowVersion,
					});
		}
		public InvoiceTypeItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<InvoiceTypeItemViewMap, InvoiceTypeItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public InvoiceType CreateInvoiceType(InvoiceType invoiceType)
		{
			try
			{
				invoiceType.InvoiceTypeGUID = Guid.NewGuid();
				base.Add(invoiceType);
				return invoiceType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateInvoiceTypeVoid(InvoiceType invoiceType)
		{
			try
			{
				CreateInvoiceType(invoiceType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public InvoiceType UpdateInvoiceType(InvoiceType dbInvoiceType, InvoiceType inputInvoiceType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbInvoiceType = dbInvoiceType.MapUpdateValues<InvoiceType>(inputInvoiceType);
				base.Update(dbInvoiceType);
				return dbInvoiceType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateInvoiceTypeVoid(InvoiceType dbInvoiceType, InvoiceType inputInvoiceType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbInvoiceType = dbInvoiceType.MapUpdateValues<InvoiceType>(inputInvoiceType, skipUpdateFields);
				base.Update(dbInvoiceType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public InvoiceType GetInvoiceTypeByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.InvoiceTypeGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public override void ValidateUpdate(IEnumerable<InvoiceType> items)
		{
			ValidateUpdate(items);
		}
		public override void ValidateUpdate(InvoiceType item)
		{
			CheckInvoiceType(item);
			base.ValidateUpdate(item);
		}

		public void CheckInvoiceType(InvoiceType item)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				IInvoiceRevenueTypeRepo invoiceRevenueTypeRepo = new InvoiceRevenueTypeRepo(db);
				InvoiceRevenueType invoiceRevenueType = invoiceRevenueTypeRepo.GetInvoiceRevenueTypeByIdNoTracking(item.AutoGenInvoiceRevenueTypeGUID);
				bool exists = invoiceRevenueType != null && (invoiceRevenueType.FeeTaxGUID != null || invoiceRevenueType.FeeWHTGUID != null);

				if (item.SuspenseInvoiceType != (int)SuspenseInvoiceType.None || 
					(item.ProductInvoice == true && ((item.ProductType == (int)ProductType.Factoring) || 
					 item.ProductType == (int)ProductType.ProjectFinance)))
				{
                    if (exists)
                    {
						ex.AddData("ERROR.90119");
					}
				}
				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public List<InvoiceType> GetInvoiceTypeByCompanyNoTracking(Guid companyGUID)
		{
			try
			{
				List<InvoiceType> invoiceTypes = Entity.Where(w => w.CompanyGUID == companyGUID).AsNoTracking().ToList();
				return invoiceTypes;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

