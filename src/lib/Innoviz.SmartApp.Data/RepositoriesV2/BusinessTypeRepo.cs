using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IBusinessTypeRepo
	{
		#region DropDown
		IEnumerable<SelectItem<BusinessTypeItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<BusinessTypeListView> GetListvw(SearchParameter search);
		BusinessTypeItemView GetByIdvw(Guid id);
		BusinessType Find(params object[] keyValues);
		BusinessType GetBusinessTypeByIdNoTracking(Guid guid);
		BusinessType CreateBusinessType(BusinessType businessType);
		void CreateBusinessTypeVoid(BusinessType businessType);
		BusinessType UpdateBusinessType(BusinessType dbBusinessType, BusinessType inputBusinessType, List<string> skipUpdateFields = null);
		void UpdateBusinessTypeVoid(BusinessType dbBusinessType, BusinessType inputBusinessType, List<string> skipUpdateFields = null);
		void Remove(BusinessType item);
	}
	public class BusinessTypeRepo : CompanyBaseRepository<BusinessType>, IBusinessTypeRepo
	{
		public BusinessTypeRepo(SmartAppDbContext context) : base(context) { }
		public BusinessTypeRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BusinessType GetBusinessTypeByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BusinessTypeGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<BusinessTypeItemViewMap> GetDropDownQuery()
		{
			return (from businessType in Entity
					select new BusinessTypeItemViewMap
					{
						CompanyGUID = businessType.CompanyGUID,
						Owner = businessType.Owner,
						OwnerBusinessUnitGUID = businessType.OwnerBusinessUnitGUID,
						BusinessTypeGUID = businessType.BusinessTypeGUID,
						BusinessTypeId = businessType.BusinessTypeId,
						Description = businessType.Description
					});
		}
		public IEnumerable<SelectItem<BusinessTypeItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<BusinessType>(search, SysParm.CompanyGUID);
				var businessType = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<BusinessTypeItemViewMap, BusinessTypeItemView>().ToDropDownItem(search.ExcludeRowData);


				return businessType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<BusinessTypeListViewMap> GetListQuery()
		{
			return (from businessType in Entity
				select new BusinessTypeListViewMap
				{
						CompanyGUID = businessType.CompanyGUID,
						Owner = businessType.Owner,
						OwnerBusinessUnitGUID = businessType.OwnerBusinessUnitGUID,
						BusinessTypeGUID = businessType.BusinessTypeGUID,
						BusinessTypeId = businessType.BusinessTypeId,
						Description = businessType.Description,
				});
		}
		public SearchResult<BusinessTypeListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<BusinessTypeListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<BusinessType>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<BusinessTypeListViewMap, BusinessTypeListView>();
				result = list.SetSearchResult<BusinessTypeListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<BusinessTypeItemViewMap> GetItemQuery()
		{
			return (from businessType in Entity
					join ownerBU in db.Set<BusinessUnit>()
					on businessType.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljBusinessTypeOwnerBU
					from ownerBU in ljBusinessTypeOwnerBU.DefaultIfEmpty()
					join company in db.Set<Company>() on businessType.CompanyGUID equals company.CompanyGUID
					select new BusinessTypeItemViewMap
					{
						CompanyGUID = businessType.CompanyGUID,
						Owner = businessType.Owner,
						OwnerBusinessUnitGUID = businessType.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CompanyId = company.CompanyId,
						CreatedBy = businessType.CreatedBy,
						CreatedDateTime = businessType.CreatedDateTime,
						ModifiedBy = businessType.ModifiedBy,
						ModifiedDateTime = businessType.ModifiedDateTime,
						BusinessTypeGUID = businessType.BusinessTypeGUID,
						BusinessTypeId = businessType.BusinessTypeId,
						Description = businessType.Description,
					
						RowVersion = businessType.RowVersion,
					});
		}
		public BusinessTypeItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<BusinessTypeItemViewMap, BusinessTypeItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public BusinessType CreateBusinessType(BusinessType businessType)
		{
			try
			{
				businessType.BusinessTypeGUID = Guid.NewGuid();
				base.Add(businessType);
				return businessType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateBusinessTypeVoid(BusinessType businessType)
		{
			try
			{
				CreateBusinessType(businessType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BusinessType UpdateBusinessType(BusinessType dbBusinessType, BusinessType inputBusinessType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBusinessType = dbBusinessType.MapUpdateValues<BusinessType>(inputBusinessType);
				base.Update(dbBusinessType);
				return dbBusinessType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateBusinessTypeVoid(BusinessType dbBusinessType, BusinessType inputBusinessType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBusinessType = dbBusinessType.MapUpdateValues<BusinessType>(inputBusinessType, skipUpdateFields);
				base.Update(dbBusinessType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

