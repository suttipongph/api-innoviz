using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IConsortiumTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<ConsortiumTableItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<ConsortiumTableListView> GetListvw(SearchParameter search);
		ConsortiumTableItemView GetByIdvw(Guid id);
		ConsortiumTable Find(params object[] keyValues);
		ConsortiumTable GetConsortiumTableByIdNoTracking(Guid guid);
		ConsortiumTable CreateConsortiumTable(ConsortiumTable consortiumTable);
		void CreateConsortiumTableVoid(ConsortiumTable consortiumTable);
		ConsortiumTable UpdateConsortiumTable(ConsortiumTable dbConsortiumTable, ConsortiumTable inputConsortiumTable, List<string> skipUpdateFields = null);
		void UpdateConsortiumTableVoid(ConsortiumTable dbConsortiumTable, ConsortiumTable inputConsortiumTable, List<string> skipUpdateFields = null);
		void Remove(ConsortiumTable item);
		void ValidateAdd(ConsortiumTable item);
		void ValidateAdd(IEnumerable<ConsortiumTable> items);
	}
	public class ConsortiumTableRepo : CompanyBaseRepository<ConsortiumTable>, IConsortiumTableRepo
	{
		public ConsortiumTableRepo(SmartAppDbContext context) : base(context) { }
		public ConsortiumTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ConsortiumTable GetConsortiumTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ConsortiumTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<ConsortiumTableItemViewMap> GetDropDownQuery()
		{
			return (from consortiumTable in Entity
					select new ConsortiumTableItemViewMap
					{
						CompanyGUID = consortiumTable.CompanyGUID,
						Owner = consortiumTable.Owner,
						OwnerBusinessUnitGUID = consortiumTable.OwnerBusinessUnitGUID,
						ConsortiumTableGUID = consortiumTable.ConsortiumTableGUID,
						ConsortiumId = consortiumTable.ConsortiumId,
						Description = consortiumTable.Description,
						DocumentStatusGUID = consortiumTable.DocumentStatusGUID
					});
		}
		public IEnumerable<SelectItem<ConsortiumTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<ConsortiumTable>(search, SysParm.CompanyGUID);
				var consortiumTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<ConsortiumTableItemViewMap, ConsortiumTableItemView>().ToDropDownItem(search.ExcludeRowData);
				return consortiumTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<ConsortiumTableListViewMap> GetListQuery()
		{
			return (from consortiumTable in Entity
				select new ConsortiumTableListViewMap
				{
						CompanyGUID = consortiumTable.CompanyGUID,
						Owner = consortiumTable.Owner,
						OwnerBusinessUnitGUID = consortiumTable.OwnerBusinessUnitGUID,
						ConsortiumTableGUID = consortiumTable.ConsortiumTableGUID,
						ConsortiumId = consortiumTable.ConsortiumId,
						Description = consortiumTable.Description,
				});
		}
		public SearchResult<ConsortiumTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<ConsortiumTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<ConsortiumTable>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<ConsortiumTableListViewMap, ConsortiumTableListView>();
				result = list.SetSearchResult<ConsortiumTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<ConsortiumTableItemViewMap> GetItemQuery()
		{
			return (from consortiumTable in Entity
					join company in db.Set<Company>()
					on consortiumTable.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on consortiumTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljConsortiumTableOwnerBU
					from ownerBU in ljConsortiumTableOwnerBU.DefaultIfEmpty()
					select new ConsortiumTableItemViewMap
					{
						CompanyGUID = consortiumTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = consortiumTable.Owner,
						OwnerBusinessUnitGUID = consortiumTable.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = consortiumTable.CreatedBy,
						CreatedDateTime = consortiumTable.CreatedDateTime,
						ModifiedBy = consortiumTable.ModifiedBy,
						ModifiedDateTime = consortiumTable.ModifiedDateTime,
						ConsortiumTableGUID = consortiumTable.ConsortiumTableGUID,
						ConsortiumId = consortiumTable.ConsortiumId,
						Description = consortiumTable.Description,
						DocumentStatusGUID = consortiumTable.DocumentStatusGUID,
					
						RowVersion = consortiumTable.RowVersion,
					});
		}
		public ConsortiumTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<ConsortiumTableItemViewMap, ConsortiumTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public ConsortiumTable CreateConsortiumTable(ConsortiumTable consortiumTable)
		{
			try
			{
				consortiumTable.ConsortiumTableGUID = Guid.NewGuid();
				base.Add(consortiumTable);
				return consortiumTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateConsortiumTableVoid(ConsortiumTable consortiumTable)
		{
			try
			{
				CreateConsortiumTable(consortiumTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ConsortiumTable UpdateConsortiumTable(ConsortiumTable dbConsortiumTable, ConsortiumTable inputConsortiumTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbConsortiumTable = dbConsortiumTable.MapUpdateValues<ConsortiumTable>(inputConsortiumTable);
				base.Update(dbConsortiumTable);
				return dbConsortiumTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateConsortiumTableVoid(ConsortiumTable dbConsortiumTable, ConsortiumTable inputConsortiumTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbConsortiumTable = dbConsortiumTable.MapUpdateValues<ConsortiumTable>(inputConsortiumTable, skipUpdateFields);
				base.Update(dbConsortiumTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

