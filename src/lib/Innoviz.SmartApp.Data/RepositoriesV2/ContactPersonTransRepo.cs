using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IContactPersonTransRepo
	{
		#region DropDown
		IEnumerable<SelectItem<ContactPersonTransItemView>> GetDropDownItem(SearchParameter search);
		IEnumerable<SelectItem<ContactPersonTransItemView>> GetDropDownItemJoinRelatedPerson(SearchParameter search);
		
		#endregion DropDown
		SearchResult<ContactPersonTransListView> GetListvw(SearchParameter search);
		ContactPersonTransItemView GetByIdvw(Guid id);
		ContactPersonTrans Find(params object[] keyValues);
		ContactPersonTrans GetContactPersonTransByIdNoTracking(Guid guid);
		ContactPersonTrans CreateContactPersonTrans(ContactPersonTrans contactPersonTrans);
		void CreateContactPersonTransVoid(ContactPersonTrans contactPersonTrans);
		ContactPersonTrans UpdateContactPersonTrans(ContactPersonTrans dbContactPersonTrans, ContactPersonTrans inputContactPersonTrans, List<string> skipUpdateFields = null);
		void UpdateContactPersonTransVoid(ContactPersonTrans dbContactPersonTrans, ContactPersonTrans inputContactPersonTrans, List<string> skipUpdateFields = null);
		void Remove(ContactPersonTrans item);
		IEnumerable<SelectItem<ContactPersonTransItemView>> GetDropDownItemByActive(SearchParameter search);
		void ValidateAdd(ContactPersonTrans item);
		void ValidateAdd(IEnumerable<ContactPersonTrans> items);
	}
	public class ContactPersonTransRepo : CompanyBaseRepository<ContactPersonTrans>, IContactPersonTransRepo
	{
		public ContactPersonTransRepo(SmartAppDbContext context) : base(context) { }
		public ContactPersonTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ContactPersonTrans GetContactPersonTransByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ContactPersonTransGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<ContactPersonTransItemViewMap> GetDropDownQuery()
		{
			return (from contactPersonTrans in Entity
					select new ContactPersonTransItemViewMap
					{
						CompanyGUID = contactPersonTrans.CompanyGUID,
						Owner = contactPersonTrans.Owner,
						OwnerBusinessUnitGUID = contactPersonTrans.OwnerBusinessUnitGUID,
						ContactPersonTransGUID = contactPersonTrans.ContactPersonTransGUID,
						RelatedPersonTableGUID = contactPersonTrans.RelatedPersonTableGUID,
						RefGUID = contactPersonTrans.RefGUID,
						RefType = contactPersonTrans.RefType,
					});
		}
		public IEnumerable<SelectItem<ContactPersonTransItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<ContactPersonTrans>(search, SysParm.CompanyGUID);
				var contactPersonTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<ContactPersonTransItemViewMap, ContactPersonTransItemView>().ToDropDownItem(search.ExcludeRowData);


				return contactPersonTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private IQueryable<ContactPersonTransItemViewMap> GetDropDownQueryJoinRelatedPerson()
		{
			return (from contactPersonTrans in Entity
					join relatedPerson in db.Set<RelatedPersonTable>()
					on contactPersonTrans.RelatedPersonTableGUID equals relatedPerson.RelatedPersonTableGUID into ljrelatedPerson
					from relatedPerson in ljrelatedPerson.DefaultIfEmpty()
					select new ContactPersonTransItemViewMap
					{
						CompanyGUID = contactPersonTrans.CompanyGUID,
						Owner = contactPersonTrans.Owner,
						OwnerBusinessUnitGUID = contactPersonTrans.OwnerBusinessUnitGUID,
						ContactPersonTransGUID = contactPersonTrans.ContactPersonTransGUID,
						RelatedPersonTableGUID = contactPersonTrans.RelatedPersonTableGUID,
						RefGUID = contactPersonTrans.RefGUID,
						RefType = contactPersonTrans.RefType,
						RelatedPersonTable_Name = relatedPerson.Name,
						RelatedPersonTable_Phone = relatedPerson.Phone,
						RelatedPersonTable_Extension = relatedPerson.Extension,
						RelatedPersonTable_Mobile = relatedPerson.Mobile,
						RelatedPersonTable_Position = relatedPerson.Position,
						RelatedPersonTable_RelatedPersonId = relatedPerson.RelatedPersonId,
						InActive = contactPersonTrans.InActive
					});
		}
		public IEnumerable<SelectItem<ContactPersonTransItemView>> GetDropDownItemJoinRelatedPerson(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<ContactPersonTrans>(search, SysParm.CompanyGUID);
				var contactPersonTrans = GetDropDownQueryJoinRelatedPerson().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<ContactPersonTransItemViewMap, ContactPersonTransItemView>().ToDropDownItem(search.ExcludeRowData);


				return contactPersonTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<ContactPersonTransListViewMap> GetListQuery()
		{
			return (from contactPersonTrans in Entity
					join relatedPersonTable in db.Set<RelatedPersonTable>() on contactPersonTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID
					select new ContactPersonTransListViewMap
					{
						CompanyGUID = contactPersonTrans.CompanyGUID,
						Owner = contactPersonTrans.Owner,
						OwnerBusinessUnitGUID = contactPersonTrans.OwnerBusinessUnitGUID,
						ContactPersonTransGUID = contactPersonTrans.ContactPersonTransGUID,
						InActive = contactPersonTrans.InActive,
						RelatedPersonTable_Name = relatedPersonTable.Name,
						RelatedPersonTable_PassportId = relatedPersonTable.PassportId,
						RelatedPersonTable_Phone = relatedPersonTable.Phone,
						RelatedPersonTable_TaxId = relatedPersonTable.TaxId,
						RefGUID = contactPersonTrans.RefGUID

					});
		}
		public SearchResult<ContactPersonTransListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<ContactPersonTransListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<ContactPersonTrans>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<ContactPersonTransListViewMap, ContactPersonTransListView>();
				result = list.SetSearchResult<ContactPersonTransListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<ContactPersonTransItemViewMap> GetItemQuery()
		{
			return (from contactPersonTrans in Entity
					join company in db.Set<Company>()
					on contactPersonTrans.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on contactPersonTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljContactPersonTransOwnerBU
					from ownerBU in ljContactPersonTransOwnerBU.DefaultIfEmpty()
					join buyer in db.Set<BuyerTable>() on contactPersonTrans.RefGUID equals buyer.BuyerTableGUID into ljContactPersonTransBuyer
					from buyer in ljContactPersonTransBuyer.DefaultIfEmpty()
					join customer in db.Set<CustomerTable>() on contactPersonTrans.RefGUID equals customer.CustomerTableGUID into ljContactPersonTransCustomer
					from customer in ljContactPersonTransCustomer.DefaultIfEmpty()
					select new ContactPersonTransItemViewMap
					{
						CompanyGUID = contactPersonTrans.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = contactPersonTrans.Owner,
						OwnerBusinessUnitGUID = contactPersonTrans.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = contactPersonTrans.CreatedBy,
						CreatedDateTime = contactPersonTrans.CreatedDateTime,
						ModifiedBy = contactPersonTrans.ModifiedBy,
						ModifiedDateTime = contactPersonTrans.ModifiedDateTime,
						ContactPersonTransGUID = contactPersonTrans.ContactPersonTransGUID,
						InActive = contactPersonTrans.InActive,
						RefGUID = contactPersonTrans.RefGUID,
						RefType = contactPersonTrans.RefType,
						RelatedPersonTableGUID = contactPersonTrans.RelatedPersonTableGUID,
						RefId = (buyer != null && contactPersonTrans.RefType == (int)RefType.Buyer) ? buyer.BuyerId :
								(customer != null && contactPersonTrans.RefType == (int)RefType.Customer) ? customer.CustomerId : null,
					
						RowVersion = contactPersonTrans.RowVersion,
					});
		}
		public ContactPersonTransItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<ContactPersonTransItemViewMap, ContactPersonTransItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public ContactPersonTrans CreateContactPersonTrans(ContactPersonTrans contactPersonTrans)
		{
			try
			{
				contactPersonTrans.ContactPersonTransGUID = Guid.NewGuid();
				base.Add(contactPersonTrans);
				return contactPersonTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateContactPersonTransVoid(ContactPersonTrans contactPersonTrans)
		{
			try
			{
				CreateContactPersonTrans(contactPersonTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ContactPersonTrans UpdateContactPersonTrans(ContactPersonTrans dbContactPersonTrans, ContactPersonTrans inputContactPersonTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbContactPersonTrans = dbContactPersonTrans.MapUpdateValues<ContactPersonTrans>(inputContactPersonTrans);
				base.Update(dbContactPersonTrans);
				return dbContactPersonTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateContactPersonTransVoid(ContactPersonTrans dbContactPersonTrans, ContactPersonTrans inputContactPersonTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbContactPersonTrans = dbContactPersonTrans.MapUpdateValues<ContactPersonTrans>(inputContactPersonTrans, skipUpdateFields);
				base.Update(dbContactPersonTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public IEnumerable<ContactPersonTrans> GetContactPersonTransByActive(Guid companyGUID, bool InActive = false)
		{
			try
			{
				return Entity.Where(t => t.CompanyGUID == companyGUID && t.InActive == InActive).AsNoTracking();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public IEnumerable<ContactPersonTrans> GetContactPersonTransByRefId(Guid companyGUID, Guid refId)
		{
			try
			{
				return Entity.Where(t => t.CompanyGUID == companyGUID && t.RefGUID == refId).AsNoTracking();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}

		public override void ValidateAdd(ContactPersonTrans item)
		{
			base.ValidateAdd(item);
			CheckDuplicateActive(item);
		}
		public override void ValidateUpdate(ContactPersonTrans item)
		{
			base.ValidateUpdate(item);
			CheckDuplicateActive(item);
		}
		#region Validate
		public void CheckDuplicateActive(ContactPersonTrans item)
		{
			try
			{
				IEnumerable<ContactPersonTrans> contactPersonTrans = GetContactPersonTransByRefId(item.CompanyGUID, item.RefGUID);
				SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
				bool isDupplicateOrdering = contactPersonTrans.Any(a => (a.InActive == false)
																	 && (item.InActive == false)
																	 && (a.RelatedPersonTableGUID == item.RelatedPersonTableGUID)
																	 && (a.ContactPersonTransGUID != item.ContactPersonTransGUID));
				if (isDupplicateOrdering)
				{
					smartAppException.AddData("ERROR.DUPLICATE", new string[] { "LABEL.RELATED_PERSON_ID", "LABEL.REF_ID", "LABEL.ACTIVE" });
				}

				if (smartAppException.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(smartAppException);
				}
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion
		#region DropDown By Condition
		private IQueryable<ContactPersonTransItemViewMap> GetDropDownQueryByActive()
		{
			return (from contactPersonTrans in Entity
					join relatedPersonTable in db.Set<RelatedPersonTable>() on contactPersonTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID
					select new ContactPersonTransItemViewMap
					{
						ContactPersonTrans_Values = SmartAppUtil.GetDropDownLabel(relatedPersonTable.RelatedPersonId, relatedPersonTable.Name),
						RefGUID = contactPersonTrans.RefGUID,
						RefType = contactPersonTrans.RefType,
						InActive = contactPersonTrans.InActive,
						CompanyGUID = contactPersonTrans.CompanyGUID,
						ContactPersonTransGUID = contactPersonTrans.ContactPersonTransGUID,
					});
		}
		public IEnumerable<SelectItem<ContactPersonTransItemView>> GetDropDownItemByActive(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<ContactPersonTrans>(search, SysParm.CompanyGUID);
				var contactPersonTrans = GetDropDownQueryByActive().Where(predicate.Predicates, predicate.Values)
																   .ToMaps<ContactPersonTransItemViewMap, ContactPersonTransItemView>()
																   .ToDropDownItem(search.ExcludeRowData, true);
				return contactPersonTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}

