using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IPaymentHistoryRepo
	{
		#region DropDown
		IEnumerable<SelectItem<PaymentHistoryItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<PaymentHistoryListView> GetListvw(SearchParameter search);
		PaymentHistoryItemView GetByIdvw(Guid id);
		PaymentHistory Find(params object[] keyValues);
		PaymentHistory GetPaymentHistoryByIdNoTracking(Guid guid);
		PaymentHistory CreatePaymentHistory(PaymentHistory paymentHistory);
		void CreatePaymentHistoryVoid(PaymentHistory paymentHistory);
		PaymentHistory UpdatePaymentHistory(PaymentHistory dbPaymentHistory, PaymentHistory inputPaymentHistory, List<string> skipUpdateFields = null);
		void UpdatePaymentHistoryVoid(PaymentHistory dbPaymentHistory, PaymentHistory inputPaymentHistory, List<string> skipUpdateFields = null);
		void Remove(PaymentHistory item);
		PaymentHistory GetPaymentHistoryByIdNoTrackingByAccessLevel(Guid guid);
		#region function
		IEnumerable<PaymentHistory> GetPaymentHistoryByInvoiceTableGuid(Guid invoiceTableGuid);
		#endregion
		List<PaymentHistory> GetPaymentHistoryByIdNoTracking(List<Guid> paymentHistoryGUID);
		List<PaymentHistory> GetPaymentHistoryForLastPaymentDateNoTracking(Guid invoiceTableGUIDs);
		List<PaymentHistory> GetPaymentHistoryForLastPaymentDateNoTracking(IEnumerable<Guid> invoiceTableGUIDs);
	}
    public class PaymentHistoryRepo : BranchCompanyBaseRepository<PaymentHistory>, IPaymentHistoryRepo
	{
		public PaymentHistoryRepo(SmartAppDbContext context) : base(context) { }
		public PaymentHistoryRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public PaymentHistory GetPaymentHistoryByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.PaymentHistoryGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<PaymentHistoryItemViewMap> GetDropDownQuery()
		{
			return (from paymentHistory in Entity
					select new PaymentHistoryItemViewMap
					{
						CompanyGUID = paymentHistory.CompanyGUID,
						Owner = paymentHistory.Owner,
						OwnerBusinessUnitGUID = paymentHistory.OwnerBusinessUnitGUID,
						PaymentHistoryGUID = paymentHistory.PaymentHistoryGUID
					});
		}
		public IEnumerable<SelectItem<PaymentHistoryItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<PaymentHistory>(search, db.GetAvailableBranch(), SysParm.CompanyGUID, SysParm.BranchGUID);
				var paymentHistory = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<PaymentHistoryItemViewMap, PaymentHistoryItemView>().ToDropDownItem(search.ExcludeRowData);
				return paymentHistory;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<PaymentHistoryListViewMap> GetListQuery()
		{
			return (from paymentHistory in Entity
						  join customerTable in db.Set<CustomerTable>()
						  on paymentHistory.CustomerTableGUID equals customerTable.CustomerTableGUID into ljcustomerTable
						  from customerTable in ljcustomerTable.DefaultIfEmpty()
						  join creditAppTable in db.Set<CreditAppTable>()
						  on paymentHistory.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljcreditAppTable
						  from creditAppTable in ljcreditAppTable.DefaultIfEmpty()
						  join invoiceTable in db.Set<InvoiceTable>()
						  on paymentHistory.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljinvoiceTable
						  from invoiceTable in ljinvoiceTable.DefaultIfEmpty()
						  select new PaymentHistoryListViewMap
						  {
							  CompanyGUID = paymentHistory.CompanyGUID,
							  Owner = paymentHistory.Owner,
							  OwnerBusinessUnitGUID = paymentHistory.OwnerBusinessUnitGUID,
							  PaymentHistoryGUID = paymentHistory.PaymentHistoryGUID,
							  CustomerTableGUID = paymentHistory.CustomerTableGUID,
							  PaymentAmount = paymentHistory.PaymentAmount,
							  WHTAmount = paymentHistory.WHTAmount,
							  CreditAppTableGUID = paymentHistory.CreditAppTableGUID,
							  ProductType = paymentHistory.ProductType,
							  InvoiceTableGUID = paymentHistory.InvoiceTableGUID,
							  PaymentDate = paymentHistory.PaymentDate,
							  ReceivedDate = paymentHistory.ReceivedDate,
							  ReceivedFrom = paymentHistory.ReceivedFrom,
							  PaymentBaseAmount = paymentHistory.PaymentBaseAmount,
							  PaymentTaxAmount = paymentHistory.PaymentTaxAmount,
							  CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
							  CreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
							  InvoiceTable_Values = SmartAppUtil.GetDropDownLabel(invoiceTable.InvoiceId, invoiceTable.IssuedDate.DateToString()),
							  CustomerTable_CustomerId = customerTable.CustomerId,
							  CreditAppTable_CreditAppTableId = creditAppTable.CreditAppId,
							  InvoiceTable_InvoiceTableId = invoiceTable.InvoiceId,
							  WHTSlipReceivedByCustomer = paymentHistory.WHTSlipReceivedByCustomer
						  });
		
		}
		public SearchResult<PaymentHistoryListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<PaymentHistoryListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<PaymentHistory>(search, db.GetAvailableBranch(), SysParm.CompanyGUID, SysParm.BranchGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<PaymentHistoryListViewMap, PaymentHistoryListView>();
				result = list.SetSearchResult<PaymentHistoryListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<PaymentHistoryItemViewMap> GetItemQuery()
		{
			return (from paymentHistory in Entity
					join company in db.Set<Company>()
					on paymentHistory.CompanyGUID equals company.CompanyGUID
					join branch in db.Set<Branch>()
					on paymentHistory.BranchGUID equals branch.BranchGUID
					join ownerBU in db.Set<BusinessUnit>()
					on paymentHistory.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljPaymentHistoryOwnerBU
					from ownerBU in ljPaymentHistoryOwnerBU.DefaultIfEmpty()

					join customerTable in db.Set<CustomerTable>()
					on paymentHistory.CustomerTableGUID equals customerTable.CustomerTableGUID into ljcustomerTable
					from customerTable in ljcustomerTable.DefaultIfEmpty()

					join creditAppTable in db.Set<CreditAppTable>()
					on paymentHistory.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljcreditAppTable
					from creditAppTable in ljcreditAppTable.DefaultIfEmpty()

					join invoiceTable in db.Set<InvoiceTable>()
					on paymentHistory.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljinvoiceTable
					from invoiceTable in ljinvoiceTable.DefaultIfEmpty()

					join currency in db.Set<Currency>()
					on paymentHistory.CurrencyGUID equals currency.CurrencyGUID into ljcurrency
					from currency in ljcurrency.DefaultIfEmpty()

					join withHolding in db.Set<WithholdingTaxTable>()
					on paymentHistory.WithholdingTaxTableGUID equals withHolding.WithholdingTaxTableGUID into ljwithHolding
					from withHolding in ljwithHolding.DefaultIfEmpty()

					join receiptTable in db.Set<ReceiptTable>()
					on paymentHistory.ReceiptTableGUID equals receiptTable.ReceiptTableGUID into ljreceiptTable
					from receiptTable in ljreceiptTable.DefaultIfEmpty()

					join taxTable in db.Set<TaxTable>()
					on paymentHistory.OrigTaxTableGUID equals taxTable.TaxTableGUID into ljtaxTable
					from taxTable in ljtaxTable.DefaultIfEmpty()

					join invoiceSettlementDetail in db.Set<InvoiceSettlementDetail>()
					on paymentHistory.InvoiceSettlementDetailGUID equals invoiceSettlementDetail.InvoiceSettlementDetailGUID into ljinvoiceSettlementDetail
					from invoiceSettlementDetail in ljinvoiceSettlementDetail.DefaultIfEmpty()

					join invoicetable_Settlement in db.Set<InvoiceTable>()
					on invoiceSettlementDetail.InvoiceTableGUID equals invoicetable_Settlement.InvoiceTableGUID into ljinvoicetable_Settle
					from invoicetable_Settlement in ljinvoicetable_Settle.DefaultIfEmpty()

					join receiptTempTable in db.Set<ReceiptTempTable>()
					on paymentHistory.RefGUID equals receiptTempTable.ReceiptTempTableGUID into ljreceiptTempTable
					from receiptTempTable in ljreceiptTempTable.DefaultIfEmpty()
					join customerRefundTable in db.Set<CustomerRefundTable> ()
					on paymentHistory.RefGUID equals customerRefundTable.CustomerRefundTableGUID into ljcustomerRefundTable
					from customerRefundTable in ljcustomerRefundTable.DefaultIfEmpty()

					select new PaymentHistoryItemViewMap
					{
						CompanyGUID = paymentHistory.CompanyGUID,
						CompanyId = company.CompanyId,
						BranchGUID = paymentHistory.BranchGUID,
						BranchId = branch.BranchId,
						Owner = paymentHistory.Owner,
						OwnerBusinessUnitGUID = paymentHistory.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = paymentHistory.CreatedBy,
						CreatedDateTime = paymentHistory.CreatedDateTime,
						ModifiedBy = paymentHistory.ModifiedBy,
						ModifiedDateTime = paymentHistory.ModifiedDateTime,
						PaymentHistoryGUID = paymentHistory.PaymentHistoryGUID,
						Cancel = paymentHistory.Cancel,
						CreditAppTableGUID = paymentHistory.CreditAppTableGUID,
						CurrencyGUID = paymentHistory.CurrencyGUID,
						CustomerTableGUID = paymentHistory.CustomerTableGUID,
						CustTransGUID = paymentHistory.CustTransGUID,
						DueDate = paymentHistory.DueDate,
						ExchangeRate = paymentHistory.ExchangeRate,
						InvoiceTableGUID = paymentHistory.InvoiceTableGUID,
						PaymentAmount = paymentHistory.PaymentAmount,
						PaymentAmountMST = paymentHistory.PaymentAmountMST,
						PaymentBaseAmount = paymentHistory.PaymentBaseAmount,
						PaymentBaseAmountMST = paymentHistory.PaymentBaseAmountMST,
						PaymentDate = paymentHistory.PaymentDate,
						PaymentTaxAmount = paymentHistory.PaymentTaxAmount,
						PaymentTaxAmountMST = paymentHistory.PaymentTaxAmountMST,
						PaymentTaxBaseAmount = paymentHistory.PaymentTaxBaseAmount,
						PaymentTaxBaseAmountMST = paymentHistory.PaymentTaxBaseAmountMST,
						ProductType = paymentHistory.ProductType,
						ReceiptTableGUID = paymentHistory.ReceiptTableGUID,
						InvoiceSettlementDetailGUID = paymentHistory.InvoiceSettlementDetailGUID,
						InvoiceSettlementDetail_Values = SmartAppUtil.GetDropDownLabel(invoicetable_Settlement.InvoiceId,invoicetable_Settlement.IssuedDate.ToString()),
						OrigTaxTableGUID = paymentHistory.OrigTaxTableGUID,
						OrigTaxTable_Values = SmartAppUtil.GetDropDownLabel(taxTable.TaxCode,taxTable.Description),
						WHTSlipReceivedByCustomer = paymentHistory.WHTSlipReceivedByCustomer,
						ReceivedFrom = paymentHistory.ReceivedFrom,
						WHTAmount = paymentHistory.WHTAmount,
						WHTAmountMST = paymentHistory.WHTAmountMST,
						WithholdingTaxTableGUID = paymentHistory.WithholdingTaxTableGUID,
						CreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
						Currency_Values = SmartAppUtil.GetDropDownLabel(currency.CurrencyId, currency.Name),

						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),

						InvoiceTable_Values = SmartAppUtil.GetDropDownLabel(invoiceTable.InvoiceId, invoiceTable.IssuedDate.DateToString()),

						WithholdingTaxTable_Values = withHolding.WHTCode,
						ReceiptTable_Values = SmartAppUtil.GetDropDownLabel(receiptTable.ReceiptId, receiptTable.ReceiptDate.DateToString()),
						RefID = (receiptTempTable != null && paymentHistory.RefType ==  (int)RefType.ReceiptTemp)? receiptTempTable.ReceiptTempId:
								(customerRefundTable != null && paymentHistory.RefType == (int)RefType.CustomerRefund)? customerRefundTable.CustomerRefundId:null,
						RefType = paymentHistory.RefType,
						RefGUID = paymentHistory.RefGUID,
						ReceivedDate = paymentHistory.ReceivedDate,
					
						RowVersion = paymentHistory.RowVersion,
					});
		}
		public PaymentHistoryItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<PaymentHistoryItemViewMap, PaymentHistoryItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public PaymentHistory CreatePaymentHistory(PaymentHistory paymentHistory)
		{
			try
			{
				paymentHistory.PaymentHistoryGUID = Guid.NewGuid();
				base.Add(paymentHistory);
				return paymentHistory;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreatePaymentHistoryVoid(PaymentHistory paymentHistory)
		{
			try
			{
				CreatePaymentHistory(paymentHistory);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public PaymentHistory UpdatePaymentHistory(PaymentHistory dbPaymentHistory, PaymentHistory inputPaymentHistory, List<string> skipUpdateFields = null)
		{
			try
			{
				dbPaymentHistory = dbPaymentHistory.MapUpdateValues<PaymentHistory>(inputPaymentHistory);
				base.Update(dbPaymentHistory);
				return dbPaymentHistory;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdatePaymentHistoryVoid(PaymentHistory dbPaymentHistory, PaymentHistory inputPaymentHistory, List<string> skipUpdateFields = null)
		{
			try
			{
				dbPaymentHistory = dbPaymentHistory.MapUpdateValues<PaymentHistory>(inputPaymentHistory, skipUpdateFields);
				base.Update(dbPaymentHistory);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public PaymentHistory GetPaymentHistoryByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.PaymentHistoryGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        #region function
		public IEnumerable<PaymentHistory> GetPaymentHistoryByInvoiceTableGuid(Guid invoiceTableGuid)
        {
            try
            {
				IEnumerable<PaymentHistory> paymentHistories = Entity.Where(w => w.InvoiceTableGUID == invoiceTableGuid)
					.AsNoTracking();
				return paymentHistories;

			}
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		#endregion
		public List<PaymentHistory> GetPaymentHistoryByIdNoTracking(List<Guid> paymentHistoryGUID)
		{
            try
            {
				List<PaymentHistory> paymentHistories = Entity.Where(w => paymentHistoryGUID.Contains(w.PaymentHistoryGUID)).ToList();
				return paymentHistories;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<PaymentHistory> GetPaymentHistoryForLastPaymentDateNoTracking(Guid invoiceTableGUID)
        {
            try
            {
				var invoiceTableGuids = new List<Guid>() { invoiceTableGUID };
				return GetPaymentHistoryForLastPaymentDateNoTracking(invoiceTableGuids);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public List<PaymentHistory> GetPaymentHistoryForLastPaymentDateNoTracking(IEnumerable<Guid> invoiceTableGUIDs)
        {
            try
            {
				var result = Entity.Where(w => invoiceTableGUIDs.Contains(w.InvoiceTableGUID) && w.Cancel == false)
								.AsNoTracking()
								.ToList();
				return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
	}
}

