using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IBuyerCreditLimitByProductRepo
	{
		#region DropDown
		IEnumerable<SelectItem<BuyerCreditLimitByProductItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<BuyerCreditLimitByProductListView> GetListvw(SearchParameter search);
		BuyerCreditLimitByProductItemView GetByIdvw(Guid id);
		BuyerCreditLimitByProduct Find(params object[] keyValues);
		BuyerCreditLimitByProduct GetBuyerCreditLimitByProductByIdNoTracking(Guid guid);
		BuyerCreditLimitByProduct CreateBuyerCreditLimitByProduct(BuyerCreditLimitByProduct buyerCreditLimitByProduct);
		void CreateBuyerCreditLimitByProductVoid(BuyerCreditLimitByProduct buyerCreditLimitByProduct);
		BuyerCreditLimitByProduct UpdateBuyerCreditLimitByProduct(BuyerCreditLimitByProduct dbBuyerCreditLimitByProduct, BuyerCreditLimitByProduct inputBuyerCreditLimitByProduct, List<string> skipUpdateFields = null);
		void UpdateBuyerCreditLimitByProductVoid(BuyerCreditLimitByProduct dbBuyerCreditLimitByProduct, BuyerCreditLimitByProduct inputBuyerCreditLimitByProduct, List<string> skipUpdateFields = null);
		void Remove(BuyerCreditLimitByProduct item);
		IQueryable<BuyerCreditLimitByProduct> GetBuyerCreditLimitByProductByBuyer(Guid buyerTableGUID);
		void ValidateAdd(BuyerCreditLimitByProduct item);
		void ValidateAdd(IEnumerable<BuyerCreditLimitByProduct> items);
	}
	public class BuyerCreditLimitByProductRepo : CompanyBaseRepository<BuyerCreditLimitByProduct>, IBuyerCreditLimitByProductRepo
	{
		public BuyerCreditLimitByProductRepo(SmartAppDbContext context) : base(context) { }
		public BuyerCreditLimitByProductRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BuyerCreditLimitByProduct GetBuyerCreditLimitByProductByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BuyerCreditLimitByProductGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<BuyerCreditLimitByProductItemViewMap> GetDropDownQuery()
		{
			return (from buyerCreditLimitByProduct in Entity
					select new BuyerCreditLimitByProductItemViewMap
					{
						CompanyGUID = buyerCreditLimitByProduct.CompanyGUID,
						Owner = buyerCreditLimitByProduct.Owner,
						OwnerBusinessUnitGUID = buyerCreditLimitByProduct.OwnerBusinessUnitGUID,
						BuyerCreditLimitByProductGUID = buyerCreditLimitByProduct.BuyerCreditLimitByProductGUID
					});
		}
		public IEnumerable<SelectItem<BuyerCreditLimitByProductItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<BuyerCreditLimitByProduct>(search, SysParm.CompanyGUID);
				var buyerCreditLimitByProduct = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<BuyerCreditLimitByProductItemViewMap, BuyerCreditLimitByProductItemView>().ToDropDownItem(search.ExcludeRowData);


				return buyerCreditLimitByProduct;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<BuyerCreditLimitByProductListViewMap> GetListQuery()
		{
			return (from buyerCreditLimitByProduct in Entity
				select new BuyerCreditLimitByProductListViewMap
				{
						CompanyGUID = buyerCreditLimitByProduct.CompanyGUID,
						Owner = buyerCreditLimitByProduct.Owner,
						OwnerBusinessUnitGUID = buyerCreditLimitByProduct.OwnerBusinessUnitGUID,
						BuyerCreditLimitByProductGUID = buyerCreditLimitByProduct.BuyerCreditLimitByProductGUID,
						ProductType = buyerCreditLimitByProduct.ProductType,
						CreditLimit = buyerCreditLimitByProduct.CreditLimit,
						BuyerTableGUID = buyerCreditLimitByProduct.BuyerTableGUID,
				});
		}
		public SearchResult<BuyerCreditLimitByProductListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<BuyerCreditLimitByProductListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<BuyerCreditLimitByProduct>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<BuyerCreditLimitByProductListViewMap, BuyerCreditLimitByProductListView>();
				result = list.SetSearchResult<BuyerCreditLimitByProductListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<BuyerCreditLimitByProductItemViewMap> GetItemQuery()
		{
			return (from buyerCreditLimitByProduct in Entity
					join company in db.Set<Company>()
					on buyerCreditLimitByProduct.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on buyerCreditLimitByProduct.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljBuyerCreditLimitByProductOwnerBU
					from ownerBU in ljBuyerCreditLimitByProductOwnerBU.DefaultIfEmpty()
					select new BuyerCreditLimitByProductItemViewMap
					{
						CompanyGUID = buyerCreditLimitByProduct.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = buyerCreditLimitByProduct.Owner,
						OwnerBusinessUnitGUID = buyerCreditLimitByProduct.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = buyerCreditLimitByProduct.CreatedBy,
						CreatedDateTime = buyerCreditLimitByProduct.CreatedDateTime,
						ModifiedBy = buyerCreditLimitByProduct.ModifiedBy,
						ModifiedDateTime = buyerCreditLimitByProduct.ModifiedDateTime,
						BuyerCreditLimitByProductGUID = buyerCreditLimitByProduct.BuyerCreditLimitByProductGUID,
						BuyerTableGUID = buyerCreditLimitByProduct.BuyerTableGUID,
						CreditLimit = buyerCreditLimitByProduct.CreditLimit,
						ProductType = buyerCreditLimitByProduct.ProductType,
					
						RowVersion = buyerCreditLimitByProduct.RowVersion,
					});
		}
		public BuyerCreditLimitByProductItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<BuyerCreditLimitByProductItemViewMap, BuyerCreditLimitByProductItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public BuyerCreditLimitByProduct CreateBuyerCreditLimitByProduct(BuyerCreditLimitByProduct buyerCreditLimitByProduct)
		{
			try
			{
				buyerCreditLimitByProduct.BuyerCreditLimitByProductGUID = Guid.NewGuid();
				base.Add(buyerCreditLimitByProduct);
				return buyerCreditLimitByProduct;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateBuyerCreditLimitByProductVoid(BuyerCreditLimitByProduct buyerCreditLimitByProduct)
		{
			try
			{
				CreateBuyerCreditLimitByProduct(buyerCreditLimitByProduct);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BuyerCreditLimitByProduct UpdateBuyerCreditLimitByProduct(BuyerCreditLimitByProduct dbBuyerCreditLimitByProduct, BuyerCreditLimitByProduct inputBuyerCreditLimitByProduct, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBuyerCreditLimitByProduct = dbBuyerCreditLimitByProduct.MapUpdateValues<BuyerCreditLimitByProduct>(inputBuyerCreditLimitByProduct);
				base.Update(dbBuyerCreditLimitByProduct);
				return dbBuyerCreditLimitByProduct;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateBuyerCreditLimitByProductVoid(BuyerCreditLimitByProduct dbBuyerCreditLimitByProduct, BuyerCreditLimitByProduct inputBuyerCreditLimitByProduct, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBuyerCreditLimitByProduct = dbBuyerCreditLimitByProduct.MapUpdateValues<BuyerCreditLimitByProduct>(inputBuyerCreditLimitByProduct, skipUpdateFields);
				base.Update(dbBuyerCreditLimitByProduct);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public IQueryable<BuyerCreditLimitByProduct> GetBuyerCreditLimitByProductByBuyer(Guid buyerTableGUID)
		{
			try
			{
				var result = Entity.Where(item => item.BuyerTableGUID == buyerTableGUID).AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

