using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IReceiptTempTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<ReceiptTempTableItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<ReceiptTempTableListView> GetListvw(SearchParameter search);
		ReceiptTempTableItemView GetByIdvw(Guid id);
		ReceiptTempTableItemView GetByIdvwByCancelReceiptTemp(Guid guid);
		ReceiptTempTable Find(params object[] keyValues);
		ReceiptTempTable GetReceiptTempTableByIdNoTracking(Guid guid);
		ReceiptTempTable CreateReceiptTempTable(ReceiptTempTable receiptTempTable);
		void CreateReceiptTempTableVoid(ReceiptTempTable receiptTempTable);
		ReceiptTempTable UpdateReceiptTempTable(ReceiptTempTable dbReceiptTempTable, ReceiptTempTable inputReceiptTempTable, List<string> skipUpdateFields = null);
		void UpdateReceiptTempTableVoid(ReceiptTempTable dbReceiptTempTable, ReceiptTempTable inputReceiptTempTable, List<string> skipUpdateFields = null);
		void Remove(ReceiptTempTable item);
		ReceiptTempTable GetReceiptTempTableByIdNoTrackingByAccessLevel(Guid guid);
		List<ReceiptTempTable> GetReceiptTempTableByIdNoTracking(List<Guid> receiptTempTableGuids);
		ReceiptTempTable GetReceiptTempTableByReceiptTempIdNoTracking(string id);
	}
	public class ReceiptTempTableRepo : CompanyBaseRepository<ReceiptTempTable>, IReceiptTempTableRepo
	{
		public ReceiptTempTableRepo(SmartAppDbContext context) : base(context) { }
		public ReceiptTempTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ReceiptTempTable GetReceiptTempTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ReceiptTempTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<ReceiptTempTableItemViewMap> GetDropDownQuery()
		{
			return (from receiptTempTable in Entity
					select new ReceiptTempTableItemViewMap
					{
						CompanyGUID = receiptTempTable.CompanyGUID,
						Owner = receiptTempTable.Owner,
						OwnerBusinessUnitGUID = receiptTempTable.OwnerBusinessUnitGUID,
						ReceiptTempTableGUID = receiptTempTable.ReceiptTempTableGUID,
						ReceiptTempId = receiptTempTable.ReceiptTempId,
						ReceiptDate = receiptTempTable.ReceiptDate
					});
		}
		public IEnumerable<SelectItem<ReceiptTempTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<ReceiptTempTable>(search, SysParm.CompanyGUID);
				var receiptTempTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<ReceiptTempTableItemViewMap, ReceiptTempTableItemView>().ToDropDownItem(search.ExcludeRowData);
				return receiptTempTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<ReceiptTempTableListViewMap> GetListQuery()
		{
			return (from receiptTempTable in Entity

					join customerTable in db.Set<CustomerTable>()
					on receiptTempTable.CustomerTableGUID equals customerTable.CustomerTableGUID

					join documentstatus in db.Set<DocumentStatus>()
					on receiptTempTable.DocumentStatusGUID equals documentstatus.DocumentStatusGUID

					join currency in db.Set<Currency>()
					on receiptTempTable.CurrencyGUID equals currency.CurrencyGUID into ljCurrency
					from currency in ljCurrency.DefaultIfEmpty()

					join buyerTable in db.Set<BuyerTable>()
					on receiptTempTable.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljBuyerTable
					from buyerTable in ljBuyerTable.DefaultIfEmpty()

					select new ReceiptTempTableListViewMap
				{
					CompanyGUID = receiptTempTable.CompanyGUID,
					Owner = receiptTempTable.Owner,
					OwnerBusinessUnitGUID = receiptTempTable.OwnerBusinessUnitGUID,
					ReceiptTempTableGUID = receiptTempTable.ReceiptTempTableGUID,
					ReceiptTempId = receiptTempTable.ReceiptTempId,
					DocumentStatusGUID = receiptTempTable.DocumentStatusGUID,
					ReceiptDate = receiptTempTable.ReceiptDate,
					TransDate = receiptTempTable.TransDate,
					ProductType = receiptTempTable.ProductType,
					ReceivedFrom = receiptTempTable.ReceivedFrom,
					CustomerTableGUID = receiptTempTable.CustomerTableGUID,
					BuyerTableGUID = receiptTempTable.BuyerTableGUID,
					ReceiptAmount = receiptTempTable.ReceiptAmount,
					CurrencyGUID = receiptTempTable.CurrencyGUID,
					CustomerTable_CustomerId = customerTable.CustomerId,
					CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
					BuyerTable_BuyerId = (buyerTable != null) ? buyerTable.BuyerId: null,
					BuyerTable_Values = (buyerTable != null) ? SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName) : null,
					DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentstatus.Description),
					DocumentStatus_StatusId = documentstatus.StatusId,
					Currency_Values = (currency != null) ? SmartAppUtil.GetDropDownLabel(currency.CurrencyId, currency.Name) : null,
					Currency_CurrencyId = (currency != null) ? currency.CurrencyId : null,
					ReceiptTempRefType = receiptTempTable.ReceiptTempRefType
					});
		}
		public SearchResult<ReceiptTempTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<ReceiptTempTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<ReceiptTempTable>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<ReceiptTempTableListViewMap, ReceiptTempTableListView>();
				result = list.SetSearchResult<ReceiptTempTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<ReceiptTempTableItemViewMap> GetItemQuery()
		{
			return (from receiptTempTable in Entity
					join company in db.Set<Company>()
					on receiptTempTable.CompanyGUID equals company.CompanyGUID

					join documentStatus in db.Set<DocumentStatus>() on receiptTempTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID

					join documentReason in db.Set<DocumentReason>() on receiptTempTable.DocumentReasonGUID equals documentReason.DocumentReasonGUID into ljDocumentReason
					from documentReason in ljDocumentReason.DefaultIfEmpty()

					join refReceiptTempTable in db.Set<ReceiptTempTable>() on receiptTempTable.RefReceiptTempGUID equals refReceiptTempTable.ReceiptTempTableGUID into ljRefReceiptTempTable
					from refReceiptTempTable in ljRefReceiptTempTable.DefaultIfEmpty()

					join mainReceiptTempTable in db.Set<ReceiptTempTable>() on receiptTempTable.MainReceiptTempGUID equals mainReceiptTempTable.ReceiptTempTableGUID into ljMainReceiptTempTable
					from mainReceiptTempTable in ljMainReceiptTempTable.DefaultIfEmpty()

					join invoiceType in db.Set<InvoiceType>() on receiptTempTable.SuspenseInvoiceTypeGUID equals invoiceType.InvoiceTypeGUID into ljInvoiceType
					from invoiceType in ljInvoiceType.DefaultIfEmpty()

					join ownerBU in db.Set<BusinessUnit>()
					on receiptTempTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljReceiptTempTableOwnerBU
					from ownerBU in ljReceiptTempTableOwnerBU.DefaultIfEmpty()

					join documentProcess in db.Set<DocumentProcess>()
					on documentStatus.DocumentProcessGUID equals documentProcess.DocumentProcessGUID into ljdocumentProcess
					from documentProcess in ljdocumentProcess.DefaultIfEmpty()

					join customerTable in db.Set<CustomerTable>()
					on receiptTempTable.CustomerTableGUID equals customerTable.CustomerTableGUID into ljcustomerTable
					from customerTable in ljcustomerTable.DefaultIfEmpty()

					join buyerTable in db.Set<BuyerTable>()
					on receiptTempTable.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
					from buyerTable in ljbuyerTable.DefaultIfEmpty()

					join creditAppTable in db.Set<CreditAppTable>()
					on receiptTempTable.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljcreditAppTable
					from creditAppTable in ljcreditAppTable.DefaultIfEmpty()

					join suspenseInvoiceType in db.Set<InvoiceType>()
					on receiptTempTable.SuspenseInvoiceTypeGUID equals suspenseInvoiceType.InvoiceTypeGUID into ljsuspenseInvoiceType
					from suspenseInvoiceType in ljsuspenseInvoiceType.DefaultIfEmpty()
					select new ReceiptTempTableItemViewMap
					{
						CompanyGUID = receiptTempTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = receiptTempTable.Owner,
						OwnerBusinessUnitGUID = receiptTempTable.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = receiptTempTable.CreatedBy,
						CreatedDateTime = receiptTempTable.CreatedDateTime,
						ModifiedBy = receiptTempTable.ModifiedBy,
						ModifiedDateTime = receiptTempTable.ModifiedDateTime,
						ReceiptTempTableGUID = receiptTempTable.ReceiptTempTableGUID,
						BuyerReceiptTableGUID = receiptTempTable.BuyerReceiptTableGUID,
						BuyerTableGUID = receiptTempTable.BuyerTableGUID,
						CreditAppTableGUID = receiptTempTable.CreditAppTableGUID,
						CurrencyGUID = receiptTempTable.CurrencyGUID,
						CustomerTableGUID = receiptTempTable.CustomerTableGUID,
						Dimension1GUID = receiptTempTable.Dimension1GUID,
						Dimension2GUID = receiptTempTable.Dimension2GUID,
						Dimension3GUID = receiptTempTable.Dimension3GUID,
						Dimension4GUID = receiptTempTable.Dimension4GUID,
						Dimension5GUID = receiptTempTable.Dimension5GUID,
						DocumentReasonGUID = receiptTempTable.DocumentReasonGUID,
						DocumentStatusGUID = receiptTempTable.DocumentStatusGUID,
						ExchangeRate = receiptTempTable.ExchangeRate,
						MainReceiptTempGUID = receiptTempTable.MainReceiptTempGUID,
						ProductType = receiptTempTable.ProductType,
						ReceiptAmount = receiptTempTable.ReceiptAmount,
						ReceiptDate = receiptTempTable.ReceiptDate,
						ReceiptTempId = receiptTempTable.ReceiptTempId,
						ReceiptTempRefType = receiptTempTable.ReceiptTempRefType,
						ReceivedFrom = receiptTempTable.ReceivedFrom,
						RefReceiptTempGUID = receiptTempTable.RefReceiptTempGUID,
						Remark = receiptTempTable.Remark,
						SettleAmount = receiptTempTable.SettleAmount,
						SettleFeeAmount = receiptTempTable.SettleFeeAmount,
						SuspenseAmount = receiptTempTable.SuspenseAmount,
						SuspenseInvoiceTypeGUID = receiptTempTable.SuspenseInvoiceTypeGUID,
						TransDate = receiptTempTable.TransDate,
						DocumentReason_Values = (documentReason != null) ? SmartAppUtil.GetDropDownLabel(documentReason.ReasonId, documentReason.Description): null,
						DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
						MainReceiptTemp_Values = (mainReceiptTempTable != null) ? SmartAppUtil.GetDropDownLabel(mainReceiptTempTable.ReceiptTempId, mainReceiptTempTable.ReceiptDate.DateToString()) : null,
						RefReceiptTemp_Values = (refReceiptTempTable != null) ? SmartAppUtil.GetDropDownLabel(refReceiptTempTable.ReceiptTempId, refReceiptTempTable.ReceiptDate.DateToString()) : null,
						InvoiceType_ValidateDirectReceiveByCA = (invoiceType != null) ? invoiceType.ValidateDirectReceiveByCA : false,
						DocumentStatus_StatusId = documentStatus.StatusId,
						Process_Id = documentProcess.ProcessId,
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId,customerTable.Name),
						BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId,buyerTable.BuyerName),
						CreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId,creditAppTable.Description),
						SuspenseInvoiceType_Values = SmartAppUtil.GetDropDownLabel(suspenseInvoiceType.InvoiceTypeId,suspenseInvoiceType.Description)
					});
		}

		private IQueryable<ReceiptTempTableItemViewMap> GetItemByCancelReceiptTempQuery()
		{
			return (from receiptTempTable in Entity
					join company in db.Set<Company>()
					on receiptTempTable.CompanyGUID equals company.CompanyGUID

					join documentStatus in db.Set<DocumentStatus>() on receiptTempTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID

					join documentReason in db.Set<DocumentReason>() on receiptTempTable.DocumentReasonGUID equals documentReason.DocumentReasonGUID into ljDocumentReason
					from documentReason in ljDocumentReason.DefaultIfEmpty()

					join refReceiptTempTable in db.Set<ReceiptTempTable>() on receiptTempTable.RefReceiptTempGUID equals refReceiptTempTable.ReceiptTempTableGUID into ljRefReceiptTempTable
					from refReceiptTempTable in ljRefReceiptTempTable.DefaultIfEmpty()

					join mainReceiptTempTable in db.Set<ReceiptTempTable>() on receiptTempTable.MainReceiptTempGUID equals mainReceiptTempTable.ReceiptTempTableGUID into ljMainReceiptTempTable
					from mainReceiptTempTable in ljMainReceiptTempTable.DefaultIfEmpty()

					join invoiceType in db.Set<InvoiceType>() on receiptTempTable.SuspenseInvoiceTypeGUID equals invoiceType.InvoiceTypeGUID into ljInvoiceType
					from invoiceType in ljInvoiceType.DefaultIfEmpty()

					join customerTable in db.Set<CustomerTable>()
					on receiptTempTable.CustomerTableGUID equals customerTable.CustomerTableGUID

					join ownerBU in db.Set<BusinessUnit>()
					on receiptTempTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljReceiptTempTableOwnerBU
					from ownerBU in ljReceiptTempTableOwnerBU.DefaultIfEmpty()

					join buyerTable in db.Set<BuyerTable>()
					on receiptTempTable.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljBuyerTable
					from buyerTable in ljBuyerTable.DefaultIfEmpty()

					join creditAppTable in db.Set<CreditAppTable>()
					on receiptTempTable.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljCreditAppTable
					from creditAppTable in ljCreditAppTable.DefaultIfEmpty()

					select new ReceiptTempTableItemViewMap
					{
						CompanyGUID = receiptTempTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = receiptTempTable.Owner,
						OwnerBusinessUnitGUID = receiptTempTable.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = receiptTempTable.CreatedBy,
						CreatedDateTime = receiptTempTable.CreatedDateTime,
						ModifiedBy = receiptTempTable.ModifiedBy,
						ModifiedDateTime = receiptTempTable.ModifiedDateTime,
						ReceiptTempTableGUID = receiptTempTable.ReceiptTempTableGUID,
						BuyerReceiptTableGUID = receiptTempTable.BuyerReceiptTableGUID,
						BuyerTableGUID = receiptTempTable.BuyerTableGUID,
						CreditAppTableGUID = receiptTempTable.CreditAppTableGUID,
						CurrencyGUID = receiptTempTable.CurrencyGUID,
						CustomerTableGUID = receiptTempTable.CustomerTableGUID,
						Dimension1GUID = receiptTempTable.Dimension1GUID,
						Dimension2GUID = receiptTempTable.Dimension2GUID,
						Dimension3GUID = receiptTempTable.Dimension3GUID,
						Dimension4GUID = receiptTempTable.Dimension4GUID,
						Dimension5GUID = receiptTempTable.Dimension5GUID,
						DocumentReasonGUID = receiptTempTable.DocumentReasonGUID,
						DocumentStatusGUID = receiptTempTable.DocumentStatusGUID,
						ExchangeRate = receiptTempTable.ExchangeRate,
						MainReceiptTempGUID = receiptTempTable.MainReceiptTempGUID,
						ProductType = receiptTempTable.ProductType,
						ReceiptAmount = receiptTempTable.ReceiptAmount,
						ReceiptDate = receiptTempTable.ReceiptDate,
						ReceiptTempId = receiptTempTable.ReceiptTempId,
						ReceiptTempRefType = receiptTempTable.ReceiptTempRefType,
						ReceivedFrom = receiptTempTable.ReceivedFrom,
						RefReceiptTempGUID = receiptTempTable.RefReceiptTempGUID,
						Remark = receiptTempTable.Remark,
						SettleAmount = receiptTempTable.SettleAmount,
						SettleFeeAmount = receiptTempTable.SettleFeeAmount,
						SuspenseAmount = receiptTempTable.SuspenseAmount,
						SuspenseInvoiceTypeGUID = receiptTempTable.SuspenseInvoiceTypeGUID,
						TransDate = receiptTempTable.TransDate,
						DocumentReason_Values = (documentReason != null) ? SmartAppUtil.GetDropDownLabel(documentReason.ReasonId, documentReason.Description) : null,
						DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
						MainReceiptTemp_Values = (mainReceiptTempTable != null) ? SmartAppUtil.GetDropDownLabel(mainReceiptTempTable.ReceiptTempId, mainReceiptTempTable.ReceiptDate.DateToString()) : null,
						RefReceiptTemp_Values = (refReceiptTempTable != null) ? SmartAppUtil.GetDropDownLabel(refReceiptTempTable.ReceiptTempId, refReceiptTempTable.ReceiptDate.DateToString()) : null,
						InvoiceType_ValidateDirectReceiveByCA = (invoiceType != null) ? invoiceType.ValidateDirectReceiveByCA : false,
						DocumentStatus_StatusId = documentStatus.StatusId,

						BuyerTable_Values = (buyerTable != null) ? SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName) : null,
						CreditAppTable_Values = (creditAppTable != null) ? SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description) : null,
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
						SuspenseInvoiceType_Values = (invoiceType != null) ? SmartAppUtil.GetDropDownLabel(invoiceType.InvoiceTypeId, invoiceType.Description) : null,
					
						RowVersion = receiptTempTable.RowVersion,
					});
		}
		public ReceiptTempTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<ReceiptTempTableItemViewMap, ReceiptTempTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public ReceiptTempTableItemView GetByIdvwByCancelReceiptTemp(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemByCancelReceiptTempQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<ReceiptTempTableItemViewMap, ReceiptTempTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public ReceiptTempTable CreateReceiptTempTable(ReceiptTempTable receiptTempTable)
		{
			try
			{
				receiptTempTable.ReceiptTempTableGUID = Guid.NewGuid();
				base.Add(receiptTempTable);
				return receiptTempTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateReceiptTempTableVoid(ReceiptTempTable receiptTempTable)
		{
			try
			{
				CreateReceiptTempTable(receiptTempTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ReceiptTempTable UpdateReceiptTempTable(ReceiptTempTable dbReceiptTempTable, ReceiptTempTable inputReceiptTempTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbReceiptTempTable = dbReceiptTempTable.MapUpdateValues<ReceiptTempTable>(inputReceiptTempTable);
				base.Update(dbReceiptTempTable);
				return dbReceiptTempTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateReceiptTempTableVoid(ReceiptTempTable dbReceiptTempTable, ReceiptTempTable inputReceiptTempTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbReceiptTempTable = dbReceiptTempTable.MapUpdateValues<ReceiptTempTable>(inputReceiptTempTable, skipUpdateFields);
				base.Update(dbReceiptTempTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		#region Validate
		public override void ValidateAdd(ReceiptTempTable item)
		{
			base.ValidateAdd(item);
			CheckDuplicateBuyerReceiptTable(item);
		}
		public override void ValidateUpdate(ReceiptTempTable item)
		{
			base.ValidateUpdate(item);
			CheckDuplicateBuyerReceiptTable(item);
		}
		public void CheckDuplicateBuyerReceiptTable(ReceiptTempTable item)
		{
			try
			{
				if (ConditionService.IsNotNullAndNotEmptyGUID(item.BuyerReceiptTableGUID))
				{
					SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
					IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
					Guid cancelStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(Convert.ToInt32(TemporaryReceiptStatus.Cancelled).ToString()).DocumentStatusGUID;
					bool isDupplicateBuyerReceiptTable = Entity.Any(t => t.BuyerReceiptTableGUID == item.BuyerReceiptTableGUID && t.DocumentStatusGUID != cancelStatus && t.ReceiptTempTableGUID != item.ReceiptTempTableGUID && t.CompanyGUID == item.CompanyGUID);

					if (isDupplicateBuyerReceiptTable)
					{
						smartAppException.AddData("ERROR.DUPLICATE", new string[] { "LABEL.BUYER_RECEIPT_ID" });
					}
					if (smartAppException.Data.Count > 0)
					{
						throw SmartAppUtil.AddStackTrace(smartAppException);
					}
				}
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion
		public ReceiptTempTable GetReceiptTempTableByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ReceiptTempTableGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<ReceiptTempTable> GetReceiptTempTableByIdNoTracking(List<Guid> receiptTempTableGuids)
        {
            try
            {
				List<ReceiptTempTable> receiptTempTables = Entity.Where(w => receiptTempTableGuids.Contains(w.ReceiptTempTableGUID)).AsNoTracking().ToList();
				return receiptTempTables;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ReceiptTempTable GetReceiptTempTableByReceiptTempIdNoTracking(string id)
		{
			try
			{
				var result = Entity.Where(item => item.ReceiptTempId == id).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}

