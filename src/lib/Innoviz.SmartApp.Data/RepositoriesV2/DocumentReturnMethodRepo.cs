using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IDocumentReturnMethodRepo
	{
		#region DropDown
		IEnumerable<SelectItem<DocumentReturnMethodItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<DocumentReturnMethodListView> GetListvw(SearchParameter search);
		DocumentReturnMethodItemView GetByIdvw(Guid id);
		DocumentReturnMethod Find(params object[] keyValues);
		DocumentReturnMethod GetDocumentReturnMethodByIdNoTracking(Guid guid);
		DocumentReturnMethod CreateDocumentReturnMethod(DocumentReturnMethod documentReturnMethod);
		void CreateDocumentReturnMethodVoid(DocumentReturnMethod documentReturnMethod);
		DocumentReturnMethod UpdateDocumentReturnMethod(DocumentReturnMethod dbDocumentReturnMethod, DocumentReturnMethod inputDocumentReturnMethod, List<string> skipUpdateFields = null);
		void UpdateDocumentReturnMethodVoid(DocumentReturnMethod dbDocumentReturnMethod, DocumentReturnMethod inputDocumentReturnMethod, List<string> skipUpdateFields = null);
		void Remove(DocumentReturnMethod item);
		DocumentReturnMethod GetDocumentReturnMethodByIdNoTrackingByAccessLevel(Guid guid);
	}
	public class DocumentReturnMethodRepo : CompanyBaseRepository<DocumentReturnMethod>, IDocumentReturnMethodRepo
	{
		public DocumentReturnMethodRepo(SmartAppDbContext context) : base(context) { }
		public DocumentReturnMethodRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public DocumentReturnMethod GetDocumentReturnMethodByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.DocumentReturnMethodGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<DocumentReturnMethodItemViewMap> GetDropDownQuery()
		{
			return (from documentReturnMethod in Entity
					select new DocumentReturnMethodItemViewMap
					{
						CompanyGUID = documentReturnMethod.CompanyGUID,
						Owner = documentReturnMethod.Owner,
						OwnerBusinessUnitGUID = documentReturnMethod.OwnerBusinessUnitGUID,
						DocumentReturnMethodGUID = documentReturnMethod.DocumentReturnMethodGUID,
						DocumentReturnMethodId = documentReturnMethod.DocumentReturnMethodId,
						Description = documentReturnMethod.Description
					});
		}
		public IEnumerable<SelectItem<DocumentReturnMethodItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<DocumentReturnMethod>(search, SysParm.CompanyGUID);
				var documentReturnMethod = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<DocumentReturnMethodItemViewMap, DocumentReturnMethodItemView>().ToDropDownItem(search.ExcludeRowData);


				return documentReturnMethod;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<DocumentReturnMethodListViewMap> GetListQuery()
		{
			return (from documentReturnMethod in Entity
				select new DocumentReturnMethodListViewMap
				{
						CompanyGUID = documentReturnMethod.CompanyGUID,
						Owner = documentReturnMethod.Owner,
						OwnerBusinessUnitGUID = documentReturnMethod.OwnerBusinessUnitGUID,
						DocumentReturnMethodGUID = documentReturnMethod.DocumentReturnMethodGUID,
						DocumentReturnMethodId = documentReturnMethod.DocumentReturnMethodId,
						Description = documentReturnMethod.Description,
				});
		}
		public SearchResult<DocumentReturnMethodListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<DocumentReturnMethodListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<DocumentReturnMethod>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<DocumentReturnMethodListViewMap, DocumentReturnMethodListView>();
				result = list.SetSearchResult<DocumentReturnMethodListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<DocumentReturnMethodItemViewMap> GetItemQuery()
		{
			return (from documentReturnMethod in Entity
					join company in db.Set<Company>()
					on documentReturnMethod.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on documentReturnMethod.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljDocumentReturnMethodOwnerBU
					from ownerBU in ljDocumentReturnMethodOwnerBU.DefaultIfEmpty()
					select new DocumentReturnMethodItemViewMap
					{
						CompanyGUID = documentReturnMethod.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = documentReturnMethod.Owner,
						OwnerBusinessUnitGUID = documentReturnMethod.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = documentReturnMethod.CreatedBy,
						CreatedDateTime = documentReturnMethod.CreatedDateTime,
						ModifiedBy = documentReturnMethod.ModifiedBy,
						ModifiedDateTime = documentReturnMethod.ModifiedDateTime,
						DocumentReturnMethodGUID = documentReturnMethod.DocumentReturnMethodGUID,
						Description = documentReturnMethod.Description,
						DocumentReturnMethodId = documentReturnMethod.DocumentReturnMethodId,
					
						RowVersion = documentReturnMethod.RowVersion,
					});
		}
		public DocumentReturnMethodItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<DocumentReturnMethodItemViewMap, DocumentReturnMethodItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public DocumentReturnMethod CreateDocumentReturnMethod(DocumentReturnMethod documentReturnMethod)
		{
			try
			{
				documentReturnMethod.DocumentReturnMethodGUID = Guid.NewGuid();
				base.Add(documentReturnMethod);
				return documentReturnMethod;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateDocumentReturnMethodVoid(DocumentReturnMethod documentReturnMethod)
		{
			try
			{
				CreateDocumentReturnMethod(documentReturnMethod);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentReturnMethod UpdateDocumentReturnMethod(DocumentReturnMethod dbDocumentReturnMethod, DocumentReturnMethod inputDocumentReturnMethod, List<string> skipUpdateFields = null)
		{
			try
			{
				dbDocumentReturnMethod = dbDocumentReturnMethod.MapUpdateValues<DocumentReturnMethod>(inputDocumentReturnMethod);
				base.Update(dbDocumentReturnMethod);
				return dbDocumentReturnMethod;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateDocumentReturnMethodVoid(DocumentReturnMethod dbDocumentReturnMethod, DocumentReturnMethod inputDocumentReturnMethod, List<string> skipUpdateFields = null)
		{
			try
			{
				dbDocumentReturnMethod = dbDocumentReturnMethod.MapUpdateValues<DocumentReturnMethod>(inputDocumentReturnMethod, skipUpdateFields);
				base.Update(dbDocumentReturnMethod);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public DocumentReturnMethod GetDocumentReturnMethodByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.DocumentReturnMethodGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

