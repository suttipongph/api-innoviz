using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ITaxInvoiceTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<TaxInvoiceTableItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<TaxInvoiceTableListView> GetListvw(SearchParameter search);
		TaxInvoiceTableItemView GetByIdvw(Guid id);
		TaxInvoiceTable Find(params object[] keyValues);
		TaxInvoiceTable GetTaxInvoiceTableByIdNoTracking(Guid guid);
		TaxInvoiceTable CreateTaxInvoiceTable(TaxInvoiceTable taxInvoiceTable);
		void CreateTaxInvoiceTableVoid(TaxInvoiceTable taxInvoiceTable);
		TaxInvoiceTable UpdateTaxInvoiceTable(TaxInvoiceTable dbTaxInvoiceTable, TaxInvoiceTable inputTaxInvoiceTable, List<string> skipUpdateFields = null);
		void UpdateTaxInvoiceTableVoid(TaxInvoiceTable dbTaxInvoiceTable, TaxInvoiceTable inputTaxInvoiceTable, List<string> skipUpdateFields = null);
		void Remove(TaxInvoiceTable item);
		TaxInvoiceTable GetTaxInvoiceTableByIdNoTrackingByAccessLevel(Guid guid);
		TaxInvoiceTable GetTaxInvoiceTableByTaxInvoiceId(string taxInvoiceId);
		List<TaxInvoiceTable> GetTaxInvoiceTableByIdNoTracking(List<Guid> taxInvoiceTableGUIDs);
	}
	public class TaxInvoiceTableRepo : BranchCompanyBaseRepository<TaxInvoiceTable>, ITaxInvoiceTableRepo
	{
		public TaxInvoiceTableRepo(SmartAppDbContext context) : base(context) { }
		public TaxInvoiceTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public TaxInvoiceTable GetTaxInvoiceTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.TaxInvoiceTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<TaxInvoiceTableItemViewMap> GetDropDownQuery()
		{
			return (from taxInvoiceTable in Entity
					select new TaxInvoiceTableItemViewMap
					{
						CompanyGUID = taxInvoiceTable.CompanyGUID,
						Owner = taxInvoiceTable.Owner,
						OwnerBusinessUnitGUID = taxInvoiceTable.OwnerBusinessUnitGUID,
						TaxInvoiceTableGUID = taxInvoiceTable.TaxInvoiceTableGUID,
						TaxInvoiceId = taxInvoiceTable.TaxInvoiceId,
						IssuedDate = taxInvoiceTable.IssuedDate
					});
		}
		public IEnumerable<SelectItem<TaxInvoiceTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<TaxInvoiceTable>(search, db.GetAvailableBranch(), SysParm.CompanyGUID, SysParm.BranchGUID);
				var taxInvoiceTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<TaxInvoiceTableItemViewMap, TaxInvoiceTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return taxInvoiceTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<TaxInvoiceTableListViewMap> GetListQuery()
		{
			return (from taxInvoiceTable in Entity
					join customerTable in db.Set<CustomerTable>()
					on taxInvoiceTable.CustomerTableGUID equals customerTable.CustomerTableGUID
					join documentStatus in db.Set<DocumentStatus>()
					on taxInvoiceTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus
					from documentStatus in ljdocumentStatus
					select new TaxInvoiceTableListViewMap
				{
						CompanyGUID = taxInvoiceTable.CompanyGUID,
						BranchGUID = taxInvoiceTable.BranchGUID,
						InvoiceTableGUID = taxInvoiceTable.InvoiceTableGUID,
						Owner = taxInvoiceTable.Owner,
						OwnerBusinessUnitGUID = taxInvoiceTable.OwnerBusinessUnitGUID,
						TaxInvoiceTableGUID = taxInvoiceTable.TaxInvoiceTableGUID,
						TaxInvoiceId = taxInvoiceTable.TaxInvoiceId,
						IssuedDate = taxInvoiceTable.IssuedDate,
						DueDate = taxInvoiceTable.DueDate,
						CustomerTableGUID = taxInvoiceTable.CustomerTableGUID,
						CustomerName = taxInvoiceTable.CustomerName,
						InvoiceAmountBeforeTax = taxInvoiceTable.InvoiceAmountBeforeTax,
						TaxAmount = taxInvoiceTable.TaxAmount,
						InvoiceAmount = taxInvoiceTable.InvoiceAmount,
						DocumentStatusGUID = taxInvoiceTable.DocumentStatusGUID,
						DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
						TaxInvoiceRefGUID = taxInvoiceTable.TaxInvoiceRefGUID,
						TaxInvoiceRefType = taxInvoiceTable.TaxInvoiceRefType,
						DocumentStatus_StatusId = documentStatus.StatusId,
						CustomerTable_CustomerId = customerTable.CustomerId
					});
		}
		public SearchResult<TaxInvoiceTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<TaxInvoiceTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<TaxInvoiceTable>(search, db.GetAvailableBranch(), SysParm.CompanyGUID, SysParm.BranchGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<TaxInvoiceTableListViewMap, TaxInvoiceTableListView>();
				result = list.SetSearchResult<TaxInvoiceTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<TaxInvoiceTableItemViewMap> GetItemQuery()
		{
			return (from taxInvoiceTable in Entity
					join company in db.Set<Company>()
					on taxInvoiceTable.CompanyGUID equals company.CompanyGUID
					join branch in db.Set<Branch>()
					on taxInvoiceTable.BranchGUID equals branch.BranchGUID
					join ownerBU in db.Set<BusinessUnit>()
					on taxInvoiceTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljTaxInvoiceTableOwnerBU
					from ownerBU in ljTaxInvoiceTableOwnerBU.DefaultIfEmpty()

					join customerTable in db.Set< CustomerTable >()
					on taxInvoiceTable.CustomerTableGUID equals customerTable.CustomerTableGUID into ljcustomerTable
					from customerTable in ljcustomerTable.DefaultIfEmpty()
					join documentStatus in db.Set<DocumentStatus>()
					on taxInvoiceTable.DocumentStatusGUID  equals documentStatus.DocumentStatusGUID into ljdocumentStatus
					from documentStatus in ljdocumentStatus.DefaultIfEmpty()
					join invoiceTable in db.Set<InvoiceTable>()
					on taxInvoiceTable.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljinvoiceTable
					from invoiceTable in ljinvoiceTable.DefaultIfEmpty()
					join invoiceType in db.Set< InvoiceType >()
					on taxInvoiceTable.InvoiceTypeGUID equals invoiceType.InvoiceTypeGUID into ljinvoiceType
					from invoiceType in ljinvoiceType.DefaultIfEmpty()
					join currency in db.Set< Currency >()
					on taxInvoiceTable.CurrencyGUID equals currency.CurrencyGUID into ljcurrency
					from currency in ljcurrency.DefaultIfEmpty()
					join methodOfPayment in db.Set< MethodOfPayment >()
					on taxInvoiceTable.MethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID into ljmethodOfPayment
					from methodOfPayment in ljmethodOfPayment.DefaultIfEmpty()
					join documentReason in db.Set< DocumentReason >()
					on taxInvoiceTable.CNReasonGUID equals documentReason.DocumentReasonGUID into ljdocumentReason
					from documentReason in ljdocumentReason.DefaultIfEmpty()
					join ledgerDimension1 in db.Set<LedgerDimension>()
					on taxInvoiceTable.Dimension1GUID equals ledgerDimension1.LedgerDimensionGUID into ljledgerDimension1
					from ledgerDimension1 in ljledgerDimension1.DefaultIfEmpty()
					join ledgerDimension2 in db.Set<LedgerDimension>()
					on taxInvoiceTable.Dimension2GUID equals ledgerDimension2.LedgerDimensionGUID into ljledgerDimension2
					from ledgerDimension2 in ljledgerDimension2.DefaultIfEmpty()
					join ledgerDimension3 in db.Set<LedgerDimension>()
					on taxInvoiceTable.Dimension3GUID equals ledgerDimension3.LedgerDimensionGUID into ljledgerDimension3
					from ledgerDimension3 in ljledgerDimension3.DefaultIfEmpty()
					join ledgerDimension4 in db.Set<LedgerDimension>()
					on taxInvoiceTable.Dimension4GUID equals ledgerDimension4.LedgerDimensionGUID into ljledgerDimension4
					from ledgerDimension4 in ljledgerDimension4.DefaultIfEmpty()
					join ledgerDimension5 in db.Set<LedgerDimension>()
					on taxInvoiceTable.Dimension5GUID equals ledgerDimension5.LedgerDimensionGUID into ljledgerDimension5
					from ledgerDimension5 in ljledgerDimension5.DefaultIfEmpty()

					select new TaxInvoiceTableItemViewMap
					{
						CompanyGUID = taxInvoiceTable.CompanyGUID,
						CompanyId = company.CompanyId,
						BranchGUID = taxInvoiceTable.BranchGUID,
						BranchId = branch.BranchId,
						Owner = taxInvoiceTable.Owner,
						OwnerBusinessUnitGUID = taxInvoiceTable.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = taxInvoiceTable.CreatedBy,
						CreatedDateTime = taxInvoiceTable.CreatedDateTime,
						ModifiedBy = taxInvoiceTable.ModifiedBy,
						ModifiedDateTime = taxInvoiceTable.ModifiedDateTime,
						TaxInvoiceTableGUID = taxInvoiceTable.TaxInvoiceTableGUID,
						CurrencyGUID = taxInvoiceTable.CurrencyGUID,
						Currency_Values = SmartAppUtil.GetDropDownLabel(currency.CurrencyId,currency.Name),
						CustomerName = taxInvoiceTable.CustomerName,
						CustomerTableGUID = taxInvoiceTable.CustomerTableGUID,
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId,customerTable.Name),
						Dimension1GUID = taxInvoiceTable.Dimension1GUID,
						Dimension2GUID = taxInvoiceTable.Dimension2GUID,
						Dimension3GUID = taxInvoiceTable.Dimension3GUID,
						Dimension4GUID = taxInvoiceTable.Dimension4GUID,
						Dimension5GUID = taxInvoiceTable.Dimension5GUID,
						Dimension1_Values = SmartAppUtil.GetDropDownLabel(ledgerDimension1.DimensionCode, ledgerDimension1.Description),
						Dimension2_Values = SmartAppUtil.GetDropDownLabel(ledgerDimension2.DimensionCode, ledgerDimension2.Description),
						Dimension3_Values = SmartAppUtil.GetDropDownLabel(ledgerDimension3.DimensionCode, ledgerDimension3.Description),
						Dimension4_Values = SmartAppUtil.GetDropDownLabel(ledgerDimension4.DimensionCode, ledgerDimension4.Description),
						Dimension5_Values = SmartAppUtil.GetDropDownLabel(ledgerDimension5.DimensionCode, ledgerDimension5.Description),
						DocumentId = taxInvoiceTable.DocumentId,
						CNReasonGUID = taxInvoiceTable.CNReasonGUID,
						CNReason_Values = SmartAppUtil.GetDropDownLabel(documentReason.ReasonId,documentReason.Description),
						DocumentStatusGUID = taxInvoiceTable.DocumentStatusGUID,
						DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
						DueDate = taxInvoiceTable.DueDate,
						ExchangeRate = taxInvoiceTable.ExchangeRate,
						InvoiceAddress1 = taxInvoiceTable.InvoiceAddress1,
						InvoiceAddress2 = taxInvoiceTable.InvoiceAddress2,
						InvoiceAddress_Values = string.Concat(taxInvoiceTable.InvoiceAddress1, " ", taxInvoiceTable.InvoiceAddress2),
						InvoiceAmount = taxInvoiceTable.InvoiceAmount,
						InvoiceAmountBeforeTax = taxInvoiceTable.InvoiceAmountBeforeTax,
						InvoiceAmountBeforeTaxMST = taxInvoiceTable.InvoiceAmountBeforeTaxMST,
						InvoiceAmountMST = taxInvoiceTable.InvoiceAmountMST,
						InvoiceTableGUID = taxInvoiceTable.InvoiceTableGUID,
						InvoiceTable_Values = SmartAppUtil.GetDropDownLabel(invoiceTable.InvoiceId,invoiceTable.IssuedDate.DateToString()),
						InvoiceTypeGUID = taxInvoiceTable.InvoiceTypeGUID,
						InvoiceType_Values = SmartAppUtil.GetDropDownLabel(invoiceType.InvoiceTypeId,invoiceType.Description),
						IssuedDate = taxInvoiceTable.IssuedDate,
						MailingInvoiceAddress1 = taxInvoiceTable.MailingInvoiceAddress1,
						MailingInvoiceAddress2 = taxInvoiceTable.MailingInvoiceAddress2,
						MailingInvoiceAddress_Values = string.Concat(taxInvoiceTable.MailingInvoiceAddress1, " ", taxInvoiceTable.MailingInvoiceAddress2),
						MethodOfPaymentGUID = taxInvoiceTable.MethodOfPaymentGUID,
						MethodOfPayment_Values = SmartAppUtil.GetDropDownLabel(methodOfPayment.MethodOfPaymentId,methodOfPayment.Description),
						OrigTaxInvoiceAmount = taxInvoiceTable.OrigTaxInvoiceAmount,
						OrigTaxInvoiceId = taxInvoiceTable.OrigTaxInvoiceId,
						TaxInvoiceRefGUID = taxInvoiceTable.TaxInvoiceRefGUID,
						Ref_Values = SmartAppUtil.GetDropDownLabel(taxInvoiceTable.TaxInvoiceId,taxInvoiceTable.IssuedDate.DateToString()),
						RefTaxInvoiceGUID = taxInvoiceTable.RefTaxInvoiceGUID,
						RefTaxInvoice_Values = SmartAppUtil.GetDropDownLabel(taxInvoiceTable.TaxInvoiceId, taxInvoiceTable.IssuedDate.DateToString()),
						Remark = taxInvoiceTable.Remark,
						TaxAmount = taxInvoiceTable.TaxAmount,
						TaxAmountMST = taxInvoiceTable.TaxAmountMST,
						TaxBranchId = taxInvoiceTable.TaxBranchId,
						TaxBranchName = taxInvoiceTable.TaxBranchName,
						TaxInvoiceId = taxInvoiceTable.TaxInvoiceId,
						TaxInvoiceRefType = taxInvoiceTable.TaxInvoiceRefType,
					
						RowVersion = taxInvoiceTable.RowVersion,
					});
		}
		public TaxInvoiceTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<TaxInvoiceTableItemViewMap, TaxInvoiceTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public TaxInvoiceTable CreateTaxInvoiceTable(TaxInvoiceTable taxInvoiceTable)
		{
			try
			{
				taxInvoiceTable.TaxInvoiceTableGUID = taxInvoiceTable.TaxInvoiceTableGUID == Guid.Empty ? Guid.NewGuid() : taxInvoiceTable.TaxInvoiceTableGUID;
				base.Add(taxInvoiceTable);
				return taxInvoiceTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateTaxInvoiceTableVoid(TaxInvoiceTable taxInvoiceTable)
		{
			try
			{
				CreateTaxInvoiceTable(taxInvoiceTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public TaxInvoiceTable UpdateTaxInvoiceTable(TaxInvoiceTable dbTaxInvoiceTable, TaxInvoiceTable inputTaxInvoiceTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbTaxInvoiceTable = dbTaxInvoiceTable.MapUpdateValues<TaxInvoiceTable>(inputTaxInvoiceTable);
				base.Update(dbTaxInvoiceTable);
				return dbTaxInvoiceTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateTaxInvoiceTableVoid(TaxInvoiceTable dbTaxInvoiceTable, TaxInvoiceTable inputTaxInvoiceTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbTaxInvoiceTable = dbTaxInvoiceTable.MapUpdateValues<TaxInvoiceTable>(inputTaxInvoiceTable, skipUpdateFields);
				base.Update(dbTaxInvoiceTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public TaxInvoiceTable GetTaxInvoiceTableByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.TaxInvoiceTableGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public TaxInvoiceTable GetTaxInvoiceTableByTaxInvoiceId(string taxInvoiceId)
        {
            try
            {
				var result = Entity.Where(item => item.TaxInvoiceId == taxInvoiceId)
				.AsNoTracking()
				.FirstOrDefault();
				return result;
			}
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public List<TaxInvoiceTable> GetTaxInvoiceTableByIdNoTracking(List<Guid> taxInvoiceTableGUIDs)
        {
			try
			{
				List<TaxInvoiceTable> taxInvoiceTables = Entity.Where(w => taxInvoiceTableGUIDs.Contains(w.TaxInvoiceTableGUID)).AsNoTracking().ToList();
				return taxInvoiceTables;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

