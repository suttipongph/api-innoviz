using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IRegistrationTypeRepo
	{
		#region DropDown
		IEnumerable<SelectItem<RegistrationTypeItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<RegistrationTypeListView> GetListvw(SearchParameter search);
		RegistrationTypeItemView GetByIdvw(Guid id);
		RegistrationType Find(params object[] keyValues);
		RegistrationType GetRegistrationTypeByIdNoTracking(Guid guid);
		RegistrationType CreateRegistrationType(RegistrationType registrationType);
		void CreateRegistrationTypeVoid(RegistrationType registrationType);
		RegistrationType UpdateRegistrationType(RegistrationType dbRegistrationType, RegistrationType inputRegistrationType, List<string> skipUpdateFields = null);
		void UpdateRegistrationTypeVoid(RegistrationType dbRegistrationType, RegistrationType inputRegistrationType, List<string> skipUpdateFields = null);
		void Remove(RegistrationType item);
	}
	public class RegistrationTypeRepo : CompanyBaseRepository<RegistrationType>, IRegistrationTypeRepo
	{
		public RegistrationTypeRepo(SmartAppDbContext context) : base(context) { }
		public RegistrationTypeRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public RegistrationType GetRegistrationTypeByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.RegistrationTypeGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<RegistrationTypeItemViewMap> GetDropDownQuery()
		{
			return (from registrationType in Entity
					select new RegistrationTypeItemViewMap
					{
						CompanyGUID = registrationType.CompanyGUID,
						Owner = registrationType.Owner,
						OwnerBusinessUnitGUID = registrationType.OwnerBusinessUnitGUID,
						RegistrationTypeGUID = registrationType.RegistrationTypeGUID,
						RegistrationTypeId = registrationType.RegistrationTypeId,
						Description = registrationType.Description
					});
		}
		public IEnumerable<SelectItem<RegistrationTypeItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<RegistrationType>(search, SysParm.CompanyGUID);
				var registrationType = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<RegistrationTypeItemViewMap, RegistrationTypeItemView>().ToDropDownItem(search.ExcludeRowData);


				return registrationType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<RegistrationTypeListViewMap> GetListQuery()
		{
			return (from registrationType in Entity
				select new RegistrationTypeListViewMap
				{
						CompanyGUID = registrationType.CompanyGUID,
						Owner = registrationType.Owner,
						OwnerBusinessUnitGUID = registrationType.OwnerBusinessUnitGUID,
						RegistrationTypeGUID = registrationType.RegistrationTypeGUID,
						RegistrationTypeId = registrationType.RegistrationTypeId,
						Description = registrationType.Description,
				});
		}
		public SearchResult<RegistrationTypeListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<RegistrationTypeListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<RegistrationType>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<RegistrationTypeListViewMap, RegistrationTypeListView>();
				result = list.SetSearchResult<RegistrationTypeListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<RegistrationTypeItemViewMap> GetItemQuery()
		{
			return (from registrationType in Entity
					join ownerBU in db.Set<BusinessUnit>()
					on registrationType.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljRegistrationTypeOwnerBU
					from ownerBU in ljRegistrationTypeOwnerBU.DefaultIfEmpty()
					join company in db.Set<Company>() on registrationType.CompanyGUID equals company.CompanyGUID
					select new RegistrationTypeItemViewMap
					{
						CompanyGUID = registrationType.CompanyGUID,
						Owner = registrationType.Owner,
						OwnerBusinessUnitGUID = registrationType.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CompanyId = company.CompanyId,
						CreatedBy = registrationType.CreatedBy,
						CreatedDateTime = registrationType.CreatedDateTime,
						ModifiedBy = registrationType.ModifiedBy,
						ModifiedDateTime = registrationType.ModifiedDateTime,
						RegistrationTypeGUID = registrationType.RegistrationTypeGUID,
						Description = registrationType.Description,
						RegistrationTypeId = registrationType.RegistrationTypeId,
					
						RowVersion = registrationType.RowVersion,
					});
		}
		public RegistrationTypeItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<RegistrationTypeItemViewMap, RegistrationTypeItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public RegistrationType CreateRegistrationType(RegistrationType registrationType)
		{
			try
			{
				registrationType.RegistrationTypeGUID = Guid.NewGuid();
				base.Add(registrationType);
				return registrationType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateRegistrationTypeVoid(RegistrationType registrationType)
		{
			try
			{
				CreateRegistrationType(registrationType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public RegistrationType UpdateRegistrationType(RegistrationType dbRegistrationType, RegistrationType inputRegistrationType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbRegistrationType = dbRegistrationType.MapUpdateValues<RegistrationType>(inputRegistrationType);
				base.Update(dbRegistrationType);
				return dbRegistrationType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateRegistrationTypeVoid(RegistrationType dbRegistrationType, RegistrationType inputRegistrationType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbRegistrationType = dbRegistrationType.MapUpdateValues<RegistrationType>(inputRegistrationType, skipUpdateFields);
				base.Update(dbRegistrationType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

