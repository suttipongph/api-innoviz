using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface IEmployeeTableRepo
    {
        #region DropDown
        IEnumerable<SelectItem<EmployeeTableItemView>> GetDropDownItem(SearchParameter search);
        IEnumerable<SelectItem<EmployeeTableItemView>> GetDropDownByInputCompany(SearchParameter search);
        IEnumerable<SelectItem<EmployeeTableItemView>> GetDropDownItemForWF(SearchParameter search);
        IEnumerable<SelectItem<EmployeeTableItemView>> GetDropDownItemWithCompanySignature(SearchParameter search);
        #endregion DropDown
        SearchResult<EmployeeTableListView> GetListvw(SearchParameter search);
        EmployeeTableItemView GetByIdvw(Guid id);
        EmployeeTable Find(params object[] keyValues);
        EmployeeTable GetEmployeeTableByIdNoTracking(Guid guid);
        EmployeeTable CreateEmployeeTable(EmployeeTable employeeTable);
        void CreateEmployeeTableVoid(EmployeeTable employeeTable);
        EmployeeTable UpdateEmployeeTable(EmployeeTable dbEmployeeTable, EmployeeTable inputEmployeeTable, List<string> skipUpdateFields = null);
        void UpdateEmployeeTableVoid(EmployeeTable dbEmployeeTable, EmployeeTable inputEmployeeTable, List<string> skipUpdateFields = null);
        void Remove(EmployeeTable item);
        public EmployeeTableItemView GetEmployeeTableByUserIdAndCompany(string userId, string companyGUID);
        EmployeeTable GetByEmployeeGUIDNoTracking(Guid empTableGuid);
        EmployeeTable UpdateUserMapping(EmployeeTable item);
        IEnumerable<EmployeeTable> UpdateUserMapping(IEnumerable<EmployeeTable> items, string userId);
        IEnumerable<SelectItem<EmployeeTableItemView>> GetActiveEmployeeDropDown(SearchParameter search);
        public EmployeeTable GetEmployeeTableByIdNoTrackingByAccessLevel(Guid guid);
        bool ExistsByEmployeeIdAndCompany(string employeeId, Guid companyGUID);
        List<EmployeeTable> GetMappedEmployeesByUserNoTracking(SysUserTable user);
        List<EmployeeTable> GetMappedEmployeesByUserNoTracking(List<SysUserTable> users, Guid? companyGUID = null);
        List<EmployeeTable> GetMappedEmployeesByUserNoTracking(List<Guid> userGUIDs, Guid? companyGUID = null);
        List<EmployeeTable> GetMappedEmployeesByUserNoTracking(List<string> usernames, Guid? companyGUID = null);
        SysUserTable GetSysUserTableByReportToEmployeeTable(Guid userId, Guid companyGUID);
        void ValidateAdd(EmployeeTable item);
        void ValidateAdd(IEnumerable<EmployeeTable> items);
        EmployeeTableItemView GetByIdIgnoreAccessLevelvw(Guid guid);
    }
    public class EmployeeTableRepo : CompanyBaseRepository<EmployeeTable>, IEmployeeTableRepo
    {
        public EmployeeTableRepo(SmartAppDbContext context) : base(context) { }
        public EmployeeTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public EmployeeTable GetEmployeeTableByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.EmployeeTableGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        private IQueryable<EmployeeTableItemViewMap> GetDropDownQuery()
        {
            return (from employeeTable in Entity
                    select new EmployeeTableItemViewMap
                    {
                        CompanyGUID = employeeTable.CompanyGUID,
                        Owner = employeeTable.Owner,
                        OwnerBusinessUnitGUID = employeeTable.OwnerBusinessUnitGUID,
                        EmployeeTableGUID = employeeTable.EmployeeTableGUID,
                        EmployeeId = employeeTable.EmployeeId,
                        Name = employeeTable.Name,
                        InActive = employeeTable.InActive
                    });
        }
        public IEnumerable<SelectItem<EmployeeTableItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<EmployeeTable>(search, SysParm.CompanyGUID);
                var employeeTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
                    .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                    .Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
                    .ToMaps<EmployeeTableItemViewMap, EmployeeTableItemView>().ToDropDownItem(search.ExcludeRowData);
                return employeeTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<EmployeeTableItemView>> GetDropDownByInputCompany(SearchParameter search)
        {
            try
            {
                var predicate = search.GetSearchPredicate(typeof(EmployeeTableItemViewMap));
                var employeeTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<EmployeeTableItemViewMap, EmployeeTableItemView>().ToDropDownItem(search.ExcludeRowData);


                return employeeTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private IQueryable<EmployeeTableItemViewMap> GetDropDownQueryForWF()
        {
            return (from employeeTable in Entity
                    join sysUserTable in db.Set<SysUserTable>()
                    on employeeTable.UserId equals sysUserTable.Id into ljSysUserTable
                    from sysUserTable in ljSysUserTable.DefaultIfEmpty()
                    where employeeTable.InActive == false && employeeTable.AssistMD == true
                    select new EmployeeTableItemViewMap
                    {
                        CompanyGUID = employeeTable.CompanyGUID,
                        Owner = employeeTable.Owner,
                        OwnerBusinessUnitGUID = employeeTable.OwnerBusinessUnitGUID,
                        EmployeeTableGUID = employeeTable.EmployeeTableGUID,
                        EmployeeId = employeeTable.EmployeeId,
                        Name = employeeTable.Name,
                        SysuserTable_UserName = (sysUserTable != null) ? sysUserTable.UserName: string.Empty,
                        AssistMD = employeeTable.AssistMD,
                        InActive = employeeTable.InActive,
                    });
        }
        private IQueryable<EmployeeTableItemViewMap> GetDropDownQueryWithCompanySignature()
        {
            return (from employeeTable in Entity
                    join companySignature in db.Set<CompanySignature>()
                    on employeeTable.EmployeeTableGUID equals companySignature.EmployeeTableGUID
                    select new EmployeeTableItemViewMap
                    {
                        CompanyGUID = employeeTable.CompanyGUID,
                        Owner = employeeTable.Owner,
                        OwnerBusinessUnitGUID = employeeTable.OwnerBusinessUnitGUID,
                        EmployeeTableGUID = employeeTable.EmployeeTableGUID,
                        EmployeeId = employeeTable.EmployeeId,
                        Name = employeeTable.Name,
                    });
        }
        public IEnumerable<SelectItem<EmployeeTableItemView>> GetDropDownItemForWF(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<EmployeeTable>(search, SysParm.CompanyGUID);
                var employeeTable = GetDropDownQueryForWF().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<EmployeeTableItemViewMap, EmployeeTableItemView>().ToDropDownItem(search.ExcludeRowData);


                return employeeTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public IEnumerable<SelectItem<EmployeeTableItemView>> GetDropDownItemWithCompanySignature(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<EmployeeTable>(search, SysParm.CompanyGUID);
                var employeeTable = GetDropDownQueryWithCompanySignature().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<EmployeeTableItemViewMap, EmployeeTableItemView>().ToDropDownItem(search.ExcludeRowData);


                return employeeTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion DropDown
        #region GetListvw
        private IQueryable<EmployeeTableListViewMap> GetListQuery()
        {
            return (from employeeTable in Entity
                    join sysUserTable in db.Set<SysUserTable>()
                    on employeeTable.UserId equals sysUserTable.Id into ljEmployeeTableSysUserTable
                    from sysUserTable in ljEmployeeTableSysUserTable.DefaultIfEmpty()
                    join emplTeam in db.Set<EmplTeam>()
                    on employeeTable.EmplTeamGUID equals emplTeam.EmplTeamGUID into ljEmployeeTableEmplTeam
                    from emplTeam in ljEmployeeTableEmplTeam.DefaultIfEmpty()
                    join department in db.Set<Department>()
                    on employeeTable.DepartmentGUID equals department.DepartmentGUID into ljEmployeeTableDepartment
                    from department in ljEmployeeTableDepartment.DefaultIfEmpty()
                    join businessUnit in db.Set<BusinessUnit>()
                    on employeeTable.BusinessUnitGUID equals businessUnit.BusinessUnitGUID into ljEmployeeTableBusinessUnit
                    from businessUnit in ljEmployeeTableBusinessUnit.DefaultIfEmpty()
                    select new EmployeeTableListViewMap
                    {
                        CompanyGUID = employeeTable.CompanyGUID,
                        Owner = employeeTable.Owner,
                        OwnerBusinessUnitGUID = employeeTable.OwnerBusinessUnitGUID,
                        EmployeeTableGUID = employeeTable.EmployeeTableGUID,
                        EmployeeId = employeeTable.EmployeeId,
                        Name = employeeTable.Name,
                        DepartmentGUID = employeeTable.DepartmentGUID,
                        Department_Values = SmartAppUtil.GetDropDownLabel(department.DepartmentId, department.Description),
                        BusinessUnit_Values= SmartAppUtil.GetDropDownLabel(businessUnit.BusinessUnitId, businessUnit.Description),
                        InActive = employeeTable.InActive,
                        AssistMD = employeeTable.AssistMD,
                        SysuserTable_UserName = (sysUserTable != null) ? sysUserTable.UserName : null,
                        Department_DepartmentId = department.DepartmentId,
                        EmplTeam_TeamId = (emplTeam != null) ? emplTeam.TeamId : null,
                        Position = employeeTable.Position,
                        BusinessUnit_BusinessUnitId = businessUnit.BusinessUnitId,
                        BusinessUnitGUID = employeeTable.BusinessUnitGUID
                    });
        }
        public SearchResult<EmployeeTableListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<EmployeeTableListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<EmployeeTable>(search, SysParm.CompanyGUID);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<EmployeeTableListViewMap, EmployeeTableListView>();
                result = list.SetSearchResult<EmployeeTableListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<EmployeeTableItemViewMap> GetItemQuery()
        {
            return (from employeeTable in Entity
                    join sysUserTable in db.Set<SysUserTable>()
                    on employeeTable.UserId equals sysUserTable.Id into ljEmployeeTableSysUserTable
                    from sysUserTable in ljEmployeeTableSysUserTable.DefaultIfEmpty()
                    join company in db.Set<Company>()
                    on employeeTable.CompanyGUID equals company.CompanyGUID
                    join ownerBU in db.Set<BusinessUnit>()
                    on employeeTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljEmployeeTableOwnerBU
                    from ownerBU in ljEmployeeTableOwnerBU.DefaultIfEmpty()
                    select new EmployeeTableItemViewMap
                    {
                        CompanyGUID = employeeTable.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = employeeTable.Owner,
                        OwnerBusinessUnitGUID = employeeTable.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        BusinessUnitGUID = employeeTable.BusinessUnitGUID,
                        CreatedBy = employeeTable.CreatedBy,
                        CreatedDateTime = employeeTable.CreatedDateTime,
                        ModifiedBy = employeeTable.ModifiedBy,
                        ModifiedDateTime = employeeTable.ModifiedDateTime,
                        EmployeeTableGUID = employeeTable.EmployeeTableGUID,
                        Dimension1GUID = employeeTable.Dimension1GUID,
                        Dimension2GUID = employeeTable.Dimension2GUID,
                        Dimension3GUID = employeeTable.Dimension3GUID,
                        Dimension4GUID = employeeTable.Dimension4GUID,
                        Dimension5GUID = employeeTable.Dimension5GUID,
                        EmployeeId = employeeTable.EmployeeId,
                        EmplTeamGUID = employeeTable.EmplTeamGUID,
                        DepartmentGUID = employeeTable.DepartmentGUID,
                        InActive = employeeTable.InActive,
                        AssistMD = employeeTable.AssistMD,
                        Name = employeeTable.Name,
                        Signature = employeeTable.Signature,
                        UserId = employeeTable.UserId,
                        SysuserTable_UserName = (sysUserTable != null) ? sysUserTable.UserName : null,
                        ReportToEmployeeTableGUID = employeeTable.ReportToEmployeeTableGUID,
                        Position = employeeTable.Position,
                        
                    
                        RowVersion = employeeTable.RowVersion,
                    });
        }
        public EmployeeTableItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .ToMap<EmployeeTableItemViewMap, EmployeeTableItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        public EmployeeTableItemView GetByIdIgnoreAccessLevelvw(Guid guid)
        {
            try
            {
                var item = GetItemQuery().Where(w => w.EmployeeTableGUID == guid)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .ToMap<EmployeeTableItemViewMap, EmployeeTableItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public EmployeeTable CreateEmployeeTable(EmployeeTable employeeTable)
        {
            try
            {
                employeeTable.EmployeeTableGUID = Guid.NewGuid();
                base.Add(employeeTable);
                return employeeTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateEmployeeTableVoid(EmployeeTable employeeTable)
        {
            try
            {
                CreateEmployeeTable(employeeTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public EmployeeTable UpdateEmployeeTable(EmployeeTable dbEmployeeTable, EmployeeTable inputEmployeeTable, List<string> skipUpdateFields = null)
        {
            try
            {
                dbEmployeeTable = dbEmployeeTable.MapUpdateValues<EmployeeTable>(inputEmployeeTable);
                base.Update(dbEmployeeTable);
                return dbEmployeeTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateEmployeeTableVoid(EmployeeTable dbEmployeeTable, EmployeeTable inputEmployeeTable, List<string> skipUpdateFields = null)
        {
            try
            {
                dbEmployeeTable = dbEmployeeTable.MapUpdateValues<EmployeeTable>(inputEmployeeTable, skipUpdateFields);
                base.Update(dbEmployeeTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Create, Update, Delete

        public EmployeeTableItemView GetEmployeeTableByUserIdAndCompany(string userId, string companyGUID)
        {
            try
            {
                Guid userGuid = new Guid(userId);
                Guid companyGuid = new Guid(companyGUID);
                var result = GetItemQuery()
                            .Where(item => item.UserId == userGuid &&
                                         item.CompanyGUID == companyGuid)
                            .FirstOrDefault()
                            .ToMap<EmployeeTableItemViewMap, EmployeeTableItemView>();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public EmployeeTable UpdateUserMapping(EmployeeTable item)
        {
            try
            {

                item.ModifiedBy = db.GetUserName();
                item.ModifiedDateTime = DateTime.Now;

                string[] fieldsToUpdate = new string[] { "UserId", "ModifiedBy", "ModifiedDateTime" };
                var dbEntityEntry = db.Entry(item);
                db.Attach(item);
                foreach (string prop in fieldsToUpdate)
                {
                    dbEntityEntry.Property(prop).IsModified = true;
                }

                return item;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<EmployeeTableItemView>> GetActiveEmployeeDropDown(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<EmployeeTable>(search, SysParm.CompanyGUID);
                var employeeTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
                    .ToMaps<EmployeeTableItemViewMap, EmployeeTableItemView>().ToDropDownItem(search.ExcludeRowData);
                return employeeTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public EmployeeTable GetByEmployeeGUIDNoTracking(Guid empTableGuid)
        {
            try
            {
                return Entity.Where(w => w.EmployeeTableGUID == empTableGuid)
                            .AsNoTracking()
                            .FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public IEnumerable<EmployeeTable> UpdateUserMapping(IEnumerable<EmployeeTable> newItems, string userId)
        {
            try
            {
                var employeesToUpdate = new List<EmployeeTable>();
                Guid? userToMap = new Guid(userId);
                var oldList = Entity.Where(item => item.UserId == userToMap).ToList();

                // unmap existing values, map new values, 
                // do nothing to unchanged values
                if (oldList != null && newItems != null)
                {
                    var oldItemsToUpdate = oldList.Where(e => !newItems.Any(e2 => e2.EmployeeTableGUID == e.EmployeeTableGUID));

                    if (oldItemsToUpdate.Count() != 0)
                    {
                        foreach (var item in oldItemsToUpdate)
                        {
                            item.UserId = null;
                            UpdateUserMapping(item);
                        }
                    }

                    var newItemsToUpdate = newItems.Where(e => !oldList.Any(e2 => e2.EmployeeTableGUID == e.EmployeeTableGUID));
                    if (newItemsToUpdate.Count() != 0)
                    {
                        foreach (var item in newItemsToUpdate)
                        {
                            item.UserId = userToMap;
                            UpdateUserMapping(item);
                        }
                    }
                }
                else
                {
                    if (oldList != null && oldList.Count() != 0)
                    {
                        foreach (var item in oldList)
                        {
                            item.UserId = null;
                            UpdateUserMapping(item);
                        }
                    }
                    else if (newItems != null && newItems.Count() != 0)
                    {
                        foreach (var item in newItems)
                        {
                            item.UserId = userToMap;
                            UpdateUserMapping(item);
                        }
                    }

                }
                return newItems;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public EmployeeTable GetEmployeeTableByIdNoTrackingByAccessLevel(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.EmployeeTableGUID == guid)
                                    .FilterByAccessLevel(SysParm.AccessLevel)
                                    .AsNoTracking()
                                    .FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool ExistsByEmployeeIdAndCompany(string employeeId, Guid companyGUID)
        {
            try
            {
                var result = Entity.Where(item => item.EmployeeId == employeeId &&
                                                    item.CompanyGUID == companyGUID)
                                    .AsNoTracking()
                                    .Count() > 0;
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<EmployeeTable> GetMappedEmployeesByUserNoTracking(SysUserTable user)
        {
            try
            {
                if(user == null)
                {
                    return null;
                }
                List<SysUserTable> userParam  = new List<SysUserTable>() { user };
                return GetMappedEmployeesByUserNoTracking(userParam);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<EmployeeTable> GetMappedEmployeesByUserNoTracking(List<SysUserTable> users, Guid? companyGUID = null)
        {
            try
            {
                if(users == null)
                {
                    return null;
                }
                List<Guid> userParam = users.Select(item => item.Id).ToList();
                return GetMappedEmployeesByUserNoTracking(userParam, companyGUID);
                
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<EmployeeTable> GetMappedEmployeesByUserNoTracking(List<Guid> userGUIDs, Guid? companyGUID = null)
        {
            try
            {
                if(userGUIDs == null)
                {
                    return null;
                }
                else
                {
                    var employeeTableQuery = Entity.Where(item => item.UserId.HasValue &&
                                                                  userGUIDs.Contains(item.UserId.Value));
                    if(companyGUID != null)
                    {
                        employeeTableQuery = employeeTableQuery.Where(item => item.CompanyGUID == companyGUID);
                    }
                    return employeeTableQuery.AsNoTracking().ToList();
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<EmployeeTable> GetMappedEmployeesByUserNoTracking(List<string> usernames, Guid? companyGUID = null)
        {
            try
            {
                if (usernames == null)
                {
                    return null;
                }
                else
                {
                    var employeeTableQuery =
                        (from employee in Entity.Where(w => w.UserId.HasValue)
                         join user in db.Set<SysUserTable>().Where(w => usernames.Contains(w.UserName))
                         on employee.UserId equals user.Id
                         select employee);

                    if (companyGUID != null)
                    {
                        employeeTableQuery = employeeTableQuery.Where(item => item.CompanyGUID == companyGUID);
                    }
                    return employeeTableQuery.AsNoTracking().ToList();
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SysUserTable GetSysUserTableByReportToEmployeeTable(Guid userId, Guid companyGUID)
        {
            try
            {
                var result = (from employeeTable in Entity
                              join reportToEmployeeTable in Entity
                              on employeeTable.ReportToEmployeeTableGUID equals reportToEmployeeTable.EmployeeTableGUID
                              join sysUserTable in db.Set<SysUserTable>()
                              on reportToEmployeeTable.UserId equals sysUserTable.Id
                              where employeeTable.UserId == userId && employeeTable.CompanyGUID == companyGUID
                              select sysUserTable).FirstOrDefault();
                              
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}

