using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IProjectProgressTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<ProjectProgressTableItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<ProjectProgressTableListView> GetListvw(SearchParameter search);
		ProjectProgressTableItemView GetByIdvw(Guid id);
		ProjectProgressTable Find(params object[] keyValues);
		ProjectProgressTable GetProjectProgressTableByIdNoTracking(Guid guid);
		ProjectProgressTable CreateProjectProgressTable(ProjectProgressTable projectProgressTable);
		void CreateProjectProgressTableVoid(ProjectProgressTable projectProgressTable);
		ProjectProgressTable UpdateProjectProgressTable(ProjectProgressTable dbProjectProgressTable, ProjectProgressTable inputProjectProgressTable, List<string> skipUpdateFields = null);
		void UpdateProjectProgressTableVoid(ProjectProgressTable dbProjectProgressTable, ProjectProgressTable inputProjectProgressTable, List<string> skipUpdateFields = null);
		void Remove(ProjectProgressTable item);
		ProjectProgressTable GetProjectProgressTableByIdNoTrackingByAccessLevel(Guid guid);
		void ValidateAdd(ProjectProgressTable item);
		void ValidateAdd(IEnumerable<ProjectProgressTable> items);
	}
	public class ProjectProgressTableRepo : CompanyBaseRepository<ProjectProgressTable>, IProjectProgressTableRepo
	{
		public ProjectProgressTableRepo(SmartAppDbContext context) : base(context) { }
		public ProjectProgressTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ProjectProgressTable GetProjectProgressTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ProjectProgressTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<ProjectProgressTableItemViewMap> GetDropDownQuery()
		{
			return (from projectProgressTable in Entity
					select new ProjectProgressTableItemViewMap
					{
						CompanyGUID = projectProgressTable.CompanyGUID,
						Owner = projectProgressTable.Owner,
						OwnerBusinessUnitGUID = projectProgressTable.OwnerBusinessUnitGUID,
						ProjectProgressTableGUID = projectProgressTable.ProjectProgressTableGUID,
						ProjectProgressId = projectProgressTable.ProjectProgressId,
						Description = projectProgressTable.Description
					});
		}
		public IEnumerable<SelectItem<ProjectProgressTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<ProjectProgressTable>(search, SysParm.CompanyGUID);
				var projectProgressTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<ProjectProgressTableItemViewMap, ProjectProgressTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return projectProgressTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<ProjectProgressTableListViewMap> GetListQuery()
		{
			return (from projectProgressTable in Entity
				select new ProjectProgressTableListViewMap
				{
						CompanyGUID = projectProgressTable.CompanyGUID,
						Owner = projectProgressTable.Owner,
						OwnerBusinessUnitGUID = projectProgressTable.OwnerBusinessUnitGUID,
						ProjectProgressTableGUID = projectProgressTable.ProjectProgressTableGUID,
						ProjectProgressId = projectProgressTable.ProjectProgressId,
						TransDate = projectProgressTable.TransDate,
						Description = projectProgressTable.Description,
					WithdrawalTableGUID = projectProgressTable.WithdrawalTableGUID
				});
		}
		public SearchResult<ProjectProgressTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<ProjectProgressTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<ProjectProgressTable>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<ProjectProgressTableListViewMap, ProjectProgressTableListView>();
				result = list.SetSearchResult<ProjectProgressTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<ProjectProgressTableItemViewMap> GetItemQuery()
		{
			return (from projectProgressTable in Entity
					join company in db.Set<Company>()
					on projectProgressTable.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on projectProgressTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljProjectProgressTableOwnerBU
					from ownerBU in ljProjectProgressTableOwnerBU.DefaultIfEmpty()

					join withdrawal_table in db.Set<WithdrawalTable>()
					on projectProgressTable.WithdrawalTableGUID equals withdrawal_table.WithdrawalTableGUID into ljwithdrawal
					from withdrawal_table in ljwithdrawal.DefaultIfEmpty()
					select new ProjectProgressTableItemViewMap
					{
						CompanyGUID = projectProgressTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = projectProgressTable.Owner,
						OwnerBusinessUnitGUID = projectProgressTable.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = projectProgressTable.CreatedBy,
						CreatedDateTime = projectProgressTable.CreatedDateTime,
						ModifiedBy = projectProgressTable.ModifiedBy,
						ModifiedDateTime = projectProgressTable.ModifiedDateTime,
						ProjectProgressTableGUID = projectProgressTable.ProjectProgressTableGUID,
						Description = projectProgressTable.Description,
						ProjectProgressId = projectProgressTable.ProjectProgressId,
						Remark = projectProgressTable.Remark,
						TransDate = projectProgressTable.TransDate,
						WithdrawalTableGUID = projectProgressTable.WithdrawalTableGUID,
						Withdrawal_Value =SmartAppUtil.GetDropDownLabel(withdrawal_table.WithdrawalId,withdrawal_table.Description),
					
						RowVersion = projectProgressTable.RowVersion,
					});
		}
		public ProjectProgressTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<ProjectProgressTableItemViewMap, ProjectProgressTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public ProjectProgressTable CreateProjectProgressTable(ProjectProgressTable projectProgressTable)
		{
			try
			{
				projectProgressTable.ProjectProgressTableGUID = Guid.NewGuid();
				base.Add(projectProgressTable);
				return projectProgressTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateProjectProgressTableVoid(ProjectProgressTable projectProgressTable)
		{
			try
			{
				CreateProjectProgressTable(projectProgressTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ProjectProgressTable UpdateProjectProgressTable(ProjectProgressTable dbProjectProgressTable, ProjectProgressTable inputProjectProgressTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbProjectProgressTable = dbProjectProgressTable.MapUpdateValues<ProjectProgressTable>(inputProjectProgressTable);
				base.Update(dbProjectProgressTable);
				return dbProjectProgressTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateProjectProgressTableVoid(ProjectProgressTable dbProjectProgressTable, ProjectProgressTable inputProjectProgressTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbProjectProgressTable = dbProjectProgressTable.MapUpdateValues<ProjectProgressTable>(inputProjectProgressTable, skipUpdateFields);
				base.Update(dbProjectProgressTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public ProjectProgressTable GetProjectProgressTableByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ProjectProgressTableGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

