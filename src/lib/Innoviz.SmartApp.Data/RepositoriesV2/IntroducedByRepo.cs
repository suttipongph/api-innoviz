using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IIntroducedByRepo
	{
		#region DropDown
		IEnumerable<SelectItem<IntroducedByItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<IntroducedByListView> GetListvw(SearchParameter search);
		IntroducedByItemView GetByIdvw(Guid id);
		IntroducedBy Find(params object[] keyValues);
		IntroducedBy GetIntroducedByByIdNoTracking(Guid guid);
		IntroducedBy CreateIntroducedBy(IntroducedBy introducedBy);
		void CreateIntroducedByVoid(IntroducedBy introducedBy);
		IntroducedBy UpdateIntroducedBy(IntroducedBy dbIntroducedBy, IntroducedBy inputIntroducedBy, List<string> skipUpdateFields = null);
		void UpdateIntroducedByVoid(IntroducedBy dbIntroducedBy, IntroducedBy inputIntroducedBy, List<string> skipUpdateFields = null);
		void Remove(IntroducedBy item);
	}
	public class IntroducedByRepo : CompanyBaseRepository<IntroducedBy>, IIntroducedByRepo
	{
		public IntroducedByRepo(SmartAppDbContext context) : base(context) { }
		public IntroducedByRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public IntroducedBy GetIntroducedByByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.IntroducedByGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<IntroducedByItemViewMap> GetDropDownQuery()
		{
			return (from introducedBy in Entity
					select new IntroducedByItemViewMap
					{
						CompanyGUID = introducedBy.CompanyGUID,
						Owner = introducedBy.Owner,
						OwnerBusinessUnitGUID = introducedBy.OwnerBusinessUnitGUID,
						IntroducedByGUID = introducedBy.IntroducedByGUID,
						IntroducedById = introducedBy.IntroducedById,
						Description = introducedBy.Description
					});
		}
		public IEnumerable<SelectItem<IntroducedByItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<IntroducedBy>(search, SysParm.CompanyGUID);
				var introducedBy = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<IntroducedByItemViewMap, IntroducedByItemView>().ToDropDownItem(search.ExcludeRowData);


				return introducedBy;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<IntroducedByListViewMap> GetListQuery()
		{
			return (from introducedBy in Entity
					join venDor in db.Set<VendorTable>()
					on introducedBy.VendorTableGUID equals venDor.VendorTableGUID into ljIntroducedByvenDor
					from venDor in ljIntroducedByvenDor.DefaultIfEmpty()
					select new IntroducedByListViewMap
				{
						CompanyGUID = introducedBy.CompanyGUID,
						Owner = introducedBy.Owner,
						OwnerBusinessUnitGUID = introducedBy.OwnerBusinessUnitGUID,
						IntroducedByGUID = introducedBy.IntroducedByGUID,
						IntroducedById = introducedBy.IntroducedById,
						Description = introducedBy.Description,
						VendorTableGUID = introducedBy.VendorTableGUID,
						VendorTable_VendorId = venDor.VendorId,
						VendorTable_Name = venDor.Name,
						VendorTable_Values = SmartAppUtil.GetDropDownLabel(venDor.VendorId, venDor.Name)
				});
		}
		public SearchResult<IntroducedByListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<IntroducedByListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<IntroducedBy>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<IntroducedByListViewMap, IntroducedByListView>();
				result = list.SetSearchResult<IntroducedByListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<IntroducedByItemViewMap> GetItemQuery()
		{
			return (from introducedBy in Entity
					join company in db.Set<Company>()
					on introducedBy.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on introducedBy.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljIntroducedByOwnerBU
					from ownerBU in ljIntroducedByOwnerBU.DefaultIfEmpty()
					select new IntroducedByItemViewMap
					{
						CompanyGUID = introducedBy.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = introducedBy.Owner,
						OwnerBusinessUnitGUID = introducedBy.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = introducedBy.CreatedBy,
						CreatedDateTime = introducedBy.CreatedDateTime,
						ModifiedBy = introducedBy.ModifiedBy,
						ModifiedDateTime = introducedBy.ModifiedDateTime,
						IntroducedByGUID = introducedBy.IntroducedByGUID,
						Description = introducedBy.Description,
						IntroducedById = introducedBy.IntroducedById,
						VendorTableGUID = introducedBy.VendorTableGUID,
					
						RowVersion = introducedBy.RowVersion,
					});
		}
		public IntroducedByItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<IntroducedByItemViewMap, IntroducedByItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public IntroducedBy CreateIntroducedBy(IntroducedBy introducedBy)
		{
			try
			{
				introducedBy.IntroducedByGUID = Guid.NewGuid();
				base.Add(introducedBy);
				return introducedBy;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateIntroducedByVoid(IntroducedBy introducedBy)
		{
			try
			{
				CreateIntroducedBy(introducedBy);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IntroducedBy UpdateIntroducedBy(IntroducedBy dbIntroducedBy, IntroducedBy inputIntroducedBy, List<string> skipUpdateFields = null)
		{
			try
			{
				dbIntroducedBy = dbIntroducedBy.MapUpdateValues<IntroducedBy>(inputIntroducedBy);
				base.Update(dbIntroducedBy);
				return dbIntroducedBy;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateIntroducedByVoid(IntroducedBy dbIntroducedBy, IntroducedBy inputIntroducedBy, List<string> skipUpdateFields = null)
		{
			try
			{
				dbIntroducedBy = dbIntroducedBy.MapUpdateValues<IntroducedBy>(inputIntroducedBy, skipUpdateFields);
				base.Update(dbIntroducedBy);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

