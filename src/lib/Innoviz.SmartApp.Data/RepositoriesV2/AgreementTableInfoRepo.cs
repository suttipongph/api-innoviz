using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IAgreementTableInfoRepo
	{
		#region DropDown
		IEnumerable<SelectItem<AgreementTableInfoItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<AgreementTableInfoListView> GetListvw(SearchParameter search);
		AgreementTableInfoItemView GetByIdvw(Guid id);
		AgreementTableInfo Find(params object[] keyValues);
		AgreementTableInfo GetAgreementTableInfoByIdNoTracking(Guid guid);
		AgreementTableInfo CreateAgreementTableInfo(AgreementTableInfo agreementTableInfo);
		void CreateAgreementTableInfoVoid(AgreementTableInfo agreementTableInfo);
		AgreementTableInfo UpdateAgreementTableInfo(AgreementTableInfo dbAgreementTableInfo, AgreementTableInfo inputAgreementTableInfo, List<string> skipUpdateFields = null);
		void UpdateAgreementTableInfoVoid(AgreementTableInfo dbAgreementTableInfo, AgreementTableInfo inputAgreementTableInfo, List<string> skipUpdateFields = null);
		void Remove(AgreementTableInfo item);
		AgreementTableInfo GetAgreementTableInfoByIdNoTrackingByAccessLevel(Guid guid);
		AgreementTableInfo GetAgreementTableInfoByReferenceTableNoTracking(Guid refGUID, int refType);
		void ValidateAdd(IEnumerable<AgreementTableInfo> items);
	}
	public class AgreementTableInfoRepo : CompanyBaseRepository<AgreementTableInfo>, IAgreementTableInfoRepo
	{
		public AgreementTableInfoRepo(SmartAppDbContext context) : base(context) { }
		public AgreementTableInfoRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public AgreementTableInfo GetAgreementTableInfoByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.AgreementTableInfoGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<AgreementTableInfoItemViewMap> GetDropDownQuery()
		{
			return (from agreementTableInfo in Entity
					select new AgreementTableInfoItemViewMap
					{
						CompanyGUID = agreementTableInfo.CompanyGUID,
						Owner = agreementTableInfo.Owner,
						OwnerBusinessUnitGUID = agreementTableInfo.OwnerBusinessUnitGUID,
						AgreementTableInfoGUID = agreementTableInfo.AgreementTableInfoGUID
					});
		}
		public IEnumerable<SelectItem<AgreementTableInfoItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<AgreementTableInfo>(search, SysParm.CompanyGUID);
				var agreementTableInfo = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<AgreementTableInfoItemViewMap, AgreementTableInfoItemView>().ToDropDownItem(search.ExcludeRowData);


				return agreementTableInfo;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<AgreementTableInfoListViewMap> GetListQuery()
		{
			return (from agreementTableInfo in Entity
				select new AgreementTableInfoListViewMap
				{
						CompanyGUID = agreementTableInfo.CompanyGUID,
						Owner = agreementTableInfo.Owner,
						OwnerBusinessUnitGUID = agreementTableInfo.OwnerBusinessUnitGUID,
						AgreementTableInfoGUID = agreementTableInfo.AgreementTableInfoGUID,
				});
		}
		public SearchResult<AgreementTableInfoListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<AgreementTableInfoListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<AgreementTableInfo>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<AgreementTableInfoListViewMap, AgreementTableInfoListView>();
				result = list.SetSearchResult<AgreementTableInfoListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<AgreementTableInfoItemViewMap> GetItemQuery()
		{
			return (from agreementTableInfo in Entity

					join company in db.Set<Company>()
					on agreementTableInfo.CompanyGUID equals company.CompanyGUID

					join ownerBU in db.Set<BusinessUnit>()
					on agreementTableInfo.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljAgreementTableInfoOwnerBU
					from ownerBU in ljAgreementTableInfoOwnerBU.DefaultIfEmpty()

					join mainAgreementTable in db.Set<MainAgreementTable>()
					on agreementTableInfo.RefGUID equals mainAgreementTable.MainAgreementTableGUID into ljMainAgreementTable
					from mainAgreementTable in ljMainAgreementTable.DefaultIfEmpty()

					join assignmentAgreementTable in db.Set<AssignmentAgreementTable>()
					on agreementTableInfo.RefGUID equals assignmentAgreementTable.AssignmentAgreementTableGUID into ljAssignmentAgreementTable
					from assignmentAgreementTable in ljAssignmentAgreementTable.DefaultIfEmpty()


					join businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
					on agreementTableInfo.RefGUID equals businessCollateralAgmTable.BusinessCollateralAgmTableGUID into lBusinessCollateralAgmTable
					from businessCollateralAgmTable in lBusinessCollateralAgmTable.DefaultIfEmpty()

					join agreementTableInfoText in db.Set<AgreementTableInfoText>()
					on agreementTableInfo.AgreementTableInfoGUID equals agreementTableInfoText.AgreementTableInfoGUID into lAgreementTableInfoText
					from agreementTableInfoText in lAgreementTableInfoText.DefaultIfEmpty()

					select new AgreementTableInfoItemViewMap
					{
						CompanyGUID = agreementTableInfo.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = agreementTableInfo.Owner,
						OwnerBusinessUnitGUID = agreementTableInfo.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = agreementTableInfo.CreatedBy,
						CreatedDateTime = agreementTableInfo.CreatedDateTime,
						ModifiedBy = agreementTableInfo.ModifiedBy,
						ModifiedDateTime = agreementTableInfo.ModifiedDateTime,
						AgreementTableInfoGUID = agreementTableInfo.AgreementTableInfoGUID,
						AuthorizedPersonTransBuyer1GUID = agreementTableInfo.AuthorizedPersonTransBuyer1GUID,
						AuthorizedPersonTransBuyer2GUID = agreementTableInfo.AuthorizedPersonTransBuyer2GUID,
						AuthorizedPersonTransBuyer3GUID = agreementTableInfo.AuthorizedPersonTransBuyer3GUID,
						AuthorizedPersonTransCompany1GUID = agreementTableInfo.AuthorizedPersonTransCompany1GUID,
						AuthorizedPersonTransCompany2GUID = agreementTableInfo.AuthorizedPersonTransCompany2GUID,
						AuthorizedPersonTransCompany3GUID = agreementTableInfo.AuthorizedPersonTransCompany3GUID,
						AuthorizedPersonTransCustomer1GUID = agreementTableInfo.AuthorizedPersonTransCustomer1GUID,
						AuthorizedPersonTransCustomer2GUID = agreementTableInfo.AuthorizedPersonTransCustomer2GUID,
						AuthorizedPersonTransCustomer3GUID = agreementTableInfo.AuthorizedPersonTransCustomer3GUID,
						AuthorizedPersonTypeBuyerGUID = agreementTableInfo.AuthorizedPersonTypeBuyerGUID,
						AuthorizedPersonTypeCompanyGUID = agreementTableInfo.AuthorizedPersonTypeCompanyGUID,
						AuthorizedPersonTypeCustomerGUID = agreementTableInfo.AuthorizedPersonTypeCustomerGUID,
						BuyerAddress1 = agreementTableInfo.BuyerAddress1,
						BuyerAddress2 = agreementTableInfo.BuyerAddress2,
						CompanyAddress1 = agreementTableInfo.CompanyAddress1,
						CompanyAddress2 = agreementTableInfo.CompanyAddress2,
						CompanyAltName = agreementTableInfo.CompanyAltName,
						CompanyBankGUID = agreementTableInfo.CompanyBankGUID,
						CompanyFax = agreementTableInfo.CompanyFax,
						CompanyName = agreementTableInfo.CompanyName,
						CompanyPhone = agreementTableInfo.CompanyPhone,
						CustomerAddress1 = agreementTableInfo.CustomerAddress1,
						CustomerAddress2 = agreementTableInfo.CustomerAddress2,
						GuarantorName = agreementTableInfo.GuarantorName,
						GuaranteeText =agreementTableInfo.GuaranteeText,
						RefGUID = agreementTableInfo.RefGUID,
						RefType = agreementTableInfo.RefType,
						AgreementTableInfoText_Text1 = (agreementTableInfoText != null) ? agreementTableInfoText.Text1 : null,
						AgreementTableInfoText_Text2 = (agreementTableInfoText != null) ? agreementTableInfoText.Text2 : null,
						AgreementTableInfoText_Text3 = (agreementTableInfoText != null) ? agreementTableInfoText.Text3 : null,
						AgreementTableInfoText_Text4 = (agreementTableInfoText != null) ? agreementTableInfoText.Text4 : null,
						WitnessCompany1GUID = agreementTableInfo.WitnessCompany1GUID,
						WitnessCompany2GUID = agreementTableInfo.WitnessCompany2GUID,
						WitnessCustomer1 = agreementTableInfo.WitnessCustomer1,
						WitnessCustomer2 = agreementTableInfo.WitnessCustomer2,
						Company_DefaultBranchGUID = company.DefaultBranchGUID,
						UnboundBuyerAddress = agreementTableInfo.BuyerAddress1 + " " + agreementTableInfo.BuyerAddress2,
						UnboundCompanyAddress = agreementTableInfo.CompanyAddress1 + " " + agreementTableInfo.CompanyAddress2,
						UnboundCustomerAddress = agreementTableInfo.CustomerAddress1 + " " + agreementTableInfo.CustomerAddress2,
						RefId = (mainAgreementTable != null && agreementTableInfo.RefType == (int)RefType.MainAgreement) ? mainAgreementTable.InternalMainAgreementId :
								(assignmentAgreementTable != null && agreementTableInfo.RefType == (int)RefType.AssignmentAgreement) ? assignmentAgreementTable.InternalAssignmentAgreementId:
								(businessCollateralAgmTable != null && agreementTableInfo.RefType == (int)RefType.BusinessCollateralAgreement) ? businessCollateralAgmTable.InternalBusinessCollateralAgmId :
								null,
						ParentTable_ProductType = (mainAgreementTable != null) ? mainAgreementTable.ProductType :
						(assignmentAgreementTable != null) ? (int)ProductType.None:
						(businessCollateralAgmTable != null) ? businessCollateralAgmTable.ProductType :
						0,
						ParentTable_CustomerTableGUID = (mainAgreementTable != null) ? (Guid?)mainAgreementTable.CustomerTableGUID : 
						(assignmentAgreementTable != null) ? (Guid?)assignmentAgreementTable.CustomerTableGUID:
						(businessCollateralAgmTable != null) ? (Guid?)businessCollateralAgmTable.CustomerTableGUID :
						null,
						ParentTable_CreditAppTableGUID = (mainAgreementTable != null) ? (Guid?)mainAgreementTable.CreditAppTableGUID :
						(businessCollateralAgmTable != null) ? (Guid?)businessCollateralAgmTable.CreditAppTableGUID :
						null,
						ParentTable_BuyerTableGUID = (mainAgreementTable != null) ? (Guid?)mainAgreementTable.BuyerTableGUID :
						(assignmentAgreementTable != null) ? (Guid?)assignmentAgreementTable.BuyerTableGUID :
						null,
					
						RowVersion = agreementTableInfo.RowVersion,
					});
		}
		public AgreementTableInfoItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<AgreementTableInfoItemViewMap, AgreementTableInfoItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public AgreementTableInfo CreateAgreementTableInfo(AgreementTableInfo agreementTableInfo)
		{
			try
			{
				agreementTableInfo.AgreementTableInfoGUID = agreementTableInfo.AgreementTableInfoGUID == Guid.Empty ? Guid.NewGuid(): agreementTableInfo.AgreementTableInfoGUID;
				base.Add(agreementTableInfo);
				return agreementTableInfo;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateAgreementTableInfoVoid(AgreementTableInfo agreementTableInfo)
		{
			try
			{
				CreateAgreementTableInfo(agreementTableInfo);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AgreementTableInfo UpdateAgreementTableInfo(AgreementTableInfo dbAgreementTableInfo, AgreementTableInfo inputAgreementTableInfo, List<string> skipUpdateFields = null)
		{
			try
			{
				dbAgreementTableInfo = dbAgreementTableInfo.MapUpdateValues<AgreementTableInfo>(inputAgreementTableInfo);
				base.Update(dbAgreementTableInfo);
				return dbAgreementTableInfo;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateAgreementTableInfoVoid(AgreementTableInfo dbAgreementTableInfo, AgreementTableInfo inputAgreementTableInfo, List<string> skipUpdateFields = null)
		{
			try
			{
				dbAgreementTableInfo = dbAgreementTableInfo.MapUpdateValues<AgreementTableInfo>(inputAgreementTableInfo, skipUpdateFields);
				base.Update(dbAgreementTableInfo);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public AgreementTableInfo GetAgreementTableInfoByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.AgreementTableInfoGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AgreementTableInfo GetAgreementTableInfoByReferenceTableNoTracking(Guid refGUID, int refType)
		{
			try
			{
				var result = Entity.Where(item => item.RefGUID == refGUID && item.RefType == refType).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #region Validate
        public override void ValidateAdd(IEnumerable<AgreementTableInfo> items)
        {
            base.ValidateAdd(items);
        }
        #endregion Validate
    }
}

