using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICompanyRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CompanyItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<CompanyListView> GetListvw(SearchParameter search);
		CompanyItemView GetByIdvw(Guid id);
		Company Find(params object[] keyValues);
		Company GetCompanyByIdNoTracking(Guid guid);
		Company CreateCompany(Company company);
		void CreateCompanyVoid(Company company);
		Company UpdateCompany(Company dbCompany, Company inputCompany, List<string> skipUpdateFields = null);
		void UpdateCompanyVoid(Company dbCompany, Company inputCompany, List<string> skipUpdateFields = null);
		void Remove(Company item);
		bool HasAnyCompany();
		List<Company> GetListCompanyNoTracking();
		Company GetCompanyByCompanyIdNoTracking(string companyId);
	}
	public class CompanyRepo : CompanyBaseRepository<Company>, ICompanyRepo
	{
		public CompanyRepo(SmartAppDbContext context) : base(context) { }
		public CompanyRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public Company GetCompanyByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CompanyGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CompanyItemViewMap> GetDropDownQuery()
		{
			return (from company in Entity
					select new CompanyItemViewMap
					{
						CompanyId = company.CompanyId,
						Name = company.Name,
						CompanyGUID = company.CompanyGUID,
					});
		}
		public IEnumerable<SelectItem<CompanyItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<Company>(search);
				var company = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CompanyItemViewMap, CompanyItemView>().ToDropDownItem(search.ExcludeRowData);


				return company;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CompanyListViewMap> GetListQuery()
		{
			return (from company in Entity
					join branch in db.Set<Branch>()
					on company.DefaultBranchGUID equals branch.BranchGUID into ljBranch
					from branch in ljBranch.DefaultIfEmpty()
					select new CompanyListViewMap
					{
						CompanyGUID = company.CompanyGUID,
						CompanyId = company.CompanyId,
						Name = company.Name,
						SecondName = company.SecondName,
						TaxId = company.CompanyId,
						DefaultBranchGUID = company.DefaultBranchGUID,
						AltName = company.AltName,
						Branch_BranchId = (branch != null) ? branch.BranchId : null,
						Branch_Values = (branch != null) ? SmartAppUtil.GetDropDownLabel(branch.BranchId, branch.Name) : null,
					});
		}
		public SearchResult<CompanyListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CompanyListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<Company>(search);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CompanyListViewMap, CompanyListView>();
				result = list.SetSearchResult<CompanyListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CompanyItemViewMap> GetItemQuery()
		{
			return (from company in Entity
					select new CompanyItemViewMap
					{
						CreatedBy = company.CreatedBy,
						CreatedDateTime = company.CreatedDateTime,
						ModifiedBy = company.ModifiedBy,
						ModifiedDateTime = company.ModifiedDateTime,
						CompanyGUID = company.CompanyGUID,
						CompanyId = company.CompanyId,
						CompanyLogo = company.CompanyLogo,
						Name = company.Name,
						SecondName = company.SecondName,
					    TaxId = company.TaxId,
						AltName = company.AltName,
						DefaultBranchGUID = company.DefaultBranchGUID,
					
						RowVersion = company.RowVersion,
					});
		}
		public CompanyItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<CompanyItemViewMap, CompanyItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public Company CreateCompany(Company company)
		{
			try
			{
				company.CompanyGUID = Guid.NewGuid();
				base.Add(company);
				return company;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCompanyVoid(Company company)
		{
			try
			{
				CreateCompany(company);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public Company UpdateCompany(Company dbCompany, Company inputCompany, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCompany = dbCompany.MapUpdateValues<Company>(inputCompany);
				base.Update(dbCompany);
				return dbCompany;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCompanyVoid(Company dbCompany, Company inputCompany, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCompany = dbCompany.MapUpdateValues<Company>(inputCompany, skipUpdateFields);
				base.Update(dbCompany);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public bool HasAnyCompany()
        {
            try
            {
				return Entity.Any();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public List<Company> GetListCompanyNoTracking()
        {
            try
            {
				return Entity.AsNoTracking().ToList();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public Company GetCompanyByCompanyIdNoTracking(string companyId)
        {
            try
            {
				var result = Entity.Where(w => w.CompanyId == companyId)
									.AsNoTracking()
									.FirstOrDefault();
				return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
	}
}

