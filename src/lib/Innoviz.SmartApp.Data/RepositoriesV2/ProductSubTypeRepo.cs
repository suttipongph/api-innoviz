using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IProductSubTypeRepo
	{
		#region DropDown
		IEnumerable<SelectItem<ProductSubTypeItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<ProductSubTypeListView> GetListvw(SearchParameter search);
		ProductSubTypeItemView GetByIdvw(Guid id);
		ProductSubType Find(params object[] keyValues);
		ProductSubType GetProductSubTypeByIdNoTracking(Guid guid);
		ProductSubType CreateProductSubType(ProductSubType productSubType);
		void CreateProductSubTypeVoid(ProductSubType productSubType);
		ProductSubType UpdateProductSubType(ProductSubType dbProductSubType, ProductSubType inputProductSubType, List<string> skipUpdateFields = null);
		void UpdateProductSubTypeVoid(ProductSubType dbProductSubType, ProductSubType inputProductSubType, List<string> skipUpdateFields = null);
		void Remove(ProductSubType item);
	}
	public class ProductSubTypeRepo : CompanyBaseRepository<ProductSubType>, IProductSubTypeRepo
	{
		public ProductSubTypeRepo(SmartAppDbContext context) : base(context) { }
		public ProductSubTypeRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ProductSubType GetProductSubTypeByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ProductSubTypeGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<ProductSubTypeItemViewMap> GetDropDownQuery()
		{
			return (from productSubType in Entity
					select new ProductSubTypeItemViewMap
					{
						CompanyGUID = productSubType.CompanyGUID,
						Owner = productSubType.Owner,
						OwnerBusinessUnitGUID = productSubType.OwnerBusinessUnitGUID,
						ProductSubTypeGUID = productSubType.ProductSubTypeGUID,
						ProductSubTypeId = productSubType.ProductSubTypeId,
						Description = productSubType.Description,
						ProductType = productSubType.ProductType
					});
		}
		public IEnumerable<SelectItem<ProductSubTypeItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<ProductSubType>(search, SysParm.CompanyGUID);
				var productSubType = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<ProductSubTypeItemViewMap, ProductSubTypeItemView>().ToDropDownItem(search.ExcludeRowData);


				return productSubType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<ProductSubTypeListViewMap> GetListQuery()
		{
			return (from productSubType in Entity
				select new ProductSubTypeListViewMap
				{
						CompanyGUID = productSubType.CompanyGUID,
						Owner = productSubType.Owner,
						OwnerBusinessUnitGUID = productSubType.OwnerBusinessUnitGUID,
						ProductSubTypeGUID = productSubType.ProductSubTypeGUID,
						ProductSubTypeId = productSubType.ProductSubTypeId,
						Description = productSubType.Description,
						ProductType = productSubType.ProductType,
						CalcInterestMethod = productSubType.CalcInterestMethod,
						CalcInterestDayMethod = productSubType.CalcInterestDayMethod,
						MaxInterestFeePct  = productSubType.MaxInterestFeePct ,
						GuarantorAgreementYear = productSubType.GuarantorAgreementYear,
				});
		}
		public SearchResult<ProductSubTypeListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<ProductSubTypeListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<ProductSubType>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<ProductSubTypeListViewMap, ProductSubTypeListView>();
				result = list.SetSearchResult<ProductSubTypeListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<ProductSubTypeItemViewMap> GetItemQuery()
		{
			return (from productSubType in Entity
					join ownerBU in db.Set<BusinessUnit>()
					on productSubType.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljProductSubTypeOwnerBU
					from ownerBU in ljProductSubTypeOwnerBU.DefaultIfEmpty()
					join company in db.Set<Company>() on productSubType.CompanyGUID equals company.CompanyGUID
					select new ProductSubTypeItemViewMap
					{
						CompanyGUID = productSubType.CompanyGUID,
						Owner = productSubType.Owner,
						OwnerBusinessUnitGUID = productSubType.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CompanyId = company.CompanyId,
						CreatedBy = productSubType.CreatedBy,
						CreatedDateTime = productSubType.CreatedDateTime,
						ModifiedBy = productSubType.ModifiedBy,
						ModifiedDateTime = productSubType.ModifiedDateTime,
						ProductSubTypeGUID = productSubType.ProductSubTypeGUID,
						Description = productSubType.Description,
						GuarantorAgreementYear = productSubType.GuarantorAgreementYear,
						MaxInterestFeePct  = productSubType.MaxInterestFeePct ,
						ProductSubTypeId = productSubType.ProductSubTypeId,
						ProductType = productSubType.ProductType,
						CalcInterestMethod = productSubType.CalcInterestMethod,
						CalcInterestDayMethod = productSubType.CalcInterestDayMethod,
					
						RowVersion = productSubType.RowVersion,
					});
		}
		public ProductSubTypeItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<ProductSubTypeItemViewMap, ProductSubTypeItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public ProductSubType CreateProductSubType(ProductSubType productSubType)
		{
			try
			{
				productSubType.ProductSubTypeGUID = Guid.NewGuid();
				base.Add(productSubType);
				return productSubType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateProductSubTypeVoid(ProductSubType productSubType)
		{
			try
			{
				CreateProductSubType(productSubType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ProductSubType UpdateProductSubType(ProductSubType dbProductSubType, ProductSubType inputProductSubType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbProductSubType = dbProductSubType.MapUpdateValues<ProductSubType>(inputProductSubType);
				base.Update(dbProductSubType);
				return dbProductSubType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateProductSubTypeVoid(ProductSubType dbProductSubType, ProductSubType inputProductSubType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbProductSubType = dbProductSubType.MapUpdateValues<ProductSubType>(inputProductSubType, skipUpdateFields);
				base.Update(dbProductSubType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

