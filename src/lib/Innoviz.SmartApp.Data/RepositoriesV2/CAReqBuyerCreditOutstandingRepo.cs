using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICAReqBuyerCreditOutstandingRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CAReqBuyerCreditOutstandingItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<CAReqBuyerCreditOutstandingListView> GetListvw(SearchParameter search);
		CAReqBuyerCreditOutstandingItemView GetByIdvw(Guid id);
		CAReqBuyerCreditOutstanding Find(params object[] keyValues);
		CAReqBuyerCreditOutstanding GetCAReqBuyerCreditOutstandingByIdNoTracking(Guid guid);
		CAReqBuyerCreditOutstanding CreateCAReqBuyerCreditOutstanding(CAReqBuyerCreditOutstanding caReqBuyerCreditOutstanding);
		void CreateCAReqBuyerCreditOutstandingVoid(CAReqBuyerCreditOutstanding caReqBuyerCreditOutstanding);
		CAReqBuyerCreditOutstanding UpdateCAReqBuyerCreditOutstanding(CAReqBuyerCreditOutstanding dbCAReqBuyerCreditOutstanding, CAReqBuyerCreditOutstanding inputCAReqBuyerCreditOutstanding, List<string> skipUpdateFields = null);
		void UpdateCAReqBuyerCreditOutstandingVoid(CAReqBuyerCreditOutstanding dbCAReqBuyerCreditOutstanding, CAReqBuyerCreditOutstanding inputCAReqBuyerCreditOutstanding, List<string> skipUpdateFields = null);
		void Remove(CAReqBuyerCreditOutstanding item);
		CAReqBuyerCreditOutstanding GetCAReqBuyerCreditOutstandingByIdNoTrackingByAccessLevel(Guid guid);
	}
	public class CAReqBuyerCreditOutstandingRepo : CompanyBaseRepository<CAReqBuyerCreditOutstanding>, ICAReqBuyerCreditOutstandingRepo
	{
		public CAReqBuyerCreditOutstandingRepo(SmartAppDbContext context) : base(context) { }
		public CAReqBuyerCreditOutstandingRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CAReqBuyerCreditOutstanding GetCAReqBuyerCreditOutstandingByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CAReqBuyerCreditOutstandingGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CAReqBuyerCreditOutstandingItemViewMap> GetDropDownQuery()
		{
			return (from caReqBuyerCreditOutstanding in Entity
					select new CAReqBuyerCreditOutstandingItemViewMap
					{
						CompanyGUID = caReqBuyerCreditOutstanding.CompanyGUID,
						Owner = caReqBuyerCreditOutstanding.Owner,
						OwnerBusinessUnitGUID = caReqBuyerCreditOutstanding.OwnerBusinessUnitGUID,
						CAReqBuyerCreditOutstandingGUID = caReqBuyerCreditOutstanding.CAReqBuyerCreditOutstandingGUID
					});
		}
		public IEnumerable<SelectItem<CAReqBuyerCreditOutstandingItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CAReqBuyerCreditOutstanding>(search, SysParm.CompanyGUID);
				var caReqBuyerCreditOutstanding = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CAReqBuyerCreditOutstandingItemViewMap, CAReqBuyerCreditOutstandingItemView>().ToDropDownItem(search.ExcludeRowData);


				return caReqBuyerCreditOutstanding;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CAReqBuyerCreditOutstandingListViewMap> GetListQuery()
		{
			return (from caReqBuyerCreditOutstanding in Entity
					join buyerTable in db.Set<BuyerTable>() 
					on caReqBuyerCreditOutstanding.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljBuyerTable
					from buyerTable in ljBuyerTable.DefaultIfEmpty()
					join assignmentMethod in db.Set<AssignmentMethod>()
					on caReqBuyerCreditOutstanding.AssignmentMethodGUID equals assignmentMethod.AssignmentMethodGUID into ljAssignmentMethod
					from assignmentMethod in ljAssignmentMethod.DefaultIfEmpty()
					join billingResponsibleBy in db.Set<BillingResponsibleBy>()
					on caReqBuyerCreditOutstanding.BillingResponsibleByGUID equals billingResponsibleBy.BillingResponsibleByGUID into ljBillingResponsibleBy
					from billingResponsibleBy in ljBillingResponsibleBy.DefaultIfEmpty()
					join methodOfPayment in db.Set<MethodOfPayment>()
					on caReqBuyerCreditOutstanding.MethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID into ljMethodOfPayment
					from methodOfPayment in ljMethodOfPayment.DefaultIfEmpty()
					join creditAppTable in db.Set<CreditAppTable>() 
					on caReqBuyerCreditOutstanding.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljCreditAppTable
					from creditAppTable in ljCreditAppTable.DefaultIfEmpty()
					select new CAReqBuyerCreditOutstandingListViewMap
				{
						CompanyGUID = caReqBuyerCreditOutstanding.CompanyGUID,
						Owner = caReqBuyerCreditOutstanding.Owner,
						OwnerBusinessUnitGUID = caReqBuyerCreditOutstanding.OwnerBusinessUnitGUID,
						CAReqBuyerCreditOutstandingGUID = caReqBuyerCreditOutstanding.CAReqBuyerCreditOutstandingGUID,
						LineNum = caReqBuyerCreditOutstanding.LineNum,
						BuyerTableGUID = caReqBuyerCreditOutstanding.BuyerTableGUID,
						Status = caReqBuyerCreditOutstanding.Status,
						ApprovedCreditLimitLine = caReqBuyerCreditOutstanding.ApprovedCreditLimitLine,
						ARBalance = caReqBuyerCreditOutstanding.ARBalance,
						MaxPurchasePct = caReqBuyerCreditOutstanding.MaxPurchasePct,
						AssignmentMethodGUID = caReqBuyerCreditOutstanding.AssignmentMethodGUID,
						BillingResponsibleByGUID = caReqBuyerCreditOutstanding.BillingResponsibleByGUID,
						MethodOfPaymentGUID = caReqBuyerCreditOutstanding.MethodOfPaymentGUID,
						CreditAppRequestTableGUID = caReqBuyerCreditOutstanding.CreditAppRequestTableGUID,
						BuyerTable_Values = (buyerTable != null) ? SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName) : string.Empty,
						BuyerTable_BuyerId = (buyerTable != null) ?buyerTable.BuyerId : string.Empty,
						AssignmentMethod_Values = (assignmentMethod != null) ? SmartAppUtil.GetDropDownLabel(assignmentMethod.AssignmentMethodId, assignmentMethod.Description) : string.Empty,
						AssignmentMethod_AssignmentMethodId = (assignmentMethod != null) ? assignmentMethod.AssignmentMethodId: string.Empty,
						BillingResponsibleBy_Values = (billingResponsibleBy != null) ? SmartAppUtil.GetDropDownLabel(billingResponsibleBy.BillingResponsibleById, billingResponsibleBy.Description) : string.Empty,
						BillingResponsibleBy_BillingResponsibleById = (billingResponsibleBy != null) ? billingResponsibleBy.BillingResponsibleById : string.Empty,
						MethodOfPayment_Values = (methodOfPayment != null) ? SmartAppUtil.GetDropDownLabel(methodOfPayment.MethodOfPaymentId, methodOfPayment.Description) : string.Empty,
						MethodOfPayment_MethodOfPaymentId = (methodOfPayment != null) ? methodOfPayment.MethodOfPaymentId : string.Empty,
						CreditAppTable_CreditAppId = (creditAppTable != null) ? creditAppTable.CreditAppId : string.Empty,
					});
		}
		public SearchResult<CAReqBuyerCreditOutstandingListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CAReqBuyerCreditOutstandingListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CAReqBuyerCreditOutstanding>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CAReqBuyerCreditOutstandingListViewMap, CAReqBuyerCreditOutstandingListView>();
				result = list.SetSearchResult<CAReqBuyerCreditOutstandingListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CAReqBuyerCreditOutstandingItemViewMap> GetItemQuery()
		{
			return (from caReqBuyerCreditOutstanding in Entity
					join company in db.Set<Company>()
					on caReqBuyerCreditOutstanding.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on caReqBuyerCreditOutstanding.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCAReqBuyerCreditOutstandingOwnerBU
					from ownerBU in ljCAReqBuyerCreditOutstandingOwnerBU.DefaultIfEmpty()
					select new CAReqBuyerCreditOutstandingItemViewMap
					{
						CompanyGUID = caReqBuyerCreditOutstanding.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = caReqBuyerCreditOutstanding.Owner,
						OwnerBusinessUnitGUID = caReqBuyerCreditOutstanding.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = caReqBuyerCreditOutstanding.CreatedBy,
						CreatedDateTime = caReqBuyerCreditOutstanding.CreatedDateTime,
						ModifiedBy = caReqBuyerCreditOutstanding.ModifiedBy,
						ModifiedDateTime = caReqBuyerCreditOutstanding.ModifiedDateTime,
						CAReqBuyerCreditOutstandingGUID = caReqBuyerCreditOutstanding.CAReqBuyerCreditOutstandingGUID,
						ApprovedCreditLimitLine = caReqBuyerCreditOutstanding.ApprovedCreditLimitLine,
						ARBalance = caReqBuyerCreditOutstanding.ARBalance,
						AssignmentMethodGUID = caReqBuyerCreditOutstanding.AssignmentMethodGUID,
						BillingResponsibleByGUID = caReqBuyerCreditOutstanding.BillingResponsibleByGUID,
						BuyerTableGUID = caReqBuyerCreditOutstanding.BuyerTableGUID,
						CreditAppLineGUID = caReqBuyerCreditOutstanding.CreditAppLineGUID,
						CreditAppRequestTableGUID = caReqBuyerCreditOutstanding.CreditAppRequestTableGUID,
						CreditAppTableGUID = caReqBuyerCreditOutstanding.CreditAppTableGUID,
						LineNum = caReqBuyerCreditOutstanding.LineNum,
						MaxPurchasePct = caReqBuyerCreditOutstanding.MaxPurchasePct,
						MethodOfPaymentGUID = caReqBuyerCreditOutstanding.MethodOfPaymentGUID,
						ProductType = caReqBuyerCreditOutstanding.ProductType,
						Status = caReqBuyerCreditOutstanding.Status,
					
						RowVersion = caReqBuyerCreditOutstanding.RowVersion,
					});
		}
		public CAReqBuyerCreditOutstandingItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<CAReqBuyerCreditOutstandingItemViewMap, CAReqBuyerCreditOutstandingItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public CAReqBuyerCreditOutstanding CreateCAReqBuyerCreditOutstanding(CAReqBuyerCreditOutstanding caReqBuyerCreditOutstanding)
		{
			try
			{
				caReqBuyerCreditOutstanding.CAReqBuyerCreditOutstandingGUID = Guid.NewGuid();
				base.Add(caReqBuyerCreditOutstanding);
				return caReqBuyerCreditOutstanding;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCAReqBuyerCreditOutstandingVoid(CAReqBuyerCreditOutstanding caReqBuyerCreditOutstanding)
		{
			try
			{
				CreateCAReqBuyerCreditOutstanding(caReqBuyerCreditOutstanding);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CAReqBuyerCreditOutstanding UpdateCAReqBuyerCreditOutstanding(CAReqBuyerCreditOutstanding dbCAReqBuyerCreditOutstanding, CAReqBuyerCreditOutstanding inputCAReqBuyerCreditOutstanding, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCAReqBuyerCreditOutstanding = dbCAReqBuyerCreditOutstanding.MapUpdateValues<CAReqBuyerCreditOutstanding>(inputCAReqBuyerCreditOutstanding);
				base.Update(dbCAReqBuyerCreditOutstanding);
				return dbCAReqBuyerCreditOutstanding;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCAReqBuyerCreditOutstandingVoid(CAReqBuyerCreditOutstanding dbCAReqBuyerCreditOutstanding, CAReqBuyerCreditOutstanding inputCAReqBuyerCreditOutstanding, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCAReqBuyerCreditOutstanding = dbCAReqBuyerCreditOutstanding.MapUpdateValues<CAReqBuyerCreditOutstanding>(inputCAReqBuyerCreditOutstanding, skipUpdateFields);
				base.Update(dbCAReqBuyerCreditOutstanding);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public CAReqBuyerCreditOutstanding GetCAReqBuyerCreditOutstandingByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CAReqBuyerCreditOutstandingGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

