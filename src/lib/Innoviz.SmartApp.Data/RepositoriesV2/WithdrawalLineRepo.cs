using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface IWithdrawalLineRepo
    {
        #region DropDown
        IEnumerable<SelectItem<WithdrawalLineItemView>> GetDropDownItem(SearchParameter search);
        #endregion DropDown
        SearchResult<WithdrawalLineListView> GetListvw(SearchParameter search);
        WithdrawalLineItemView GetByIdvw(Guid id);
        WithdrawalLine Find(params object[] keyValues);
        WithdrawalLine GetWithdrawalLineByIdNoTracking(Guid guid);
        List<WithdrawalLine> GetWithdrawalLineByIdNoTracking(IEnumerable<Guid> withdrawalLineGuids);
        WithdrawalLine CreateWithdrawalLine(WithdrawalLine withdrawalLine);
        void CreateWithdrawalLineVoid(WithdrawalLine withdrawalLine);
        WithdrawalLine UpdateWithdrawalLine(WithdrawalLine dbWithdrawalLine, WithdrawalLine inputWithdrawalLine, List<string> skipUpdateFields = null);
        void UpdateWithdrawalLineVoid(WithdrawalLine dbWithdrawalLine, WithdrawalLine inputWithdrawalLine, List<string> skipUpdateFields = null);
        void Remove(WithdrawalLine item);
        WithdrawalLine GetWithdrawalLineByIdNoTrackingByAccessLevel(Guid guid);
        IEnumerable<WithdrawalLine> GetWithdrawalLineByWithdrawalTableNoTracking(Guid withdrawalTableGUID);
        void ValidateAdd(WithdrawalLine item);
        void ValidateAdd(IEnumerable<WithdrawalLine> items);
        #region shared
        IEnumerable<WithdrawalLine> GetWithdrawalLineByWithdrawalTableGuid(Guid guid);
        IEnumerable<WithdrawalLineItemViewMap> GetWithdrawalLineItemViewMapByOriginalWithdrawalTableGuid(Guid withdrawalTableGuid);
        IEnumerable<WithdrawalLineItemViewMap> GetWithdrawalLineItemViewMapByWithdrawalTableGuid(Guid withdrawalTableGuid);
        WithdrawalTableItemViewMap GetWithdrawalTable(Guid guid);
        #endregion
        #region Inquiry withdrawal line outstand
        SearchResult<InquiryWithdrawalLineOutstandListView> GetInquiryWithdrawalLineOutstandListvw(SearchParameter search);
        InquiryWithdrawalLineOutstandItemView GetInquiryWithdrawalLineOutstandByIdvw(Guid id);
        #endregion
    }
    public class WithdrawalLineRepo : CompanyBaseRepository<WithdrawalLine>, IWithdrawalLineRepo
    {
        public WithdrawalLineRepo(SmartAppDbContext context) : base(context) { }
        public WithdrawalLineRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public WithdrawalLine GetWithdrawalLineByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.WithdrawalLineGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<WithdrawalLine> GetWithdrawalLineByIdNoTracking(IEnumerable<Guid> withdrawalLineGuids)
        {
            try
            {
                var result = Entity.Where(w => withdrawalLineGuids.Contains(w.WithdrawalLineGUID))
                                    .AsNoTracking()
                                    .ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        private IQueryable<WithdrawalLineItemViewMap> GetDropDownQuery()
        {
            return (from withdrawalLine in Entity
                    select new WithdrawalLineItemViewMap
                    {
                        CompanyGUID = withdrawalLine.CompanyGUID,
                        Owner = withdrawalLine.Owner,
                        OwnerBusinessUnitGUID = withdrawalLine.OwnerBusinessUnitGUID,
                        WithdrawalLineGUID = withdrawalLine.WithdrawalLineGUID,
                        WithdrawalLineType = withdrawalLine.WithdrawalLineType,
                        StartDate = withdrawalLine.StartDate,
                        LineNum = withdrawalLine.LineNum
                    });
        }
        public IEnumerable<SelectItem<WithdrawalLineItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<WithdrawalLine>(search, SysParm.CompanyGUID);
                var withdrawalLine = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
                    .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                    .Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
                    .ToMaps<WithdrawalLineItemViewMap, WithdrawalLineItemView>().ToDropDownItem(search.ExcludeRowData);
                return withdrawalLine;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #region GetListvw
        private IQueryable<WithdrawalLineListViewMap> GetListQuery()
        {
            return (from withdrawalLine in Entity
                    select new WithdrawalLineListViewMap
                    {
                        CompanyGUID = withdrawalLine.CompanyGUID,
                        Owner = withdrawalLine.Owner,
                        OwnerBusinessUnitGUID = withdrawalLine.OwnerBusinessUnitGUID,
                        WithdrawalLineGUID = withdrawalLine.WithdrawalLineGUID,
                        WithdrawalLineType = withdrawalLine.WithdrawalLineType,
                        StartDate = withdrawalLine.StartDate,
                        DueDate = withdrawalLine.DueDate,
                        InterestDate = withdrawalLine.InterestDate,
                        InterestDay = withdrawalLine.InterestDay,
                        InterestAmount = withdrawalLine.InterestAmount,
                        WithdrawalTableGUID = withdrawalLine.WithdrawalTableGUID,
                        LineNum = withdrawalLine.LineNum
                    });
        }
        public SearchResult<WithdrawalLineListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<WithdrawalLineListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<WithdrawalLine>(search, SysParm.CompanyGUID);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<WithdrawalLineListViewMap, WithdrawalLineListView>();
                result = list.SetSearchResult<WithdrawalLineListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<WithdrawalLineItemViewMap> GetItemQuery()
        {
            return (from withdrawalLine in Entity
                    join company in db.Set<Company>()
                    on withdrawalLine.CompanyGUID equals company.CompanyGUID
                    join withdrawalTable in db.Set<WithdrawalTable>()
                    on withdrawalLine.WithdrawalTableGUID equals withdrawalTable.WithdrawalTableGUID
                    join creditAppTable in db.Set<CreditAppTable>()
                    on withdrawalTable.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID

                    join documentStatus in db.Set<DocumentStatus>()
                    on withdrawalTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID

                    join ownerBU in db.Set<BusinessUnit>()
                    on withdrawalLine.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljWithdrawalLineOwnerBU
                    from ownerBU in ljWithdrawalLineOwnerBU.DefaultIfEmpty()

                    join invoiceTable in db.Set<InvoiceTable>()
                    on withdrawalLine.WithdrawalLineInvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljWithdrawalLineInvoiceTable
                    from invoiceTable in ljWithdrawalLineInvoiceTable.DefaultIfEmpty()

                    join chequeTable in db.Set<ChequeTable>()
                    on withdrawalLine.CustomerPDCTableGUID equals chequeTable.ChequeTableGUID into ljWithdrawalLineChequeTable
                    from chequeTable in ljWithdrawalLineChequeTable.DefaultIfEmpty()
                    select new WithdrawalLineItemViewMap
                    {
                        CompanyGUID = withdrawalLine.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = withdrawalLine.Owner,
                        OwnerBusinessUnitGUID = withdrawalLine.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        CreatedBy = withdrawalLine.CreatedBy,
                        CreatedDateTime = withdrawalLine.CreatedDateTime,
                        ModifiedBy = withdrawalLine.ModifiedBy,
                        ModifiedDateTime = withdrawalLine.ModifiedDateTime,
                        WithdrawalLineGUID = withdrawalLine.WithdrawalLineGUID,
                        BillingDate = withdrawalLine.BillingDate,
                        BuyerPDCTableGUID = withdrawalLine.BuyerPDCTableGUID,
                        ClosedForTermExtension = withdrawalLine.ClosedForTermExtension,
                        CollectionDate = withdrawalLine.CollectionDate,
                        CustomerPDCTableGUID = withdrawalLine.CustomerPDCTableGUID,
                        Dimension1GUID = withdrawalLine.Dimension1GUID,
                        Dimension2GUID = withdrawalLine.Dimension2GUID,
                        Dimension3GUID = withdrawalLine.Dimension3GUID,
                        Dimension4GUID = withdrawalLine.Dimension4GUID,
                        Dimension5GUID = withdrawalLine.Dimension5GUID,
                        DueDate = withdrawalLine.DueDate,
                        InterestAmount = withdrawalLine.InterestAmount,
                        InterestDate = withdrawalLine.InterestDate,
                        InterestDay = withdrawalLine.InterestDay,
                        StartDate = withdrawalLine.StartDate,
                        WithdrawalLineInvoiceTableGUID = withdrawalLine.WithdrawalLineInvoiceTableGUID,
                        WithdrawalLineType = withdrawalLine.WithdrawalLineType,
                        WithdrawalTableGUID = withdrawalLine.WithdrawalTableGUID,
                        WithdrawalTable_WithdrawalDate = withdrawalTable.WithdrawalDate,
                        WithdrawalTable_BuyerTableGUID = withdrawalTable.BuyerTableGUID,
                        WithdrawalTable_CustomerTableGUID = withdrawalTable.CustomerTableGUID,
                        InvoiceTable_Values = (invoiceTable != null) ? SmartAppUtil.GetDropDownLabel(invoiceTable.InvoiceId, invoiceTable.IssuedDate.DateToString()) : null,
                        WithdrawalTable_Values = SmartAppUtil.GetDropDownLabel(withdrawalTable.WithdrawalId, withdrawalTable.Description),
                        LineNum = withdrawalLine.LineNum,
                        CustomerPDCAmount = (chequeTable != null) ? chequeTable.Amount : 0,
                        CustomerPDCDate = (chequeTable != null) ? (DateTime?)chequeTable.ChequeDate : null,
                        WithdrawalTable_StatusId = documentStatus.StatusId,
                    
                        RowVersion = withdrawalLine.RowVersion,
                    });
        }
        public WithdrawalLineItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<WithdrawalLineItemViewMap, WithdrawalLineItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public WithdrawalLine CreateWithdrawalLine(WithdrawalLine withdrawalLine)
        {
            try
            {
                withdrawalLine.WithdrawalLineGUID = withdrawalLine.WithdrawalLineGUID == Guid.Empty ? Guid.NewGuid() : withdrawalLine.WithdrawalLineGUID;
                base.Add(withdrawalLine);
                return withdrawalLine;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateWithdrawalLineVoid(WithdrawalLine withdrawalLine)
        {
            try
            {
                CreateWithdrawalLine(withdrawalLine);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public WithdrawalLine UpdateWithdrawalLine(WithdrawalLine dbWithdrawalLine, WithdrawalLine inputWithdrawalLine, List<string> skipUpdateFields = null)
        {
            try
            {
                dbWithdrawalLine = dbWithdrawalLine.MapUpdateValues<WithdrawalLine>(inputWithdrawalLine);
                base.Update(dbWithdrawalLine);
                return dbWithdrawalLine;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateWithdrawalLineVoid(WithdrawalLine dbWithdrawalLine, WithdrawalLine inputWithdrawalLine, List<string> skipUpdateFields = null)
        {
            try
            {
                dbWithdrawalLine = dbWithdrawalLine.MapUpdateValues<WithdrawalLine>(inputWithdrawalLine, skipUpdateFields);
                base.Update(dbWithdrawalLine);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Create, Update, Delete
        public WithdrawalLine GetWithdrawalLineByIdNoTrackingByAccessLevel(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.WithdrawalLineGUID == guid)
                    .FilterByAccessLevel(SysParm.AccessLevel)
                    .AsNoTracking()
                    .FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public IEnumerable<WithdrawalLine> GetWithdrawalLineByWithdrawalTableNoTracking(Guid withdrawalTableGUID)
        {
            try
            {
                var result = Entity.Where(item => item.WithdrawalTableGUID == withdrawalTableGUID).AsNoTracking();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region shared
        public IEnumerable<WithdrawalLine> GetWithdrawalLineByWithdrawalTableGuid(Guid guid)
        {
            try
            {
                IEnumerable<WithdrawalLine> result = Entity.Where(item => item.WithdrawalTableGUID == guid)
                    .AsNoTracking().ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<WithdrawalLineItemViewMap> GetWithdrawalLineItemViewMapByOriginalWithdrawalTableGuid(Guid OrgWithdrawalTableGuid)
        {
            try
            {
                IEnumerable<WithdrawalLineItemViewMap> result = (from withdrawalLine in Entity
                                                                 join custTrans in db.Set<CustTrans>() on withdrawalLine.WithdrawalLineInvoiceTableGUID equals custTrans.InvoiceTableGUID
                                                                 join withdrawalTable in db.Set<WithdrawalTable>() on withdrawalLine.WithdrawalTableGUID equals withdrawalTable.WithdrawalTableGUID
                                                                 where withdrawalLine.WithdrawalTableGUID == OrgWithdrawalTableGuid || withdrawalTable.OriginalWithdrawalTableGUID == OrgWithdrawalTableGuid

                                                                 select new WithdrawalLineItemViewMap
                                                                 {
                                                                     WithdrawalLineGUID = withdrawalLine.WithdrawalLineGUID,
                                                                     WithdrawalTable_OriginalWithdrawalTableGUID = withdrawalTable.OriginalWithdrawalTableGUID,
                                                                     CustTrans_TransAmount = custTrans.TransAmount,
                                                                     CustTrans_SettleAmount = custTrans.SettleAmount
                                                                 }
                                                      ).AsNoTracking().ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<WithdrawalLineItemViewMap> GetWithdrawalLineItemViewMapByWithdrawalTableGuid(Guid withdrawalTableGuid)
        {
            try
            {
                IEnumerable<WithdrawalLineItemViewMap> result = (from withdrawalLine in Entity
                                                                 join custTrans in db.Set<CustTrans>() on withdrawalLine.WithdrawalLineInvoiceTableGUID equals custTrans.InvoiceTableGUID
                                                                 join withdrawalTable in db.Set<WithdrawalTable>() on withdrawalLine.WithdrawalTableGUID equals withdrawalTable.WithdrawalTableGUID
                                                                 where withdrawalLine.WithdrawalTableGUID == withdrawalTableGuid

                                                                 select new WithdrawalLineItemViewMap
                                                                 {
                                                                     WithdrawalLineGUID = withdrawalLine.WithdrawalLineGUID,
                                                                     WithdrawalLineType = withdrawalLine.WithdrawalLineType,
                                                                     CustTrans_TransAmount = custTrans.TransAmount,
                                                                     CustTrans_SettleAmount = custTrans.SettleAmount
                                                                 }
                                                      ).AsNoTracking().ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public WithdrawalTableItemViewMap GetWithdrawalTable(Guid guid)
        {
            try
            {
            var result = (from withdrawalLine in Entity
                            join withdrawalTable in db.Set<WithdrawalTable>() 
                            on withdrawalLine.WithdrawalTableGUID equals withdrawalTable.WithdrawalTableGUID
                            into ljwithdrawalTable
                            from withdrawalTable in ljwithdrawalTable.DefaultIfEmpty()
                          where withdrawalLine.WithdrawalLineGUID == guid

                          select new WithdrawalTableItemViewMap
                          {
                                ProductType = withdrawalTable.ProductType,
                                CreditAppLineGUID = withdrawalTable.CreditAppLineGUID,
                                CustomerTableGUID = withdrawalTable.CustomerTableGUID,
                                BuyerTableGUID = withdrawalTable.BuyerTableGUID,
                                WithdrawalLine_Values = SmartAppUtil.GetDropDownLabel( ((WithdrawalLineType)withdrawalLine.WithdrawalLineType).ToString(), withdrawalLine.StartDate.DateToString())


    }
                ).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region GetInquiryWithdrawalLineOutstandListvw
        private IQueryable<InquiryWithdrawalLineOutstandListViewMap> GetInquiryWithdrawalLineOutstandListQuery()
        {
            return (from withdrawalLine in Entity
                    join company in db.Set<Company>()
                    on withdrawalLine.CompanyGUID equals company.CompanyGUID
                    join ownerBU in db.Set<BusinessUnit>()
                    on withdrawalLine.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljWithdrawalLineOwnerBU
                    from ownerBU in ljWithdrawalLineOwnerBU.DefaultIfEmpty()
                    join withdrawalTable in db.Set<WithdrawalTable>()
                    on withdrawalLine.WithdrawalTableGUID equals withdrawalTable.WithdrawalTableGUID
                    join customerTable in db.Set<CustomerTable>()
                    on withdrawalTable.CustomerTableGUID equals customerTable.CustomerTableGUID
                    join buyerTable in db.Set<BuyerTable>()
                    on withdrawalTable.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljWithdrawalLineBuyerTable
                    from buyerTable in ljWithdrawalLineBuyerTable.DefaultIfEmpty()
                    join methodOfPayment in db.Set<MethodOfPayment>()
                    on withdrawalTable.MethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID into ljWithdrawalLineMethodOfPayment
                    from methodOfPayment in ljWithdrawalLineMethodOfPayment.DefaultIfEmpty()
                    join invoiceTable in db.Set<InvoiceTable>()
                    on withdrawalLine.WithdrawalLineInvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljWithdrawalLineInvoiceTable
                    from invoiceTable in ljWithdrawalLineInvoiceTable.DefaultIfEmpty()
                    join custTrans in db.Set<CustTrans>()
                    on invoiceTable.InvoiceTableGUID equals custTrans.InvoiceTableGUID into ljInvoiceTableCusttrans
                    from custTrans in ljInvoiceTableCusttrans.DefaultIfEmpty()
                    where custTrans.CustTransStatus == (int)CustTransStatus.Open
                    select new InquiryWithdrawalLineOutstandListViewMap
                    {
                        CompanyGUID = withdrawalLine.CompanyGUID,
                        Owner = withdrawalLine.Owner,
                        OwnerBusinessUnitGUID = withdrawalLine.OwnerBusinessUnitGUID,
                        WithdrawalLineGUID = withdrawalLine.WithdrawalLineGUID,
                        WithdrawalTable_Values = SmartAppUtil.GetDropDownLabel(withdrawalTable.WithdrawalId, withdrawalTable.Description),
                        WithdrawalTableGUID = withdrawalLine.WithdrawalTableGUID,
                        WithdrawalTable_WithdrawalId = withdrawalTable.WithdrawalId,
                        CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                        CustomerTableGUID = withdrawalTable.CustomerTableGUID,
                        CustomerTable_CustomerId = customerTable.CustomerId,
                        BuyerTable_Values = (buyerTable != null) ? SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName) : null,
                        BuyerTableGUID = withdrawalTable.BuyerTableGUID,
                        BuyerTable_BuyerId = (buyerTable != null) ? buyerTable.BuyerId : null,
                        DueDate = withdrawalLine.DueDate,
                        BillingDate = withdrawalLine.BillingDate,
                        CollectionDate = withdrawalLine.CollectionDate,
                        WithdrawalAmount = withdrawalTable.WithdrawalAmount,
                        Outstanding = (custTrans != null) ? custTrans.TransAmount - custTrans.SettleAmount : 0,
                        MethodOfPayment_Values = (methodOfPayment != null) ? SmartAppUtil.GetDropDownLabel(methodOfPayment.MethodOfPaymentId, methodOfPayment.Description) : null,
                        MethodOfPaymentGUID = withdrawalTable.MethodOfPaymentGUID,
                        MethodOfPayment_MethodOfPaymentId = (methodOfPayment != null) ? methodOfPayment.MethodOfPaymentId : null,
                    });
        }
        public SearchResult<InquiryWithdrawalLineOutstandListView> GetInquiryWithdrawalLineOutstandListvw(SearchParameter search)
        {
            var result = new SearchResult<InquiryWithdrawalLineOutstandListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<WithdrawalLine>(search, SysParm.CompanyGUID);
                var total = GetInquiryWithdrawalLineOutstandListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetInquiryWithdrawalLineOutstandListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<InquiryWithdrawalLineOutstandListViewMap, InquiryWithdrawalLineOutstandListView>();
                result = list.SetSearchResult<InquiryWithdrawalLineOutstandListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetInquiryWithdrawalLineOutstandListvw
        #region GetInquiryWithdrawalLineOutstandByIdvw
        private IQueryable<InquiryWithdrawalLineOutstandItemViewMap> GetInquiryWithdrawalLineOutstandItemQuery()
        {
            var result = (from withdrawalLine in Entity
                    join company in db.Set<Company>()
                    on withdrawalLine.CompanyGUID equals company.CompanyGUID
                    join ownerBU in db.Set<BusinessUnit>()
                    on withdrawalLine.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljWithdrawalLineOwnerBU
                    from ownerBU in ljWithdrawalLineOwnerBU.DefaultIfEmpty()
                    join withdrawalTable in db.Set<WithdrawalTable>()
                    on withdrawalLine.WithdrawalTableGUID equals withdrawalTable.WithdrawalTableGUID
                    join customerTable in db.Set<CustomerTable>()
                    on withdrawalTable.CustomerTableGUID equals customerTable.CustomerTableGUID

                    join buyerTable in db.Set<BuyerTable>()
                    on withdrawalTable.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljWithdrawalLineBuyerTable
                    from buyerTable in ljWithdrawalLineBuyerTable.DefaultIfEmpty()

                    join buyerAgreementTrans in db.Set<BuyerAgreementTrans>()
                    on withdrawalTable.BuyerAgreementTableGUID equals buyerAgreementTrans.BuyerAgreementTableGUID into ljWithdrawalLineBuyerAgreementTrans
                    from buyerAgreementTrans in ljWithdrawalLineBuyerAgreementTrans.DefaultIfEmpty()
                    join buyerAgreementTable in db.Set<BuyerAgreementTable>()
                    on buyerAgreementTrans.BuyerAgreementTableGUID equals buyerAgreementTable.BuyerAgreementTableGUID into ljBuyerAgreementTransBuyerAgreementTable
                    from buyerAgreementTable in ljBuyerAgreementTransBuyerAgreementTable.DefaultIfEmpty()
                    join buyerAgreementLine in db.Set<BuyerAgreementLine>()
                    on buyerAgreementTrans.BuyerAgreementLineGUID equals buyerAgreementLine.BuyerAgreementLineGUID into ljBuyerAgreementTransBuyerAgreementLine
                    from buyerAgreementLine in ljBuyerAgreementTransBuyerAgreementLine.DefaultIfEmpty()
                    join methodOfPayment in db.Set<MethodOfPayment>()
                    on withdrawalTable.MethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID into ljWithdrawalLineMethodOfPayment
                    from methodOfPayment in ljWithdrawalLineMethodOfPayment.DefaultIfEmpty()
                    join invoiceTable in db.Set<InvoiceTable>()
                    on withdrawalLine.WithdrawalLineInvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljWithdrawalLineInvoiceTable
                    from invoiceTable in ljWithdrawalLineInvoiceTable.DefaultIfEmpty()
                    join custTrans in db.Set<CustTrans>()
                    on invoiceTable.InvoiceTableGUID equals custTrans.InvoiceTableGUID into ljInvoiceTableCusttrans
                    from custTrans in ljInvoiceTableCusttrans.DefaultIfEmpty()
                    join chequeTable in db.Set<ChequeTable>()
                    on withdrawalLine.CustomerPDCTableGUID equals chequeTable.ChequeTableGUID into ljWithdrawalLineChequeTable
                    from chequeTable in ljWithdrawalLineChequeTable.DefaultIfEmpty()

                    join creditAppTable in db.Set<CreditAppTable>()
                    on withdrawalTable.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID

                    join creditAppTableBankAccountControl_CustBank in db.Set<CustBank>()
                    on creditAppTable.BankAccountControlGUID equals creditAppTableBankAccountControl_CustBank.CustBankGUID into ljCreditAppTableBankAccountControlCustBank
                    from creditAppTableBankAccountControl_CustBank in ljCreditAppTableBankAccountControlCustBank.DefaultIfEmpty()
                    
                    join creditAppTablePDCBank_CustBank in db.Set<CustBank>()
                    on creditAppTable.BankAccountControlGUID equals creditAppTablePDCBank_CustBank.CustBankGUID into ljCreditAppTablePDCBankCustBank
                    from creditAppTablePDCBank_CustBank in ljCreditAppTablePDCBankCustBank.DefaultIfEmpty()
                    
                    join creditAppLine in db.Set<CreditAppLine>()
                    on withdrawalTable.CreditAppLineGUID equals creditAppLine.CreditAppLineGUID into ljWithdrawalTableCreditAppLine
                    from creditAppLine in ljWithdrawalTableCreditAppLine.DefaultIfEmpty()

                    select new InquiryWithdrawalLineOutstandItemViewMap
                    {
                        CompanyGUID = withdrawalLine.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = withdrawalLine.Owner,
                        OwnerBusinessUnitGUID = withdrawalLine.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        CreatedBy = withdrawalLine.CreatedBy,
                        CreatedDateTime = withdrawalLine.CreatedDateTime,
                        ModifiedBy = withdrawalLine.ModifiedBy,
                        ModifiedDateTime = withdrawalLine.ModifiedDateTime,
                        WithdrawalLineGUID = withdrawalLine.WithdrawalLineGUID,
                        WithdrawalTable_Values = SmartAppUtil.GetDropDownLabel(withdrawalTable.WithdrawalId, withdrawalTable.Description),
                        CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                        BuyerTable_Values = (buyerTable != null) ? SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName) : null,
                        BuyerAgreementTrans_Values = (buyerAgreementTrans != null) ? SmartAppUtil.GetDropDownLabel(buyerAgreementTable.BuyerAgreementId, buyerAgreementLine.Period.ToString()) : null,
                        WithdrawalAmount = withdrawalTable.WithdrawalAmount,
                        Outstanding = (custTrans != null) ? custTrans.TransAmount - custTrans.SettleAmount : 0,
                        MethodOfPayment_Values = (methodOfPayment != null) ? SmartAppUtil.GetDropDownLabel(methodOfPayment.MethodOfPaymentId, methodOfPayment.Description) : null,
                        DueDate = withdrawalLine.DueDate,
                        ChequeTable_ChequeDate = (chequeTable != null) ? chequeTable.ChequeDate : (DateTime?)null,
                        BillingDate = withdrawalLine.BillingDate,
                        CollectionDate = withdrawalLine.CollectionDate,
                        CreditAppTableBankAccountControl_BankAccountName = (creditAppTableBankAccountControl_CustBank != null) ? creditAppTableBankAccountControl_CustBank.BankAccountName : null,
                        CreditAppTablePDCBank_BankAccountName = (creditAppTablePDCBank_CustBank != null) ? creditAppTablePDCBank_CustBank.BankAccountName : null,
                        WithdrawalLineType = withdrawalLine.WithdrawalLineType,
                        CreditAppLine_Values = SmartAppUtil.GetDropDownLabel(creditAppLine.LineNum.ToString(), creditAppLine.ApprovedCreditLimitLine.ToString())
                    });
            return result;
        }
        public InquiryWithdrawalLineOutstandItemView GetInquiryWithdrawalLineOutstandByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetInquiryWithdrawalLineOutstandItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<InquiryWithdrawalLineOutstandItemViewMap, InquiryWithdrawalLineOutstandItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetInquiryWithdrawalLineOutstandByIdvw
    }
}

