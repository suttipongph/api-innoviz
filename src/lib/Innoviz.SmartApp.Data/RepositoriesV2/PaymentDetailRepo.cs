using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IPaymentDetailRepo
	{
		#region DropDown
		IEnumerable<SelectItem<PaymentDetailItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<PaymentDetailListView> GetListvw(SearchParameter search);
		PaymentDetailItemView GetByIdvw(Guid id);
		PaymentDetail Find(params object[] keyValues);
		PaymentDetail GetPaymentDetailByIdNoTracking(Guid guid);
		PaymentDetail CreatePaymentDetail(PaymentDetail paymentDetail);
		void CreatePaymentDetailVoid(PaymentDetail paymentDetail);
		PaymentDetail UpdatePaymentDetail(PaymentDetail dbPaymentDetail, PaymentDetail inputPaymentDetail, List<string> skipUpdateFields = null);
		void UpdatePaymentDetailVoid(PaymentDetail dbPaymentDetail, PaymentDetail inputPaymentDetail, List<string> skipUpdateFields = null);
		void Remove(PaymentDetail item);
		PaymentDetail GetPaymentDetailByIdNoTrackingByAccessLevel(Guid guid);
		IEnumerable<PaymentDetail> GetPaymentDetailByReferanceNoTracking(Guid refGUID, int refType);
		void ValidateAdd(IEnumerable<PaymentDetail> items);
		List<PaymentDetail> GetPaymentDetailByIdNoTracking(List<Guid> paymentDetailGuids);
	}
	public class PaymentDetailRepo : CompanyBaseRepository<PaymentDetail>, IPaymentDetailRepo
	{
		public PaymentDetailRepo(SmartAppDbContext context) : base(context) { }
		public PaymentDetailRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public PaymentDetail GetPaymentDetailByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.PaymentDetailGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<PaymentDetailItemViewMap> GetDropDownQuery()
		{
			return (from paymentDetail in Entity
					select new PaymentDetailItemViewMap
					{
						CompanyGUID = paymentDetail.CompanyGUID,
						Owner = paymentDetail.Owner,
						OwnerBusinessUnitGUID = paymentDetail.OwnerBusinessUnitGUID,
						PaymentDetailGUID = paymentDetail.PaymentDetailGUID
					});
		}
		public IEnumerable<SelectItem<PaymentDetailItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<PaymentDetail>(search, SysParm.CompanyGUID);
				var paymentDetail = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<PaymentDetailItemViewMap, PaymentDetailItemView>().ToDropDownItem(search.ExcludeRowData);


				return paymentDetail;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<PaymentDetailListViewMap> GetListQuery()
		{
			return (from paymentDetail in Entity
					join invoiceType in db.Set<InvoiceType>()
					on paymentDetail.InvoiceTypeGUID equals invoiceType.InvoiceTypeGUID into lj1
					from invoiceType in lj1.DefaultIfEmpty()
					join customerTable in db.Set<CustomerTable>()
					on paymentDetail.CustomerTableGUID equals customerTable.CustomerTableGUID into lj2
					from customerTable in lj2.DefaultIfEmpty()
					join vendorTable in db.Set<VendorTable>()
					on paymentDetail.VendorTableGUID equals vendorTable.VendorTableGUID into lj3
					from vendorTable in lj3.DefaultIfEmpty()
					select new PaymentDetailListViewMap
				{
						CompanyGUID = paymentDetail.CompanyGUID,
						Owner = paymentDetail.Owner,
						OwnerBusinessUnitGUID = paymentDetail.OwnerBusinessUnitGUID,
						PaymentDetailGUID = paymentDetail.PaymentDetailGUID,
						PaidToType = paymentDetail.PaidToType,
						InvoiceTypeGUID = paymentDetail.InvoiceTypeGUID,
						SuspenseTransfer = paymentDetail.SuspenseTransfer,
						CustomerTableGUID = paymentDetail.CustomerTableGUID,
						VendorTableGUID = paymentDetail.VendorTableGUID,
						PaymentAmount = paymentDetail.PaymentAmount,
						InvoiceType_Values = (invoiceType == null) ? null : SmartAppUtil.GetDropDownLabel(invoiceType.InvoiceTypeId, invoiceType.Description),
						CustomerTable_Values = (customerTable == null) ? null : SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
						VendorTable_Values = (vendorTable == null) ? null : SmartAppUtil.GetDropDownLabel(vendorTable.VendorId, vendorTable.Name),
						InvoiceType_InvoiceTypeId = (invoiceType == null) ? null : invoiceType.InvoiceTypeId,
						CustomerTable_CustomerId = (customerTable == null) ? null : customerTable.CustomerId,
						VendorTable_VendorId = (vendorTable == null) ? null : vendorTable.VendorId,
						RefGUID = paymentDetail.RefGUID
				});
		}
		public SearchResult<PaymentDetailListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<PaymentDetailListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<PaymentDetail>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<PaymentDetailListViewMap, PaymentDetailListView>();
				result = list.SetSearchResult<PaymentDetailListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<PaymentDetailItemViewMap> GetItemQuery()
		{
			return (from paymentDetail in Entity
					join company in db.Set<Company>()
					on paymentDetail.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on paymentDetail.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljPaymentDetailOwnerBU
					from ownerBU in ljPaymentDetailOwnerBU.DefaultIfEmpty()
					join purchaseTable in db.Set<PurchaseTable>()
					on paymentDetail.RefGUID equals purchaseTable.PurchaseTableGUID into ljPaymentDetailPurchaseTable
					from purchaseTable in ljPaymentDetailPurchaseTable.DefaultIfEmpty()
					select new PaymentDetailItemViewMap
					{
						CompanyGUID = paymentDetail.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = paymentDetail.Owner,
						OwnerBusinessUnitGUID = paymentDetail.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = paymentDetail.CreatedBy,
						CreatedDateTime = paymentDetail.CreatedDateTime,
						ModifiedBy = paymentDetail.ModifiedBy,
						ModifiedDateTime = paymentDetail.ModifiedDateTime,
						PaymentDetailGUID = paymentDetail.PaymentDetailGUID,
						CustomerTableGUID = paymentDetail.CustomerTableGUID,
						InvoiceTypeGUID = paymentDetail.InvoiceTypeGUID,
						PaidToType = paymentDetail.PaidToType,
						PaymentAmount = paymentDetail.PaymentAmount,
						RefGUID = paymentDetail.RefGUID,
						RefType = paymentDetail.RefType,
						SuspenseTransfer = paymentDetail.SuspenseTransfer,
						VendorTableGUID = paymentDetail.VendorTableGUID,
					
						RowVersion = paymentDetail.RowVersion,
					});
		}
		public PaymentDetailItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<PaymentDetailItemViewMap, PaymentDetailItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public PaymentDetail CreatePaymentDetail(PaymentDetail paymentDetail)
		{
			try
			{
				paymentDetail.PaymentDetailGUID = Guid.NewGuid();
				base.Add(paymentDetail);
				return paymentDetail;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreatePaymentDetailVoid(PaymentDetail paymentDetail)
		{
			try
			{
				CreatePaymentDetail(paymentDetail);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public PaymentDetail UpdatePaymentDetail(PaymentDetail dbPaymentDetail, PaymentDetail inputPaymentDetail, List<string> skipUpdateFields = null)
		{
			try
			{
				dbPaymentDetail = dbPaymentDetail.MapUpdateValues<PaymentDetail>(inputPaymentDetail);
				base.Update(dbPaymentDetail);
				return dbPaymentDetail;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdatePaymentDetailVoid(PaymentDetail dbPaymentDetail, PaymentDetail inputPaymentDetail, List<string> skipUpdateFields = null)
		{
			try
			{
				dbPaymentDetail = dbPaymentDetail.MapUpdateValues<PaymentDetail>(inputPaymentDetail, skipUpdateFields);
				base.Update(dbPaymentDetail);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public PaymentDetail GetPaymentDetailByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.PaymentDetailGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<PaymentDetail> GetPaymentDetailByReferanceNoTracking(Guid refGUID,int refType)
		{
			try
			{
				var result = Entity.Where(item => item.RefGUID == refGUID && item.RefType == refType)
					.AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<PaymentDetail> GetPaymentDetailByIdNoTracking(List<Guid> paymentDetailGuids)
		{
			try
			{
				List<PaymentDetail> paymentDetails = Entity.Where(w => paymentDetailGuids.Contains(w.PaymentDetailGUID)).AsNoTracking().ToList();
				return paymentDetails;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Validate
		public override void ValidateAdd(IEnumerable<PaymentDetail> items)
        {
            base.ValidateAdd(items);
        }
        #endregion Validate
    }
}

