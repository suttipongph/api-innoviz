using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IVendBankRepo
	{
		#region DropDown
		IEnumerable<SelectItem<VendBankItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<VendBankListView> GetListvw(SearchParameter search);
		VendBankItemView GetByIdvw(Guid id);
		VendBank Find(params object[] keyValues);
		VendBank GetVendBankByIdNoTracking(Guid guid);
		VendBank CreateVendBank(VendBank vendBank);
		void CreateVendBankVoid(VendBank vendBank);
		VendBank UpdateVendBank(VendBank dbVendBank, VendBank inputVendBank, List<string> skipUpdateFields = null);
		void UpdateVendBankVoid(VendBank dbVendBank, VendBank inputVendBank, List<string> skipUpdateFields = null);
		void Remove(VendBank item);
		void ValidateAdd(VendBank item);
		void ValidateAdd(IEnumerable<VendBank> items);
		List<VendBank> GetVendBankPrimaryByVendorNoTracking(List<Guid> vendorTableGuids);
	}
	public class VendBankRepo : CompanyBaseRepository<VendBank>, IVendBankRepo
	{
		public VendBankRepo(SmartAppDbContext context) : base(context) { }
		public VendBankRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public VendBank GetVendBankByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.VendBankGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<VendBankItemViewMap> GetDropDownQuery()
		{
			return (from vendBank in Entity
					select new VendBankItemViewMap
					{
						CompanyGUID = vendBank.CompanyGUID,
						Owner = vendBank.Owner,
						OwnerBusinessUnitGUID = vendBank.OwnerBusinessUnitGUID,
						VendBankGUID = vendBank.VendBankGUID
					});
		}
		public IEnumerable<SelectItem<VendBankItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<VendBank>(search, SysParm.CompanyGUID);
				var vendBank = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<VendBankItemViewMap, VendBankItemView>().ToDropDownItem(search.ExcludeRowData);


				return vendBank;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<VendBankListViewMap> GetListQuery()
		{
			return (from vendBank in Entity
					join bankGroup in db.Set<BankGroup>()
					on vendBank.BankGroupGUID equals bankGroup.BankGroupGUID
					select new VendBankListViewMap
				{
						CompanyGUID = vendBank.CompanyGUID,
						Owner = vendBank.Owner,
						OwnerBusinessUnitGUID = vendBank.OwnerBusinessUnitGUID,
						VendBankGUID = vendBank.VendBankGUID,
						BankAccount = vendBank.BankAccount,
						BankAccountName = vendBank.BankAccountName,
						BankBranch = vendBank.BankBranch,
						BankGroupGUID = vendBank.BankGroupGUID,
						BankGroup_BankGroupId = bankGroup.BankGroupId,
						BankGroup_Description = bankGroup.Description,
						VendorTableGUID = vendBank.VendorTableGUID,
						Primary = vendBank.Primary
					});
		}
		public SearchResult<VendBankListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<VendBankListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<VendBank>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<VendBankListViewMap, VendBankListView>();
				result = list.SetSearchResult<VendBankListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<VendBankItemViewMap> GetItemQuery()
		{
			return (from vendBank in Entity
					join company in db.Set<Company>()
					on vendBank.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on vendBank.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljVendBankOwnerBU
					from ownerBU in ljVendBankOwnerBU.DefaultIfEmpty()
					select new VendBankItemViewMap
					{
						CompanyGUID = vendBank.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = vendBank.Owner,
						OwnerBusinessUnitGUID = vendBank.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = vendBank.CreatedBy,
						CreatedDateTime = vendBank.CreatedDateTime,
						ModifiedBy = vendBank.ModifiedBy,
						ModifiedDateTime = vendBank.ModifiedDateTime,
						VendBankGUID = vendBank.VendBankGUID,
						BankAccount = vendBank.BankAccount,
						BankAccountName = vendBank.BankAccountName,
						BankBranch = vendBank.BankBranch,
						BankGroupGUID = vendBank.BankGroupGUID,
						VendorTableGUID = vendBank.VendorTableGUID,
						Primary = vendBank.Primary,
					
						RowVersion = vendBank.RowVersion,
					});
		}
		public VendBankItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.ToMap<VendBankItemViewMap, VendBankItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public VendBank CreateVendBank(VendBank vendBank)
		{
			try
			{
				vendBank.VendBankGUID = Guid.NewGuid();
				base.Add(vendBank);
				return vendBank;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateVendBankVoid(VendBank vendBank)
		{
			try
			{
				CreateVendBank(vendBank);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public VendBank UpdateVendBank(VendBank dbVendBank, VendBank inputVendBank, List<string> skipUpdateFields = null)
		{
			try
			{
				dbVendBank = dbVendBank.MapUpdateValues<VendBank>(inputVendBank);
				base.Update(dbVendBank);
				return dbVendBank;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateVendBankVoid(VendBank dbVendBank, VendBank inputVendBank, List<string> skipUpdateFields = null)
		{
			try
			{
				dbVendBank = dbVendBank.MapUpdateValues<VendBank>(inputVendBank, skipUpdateFields);
				base.Update(dbVendBank);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public override void ValidateAdd(VendBank item)
		{
			base.ValidateAdd(item);
			CheckPrimary(item);
		}
		public override void ValidateUpdate(VendBank item)
		{
			base.ValidateUpdate(item);
			CheckPrimary(item);
		}
		private void CheckPrimary(VendBank item)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				if (item.Primary)
				{
					bool isDupplicate  = Entity.Any(t => t.CompanyGUID == item.CompanyGUID &&
										 t.Primary == true &&
										 t.VendorTableGUID == item.VendorTableGUID &&
										 t.VendBankGUID != item.VendBankGUID);
					if (isDupplicate)
					{
						ex.AddData("ERROR.90060", "LABEL.PRIMARY",item.BankAccount);
					}
				}

				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public List<VendBank> GetVendBankPrimaryByVendorNoTracking(List<Guid> vendorTableGuids)
		{
			try
			{
				var result = Entity.Where(w => vendorTableGuids.Contains(w.VendorTableGUID)
												&& w.Primary == true)
									.AsNoTracking()
									.ToList();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

