using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICustTransRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CustTransItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<CustTransListView> GetListvw(SearchParameter search);
		CustTransItemView GetByIdvw(Guid id);
		CustTrans Find(params object[] keyValues);
		CustTrans GetCustTransByIdNoTracking(Guid guid);
		CustTrans CreateCustTrans(CustTrans custTrans);
		void CreateCustTransVoid(CustTrans custTrans);
		CustTrans UpdateCustTrans(CustTrans dbCustTrans, CustTrans inputCustTrans, List<string> skipUpdateFields = null);
		void UpdateCustTransVoid(CustTrans dbCustTrans, CustTrans inputCustTrans, List<string> skipUpdateFields = null);
		void Remove(CustTrans item);
		CustTrans GetCustTransByIdNoTrackingByAccessLevel(Guid guid);
		CustTrans GetCustTransByInvoiceTableNoTracking(Guid guid);
		IEnumerable<CustTrans> GetCustTransByInvoiceTableNoTracking(List<Guid> invoiceTableGUIDs);
		IEnumerable<CustTrans> GetCustTransByCompanyNoTracking(Guid guid);
		#region Shared
		public IQueryable<CustTrans> GetByIsProductInvoice();
		IEnumerable<CustTrans> GetCustTransCalcPDCInterestOutstanding(Guid guid);
		#endregion Shared
	}
	public class CustTransRepo : BranchCompanyBaseRepository<CustTrans>, ICustTransRepo
	{
		public CustTransRepo(SmartAppDbContext context) : base(context) { }
		public CustTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CustTrans GetCustTransByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CustTransGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CustTransItemViewMap> GetDropDownQuery()
		{
			return (from custTrans in Entity
					select new CustTransItemViewMap
					{
						CompanyGUID = custTrans.CompanyGUID,
						Owner = custTrans.Owner,
						OwnerBusinessUnitGUID = custTrans.OwnerBusinessUnitGUID,
						CustTransGUID = custTrans.CustTransGUID,
						InvoiceTableGUID = custTrans.InvoiceTableGUID,
						Description = custTrans.Description
					});
		}
		public IEnumerable<SelectItem<CustTransItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CustTrans>(search, db.GetAvailableBranch(), SysParm.CompanyGUID, SysParm.BranchGUID);
				var custTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CustTransItemViewMap, CustTransItemView>().ToDropDownItem(search.ExcludeRowData);


				return custTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CustTransListViewMap> GetListQuery()
		{

			return (from custTrans in Entity
					  join customerValue in db.Set<CustomerTable>()
					  on custTrans.CustomerTableGUID equals customerValue.CustomerTableGUID into ljcustomerValue
					  from customerValue in ljcustomerValue.DefaultIfEmpty()

					  join buyerValue in db.Set<BuyerTable>()
					  on custTrans.BuyerTableGUID equals buyerValue.BuyerTableGUID into ljbuyerValue
					  from buyerValue in ljbuyerValue.DefaultIfEmpty()

					  join currencyValue in db.Set<Currency>()
					  on custTrans.CurrencyGUID equals currencyValue.CurrencyGUID into ljcurrencyValue
					  from currencyValue in ljcurrencyValue.DefaultIfEmpty()

					  join invoiceType in db.Set<InvoiceType>()
					  on custTrans.InvoiceTypeGUID equals invoiceType.InvoiceTypeGUID into ljinvoiceType
					  from invoiceType in ljinvoiceType.DefaultIfEmpty()

					join creditApp in db.Set<CreditAppTable>()
					 on custTrans.CreditAppTableGUID equals creditApp.CreditAppTableGUID into ljcreditapp
					from creditApp in ljcreditapp.DefaultIfEmpty()

                    join invoice_Value in db.Set<InvoiceTable>()
                    on custTrans.InvoiceTableGUID equals invoice_Value.InvoiceTableGUID into ljinvoice
                    from invoice_Value in ljinvoice.DefaultIfEmpty()


                    select new CustTransListViewMap
					{
						CompanyGUID = custTrans.CompanyGUID,
						Owner = custTrans.Owner,
						OwnerBusinessUnitGUID = custTrans.OwnerBusinessUnitGUID,
						CustTransGUID = custTrans.CustTransGUID,
						InvoiceTableGUID = custTrans.InvoiceTableGUID,
						CustTransStatus = custTrans.CustTransStatus,
						CustomerTableGUID = custTrans.CustomerTableGUID,
						BuyerTableGUID = custTrans.BuyerTableGUID,
						ProductType = custTrans.ProductType,
						InvoiceTypeGUID = custTrans.InvoiceTypeGUID,
						DueDate = custTrans.DueDate,
						CurrencyGUID = custTrans.CurrencyGUID,
						TransAmount = custTrans.TransAmount,
						SettleAmount = custTrans.SettleAmount,
						InvoiceTable_Values = SmartAppUtil.GetDropDownLabel(invoice_Value.InvoiceId,invoice_Value.IssuedDate.DateToString()),
						InvoiceTable_InvoiceId = invoice_Value.InvoiceId,
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerValue.CustomerId, customerValue.Name),
						BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerValue.BuyerId, buyerValue.BuyerName),
						InvoiceType_Values = SmartAppUtil.GetDropDownLabel(invoiceType.InvoiceTypeId, invoiceType.Description),
						Currency_Values = SmartAppUtil.GetDropDownLabel(currencyValue.CurrencyId,currencyValue.Name),
						CreditApp_Values = SmartAppUtil.GetDropDownLabel(creditApp.CreditAppId, creditApp.Description),
						CustomerId=customerValue.CustomerId,
						CustomerName=customerValue.Name,
						CustomerTable_CustomerId = customerValue.CustomerId,
						BuyerTable_BuyerId = buyerValue.BuyerId,
						InvoiceType_InvoiceTypeId = invoiceType.InvoiceTypeId,
						Currency_CurrencyId = currencyValue.CurrencyId,
						CreditApp_CreditAppId = creditApp.CreditAppId
					});
		}
		public SearchResult<CustTransListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CustTransListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CustTrans>(search, db.GetAvailableBranch(), SysParm.CompanyGUID, SysParm.BranchGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CustTransListViewMap, CustTransListView>();
				result = list.SetSearchResult<CustTransListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CustTransItemViewMap> GetItemQuery()
		{
			return (from custTrans in Entity
					join company in db.Set<Company>()
					on custTrans.CompanyGUID equals company.CompanyGUID
					join branch in db.Set<Branch>()
					on custTrans.BranchGUID equals branch.BranchGUID
					join ownerBU in db.Set<BusinessUnit>()
					on custTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCustTransOwnerBU
					from ownerBU in ljCustTransOwnerBU.DefaultIfEmpty()

					join customerValue in db.Set<CustomerTable>()
					on custTrans.CustomerTableGUID equals customerValue.CustomerTableGUID into ljcustomerValue
					from customerValue in ljcustomerValue.DefaultIfEmpty()

					join buyerValue in db.Set<BuyerTable>()
					on custTrans.BuyerTableGUID equals buyerValue.BuyerTableGUID into ljbuyerValue
					from buyerValue in ljbuyerValue.DefaultIfEmpty()

					join currencyValue in db.Set<Currency>()
					on custTrans.CurrencyGUID equals currencyValue.CurrencyGUID into ljcurrencyValue
					from currencyValue in ljcurrencyValue.DefaultIfEmpty()

					join invoiceType in db.Set<InvoiceType>()
					on custTrans.InvoiceTypeGUID equals invoiceType.InvoiceTypeGUID into ljinvoiceType
					from invoiceType in ljinvoiceType.DefaultIfEmpty()

					join creditApp in db.Set<CreditAppTable>()
					on custTrans.CreditAppTableGUID equals creditApp.CreditAppTableGUID into ljcreditapp
					from creditApp in ljcreditapp.DefaultIfEmpty()

                    join invoice_Value in db.Set<InvoiceTable>()
					on custTrans.InvoiceTableGUID equals invoice_Value.InvoiceTableGUID into ljinvoice
                    from invoice_Value in ljinvoice.DefaultIfEmpty()

					join creditAppLine in db.Set<CreditAppLine>()
					on custTrans.CreditAppLineGUID equals creditAppLine.CreditAppLineGUID into ljcreditappline
					from creditAppLine in ljcreditappline.DefaultIfEmpty()

					select new CustTransItemViewMap
					{
						CompanyGUID = custTrans.CompanyGUID,
						CompanyId = company.CompanyId,
						BranchGUID = custTrans.BranchGUID,
						BranchId = branch.BranchId,
						Owner = custTrans.Owner,
						OwnerBusinessUnitGUID = custTrans.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = custTrans.CreatedBy,
						CreatedDateTime = custTrans.CreatedDateTime,
						ModifiedBy = custTrans.ModifiedBy,
						ModifiedDateTime = custTrans.ModifiedDateTime,
						CustTransGUID = custTrans.CustTransGUID,
						BuyerTableGUID = custTrans.BuyerTableGUID,
						CreditAppTableGUID = custTrans.CreditAppTableGUID,
						CurrencyGUID = custTrans.CurrencyGUID,
						CustomerTableGUID = custTrans.CustomerTableGUID,
						CustTransStatus = custTrans.CustTransStatus,
						Description = custTrans.Description,
						DueDate = custTrans.DueDate,
						ExchangeRate = custTrans.ExchangeRate,
						InvoiceTableGUID = custTrans.InvoiceTableGUID,
						InvoiceTypeGUID = custTrans.InvoiceTypeGUID,
						LastSettleDate = custTrans.LastSettleDate,
						ProductType = custTrans.ProductType,
						SettleAmount = custTrans.SettleAmount,
						SettleAmountMST = custTrans.SettleAmountMST,
						TransAmount = custTrans.TransAmount,
						TransAmountMST = custTrans.TransAmountMST,
						TransDate = custTrans.TransDate,
						CreditAppLineGUID = custTrans.CreditAppLineGUID,
						InvoiceTable_Values = SmartAppUtil.GetDropDownLabel(invoice_Value.InvoiceId,invoice_Value.IssuedDate.DateToString()),
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerValue.CustomerId,customerValue.Name),
						BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerValue.BuyerId,buyerValue.BuyerName),
						InvoiceType_Values = SmartAppUtil.GetDropDownLabel(invoiceType.InvoiceTypeId, invoiceType.Description),
						Currency_Values = SmartAppUtil.GetDropDownLabel(currencyValue.CurrencyId, currencyValue.Name),
						CreditApp_Value = SmartAppUtil.GetDropDownLabel(creditApp.CreditAppId,creditApp.Description),
						CreditAppLine_Value = SmartAppUtil.GetDropDownLabel(creditAppLine.LineNum.ToString(), creditAppLine.ApprovedCreditLimitLine.ToString()),
					
						RowVersion = custTrans.RowVersion,
					});
		}
		public CustTransItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<CustTransItemViewMap, CustTransItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public CustTrans CreateCustTrans(CustTrans custTrans)
		{
			try
			{
				custTrans.CustTransGUID = custTrans.CustTransGUID == Guid.Empty ? Guid.NewGuid() : custTrans.CustTransGUID;
				base.Add(custTrans);
				return custTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCustTransVoid(CustTrans custTrans)
		{
			try
			{
				CreateCustTrans(custTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CustTrans UpdateCustTrans(CustTrans dbCustTrans, CustTrans inputCustTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCustTrans = dbCustTrans.MapUpdateValues<CustTrans>(inputCustTrans);
				base.Update(dbCustTrans);
				return dbCustTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCustTransVoid(CustTrans dbCustTrans, CustTrans inputCustTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCustTrans = dbCustTrans.MapUpdateValues<CustTrans>(inputCustTrans, skipUpdateFields);
				base.Update(dbCustTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public CustTrans GetCustTransByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CustTransGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public CustTrans GetCustTransByInvoiceTableNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.InvoiceTableGUID == guid)
					.AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<CustTrans> GetCustTransByInvoiceTableNoTracking(List<Guid> invoiceTableGUIDs)
        {
            try
            {
				var result = Entity.Where(w => invoiceTableGUIDs.Contains(w.InvoiceTableGUID))
								.AsNoTracking()
								.ToList();
				return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public IEnumerable<CustTrans> GetCustTransByCompanyNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CompanyGUID == guid)
					.AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #region Shared
        public IQueryable<CustTrans> GetByIsProductInvoice()
		{
			try
			{
				var result = (from custTrans in Entity
							  join invoiceTable in db.Set<InvoiceTable>()
							  on custTrans.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID
							  where invoiceTable.ProductInvoice == true
							  select custTrans).AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		
		public IEnumerable<CustTrans> GetCustTransCalcPDCInterestOutstanding(Guid guid)
		{
			try
			{
				IEnumerable<CustTrans> custTrans = (from withdrawalTable in db.Set<WithdrawalTable>()
													join withdrawalLine in db.Set<WithdrawalLine>() on withdrawalTable.WithdrawalTableGUID equals withdrawalLine.WithdrawalTableGUID
													join custTran in Entity on withdrawalLine.WithdrawalLineInvoiceTableGUID equals custTran.InvoiceTableGUID
													where (withdrawalTable.WithdrawalTableGUID == guid || withdrawalTable.OriginalWithdrawalTableGUID == guid)
													&& withdrawalLine.WithdrawalLineType == (int)WithdrawalLineType.Interest && custTran.CustTransStatus == (int)CustTransStatus.Open
													select custTran).AsNoTracking().ToList();
				return custTrans;
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Shared
	}
}

