using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IPropertyTypeRepo
	{
		#region DropDown
		IEnumerable<SelectItem<PropertyTypeItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<PropertyTypeListView> GetListvw(SearchParameter search);
		PropertyTypeItemView GetByIdvw(Guid id);
		PropertyType Find(params object[] keyValues);
		PropertyType GetPropertyTypeByIdNoTracking(Guid guid);
		PropertyType CreatePropertyType(PropertyType propertyType);
		void CreatePropertyTypeVoid(PropertyType propertyType);
		PropertyType UpdatePropertyType(PropertyType dbPropertyType, PropertyType inputPropertyType, List<string> skipUpdateFields = null);
		void UpdatePropertyTypeVoid(PropertyType dbPropertyType, PropertyType inputPropertyType, List<string> skipUpdateFields = null);
		void Remove(PropertyType item);
	}
	public class PropertyTypeRepo : CompanyBaseRepository<PropertyType>, IPropertyTypeRepo
	{
		public PropertyTypeRepo(SmartAppDbContext context) : base(context) { }
		public PropertyTypeRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public PropertyType GetPropertyTypeByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.PropertyTypeGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<PropertyTypeItemViewMap> GetDropDownQuery()
		{
			return (from propertyType in Entity
					select new PropertyTypeItemViewMap
					{
						CompanyGUID = propertyType.CompanyGUID,
						Owner = propertyType.Owner,
						OwnerBusinessUnitGUID = propertyType.OwnerBusinessUnitGUID,
						PropertyTypeGUID = propertyType.PropertyTypeGUID,
						PropertyTypeId = propertyType.PropertyTypeId,
						Description = propertyType.Description
					});
		}
		public IEnumerable<SelectItem<PropertyTypeItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<PropertyType>(search, SysParm.CompanyGUID);
				var propertyType = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<PropertyTypeItemViewMap, PropertyTypeItemView>().ToDropDownItem(search.ExcludeRowData);


				return propertyType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<PropertyTypeListViewMap> GetListQuery()
		{
			return (from propertyType in Entity
				select new PropertyTypeListViewMap
				{
						CompanyGUID = propertyType.CompanyGUID,
						Owner = propertyType.Owner,
						OwnerBusinessUnitGUID = propertyType.OwnerBusinessUnitGUID,
						PropertyTypeGUID = propertyType.PropertyTypeGUID,
						PropertyTypeId = propertyType.PropertyTypeId,
						Description = propertyType.Description
				});
		}
		public SearchResult<PropertyTypeListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<PropertyTypeListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<PropertyType>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<PropertyTypeListViewMap, PropertyTypeListView>();
				result = list.SetSearchResult<PropertyTypeListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<PropertyTypeItemViewMap> GetItemQuery()
		{
			return (from propertyType in Entity
					join company in db.Set<Company>()
					on propertyType.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on propertyType.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljPropertyTypeOwnerBU
					from ownerBU in ljPropertyTypeOwnerBU.DefaultIfEmpty()
					select new PropertyTypeItemViewMap
					{
						CompanyGUID = propertyType.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = propertyType.Owner,
						OwnerBusinessUnitGUID = propertyType.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = propertyType.CreatedBy,
						CreatedDateTime = propertyType.CreatedDateTime,
						ModifiedBy = propertyType.ModifiedBy,
						ModifiedDateTime = propertyType.ModifiedDateTime,
						PropertyTypeGUID = propertyType.PropertyTypeGUID,
						Description = propertyType.Description,
						PropertyTypeId = propertyType.PropertyTypeId,
					
						RowVersion = propertyType.RowVersion,
					});
		}
		public PropertyTypeItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<PropertyTypeItemViewMap, PropertyTypeItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public PropertyType CreatePropertyType(PropertyType propertyType)
		{
			try
			{
				propertyType.PropertyTypeGUID = Guid.NewGuid();
				base.Add(propertyType);
				return propertyType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreatePropertyTypeVoid(PropertyType propertyType)
		{
			try
			{
				CreatePropertyType(propertyType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public PropertyType UpdatePropertyType(PropertyType dbPropertyType, PropertyType inputPropertyType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbPropertyType = dbPropertyType.MapUpdateValues<PropertyType>(inputPropertyType);
				base.Update(dbPropertyType);
				return dbPropertyType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdatePropertyTypeVoid(PropertyType dbPropertyType, PropertyType inputPropertyType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbPropertyType = dbPropertyType.MapUpdateValues<PropertyType>(inputPropertyType, skipUpdateFields);
				base.Update(dbPropertyType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

