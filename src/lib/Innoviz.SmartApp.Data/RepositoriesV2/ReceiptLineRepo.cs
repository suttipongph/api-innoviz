using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IReceiptLineRepo
	{
		#region DropDown
		IEnumerable<SelectItem<ReceiptLineItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<ReceiptLineListView> GetListvw(SearchParameter search);
		ReceiptLineItemView GetByIdvw(Guid id);
		ReceiptLine Find(params object[] keyValues);
		ReceiptLine GetReceiptLineByIdNoTracking(Guid guid);
		ReceiptLine CreateReceiptLine(ReceiptLine receiptLine);
		void CreateReceiptLineVoid(ReceiptLine receiptLine);
		ReceiptLine UpdateReceiptLine(ReceiptLine dbReceiptLine, ReceiptLine inputReceiptLine, List<string> skipUpdateFields = null);
		void UpdateReceiptLineVoid(ReceiptLine dbReceiptLine, ReceiptLine inputReceiptLine, List<string> skipUpdateFields = null);
		void Remove(ReceiptLine item);
		ReceiptLine GetReceiptLineByIdNoTrackingByAccessLevel(Guid guid);
	}
	public class ReceiptLineRepo : BranchCompanyBaseRepository<ReceiptLine>, IReceiptLineRepo
	{
		public ReceiptLineRepo(SmartAppDbContext context) : base(context) { }
		public ReceiptLineRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ReceiptLine GetReceiptLineByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ReceiptLineGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<ReceiptLineItemViewMap> GetDropDownQuery()
		{
			return (from receiptLine in Entity
					select new ReceiptLineItemViewMap
					{
						CompanyGUID = receiptLine.CompanyGUID,
						Owner = receiptLine.Owner,
						OwnerBusinessUnitGUID = receiptLine.OwnerBusinessUnitGUID,
						ReceiptLineGUID = receiptLine.ReceiptLineGUID
					});
		}
		public IEnumerable<SelectItem<ReceiptLineItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<ReceiptLine>(search, db.GetAvailableBranch(), SysParm.CompanyGUID, SysParm.BranchGUID);
				var receiptLine = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<ReceiptLineItemViewMap, ReceiptLineItemView>().ToDropDownItem(search.ExcludeRowData);


				return receiptLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<ReceiptLineListViewMap> GetListQuery()
		{
			return (from receiptLine in Entity
					join invoiceTable in db.Set<InvoiceTable>()
					on receiptLine.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljinvoiceTable
					from invoiceTable in ljinvoiceTable.DefaultIfEmpty()
					select new ReceiptLineListViewMap
				{
						CompanyGUID = receiptLine.CompanyGUID,
						Owner = receiptLine.Owner,
						OwnerBusinessUnitGUID = receiptLine.OwnerBusinessUnitGUID,
						ReceiptLineGUID = receiptLine.ReceiptLineGUID,
						LineNum = receiptLine.LineNum,
						InvoiceTableGUID = receiptLine.InvoiceTableGUID,
						InvoiceAmount = receiptLine.InvoiceAmount,
						SettleBaseAmount = receiptLine.SettleBaseAmount,
						SettleTaxAmount = receiptLine.SettleTaxAmount,
						SettleAmount = receiptLine.SettleAmount,
						WHTAmount = receiptLine.WHTAmount,
						ReceiptTableGUID = receiptLine.ReceiptTableGUID,
						InvoiceTable_Values =  SmartAppUtil.GetDropDownLabel(invoiceTable.InvoiceId, invoiceTable.IssuedDate.DateToString()),
						InvoiceTable_InvoiceId = invoiceTable.InvoiceId
					});
		}
		public SearchResult<ReceiptLineListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<ReceiptLineListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<ReceiptLine>(search, db.GetAvailableBranch(), SysParm.CompanyGUID, SysParm.BranchGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<ReceiptLineListViewMap, ReceiptLineListView>();
				result = list.SetSearchResult<ReceiptLineListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<ReceiptLineItemViewMap> GetItemQuery()
		{
			var result = (from receiptLine in Entity
						  join company in db.Set<Company>()
						  on receiptLine.CompanyGUID equals company.CompanyGUID
						  join branch in db.Set<Branch>()
						  on receiptLine.BranchGUID equals branch.BranchGUID
						  join ownerBU in db.Set<BusinessUnit>()
						  on receiptLine.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljReceiptLineOwnerBU
						  from ownerBU in ljReceiptLineOwnerBU.DefaultIfEmpty()
						  join receiptTable in db.Set<ReceiptTable>()
						  on receiptLine.ReceiptTableGUID equals receiptTable.ReceiptTableGUID into ljreceiptTable
						  from receiveTable in ljreceiptTable.DefaultIfEmpty()
						  join invoiceTable in db.Set<InvoiceTable>()
						  on receiptLine.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljinvoiceTable
						  from invoiceTable in ljinvoiceTable.DefaultIfEmpty()
						  join taxTable in db.Set<TaxTable>()
						  on receiptLine.TaxTableGUID equals taxTable.TaxTableGUID into ljtaxTable
						  from taxTable in ljtaxTable.DefaultIfEmpty()
						  join withholding in db.Set<WithholdingTaxTable>()
						  on receiptLine.WithholdingTaxTableGUID equals withholding.WithholdingTaxTableGUID into ljwithholding
						  from withholding in ljwithholding.DefaultIfEmpty()
						  select new ReceiptLineItemViewMap
						  {
							  CompanyGUID = receiptLine.CompanyGUID,
							  CompanyId = company.CompanyId,
							  BranchGUID = receiptLine.BranchGUID,
							  BranchId = branch.BranchId,
							  Owner = receiptLine.Owner,
							  OwnerBusinessUnitGUID = receiptLine.OwnerBusinessUnitGUID,
							  OwnerBusinessUnitId = ownerBU.BusinessUnitId,
							  CreatedBy = receiptLine.CreatedBy,
							  CreatedDateTime = receiptLine.CreatedDateTime,
							  ModifiedBy = receiptLine.ModifiedBy,
							  ModifiedDateTime = receiptLine.ModifiedDateTime,
							  ReceiptLineGUID = receiptLine.ReceiptLineGUID,
							  DueDate = receiptLine.DueDate,
							  InvoiceAmount = receiptLine.InvoiceAmount,
							  InvoiceTableGUID = receiptLine.InvoiceTableGUID,
							  LineNum = receiptLine.LineNum,
							  ReceiptTableGUID = receiptLine.ReceiptTableGUID,
							  SettleAmount = receiptLine.SettleAmount,
							  SettleAmountMST = receiptLine.SettleAmountMST,
							  SettleBaseAmount = receiptLine.SettleBaseAmount,
							  SettleBaseAmountMST = receiptLine.SettleBaseAmountMST,
							  SettleTaxAmount = receiptLine.SettleTaxAmount,
							  SettleTaxAmountMST = receiptLine.SettleTaxAmountMST,
							  TaxAmount = receiptLine.TaxAmount,
							  TaxTableGUID = receiptLine.TaxTableGUID,
							  WHTAmount = receiptLine.WHTAmount,
							  WithholdingTaxTableGUID = receiptLine.WithholdingTaxTableGUID,
							  ReceiptTable_Values = SmartAppUtil.GetDropDownLabel(receiveTable.ReceiptId, receiveTable.ReceiptDate.DateToString()),
							  InvoiceTable_Values = SmartAppUtil.GetDropDownLabel(invoiceTable.InvoiceId, invoiceTable.IssuedDate.DateToString()),
							  TaxTable_Code = taxTable.TaxCode,
							  WithholdingTaxTable_Code = withholding.WHTCode,
							 
							  RowVersion = receiptLine.RowVersion,
						  });
			return result;
		}
		public ReceiptLineItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<ReceiptLineItemViewMap, ReceiptLineItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public ReceiptLine CreateReceiptLine(ReceiptLine receiptLine)
		{
			try
			{
				receiptLine.ReceiptLineGUID = Guid.NewGuid();
				base.Add(receiptLine);
				return receiptLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateReceiptLineVoid(ReceiptLine receiptLine)
		{
			try
			{
				CreateReceiptLine(receiptLine);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ReceiptLine UpdateReceiptLine(ReceiptLine dbReceiptLine, ReceiptLine inputReceiptLine, List<string> skipUpdateFields = null)
		{
			try
			{
				dbReceiptLine = dbReceiptLine.MapUpdateValues<ReceiptLine>(inputReceiptLine);
				base.Update(dbReceiptLine);
				return dbReceiptLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateReceiptLineVoid(ReceiptLine dbReceiptLine, ReceiptLine inputReceiptLine, List<string> skipUpdateFields = null)
		{
			try
			{
				dbReceiptLine = dbReceiptLine.MapUpdateValues<ReceiptLine>(inputReceiptLine, skipUpdateFields);
				base.Update(dbReceiptLine);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public ReceiptLine GetReceiptLineByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ReceiptLineGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}
