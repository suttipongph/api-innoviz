using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICompanySignatureRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CompanySignatureItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<CompanySignatureListView> GetListvw(SearchParameter search);
		CompanySignatureItemView GetByIdvw(Guid id);
		CompanySignature Find(params object[] keyValues);
		CompanySignature GetCompanySignatureByIdNoTracking(Guid guid);
		CompanySignature CreateCompanySignature(CompanySignature companySignature);
		void CreateCompanySignatureVoid(CompanySignature companySignature);
		CompanySignature UpdateCompanySignature(CompanySignature dbCompanySignature, CompanySignature inputCompanySignature, List<string> skipUpdateFields = null);
		void UpdateCompanySignatureVoid(CompanySignature dbCompanySignature, CompanySignature inputCompanySignature, List<string> skipUpdateFields = null);
		void Remove(CompanySignature item);
		CompanySignature GetCompanySignatureByIdNoTrackingByAccessLevel(Guid guid);
		IQueryable<CompanySignature> GetByRefTypeAndBranch(int refType, string branch);
	}
	public class CompanySignatureRepo : CompanyBaseRepository<CompanySignature>, ICompanySignatureRepo
	{
		public CompanySignatureRepo(SmartAppDbContext context) : base(context) { }
		public CompanySignatureRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CompanySignature GetCompanySignatureByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CompanySignatureGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CompanySignatureItemViewMap> GetDropDownQuery()
		{
			return (from companySignature in Entity
					join employeeTable in db.Set<EmployeeTable>()
					on companySignature.EmployeeTableGUID equals employeeTable.EmployeeTableGUID
					select new CompanySignatureItemViewMap
					{
						CompanyGUID = companySignature.CompanyGUID,
						Owner = companySignature.Owner,
						OwnerBusinessUnitGUID = companySignature.OwnerBusinessUnitGUID,
						CompanySignatureGUID = companySignature.CompanySignatureGUID,
						EmployeeTable_EmployeeId = employeeTable.EmployeeId,
						EmployeeTable_Name = employeeTable.Name
					});
		}
		public IEnumerable<SelectItem<CompanySignatureItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
                var predicate = base.GetFilterLevelPredicate<CompanySignature>(search, db.GetAvailableBranch(), SysParm.CompanyGUID, SysParm.BranchGUID); 
                var companySignature = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CompanySignatureItemViewMap, CompanySignatureItemView>().ToDropDownItem(search.ExcludeRowData);


                return companySignature;
            }
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CompanySignatureListViewMap> GetListQuery()
		{
			return (from companySignature in Entity
                    join employeeTable in db.Set<EmployeeTable>()
                    on companySignature.EmployeeTableGUID equals employeeTable.EmployeeTableGUID into ljEmployeeCompany
                    from employeeTable in ljEmployeeCompany.DefaultIfEmpty()
                    join branch in db.Set<Branch>()
                    on companySignature.BranchGUID equals branch.BranchGUID
					select new CompanySignatureListViewMap
				{
						CompanyGUID = companySignature.CompanyGUID,
						Owner = companySignature.Owner,
						OwnerBusinessUnitGUID = companySignature.OwnerBusinessUnitGUID,
						CompanySignatureGUID = companySignature.CompanySignatureGUID,
						EmployeeTableGUID = companySignature.EmployeeTableGUID,
						Ordering = companySignature.Ordering,
						RefType = companySignature.RefType,
                        BranchGUID = branch.BranchGUID,
						Branch_BranchId = branch.BranchId,
						Branch_Values = SmartAppUtil.GetDropDownLabel(branch.BranchId,branch.Name),
                        EmployeeTable_EmployeeId = employeeTable.EmployeeId,
                        EmployeeTable_Name = employeeTable.Name
                    });
		}
		public SearchResult<CompanySignatureListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CompanySignatureListView>();
			try
			{
                var predicate = base.GetFilterLevelPredicate<CompanySignature>(search, db.GetAvailableBranch() ,SysParm.CompanyGUID, SysParm.BranchGUID);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<CompanySignatureListViewMap, CompanySignatureListView>();
                result = list.SetSearchResult<CompanySignatureListView>(total, search);
                return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CompanySignatureItemViewMap> GetItemQuery()
		{
			return (from companySignature in Entity
					join company in db.Set<Company>()
					on companySignature.CompanyGUID equals company.CompanyGUID
					join branch in db.Set<Branch>()
					on companySignature.BranchGUID equals branch.BranchGUID
					join ownerBU in db.Set<BusinessUnit>()
					on companySignature.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCompanySignatureOwnerBU
					from ownerBU in ljCompanySignatureOwnerBU.DefaultIfEmpty()
					select new CompanySignatureItemViewMap
					{
						CompanyGUID = companySignature.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = companySignature.Owner,
						OwnerBusinessUnitGUID = companySignature.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = companySignature.CreatedBy,
						CreatedDateTime = companySignature.CreatedDateTime,
						ModifiedBy = companySignature.ModifiedBy,
						ModifiedDateTime = companySignature.ModifiedDateTime,
						CompanySignatureGUID = companySignature.CompanySignatureGUID,
						EmployeeTableGUID = companySignature.EmployeeTableGUID,
						Ordering = companySignature.Ordering,
						RefType = companySignature.RefType,
						BranchGUID = branch.BranchGUID,
						Branch_Values = SmartAppUtil.GetDropDownLabel(branch.BranchId, branch.Name),
						DefaultBranchGUID = companySignature.BranchGUID, 
					
						RowVersion = companySignature.RowVersion,
					});
		}
		public CompanySignatureItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<CompanySignatureItemViewMap, CompanySignatureItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public CompanySignature CreateCompanySignature(CompanySignature companySignature)
		{
			try
			{
				companySignature.CompanySignatureGUID = Guid.NewGuid();
				base.Add(companySignature);
				return companySignature;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCompanySignatureVoid(CompanySignature companySignature)
		{
			try
			{
				CreateCompanySignature(companySignature);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CompanySignature UpdateCompanySignature(CompanySignature dbCompanySignature, CompanySignature inputCompanySignature, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCompanySignature = dbCompanySignature.MapUpdateValues<CompanySignature>(inputCompanySignature);
				base.Update(dbCompanySignature);
				return dbCompanySignature;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCompanySignatureVoid(CompanySignature dbCompanySignature, CompanySignature inputCompanySignature, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCompanySignature = dbCompanySignature.MapUpdateValues<CompanySignature>(inputCompanySignature, skipUpdateFields);
				base.Update(dbCompanySignature);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public CompanySignature GetCompanySignatureByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CompanySignatureGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IQueryable<CompanySignature> GetByRefTypeAndBranch(int refType, string branch)
		{
			try
			{
				Guid branchGUID = branch.StringToGuid();
				return Entity.Where(w => w.RefType == refType && w.BranchGUID == branchGUID);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

