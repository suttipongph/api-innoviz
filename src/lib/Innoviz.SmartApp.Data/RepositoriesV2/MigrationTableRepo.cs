using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface IMigrationTableRepo
    {
        #region DropDown
        IEnumerable<SelectItem<MigrationTableItemView>> GetDropDownItem(SearchParameter search);
        #endregion DropDown
        SearchResult<MigrationTableListView> GetListvw(SearchParameter search);
        MigrationTableItemView GetByIdvw(Guid guid);
        void Update(MigrationTable item);
        void ExecuteSSISPackage(string packageName);
        void GenerateDummyRecord(string companyGUID);
        MigrationTable Find(params object[] keyValues);
    }
    public class MigrationTableRepo : BaseRepository<MigrationTable>, IMigrationTableRepo
    {
        public MigrationTableRepo(SmartAppDbContext context) : base(context) { }
        public MigrationTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }

        #region DropDown
        private IQueryable<MigrationTableItemViewMap> GetDropDownQuery()
        {
            return (from migration in Entity
                    select new MigrationTableItemViewMap
                    {
                        GroupName = migration.GroupName,
                        GroupOrder = migration.GroupOrder,
                        MigrationName = migration.MigrateName,
                        MigrationOrder = migration.MigrateOrder,
                        MigrationResult = migration.MigrateResult,
                        MigrationTableGUID = migration.MigrationTableGUID,
                        PackageName = migration.PackageName
                    }).OrderBy(o => o.GroupOrder).ThenBy(t => t.MigrationOrder);
        }
        public IEnumerable<SelectItem<MigrationTableItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<MigrationTable>(search);
                var methodOfPayment = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<MigrationTableItemViewMap, MigrationTableItemView>().ToDropDownItem(search.ExcludeRowData);


                return methodOfPayment;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region GetListvw
        public IQueryable<MigrationTableListViewMap> GetListQuery()
        {
            try
            {
                var result = (from migration in Entity
                              select new MigrationTableListViewMap
                              {
                                  CreatedBy = migration.CreatedBy,
                                  CreatedDateTime = migration.CreatedDateTime,
                                  ModifiedBy = migration.ModifiedBy,
                                  ModifiedDateTime = migration.ModifiedDateTime,
                                  GroupName = migration.GroupName,
                                  GroupOrder = migration.GroupOrder,
                                  MigrationName = migration.MigrateName,
                                  MigrationOrder = migration.MigrateOrder,
                                  MigrationResult = migration.MigrateResult,
                                  MigrationTableGUID = migration.MigrationTableGUID,
                                  PackageName = migration.PackageName
                              });
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SearchResult<MigrationTableListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<MigrationTableListView>();
            try
            {
                var predicate = search.GetPredicate_entity<MigrationTable>();
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<MigrationTableListViewMap, MigrationTableListView>();
                result = list.SetSearchResult<MigrationTableListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        public IQueryable<MigrationTableItemViewMap> GetItemQuery()
        {
            try
            {
                return (from migration in Entity
                        select new MigrationTableItemViewMap
                        {
                            CreatedBy = migration.CreatedBy,
                            CreatedDateTime = migration.CreatedDateTime,
                            ModifiedBy = migration.ModifiedBy,
                            ModifiedDateTime = migration.ModifiedDateTime,
                            GroupName = migration.GroupName,
                            GroupOrder = migration.GroupOrder,
                            MigrationName = migration.MigrateName,
                            MigrationOrder = migration.MigrateOrder,
                            MigrationResult = migration.MigrateResult,
                            MigrationTableGUID = migration.MigrationTableGUID,
                            PackageName = migration.PackageName,
                        
                            RowVersion = migration.RowVersion,
                        });
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public MigrationTableItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<MigrationTableItemViewMap, MigrationTableItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        public void ExecuteSSISPackage(string packageName)
        {
            try
            {
                string connectionString = db.Database.GetDbConnection().ConnectionString;
                string procName = "LSS_ExecuteSsisPackage";
                int result = -1;

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(procName, connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    SqlParameter packageNameString = new SqlParameter("@PackageName", SqlDbType.NVarChar);
                    SqlParameter pacakgeFolderString = new SqlParameter("@PackageFolder", SqlDbType.NVarChar);
                    packageNameString.Value = packageName;
                    pacakgeFolderString.Value = "Migration:PackageFolder".GetConfigurationValue(); 

                    command.Parameters.Add(packageNameString);
                    command.Parameters.Add(pacakgeFolderString);

                    connection.Open();

                    result = command.ExecuteNonQuery();

                    connection.Close();
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void GenerateDummyRecord(string companyGUID)
        {
            try
            {
                string connectionString = db.Database.GetDbConnection().ConnectionString;
                string procName = "LSS_GenerateDummyRecord";
                int result = -1;

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(procName, connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    SqlParameter companyGUIDString = new SqlParameter("@CompanyGUID", SqlDbType.NVarChar);
                    companyGUIDString.Value = companyGUID;

                    command.Parameters.Add(companyGUIDString);

                    connection.Open();

                    result = command.ExecuteNonQuery();

                    connection.Close();
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}

