using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IVendorPaymentTransRepo
	{
		#region DropDown
		IEnumerable<SelectItem<VendorPaymentTransItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<VendorPaymentTransListView> GetListvw(SearchParameter search);
		VendorPaymentTransItemView GetByIdvw(Guid id);
		VendorPaymentTrans Find(params object[] keyValues);
		VendorPaymentTrans GetVendorPaymentTransByIdNoTracking(Guid guid);
		VendorPaymentTrans CreateVendorPaymentTrans(VendorPaymentTrans vendorPaymentTrans);
		void CreateVendorPaymentTransVoid(VendorPaymentTrans vendorPaymentTrans);
		VendorPaymentTrans UpdateVendorPaymentTrans(VendorPaymentTrans dbVendorPaymentTrans, VendorPaymentTrans inputVendorPaymentTrans, List<string> skipUpdateFields = null);
		void UpdateVendorPaymentTransVoid(VendorPaymentTrans dbVendorPaymentTrans, VendorPaymentTrans inputVendorPaymentTrans, List<string> skipUpdateFields = null);
		void Remove(VendorPaymentTrans item);
		VendorPaymentTrans GetVendorPaymentTransByIdNoTrackingByAccessLevel(Guid guid);
		List<VendorPaymentTrans> GetVendorPaymentTransByProcessTransNoTracking(List<Guid> refGUIDs);
	}
	public class VendorPaymentTransRepo : CompanyBaseRepository<VendorPaymentTrans>, IVendorPaymentTransRepo
	{
		public VendorPaymentTransRepo(SmartAppDbContext context) : base(context) { }
		public VendorPaymentTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public VendorPaymentTrans GetVendorPaymentTransByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.VendorPaymentTransGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<VendorPaymentTransItemViewMap> GetDropDownQuery()
		{
			return (from vendorPaymentTrans in Entity
					join venDor in db.Set<VendorTable>() on vendorPaymentTrans.VendorTableGUID equals venDor.VendorTableGUID into ljIntroducedByvenDor
					from venDor in ljIntroducedByvenDor.DefaultIfEmpty()
					select new VendorPaymentTransItemViewMap
					{
						CompanyGUID = vendorPaymentTrans.CompanyGUID,
						Owner = vendorPaymentTrans.Owner,
						OwnerBusinessUnitGUID = vendorPaymentTrans.OwnerBusinessUnitGUID,
						VendorPaymentTransGUID = vendorPaymentTrans.VendorPaymentTransGUID,
						VendorTable_VendorId = venDor.VendorId,
						TotalAmount = vendorPaymentTrans.TotalAmount
					});
		}
		public IEnumerable<SelectItem<VendorPaymentTransItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<VendorPaymentTrans>(search, SysParm.CompanyGUID);
				var vendorPaymentTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<VendorPaymentTransItemViewMap, VendorPaymentTransItemView>().ToDropDownItem(search.ExcludeRowData);


				return vendorPaymentTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<VendorPaymentTransListViewMap> GetListQuery()
		{
			return (from vendorPaymentTrans in Entity
					join vendorTable in db.Set<VendorTable>()
					on vendorPaymentTrans.VendorTableGUID equals vendorTable.VendorTableGUID into lj1
					from vendorTable in lj1.DefaultIfEmpty()
					join documentStatus in db.Set<DocumentStatus>()
					on vendorPaymentTrans.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
					join creditAppTable in db.Set<CreditAppTable>()
					on vendorPaymentTrans.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljcreditAppTable
					from creditAppTable in ljcreditAppTable.DefaultIfEmpty()
					select new VendorPaymentTransListViewMap
				{
						CompanyGUID = vendorPaymentTrans.CompanyGUID,
						Owner = vendorPaymentTrans.Owner,
						OwnerBusinessUnitGUID = vendorPaymentTrans.OwnerBusinessUnitGUID,
						VendorPaymentTransGUID = vendorPaymentTrans.VendorPaymentTransGUID,
						CreditAppTableGUID = vendorPaymentTrans.CreditAppTableGUID,
						VendorTableGUID = vendorPaymentTrans.VendorTableGUID,
						PaymentDate = vendorPaymentTrans.PaymentDate,
						OffsetAccount = vendorPaymentTrans.OffsetAccount,
						TotalAmount = vendorPaymentTrans.TotalAmount,
						DocumentStatusGUID = vendorPaymentTrans.DocumentStatusGUID,
						CreditAppTable_Values = (creditAppTable != null) ? SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description) : null,
						CreditAppTable_CreditAppId = (creditAppTable != null) ? creditAppTable.CreditAppId : null,
						VendorTable_Values = (vendorTable == null) ? null : SmartAppUtil.GetDropDownLabel(vendorTable.VendorId, vendorTable.Name),
						VendorTable_VendorId = (vendorTable == null) ? null : vendorTable.VendorId,
						DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
						DocumentStatus_StatusId = documentStatus.StatusId,
						RefGUID = vendorPaymentTrans.RefGUID
				});
		}
		public SearchResult<VendorPaymentTransListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<VendorPaymentTransListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<VendorPaymentTrans>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<VendorPaymentTransListViewMap, VendorPaymentTransListView>();
				result = list.SetSearchResult<VendorPaymentTransListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<VendorPaymentTransItemViewMap> GetItemQuery()
		{
			return (from vendorPaymentTrans in Entity
					join company in db.Set<Company>()
					on vendorPaymentTrans.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on vendorPaymentTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljVendorPaymentTransOwnerBU
					from ownerBU in ljVendorPaymentTransOwnerBU.DefaultIfEmpty()
					join creditAppTable in db.Set<CreditAppTable>() 
					on vendorPaymentTrans.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljcreditAppTable
					from creditAppTable in ljcreditAppTable.DefaultIfEmpty()
					join venDor in db.Set<VendorTable>() 
					on vendorPaymentTrans.VendorTableGUID equals venDor.VendorTableGUID into ljIntroducedByvenDor
					from venDor in ljIntroducedByvenDor.DefaultIfEmpty()						
					join documentStatus in db.Set<DocumentStatus>() on vendorPaymentTrans.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
					join ledgerDimension1 in db.Set<LedgerDimension>()
					on vendorPaymentTrans.Dimension1 equals ledgerDimension1.LedgerDimensionGUID into ljledgerDimension1
					from ledgerDimension1 in ljledgerDimension1.DefaultIfEmpty()
					join ledgerDimension2 in db.Set<LedgerDimension>()
					on vendorPaymentTrans.Dimension2 equals ledgerDimension2.LedgerDimensionGUID into ljledgerDimension2
					from ledgerDimension2 in ljledgerDimension2.DefaultIfEmpty()
					join ledgerDimension3 in db.Set<LedgerDimension>()
					on vendorPaymentTrans.Dimension3 equals ledgerDimension3.LedgerDimensionGUID into ljledgerDimension3
					from ledgerDimension3 in ljledgerDimension3.DefaultIfEmpty()		
					join ledgerDimension4 in db.Set<LedgerDimension>()
					on vendorPaymentTrans.Dimension4 equals ledgerDimension4.LedgerDimensionGUID into ljledgerDimension4
					from ledgerDimension4 in ljledgerDimension4.DefaultIfEmpty()
					join ledgerDimension5 in db.Set<LedgerDimension>()
					on vendorPaymentTrans.Dimension5 equals ledgerDimension5.LedgerDimensionGUID into ljledgerDimension5
					from ledgerDimension5 in ljledgerDimension5.DefaultIfEmpty()

					join withdrawalTable in db.Set<WithdrawalTable>()
					on vendorPaymentTrans.RefGUID equals withdrawalTable.WithdrawalTableGUID into ljWithdrawalTable
					from withdrawalTable in ljWithdrawalTable.DefaultIfEmpty()
					join customerRefundTable in db.Set<CustomerRefundTable>()
					on vendorPaymentTrans.RefGUID equals customerRefundTable.CustomerRefundTableGUID into ljcustomerRefunTable
					from customerRefundTable in ljcustomerRefunTable.DefaultIfEmpty()
					join purchaseTable in db.Set<PurchaseTable>()
					on vendorPaymentTrans.RefGUID equals purchaseTable.PurchaseTableGUID into ljPurchaseTable
					from purchaseTable in ljPurchaseTable.DefaultIfEmpty()
					select new VendorPaymentTransItemViewMap
					{
						CompanyGUID = vendorPaymentTrans.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = vendorPaymentTrans.Owner,
						OwnerBusinessUnitGUID = vendorPaymentTrans.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = vendorPaymentTrans.CreatedBy,
						CreatedDateTime = vendorPaymentTrans.CreatedDateTime,
						ModifiedBy = vendorPaymentTrans.ModifiedBy,
						ModifiedDateTime = vendorPaymentTrans.ModifiedDateTime,
						VendorPaymentTransGUID = vendorPaymentTrans.VendorPaymentTransGUID,
						AmountBeforeTax = vendorPaymentTrans.AmountBeforeTax,
						CreditAppTableGUID = vendorPaymentTrans.CreditAppTableGUID,
						Dimension1 = vendorPaymentTrans.Dimension1,
						Dimension2 = vendorPaymentTrans.Dimension2,
						Dimension3 = vendorPaymentTrans.Dimension3,
						Dimension4 = vendorPaymentTrans.Dimension4,
						Dimension5 = vendorPaymentTrans.Dimension5,
						DocumentStatusGUID = vendorPaymentTrans.DocumentStatusGUID,
						OffsetAccount = vendorPaymentTrans.OffsetAccount,
						PaymentDate = vendorPaymentTrans.PaymentDate,
						RefGUID = vendorPaymentTrans.RefGUID,
						RefType = vendorPaymentTrans.RefType,
						TaxAmount = vendorPaymentTrans.TaxAmount,
						TaxTableGUID = vendorPaymentTrans.TaxTableGUID,
						TotalAmount = vendorPaymentTrans.TotalAmount,
						VendorTableGUID = vendorPaymentTrans.VendorTableGUID,
						CreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
						VendorTable_Values = SmartAppUtil.GetDropDownLabel(venDor.VendorId, venDor.Name),
						DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
						Dimension1_Values = (vendorPaymentTrans.Dimension1 != null) ? SmartAppUtil.GetDropDownLabel(ledgerDimension1.DimensionCode, ledgerDimension1.Description) : null,
						Dimension2_Values = (vendorPaymentTrans.Dimension2 != null) ? SmartAppUtil.GetDropDownLabel(ledgerDimension2.DimensionCode, ledgerDimension2.Description) : null,
						Dimension3_Values = (vendorPaymentTrans.Dimension3 != null) ? SmartAppUtil.GetDropDownLabel(ledgerDimension3.DimensionCode, ledgerDimension3.Description) : null,
						Dimension4_Values = (vendorPaymentTrans.Dimension4 != null) ? SmartAppUtil.GetDropDownLabel(ledgerDimension4.DimensionCode, ledgerDimension4.Description) : null,
						Dimension5_Values = (vendorPaymentTrans.Dimension5 != null) ? SmartAppUtil.GetDropDownLabel(ledgerDimension5.DimensionCode, ledgerDimension5.Description) : null,
					    RefId = (customerRefundTable != null && vendorPaymentTrans.RefType == (int)RefType.CustomerRefund) ? customerRefundTable.CustomerRefundId :
								(purchaseTable != null && vendorPaymentTrans.RefType == (int)RefType.PurchaseTable) ? purchaseTable.PurchaseId :
								(withdrawalTable != null && vendorPaymentTrans.RefType == (int)RefType.WithdrawalTable) ? withdrawalTable.WithdrawalId : null,
						VendorTaxInvoiceId = vendorPaymentTrans.VendorTaxInvoiceId,
					
						RowVersion = vendorPaymentTrans.RowVersion,
					});
		}
		public VendorPaymentTransItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<VendorPaymentTransItemViewMap, VendorPaymentTransItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public VendorPaymentTrans CreateVendorPaymentTrans(VendorPaymentTrans vendorPaymentTrans)
		{
			try
			{
				vendorPaymentTrans.VendorPaymentTransGUID = Guid.NewGuid();
				base.Add(vendorPaymentTrans);
				return vendorPaymentTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateVendorPaymentTransVoid(VendorPaymentTrans vendorPaymentTrans)
		{
			try
			{
				CreateVendorPaymentTrans(vendorPaymentTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public VendorPaymentTrans UpdateVendorPaymentTrans(VendorPaymentTrans dbVendorPaymentTrans, VendorPaymentTrans inputVendorPaymentTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbVendorPaymentTrans = dbVendorPaymentTrans.MapUpdateValues<VendorPaymentTrans>(inputVendorPaymentTrans);
				base.Update(dbVendorPaymentTrans);
				return dbVendorPaymentTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateVendorPaymentTransVoid(VendorPaymentTrans dbVendorPaymentTrans, VendorPaymentTrans inputVendorPaymentTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbVendorPaymentTrans = dbVendorPaymentTrans.MapUpdateValues<VendorPaymentTrans>(inputVendorPaymentTrans, skipUpdateFields);
				base.Update(dbVendorPaymentTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public VendorPaymentTrans GetVendorPaymentTransByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.VendorPaymentTransGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<VendorPaymentTrans> GetVendorPaymentTransByProcessTransNoTracking(List<Guid> refGUIDs)
		{
			try
			{
				List<VendorPaymentTrans> vendorPaymentTrans = Entity.Where(w => refGUIDs.Contains(w.VendorPaymentTransGUID))
																	.AsNoTracking().ToList();

				return vendorPaymentTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

