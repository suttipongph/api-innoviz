using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface IAddressTransRepo
    {
        #region DropDown
        IEnumerable<SelectItem<AddressTransItemView>> GetDropDownItem(SearchParameter search);
        #endregion DropDown
        SearchResult<AddressTransListView> GetListvw(SearchParameter search);
        AddressTransItemView GetByIdvw(Guid id);
        AddressTrans Find(params object[] keyValues);
        AddressTrans GetAddressTransByIdNoTracking(Guid guid);
        List<AddressTrans> GetAddressTransByIdNoTracking(IEnumerable<Guid> addressTransGuids);
        AddressTrans CreateAddressTrans(AddressTrans addressTrans);
        void CreateAddressTransVoid(AddressTrans addressTrans);
        AddressTrans UpdateAddressTrans(AddressTrans dbAddressTrans, AddressTrans inputAddressTrans, List<string> skipUpdateFields = null);
        void UpdateAddressTransVoid(AddressTrans dbAddressTrans, AddressTrans inputAddressTrans, List<string> skipUpdateFields = null);
        void Remove(AddressTrans item);
        AddressTrans GetPrimaryAddressByReference(RefType refType, Guid refGuid);
        List<AddressTrans> GetPrimaryAddressByReference(RefType refType, List<Guid> refGuids);
        #region function
        IEnumerable<AddressTrans> GetAddressTransByRefType(int refType, Guid refGuid);
        #endregion
        void ValidateAdd(AddressTrans item);
        void ValidateAdd(IEnumerable<AddressTrans> items);
        AddressTransItemViewMap GetAddressTransViewMapById(Guid addressTransGUID);
    }
    public class AddressTransRepo : CompanyBaseRepository<AddressTrans>, IAddressTransRepo
    {
        public AddressTransRepo(SmartAppDbContext context) : base(context) { }
        public AddressTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public AddressTrans GetAddressTransByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.AddressTransGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<AddressTrans> GetAddressTransByIdNoTracking(IEnumerable<Guid> addressTransGuids)
        {
            try
            {
                var result = Entity.Where(w => addressTransGuids.Contains(w.AddressTransGUID))
                                    .AsNoTracking()
                                    .ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        #region Default
        private IQueryable<AddressTransItemViewMap> GetDropDownQuery()
        {
            return (from addressTrans in Entity
                    select new AddressTransItemViewMap
                    {
                        CompanyGUID = addressTrans.CompanyGUID,
                        Owner = addressTrans.Owner,
                        OwnerBusinessUnitGUID = addressTrans.OwnerBusinessUnitGUID,
                        AddressTransGUID = addressTrans.AddressTransGUID,
                        Name = addressTrans.Name,
                        Address1 = addressTrans.Address1,
                        Address2 = addressTrans.Address2,
                        RefGUID = addressTrans.RefGUID,
                        RefType = addressTrans.RefType,
                        IsTax = addressTrans.IsTax,
                        TaxBranchId = addressTrans.TaxBranchId,
                        TaxBranchName = addressTrans.TaxBranchName,
                        UnboundAddress = addressTrans.Address1 + " " + addressTrans.Address2,
                    });
        }
        public IEnumerable<SelectItem<AddressTransItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<AddressTrans>(search, SysParm.CompanyGUID);
                var addressTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<AddressTransItemViewMap, AddressTransItemView>().ToDropDownItem(search.ExcludeRowData);


                return addressTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Default
        #endregion DropDown
        #region GetListvw
        private IQueryable<AddressTransListViewMap> GetListQuery()
        {
            return (from addressTrans in Entity
                    select new AddressTransListViewMap
                    {
                        CompanyGUID = addressTrans.CompanyGUID,
                        Owner = addressTrans.Owner,
                        OwnerBusinessUnitGUID = addressTrans.OwnerBusinessUnitGUID,
                        AddressTransGUID = addressTrans.AddressTransGUID,
                        Name = addressTrans.Name,
                        Primary = addressTrans.Primary,
                        CurrentAddress = addressTrans.CurrentAddress,
                        Address1 = addressTrans.Address1,
                        RefGUID = addressTrans.RefGUID
                    });
        }
        public SearchResult<AddressTransListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<AddressTransListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<AddressTrans>(search, SysParm.CompanyGUID);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<AddressTransListViewMap, AddressTransListView>();
                result = list.SetSearchResult<AddressTransListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<AddressTransItemViewMap> GetItemQuery()
        {
            return (from addressTrans in Entity
                    join company in db.Set<Company>()
                    on addressTrans.CompanyGUID equals company.CompanyGUID
                    join ownerBU in db.Set<BusinessUnit>()
                    on addressTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljAddressTransOwnerBU
                    from ownerBU in ljAddressTransOwnerBU.DefaultIfEmpty()
                    join customerTable in db.Set<CustomerTable>()
                    on addressTrans.RefGUID equals customerTable.CustomerTableGUID into ljCustomerTable
                    from customerTable in ljCustomerTable.DefaultIfEmpty()
                    join buyerTable in db.Set<BuyerTable>()
                    on addressTrans.RefGUID equals buyerTable.BuyerTableGUID into ljBuyerTable
                    from buyerTable in ljBuyerTable.DefaultIfEmpty()
                    join relatedPersonTable in db.Set<RelatedPersonTable>()
                    on addressTrans.RefGUID equals relatedPersonTable.RelatedPersonTableGUID into ljRelatedPersonTable
                    from relatedPersonTable in ljRelatedPersonTable.DefaultIfEmpty()
                    join addressSubDistrict in db.Set<AddressSubDistrict>()
                    on addressTrans.RefGUID equals addressSubDistrict.AddressSubDistrictGUID into ljAddressSubDistrict
                    from addressSubDistrict in ljAddressSubDistrict.DefaultIfEmpty()
                    join addressDistrict in db.Set<AddressDistrict>()
                    on addressTrans.RefGUID equals addressDistrict.AddressDistrictGUID into ljAddressDistrict
                    from addressDistrict in ljAddressDistrict.DefaultIfEmpty()
                    join addressProvince in db.Set<AddressProvince>()
                    on addressTrans.RefGUID equals addressProvince.AddressProvinceGUID into ljAddressProvince
                    from addressProvince in ljAddressProvince.DefaultIfEmpty()
                    join addressCountry in db.Set<AddressCountry>()
                    on addressTrans.RefGUID equals addressCountry.AddressCountryGUID into ljAddressCountry
                    from addressCountry in ljAddressCountry.DefaultIfEmpty()
                    join addressPostalCode in db.Set<AddressPostalCode>()
                    on addressTrans.RefGUID equals addressPostalCode.AddressPostalCodeGUID into ljAddressPostalCode
                    from addressPostalCode in ljAddressPostalCode.DefaultIfEmpty()
                    select new AddressTransItemViewMap
                    {
                        CompanyGUID = addressTrans.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = addressTrans.Owner,
                        OwnerBusinessUnitGUID = addressTrans.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        CreatedBy = addressTrans.CreatedBy,
                        CreatedDateTime = addressTrans.CreatedDateTime,
                        ModifiedBy = addressTrans.ModifiedBy,
                        ModifiedDateTime = addressTrans.ModifiedDateTime,
                        AddressTransGUID = addressTrans.AddressTransGUID,
                        Address1 = addressTrans.Address1,
                        Address2 = addressTrans.Address2,
                        AddressCountryGUID = addressTrans.AddressCountryGUID,
                        AddressDistrictGUID = addressTrans.AddressDistrictGUID,
                        AddressPostalCodeGUID = addressTrans.AddressPostalCodeGUID,
                        AddressProvinceGUID = addressTrans.AddressProvinceGUID,
                        AddressSubDistrictGUID = addressTrans.AddressSubDistrictGUID,
                        CurrentAddress = addressTrans.CurrentAddress,
                        IsTax = addressTrans.IsTax,
                        Name = addressTrans.Name,
                        OwnershipGUID = addressTrans.OwnershipGUID,
                        Primary = addressTrans.Primary,
                        PropertyTypeGUID = addressTrans.PropertyTypeGUID,
                        RefGUID = addressTrans.RefGUID,
                        RefType = addressTrans.RefType,
                        TaxBranchId = addressTrans.TaxBranchId,
                        TaxBranchName = addressTrans.TaxBranchName,
                        AddressSubDistrict_Name = addressSubDistrict.Name,
                        AddressDistrict_Name = addressDistrict.Name,
                        AddressProvince_Name = addressProvince.Name,
                        AddressCountry_Name = addressCountry.Name,
                        AddressPostalCode_PostalCode = addressPostalCode.PostalCode,
                        RefId = (customerTable != null && addressTrans.RefType == (int)RefType.Customer) ? customerTable.CustomerId :
                                (buyerTable != null && addressTrans.RefType == (int)RefType.Buyer) ? buyerTable.BuyerId :
                                (relatedPersonTable != null && addressTrans.RefType == (int)RefType.RelatedPerson) ? relatedPersonTable.RelatedPersonId :
                                null,
                        AltAddress = addressTrans.AltAddress,
                    
                        RowVersion = addressTrans.RowVersion,
                    });
        }
        public AddressTransItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<AddressTransItemViewMap, AddressTransItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public AddressTrans CreateAddressTrans(AddressTrans addressTrans)
        {
            try
            {
                addressTrans.AddressTransGUID = Guid.NewGuid();
                base.Add(addressTrans);
                return addressTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateAddressTransVoid(AddressTrans addressTrans)
        {
            try
            {
                CreateAddressTrans(addressTrans);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AddressTrans UpdateAddressTrans(AddressTrans dbAddressTrans, AddressTrans inputAddressTrans, List<string> skipUpdateFields = null)
        {
            try
            {
                dbAddressTrans = dbAddressTrans.MapUpdateValues<AddressTrans>(inputAddressTrans);
                base.Update(dbAddressTrans);
                return dbAddressTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateAddressTransVoid(AddressTrans dbAddressTrans, AddressTrans inputAddressTrans, List<string> skipUpdateFields = null)
        {
            try
            {
                dbAddressTrans = dbAddressTrans.MapUpdateValues<AddressTrans>(inputAddressTrans, skipUpdateFields);
                base.Update(dbAddressTrans);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Create, Update, Delete
        #region Validation
        public override void ValidateAdd(AddressTrans item)
        {
            base.ValidateAdd(item);
            ValidateRefId(item);
        }
        public override void ValidateUpdate(AddressTrans item)
        {
            base.ValidateUpdate(item);
            ValidateRefId(item);
        }

        private void ValidateRefId(AddressTrans item)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                if (item.Primary)
                {
                    bool isDupplicateRefId = Entity.Any(t => t.CompanyGUID == item.CompanyGUID &&
                                         t.Primary == true &&
                                         t.AddressTransGUID != item.AddressTransGUID &&
                                         t.RefGUID == item.RefGUID);
                    if (isDupplicateRefId)
                    {
                        ex.AddData("ERROR.00375");
                    }
                }

                if (item.CurrentAddress)
                {
                    bool isDupplicateRefId = Entity.Any(t => t.CompanyGUID == item.CompanyGUID &&
                                         t.CurrentAddress == true &&
                                         t.AddressTransGUID != item.AddressTransGUID &&
                                         t.RefGUID == item.RefGUID);
                    if (isDupplicateRefId)
                    {
                        ex.AddData("ERROR.00741");
                    }
                }

                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Validate
        #region function
        public IEnumerable<AddressTrans> GetAddressTransByRefType(int refType, Guid refGuid)
        {
            try
            {
                IEnumerable<AddressTrans> list = Entity.Where(w => w.RefType == refType && w.RefGUID == refGuid)
                    .AsNoTracking();
                return list;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        public AddressTrans GetPrimaryAddressByReference(RefType refType, Guid refGuid)
        {
            try
            {
                var result = Entity.Where(w => w.RefType == (int)refType && w.RefGUID == refGuid && w.Primary)
                                    .AsNoTracking()
                                    .FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<AddressTrans> GetPrimaryAddressByReference(RefType refType, List<Guid> refGuids)
        {
            try
            {
                List<AddressTrans> results = Entity.Where(w => w.RefType == (int)refType &&
                                                               w.RefGUID.HasValue &&
                                                               refGuids.Contains(w.RefGUID.Value) &&
                                                               w.Primary == true)
                                                   .AsNoTracking().ToList();
                return results;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AddressTransItemViewMap GetAddressTransViewMapById(Guid addressTransGUID)
        {
            try
            {
                return (from addressTrans in Entity
                        join company in db.Set<Company>()
                        on addressTrans.CompanyGUID equals company.CompanyGUID
                        join ownerBU in db.Set<BusinessUnit>()
                        on addressTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljAddressTransOwnerBU
                        from ownerBU in ljAddressTransOwnerBU.DefaultIfEmpty()
                        join addressSubDistrict in db.Set<AddressSubDistrict>()
                        on addressTrans.AddressSubDistrictGUID equals addressSubDistrict.AddressSubDistrictGUID into ljAddressSubDistrict
                        from addressSubDistrict in ljAddressSubDistrict.DefaultIfEmpty()
                        join addressDistrict in db.Set<AddressDistrict>()
                        on addressTrans.AddressDistrictGUID equals addressDistrict.AddressDistrictGUID into ljAddressDistrict
                        from addressDistrict in ljAddressDistrict.DefaultIfEmpty()
                        join addressProvince in db.Set<AddressProvince>()
                        on addressTrans.AddressProvinceGUID equals addressProvince.AddressProvinceGUID into ljAddressProvince
                        from addressProvince in ljAddressProvince.DefaultIfEmpty()
                        join addressCountry in db.Set<AddressCountry>()
                        on addressTrans.AddressCountryGUID equals addressCountry.AddressCountryGUID into ljAddressCountry
                        from addressCountry in ljAddressCountry.DefaultIfEmpty()
                        join addressPostalCode in db.Set<AddressPostalCode>()
                        on addressTrans.AddressPostalCodeGUID equals addressPostalCode.AddressPostalCodeGUID into ljAddressPostalCode
                        from addressPostalCode in ljAddressPostalCode.DefaultIfEmpty()
                        where addressTrans.AddressTransGUID == addressTransGUID
                        select new AddressTransItemViewMap
                        {
                            CompanyGUID = addressTrans.CompanyGUID,
                            CompanyId = company.CompanyId,
                            Owner = addressTrans.Owner,
                            OwnerBusinessUnitGUID = addressTrans.OwnerBusinessUnitGUID,
                            OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                            CreatedBy = addressTrans.CreatedBy,
                            CreatedDateTime = addressTrans.CreatedDateTime,
                            ModifiedBy = addressTrans.ModifiedBy,
                            ModifiedDateTime = addressTrans.ModifiedDateTime,
                            AddressTransGUID = addressTrans.AddressTransGUID,
                            Address1 = addressTrans.Address1,
                            Address2 = addressTrans.Address2,
                            AddressCountryGUID = addressTrans.AddressCountryGUID,
                            AddressDistrictGUID = addressTrans.AddressDistrictGUID,
                            AddressPostalCodeGUID = addressTrans.AddressPostalCodeGUID,
                            AddressProvinceGUID = addressTrans.AddressProvinceGUID,
                            AddressSubDistrictGUID = addressTrans.AddressSubDistrictGUID,
                            CurrentAddress = addressTrans.CurrentAddress,
                            IsTax = addressTrans.IsTax,
                            Name = addressTrans.Name,
                            OwnershipGUID = addressTrans.OwnershipGUID,
                            Primary = addressTrans.Primary,
                            PropertyTypeGUID = addressTrans.PropertyTypeGUID,
                            RefGUID = addressTrans.RefGUID,
                            RefType = addressTrans.RefType,
                            TaxBranchId = addressTrans.TaxBranchId,
                            TaxBranchName = addressTrans.TaxBranchName,
                            AddressSubDistrict_Name = addressSubDistrict.Name,
                            AddressDistrict_Name = addressDistrict.Name,
                            AddressProvince_Name = addressProvince.Name,
                            AddressCountry_Name = addressCountry.Name,
                            AltAddress = addressTrans.AltAddress,
                            AddressCountry_CountryId = addressCountry.CountryId,
                            AddressProvince_ProvinceId = addressProvince.ProvinceId,
                            AddressDistrict_DistrictId = addressDistrict.DistrictId,
                            AddressSubDistrict_SubDistrictId = addressSubDistrict.SubDistrictId,
                            AddressPostalCode_PostalCode = addressPostalCode.PostalCode,
                        }).AsNoTracking()
                      .FirstOrDefault();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}

