using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IAgreementTableInfoTextRepo
	{
		#region DropDown
		IEnumerable<SelectItem<AgreementTableInfoTextItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<AgreementTableInfoTextListView> GetListvw(SearchParameter search);
		AgreementTableInfoTextItemView GetByIdvw(Guid id);
		AgreementTableInfoText Find(params object[] keyValues);
		AgreementTableInfoText GetAgreementTableInfoTextByIdNoTracking(Guid guid);
		AgreementTableInfoText CreateAgreementTableInfoText(AgreementTableInfoText agreementTableInfoText);
		void CreateAgreementTableInfoTextVoid(AgreementTableInfoText agreementTableInfoText);
		AgreementTableInfoText UpdateAgreementTableInfoText(AgreementTableInfoText dbAgreementTableInfoText, AgreementTableInfoText inputAgreementTableInfoText, List<string> skipUpdateFields = null);
		void UpdateAgreementTableInfoTextVoid(AgreementTableInfoText dbAgreementTableInfoText, AgreementTableInfoText inputAgreementTableInfoText, List<string> skipUpdateFields = null);
		void Remove(AgreementTableInfoText item);
		AgreementTableInfoText GetAgreementTableInfoTextByIdNoTrackingByAccessLevel(Guid guid);
		AgreementTableInfoText GetAgreementTableInfoTextByAgreementTableInfoNoTracking(Guid guid);
	}
	public class AgreementTableInfoTextRepo : CompanyBaseRepository<AgreementTableInfoText>, IAgreementTableInfoTextRepo
	{
		public AgreementTableInfoTextRepo(SmartAppDbContext context) : base(context) { }
		public AgreementTableInfoTextRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public AgreementTableInfoText GetAgreementTableInfoTextByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.AgreementTableInfoTextGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<AgreementTableInfoTextItemViewMap> GetDropDownQuery()
		{
			return (from agreementTableInfoText in Entity
					select new AgreementTableInfoTextItemViewMap
					{
						CompanyGUID = agreementTableInfoText.CompanyGUID,
						Owner = agreementTableInfoText.Owner,
						OwnerBusinessUnitGUID = agreementTableInfoText.OwnerBusinessUnitGUID,
						AgreementTableInfoTextGUID = agreementTableInfoText.AgreementTableInfoTextGUID
					});
		}
		public IEnumerable<SelectItem<AgreementTableInfoTextItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<AgreementTableInfoText>(search, SysParm.CompanyGUID);
				var agreementTableInfoText = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<AgreementTableInfoTextItemViewMap, AgreementTableInfoTextItemView>().ToDropDownItem(search.ExcludeRowData);


				return agreementTableInfoText;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<AgreementTableInfoTextListViewMap> GetListQuery()
		{
			return (from agreementTableInfoText in Entity
				select new AgreementTableInfoTextListViewMap
				{
						CompanyGUID = agreementTableInfoText.CompanyGUID,
						Owner = agreementTableInfoText.Owner,
						OwnerBusinessUnitGUID = agreementTableInfoText.OwnerBusinessUnitGUID,
						AgreementTableInfoTextGUID = agreementTableInfoText.AgreementTableInfoTextGUID,
				});
		}
		public SearchResult<AgreementTableInfoTextListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<AgreementTableInfoTextListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<AgreementTableInfoText>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<AgreementTableInfoTextListViewMap, AgreementTableInfoTextListView>();
				result = list.SetSearchResult<AgreementTableInfoTextListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<AgreementTableInfoTextItemViewMap> GetItemQuery()
		{
			return (from agreementTableInfoText in Entity
					join company in db.Set<Company>()
					on agreementTableInfoText.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on agreementTableInfoText.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljAgreementTableInfoTextOwnerBU
					from ownerBU in ljAgreementTableInfoTextOwnerBU.DefaultIfEmpty()
					select new AgreementTableInfoTextItemViewMap
					{
						CompanyGUID = agreementTableInfoText.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = agreementTableInfoText.Owner,
						OwnerBusinessUnitGUID = agreementTableInfoText.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = agreementTableInfoText.CreatedBy,
						CreatedDateTime = agreementTableInfoText.CreatedDateTime,
						ModifiedBy = agreementTableInfoText.ModifiedBy,
						ModifiedDateTime = agreementTableInfoText.ModifiedDateTime,
						AgreementTableInfoTextGUID = agreementTableInfoText.AgreementTableInfoTextGUID,
						AgreementTableInfoGUID = agreementTableInfoText.AgreementTableInfoGUID,
						Text1 = agreementTableInfoText.Text1,
						Text2 = agreementTableInfoText.Text2,
						Text3 = agreementTableInfoText.Text3,
						Text4 = agreementTableInfoText.Text4,
					
						RowVersion = agreementTableInfoText.RowVersion,
					});
		}
		public AgreementTableInfoTextItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<AgreementTableInfoTextItemViewMap, AgreementTableInfoTextItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public AgreementTableInfoText CreateAgreementTableInfoText(AgreementTableInfoText agreementTableInfoText)
		{
			try
			{
				agreementTableInfoText.AgreementTableInfoTextGUID = Guid.NewGuid();
				base.Add(agreementTableInfoText);
				return agreementTableInfoText;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateAgreementTableInfoTextVoid(AgreementTableInfoText agreementTableInfoText)
		{
			try
			{
				CreateAgreementTableInfoText(agreementTableInfoText);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AgreementTableInfoText UpdateAgreementTableInfoText(AgreementTableInfoText dbAgreementTableInfoText, AgreementTableInfoText inputAgreementTableInfoText, List<string> skipUpdateFields = null)
		{
			try
			{
				dbAgreementTableInfoText = dbAgreementTableInfoText.MapUpdateValues<AgreementTableInfoText>(inputAgreementTableInfoText);
				base.Update(dbAgreementTableInfoText);
				return dbAgreementTableInfoText;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateAgreementTableInfoTextVoid(AgreementTableInfoText dbAgreementTableInfoText, AgreementTableInfoText inputAgreementTableInfoText, List<string> skipUpdateFields = null)
		{
			try
			{
				dbAgreementTableInfoText = dbAgreementTableInfoText.MapUpdateValues<AgreementTableInfoText>(inputAgreementTableInfoText, skipUpdateFields);
				base.Update(dbAgreementTableInfoText);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public AgreementTableInfoText GetAgreementTableInfoTextByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.AgreementTableInfoTextGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public AgreementTableInfoText GetAgreementTableInfoTextByAgreementTableInfoNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.AgreementTableInfoGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

