using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICalendarNonWorkingDateRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CalendarNonWorkingDateItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<CalendarNonWorkingDateListView> GetListvw(SearchParameter search);
		CalendarNonWorkingDateItemView GetByIdvw(Guid id);
		CalendarNonWorkingDate Find(params object[] keyValues);
		CalendarNonWorkingDate GetCalendarNonWorkingDateByIdNoTracking(Guid guid);
		CalendarNonWorkingDate CreateCalendarNonWorkingDate(CalendarNonWorkingDate calendarNonWorkingDate);
		void CreateCalendarNonWorkingDateVoid(CalendarNonWorkingDate calendarNonWorkingDate);
		CalendarNonWorkingDate UpdateCalendarNonWorkingDate(CalendarNonWorkingDate dbCalendarNonWorkingDate, CalendarNonWorkingDate inputCalendarNonWorkingDate, List<string> skipUpdateFields = null);
		void UpdateCalendarNonWorkingDateVoid(CalendarNonWorkingDate dbCalendarNonWorkingDate, CalendarNonWorkingDate inputCalendarNonWorkingDate, List<string> skipUpdateFields = null);
		void Remove(CalendarNonWorkingDate item);
		CalendarNonWorkingDate GetCalendarNonWorkingDateByIdNoTrackingByAccessLevel(Guid guid);
		#region shared
		CalendarNonWorkingDate GetCalendarNonWorkingDateByDate(Guid calendarGroupGuid, DateTime date);
        #endregion
    }
    public class CalendarNonWorkingDateRepo : CompanyBaseRepository<CalendarNonWorkingDate>, ICalendarNonWorkingDateRepo
	{
		public CalendarNonWorkingDateRepo(SmartAppDbContext context) : base(context) { }
		public CalendarNonWorkingDateRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CalendarNonWorkingDate GetCalendarNonWorkingDateByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CalendarNonWorkingDateGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CalendarNonWorkingDateItemViewMap> GetDropDownQuery()
		{
			return (from calendarNonWorkingDate in Entity
					select new CalendarNonWorkingDateItemViewMap
					{
						CompanyGUID = calendarNonWorkingDate.CompanyGUID,
						Owner = calendarNonWorkingDate.Owner,
						OwnerBusinessUnitGUID = calendarNonWorkingDate.OwnerBusinessUnitGUID,
						CalendarNonWorkingDateGUID = calendarNonWorkingDate.CalendarNonWorkingDateGUID,
						CalendarDate = calendarNonWorkingDate.CalendarDate,
						Description = calendarNonWorkingDate.Description
					});
		}
		public IEnumerable<SelectItem<CalendarNonWorkingDateItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CalendarNonWorkingDate>(search, SysParm.CompanyGUID);
				var calendarNonWorkingDate = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CalendarNonWorkingDateItemViewMap, CalendarNonWorkingDateItemView>().ToDropDownItem(search.ExcludeRowData);


				return calendarNonWorkingDate;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CalendarNonWorkingDateListViewMap> GetListQuery()
		{
			return (from calendarNonWorkingDate in Entity
					join calendarGroup in db.Set<CalendarGroup>()
					on calendarNonWorkingDate.CalendarGroupGUID equals calendarGroup.CalendarGroupGUID into ljCalendarNonWorkingDatecalendarGroup
					from calendarGroup in ljCalendarNonWorkingDatecalendarGroup.DefaultIfEmpty()
					select new CalendarNonWorkingDateListViewMap
				{
						CompanyGUID = calendarNonWorkingDate.CompanyGUID,
						Owner = calendarNonWorkingDate.Owner,
						OwnerBusinessUnitGUID = calendarNonWorkingDate.OwnerBusinessUnitGUID,
						CalendarNonWorkingDateGUID = calendarNonWorkingDate.CalendarNonWorkingDateGUID,
						CalendarGroupGUID = calendarNonWorkingDate.CalendarGroupGUID,
						CalendarDate = calendarNonWorkingDate.CalendarDate,
						Description = calendarNonWorkingDate.Description,
						HolidayType = calendarNonWorkingDate.HolidayType,
						CalendarGroup_Values = SmartAppUtil.GetDropDownLabel(calendarGroup.CalendarGroupId,calendarGroup.Description),
						CalendarGroup_calendarGroupId = calendarGroup.CalendarGroupId
				});
		}
		public SearchResult<CalendarNonWorkingDateListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CalendarNonWorkingDateListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CalendarNonWorkingDate>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CalendarNonWorkingDateListViewMap, CalendarNonWorkingDateListView>();
				result = list.SetSearchResult<CalendarNonWorkingDateListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CalendarNonWorkingDateItemViewMap> GetItemQuery()
		{
			return (from calendarNonWorkingDate in Entity
					join company in db.Set<Company>()
					on calendarNonWorkingDate.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on calendarNonWorkingDate.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCalendarNonWorkingDateOwnerBU
					from ownerBU in ljCalendarNonWorkingDateOwnerBU.DefaultIfEmpty()
					select new CalendarNonWorkingDateItemViewMap
					{
						CompanyGUID = calendarNonWorkingDate.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = calendarNonWorkingDate.Owner,
						OwnerBusinessUnitGUID = calendarNonWorkingDate.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = calendarNonWorkingDate.CreatedBy,
						CreatedDateTime = calendarNonWorkingDate.CreatedDateTime,
						ModifiedBy = calendarNonWorkingDate.ModifiedBy,
						ModifiedDateTime = calendarNonWorkingDate.ModifiedDateTime,
						CalendarNonWorkingDateGUID = calendarNonWorkingDate.CalendarNonWorkingDateGUID,
						CalendarDate = calendarNonWorkingDate.CalendarDate,
						CalendarGroupGUID = calendarNonWorkingDate.CalendarGroupGUID,
						Description = calendarNonWorkingDate.Description,
						HolidayType = calendarNonWorkingDate.HolidayType,
					
						RowVersion = calendarNonWorkingDate.RowVersion,
					});
		}
		public CalendarNonWorkingDateItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<CalendarNonWorkingDateItemViewMap, CalendarNonWorkingDateItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public CalendarNonWorkingDate CreateCalendarNonWorkingDate(CalendarNonWorkingDate calendarNonWorkingDate)
		{
			try
			{
				calendarNonWorkingDate.CalendarNonWorkingDateGUID = Guid.NewGuid();
				base.Add(calendarNonWorkingDate);
				return calendarNonWorkingDate;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCalendarNonWorkingDateVoid(CalendarNonWorkingDate calendarNonWorkingDate)
		{
			try
			{
				CreateCalendarNonWorkingDate(calendarNonWorkingDate);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CalendarNonWorkingDate UpdateCalendarNonWorkingDate(CalendarNonWorkingDate dbCalendarNonWorkingDate, CalendarNonWorkingDate inputCalendarNonWorkingDate, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCalendarNonWorkingDate = dbCalendarNonWorkingDate.MapUpdateValues<CalendarNonWorkingDate>(inputCalendarNonWorkingDate);
				base.Update(dbCalendarNonWorkingDate);
				return dbCalendarNonWorkingDate;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCalendarNonWorkingDateVoid(CalendarNonWorkingDate dbCalendarNonWorkingDate, CalendarNonWorkingDate inputCalendarNonWorkingDate, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCalendarNonWorkingDate = dbCalendarNonWorkingDate.MapUpdateValues<CalendarNonWorkingDate>(inputCalendarNonWorkingDate, skipUpdateFields);
				base.Update(dbCalendarNonWorkingDate);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public CalendarNonWorkingDate GetCalendarNonWorkingDateByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CalendarNonWorkingDateGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        #region shared
		public CalendarNonWorkingDate GetCalendarNonWorkingDateByDate(Guid calendarGroupGuid,DateTime date)
        {
			try
			{
				var result = Entity.Where(w => w.CalendarGroupGUID == calendarGroupGuid && w.CalendarDate == date )
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #endregion
    }
}

