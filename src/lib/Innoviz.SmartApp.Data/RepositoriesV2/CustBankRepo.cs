using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ICustBankRepo
	{
		#region DropDown
		IEnumerable<SelectItem<CustBankItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<CustBankListView> GetListvw(SearchParameter search);
		CustBankItemView GetByIdvw(Guid id);
		CustBank Find(params object[] keyValues);
		CustBank GetCustBankByIdNoTracking(Guid guid);
		CustBank CreateCustBank(CustBank custBank);
		void CreateCustBankVoid(CustBank custBank);
		CustBank UpdateCustBank(CustBank dbCustBank, CustBank inputCustBank, List<string> skipUpdateFields = null);
		void UpdateCustBankVoid(CustBank dbCustBank, CustBank inputCustBank, List<string> skipUpdateFields = null);
		void Remove(CustBank item);
		void ValidateAdd(CustBank item);
		void ValidateAdd(IEnumerable<CustBank> items);
	}
	public class CustBankRepo : CompanyBaseRepository<CustBank>, ICustBankRepo
	{
		public CustBankRepo(SmartAppDbContext context) : base(context) { }
		public CustBankRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CustBank GetCustBankByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CustBankGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<CustBankItemViewMap> GetDropDownQuery()
		{
			return (from custBank in Entity
					select new CustBankItemViewMap
					{
						CompanyGUID = custBank.CompanyGUID,
						Owner = custBank.Owner,
						OwnerBusinessUnitGUID = custBank.OwnerBusinessUnitGUID,
						CustBankGUID = custBank.CustBankGUID,
						BankGroupGUID = custBank.BankGroupGUID,
						BankAccountName = custBank.BankAccountName,
						AccountNumber = custBank.AccountNumber,
						InActive = custBank.InActive,
						PDC = custBank.PDC,
						BankAccountControl = custBank.BankAccountControl,
						CustomerTableGUID = custBank.CustomerTableGUID,
					});
		}
		public IEnumerable<SelectItem<CustBankItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<CustBank>(search, SysParm.CompanyGUID);
				var custBank = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<CustBankItemViewMap, CustBankItemView>().ToDropDownItem(search.ExcludeRowData);


				return custBank;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<CustBankListViewMap> GetListQuery()
		{
			return (from custBank in Entity
					join bankGroup in db.Set<BankGroup>()
					on custBank.BankGroupGUID equals bankGroup.BankGroupGUID
					join bankType in db.Set<BankType>()
					on custBank.BankTypeGUID equals bankType.BankTypeGUID
					select new CustBankListViewMap
				    {
						CompanyGUID = custBank.CompanyGUID,
						Owner = custBank.Owner,
						OwnerBusinessUnitGUID = custBank.OwnerBusinessUnitGUID,
						CustBankGUID = custBank.CustBankGUID,
						BankGroupGUID = custBank.BankGroupGUID,
						BankTypeGUID = custBank.BankTypeGUID,
						AccountNumber = custBank.AccountNumber,
						BankAccountName = custBank.BankAccountName,
						BankAccountControl = custBank.BankAccountControl,
						PDC = custBank.PDC,
						Primary = custBank.Primary,
						InActive = custBank.InActive,
						BankGroup_BankGroupId = bankGroup.BankGroupId,
						CustomerTableGUID = custBank.CustomerTableGUID,
						BankGroup_Values = SmartAppUtil.GetDropDownLabel(bankGroup.BankGroupId, bankGroup.Description),
						BankType_Values = SmartAppUtil.GetDropDownLabel(bankType.BankTypeId, bankType.Description),
					});
		}
		public SearchResult<CustBankListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<CustBankListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<CustBank>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<CustBankListViewMap, CustBankListView>();
				result = list.SetSearchResult<CustBankListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<CustBankItemViewMap> GetItemQuery()
		{
			return (from custBank in Entity
					join company in db.Set<Company>()
					on custBank.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on custBank.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljCustBankOwnerBU
					from ownerBU in ljCustBankOwnerBU.DefaultIfEmpty()
					join customerTable in db.Set<CustomerTable>()
					on custBank.CustomerTableGUID equals customerTable.CustomerTableGUID into ljcustomerTable
					from customerTable in ljcustomerTable.DefaultIfEmpty()
					select new CustBankItemViewMap
					{
						CompanyGUID = custBank.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = custBank.Owner,
						OwnerBusinessUnitGUID = custBank.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = custBank.CreatedBy,
						CreatedDateTime = custBank.CreatedDateTime,
						ModifiedBy = custBank.ModifiedBy,
						ModifiedDateTime = custBank.ModifiedDateTime,
						CustBankGUID = custBank.CustBankGUID,
						AccountNumber = custBank.AccountNumber,
						BankAccountControl = custBank.BankAccountControl,
						BankAccountName = custBank.BankAccountName,
						BankBranch = custBank.BankBranch,
						BankGroupGUID = custBank.BankGroupGUID,
						BankTypeGUID = custBank.BankTypeGUID,
						CustomerTableGUID = custBank.CustomerTableGUID,
						InActive = custBank.InActive,
						PDC = custBank.PDC,
						Primary = custBank.Primary,
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
					
						RowVersion = custBank.RowVersion,
					});
		}
		public CustBankItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.ToMap<CustBankItemViewMap, CustBankItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public CustBank CreateCustBank(CustBank custBank)
		{
			try
			{
				custBank.CustBankGUID = Guid.NewGuid();
				base.Add(custBank);
				return custBank;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCustBankVoid(CustBank custBank)
		{
			try
			{
				CreateCustBank(custBank);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CustBank UpdateCustBank(CustBank dbCustBank, CustBank inputCustBank, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCustBank = dbCustBank.MapUpdateValues<CustBank>(inputCustBank);
				base.Update(dbCustBank);
				return dbCustBank;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCustBankVoid(CustBank dbCustBank, CustBank inputCustBank, List<string> skipUpdateFields = null)
		{
			try
			{
				dbCustBank = dbCustBank.MapUpdateValues<CustBank>(inputCustBank, skipUpdateFields);
				base.Update(dbCustBank);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public override void ValidateAdd(CustBank item)
		{
			ValidatePrimary(item);
			base.ValidateAdd(item);
		}
		public override void ValidateUpdate(CustBank item)
		{
			ValidatePrimary(item);
			base.ValidateUpdate(item);
		}

		private void ValidatePrimary(CustBank item)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				if (item.Primary)
				{
					bool isDupplicateRefId = Entity.Any(t => t.CompanyGUID == item.CompanyGUID &&
										 t.Primary == true &&
										 t.CustBankGUID != item.CustBankGUID &&
										 t.CustomerTableGUID == item.CustomerTableGUID);
					if (isDupplicateRefId)
					{
						ex.AddData("ERROR.90060", new string[] {"LABEL.PRIMARY","LABEL.CUSTOMER_BANK_ACCOUNT"});
					}
				}

				if (item.BankAccountControl)
				{
					bool isDupplicateRefId = Entity.Any(t => t.CompanyGUID == item.CompanyGUID &&
										 t.BankAccountControl == true &&
										 t.CustBankGUID != item.CustBankGUID &&
										 t.CustomerTableGUID == item.CustomerTableGUID);
					if (isDupplicateRefId)
					{
						ex.AddData("ERROR.90060", new string[] { "LABEL.BANK_ACCOUNT_CONTROL", "LABEL.CUSTOMER_BANK_ACCOUNT" });
					}
				}

				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

