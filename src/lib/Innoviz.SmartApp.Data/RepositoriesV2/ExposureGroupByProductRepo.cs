using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IExposureGroupByProductRepo
	{
		#region DropDown
		IEnumerable<SelectItem<ExposureGroupByProductItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<ExposureGroupByProductListView> GetListvw(SearchParameter search);
		ExposureGroupByProductItemView GetByIdvw(Guid id);
		ExposureGroupByProduct Find(params object[] keyValues);
		ExposureGroupByProduct GetExposureGroupByProductByIdNoTracking(Guid guid);
		ExposureGroupByProduct CreateExposureGroupByProduct(ExposureGroupByProduct exposureGroupByProduct);
		void CreateExposureGroupByProductVoid(ExposureGroupByProduct exposureGroupByProduct);
		ExposureGroupByProduct UpdateExposureGroupByProduct(ExposureGroupByProduct dbExposureGroupByProduct, ExposureGroupByProduct inputExposureGroupByProduct, List<string> skipUpdateFields = null);
		void UpdateExposureGroupByProductVoid(ExposureGroupByProduct dbExposureGroupByProduct, ExposureGroupByProduct inputExposureGroupByProduct, List<string> skipUpdateFields = null);
		void Remove(ExposureGroupByProduct item);
		ExposureGroupByProduct GetExposureGroupByProductByIdNoTrackingByAccessLevel(Guid guid);
	}
	public class ExposureGroupByProductRepo : CompanyBaseRepository<ExposureGroupByProduct>, IExposureGroupByProductRepo
	{
		public ExposureGroupByProductRepo(SmartAppDbContext context) : base(context) { }
		public ExposureGroupByProductRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ExposureGroupByProduct GetExposureGroupByProductByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ExposureGroupByProductGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<ExposureGroupByProductItemViewMap> GetDropDownQuery()
		{
			return (from exposureGroupByProduct in Entity
					select new ExposureGroupByProductItemViewMap
					{
						CompanyGUID = exposureGroupByProduct.CompanyGUID,
						Owner = exposureGroupByProduct.Owner,
						OwnerBusinessUnitGUID = exposureGroupByProduct.OwnerBusinessUnitGUID,
						ExposureGroupByProductGUID = exposureGroupByProduct.ExposureGroupByProductGUID
					});
		}
		public IEnumerable<SelectItem<ExposureGroupByProductItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<ExposureGroupByProduct>(search, SysParm.CompanyGUID);
				var exposureGroupByProduct = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<ExposureGroupByProductItemViewMap, ExposureGroupByProductItemView>().ToDropDownItem(search.ExcludeRowData);


				return exposureGroupByProduct;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<ExposureGroupByProductListViewMap> GetListQuery()
		{
			return (from exposureGroupByProduct in Entity
					join exposureGroup in db.Set<ExposureGroup>()
					on exposureGroupByProduct.ExposureGroupGUID equals exposureGroup.ExposureGroupGUID
					select new ExposureGroupByProductListViewMap
				{
						CompanyGUID = exposureGroupByProduct.CompanyGUID,
						Owner = exposureGroupByProduct.Owner,
						OwnerBusinessUnitGUID = exposureGroupByProduct.OwnerBusinessUnitGUID,
						ExposureGroupByProductGUID = exposureGroupByProduct.ExposureGroupByProductGUID,
						ExposureGroupGUID = exposureGroupByProduct.ExposureGroupGUID,
						ProductType = exposureGroupByProduct.ProductType,
						ExposureAmount = exposureGroupByProduct.ExposureAmount,
						ExposureGroup_ExposureGroupId = exposureGroup.ExposureGroupId,
						DocumentConditionTemplateTable_Values = exposureGroupByProduct.ExposureGroupGUID.GuidNullToString(),
				});
		}
		public SearchResult<ExposureGroupByProductListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<ExposureGroupByProductListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<ExposureGroupByProduct>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<ExposureGroupByProductListViewMap, ExposureGroupByProductListView>();
				result = list.SetSearchResult<ExposureGroupByProductListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<ExposureGroupByProductItemViewMap> GetItemQuery()
		{
			return (from exposureGroupByProduct in Entity
					join company in db.Set<Company>()
					on exposureGroupByProduct.CompanyGUID equals company.CompanyGUID
					join exposureGroup in db.Set<ExposureGroup>()
					on exposureGroupByProduct.ExposureGroupGUID equals exposureGroup.ExposureGroupGUID
					join ownerBU in db.Set<BusinessUnit>()
					on exposureGroupByProduct.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljExposureGroupByProductOwnerBU
					from ownerBU in ljExposureGroupByProductOwnerBU.DefaultIfEmpty()
					select new ExposureGroupByProductItemViewMap
					{
						CompanyGUID = exposureGroupByProduct.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = exposureGroupByProduct.Owner,
						OwnerBusinessUnitGUID = exposureGroupByProduct.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = exposureGroupByProduct.CreatedBy,
						CreatedDateTime = exposureGroupByProduct.CreatedDateTime,
						ModifiedBy = exposureGroupByProduct.ModifiedBy,
						ModifiedDateTime = exposureGroupByProduct.ModifiedDateTime,
						ExposureGroupByProductGUID = exposureGroupByProduct.ExposureGroupByProductGUID,
						ExposureAmount = exposureGroupByProduct.ExposureAmount,
						ExposureGroupGUID = exposureGroupByProduct.ExposureGroupGUID,
						ProductType = exposureGroupByProduct.ProductType,
						ExposureGroup_Values = SmartAppUtil.GetDropDownLabel(exposureGroup.ExposureGroupId, exposureGroup.Description),
					
						RowVersion = exposureGroupByProduct.RowVersion,
					});
		}
		public ExposureGroupByProductItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<ExposureGroupByProductItemViewMap, ExposureGroupByProductItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public ExposureGroupByProduct CreateExposureGroupByProduct(ExposureGroupByProduct exposureGroupByProduct)
		{
			try
			{
				exposureGroupByProduct.ExposureGroupByProductGUID = Guid.NewGuid();
				base.Add(exposureGroupByProduct);
				return exposureGroupByProduct;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateExposureGroupByProductVoid(ExposureGroupByProduct exposureGroupByProduct)
		{
			try
			{
				CreateExposureGroupByProduct(exposureGroupByProduct);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ExposureGroupByProduct UpdateExposureGroupByProduct(ExposureGroupByProduct dbExposureGroupByProduct, ExposureGroupByProduct inputExposureGroupByProduct, List<string> skipUpdateFields = null)
		{
			try
			{
				dbExposureGroupByProduct = dbExposureGroupByProduct.MapUpdateValues<ExposureGroupByProduct>(inputExposureGroupByProduct);
				base.Update(dbExposureGroupByProduct);
				return dbExposureGroupByProduct;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateExposureGroupByProductVoid(ExposureGroupByProduct dbExposureGroupByProduct, ExposureGroupByProduct inputExposureGroupByProduct, List<string> skipUpdateFields = null)
		{
			try
			{
				dbExposureGroupByProduct = dbExposureGroupByProduct.MapUpdateValues<ExposureGroupByProduct>(inputExposureGroupByProduct, skipUpdateFields);
				base.Update(dbExposureGroupByProduct);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public ExposureGroupByProduct GetExposureGroupByProductByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ExposureGroupByProductGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

