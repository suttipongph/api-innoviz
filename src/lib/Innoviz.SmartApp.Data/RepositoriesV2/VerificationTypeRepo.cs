using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IVerificationTypeRepo
	{
		#region DropDown
		IEnumerable<SelectItem<VerificationTypeItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<VerificationTypeListView> GetListvw(SearchParameter search);
		VerificationTypeItemView GetByIdvw(Guid id);
		VerificationType Find(params object[] keyValues);
		VerificationType GetVerificationTypeByIdNoTracking(Guid guid);
		VerificationType CreateVerificationType(VerificationType verificationType);
		void CreateVerificationTypeVoid(VerificationType verificationType);
		VerificationType UpdateVerificationType(VerificationType dbVerificationType, VerificationType inputVerificationType, List<string> skipUpdateFields = null);
		void UpdateVerificationTypeVoid(VerificationType dbVerificationType, VerificationType inputVerificationType, List<string> skipUpdateFields = null);
		void Remove(VerificationType item);
	}
	public class VerificationTypeRepo : CompanyBaseRepository<VerificationType>, IVerificationTypeRepo
	{
		public VerificationTypeRepo(SmartAppDbContext context) : base(context) { }
		public VerificationTypeRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public VerificationType GetVerificationTypeByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.VerificationTypeGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<VerificationTypeItemViewMap> GetDropDownQuery()
		{
			return (from verificationType in Entity
					select new VerificationTypeItemViewMap
					{
						CompanyGUID = verificationType.CompanyGUID,
						Owner = verificationType.Owner,
						OwnerBusinessUnitGUID = verificationType.OwnerBusinessUnitGUID,
						VerificationTypeGUID = verificationType.VerificationTypeGUID,
						VerificationTypeId = verificationType.VerificationTypeId,
						Description = verificationType.Description
					});
		}
		public IEnumerable<SelectItem<VerificationTypeItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<VerificationType>(search, SysParm.CompanyGUID);
				var verificationType = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<VerificationTypeItemViewMap, VerificationTypeItemView>().ToDropDownItem(search.ExcludeRowData);


				return verificationType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<VerificationTypeListViewMap> GetListQuery()
		{
			return (from verificationType in Entity
				select new VerificationTypeListViewMap
				{
						CompanyGUID = verificationType.CompanyGUID,
						Owner = verificationType.Owner,
						OwnerBusinessUnitGUID = verificationType.OwnerBusinessUnitGUID,
						VerificationTypeGUID = verificationType.VerificationTypeGUID,
						VerificationTypeId = verificationType.VerificationTypeId,
						Description = verificationType.Description,
						VerifyType = verificationType.VerifyType
				});
		}
		public SearchResult<VerificationTypeListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<VerificationTypeListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<VerificationType>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<VerificationTypeListViewMap, VerificationTypeListView>();
				result = list.SetSearchResult<VerificationTypeListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<VerificationTypeItemViewMap> GetItemQuery()
		{
			return (from verificationType in Entity
					join company in db.Set<Company>()
					on verificationType.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on verificationType.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljVerificationTypeOwnerBU
					from ownerBU in ljVerificationTypeOwnerBU.DefaultIfEmpty()
					select new VerificationTypeItemViewMap
					{
						CompanyGUID = verificationType.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = verificationType.Owner,
						OwnerBusinessUnitGUID = verificationType.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = verificationType.CreatedBy,
						CreatedDateTime = verificationType.CreatedDateTime,
						ModifiedBy = verificationType.ModifiedBy,
						ModifiedDateTime = verificationType.ModifiedDateTime,
						VerificationTypeGUID = verificationType.VerificationTypeGUID,
						Description = verificationType.Description,
						VerificationTypeId = verificationType.VerificationTypeId,
						VerifyType = verificationType.VerifyType,
					
						RowVersion = verificationType.RowVersion,
					});
		}
		public VerificationTypeItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<VerificationTypeItemViewMap, VerificationTypeItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public VerificationType CreateVerificationType(VerificationType verificationType)
		{
			try
			{
				verificationType.VerificationTypeGUID = Guid.NewGuid();
				base.Add(verificationType);
				return verificationType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateVerificationTypeVoid(VerificationType verificationType)
		{
			try
			{
				CreateVerificationType(verificationType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public VerificationType UpdateVerificationType(VerificationType dbVerificationType, VerificationType inputVerificationType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbVerificationType = dbVerificationType.MapUpdateValues<VerificationType>(inputVerificationType);
				base.Update(dbVerificationType);
				return dbVerificationType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateVerificationTypeVoid(VerificationType dbVerificationType, VerificationType inputVerificationType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbVerificationType = dbVerificationType.MapUpdateValues<VerificationType>(inputVerificationType, skipUpdateFields);
				base.Update(dbVerificationType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

