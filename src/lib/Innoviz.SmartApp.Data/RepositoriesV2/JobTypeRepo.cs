using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IJobTypeRepo
	{
		#region DropDown
		IEnumerable<SelectItem<JobTypeItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<JobTypeListView> GetListvw(SearchParameter search);
		JobTypeItemView GetByIdvw(Guid id);
		JobType Find(params object[] keyValues);
		JobType GetJobTypeByIdNoTracking(Guid guid);
		JobType CreateJobType(JobType jobType);
		void CreateJobTypeVoid(JobType jobType);
		JobType UpdateJobType(JobType dbJobType, JobType inputJobType, List<string> skipUpdateFields = null);
		void UpdateJobTypeVoid(JobType dbJobType, JobType inputJobType, List<string> skipUpdateFields = null);
		void Remove(JobType item);
		JobType GetJobTypeByIdNoTrackingByAccessLevel(Guid guid);
	}
	public class JobTypeRepo : CompanyBaseRepository<JobType>, IJobTypeRepo
	{
		public JobTypeRepo(SmartAppDbContext context) : base(context) { }
		public JobTypeRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public JobType GetJobTypeByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.JobTypeGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<JobTypeItemViewMap> GetDropDownQuery()
		{
			return (from jobType in Entity
					select new JobTypeItemViewMap
					{
						CompanyGUID = jobType.CompanyGUID,
						Owner = jobType.Owner,
						OwnerBusinessUnitGUID = jobType.OwnerBusinessUnitGUID,
						JobTypeGUID = jobType.JobTypeGUID,
						JobTypeId = jobType.JobTypeId,
						Description = jobType.Description,
						Assignment = jobType.Assignment
					});
		}
		public IEnumerable<SelectItem<JobTypeItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<JobType>(search, SysParm.CompanyGUID);
				var jobType = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<JobTypeItemViewMap, JobTypeItemView>().ToDropDownItem(search.ExcludeRowData);


				return jobType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<JobTypeListViewMap> GetListQuery()
		{
			return (from jobType in Entity
				select new JobTypeListViewMap
				{
						CompanyGUID = jobType.CompanyGUID,
						Owner = jobType.Owner,
						OwnerBusinessUnitGUID = jobType.OwnerBusinessUnitGUID,
						JobTypeGUID = jobType.JobTypeGUID,
						JobTypeId = jobType.JobTypeId,
						Description = jobType.Description,
						ShowDocConVerifyType = jobType.ShowDocConVerifyType,
						Assignment = jobType.Assignment
				});
		}
		public SearchResult<JobTypeListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<JobTypeListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<JobType>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<JobTypeListViewMap, JobTypeListView>();
				result = list.SetSearchResult<JobTypeListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<JobTypeItemViewMap> GetItemQuery()
		{
			return (from jobType in Entity
					join company in db.Set<Company>()
					on jobType.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on jobType.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljJobTypeOwnerBU
					from ownerBU in ljJobTypeOwnerBU.DefaultIfEmpty()
					select new JobTypeItemViewMap
					{
						CompanyGUID = jobType.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = jobType.Owner,
						OwnerBusinessUnitGUID = jobType.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = jobType.CreatedBy,
						CreatedDateTime = jobType.CreatedDateTime,
						ModifiedBy = jobType.ModifiedBy,
						ModifiedDateTime = jobType.ModifiedDateTime,
						JobTypeGUID = jobType.JobTypeGUID,
						Description = jobType.Description,
						JobTypeId = jobType.JobTypeId,
						ShowDocConVerifyType = jobType.ShowDocConVerifyType,
						Assignment = jobType.Assignment,
					
						RowVersion = jobType.RowVersion,
					});
		}
		public JobTypeItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<JobTypeItemViewMap, JobTypeItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public JobType CreateJobType(JobType jobType)
		{
			try
			{
				jobType.JobTypeGUID = Guid.NewGuid();
				base.Add(jobType);
				return jobType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateJobTypeVoid(JobType jobType)
		{
			try
			{
				CreateJobType(jobType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public JobType UpdateJobType(JobType dbJobType, JobType inputJobType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbJobType = dbJobType.MapUpdateValues<JobType>(inputJobType);
				base.Update(dbJobType);
				return dbJobType;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateJobTypeVoid(JobType dbJobType, JobType inputJobType, List<string> skipUpdateFields = null)
		{
			try
			{
				dbJobType = dbJobType.MapUpdateValues<JobType>(inputJobType, skipUpdateFields);
				base.Update(dbJobType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public JobType GetJobTypeByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.JobTypeGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

