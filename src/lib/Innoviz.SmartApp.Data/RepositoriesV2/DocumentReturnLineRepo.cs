using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IDocumentReturnLineRepo
	{
		#region DropDown
		IEnumerable<SelectItem<DocumentReturnLineItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<DocumentReturnLineListView> GetListvw(SearchParameter search);
		DocumentReturnLineItemView GetByIdvw(Guid id);
		DocumentReturnLine Find(params object[] keyValues);
		DocumentReturnLine GetDocumentReturnLineByIdNoTracking(Guid guid);
		DocumentReturnLine CreateDocumentReturnLine(DocumentReturnLine documentReturnLine);
		void CreateDocumentReturnLineVoid(DocumentReturnLine documentReturnLine);
		DocumentReturnLine UpdateDocumentReturnLine(DocumentReturnLine dbDocumentReturnLine, DocumentReturnLine inputDocumentReturnLine, List<string> skipUpdateFields = null);
		void UpdateDocumentReturnLineVoid(DocumentReturnLine dbDocumentReturnLine, DocumentReturnLine inputDocumentReturnLine, List<string> skipUpdateFields = null);
		void Remove(DocumentReturnLine item);
		DocumentReturnLine GetDocumentReturnLineByIdNoTrackingByAccessLevel(Guid guid);
		int GetMaxLineNum(Guid id);
	}
	public class DocumentReturnLineRepo : CompanyBaseRepository<DocumentReturnLine>, IDocumentReturnLineRepo
	{
		public DocumentReturnLineRepo(SmartAppDbContext context) : base(context) { }
		public DocumentReturnLineRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public DocumentReturnLine GetDocumentReturnLineByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.DocumentReturnLineGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<DocumentReturnLineItemViewMap> GetDropDownQuery()
		{
			return (from documentReturnLine in Entity
					select new DocumentReturnLineItemViewMap
					{
						CompanyGUID = documentReturnLine.CompanyGUID,
						Owner = documentReturnLine.Owner,
						OwnerBusinessUnitGUID = documentReturnLine.OwnerBusinessUnitGUID,
						DocumentReturnLineGUID = documentReturnLine.DocumentReturnLineGUID,
						LineNum = documentReturnLine.LineNum,
						DocumentNo = documentReturnLine.DocumentNo
					});
		}
		public IEnumerable<SelectItem<DocumentReturnLineItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<DocumentReturnLine>(search, SysParm.CompanyGUID);
				var documentReturnLine = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<DocumentReturnLineItemViewMap, DocumentReturnLineItemView>().ToDropDownItem(search.ExcludeRowData);


				return documentReturnLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<DocumentReturnLineListViewMap> GetListQuery()
		{
			var result = (from documentReturnLine in Entity
						  join buyerTable in db.Set<BuyerTable>()
						  on documentReturnLine.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
						  from buyerTable in ljbuyerTable.DefaultIfEmpty()

						  join documentType in db.Set<DocumentType>()
						  on documentReturnLine.DocumentTypeGUID equals documentType.DocumentTypeGUID into ljdocumentType
						  from documentType in ljdocumentType.DefaultIfEmpty()
						  select new DocumentReturnLineListViewMap
				{
						CompanyGUID = documentReturnLine.CompanyGUID,
						Owner = documentReturnLine.Owner,
						OwnerBusinessUnitGUID = documentReturnLine.OwnerBusinessUnitGUID,
						DocumentReturnLineGUID = documentReturnLine.DocumentReturnLineGUID,
						LineNum = documentReturnLine.LineNum,
						BuyerTableGUID = documentReturnLine.BuyerTableGUID,
						ContactPersonName = documentReturnLine.ContactPersonName,
						DocumentTypeGUID = documentReturnLine.DocumentTypeGUID,
						DocumentNo = documentReturnLine.DocumentNo,
						DocumentReturnTableGUID = documentReturnLine.DocumentReturnTableGUID.ToString(),
						BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId,buyerTable.BuyerName),
						DocumentType_Values = SmartAppUtil.GetDropDownLabel(documentType.DocumentTypeId, documentType.Description),
						BuyerTable_BuyerId = buyerTable.BuyerId,
						DocumentType_DocumentTypeId = documentType.DocumentTypeId
				});
			return result;
		}
		public SearchResult<DocumentReturnLineListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<DocumentReturnLineListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<DocumentReturnLine>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<DocumentReturnLineListViewMap, DocumentReturnLineListView>();
				result = list.SetSearchResult<DocumentReturnLineListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<DocumentReturnLineItemViewMap> GetItemQuery()
		{
			return (from documentReturnLine in Entity
					join company in db.Set<Company>()
					on documentReturnLine.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on documentReturnLine.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljDocumentReturnLineOwnerBU
					from ownerBU in ljDocumentReturnLineOwnerBU.DefaultIfEmpty()

					join documentReturnTable in db.Set<DocumentReturnTable>()
					on documentReturnLine.DocumentReturnTableGUID equals documentReturnTable.DocumentReturnTableGUID into ljdocumentReturnTable
					from documentReturnTable in ljdocumentReturnTable.DefaultIfEmpty()

					join documentStatus in db.Set<DocumentStatus>()
					on documentReturnTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus
					from documentStatus in ljdocumentStatus.DefaultIfEmpty()

					select new DocumentReturnLineItemViewMap
					{
						CompanyGUID = documentReturnLine.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = documentReturnLine.Owner,
						OwnerBusinessUnitGUID = documentReturnLine.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = documentReturnLine.CreatedBy,
						CreatedDateTime = documentReturnLine.CreatedDateTime,
						ModifiedBy = documentReturnLine.ModifiedBy,
						ModifiedDateTime = documentReturnLine.ModifiedDateTime,
						DocumentReturnLineGUID = documentReturnLine.DocumentReturnLineGUID,
						Address = documentReturnLine.Address,
						BuyerTableGUID = documentReturnLine.BuyerTableGUID,
						ContactPersonName = documentReturnLine.ContactPersonName,
						DocumentNo = documentReturnLine.DocumentNo,
						DocumentReturnTableGUID = documentReturnLine.DocumentReturnTableGUID,
						DocumentTypeGUID = documentReturnLine.DocumentTypeGUID,
						LineNum = documentReturnLine.LineNum,
						Remark = documentReturnLine.Remark,
						DocumentReturnTable_ContactTo = documentReturnTable.ContactTo,
						DocumentReturnTable_DocumentStatus_StatusId = documentStatus.StatusId,
						DocumentReturnTable_DocumentReturnId = SmartAppUtil.GetDropDownLabel(documentReturnTable.DocumentReturnId, documentReturnTable.ContactPersonName),
					
						RowVersion = documentReturnLine.RowVersion,
					});
		}
		public DocumentReturnLineItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<DocumentReturnLineItemViewMap, DocumentReturnLineItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public DocumentReturnLine CreateDocumentReturnLine(DocumentReturnLine documentReturnLine)
		{
			try
			{
				documentReturnLine.DocumentReturnLineGUID = Guid.NewGuid();
				base.Add(documentReturnLine);
				return documentReturnLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateDocumentReturnLineVoid(DocumentReturnLine documentReturnLine)
		{
			try
			{
				CreateDocumentReturnLine(documentReturnLine);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentReturnLine UpdateDocumentReturnLine(DocumentReturnLine dbDocumentReturnLine, DocumentReturnLine inputDocumentReturnLine, List<string> skipUpdateFields = null)
		{
			try
			{
				dbDocumentReturnLine = dbDocumentReturnLine.MapUpdateValues<DocumentReturnLine>(inputDocumentReturnLine);
				base.Update(dbDocumentReturnLine);
				return dbDocumentReturnLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateDocumentReturnLineVoid(DocumentReturnLine dbDocumentReturnLine, DocumentReturnLine inputDocumentReturnLine, List<string> skipUpdateFields = null)
		{
			try
			{
				dbDocumentReturnLine = dbDocumentReturnLine.MapUpdateValues<DocumentReturnLine>(inputDocumentReturnLine, skipUpdateFields);
				base.Update(dbDocumentReturnLine);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public DocumentReturnLine GetDocumentReturnLineByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.DocumentReturnLineGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        public int GetMaxLineNum(Guid id)
        {
			try
			{
				var result = (from documentReturnLine in db.Set<DocumentReturnLine>()
							   where documentReturnLine.DocumentReturnTableGUID == id
							   orderby documentReturnLine.LineNum descending
							   select new DocumentReturnLineItemViewMap
							   {
								   LineNum = documentReturnLine.LineNum
							   }
							   ).AsNoTracking().FirstOrDefault();
                if (result != null)
                {
					return result.LineNum;
                }
   				return 0;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
    }
}

