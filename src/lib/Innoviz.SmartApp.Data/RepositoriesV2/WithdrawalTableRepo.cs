using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Data.ServicesV2;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IWithdrawalTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<WithdrawalTableItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<WithdrawalTableListView> GetListvw(SearchParameter search);
		WithdrawalTableItemView GetByIdvw(Guid id);
		WithdrawalTable Find(params object[] keyValues);
		WithdrawalTable GetWithdrawalTableByIdNoTracking(Guid guid);
		WithdrawalTable CreateWithdrawalTable(WithdrawalTable withdrawalTable);
		void CreateWithdrawalTableVoid(WithdrawalTable withdrawalTable);
		WithdrawalTable UpdateWithdrawalTable(WithdrawalTable dbWithdrawalTable, WithdrawalTable inputWithdrawalTable, List<string> skipUpdateFields = null);
		void UpdateWithdrawalTableVoid(WithdrawalTable dbWithdrawalTable, WithdrawalTable inputWithdrawalTable, List<string> skipUpdateFields = null);
		void Remove(WithdrawalTable item);
		WithdrawalTable GetWithdrawalTableByIdNoTrackingByAccessLevel(Guid guid);
		IEnumerable<SelectItem<WithdrawalTableItemView>> GetDropDownItemByMainAgreementTable(SearchParameter search);
		#region function
		WithdrawalTable GetWithdrawalTableByInvoiceTableRefGuid(Guid guid);
		WithdrawalTable GetWithdrawalExtendTerm(Guid withdrawalTableGuid);
		WithdrawalTableItemView GetWithdrawalItemViewExtendTerm(Guid withdrawalTableGuid);
		List<WithdrawalTable> GetWithdrawalTableByExtendWithdrawalTableGUID(Guid withdrawalTableGuid);
		IEnumerable<WithdrawalTable> GetWithdrawalTableByOriWithdrawalTableGUID(Guid oriWithdrawalTableGuid);
		WithdrawalTableItemView GetWithdrawalItemViewPostWithdrawal(Guid withdrawalTableGuid);
		#endregion
		WithdrawalTable GetWithdrawalTableByInvoice(Guid guid);
		List<WithdrawalTable> GetWithdrawalTableByIdNoTracking(IEnumerable<Guid> withdrawalTableGuids);
		void ValidateAdd(IEnumerable<WithdrawalTable> items);
		void ValidateAdd(WithdrawalTable item);
	}
    public class WithdrawalTableRepo : CompanyBaseRepository<WithdrawalTable>, IWithdrawalTableRepo
	{
		public WithdrawalTableRepo(SmartAppDbContext context) : base(context) { }
		public WithdrawalTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public WithdrawalTable GetWithdrawalTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.WithdrawalTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<WithdrawalTable> GetWithdrawalTableByIdNoTracking(IEnumerable<Guid> withdrawalTableGuids)
		{
			try
			{
				var result = Entity.Where(w => withdrawalTableGuids.Contains(w.WithdrawalTableGUID))
									.AsNoTracking()
									.ToList();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<WithdrawalTableItemViewMap> GetDropDownQuery()
		{
			return (from withdrawalTable in Entity
					select new WithdrawalTableItemViewMap
					{
						CompanyGUID = withdrawalTable.CompanyGUID,
						Owner = withdrawalTable.Owner,
						OwnerBusinessUnitGUID = withdrawalTable.OwnerBusinessUnitGUID,
						WithdrawalTableGUID = withdrawalTable.WithdrawalTableGUID,
						WithdrawalId = withdrawalTable.WithdrawalId,
						Description = withdrawalTable.Description,
						DueDate = withdrawalTable.DueDate
					});
		}
		private IQueryable<WithdrawalTableItemViewMap> GetDropDownQueryByMainAgreementTable()
		{
			return (from withdrawalTable in Entity
					join documentStatus in db.Set<DocumentStatus>()
					on withdrawalTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
					select new WithdrawalTableItemViewMap
					{
						CompanyGUID = withdrawalTable.CompanyGUID,
						Owner = withdrawalTable.Owner,
						OwnerBusinessUnitGUID = withdrawalTable.OwnerBusinessUnitGUID,
						WithdrawalTableGUID = withdrawalTable.WithdrawalTableGUID,
						WithdrawalId = withdrawalTable.WithdrawalId,
						Description = withdrawalTable.Description,
						DocumentStatus_StatusId = documentStatus.StatusId,
						CreditAppTableGUID = withdrawalTable.CreditAppTableGUID,
						DueDate = withdrawalTable.DueDate
					});
		}
		public IEnumerable<SelectItem<WithdrawalTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<WithdrawalTable>(search, SysParm.CompanyGUID);
				var withdrawalTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<WithdrawalTableItemViewMap, WithdrawalTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return withdrawalTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<WithdrawalTableListViewMap> GetListQuery()
		{
			return (from withdrawalTable in Entity
					join customerTable in db.Set<CustomerTable>() on withdrawalTable.CustomerTableGUID equals customerTable.CustomerTableGUID
					join documentStatus in db .Set<DocumentStatus>() on withdrawalTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
					join creditTerm in db.Set<CreditTerm>() on withdrawalTable.CreditTermGUID equals creditTerm.CreditTermGUID

					select new WithdrawalTableListViewMap
				{
						CompanyGUID = withdrawalTable.CompanyGUID,
						Owner = withdrawalTable.Owner,
						OwnerBusinessUnitGUID = withdrawalTable.OwnerBusinessUnitGUID,
						WithdrawalTableGUID = withdrawalTable.WithdrawalTableGUID,
						WithdrawalId = withdrawalTable.WithdrawalId,
						Description = withdrawalTable.Description,
						CustomerTableGUID = withdrawalTable.CustomerTableGUID,
						WithdrawalDate = withdrawalTable.WithdrawalDate,
						DueDate = withdrawalTable.DueDate,
						CreditTermGUID = withdrawalTable.CreditTermGUID,
						DocumentStatusGUID = withdrawalTable.DocumentStatusGUID,
						CustomerTable_CustomerId = customerTable.CustomerId,
						CreditTerm_CreditTermId = creditTerm.CreditTermId,
						DocumentStatus_StatusId = documentStatus.StatusId,
						CreditAppTableGUID = withdrawalTable.CreditAppTableGUID,
						DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
						CreditTerm_Values = SmartAppUtil.GetDropDownLabel(creditTerm.CreditTermId, creditTerm.Description),
						TermExtension = withdrawalTable.TermExtension,
						OriginWithdrawalTableGUID = withdrawalTable.OriginalWithdrawalTableGUID,
						NumberOfExtension = withdrawalTable.NumberOfExtension
					});
		}
		public SearchResult<WithdrawalTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<WithdrawalTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<WithdrawalTable>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<WithdrawalTableListViewMap, WithdrawalTableListView>();
				result = list.SetSearchResult<WithdrawalTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<WithdrawalTableItemViewMap> GetItemQuery()
		{
			return (from withdrawalTable in Entity
					join company in db.Set<Company>()
					on withdrawalTable.CompanyGUID equals company.CompanyGUID
					join documentStatus in db.Set<DocumentStatus>() on withdrawalTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
					join creditAppTable in db.Set<CreditAppTable>() on withdrawalTable.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID
					join creditLimitType in db.Set<CreditLimitType>() on creditAppTable.CreditLimitTypeGUID equals creditLimitType.CreditLimitTypeGUID
					join customerTable in db.Set<CustomerTable>() on withdrawalTable.CustomerTableGUID equals customerTable.CustomerTableGUID

					join buyerTable in db.Set<BuyerTable>()
					on withdrawalTable.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljBuyerTable
					from buyerTable in ljBuyerTable.DefaultIfEmpty()

					join methodOfPayment in db.Set<MethodOfPayment>() 
					on withdrawalTable.MethodOfPaymentGUID equals methodOfPayment.MethodOfPaymentGUID into ljWithdrawalTableMethodOfPayment
					from methodOfPayment in ljWithdrawalTableMethodOfPayment.DefaultIfEmpty()

					join originWithdrawal in db.Set<WithdrawalTable>()
					on withdrawalTable.OriginalWithdrawalTableGUID equals originWithdrawal.WithdrawalTableGUID into ljWithdrawalTableOrigin
					from originWithdrawal in ljWithdrawalTableOrigin.DefaultIfEmpty()

					join extendWithdrawal in db.Set<WithdrawalTable>()
					on withdrawalTable.ExtendWithdrawalTableGUID equals extendWithdrawal.WithdrawalTableGUID into ljWithdrawalTableExtend
					from extendWithdrawal in ljWithdrawalTableExtend.DefaultIfEmpty()

					join ownerBU in db.Set<BusinessUnit>()
					on withdrawalTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljWithdrawalTableOwnerBU
					from ownerBU in ljWithdrawalTableOwnerBU.DefaultIfEmpty()

					join creditTerm in db.Set<CreditTerm>()
					on withdrawalTable.CreditTermGUID equals creditTerm.CreditTermGUID into ljcreditTerm
					from creditTerm in ljcreditTerm.DefaultIfEmpty()

					join invoiceRevenueType in db.Set<InvoiceRevenueType>()
					on withdrawalTable.TermExtensionInvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID into ljinvoiceRevenueType
					from invoiceRevenueType in ljinvoiceRevenueType.DefaultIfEmpty()
					select new WithdrawalTableItemViewMap
					{
						CompanyGUID = withdrawalTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = withdrawalTable.Owner,
						OwnerBusinessUnitGUID = withdrawalTable.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = withdrawalTable.CreatedBy,
						CreatedDateTime = withdrawalTable.CreatedDateTime,
						ModifiedBy = withdrawalTable.ModifiedBy,
						ModifiedDateTime = withdrawalTable.ModifiedDateTime,
						WithdrawalTableGUID = withdrawalTable.WithdrawalTableGUID,
						AssignmentAgreementTableGUID = withdrawalTable.AssignmentAgreementTableGUID,
						AssignmentAmount = withdrawalTable.AssignmentAmount,
						BuyerAgreementTableGUID = withdrawalTable.BuyerAgreementTableGUID,
						BuyerTableGUID = withdrawalTable.BuyerTableGUID,
						CreditAppLineGUID = withdrawalTable.CreditAppLineGUID,
						CreditAppTableGUID = withdrawalTable.CreditAppTableGUID,
						CreditComment = withdrawalTable.CreditComment,
						CreditMarkComment = withdrawalTable.CreditMarkComment,
						CreditTermGUID = withdrawalTable.CreditTermGUID,
						CustomerTableGUID = withdrawalTable.CustomerTableGUID,
						Description = withdrawalTable.Description,
						Dimension1GUID = withdrawalTable.Dimension1GUID,
						Dimension2GUID = withdrawalTable.Dimension2GUID,
						Dimension3GUID = withdrawalTable.Dimension3GUID,
						Dimension4GUID = withdrawalTable.Dimension4GUID,
						Dimension5GUID = withdrawalTable.Dimension5GUID,
						DocumentStatusGUID = withdrawalTable.DocumentStatusGUID,
						DueDate = withdrawalTable.DueDate,
						ExtendInterestDate = withdrawalTable.ExtendInterestDate,
						ExtendWithdrawalTableGUID = withdrawalTable.ExtendWithdrawalTableGUID,
						InterestCutDay = withdrawalTable.InterestCutDay,
						MarketingComment = withdrawalTable.MarketingComment,
						MarketingMarkComment = withdrawalTable.MarketingMarkComment,
						MethodOfPaymentGUID = withdrawalTable.MethodOfPaymentGUID,
						NumberOfExtension = withdrawalTable.NumberOfExtension,
						OperationComment = withdrawalTable.OperationComment,
						OperationMarkComment = withdrawalTable.OperationMarkComment,
						OriginalWithdrawalTableGUID = withdrawalTable.OriginalWithdrawalTableGUID,
						ProductType = withdrawalTable.ProductType,
						ReceiptTempTableGUID = withdrawalTable.ReceiptTempTableGUID,
						Remark = withdrawalTable.Remark,
						RetentionAmount = withdrawalTable.RetentionAmount,
						RetentionCalculateBase = withdrawalTable.RetentionCalculateBase,
						RetentionPct = withdrawalTable.RetentionPct,
						SettleTermExtensionFeeAmount = withdrawalTable.SettleTermExtensionFeeAmount,
						TermExtension = withdrawalTable.TermExtension,
						TermExtensionFeeAmount = withdrawalTable.TermExtensionFeeAmount,
						TermExtensionInvoiceRevenueTypeGUID = withdrawalTable.TermExtensionInvoiceRevenueTypeGUID,
						TotalInterestPct = withdrawalTable.TotalInterestPct,
						WithdrawalAmount = withdrawalTable.WithdrawalAmount,
						WithdrawalDate = withdrawalTable.WithdrawalDate,
						WithdrawalId = withdrawalTable.WithdrawalId,
						DocumentStatus_StatusId = documentStatus.StatusId,
						DocumentReasonGUID = withdrawalTable.DocumentReasonGUID,
						CreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
						CreditLimitType_CreditLimitConditionType = creditLimitType.CreditLimitConditionType,
						CreditLimitType_ValidateBuyerAgreement = creditLimitType.ValidateBuyerAgreement,
						BuyerTable_Values = (buyerTable != null) ? SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName) : null,
						CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
						DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
						MethodOfPayment_Values = (methodOfPayment != null) ? SmartAppUtil.GetDropDownLabel(methodOfPayment.MethodOfPaymentId, methodOfPayment.Description) : null,
						OriginalWithdrawalTable_Values = (originWithdrawal != null) ? SmartAppUtil.GetDropDownLabel(originWithdrawal.WithdrawalId, originWithdrawal.Description) : null,
						ExtendWithdrawalTable_Values = (extendWithdrawal != null) ? SmartAppUtil.GetDropDownLabel(extendWithdrawal.WithdrawalId, extendWithdrawal.Description) : null,
						OperReportSignatureGUID = withdrawalTable.OperReportSignatureGUID,
						CreditTerm_Values = SmartAppUtil.GetDropDownLabel(creditTerm.CreditTermId,creditTerm.Description),
						InvoiceRevenueType_IntercompanyTableGUID = (invoiceRevenueType != null) ? invoiceRevenueType.IntercompanyTableGUID : null,

						RowVersion = withdrawalTable.RowVersion,
					});
		}
		public WithdrawalTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<WithdrawalTableItemViewMap, WithdrawalTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public WithdrawalTable CreateWithdrawalTable(WithdrawalTable withdrawalTable)
		{
			try
			{
				withdrawalTable.WithdrawalTableGUID = withdrawalTable.WithdrawalTableGUID == Guid.Empty? Guid.NewGuid(): withdrawalTable.WithdrawalTableGUID;
				base.Add(withdrawalTable);
				return withdrawalTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateWithdrawalTableVoid(WithdrawalTable withdrawalTable)
		{
			try
			{
				CreateWithdrawalTable(withdrawalTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public WithdrawalTable UpdateWithdrawalTable(WithdrawalTable dbWithdrawalTable, WithdrawalTable inputWithdrawalTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbWithdrawalTable = dbWithdrawalTable.MapUpdateValues<WithdrawalTable>(inputWithdrawalTable);
				base.Update(dbWithdrawalTable);
				return dbWithdrawalTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateWithdrawalTableVoid(WithdrawalTable dbWithdrawalTable, WithdrawalTable inputWithdrawalTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbWithdrawalTable = dbWithdrawalTable.MapUpdateValues<WithdrawalTable>(inputWithdrawalTable, skipUpdateFields);
				base.Update(dbWithdrawalTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public override void ValidateAdd(WithdrawalTable item)
		{
			ValidateCreateShare(item);
			base.ValidateAdd(item);
		}
		public override void ValidateUpdate(WithdrawalTable item)
		{
			ValidateUpdateShare(item);
			base.ValidateUpdate(item);
		}
		#endregion Create, Update, Delete
		public void ValidateCreateShare(WithdrawalTable item)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
				CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTracking(item.CreditAppTableGUID);

				if (item.WithdrawalAmount == 0)
				{
					ex.AddData("ERROR.GREATER_THAN_ZERO", new string[] { "LABEL.WITHDRAWAL_AMOUNT" });
				}

				if (item.SettleTermExtensionFeeAmount > item.TermExtensionFeeAmount)
				{
					ex.AddData("ERROR.90031", new string[] { "LABEL.SETTLE_TERM_EXTENSION_FEE_AMOUNT", "LABEL.TERM_EXTENSION_FEE_AMOUNT" });
				}

				if (item.WithdrawalDate >= creditAppTable.ExpiryDate)
				{
					ex.AddData("ERROR.90113", new string[] { creditAppTable.CreditAppId, "LABEL.WITHDRAWAL" });
				}

				if (item.CreditAppLineGUID != null)
                {
					ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
					CreditAppLine creditAppLine = creditAppLineRepo.GetCreditAppLineByIdNoTracking(item.CreditAppLineGUID.Value);
					if (item.WithdrawalDate >= creditAppLine.ExpiryDate)
					{
						ex.AddData("ERROR.90153", new string[] { SmartAppUtil.GetDropDownLabel(creditAppLine.LineNum.ToString(), creditAppLine.ApprovedCreditLimitLine.ToString()) , "LABEL.WITHDRAWAL_LINE" });
					}
				}

				if (!item.TermExtension)
				{
					// Validate No.6
					ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
					decimal creditLimitBalanceByCreditConditionType = creditAppTableService.GetCreditLimitBalanceByCreditConditionType(item.CreditAppTableGUID.ToString(), item.WithdrawalDate.DateToString());

					if (item.WithdrawalAmount > creditLimitBalanceByCreditConditionType)
					{
						ex.AddData("ERROR.90107", new string[] { "LABEL.WITHDRAWAL_AMOUNT", "LABEL.CREDIT_LIMIT_CONDITION_TYPE", creditLimitBalanceByCreditConditionType.ToString() });
					}
					if (item.CreditAppLineGUID.HasValue)
					{

						// Validate No.7
						decimal creditLimitBalanceByCreditAppLine = item.CreditAppLineGUID.HasValue ? creditAppTableService.GetCreditLimitBalanceByCreditAppLine(item.CreditAppLineGUID.ToString()) : 0;

						if (item.WithdrawalAmount > creditLimitBalanceByCreditAppLine)
						{
							ex.AddData("ERROR.90107", new string[] { "LABEL.WITHDRAWAL_AMOUNT", "LABEL.CREDIT_APPLICATION_LINE", creditLimitBalanceByCreditAppLine.ToString() });
						}

						// Validate No.8
						IBuyerCreditLimitByProductRepo buyerCreditLimitByProductRepo = new BuyerCreditLimitByProductRepo(db);
						IEnumerable<BuyerCreditLimitByProduct> buyerCreditLimitByProducts = buyerCreditLimitByProductRepo.GetBuyerCreditLimitByProductByBuyer(item.BuyerTableGUID.HasValue ? item.BuyerTableGUID.Value : Guid.Empty)
																														 .Where(w => w.ProductType == item.ProductType);
						bool isBuyerCreditLimitByProducts = ConditionService.IsNotZero(buyerCreditLimitByProducts.Count());
						decimal creditLimitBalanceByProduct = creditAppTableService.GetCreditLimitBalanceByProduct(item.BuyerTableGUID.ToString(), (ProductType)item.ProductType);

						if (isBuyerCreditLimitByProducts && item.WithdrawalAmount > creditLimitBalanceByProduct)
						{
							ex.AddData("ERROR.90107", new string[] { "LABEL.WITHDRAWAL_AMOUNT", "LABEL.BUYER_CREDIT_LIMIT_BY_PRODUCT", creditLimitBalanceByProduct.ToString() });
						}
					}
				}

				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void ValidateUpdateShare(WithdrawalTable item)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");

				if (item.SettleTermExtensionFeeAmount > item.TermExtensionFeeAmount)
				{
					ex.AddData("ERROR.90031", new string[] { "LABEL.SETTLE_TERM_EXTENSION_FEE_AMOUNT", "LABEL.TERM_EXTENSION_FEE_AMOUNT" });
				}

				if (item.CreditAppLineGUID != null)
				{
					ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
					CreditAppLine creditAppLine = creditAppLineRepo.GetCreditAppLineByIdNoTracking(item.CreditAppLineGUID.Value);
					if (item.WithdrawalDate >= creditAppLine.ExpiryDate)
					{
						ex.AddData("ERROR.90153", new string[] { SmartAppUtil.GetDropDownLabel(creditAppLine.LineNum.ToString(), creditAppLine.ApprovedCreditLimitLine.ToString()), "LABEL.WITHDRAWAL_LINE" });
					}
				}

				if (!item.TermExtension)
				{
					// Validate No.3
					ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
					decimal creditLimitBalanceByCreditConditionType = creditAppTableService.GetCreditLimitBalanceByCreditConditionType(item.CreditAppTableGUID.ToString(), item.WithdrawalDate.DateToString());

					if (item.WithdrawalAmount > creditLimitBalanceByCreditConditionType)
					{
						ex.AddData("ERROR.90107", new string[] { "LABEL.WITHDRAWAL_AMOUNT", "LABEL.CREDIT_LIMIT_CONDITION_TYPE", creditLimitBalanceByCreditConditionType.ToString() });
					}
					if (item.CreditAppLineGUID.HasValue)
					{

						// Validate No.4
						decimal creditLimitBalanceByCreditAppLine = item.CreditAppLineGUID.HasValue ? creditAppTableService.GetCreditLimitBalanceByCreditAppLine(item.CreditAppLineGUID.ToString()) : 0;

						if (item.WithdrawalAmount > creditLimitBalanceByCreditAppLine)
						{
							ex.AddData("ERROR.90107", new string[] { "LABEL.WITHDRAWAL_AMOUNT", "LABEL.CREDIT_APPLICATION_LINE", creditLimitBalanceByCreditAppLine.ToString() });
						}

						// Validate No.5
						IBuyerCreditLimitByProductRepo buyerCreditLimitByProductRepo = new BuyerCreditLimitByProductRepo(db);
						IEnumerable<BuyerCreditLimitByProduct> buyerCreditLimitByProducts = buyerCreditLimitByProductRepo.GetBuyerCreditLimitByProductByBuyer(item.BuyerTableGUID.HasValue ? item.BuyerTableGUID.Value : Guid.Empty)
																														 .Where(w => w.ProductType == item.ProductType);
						bool isBuyerCreditLimitByProducts = ConditionService.IsNotZero(buyerCreditLimitByProducts.Count());
						decimal creditLimitBalanceByProduct = creditAppTableService.GetCreditLimitBalanceByProduct(item.BuyerTableGUID.ToString(), (ProductType)item.ProductType);

						if (isBuyerCreditLimitByProducts && item.WithdrawalAmount > creditLimitBalanceByProduct)
						{
							ex.AddData("ERROR.90107", new string[] { "LABEL.WITHDRAWAL_AMOUNT", "LABEL.BUYER_CREDIT_LIMIT_BY_PRODUCT", creditLimitBalanceByProduct.ToString() });
						}
					}
				}

				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public WithdrawalTable GetWithdrawalTableByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.WithdrawalTableGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<WithdrawalTableItemView>> GetDropDownItemByMainAgreementTable(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<WithdrawalTable>(search, SysParm.CompanyGUID);
				var withdrawalTable = GetDropDownQueryByMainAgreementTable().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<WithdrawalTableItemViewMap, WithdrawalTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return withdrawalTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        #region function
		public WithdrawalTable GetWithdrawalTableByInvoiceTableRefGuid(Guid guid)
        {
            try
            {
				WithdrawalTable withdrawalTable = (from withdrawal in Entity
												   join withdrawalLine in db.Set<WithdrawalLine>() on withdrawal.WithdrawalTableGUID equals withdrawalLine.WithdrawalTableGUID
												   where withdrawalLine.WithdrawalLineGUID == guid
												   select withdrawal).AsNoTracking().FirstOrDefault();
				return withdrawalTable;
			}
            catch (Exception e)
            {

                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public WithdrawalTable GetWithdrawalExtendTerm(Guid withdrawalTableGuid)
        {
            try
            {
				WithdrawalTable withdrawalTable = (from wt in Entity
												   join documentStatus in db.Set<DocumentStatus>() 
												   on wt.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
												   where wt.OriginalWithdrawalTableGUID == withdrawalTableGuid
															&& wt.TermExtension == true
															&& documentStatus.StatusId == ((int)WithdrawalStatus.Posted).ToString()
												   select wt).OrderByDescending(o => o.WithdrawalDate)
				.AsNoTracking().FirstOrDefault();
				return withdrawalTable;
			}
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public WithdrawalTableItemView GetWithdrawalItemViewExtendTerm(Guid withdrawalTableGuid)
        {
			try
			{
				WithdrawalTableItemView withdrawalTableItemView = (from withdrawalTable in Entity
																   join documentStatus in db.Set<DocumentStatus>() on withdrawalTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
																   join customer in db.Set<CustomerTable>() on withdrawalTable.CustomerTableGUID equals customer.CustomerTableGUID into ljcusttomer
																   from customer in ljcusttomer.DefaultIfEmpty()
																   join buyer in db.Set<BuyerTable>() on withdrawalTable.BuyerTableGUID equals buyer.BuyerTableGUID into ljbuyer
																   from buyer in ljbuyer.DefaultIfEmpty()
																   join creditTerm in db.Set<CreditTerm>() on withdrawalTable.CreditTermGUID equals creditTerm.CreditTermGUID
																   where withdrawalTable.WithdrawalTableGUID == withdrawalTableGuid
																   select new WithdrawalTableItemView
																   {
																	   WithdrawalTableGUID = withdrawalTable.WithdrawalTableGUID.ToString(),
																	   WithdrawalId = withdrawalTable.WithdrawalId,
																	   DocumentStatus_StatusId = SmartAppUtil.GetDropDownLabel(documentStatus.StatusId, documentStatus.Description),
																	   CustomerTable_CustomerId = customer == null ? null : SmartAppUtil.GetDropDownLabel(customer.CustomerId, customer.Name),
																	   BuyerTable_BuyerId = buyer == null ? null : SmartAppUtil.GetDropDownLabel(buyer.BuyerId, buyer.BuyerName),
																	   WithdrawalDate = withdrawalTable.WithdrawalDate.DateToString(),
																	   CreditTerm_CreditTermId = SmartAppUtil.GetDropDownLabel(creditTerm.CreditTermId, creditTerm.Description),
																	   DueDate = withdrawalTable.DueDate.DateToString(),
																	   TermExtension = withdrawalTable.TermExtension,
																	   NumberOfExtension = withdrawalTable.NumberOfExtension,
																	   TotalInterestPct = withdrawalTable.TotalInterestPct,
																	   WithdrawalAmount = withdrawalTable.WithdrawalAmount,
																	   DocumentStatusGUID = withdrawalTable.DocumentStatusGUID.ToString(),
																	   CompanyGUID = withdrawalTable.CompanyGUID.ToString()
																   }).AsNoTracking().FirstOrDefault();
				return withdrawalTableItemView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<WithdrawalTable> GetWithdrawalTableByExtendWithdrawalTableGUID(Guid withdrawalTableGuid)
        {
            try
            {
				List<WithdrawalTable> withdrawalTables = Entity.Where(w => w.ExtendWithdrawalTableGUID == withdrawalTableGuid)
															.AsNoTracking().ToList();
				return withdrawalTables;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<WithdrawalTable> GetWithdrawalTableByOriWithdrawalTableGUID(Guid oriWithdrawalTableGuid)
		{
			try
			{
				IEnumerable< WithdrawalTable> withdrawalTables = Entity.Where(w => w.OriginalWithdrawalTableGUID == oriWithdrawalTableGuid)
				.AsNoTracking().ToList();
				return withdrawalTables;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public WithdrawalTableItemView GetWithdrawalItemViewPostWithdrawal(Guid withdrawalTableGuid)
		{
			try
			{
				WithdrawalTableItemView withdrawalTableItemView = (from withdrawalTable in Entity
																   join documentStatus in db.Set<DocumentStatus>() on withdrawalTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
																   join customer in db.Set<CustomerTable>() on withdrawalTable.CustomerTableGUID equals customer.CustomerTableGUID
																   join buyer in db.Set<BuyerTable>() on withdrawalTable.BuyerTableGUID equals buyer.BuyerTableGUID into ljBuyerTable
																   from buyer in ljBuyerTable.DefaultIfEmpty()
																   join creditTerm in db.Set<CreditTerm>() on withdrawalTable.CreditTermGUID equals creditTerm.CreditTermGUID
																   where withdrawalTable.WithdrawalTableGUID == withdrawalTableGuid
																   select new WithdrawalTableItemView
																   {
																	   WithdrawalTableGUID = withdrawalTable.WithdrawalTableGUID.ToString(),
																	   WithdrawalId = SmartAppUtil.GetDropDownLabel(withdrawalTable.WithdrawalId, withdrawalTable.Description),
																	   DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.StatusId, documentStatus.Description),
																	   CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customer.CustomerId, customer.Name),
																	   BuyerTable_Values = buyer != null ? SmartAppUtil.GetDropDownLabel(buyer.BuyerId, buyer.BuyerName) : null,
																	   WithdrawalDate = withdrawalTable.WithdrawalDate.DateToString(),
																	   CreditTerm_CreditTermId = SmartAppUtil.GetDropDownLabel(creditTerm.CreditTermId, creditTerm.Description),
																	   DueDate = withdrawalTable.DueDate.DateToString(),
																	   TermExtension = withdrawalTable.TermExtension,
																	   NumberOfExtension = withdrawalTable.NumberOfExtension,
																	   TotalInterestPct = withdrawalTable.TotalInterestPct,
																	   WithdrawalAmount = withdrawalTable.WithdrawalAmount
																   }).AsNoTracking().FirstOrDefault();
				return withdrawalTableItemView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		public WithdrawalTable GetWithdrawalTableByInvoice(Guid guid)
		{
			try
			{
				return (from withdrawalLine in db.Set<WithdrawalLine>()
						join withdrawalTable in Entity on withdrawalLine.WithdrawalTableGUID equals withdrawalTable.WithdrawalTableGUID
						where withdrawalLine.WithdrawalLineInvoiceTableGUID == guid
						select withdrawalTable).AsNoTracking().FirstOrDefault();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #region Validate
        public override void ValidateAdd(IEnumerable<WithdrawalTable> items)
        {
            base.ValidateAdd(items);
        }
        #endregion Validate
    }
}

