using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IBookmarkDocumentTemplateTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<BookmarkDocumentTemplateTableItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<BookmarkDocumentTemplateTableListView> GetListvw(SearchParameter search);
		BookmarkDocumentTemplateTableItemView GetByIdvw(Guid id);
		BookmarkDocumentTemplateTable Find(params object[] keyValues);
		BookmarkDocumentTemplateTable GetBookmarkDocumentTemplateTableByIdNoTracking(Guid guid);
		BookmarkDocumentTemplateTable CreateBookmarkDocumentTemplateTable(BookmarkDocumentTemplateTable bookmarkDocumentTemplateTable);
		void CreateBookmarkDocumentTemplateTableVoid(BookmarkDocumentTemplateTable bookmarkDocumentTemplateTable);
		BookmarkDocumentTemplateTable UpdateBookmarkDocumentTemplateTable(BookmarkDocumentTemplateTable dbBookmarkDocumentTemplateTable, BookmarkDocumentTemplateTable inputBookmarkDocumentTemplateTable, List<string> skipUpdateFields = null);
		void UpdateBookmarkDocumentTemplateTableVoid(BookmarkDocumentTemplateTable dbBookmarkDocumentTemplateTable, BookmarkDocumentTemplateTable inputBookmarkDocumentTemplateTable, List<string> skipUpdateFields = null);
		void Remove(BookmarkDocumentTemplateTable item);
		BookmarkDocumentTemplateTable GetBookmarkDocumentTemplateTableByIdNoTrackingByAccessLevel(Guid guid);
	}
	public class BookmarkDocumentTemplateTableRepo : CompanyBaseRepository<BookmarkDocumentTemplateTable>, IBookmarkDocumentTemplateTableRepo
	{
		public BookmarkDocumentTemplateTableRepo(SmartAppDbContext context) : base(context) { }
		public BookmarkDocumentTemplateTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BookmarkDocumentTemplateTable GetBookmarkDocumentTemplateTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BookmarkDocumentTemplateTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<BookmarkDocumentTemplateTableItemViewMap> GetDropDownQuery()
		{
			var result = (from bookmarkDocumentTemplateTable in Entity
					select new BookmarkDocumentTemplateTableItemViewMap
					{
						CompanyGUID = bookmarkDocumentTemplateTable.CompanyGUID,
						Owner = bookmarkDocumentTemplateTable.Owner,
						OwnerBusinessUnitGUID = bookmarkDocumentTemplateTable.OwnerBusinessUnitGUID,
						BookmarkDocumentTemplateTableGUID = bookmarkDocumentTemplateTable.BookmarkDocumentTemplateTableGUID,
						BookmarkDocumentTemplateId = bookmarkDocumentTemplateTable.BookmarkDocumentTemplateId,
						Description = bookmarkDocumentTemplateTable.Description,
						BookmarkDocumentRefType=bookmarkDocumentTemplateTable.BookmarkDocumentRefType
					});
			return result;
		}
		public IEnumerable<SelectItem<BookmarkDocumentTemplateTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<BookmarkDocumentTemplateTable>(search, SysParm.CompanyGUID);
				var bookmarkDocumentTemplateTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<BookmarkDocumentTemplateTableItemViewMap, BookmarkDocumentTemplateTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return bookmarkDocumentTemplateTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<BookmarkDocumentTemplateTableListViewMap> GetListQuery()
		{
			return (from bookmarkDocumentTemplateTable in Entity
				select new BookmarkDocumentTemplateTableListViewMap
				{
						CompanyGUID = bookmarkDocumentTemplateTable.CompanyGUID,
						Owner = bookmarkDocumentTemplateTable.Owner,
						OwnerBusinessUnitGUID = bookmarkDocumentTemplateTable.OwnerBusinessUnitGUID,
						BookmarkDocumentTemplateTableGUID = bookmarkDocumentTemplateTable.BookmarkDocumentTemplateTableGUID,
						BookmarkDocumentTemplateId = bookmarkDocumentTemplateTable.BookmarkDocumentTemplateId,
						Description = bookmarkDocumentTemplateTable.Description,
						BookmarkDocumentRefType = bookmarkDocumentTemplateTable.BookmarkDocumentRefType,
				});
		}
		public SearchResult<BookmarkDocumentTemplateTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<BookmarkDocumentTemplateTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<BookmarkDocumentTemplateTable>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<BookmarkDocumentTemplateTableListViewMap, BookmarkDocumentTemplateTableListView>();
				result = list.SetSearchResult<BookmarkDocumentTemplateTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<BookmarkDocumentTemplateTableItemViewMap> GetItemQuery()
		{
			return (from bookmarkDocumentTemplateTable in Entity
					join company in db.Set<Company>()
					on bookmarkDocumentTemplateTable.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on bookmarkDocumentTemplateTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljBookmarkDocumentTemplateTableOwnerBU
					from ownerBU in ljBookmarkDocumentTemplateTableOwnerBU.DefaultIfEmpty()
					select new BookmarkDocumentTemplateTableItemViewMap
					{
						CompanyGUID = bookmarkDocumentTemplateTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = bookmarkDocumentTemplateTable.Owner,
						OwnerBusinessUnitGUID = bookmarkDocumentTemplateTable.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = bookmarkDocumentTemplateTable.CreatedBy,
						CreatedDateTime = bookmarkDocumentTemplateTable.CreatedDateTime,
						ModifiedBy = bookmarkDocumentTemplateTable.ModifiedBy,
						ModifiedDateTime = bookmarkDocumentTemplateTable.ModifiedDateTime,
						BookmarkDocumentTemplateTableGUID = bookmarkDocumentTemplateTable.BookmarkDocumentTemplateTableGUID,
						BookmarkDocumentRefType = bookmarkDocumentTemplateTable.BookmarkDocumentRefType,
						BookmarkDocumentTemplateId = bookmarkDocumentTemplateTable.BookmarkDocumentTemplateId,
						Description = bookmarkDocumentTemplateTable.Description,
					
						RowVersion = bookmarkDocumentTemplateTable.RowVersion,
					});
		}
		public BookmarkDocumentTemplateTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<BookmarkDocumentTemplateTableItemViewMap, BookmarkDocumentTemplateTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public BookmarkDocumentTemplateTable CreateBookmarkDocumentTemplateTable(BookmarkDocumentTemplateTable bookmarkDocumentTemplateTable)
		{
			try
			{
				bookmarkDocumentTemplateTable.BookmarkDocumentTemplateTableGUID = Guid.NewGuid();
				base.Add(bookmarkDocumentTemplateTable);
				return bookmarkDocumentTemplateTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateBookmarkDocumentTemplateTableVoid(BookmarkDocumentTemplateTable bookmarkDocumentTemplateTable)
		{
			try
			{
				CreateBookmarkDocumentTemplateTable(bookmarkDocumentTemplateTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BookmarkDocumentTemplateTable UpdateBookmarkDocumentTemplateTable(BookmarkDocumentTemplateTable dbBookmarkDocumentTemplateTable, BookmarkDocumentTemplateTable inputBookmarkDocumentTemplateTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBookmarkDocumentTemplateTable = dbBookmarkDocumentTemplateTable.MapUpdateValues<BookmarkDocumentTemplateTable>(inputBookmarkDocumentTemplateTable);
				base.Update(dbBookmarkDocumentTemplateTable);
				return dbBookmarkDocumentTemplateTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateBookmarkDocumentTemplateTableVoid(BookmarkDocumentTemplateTable dbBookmarkDocumentTemplateTable, BookmarkDocumentTemplateTable inputBookmarkDocumentTemplateTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBookmarkDocumentTemplateTable = dbBookmarkDocumentTemplateTable.MapUpdateValues<BookmarkDocumentTemplateTable>(inputBookmarkDocumentTemplateTable, skipUpdateFields);
				base.Update(dbBookmarkDocumentTemplateTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public BookmarkDocumentTemplateTable GetBookmarkDocumentTemplateTableByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BookmarkDocumentTemplateTableGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

