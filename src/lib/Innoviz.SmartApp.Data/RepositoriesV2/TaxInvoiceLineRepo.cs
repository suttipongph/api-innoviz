using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ITaxInvoiceLineRepo
	{
		#region DropDown
		IEnumerable<SelectItem<TaxInvoiceLineItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<TaxInvoiceLineListView> GetListvw(SearchParameter search);
		TaxInvoiceLineItemView GetByIdvw(Guid id);
		TaxInvoiceLine Find(params object[] keyValues);
		TaxInvoiceLine GetTaxInvoiceLineByIdNoTracking(Guid guid);
		TaxInvoiceLine CreateTaxInvoiceLine(TaxInvoiceLine taxInvoiceLine);
		void CreateTaxInvoiceLineVoid(TaxInvoiceLine taxInvoiceLine);
		TaxInvoiceLine UpdateTaxInvoiceLine(TaxInvoiceLine dbTaxInvoiceLine, TaxInvoiceLine inputTaxInvoiceLine, List<string> skipUpdateFields = null);
		void UpdateTaxInvoiceLineVoid(TaxInvoiceLine dbTaxInvoiceLine, TaxInvoiceLine inputTaxInvoiceLine, List<string> skipUpdateFields = null);
		void Remove(TaxInvoiceLine item);
		TaxInvoiceLine GetTaxInvoiceLineByIdNoTrackingByAccessLevel(Guid guid);
		#region function
		IEnumerable<TaxInvoiceLine> GetTaxInvoiceLinesByTaxInvoiceTableGuid(Guid guid);
		#endregion
	}
    public class TaxInvoiceLineRepo : BranchCompanyBaseRepository<TaxInvoiceLine>, ITaxInvoiceLineRepo
	{
		public TaxInvoiceLineRepo(SmartAppDbContext context) : base(context) { }
		public TaxInvoiceLineRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public TaxInvoiceLine GetTaxInvoiceLineByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.TaxInvoiceLineGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<TaxInvoiceLineItemViewMap> GetDropDownQuery()
		{
			return (from taxInvoiceLine in Entity
					select new TaxInvoiceLineItemViewMap
					{
						CompanyGUID = taxInvoiceLine.CompanyGUID,
						Owner = taxInvoiceLine.Owner,
						OwnerBusinessUnitGUID = taxInvoiceLine.OwnerBusinessUnitGUID,
						TaxInvoiceLineGUID = taxInvoiceLine.TaxInvoiceLineGUID
					});
		}
		public IEnumerable<SelectItem<TaxInvoiceLineItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<TaxInvoiceLine>(search, db.GetAvailableBranch(), SysParm.CompanyGUID, SysParm.BranchGUID);
				var taxInvoiceLine = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<TaxInvoiceLineItemViewMap, TaxInvoiceLineItemView>().ToDropDownItem(search.ExcludeRowData);


				return taxInvoiceLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<TaxInvoiceLineListViewMap> GetListQuery()
		{
			return (from taxInvoiceLine in Entity
					join taxInvoiceTable in db.Set<TaxInvoiceTable>()
					on taxInvoiceLine.TaxInvoiceTableGUID equals taxInvoiceTable.TaxInvoiceTableGUID
					join invoiceRevenueType in db.Set<InvoiceRevenueType>()
					on taxInvoiceLine.InvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID into ljinvoiceRevenueType
					from invoiceRevenueType in ljinvoiceRevenueType.DefaultIfEmpty()
					join prodUnit in db.Set<ProdUnit>()
					on taxInvoiceLine.ProdUnitGUID equals prodUnit.ProdUnitGUID into ljprodUnit
					from prodUnit in ljprodUnit.DefaultIfEmpty()
					join ledgerDimension1 in db.Set<LedgerDimension>()
					on taxInvoiceLine.Dimension1GUID equals ledgerDimension1.LedgerDimensionGUID into ljledgerDimension1
					from ledgerDimension1 in ljledgerDimension1.DefaultIfEmpty()
					join ledgerDimension2 in db.Set<LedgerDimension>()
					on taxInvoiceLine.Dimension2GUID equals ledgerDimension2.LedgerDimensionGUID into ljledgerDimension2
					from ledgerDimension2 in ljledgerDimension2.DefaultIfEmpty()
					join ledgerDimension3 in db.Set<LedgerDimension>()
					on taxInvoiceLine.Dimension3GUID equals ledgerDimension3.LedgerDimensionGUID into ljledgerDimension3
					from ledgerDimension3 in ljledgerDimension3.DefaultIfEmpty()
					join ledgerDimension4 in db.Set<LedgerDimension>()
					on taxInvoiceLine.Dimension4GUID equals ledgerDimension4.LedgerDimensionGUID into ljledgerDimension4
					from ledgerDimension4 in ljledgerDimension4.DefaultIfEmpty()
					join ledgerDimension5 in db.Set<LedgerDimension>()
					on taxInvoiceLine.Dimension5GUID equals ledgerDimension5.LedgerDimensionGUID into ljledgerDimension5
					from ledgerDimension5 in ljledgerDimension5.DefaultIfEmpty()
					join taxTable in db.Set< TaxTable >()
					on taxInvoiceLine.TaxTableGUID equals taxTable.TaxTableGUID into ljtaxTable
					from taxTable in ljtaxTable.DefaultIfEmpty()

					select new TaxInvoiceLineListViewMap
				{
						CompanyGUID = taxInvoiceLine.CompanyGUID,
						Owner = taxInvoiceLine.Owner,
						OwnerBusinessUnitGUID = taxInvoiceLine.OwnerBusinessUnitGUID,
						TaxInvoiceLineGUID = taxInvoiceLine.TaxInvoiceLineGUID,
						TaxInvoiceTableGUID = taxInvoiceLine.TaxInvoiceTableGUID,
						LineNum = taxInvoiceLine.LineNum,
						UnitPrice = taxInvoiceLine.UnitPrice,
						InvoiceRevenueTypeGUID = taxInvoiceLine.InvoiceRevenueTypeGUID,
						TotalAmountBeforeTax = taxInvoiceLine.TotalAmountBeforeTax,
						TaxAmount = taxInvoiceLine.TaxAmount,
						TotalAmount = taxInvoiceLine.TotalAmount,
						ProdUnitGUID = (Guid)taxInvoiceLine.ProdUnitGUID,
						TaxTableGUID = taxInvoiceLine.TaxTableGUID,
						TaxTable_Values = SmartAppUtil.GetDropDownLabel(taxTable.TaxCode,taxTable.Description),
						TaxInvoiceTable_Values = SmartAppUtil.GetDropDownLabel(taxInvoiceTable.TaxInvoiceId, taxInvoiceTable.IssuedDate.DateToString()),
						InvoiceRevenueType_Values = SmartAppUtil.GetDropDownLabel(invoiceRevenueType.RevenueTypeId, invoiceRevenueType.Description),
						ProdUnit_Values = SmartAppUtil.GetDropDownLabel(prodUnit.UnitId, prodUnit.Description),
						Dimension1GUID = (Guid)taxInvoiceLine.Dimension1GUID,
						Dimension2GUID = (Guid)taxInvoiceLine.Dimension2GUID,
						Dimension3GUID = (Guid)taxInvoiceLine.Dimension3GUID,
						Dimension4GUID = (Guid)taxInvoiceLine.Dimension4GUID,
						Dimension5GUID = (Guid)taxInvoiceLine.Dimension5GUID,
						Dimension1_Values = SmartAppUtil.GetDropDownLabel(ledgerDimension1.DimensionCode, ledgerDimension1.Description),
						Dimension2_Values = SmartAppUtil.GetDropDownLabel(ledgerDimension2.DimensionCode, ledgerDimension2.Description),
						Dimension3_Values = SmartAppUtil.GetDropDownLabel(ledgerDimension3.DimensionCode, ledgerDimension3.Description),
						Dimension4_Values = SmartAppUtil.GetDropDownLabel(ledgerDimension4.DimensionCode, ledgerDimension4.Description),
						Dimension5_Values = SmartAppUtil.GetDropDownLabel(ledgerDimension5.DimensionCode, ledgerDimension5.Description),
						InvoiceRevenueType_InvoiceRevenueTypeId = invoiceRevenueType.RevenueTypeId,
						TaxTable_TaxId = taxTable.TaxCode
					});
		}
		public SearchResult<TaxInvoiceLineListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<TaxInvoiceLineListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<TaxInvoiceLine>(search, db.GetAvailableBranch(), SysParm.CompanyGUID, SysParm.BranchGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<TaxInvoiceLineListViewMap, TaxInvoiceLineListView>();
				result = list.SetSearchResult<TaxInvoiceLineListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<TaxInvoiceLineItemViewMap> GetItemQuery()
		{
			return (from taxInvoiceLine in Entity
					join company in db.Set<Company>()
					on taxInvoiceLine.CompanyGUID equals company.CompanyGUID
					join branch in db.Set<Branch>()
					on taxInvoiceLine.BranchGUID equals branch.BranchGUID
					join ownerBU in db.Set<BusinessUnit>()
					on taxInvoiceLine.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljTaxInvoiceLineOwnerBU
					from ownerBU in ljTaxInvoiceLineOwnerBU.DefaultIfEmpty()
					join taxInvoiceTable in db.Set<TaxInvoiceTable>()
					on taxInvoiceLine.TaxInvoiceTableGUID equals taxInvoiceTable.TaxInvoiceTableGUID
					join invoiceRevenueType in db.Set<InvoiceRevenueType>()
					on taxInvoiceLine.InvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID into ljinvoiceRevenueType
					from invoiceRevenueType in ljinvoiceRevenueType.DefaultIfEmpty()
					join prodUnit in db.Set<ProdUnit>()
					on taxInvoiceLine.ProdUnitGUID equals prodUnit.ProdUnitGUID into ljprodUnit
					from prodUnit in ljprodUnit.DefaultIfEmpty()
					join ledgerDimension1 in db.Set<LedgerDimension>()
					on taxInvoiceLine.Dimension1GUID equals ledgerDimension1.LedgerDimensionGUID into ljledgerDimension1
					from ledgerDimension1 in ljledgerDimension1.DefaultIfEmpty()
					join ledgerDimension2 in db.Set<LedgerDimension>()
					on taxInvoiceLine.Dimension2GUID equals ledgerDimension2.LedgerDimensionGUID into ljledgerDimension2
					from ledgerDimension2 in ljledgerDimension2.DefaultIfEmpty()
					join ledgerDimension3 in db.Set<LedgerDimension>()
					on taxInvoiceLine.Dimension3GUID equals ledgerDimension3.LedgerDimensionGUID into ljledgerDimension3
					from ledgerDimension3 in ljledgerDimension3.DefaultIfEmpty()
					join ledgerDimension4 in db.Set<LedgerDimension>()
					on taxInvoiceLine.Dimension4GUID equals ledgerDimension4.LedgerDimensionGUID into ljledgerDimension4
					from ledgerDimension4 in ljledgerDimension4.DefaultIfEmpty()
					join ledgerDimension5 in db.Set<LedgerDimension>()
					on taxInvoiceLine.Dimension5GUID equals ledgerDimension5.LedgerDimensionGUID into ljledgerDimension5
					from ledgerDimension5 in ljledgerDimension5.DefaultIfEmpty()
					join taxTable in db.Set<TaxTable>()
					on taxInvoiceLine.TaxTableGUID equals taxTable.TaxTableGUID into ljtaxTable
					from taxTable in ljtaxTable.DefaultIfEmpty()

					select new TaxInvoiceLineItemViewMap
					{
						CompanyGUID = taxInvoiceLine.CompanyGUID,
						CompanyId = company.CompanyId,
						BranchGUID = taxInvoiceLine.BranchGUID,
						BranchId = branch.BranchId,
						Owner = taxInvoiceLine.Owner,
						OwnerBusinessUnitGUID = taxInvoiceLine.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = taxInvoiceLine.CreatedBy,
						CreatedDateTime = taxInvoiceLine.CreatedDateTime,
						ModifiedBy = taxInvoiceLine.ModifiedBy,
						ModifiedDateTime = taxInvoiceLine.ModifiedDateTime,
						TaxInvoiceLineGUID = taxInvoiceLine.TaxInvoiceLineGUID,
						InvoiceRevenueTypeGUID = taxInvoiceLine.InvoiceRevenueTypeGUID,
						InvoiceText = taxInvoiceLine.InvoiceText,
						LineNum = taxInvoiceLine.LineNum,
						Qty = taxInvoiceLine.Qty,
						TaxAmount = taxInvoiceLine.TaxAmount,
						TaxInvoiceTableGUID = taxInvoiceLine.TaxInvoiceTableGUID,
						TaxTableGUID = taxInvoiceLine.TaxTableGUID,
						TotalAmount = taxInvoiceLine.TotalAmount,
						TotalAmountBeforeTax = taxInvoiceLine.TotalAmountBeforeTax,
						UnitPrice = taxInvoiceLine.UnitPrice,
						ProdUnitGUID = (Guid)taxInvoiceLine.ProdUnitGUID,
						TaxTable_Values = SmartAppUtil.GetDropDownLabel(taxTable.TaxCode, taxTable.Description),
						TaxInvoiceTable_Values = SmartAppUtil.GetDropDownLabel(taxInvoiceTable.TaxInvoiceId, taxInvoiceTable.IssuedDate.DateToString()),
						InvoiceRevenueType_Values = SmartAppUtil.GetDropDownLabel(invoiceRevenueType.RevenueTypeId, invoiceRevenueType.Description),
						ProdUnit_Values = SmartAppUtil.GetDropDownLabel(prodUnit.UnitId, prodUnit.Description),
						Dimension1GUID = (Guid)taxInvoiceLine.Dimension1GUID,
						Dimension2GUID = (Guid)taxInvoiceLine.Dimension2GUID,
						Dimension3GUID = (Guid)taxInvoiceLine.Dimension3GUID,
						Dimension4GUID = (Guid)taxInvoiceLine.Dimension4GUID,
						Dimension5GUID = (Guid)taxInvoiceLine.Dimension5GUID,
						Dimension1_Values = SmartAppUtil.GetDropDownLabel(ledgerDimension1.DimensionCode, ledgerDimension1.Description),
						Dimension2_Values = SmartAppUtil.GetDropDownLabel(ledgerDimension2.DimensionCode, ledgerDimension2.Description),
						Dimension3_Values = SmartAppUtil.GetDropDownLabel(ledgerDimension3.DimensionCode, ledgerDimension3.Description),
						Dimension4_Values = SmartAppUtil.GetDropDownLabel(ledgerDimension4.DimensionCode, ledgerDimension4.Description),
						Dimension5_Values = SmartAppUtil.GetDropDownLabel(ledgerDimension5.DimensionCode, ledgerDimension5.Description),

					
						RowVersion = taxInvoiceLine.RowVersion,
					});
		}
		public TaxInvoiceLineItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<TaxInvoiceLineItemViewMap, TaxInvoiceLineItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public TaxInvoiceLine CreateTaxInvoiceLine(TaxInvoiceLine taxInvoiceLine)
		{
			try
			{
				taxInvoiceLine.TaxInvoiceLineGUID = Guid.NewGuid();
				base.Add(taxInvoiceLine);
				return taxInvoiceLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateTaxInvoiceLineVoid(TaxInvoiceLine taxInvoiceLine)
		{
			try
			{
				CreateTaxInvoiceLine(taxInvoiceLine);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public TaxInvoiceLine UpdateTaxInvoiceLine(TaxInvoiceLine dbTaxInvoiceLine, TaxInvoiceLine inputTaxInvoiceLine, List<string> skipUpdateFields = null)
		{
			try
			{
				dbTaxInvoiceLine = dbTaxInvoiceLine.MapUpdateValues<TaxInvoiceLine>(inputTaxInvoiceLine);
				base.Update(dbTaxInvoiceLine);
				return dbTaxInvoiceLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateTaxInvoiceLineVoid(TaxInvoiceLine dbTaxInvoiceLine, TaxInvoiceLine inputTaxInvoiceLine, List<string> skipUpdateFields = null)
		{
			try
			{
				dbTaxInvoiceLine = dbTaxInvoiceLine.MapUpdateValues<TaxInvoiceLine>(inputTaxInvoiceLine, skipUpdateFields);
				base.Update(dbTaxInvoiceLine);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public TaxInvoiceLine GetTaxInvoiceLineByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.TaxInvoiceLineGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        #region function
		public IEnumerable<TaxInvoiceLine> GetTaxInvoiceLinesByTaxInvoiceTableGuid(Guid guid)
        {
            try
            {
				IEnumerable<TaxInvoiceLine> taxInvoiceLines = Entity.Where(w => w.TaxInvoiceTableGUID == guid)
					.AsNoTracking().ToList();
				return taxInvoiceLines;
			}
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		#endregion
	}
}

