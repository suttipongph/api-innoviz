using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IVendGroupRepo
	{
		#region DropDown
		IEnumerable<SelectItem<VendGroupItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<VendGroupListView> GetListvw(SearchParameter search);
		VendGroupItemView GetByIdvw(Guid id);
		VendGroup Find(params object[] keyValues);
		VendGroup GetVendGroupByIdNoTracking(Guid guid);
		VendGroup CreateVendGroup(VendGroup vendGroup);
		void CreateVendGroupVoid(VendGroup vendGroup);
		VendGroup UpdateVendGroup(VendGroup dbVendGroup, VendGroup inputVendGroup, List<string> skipUpdateFields = null);
		void UpdateVendGroupVoid(VendGroup dbVendGroup, VendGroup inputVendGroup, List<string> skipUpdateFields = null);
		void Remove(VendGroup item);
		List<VendGroup> GetVendGroupByCompanyNoTracking(Guid companyGUID);
	}
	public class VendGroupRepo : CompanyBaseRepository<VendGroup>, IVendGroupRepo
	{
		public VendGroupRepo(SmartAppDbContext context) : base(context) { }
		public VendGroupRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public VendGroup GetVendGroupByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.VendGroupGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<VendGroupItemViewMap> GetDropDownQuery()
		{
			return (from vendGroup in Entity
					select new VendGroupItemViewMap
					{
						CompanyGUID = vendGroup.CompanyGUID,
						Owner = vendGroup.Owner,
						OwnerBusinessUnitGUID = vendGroup.OwnerBusinessUnitGUID,
						VendGroupGUID = vendGroup.VendGroupGUID,
						VendGroupId = vendGroup.VendGroupId,
						Description = vendGroup.Description
					});
		}
		public IEnumerable<SelectItem<VendGroupItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<VendGroup>(search, SysParm.CompanyGUID);
				var vendGroup = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<VendGroupItemViewMap, VendGroupItemView>().ToDropDownItem(search.ExcludeRowData);


				return vendGroup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<VendGroupListViewMap> GetListQuery()
		{
			return (from vendGroup in Entity
				select new VendGroupListViewMap
				{
						CompanyGUID = vendGroup.CompanyGUID,
						Owner = vendGroup.Owner,
						OwnerBusinessUnitGUID = vendGroup.OwnerBusinessUnitGUID,
						VendGroupGUID = vendGroup.VendGroupGUID,
						VendGroupId = vendGroup.VendGroupId,
						Description = vendGroup.Description
				});
		}
		public SearchResult<VendGroupListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<VendGroupListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<VendGroup>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<VendGroupListViewMap, VendGroupListView>();
				result = list.SetSearchResult<VendGroupListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<VendGroupItemViewMap> GetItemQuery()
		{
			return (from vendGroup in Entity
					join company in db.Set<Company>()
					on vendGroup.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on vendGroup.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljVendGroupOwnerBU
					from ownerBU in ljVendGroupOwnerBU.DefaultIfEmpty()
					select new VendGroupItemViewMap
					{
						CompanyGUID = vendGroup.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = vendGroup.Owner,
						OwnerBusinessUnitGUID = vendGroup.OwnerBusinessUnitGUID,
						CreatedBy = vendGroup.CreatedBy,
						CreatedDateTime = vendGroup.CreatedDateTime,
						ModifiedBy = vendGroup.ModifiedBy,
						ModifiedDateTime = vendGroup.ModifiedDateTime,
						VendGroupGUID = vendGroup.VendGroupGUID,
						Description = vendGroup.Description,
						VendGroupId = vendGroup.VendGroupId,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
					
						RowVersion = vendGroup.RowVersion,
					});
		}
		public VendGroupItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<VendGroupItemViewMap, VendGroupItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public VendGroup CreateVendGroup(VendGroup vendGroup)
		{
			try
			{
				vendGroup.VendGroupGUID = Guid.NewGuid();
				base.Add(vendGroup);
				return vendGroup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateVendGroupVoid(VendGroup vendGroup)
		{
			try
			{
				CreateVendGroup(vendGroup);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public VendGroup UpdateVendGroup(VendGroup dbVendGroup, VendGroup inputVendGroup, List<string> skipUpdateFields = null)
		{
			try
			{
				dbVendGroup = dbVendGroup.MapUpdateValues<VendGroup>(inputVendGroup);
				base.Update(dbVendGroup);
				return dbVendGroup;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateVendGroupVoid(VendGroup dbVendGroup, VendGroup inputVendGroup, List<string> skipUpdateFields = null)
		{
			try
			{
				dbVendGroup = dbVendGroup.MapUpdateValues<VendGroup>(inputVendGroup, skipUpdateFields);
				base.Update(dbVendGroup);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public List<VendGroup> GetVendGroupByCompanyNoTracking(Guid companyGUID)
		{
			try
			{
				List<VendGroup> vendGroups = Entity.Where(w => w.CompanyGUID == companyGUID).AsNoTracking().ToList();
				return vendGroups;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

