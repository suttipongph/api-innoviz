using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IAssignmentMethodRepo
	{
		#region DropDown
		IEnumerable<SelectItem<AssignmentMethodItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<AssignmentMethodListView> GetListvw(SearchParameter search);
		AssignmentMethodItemView GetByIdvw(Guid id);
		AssignmentMethod Find(params object[] keyValues);
		AssignmentMethod GetAssignmentMethodByIdNoTracking(Guid guid);
		AssignmentMethod CreateAssignmentMethod(AssignmentMethod assignmentMethod);
		void CreateAssignmentMethodVoid(AssignmentMethod assignmentMethod);
		AssignmentMethod UpdateAssignmentMethod(AssignmentMethod dbAssignmentMethod, AssignmentMethod inputAssignmentMethod, List<string> skipUpdateFields = null);
		void UpdateAssignmentMethodVoid(AssignmentMethod dbAssignmentMethod, AssignmentMethod inputAssignmentMethod, List<string> skipUpdateFields = null);
		void Remove(AssignmentMethod item);
	}
	public class AssignmentMethodRepo : CompanyBaseRepository<AssignmentMethod>, IAssignmentMethodRepo
	{
		public AssignmentMethodRepo(SmartAppDbContext context) : base(context) { }
		public AssignmentMethodRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public AssignmentMethod GetAssignmentMethodByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.AssignmentMethodGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<AssignmentMethodItemViewMap> GetDropDownQuery()
		{
			return (from assignmentMethod in Entity
					select new AssignmentMethodItemViewMap
					{
						CompanyGUID = assignmentMethod.CompanyGUID,
						Owner = assignmentMethod.Owner,
						OwnerBusinessUnitGUID = assignmentMethod.OwnerBusinessUnitGUID,
						AssignmentMethodGUID = assignmentMethod.AssignmentMethodGUID,
						AssignmentMethodId = assignmentMethod.AssignmentMethodId,
						Description = assignmentMethod.Description,
					});
		}
		public IEnumerable<SelectItem<AssignmentMethodItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<AssignmentMethod>(search, SysParm.CompanyGUID);
				var assignmentMethod = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<AssignmentMethodItemViewMap, AssignmentMethodItemView>().ToDropDownItem(search.ExcludeRowData);


				return assignmentMethod;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<AssignmentMethodListViewMap> GetListQuery()
		{
			return (from assignmentMethod in Entity
					select new AssignmentMethodListViewMap
					{
						CompanyGUID = assignmentMethod.CompanyGUID,
						Owner = assignmentMethod.Owner,
						OwnerBusinessUnitGUID = assignmentMethod.OwnerBusinessUnitGUID,
						AssignmentMethodGUID = assignmentMethod.AssignmentMethodGUID,
						AssignmentMethodId = assignmentMethod.AssignmentMethodId,
						Description = assignmentMethod.Description,
						ValidateAssignmentBalance = assignmentMethod.ValidateAssignmentBalance,
					});
		}
		public SearchResult<AssignmentMethodListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<AssignmentMethodListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<AssignmentMethod>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<AssignmentMethodListViewMap, AssignmentMethodListView>();
				result = list.SetSearchResult<AssignmentMethodListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<AssignmentMethodItemViewMap> GetItemQuery()
		{
			return (from assignmentMethod in Entity
					join company in db.Set<Company>()
					on assignmentMethod.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on assignmentMethod.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljAssignmentMethodOwnerBU
					from ownerBU in ljAssignmentMethodOwnerBU.DefaultIfEmpty()
					select new AssignmentMethodItemViewMap
					{
						CompanyGUID = assignmentMethod.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = assignmentMethod.Owner,
						OwnerBusinessUnitGUID = assignmentMethod.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = assignmentMethod.CreatedBy,
						CreatedDateTime = assignmentMethod.CreatedDateTime,
						ModifiedBy = assignmentMethod.ModifiedBy,
						ModifiedDateTime = assignmentMethod.ModifiedDateTime,
						AssignmentMethodGUID = assignmentMethod.AssignmentMethodGUID,
						AssignmentMethodId = assignmentMethod.AssignmentMethodId,
						Description = assignmentMethod.Description,
						ValidateAssignmentBalance = assignmentMethod.ValidateAssignmentBalance,

						RowVersion = assignmentMethod.RowVersion,
					});
		}
		public AssignmentMethodItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<AssignmentMethodItemViewMap, AssignmentMethodItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public AssignmentMethod CreateAssignmentMethod(AssignmentMethod assignmentMethod)
		{
			try
			{
				assignmentMethod.AssignmentMethodGUID = Guid.NewGuid();
				base.Add(assignmentMethod);
				return assignmentMethod;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateAssignmentMethodVoid(AssignmentMethod assignmentMethod)
		{
			try
			{
				CreateAssignmentMethod(assignmentMethod);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AssignmentMethod UpdateAssignmentMethod(AssignmentMethod dbAssignmentMethod, AssignmentMethod inputAssignmentMethod, List<string> skipUpdateFields = null)
		{
			try
			{
				dbAssignmentMethod = dbAssignmentMethod.MapUpdateValues<AssignmentMethod>(inputAssignmentMethod);
				base.Update(dbAssignmentMethod);
				return dbAssignmentMethod;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateAssignmentMethodVoid(AssignmentMethod dbAssignmentMethod, AssignmentMethod inputAssignmentMethod, List<string> skipUpdateFields = null)
		{
			try
			{
				dbAssignmentMethod = dbAssignmentMethod.MapUpdateValues<AssignmentMethod>(inputAssignmentMethod, skipUpdateFields);
				base.Update(dbAssignmentMethod);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

