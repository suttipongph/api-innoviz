using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IFinancialCreditTransRepo
	{
		#region DropDown
		IEnumerable<SelectItem<FinancialCreditTransItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<FinancialCreditTransListView> GetListvw(SearchParameter search);
		FinancialCreditTransItemView GetByIdvw(Guid id);
		FinancialCreditTrans Find(params object[] keyValues);
		FinancialCreditTrans GetFinancialCreditTransByIdNoTracking(Guid guid);
		FinancialCreditTrans CreateFinancialCreditTrans(FinancialCreditTrans financialCreditTrans);
		void CreateFinancialCreditTransVoid(FinancialCreditTrans financialCreditTrans);
		FinancialCreditTrans UpdateFinancialCreditTrans(FinancialCreditTrans dbFinancialCreditTrans, FinancialCreditTrans inputFinancialCreditTrans, List<string> skipUpdateFields = null);
		void UpdateFinancialCreditTransVoid(FinancialCreditTrans dbFinancialCreditTrans, FinancialCreditTrans inputFinancialCreditTrans, List<string> skipUpdateFields = null);
		void Remove(FinancialCreditTrans item);
		FinancialCreditTrans GetFinancialCreditTransByIdNoTrackingByAccessLevel(Guid guid);
		void ValidateAdd(FinancialCreditTrans item);
		void ValidateAdd(IEnumerable<FinancialCreditTrans> items);
	}
	public class FinancialCreditTransRepo : CompanyBaseRepository<FinancialCreditTrans>, IFinancialCreditTransRepo
	{
		public FinancialCreditTransRepo(SmartAppDbContext context) : base(context) { }
		public FinancialCreditTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public FinancialCreditTrans GetFinancialCreditTransByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.FinancialCreditTransGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<FinancialCreditTransItemViewMap> GetDropDownQuery()
		{
			return (from financialCreditTrans in Entity
					select new FinancialCreditTransItemViewMap
					{
						CompanyGUID = financialCreditTrans.CompanyGUID,
						Owner = financialCreditTrans.Owner,
						OwnerBusinessUnitGUID = financialCreditTrans.OwnerBusinessUnitGUID,
						FinancialCreditTransGUID = financialCreditTrans.FinancialCreditTransGUID,
						BankGroupGUID = financialCreditTrans.BankGroupGUID,
						CreditTypeGUID = financialCreditTrans.CreditTypeGUID
					});
		}
		public IEnumerable<SelectItem<FinancialCreditTransItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<FinancialCreditTrans>(search, SysParm.CompanyGUID);
				var financialCreditTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<FinancialCreditTransItemViewMap, FinancialCreditTransItemView>().ToDropDownItem(search.ExcludeRowData);


				return financialCreditTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<FinancialCreditTransListViewMap> GetListQuery()
		{
			return (from financialCreditTrans in Entity
					join bankGroup in db.Set<BankGroup>()
					on financialCreditTrans.BankGroupGUID equals bankGroup.BankGroupGUID
					join creditType in db.Set<CreditType>()
					on financialCreditTrans.CreditTypeGUID equals creditType.CreditTypeGUID
					select new FinancialCreditTransListViewMap
				{
						CompanyGUID = financialCreditTrans.CompanyGUID,
						Owner = financialCreditTrans.Owner,
						OwnerBusinessUnitGUID = financialCreditTrans.OwnerBusinessUnitGUID,
						FinancialCreditTransGUID = financialCreditTrans.FinancialCreditTransGUID,
						BankGroupGUID = financialCreditTrans.BankGroupGUID,
						CreditTypeGUID = financialCreditTrans.CreditTypeGUID,
						Amount = financialCreditTrans.Amount,
						BankGroup_BankGroupId = bankGroup.BankGroupId,
						CreditType_CreditTypeId = creditType.CreditTypeId,
						BankGroup_Values = SmartAppUtil.GetDropDownLabel(bankGroup.BankGroupId, bankGroup.Description),
						CreditType_Values = SmartAppUtil.GetDropDownLabel(creditType.CreditTypeId, creditType.Description),
						RefGUID = financialCreditTrans.RefGUID,
				});
		}
		public SearchResult<FinancialCreditTransListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<FinancialCreditTransListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<FinancialCreditTrans>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<FinancialCreditTransListViewMap, FinancialCreditTransListView>();
				result = list.SetSearchResult<FinancialCreditTransListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<FinancialCreditTransItemViewMap> GetItemQuery()
		{
			return (from financialCreditTrans in Entity
					join company in db.Set<Company>()
					on financialCreditTrans.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on financialCreditTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljFinancialCreditTransOwnerBU
					from ownerBU in ljFinancialCreditTransOwnerBU.DefaultIfEmpty()
					join creditAppRequestTable in db.Set<CreditAppRequestTable>()
					on financialCreditTrans.RefGUID equals creditAppRequestTable.CreditAppRequestTableGUID into ljCreditAppRequestTable
					from creditAppRequestTable in ljCreditAppRequestTable.DefaultIfEmpty()
					select new FinancialCreditTransItemViewMap
					{
						CompanyGUID = financialCreditTrans.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = financialCreditTrans.Owner,
						OwnerBusinessUnitGUID = financialCreditTrans.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = financialCreditTrans.CreatedBy,
						CreatedDateTime = financialCreditTrans.CreatedDateTime,
						ModifiedBy = financialCreditTrans.ModifiedBy,
						ModifiedDateTime = financialCreditTrans.ModifiedDateTime,
						FinancialCreditTransGUID = financialCreditTrans.FinancialCreditTransGUID,
						Amount = financialCreditTrans.Amount,
						BankGroupGUID = financialCreditTrans.BankGroupGUID,
						CreditTypeGUID = financialCreditTrans.CreditTypeGUID,
						RefGUID = financialCreditTrans.RefGUID,
						RefType = financialCreditTrans.RefType,
						RefId = (creditAppRequestTable != null && financialCreditTrans.RefType == (int)RefType.CreditAppRequestTable) ? creditAppRequestTable.CreditAppRequestId : null,
					
						RowVersion = financialCreditTrans.RowVersion,
					});
		}
		public FinancialCreditTransItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<FinancialCreditTransItemViewMap, FinancialCreditTransItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public FinancialCreditTrans CreateFinancialCreditTrans(FinancialCreditTrans financialCreditTrans)
		{
			try
			{
				financialCreditTrans.FinancialCreditTransGUID = Guid.NewGuid();
				base.Add(financialCreditTrans);
				return financialCreditTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateFinancialCreditTransVoid(FinancialCreditTrans financialCreditTrans)
		{
			try
			{
				CreateFinancialCreditTrans(financialCreditTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public FinancialCreditTrans UpdateFinancialCreditTrans(FinancialCreditTrans dbFinancialCreditTrans, FinancialCreditTrans inputFinancialCreditTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbFinancialCreditTrans = dbFinancialCreditTrans.MapUpdateValues<FinancialCreditTrans>(inputFinancialCreditTrans);
				base.Update(dbFinancialCreditTrans);
				return dbFinancialCreditTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateFinancialCreditTransVoid(FinancialCreditTrans dbFinancialCreditTrans, FinancialCreditTrans inputFinancialCreditTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbFinancialCreditTrans = dbFinancialCreditTrans.MapUpdateValues<FinancialCreditTrans>(inputFinancialCreditTrans, skipUpdateFields);
				base.Update(dbFinancialCreditTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public FinancialCreditTrans GetFinancialCreditTransByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.FinancialCreditTransGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public override void ValidateAdd(IEnumerable<FinancialCreditTrans> items)
		{
			base.ValidateAdd(items);
		}
		public override void ValidateAdd(FinancialCreditTrans item)
		{
			base.ValidateAdd(item);
		}
	}
}

