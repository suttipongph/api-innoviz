using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Microsoft.AspNetCore.Identity;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface IMainAgreementTableRepo
    {
        #region DropDown
        IEnumerable<SelectItem<MainAgreementTableItemView>> GetDropDownItem(SearchParameter search);
        IEnumerable<SelectItem<MainAgreementTableItemView>> GetDropDownItemByGuarantor(SearchParameter search);
        #endregion DropDown
        SearchResult<MainAgreementTableListView> GetListvw(SearchParameter search);
        MainAgreementTableItemView GetByIdvw(Guid id);
        MainAgreementTable Find(params object[] keyValues);
        MainAgreementTable GetMainAgreementTableByIdNoTracking(Guid guid);
        MainAgreementTable GetMainAgreementTableByMainAgreementId(string id);
        MainAgreementTable CreateMainAgreementTable(MainAgreementTable mainAgreementTable);
        void CreateMainAgreementTableVoid(MainAgreementTable mainAgreementTable);
        MainAgreementTable UpdateMainAgreementTable(MainAgreementTable dbMainAgreementTable, MainAgreementTable inputMainAgreementTable, List<string> skipUpdateFields = null);
        void UpdateMainAgreementTableVoid(MainAgreementTable dbMainAgreementTable, MainAgreementTable inputMainAgreementTable, List<string> skipUpdateFields = null);
        void Remove(MainAgreementTable item);
        MainAgreementTable GetMainAgreementTableByIdNoTrackingByAccessLevel(Guid guid);
        public AgreementTableInfo GenerateAgreementInfo(MainAgreementTable mainAgreementTableGUID);
        public GenMainAgmAddendumView GetGenMainAgmAddendumByIdvw(Guid guid);
        public GenMainAgmLoanRequestView GetGenMainAgmLoanRequestByIdvw(Guid guid);
        List<MainAgreementTable> GetListByRefMainAgreementTableAndAgreementDocType(Guid guid, AgreementDocType agreementDocType);
        GenMainAgmNoticeOfCancelView GetGenMainAgmNoticeOfCancelByIdvw(Guid guid);
        List<MainAgreementTable> GetMainAgreementTableByCreditAppAndMainAgmTableStatus(Guid creditAppTableGUID, MainAgreementStatus status);
        GenMainAgreementBookmarkSharedView MainAgreementBookmarkSharedView(Guid refGUID);
        MainAgreementPFBookmarkViewMap GetMainAgreementPFBookmarkValue(Guid refGuid);
        GenMainAgreementExpenceDetailView MainAgreementExpenceDetailView(Guid refGUID);
        GenMainAgreementBookmarkSharedJVView MainAgreementBookmarkSharedJVView(Guid refGUID);
        GenMainAgreementConsortiumBookmarkView MainAgreementConsortiumBookmarkView(Guid refGUID);
        AddendumMainAgreementBookmarkView GetAddendumMainAgreementBookmarkView(Guid refGuid);
        void ValidateAdd(IEnumerable<MainAgreementTable> items);
        void ValidateUpdate(IEnumerable<MainAgreementTable> items);
        bool IsDuplicateMainAgreementId(Guid guid, string id);
    }
    public class MainAgreementTableRepo : CompanyBaseRepository<MainAgreementTable>, IMainAgreementTableRepo
    {
        public MainAgreementTableRepo(SmartAppDbContext context) : base(context) { }
        public MainAgreementTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public MainAgreementTable GetMainAgreementTableByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.MainAgreementTableGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public MainAgreementTable GetMainAgreementTableByMainAgreementId(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return null;
                }
                else
                {
                    var result = Entity.Where(item => item.MainAgreementId == id && item.CompanyGUID == db.GetCompanyFilter().StringToGuid()).AsNoTracking().FirstOrDefault();
                    return result;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        private IQueryable<MainAgreementTableItemViewMap> GetDropDownQuery()
        {
            return (from mainAgreementTable in Entity
                    select new MainAgreementTableItemViewMap
                    {
                        CompanyGUID = mainAgreementTable.CompanyGUID,
                        Owner = mainAgreementTable.Owner,
                        OwnerBusinessUnitGUID = mainAgreementTable.OwnerBusinessUnitGUID,
                        MainAgreementTableGUID = mainAgreementTable.MainAgreementTableGUID,
                        InternalMainAgreementId = mainAgreementTable.InternalMainAgreementId,
                        Description = mainAgreementTable.Description
                    });
        }
        public IEnumerable<SelectItem<MainAgreementTableItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<MainAgreementTable>(search, SysParm.CompanyGUID);
                var mainAgreementTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
                    .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                    .Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
                    .ToMaps<MainAgreementTableItemViewMap, MainAgreementTableItemView>().ToDropDownItem(search.ExcludeRowData);
                return mainAgreementTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private IQueryable<MainAgreementTableItemViewMap> GetDropDownQueryByGuarantor()
        {
            var result = (from mainAgreementTable in Entity
                          select new MainAgreementTableItemViewMap
                          {
                              CompanyGUID = mainAgreementTable.CompanyGUID,
                              Owner = mainAgreementTable.Owner,
                              OwnerBusinessUnitGUID = mainAgreementTable.OwnerBusinessUnitGUID,
                              MainAgreementTableGUID = mainAgreementTable.MainAgreementTableGUID,
                              InternalMainAgreementId = mainAgreementTable.InternalMainAgreementId,
                              Description = mainAgreementTable.Description,
                              CustomerTableGUID = mainAgreementTable.CustomerTableGUID,
                              ProductType = mainAgreementTable.ProductType,
                              AgreementDocType = mainAgreementTable.AgreementDocType
                          });
            return result;
        }
        public IEnumerable<SelectItem<MainAgreementTableItemView>> GetDropDownItemByGuarantor(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<MainAgreementTable>(search, SysParm.CompanyGUID);
                var mainAgreementTable = GetDropDownQueryByGuarantor().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<MainAgreementTableItemViewMap, MainAgreementTableItemView>().ToDropDownItem(search.ExcludeRowData);


                return mainAgreementTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #region GetListvw
        private IQueryable<MainAgreementTableListViewMap> GetListQuery()
        {
            return (from mainAgreementTable in Entity
                    join creditAppTable in db.Set<CreditAppTable>()
                    on mainAgreementTable.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID
                    join customerTable in db.Set<CustomerTable>()
                    on mainAgreementTable.CustomerTableGUID equals customerTable.CustomerTableGUID
                    join documentStatus in db.Set<DocumentStatus>()
                    on mainAgreementTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
                    join creditLimitType in db.Set<CreditLimitType>()
                    on mainAgreementTable.CreditLimitTypeGUID equals creditLimitType.CreditLimitTypeGUID into ljCreditLimitType
                    from creditLimitType in ljCreditLimitType.DefaultIfEmpty()
                    join buyerTable in db.Set<BuyerTable>()
                    on mainAgreementTable.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljBuyerTable
                    from buyerTable in ljBuyerTable.DefaultIfEmpty()
                    select new MainAgreementTableListViewMap
                    {
                        CompanyGUID = mainAgreementTable.CompanyGUID,
                        Owner = mainAgreementTable.Owner,
                        OwnerBusinessUnitGUID = mainAgreementTable.OwnerBusinessUnitGUID,
                        MainAgreementTableGUID = mainAgreementTable.MainAgreementTableGUID,
                        InternalMainAgreementId = mainAgreementTable.InternalMainAgreementId,
                        DocumentStatusGUID = mainAgreementTable.DocumentStatusGUID,
                        MainAgreementId = mainAgreementTable.MainAgreementId,
                        AgreementDate = mainAgreementTable.AgreementDate,
                        CreditAppTableGUID = mainAgreementTable.CreditAppTableGUID,
                        CustomerTableGUID = mainAgreementTable.CustomerTableGUID,
                        BuyerTableGUID = mainAgreementTable.BuyerTableGUID,
                        ProductType = mainAgreementTable.ProductType,
                        AgreementDocType = mainAgreementTable.AgreementDocType,
                        CreditLimitTypeGUID = mainAgreementTable.CreditLimitTypeGUID,
                        RefMainAgreementTableGUID = mainAgreementTable.RefMainAgreementTableGUID,
                        CreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
                        CreditAppTable_CreditAppId = creditAppTable.CreditAppId,
                        CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                        CustomerTable_CustomerId = customerTable.CustomerId,
                        DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
                        CreditLimitType_Values = (creditLimitType != null) ? SmartAppUtil.GetDropDownLabel(creditLimitType.CreditLimitTypeId, creditLimitType.Description) : null,
                        CreditLimitType_CreditLimitTypeId = (creditLimitType != null) ? creditLimitType.CreditLimitTypeId : null,
                        BuyerTable_Values = (buyerTable != null) ? SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName) : null,
                        BuyerTable_BuyerId = (buyerTable != null) ? buyerTable.BuyerId : null,
                        DocumentStatus_StatusId = documentStatus.StatusId,
                        CreditLimitType_Revolving = (creditLimitType != null) ? creditLimitType.Revolving : false,
                        Description = mainAgreementTable.Description,
                    });
        }
        public SearchResult<MainAgreementTableListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<MainAgreementTableListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<MainAgreementTable>(search, SysParm.CompanyGUID);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<MainAgreementTableListViewMap, MainAgreementTableListView>();
                result = list.SetSearchResult<MainAgreementTableListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<MainAgreementTableItemViewMap> GetItemQuery()
        {
            return (from mainAgreementTable in Entity
                    join company in db.Set<Company>()
                    on mainAgreementTable.CompanyGUID equals company.CompanyGUID
                    join customerTable in db.Set<CustomerTable>()
                    on mainAgreementTable.CustomerTableGUID equals customerTable.CustomerTableGUID
                    join documentStatus in db.Set<DocumentStatus>()
                    on mainAgreementTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
                    join ownerBU in db.Set<BusinessUnit>()
                    on mainAgreementTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljMainAgreementTableOwnerBU
                    from ownerBU in ljMainAgreementTableOwnerBU.DefaultIfEmpty()
                    join consortiumTable in db.Set<ConsortiumTable>()
                    on mainAgreementTable.ConsortiumTableGUID equals consortiumTable.ConsortiumTableGUID into ljConsortiumTable
                    from consortiumTable in ljConsortiumTable.DefaultIfEmpty()
                    join creditAppRequestTable in db.Set<CreditAppRequestTable>()
                    on mainAgreementTable.CreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID into ljCreditAppRequestTable
                    from creditAppRequestTable in ljCreditAppRequestTable.DefaultIfEmpty()
                    join creditLimitType in db.Set<CreditLimitType>()
                    on mainAgreementTable.CreditLimitTypeGUID equals creditLimitType.CreditLimitTypeGUID into ljCreditLimitType
                    from creditLimitType in ljCreditLimitType.DefaultIfEmpty()
                    join refMainAgreementTable in db.Set<MainAgreementTable>()
                    on mainAgreementTable.RefMainAgreementTableGUID equals refMainAgreementTable.MainAgreementTableGUID into ljRefMainAgreementTable
                    from refMainAgreementTable in ljRefMainAgreementTable.DefaultIfEmpty()
                    join documentReason in db.Set<DocumentReason>()
                    on mainAgreementTable.DocumentReasonGUID equals documentReason.DocumentReasonGUID into ljdocumentReason
                    from documentReason in ljdocumentReason.DefaultIfEmpty()
                    join withdrawalTable in db.Set<WithdrawalTable>()
                    on mainAgreementTable.WithdrawalTableGUID equals withdrawalTable.WithdrawalTableGUID into ljwithdrawalTable
                    from withdrawalTable in ljwithdrawalTable.DefaultIfEmpty()
                    join buyerTable in db.Set<BuyerTable>()
                    on mainAgreementTable.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljBuyerTable
                    from buyerTable in ljBuyerTable.DefaultIfEmpty()
                    select new MainAgreementTableItemViewMap
                    {
                        CompanyGUID = mainAgreementTable.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = mainAgreementTable.Owner,
                        OwnerBusinessUnitGUID = mainAgreementTable.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        CreatedBy = mainAgreementTable.CreatedBy,
                        CreatedDateTime = mainAgreementTable.CreatedDateTime,
                        ModifiedBy = mainAgreementTable.ModifiedBy,
                        ModifiedDateTime = mainAgreementTable.ModifiedDateTime,
                        MainAgreementTableGUID = mainAgreementTable.MainAgreementTableGUID,
                        AgreementDate = mainAgreementTable.AgreementDate,
                        AgreementDocType = mainAgreementTable.AgreementDocType,
                        Agreementextension = mainAgreementTable.Agreementextension,
                        AgreementYear = mainAgreementTable.AgreementYear,
                        ApprovedCreditLimit = mainAgreementTable.ApprovedCreditLimit,
                        ApprovedCreditLimitLine = mainAgreementTable.ApprovedCreditLimitLine,
                        BuyerAgreementDescription = mainAgreementTable.BuyerAgreementDescription,
                        BuyerAgreementReferenceId = mainAgreementTable.BuyerAgreementReferenceId,
                        BuyerName = mainAgreementTable.BuyerName,
                        BuyerTableGUID = mainAgreementTable.BuyerTableGUID,
                        ConsortiumTableGUID = mainAgreementTable.ConsortiumTableGUID,
                        CreditAppRequestTableGUID = mainAgreementTable.CreditAppRequestTableGUID,
                        CreditAppTableGUID = mainAgreementTable.CreditAppTableGUID,
                        CreditLimitTypeGUID = mainAgreementTable.CreditLimitTypeGUID,
                        CustomerAltName = mainAgreementTable.CustomerAltName,
                        CustomerName = mainAgreementTable.CustomerName,
                        CustomerTableGUID = mainAgreementTable.CustomerTableGUID,
                        Description = mainAgreementTable.Description,
                        DocumentReasonGUID = mainAgreementTable.DocumentReasonGUID,
                        DocumentStatusGUID = mainAgreementTable.DocumentStatusGUID,
                        ExpiryDate = mainAgreementTable.ExpiryDate,
                        InternalMainAgreementId = mainAgreementTable.InternalMainAgreementId,
                        MainAgreementId = mainAgreementTable.MainAgreementId,
                        MaxInterestPct = mainAgreementTable.MaxInterestPct,
                        ProductType = mainAgreementTable.ProductType,
                        RefMainAgreementId = (refMainAgreementTable != null) ? refMainAgreementTable.MainAgreementId : null,
                        RefMainAgreementTableGUID = mainAgreementTable.RefMainAgreementTableGUID,
                        Remark = mainAgreementTable.Remark,
                        SigningDate = mainAgreementTable.SigningDate,
                        StartDate = mainAgreementTable.StartDate,
                        TotalInterestPct = mainAgreementTable.TotalInterestPct,
                        WithdrawalTable_DueDate = (withdrawalTable != null) ? withdrawalTable.DueDate : (DateTime?)null,
                        WithdrawalTableGUID = mainAgreementTable.WithdrawalTableGUID,
                        DocumentStatus_StatusId = documentStatus.StatusId,
                        DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
                        CustomerTable_CustomerId = customerTable.CustomerId,
                        MainAgreementTable_Values = (refMainAgreementTable != null) ? SmartAppUtil.GetDropDownLabel(refMainAgreementTable.InternalMainAgreementId, refMainAgreementTable.Description) : null,
                        CreditAppRequestTable_Values = (creditAppRequestTable != null) ? SmartAppUtil.GetDropDownLabel(creditAppRequestTable.CreditAppRequestId, creditAppRequestTable.Description) : null,
                        ConsortiumTable_Values = (consortiumTable != null) ? SmartAppUtil.GetDropDownLabel(consortiumTable.ConsortiumId, consortiumTable.Description) : null,
                        CreditLimitType_Values = (creditLimitType != null) ? SmartAppUtil.GetDropDownLabel(creditLimitType.CreditLimitTypeId, creditLimitType.Description) : null,
                        DocumentReason_Values = (documentReason != null) ? SmartAppUtil.GetDropDownLabel(documentReason.ReasonId, documentReason.Description) : null,
                        BuyerTable_Values = (buyerTable != null) ? SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName) : null,
                        CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                        CreditLimitType_Revolving = (creditLimitType != null) ? creditLimitType.Revolving : false,

                        RowVersion = mainAgreementTable.RowVersion,
                    });
        }
        private IQueryable<GenMainAgmAddendumViewMap> GetGenMainAgmAddendumItemQuery()
        {
            return (from mainAgreementTable in Entity
                    join company in db.Set<Company>()
                    on mainAgreementTable.CompanyGUID equals company.CompanyGUID
                    join customerTable in db.Set<CustomerTable>()
                    on mainAgreementTable.CustomerTableGUID equals customerTable.CustomerTableGUID
                    join documentStatus in db.Set<DocumentStatus>()
                    on mainAgreementTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
                    join ownerBU in db.Set<BusinessUnit>()
                    on mainAgreementTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljMainAgreementTableOwnerBU
                    from ownerBU in ljMainAgreementTableOwnerBU.DefaultIfEmpty()
                    join consortiumTable in db.Set<ConsortiumTable>()
                    on mainAgreementTable.ConsortiumTableGUID equals consortiumTable.ConsortiumTableGUID into ljConsortiumTable
                    from consortiumTable in ljConsortiumTable.DefaultIfEmpty()
                    join creditAppRequestTable in db.Set<CreditAppRequestTable>()
                    on mainAgreementTable.CreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID into ljCreditAppRequestTable
                    from creditAppRequestTable in ljCreditAppRequestTable.DefaultIfEmpty()
                    join creditLimitType in db.Set<CreditLimitType>()
                    on mainAgreementTable.CreditLimitTypeGUID equals creditLimitType.CreditLimitTypeGUID into ljCreditLimitType
                    from creditLimitType in ljCreditLimitType.DefaultIfEmpty()
                    join refMainAgreementTable in db.Set<MainAgreementTable>()
                    on mainAgreementTable.RefMainAgreementTableGUID equals refMainAgreementTable.MainAgreementTableGUID into ljRefMainAgreementTable
                    from refMainAgreementTable in ljRefMainAgreementTable.DefaultIfEmpty()
                    join buyerTable in db.Set<BuyerTable>()
                    on mainAgreementTable.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
                    from buyerTable in ljbuyerTable.DefaultIfEmpty()
                    select new GenMainAgmAddendumViewMap
                    {
                        CompanyGUID = mainAgreementTable.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = mainAgreementTable.Owner,
                        OwnerBusinessUnitGUID = mainAgreementTable.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        CreatedBy = mainAgreementTable.CreatedBy,
                        CreatedDateTime = mainAgreementTable.CreatedDateTime,
                        ModifiedBy = mainAgreementTable.ModifiedBy,
                        ModifiedDateTime = mainAgreementTable.ModifiedDateTime,
                        MainAgreementTableGUID = mainAgreementTable.MainAgreementTableGUID,
                        AgreementDocType = mainAgreementTable.AgreementDocType,
                        MainAgreementDescription = mainAgreementTable.Description,
                        AgreementDate = mainAgreementTable.AgreementDate,
                        CustomerGUID = mainAgreementTable.CustomerTableGUID,
                        CustomerId = customerTable.CustomerId,
                        CustomerName = customerTable.Name,
                        Customer_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                        Buyer_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
                        BuyerId = buyerTable.BuyerId,
                        BuyerGUID = buyerTable.BuyerTableGUID,
                        BuyerName = buyerTable.BuyerName,
                        InternalMainAgreementId = mainAgreementTable.InternalMainAgreementId,
                        MainAgreementId = mainAgreementTable.MainAgreementId,
                        CreditAppRequestType = creditAppRequestTable.CreditAppRequestType,
                        CreditLimitType_Values = SmartAppUtil.GetDropDownLabel(creditLimitType.CreditLimitTypeId, creditLimitType.Description),
                        CreditLimitType_Revolving = (creditLimitType != null) ? creditLimitType.Revolving : false,
                    });
        }
        private IQueryable<GenMainAgmNoticeOfCancelViewMap> GetGenMainAgmNoticeOfCancelItemQuery()
        {
            return (from mainAgreementTable in Entity
                    join company in db.Set<Company>()
                    on mainAgreementTable.CompanyGUID equals company.CompanyGUID
                    join customerTable in db.Set<CustomerTable>()
                    on mainAgreementTable.CustomerTableGUID equals customerTable.CustomerTableGUID
                    join documentStatus in db.Set<DocumentStatus>()
                    on mainAgreementTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
                    join ownerBU in db.Set<BusinessUnit>()
                    on mainAgreementTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljMainAgreementTableOwnerBU
                    from ownerBU in ljMainAgreementTableOwnerBU.DefaultIfEmpty()
                    join consortiumTable in db.Set<ConsortiumTable>()
                    on mainAgreementTable.ConsortiumTableGUID equals consortiumTable.ConsortiumTableGUID into ljConsortiumTable
                    from consortiumTable in ljConsortiumTable.DefaultIfEmpty()
                    join creditAppRequestTable in db.Set<CreditAppRequestTable>()
                    on mainAgreementTable.CreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID into ljCreditAppRequestTable
                    from creditAppRequestTable in ljCreditAppRequestTable.DefaultIfEmpty()
                    join creditLimitType in db.Set<CreditLimitType>()
                    on mainAgreementTable.CreditLimitTypeGUID equals creditLimitType.CreditLimitTypeGUID into ljCreditLimitType
                    from creditLimitType in ljCreditLimitType.DefaultIfEmpty()
                    join refMainAgreementTable in db.Set<MainAgreementTable>()
                    on mainAgreementTable.RefMainAgreementTableGUID equals refMainAgreementTable.MainAgreementTableGUID into ljRefMainAgreementTable
                    from refMainAgreementTable in ljRefMainAgreementTable.DefaultIfEmpty()
                    join buyerTable in db.Set<BuyerTable>()
                    on mainAgreementTable.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
                    from buyerTable in ljbuyerTable.DefaultIfEmpty()
                    select new GenMainAgmNoticeOfCancelViewMap
                    {
                        CompanyGUID = mainAgreementTable.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = mainAgreementTable.Owner,
                        OwnerBusinessUnitGUID = mainAgreementTable.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        CreatedBy = mainAgreementTable.CreatedBy,
                        CreatedDateTime = mainAgreementTable.CreatedDateTime,
                        ModifiedBy = mainAgreementTable.ModifiedBy,
                        ModifiedDateTime = mainAgreementTable.ModifiedDateTime,
                        MainAgreementTableGUID = mainAgreementTable.MainAgreementTableGUID,
                        InternalMainAgreementId = mainAgreementTable.InternalMainAgreementId,
                        MainAgreementId = mainAgreementTable.MainAgreementId,
                        MainAgreementDescription = mainAgreementTable.Description,
                        AgreementDate = mainAgreementTable.AgreementDate,
                        AgreementDocType = mainAgreementTable.AgreementDocType,
                        CustomerTableGUID = mainAgreementTable.CustomerTableGUID,
                        CustomerName = mainAgreementTable.CustomerName,
                        BuyerTableGUID = mainAgreementTable.BuyerTableGUID,
                        BuyerName = buyerTable.BuyerName,
                        DocumentReasonGUID = mainAgreementTable.DocumentReasonGUID,
                        CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                        BuyerTable_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
                        CreditLimitType_Revolving = (creditLimitType != null) ? creditLimitType.Revolving : false,
                    });
        }
        private IQueryable<GenMainAgmLoanRequestViewMap> GetGenMainAgmLoanRequestItemQuery()
        {
            return (from mainAgreementTable in Entity
                    join company in db.Set<Company>()
                    on mainAgreementTable.CompanyGUID equals company.CompanyGUID
                    join customerTable in db.Set<CustomerTable>()
                    on mainAgreementTable.CustomerTableGUID equals customerTable.CustomerTableGUID
                    join documentStatus in db.Set<DocumentStatus>()
                    on mainAgreementTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
                    join ownerBU in db.Set<BusinessUnit>()
                    on mainAgreementTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljMainAgreementTableOwnerBU
                    from ownerBU in ljMainAgreementTableOwnerBU.DefaultIfEmpty()
                    join consortiumTable in db.Set<ConsortiumTable>()
                    on mainAgreementTable.ConsortiumTableGUID equals consortiumTable.ConsortiumTableGUID into ljConsortiumTable
                    from consortiumTable in ljConsortiumTable.DefaultIfEmpty()
                    join creditAppTable in db.Set<CreditAppTable>()
                    on mainAgreementTable.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljCreditAppTable
                    from creditAppTable in ljCreditAppTable.DefaultIfEmpty()
                    join creditAppRequestTable in db.Set<CreditAppRequestTable>()
                    on mainAgreementTable.CreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID into ljCreditAppRequestTable
                    from creditAppRequestTable in ljCreditAppRequestTable.DefaultIfEmpty()
                    join creditLimitType in db.Set<CreditLimitType>()
                    on mainAgreementTable.CreditLimitTypeGUID equals creditLimitType.CreditLimitTypeGUID into ljCreditLimitType
                    from creditLimitType in ljCreditLimitType.DefaultIfEmpty()
                    join refMainAgreementTable in db.Set<MainAgreementTable>()
                    on mainAgreementTable.RefMainAgreementTableGUID equals refMainAgreementTable.MainAgreementTableGUID into ljRefMainAgreementTable
                    from refMainAgreementTable in ljRefMainAgreementTable.DefaultIfEmpty()
                    join buyerTable in db.Set<BuyerTable>()
                    on mainAgreementTable.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljbuyerTable
                    from buyerTable in ljbuyerTable.DefaultIfEmpty()
                    select new GenMainAgmLoanRequestViewMap
                    {
                        CompanyGUID = mainAgreementTable.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = mainAgreementTable.Owner,
                        OwnerBusinessUnitGUID = mainAgreementTable.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        CreatedBy = mainAgreementTable.CreatedBy,
                        CreatedDateTime = mainAgreementTable.CreatedDateTime,
                        ModifiedBy = mainAgreementTable.ModifiedBy,
                        ModifiedDateTime = mainAgreementTable.ModifiedDateTime,
                        MainAgreementTableGUID = mainAgreementTable.MainAgreementTableGUID,
                        InternalMainAgreementId = mainAgreementTable.InternalMainAgreementId,
                        MainAgreementId = mainAgreementTable.MainAgreementId,
                        MainAgreementDescription = mainAgreementTable.Description,
                        CustomerGUID = mainAgreementTable.CustomerTableGUID,
                        CustomerName = mainAgreementTable.CustomerName,
                        BuyerGUID = mainAgreementTable.BuyerTableGUID,
                        BuyerName = buyerTable.BuyerName,
                        CreditAppTableGUID = mainAgreementTable.CreditAppTableGUID,
                        CreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
                        Customer_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                        Buyer_Values = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
                    });
        }
        public MainAgreementTableItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<MainAgreementTableItemViewMap, MainAgreementTableItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public GenMainAgmAddendumView GetGenMainAgmAddendumByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetGenMainAgmAddendumItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<GenMainAgmAddendumViewMap, GenMainAgmAddendumView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public GenMainAgmNoticeOfCancelView GetGenMainAgmNoticeOfCancelByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetGenMainAgmNoticeOfCancelItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<GenMainAgmNoticeOfCancelViewMap, GenMainAgmNoticeOfCancelView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public GenMainAgmLoanRequestView GetGenMainAgmLoanRequestByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetGenMainAgmLoanRequestItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<GenMainAgmLoanRequestViewMap, GenMainAgmLoanRequestView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public MainAgreementTable CreateMainAgreementTable(MainAgreementTable mainAgreementTable)
        {
            try
            {
                mainAgreementTable.MainAgreementTableGUID = Guid.NewGuid();
                base.Add(mainAgreementTable);
                return mainAgreementTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateMainAgreementTableVoid(MainAgreementTable mainAgreementTable)
        {
            try
            {
                CreateMainAgreementTable(mainAgreementTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public MainAgreementTable UpdateMainAgreementTable(MainAgreementTable dbMainAgreementTable, MainAgreementTable inputMainAgreementTable, List<string> skipUpdateFields = null)
        {
            try
            {
                dbMainAgreementTable = dbMainAgreementTable.MapUpdateValues<MainAgreementTable>(inputMainAgreementTable);
                base.Update(dbMainAgreementTable);
                return dbMainAgreementTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateMainAgreementTableVoid(MainAgreementTable dbMainAgreementTable, MainAgreementTable inputMainAgreementTable, List<string> skipUpdateFields = null)
        {
            try
            {
                dbMainAgreementTable = dbMainAgreementTable.MapUpdateValues<MainAgreementTable>(inputMainAgreementTable, skipUpdateFields);
                base.Update(dbMainAgreementTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Create, Update, Delete
        public MainAgreementTable GetMainAgreementTableByIdNoTrackingByAccessLevel(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.MainAgreementTableGUID == guid)
                    .FilterByAccessLevel(SysParm.AccessLevel)
                    .AsNoTracking()
                    .FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public AgreementTableInfo GenerateAgreementInfo(MainAgreementTable input)
        {
            try
            {
                List<MainAgreementTable> mainAgreementTables = new List<MainAgreementTable>();
                mainAgreementTables.Add(input);
                var guarantorTransName = (from creditAppTable in db.Set<CreditAppTable>().Where(t => t.CreditAppTableGUID == input.CreditAppTableGUID).ToList()
                                          join guarantorTrans in db.Set<GuarantorTrans>().Where(t => t.InActive == false).ToList()
                                          on creditAppTable.CreditAppTableGUID equals guarantorTrans.RefGUID
                                          join relatedPersonTable in db.Set<RelatedPersonTable>()
                                          on guarantorTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID
                                          group new { creditAppTable, relatedPersonTable } by creditAppTable.CreditAppTableGUID into g
                                          select new
                                          {
                                              CreditAppTableGUID = g.Key,
                                              GuarantorName = string.Join(", ", g.Select(y => y.relatedPersonTable.Name))
                                          }).ToList();

                return (from mainAgreementTable in mainAgreementTables

                        join company in db.Set<Company>().Where(t => t.CompanyGUID == input.CompanyGUID).ToList()
                        on mainAgreementTable.CompanyGUID equals company.CompanyGUID

                        join creditAppTable in db.Set<CreditAppTable>().Where(t => t.CreditAppTableGUID == input.CreditAppTableGUID).ToList()
                        on mainAgreementTable.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID

                        join customerTable in db.Set<CustomerTable>()
                        on mainAgreementTable.CustomerTableGUID equals customerTable.CustomerTableGUID

                        join branch in db.Set<Branch>()
                        on company.DefaultBranchGUID equals branch.BranchGUID into ljBranch
                        from branch in ljBranch.DefaultIfEmpty()

                        join buyerTable in db.Set<BuyerTable>()
                        on mainAgreementTable.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljBuyerTable
                        from buyerTable in ljBuyerTable.DefaultIfEmpty()

                        join guarantorName in guarantorTransName
                        on mainAgreementTable.CreditAppTableGUID equals (guarantorName != null) ? guarantorName.CreditAppTableGUID : Guid.Empty into ljGuarantorName
                        from guarantorName in ljGuarantorName.DefaultIfEmpty()

                        join addressTransGuarantor in db.Set<AddressTrans>()
                        on creditAppTable.RegisteredAddressGUID equals addressTransGuarantor.AddressTransGUID into ljAddressTransGuarantor
                        from addressTransGuarantor in ljAddressTransGuarantor.DefaultIfEmpty()

                        join addressTransBuyer in db.Set<AddressTrans>().Where(t => t.Primary == true).ToList()
                        on (buyerTable != null) ? buyerTable.BuyerTableGUID : Guid.Empty equals addressTransBuyer.RefGUID into ljAddressTransBuyer
                        from addressTransBuyer in ljAddressTransBuyer.DefaultIfEmpty()

                        join addressTransBranch in db.Set<AddressTrans>()
                        on (branch != null) ? branch.BranchGUID : Guid.Empty equals addressTransBranch.RefGUID into ljAddressTransBranch
                        from addressTransBranch in ljAddressTransBranch.DefaultIfEmpty()

                        join contactTransPhone in db.Set<ContactTrans>().Where(t => t.PrimaryContact == true &&
                                                                                    t.ContactType == (int)ContactType.Phone).ToList()
                        on company.DefaultBranchGUID equals contactTransPhone.RefGUID into ljContactTransPhone
                        from contactTransPhone in ljContactTransPhone.DefaultIfEmpty()

                        join contactTransFax in db.Set<ContactTrans>().Where(t => t.PrimaryContact == true &&
                                                                                    t.ContactType == (int)ContactType.Fax).ToList()
                        on company.DefaultBranchGUID equals contactTransFax.RefGUID into ljContactTransFax
                        from contactTransFax in ljContactTransFax.DefaultIfEmpty()

                        join companyBank in db.Set<CompanyBank>().Where(t => t.Primary == true).ToList()
                        on company.CompanyGUID equals companyBank.CompanyGUID into ljCompanyBank
                        from companyBank in ljCompanyBank.DefaultIfEmpty()

                        join sysUserTable in db.Set<SysUserTable>()
                        on mainAgreementTable.CreatedBy equals sysUserTable.UserName into ljSysUserTable
                        from sysUserTable in ljSysUserTable.DefaultIfEmpty()

                        join employeeTable in db.Set<EmployeeTable>()
                        on sysUserTable.Id equals employeeTable.UserId into ljEmployeeTable
                        from employeeTable in ljEmployeeTable.DefaultIfEmpty()
                        select new AgreementTableInfo
                        {
                            RefGUID = mainAgreementTable.MainAgreementTableGUID,
                            RefType = (int)RefType.MainAgreement,
                            GuarantorName = (guarantorName != null) ? guarantorName.GuarantorName : null,
                            CustomerAddress1 = addressTransGuarantor.Address1,
                            CustomerAddress2 = addressTransGuarantor.Address2,
                            BuyerAddress1 = (addressTransBuyer != null) ? addressTransBuyer.Address1 : null,
                            BuyerAddress2 = (addressTransBuyer != null) ? addressTransBuyer.Address2 : null,
                            CompanyName = company.Name,
                            CompanyAltName = company.AltName,
                            CompanyAddress1 = (addressTransBranch != null) ? addressTransBranch.Address1 : null,
                            CompanyAddress2 = (addressTransBranch != null) ? addressTransBranch.Address2 : null,
                            CompanyPhone = (contactTransPhone != null) ? contactTransPhone.Description : null,
                            CompanyFax = (contactTransFax != null) ? contactTransFax.Description : null,
                            CompanyBankGUID = companyBank?.CompanyBankGUID,
                            WitnessCompany1GUID = customerTable.ResponsibleByGUID,
                            WitnessCompany2GUID = employeeTable?.EmployeeTableGUID,
                            CompanyGUID = company.CompanyGUID,
                            AuthorizedPersonTransBuyer1GUID = null,
                            AuthorizedPersonTransBuyer2GUID = null,
                            AuthorizedPersonTransBuyer3GUID = null,
                            AuthorizedPersonTransCompany1GUID = null,
                            AuthorizedPersonTransCompany2GUID = null,
                            AuthorizedPersonTransCompany3GUID = null,
                            AuthorizedPersonTransCustomer1GUID = null,
                            AuthorizedPersonTransCustomer2GUID = null,
                            AuthorizedPersonTransCustomer3GUID = null,
                            AuthorizedPersonTypeCompanyGUID = null,
                            AuthorizedPersonTypeCustomerGUID = null,
                            AuthorizedPersonTypeBuyerGUID = null,
                            WitnessCustomer1 = null,
                            WitnessCustomer2 = null,
                            Owner = mainAgreementTable.Owner,
                            OwnerBusinessUnitGUID = mainAgreementTable.OwnerBusinessUnitGUID,
                        }).ToList().FirstOrDefault();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<MainAgreementTable> GetListByRefMainAgreementTableAndAgreementDocType(Guid guid, AgreementDocType agreementDocType)
        {
            try
            {
                var result = Entity.Where(item => item.RefMainAgreementTableGUID == guid && item.AgreementDocType == (int)agreementDocType).AsNoTracking().ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<MainAgreementTable> GetMainAgreementTableByCreditAppAndMainAgmTableStatus(Guid creditAppTableGUID, MainAgreementStatus status)
        {
            try
            {
                var statusId = ((int)status).ToString();
                var result =
                    (from mainAgm in Entity.Where(w => w.CreditAppTableGUID == creditAppTableGUID)
                     join documentStatus in db.Set<DocumentStatus>().Where(w => w.StatusId == statusId)
                     on mainAgm.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
                     select mainAgm)
                     .AsNoTracking()
                     .ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Validate
        public override void ValidateAdd(IEnumerable<MainAgreementTable> items)
        {
            base.ValidateAdd(items);
            items.ToList().ForEach(f => CheckDuplicateMainAgreementId(f));
        }
        public override void ValidateAdd(MainAgreementTable item)
        {
            base.ValidateAdd(item);
            CheckDuplicateMainAgreementId(item);
        }
        public override void ValidateUpdate(IEnumerable<MainAgreementTable> items)
        {
            base.ValidateUpdate(items);
            items.ToList().ForEach(f => CheckDuplicateMainAgreementId(f));
        }
        public override void ValidateUpdate(MainAgreementTable item)
        {
            base.ValidateUpdate(item);
            CheckDuplicateMainAgreementId(item);
        }
        public void CheckDuplicateMainAgreementId(MainAgreementTable item)
        {
            try
            {
                SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
                bool isDuplicateMainAgreementId = IsDuplicateMainAgreementId(item.MainAgreementTableGUID, item.MainAgreementId);
                if (isDuplicateMainAgreementId)
                {
                    smartAppException.AddData("ERROR.DUPLICATE", new string[] { "LABEL.MAIN_AGREEMENT_ID" });
                }
                if (smartAppException.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(smartAppException);
                }
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        public bool IsDuplicateMainAgreementId(Guid guid, string id)
        {
            try
            {
                bool isDuplicateMainAgreementId = Entity.Any(a => a.MainAgreementId == id
                                                                    && a.CompanyGUID == db.GetCompanyFilter().StringToGuid()
                                                                    && !string.IsNullOrEmpty(a.MainAgreementId)
                                                                    && a.MainAgreementTableGUID != guid);
                return isDuplicateMainAgreementId;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion

        #region Bookmark Shared
        public GenMainAgreementBookmarkSharedView MainAgreementBookmarkSharedView(Guid refGUID)
        {

            try
            {
                var result = (from mainAgreementTable in Entity.Where(w => w.MainAgreementTableGUID == refGUID)

                              join customer in db.Set<CustomerTable>() on mainAgreementTable.CustomerTableGUID equals customer
                                 .CustomerTableGUID

                              join agreementTableInfo in db.Set<AgreementTableInfo>().Where(w => w.RefType == (int)RefType.MainAgreement)
                               on mainAgreementTable.MainAgreementTableGUID equals agreementTableInfo.RefGUID
                               into ljAgreementInfo
                              from agreementTableInfo in ljAgreementInfo.DefaultIfEmpty()

                              join employeeTableWitness1 in db.Set<EmployeeTable>() on agreementTableInfo.WitnessCompany1GUID equals employeeTableWitness1.EmployeeTableGUID
                              into ljWitnessCompany1
                              from employeeTableWitness1 in ljWitnessCompany1.DefaultIfEmpty()

                              join employeeTableWitness2 in db.Set<EmployeeTable>() on agreementTableInfo.WitnessCompany2GUID equals employeeTableWitness2.EmployeeTableGUID
                              into ljWitnessCompany2
                              from employeeTableWitness2 in ljWitnessCompany2.DefaultIfEmpty()


                              join agreementTableInfoText in db.Set<AgreementTableInfoText>()
                              on agreementTableInfo.AgreementTableInfoGUID equals agreementTableInfoText.AgreementTableInfoGUID into lAgreementTableInfoText
                              from agreementTableInfoText in lAgreementTableInfoText.DefaultIfEmpty()

                              join employeeTable1 in db.Set<EmployeeTable>() on agreementTableInfo.AuthorizedPersonTransCompany1GUID equals employeeTable1.EmployeeTableGUID into ljCompSign1
                              from employeeTable1 in ljCompSign1.DefaultIfEmpty()

                              join employeeTable2 in db.Set<EmployeeTable>() on agreementTableInfo.AuthorizedPersonTransCompany2GUID equals employeeTable2.EmployeeTableGUID into ljCompSign2
                              from employeeTable2 in ljCompSign2.DefaultIfEmpty()

                              join employeeTable3 in db.Set<EmployeeTable>() on agreementTableInfo.AuthorizedPersonTransCompany3GUID equals employeeTable3.EmployeeTableGUID into ljCompSign3
                              from employeeTable3 in ljCompSign3.DefaultIfEmpty()


                              join authorizedPersonTransBuyer1 in db.Set<AuthorizedPersonTrans>()
                              on agreementTableInfo.AuthorizedPersonTransBuyer1GUID equals authorizedPersonTransBuyer1.AuthorizedPersonTransGUID into ljAuthBuyerRelate1
                              from authorizedPersonTransBuyer1 in ljAuthBuyerRelate1.DefaultIfEmpty()
                              join relatedPersonTable1 in db.Set<RelatedPersonTable>() on authorizedPersonTransBuyer1.RelatedPersonTableGUID equals relatedPersonTable1.RelatedPersonTableGUID
                              into authRelateTable1
                              from relatedPersonTable1 in authRelateTable1.DefaultIfEmpty()

                              join authorizedPersonTransBuyer2 in db.Set<AuthorizedPersonTrans>() on agreementTableInfo
                                      .AuthorizedPersonTransBuyer2GUID equals authorizedPersonTransBuyer2.AuthorizedPersonTransGUID
                                  into ljAuthBuyerRelate2
                              from authorizedPersonTransBuyer2 in ljAuthBuyerRelate2.DefaultIfEmpty()
                              join relatedPersonTable2 in db.Set<RelatedPersonTable>() on authorizedPersonTransBuyer2.RelatedPersonTableGUID equals
                                relatedPersonTable2.RelatedPersonTableGUID into authRelateTable2
                              from relatedPersonTable2 in authRelateTable2.DefaultIfEmpty()

                              join authorizedPersonTransBuyer3 in db.Set<AuthorizedPersonTrans>() on agreementTableInfo
                                      .AuthorizedPersonTransBuyer3GUID equals authorizedPersonTransBuyer3.AuthorizedPersonTransGUID into ljAuthBuyerRelate3
                              from authorizedPersonTransBuyer3 in ljAuthBuyerRelate3.DefaultIfEmpty()
                              join relatedPersonTable3 in db.Set<RelatedPersonTable>() on authorizedPersonTransBuyer3.RelatedPersonTableGUID equals
                               relatedPersonTable3.RelatedPersonTableGUID into authRelateTable3
                              from relatedPersonTable3 in authRelateTable3.DefaultIfEmpty()

                              join authorizedPersonTransCustomer1 in db.Set<AuthorizedPersonTrans>() on agreementTableInfo
                                     .AuthorizedPersonTransCustomer1GUID equals authorizedPersonTransCustomer1.AuthorizedPersonTransGUID
                                 into ljAuthCustomerRelate1
                              from authorizedPersonTransCustomer1 in ljAuthCustomerRelate1.DefaultIfEmpty()
                              join relatedPersonTableCustomer1 in db.Set<RelatedPersonTable>() on authorizedPersonTransCustomer1.RelatedPersonTableGUID equals
                                  relatedPersonTableCustomer1.RelatedPersonTableGUID into authRelatePerson1
                              from relatedPersonTableCustomer1 in authRelatePerson1.DefaultIfEmpty()

                              join authorizedPersonTransCustomer2 in db.Set<AuthorizedPersonTrans>() on agreementTableInfo
                                      .AuthorizedPersonTransCustomer2GUID equals authorizedPersonTransCustomer2.AuthorizedPersonTransGUID
                                  into ljAuthCustomerRelate2
                              from authorizedPersonTransCustomer2 in ljAuthCustomerRelate2.DefaultIfEmpty()
                              join relatedPersonTableCustomer2 in db.Set<RelatedPersonTable>() on authorizedPersonTransCustomer2.RelatedPersonTableGUID equals
                                  relatedPersonTableCustomer2.RelatedPersonTableGUID into authRelatePerson2
                              from relatedPersonTableCustomer2 in authRelatePerson2.DefaultIfEmpty()


                              join authorizedPersonTransCustomer3 in db.Set<AuthorizedPersonTrans>() on agreementTableInfo
                                      .AuthorizedPersonTransCustomer3GUID equals authorizedPersonTransCustomer3.AuthorizedPersonTransGUID
                                  into ljAuthCustomerRelate3
                              from authorizedPersonTransCustomer3 in ljAuthCustomerRelate3.DefaultIfEmpty()
                              join relatedPersonTableCustomer3 in db.Set<RelatedPersonTable>() on authorizedPersonTransCustomer3.RelatedPersonTableGUID equals
                                  relatedPersonTableCustomer3.RelatedPersonTableGUID into authRelatePerson3
                              from relatedPersonTableCustomer3 in authRelatePerson3.DefaultIfEmpty()

                              join authorizedPersonTypeCustomer in db.Set<AuthorizedPersonType>() on agreementTableInfo
                                       .AuthorizedPersonTypeCustomerGUID equals authorizedPersonTypeCustomer.AuthorizedPersonTypeGUID
                                   into ljAuthCustomerPersonType
                              from authorizedPersonTypeCustomer in ljAuthCustomerPersonType.DefaultIfEmpty()

                              join authorizedPersonTypeBuyer in db.Set<AuthorizedPersonType>() on agreementTableInfo
                                      .AuthorizedPersonTypeBuyerGUID equals authorizedPersonTypeBuyer.AuthorizedPersonTypeGUID
                                  into ljAuthBuyerPersonType
                              from authorizedPersonTypeBuyer in ljAuthBuyerPersonType.DefaultIfEmpty()

                              join authorizedPersonTypeCompany in db.Set<AuthorizedPersonType>() on agreementTableInfo
                                      .AuthorizedPersonTypeCompanyGUID equals authorizedPersonTypeCompany.AuthorizedPersonTypeGUID
                                  into ljAuthCompanyPersonType
                              from authorizedPersonTypeCompany in ljAuthCompanyPersonType.DefaultIfEmpty()

                              join companyWitness1 in db.Set<EmployeeTable>() on agreementTableInfo.WitnessCompany1GUID equals companyWitness1.EmployeeTableGUID into ljCompanyWitness1
                              from companyWitness1 in ljCompanyWitness1.DefaultIfEmpty()

                              join companyWitness2 in db.Set<EmployeeTable>() on agreementTableInfo.WitnessCompany2GUID equals companyWitness2.EmployeeTableGUID into ljCompanyWitness2
                              from companyWitness2 in ljCompanyWitness2.DefaultIfEmpty()
                              select new GenMainAgreementBookmarkSharedView
                              {
                                  CustomerAltName = mainAgreementTable.CustomerAltName,
                                  CustomerName = mainAgreementTable.CustomerName,
                                  ContAmountFirst1 = mainAgreementTable.ApprovedCreditLimit,
                                  ContAmountSecond1 = null,
                                  AgreementDate = mainAgreementTable.AgreementDate,
                                  AgreementDate1 = null,
                                  AgreementDate2 = null,
                                  AgreementDate3 = null,
                                  AgreementDate4 = null,
                                  AgreementDate5 = null,
                                  AgreementNo1 = mainAgreementTable.MainAgreementId,
                                  AgreementNo2 = mainAgreementTable.MainAgreementId,
                                  AgreementNo3 = mainAgreementTable.MainAgreementId,
                                  AgreementNo4 = mainAgreementTable.MainAgreementId,
                                  AgreementNo5 = mainAgreementTable.MainAgreementId,
                                  InterestRate1 = mainAgreementTable.TotalInterestPct,
                                  AliasNameCust1 = mainAgreementTable.CustomerAltName,
                                  AliasNameCust2 = mainAgreementTable.CustomerAltName,
                                  AliasNameCust3 = mainAgreementTable.CustomerAltName,
                                  AliasNameCust4 = mainAgreementTable.CustomerAltName,
                                  AliasNameCust5 = mainAgreementTable.CustomerAltName,
                                  AliasNameCust6 = mainAgreementTable.CustomerAltName,
                                  AliasNameCust7 = mainAgreementTable.CustomerAltName,
                                  AliasNameCust8 = mainAgreementTable.CustomerAltName,
                                  CustName1 = mainAgreementTable.CustomerName,
                                  CustName2 = mainAgreementTable.CustomerName,
                                  CustName3 = mainAgreementTable.CustomerName,
                                  CustName4 = mainAgreementTable.CustomerName,
                                  CustName5 = mainAgreementTable.CustomerName,
                                  CustName6 = mainAgreementTable.CustomerName,
                                  CustName7 = mainAgreementTable.CustomerName,
                                  CustName8 = mainAgreementTable.CustomerName,
                                  CustTaxID1 = customer.TaxID,
                                  CompanyWitnessFirst1 = companyWitness1 == null ? null : companyWitness1.Name,
                                  CompanyWitnessSecond1 = companyWitness2 == null ? null : companyWitness2.Name,
                                  Text1 = agreementTableInfoText != null ? agreementTableInfoText.Text1 : "",
                                  Text2 = agreementTableInfoText != null ? agreementTableInfoText.Text2 : "",
                                  Text3 = agreementTableInfoText != null ? agreementTableInfoText.Text3 : "",
                                  Text4 = agreementTableInfoText != null ? agreementTableInfoText.Text4 : "",
                                  AuthorityPersonFirst1 = employeeTable1 != null ? employeeTable1.Name : "",
                                  AuthorityPersonFirst2 = employeeTable1 != null ? employeeTable1.Name : "",
                                  AuthorityPersonSecond1 = employeeTable2 != null ? employeeTable2.Name : "",
                                  AuthorityPersonSecond2 = employeeTable2 != null ? employeeTable2.Name : "",
                                  AuthorityPersonThird1 = employeeTable3 != null ? employeeTable3.Name : "",
                                  AuthorityPersonThird2 = employeeTable3 != null ? employeeTable3.Name : "",
                                  AuthorizedBuyerFirst1 = relatedPersonTable1 != null ? relatedPersonTable1.Name : "",
                                  AuthorizedBuyerFirst2 = relatedPersonTable1 != null ? relatedPersonTable1.Name : "",
                                  AuthorizedBuyerSecond1 = relatedPersonTable2 != null ? relatedPersonTable2.Name : "",
                                  AuthorizedBuyerSecond2 = relatedPersonTable2 != null ? relatedPersonTable2.Name : "",
                                  AuthorizedBuyerThird1 = relatedPersonTable3 != null ? relatedPersonTable3.Name : "",
                                  AuthorizedBuyerThird2 = relatedPersonTable3 != null ? relatedPersonTable3.Name : "",
                                  AuthorizedCustFirst1 = relatedPersonTableCustomer1 != null ? relatedPersonTableCustomer1.Name : "",
                                  AuthorizedCustFirst2 = relatedPersonTableCustomer1 != null ? relatedPersonTableCustomer1.Name : "",
                                  AuthorizedCustSecond1 = relatedPersonTableCustomer2 != null ? relatedPersonTableCustomer2.Name : "",
                                  AuthorizedCustSecond2 = relatedPersonTableCustomer2 != null ? relatedPersonTableCustomer2.Name : "",
                                  AuthorizedCustThird1 = relatedPersonTableCustomer3 != null ? relatedPersonTableCustomer3.Name : "",
                                  AuthorizedCustThird2 = relatedPersonTableCustomer3 != null ? relatedPersonTableCustomer3.Name : "",
                                  CustWitnessFirst1 = agreementTableInfo != null ? agreementTableInfo.WitnessCustomer1 : "",
                                  CustWitnessSecond1 = agreementTableInfo != null ? agreementTableInfo.WitnessCustomer2 : "",
                                  PositionBuyer1 = authorizedPersonTypeBuyer != null ? authorizedPersonTypeBuyer.Description : "",
                                  PositionCust1 = authorizedPersonTypeCustomer != null ? authorizedPersonTypeCustomer.Description : "",
                                  PositionCust2 = authorizedPersonTypeCustomer != null ? authorizedPersonTypeCustomer.Description : "",
                                  PositionCust3 = authorizedPersonTypeCustomer != null ? authorizedPersonTypeCustomer.Description : "",
                                  PositionLIT1 = authorizedPersonTypeCompany != null ? authorizedPersonTypeCompany.Description : "",
                                  PositionLIT2 = authorizedPersonTypeCompany != null ? authorizedPersonTypeCompany.Description : "",
                                  PositionLIT3 = authorizedPersonTypeCompany != null ? authorizedPersonTypeCompany.Description : "",
                                  CustAddress1 = agreementTableInfo != null ? string.Concat(agreementTableInfo.CustomerAddress1, " ", agreementTableInfo.CustomerAddress2) : "",
                                  CustAddress2 = agreementTableInfo != null ? string.Concat(agreementTableInfo.CustomerAddress1, " ", agreementTableInfo.CustomerAddress2) : "",
                                  WitnessCompany1GUID = agreementTableInfo.WitnessCompany1GUID,
                                  WitnessCompany2GUID = agreementTableInfo.WitnessCompany2GUID,
                                  ManualNo1 = null
                              }).AsNoTracking().FirstOrDefault();

                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        public GenMainAgreementBookmarkSharedJVView MainAgreementBookmarkSharedJVView(Guid refGUID)
        {
            try
            {
                var result = (from mainAgreementTable in Entity
                              join company in db.Set<Company>() on mainAgreementTable.CompanyGUID equals company.CompanyGUID

                              join customer in db.Set<CustomerTable>() on mainAgreementTable.CustomerTableGUID equals customer
                                 .CustomerTableGUID

                              join agreementTableInfo in db.Set<AgreementTableInfo>().Where(w => w.RefType == (int)RefType.MainAgreement)
                               on mainAgreementTable.MainAgreementTableGUID equals agreementTableInfo.RefGUID
                               into ljAgreementInfo
                              from agreementTableInfo in ljAgreementInfo

                              join agreementTableInfoText in db.Set<AgreementTableInfoText>()
                             on agreementTableInfo.AgreementTableInfoGUID equals agreementTableInfoText.AgreementTableInfoGUID into lAgreementTableInfoText
                              from agreementTableInfoText in lAgreementTableInfoText.DefaultIfEmpty()

                              join companySignature1 in db.Set<CompanySignature>()
                              on agreementTableInfo.AuthorizedPersonTransCompany1GUID equals companySignature1.CompanySignatureGUID into lCompanySignatureAgreementTable1
                              from companySignature1 in lCompanySignatureAgreementTable1.DefaultIfEmpty()
                              join employeeTable1 in db.Set<EmployeeTable>() on companySignature1.EmployeeTableGUID equals employeeTable1.EmployeeTableGUID into ljCompSign1
                              from employeeTable1 in ljCompSign1.DefaultIfEmpty()

                              join companySignature2 in db.Set<CompanySignature>()
                              on agreementTableInfo.AuthorizedPersonTransCompany2GUID equals companySignature2.CompanySignatureGUID into lComSignatureAgreementTable2
                              from companySignature2 in lComSignatureAgreementTable2.DefaultIfEmpty()
                              join employeeTable2 in db.Set<EmployeeTable>() on companySignature2.EmployeeTableGUID equals employeeTable2.EmployeeTableGUID into ljCompSign2
                              from employeeTable2 in ljCompSign2.DefaultIfEmpty()

                              join companySignature3 in db.Set<CompanySignature>()
                              on agreementTableInfo.AuthorizedPersonTransCompany3GUID equals companySignature3.CompanySignatureGUID into lComSignatureAgreementTable3
                              from companySignature3 in lComSignatureAgreementTable3.DefaultIfEmpty()
                              join employeeTable3 in db.Set<EmployeeTable>() on companySignature3.EmployeeTableGUID equals employeeTable3.EmployeeTableGUID into ljCompSign3
                              from employeeTable3 in ljCompSign3.DefaultIfEmpty()


                              join authorizedPersonTransBuyer1 in db.Set<AuthorizedPersonTrans>()
                              on agreementTableInfo.AuthorizedPersonTransBuyer1GUID equals authorizedPersonTransBuyer1.AuthorizedPersonTransGUID into ljAuthBuyerRelate1
                              from authorizedPersonTransBuyer1 in ljAuthBuyerRelate1.DefaultIfEmpty()
                              join relatedPersonTable1 in db.Set<RelatedPersonTable>() on authorizedPersonTransBuyer1.RelatedPersonTableGUID equals relatedPersonTable1.RelatedPersonTableGUID
                              into authRelateTable1
                              from relatedPersonTable1 in authRelateTable1.DefaultIfEmpty()

                              join authorizedPersonTransBuyer2 in db.Set<AuthorizedPersonTrans>() on agreementTableInfo
                                      .AuthorizedPersonTransBuyer2GUID equals authorizedPersonTransBuyer2.AuthorizedPersonTransGUID
                                  into ljAuthBuyerRelate2
                              from authorizedPersonTransBuyer2 in ljAuthBuyerRelate2.DefaultIfEmpty()
                              join relatedPersonTable2 in db.Set<RelatedPersonTable>() on authorizedPersonTransBuyer2.RelatedPersonTableGUID equals
                                relatedPersonTable2.RelatedPersonTableGUID into authRelateTable2
                              from relatedPersonTable2 in authRelateTable2.DefaultIfEmpty()

                              join authorizedPersonTransBuyer3 in db.Set<AuthorizedPersonTrans>() on agreementTableInfo
                                      .AuthorizedPersonTransBuyer3GUID equals authorizedPersonTransBuyer3.AuthorizedPersonTransGUID into ljAuthBuyerRelate3
                              from authorizedPersonTransBuyer3 in ljAuthBuyerRelate3.DefaultIfEmpty()
                              join relatedPersonTable3 in db.Set<RelatedPersonTable>() on authorizedPersonTransBuyer3.RelatedPersonTableGUID equals
                               relatedPersonTable3.RelatedPersonTableGUID into authRelateTable3
                              from relatedPersonTable3 in authRelateTable3.DefaultIfEmpty()

                              join authorizedPersonTransCustomer1 in db.Set<AuthorizedPersonTrans>() on agreementTableInfo
                                     .AuthorizedPersonTransCustomer1GUID equals authorizedPersonTransCustomer1.AuthorizedPersonTransGUID
                                 into ljAuthCustomerRelate1
                              from authorizedPersonTransCustomer1 in ljAuthCustomerRelate1.DefaultIfEmpty()
                              join relatedPersonTableCustomer1 in db.Set<RelatedPersonTable>() on authorizedPersonTransCustomer1.RelatedPersonTableGUID equals
                                  relatedPersonTableCustomer1.RelatedPersonTableGUID into authRelatePerson1
                              from relatedPersonTableCustomer1 in authRelatePerson1.DefaultIfEmpty()

                              join authorizedPersonTransCustomer2 in db.Set<AuthorizedPersonTrans>() on agreementTableInfo
                                      .AuthorizedPersonTransCustomer2GUID equals authorizedPersonTransCustomer2.AuthorizedPersonTransGUID
                                  into ljAuthCustomerRelate2
                              from authorizedPersonTransCustomer2 in ljAuthCustomerRelate2.DefaultIfEmpty()
                              join relatedPersonTableCustomer2 in db.Set<RelatedPersonTable>() on authorizedPersonTransCustomer2.RelatedPersonTableGUID equals
                                  relatedPersonTableCustomer2.RelatedPersonTableGUID into authRelatePerson2
                              from relatedPersonTableCustomer2 in authRelatePerson2.DefaultIfEmpty()


                              join authorizedPersonTransCustomer3 in db.Set<AuthorizedPersonTrans>() on agreementTableInfo
                                      .AuthorizedPersonTransCustomer3GUID equals authorizedPersonTransCustomer3.AuthorizedPersonTransGUID
                                  into ljAuthCustomerRelate3
                              from authorizedPersonTransCustomer3 in ljAuthCustomerRelate3.DefaultIfEmpty()
                              join relatedPersonTableCustomer3 in db.Set<RelatedPersonTable>() on authorizedPersonTransCustomer3.RelatedPersonTableGUID equals
                                  relatedPersonTableCustomer3.RelatedPersonTableGUID into authRelatePerson3
                              from relatedPersonTableCustomer3 in authRelatePerson3.DefaultIfEmpty()

                              join authorizedPersonTypeCustomer in db.Set<AuthorizedPersonType>() on agreementTableInfo
                                       .AuthorizedPersonTypeCustomerGUID equals authorizedPersonTypeCustomer.AuthorizedPersonTypeGUID
                                   into ljAuthCustomerPersonType
                              from authorizedPersonTypeCustomer in ljAuthCustomerPersonType.DefaultIfEmpty()

                              join authorizedPersonTypeBuyer in db.Set<AuthorizedPersonType>() on agreementTableInfo
                                      .AuthorizedPersonTypeBuyerGUID equals authorizedPersonTypeBuyer.AuthorizedPersonTypeGUID
                                  into ljAuthBuyerPersonType
                              from authorizedPersonTypeBuyer in ljAuthBuyerPersonType.DefaultIfEmpty()

                              join authorizedPersonTypeCompany in db.Set<AuthorizedPersonType>() on agreementTableInfo
                                      .AuthorizedPersonTypeCompanyGUID equals authorizedPersonTypeCompany.AuthorizedPersonTypeGUID
                                  into ljAuthCompanyPersonType
                              from authorizedPersonTypeCompany in ljAuthCompanyPersonType.DefaultIfEmpty()

                              join addressBranch in db.Set<AddressTrans>() on company
                                    .DefaultBranchGUID equals addressBranch.RefGUID
                                  into ljAddressBranchCompany
                              from addressBranch in ljAddressBranchCompany

                              join contractTransFax in db.Set<ContactTrans>().Where(w => w.RefType == (int)RefType.Branch && w.ContactType == (int)ContactType.Fax && w.PrimaryContact)
                                  on company.DefaultBranchGUID equals contractTransFax.RefGUID into ljContractTransFax
                              from contractTransFax in ljContractTransFax.DefaultIfEmpty()

                              join contractTransPhone in db.Set<ContactTrans>().Where(w => w.RefType == (int)RefType.Branch && w.ContactType == (int)ContactType.Phone && w.PrimaryContact)
                                  on company.DefaultBranchGUID equals contractTransPhone.RefGUID into ljContractTransPhone
                              from contractTransPhone in ljContractTransPhone.DefaultIfEmpty()

                              join companyBank in db.Set<CompanyBank>().Where(w => w.Primary)
                              on company.CompanyGUID equals companyBank.CompanyGUID into lCompanyBank
                              from companyBank in lCompanyBank.DefaultIfEmpty()
                              join bankGroup in db.Set<BankGroup>()
                              on companyBank.BankGroupGUID equals bankGroup.BankGroupGUID into ljBankGroup
                              from bankGroup in ljBankGroup.DefaultIfEmpty()

                              join bankType in db.Set<BankType>()
                              on companyBank.BankTypeGUID equals bankType.BankTypeGUID into ljBankType
                              from bankType in ljBankType.DefaultIfEmpty()


                              join jointVentureTrans1 in db.Set<JointVentureTrans>().Where(w => w.Ordering == 1)
                              on mainAgreementTable.MainAgreementTableGUID equals jointVentureTrans1.RefGUID into ljjointVentureTrans1
                              from jointVentureTrans1 in ljjointVentureTrans1.DefaultIfEmpty()
                              join authorizedPersonType1 in db.Set<AuthorizedPersonType>()
                              on jointVentureTrans1.AuthorizedPersonTypeGUID equals authorizedPersonType1.AuthorizedPersonTypeGUID
                              into ljauthType1
                              from authorizedPersonType1 in ljauthType1.DefaultIfEmpty()

                              join jointVentureTrans2 in db.Set<JointVentureTrans>().Where(w => w.Ordering == 2)
                              on jointVentureTrans1.RefGUID equals jointVentureTrans2.RefGUID into ljjointVentureTrans2
                              from jointVentureTrans2 in ljjointVentureTrans2.DefaultIfEmpty()
                              join authorizedPersonType2 in db.Set<AuthorizedPersonType>()
                              on jointVentureTrans2.AuthorizedPersonTypeGUID equals authorizedPersonType2.AuthorizedPersonTypeGUID
                              into ljauthType2
                              from authorizedPersonType2 in ljauthType2.DefaultIfEmpty()

                              join jointVentureTrans3 in db.Set<JointVentureTrans>().Where(w => w.Ordering == 3)
                              on jointVentureTrans1.RefGUID equals jointVentureTrans3.RefGUID into ljjointVentureTrans3
                              from jointVentureTrans3 in ljjointVentureTrans3.DefaultIfEmpty()
                              join authorizedPersonType3 in db.Set<AuthorizedPersonType>()
                              on jointVentureTrans3.AuthorizedPersonTypeGUID equals authorizedPersonType3.AuthorizedPersonTypeGUID
                              into ljauthType3
                              from authorizedPersonType3 in ljauthType3.DefaultIfEmpty()

                              join jointVentureTrans4 in db.Set<JointVentureTrans>().Where(w => w.Ordering == 4)
                              on jointVentureTrans1.RefGUID equals jointVentureTrans4.RefGUID into ljjointVentureTrans4
                              from jointVentureTrans4 in ljjointVentureTrans4.DefaultIfEmpty()
                              join authorizedPersonType4 in db.Set<AuthorizedPersonType>()
                              on jointVentureTrans4.AuthorizedPersonTypeGUID equals authorizedPersonType4.AuthorizedPersonTypeGUID
                              into ljauthType4
                              from authorizedPersonType4 in ljauthType4.DefaultIfEmpty()

                              where mainAgreementTable.MainAgreementTableGUID == refGUID
                              select new GenMainAgreementBookmarkSharedJVView
                              {
                                  CompanyName1 = company.Name,
                                  CompanyName2 = company.Name,
                                  CompanyName3 = company.Name,
                                  CompanyName4 = company.Name,
                                  CompanyName5 = company.Name,
                                  CompanyName6 = company.Name,
                                  CompanyName7 = company.Name,
                                  ContAmountFirst1 = mainAgreementTable.ApprovedCreditLimit,
                                  ContAmountSecond1 = null,
                                  AgreementDate = mainAgreementTable.AgreementDate,
                                  AgreementDate1 = null,
                                  AgreementDate2 = null,
                                  AgreementDate3 = null,
                                  AgreementDate4 = null,
                                  AgreementDate5 = null,
                                  AgreementNo1 = mainAgreementTable.MainAgreementId,
                                  AgreementNo2 = mainAgreementTable.MainAgreementId,
                                  AgreementNo3 = mainAgreementTable.MainAgreementId,
                                  AgreementNo4 = mainAgreementTable.MainAgreementId,
                                  AgreementNo5 = mainAgreementTable.MainAgreementId,
                                  InterestRate1 = mainAgreementTable.TotalInterestPct,
                                  AliasNameCust1 = customer.AltName,
                                  AliasNameCust2 = customer.AltName,
                                  AliasNameCust3 = customer.AltName,
                                  AliasNameCust4 = customer.AltName,
                                  AliasNameCust5 = customer.AltName,
                                  AliasNameCust6 = customer.AltName,
                                  AliasNameCust7 = customer.AltName,
                                  AliasNameCust8 = customer.AltName,
                                  CustName1 = customer.Name,
                                  CustName2 = customer.Name,
                                  CustName3 = customer.Name,
                                  CustName4 = customer.Name,
                                  CustName5 = customer.Name,
                                  CustName6 = customer.Name,
                                  CustName7 = customer.Name,
                                  CustName8 = customer.Name,
                                  CustTaxID1 = customer.TaxID,
                                  CompanyWitnessFirst1 = agreementTableInfo.WitnessCustomer1,
                                  CompanyWitnessSecond1 = agreementTableInfo.WitnessCustomer2,
                                  Text1 = agreementTableInfoText.Text1,
                                  Text2 = agreementTableInfoText.Text2,
                                  Text3 = agreementTableInfoText.Text3,
                                  Text4 = agreementTableInfoText.Text4,
                                  AuthorityPersonFirst1 = employeeTable1.Name,
                                  AuthorityPersonFirst2 = employeeTable1.Name,
                                  AuthorityPersonSecond1 = employeeTable2.Name,
                                  AuthorityPersonSecond2 = employeeTable2.Name,
                                  AuthorityPersonThird1 = employeeTable3.Name,
                                  AuthorityPersonThird2 = employeeTable3.Name,
                                  AuthorizedBuyerFirst1 = relatedPersonTable1.Name,
                                  AuthorizedBuyerFirst2 = relatedPersonTable1.Name,
                                  AuthorizedBuyerSecond1 = relatedPersonTable2.Name,
                                  AuthorizedBuyerSecond2 = relatedPersonTable2.Name,
                                  AuthorizedBuyerThird1 = relatedPersonTable3.Name,
                                  AuthorizedBuyerThird2 = relatedPersonTable3.Name,
                                  AuthorizedCustFirst1 = relatedPersonTableCustomer1.Name,
                                  AuthorizedCustFirst2 = relatedPersonTableCustomer1.Name,
                                  AuthorizedCustSecond1 = relatedPersonTableCustomer2.Name,
                                  AuthorizedCustSecond2 = relatedPersonTableCustomer2.Name,
                                  AuthorizedCustThird1 = relatedPersonTableCustomer3.Name,
                                  AuthorizedCustThird2 = relatedPersonTableCustomer3.Name,
                                  CustWitnessFirst1 = agreementTableInfo.WitnessCustomer1,
                                  CustWitnessSecond1 = agreementTableInfo.WitnessCustomer2,
                                  PositionBuyer1 = authorizedPersonTypeBuyer.Description,
                                  PositionCust1 = authorizedPersonTypeCustomer.Description,
                                  PositionCust2 = authorizedPersonTypeCustomer.Description,
                                  PositionCust3 = authorizedPersonTypeCustomer.Description,
                                  PositionLIT1 = authorizedPersonTypeCompany.Description,
                                  PositionLIT2 = authorizedPersonTypeCompany.Description,
                                  PositionLIT3 = authorizedPersonTypeCompany.Description,
                                  CustAddress1 = agreementTableInfo.CustomerAddress1,
                                  CustAddress2 = agreementTableInfo.CustomerAddress1,
                                  CompanyAddress1 = addressBranch.Address1 + " " + addressBranch.Address2,
                                  CompanyAddress2 = addressBranch.Address1 + " " + addressBranch.Address2,
                                  TelNumber1 = contractTransPhone.ContactValue,
                                  FaxNumber1 = contractTransFax.ContactValue,
                                  FaxNumber2 = contractTransFax.ContactValue,
                                  CompanyBankBranch1 = companyBank.BankBranch,
                                  CompanyBankBranch2 = companyBank.BankBranch,
                                  CompanyBankName1 = bankGroup.Description,
                                  CompanyBankName2 = bankGroup.Description,
                                  CompanyBankNo1 = companyBank.AccountNumber,
                                  CompanyBankNo2 = companyBank.AccountNumber,
                                  CompanyBankType1 = bankType.Description,
                                  CompanyBankType2 = bankType.Description,
                                  ManualNo1 = null,

                                  JointAddressFirst1 = jointVentureTrans1.Address,
                                  JointNameFirst1 = jointVentureTrans1.Name,
                                  JointAddressSecond1 = jointVentureTrans2.Address,
                                  JointNameSecond1 = jointVentureTrans2.Name,
                                  JointAddressThird1 = jointVentureTrans3.Address,
                                  JointNameThird1 = jointVentureTrans3.Name,
                                  JointAddressFourth1 = jointVentureTrans4.Address,
                                  JointNameFourth1 = jointVentureTrans4.Name,
                                  PositionJointFirst1 = authorizedPersonType1.AuthorizedPersonTypeId,
                                  PositionJointSecond1 = authorizedPersonType2.AuthorizedPersonTypeId,
                                  PositionJointThird1 = authorizedPersonType3.AuthorizedPersonTypeId,
                                  PositionJointFourth1 = authorizedPersonType4.AuthorizedPersonTypeId,
                                  JointAddressFirst2 = jointVentureTrans1.Address,
                                  JointNameFirst2 = jointVentureTrans1.Name,
                                  JointAddressSecond2 = jointVentureTrans2.Address,
                                  JointNameSecond2 = jointVentureTrans2.Name,
                                  JointAddressThird2 = jointVentureTrans3.Address,
                                  JointNameThird2 = jointVentureTrans3.Name,
                                  JointAddressFourth2 = jointVentureTrans4.Address,
                                  JointNameFourth2 = jointVentureTrans4.Name,
                                  PositionJointFirst2 = authorizedPersonType1.AuthorizedPersonTypeId,
                                  PositionJointSecond2 = authorizedPersonType2.AuthorizedPersonTypeId,
                                  PositionJointThird2 = authorizedPersonType3.AuthorizedPersonTypeId,
                                  PositionJointFourth2 = authorizedPersonType4.AuthorizedPersonTypeId,
                                  JointAddressFirst3 = jointVentureTrans1.Address,
                                  JointNameFirst3 = jointVentureTrans1.Name,
                                  JointAddressSecond3 = jointVentureTrans2.Address,
                                  JointNameSecond3 = jointVentureTrans2.Name,
                                  JointAddressThird3 = jointVentureTrans3.Address,
                                  JointNameThird3 = jointVentureTrans3.Name,
                                  JointAddressFourth3 = jointVentureTrans4.Address,
                                  JointNameFourth3 = jointVentureTrans4.Name,
                                  PositionJointFirst3 = authorizedPersonType1.AuthorizedPersonTypeId,
                                  PositionJointSecond3 = authorizedPersonType2.AuthorizedPersonTypeId,
                                  PositionJointThird3 = authorizedPersonType3.AuthorizedPersonTypeId,
                                  PositionJointFourth3 = authorizedPersonType4.AuthorizedPersonTypeId,
                                  JointAddressFirst4 = jointVentureTrans1.Address,
                                  JointNameFirst4 = jointVentureTrans1.Name,
                                  JointAddressSecond4 = jointVentureTrans2.Address,
                                  JointNameSecond4 = jointVentureTrans2.Name,
                                  JointAddressThird4 = jointVentureTrans3.Address,
                                  JointNameThird4 = jointVentureTrans3.Name,
                                  JointAddressFourth4 = jointVentureTrans4.Address,
                                  JointNameFourth4 = jointVentureTrans4.Name,
                                  PositionJointFirst4 = authorizedPersonType1.AuthorizedPersonTypeId,
                                  PositionJointSecond4 = authorizedPersonType2.AuthorizedPersonTypeId,
                                  PositionJointThird4 = authorizedPersonType3.AuthorizedPersonTypeId,
                                  PositionJointFourth4 = authorizedPersonType4.AuthorizedPersonTypeId,
                                  TextJVDetail1 = jointVentureTrans1.Remark,
                              }).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }

        }
        #endregion

        #region Bookmark PF
        private List<MainAgreementPFBookmarkViewMap> GetQueryWithdrawalLine(Guid refGuid, int lineNum)
        {
            return (from mainAgreementTable in Entity
                    join withdrawalTable in db.Set<WithdrawalTable>()
                    on mainAgreementTable.WithdrawalTableGUID equals withdrawalTable.WithdrawalTableGUID
                    into ljwithdrawalTable
                    from withdrawalTable in ljwithdrawalTable.DefaultIfEmpty()
                        //qWithdrawalLine1
                    join withdrawalLine in db.Set<WithdrawalLine>()
                    on new { withdrawalTable.WithdrawalTableGUID, WithdrawalLineType = (int)WithdrawalLineType.Interest, LineNum = lineNum }
                    equals new { withdrawalLine.WithdrawalTableGUID, withdrawalLine.WithdrawalLineType, withdrawalLine.LineNum }
                    into ljwithdrawalLine
                    from withdrawalLine in ljwithdrawalLine.DefaultIfEmpty()
                    join chequeTable in db.Set<ChequeTable>()
                    on withdrawalLine.CustomerPDCTableGUID equals chequeTable.RefGUID
                    into ljchequeTable
                    from chequeTable in ljchequeTable.DefaultIfEmpty()
                    where mainAgreementTable.MainAgreementTableGUID == refGuid
                    select new MainAgreementPFBookmarkViewMap
                    {
                        MainAgreementTableGUID = mainAgreementTable.MainAgreementTableGUID,
                        ChequeNo = chequeTable.ChequeNo,
                        ChequeDate = chequeTable.ChequeDate,
                        InterestAmount = withdrawalLine.InterestAmount
                    }).AsNoTracking().ToList();
        }
        public MainAgreementPFBookmarkViewMap GetMainAgreementPFBookmarkValue(Guid refGuid)
        {
            try
            {

                #region mainAgreementPFBookmarkView



                MainAgreementPFBookmarkViewMap qMainAgreementTable = (from mainAgreementTable in Entity
                                                                      join agreementTableInfo in db.Set<AgreementTableInfo>()
                                                                      on new { mainAgreementTable.MainAgreementTableGUID, RefType = (int)RefType.MainAgreement }
                                                                      equals new { MainAgreementTableGUID = agreementTableInfo.RefGUID, agreementTableInfo.RefType }
                                                                      into ljagreementTableInfo
                                                                      from agreementTableInfo in ljagreementTableInfo.DefaultIfEmpty()
                                                                          //qWithdrawalTable 
                                                                      join creditLimitType in db.Set<CreditLimitType>()
                                                                      on mainAgreementTable.CreditLimitTypeGUID equals creditLimitType.CreditLimitTypeGUID
                                                                      into ljcreditLimitType
                                                                      from creditLimitType in ljcreditLimitType.DefaultIfEmpty()
                                                                      where mainAgreementTable.MainAgreementTableGUID == refGuid
                                                                      select new MainAgreementPFBookmarkViewMap
                                                                      {
                                                                          MainAgreementTableGUID = mainAgreementTable.MainAgreementTableGUID,
                                                                          CreditAppTableGUID = mainAgreementTable.CreditAppTableGUID,
                                                                          AgreementYear = mainAgreementTable.AgreementYear.ToString(),
                                                                          Description = mainAgreementTable.Description,
                                                                          CreditLimitTypeGUID = creditLimitType.CreditLimitTypeGUID,
                                                                          GuarantorName = agreementTableInfo.GuarantorName,
                                                                          GuaranteeText = agreementTableInfo.GuaranteeText,
                                                                          CreditLimitType = creditLimitType.Description,
                                                                          StartDate = mainAgreementTable.StartDate,
                                                                          ExpiryDate = mainAgreementTable.ExpiryDate,
                                                                          ApprovedCreditLimit = mainAgreementTable.ApprovedCreditLimit,
                                                                          Revolving = creditLimitType.Revolving,
                                                                          WithdrawalTableGUID = mainAgreementTable.WithdrawalTableGUID
                                                                      }).AsNoTracking().FirstOrDefault();

                MainAgreementPFBookmarkViewMap qWithdrawalTable = (from withdrawalTable in db.Set<WithdrawalTable>()
                                                                   where withdrawalTable.WithdrawalTableGUID == qMainAgreementTable.WithdrawalTableGUID
                                                                   select new MainAgreementPFBookmarkViewMap
                                                                   {
                                                                       MainAgreementTableGUID = qMainAgreementTable.MainAgreementTableGUID,
                                                                       WithdrawalTableGUID = qMainAgreementTable.WithdrawalTableGUID,
                                                                       DueDate = withdrawalTable.DueDate,
                                                                       WithdrawalAmount = withdrawalTable.WithdrawalAmount
                                                                   }).AsNoTracking().FirstOrDefault();
                List<MainAgreementPFBookmarkViewMap> qWithdrawalLine = new List<MainAgreementPFBookmarkViewMap>();
                decimal qSumWithdrawalLine = 0;
                if (qWithdrawalTable != null)
                {
                    qWithdrawalLine = (from withdrawalLine in db.Set<WithdrawalLine>()
                                       join chequeTable in db.Set<ChequeTable>() on withdrawalLine.CustomerPDCTableGUID equals chequeTable.ChequeTableGUID
                                       into ljChequeTable
                                       from chequeTable in ljChequeTable.DefaultIfEmpty()
                                       where withdrawalLine.WithdrawalTableGUID == qWithdrawalTable.WithdrawalTableGUID 
                                          && withdrawalLine.WithdrawalLineType == (int)WithdrawalLineType.Interest
                                       orderby withdrawalLine.LineNum
                                       select new MainAgreementPFBookmarkViewMap
                                       {
                                           MainAgreementTableGUID = qWithdrawalTable.MainAgreementTableGUID,
                                           ChequeNo = chequeTable.ChequeNo,
                                           ChequeDate = chequeTable.ChequeDate,
                                           InterestAmount = withdrawalLine.InterestAmount,
                                           LineNum = withdrawalLine.LineNum
                                       }).AsNoTracking().ToList();

                    qWithdrawalLine = qWithdrawalLine.Select((o, i) =>
                    {
                        o.RowNumber = i + 1;
                        return o;
                    }).ToList();

                    qSumWithdrawalLine = (from withdrawalLine in db.Set<WithdrawalLine>()
                                          where withdrawalLine.WithdrawalTableGUID == qWithdrawalTable.WithdrawalTableGUID.Value
                                          && withdrawalLine.WithdrawalLineType == (int)WithdrawalLineType.Interest
                                          select withdrawalLine).Sum(s => s.InterestAmount);
                }

                MainAgreementPFBookmarkViewMap qWithdrawalLine1 = qWithdrawalLine.Find(w => w.RowNumber == 1);
                MainAgreementPFBookmarkViewMap qWithdrawalLine2 = qWithdrawalLine.Find(w => w.RowNumber == 2);
                MainAgreementPFBookmarkViewMap qWithdrawalLine3 = qWithdrawalLine.Find(w => w.RowNumber == 3);
                MainAgreementPFBookmarkViewMap qWithdrawalLine4 = qWithdrawalLine.Find(w => w.RowNumber == 4);
                MainAgreementPFBookmarkViewMap qWithdrawalLine5 = qWithdrawalLine.Find(w => w.RowNumber == 5);
                MainAgreementPFBookmarkViewMap qWithdrawalLine6 = qWithdrawalLine.Find(w => w.RowNumber == 6);
                MainAgreementPFBookmarkViewMap qWithdrawalLine7 = qWithdrawalLine.Find(w => w.RowNumber == 7);
                MainAgreementPFBookmarkViewMap qWithdrawalLine8 = qWithdrawalLine.Find(w => w.RowNumber == 8);
                MainAgreementPFBookmarkViewMap qWithdrawalLine9 = qWithdrawalLine.Find(w => w.RowNumber == 9);
                MainAgreementPFBookmarkViewMap qWithdrawalLine10 = qWithdrawalLine.Find(w => w.RowNumber == 10);
                MainAgreementPFBookmarkViewMap qWithdrawalLine11 = qWithdrawalLine.Find(w => w.RowNumber == 11);
                MainAgreementPFBookmarkViewMap qWithdrawalLine12 = qWithdrawalLine.Find(w => w.RowNumber == 12);
                MainAgreementPFBookmarkViewMap qWithdrawalLine13 = qWithdrawalLine.Find(w => w.RowNumber == 13);
                MainAgreementPFBookmarkViewMap qWithdrawalLine14 = qWithdrawalLine.Find(w => w.RowNumber == 14);
                MainAgreementPFBookmarkViewMap qWithdrawalLine15 = qWithdrawalLine.Find(w => w.RowNumber == 15);
                MainAgreementPFBookmarkViewMap qWithdrawalLine16 = qWithdrawalLine.Find(w => w.RowNumber == 16);
                MainAgreementPFBookmarkViewMap qWithdrawalLine17 = qWithdrawalLine.Find(w => w.RowNumber == 17);
                MainAgreementPFBookmarkViewMap qWithdrawalLine18 = qWithdrawalLine.Find(w => w.RowNumber == 18);
                MainAgreementPFBookmarkViewMap qWithdrawalLine19 = qWithdrawalLine.Find(w => w.RowNumber == 19);
                MainAgreementPFBookmarkViewMap qWithdrawalLine20 = qWithdrawalLine.Find(w => w.RowNumber == 20);
                MainAgreementPFBookmarkViewMap qWithdrawalLine21 = qWithdrawalLine.Find(w => w.RowNumber == 21);
                MainAgreementPFBookmarkViewMap qWithdrawalLine22 = qWithdrawalLine.Find(w => w.RowNumber == 22);
                MainAgreementPFBookmarkViewMap qWithdrawalLine23 = qWithdrawalLine.Find(w => w.RowNumber == 23);
                MainAgreementPFBookmarkViewMap qWithdrawalLine24 = qWithdrawalLine.Find(w => w.RowNumber == 24);

                //qMainAgreementTable 

                MainAgreementPFBookmarkViewMap mainAgreementPFBookmarkView = new MainAgreementPFBookmarkViewMap
                {
                    CreditLimitType = qMainAgreementTable.CreditLimitType,
                    Description = qMainAgreementTable.Description,
                    AgreementYear = qMainAgreementTable.AgreementYear,
                    StartDate = qMainAgreementTable.StartDate,
                    ExpiryDate = qMainAgreementTable.ExpiryDate,
                    WithdrawalAmount = qWithdrawalTable != null ? qWithdrawalTable.WithdrawalAmount : 0,
                    ApprovedCreditLimit = qMainAgreementTable.ApprovedCreditLimit,
                    DueDate = qWithdrawalTable != null ? qWithdrawalTable.DueDate : null,
                    GuarantorName = qMainAgreementTable.GuarantorName,
                    GuaranteeText = qMainAgreementTable.GuaranteeText,
                    ChequeNo1 = qWithdrawalLine1 != null ? qWithdrawalLine1.ChequeNo : "",
                    ChequeNo2 = qWithdrawalLine2 != null ? qWithdrawalLine2.ChequeNo : "",
                    ChequeNo3 = qWithdrawalLine3 != null ? qWithdrawalLine3.ChequeNo : "",
                    ChequeNo4 = qWithdrawalLine4 != null ? qWithdrawalLine4.ChequeNo : "",
                    ChequeNo5 = qWithdrawalLine5 != null ? qWithdrawalLine5.ChequeNo : "",
                    ChequeNo6 = qWithdrawalLine6 != null ? qWithdrawalLine6.ChequeNo : "",
                    ChequeNo7 = qWithdrawalLine7 != null ? qWithdrawalLine7.ChequeNo : "",
                    ChequeNo8 = qWithdrawalLine8 != null ? qWithdrawalLine8.ChequeNo : "",
                    ChequeNo9 = qWithdrawalLine9 != null ? qWithdrawalLine9.ChequeNo : "",
                    ChequeNo10 = qWithdrawalLine10 != null ? qWithdrawalLine10.ChequeNo : "",
                    ChequeNo11 = qWithdrawalLine11 != null ? qWithdrawalLine11.ChequeNo : "",
                    ChequeNo12 = qWithdrawalLine12 != null ? qWithdrawalLine12.ChequeNo : "",
                    ChequeNo13 = qWithdrawalLine13 != null ? qWithdrawalLine13.ChequeNo : "",
                    ChequeNo14 = qWithdrawalLine14 != null ? qWithdrawalLine14.ChequeNo : "",
                    ChequeNo15 = qWithdrawalLine15 != null ? qWithdrawalLine15.ChequeNo : "",
                    ChequeNo16 = qWithdrawalLine16 != null ? qWithdrawalLine16.ChequeNo : "",
                    ChequeNo17 = qWithdrawalLine17 != null ? qWithdrawalLine17.ChequeNo : "",
                    ChequeNo18 = qWithdrawalLine18 != null ? qWithdrawalLine18.ChequeNo : "",
                    ChequeNo19 = qWithdrawalLine19 != null ? qWithdrawalLine19.ChequeNo : "",
                    ChequeNo20 = qWithdrawalLine20 != null ? qWithdrawalLine20.ChequeNo : "",
                    ChequeNo21 = qWithdrawalLine21 != null ? qWithdrawalLine21.ChequeNo : "",
                    ChequeNo22 = qWithdrawalLine22 != null ? qWithdrawalLine22.ChequeNo : "",
                    ChequeNo23 = qWithdrawalLine23 != null ? qWithdrawalLine23.ChequeNo : "",
                    ChequeNo24 = qWithdrawalLine24 != null ? qWithdrawalLine24.ChequeNo : "",
                    ChequeDate1 = qWithdrawalLine1 != null ? qWithdrawalLine1.ChequeDate : null,
                    ChequeDate2 = qWithdrawalLine2 != null ? qWithdrawalLine2.ChequeDate : null,
                    ChequeDate3 = qWithdrawalLine3 != null ? qWithdrawalLine3.ChequeDate : null,
                    ChequeDate4 = qWithdrawalLine4 != null ? qWithdrawalLine4.ChequeDate : null,
                    ChequeDate5 = qWithdrawalLine5 != null ? qWithdrawalLine5.ChequeDate : null,
                    ChequeDate6 = qWithdrawalLine6 != null ? qWithdrawalLine6.ChequeDate : null,
                    ChequeDate7 = qWithdrawalLine7 != null ? qWithdrawalLine7.ChequeDate : null,
                    ChequeDate8 = qWithdrawalLine8 != null ? qWithdrawalLine8.ChequeDate : null,
                    ChequeDate9 = qWithdrawalLine9 != null ? qWithdrawalLine9.ChequeDate : null,
                    ChequeDate10 = qWithdrawalLine10 != null ? qWithdrawalLine10.ChequeDate : null,
                    ChequeDate11 = qWithdrawalLine11 != null ? qWithdrawalLine11.ChequeDate : null,
                    ChequeDate12 = qWithdrawalLine12 != null ? qWithdrawalLine12.ChequeDate : null,
                    ChequeDate13 = qWithdrawalLine13 != null ? qWithdrawalLine13.ChequeDate : null,
                    ChequeDate14 = qWithdrawalLine14 != null ? qWithdrawalLine14.ChequeDate : null,
                    ChequeDate15 = qWithdrawalLine15 != null ? qWithdrawalLine15.ChequeDate : null,
                    ChequeDate16 = qWithdrawalLine16 != null ? qWithdrawalLine16.ChequeDate : null,
                    ChequeDate17 = qWithdrawalLine17 != null ? qWithdrawalLine17.ChequeDate : null,
                    ChequeDate18 = qWithdrawalLine18 != null ? qWithdrawalLine18.ChequeDate : null,
                    ChequeDate19 = qWithdrawalLine19 != null ? qWithdrawalLine19.ChequeDate : null,
                    ChequeDate20 = qWithdrawalLine20 != null ? qWithdrawalLine20.ChequeDate : null,
                    ChequeDate21 = qWithdrawalLine21 != null ? qWithdrawalLine21.ChequeDate : null,
                    ChequeDate22 = qWithdrawalLine22 != null ? qWithdrawalLine22.ChequeDate : null,
                    ChequeDate23 = qWithdrawalLine23 != null ? qWithdrawalLine23.ChequeDate : null,
                    ChequeDate24 = qWithdrawalLine24 != null ? qWithdrawalLine24.ChequeDate : null,
                    InterestAmount1 = qWithdrawalLine1 != null ? qWithdrawalLine1.InterestAmount : 0,
                    InterestAmount2 = qWithdrawalLine2 != null ? qWithdrawalLine2.InterestAmount : 0,
                    InterestAmount3 = qWithdrawalLine3 != null ? qWithdrawalLine3.InterestAmount : 0,
                    InterestAmount4 = qWithdrawalLine4 != null ? qWithdrawalLine4.InterestAmount : 0,
                    InterestAmount5 = qWithdrawalLine5 != null ? qWithdrawalLine5.InterestAmount : 0,
                    InterestAmount6 = qWithdrawalLine6 != null ? qWithdrawalLine6.InterestAmount : 0,
                    InterestAmount7 = qWithdrawalLine7 != null ? qWithdrawalLine7.InterestAmount : 0,
                    InterestAmount8 = qWithdrawalLine8 != null ? qWithdrawalLine8.InterestAmount : 0,
                    InterestAmount9 = qWithdrawalLine9 != null ? qWithdrawalLine9.InterestAmount : 0,
                    InterestAmount10 = qWithdrawalLine10 != null ? qWithdrawalLine10.InterestAmount : 0,
                    InterestAmount11 = qWithdrawalLine11 != null ? qWithdrawalLine11.InterestAmount : 0,
                    InterestAmount12 = qWithdrawalLine12 != null ? qWithdrawalLine12.InterestAmount : 0,
                    InterestAmount13 = qWithdrawalLine13 != null ? qWithdrawalLine13.InterestAmount : 0,
                    InterestAmount14 = qWithdrawalLine14 != null ? qWithdrawalLine14.InterestAmount : 0,
                    InterestAmount15 = qWithdrawalLine15 != null ? qWithdrawalLine15.InterestAmount : 0,
                    InterestAmount16 = qWithdrawalLine16 != null ? qWithdrawalLine16.InterestAmount : 0,
                    InterestAmount17 = qWithdrawalLine17 != null ? qWithdrawalLine17.InterestAmount : 0,
                    InterestAmount18 = qWithdrawalLine18 != null ? qWithdrawalLine18.InterestAmount : 0,
                    InterestAmount19 = qWithdrawalLine19 != null ? qWithdrawalLine19.InterestAmount : 0,
                    InterestAmount20 = qWithdrawalLine20 != null ? qWithdrawalLine20.InterestAmount : 0,
                    InterestAmount21 = qWithdrawalLine21 != null ? qWithdrawalLine21.InterestAmount : 0,
                    InterestAmount22 = qWithdrawalLine22 != null ? qWithdrawalLine22.InterestAmount : 0,
                    InterestAmount23 = qWithdrawalLine23 != null ? qWithdrawalLine23.InterestAmount : 0,
                    InterestAmount24 = qWithdrawalLine24 != null ? qWithdrawalLine24.InterestAmount : 0,
                    MainAgmStartDate = qMainAgreementTable.StartDate,
                    MainAgmExpiryDate = qMainAgreementTable.ExpiryDate,
                    WithdrawalDueDate = qWithdrawalTable != null ? qWithdrawalTable.DueDate : null,
                    THSumInterestAmount = qSumWithdrawalLine,
                    Revolving = qMainAgreementTable.Revolving
                };
                return mainAgreementPFBookmarkView;

                #endregion

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }
        #endregion
        #region MainAgreementExpenceDetail

        public GenMainAgreementExpenceDetailView MainAgreementExpenceDetailView(Guid refGUID)
        {
            try
            {
                GenMainAgreementExpenceDetailView result = (from mainAgreementTable in Entity
                              join creditAppTable in db.Set<CreditAppTable>()
                                  on mainAgreementTable.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID into lCreditApptableMainAgreementTable
                              from creditAppTable in lCreditApptableMainAgreementTable.DefaultIfEmpty()
                              join creditTerm in db.Set<CreditTerm>()
                                  on creditAppTable.CreditTermGUID equals creditTerm.CreditTermGUID into ljCreditApptableCreditTerm
                              from creditTerm in ljCreditApptableCreditTerm.DefaultIfEmpty()

                              join creditAppLine in db.Set<CreditAppLine>()
                              on creditAppTable.CreditAppTableGUID equals creditAppLine.CreditAppTableGUID into ljCreditAppLineTable
                              from creditAppLine in ljCreditAppLineTable.DefaultIfEmpty()

                              join companyParameter in db.Set<CompanyParameter>()
                                    on mainAgreementTable.CompanyGUID equals companyParameter.CompanyGUID into ljCompanyParameterMainAgreementTable
                              from companyParameter in ljCompanyParameterMainAgreementTable.DefaultIfEmpty()
                              join creditTerm2 in db.Set<CreditTerm>()
                                  on companyParameter.PFExtendCreditTermGUID equals creditTerm2.CreditTermGUID
                                  into ljCreditTerm2
                              from creditTerm2 in ljCreditTerm2.DefaultIfEmpty()                

                              join consortiumTable in db.Set<ConsortiumTable>()
                                 on mainAgreementTable.ConsortiumTableGUID equals consortiumTable.ConsortiumTableGUID
                                 into ljConsortiumTable
                              from consortiumTable in ljConsortiumTable.DefaultIfEmpty()

                              join agreementTableInfo in db.Set<AgreementTableInfo>().Where(w => w.RefType == (int)RefType.MainAgreement)
                               on mainAgreementTable.MainAgreementTableGUID equals agreementTableInfo.RefGUID
                               into ljAgreementInfo
                              from agreementTableInfo in ljAgreementInfo

                              join employeeTable1 in db.Set<EmployeeTable>()
                                 on agreementTableInfo.AuthorizedPersonTransCompany1GUID equals employeeTable1.EmployeeTableGUID into ljEmployeeTable1
                              from employeeTable1 in ljEmployeeTable1.DefaultIfEmpty()

                              join employeeTable2 in db.Set<EmployeeTable>()
                               on agreementTableInfo.AuthorizedPersonTransCompany2GUID equals employeeTable2.EmployeeTableGUID into ljEmployeeTable2
                              from employeeTable2 in ljEmployeeTable2.DefaultIfEmpty()

                              join employeeTable3 in db.Set<EmployeeTable>()
                                on agreementTableInfo.AuthorizedPersonTransCompany3GUID equals employeeTable3.EmployeeTableGUID into ljEmployeeTable3
                              from employeeTable3 in ljEmployeeTable3.DefaultIfEmpty()

                                                            join authorizedPersonTrans1 in db.Set<AuthorizedPersonTrans>()
                                                                                             on agreementTableInfo.AuthorizedPersonTransCustomer1GUID equals authorizedPersonTrans1.AuthorizedPersonTransGUID into ljAuthTrans1
                                                            from authorizedPersonTrans1 in ljAuthTrans1.DefaultIfEmpty()
                                                            join relatedPersonTable1 in db.Set<RelatedPersonTable>()
                                                            on authorizedPersonTrans1.RelatedPersonTableGUID equals relatedPersonTable1.RelatedPersonTableGUID into ljRelatePerson1
                                                            from relatedPersonTable1 in ljRelatePerson1.DefaultIfEmpty()

                                                            join authorizedPersonTrans2 in db.Set<AuthorizedPersonTrans>()
                                                            on agreementTableInfo.AuthorizedPersonTransCustomer2GUID equals authorizedPersonTrans2.AuthorizedPersonTransGUID into ljAuthTrans2
                                                            from authorizedPersonTrans2 in ljAuthTrans2.DefaultIfEmpty()
                                                            join relatedPersonTable2 in db.Set<RelatedPersonTable>()
                                                            on authorizedPersonTrans2.RelatedPersonTableGUID equals relatedPersonTable2.RelatedPersonTableGUID into ljRelatePerson2
                                                            from relatedPersonTable2 in ljRelatePerson2.DefaultIfEmpty()

                                                            join authorizedPersonTrans3 in db.Set<AuthorizedPersonTrans>()
                                                            on agreementTableInfo.AuthorizedPersonTransCustomer3GUID equals authorizedPersonTrans3.AuthorizedPersonTransGUID into ljAuthTrans3
                                                            from authorizedPersonTrans3 in ljAuthTrans3.DefaultIfEmpty()
                                                            join relatedPersonTable3 in db.Set<RelatedPersonTable>()
                                                            on authorizedPersonTrans3.RelatedPersonTableGUID equals relatedPersonTable3.RelatedPersonTableGUID into ljRelatePerson3
                                                            from relatedPersonTable3 in ljRelatePerson3.DefaultIfEmpty()


                                                            join customerTable in db.Set<CustomerTable>()
                              on mainAgreementTable.CustomerTableGUID equals customerTable.CustomerTableGUID
                              join employeeTable in db.Set<EmployeeTable>()
                              on customerTable.ResponsibleByGUID equals employeeTable.EmployeeTableGUID into ljResp
                              from employeeTable in ljResp.DefaultIfEmpty()

                              join withdrawalTable in db.Set<WithdrawalTable>()
                              on mainAgreementTable.WithdrawalTableGUID equals withdrawalTable.WithdrawalTableGUID into ljWithdrawalTable
                              from withdrawalTable in ljWithdrawalTable.DefaultIfEmpty()

                              join buyerTable in db.Set<BuyerTable>()
                              on withdrawalTable.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljBuyerTable
                              from buyerTable in ljBuyerTable.DefaultIfEmpty()

                               where mainAgreementTable.MainAgreementTableGUID == refGUID
                              select new GenMainAgreementExpenceDetailView
                              {
                                  AuthorizedCustFirst1 = relatedPersonTable1 != null ? relatedPersonTable1.Name : "",
                                  AuthorizedCustFirst2 = relatedPersonTable1 != null ? relatedPersonTable1.Name : "",
                                  AuthorizedCustSecond1 = relatedPersonTable2 != null ? relatedPersonTable2.Name : "",
                                  AuthorizedCustSecond2 = relatedPersonTable2 != null ? relatedPersonTable2.Name : "",
                                  AuthorizedCustThird1 = relatedPersonTable3 != null ? relatedPersonTable3.Name : "",
                                  AuthorizedCustThird2 = relatedPersonTable3 != null ? relatedPersonTable3.Name : "",
                                  AgreementDate = mainAgreementTable.AgreementDate,
                                  ConsortiumName1 = consortiumTable != null ? consortiumTable.Description : "",
                                  ContAmountFirst1 = 0,
                                  ContAmountVatFirst1 = 0,
                                  ContractNo1 = mainAgreementTable.MainAgreementId,
                                  ContractNo2 = mainAgreementTable.MainAgreementId,
                                  CustName1 = mainAgreementTable.CustomerName,
                                  CustName2 = mainAgreementTable.CustomerName,
                                  CustName3 = mainAgreementTable.CustomerName,
                                  DateOfCreditFirst1 = creditTerm != null ? creditTerm.NumberOfDays.ToString() : "",
                                  DateOfCreditSecond1 = creditTerm2 != null ? creditTerm2.NumberOfDays.ToString() : "",
                                  ExpAmountApproved1 = mainAgreementTable.ApprovedCreditLimit,
                                  ExpAmountApproved2 = mainAgreementTable.ApprovedCreditLimit,
                                  ExpAmountLoan1 = mainAgreementTable.ApprovedCreditLimitLine,
                                  ExpAmountLoan2 = mainAgreementTable.ApprovedCreditLimitLine,
                                  MaketingName1 = employeeTable != null? employeeTable.Name:"",
                                  NameProject1 = buyerTable != null ? buyerTable.BuyerName : "",
                                  NameProject2 = buyerTable != null ? buyerTable.BuyerName : "",
                              }).AsNoTracking().FirstOrDefault();

                result.ConsortiumTrans = (from consortiumTrans in db.Set<ConsortiumTrans>()
                                        where consortiumTrans.RefGUID == refGUID
                                        orderby consortiumTrans.Ordering ascending
                                          select consortiumTrans).AsNoTracking().ToList();
                for (int i = result.ConsortiumTrans.Count; i < 3; i++)
                {
                    result.ConsortiumTrans.Add(new ConsortiumTrans
                    {
                        OperatedBy = "",
                        CustomerName = ""
                    });
                }
                result.ServiceFeeTrans = (from serviceFeeTrans in db.Set<ServiceFeeTrans>()
                                          join invoiceRevenueType in db.Set<InvoiceRevenueType>()
                                          on serviceFeeTrans.InvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID
                                          join intercompany in db.Set<Intercompany>()
                                          on invoiceRevenueType.IntercompanyTableGUID equals intercompany.IntercompanyGUID into ljIntercompany
                                          from intercompany in ljIntercompany.DefaultIfEmpty()
                                          where serviceFeeTrans.RefGUID == refGUID && invoiceRevenueType.IntercompanyTableGUID == null
                                          orderby serviceFeeTrans.Ordering ascending
                                          select serviceFeeTrans).AsNoTracking().ToList();
                for (int i = result.ServiceFeeTrans.Count; i < 10; i++)
                {
                    result.ServiceFeeTrans.Add(new ServiceFeeTrans
                    {
                        Ordering = 0,
                        Description = "",
                        AmountBeforeTax =0,
                        TaxAmount=0,
                        AmountIncludeTax=0,
                        WHTAmount=0
                    });
                }
                result.ServiceFeeTransSM = (from serviceFeeTrans in db.Set<ServiceFeeTrans>()
                          join invoiceRevenueType in db.Set<InvoiceRevenueType>()
                          on serviceFeeTrans.InvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID
                          join intercompany in db.Set<Intercompany>()
                          on invoiceRevenueType.IntercompanyTableGUID equals intercompany.IntercompanyGUID
                          where serviceFeeTrans.RefGUID == refGUID && intercompany.IntercompanyId == TextConstants.InterCompanyId_LISM
                          orderby serviceFeeTrans.Ordering ascending
                          select serviceFeeTrans).AsNoTracking().ToList();
                for (int i = result.ServiceFeeTransSM.Count; i < 10; i++)
                {
                    result.ServiceFeeTransSM.Add(new ServiceFeeTrans
                    {
                        Ordering = 0,
                        Description = "",
                        AmountBeforeTax = 0,
                        TaxAmount = 0,
                        AmountIncludeTax = 0,
                        WHTAmount = 0
                    });
                }
                result.AuthorizedConsCustFirst1 = result.ConsortiumTrans[0].OperatedBy;
                result.AuthorizedConsCustFirst2 = result.ConsortiumTrans[0].OperatedBy;
                result.AuthorizedConsCustSecond1 = result.ConsortiumTrans[1].OperatedBy;
                result.AuthorizedConsCustSecond2 = result.ConsortiumTrans[1].OperatedBy;
                result.AuthorizedConsCustThird1 = result.ConsortiumTrans[2].OperatedBy;
                result.AuthorizedConsCustThird2 = result.ConsortiumTrans[2].OperatedBy;
                result.ConsCustNameFirst1 = result.ConsortiumTrans[0].CustomerName;
                result.ConsCustNameSecond1 = result.ConsortiumTrans[1].CustomerName;
                result.Seq1st = result.ServiceFeeTrans[0].Ordering;
                result.LineServiceFeeDescription1st = result.ServiceFeeTrans[0].Description;
                result.LineServiceFeeAmtBefTax1st = result.ServiceFeeTrans[0].AmountBeforeTax;
                result.LineServiceFeeVAT1st = result.ServiceFeeTrans[0].TaxAmount;
                result.LineServiceFeeAmtIncTax1st = result.ServiceFeeTrans[0].AmountIncludeTax;
                result.LIneServiceFeeWHT1st = result.ServiceFeeTrans[0].WHTAmount;
                result.LineServiceFeeNet1st = result.ServiceFeeTrans[0].AmountIncludeTax - result.ServiceFeeTrans[0].WHTAmount;
                result.Seq2nd = result.ServiceFeeTrans[1].Ordering;
                result.LineServiceFeeDescription2nd = result.ServiceFeeTrans[1].Description;
                result.LineServiceFeeAmtBefTax2nd = result.ServiceFeeTrans[1].AmountBeforeTax;
                result.LineServiceFeeVAT2nd = result.ServiceFeeTrans[1].TaxAmount;
                result.LineServiceFeeAmtIncTax2nd = result.ServiceFeeTrans[1].AmountIncludeTax;
                result.LIneServiceFeeWHT2nd = result.ServiceFeeTrans[1].WHTAmount;
                result.LineServiceFeeNet2nd = result.ServiceFeeTrans[1].AmountIncludeTax - result.ServiceFeeTrans[1].WHTAmount;
                result.Seq3rd = result.ServiceFeeTrans[2].Ordering;
                result.LineServiceFeeDescription3rd = result.ServiceFeeTrans[2].Description;
                result.LineServiceFeeAmtBefTax3rd = result.ServiceFeeTrans[2].AmountBeforeTax;
                result.LineServiceFeeVAT3rd = result.ServiceFeeTrans[2].TaxAmount;
                result.LineServiceFeeAmtIncTax3rd = result.ServiceFeeTrans[2].AmountIncludeTax;
                result.LIneServiceFeeWHT3rd = result.ServiceFeeTrans[2].WHTAmount;
                result.LineServiceFeeNet3rd = result.ServiceFeeTrans[2].AmountIncludeTax - result.ServiceFeeTrans[2].WHTAmount;
                result.Seq4th = result.ServiceFeeTrans[3].Ordering;
                result.LineServiceFeeDescription4th = result.ServiceFeeTrans[3].Description;
                result.LineServiceFeeAmtBefTax4th = result.ServiceFeeTrans[3].AmountBeforeTax;
                result.LineServiceFeeVAT4th = result.ServiceFeeTrans[3].TaxAmount;
                result.LineServiceFeeAmtIncTax4th = result.ServiceFeeTrans[3].AmountIncludeTax;
                result.LIneServiceFeeWHT4th = result.ServiceFeeTrans[3].WHTAmount;
                result.LineServiceFeeNet4th = result.ServiceFeeTrans[3].AmountIncludeTax - result.ServiceFeeTrans[3].WHTAmount;
                result.Seq5th = result.ServiceFeeTrans[4].Ordering;
                result.LineServiceFeeDescription5th = result.ServiceFeeTrans[4].Description;
                result.LineServiceFeeAmtBefTax5th = result.ServiceFeeTrans[4].AmountBeforeTax;
                result.LineServiceFeeVAT5th = result.ServiceFeeTrans[4].TaxAmount;
                result.LineServiceFeeAmtIncTax5th = result.ServiceFeeTrans[4].AmountIncludeTax;
                result.LIneServiceFeeWHT5th = result.ServiceFeeTrans[4].WHTAmount;
                result.LineServiceFeeNet5th = result.ServiceFeeTrans[4].AmountIncludeTax - result.ServiceFeeTrans[4].WHTAmount;
                result.Seq6th = result.ServiceFeeTrans[5].Ordering;
                result.LineServiceFeeDescription6th = result.ServiceFeeTrans[5].Description;
                result.LineServiceFeeAmtBefTax6th = result.ServiceFeeTrans[5].AmountBeforeTax;
                result.LineServiceFeeVAT6th = result.ServiceFeeTrans[5].TaxAmount;
                result.LineServiceFeeAmtIncTax6th = result.ServiceFeeTrans[5].AmountIncludeTax;
                result.LIneServiceFeeWHT6th = result.ServiceFeeTrans[5].WHTAmount;
                result.LineServiceFeeNet6th = result.ServiceFeeTrans[5].AmountIncludeTax - result.ServiceFeeTrans[5].WHTAmount;
                result.Seq7th = result.ServiceFeeTrans[6].Ordering;
                result.LineServiceFeeDescription7th = result.ServiceFeeTrans[6].Description;
                result.LineServiceFeeAmtBefTax7th = result.ServiceFeeTrans[6].AmountBeforeTax;
                result.LineServiceFeeVAT7th = result.ServiceFeeTrans[6].TaxAmount;
                result.LineServiceFeeAmtIncTax7th = result.ServiceFeeTrans[6].AmountIncludeTax;
                result.LIneServiceFeeWHT7th = result.ServiceFeeTrans[6].WHTAmount;
                result.LineServiceFeeNet7th = result.ServiceFeeTrans[6].AmountIncludeTax - result.ServiceFeeTrans[6].WHTAmount;
                result.Seq8th = result.ServiceFeeTrans[7].Ordering;
                result.LineServiceFeeDescription8th = result.ServiceFeeTrans[7].Description;
                result.LineServiceFeeAmtBefTax8th = result.ServiceFeeTrans[7].AmountBeforeTax;
                result.LineServiceFeeVAT8th = result.ServiceFeeTrans[7].TaxAmount;
                result.LineServiceFeeAmtIncTax8th = result.ServiceFeeTrans[7].AmountIncludeTax;
                result.LIneServiceFeeWHT8th = result.ServiceFeeTrans[7].WHTAmount;
                result.LineServiceFeeNet8th = result.ServiceFeeTrans[7].AmountIncludeTax - result.ServiceFeeTrans[7].WHTAmount;
                result.Seq9th = result.ServiceFeeTrans[8].Ordering;
                result.LineServiceFeeDescription9th = result.ServiceFeeTrans[8].Description;
                result.LineServiceFeeAmtBefTax9th = result.ServiceFeeTrans[8].AmountBeforeTax;
                result.LineServiceFeeVAT9th = result.ServiceFeeTrans[8].TaxAmount;
                result.LineServiceFeeAmtIncTax9th = result.ServiceFeeTrans[8].AmountIncludeTax;
                result.LIneServiceFeeWHT9th = result.ServiceFeeTrans[8].WHTAmount;
                result.LineServiceFeeNet9th = result.ServiceFeeTrans[8].AmountIncludeTax - result.ServiceFeeTrans[8].WHTAmount;
                result.Seq10th = result.ServiceFeeTrans[9].Ordering;
                result.LineServiceFeeDescription10th = result.ServiceFeeTrans[9].Description;
                result.LineServiceFeeAmtBefTax10th = result.ServiceFeeTrans[9].AmountBeforeTax;
                result.LineServiceFeeVAT10th = result.ServiceFeeTrans[9].TaxAmount;
                result.LineServiceFeeAmtIncTax10th = result.ServiceFeeTrans[9].AmountIncludeTax;
                result.LIneServiceFeeWHT10th = result.ServiceFeeTrans[9].WHTAmount;
                result.LineServiceFeeNet10th = result.ServiceFeeTrans[9].AmountIncludeTax - result.ServiceFeeTrans[9].WHTAmount;
                result.TotalServiceFee1 = result.ServiceFeeTrans.Sum(s => s.AmountIncludeTax - s.WHTAmount);
                result.TotalServiceFee2 = result.ServiceFeeTrans.Sum(s => s.AmountIncludeTax - s.WHTAmount);
                result.SMSeq1st = result.ServiceFeeTransSM[0].Ordering;
                result.SMLineServiceFeeDescription1st = result.ServiceFeeTransSM[0].Description;
                result.SMLineServiceFeeAmtBefTax1st = result.ServiceFeeTransSM[0].AmountBeforeTax;
                result.SMLineServiceFeeVAT1st = result.ServiceFeeTransSM[0].TaxAmount;
                result.SMLineServiceFeeAmtIncTax1st = result.ServiceFeeTransSM[0].AmountIncludeTax;
                result.SMLIneServiceFeeWHT1st = result.ServiceFeeTransSM[0].WHTAmount;
                result.SMLineServiceFeeNet1st1 = result.ServiceFeeTransSM[0].AmountIncludeTax - result.ServiceFeeTransSM[0].WHTAmount;
                result.SMLineServiceFeeNet1st2 = result.ServiceFeeTransSM[0].AmountIncludeTax - result.ServiceFeeTransSM[0].WHTAmount;
                result.SMLineServiceFeeNet1st3 = result.ServiceFeeTransSM[0].AmountIncludeTax - result.ServiceFeeTransSM[0].WHTAmount;
                result.SMSeq2nd = result.ServiceFeeTransSM[1].Ordering;
                result.SMLineServiceFeeDescription2nd = result.ServiceFeeTransSM[1].Description;
                result.SMLineServiceFeeAmtBefTax2nd = result.ServiceFeeTransSM[1].AmountBeforeTax;
                result.SMLineServiceFeeVAT2nd = result.ServiceFeeTransSM[1].TaxAmount;
                result.SMLineServiceFeeAmtIncTax2nd = result.ServiceFeeTransSM[1].AmountIncludeTax;
                result.SMLIneServiceFeeWHT2nd = result.ServiceFeeTransSM[1].WHTAmount;
                result.SMLineServiceFeeNet2nd1 = result.ServiceFeeTransSM[1].AmountIncludeTax - result.ServiceFeeTransSM[1].WHTAmount;
                result.SMLineServiceFeeNet2nd2 = result.ServiceFeeTransSM[1].AmountIncludeTax - result.ServiceFeeTransSM[1].WHTAmount;
                result.SMLineServiceFeeNet2nd3 = result.ServiceFeeTransSM[1].AmountIncludeTax - result.ServiceFeeTransSM[1].WHTAmount;
                result.SMSeq3rd = result.ServiceFeeTransSM[2].Ordering;
                result.SMLineServiceFeeDescription3rd = result.ServiceFeeTransSM[2].Description;
                result.SMLineServiceFeeAmtBefTax3rd = result.ServiceFeeTransSM[2].AmountBeforeTax;
                result.SMLineServiceFeeVAT3rd = result.ServiceFeeTransSM[2].TaxAmount;
                result.SMLineServiceFeeAmtIncTax3rd = result.ServiceFeeTransSM[2].AmountIncludeTax;
                result.SMLIneServiceFeeWHT3rd = result.ServiceFeeTransSM[2].WHTAmount;
                result.SMLineServiceFeeNet3rd1 = result.ServiceFeeTransSM[2].AmountIncludeTax - result.ServiceFeeTransSM[2].WHTAmount;
                result.SMLineServiceFeeNet3rd2 = result.ServiceFeeTransSM[2].AmountIncludeTax - result.ServiceFeeTransSM[2].WHTAmount;
                result.SMLineServiceFeeNet3rd3 = result.ServiceFeeTransSM[2].AmountIncludeTax - result.ServiceFeeTransSM[2].WHTAmount;
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        #endregion
        #region MainAgreementConsortiumBookmarkView
        public GenMainAgreementConsortiumBookmarkView MainAgreementConsortiumBookmarkView(Guid refGUID)
        {

            try
            {
                GenMainAgreementConsortiumBookmarkView result = (from mainAgreementTable in Entity
                                                                 join company in db.Set<Company>() on mainAgreementTable.CompanyGUID equals company.CompanyGUID

                                                                 join customer in db.Set<CustomerTable>() on mainAgreementTable.CustomerTableGUID equals customer
                                                                    .CustomerTableGUID

                                                                 join agreementTableInfo in db.Set<AgreementTableInfo>().Where(w => w.RefType == (int)RefType.MainAgreement)
                                                                  on mainAgreementTable.MainAgreementTableGUID equals agreementTableInfo.RefGUID
                                                                  into ljAgreementInfo
                                                                 from agreementTableInfo in ljAgreementInfo

                                                                 join agreementTableInfoText in db.Set<AgreementTableInfoText>()
                                                                 on agreementTableInfo.AgreementTableInfoGUID equals agreementTableInfoText.AgreementTableInfoGUID into lAgreementTableInfoText
                                                                 from agreementTableInfoText in lAgreementTableInfoText.DefaultIfEmpty()

                                                                 join companySignature1 in db.Set<CompanySignature>()
                                                                 on agreementTableInfo.AuthorizedPersonTransCompany1GUID equals companySignature1.CompanySignatureGUID into lCompanySignatureAgreementTable1
                                                                 from companySignature1 in lCompanySignatureAgreementTable1.DefaultIfEmpty()
                                                                 join employeeTable1 in db.Set<EmployeeTable>() on companySignature1.EmployeeTableGUID equals employeeTable1.EmployeeTableGUID into ljCompSign1
                                                                 from employeeTable1 in ljCompSign1.DefaultIfEmpty()

                                                                 join companySignature2 in db.Set<CompanySignature>()
                                                                 on agreementTableInfo.AuthorizedPersonTransCompany2GUID equals companySignature2.CompanySignatureGUID into lComSignatureAgreementTable2
                                                                 from companySignature2 in lComSignatureAgreementTable2.DefaultIfEmpty()
                                                                 join employeeTable2 in db.Set<EmployeeTable>() on companySignature2.EmployeeTableGUID equals employeeTable2.EmployeeTableGUID into ljCompSign2
                                                                 from employeeTable2 in ljCompSign2.DefaultIfEmpty()

                                                                 join companySignature3 in db.Set<CompanySignature>()
                                                                 on agreementTableInfo.AuthorizedPersonTransCompany3GUID equals companySignature3.CompanySignatureGUID into lComSignatureAgreementTable3
                                                                 from companySignature3 in lComSignatureAgreementTable3.DefaultIfEmpty()
                                                                 join employeeTable3 in db.Set<EmployeeTable>() on companySignature3.EmployeeTableGUID equals employeeTable3.EmployeeTableGUID into ljCompSign3
                                                                 from employeeTable3 in ljCompSign3.DefaultIfEmpty()


                                                                 join authorizedPersonTransBuyer1 in db.Set<AuthorizedPersonTrans>()
                                                                 on agreementTableInfo.AuthorizedPersonTransBuyer1GUID equals authorizedPersonTransBuyer1.AuthorizedPersonTransGUID into ljAuthBuyerRelate1
                                                                 from authorizedPersonTransBuyer1 in ljAuthBuyerRelate1.DefaultIfEmpty()
                                                                 join relatedPersonTable1 in db.Set<RelatedPersonTable>() on authorizedPersonTransBuyer1.RelatedPersonTableGUID equals relatedPersonTable1.RelatedPersonTableGUID
                                                                 into authRelateTable1
                                                                 from relatedPersonTable1 in authRelateTable1.DefaultIfEmpty()

                                                                 join authorizedPersonTransBuyer2 in db.Set<AuthorizedPersonTrans>() on agreementTableInfo
                                                                         .AuthorizedPersonTransBuyer2GUID equals authorizedPersonTransBuyer2.AuthorizedPersonTransGUID
                                                                     into ljAuthBuyerRelate2
                                                                 from authorizedPersonTransBuyer2 in ljAuthBuyerRelate2.DefaultIfEmpty()
                                                                 join relatedPersonTable2 in db.Set<RelatedPersonTable>() on authorizedPersonTransBuyer2.RelatedPersonTableGUID equals
                                                                   relatedPersonTable2.RelatedPersonTableGUID into authRelateTable2
                                                                 from relatedPersonTable2 in authRelateTable2.DefaultIfEmpty()

                                                                 join authorizedPersonTransBuyer3 in db.Set<AuthorizedPersonTrans>() on agreementTableInfo
                                                                         .AuthorizedPersonTransBuyer3GUID equals authorizedPersonTransBuyer3.AuthorizedPersonTransGUID into ljAuthBuyerRelate3
                                                                 from authorizedPersonTransBuyer3 in ljAuthBuyerRelate3.DefaultIfEmpty()
                                                                 join relatedPersonTable3 in db.Set<RelatedPersonTable>() on authorizedPersonTransBuyer3.RelatedPersonTableGUID equals
                                                                  relatedPersonTable3.RelatedPersonTableGUID into authRelateTable3
                                                                 from relatedPersonTable3 in authRelateTable3.DefaultIfEmpty()

                                                                 join authorizedPersonTransCustomer1 in db.Set<AuthorizedPersonTrans>() on agreementTableInfo
                                                                        .AuthorizedPersonTransCustomer1GUID equals authorizedPersonTransCustomer1.AuthorizedPersonTransGUID
                                                                    into ljAuthCustomerRelate1
                                                                 from authorizedPersonTransCustomer1 in ljAuthCustomerRelate1.DefaultIfEmpty()
                                                                 join relatedPersonTableCustomer1 in db.Set<RelatedPersonTable>() on authorizedPersonTransCustomer1.RelatedPersonTableGUID equals
                                                                     relatedPersonTableCustomer1.RelatedPersonTableGUID into authRelatePerson1
                                                                 from relatedPersonTableCustomer1 in authRelatePerson1.DefaultIfEmpty()

                                                                 join authorizedPersonTransCustomer2 in db.Set<AuthorizedPersonTrans>() on agreementTableInfo
                                                                         .AuthorizedPersonTransCustomer2GUID equals authorizedPersonTransCustomer2.AuthorizedPersonTransGUID
                                                                     into ljAuthCustomerRelate2
                                                                 from authorizedPersonTransCustomer2 in ljAuthCustomerRelate2.DefaultIfEmpty()
                                                                 join relatedPersonTableCustomer2 in db.Set<RelatedPersonTable>() on authorizedPersonTransCustomer2.RelatedPersonTableGUID equals
                                                                     relatedPersonTableCustomer2.RelatedPersonTableGUID into authRelatePerson2
                                                                 from relatedPersonTableCustomer2 in authRelatePerson2.DefaultIfEmpty()


                                                                 join authorizedPersonTransCustomer3 in db.Set<AuthorizedPersonTrans>() on agreementTableInfo
                                                                         .AuthorizedPersonTransCustomer3GUID equals authorizedPersonTransCustomer3.AuthorizedPersonTransGUID
                                                                     into ljAuthCustomerRelate3
                                                                 from authorizedPersonTransCustomer3 in ljAuthCustomerRelate3.DefaultIfEmpty()
                                                                 join relatedPersonTableCustomer3 in db.Set<RelatedPersonTable>() on authorizedPersonTransCustomer3.RelatedPersonTableGUID equals
                                                                     relatedPersonTableCustomer3.RelatedPersonTableGUID into authRelatePerson3
                                                                 from relatedPersonTableCustomer3 in authRelatePerson3.DefaultIfEmpty()

                                                                 join authorizedPersonTypeCustomer in db.Set<AuthorizedPersonType>() on agreementTableInfo
                                                                          .AuthorizedPersonTypeCustomerGUID equals authorizedPersonTypeCustomer.AuthorizedPersonTypeGUID
                                                                      into ljAuthCustomerPersonType
                                                                 from authorizedPersonTypeCustomer in ljAuthCustomerPersonType.DefaultIfEmpty()

                                                                 join authorizedPersonTypeBuyer in db.Set<AuthorizedPersonType>() on agreementTableInfo
                                                                         .AuthorizedPersonTypeBuyerGUID equals authorizedPersonTypeBuyer.AuthorizedPersonTypeGUID
                                                                     into ljAuthBuyerPersonType
                                                                 from authorizedPersonTypeBuyer in ljAuthBuyerPersonType.DefaultIfEmpty()

                                                                 join authorizedPersonTypeCompany in db.Set<AuthorizedPersonType>() on agreementTableInfo
                                                                         .AuthorizedPersonTypeCompanyGUID equals authorizedPersonTypeCompany.AuthorizedPersonTypeGUID
                                                                     into ljAuthCompanyPersonType
                                                                 from authorizedPersonTypeCompany in ljAuthCompanyPersonType.DefaultIfEmpty()

                                                                 join addressBranch in db.Set<AddressTrans>() on company
                                                                       .DefaultBranchGUID equals addressBranch.RefGUID
                                                                     into ljAddressBranchCompany
                                                                 from addressBranch in ljAddressBranchCompany

                                                                 join contractTransFax in db.Set<ContactTrans>().Where(w => w.RefType == (int)RefType.Branch && w.ContactType == (int)ContactType.Fax && w.PrimaryContact)
                                                                     on company.DefaultBranchGUID equals contractTransFax.RefGUID into ljContractTransFax
                                                                 from contractTransFax in ljContractTransFax.DefaultIfEmpty()

                                                                 join contractTransPhone in db.Set<ContactTrans>().Where(w => w.RefType == (int)RefType.Branch && w.ContactType == (int)ContactType.Phone && w.PrimaryContact)
                                                                     on company.DefaultBranchGUID equals contractTransPhone.RefGUID into ljContractTransPhone
                                                                 from contractTransPhone in ljContractTransPhone.DefaultIfEmpty()

                                                                 join companyBank in db.Set<CompanyBank>().Where(w => w.Primary)
                                                                 on company.CompanyGUID equals companyBank.CompanyGUID into lCompanyBank
                                                                 from companyBank in lCompanyBank.DefaultIfEmpty()
                                                                 join bankGroup in db.Set<BankGroup>()
                                                                 on companyBank.BankGroupGUID equals bankGroup.BankGroupGUID into ljBankGroup
                                                                 from bankGroup in ljBankGroup.DefaultIfEmpty()

                                                                 join bankType in db.Set<BankType>()
                                                                 on companyBank.BankTypeGUID equals bankType.BankTypeGUID into ljBankType
                                                                 from bankType in ljBankType.DefaultIfEmpty()

                                                                     //GetMainAgreementConsortiumBookmarkValue
                                                                 join consortiumTrans1 in db.Set<ConsortiumTrans>().Where(w => w.Ordering == 1)
                                                                 on mainAgreementTable.MainAgreementTableGUID equals consortiumTrans1.RefGUID into ljConsortiumTransMain1
                                                                 from consortiumTrans1 in ljConsortiumTransMain1.DefaultIfEmpty()
                                                                 join authorizedPersonType1 in db.Set<AuthorizedPersonType>()
                                                                 on consortiumTrans1.AuthorizedPersonTypeGUID equals authorizedPersonType1.AuthorizedPersonTypeGUID

                                                                 join consortiumLine in db.Set<ConsortiumLine>()
                                                                 on consortiumTrans1.ConsortiumLineGUID equals consortiumLine.ConsortiumLineGUID into ljConsortiumLine1
                                                                 from consortiumLine in ljConsortiumLine1.DefaultIfEmpty()

                                                                 join consortiumTable1 in db.Set<ConsortiumTable>()
                                                                 on consortiumLine.ConsortiumTableGUID equals consortiumTable1.ConsortiumTableGUID into ljconsortiumTable1
                                                                 from consortiumTable1 in ljconsortiumTable1.DefaultIfEmpty()

                                                                 join consortiumTrans2 in db.Set<ConsortiumTrans>().Where(w => w.Ordering == 2)
                                                                 on consortiumTrans1.RefGUID equals consortiumTrans2.RefGUID into ljConsortiumTrans2
                                                                 from consortiumTrans2 in ljConsortiumTrans2.DefaultIfEmpty()
                                                                 join authorizedPersonType2 in db.Set<AuthorizedPersonType>()
                                                                 on consortiumTrans2.AuthorizedPersonTypeGUID equals authorizedPersonType2.AuthorizedPersonTypeGUID
                                                                 into ljauthType2
                                                                 from authorizedPersonType2 in ljauthType2.DefaultIfEmpty()

                                                                 join consortiumTrans3 in db.Set<ConsortiumTrans>().Where(w => w.Ordering == 3)
                                                                 on consortiumTrans1.RefGUID equals consortiumTrans3.RefGUID into ljConsortiumTrans3
                                                                 from consortiumTrans3 in ljConsortiumTrans3.DefaultIfEmpty()
                                                                 join authorizedPersonType3 in db.Set<AuthorizedPersonType>()
                                                                 on consortiumTrans3.AuthorizedPersonTypeGUID equals authorizedPersonType3.AuthorizedPersonTypeGUID
                                                                 into ljauthType3
                                                                 from authorizedPersonType3 in ljauthType3.DefaultIfEmpty()

                                                                 join consortiumTrans4 in db.Set<ConsortiumTrans>().Where(w => w.Ordering == 4)
                                                                 on consortiumTrans1.RefGUID equals consortiumTrans4.RefGUID into ljConsortiumTrans4
                                                                 from consortiumTrans4 in ljConsortiumTrans4.DefaultIfEmpty()
                                                                 join authorizedPersonType4 in db.Set<AuthorizedPersonType>()
                                                                 on consortiumTrans4.AuthorizedPersonTypeGUID equals authorizedPersonType4.AuthorizedPersonTypeGUID
                                                                 into ljauthType4
                                                                 from authorizedPersonType4 in ljauthType4.DefaultIfEmpty()

                                                                 where mainAgreementTable.MainAgreementTableGUID == refGUID
                                                                 select new GenMainAgreementConsortiumBookmarkView
                                                                 {
                                                                     AliasNameCompany1 = company.AltName,
                                                                     AliasNameCompany2 = company.AltName,
                                                                     AliasNameCompany3 = company.AltName,
                                                                     AliasNameCompany4 = company.AltName,
                                                                     AliasNameCompany5 = company.AltName,
                                                                     AliasNameCompany6 = company.AltName,
                                                                     AliasNameCompany7 = company.AltName,
                                                                     AliasNameCompany8 = company.AltName,
                                                                     CompanyName1 = company.Name,
                                                                     CompanyName2 = company.Name,
                                                                     CompanyName3 = company.Name,
                                                                     CompanyName4 = company.Name,
                                                                     CompanyName5 = company.Name,
                                                                     CompanyName6 = company.Name,
                                                                     CompanyName7 = company.Name,
                                                                     ContAmountFirst1 = mainAgreementTable.ApprovedCreditLimit,
                                                                     ContAmountSecond1 = null,
                                                                     AgreementDate = mainAgreementTable.AgreementDate,
                                                                     AgreementDate1 = null,
                                                                     AgreementDate2 = null,
                                                                     AgreementDate3 = null,
                                                                     AgreementDate4 = null,
                                                                     AgreementDate5 = null,
                                                                     AgreementNo1 = mainAgreementTable.MainAgreementId,
                                                                     AgreementNo2 = mainAgreementTable.MainAgreementId,
                                                                     AgreementNo3 = mainAgreementTable.MainAgreementId,
                                                                     AgreementNo4 = mainAgreementTable.MainAgreementId,
                                                                     AgreementNo5 = mainAgreementTable.MainAgreementId,
                                                                     InterestRate1 = mainAgreementTable.TotalInterestPct,
                                                                     AliasNameCust1 = customer.AltName,
                                                                     AliasNameCust2 = customer.AltName,
                                                                     AliasNameCust3 = customer.AltName,
                                                                     AliasNameCust4 = customer.AltName,
                                                                     AliasNameCust5 = customer.AltName,
                                                                     AliasNameCust6 = customer.AltName,
                                                                     AliasNameCust7 = customer.AltName,
                                                                     AliasNameCust8 = customer.AltName,
                                                                     CustName1 = customer.Name,
                                                                     CustName2 = customer.Name,
                                                                     CustName3 = customer.Name,
                                                                     CustName4 = customer.Name,
                                                                     CustName5 = customer.Name,
                                                                     CustName6 = customer.Name,
                                                                     CustName7 = customer.Name,
                                                                     CustName8 = customer.Name,
                                                                     CustTaxID1 = customer.TaxID,
                                                                     CompanyWitnessFirst1 = agreementTableInfo.WitnessCustomer1,
                                                                     CompanyWitnessSecond1 = agreementTableInfo.WitnessCustomer2,
                                                                     Text1 = agreementTableInfoText.Text1,
                                                                     Text2 = agreementTableInfoText.Text2,
                                                                     Text3 = agreementTableInfoText.Text3,
                                                                     Text4 = agreementTableInfoText.Text4,
                                                                     AuthorityPersonFirst1 = employeeTable1.Name,
                                                                     AuthorityPersonFirst2 = employeeTable1.Name,
                                                                     AuthorityPersonSecond1 = employeeTable2.Name,
                                                                     AuthorityPersonSecond2 = employeeTable2.Name,
                                                                     AuthorityPersonThird1 = employeeTable3.Name,
                                                                     AuthorityPersonThird2 = employeeTable3.Name,
                                                                     AuthorizedBuyerFirst1 = relatedPersonTable1.Name,
                                                                     AuthorizedBuyerFirst2 = relatedPersonTable1.Name,
                                                                     AuthorizedBuyerSecond1 = relatedPersonTable2.Name,
                                                                     AuthorizedBuyerSecond2 = relatedPersonTable2.Name,
                                                                     AuthorizedBuyerThird1 = relatedPersonTable3.Name,
                                                                     AuthorizedBuyerThird2 = relatedPersonTable3.Name,
                                                                     AuthorizedCustFirst1 = relatedPersonTableCustomer1.Name,
                                                                     AuthorizedCustFirst2 = relatedPersonTableCustomer1.Name,
                                                                     AuthorizedCustSecond1 = relatedPersonTableCustomer2.Name,
                                                                     AuthorizedCustSecond2 = relatedPersonTableCustomer2.Name,
                                                                     AuthorizedCustThird1 = relatedPersonTableCustomer3.Name,
                                                                     AuthorizedCustThird2 = relatedPersonTableCustomer3.Name,
                                                                     CustWitnessFirst1 = agreementTableInfo.WitnessCustomer1,
                                                                     CustWitnessSecond1 = agreementTableInfo.WitnessCustomer2,
                                                                     PositionBuyer1 = authorizedPersonTypeBuyer.Description,
                                                                     PositionCust1 = authorizedPersonTypeCustomer.Description,
                                                                     PositionCust2 = authorizedPersonTypeCustomer.Description,
                                                                     PositionCust3 = authorizedPersonTypeCustomer.Description,
                                                                     PositionLIT1 = authorizedPersonTypeCompany.Description,
                                                                     PositionLIT2 = authorizedPersonTypeCompany.Description,
                                                                     PositionLIT3 = authorizedPersonTypeCompany.Description,
                                                                     CustAddress1 = agreementTableInfo.CustomerAddress1,
                                                                     CustAddress2 = agreementTableInfo.CustomerAddress1,
                                                                     CompanyAddress1 = addressBranch.Address1 + " " + addressBranch.Address2,
                                                                     CompanyAddress2 = addressBranch.Address1 + " " + addressBranch.Address2,
                                                                     TelNumber1 = contractTransPhone.ContactValue,
                                                                     FaxNumber1 = contractTransFax.ContactValue,
                                                                     FaxNumber2 = contractTransFax.ContactValue,
                                                                     CompanyBankBranch1 = companyBank.BankBranch,
                                                                     CompanyBankBranch2 = companyBank.BankBranch,
                                                                     CompanyBankName1 = bankGroup.Description,
                                                                     CompanyBankName2 = bankGroup.Description,
                                                                     CompanyBankNo1 = companyBank.AccountNumber,
                                                                     CompanyBankNo2 = companyBank.AccountNumber,
                                                                     CompanyBankType1 = bankType.Description,
                                                                     CompanyBankType2 = bankType.Description,
                                                                     ManualNo1 = null,
                                                                     ConsName1 = consortiumTable1.Description,
                                                                     ConsName2 = consortiumTable1.Description,
                                                                     ConsName3 = consortiumTable1.Description,
                                                                     ConsName4 = consortiumTable1.Description,

                                                                     ConsNameCustFirst1 = consortiumTrans1.CustomerName,
                                                                     ConsNameCustFirst2 = consortiumTrans1.CustomerName,
                                                                     ConsNameCustFirst3 = consortiumTrans1.CustomerName,
                                                                     ConsNameCustFirst4 = consortiumTrans1.CustomerName,

                                                                     ConsNameCustSecond1 = consortiumTrans2.CustomerName,
                                                                     ConsNameCustSecond2 = consortiumTrans2.CustomerName,
                                                                     ConsNameCustSecond3 = consortiumTrans2.CustomerName,
                                                                     ConsNameCustSecond4 = consortiumTrans2.CustomerName,

                                                                     ConsNameCustThird1 = consortiumTrans3.CustomerName,
                                                                     ConsNameCustThird2 = consortiumTrans3.CustomerName,
                                                                     ConsNameCustThird3 = consortiumTrans3.CustomerName,
                                                                     ConsNameCustThird4 = consortiumTrans3.CustomerName,

                                                                     ConsNameCustForth1 = consortiumTrans4.CustomerName,
                                                                     ConsNameCustForth2 = consortiumTrans4.CustomerName,
                                                                     ConsNameCustForth3 = consortiumTrans4.CustomerName,
                                                                     ConsNameCustForth4 = consortiumTrans4.CustomerName,

                                                                     ConsAddressCustFirst1 = consortiumTrans1.Address,
                                                                     ConsAddressCustFirst2 = consortiumTrans1.Address,
                                                                     ConsAddressCustFirst3 = consortiumTrans1.Address,
                                                                     ConsAddressCustFirst4 = consortiumTrans1.Address,

                                                                     ConsAddressCustSecond1 = consortiumTrans2.Address,
                                                                     ConsAddressCustSecond2 = consortiumTrans2.Address,
                                                                     ConsAddressCustSecond3 = consortiumTrans2.Address,
                                                                     ConsAddressCustSecond4 = consortiumTrans2.Address,

                                                                     ConsAddressCustThird1 = consortiumTrans3.Address,
                                                                     ConsAddressCustThird2 = consortiumTrans3.Address,
                                                                     ConsAddressCustThird3 = consortiumTrans3.Address,
                                                                     ConsAddressCustThird4 = consortiumTrans3.Address,

                                                                     ConsAddressCustForth1 = consortiumTrans4.Address,
                                                                     ConsAddressCustForth2 = consortiumTrans4.Address,
                                                                     ConsAddressCustForth3 = consortiumTrans4.Address,
                                                                     ConsAddressCustForth4 = consortiumTrans4.Address,

                                                                     ConOperatedbyFirst1 = consortiumTrans1.OperatedBy,
                                                                     ConOperatedbyFirst2 = consortiumTrans1.OperatedBy,
                                                                     ConOperatedbyFirst3 = consortiumTrans1.OperatedBy,
                                                                     ConOperatedbyFirst4 = consortiumTrans1.OperatedBy,

                                                                     ConOperatedbySecond1 = consortiumTrans2.OperatedBy,
                                                                     ConOperatedbySecond2 = consortiumTrans2.OperatedBy,
                                                                     ConOperatedbySecond3 = consortiumTrans2.OperatedBy,
                                                                     ConOperatedbySecond4 = consortiumTrans2.OperatedBy,

                                                                     ConOperatedbyThird1 = consortiumTrans3.OperatedBy,
                                                                     ConOperatedbyThird2 = consortiumTrans3.OperatedBy,
                                                                     ConOperatedbyThird3 = consortiumTrans3.OperatedBy,
                                                                     ConOperatedbyThird4 = consortiumTrans3.OperatedBy,

                                                                     ConOperatedbyForth1 = consortiumTrans4.OperatedBy,
                                                                     ConOperatedbyForth2 = consortiumTrans4.OperatedBy,
                                                                     ConOperatedbyForth3 = consortiumTrans4.OperatedBy,
                                                                     ConOperatedbyForth4 = consortiumTrans4.OperatedBy,

                                                                     PositionConsCustFirst1 = authorizedPersonType1.Description,
                                                                     PositionConsCustFirst2 = authorizedPersonType1.Description,
                                                                     PositionConsCustFirst3 = authorizedPersonType1.Description,
                                                                     PositionConsCustFirst4 = authorizedPersonType1.Description,

                                                                     PositionConsCustSecond1 = authorizedPersonType2.Description,
                                                                     PositionConsCustSecond2 = authorizedPersonType2.Description,
                                                                     PositionConsCustSecond3 = authorizedPersonType2.Description,
                                                                     PositionConsCustSecond4 = authorizedPersonType2.Description,

                                                                     PositionConsCustThird1 = authorizedPersonType3.Description,
                                                                     PositionConsCustThird2 = authorizedPersonType3.Description,
                                                                     PositionConsCustThird3 = authorizedPersonType3.Description,
                                                                     PositionConsCustThird4 = authorizedPersonType3.Description,

                                                                     PositionConsCustForth1 = authorizedPersonType3.Description,
                                                                     PositionConsCustForth2 = authorizedPersonType3.Description,
                                                                     PositionConsCustForth3 = authorizedPersonType3.Description,
                                                                     PositionConsCustForth4 = authorizedPersonType3.Description,

                                                                 }).AsNoTracking().FirstOrDefault();

                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region AddandumMainAgreementBookmark
        public AddendumMainAgreementBookmarkView GetAddendumMainAgreementBookmarkView(Guid refGuid)
        {
            try
            {
                AddendumMainAgreementBookmarkView addendumQ = (from mainAgreement in Entity
                                                               join agreementTableInfo in db.Set<AgreementTableInfo>().Where(w => w.RefType == (int)RefType.MainAgreement)
                                                               on mainAgreement.MainAgreementTableGUID equals agreementTableInfo.RefGUID

                                                               join employeeTable1 in db.Set<EmployeeTable>()
                                                               on agreementTableInfo.AuthorizedPersonTransCompany1GUID equals employeeTable1.EmployeeTableGUID into ljEmployee1
                                                               from employeeTable1 in ljEmployee1.DefaultIfEmpty()

                                                               join employeeTable2 in db.Set<EmployeeTable>()
                                                               on agreementTableInfo.AuthorizedPersonTransCompany2GUID equals employeeTable2.EmployeeTableGUID into ljEmployee2
                                                               from employeeTable2 in ljEmployee2.DefaultIfEmpty()

                                                               join employeeTable3 in db.Set<EmployeeTable>()
                                                               on agreementTableInfo.AuthorizedPersonTransCompany3GUID equals employeeTable3.EmployeeTableGUID into ljEmployee3
                                                               from employeeTable3 in ljEmployee3.DefaultIfEmpty()

                                                               join authorizedPersonType in db.Set<AuthorizedPersonType>()
                                                               on agreementTableInfo.AuthorizedPersonTypeCompanyGUID equals authorizedPersonType.AuthorizedPersonTypeGUID into ljAuthType
                                                               from authorizedPersonType in ljAuthType.DefaultIfEmpty()

                                                               join authorizedPersonTrans1 in db.Set<AuthorizedPersonTrans>()
                                                               on agreementTableInfo.AuthorizedPersonTransCustomer1GUID equals authorizedPersonTrans1.AuthorizedPersonTransGUID into ljAuthTrans1
                                                               from authorizedPersonTrans1 in ljAuthTrans1.DefaultIfEmpty()
                                                               join relatedPersonTable1 in db.Set<RelatedPersonTable>()
                                                               on authorizedPersonTrans1.RelatedPersonTableGUID equals relatedPersonTable1.RelatedPersonTableGUID into ljRelatePerson1
                                                               from relatedPersonTable1 in ljRelatePerson1.DefaultIfEmpty()

                                                               join authorizedPersonTrans2 in db.Set<AuthorizedPersonTrans>()
                                                               on agreementTableInfo.AuthorizedPersonTransCustomer2GUID equals authorizedPersonTrans2.AuthorizedPersonTransGUID into ljAuthTrans2
                                                               from authorizedPersonTrans2 in ljAuthTrans2.DefaultIfEmpty()
                                                               join relatedPersonTable2 in db.Set<RelatedPersonTable>()
                                                               on authorizedPersonTrans2.RelatedPersonTableGUID equals relatedPersonTable2.RelatedPersonTableGUID into ljRelatePerson2
                                                               from relatedPersonTable2 in ljRelatePerson2.DefaultIfEmpty()

                                                               join authorizedPersonTrans3 in db.Set<AuthorizedPersonTrans>()
                                                               on agreementTableInfo.AuthorizedPersonTransCustomer3GUID equals authorizedPersonTrans3.AuthorizedPersonTransGUID into ljAuthTrans3
                                                               from authorizedPersonTrans3 in ljAuthTrans3.DefaultIfEmpty()
                                                               join relatedPersonTable3 in db.Set<RelatedPersonTable>()
                                                               on authorizedPersonTrans3.RelatedPersonTableGUID equals relatedPersonTable3.RelatedPersonTableGUID into ljRelatePerson3
                                                               from relatedPersonTable3 in ljRelatePerson3.DefaultIfEmpty()
                                                                   // creditAppRequestQuery
                                                               join creditAppRequestTable in db.Set<CreditAppRequestTable>() on mainAgreement.CreditAppRequestTableGUID equals creditAppRequestTable.CreditAppRequestTableGUID
                                                               join creditAppRequestTableAmend in db.Set<CreditAppRequestTableAmend>()
                                                               on creditAppRequestTable.CreditAppRequestTableGUID equals creditAppRequestTableAmend.CreditAppRequestTableGUID

                                                               join customertable in db.Set<CustomerTable>() on creditAppRequestTable.CustomerTableGUID equals customertable.CustomerTableGUID
                                                               join interestTypeValue in db.Set<InterestTypeValue>() on creditAppRequestTable.InterestTypeGUID equals interestTypeValue.InterestTypeGUID
                                                               join interestType in db.Set<InterestType>() on interestTypeValue.InterestTypeGUID equals interestType.InterestTypeGUID
                                                               // creditAppRequestQuery



                                                               where mainAgreement.MainAgreementTableGUID == refGuid && mainAgreement.AgreementDocType == (int)AgreementDocType.Addendum
                                                               select new AddendumMainAgreementBookmarkView
                                                               {
                                                                   CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
                                                                   InterestTypeId = interestType.InterestTypeId,
                                                                   InterestAdjustment = creditAppRequestTable.InterestAdjustment,
                                                                   InterestValue = interestTypeValue.Value,
                                                                   AuthorityPerson1st_1 = employeeTable1 != null ? employeeTable1.Name : "",
                                                                   AuthorityPerson2nd_1 = employeeTable2 != null ? employeeTable2.Name : "",
                                                                   AuthorityPerson3rd_1 = employeeTable3 != null ? employeeTable3.Name : "",
                                                                   PositionLIT1 = authorizedPersonType != null ? authorizedPersonType.Description : "",
                                                                   AuthorizedCust1st_1 = relatedPersonTable1 != null ? relatedPersonTable1.Name : "",
                                                                   AuthorizedCust1st_2 = relatedPersonTable1 != null ? relatedPersonTable1.Name : "",
                                                                   AuthorizedCust2nd_1 = relatedPersonTable2 != null ? relatedPersonTable2.Name : "",
                                                                   AuthorizedCust2nd_2 = relatedPersonTable2 != null ? relatedPersonTable2.Name : "",
                                                                   AuthorizedCust3rd_1 = relatedPersonTable3 != null ? relatedPersonTable3.Name : "",
                                                                   AuthorizedCust3rd_2 = relatedPersonTable3 != null ? relatedPersonTable3.Name : "",
                                                                   AgreementDate = mainAgreement.AgreementDate,
                                                                   RequestDate = creditAppRequestTable.RequestDate,
                                                                   ContractNo1 = mainAgreement.MainAgreementId,
                                                                   ContractNo2 = mainAgreement.MainAgreementId,
                                                                   ContractNo3 = mainAgreement.MainAgreementId,
                                                                   ContractNo4 = mainAgreement.MainAgreementId,
                                                                   ContractNo5 = mainAgreement.MainAgreementId,
                                                                   ContractNo6 = mainAgreement.MainAgreementId,
                                                                   ContractNo7 = mainAgreement.MainAgreementId,
                                                                   ContractNo8 = mainAgreement.MainAgreementId,
                                                                   ContractNo9 = mainAgreement.MainAgreementId,
                                                                   ContractNo10 = mainAgreement.MainAgreementId,
                                                                   CustName1 = customertable.Name,
                                                                   CustName2 = customertable.Name,
                                                                   CustName3 = customertable.Name,
                                                                   CustName4 = customertable.Name,
                                                                   CustName5 = customertable.Name,
                                                                   CustName6 = customertable.Name,
                                                                   CustName7 = customertable.Name,
                                                                   CustName8 = customertable.Name,
                                                                   CustName9 = customertable.Name,
                                                                   CustName10 = customertable.Name,
                                                                   CustName11 = customertable.Name,
                                                                   CustName12 = customertable.Name,
                                                                   CustName13 = customertable.Name,
                                                                   CustName14 = customertable.Name,
                                                                   CustName15 = customertable.Name,
                                                                   CustName16 = customertable.Name,
                                                                   CustName17 = customertable.Name,
                                                                   CustName18 = customertable.Name,
                                                                   CustName19 = customertable.Name,
                                                                   CustName20 = customertable.Name,
                                                                   CustName21 = customertable.Name,
                                                                   CustName22 = customertable.Name,
                                                                   CustName23 = customertable.Name,
                                                                   CustName24 = customertable.Name,
                                                                   CustName25 = customertable.Name,
                                                                   CustName26 = customertable.Name,
                                                                   CustName27 = customertable.Name,
                                                                   CustName28 = customertable.Name,
                                                                   CustName29 = customertable.Name,
                                                                   CustName30 = customertable.Name,
                                                                   CustName31 = customertable.Name,
                                                                   CustName32 = customertable.Name,
                                                                   CustName33 = customertable.Name,
                                                                   CustName34 = customertable.Name,
                                                                   CustName35 = customertable.Name,
                                                                   CustName36 = customertable.Name,
                                                                   CustName37 = customertable.Name,
                                                                   CustName38 = customertable.Name,
                                                                   CustName39 = customertable.Name,
                                                                   CustName40 = customertable.Name,
                                                                   CustName41 = customertable.Name,
                                                                   CustName42 = customertable.Name,
                                                                   CustName43 = customertable.Name,
                                                                   CustName44 = customertable.Name,
                                                                   CustName45 = customertable.Name,
                                                                   CustName46 = customertable.Name,
                                                                   CustName47 = customertable.Name,
                                                                   CustName48 = customertable.Name,
                                                                   CustName49 = customertable.Name,
                                                                   CustName50 = customertable.Name,
                                                                   OldInterestRate1 = creditAppRequestTableAmend.OriginalTotalInterestPct.ToString()
                                                               }).FirstOrDefault();

                if (addendumQ != null)
                {
                    List<RelatedPersonTable> relatedPersonTableNews = (from authorizedPersonTrans in db.Set<AuthorizedPersonTrans>()
                                                                       join relatedPersonTable in db.Set<RelatedPersonTable>()
                                                                       on authorizedPersonTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID
                                                                       where authorizedPersonTrans.RefGUID == addendumQ.CreditAppRequestTableGUID && authorizedPersonTrans.RefType == (int)RefType.CreditAppRequestTable
                                                                       && !authorizedPersonTrans.InActive && authorizedPersonTrans.RefAuthorizedPersonTransGUID == null
                                                                       select relatedPersonTable).OrderBy(o => o.Name).Take(6).ToList();
                    List<RelatedPersonTable> relatedPersonTablesOlds = (from authorizedPersonTrans in db.Set<AuthorizedPersonTrans>()
                                                                        join relatedPersonTable in db.Set<RelatedPersonTable>()
                                                                        on authorizedPersonTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID
                                                                        where authorizedPersonTrans.RefGUID == addendumQ.CreditAppRequestTableGUID && authorizedPersonTrans.AuthorizedPersonTransGUID == authorizedPersonTrans.ReplacedAuthorizedPersonTransGUID
                                                                        && authorizedPersonTrans.RefType == (int)RefType.CreditAppRequestTable
                                                                        && !authorizedPersonTrans.InActive && authorizedPersonTrans.RefAuthorizedPersonTransGUID == null
                                                                        select relatedPersonTable).OrderBy(o => o.Name).Take(6).ToList();
                    addendumQ.AuthorizedCustNew1st_1 = relatedPersonTableNews.ElementAtOrDefault(0) == null ? "" : relatedPersonTableNews[0].Name;
                    addendumQ.AuthorizedCustNew1st_2 = relatedPersonTableNews.ElementAtOrDefault(0) == null ? "" : relatedPersonTableNews[0].Name;
                    addendumQ.AuthorizedCustNew2nd_1 = relatedPersonTableNews.ElementAtOrDefault(1) == null ? "" : relatedPersonTableNews[1].Name;
                    addendumQ.AuthorizedCustNew2nd_2 = relatedPersonTableNews.ElementAtOrDefault(1) == null ? "" : relatedPersonTableNews[1].Name;
                    addendumQ.AuthorizedCustNew3rd_1 = relatedPersonTableNews.ElementAtOrDefault(2) == null ? "" : relatedPersonTableNews[2].Name;
                    addendumQ.AuthorizedCustNew3rd_2 = relatedPersonTableNews.ElementAtOrDefault(2) == null ? "" : relatedPersonTableNews[2].Name;
                    addendumQ.AuthorizedCustNew4th_1 = relatedPersonTableNews.ElementAtOrDefault(3) == null ? "" : relatedPersonTableNews[3].Name;
                    addendumQ.AuthorizedCustNew4th_2 = relatedPersonTableNews.ElementAtOrDefault(3) == null ? "" : relatedPersonTableNews[3].Name;
                    addendumQ.AuthorizedCustNew5th_1 = relatedPersonTableNews.ElementAtOrDefault(4) == null ? "" : relatedPersonTableNews[4].Name;
                    addendumQ.AuthorizedCustNew5th_2 = relatedPersonTableNews.ElementAtOrDefault(4) == null ? "" : relatedPersonTableNews[4].Name;
                    addendumQ.AuthorizedCustNew6th_1 = relatedPersonTableNews.ElementAtOrDefault(5) == null ? "" : relatedPersonTableNews[5].Name;
                    addendumQ.AuthorizedCustNew6th_2 = relatedPersonTableNews.ElementAtOrDefault(5) == null ? "" : relatedPersonTableNews[5].Name;
                    addendumQ.AuthorizedCustOld1st_1 = relatedPersonTablesOlds.ElementAtOrDefault(0) == null ? "" : relatedPersonTablesOlds[0].Name;
                    addendumQ.AuthorizedCustOld1st_2 = relatedPersonTablesOlds.ElementAtOrDefault(0) == null ? "" : relatedPersonTablesOlds[0].Name;
                    addendumQ.AuthorizedCustOld2nd_1 = relatedPersonTablesOlds.ElementAtOrDefault(1) == null ? "" : relatedPersonTablesOlds[1].Name;
                    addendumQ.AuthorizedCustOld2nd_2 = relatedPersonTablesOlds.ElementAtOrDefault(1) == null ? "" : relatedPersonTablesOlds[1].Name;
                    addendumQ.AuthorizedCustOld3rd_1 = relatedPersonTablesOlds.ElementAtOrDefault(2) == null ? "" : relatedPersonTablesOlds[2].Name;
                    addendumQ.AuthorizedCustOld3rd_2 = relatedPersonTablesOlds.ElementAtOrDefault(2) == null ? "" : relatedPersonTablesOlds[2].Name;
                    addendumQ.AuthorizedCustOld4th_1 = relatedPersonTablesOlds.ElementAtOrDefault(3) == null ? "" : relatedPersonTablesOlds[3].Name;
                    addendumQ.AuthorizedCustOld4th_2 = relatedPersonTablesOlds.ElementAtOrDefault(3) == null ? "" : relatedPersonTablesOlds[3].Name;
                    addendumQ.AuthorizedCustOld5th_1 = relatedPersonTablesOlds.ElementAtOrDefault(4) == null ? "" : relatedPersonTablesOlds[4].Name;
                    addendumQ.AuthorizedCustOld5th_2 = relatedPersonTablesOlds.ElementAtOrDefault(4) == null ? "" : relatedPersonTablesOlds[4].Name;
                    addendumQ.AuthorizedCustOld6th_1 = relatedPersonTablesOlds.ElementAtOrDefault(5) == null ? "" : relatedPersonTablesOlds[5].Name;
                    addendumQ.AuthorizedCustOld6th_2 = relatedPersonTablesOlds.ElementAtOrDefault(5) == null ? "" : relatedPersonTablesOlds[5].Name;
                    addendumQ.NewInterestRate1 = string.IsNullOrEmpty(addendumQ.InterestTypeId) ? addendumQ.InterestAdjustment.ToString() : (addendumQ.InterestValue + addendumQ.InterestAdjustment).ToString();
                    return addendumQ;
                }
                else
                {
                    return new AddendumMainAgreementBookmarkView();
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }
}
