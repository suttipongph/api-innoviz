using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IAddressPostalCodeRepo
	{
		#region DropDown
		IEnumerable<SelectItem<AddressPostalCodeItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<AddressPostalCodeListView> GetListvw(SearchParameter search);
		AddressPostalCodeItemView GetByIdvw(Guid id);
		AddressPostalCode Find(params object[] keyValues);
		AddressPostalCode GetAddressPostalCodeByIdNoTracking(Guid guid);
		AddressPostalCode CreateAddressPostalCode(AddressPostalCode addressPostalCode);
		void CreateAddressPostalCodeVoid(AddressPostalCode addressPostalCode);
		AddressPostalCode UpdateAddressPostalCode(AddressPostalCode dbAddressPostalCode, AddressPostalCode inputAddressPostalCode, List<string> skipUpdateFields = null);
		void UpdateAddressPostalCodeVoid(AddressPostalCode dbAddressPostalCode, AddressPostalCode inputAddressPostalCode, List<string> skipUpdateFields = null);
		void Remove(AddressPostalCode item);
		List<AddressPostalCode> GetAddressPostalCodeByCompanyNoTracking(Guid companyGUID);
	}
	public class AddressPostalCodeRepo : CompanyBaseRepository<AddressPostalCode>, IAddressPostalCodeRepo
	{
		public AddressPostalCodeRepo(SmartAppDbContext context) : base(context) { }
		public AddressPostalCodeRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public AddressPostalCode GetAddressPostalCodeByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.AddressPostalCodeGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<AddressPostalCodeItemViewMap> GetDropDownQuery()
		{
			return (from addressPostalCode in Entity
					join addressSubDistrict in db.Set<AddressSubDistrict>()
					on addressPostalCode.AddressSubDistrictGUID equals addressSubDistrict.AddressSubDistrictGUID into ljAddressSubDistrict
					from addressSubDistrict in ljAddressSubDistrict.DefaultIfEmpty()
					select new AddressPostalCodeItemViewMap
					{
						CompanyGUID = addressPostalCode.CompanyGUID,
						Owner = addressPostalCode.Owner,
						OwnerBusinessUnitGUID = addressPostalCode.OwnerBusinessUnitGUID,
						AddressPostalCodeGUID = addressPostalCode.AddressPostalCodeGUID,
						PostalCode = addressPostalCode.PostalCode,
						AddressSubDistrictGUID = addressPostalCode.AddressSubDistrictGUID,
						AddressSubDistrict_Value = SmartAppUtil.GetDropDownLabel(addressSubDistrict.SubDistrictId, addressSubDistrict.Name)
					});
		}
		public IEnumerable<SelectItem<AddressPostalCodeItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<AddressPostalCode>(search, SysParm.CompanyGUID);
				var addressPostalCode = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<AddressPostalCodeItemViewMap, AddressPostalCodeItemView>().ToDropDownItem(search.ExcludeRowData);


				return addressPostalCode;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<AddressPostalCodeListViewMap> GetListQuery()
		{
			return (from addressPostalCode in Entity
					join addressSubDistrict in db.Set<AddressSubDistrict>()
					on addressPostalCode.AddressSubDistrictGUID equals addressSubDistrict.AddressSubDistrictGUID into ljAddressSubDistrict
					from addressSubDistrict in ljAddressSubDistrict.DefaultIfEmpty()
					select new AddressPostalCodeListViewMap
				{
						CompanyGUID = addressPostalCode.CompanyGUID,
						Owner = addressPostalCode.Owner,
						OwnerBusinessUnitGUID = addressPostalCode.OwnerBusinessUnitGUID,
						AddressPostalCodeGUID = addressPostalCode.AddressPostalCodeGUID,
						PostalCode = addressPostalCode.PostalCode,
						AddressSubDistrictGUID = addressPostalCode.AddressSubDistrictGUID,
						AddressSubDistrict_Value = SmartAppUtil.GetDropDownLabel(addressSubDistrict.SubDistrictId, addressSubDistrict.Name)
					});
		}
		public SearchResult<AddressPostalCodeListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<AddressPostalCodeListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<AddressPostalCode>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<AddressPostalCodeListViewMap, AddressPostalCodeListView>();
				result = list.SetSearchResult<AddressPostalCodeListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<AddressPostalCodeItemViewMap> GetItemQuery()
		{
			return (from addressPostalCode in Entity
					join company in db.Set<Company>()
					on addressPostalCode.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on addressPostalCode.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljAddressPostalCodeOwnerBU
					from ownerBU in ljAddressPostalCodeOwnerBU.DefaultIfEmpty()
					join addressSubDistrict in db.Set<AddressSubDistrict>()
					on addressPostalCode.AddressSubDistrictGUID equals addressSubDistrict.AddressSubDistrictGUID into ljAddressSubDistrict
					from addressSubDistrict in ljAddressSubDistrict.DefaultIfEmpty()
					select new AddressPostalCodeItemViewMap
					{
						CompanyGUID = addressPostalCode.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = addressPostalCode.Owner,
						OwnerBusinessUnitGUID = addressPostalCode.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = addressPostalCode.CreatedBy,
						CreatedDateTime = addressPostalCode.CreatedDateTime,
						ModifiedBy = addressPostalCode.ModifiedBy,
						ModifiedDateTime = addressPostalCode.ModifiedDateTime,
						AddressPostalCodeGUID = addressPostalCode.AddressPostalCodeGUID,
						PostalCode = addressPostalCode.PostalCode,
						AddressSubDistrictGUID = addressPostalCode.AddressSubDistrictGUID,
						AddressSubDistrict_Value = SmartAppUtil.GetDropDownLabel(addressSubDistrict.SubDistrictId, addressSubDistrict.Name),

					
						RowVersion = addressPostalCode.RowVersion,
					});
		}
		public AddressPostalCodeItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<AddressPostalCodeItemViewMap, AddressPostalCodeItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public AddressPostalCode CreateAddressPostalCode(AddressPostalCode addressPostalCode)
		{
			try
			{
				addressPostalCode.AddressPostalCodeGUID = Guid.NewGuid();
				base.Add(addressPostalCode);
				return addressPostalCode;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateAddressPostalCodeVoid(AddressPostalCode addressPostalCode)
		{
			try
			{
				CreateAddressPostalCode(addressPostalCode);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AddressPostalCode UpdateAddressPostalCode(AddressPostalCode dbAddressPostalCode, AddressPostalCode inputAddressPostalCode, List<string> skipUpdateFields = null)
		{
			try
			{
				dbAddressPostalCode = dbAddressPostalCode.MapUpdateValues<AddressPostalCode>(inputAddressPostalCode);
				base.Update(dbAddressPostalCode);
				return dbAddressPostalCode;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateAddressPostalCodeVoid(AddressPostalCode dbAddressPostalCode, AddressPostalCode inputAddressPostalCode, List<string> skipUpdateFields = null)
		{
			try
			{
				dbAddressPostalCode = dbAddressPostalCode.MapUpdateValues<AddressPostalCode>(inputAddressPostalCode, skipUpdateFields);
				base.Update(dbAddressPostalCode);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public List<AddressPostalCode> GetAddressPostalCodeByCompanyNoTracking(Guid companyGUID)
		{
			try
			{
				List<AddressPostalCode> addressPostalCodes = Entity.Where(w => w.CompanyGUID == companyGUID).AsNoTracking().ToList();
				return addressPostalCodes;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

