using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IServiceFeeCondTemplateTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<ServiceFeeCondTemplateTableItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<ServiceFeeCondTemplateTableListView> GetListvw(SearchParameter search);
		ServiceFeeCondTemplateTableItemView GetByIdvw(Guid id);
		ServiceFeeCondTemplateTable Find(params object[] keyValues);
		ServiceFeeCondTemplateTable GetServiceFeeCondTemplateTableByIdNoTracking(Guid guid);
		ServiceFeeCondTemplateTable CreateServiceFeeCondTemplateTable(ServiceFeeCondTemplateTable serviceFeeCondTemplateTable);
		void CreateServiceFeeCondTemplateTableVoid(ServiceFeeCondTemplateTable serviceFeeCondTemplateTable);
		ServiceFeeCondTemplateTable UpdateServiceFeeCondTemplateTable(ServiceFeeCondTemplateTable dbServiceFeeCondTemplateTable, ServiceFeeCondTemplateTable inputServiceFeeCondTemplateTable, List<string> skipUpdateFields = null);
		void UpdateServiceFeeCondTemplateTableVoid(ServiceFeeCondTemplateTable dbServiceFeeCondTemplateTable, ServiceFeeCondTemplateTable inputServiceFeeCondTemplateTable, List<string> skipUpdateFields = null);
		void Remove(ServiceFeeCondTemplateTable item);
		public ServiceFeeCondTemplateTable GetServiceFeeCondTemplateTableByIdNoTrackingByAccessLevel(Guid guid);
	}
	public class ServiceFeeCondTemplateTableRepo : CompanyBaseRepository<ServiceFeeCondTemplateTable>, IServiceFeeCondTemplateTableRepo
	{
		public ServiceFeeCondTemplateTableRepo(SmartAppDbContext context) : base(context) { }
		public ServiceFeeCondTemplateTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ServiceFeeCondTemplateTable GetServiceFeeCondTemplateTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ServiceFeeCondTemplateTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ServiceFeeCondTemplateTable GetServiceFeeCondTemplateTableByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ServiceFeeCondTemplateTableGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<ServiceFeeCondTemplateTableItemViewMap> GetDropDownQuery()
		{
			return (from serviceFeeCondTemplateTable in Entity
					select new ServiceFeeCondTemplateTableItemViewMap
					{
						CompanyGUID = serviceFeeCondTemplateTable.CompanyGUID,
						Owner = serviceFeeCondTemplateTable.Owner,
						OwnerBusinessUnitGUID = serviceFeeCondTemplateTable.OwnerBusinessUnitGUID,
						ServiceFeeCondTemplateTableGUID = serviceFeeCondTemplateTable.ServiceFeeCondTemplateTableGUID,
						ServiceFeeCondTemplateId = serviceFeeCondTemplateTable.ServiceFeeCondTemplateId,
						Description = serviceFeeCondTemplateTable.Description,
						ProductType = serviceFeeCondTemplateTable.ProductType
					});
		}
		public IEnumerable<SelectItem<ServiceFeeCondTemplateTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<ServiceFeeCondTemplateTable>(search, SysParm.CompanyGUID);
				var serviceFeeCondTemplateTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<ServiceFeeCondTemplateTableItemViewMap, ServiceFeeCondTemplateTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return serviceFeeCondTemplateTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<ServiceFeeCondTemplateTableListViewMap> GetListQuery()
		{
			return (from serviceFeeCondTemplateTable in Entity
				select new ServiceFeeCondTemplateTableListViewMap
				{
						CompanyGUID = serviceFeeCondTemplateTable.CompanyGUID,
						Owner = serviceFeeCondTemplateTable.Owner,
						OwnerBusinessUnitGUID = serviceFeeCondTemplateTable.OwnerBusinessUnitGUID,
						ServiceFeeCondTemplateTableGUID = serviceFeeCondTemplateTable.ServiceFeeCondTemplateTableGUID,
						ProductType = serviceFeeCondTemplateTable.ProductType,
						ServiceFeeCondTemplateId = serviceFeeCondTemplateTable.ServiceFeeCondTemplateId,
						Description = serviceFeeCondTemplateTable.Description,
				});
		}
		public SearchResult<ServiceFeeCondTemplateTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<ServiceFeeCondTemplateTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<ServiceFeeCondTemplateTable>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<ServiceFeeCondTemplateTableListViewMap, ServiceFeeCondTemplateTableListView>();
				result = list.SetSearchResult<ServiceFeeCondTemplateTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<ServiceFeeCondTemplateTableItemViewMap> GetItemQuery()
		{
			return (from serviceFeeCondTemplateTable in Entity
					join company in db.Set<Company>()
					on serviceFeeCondTemplateTable.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on serviceFeeCondTemplateTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljServiceFeeCondTemplateTableOwnerBU
					from ownerBU in ljServiceFeeCondTemplateTableOwnerBU.DefaultIfEmpty()
					select new ServiceFeeCondTemplateTableItemViewMap
					{
						CompanyGUID = serviceFeeCondTemplateTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = serviceFeeCondTemplateTable.Owner,
						OwnerBusinessUnitGUID = serviceFeeCondTemplateTable.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = serviceFeeCondTemplateTable.CreatedBy,
						CreatedDateTime = serviceFeeCondTemplateTable.CreatedDateTime,
						ModifiedBy = serviceFeeCondTemplateTable.ModifiedBy,
						ModifiedDateTime = serviceFeeCondTemplateTable.ModifiedDateTime,
						ServiceFeeCondTemplateTableGUID = serviceFeeCondTemplateTable.ServiceFeeCondTemplateTableGUID,
						Description = serviceFeeCondTemplateTable.Description,
						ProductType = serviceFeeCondTemplateTable.ProductType,
						ServiceFeeCondTemplateId = serviceFeeCondTemplateTable.ServiceFeeCondTemplateId,
					
						RowVersion = serviceFeeCondTemplateTable.RowVersion,
					});
		}
		public ServiceFeeCondTemplateTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<ServiceFeeCondTemplateTableItemViewMap, ServiceFeeCondTemplateTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public ServiceFeeCondTemplateTable CreateServiceFeeCondTemplateTable(ServiceFeeCondTemplateTable serviceFeeCondTemplateTable)
		{
			try
			{
				serviceFeeCondTemplateTable.ServiceFeeCondTemplateTableGUID = Guid.NewGuid();
				base.Add(serviceFeeCondTemplateTable);
				return serviceFeeCondTemplateTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateServiceFeeCondTemplateTableVoid(ServiceFeeCondTemplateTable serviceFeeCondTemplateTable)
		{
			try
			{
				CreateServiceFeeCondTemplateTable(serviceFeeCondTemplateTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ServiceFeeCondTemplateTable UpdateServiceFeeCondTemplateTable(ServiceFeeCondTemplateTable dbServiceFeeCondTemplateTable, ServiceFeeCondTemplateTable inputServiceFeeCondTemplateTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbServiceFeeCondTemplateTable = dbServiceFeeCondTemplateTable.MapUpdateValues<ServiceFeeCondTemplateTable>(inputServiceFeeCondTemplateTable);
				base.Update(dbServiceFeeCondTemplateTable);
				return dbServiceFeeCondTemplateTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateServiceFeeCondTemplateTableVoid(ServiceFeeCondTemplateTable dbServiceFeeCondTemplateTable, ServiceFeeCondTemplateTable inputServiceFeeCondTemplateTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbServiceFeeCondTemplateTable = dbServiceFeeCondTemplateTable.MapUpdateValues<ServiceFeeCondTemplateTable>(inputServiceFeeCondTemplateTable, skipUpdateFields);
				base.Update(dbServiceFeeCondTemplateTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

