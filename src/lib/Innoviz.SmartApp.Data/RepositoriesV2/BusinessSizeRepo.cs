using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IBusinessSizeRepo
	{
		#region DropDown
		IEnumerable<SelectItem<BusinessSizeItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<BusinessSizeListView> GetListvw(SearchParameter search);
		BusinessSizeItemView GetByIdvw(Guid id);
		BusinessSize Find(params object[] keyValues);
		BusinessSize GetBusinessSizeByIdNoTracking(Guid guid);
		BusinessSize CreateBusinessSize(BusinessSize businessSize);
		void CreateBusinessSizeVoid(BusinessSize businessSize);
		BusinessSize UpdateBusinessSize(BusinessSize dbBusinessSize, BusinessSize inputBusinessSize, List<string> skipUpdateFields = null);
		void UpdateBusinessSizeVoid(BusinessSize dbBusinessSize, BusinessSize inputBusinessSize, List<string> skipUpdateFields = null);
		void Remove(BusinessSize item);
	}
	public class BusinessSizeRepo : CompanyBaseRepository<BusinessSize>, IBusinessSizeRepo
	{
		public BusinessSizeRepo(SmartAppDbContext context) : base(context) { }
		public BusinessSizeRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BusinessSize GetBusinessSizeByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BusinessSizeGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<BusinessSizeItemViewMap> GetDropDownQuery()
		{
			return (from businessSize in Entity
					select new BusinessSizeItemViewMap
					{
						CompanyGUID = businessSize.CompanyGUID,
						Owner = businessSize.Owner,
						OwnerBusinessUnitGUID = businessSize.OwnerBusinessUnitGUID,
						BusinessSizeGUID = businessSize.BusinessSizeGUID,
						BusinessSizeId = businessSize.BusinessSizeId,
						Description = businessSize.Description
					});
		}
		public IEnumerable<SelectItem<BusinessSizeItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<BusinessSize>(search, SysParm.CompanyGUID);
				var businessSize = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<BusinessSizeItemViewMap, BusinessSizeItemView>().ToDropDownItem(search.ExcludeRowData);


				return businessSize;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<BusinessSizeListViewMap> GetListQuery()
		{
			return (from businessSize in Entity
				select new BusinessSizeListViewMap
				{
						CompanyGUID = businessSize.CompanyGUID,
						Owner = businessSize.Owner,
						OwnerBusinessUnitGUID = businessSize.OwnerBusinessUnitGUID,
						BusinessSizeGUID = businessSize.BusinessSizeGUID,
						BusinessSizeId = businessSize.BusinessSizeId,
						Description = businessSize.Description,
				});
		}
		public SearchResult<BusinessSizeListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<BusinessSizeListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<BusinessSize>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<BusinessSizeListViewMap, BusinessSizeListView>();
				result = list.SetSearchResult<BusinessSizeListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<BusinessSizeItemViewMap> GetItemQuery()
		{
			return (from businessSize in Entity
					join ownerBU in db.Set<BusinessUnit>()
					on businessSize.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljBusinessSizeOwnerBU
					from ownerBU in ljBusinessSizeOwnerBU.DefaultIfEmpty()
					join company in db.Set<Company>() on businessSize.CompanyGUID equals company.CompanyGUID
					select new BusinessSizeItemViewMap
					{
						CompanyGUID = businessSize.CompanyGUID,
						Owner = businessSize.Owner,
						OwnerBusinessUnitGUID = businessSize.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CompanyId = company.CompanyId,
						CreatedBy = businessSize.CreatedBy,
						CreatedDateTime = businessSize.CreatedDateTime,
						ModifiedBy = businessSize.ModifiedBy,
						ModifiedDateTime = businessSize.ModifiedDateTime,
						BusinessSizeGUID = businessSize.BusinessSizeGUID,
						BusinessSizeId = businessSize.BusinessSizeId,
						Description = businessSize.Description,
					
						RowVersion = businessSize.RowVersion,
					});
		}
		public BusinessSizeItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<BusinessSizeItemViewMap, BusinessSizeItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public BusinessSize CreateBusinessSize(BusinessSize businessSize)
		{
			try
			{
				businessSize.BusinessSizeGUID = Guid.NewGuid();
				base.Add(businessSize);
				return businessSize;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateBusinessSizeVoid(BusinessSize businessSize)
		{
			try
			{
				CreateBusinessSize(businessSize);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BusinessSize UpdateBusinessSize(BusinessSize dbBusinessSize, BusinessSize inputBusinessSize, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBusinessSize = dbBusinessSize.MapUpdateValues<BusinessSize>(inputBusinessSize);
				base.Update(dbBusinessSize);
				return dbBusinessSize;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateBusinessSizeVoid(BusinessSize dbBusinessSize, BusinessSize inputBusinessSize, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBusinessSize = dbBusinessSize.MapUpdateValues<BusinessSize>(inputBusinessSize, skipUpdateFields);
				base.Update(dbBusinessSize);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

