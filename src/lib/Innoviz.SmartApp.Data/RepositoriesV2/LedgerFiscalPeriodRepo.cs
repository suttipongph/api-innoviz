using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface ILedgerFiscalPeriodRepo
	{
		#region DropDown
		IEnumerable<SelectItem<LedgerFiscalPeriodItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<LedgerFiscalPeriodListView> GetListvw(SearchParameter search);
		LedgerFiscalPeriodItemView GetByIdvw(Guid id);
		LedgerFiscalPeriod Find(params object[] keyValues);
		LedgerFiscalPeriod GetLedgerFiscalPeriodByIdNoTracking(Guid guid);
		LedgerFiscalPeriod CreateLedgerFiscalPeriod(LedgerFiscalPeriod ledgerFiscalPeriod);
		void CreateLedgerFiscalPeriodVoid(LedgerFiscalPeriod ledgerFiscalPeriod);
		LedgerFiscalPeriod UpdateLedgerFiscalPeriod(LedgerFiscalPeriod dbLedgerFiscalPeriod, LedgerFiscalPeriod inputLedgerFiscalPeriod, List<string> skipUpdateFields = null);
		void UpdateLedgerFiscalPeriodVoid(LedgerFiscalPeriod dbLedgerFiscalPeriod, LedgerFiscalPeriod inputLedgerFiscalPeriod, List<string> skipUpdateFields = null);
		void Remove(LedgerFiscalPeriod item);
		LedgerFiscalPeriod GetLedgerFiscalPeriodByIdNoTrackingByAccessLevel(Guid guid);
		IEnumerable<LedgerFiscalPeriod> GetLedgerFiscalPeriodByLedgerFiscalYearGUID(Guid LedgerFiscalYearGUID);
		void ValidateAdd(LedgerFiscalPeriod item);
		void ValidateAdd(IEnumerable<LedgerFiscalPeriod> items);
		#region shared
		LedgerFiscalPeriod GetByDate(Guid companyGUID, DateTime asOfDate);
        #endregion
    }
    public class LedgerFiscalPeriodRepo : CompanyBaseRepository<LedgerFiscalPeriod>, ILedgerFiscalPeriodRepo
	{
		public LedgerFiscalPeriodRepo(SmartAppDbContext context) : base(context) { }
		public LedgerFiscalPeriodRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public LedgerFiscalPeriod GetLedgerFiscalPeriodByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.LedgerFiscalPeriodGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<LedgerFiscalPeriodItemViewMap> GetDropDownQuery()
		{
			return (from ledgerFiscalPeriod in Entity
					select new LedgerFiscalPeriodItemViewMap
					{
						CompanyGUID = ledgerFiscalPeriod.CompanyGUID,
						Owner = ledgerFiscalPeriod.Owner,
						OwnerBusinessUnitGUID = ledgerFiscalPeriod.OwnerBusinessUnitGUID,
						LedgerFiscalPeriodGUID = ledgerFiscalPeriod.LedgerFiscalPeriodGUID
					});
		}
		public IEnumerable<SelectItem<LedgerFiscalPeriodItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<LedgerFiscalPeriod>(search, SysParm.CompanyGUID);
				var ledgerFiscalPeriod = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<LedgerFiscalPeriodItemViewMap, LedgerFiscalPeriodItemView>().ToDropDownItem(search.ExcludeRowData);


				return ledgerFiscalPeriod;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<LedgerFiscalPeriodListViewMap> GetListQuery()
		{
			return (from ledgerFiscalPeriod in Entity
				select new LedgerFiscalPeriodListViewMap
				{
						LedgerFiscalPeriodGUID = ledgerFiscalPeriod.LedgerFiscalPeriodGUID,
						StartDate = ledgerFiscalPeriod.StartDate,
						EndDate = ledgerFiscalPeriod.EndDate,
						LedgerFiscalYearGUID = ledgerFiscalPeriod.LedgerFiscalYearGUID,
						PeriodStatus = ledgerFiscalPeriod.PeriodStatus
				});
		}
		public SearchResult<LedgerFiscalPeriodListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<LedgerFiscalPeriodListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<LedgerFiscalPeriod>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<LedgerFiscalPeriodListViewMap, LedgerFiscalPeriodListView>();
				result = list.SetSearchResult<LedgerFiscalPeriodListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<LedgerFiscalPeriodItemViewMap> GetItemQuery()
		{
			return (from ledgerFiscalPeriod in Entity
					join company in db.Set<Company>()
					on ledgerFiscalPeriod.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on ledgerFiscalPeriod.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljLedgerFiscalPeriodOwnerBU
					from ownerBU in ljLedgerFiscalPeriodOwnerBU.DefaultIfEmpty()
					select new LedgerFiscalPeriodItemViewMap
					{
						CompanyGUID = ledgerFiscalPeriod.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = ledgerFiscalPeriod.Owner,
						OwnerBusinessUnitGUID = ledgerFiscalPeriod.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = ledgerFiscalPeriod.CreatedBy,
						CreatedDateTime = ledgerFiscalPeriod.CreatedDateTime,
						ModifiedBy = ledgerFiscalPeriod.ModifiedBy,
						ModifiedDateTime = ledgerFiscalPeriod.ModifiedDateTime,
						LedgerFiscalPeriodGUID = ledgerFiscalPeriod.LedgerFiscalPeriodGUID,
						EndDate = ledgerFiscalPeriod.EndDate,
						LedgerFiscalYearGUID = ledgerFiscalPeriod.LedgerFiscalYearGUID,
						PeriodStatus = ledgerFiscalPeriod.PeriodStatus,
						StartDate = ledgerFiscalPeriod.StartDate,
					
						RowVersion = ledgerFiscalPeriod.RowVersion,
					});
		}
		public LedgerFiscalPeriodItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<LedgerFiscalPeriodItemViewMap, LedgerFiscalPeriodItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public LedgerFiscalPeriod CreateLedgerFiscalPeriod(LedgerFiscalPeriod ledgerFiscalPeriod)
		{
			try
			{
				ledgerFiscalPeriod.LedgerFiscalPeriodGUID = Guid.NewGuid();
				base.Add(ledgerFiscalPeriod);
				return ledgerFiscalPeriod;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateLedgerFiscalPeriodVoid(LedgerFiscalPeriod ledgerFiscalPeriod)
		{
			try
			{
				CreateLedgerFiscalPeriod(ledgerFiscalPeriod);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public LedgerFiscalPeriod UpdateLedgerFiscalPeriod(LedgerFiscalPeriod dbLedgerFiscalPeriod, LedgerFiscalPeriod inputLedgerFiscalPeriod, List<string> skipUpdateFields = null)
		{
			try
			{
				dbLedgerFiscalPeriod = dbLedgerFiscalPeriod.MapUpdateValues<LedgerFiscalPeriod>(inputLedgerFiscalPeriod);
				base.Update(dbLedgerFiscalPeriod);
				return dbLedgerFiscalPeriod;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateLedgerFiscalPeriodVoid(LedgerFiscalPeriod dbLedgerFiscalPeriod, LedgerFiscalPeriod inputLedgerFiscalPeriod, List<string> skipUpdateFields = null)
		{
			try
			{
				dbLedgerFiscalPeriod = dbLedgerFiscalPeriod.MapUpdateValues<LedgerFiscalPeriod>(inputLedgerFiscalPeriod, skipUpdateFields);
				base.Update(dbLedgerFiscalPeriod);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<LedgerFiscalPeriod> GetLedgerFiscalPeriodByLedgerFiscalYearGUID(Guid LedgerFiscalYearGUID)
		{
			try
			{
				
				return Entity.Where(a => a.LedgerFiscalYearGUID == LedgerFiscalYearGUID).AsNoTracking();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public LedgerFiscalPeriod GetLedgerFiscalPeriodByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.LedgerFiscalPeriodGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public override void ValidateAdd(IEnumerable<LedgerFiscalPeriod> items)
		{
			base.ValidateAdd(items);
		}
		public override void ValidateAdd(LedgerFiscalPeriod item)
		{
			base.ValidateAdd(item);
		}

		#region shared
		public LedgerFiscalPeriod GetByDate(Guid companyGUID, DateTime asOfDate)
        {
            try
            {
				LedgerFiscalPeriod ledgerFiscalPeriod = Entity.Where(a => a.CompanyGUID == companyGUID
																	   && asOfDate >= a.StartDate
																	   && asOfDate <= a.EndDate).AsNoTracking().FirstOrDefault();
				if (ledgerFiscalPeriod == null)
                {
					SmartAppException ex = new SmartAppException("ERROR.ERROR");
					IWithholdingTaxTableRepo withholdingTaxTableRepo = new WithholdingTaxTableRepo(db);
					ex.AddData("ERROR.90037", new string[] { "LABEL.LEDGER_FISCAL_PERIOD"});
					throw SmartAppUtil.AddStackTrace(ex);
				}
                else
                {
					return ledgerFiscalPeriod;
				}	
			}
            catch (Exception)
            {

                throw;
            }
        }



        #endregion
    }
}

