using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.InterfaceWithOtherSystems.Models;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IStagingTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<StagingTableItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<StagingTableListView> GetListvw(SearchParameter search);
		StagingTableItemView GetByIdvw(Guid id);
		StagingTable Find(params object[] keyValues);
		StagingTable GetStagingTableByIdNoTracking(Guid guid);
		StagingTable CreateStagingTable(StagingTable stagingTable);
		void CreateStagingTableVoid(StagingTable stagingTable);
		StagingTable UpdateStagingTable(StagingTable dbStagingTable, StagingTable inputStagingTable, List<string> skipUpdateFields = null);
		void UpdateStagingTableVoid(StagingTable dbStagingTable, StagingTable inputStagingTable, List<string> skipUpdateFields = null);
		void Remove(StagingTable item);
		StagingTable GetStagingTableByIdNoTrackingByAccessLevel(Guid guid);
        #region interface
        List<StagingTable> GetInterfaceAccountingStagingData(List<int> transactionTypes, List<int> interfaceStatuses); 
		List<StagingTable> GetInterfaceVendorInvoiceStagingData(List<int> transactionTypes, List<int> interfaceStatuses, 
																		IEnumerable<Guid> refGUIDList = null);
		#endregion
		List<StagingTable> GetStagingTableVendorPaymentTransByRefTypeRefGUID(int refType, Guid refGUID);
    }
    public class StagingTableRepo : CompanyBaseRepository<StagingTable>, IStagingTableRepo
	{
		public StagingTableRepo(SmartAppDbContext context) : base(context) { }
		public StagingTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public StagingTable GetStagingTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.StagingTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<StagingTableItemViewMap> GetDropDownQuery()
		{
			return (from stagingTable in Entity
					select new StagingTableItemViewMap
					{
						CompanyGUID = stagingTable.CompanyGUID,
						Owner = stagingTable.Owner,
						OwnerBusinessUnitGUID = stagingTable.OwnerBusinessUnitGUID,
						StagingTableGUID = stagingTable.StagingTableGUID
					});
		}
		public IEnumerable<SelectItem<StagingTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<StagingTable>(search, SysParm.CompanyGUID);
				var stagingTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<StagingTableItemViewMap, StagingTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return stagingTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<StagingTableListViewMap> GetListQuery()
		{
			return (from stagingTable in Entity

					join withdrawalTable in db.Set<WithdrawalTable>()
					on stagingTable.RefGUID equals withdrawalTable.WithdrawalTableGUID into ljwithdrawal
					from withdrawalTable in ljwithdrawal.DefaultIfEmpty()

					join customerRefundTable in db.Set<CustomerRefundTable>()
					on stagingTable.RefGUID equals customerRefundTable.CustomerRefundTableGUID into ljcustomerRefund
					from customerRefundTable in ljcustomerRefund.DefaultIfEmpty()

					join mainAgreementTable in db.Set<MainAgreementTable>()
					on stagingTable.RefGUID equals mainAgreementTable.MainAgreementTableGUID into ljmainAgreement
					from mainAgreementTable in ljmainAgreement.DefaultIfEmpty()

					join businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
					on stagingTable.RefGUID equals businessCollateralAgmTable.BusinessCollateralAgmTableGUID into ljbusinessCollateral
					from businessCollateralAgmTable in ljbusinessCollateral.DefaultIfEmpty()

					join assignmentAgreementTable in db.Set<AssignmentAgreementTable>()
					on stagingTable.RefGUID equals assignmentAgreementTable.AssignmentAgreementTableGUID into ljassignmentAgreement
					from assignmentAgreementTable in ljassignmentAgreement.DefaultIfEmpty()
					
					join purchaseTable in db.Set<PurchaseTable>()
					on stagingTable.RefGUID equals purchaseTable.PurchaseTableGUID into ljpurchase
					from purchaseTable in ljpurchase.DefaultIfEmpty()

					join messengetJobTable in db.Set<MessengerJobTable>()
					on stagingTable.RefGUID equals messengetJobTable.MessengerJobTableGUID into ljmessengerJob
					from messengetJobTable in ljmessengerJob.DefaultIfEmpty()

					join receiptTempTable in db.Set<ReceiptTempTable>()
					on stagingTable.RefGUID equals receiptTempTable.ReceiptTempTableGUID into ljreceiptTemp
					from receiptTempTable in ljreceiptTemp.DefaultIfEmpty()

					join vendorPaymentTrans in db.Set<VendorPaymentTrans>()
					on stagingTable.RefGUID equals vendorPaymentTrans.VendorPaymentTransGUID into ljvendorPaymentTrans
					from vendorPaymentTrans in ljvendorPaymentTrans.DefaultIfEmpty()

					join vendorTable in db.Set<VendorTable>()
					on vendorPaymentTrans.VendorTableGUID equals vendorTable.VendorTableGUID into ljvendorTable
					from vendorTable in ljvendorTable.DefaultIfEmpty()

					join interestRealizedTrans in db.Set<InterestRealizedTrans>()
					on stagingTable.RefGUID equals interestRealizedTrans.InterestRealizedTransGUID into ljinterestRealizedTrans
					from interestRealizedTrans in ljinterestRealizedTrans.DefaultIfEmpty()

					join processTransTable in db.Set<ProcessTrans>()
					on stagingTable.ProcessTransGUID equals processTransTable.ProcessTransGUID into ljprocessTrans
					from processTransTable in ljprocessTrans.DefaultIfEmpty()

					join invoiceTable in db.Set<InvoiceTable>()
					on processTransTable.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljinvoiceTable
					from invoiceTable in ljinvoiceTable.DefaultIfEmpty()
					select new StagingTableListViewMap
				{
						CompanyGUID = stagingTable.CompanyGUID,
						Owner = stagingTable.Owner,
						OwnerBusinessUnitGUID = stagingTable.OwnerBusinessUnitGUID,
						StagingTableGUID = stagingTable.StagingTableGUID,
						StagingBatchId = stagingTable.StagingBatchId,
						InterfaceStagingBatchId = stagingTable.InterfaceStagingBatchId,
						InterfaceStatus = stagingTable.InterfaceStatus,
						RefType = stagingTable.RefType,
						TransDate = stagingTable.TransDate,
						ProcessTransType = stagingTable.ProcessTransType,
						AccountType = stagingTable.AccountType,
						AccountNum = stagingTable.AccountNum,
						AmountMST = stagingTable.AmountMST,
						ProcessTransGUID = stagingTable.ProcessTransGUID,
						SourceRefType = stagingTable.SourceRefType,
						SourceRefId = stagingTable.SourceRefId,
						ProcessTrans_Values = (processTransTable != null) ? SmartAppUtil.GetDropDownLabel(((ProcessTransType)processTransTable.ProcessTransType).ToString(), processTransTable.DocumentId) : null,
						RefId = (withdrawalTable != null && stagingTable.RefType ==(int)RefType.WithdrawalTable)?withdrawalTable.WithdrawalId:
								(customerRefundTable != null && stagingTable.RefType ==(int)RefType.CustomerRefund)?customerRefundTable.CustomerRefundId:
								(mainAgreementTable !=null && stagingTable.RefType == (int)RefType.MainAgreement)?mainAgreementTable.InternalMainAgreementId:
								(businessCollateralAgmTable !=null && stagingTable.RefType == (int)RefType.BusinessCollateralAgreement)?businessCollateralAgmTable.InternalBusinessCollateralAgmId:
								(assignmentAgreementTable !=null && stagingTable.RefType == (int)RefType.AssignmentAgreement)?assignmentAgreementTable.InternalAssignmentAgreementId:
								(purchaseTable != null && stagingTable.RefType == (int)RefType.PurchaseTable)?purchaseTable.PurchaseId:
								(messengetJobTable != null && stagingTable.RefType == (int)RefType.MessengerJob)?messengetJobTable.JobId:
								(receiptTempTable != null && stagingTable.RefType == (int)RefType.ReceiptTemp)?receiptTempTable.ReceiptTempId :
								(vendorPaymentTrans != null && vendorTable != null && stagingTable.RefType == (int)RefType.VendorPaymentTrans) ? vendorTable.VendorId :
								(interestRealizedTrans != null && stagingTable.RefType == (int)RefType.InterestRealizedTrans) ? interestRealizedTrans.DocumentId : null,
						ProcessTrans_ProcessTransId = processTransTable.DocumentId,
						InvoiceTableGUID = processTransTable.InvoiceTableGUID,
						InvoiceTable_Values = invoiceTable != null ? SmartAppUtil.GetDropDownLabel(invoiceTable.InvoiceId,invoiceTable.IssuedDate.DateToString()) : null,
						InvoiceTable_invoiceId = invoiceTable != null ? invoiceTable.InvoiceId : null

					});
		
		}
		public SearchResult<StagingTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<StagingTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<StagingTable>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<StagingTableListViewMap, StagingTableListView>();
				result = list.SetSearchResult<StagingTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<StagingTableItemViewMap> GetItemQuery()
		{
			return (from stagingTable in Entity
					join company in db.Set<Company>()
					on stagingTable.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on stagingTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljStagingTableOwnerBU
					from ownerBU in ljStagingTableOwnerBU.DefaultIfEmpty()
					join withdrawalTable in db.Set<WithdrawalTable>()
					on stagingTable.RefGUID equals withdrawalTable.WithdrawalTableGUID into ljwithdrawal
					from withdrawalTable in ljwithdrawal.DefaultIfEmpty()

					join customerRefundTable in db.Set<CustomerRefundTable>()
					on stagingTable.RefGUID equals customerRefundTable.CustomerRefundTableGUID into ljcustomerRefund
					from customerRefundTable in ljcustomerRefund.DefaultIfEmpty()

					join mainAgreementTable in db.Set<MainAgreementTable>()
					on stagingTable.RefGUID equals mainAgreementTable.MainAgreementTableGUID into ljmainAgreement
					from mainAgreementTable in ljmainAgreement.DefaultIfEmpty()

					join businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
					on stagingTable.RefGUID equals businessCollateralAgmTable.BusinessCollateralAgmTableGUID into ljbusinessCollateral
					from businessCollateralAgmTable in ljbusinessCollateral.DefaultIfEmpty()

					join assignmentAgreementTable in db.Set<AssignmentAgreementTable>()
					on stagingTable.RefGUID equals assignmentAgreementTable.AssignmentAgreementTableGUID into ljassignmentAgreement
					from assignmentAgreementTable in ljassignmentAgreement.DefaultIfEmpty()

					join purchaseTable in db.Set<PurchaseTable>()
					on stagingTable.RefGUID equals purchaseTable.PurchaseTableGUID into ljpurchase
					from purchaseTable in ljpurchase.DefaultIfEmpty()

					join messengetJobTable in db.Set<MessengerJobTable>()
					on stagingTable.RefGUID equals messengetJobTable.MessengerJobTableGUID into ljmessengerJob
					from messengetJobTable in ljmessengerJob.DefaultIfEmpty()

					join receiptTempTable in db.Set<ReceiptTempTable>()
					on stagingTable.RefGUID equals receiptTempTable.ReceiptTempTableGUID into ljreceiptTemp
					from receiptTempTable in ljreceiptTemp.DefaultIfEmpty()

					join vendorPaymentTrans in db.Set<VendorPaymentTrans>()
					on stagingTable.RefGUID equals vendorPaymentTrans.VendorPaymentTransGUID into ljvendorPaymentTrans
					from vendorPaymentTrans in ljvendorPaymentTrans.DefaultIfEmpty()

					join vendorTable in db.Set<VendorTable>()
					on vendorPaymentTrans.VendorTableGUID equals vendorTable.VendorTableGUID into ljvendorTable
					from vendorTable in ljvendorTable.DefaultIfEmpty()

					join interestRealizedTrans in db.Set<InterestRealizedTrans>()
					on stagingTable.RefGUID equals interestRealizedTrans.InterestRealizedTransGUID into ljinterestRealizedTrans
					from interestRealizedTrans in ljinterestRealizedTrans.DefaultIfEmpty()

					join processTransTable in db.Set<ProcessTrans>()
					on stagingTable.ProcessTransGUID equals processTransTable.ProcessTransGUID into ljprocessTrans
					from processTransTable in ljprocessTrans.DefaultIfEmpty()

					join invoiceTable in db.Set<InvoiceTable>()
					on processTransTable.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljinvoiceTable
					from invoiceTable in ljinvoiceTable.DefaultIfEmpty()

					select new StagingTableItemViewMap
					{
						CompanyGUID = stagingTable.CompanyGUID,
						Owner = stagingTable.Owner,
						OwnerBusinessUnitGUID = stagingTable.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = stagingTable.CreatedBy,
						CreatedDateTime = stagingTable.CreatedDateTime,
						ModifiedBy = stagingTable.ModifiedBy,
						ModifiedDateTime = stagingTable.ModifiedDateTime,
						StagingTableGUID = stagingTable.StagingTableGUID,
						AccountNum = stagingTable.AccountNum,
						AccountType = stagingTable.AccountType,
						AmountMST = stagingTable.AmountMST,
						CompanyId = company.CompanyId,
						CompanyTaxBranchId = stagingTable.CompanyTaxBranchId,
						DimensionCode1 = stagingTable.DimensionCode1,
						DimensionCode2 = stagingTable.DimensionCode2,
						DimensionCode3 = stagingTable.DimensionCode3,
						DimensionCode4 = stagingTable.DimensionCode4,
						DimensionCode5 = stagingTable.DimensionCode5,
						DocumentId = stagingTable.DocumentId,
						InterfaceStagingBatchId = stagingTable.InterfaceStagingBatchId,
						InterfaceStatus = stagingTable.InterfaceStatus,
						ProcessTransGUID = stagingTable.ProcessTransGUID,
						ProcessTransType = stagingTable.ProcessTransType,
						ProductType = stagingTable.ProductType,
						RefGUID = stagingTable.RefGUID,
						RefType = stagingTable.RefType,
						SourceRefType = stagingTable.SourceRefType,
						SourceRefId = stagingTable.SourceRefId,
						StagingBatchId = stagingTable.StagingBatchId,
						TaxAccountId = stagingTable.TaxAccountId,
						TaxAccountName = stagingTable.TaxAccountName,
						TaxAddress = stagingTable.TaxAddress,
						TaxBaseAmount = stagingTable.TaxBaseAmount,
						TaxBranchId = stagingTable.TaxBranchId,
						TaxCode = stagingTable.TaxCode,
						TaxId = stagingTable.TaxId,
						TaxInvoiceId = stagingTable.TaxInvoiceId,
						TransDate = stagingTable.TransDate,
						TransText = stagingTable.TransText,
						VendorTaxInvoiceId = stagingTable.VendorTaxInvoiceId,
						WHTCode = stagingTable.WHTCode,
						RefId = (withdrawalTable != null && stagingTable.RefType == (int)RefType.WithdrawalTable) ? withdrawalTable.WithdrawalId :
								(customerRefundTable != null && stagingTable.RefType == (int)RefType.CustomerRefund) ? customerRefundTable.CustomerRefundId :
								(mainAgreementTable != null && stagingTable.RefType == (int)RefType.MainAgreement) ? mainAgreementTable.InternalMainAgreementId :
								(businessCollateralAgmTable != null && stagingTable.RefType == (int)RefType.BusinessCollateralAgreement) ? businessCollateralAgmTable.InternalBusinessCollateralAgmId :
								(assignmentAgreementTable != null && stagingTable.RefType == (int)RefType.AssignmentAgreement) ? assignmentAgreementTable.InternalAssignmentAgreementId :
								(purchaseTable != null && stagingTable.RefType == (int)RefType.PurchaseTable) ? purchaseTable.PurchaseId :
								(messengetJobTable != null && stagingTable.RefType == (int)RefType.MessengerJob) ? messengetJobTable.JobId :
								(receiptTempTable != null && stagingTable.RefType == (int)RefType.ReceiptTemp) ? receiptTempTable.ReceiptTempId :
								(vendorPaymentTrans != null && vendorTable != null && stagingTable.RefType == (int)RefType.VendorPaymentTrans) ? vendorTable.VendorId :
								(interestRealizedTrans != null && stagingTable.RefType == (int)RefType.InterestRealizedTrans) ? interestRealizedTrans.DocumentId : null,
						ProcessTrans_Values = (processTransTable != null) ? SmartAppUtil.GetDropDownLabel(((ProcessTransType)processTransTable.ProcessTransType).ToString(), processTransTable.DocumentId):null,
						StagingTableCompanyId = stagingTable.CompanyId,
						InvoiceTableGUID = processTransTable.InvoiceTableGUID,
						InvoiceTable_Values = invoiceTable != null ? SmartAppUtil.GetDropDownLabel(invoiceTable.InvoiceId, invoiceTable.IssuedDate.DateToString()) : null,
					
						RowVersion = stagingTable.RowVersion,
					});
		}
		public StagingTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<StagingTableItemViewMap, StagingTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public StagingTable CreateStagingTable(StagingTable stagingTable)
		{
			try
			{
				stagingTable.StagingTableGUID = Guid.NewGuid();
				base.Add(stagingTable);
				return stagingTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateStagingTableVoid(StagingTable stagingTable)
		{
			try
			{
				CreateStagingTable(stagingTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public StagingTable UpdateStagingTable(StagingTable dbStagingTable, StagingTable inputStagingTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbStagingTable = dbStagingTable.MapUpdateValues<StagingTable>(inputStagingTable);
				base.Update(dbStagingTable);
				return dbStagingTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateStagingTableVoid(StagingTable dbStagingTable, StagingTable inputStagingTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbStagingTable = dbStagingTable.MapUpdateValues<StagingTable>(inputStagingTable, skipUpdateFields);
				base.Update(dbStagingTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public StagingTable GetStagingTableByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.StagingTableGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region interface
		public List<StagingTable> GetInterfaceAccountingStagingData(List<int> transactionTypes, List<int> interfaceStatuses)
        {
            try
            {
				Guid companyGUID = SysParm.CompanyGUID.StringToGuid();
				interfaceStatuses = interfaceStatuses != null && interfaceStatuses.Count > 0 ? 
					interfaceStatuses : new List<int> { (int)InterfaceStatus.None, (int)InterfaceStatus.Fail };
				
				transactionTypes = transactionTypes != null && transactionTypes.Count > 0 ? 
					transactionTypes : 
					new List<int> { (int)ProcessTransType.Invoice, (int)ProcessTransType.IncomeRealization, (int)ProcessTransType.Payment };
				

				var result = Entity.Where(w =>  w.CompanyGUID == companyGUID &&
												interfaceStatuses.Contains(w.InterfaceStatus) && 
												transactionTypes.Contains(w.ProcessTransType))
									.AsNoTracking()
									.ToList();
				return result;
			}
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public List<StagingTable> GetInterfaceVendorInvoiceStagingData(List<int> transactionTypes, List<int> interfaceStatuses,
																			IEnumerable<Guid> refGUIDList = null)
        {
            try
            {
				Guid companyGUID = SysParm.CompanyGUID.StringToGuid();
				interfaceStatuses = interfaceStatuses != null && interfaceStatuses.Count > 0 ?
					interfaceStatuses : new List<int> { (int)InterfaceStatus.None, (int)InterfaceStatus.Fail };

				transactionTypes = transactionTypes != null && transactionTypes.Count > 0 ?
					transactionTypes :
					new List<int> { (int)ProcessTransType.VendorPayment };

				string interfaceStagingBatchId = SmartAppUtil.GenIDByDateTime(DateTime.Now);

				var result =
					Entity.Where(w => w.CompanyGUID == companyGUID &&
															transactionTypes.Contains(w.ProcessTransType) &&
															interfaceStatuses.Contains(w.InterfaceStatus) &&
															((refGUIDList != null && w.RefGUID.HasValue && refGUIDList.Contains(w.RefGUID.Value))
															|| refGUIDList == null))
							 .AsNoTracking()
							 .ToList();
				return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		#endregion
		public List<StagingTable> GetStagingTableVendorPaymentTransByRefTypeRefGUID(int refType, Guid refGUID)
        {
            try
            {
				var result =
					(from stagingTable in Entity
					 join vendorPaymentTrans in db.Set<VendorPaymentTrans>().Where(w => w.RefType == refType && w.RefGUID == refGUID)
					 on stagingTable.RefGUID equals vendorPaymentTrans.VendorPaymentTransGUID
					 select stagingTable)
					 .AsNoTracking()
					 .ToList();
				return result;
			}
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
	}
}

