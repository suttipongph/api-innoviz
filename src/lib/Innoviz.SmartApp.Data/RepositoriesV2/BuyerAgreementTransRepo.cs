using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IBuyerAgreementTransRepo
	{
		#region DropDown
		IEnumerable<SelectItem<BuyerAgreementTransItemView>> GetDropDownItem(SearchParameter search);
		IEnumerable<SelectItem<BuyerAgreementTransItemView>> GetDropDownItemByWithdrawalTable(SearchParameter search);
		#endregion DropDown
		SearchResult<BuyerAgreementTransListView> GetListvw(SearchParameter search);
		BuyerAgreementTransItemView GetByIdvw(Guid id);
		BuyerAgreementTrans Find(params object[] keyValues);
		BuyerAgreementTrans GetBuyerAgreementTransByIdNoTracking(Guid guid);
		BuyerAgreementTrans CreateBuyerAgreementTrans(BuyerAgreementTrans buyerAgreementTrans);
		void CreateBuyerAgreementTransVoid(BuyerAgreementTrans buyerAgreementTrans);
		BuyerAgreementTrans UpdateBuyerAgreementTrans(BuyerAgreementTrans dbBuyerAgreementTrans, BuyerAgreementTrans inputBuyerAgreementTrans, List<string> skipUpdateFields = null);
		void UpdateBuyerAgreementTransVoid(BuyerAgreementTrans dbBuyerAgreementTrans, BuyerAgreementTrans inputBuyerAgreementTrans, List<string> skipUpdateFields = null);
		void Remove(BuyerAgreementTrans item);
		BuyerAgreementTrans GetBuyerAgreementTransByIdNoTrackingByAccessLevel(Guid guid);
		BuyerAgreementTrans GetBuyerAgreementTransByReferenceNoTracking(Guid guid, int refType);
		IEnumerable<BuyerAgreementTrans> GetBuyerAgreementTransByReferanceNoTracking(int refType, Guid refGUID);
		bool IsExistByWithdrawal(Guid withdrawalTableGUID);
		void ValidateAdd(BuyerAgreementTrans item);
		void ValidateAdd(IEnumerable<BuyerAgreementTrans> items);
		#region function
		IEnumerable<BuyerAgreementTrans> GetCopyBuyerAgreementTransByReference(Guid refGUID, int refType);
		CopyBuyerAgreementTransView GetCopyBuyerAgreementTranByAppReqLineGUID(Guid guid);
		#endregion
		IEnumerable<BuyerAgreementTrans> GetBuyerAgreementTransCaReqLineByCaReqIdNoTracking(Guid creditAppRequestTableGUID);
	}
    public class BuyerAgreementTransRepo : CompanyBaseRepository<BuyerAgreementTrans>, IBuyerAgreementTransRepo
	{
		public BuyerAgreementTransRepo(SmartAppDbContext context) : base(context) { }
		public BuyerAgreementTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BuyerAgreementTrans GetBuyerAgreementTransByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BuyerAgreementTransGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<BuyerAgreementTransItemViewMap> GetDropDownQuery()
		{
			return (from buyerAgreementTrans in Entity
					select new BuyerAgreementTransItemViewMap
					{
						CompanyGUID = buyerAgreementTrans.CompanyGUID,
						Owner = buyerAgreementTrans.Owner,
						OwnerBusinessUnitGUID = buyerAgreementTrans.OwnerBusinessUnitGUID,
						BuyerAgreementTransGUID = buyerAgreementTrans.BuyerAgreementTransGUID,
						BuyerAgreementTableGUID = buyerAgreementTrans.BuyerAgreementTableGUID,
						BuyerAgreementLineGUID = buyerAgreementTrans.BuyerAgreementLineGUID,
						RefGUID = buyerAgreementTrans.RefGUID,
						RefType = buyerAgreementTrans.RefType
					});
		}
		private IQueryable<BuyerAgreementTransItemViewMap> GetDropDownQueryByWithdrawalTable()
		{
			return (from buyerAgreementTrans in Entity
					join buyerAgreementTable in db.Set<BuyerAgreementTable>() on buyerAgreementTrans.BuyerAgreementTableGUID equals buyerAgreementTable.BuyerAgreementTableGUID

					join buyerAgreementLine in db.Set<BuyerAgreementLine>()
					on buyerAgreementTable.BuyerAgreementTableGUID equals buyerAgreementLine.BuyerAgreementTableGUID into ljBuyerAgreementLine
					from buyerAgreementLine in ljBuyerAgreementLine.DefaultIfEmpty()
					select new BuyerAgreementTransItemViewMap
					{
						CompanyGUID = buyerAgreementTrans.CompanyGUID,
						Owner = buyerAgreementTrans.Owner,
						OwnerBusinessUnitGUID = buyerAgreementTrans.OwnerBusinessUnitGUID,
						BuyerAgreementTransGUID = buyerAgreementTrans.BuyerAgreementTransGUID,
						BuyerAgreementTableGUID = buyerAgreementTrans.BuyerAgreementTableGUID,
						BuyerAgreementLineGUID = buyerAgreementTrans.BuyerAgreementLineGUID,
						RefGUID = buyerAgreementTrans.RefGUID,
						RefType = buyerAgreementTrans.RefType,
						BuyerAgreementTable_BuyerAgreementId = buyerAgreementTable.BuyerAgreementId,
						BuyerAgreementLine_Description = (buyerAgreementLine != null) ? buyerAgreementLine.Description : null
					});
		}
		public IEnumerable<SelectItem<BuyerAgreementTransItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<BuyerAgreementTrans>(search, SysParm.CompanyGUID);
				var buyerAgreementTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<BuyerAgreementTransItemViewMap, BuyerAgreementTransItemView>().ToDropDownItem(search.ExcludeRowData);


				return buyerAgreementTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<BuyerAgreementTransItemView>> GetDropDownItemByWithdrawalTable(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<BuyerAgreementTrans>(search, SysParm.CompanyGUID);
				var buyerAgreementTrans = GetDropDownQueryByWithdrawalTable().Where(predicate.Predicates, predicate.Values).ToMaps<BuyerAgreementTransItemViewMap, BuyerAgreementTransItemView>().ToDropDownItemByWithdrawal();
				return buyerAgreementTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<BuyerAgreementTransListViewMap> GetListQuery()
		{
			return (from buyerAgreementTrans in Entity
					join buyerAgreementTable in db.Set<BuyerAgreementTable>()
					on buyerAgreementTrans.BuyerAgreementTableGUID equals buyerAgreementTable.BuyerAgreementTableGUID
					join buyerAgreementLine in db.Set<BuyerAgreementLine>()
					on buyerAgreementTrans.BuyerAgreementLineGUID equals buyerAgreementLine.BuyerAgreementLineGUID
					select new BuyerAgreementTransListViewMap
				{
						CompanyGUID = buyerAgreementTrans.CompanyGUID,
						Owner = buyerAgreementTrans.Owner,
						OwnerBusinessUnitGUID = buyerAgreementTrans.OwnerBusinessUnitGUID,
						BuyerAgreementTransGUID = buyerAgreementTrans.BuyerAgreementTransGUID,
						BuyerAgreementTableGUID = buyerAgreementTrans.BuyerAgreementTableGUID,
						BuyerAgreementLineGUID = buyerAgreementTrans.BuyerAgreementLineGUID,
						BuyerAgreementTable_Values = SmartAppUtil.GetDropDownLabel(buyerAgreementTable.ReferenceAgreementID, buyerAgreementTable.Description),
						BuyerAgreementLine_Values = SmartAppUtil.GetDropDownLabel(buyerAgreementLine.Period.ToString(), buyerAgreementLine.Description),
						BuyerAgreementLine_DueDate = buyerAgreementLine.DueDate,
						BuyerAgreementLine_Period = buyerAgreementLine.Period,
						BuyerAgreementLine_Amount = buyerAgreementLine.Amount,
						BuyerAgreementTable_BuyerAgreementId = buyerAgreementTable.BuyerAgreementId,
						RefGUID = buyerAgreementTrans.RefGUID
					});
		}
		public SearchResult<BuyerAgreementTransListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<BuyerAgreementTransListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<BuyerAgreementTrans>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<BuyerAgreementTransListViewMap, BuyerAgreementTransListView>();
				result = list.SetSearchResult<BuyerAgreementTransListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<BuyerAgreementTransItemViewMap> GetItemQuery()
		{
			return (from buyerAgreementTrans in Entity
					join company in db.Set<Company>()
					on buyerAgreementTrans.CompanyGUID equals company.CompanyGUID
					join buyerAgreementLine in db.Set<BuyerAgreementLine>()
					on buyerAgreementTrans.BuyerAgreementLineGUID equals buyerAgreementLine.BuyerAgreementLineGUID
					join ownerBU in db.Set<BusinessUnit>()
					on buyerAgreementTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljBuyerAgreementTransOwnerBU
					from ownerBU in ljBuyerAgreementTransOwnerBU.DefaultIfEmpty()
					join creditAppRequestLine in db.Set<CreditAppRequestLine>()
					on buyerAgreementTrans.RefGUID equals creditAppRequestLine.CreditAppRequestLineGUID into ljCreditAppRequestLine
					from creditAppRequestLine in ljCreditAppRequestLine.DefaultIfEmpty()
					join creditAppLine in db.Set<CreditAppLine>()
					on buyerAgreementTrans.RefGUID equals creditAppLine.CreditAppLineGUID into ljCreditAppLine
					from creditAppLine in ljCreditAppLine.DefaultIfEmpty()

					join withdrawalTable in db.Set<WithdrawalTable>()
					on buyerAgreementTrans.RefGUID equals withdrawalTable.WithdrawalTableGUID into ljWithdrawalTable
					from withdrawalTable in ljWithdrawalTable.DefaultIfEmpty()
					select new BuyerAgreementTransItemViewMap
					{
						CompanyGUID = buyerAgreementTrans.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = buyerAgreementTrans.Owner,
						OwnerBusinessUnitGUID = buyerAgreementTrans.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = buyerAgreementTrans.CreatedBy,
						CreatedDateTime = buyerAgreementTrans.CreatedDateTime,
						ModifiedBy = buyerAgreementTrans.ModifiedBy,
						ModifiedDateTime = buyerAgreementTrans.ModifiedDateTime,
						BuyerAgreementTransGUID = buyerAgreementTrans.BuyerAgreementTransGUID,
						BuyerAgreementLineGUID = buyerAgreementTrans.BuyerAgreementLineGUID,
						BuyerAgreementTableGUID = buyerAgreementTrans.BuyerAgreementTableGUID,
						RefGUID = buyerAgreementTrans.RefGUID,
						RefType = buyerAgreementTrans.RefType,
						BuyerAgreementLine_DueDate = buyerAgreementLine.DueDate,
						BuyerAgreementLine_Amount = buyerAgreementLine.Amount,
						RefId = (creditAppRequestLine != null && buyerAgreementTrans.RefType == (int)RefType.CreditAppRequestLine) ? creditAppRequestLine.LineNum.ToString():
								(creditAppLine != null && buyerAgreementTrans.RefType == (int)RefType.CreditAppLine) ? creditAppLine.LineNum.ToString() :
								(withdrawalTable != null && buyerAgreementTrans.RefType == (int)RefType.WithdrawalTable) ? withdrawalTable.WithdrawalId :
								 null,
					
						RowVersion = buyerAgreementTrans.RowVersion,
					});
		}
		public BuyerAgreementTransItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<BuyerAgreementTransItemViewMap, BuyerAgreementTransItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public BuyerAgreementTrans CreateBuyerAgreementTrans(BuyerAgreementTrans buyerAgreementTrans)
		{
			try
			{
				buyerAgreementTrans.BuyerAgreementTransGUID = Guid.NewGuid();
				base.Add(buyerAgreementTrans);
				return buyerAgreementTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateBuyerAgreementTransVoid(BuyerAgreementTrans buyerAgreementTrans)
		{
			try
			{
				CreateBuyerAgreementTrans(buyerAgreementTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BuyerAgreementTrans UpdateBuyerAgreementTrans(BuyerAgreementTrans dbBuyerAgreementTrans, BuyerAgreementTrans inputBuyerAgreementTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBuyerAgreementTrans = dbBuyerAgreementTrans.MapUpdateValues<BuyerAgreementTrans>(inputBuyerAgreementTrans);
				base.Update(dbBuyerAgreementTrans);
				return dbBuyerAgreementTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateBuyerAgreementTransVoid(BuyerAgreementTrans dbBuyerAgreementTrans, BuyerAgreementTrans inputBuyerAgreementTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBuyerAgreementTrans = dbBuyerAgreementTrans.MapUpdateValues<BuyerAgreementTrans>(inputBuyerAgreementTrans, skipUpdateFields);
				base.Update(dbBuyerAgreementTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public BuyerAgreementTrans GetBuyerAgreementTransByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BuyerAgreementTransGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BuyerAgreementTrans GetBuyerAgreementTransByReferenceNoTracking(Guid guid,int refType)
		{
			try
			{
				var result = Entity.Where(item => item.RefGUID == guid && item.RefType == refType)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool IsExistByWithdrawal(Guid withdrawalTableGUID)
		{
			try
			{
				IEnumerable<BuyerAgreementTrans> list = GetBuyerAgreementTransByReferanceNoTracking((int)RefType.WithdrawalTable, withdrawalTableGUID);

				return (list.Count() > 0) ? true : false;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<BuyerAgreementTrans> GetBuyerAgreementTransByReferanceNoTracking(int refType, Guid refGUID)
		{
			try
			{
				var result = Entity.Where(item => item.RefGUID == refGUID && item.RefType == refType)
					.AsNoTracking().ToList();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<BuyerAgreementTrans> GetBuyerAgreementTransCaReqLineByCaReqIdNoTracking(Guid creditAppRequestTableGUID)
		{
			try
			{
				var result = (from creditAppReqTable in db.Set<CreditAppRequestTable>()
							  join creditAppReqLine in db.Set<CreditAppRequestLine>()
							  on creditAppReqTable.CreditAppRequestTableGUID equals creditAppReqLine.CreditAppRequestTableGUID into ljCreditAppReqTableLine
							  from creditAppReqLine in ljCreditAppReqTableLine.DefaultIfEmpty()
							  join buyerAgreementTrans in Entity 
							  on creditAppReqLine.CreditAppRequestLineGUID equals buyerAgreementTrans.RefGUID
							  where creditAppReqTable.CreditAppRequestTableGUID == creditAppRequestTableGUID
							  && buyerAgreementTrans.RefType == (int)RefType.CreditAppRequestLine
							  select buyerAgreementTrans).AsNoTracking();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region function
		public CopyBuyerAgreementTransView GetCopyBuyerAgreementTranByAppReqLineGUID(Guid guid)
		{
			try
			{
				var result = (from creditAppReqTable in db.Set<CreditAppRequestTable>()
							  join creditAppReqLine in db.Set<CreditAppRequestLine>()
							  on creditAppReqTable.CreditAppRequestTableGUID equals creditAppReqLine.CreditAppRequestTableGUID into ljCreditAppReqTableLine
							  from creditAppReqLine in ljCreditAppReqTableLine.DefaultIfEmpty()
							  where creditAppReqLine.CreditAppRequestLineGUID == guid
							  select new CopyBuyerAgreementTransView
							  {
								  CustomerTableGUID = creditAppReqTable.CustomerTableGUID.ToString(),
								  BuyerTableGUID = creditAppReqLine.BuyerTableGUID.ToString(),
								  RefType =(int)RefType.CreditAppRequestLine,
								  RefGUID = creditAppReqLine.CreditAppRequestLineGUID.ToString()
							  }).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<BuyerAgreementTrans> GetCopyBuyerAgreementTransByReference(Guid refGUID, int refType)
		{
			try
			{
				IEnumerable<BuyerAgreementTrans> list = Entity.Where(w => w.RefGUID == refGUID && w.RefType == refType)
					.AsNoTracking().ToList();
				return list;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}

