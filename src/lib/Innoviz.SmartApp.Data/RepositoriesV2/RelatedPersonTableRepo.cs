using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IRelatedPersonTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<RelatedPersonTableItemView>> GetDropDownItem(SearchParameter search);
		IEnumerable<SelectItem<RelatedPersonTableItemView>> GetDropDownItemByCreditAppRequestTable(SearchParameter search);
		IEnumerable<SelectItem<RelatedPersonTableItemView>> GetDropDownItemByCreditAppRequestTableGuarantor(SearchParameter search);
		IEnumerable<SelectItem<RelatedPersonTableItemView>> GetDropDownItemByCreditAppRequestTableOwnerTrans(SearchParameter search);
		IEnumerable<SelectItem<RelatedPersonTableItemView>> GetDropDownItemByCreditAppRequestTableAuthorizedPersonTrans(SearchParameter search);
		IEnumerable<SelectItem<RelatedPersonTableItemView>> GetDropDownItemByAmendCa(SearchParameter search);
		#endregion DropDown
		SearchResult<RelatedPersonTableListView> GetListvw(SearchParameter search);
		RelatedPersonTableItemView GetByIdvw(Guid id);
		RelatedPersonTable Find(params object[] keyValues);
		RelatedPersonTable GetRelatedPersonTableByIdNoTracking(Guid guid);
		RelatedPersonTable CreateRelatedPersonTable(RelatedPersonTable relatedPersonTable);
		void CreateRelatedPersonTableVoid(RelatedPersonTable relatedPersonTable);
		RelatedPersonTable UpdateRelatedPersonTable(RelatedPersonTable dbRelatedPersonTable, RelatedPersonTable inputRelatedPersonTable, List<string> skipUpdateFields = null);
		void UpdateRelatedPersonTableVoid(RelatedPersonTable dbRelatedPersonTable, RelatedPersonTable inputRelatedPersonTable, List<string> skipUpdateFields = null);
		void Remove(RelatedPersonTable item);
		RelatedPersonTable GetRelatedPersonTableByIdNoTrackingByAccessLevel(Guid guid);
		void ValidateAdd(RelatedPersonTable item);
		void ValidateAdd(IEnumerable<RelatedPersonTable> items);
	}
	public class RelatedPersonTableRepo : CompanyBaseRepository<RelatedPersonTable>, IRelatedPersonTableRepo
	{
		public RelatedPersonTableRepo(SmartAppDbContext context) : base(context) { }
		public RelatedPersonTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public RelatedPersonTable GetRelatedPersonTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.RelatedPersonTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #region DropDown
        #region Default
        private IQueryable<RelatedPersonTableItemViewMap> GetDropDownQuery()
		{
			return (from relatedPersonTable in Entity
					join nationality in db.Set<Nationality>()
					on relatedPersonTable.NationalityGUID equals nationality.NationalityGUID into ljNationality
					from nationality in ljNationality.DefaultIfEmpty()
					join race in db.Set<Race>()
					on relatedPersonTable.RaceGUID equals race.RaceGUID into ljRace
					from race in ljRace.DefaultIfEmpty()
					select new RelatedPersonTableItemViewMap
					{
						CompanyGUID = relatedPersonTable.CompanyGUID,
						Owner = relatedPersonTable.Owner,
						OwnerBusinessUnitGUID = relatedPersonTable.OwnerBusinessUnitGUID,
						RelatedPersonTableGUID = relatedPersonTable.RelatedPersonTableGUID,
						RelatedPersonId = relatedPersonTable.RelatedPersonId,
						Name = relatedPersonTable.Name,
						IdentificationType = relatedPersonTable.IdentificationType,
						TaxId = relatedPersonTable.TaxId,
						PassportId = relatedPersonTable.PassportId,
						WorkPermitId = relatedPersonTable.WorkPermitId,
						DateOfBirth = relatedPersonTable.DateOfBirth,
						Nationality_NationalityId = SmartAppUtil.GetDropDownLabel(nationality.NationalityId, nationality.Description),
						Race_RaceId = SmartAppUtil.GetDropDownLabel(race.RaceId, race.Description),
						Phone = relatedPersonTable.Phone,
						Extension = relatedPersonTable.Extension,
						Mobile = relatedPersonTable.Mobile,
						Fax = relatedPersonTable.Fax,
						Email = relatedPersonTable.Email,
						LineId = relatedPersonTable.LineId,
						BackgroundSummary = relatedPersonTable.BackgroundSummary,
						Age = relatedPersonTable.DateOfBirth == null ? 0 : Convert.ToInt32(Math.Floor(DateTime.Now.Subtract(relatedPersonTable.DateOfBirth.Value).TotalDays / 365.25)),
						Position = relatedPersonTable.Position,

						Nationality_Values = SmartAppUtil.GetDropDownLabel(nationality.NationalityId, nationality.Description),
						Race_Values = SmartAppUtil.GetDropDownLabel(race.RaceId, race.Description),

					});
		}
		public IEnumerable<SelectItem<RelatedPersonTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<RelatedPersonTable>(search, SysParm.CompanyGUID);
				var relatedPersonTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<RelatedPersonTableItemViewMap, RelatedPersonTableItemView>().ToDropDownItem(search.ExcludeRowData);
				return relatedPersonTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Default
		private IQueryable<RelatedPersonTableItemViewMap> GetDropDownItemByCreditAppRequestTable()
		{
			return (from relatedPersonTable in Entity
					join authorizedPersonTrans in db.Set<AuthorizedPersonTrans>() on relatedPersonTable.RelatedPersonTableGUID equals authorizedPersonTrans.RelatedPersonTableGUID
					join customerTable in db.Set<CustomerTable>() on authorizedPersonTrans.RefGUID equals customerTable.CustomerTableGUID
					join creditAppRequestTable in db.Set<CreditAppRequestTable>() on customerTable.CustomerTableGUID equals creditAppRequestTable.CustomerTableGUID
                    join nationality in db.Set<Nationality>()
                    on relatedPersonTable.NationalityGUID equals nationality.NationalityGUID into ljNationality
                    from nationality in ljNationality.DefaultIfEmpty()
                    join race in db.Set<Race>()
                    on relatedPersonTable.RaceGUID equals race.RaceGUID into ljRace
                    from race in ljRace.DefaultIfEmpty()
                    select new RelatedPersonTableItemViewMap
					{
						CompanyGUID = relatedPersonTable.CompanyGUID,
						Owner = relatedPersonTable.Owner,
						OwnerBusinessUnitGUID = relatedPersonTable.OwnerBusinessUnitGUID,
						RelatedPersonTableGUID = relatedPersonTable.RelatedPersonTableGUID,
						RelatedPersonId = relatedPersonTable.RelatedPersonId,
						Name = relatedPersonTable.Name,
						IdentificationType = relatedPersonTable.IdentificationType,
						TaxId = relatedPersonTable.TaxId,
						PassportId = relatedPersonTable.PassportId,
						WorkPermitId = relatedPersonTable.WorkPermitId,
						DateOfBirth = relatedPersonTable.DateOfBirth,
                        Nationality_NationalityId = nationality.NationalityId,
                        Race_RaceId = race.RaceId,
                        Phone = relatedPersonTable.Phone,
						Extension = relatedPersonTable.Extension,
						Mobile = relatedPersonTable.Mobile,
						Fax = relatedPersonTable.Fax,
						Email = relatedPersonTable.Email,
						LineId = relatedPersonTable.LineId,
						BackgroundSummary = relatedPersonTable.BackgroundSummary,
						Age = relatedPersonTable.DateOfBirth == null ? 0 : Convert.ToInt32(Math.Floor(DateTime.Now.Subtract(relatedPersonTable.DateOfBirth.Value).TotalDays / 365.25)),
						Nationality_Values = SmartAppUtil.GetDropDownLabel(nationality.NationalityId, nationality.Description),
						Race_Values = SmartAppUtil.GetDropDownLabel(race.RaceId, race.Description),

						#region GetRelatedPersonTableByCreditAppTableDropDown
						CreditAppRequestTable_CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
						Reference_RefType = authorizedPersonTrans.RefType,
						Reference_Inactive = authorizedPersonTrans.InActive,
						#endregion GetRelatedPersonTableByCreditAppTableDropDown
					});
		}
		public IEnumerable<SelectItem<RelatedPersonTableItemView>> GetDropDownItemByCreditAppRequestTable(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<RelatedPersonTable>(search, SysParm.CompanyGUID);
				var relatedPersonTable = GetDropDownItemByCreditAppRequestTable().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<RelatedPersonTableItemViewMap, RelatedPersonTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return relatedPersonTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private IQueryable<RelatedPersonTableItemViewMap> GetDropDownItemByCreditAppRequestTableGuarantor()
		{
			return (from relatedPersonTable in Entity
					join guarantorTrans in db.Set<GuarantorTrans>() on relatedPersonTable.RelatedPersonTableGUID equals guarantorTrans.RelatedPersonTableGUID
					join customerTable in db.Set<CustomerTable>() on guarantorTrans.RefGUID equals customerTable.CustomerTableGUID
					join creditAppRequestTable in db.Set<CreditAppRequestTable>() on customerTable.CustomerTableGUID equals creditAppRequestTable.CustomerTableGUID
					join nationality in db.Set<Nationality>()
					on relatedPersonTable.NationalityGUID equals nationality.NationalityGUID into ljNationality
					from nationality in ljNationality.DefaultIfEmpty()
					join race in db.Set<Race>()
					on relatedPersonTable.RaceGUID equals race.RaceGUID into ljRace
					from race in ljRace.DefaultIfEmpty()
					select new RelatedPersonTableItemViewMap
					{
						CompanyGUID = relatedPersonTable.CompanyGUID,
						Owner = relatedPersonTable.Owner,
						OwnerBusinessUnitGUID = relatedPersonTable.OwnerBusinessUnitGUID,
						RelatedPersonTableGUID = relatedPersonTable.RelatedPersonTableGUID,
						RelatedPersonId = relatedPersonTable.RelatedPersonId,
						Name = relatedPersonTable.Name,
						IdentificationType = relatedPersonTable.IdentificationType,
						TaxId = relatedPersonTable.TaxId,
						PassportId = relatedPersonTable.PassportId,
						WorkPermitId = relatedPersonTable.WorkPermitId,
						DateOfBirth = relatedPersonTable.DateOfBirth,
						Nationality_NationalityId = nationality.NationalityId,
						Race_RaceId = race.RaceId,
						Phone = relatedPersonTable.Phone,
						Extension = relatedPersonTable.Extension,
						Mobile = relatedPersonTable.Mobile,
						Fax = relatedPersonTable.Fax,
						Email = relatedPersonTable.Email,
						LineId = relatedPersonTable.LineId,
						BackgroundSummary = relatedPersonTable.BackgroundSummary,
						Age = relatedPersonTable.DateOfBirth == null ? 0 : Convert.ToInt32(Math.Floor(DateTime.Now.Subtract(relatedPersonTable.DateOfBirth.Value).TotalDays / 365.25)),
						Nationality_Values = SmartAppUtil.GetDropDownLabel(nationality.NationalityId, nationality.Description),
						Race_Values = SmartAppUtil.GetDropDownLabel(race.RaceId, race.Description),

						#region GetRelatedPersonTableByCreditAppTableDropDown
						CreditAppRequestTable_CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
						Reference_RefType = guarantorTrans.RefType,
						Reference_Inactive = guarantorTrans.InActive,
						#endregion GetRelatedPersonTableByCreditAppTableDropDown
					});
		}
		public IEnumerable<SelectItem<RelatedPersonTableItemView>> GetDropDownItemByCreditAppRequestTableGuarantor(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<RelatedPersonTable>(search, SysParm.CompanyGUID);
				var relatedPersonTable = GetDropDownItemByCreditAppRequestTableGuarantor().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<RelatedPersonTableItemViewMap, RelatedPersonTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return relatedPersonTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private IQueryable<RelatedPersonTableItemViewMap> GetDropDownItemByCreditAppRequestTableOwnerTrans()
		{
			return (from relatedPersonTable in Entity
					join ownerTrans in db.Set<OwnerTrans>() on relatedPersonTable.RelatedPersonTableGUID equals ownerTrans.RelatedPersonTableGUID
					join customerTable in db.Set<CustomerTable>() on ownerTrans.RefGUID equals customerTable.CustomerTableGUID
					join creditAppRequestTable in db.Set<CreditAppRequestTable>() on customerTable.CustomerTableGUID equals creditAppRequestTable.CustomerTableGUID
					join nationality in db.Set<Nationality>()
					on relatedPersonTable.NationalityGUID equals nationality.NationalityGUID into ljNationality
					from nationality in ljNationality.DefaultIfEmpty()
					join race in db.Set<Race>()
					on relatedPersonTable.RaceGUID equals race.RaceGUID into ljRace
					from race in ljRace.DefaultIfEmpty()
					select new RelatedPersonTableItemViewMap
					{
						CompanyGUID = relatedPersonTable.CompanyGUID,
						Owner = relatedPersonTable.Owner,
						OwnerBusinessUnitGUID = relatedPersonTable.OwnerBusinessUnitGUID,
						RelatedPersonTableGUID = relatedPersonTable.RelatedPersonTableGUID,
						RelatedPersonId = relatedPersonTable.RelatedPersonId,
						Name = relatedPersonTable.Name,
						IdentificationType = relatedPersonTable.IdentificationType,
						TaxId = relatedPersonTable.TaxId,
						PassportId = relatedPersonTable.PassportId,
						WorkPermitId = relatedPersonTable.WorkPermitId,
						DateOfBirth = relatedPersonTable.DateOfBirth,
						Nationality_NationalityId = nationality.NationalityId,
						Race_RaceId = race.RaceId,
						Phone = relatedPersonTable.Phone,
						Extension = relatedPersonTable.Extension,
						Mobile = relatedPersonTable.Mobile,
						Fax = relatedPersonTable.Fax,
						Email = relatedPersonTable.Email,
						LineId = relatedPersonTable.LineId,
						BackgroundSummary = relatedPersonTable.BackgroundSummary,
						Age = relatedPersonTable.DateOfBirth == null ? 0 : Convert.ToInt32(Math.Floor(DateTime.Now.Subtract(relatedPersonTable.DateOfBirth.Value).TotalDays / 365.25)),
						Nationality_Values = SmartAppUtil.GetDropDownLabel(nationality.NationalityId, nationality.Description),
						Race_Values = SmartAppUtil.GetDropDownLabel(race.RaceId, race.Description),

						#region GetRelatedPersonTableByCreditAppTableDropDown
						CreditAppRequestTable_CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
						Reference_RefType = ownerTrans.RefType,
						Reference_Inactive = ownerTrans.InActive,
						#endregion GetRelatedPersonTableByCreditAppTableDropDown
					});
		}
		public IEnumerable<SelectItem<RelatedPersonTableItemView>> GetDropDownItemByCreditAppRequestTableOwnerTrans(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<RelatedPersonTable>(search, SysParm.CompanyGUID);
				var relatedPersonTable = GetDropDownItemByCreditAppRequestTableOwnerTrans().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<RelatedPersonTableItemViewMap, RelatedPersonTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return relatedPersonTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private IQueryable<RelatedPersonTableItemViewMap> GetDropDownItemByCreditAppRequestTableAuthorizedPersonTrans()
		{
			return (from relatedPersonTable in Entity
					join authorizedPersonTrans in db.Set<AuthorizedPersonTrans>() on relatedPersonTable.RelatedPersonTableGUID equals authorizedPersonTrans.RelatedPersonTableGUID
					join customerTable in db.Set<CustomerTable>() on authorizedPersonTrans.RefGUID equals customerTable.CustomerTableGUID
					join creditAppRequestTable in db.Set<CreditAppRequestTable>() on customerTable.CustomerTableGUID equals creditAppRequestTable.CustomerTableGUID
					join nationality in db.Set<Nationality>()
					on relatedPersonTable.NationalityGUID equals nationality.NationalityGUID into ljNationality
					from nationality in ljNationality.DefaultIfEmpty()
					join race in db.Set<Race>()
					on relatedPersonTable.RaceGUID equals race.RaceGUID into ljRace
					from race in ljRace.DefaultIfEmpty()
					select new RelatedPersonTableItemViewMap
					{
						CompanyGUID = relatedPersonTable.CompanyGUID,
						Owner = relatedPersonTable.Owner,
						OwnerBusinessUnitGUID = relatedPersonTable.OwnerBusinessUnitGUID,
						RelatedPersonTableGUID = relatedPersonTable.RelatedPersonTableGUID,
						RelatedPersonId = relatedPersonTable.RelatedPersonId,
						Name = relatedPersonTable.Name,
						IdentificationType = relatedPersonTable.IdentificationType,
						TaxId = relatedPersonTable.TaxId,
						PassportId = relatedPersonTable.PassportId,
						WorkPermitId = relatedPersonTable.WorkPermitId,
						DateOfBirth = relatedPersonTable.DateOfBirth,
						Nationality_NationalityId = nationality.NationalityId,
						Race_RaceId = race.RaceId,
						Phone = relatedPersonTable.Phone,
						Extension = relatedPersonTable.Extension,
						Mobile = relatedPersonTable.Mobile,
						Fax = relatedPersonTable.Fax,
						Email = relatedPersonTable.Email,
						LineId = relatedPersonTable.LineId,
						BackgroundSummary = relatedPersonTable.BackgroundSummary,
						Age = relatedPersonTable.DateOfBirth == null ? 0 : Convert.ToInt32(Math.Floor(DateTime.Now.Subtract(relatedPersonTable.DateOfBirth.Value).TotalDays / 365.25)),
						Nationality_Values = SmartAppUtil.GetDropDownLabel(nationality.NationalityId, nationality.Description),
						Race_Values = SmartAppUtil.GetDropDownLabel(race.RaceId, race.Description),

						#region GetRelatedPersonTableByCreditAppTableDropDown
						CreditAppRequestTable_CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
						Reference_RefType = authorizedPersonTrans.RefType,
						Reference_Inactive = authorizedPersonTrans.InActive,
						#endregion GetRelatedPersonTableByCreditAppTableDropDown
					});
		}
		public IEnumerable<SelectItem<RelatedPersonTableItemView>> GetDropDownItemByCreditAppRequestTableAuthorizedPersonTrans(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<RelatedPersonTable>(search, SysParm.CompanyGUID);
				var relatedPersonTable = GetDropDownItemByCreditAppRequestTableAuthorizedPersonTrans().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<RelatedPersonTableItemViewMap, RelatedPersonTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return relatedPersonTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private IQueryable<RelatedPersonTableItemViewMap> GetDropDownItemByAmendCA()
		{
			return (from relatedPersonTable in Entity
					join authorizedPersonTrans in db.Set<AuthorizedPersonTrans>() on relatedPersonTable.RelatedPersonTableGUID equals authorizedPersonTrans.RelatedPersonTableGUID
					join creditAppRequestTable in db.Set<CreditAppRequestTable>() on authorizedPersonTrans.RefGUID equals creditAppRequestTable.CreditAppRequestTableGUID
					join nationality in db.Set<Nationality>()
					on relatedPersonTable.NationalityGUID equals nationality.NationalityGUID into ljNationality
					from nationality in ljNationality.DefaultIfEmpty()
					join race in db.Set<Race>()
					on relatedPersonTable.RaceGUID equals race.RaceGUID into ljRace
					from race in ljRace.DefaultIfEmpty()
					select new RelatedPersonTableItemViewMap
					{
						CompanyGUID = relatedPersonTable.CompanyGUID,
						Owner = relatedPersonTable.Owner,
						OwnerBusinessUnitGUID = relatedPersonTable.OwnerBusinessUnitGUID,
						RelatedPersonTableGUID = relatedPersonTable.RelatedPersonTableGUID,
						RelatedPersonId = relatedPersonTable.RelatedPersonId,
						Name = relatedPersonTable.Name,
						IdentificationType = relatedPersonTable.IdentificationType,
						TaxId = relatedPersonTable.TaxId,
						PassportId = relatedPersonTable.PassportId,
						WorkPermitId = relatedPersonTable.WorkPermitId,
						DateOfBirth = relatedPersonTable.DateOfBirth,
						Nationality_NationalityId = nationality.NationalityId,
						Race_RaceId = race.RaceId,
						Phone = relatedPersonTable.Phone,
						Extension = relatedPersonTable.Extension,
						Mobile = relatedPersonTable.Mobile,
						Fax = relatedPersonTable.Fax,
						Email = relatedPersonTable.Email,
						LineId = relatedPersonTable.LineId,
						BackgroundSummary = relatedPersonTable.BackgroundSummary,
						Age = relatedPersonTable.DateOfBirth == null ? 0 : Convert.ToInt32(Math.Floor(DateTime.Now.Subtract(relatedPersonTable.DateOfBirth.Value).TotalDays / 365.25)),
						Nationality_Values = SmartAppUtil.GetDropDownLabel(nationality.NationalityId, nationality.Description),
						Race_Values = SmartAppUtil.GetDropDownLabel(race.RaceId, race.Description),

						#region GetRelatedPersonTableByCreditAppTableDropDown
						CreditAppRequestTable_CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
						Reference_RefType = authorizedPersonTrans.RefType,
						Reference_Inactive = authorizedPersonTrans.InActive,
						#endregion GetRelatedPersonTableByCreditAppTableDropDown
					});
		}
		public IEnumerable<SelectItem<RelatedPersonTableItemView>> GetDropDownItemByAmendCa(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<RelatedPersonTable>(search, SysParm.CompanyGUID);
				var relatedPersonTable = GetDropDownItemByAmendCA().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<RelatedPersonTableItemViewMap, RelatedPersonTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return relatedPersonTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#endregion DropDown
		#region GetListvw
		private IQueryable<RelatedPersonTableListViewMap> GetListQuery()
		{
			return (from relatedPersonTable in Entity
				select new RelatedPersonTableListViewMap
				{
						CompanyGUID = relatedPersonTable.CompanyGUID,
						Owner = relatedPersonTable.Owner,
						OwnerBusinessUnitGUID = relatedPersonTable.OwnerBusinessUnitGUID,
						RelatedPersonTableGUID = relatedPersonTable.RelatedPersonTableGUID,
						RelatedPersonId = relatedPersonTable.RelatedPersonId,
						Name = relatedPersonTable.Name,
						RecordType = relatedPersonTable.RecordType,
						TaxId = relatedPersonTable.TaxId,
						PassportId = relatedPersonTable.PassportId,
						Position = relatedPersonTable.Position,
						CompanyName = relatedPersonTable.CompanyName,
						OperatedBy = relatedPersonTable.OperatedBy
				});
		}
		public SearchResult<RelatedPersonTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<RelatedPersonTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<RelatedPersonTable>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<RelatedPersonTableListViewMap, RelatedPersonTableListView>();
				result = list.SetSearchResult<RelatedPersonTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<RelatedPersonTableItemViewMap> GetItemQuery()
		{
			return (from relatedPersonTable in Entity
					join ownerBU in db.Set<BusinessUnit>()
					on relatedPersonTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljRelatedPersonTableOwnerBU
					from ownerBU in ljRelatedPersonTableOwnerBU.DefaultIfEmpty()
					join company in db.Set<Company>() on relatedPersonTable.CompanyGUID equals company.CompanyGUID
					select new RelatedPersonTableItemViewMap
					{
						CompanyGUID = relatedPersonTable.CompanyGUID,
						Owner = relatedPersonTable.Owner,
						OwnerBusinessUnitGUID = relatedPersonTable.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CompanyId  = company.CompanyId,
						CreatedBy = relatedPersonTable.CreatedBy,
						CreatedDateTime = relatedPersonTable.CreatedDateTime,
						ModifiedBy = relatedPersonTable.ModifiedBy,
						ModifiedDateTime = relatedPersonTable.ModifiedDateTime,
						RelatedPersonTableGUID = relatedPersonTable.RelatedPersonTableGUID,
						BackgroundSummary = relatedPersonTable.BackgroundSummary,
						CompanyName = relatedPersonTable.CompanyName,
						DateOfBirth = relatedPersonTable.DateOfBirth,
						DateOfEstablishment = relatedPersonTable.DateOfEstablishment,
						DateOfExpiry = relatedPersonTable.DateOfExpiry,
						DateOfIssue = relatedPersonTable.DateOfIssue,
						Email = relatedPersonTable.Email,
						Extension = relatedPersonTable.Extension,
						Fax = relatedPersonTable.Fax,
						GenderGUID = relatedPersonTable.GenderGUID,
						IdentificationType = relatedPersonTable.IdentificationType,
						Income = relatedPersonTable.Income,
						IssuedBy = relatedPersonTable.IssuedBy,
						LineId = relatedPersonTable.LineId,
						MaritalStatusGUID = relatedPersonTable.MaritalStatusGUID,
						Mobile = relatedPersonTable.Mobile,
						Name = relatedPersonTable.Name,
						NationalityGUID = relatedPersonTable.NationalityGUID,
						OccupationGUID = relatedPersonTable.OccupationGUID,
						OtherIncome = relatedPersonTable.OtherIncome,
						OtherSourceIncome = relatedPersonTable.OtherSourceIncome,
						PaidUpCapital = relatedPersonTable.PaidUpCapital,
						PassportId = relatedPersonTable.PassportId,
						Phone = relatedPersonTable.Phone,
						Position = relatedPersonTable.Position,
						RaceGUID = relatedPersonTable.RaceGUID,
						RecordType = relatedPersonTable.RecordType,
						RegisteredCapital = relatedPersonTable.RegisteredCapital,
						RegistrationTypeGUID = relatedPersonTable.RegistrationTypeGUID,
						RelatedPersonId = relatedPersonTable.RelatedPersonId,
						SpouseName = relatedPersonTable.SpouseName,
						TaxId = relatedPersonTable.TaxId,
						WorkExperienceMonth = relatedPersonTable.WorkExperienceMonth,
						WorkExperienceYear = relatedPersonTable.WorkExperienceYear,
						WorkPermitId = relatedPersonTable.WorkPermitId,
						Age = relatedPersonTable.DateOfBirth == null ? 0 : Convert.ToInt32(Math.Floor(DateTime.Now.Subtract(relatedPersonTable.DateOfBirth.Value).TotalDays / 365.25)),
						OperatedBy = relatedPersonTable.OperatedBy,
					
						RowVersion = relatedPersonTable.RowVersion,
					});
		}
		public RelatedPersonTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<RelatedPersonTableItemViewMap, RelatedPersonTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public RelatedPersonTable CreateRelatedPersonTable(RelatedPersonTable relatedPersonTable)
		{
			try
			{
				relatedPersonTable.RelatedPersonTableGUID = Guid.NewGuid();
				base.Add(relatedPersonTable);
				return relatedPersonTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateRelatedPersonTableVoid(RelatedPersonTable relatedPersonTable)
		{
			try
			{
				CreateRelatedPersonTable(relatedPersonTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public RelatedPersonTable UpdateRelatedPersonTable(RelatedPersonTable dbRelatedPersonTable, RelatedPersonTable inputRelatedPersonTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbRelatedPersonTable = dbRelatedPersonTable.MapUpdateValues<RelatedPersonTable>(inputRelatedPersonTable);
				base.Update(dbRelatedPersonTable);
				return dbRelatedPersonTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateRelatedPersonTableVoid(RelatedPersonTable dbRelatedPersonTable, RelatedPersonTable inputRelatedPersonTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbRelatedPersonTable = dbRelatedPersonTable.MapUpdateValues<RelatedPersonTable>(inputRelatedPersonTable, skipUpdateFields);
				base.Update(dbRelatedPersonTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public RelatedPersonTable GetRelatedPersonTableByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.RelatedPersonTableGUID == guid)
									.FilterByAccessLevel(SysParm.AccessLevel)
									.AsNoTracking()
									.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}

