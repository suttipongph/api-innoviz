using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IBuyerInvoiceTableRepo
	{
		#region DropDown
		IEnumerable<SelectItem<BuyerInvoiceTableItemView>> GetDropDownItem(SearchParameter search);
		IEnumerable<SelectItem<BuyerInvoiceTableItemView>> GetDropDownItemByPurchaseLine(SearchParameter search);
		#endregion DropDown
		SearchResult<BuyerInvoiceTableListView> GetListvw(SearchParameter search);
		BuyerInvoiceTableItemView GetByIdvw(Guid id);
		BuyerInvoiceTable Find(params object[] keyValues);
		BuyerInvoiceTable GetBuyerInvoiceTableByIdNoTracking(Guid guid);
		BuyerInvoiceTable CreateBuyerInvoiceTable(BuyerInvoiceTable buyerInvoiceTable);
		void CreateBuyerInvoiceTableVoid(BuyerInvoiceTable buyerInvoiceTable);
		BuyerInvoiceTable UpdateBuyerInvoiceTable(BuyerInvoiceTable dbBuyerInvoiceTable, BuyerInvoiceTable inputBuyerInvoiceTable, List<string> skipUpdateFields = null);
		void UpdateBuyerInvoiceTableVoid(BuyerInvoiceTable dbBuyerInvoiceTable, BuyerInvoiceTable inputBuyerInvoiceTable, List<string> skipUpdateFields = null);
		void Remove(BuyerInvoiceTable item);
		BuyerInvoiceTable GetBuyerInvoiceTableByIdNoTrackingByAccessLevel(Guid guid);
		BuyerInvoiceTable GetBuyerInvoiceTableByCreditAppLineIdNoTracking(Guid guid);
		IEnumerable<BuyerInvoiceTable> GetListByCompany(string companyGUID = null);
		void ValidateAdd(BuyerInvoiceTable item);
		void ValidateAdd(IEnumerable<BuyerInvoiceTable> items);
	}
	public class BuyerInvoiceTableRepo : CompanyBaseRepository<BuyerInvoiceTable>, IBuyerInvoiceTableRepo
	{
		public BuyerInvoiceTableRepo(SmartAppDbContext context) : base(context) { }
		public BuyerInvoiceTableRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BuyerInvoiceTable GetBuyerInvoiceTableByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BuyerInvoiceTableGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<BuyerInvoiceTableItemViewMap> GetDropDownQuery()
		{
			return (from buyerInvoiceTable in Entity
					join creditAppLine in db.Set<CreditAppLine>()
					on buyerInvoiceTable.CreditAppLineGUID equals creditAppLine.CreditAppLineGUID into ljBuyerInvoiceTablecreditAppLine
					from creditAppLine in ljBuyerInvoiceTablecreditAppLine.DefaultIfEmpty()
					select new BuyerInvoiceTableItemViewMap
					{
						CompanyGUID = buyerInvoiceTable.CompanyGUID,
						Owner = buyerInvoiceTable.Owner,
						OwnerBusinessUnitGUID = buyerInvoiceTable.OwnerBusinessUnitGUID,
						BuyerInvoiceTableGUID = buyerInvoiceTable.BuyerInvoiceTableGUID,
						BuyerInvoiceId = buyerInvoiceTable.BuyerInvoiceId,
						CreditAppLineGUID = buyerInvoiceTable.CreditAppLineGUID,
						CreditAppLine_CreditAppTableGUID = creditAppLine.CreditAppTableGUID
					});
		}
		private IQueryable<BuyerInvoiceTableItemViewMap> GetDropDownQueryByPurchaseLine()
		{
			IQueryable<BuyerInvoiceTableItemViewMap> buyerInvoiceTableItemViewMaps = (from buyerInvoiceTable in Entity
																					  join creditAppLine in db.Set<CreditAppLine>()
																					  on buyerInvoiceTable.CreditAppLineGUID equals creditAppLine.CreditAppLineGUID
																					  select new BuyerInvoiceTableItemViewMap
																					  {
																						  CompanyGUID = buyerInvoiceTable.CompanyGUID,
																						  Owner = buyerInvoiceTable.Owner,
																						  OwnerBusinessUnitGUID = buyerInvoiceTable.OwnerBusinessUnitGUID,
																						  BuyerInvoiceTableGUID = buyerInvoiceTable.BuyerInvoiceTableGUID,
																						  BuyerInvoiceId = buyerInvoiceTable.BuyerInvoiceId,
																						  CreditAppLine_CreditAppTableGUID = creditAppLine.CreditAppTableGUID,
																						  CreditAppLineGUID = buyerInvoiceTable.CreditAppLineGUID,
																						  Amount = buyerInvoiceTable.Amount,
																						  BuyerAgreementTableGUID = buyerInvoiceTable.BuyerAgreementTableGUID,
																					  });
			return buyerInvoiceTableItemViewMaps;
		}
		public IEnumerable<SelectItem<BuyerInvoiceTableItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<BuyerInvoiceTable>(search, SysParm.CompanyGUID);
				var buyerInvoiceTable = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<BuyerInvoiceTableItemViewMap, BuyerInvoiceTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return buyerInvoiceTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<BuyerInvoiceTableItemView>> GetDropDownItemByPurchaseLine(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<BuyerInvoiceTable>(search, SysParm.CompanyGUID);
				var buyerInvoiceTable = GetDropDownQueryByPurchaseLine().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<BuyerInvoiceTableItemViewMap, BuyerInvoiceTableItemView>().ToDropDownItem(search.ExcludeRowData);


				return buyerInvoiceTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<BuyerInvoiceTableListViewMap> GetListQuery()
		{
			var isPurchaseTableStatusNotCancelled = (from buyerInvoiceTable in Entity
													 join purchaseLine in db.Set<PurchaseLine>()
													 on buyerInvoiceTable.BuyerInvoiceTableGUID equals purchaseLine.BuyerInvoiceTableGUID
													 join purchaseTable in db.Set<PurchaseTable>()
													 on purchaseLine.PurchaseTableGUID equals purchaseTable.PurchaseTableGUID
													 join documentStatus in db.Set<DocumentStatus>()
													 on purchaseTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
													 where documentStatus.StatusId != ((int)PurchaseStatus.Cancelled).ToString()
													 group buyerInvoiceTable.BuyerInvoiceTableGUID by buyerInvoiceTable.BuyerInvoiceTableGUID into g
													 select g.Key);
			return (from buyerInvoiceTable in Entity
					join buyerAgreementTable in db.Set<BuyerAgreementTable>()
					on buyerInvoiceTable.BuyerAgreementTableGUID equals buyerAgreementTable.BuyerAgreementTableGUID into ljBuyerInvoiceTableBuyerAgreementTable
					from buyerAgreementTable in ljBuyerInvoiceTableBuyerAgreementTable.DefaultIfEmpty()
					join buyerAgreementLine in db.Set<BuyerAgreementLine>()
					on buyerInvoiceTable.BuyerAgreementLineGUID equals buyerAgreementLine.BuyerAgreementLineGUID into ljBuyerInvoiceTableBuyerAgreementLine
					from buyerAgreementLine in ljBuyerInvoiceTableBuyerAgreementLine.DefaultIfEmpty()
					join purchaseTableStatusNotCancelled in isPurchaseTableStatusNotCancelled
					on buyerInvoiceTable.BuyerInvoiceTableGUID equals purchaseTableStatusNotCancelled into ljBuyerInvoiceTablePurchaseTableStatusNotCancelled
					from purchaseTableStatusNotCancelled in ljBuyerInvoiceTablePurchaseTableStatusNotCancelled.DefaultIfEmpty()
					select new BuyerInvoiceTableListViewMap
					{
						CompanyGUID = buyerInvoiceTable.CompanyGUID,
						Owner = buyerInvoiceTable.Owner,
						OwnerBusinessUnitGUID = buyerInvoiceTable.OwnerBusinessUnitGUID,
						BuyerInvoiceTableGUID = buyerInvoiceTable.BuyerInvoiceTableGUID,
						BuyerInvoiceId = buyerInvoiceTable.BuyerInvoiceId,
						InvoiceDate = buyerInvoiceTable.InvoiceDate,
						DueDate = buyerInvoiceTable.DueDate,
						Amount = buyerInvoiceTable.Amount,
						CreditAppLineGUID = buyerInvoiceTable.CreditAppLineGUID,
						BuyerAgreementTableGUID = buyerInvoiceTable.BuyerAgreementTableGUID,
						BuyerAgreementLineGUID = buyerInvoiceTable.BuyerAgreementLineGUID,
						BuyerAgreementTable_BuyerAgreementId = buyerAgreementTable.BuyerAgreementId,
						BuyerAgreementLine_Period = buyerAgreementLine.Period.ToString(),
						BuyerAgreementTable_Values = SmartAppUtil.GetDropDownLabel(buyerAgreementTable.BuyerAgreementId, buyerAgreementTable.Description),
						BuyerAgreementLine_Values = SmartAppUtil.GetDropDownLabel(buyerAgreementLine.Period.ToString(), buyerAgreementLine.Description),
						AccessModeCanDelete = purchaseTableStatusNotCancelled == null ? true : false,
					});
		}
		public SearchResult<BuyerInvoiceTableListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<BuyerInvoiceTableListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<BuyerInvoiceTable>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<BuyerInvoiceTableListViewMap, BuyerInvoiceTableListView>();
				result = list.SetSearchResult<BuyerInvoiceTableListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<BuyerInvoiceTableItemViewMap> GetItemQuery()
		{
			return (from buyerInvoiceTable in Entity
					join company in db.Set<Company>()
					on buyerInvoiceTable.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on buyerInvoiceTable.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljBuyerInvoiceTableOwnerBU
					from ownerBU in ljBuyerInvoiceTableOwnerBU.DefaultIfEmpty()
					join creditAppLine in db.Set<CreditAppLine>()
					on buyerInvoiceTable.CreditAppLineGUID equals creditAppLine.CreditAppLineGUID into ljBuyerInvoiceTablecreditAppLine
					from creditAppLine in ljBuyerInvoiceTablecreditAppLine.DefaultIfEmpty()
					join creditAppTable in db.Set<CreditAppTable>()
					on creditAppLine.CreditAppTableGUID equals creditAppTable.CreditAppTableGUID into ljCreditAppTablecreditAppLine
					from creditAppTable in ljCreditAppTablecreditAppLine.DefaultIfEmpty()
					join buyerTable in db.Set<BuyerTable>()
					on creditAppLine.BuyerTableGUID equals buyerTable.BuyerTableGUID into ljCreditAppLinebuyerTable
					from buyerTable in ljCreditAppLinebuyerTable.DefaultIfEmpty()
					join purchaseLine in db.Set<PurchaseLine>()
					on buyerInvoiceTable.BuyerInvoiceTableGUID equals purchaseLine.BuyerInvoiceTableGUID into ljBuyerInvoiceTablePurchaseLine
					from purchaseLine in ljBuyerInvoiceTablePurchaseLine.DefaultIfEmpty()
					join purchaseTable in db.Set<PurchaseTable>()
					on purchaseLine.PurchaseTableGUID equals purchaseTable.PurchaseTableGUID into ljPurchaseLinePurchaseTable
					from purchaseTable in ljPurchaseLinePurchaseTable.DefaultIfEmpty()
					join documentStatus in db.Set<DocumentStatus>()
					on purchaseTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljDocumentStatus
					from documentStatus in ljDocumentStatus.DefaultIfEmpty()
					select new BuyerInvoiceTableItemViewMap
					{
						CompanyGUID = buyerInvoiceTable.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = buyerInvoiceTable.Owner,
						OwnerBusinessUnitGUID = buyerInvoiceTable.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = buyerInvoiceTable.CreatedBy,
						CreatedDateTime = buyerInvoiceTable.CreatedDateTime,
						ModifiedBy = buyerInvoiceTable.ModifiedBy,
						ModifiedDateTime = buyerInvoiceTable.ModifiedDateTime,
						BuyerInvoiceTableGUID = buyerInvoiceTable.BuyerInvoiceTableGUID,
						Amount = buyerInvoiceTable.Amount,
						BuyerAgreementLineGUID = buyerInvoiceTable.BuyerAgreementLineGUID,
						BuyerAgreementTableGUID = buyerInvoiceTable.BuyerAgreementTableGUID,
						BuyerInvoiceId = buyerInvoiceTable.BuyerInvoiceId,
						CreditAppLineGUID = buyerInvoiceTable.CreditAppLineGUID,
						DueDate = buyerInvoiceTable.DueDate,
						InvoiceDate = buyerInvoiceTable.InvoiceDate,
						Remark = buyerInvoiceTable.Remark,
						LineNum = creditAppLine.LineNum,
                        BuyerId = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
                        CreditAppId = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId,creditAppTable.Description),
						PurchaseLineGUID = purchaseLine.PurchaseLineGUID,
						PurchaseTableGUID = purchaseTable.PurchaseTableGUID,
						DocumentStatusGUID = purchaseTable.DocumentStatusGUID,
						DocumentStatus_StatusId = documentStatus.StatusId,
						CreditAppLine_BuyerTableGUID = creditAppLine.BuyerTableGUID,
						CreditAppTable_CustomerTableGUID = creditAppTable.CustomerTableGUID,

						RowVersion = buyerInvoiceTable.RowVersion,
					});
		}
		public BuyerInvoiceTableItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<BuyerInvoiceTableItemViewMap, BuyerInvoiceTableItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public BuyerInvoiceTable CreateBuyerInvoiceTable(BuyerInvoiceTable buyerInvoiceTable)
		{
			try
			{
				buyerInvoiceTable.BuyerInvoiceTableGUID = Guid.NewGuid();
				base.Add(buyerInvoiceTable);
				return buyerInvoiceTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateBuyerInvoiceTableVoid(BuyerInvoiceTable buyerInvoiceTable)
		{
			try
			{
				CreateBuyerInvoiceTable(buyerInvoiceTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BuyerInvoiceTable UpdateBuyerInvoiceTable(BuyerInvoiceTable dbBuyerInvoiceTable, BuyerInvoiceTable inputBuyerInvoiceTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBuyerInvoiceTable = dbBuyerInvoiceTable.MapUpdateValues<BuyerInvoiceTable>(inputBuyerInvoiceTable);
				base.Update(dbBuyerInvoiceTable);
				return dbBuyerInvoiceTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateBuyerInvoiceTableVoid(BuyerInvoiceTable dbBuyerInvoiceTable, BuyerInvoiceTable inputBuyerInvoiceTable, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBuyerInvoiceTable = dbBuyerInvoiceTable.MapUpdateValues<BuyerInvoiceTable>(inputBuyerInvoiceTable, skipUpdateFields);
				base.Update(dbBuyerInvoiceTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public BuyerInvoiceTable GetBuyerInvoiceTableByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BuyerInvoiceTableGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BuyerInvoiceTable GetBuyerInvoiceTableByCreditAppLineIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.CreditAppLineGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public override void ValidateAdd(IEnumerable<BuyerInvoiceTable> items)
		{
			base.ValidateAdd(items);
		}
		public override void ValidateAdd(BuyerInvoiceTable item)
		{
			base.ValidateAdd(item);
		}
	}
}
