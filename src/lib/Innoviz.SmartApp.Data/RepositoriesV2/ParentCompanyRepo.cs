using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IParentCompanyRepo
	{
		#region DropDown
		IEnumerable<SelectItem<ParentCompanyItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<ParentCompanyListView> GetListvw(SearchParameter search);
		ParentCompanyItemView GetByIdvw(Guid id);
		ParentCompany Find(params object[] keyValues);
		ParentCompany GetParentCompanyByIdNoTracking(Guid guid);
		ParentCompany CreateParentCompany(ParentCompany parentCompany);
		void CreateParentCompanyVoid(ParentCompany parentCompany);
		ParentCompany UpdateParentCompany(ParentCompany dbParentCompany, ParentCompany inputParentCompany, List<string> skipUpdateFields = null);
		void UpdateParentCompanyVoid(ParentCompany dbParentCompany, ParentCompany inputParentCompany, List<string> skipUpdateFields = null);
		void Remove(ParentCompany item);
	}
	public class ParentCompanyRepo : CompanyBaseRepository<ParentCompany>, IParentCompanyRepo
	{
		public ParentCompanyRepo(SmartAppDbContext context) : base(context) { }
		public ParentCompanyRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ParentCompany GetParentCompanyByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.ParentCompanyGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<ParentCompanyItemViewMap> GetDropDownQuery()
		{
			return (from parentCompany in Entity
					select new ParentCompanyItemViewMap
					{
						CompanyGUID = parentCompany.CompanyGUID,
						Owner = parentCompany.Owner,
						OwnerBusinessUnitGUID = parentCompany.OwnerBusinessUnitGUID,
						ParentCompanyGUID = parentCompany.ParentCompanyGUID,
						ParentCompanyId = parentCompany.ParentCompanyId,
						Description = parentCompany.Description
					});
		}
		public IEnumerable<SelectItem<ParentCompanyItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<ParentCompany>(search, SysParm.CompanyGUID);
				var parentCompany = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<ParentCompanyItemViewMap, ParentCompanyItemView>().ToDropDownItem(search.ExcludeRowData);


				return parentCompany;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<ParentCompanyListViewMap> GetListQuery()
		{
			return (from parentCompany in Entity
				select new ParentCompanyListViewMap
				{
						CompanyGUID = parentCompany.CompanyGUID,
						Owner = parentCompany.Owner,
						OwnerBusinessUnitGUID = parentCompany.OwnerBusinessUnitGUID,
						ParentCompanyGUID = parentCompany.ParentCompanyGUID,
						ParentCompanyId = parentCompany.ParentCompanyId,
						Description = parentCompany.Description,
				});
		}
		public SearchResult<ParentCompanyListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<ParentCompanyListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<ParentCompany>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<ParentCompanyListViewMap, ParentCompanyListView>();
				result = list.SetSearchResult<ParentCompanyListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<ParentCompanyItemViewMap> GetItemQuery()
		{
			return (from parentCompany in Entity
					join company in db.Set<Company>()
					on parentCompany.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on parentCompany.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljParentCompanyOwnerBU
					from ownerBU in ljParentCompanyOwnerBU.DefaultIfEmpty()
					select new ParentCompanyItemViewMap
					{
						CompanyGUID = parentCompany.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = parentCompany.Owner,
						OwnerBusinessUnitGUID = parentCompany.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = parentCompany.CreatedBy,
						CreatedDateTime = parentCompany.CreatedDateTime,
						ModifiedBy = parentCompany.ModifiedBy,
						ModifiedDateTime = parentCompany.ModifiedDateTime,
						ParentCompanyGUID = parentCompany.ParentCompanyGUID,
						Description = parentCompany.Description,
						ParentCompanyId = parentCompany.ParentCompanyId,
					
						RowVersion = parentCompany.RowVersion,
					});
		}
		public ParentCompanyItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<ParentCompanyItemViewMap, ParentCompanyItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public ParentCompany CreateParentCompany(ParentCompany parentCompany)
		{
			try
			{
				parentCompany.ParentCompanyGUID = Guid.NewGuid();
				base.Add(parentCompany);
				return parentCompany;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateParentCompanyVoid(ParentCompany parentCompany)
		{
			try
			{
				CreateParentCompany(parentCompany);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ParentCompany UpdateParentCompany(ParentCompany dbParentCompany, ParentCompany inputParentCompany, List<string> skipUpdateFields = null)
		{
			try
			{
				dbParentCompany = dbParentCompany.MapUpdateValues<ParentCompany>(inputParentCompany);
				base.Update(dbParentCompany);
				return dbParentCompany;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateParentCompanyVoid(ParentCompany dbParentCompany, ParentCompany inputParentCompany, List<string> skipUpdateFields = null)
		{
			try
			{
				dbParentCompany = dbParentCompany.MapUpdateValues<ParentCompany>(inputParentCompany, skipUpdateFields);
				base.Update(dbParentCompany);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
	}
}

