using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IDocumentConditionTransRepo
	{
		#region DropDown
		IEnumerable<SelectItem<DocumentConditionTransItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<DocumentConditionTransListView> GetListvw(SearchParameter search);
		DocumentConditionTransItemView GetByIdvw(Guid id);
		DocumentConditionTrans Find(params object[] keyValues);
		DocumentConditionTrans GetDocumentConditionTransByIdNoTracking(Guid guid);
		DocumentConditionTrans CreateDocumentConditionTrans(DocumentConditionTrans documentConditionTrans);
		void CreateDocumentConditionTransVoid(DocumentConditionTrans documentConditionTrans);
		DocumentConditionTrans UpdateDocumentConditionTrans(DocumentConditionTrans dbDocumentConditionTrans, DocumentConditionTrans inputDocumentConditionTrans, List<string> skipUpdateFields = null);
		void UpdateDocumentConditionTransVoid(DocumentConditionTrans dbDocumentConditionTrans, DocumentConditionTrans inputDocumentConditionTrans, List<string> skipUpdateFields = null);
		void Remove(DocumentConditionTrans item);
		IQueryable<DocumentConditionTrans> GetDocumentConditionTransByReference(Guid refId, int refType);
		IQueryable<DocumentConditionTrans> GetDocumentConditionTransByReference(List<Guid> refId, int refType);
		#region function
		IEnumerable<DocumentConditionTrans> GetDocumentTransByDocType(DocConVerifyTypeModel doctype);
		#endregion
		IEnumerable<DocumentConditionTrans> GetDocumentConditionTransByRefDocumentConditionTrans(List<Guid> refDocumentConditionTrans);
		List<QDocumentCondition_BuyerCreditApplication> GetDocumentConditionTransByBuyerCreditApplication(Guid creditAppRequestLineGUID, int docConVerifyType);
		void ValidateAdd(DocumentConditionTrans item);
		void ValidateAdd(IEnumerable<DocumentConditionTrans> items);
	}
    public class DocumentConditionTransRepo : CompanyBaseRepository<DocumentConditionTrans>, IDocumentConditionTransRepo
	{
		public DocumentConditionTransRepo(SmartAppDbContext context) : base(context) { }
		public DocumentConditionTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public DocumentConditionTrans GetDocumentConditionTransByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.DocumentConditionTransGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<DocumentConditionTransItemViewMap> GetDropDownQuery()
		{
			return (from documentConditionTrans in Entity
					select new DocumentConditionTransItemViewMap
					{
						CompanyGUID = documentConditionTrans.CompanyGUID,
						Owner = documentConditionTrans.Owner,
						OwnerBusinessUnitGUID = documentConditionTrans.OwnerBusinessUnitGUID,
						DocumentConditionTransGUID = documentConditionTrans.DocumentConditionTransGUID
					});
		}
		public IEnumerable<SelectItem<DocumentConditionTransItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<DocumentConditionTrans>(search, SysParm.CompanyGUID);
				var documentConditionTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<DocumentConditionTransItemViewMap, DocumentConditionTransItemView>().ToDropDownItem(search.ExcludeRowData);


				return documentConditionTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<DocumentConditionTransListViewMap> GetListQuery()
		{
			return (from documentConditionTrans in Entity
					join ownerBU in db.Set<BusinessUnit>()
					on documentConditionTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljDocumentConditionTransOwnerBU
					from ownerBU in ljDocumentConditionTransOwnerBU.DefaultIfEmpty()
					join documentType in db.Set<DocumentType>()
					on documentConditionTrans.DocumentTypeGUID equals documentType.DocumentTypeGUID into ljDocumentType
					from documentType in ljDocumentType.DefaultIfEmpty()
					select new DocumentConditionTransListViewMap
					{
						CompanyGUID = documentConditionTrans.CompanyGUID,
						Owner = documentConditionTrans.Owner,
						OwnerBusinessUnitGUID = documentConditionTrans.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						DocumentConditionTransGUID = documentConditionTrans.DocumentConditionTransGUID,
						DocumentTypeGUID = documentConditionTrans.DocumentTypeGUID,
						Mandatory = documentConditionTrans.Mandatory,
						DocConVerifyType = documentConditionTrans.DocConVerifyType,
						DocumentType_DocumentTypeId = documentType.DocumentTypeId,
						RefGUID = documentConditionTrans.RefGUID,
						DocumentType_Values = SmartAppUtil.GetDropDownLabel(documentType.DocumentTypeId, documentType.Description),
						RefDocumentConditionTransGUID = documentConditionTrans.RefDocumentConditionTransGUID,
					}) ;
		}
		public SearchResult<DocumentConditionTransListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<DocumentConditionTransListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<DocumentConditionTrans>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<DocumentConditionTransListViewMap, DocumentConditionTransListView>();
				result = list.SetSearchResult<DocumentConditionTransListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<DocumentConditionTransItemViewMap> GetItemQuery()
		{
			return (from documentConditionTrans in Entity
					join company in db.Set<Company>()
					on documentConditionTrans.CompanyGUID equals company.CompanyGUID
					join ownerBU in db.Set<BusinessUnit>()
					on documentConditionTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljDocumentConditionTransOwnerBU
					from ownerBU in ljDocumentConditionTransOwnerBU.DefaultIfEmpty()
					join buyerTable in db.Set<BuyerTable>()
					on documentConditionTrans.RefGUID equals buyerTable.BuyerTableGUID into ljBuyerTable
					from buyerTable in ljBuyerTable.DefaultIfEmpty()
					join creditAppRequestLine in db.Set<CreditAppRequestLine>()
					on documentConditionTrans.RefGUID equals creditAppRequestLine.CreditAppRequestLineGUID into ljCreditAppRequestLine
					from creditAppRequestLine in ljCreditAppRequestLine.DefaultIfEmpty()
					join creditAppLine in db.Set<CreditAppLine>()
					on documentConditionTrans.RefGUID equals creditAppLine.CreditAppLineGUID into ljCreditAppLine
					from creditAppLine in ljCreditAppLine.DefaultIfEmpty()
					select new DocumentConditionTransItemViewMap
					{
						CompanyGUID = documentConditionTrans.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = documentConditionTrans.Owner,
						OwnerBusinessUnitGUID = documentConditionTrans.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = documentConditionTrans.CreatedBy,
						CreatedDateTime = documentConditionTrans.CreatedDateTime,
						ModifiedBy = documentConditionTrans.ModifiedBy,
						ModifiedDateTime = documentConditionTrans.ModifiedDateTime,
						DocumentConditionTransGUID = documentConditionTrans.DocumentConditionTransGUID,
						DocConVerifyType = documentConditionTrans.DocConVerifyType,
						DocumentTypeGUID = documentConditionTrans.DocumentTypeGUID,
						Mandatory = documentConditionTrans.Mandatory,
						RefGUID = documentConditionTrans.RefGUID,
						RefType = documentConditionTrans.RefType,
						RefId = (buyerTable != null && documentConditionTrans.RefType == (int)RefType.Buyer) ? buyerTable.BuyerId :
								(creditAppRequestLine != null && documentConditionTrans.RefType == (int)RefType.CreditAppRequestLine) ? creditAppRequestLine.LineNum.ToString() :
								(creditAppLine != null && documentConditionTrans.RefType == (int)RefType.CreditAppLine) ? creditAppLine.LineNum.ToString() :
								null,
						Inactive = documentConditionTrans.Inactive,
						RefDocumentConditionTransGUID = documentConditionTrans.RefDocumentConditionTransGUID,
					
						RowVersion = documentConditionTrans.RowVersion,
					});
		}
		public DocumentConditionTransItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<DocumentConditionTransItemViewMap, DocumentConditionTransItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public DocumentConditionTrans CreateDocumentConditionTrans(DocumentConditionTrans documentConditionTrans)
		{
			try
			{
				documentConditionTrans.DocumentConditionTransGUID = Guid.NewGuid();
				base.Add(documentConditionTrans);
				return documentConditionTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateDocumentConditionTransVoid(DocumentConditionTrans documentConditionTrans)
		{
			try
			{
				CreateDocumentConditionTrans(documentConditionTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentConditionTrans UpdateDocumentConditionTrans(DocumentConditionTrans dbDocumentConditionTrans, DocumentConditionTrans inputDocumentConditionTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbDocumentConditionTrans = dbDocumentConditionTrans.MapUpdateValues<DocumentConditionTrans>(inputDocumentConditionTrans);
				base.Update(dbDocumentConditionTrans);
				return dbDocumentConditionTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateDocumentConditionTransVoid(DocumentConditionTrans dbDocumentConditionTrans, DocumentConditionTrans inputDocumentConditionTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbDocumentConditionTrans = dbDocumentConditionTrans.MapUpdateValues<DocumentConditionTrans>(inputDocumentConditionTrans, skipUpdateFields);
				base.Update(dbDocumentConditionTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public IQueryable<DocumentConditionTrans> GetDocumentConditionTransByReference(Guid refId, int refType)
		{
			try
			{
				Guid companyGUID = SysParm.CompanyGUID.StringToGuid();
				return Entity.Where(t => t.CompanyGUID == companyGUID && t.RefGUID == refId && t.RefType == refType).AsNoTracking();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#region function
		public IEnumerable<DocumentConditionTrans> GetDocumentTransByDocType(DocConVerifyTypeModel doctype)
		{
			try
			{
				var result = Entity.Where(item => item.DocConVerifyType == doctype.DocConVerifyType&& item.RefGUID == doctype.RefGUID).AsNoTracking().ToList();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		public IEnumerable<DocumentConditionTrans> GetDocumentConditionTransByRefDocumentConditionTrans(List<Guid> refDocumentConditionTrans)
		{
			try
			{
				return Entity.Where(w => refDocumentConditionTrans.Any(a => a == w.DocumentConditionTransGUID)).AsNoTracking();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IQueryable<DocumentConditionTrans> GetDocumentConditionTransByReference(List<Guid> refId, int refType)
		{
			try
			{
				Guid companyGUID = SysParm.CompanyGUID.StringToGuid();
				return Entity.Where(t => t.CompanyGUID == companyGUID && t.RefType == refType && refId.Any(a => a == t.RefGUID)).AsNoTracking();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}

		public List<QDocumentCondition_BuyerCreditApplication> GetDocumentConditionTransByBuyerCreditApplication(Guid creditAppRequestLineGUID, int docConVerifyType)
		{
            try
            {
                var result = (from documentConditionTrans in Entity
								join documentType in db.Set<DocumentType>()
								on documentConditionTrans.DocumentTypeGUID equals documentType.DocumentTypeGUID
								where documentConditionTrans.RefGUID == creditAppRequestLineGUID
									&& documentConditionTrans.RefType == (int) RefType.CreditAppRequestLine
									&& documentConditionTrans.Inactive == false
									&& documentConditionTrans.DocConVerifyType == docConVerifyType
							  orderby documentType.DocumentTypeId 
							select new QDocumentCondition_BuyerCreditApplication
							{
								CreditAppRequestLineGUID = creditAppRequestLineGUID,
								Description = documentType.Description
							}).AsNoTracking().Take(10).ToList();

				result = result.Select((o, i) =>
				{
					o.RowNumber = i;
					return o;
				}).ToList();

				return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

		public override void ValidateAdd(IEnumerable<DocumentConditionTrans> items)
		{
			foreach (var item in items)
			{
				ValidateAdd(item);
			}
		}
		public override void ValidateAdd(DocumentConditionTrans item)
		{
			CheckDupplicate(item);
			base.ValidateAdd(item);
		}
		public override void ValidateUpdate(IEnumerable<DocumentConditionTrans> items)
		{
			ValidateUpdate(items);
		}
		public override void ValidateUpdate(DocumentConditionTrans item)
		{
			CheckDupplicate(item);
			base.ValidateUpdate(item);
		}
		public void CheckDupplicate(DocumentConditionTrans item)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				IEnumerable<DocumentConditionTrans> documentConditionTrans = GetDocumentConditionTransByReference(item.RefGUID, item.RefType);

				bool isDupplicate = documentConditionTrans.Any(a => a.DocConVerifyType == item.DocConVerifyType && 
																	a.DocumentTypeGUID == item.DocumentTypeGUID &&
																	a.RefGUID == item.RefGUID &&
																	a.DocumentConditionTransGUID != item.DocumentConditionTransGUID);
				if (isDupplicate)
				{
					ex.AddData("ERROR.DUPLICATE", new string[] { "LABEL.VERIFY_TYPE_ID", "LABEL.DOCUMENT_TYPE_ID", "LABEL.REFERENCE_ID" });
				}
				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
	}
}

