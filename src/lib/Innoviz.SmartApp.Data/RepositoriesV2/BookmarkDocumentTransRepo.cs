using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.RepositoriesV2
{
	public interface IBookmarkDocumentTransRepo
	{
		#region DropDown
		IEnumerable<SelectItem<BookmarkDocumentTransItemView>> GetDropDownItem(SearchParameter search);
		#endregion DropDown
		SearchResult<BookmarkDocumentTransListView> GetListvw(SearchParameter search);
		BookmarkDocumentTransItemView GetByIdvw(Guid id);
		BookmarkDocumentTrans Find(params object[] keyValues);
		BookmarkDocumentTrans GetBookmarkDocumentTransByIdNoTracking(Guid guid);
		BookmarkDocumentTrans CreateBookmarkDocumentTrans(BookmarkDocumentTrans bookmarkDocumentTrans);
		void CreateBookmarkDocumentTransVoid(BookmarkDocumentTrans bookmarkDocumentTrans);
		BookmarkDocumentTrans UpdateBookmarkDocumentTrans(BookmarkDocumentTrans dbBookmarkDocumentTrans, BookmarkDocumentTrans inputBookmarkDocumentTrans, List<string> skipUpdateFields = null);
		void UpdateBookmarkDocumentTransVoid(BookmarkDocumentTrans dbBookmarkDocumentTrans, BookmarkDocumentTrans inputBookmarkDocumentTrans, List<string> skipUpdateFields = null);
		void Remove(BookmarkDocumentTrans item);
		BookmarkDocumentTrans GetBookmarkDocumentTransByIdNoTrackingByAccessLevel(Guid guid);
		#region function 
		IEnumerable<BookmarkDocumentTrans> GetBookmarkDocumentTransByRefType(int refType, Guid refGuid);
		#endregion
	}
    public class BookmarkDocumentTransRepo : CompanyBaseRepository<BookmarkDocumentTrans>, IBookmarkDocumentTransRepo
	{
		public BookmarkDocumentTransRepo(SmartAppDbContext context) : base(context) { }
		public BookmarkDocumentTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BookmarkDocumentTrans GetBookmarkDocumentTransByIdNoTracking(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BookmarkDocumentTransGUID == guid).AsNoTracking().FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		private IQueryable<BookmarkDocumentTransItemViewMap> GetDropDownQuery()
		{
			return (from bookmarkDocumentTrans in Entity
					select new BookmarkDocumentTransItemViewMap
					{
						CompanyGUID = bookmarkDocumentTrans.CompanyGUID,
						Owner = bookmarkDocumentTrans.Owner,
						OwnerBusinessUnitGUID = bookmarkDocumentTrans.OwnerBusinessUnitGUID,
						BookmarkDocumentTransGUID = bookmarkDocumentTrans.BookmarkDocumentTransGUID,
					});
		}
		public IEnumerable<SelectItem<BookmarkDocumentTransItemView>> GetDropDownItem(SearchParameter search)
		{
			try
			{
				var predicate = base.GetFilterLevelPredicate<BookmarkDocumentTrans>(search, SysParm.CompanyGUID);
				var bookmarkDocumentTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<BookmarkDocumentTransItemViewMap, BookmarkDocumentTransItemView>().ToDropDownItem(search.ExcludeRowData);


				return bookmarkDocumentTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region GetListvw
		private IQueryable<BookmarkDocumentTransListViewMap> GetListQuery()
		{
			return (from bookmarkDocumentTrans in Entity
					join bookmarkDocument in db.Set<BookmarkDocument>()
					on bookmarkDocumentTrans.BookmarkDocumentGUID equals bookmarkDocument.BookmarkDocumentGUID
					join documentTemplateTable in db.Set<DocumentTemplateTable>()
					on bookmarkDocumentTrans.DocumentTemplateTableGUID equals documentTemplateTable.DocumentTemplateTableGUID
					join documentStatus in db.Set<DocumentStatus>()
					on bookmarkDocumentTrans.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
					select new BookmarkDocumentTransListViewMap
				{
						CompanyGUID = bookmarkDocumentTrans.CompanyGUID,
						Owner = bookmarkDocumentTrans.Owner,
						OwnerBusinessUnitGUID = bookmarkDocumentTrans.OwnerBusinessUnitGUID,
						BookmarkDocumentTransGUID = bookmarkDocumentTrans.BookmarkDocumentTransGUID,
						BookmarkDocumentGUID = bookmarkDocumentTrans.BookmarkDocumentGUID,
						DocumentTemplateTableGUID = bookmarkDocumentTrans.DocumentTemplateTableGUID,
						DocumentStatusGUID = bookmarkDocumentTrans.DocumentStatusGUID,
						ReferenceExternalId = bookmarkDocumentTrans.ReferenceExternalId,
						BookmarkDocument_BookmarkDocumentId = bookmarkDocument.BookmarkDocumentId,
						BookmarkDocument_BookmarkDocumentRefType = bookmarkDocument.BookmarkDocumentRefType,
						BookmarkDocument_DocumentTemplateType = bookmarkDocument.DocumentTemplateType,
						DocumentTemplateTable_TemplateId = documentTemplateTable.TemplateId,
						RefGUID = bookmarkDocumentTrans.RefGUID,
						RefType = bookmarkDocumentTrans.RefType,
						DocumentStatus_StatusId = documentStatus.StatusId,
						DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
						BookmarkDocument_Values = SmartAppUtil.GetDropDownLabel(bookmarkDocument.BookmarkDocumentId, bookmarkDocument.Description),
						DocumentTemplateTable_Values = SmartAppUtil.GetDropDownLabel(documentTemplateTable.TemplateId, documentTemplateTable.Description),
				});
		}
		public SearchResult<BookmarkDocumentTransListView> GetListvw(SearchParameter search)
		{
			var result = new SearchResult<BookmarkDocumentTransListView>();
			try
			{
				var predicate = base.GetFilterLevelPredicate<BookmarkDocumentTrans>(search, SysParm.CompanyGUID);
				var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.AsNoTracking()
											.Count();
				var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
											.OrderBy(predicate.Sorting)
											.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
											.Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
											.ToMaps<BookmarkDocumentTransListViewMap, BookmarkDocumentTransListView>();
				result = list.SetSearchResult<BookmarkDocumentTransListView>(total, search);
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetListvw
		#region GetByIdvw
		private IQueryable<BookmarkDocumentTransItemViewMap> GetItemQuery()
		{
			return (from bookmarkDocumentTrans in Entity
					join company in db.Set<Company>()
					on bookmarkDocumentTrans.CompanyGUID equals company.CompanyGUID
					join bookmarkDocument in db.Set<BookmarkDocument>()
					on bookmarkDocumentTrans.BookmarkDocumentGUID equals bookmarkDocument.BookmarkDocumentGUID
					join documentTemplateTable in db.Set<DocumentTemplateTable>()
					on bookmarkDocumentTrans.DocumentTemplateTableGUID equals documentTemplateTable.DocumentTemplateTableGUID
					join documentStatus in db.Set<DocumentStatus>()
					on bookmarkDocumentTrans.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
					join ownerBU in db.Set<BusinessUnit>()
					on bookmarkDocumentTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljBookmarkDocumentTransOwnerBU
					from ownerBU in ljBookmarkDocumentTransOwnerBU.DefaultIfEmpty()
					join mainAgreementTable in db.Set<MainAgreementTable>()
					on bookmarkDocumentTrans.RefGUID equals mainAgreementTable.MainAgreementTableGUID into ljMainAgreementTable
					from mainAgreementTable in ljMainAgreementTable.DefaultIfEmpty()
					join guarantorAgreement in db.Set<GuarantorAgreementTable>()
					on bookmarkDocumentTrans.RefGUID equals guarantorAgreement.GuarantorAgreementTableGUID into ljGuarantorAgreement
					from guarantorAgreement in ljGuarantorAgreement.DefaultIfEmpty()
					join assignmentAgreementTable in db.Set<AssignmentAgreementTable>()
					on bookmarkDocumentTrans.RefGUID equals assignmentAgreementTable.AssignmentAgreementTableGUID into ljAssignmentAgreementTable
					from assignmentAgreementTable in ljAssignmentAgreementTable.DefaultIfEmpty()
					#region bookmarkdocument from creditAppRequestTable
					join creditAppRequestTable in db.Set<CreditAppRequestTable>()
					on bookmarkDocumentTrans.RefGUID equals creditAppRequestTable.CreditAppRequestTableGUID into ljCreditAppRequestTable
					from creditAppRequestTable in ljCreditAppRequestTable.DefaultIfEmpty()
					#endregion
					join businessCollateralAgmTable in db.Set<BusinessCollateralAgmTable>()
					on bookmarkDocumentTrans.RefGUID equals businessCollateralAgmTable.BusinessCollateralAgmTableGUID into ljBusinessCollateralAgmTable
					from businessCollateralAgmTable in ljBusinessCollateralAgmTable.DefaultIfEmpty()
					join creditAppRequestLine in db.Set<CreditAppRequestLine>()
					on bookmarkDocumentTrans.RefGUID equals creditAppRequestLine.CreditAppRequestLineGUID into ljCreditAppRequestLine
					from creditAppRequestLine in ljCreditAppRequestLine.DefaultIfEmpty()
					select new BookmarkDocumentTransItemViewMap
					{
						CompanyGUID = bookmarkDocumentTrans.CompanyGUID,
						CompanyId = company.CompanyId,
						Owner = bookmarkDocumentTrans.Owner,
						OwnerBusinessUnitGUID = bookmarkDocumentTrans.OwnerBusinessUnitGUID,
						OwnerBusinessUnitId = ownerBU.BusinessUnitId,
						CreatedBy = bookmarkDocumentTrans.CreatedBy,
						CreatedDateTime = bookmarkDocumentTrans.CreatedDateTime,
						ModifiedBy = bookmarkDocumentTrans.ModifiedBy,
						ModifiedDateTime = bookmarkDocumentTrans.ModifiedDateTime,
						BookmarkDocumentTransGUID = bookmarkDocumentTrans.BookmarkDocumentTransGUID,
						BookmarkDocumentGUID = bookmarkDocumentTrans.BookmarkDocumentGUID,
						DocumentStatusGUID = bookmarkDocumentTrans.DocumentStatusGUID,
						DocumentTemplateTableGUID = bookmarkDocumentTrans.DocumentTemplateTableGUID,
						ReferenceExternalId = bookmarkDocumentTrans.ReferenceExternalId,
						ReferenceExternalDate=bookmarkDocumentTrans.ReferenceExternalDate,
						RefGUID = bookmarkDocumentTrans.RefGUID,
						RefType = bookmarkDocumentTrans.RefType,
						Remark = bookmarkDocumentTrans.Remark,
						BookmarkDocument_BookmarkDocumentRefType = bookmarkDocument.DocumentTemplateType,
						BookmarkDocument_DocumentTemplateType = bookmarkDocument.DocumentTemplateType,
						DocumentStatus_StatusId = documentStatus.StatusId,
						RefId = (mainAgreementTable != null && bookmarkDocumentTrans.RefType == (int)RefType.MainAgreement) ? mainAgreementTable.InternalMainAgreementId :
								(guarantorAgreement != null && bookmarkDocumentTrans.RefType == (int)RefType.GuarantorAgreement) ? guarantorAgreement.InternalGuarantorAgreementId :
								(creditAppRequestTable != null && bookmarkDocumentTrans.RefType == (int)RefType.CreditAppRequestTable) ? creditAppRequestTable.CreditAppRequestId :
								(assignmentAgreementTable != null && bookmarkDocumentTrans.RefType == (int)RefType.AssignmentAgreement) ? assignmentAgreementTable.InternalAssignmentAgreementId :
								(businessCollateralAgmTable != null && bookmarkDocumentTrans.RefType == (int)RefType.BusinessCollateralAgreement) ? businessCollateralAgmTable.InternalBusinessCollateralAgmId:
								(creditAppRequestLine != null && bookmarkDocumentTrans.RefType == (int)RefType.CreditAppRequestLine) ? creditAppRequestLine.LineNum.ToString() :
						null,
					
						RowVersion = bookmarkDocumentTrans.RowVersion,
					});
		}
		public BookmarkDocumentTransItemView GetByIdvw(Guid guid)
		{
			try
			{
				var predicate = base.GetByIdvwFilterLevelPredicate(guid);
				var item = GetItemQuery()
									.Where(predicate.Predicates, predicate.Values)
									.AsNoTracking()
									.FirstOrDefault()
									.CheckDataNotNull()
									.ToMap<BookmarkDocumentTransItemViewMap, BookmarkDocumentTransItemView>();
				return item;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GetByIdvw
		#region Create, Update, Delete
		public BookmarkDocumentTrans CreateBookmarkDocumentTrans(BookmarkDocumentTrans bookmarkDocumentTrans)
		{
			try
			{
				bookmarkDocumentTrans.BookmarkDocumentTransGUID = Guid.NewGuid();
				base.Add(bookmarkDocumentTrans);
				return bookmarkDocumentTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateBookmarkDocumentTransVoid(BookmarkDocumentTrans bookmarkDocumentTrans)
		{
			try
			{
				CreateBookmarkDocumentTrans(bookmarkDocumentTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BookmarkDocumentTrans UpdateBookmarkDocumentTrans(BookmarkDocumentTrans dbBookmarkDocumentTrans, BookmarkDocumentTrans inputBookmarkDocumentTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBookmarkDocumentTrans = dbBookmarkDocumentTrans.MapUpdateValues<BookmarkDocumentTrans>(inputBookmarkDocumentTrans);
				base.Update(dbBookmarkDocumentTrans);
				return dbBookmarkDocumentTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateBookmarkDocumentTransVoid(BookmarkDocumentTrans dbBookmarkDocumentTrans, BookmarkDocumentTrans inputBookmarkDocumentTrans, List<string> skipUpdateFields = null)
		{
			try
			{
				dbBookmarkDocumentTrans = dbBookmarkDocumentTrans.MapUpdateValues<BookmarkDocumentTrans>(inputBookmarkDocumentTrans, skipUpdateFields);
				base.Update(dbBookmarkDocumentTrans);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Create, Update, Delete
		public BookmarkDocumentTrans GetBookmarkDocumentTransByIdNoTrackingByAccessLevel(Guid guid)
		{
			try
			{
				var result = Entity.Where(item => item.BookmarkDocumentTransGUID == guid)
					.FilterByAccessLevel(SysParm.AccessLevel)
					.AsNoTracking()
					.FirstOrDefault();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        #region function
		public IEnumerable<BookmarkDocumentTrans> GetBookmarkDocumentTransByRefType(int refType,Guid refGuid)
        {
            try
            {
				var list = Entity.Where(w => w.RefType == refType && w.RefGUID == refGuid)
					.AsNoTracking();
				return list;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

		#endregion
	}
}

