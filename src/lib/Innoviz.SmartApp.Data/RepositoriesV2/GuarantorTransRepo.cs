using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using Innoviz.SmartApp.Core.Constants;
namespace Innoviz.SmartApp.Data.RepositoriesV2
{
    public interface IGuarantorTransRepo
    {
        #region DropDown
        IEnumerable<SelectItem<GuarantorTransItemView>> GetDropDownItem(SearchParameter search);
        #endregion DropDown
        void ValidateAdd(GuarantorTrans item);
        void ValidateAdd(IEnumerable<GuarantorTrans> items);
        void ValidateUpdate(GuarantorTrans item);
        void ValidateRemove(IEnumerable<GuarantorTrans> items);
        SearchResult<GuarantorTransListView> GetListvw(SearchParameter search);
        GuarantorTransItemView GetByIdvw(Guid id);
        GuarantorTrans Find(params object[] keyValues);
        GuarantorTrans GetGuarantorTransByIdNoTracking(Guid guid);
        GuarantorTrans CreateGuarantorTrans(GuarantorTrans guarantorTrans);
        void CreateGuarantorTransVoid(GuarantorTrans guarantorTrans);
        GuarantorTrans UpdateGuarantorTrans(GuarantorTrans dbGuarantorTrans, GuarantorTrans inputGuarantorTrans, List<string> skipUpdateFields = null);
        void UpdateGuarantorTransVoid(GuarantorTrans dbGuarantorTrans, GuarantorTrans inputGuarantorTrans, List<string> skipUpdateFields = null);
        void Remove(GuarantorTrans item);
        void Remove(IEnumerable<GuarantorTrans> item);
        IEnumerable<GuarantorTrans> GetByActive(Guid companyGUID);
        IEnumerable<GuarantorTrans> GetGuarantorTransByReference(Guid refId, int refType);
        IEnumerable<GuarantorTrans> GetAuthorizedPersonTransByRefGuarantorTrans(List<Guid> refGuarantorTrans);

        IEnumerable<SelectItem<GuarantorTransItemView>> GetDropDownItemWithPerson(SearchParameter search);
        List<GuarantorTransItemView> GetGuarantorTransListForCopyFromCA(string creditAppRequstGUID, int refType, string refGuaratorTransGUID, bool inActive, bool affliate);
    }
    public class GuarantorTransRepo : CompanyBaseRepository<GuarantorTrans>, IGuarantorTransRepo
    {
        public GuarantorTransRepo(SmartAppDbContext context) : base(context) { }
        public GuarantorTransRepo(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public GuarantorTrans GetGuarantorTransByIdNoTracking(Guid guid)
        {
            try
            {
                var result = Entity.Where(item => item.GuarantorTransGUID == guid).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        private IQueryable<GuarantorTransItemViewMap> GetDropDownQuery()
        {
            return (from guarantorTrans in Entity
                    select new GuarantorTransItemViewMap
                    {
                        CompanyGUID = guarantorTrans.CompanyGUID,
                        Owner = guarantorTrans.Owner,
                        OwnerBusinessUnitGUID = guarantorTrans.OwnerBusinessUnitGUID,
                        GuarantorTransGUID = guarantorTrans.GuarantorTransGUID,
                        RelatedPersonTableGUID = guarantorTrans.RelatedPersonTableGUID,
                    });
        }
        public IEnumerable<SelectItem<GuarantorTransItemView>> GetDropDownItem(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<GuarantorTrans>(search, SysParm.CompanyGUID);
                var guarantorTrans = GetDropDownQuery().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<GuarantorTransItemViewMap, GuarantorTransItemView>().ToDropDownItem(search.ExcludeRowData);


                return guarantorTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private IQueryable<GuarantorTransItemViewMap> GetDropDownQueryWithPerson()
        {
            IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService();
            var result = (from guarantorTrans in db.Set<GuarantorTrans>()

                          join relatedPerson in db.Set<RelatedPersonTable>()
                          on guarantorTrans.RelatedPersonTableGUID equals relatedPerson.RelatedPersonTableGUID into ljrelatedPerson
                          from relatedPerson in ljrelatedPerson.DefaultIfEmpty()

                          join addressTrans in db.Set<AddressTrans>().Where(wh=>wh.Primary)
                          on guarantorTrans.RelatedPersonTableGUID equals addressTrans.RefGUID  into ljaddressTrans 
                          from addressTrans in ljaddressTrans.DefaultIfEmpty()

                          where guarantorTrans.InActive == false
                          select new GuarantorTransItemViewMap
                          {
                              CompanyGUID = guarantorTrans.CompanyGUID,
                              Owner = guarantorTrans.Owner,
                              OwnerBusinessUnitGUID = guarantorTrans.OwnerBusinessUnitGUID,
                              GuarantorTransGUID = guarantorTrans.GuarantorTransGUID,
                              RelatedPersonTableGUID = guarantorTrans.RelatedPersonTableGUID,
                              RelatedPersonTable_Name = relatedPerson.Name,
                              RelatedPersonTable_RelatedPersonId = relatedPerson.RelatedPersonId,
                              RelatedPersonTable_TaxId = relatedPerson.TaxId,
                              RelatedPersonTable_PassportId = relatedPerson.PassportId,
                              RelatedPersonTable_WorkPermitId = relatedPerson.WorkPermitId,
                              RelatedPersonTable_DateOfBirth = relatedPerson.DateOfBirth,
                              RelatedPersonTable_DateOfIssue = relatedPerson.DateOfIssue.DateNullToString(),
                              RelatedPersonTable_NationalityId = relatedPerson.NationalityGUID.GuidNullToString(),
                              RelatedPersonTable_RaceId = relatedPerson.RaceGUID.GuidNullToString(),
                              Age = relatedPersonTableService.CalcAge(relatedPerson.DateOfBirth),
                              RelatedPersonTable_Address1 = addressTrans.Address1,
                              RelatedPersonTable_Address2 = addressTrans.Address2,
                              RelatedPersonTable_RaceGUID = relatedPerson.RaceGUID.GuidNullToString(),
                              RelatedPersonTable_NationalityGUID = relatedPerson.NationalityGUID.GuidNullToString(),
                              RelatedPersonTable_OperatedBy = relatedPerson.OperatedBy,
                              RefGUID = guarantorTrans.RefGUID,
                              MaximumGuaranteeAmount = guarantorTrans.MaximumGuaranteeAmount
                          }); ;




            return result;
        }
        public IEnumerable<SelectItem<GuarantorTransItemView>> GetDropDownItemWithPerson(SearchParameter search)
        {
            try
            {
                var predicate = base.GetFilterLevelPredicate<GuarantorTrans>(search, SysParm.CompanyGUID);
                var guarantorTrans = GetDropDownQueryWithPerson().Where(predicate.Predicates, predicate.Values)
					.Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
					.Take(search.Paginator.Rows.GetPaginatorRows(DropdownConst.RowsLimit)).AsNoTracking()
					.ToMaps<GuarantorTransItemViewMap, GuarantorTransItemView>().ToDropDownItem(search.ExcludeRowData);


                return guarantorTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #region GetListvw
        private IQueryable<GuarantorTransListViewMap> GetListQuery()
        {
            return (from guarantorTrans in Entity
                    join guarantorType in db.Set<GuarantorType>()
                    on guarantorTrans.GuarantorTypeGUID equals guarantorType.GuarantorTypeGUID
                    join relatedPersonTable in db.Set<RelatedPersonTable>()
                    on guarantorTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID into ljRelatedPersonTable
                    from relatedPersonTable in ljRelatedPersonTable.DefaultIfEmpty()
                    select new GuarantorTransListViewMap
                    {
                        CompanyGUID = guarantorTrans.CompanyGUID,
                        Owner = guarantorTrans.Owner,
                        OwnerBusinessUnitGUID = guarantorTrans.OwnerBusinessUnitGUID,
                        GuarantorTransGUID = guarantorTrans.GuarantorTransGUID,
                        Ordering = guarantorTrans.Ordering,
                        GuarantorTypeGUID = guarantorTrans.GuarantorTypeGUID,
                        Affiliate = guarantorTrans.Affiliate,
                        InActive = guarantorTrans.InActive,
                        RefGUID = guarantorTrans.RefGUID,
                        RefType = guarantorTrans.RefType,
                        RefGuarantorTransGUID = guarantorTrans.RefGuarantorTransGUID,
                        RelatedPersonTable_Name = relatedPersonTable.Name,
                        RelatedPersonTable_TaxId = relatedPersonTable.TaxId,
                        RelatedPersonTable_PassportId = relatedPersonTable.PassportId,
                        GuarantorType_GuarantorTypeId = guarantorType.GuarantorTypeId,
                        GuarantorType_Values = SmartAppUtil.GetDropDownLabel(guarantorType.GuarantorTypeId, guarantorType.Description)
                    });
        }
        public SearchResult<GuarantorTransListView> GetListvw(SearchParameter search)
        {
            var result = new SearchResult<GuarantorTransListView>();
            try
            {
                var predicate = base.GetFilterLevelPredicate<GuarantorTrans>(search, SysParm.CompanyGUID);
                var total = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .AsNoTracking()
                                            .Count();
                var list = GetListQuery().Where(predicate.Predicates, predicate.Values)
                                            .OrderBy(predicate.Sorting)
                                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                            .Take(search.Paginator.Rows.GetPaginatorRows(total)).AsNoTracking()
                                            .ToMaps<GuarantorTransListViewMap, GuarantorTransListView>();
                result = list.SetSearchResult<GuarantorTransListView>(total, search);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetListvw
        #region GetByIdvw
        private IQueryable<GuarantorTransItemViewMap> GetItemQuery()
        {
            IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService();
            return (from guarantorTrans in Entity
                    join company in db.Set<Company>()
                    on guarantorTrans.CompanyGUID equals company.CompanyGUID
                    join ownerBU in db.Set<BusinessUnit>()
                    on guarantorTrans.OwnerBusinessUnitGUID equals ownerBU.BusinessUnitGUID into ljGuarantorTransOwnerBU
                    from ownerBU in ljGuarantorTransOwnerBU.DefaultIfEmpty()
                    join customerTable in db.Set<CustomerTable>()
                    on guarantorTrans.RefGUID equals customerTable.CustomerTableGUID into ljCustomerTable
                    from customerTable in ljCustomerTable.DefaultIfEmpty()
                    join relatedPersonTable in db.Set<RelatedPersonTable>()
                    on guarantorTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID into ljRelatedPersonTable
                    from relatedPersonTable in ljRelatedPersonTable.DefaultIfEmpty()
                    join nationality in db.Set<Nationality>()
                    on relatedPersonTable.NationalityGUID equals nationality.NationalityGUID into ljNationality
                    from nationality in ljNationality.DefaultIfEmpty()
                    join race in db.Set<Race>()
                    on relatedPersonTable.RaceGUID equals race.RaceGUID into ljRace
                    from race in ljRace.DefaultIfEmpty()
                    join creditAppRequestTable in db.Set<CreditAppRequestTable>()
                    on guarantorTrans.RefGUID equals creditAppRequestTable.CreditAppRequestTableGUID into ljCreditAppRequestTable
                    from creditAppRequestTable in ljCreditAppRequestTable.DefaultIfEmpty()
                    join creditAppTable in db.Set<CreditAppTable>()
                    on guarantorTrans.RefGUID equals creditAppTable.CreditAppTableGUID into ljCreditAppTable
                    from creditAppTable in ljCreditAppTable.DefaultIfEmpty()
                    select new GuarantorTransItemViewMap
                    {
                        CompanyGUID = guarantorTrans.CompanyGUID,
                        CompanyId = company.CompanyId,
                        Owner = guarantorTrans.Owner,
                        OwnerBusinessUnitGUID = guarantorTrans.OwnerBusinessUnitGUID,
                        OwnerBusinessUnitId = ownerBU.BusinessUnitId,
                        CreatedBy = guarantorTrans.CreatedBy,
                        CreatedDateTime = guarantorTrans.CreatedDateTime,
                        ModifiedBy = guarantorTrans.ModifiedBy,
                        ModifiedDateTime = guarantorTrans.ModifiedDateTime,
                        GuarantorTransGUID = guarantorTrans.GuarantorTransGUID,
                        Affiliate = guarantorTrans.Affiliate,
                        GuarantorTypeGUID = guarantorTrans.GuarantorTypeGUID,
                        InActive = guarantorTrans.InActive,
                        Ordering = guarantorTrans.Ordering,
                        RefGuarantorTransGUID = guarantorTrans.RefGuarantorTransGUID,
                        RefGUID = guarantorTrans.RefGUID,
                        RefType = guarantorTrans.RefType,
                        RelatedPersonTableGUID = guarantorTrans.RelatedPersonTableGUID,
                        RelatedPersonTable_BackgroundSummary = relatedPersonTable.BackgroundSummary,
                        RelatedPersonTable_DateOfBirth = relatedPersonTable.DateOfBirth,
                        RelatedPersonTable_Email = relatedPersonTable.Email,
                        RelatedPersonTable_Extension = relatedPersonTable.Extension,
                        RelatedPersonTable_Fax = relatedPersonTable.Fax,
                        RelatedPersonTable_IdentificationType = relatedPersonTable.IdentificationType,
                        RelatedPersonTable_LineId = relatedPersonTable.LineId,
                        RelatedPersonTable_Mobile = relatedPersonTable.Mobile,
                        RelatedPersonTable_Name = relatedPersonTable.Name,
                        RelatedPersonTable_NationalityId = nationality.NationalityId,
                        RelatedPersonTable_PassportId = relatedPersonTable.PassportId,
                        RelatedPersonTable_Phone = relatedPersonTable.Phone,
                        RelatedPersonTable_RaceId = race.RaceId,
                        RelatedPersonTable_TaxId = relatedPersonTable.TaxId,
                        RelatedPersonTable_WorkPermitId = relatedPersonTable.WorkPermitId,
                        Age = relatedPersonTableService.CalcAge(relatedPersonTable.DateOfBirth),
                        RefID = (customerTable != null && guarantorTrans.RefType == (int)RefType.Customer) ? customerTable.CustomerId :
                                (creditAppRequestTable != null && guarantorTrans.RefType == (int)RefType.CreditAppRequestTable) ? creditAppRequestTable.CreditAppRequestId :
                                (creditAppTable != null && guarantorTrans.RefType == (int)RefType.CreditAppTable) ? creditAppTable.CreditAppId : null,
                        MaximumGuaranteeAmount=guarantorTrans.MaximumGuaranteeAmount,
                    
                        RowVersion = guarantorTrans.RowVersion,
                    });
        }
        public GuarantorTransItemView GetByIdvw(Guid guid)
        {
            try
            {
                var predicate = base.GetByIdvwFilterLevelPredicate(guid);
                var item = GetItemQuery()
                                    .Where(predicate.Predicates, predicate.Values)
                                    .AsNoTracking()
                                    .FirstOrDefault()
                                    .CheckDataNotNull()
                                    .ToMap<GuarantorTransItemViewMap, GuarantorTransItemView>();
                return item;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion GetByIdvw
        #region Create, Update, Delete
        public GuarantorTrans CreateGuarantorTrans(GuarantorTrans guarantorTrans)
        {
            try
            {
                guarantorTrans.GuarantorTransGUID = Guid.NewGuid();
                base.Add(guarantorTrans);
                return guarantorTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateGuarantorTransVoid(GuarantorTrans guarantorTrans)
        {
            try
            {
                CreateGuarantorTrans(guarantorTrans);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GuarantorTrans UpdateGuarantorTrans(GuarantorTrans dbGuarantorTrans, GuarantorTrans inputGuarantorTrans, List<string> skipUpdateFields = null)
        {
            try
            {
                dbGuarantorTrans = dbGuarantorTrans.MapUpdateValues<GuarantorTrans>(inputGuarantorTrans);
                base.Update(dbGuarantorTrans);
                return dbGuarantorTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateGuarantorTransVoid(GuarantorTrans dbGuarantorTrans, GuarantorTrans inputGuarantorTrans, List<string> skipUpdateFields = null)
        {
            try
            {
                dbGuarantorTrans = dbGuarantorTrans.MapUpdateValues<GuarantorTrans>(inputGuarantorTrans, skipUpdateFields);
                base.Update(dbGuarantorTrans);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Create, Update, Delete
        public IEnumerable<GuarantorTrans> GetByActive(Guid companyGUID)
        {
            var result = new SearchResult<GuarantorTransListView>();
            try
            {
                return Entity.Where(t => t.CompanyGUID == companyGUID && t.InActive == false).AsNoTracking();
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public IEnumerable<GuarantorTrans> GetGuarantorTransByReference(Guid refId, int refType)
        {
            try
            {
                Guid companyGUID = SysParm.CompanyGUID.StringToGuid();
                return Entity.Where(t => t.CompanyGUID == companyGUID && t.RefGUID == refId && t.RefType == refType).AsNoTracking();
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public override void ValidateAdd(IEnumerable<GuarantorTrans> items)
        {
            foreach (var item in items)
            {
                ValidateAdd(item);
            }
        }
        public override void ValidateAdd(GuarantorTrans item)
        {
            CheckDupplicate(item);
            base.ValidateAdd(item);
        }
        public override void ValidateUpdate(IEnumerable<GuarantorTrans> items)
        {
            ValidateUpdate(items);
        }
        public override void ValidateUpdate(GuarantorTrans item)
        {
            CheckDupplicate(item);
            base.ValidateUpdate(item);
        }
        public void CheckDupplicate(GuarantorTrans item)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IEnumerable<GuarantorTrans> guarantorTrans = GetGuarantorTransByReference(item.RefGUID, item.RefType);
                if (guarantorTrans.Any())
                {
                    guarantorTrans = guarantorTrans.Where(w => w.InActive == false);
                }
                bool isDupplicateOrdering = guarantorTrans.Any(a => a.Ordering == item.Ordering && a.GuarantorTransGUID != item.GuarantorTransGUID);
                bool isDupplicateRelatedPerson = guarantorTrans.Any(a => a.RelatedPersonTableGUID == item.RelatedPersonTableGUID
                                                                 && a.GuarantorTransGUID != item.GuarantorTransGUID);

                if (isDupplicateOrdering)
                {
                    ex.AddData("ERROR.DUPLICATE", new string[] { "LABEL.ORDERING", "LABEL.REF_ID", "LABEL.IN_ACTIVE" });
                }
                if (isDupplicateRelatedPerson)
                {
                    ex.AddData("ERROR.DUPLICATE", new string[] { "LABEL.RELATED_PERSON_ID", "LABEL.REF_ID", "LABEL.IN_ACTIVE" });
                }
                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public IEnumerable<GuarantorTrans> GetAuthorizedPersonTransByRefGuarantorTrans(List<Guid> refGuarantorTrans)
        {
            try
            {
                return Entity.Where(w => refGuarantorTrans.Any(a => a == w.GuarantorTransGUID)).AsNoTracking();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<GuarantorTransItemView> GetGuarantorTransListForCopyFromCA(string creditAppRequstGUID, int refType, string refGuaratorTransGUID, bool inActive, bool affliate)
        {
            try
            {
                if (affliate)
                {
                    return (from guarantorTrans in db.Set<GuarantorTrans>()
                            join relatedPersonTable in db.Set<RelatedPersonTable>()
                            on guarantorTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID into ljRelatedPersonTable
                            from relatedPersonTable in ljRelatedPersonTable.DefaultIfEmpty()
                            join addressTransTrans in db.Set<AddressTrans>().Where(wh=>wh.Primary)
                            on relatedPersonTable.RelatedPersonTableGUID equals addressTransTrans.RefGUID into ljaddressTransTrans
                            from addressTransTrans in ljaddressTransTrans.DefaultIfEmpty()
                            select new GuarantorTransItemView
                            {
                                CompanyGUID = guarantorTrans.CompanyGUID.ToString(),
                                Owner = guarantorTrans.Owner,
                                OwnerBusinessUnitGUID = guarantorTrans.OwnerBusinessUnitGUID.ToString(),
                                GuarantorTransGUID = guarantorTrans.GuarantorTransGUID.ToString(),
                                Ordering = guarantorTrans.Ordering,
                                GuarantorTypeGUID = guarantorTrans.GuarantorTypeGUID.ToString(),
                                Affiliate = guarantorTrans.Affiliate,
                                InActive = guarantorTrans.InActive,
                                RefGUID = guarantorTrans.RefGUID.ToString(),
                                RefType = guarantorTrans.RefType,
                                RefGuarantorTransGUID = guarantorTrans.RefGuarantorTransGUID.ToString(),
                                RelatedPersonTable_Name = relatedPersonTable.Name,
                                RelatedPersonTable_TaxId = relatedPersonTable.TaxId,
                                RelatedPersonTable_PassportId = relatedPersonTable.PassportId,
                                RelatedPersonTable_WorkPermitId = relatedPersonTable.WorkPermitId,
                                RelatedPersonTable_DateOfIssue = relatedPersonTable.DateOfIssue.DateNullToString(),
                                RelatedPersonTable_DateOfBirth = relatedPersonTable.DateOfBirth.DateNullToString(),
                                RelatedPersonTable_NationalityGUID = relatedPersonTable.NationalityGUID.GuidNullToString(),
                                RelatedPersonTable_RaceGUID = relatedPersonTable.RaceGUID.GuidNullToString(),
                                RelatedPersonTable_Address1 = addressTransTrans != null ? addressTransTrans.Address1 : "",
                                RelatedPersonTable_Address2 = addressTransTrans != null ? addressTransTrans.Address2 : "",
                                MaximumGuaranteeAmount = guarantorTrans.MaximumGuaranteeAmount,
                                RelatedPersonTable_OperatedBy = relatedPersonTable.OperatedBy

                            }).Where(w => w.RefGUID == creditAppRequstGUID && w.RefType == refType && w.RefGuarantorTransGUID == refGuaratorTransGUID && w.InActive == inActive && w.Affiliate == affliate).ToList();
                }
                else
                {
                    return (from guarantorTrans in db.Set<GuarantorTrans>()
                            join relatedPersonTable in db.Set<RelatedPersonTable>()
                            on guarantorTrans.RelatedPersonTableGUID equals relatedPersonTable.RelatedPersonTableGUID into ljRelatedPersonTable
                            from relatedPersonTable in ljRelatedPersonTable.DefaultIfEmpty()
                            join addressTransTrans in db.Set<AddressTrans>().Where(wh => wh.Primary)
                            on relatedPersonTable.RelatedPersonTableGUID equals addressTransTrans.RefGUID into ljaddressTransTrans
                            from addressTransTrans in ljaddressTransTrans.DefaultIfEmpty()
                            select new GuarantorTransItemView
                            {
                                CompanyGUID = guarantorTrans.CompanyGUID.ToString(),
                                Owner = guarantorTrans.Owner,
                                OwnerBusinessUnitGUID = guarantorTrans.OwnerBusinessUnitGUID.ToString(),
                                GuarantorTransGUID = guarantorTrans.GuarantorTransGUID.ToString(),
                                Ordering = guarantorTrans.Ordering,
                                GuarantorTypeGUID = guarantorTrans.GuarantorTypeGUID.ToString(),
                                Affiliate = guarantorTrans.Affiliate,
                                InActive = guarantorTrans.InActive,
                                RefGUID = guarantorTrans.RefGUID.ToString(),
                                RefType = guarantorTrans.RefType,
                                RefGuarantorTransGUID = guarantorTrans.RefGuarantorTransGUID.ToString(),
                                RelatedPersonTable_Name = relatedPersonTable.Name,
                                RelatedPersonTable_TaxId = relatedPersonTable.TaxId,
                                RelatedPersonTable_PassportId = relatedPersonTable.PassportId,
                                RelatedPersonTable_WorkPermitId = relatedPersonTable.WorkPermitId,
                                RelatedPersonTable_DateOfIssue = relatedPersonTable.DateOfIssue.DateNullToString(),
                                RelatedPersonTable_DateOfBirth = relatedPersonTable.DateOfBirth.DateNullToString(),
                                RelatedPersonTable_NationalityGUID = relatedPersonTable.NationalityGUID.GuidNullToString(),
                                RelatedPersonTable_RaceGUID = relatedPersonTable.RaceGUID.GuidNullToString(),
                                RelatedPersonTable_Address1 = addressTransTrans != null ? addressTransTrans.Address1 : "",
                                RelatedPersonTable_Address2 = addressTransTrans != null ? addressTransTrans.Address2 : "",
                                MaximumGuaranteeAmount = guarantorTrans.MaximumGuaranteeAmount,
                                RelatedPersonTable_OperatedBy = relatedPersonTable.OperatedBy

                            }).Where(w => w.RefGUID == creditAppRequstGUID && w.RefType == refType && w.RefGuarantorTransGUID == refGuaratorTransGUID && w.InActive == inActive).ToList();
                }

                    

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}

