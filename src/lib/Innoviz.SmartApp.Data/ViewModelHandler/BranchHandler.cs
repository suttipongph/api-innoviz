﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Innoviz.SmartApp.Data.ViewModelHandler
{
    public static class BranchHandler
    {
        public static List<BranchView> GetBranchViewValidation(this List<BranchView> branchViews)
        {
            var result = new List<BranchView>();
            try
            {
                foreach (BranchView branch in branchViews)
                {
                    result.Add(branch.GetBranchViewValidation());
                }
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static BranchView GetBranchViewValidation(this BranchView branchView)
        {
            try
            {
                branchView.RowAuthorize = branchView.GetRowAuthorize();
                return branchView;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        private static int GetRowAuthorize(this BranchView branchView)
        {
            return AccessMode.Full.GetAttrValue();
        }
        public static Branch ToBranch(this BranchView branchView)
        {
            try
            {
                Branch branch = new Branch();
                branch.CreatedBy = branchView.CreatedBy;
                branch.CreatedDateTime = branchView.CreatedDateTime.StringToSystemDateTime();
                branch.ModifiedBy = branchView.ModifiedBy;
                branch.ModifiedDateTime = branchView.ModifiedDateTime.StringToSystemDateTime();
                branch.CompanyId = branchView.CompanyId;
                branch.BranchId = branchView.BranchId;
                branch.Name = branchView.Name;
                branch.BranchGUID = branchView.BranchGUID.StringToGuid();
                branch.CompanyGUID = branchView.CompanyGUID.StringToGuid();
                branch.TaxBranchGUID = branchView.TaxBranchGUID.StringToGuidNull();

                branch.RowVersion = branchView.RowVersion;
                return branch;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static IEnumerable<Branch> ToBranch(this IEnumerable<BranchView> branchViews)
        {
            try
            {
                List<Branch> branchs = new List<Branch>();
                foreach (BranchView item in branchViews)
                {
                    branchs.Add(item.ToBranch());
                }
                return branchs;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static BranchView ToBranchView(this Branch branch)
        {
            try
            {
                BranchView branchView = new BranchView();
                branchView.CreatedBy = branch.CreatedBy;
                branchView.CreatedDateTime = branch.CreatedDateTime.DateTimeToString();
                branchView.ModifiedBy = branch.ModifiedBy;
                branchView.ModifiedDateTime = branch.ModifiedDateTime.DateTimeToString();
                branchView.CompanyId = branch.CompanyId;
                branchView.BranchId = branch.BranchId;
                branchView.Name = branch.Name;
                branchView.BranchGUID = branch.BranchGUID.GuidNullToString();
                branchView.CompanyGUID = branch.CompanyGUID.GuidNullToString();
                branchView.TaxBranchGUID = branch.TaxBranchGUID.GuidNullToString();

                branchView.RowVersion = branch.RowVersion;
                return branchView.GetBranchViewValidation();
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static IEnumerable<BranchView> ToBranchView(this IEnumerable<Branch> branchs)
        {
            try
            {
                List<BranchView> branchViews = new List<BranchView>();
                foreach (Branch item in branchs)
                {
                    branchViews.Add(item.ToBranchView());
                }
                return branchViews;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static SelectItem<BranchView> ToDropDownItem(this Branch branch)
        {
            try
            {
                SelectItem<BranchView> selectItem = new SelectItem<BranchView>();
                selectItem.Label = "[" + branch.BranchId + "] " + branch.Name;
                selectItem.Value = branch.BranchGUID.GuidNullToString();
                selectItem.RowData = branch.ToBranchView();
                return selectItem;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static IEnumerable<SelectItem<BranchView>> ToDropDownItem(this IEnumerable<Branch> branchs)
        {
            try
            {
                List<SelectItem<BranchView>> selectItems = new List<SelectItem<BranchView>>();
                foreach (Branch item in branchs)
                {
                    selectItems.Add(item.ToDropDownItem());
                }
                return selectItems.OrderBy(o => o.RowData.BranchId);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
    }
}