﻿using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewModelHandler.Models
{
    public class SysUserTableViewMap : IViewEntityMap
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string NormalizedEmail { get; set; }
        public string NormalizedUserName { get; set; }
        public string PhoneNumber { get; set; }
        public string Name { get; set; }
        public string DateFormatId { get; set; }
        public string TimeFormatId { get; set; }
        public string NumberFormatId { get; set; }
        public bool InActive { get; set; }
        public string LanguageId { get; set; }

        public bool ShowSystemLog { get; set; }

        public Guid? DefaultCompanyGUID { get; set; }
        public Guid? DefaultBranchGUID { get; set; }

        public string Company_CompanyId { get; set; }
        public string Branch_BranchId { get; set; }

        public string UserImage { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDateTime { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
