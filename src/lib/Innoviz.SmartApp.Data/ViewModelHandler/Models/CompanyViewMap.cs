﻿using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewModelHandler.Models
{
    public class CompanyViewMap : IViewEntityMap
    {
        public string CompanyId { get; set; }
        public Guid CompanyGUID { get; set; }
        public string Name { get; set; }
        public string CompanyLogo { get; set; }
        public string SecondName { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDateTime { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
