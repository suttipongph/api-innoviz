﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelHandler.Models
{
    public class BranchViewMap : ViewCompanyBaseEntityMap
    {
        public string BranchId { get; set; }
        public string Name { get; set; }
        public Guid BranchGUID { get; set; }
        public Guid? ApplicationNumberSeqGUID { get; set; }
        public Guid? TaxBranchGUID { get; set; }

        public string ApplicationNumberSeq_NumberSeqCode { get; set; }
        public string ApplicationNumberSeq_Description { get; set; }
        public string TaxBranch_BranchId { get; set; }
    }
}
