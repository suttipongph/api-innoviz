﻿using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewModelHandler.Models
{
    public class SysRoleTableViewMap : IViewEntityMap, IViewCompanyEntityMap
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string NormalizedName { get; set; }
        public string DisplayName { get; set; }

        public int SiteLoginType { get; set; }
        public string Company_Name { get; set; }

        public Guid CompanyGUID { get; set; }
        public string CompanyId { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDateTime { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
