﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelHandler {
    public static class StaticDataHandler {

        public static SysFeatureTable ToSysFeatureTable(this StaticSysFeatureTableData staticData) {
            try {
                SysFeatureTable result = new SysFeatureTable();
                result.SysFeatureTableGUID = staticData.SysFeatureTableGUID;
                result.ParentFeatureId = staticData.ParentFeatureId;
                result.FeatureId = staticData.FeatureId;
                result.Path = staticData.Path;
                result.CreatedDateTime = new DateTime();
                result.ModifiedDateTime = new DateTime();
                result.CreatedBy = null;
                result.ModifiedBy = null;
                return result;
            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }
        public static IEnumerable<SysFeatureTable> ToSysFeatureTables(this IEnumerable<StaticSysFeatureTableData> staticFeatureTables) {
            try {
                List<SysFeatureTable> featureTables = new List<SysFeatureTable>();
                foreach (var item in staticFeatureTables) {
                    featureTables.Add(item.ToSysFeatureTable());
                }
                return featureTables.AsEnumerable();
            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static StaticSysFeatureTableData ToStaticSysFeatureTableData(this SysFeatureTable featureTable) {
            try {
                StaticSysFeatureTableData staticSysFeatureTable = new StaticSysFeatureTableData();
                staticSysFeatureTable.FeatureId = featureTable.FeatureId;
                staticSysFeatureTable.ParentFeatureId = featureTable.ParentFeatureId;
                staticSysFeatureTable.Path = featureTable.Path;
                staticSysFeatureTable.SysFeatureTableGUID = featureTable.SysFeatureTableGUID;
                return staticSysFeatureTable;
            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static IEnumerable<StaticSysFeatureTableData> ToStaticSysFeatureTableDatas(this IEnumerable<SysFeatureTable> featureTables) {
            try {
                List<StaticSysFeatureTableData> result = new List<StaticSysFeatureTableData>();
                foreach (var item in featureTables) {
                    result.Add(item.ToStaticSysFeatureTableData());
                }
                return result.AsEnumerable();
            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public static SysMessage ToSysMessage(this StaticSysMessageData staticSysMessage) {
            try {
                SysMessage result = new SysMessage();
                result.Code = staticSysMessage.Code;
                result.Message = staticSysMessage.Message;

                return result;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static IEnumerable<SysMessage> ToSysMessages(this IEnumerable<StaticSysMessageData> staticSysMessages) {
            try {
                List<SysMessage> result = new List<SysMessage>();
                foreach (var item in staticSysMessages) {
                    result.Add(item.ToSysMessage());
                }

                return result.AsEnumerable();
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static StaticSysMessageData ToStaticSysMessageData(this SysMessage sysMessage) {
            try {
                StaticSysMessageData result = new StaticSysMessageData();
                result.Code = sysMessage.Code;
                result.Message = sysMessage.Message;

                return result;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static IEnumerable<StaticSysMessageData> ToStaticSysMessages(this IEnumerable<SysMessage> sysMessages) {
            try {
                List<StaticSysMessageData> result = new List<StaticSysMessageData>();
                foreach (var item in sysMessages) {
                    result.Add(item.ToStaticSysMessageData());
                }
                return result.AsEnumerable();
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public static StaticSysDuplicateDetectionData ToStaticSysDuplicateDetectionData(this SysDuplicateDetection sysDuplicateDetection)
        {
            try
            {
                StaticSysDuplicateDetectionData result = new StaticSysDuplicateDetectionData();
                result.ModelName = sysDuplicateDetection.ModelName;
                result.PropertyName = sysDuplicateDetection.PropertyName;
                result.LabelName = sysDuplicateDetection.LabelName;
                result.RuleNum = sysDuplicateDetection.RuleNum;
                result.System = sysDuplicateDetection.System;
                result.InActive = sysDuplicateDetection.InActive;

                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static IEnumerable<StaticSysDuplicateDetectionData> ToStaticSysDuplicateDetections(this IEnumerable<SysDuplicateDetection> sysDuplicateDetections)
        {
            try
            {
                List<StaticSysDuplicateDetectionData> result = new List<StaticSysDuplicateDetectionData>();
                foreach (var item in sysDuplicateDetections)
                {
                    result.Add(item.ToStaticSysDuplicateDetectionData());
                }
                return result.AsEnumerable();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static StaticSysControllerEntityTempData ToStaticSysControllerEntityTempData(this SysControllerEntityMappingView sysControllerEntityMapping)
        {
            try
            {
                StaticSysControllerEntityTempData result = new StaticSysControllerEntityTempData();
                result.ModelName = sysControllerEntityMapping.ModelName;
                result.SysControllerTable_RouteAttribute = sysControllerEntityMapping.SysControllerTable_RouteAttribute;
                result.SysControllerTable_ControllerName = sysControllerEntityMapping.SysControllerTable_ControllerName;

                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static List<StaticSysControllerEntityTempData> ToStaticSysControllerEntityTempData(this IEnumerable<SysControllerEntityMappingView> sysControllerEntityMappings)
        {
            try
            {
                List<StaticSysControllerEntityTempData> result = new List<StaticSysControllerEntityTempData>();
                foreach (var item in sysControllerEntityMappings)
                {
                    result.Add(item.ToStaticSysControllerEntityTempData());
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
