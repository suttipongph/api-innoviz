﻿using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelHandler
{
    public static class TreeViewHandler
    {
        public static IEnumerable<TNode> Flatten<TNode>(this IEnumerable<TNode> nodes, Func<TNode, IEnumerable<TNode>> childrenSelector)
        {
            if (nodes == null) throw new ArgumentNullException(nameof(nodes));
            return nodes.SelectMany(c => childrenSelector(c).Flatten(childrenSelector)).Concat(nodes);
        }
        public static ITree<T> ToTree<T>(this IList<T> items, Func<T, T, bool> parentSelector)
        {
            if (items == null) throw new ArgumentNullException(nameof(items));
            var lookup = items.ToLookup(item => items.FirstOrDefault(parent => parentSelector(parent, item)),
                child => child);
            return TreeView<T>.FromLookup(lookup);
        }

        //public static ITree<T> GetSubTree<T>(this ITree<T> treeNode, Guid key, Func<T, Guid, bool> keySelector)
        //{
        //    try
        //    {
        //        if (keySelector(treeNode,key))
        //    }
        //    catch (Exception e)
        //    {
        //        throw SmartAppUtil.AddStackTrace(e);
        //    }
        //}
    }
}
