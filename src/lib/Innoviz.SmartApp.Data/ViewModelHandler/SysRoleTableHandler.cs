﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModels;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandler
{
    public static class SysRoleTableHandler
    {
        #region SysRoleTableListView
        public static List<SysRoleTableListView> GetSysRoleTableListViewValidation(this List<SysRoleTableListView> sysRoleTableListViews, bool skipMethod = false)
        {
            if (skipMethod)
            {
                return sysRoleTableListViews;
            }
            var result = new List<SysRoleTableListView>();
            try
            {
                foreach (SysRoleTableListView item in sysRoleTableListViews)
                {
                    result.Add(item.GetSysRoleTableListViewValidation());
                }
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static SysRoleTableListView GetSysRoleTableListViewValidation(this SysRoleTableListView sysRoleTableListView)
        {
            try
            {
                sysRoleTableListView.RowAuthorize = sysRoleTableListView.GetListRowAuthorize();
                return sysRoleTableListView;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        private static int GetListRowAuthorize(this SysRoleTableListView sysRoleTableListView)
        {
            return AccessMode.Full.GetAttrValue();
        }
        #endregion SysRoleTableListView
        #region SysRoleTableItemView
        public static SysRoleTableItemView GetSysRoleTableItemViewValidation(this SysRoleTableItemView sysRoleTableItemView)
        {
            try
            {
                sysRoleTableItemView.RowAuthorize = sysRoleTableItemView.GetItemRowAuthorize();
                return sysRoleTableItemView;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        private static int GetItemRowAuthorize(this SysRoleTableItemView sysRoleTableItemView)
        {
            return AccessMode.Full.GetAttrValue();
        }
        public static SysRoleTable ToSysRoleTable(this SysRoleTableItemView sysRoleTableItemView)
        {
            try
            {
                SysRoleTable sysRoleTable = new SysRoleTable();
                sysRoleTable.CompanyGUID = sysRoleTableItemView.CompanyGUID.StringToGuid();
                sysRoleTable.CreatedBy = sysRoleTableItemView.CreatedBy;
                sysRoleTable.CreatedDateTime = sysRoleTableItemView.CreatedDateTime.StringToSystemDateTime();
                sysRoleTable.ModifiedBy = sysRoleTableItemView.ModifiedBy;
                sysRoleTable.ModifiedDateTime = sysRoleTableItemView.ModifiedDateTime.StringToSystemDateTime();
                sysRoleTable.Owner = sysRoleTableItemView.Owner;
                sysRoleTable.OwnerBusinessUnitGUID = sysRoleTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
                sysRoleTable.Id = sysRoleTableItemView.Id.StringToGuid();
                sysRoleTable.Name = sysRoleTableItemView.Name;
                sysRoleTable.NormalizedName = sysRoleTableItemView.NormalizedName;
                sysRoleTable.DisplayName = sysRoleTableItemView.DisplayName;
                sysRoleTable.SiteLoginType = sysRoleTableItemView.SiteLoginType;

                sysRoleTable.RowVersion = sysRoleTableItemView.RowVersion;
                return sysRoleTable;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static IEnumerable<SysRoleTable> ToSysRoleTable(this IEnumerable<SysRoleTableItemView> sysRoleTableItemViews)
        {
            try
            {
                List<SysRoleTable> sysRoleTables = new List<SysRoleTable>();
                foreach (SysRoleTableItemView item in sysRoleTableItemViews)
                {
                    sysRoleTables.Add(item.ToSysRoleTable());
                }
                return sysRoleTables;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static SysRoleTableItemView ToSysRoleTableItemView(this SysRoleTable sysRoleTable)
        {
            try
            {
                SysRoleTableItemView sysRoleTableItemView = new SysRoleTableItemView();
                sysRoleTableItemView.CompanyGUID = sysRoleTable.CompanyGUID.GuidNullToString();
                sysRoleTableItemView.CreatedBy = sysRoleTable.CreatedBy;
                sysRoleTableItemView.CreatedDateTime = sysRoleTable.CreatedDateTime.DateTimeToString();
                sysRoleTableItemView.ModifiedBy = sysRoleTable.ModifiedBy;
                sysRoleTableItemView.ModifiedDateTime = sysRoleTable.ModifiedDateTime.DateTimeToString();
                sysRoleTableItemView.Owner = sysRoleTable.Owner;
                sysRoleTableItemView.OwnerBusinessUnitGUID = sysRoleTable.OwnerBusinessUnitGUID.GuidNullToString();
                sysRoleTableItemView.Id = sysRoleTable.Id.GuidNullOrEmptyToString();
                sysRoleTableItemView.Name = sysRoleTable.Name;
                sysRoleTableItemView.NormalizedName = sysRoleTable.NormalizedName;
                sysRoleTableItemView.DisplayName = sysRoleTable.DisplayName;
                sysRoleTableItemView.SiteLoginType = sysRoleTable.SiteLoginType;

                sysRoleTableItemView.RowVersion = sysRoleTable.RowVersion;
                return sysRoleTableItemView.GetSysRoleTableItemViewValidation();
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion SysRoleTableItemView
        #region ToDropDown
        public static SelectItem<SysRoleTableItemView> ToDropDownItem(this SysRoleTableItemView sysRoleTableView)
        {
            try
            {
                SelectItem<SysRoleTableItemView> selectItem = new SelectItem<SysRoleTableItemView>();
                selectItem.Label = SmartAppUtil.GetDropDownLabel(sysRoleTableView.DisplayName);
                selectItem.Value = sysRoleTableView.Id.GuidNullToString();
                selectItem.RowData = sysRoleTableView;
                return selectItem;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static IEnumerable<SelectItem<SysRoleTableItemView>> ToDropDownItem(this IEnumerable<SysRoleTableItemView> sysRoleTableItemViews)
        {
            try
            {
                List<SelectItem<SysRoleTableItemView>> selectItems = new List<SelectItem<SysRoleTableItemView>>();
                foreach (SysRoleTableItemView item in sysRoleTableItemViews)
                {
                    selectItems.Add(item.ToDropDownItem());
                }
                return selectItems.OrderBy(o => o.Label);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion ToDropDown
    }
}