﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.Constants;
using System.Linq;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data.ViewModelsV2;

namespace Innoviz.SmartApp.Data.ViewModelHandler
{
    public static class SysUserTableHandler
    {
        public static SysUserTable ToSysUserTable(this SysUserSettingsItemView sysUserSettingsView)
        {
            try
            {
                SysUserTable sysUserTable = new SysUserTable();
                sysUserTable.Id = sysUserSettingsView.Id.StringToGuid();
                sysUserTable.UserName = sysUserSettingsView.UserName;
                sysUserTable.NormalizedUserName = (sysUserSettingsView.UserName != null) ?
                    sysUserSettingsView.UserName.ToUpper() : null;
                sysUserTable.Email = sysUserSettingsView.Email;
                sysUserTable.NormalizedEmail = (sysUserSettingsView.Email != null) ? 
                        sysUserSettingsView.Email.ToUpper() : null;
                sysUserTable.PhoneNumber = sysUserSettingsView.PhoneNumber;
                sysUserTable.Name = sysUserSettingsView.Name;
                sysUserTable.DateFormatId = sysUserSettingsView.DateFormatId;
                sysUserTable.TimeFormatId = sysUserSettingsView.TimeFormatId;
                sysUserTable.NumberFormatId = sysUserSettingsView.NumberFormatId;
                sysUserTable.InActive = sysUserSettingsView.InActive;
                sysUserTable.LanguageId = sysUserSettingsView.LanguageId;
                sysUserTable.ShowSystemLog = sysUserSettingsView.ShowSystemLog;
                sysUserTable.DefaultCompanyGUID = (sysUserSettingsView.DefaultCompanyGUID != null) ?
                    sysUserSettingsView.DefaultCompanyGUID.StringToGuidNull() : null;
                sysUserTable.DefaultBranchGUID = (sysUserSettingsView.DefaultBranchGUID != null) ?
                    sysUserSettingsView.DefaultBranchGUID.StringToGuidNull() : null;
                sysUserTable.UserImage = sysUserSettingsView.UserImage;

                sysUserTable.CreatedBy = sysUserSettingsView.CreatedBy;
                sysUserTable.ModifiedBy = sysUserSettingsView.ModifiedBy;
                sysUserTable.CreatedDateTime = sysUserSettingsView.CreatedDateTime.StringToDateTime();
                sysUserTable.ModifiedDateTime = sysUserSettingsView.ModifiedDateTime.StringToDateTime();

                sysUserTable.RowVersion = sysUserSettingsView.RowVersion;
                return sysUserTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SysUserSettingsItemView ToSysUserSettingsView(this SysUserTable sysUserTable)
        {
            try
            {
                SysUserSettingsItemView sysUserSettingsView = new SysUserSettingsItemView();
                sysUserSettingsView.Id = sysUserTable.Id.GuidNullToString();
                sysUserSettingsView.UserName = sysUserTable.UserName;
                sysUserSettingsView.Email = sysUserTable.Email;
                sysUserSettingsView.PhoneNumber = sysUserTable.PhoneNumber;
                sysUserSettingsView.Name = sysUserTable.Name;
                sysUserSettingsView.DateFormatId = sysUserTable.DateFormatId;
                sysUserSettingsView.TimeFormatId = sysUserTable.TimeFormatId;
                sysUserSettingsView.NumberFormatId = sysUserTable.NumberFormatId;
                sysUserSettingsView.InActive = sysUserTable.InActive;
                sysUserSettingsView.LanguageId = sysUserTable.LanguageId;
                sysUserSettingsView.ShowSystemLog = sysUserTable.ShowSystemLog;
                sysUserSettingsView.DefaultCompanyGUID = sysUserTable.DefaultCompanyGUID.GuidNullToString();
                sysUserSettingsView.DefaultBranchGUID = sysUserTable.DefaultBranchGUID.GuidNullToString();
                sysUserSettingsView.UserImage = sysUserTable.UserImage;

                sysUserSettingsView.CreatedBy = sysUserTable.CreatedBy;
                sysUserSettingsView.ModifiedBy = sysUserTable.ModifiedBy;
                sysUserSettingsView.CreatedDateTime = sysUserTable.CreatedDateTime.DateTimeToString();
                sysUserSettingsView.ModifiedDateTime = sysUserTable.ModifiedDateTime.DateTimeToString();

                sysUserSettingsView.RowVersion = sysUserTable.RowVersion;
                return sysUserSettingsView;

            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        #region SysUserTableListView
        public static List<SysUserTableListView> GetSysUserTableListViewValidation(this List<SysUserTableListView> sysUserTableListViews, bool skipMethod = false)
        {
            if (skipMethod)
            {
                return sysUserTableListViews;
            }
            var result = new List<SysUserTableListView>();
            try
            {
                foreach (SysUserTableListView item in sysUserTableListViews)
                {
                    result.Add(item.GetSysUserTableListViewValidation());
                }
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static SysUserTableListView GetSysUserTableListViewValidation(this SysUserTableListView sysUserTableListView)
        {
            try
            {
                sysUserTableListView.RowAuthorize = sysUserTableListView.GetListRowAuthorize();
                return sysUserTableListView;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        private static int GetListRowAuthorize(this SysUserTableListView sysUserTableListView)
        {
            return AccessMode.Full.GetAttrValue();
        }
        #endregion SysUserTableListView
        #region SysUserTableItemView
        public static SysUserTableItemView GetSysUserTableItemViewValidation(this SysUserTableItemView sysUserTableItemView)
        {
            try
            {
                sysUserTableItemView.RowAuthorize = sysUserTableItemView.GetItemRowAuthorize();
                return sysUserTableItemView;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        private static int GetItemRowAuthorize(this SysUserTableItemView sysUserTableItemView)
        {
            return AccessMode.Full.GetAttrValue();
        }
        public static SysUserTable ToSysUserTable(this SysUserTableItemView sysUserTableItemView)
        {
            try
            {
                SysUserTable sysUserTable = new SysUserTable();
                sysUserTable.CreatedBy = sysUserTableItemView.CreatedBy;
                sysUserTable.CreatedDateTime = sysUserTableItemView.CreatedDateTime.StringToSystemDateTime();
                sysUserTable.ModifiedBy = sysUserTableItemView.ModifiedBy;
                sysUserTable.ModifiedDateTime = sysUserTableItemView.ModifiedDateTime.StringToSystemDateTime();
                sysUserTable.Id = sysUserTableItemView.Id.StringToGuid();

                sysUserTable.UserName = sysUserTableItemView.UserName;
                sysUserTable.Email = sysUserTableItemView.Email;
                sysUserTable.NormalizedEmail = sysUserTableItemView.Email != null ? sysUserTableItemView.Email.ToUpper() : null;
                sysUserTable.NormalizedUserName = sysUserTableItemView.UserName != null ? sysUserTableItemView.UserName.ToUpper() : null;
                sysUserTable.PhoneNumber = sysUserTableItemView.PhoneNumber;
                sysUserTable.Name = sysUserTableItemView.Name;
                sysUserTable.DateFormatId = sysUserTableItemView.DateFormatId;
                sysUserTable.TimeFormatId = sysUserTableItemView.TimeFormatId;
                sysUserTable.NumberFormatId = sysUserTableItemView.NumberFormatId;
                sysUserTable.InActive = sysUserTableItemView.InActive;
                sysUserTable.LanguageId = sysUserTableItemView.LanguageId;

                sysUserTable.RowVersion = sysUserTableItemView.RowVersion;
                return sysUserTable;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static IEnumerable<SysUserTable> ToSysUserTable(this IEnumerable<SysUserTableItemView> sysUserTableItemViews)
        {
            try
            {
                List<SysUserTable> sysUserTables = new List<SysUserTable>();
                foreach (SysUserTableItemView item in sysUserTableItemViews)
                {
                    sysUserTables.Add(item.ToSysUserTable());
                }
                return sysUserTables;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static SysUserTableItemView ToSysUserTableItemView(this SysUserTable sysUserTable)
        {
            try
            {
                SysUserTableItemView sysUserTableItemView = new SysUserTableItemView();
                sysUserTableItemView.CreatedBy = sysUserTable.CreatedBy;
                sysUserTableItemView.CreatedDateTime = sysUserTable.CreatedDateTime.DateTimeToString();
                sysUserTableItemView.ModifiedBy = sysUserTable.ModifiedBy;
                sysUserTableItemView.ModifiedDateTime = sysUserTable.ModifiedDateTime.DateTimeToString();
                sysUserTableItemView.UserName = sysUserTable.UserName;
                sysUserTableItemView.Email = sysUserTable.Email;
                sysUserTableItemView.NormalizedEmail = sysUserTable.NormalizedEmail;
                sysUserTableItemView.NormalizedUserName = sysUserTable.NormalizedUserName;
                sysUserTableItemView.PhoneNumber = sysUserTable.PhoneNumber;
                sysUserTableItemView.Name = sysUserTable.Name;
                sysUserTableItemView.DateFormatId = sysUserTable.DateFormatId;
                sysUserTableItemView.TimeFormatId = sysUserTable.TimeFormatId;
                sysUserTableItemView.NumberFormatId = sysUserTable.NumberFormatId;
                sysUserTableItemView.InActive = sysUserTable.InActive;
                sysUserTableItemView.LanguageId = sysUserTable.LanguageId;
                sysUserTableItemView.Id = sysUserTable.Id.GuidNullOrEmptyToString();

                sysUserTableItemView.RowVersion = sysUserTable.RowVersion;
                return sysUserTableItemView.GetSysUserTableItemViewValidation();
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion SysUserTableItemView
        #region ToDropDown
        public static SelectItem<SysUserTableItemView> ToDropDownItem(this SysUserTableItemView sysUserTableView, bool userNameValue)
        {
            try
            {
                SelectItem<SysUserTableItemView> selectItem = new SelectItem<SysUserTableItemView>();
                selectItem.Label = SmartAppUtil.GetDropDownLabel(sysUserTableView.UserName);
                selectItem.Value = userNameValue ? sysUserTableView.UserName : sysUserTableView.Id.GuidNullToString();
                selectItem.RowData = sysUserTableView;
                return selectItem;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static IEnumerable<SelectItem<SysUserTableItemView>> ToDropDownItem(this IEnumerable<SysUserTableItemView> sysUserTableItemViews, bool userNameValue = false)
        {
            try
            {
                List<SelectItem<SysUserTableItemView>> selectItems = new List<SelectItem<SysUserTableItemView>>();
                foreach (SysUserTableItemView item in sysUserTableItemViews)
                {
                    selectItems.Add(item.ToDropDownItem(userNameValue));
                }
                return selectItems.OrderBy(o => o.Label);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion ToDropDown
    }
}