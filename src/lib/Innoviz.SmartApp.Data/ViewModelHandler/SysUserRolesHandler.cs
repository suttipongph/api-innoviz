﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelHandler
{
    public static class SysUserRolesHandler
    {
        public static SysUserRoles ToSysUserRoles(this SysUserRolesView sysUserRolesView)
        {
            try
            {
                SysUserRoles sysUserRoles = new SysUserRoles();
                sysUserRoles.UserId = sysUserRolesView.UserId.StringToGuid();
                sysUserRoles.RoleId = sysUserRolesView.RoleId.StringToGuid();

                return sysUserRoles;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static IEnumerable<SysUserRoles> ToSysUserRoles(this IEnumerable<SysUserRolesView> list)
        {
            try
            {
                List<SysUserRoles> sysUserRolesList = new List<SysUserRoles>();
                foreach (SysUserRolesView item in list)
                {
                    sysUserRolesList.Add(item.ToSysUserRoles());
                }
                return sysUserRolesList;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
    }
}
