﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelHandler
{
    public static class SysUserCompanyMappingHandler
    {
        public static SysUserCompanyMapping ToSysUserCompanyMapping(this SysUserCompanyMappingView viewModel) {
            try {
                SysUserCompanyMapping model = new SysUserCompanyMapping();
                model.UserGUID = viewModel.UserGUID.StringToGuid();
                model.CompanyGUID = viewModel.CompanyGUID.StringToGuid();
                model.BranchGUID = viewModel.BranchGUID.StringToGuid();

                model.CreatedBy = viewModel.CreatedBy;
                model.CreatedDateTime = viewModel.CreatedDateTime.StringToSystemDateTime();
                model.ModifiedBy = viewModel.ModifiedBy;
                model.ModifiedDateTime = viewModel.ModifiedDateTime.StringToSystemDateTime();

                return model;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static IEnumerable<SysUserCompanyMapping> ToSysUserCompanyMapping(this IEnumerable<SysUserCompanyMappingView> list) {
            try {
                List<SysUserCompanyMapping> sysUserCompanyMappings = new List<SysUserCompanyMapping>();
                foreach (SysUserCompanyMappingView item in list) {
                    sysUserCompanyMappings.Add(item.ToSysUserCompanyMapping());
                }
                return sysUserCompanyMappings;
            }
            catch (Exception ex) {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
    }
}
