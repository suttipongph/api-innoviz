﻿using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class CopyAgreementInfoView : ViewCompanyBaseEntity
    {
        public AgreementTableInfo AgreementTableInfo { get; set; }
        public AgreementTableInfoText AgreementTableInfoText { get; set; }
    }
}
