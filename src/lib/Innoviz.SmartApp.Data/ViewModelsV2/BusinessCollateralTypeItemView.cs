using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BusinessCollateralTypeItemView : ViewCompanyBaseEntity
	{
		public string BusinessCollateralTypeGUID { get; set; }
		public int AgreementOrdering { get; set; }
		public string BusinessCollateralTypeId { get; set; }
		public string Description { get; set; }
	}
}
