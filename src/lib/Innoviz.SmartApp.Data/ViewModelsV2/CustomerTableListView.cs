using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CustomerTableListView : ViewCompanyBaseEntity
	{
		public string CustomerTableGUID { get; set; }
		public string CustomerId { get; set; }
		public string TaxID { get; set; }
		public string PassportID { get; set; }
		public string Name { get; set; }
		public int RecordType { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string DocumentStatus_StatusId { get; set; }
	}
}
