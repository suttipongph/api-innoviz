using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ConsortiumTransItemView : ViewCompanyBaseEntity
	{
		public string ConsortiumTransGUID { get; set; }
		public string Address { get; set; }
		public string AuthorizedPersonTypeGUID { get; set; }
		public string CustomerName { get; set; }
		public string OperatedBy { get; set; }
		public int Ordering { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public string Remark { get; set; }
		public string RefId { get; set; }
		public string ConsortiumLineGUID { get; set; }
		public string MainAgreementTable_ConsortiumTableGUID { get; set; }
}
}
