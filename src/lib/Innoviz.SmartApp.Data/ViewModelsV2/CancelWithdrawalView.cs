﻿using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class CancelWithdrawalView
    {
        public string WithdrawalTableGUID { get; set; }
        public string WithdrawalTable_Values { get; set; }
        public string BuyerTable_Values { get; set; }
        public string CreditTerm_Values { get; set; }
        public string CustomerTable_Values { get; set; }
        public string DocumentStatus_Values { get; set; }
        public string DueDate { get; set; }
        public decimal TotalInterestPct { get; set; }
        public decimal WithdrawalAmount { get; set; }
        public string WithdrawalDate { get; set; }
        public string DocumentReasonGUID { get; set; }
    }
    public class CancelWithdrawalResultView : ResultBaseEntity
    {
    }
}
