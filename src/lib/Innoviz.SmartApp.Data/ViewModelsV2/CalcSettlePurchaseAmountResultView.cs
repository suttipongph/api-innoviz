﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class CalcSettlePurchaseAmountResultView
    {
        public decimal BalanceAmount { get; set; }
        public decimal PurchaseAmount { get; set; }
        public decimal PurchaseAmountBalance { get; set; }
        public decimal SettleReserveAmount { get; set; }
        public decimal SettlePurchaseAmount { get; set; }
        public decimal SettleInvoiceAmount { get; set; }
        public decimal ReserveToBeRefund { get; set; }
        public decimal WHTAmount { get; set; }
        public decimal SettleTaxAmount { get; set; }
    }
}
