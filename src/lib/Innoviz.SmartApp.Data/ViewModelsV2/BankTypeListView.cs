using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BankTypeListView : ViewCompanyBaseEntity
	{
		public string BankTypeGUID { get; set; }
		public string BankTypeId { get; set; }
		public string Description { get; set; }
	}
}
