using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class DocumentTemplateTableItemView : ViewCompanyBaseEntity
	{
		public string DocumentTemplateTableGUID { get; set; }
		public string Base64Data { get; set; }
		public string Description { get; set; }
		public int DocumentTemplateType { get; set; }
		public string FileName { get; set; }
		public string FilePath { get; set; }
		public string TemplateId { get; set; }
	}
}
