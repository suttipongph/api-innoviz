using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class VerificationLineListView : ViewCompanyBaseEntity
	{
		public string VerificationLineGUID { get; set; }
		public string VerificationLineID { get; set; }
		public string VerificationTypeGUID { get; set; }
		public bool Pass { get; set; }
		public string VerificationType_Values { get; set; }
	}
}
