using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class TaxReportTransListView : ViewCompanyBaseEntity
	{
		public string TaxReportTransGUID { get; set; }
		public int Year { get; set; }
		public int Month { get; set; }
		public string Description { get; set; }
		public decimal Amount { get; set; }
		public string RefGUID { get; set; }
	}
}
