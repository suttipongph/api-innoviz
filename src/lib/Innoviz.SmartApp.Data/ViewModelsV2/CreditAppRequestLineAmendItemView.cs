using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CreditAppRequestLineAmendItemView : ViewCompanyBaseEntity
	{
		public string CreditAppRequestLineAmendGUID { get; set; }
		public bool AmendAssignmentMethod { get; set; }
		public bool AmendBillingDocumentCondition { get; set; }
		public bool AmendBillingInformation { get; set; }
		public bool AmendRate { get; set; }
		public bool AmendReceiptDocumentCondition { get; set; }
		public bool AmendReceiptInformation { get; set; }
		public string CreditAppRequestLineGUID { get; set; }
		public string OriginalAssignmentMethodGUID { get; set; }
		public string OriginalAssignmentMethodRemark { get; set; }
		public string OriginalBillingAddressGUID { get; set; }
		public decimal OriginalCreditLimitLineRequest { get; set; }
		public decimal OriginalMaxPurchasePct { get; set; }
		public string OriginalMethodOfPaymentGUID { get; set; }
		public int OriginalPurchaseFeeCalculateBase { get; set; }
		public decimal OriginalPurchaseFeePct { get; set; }
		public string OriginalReceiptAddressGUID { get; set; }
		public string OriginalReceiptRemark { get; set; }
		public string OriginalAssignmentMethod_Values { get; set; }
		public string OriginalBillingAddress_Values { get; set; }
		public string OriginalMethodOfPayment_Values { get; set; }
		public string OriginalReceiptAddress_Values { get; set; }
		#region CreditAppRequest
		public string CreditAppRequestTable_CustomerTableGUID { get; set; }
		public string CreditAppRequestTable_CreditAppRequestTableGUID { get; set; }
		public string CreditAppRequestTable_CreditAppRequestId { get; set; }
		public string CreditAppRequestTable_Description { get; set; }
		public int CreditAppRequestTable_CreditAppRequestType { get; set; }
		public string CreditAppRequestTable_RequestDate { get; set; }
		public decimal CreditAppRequestTable_CustomerCreditLimit { get; set; }
		public string CreditAppRequestTable_Remark { get; set; }
		public string CreditAppRequestTable_ApprovedDate { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string CreditAppRequestTable_DocumentStatusGUID { get; set; }
		public string CustomerTable_Values { get; set; }
		public string RefCreditAppTable_Values { get; set; }
		public string RefCreditAppTableGUID { get; set; }
		public string UnboundOriginalBillingAddress { get; set; }
		public string UnboundOriginalReceiptAddress { get; set; }
		public string CreditAppRequestTable_CACondition { get; set; }
		#endregion CreditAppRequest
		#region CreditAppRequestLine
		public decimal CreditAppRequestLine_NewCreditLimitLineRequest { get; set; }
		public decimal CreditAppRequestLine_BuyerCreditLimit { get; set; }
		public decimal CreditAppRequestLine_ApprovedCreditLimitLineRequest { get; set; }
		public decimal CreditAppRequestLine_NewMaxPurchasePct { get; set; }
		public decimal CreditAppRequestLine_NewPurchaseFeePct { get; set; }
		public int CreditAppRequestLine_NewPurchaseFeeCalculateBase { get; set; }
		public string CreditAppRequestLine_AssignmentMethodGUID { get; set; }
		public string CreditAppRequestLine_NewAssignmentMethodRemark { get; set; }
		public string CreditAppRequestLine_MarketingComment { get; set; }
		public string CreditAppRequestLine_CreditComment { get; set; }
		public string CreditAppRequestLine_ApproverComment { get; set; }
		public string CreditAppRequestLine_NewMethodOfPaymentGUID { get; set; }
		public string CreditAppRequestLine_NewReceiptRemark { get; set; }
		public string CreditAppRequestLine_NewReceiptAddressGUID { get; set; }
		public string CreditAppRequestLine_RefCreditAppLineGUID { get; set; }
		public string CreditAppRequestLine_NewBillingAddressGUID { get; set; }
		public string BuyerTable_Values { get; set; }
		public string CreditAppRequestLine_BuyerTableGUID { get; set; }
		public string UnboundNewBillingAddress { get; set; }
		public string unboundNewReceiptAddress { get; set; }
		public string CreditAppRequestLine_BlacklistStatusGUID { get; set; }
		public string CreditAppRequestLine_CreditScoringGUID { get; set; }
		public string CreditAppRequestLine_KYCSetupGUID { get; set; }
		public decimal CreditAppRequestLine_CustomerBuyerOutstanding { get; set; }
		public decimal CreditAppRequestLine_AllCustomerBuyerOutstanding { get; set; }
		#endregion CreditAppRequestLine
		public int ProcessInstanceId { get; set; }
	}
}
