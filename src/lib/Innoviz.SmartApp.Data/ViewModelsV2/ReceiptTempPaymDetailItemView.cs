using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ReceiptTempPaymDetailItemView : ViewCompanyBaseEntity
	{
		public string ReceiptTempPaymDetailGUID { get; set; }
		public string ChequeBankGroupGUID { get; set; }
		public string ChequeBranch { get; set; }
		public string ChequeTableGUID { get; set; }
		public string MethodOfPaymentGUID { get; set; }
		public decimal ReceiptAmount { get; set; }
		public string ReceiptTempTableGUID { get; set; }
		public string TransferReference { get; set; }
		public int MethodOfPayment_PaymentType { get; set; }
		public int ReceiptTempTable_ReceivedFrom { get; set; }
		public string ReceiptTempTable_CustomerTableGUID { get; set; }
		public string ReceiptTempTable_BuyerTableGUID { get; set; }
		public string CompanyBank_Values { get; set; }
		public int ReceiptTempTable_ProductType { get; set; }
		public string ReceiptTempTable_ReceiptTempId { get; set; }
	}
}