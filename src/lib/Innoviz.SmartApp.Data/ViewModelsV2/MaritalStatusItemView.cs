using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class MaritalStatusItemView : ViewCompanyBaseEntity
	{
		public string MaritalStatusGUID { get; set; }
		public string Description { get; set; }
		public string MaritalStatusId { get; set; }
	}
}
