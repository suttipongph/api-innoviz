using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class FinancialStatementTransItemView : ViewCompanyBaseEntity
	{
		public string FinancialStatementTransGUID { get; set; }
		public decimal Amount { get; set; }
		public string Description { get; set; }
		public int Ordering { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public int Year { get; set; }
		public string RefId { get; set; }
	}
}
