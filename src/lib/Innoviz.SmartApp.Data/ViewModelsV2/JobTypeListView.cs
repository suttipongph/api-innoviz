using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class JobTypeListView : ViewCompanyBaseEntity
	{
		public string JobTypeGUID { get; set; }
		public string JobTypeId { get; set; }
		public bool Assignment { get; set; }

		public string Description { get; set; }
		public int ShowDocConVerifyType { get; set; }
	}
}
