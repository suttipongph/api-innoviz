using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class AddressCountryItemView : ViewCompanyBaseEntity
	{
		public string AddressCountryGUID { get; set; }
		public string CountryId { get; set; }
		public string Name { get; set; }
	}
}
