using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CreditAppRequestLineItemView : ViewCompanyBaseEntity
	{
		public string CreditAppRequestLineGUID { get; set; }
		public bool AcceptanceDocument { get; set; }
		public string AcceptanceDocumentDescription { get; set; }
		public int ApprovalDecision { get; set; }
		public decimal ApprovedCreditLimitLineRequest { get; set; }
		public string ApproverComment { get; set; }
		public string AssignmentAgreementTableGUID { get; set; }
		public string AssignmentMethodGUID { get; set; }
		public string AssignmentMethodRemark { get; set; }
		public string BillingAddressGUID { get; set; }
		public string BillingContactPersonGUID { get; set; }
		public int BillingDay { get; set; }
		public string BillingDescription { get; set; }
		public string BillingRemark { get; set; }
		public string BillingResponsibleByGUID { get; set; }
		public string BlacklistStatusGUID { get; set; }
		public decimal BuyerCreditLimit { get; set; }
		public string BuyerCreditTermGUID { get; set; }
		public string BuyerProduct { get; set; }
		public string BuyerTableGUID { get; set; }
		public string CreditAppRequestTableGUID { get; set; }
		public string CreditComment { get; set; }
		public decimal CreditLimitLineRequest { get; set; }
		public string CreditScoringGUID { get; set; }
		public string CreditTermDescription { get; set; }
		public decimal CustomerContactBuyerPeriod { get; set; }
		public decimal CustomerRequest { get; set; }
		public decimal InsuranceCreditLimit { get; set; }
		public string InvoiceAddressGUID { get; set; }
		public string KYCSetupGUID { get; set; }
		public int LineNum { get; set; }
		public string MailingReceiptAddressGUID { get; set; }
		public string MarketingComment { get; set; }
		public decimal MaxPurchasePct { get; set; }
		public int MethodOfBilling { get; set; }
		public string MethodOfPaymentGUID { get; set; }
		public string PaymentCondition { get; set; }
		public int PurchaseFeeCalculateBase { get; set; }
		public decimal PurchaseFeePct { get; set; }
		public string ReceiptAddressGUID { get; set; }
		public string ReceiptContactPersonGUID { get; set; }
		public int ReceiptDay { get; set; }
		public string ReceiptDescription { get; set; }
		public string ReceiptRemark { get; set; }
		public string BlacklistStatus_Values { get; set; }
		public string BusinessSegment_Values { get; set; }
		public string BusinessType_Values { get; set; }
		public string LineOfBusiness_Values { get; set; }
		public string CreditAppRequestTable_Values { get; set; }
		public string UnboundBillingAddress { get; set; }
		public string UnboundReceiptAddress { get; set; }
		public string UnboundMailingReceiptAddress { get; set; }
		public string UnboundInvoiceAddress { get; set; }
		public string CreditAppRequestTable_StatusId { get; set; }
		public string DateOfEstablish { get; set; }
	    public int? CreditAppRequestTable_ProductType { get; set; }
		//R02
		public decimal RemainingCreditLoanRequest { get; set; }
		public decimal AllCustomerBuyerOutstanding { get; set; }
		public decimal CustomerBuyerOutstanding { get; set; }
		public string RefCreditAppLineGUID { get; set; }
		public string RefCreditAppLine_Values { get; set; }
		public string RefCreditAppTable_Values { get; set; }
		public string LineCondition { get; set; }

	}
}
