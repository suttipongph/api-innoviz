using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BookmarkDocumentTemplateLineItemView : ViewCompanyBaseEntity
	{
		public string BookmarkDocumentTemplateLineGUID { get; set; }
		public string BookmarkDocumentGUID { get; set; }
		public string BookmarkDocumentTemplateTableGUID { get; set; }
		public string DocumentTemplateTableGUID { get; set; }
		public int DocumentTemplateType { get; set; }
		public string DocumentTemplateTable_Values { get; set; }
		
	}
}
