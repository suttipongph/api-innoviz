﻿using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class OverCreditCALineView
	{
		public string CreditAppLineGUID { get; set; }
		public decimal OverCredit { get; set; }
		public decimal SumPurchaseLine { get; set; }
		public decimal CreditLimitBalance { get; set; }
	}
}
