using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class DocumentConditionTemplateTableItemView : ViewCompanyBaseEntity
	{
		public string DocumentConditionTemplateTableGUID { get; set; }
		public string Description { get; set; }
		public string DocumentConditionTemplateTableId { get; set; }
	}
}
