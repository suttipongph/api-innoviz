using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BusinessCollateralAgmTableItemView : ViewCompanyBaseEntity
	{
		public string BusinessCollateralAgmTableGUID { get; set; }
		public string AgreementDate { get; set; }
		public int AgreementDocType { get; set; }
		public int AgreementExtension { get; set; }
		public string BusinessCollateralAgmId { get; set; }
		public string CreditAppRequestTableGUID { get; set; }
		public string CreditAppTableGUID { get; set; }
		public string CreditLimitTypeGUID { get; set; }
		public string CustomerAltName { get; set; }
		public string CustomerName { get; set; }
		public string CustomerTableGUID { get; set; }
		public string Description { get; set; }
		public string DocumentReasonGUID { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string InternalBusinessCollateralAgmId { get; set; }
		public int ProductType { get; set; }
		public string RefBusinessCollateralAgmTableGUID { get; set; }
		public string SigningDate { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string CreditAppTable_CreditAppId { get; set; }
		public string CreditLimitType_CreditLimitTypeId { get; set; }
		public string ConsortiumTableGUID { get; set; }
		public string ConsortiumTable_ConsortiumId { get; set; }
		public string BusinessCollateralAgmTable_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public decimal DBDRegistrationAmount { get; set; }
		public string DBDRegistrationDate { get; set; }
		public string DBDRegistrationDescription { get; set; }
		public string DBDRegistrationId { get; set; }
		public string AttachmentRemark { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string CreditLimitType_Values { get; set; }
		public string ConsortiumTable_Values { get; set; }
		public string Remark { get; set; }
		public string DocumentReason_Values { get; set; }
		public decimal TotalBusinessCollateralValue { get; set; }
	}
	public class CopyBusinessCollateralAgmLine : ViewCompanyBaseEntity
	{
		public string CopyBusinessCollateralAgmLineGUID { get; set; }
		public string InternalBusineeCollateralId { get; set; }
		public NotificationResponse Notification { get; set; }

	}
}
