using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class LedgerDimensionListView : ViewCompanyBaseEntity
	{
		public string LedgerDimensionGUID { get; set; }
		public string Description { get; set; }
		public int Dimension { get; set; }
		public string DimensionCode { get; set; }
	}
}
