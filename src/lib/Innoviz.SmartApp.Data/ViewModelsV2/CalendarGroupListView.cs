using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CalendarGroupListView : ViewCompanyBaseEntity
	{
		public string CalendarGroupGUID { get; set; }
		public string CalendarGroupId { get; set; }
		public string Description { get; set; }
		public int WorkingDay { get; set; }
	}
}
