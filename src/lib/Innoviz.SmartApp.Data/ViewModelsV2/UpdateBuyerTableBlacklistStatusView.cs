﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class UpdateBuyerTableBlacklistStatusParamView
    {
        public string BuyerTableGUID { get; set; }
        public string BlacklistStatusGUID { get; set; }
        public string DocumentReasonGUID { get; set; }
        public string ReasonRemark { get; set; }
    }
    public class UpdateBuyerTableBlacklistStatusResultView : ResultBaseEntity
    {
        public string BuyerTableGUID { get; set; }
        public string OriginalBlacklistStatus { get; set; }
        public string OriginalBlacklistStatusGUID { get; set; }
        public string BlacklistStatusGUID { get; set; }
        public string DocumentReasonGUID { get; set; }
        public string BuyerId { get; set; }
        public string ReasonRemark { get; set; }
        public string ResultLabel { get; set; }
    }
}
