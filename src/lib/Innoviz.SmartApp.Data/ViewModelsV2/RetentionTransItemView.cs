using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class RetentionTransItemView : ViewCompanyBaseEntity
	{
		public string RetentionTransGUID { get; set; }
		public decimal Amount { get; set; }
		public string BuyerAgreementTableGUID { get; set; }
		public string BuyerTableGUID { get; set; }
		public string CreditAppTableGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public string DocumentId { get; set; }
		public int ProductType { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public string TransDate { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string BuyerAgreementTable_Values { get; set; }
		public string CustomerTable_Value { get; set; }
		public string RefID { get; set; }
	}
}
