﻿using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class GetTaxValueAndWHTValueParm
    {
        public string TaxTableGUID { get; set; }
        public string WithholdingTaxTableGUID { get; set; }
        public string TaxDate { get; set; }
    }
}
