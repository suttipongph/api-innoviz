using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CreditAppRequestLineListView : ViewCompanyBaseEntity
	{
		public string CreditAppRequestLineGUID { get; set; }
		public int LineNum { get; set; }
		public string BuyerTableGUID { get; set; }
		public decimal CreditLimitLineRequest { get; set; }
		public decimal ApprovedCreditLimitLineRequest { get; set; }
		public int ApprovalDecision { get; set; }
		public string CreditapprequesttableGUID { get; set; }
		public string BuyerTable_Values { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string BusinessSegmentGUID { get; set; }
		public string BusinessSegment_Values { get; set; }
		public string BusinessSegment_BusinessSegmentId { get; set; }
	}
}
