﻿using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class GenBusCollateralAgmAddendumView : ViewCompanyBaseEntity
    {
        public string GenBusCollateralAgmAddendumGUID { get; set; }
        public string AddendumDate { get; set; }
        public string AddendumInternalBusineeCollateralId { get; set; }
        public string AgreementDate { get; set; }
        public int AgreementDocType { get; set; }
        public string BusinessCollateralAgmDescription { get; set; }
        public string BusinessCollateralAgmId { get; set; }
        public string CreditAppRequestDescription { get; set; }
        public string CreditAppRequestTableGUID { get; set; }
        public int CreditAppRequestType { get; set; }
        public string CreditLimitTypeGUID { get; set; }
        public string CustomerId { get; set; }
        public string CustomerGUID { get; set; }
        public string CustomerTable_Values { get; set; }
        public string CustomerName { get; set; }
        public string Description { get; set; }
        public string InternalBusinessCollateralAgmId { get; set; }
        public string RequestDate { get; set; }
        public string CreditLimitType_CreditLimitTypeId { get; set; }
        public string CreditAppRequestTable_Values { get; set; }
        public string CreditLimitType_Values { get; set; }
        public string DocumentReasonGUID { get; set; }
    }
    public class GenBusCollateralAgmAddendumResultView : ResultBaseEntity
    {
        public string GenBusCollateralAgmAddendumGUID { get; set; }
        public string AddendumDate { get; set; }
        public string AddendumInternalBusineeCollateralId { get; set; }
        public string AgreementDate { get; set; }
        public int AgreementDocType { get; set; }
        public string BusinessCollateralAgmDescription { get; set; }
        public string BusinessCollateralAgmId { get; set; }
        public string CreditAppRequestDescription { get; set; }
        public string CreditAppRequestTableGUID { get; set; }
        public int CreditAppRequestType { get; set; }
        public string CreditLimitTypeGUID { get; set; }
        public string CustomerId { get; set; }
        public string CustomerGUID { get; set; }
        public string CustomerTable_Values { get; set; }
        public string CustomerName { get; set; }
        public string Description { get; set; }
        public string InternalBusinessCollateralAgmId { get; set; }
        public string RequestDate { get; set; }
        public string CreditLimitType_CreditLimitTypeId { get; set; }
        public string CreditAppRequestTable_Values { get; set; }
        public string CreditLimitType_Values { get; set; }
        public string DocumentReasonGUID { get; set; }
    }
}
