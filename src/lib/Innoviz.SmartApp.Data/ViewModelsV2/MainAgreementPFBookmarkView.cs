﻿using Innoviz.SmartApp.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class MainAgreementPFBookmarkView: GenMainAgreementBookmarkSharedView
    {
        [BookmarkMapping("TypeLoanFirst1")]
        public string TypeLoanFirst1 { get; set; }
        [BookmarkMapping("TypeLoanSecond1")]
        public string TypeLoanSecond1 { get; set; }
        [BookmarkMapping("TermLoan1")]
        public int TermLoan1 { get; set; }
        [BookmarkMapping("CreditStartDate1")]
        public string CreditStartDate1 { get; set; }
        [BookmarkMapping("CreditEndDate1")]
        public string CreditEndDate1 { get; set; }
        [BookmarkMapping("WithdrawAmountFirst1")]
        public decimal WithdrawAmountFirst1 { get; set; }
        [BookmarkMapping("WithdrawAmountSecond1")]
        public string WithdrawAmountSecond1 { get; set; }
        [BookmarkMapping("WithdrawOutstandingFirst1")]
        public decimal WithdrawOutstandingFirst1 { get; set; }
        [BookmarkMapping("WithdrawOutstandingSecond1")]
        public string WithdrawOutstandingSecond1 { get; set; }
        [BookmarkMapping("EndDateContract1")]
        public string EndDateContract1 { get; set; }
        [BookmarkMapping("EndDateContract2")]
        public string EndDateContract2 { get; set; }
        [BookmarkMapping("ContAmountFirst2")]
        public decimal ContAmountFirst2 { get; set; }
        [BookmarkMapping("ContAmountSecond2")]
        public string ContAmountSecond2 { get; set; }
        [BookmarkMapping("TextContractObjective1")]
        public string TextContractObjective1 { get; set; }
        [BookmarkMapping("GuarantorNameFirst1")]
        public string GuarantorNameFirst1 { get; set; }
        [BookmarkMapping("TextContractGuarantee1")]
        public string TextContractGuarantee1 { get; set; }
        [BookmarkMapping("InterestNoCheckFirst1")]
        public string InterestNoCheckFirst1 { get; set; }
        [BookmarkMapping("InterestNoCheckSecond1")]
        public string InterestNoCheckSecond1 { get; set; }
        [BookmarkMapping("InterestNoCheckThird1")]
        public string InterestNoCheckThird1 { get; set; }
        [BookmarkMapping("InterestNoCheckFourth1")]
        public string InterestNoCheckFourth1 { get; set; }
        [BookmarkMapping("InterestNoCheckFifth1")]
        public string InterestNoCheckFifth1 { get; set; }
        [BookmarkMapping("InterestNoCheckSixth1")]
        public string InterestNoCheckSixth1 { get; set; }
        [BookmarkMapping("InterestNoCheckSeventh1")]
        public string InterestNoCheckSeventh1 { get; set; }
        [BookmarkMapping("InterestNoCheckEighth1")]
        public string InterestNoCheckEighth1 { get; set; }
        [BookmarkMapping("InterestNoCheckNineth1")]
        public string InterestNoCheckNineth1 { get; set; }
        [BookmarkMapping("InterestNoCheckTenth1")]
        public string InterestNoCheckTenth1 { get; set; }
        [BookmarkMapping("InterestNoCheckEleventh1")]
        public string InterestNoCheckEleventh1 { get; set; }
        [BookmarkMapping("InterestNoCheckTwelfth1")]
        public string InterestNoCheckTwelfth1 { get; set; }
        [BookmarkMapping("InterestNoCheckThirteenth1")]
        public string InterestNoCheckThirteenth1 { get; set; }
        [BookmarkMapping("InterestNoCheckFourteenth1")]
        public string InterestNoCheckFourteenth1 { get; set; }
        [BookmarkMapping("InterestNoCheckFifteenth1")]
        public string InterestNoCheckFifteenth1 { get; set; }
        [BookmarkMapping("InterestNoCheckSixteenth1")]
        public string InterestNoCheckSixteenth1 { get; set; }
        [BookmarkMapping("InterestNoCheckSeventeenth1")]
        public string InterestNoCheckSeventeenth1 { get; set; }
        [BookmarkMapping("InterestNoCheckEighteenth1")]
        public string InterestNoCheckEighteenth1 { get; set; }
        [BookmarkMapping("InterestNoCheckNineteenth1")]
        public string InterestNoCheckNineteenth1 { get; set; }
        [BookmarkMapping("InterestNoCheckTwentieth1")]
        public string InterestNoCheckTwentieth1 { get; set; }
        [BookmarkMapping("InterestNoCheckTwentyfirst1")]
        public string InterestNoCheckTwentyfirst1 { get; set; }
        [BookmarkMapping("InterestNoCheckTwentysecond1")]
        public string InterestNoCheckTwentysecond1 { get; set; }
        [BookmarkMapping("InterestNoCheckTwentythird1")]
        public string InterestNoCheckTwentythird1 { get; set; }
        [BookmarkMapping("InterestNoCheckTwentyfourth1")]
        public string InterestNoCheckTwentyfourth1 { get; set; }
        [BookmarkMapping("InterestDateFirst1")]
        public string InterestDateFirst1 { get; set; }
        [BookmarkMapping("InterestDateSecond1")]
        public string InterestDateSecond1 { get; set; }
        [BookmarkMapping("InterestDateThird1")]
        public string InterestDateThird1 { get; set; }
        [BookmarkMapping("InterestDateFourth1")]
        public string InterestDateFourth1 { get; set; }
        [BookmarkMapping("InterestDateFifth1")]
        public string InterestDateFifth1 { get; set; }
        [BookmarkMapping("InterestDateSixth1")]
        public string InterestDateSixth1 { get; set; }
        [BookmarkMapping("InterestDateSeventh1")]
        public string InterestDateSeventh1 { get; set; }
        [BookmarkMapping("InterestDateEighth1")]
        public string InterestDateEighth1 { get; set; }
        [BookmarkMapping("InterestDateNineth1")]
        public string InterestDateNineth1 { get; set; }
        [BookmarkMapping("InterestDateTenth1")]
        public string InterestDateTenth1 { get; set; }
        [BookmarkMapping("InterestDateEleventh1")]
        public string InterestDateEleventh1 { get; set; }
        [BookmarkMapping("InterestDateTwelfth1")]
        public string InterestDateTwelfth1 { get; set; }
        [BookmarkMapping("InterestDateThirteenth1")]
        public string InterestDateThirteenth1 { get; set; }
        [BookmarkMapping("InterestDateFourteenth1")]
        public string InterestDateFourteenth1 { get; set; }
        [BookmarkMapping("InterestDateFifteenth1")]
        public string InterestDateFifteenth1 { get; set; }
        [BookmarkMapping("InterestDateSixteenth1")]
        public string InterestDateSixteenth1 { get; set; }
        [BookmarkMapping("InterestDateSeventeenth1")]
        public string InterestDateSeventeenth1 { get; set; }
        [BookmarkMapping("InterestDateEighteenth1")]
        public string InterestDateEighteenth1 { get; set; }
        [BookmarkMapping("InterestDateNineteenth1")]
        public string InterestDateNineteenth1 { get; set; }
        [BookmarkMapping("InterestDateTwentieth1")]
        public string InterestDateTwentieth1 { get; set; }
        [BookmarkMapping("InterestDateTwentyfirst1")]
        public string InterestDateTwentyfirst1 { get; set; }
        [BookmarkMapping("InterestDateTwentysecond1")]
        public string InterestDateTwentysecond1 { get; set; }
        [BookmarkMapping("InterestDateTwentythird1")]
        public string InterestDateTwentythird1 { get; set; }
        [BookmarkMapping("InterestDateTwentyfourth1")]
        public string InterestDateTwentyfourth1 { get; set; }
        [BookmarkMapping("InterestAmountFirst1")]
        public decimal InterestAmountFirst1 { get; set; }
        [BookmarkMapping("InterestAmountSecond1")]
        public decimal InterestAmountSecond1 { get; set; }
        [BookmarkMapping("InterestAmountThird1")]
        public decimal InterestAmountThird1 { get; set; }
        [BookmarkMapping("InterestAmountFourth1")]
        public decimal InterestAmountFourth1 { get; set; }
        [BookmarkMapping("InterestAmountFifth1")]
        public decimal InterestAmountFifth1 { get; set; }
        [BookmarkMapping("InterestAmountSixth1")]
        public decimal InterestAmountSixth1 { get; set; }
        [BookmarkMapping("InterestAmountSeventh1")]
        public decimal InterestAmountSeventh1 { get; set; }
        [BookmarkMapping("InterestAmountEighth1")]
        public decimal InterestAmountEighth1 { get; set; }
        [BookmarkMapping("InterestAmountNineth1")]
        public decimal InterestAmountNineth1 { get; set; }
        [BookmarkMapping("InterestAmountTenth1")]
        public decimal InterestAmountTenth1 { get; set; }
        [BookmarkMapping("InterestAmountEleventh1")]
        public decimal InterestAmountEleventh1 { get; set; }
        [BookmarkMapping("InterestAmountTwelfth1")]
        public decimal InterestAmountTwelfth1 { get; set; }
        [BookmarkMapping("InterestAmountThirteenth1")]
        public decimal InterestAmountThirteenth1 { get; set; }
        [BookmarkMapping("InterestAmountFourteenth1")]
        public decimal InterestAmountFourteenth1 { get; set; }
        [BookmarkMapping("InterestAmountFifteenth1")]
        public decimal InterestAmountFifteenth1 { get; set; }
        [BookmarkMapping("InterestAmountSixteenth1")]
        public decimal InterestAmountSixteenth1 { get; set; }
        [BookmarkMapping("InterestAmountSeventeenth1")]
        public decimal InterestAmountSeventeenth1 { get; set; }
        [BookmarkMapping("InterestAmountEighteenth1")]
        public decimal InterestAmountEighteenth1 { get; set; }
        [BookmarkMapping("InterestAmountNineteenth1")]
        public decimal InterestAmountNineteenth1 { get; set; }
        [BookmarkMapping("InterestAmountTwentieth1")]
        public decimal InterestAmountTwentieth1 { get; set; }
        [BookmarkMapping("InterestAmountTwentyfirst1")]
        public decimal InterestAmountTwentyfirst1 { get; set; }
        [BookmarkMapping("InterestAmountTwentysecond1")]
        public decimal InterestAmountTwentysecond1 { get; set; }
        [BookmarkMapping("InterestAmountTwentythird1")]
        public decimal InterestAmountTwentythird1 { get; set; }
        [BookmarkMapping("InterestAmountTwentyfourth1")]
        public decimal InterestAmountTwentyfourth1 { get; set; }
        [BookmarkMapping("InterestDateFirst2")]
        public string InterestDateFirst2 { get; set; }
        [BookmarkMapping("InterestDateSecond2")]
        public string InterestDateSecond2 { get; set; }
        [BookmarkMapping("InterestDateThird2")]
        public string InterestDateThird2 { get; set; }
        [BookmarkMapping("InterestDateFourth2")]
        public string InterestDateFourth2 { get; set; }
        [BookmarkMapping("InterestDateFifth2")]
        public string InterestDateFifth2 { get; set; }
        [BookmarkMapping("InterestDateSixth2")]
        public string InterestDateSixth2 { get; set; }
        [BookmarkMapping("InterestDateSeventh2")]
        public string InterestDateSeventh2 { get; set; }
        [BookmarkMapping("InterestDateEighth2")]
        public string InterestDateEighth2 { get; set; }
        [BookmarkMapping("InterestDateNineth2")]
        public string InterestDateNineth2 { get; set; }
        [BookmarkMapping("InterestDateTenth2")]
        public string InterestDateTenth2 { get; set; }
        [BookmarkMapping("InterestDateEleventh2")]
        public string InterestDateEleventh2 { get; set; }
        [BookmarkMapping("InterestDateTwelfth2")]
        public string InterestDateTwelfth2 { get; set; }
        [BookmarkMapping("InterestDateThirteenth2")]
        public string InterestDateThirteenth2 { get; set; }
        [BookmarkMapping("InterestDateFourteenth2")]
        public string InterestDateFourteenth2 { get; set; }
        [BookmarkMapping("InterestDateFifteenth2")]
        public string InterestDateFifteenth2 { get; set; }
        [BookmarkMapping("InterestDateSixteenth2")]
        public string InterestDateSixteenth2 { get; set; }
        [BookmarkMapping("InterestDateSeventeenth2")]
        public string InterestDateSeventeenth2 { get; set; }
        [BookmarkMapping("InterestDateEighteenth2")]
        public string InterestDateEighteenth2 { get; set; }
        [BookmarkMapping("InterestDateNineteenth2")]
        public string InterestDateNineteenth2 { get; set; }
        [BookmarkMapping("InterestDateTwentieth2")]
        public string InterestDateTwentieth2 { get; set; }
        [BookmarkMapping("InterestDateTwentyfirst2")]
        public string InterestDateTwentyfirst2 { get; set; }
        [BookmarkMapping("InterestDateTwentysecond2")]
        public string InterestDateTwentysecond2 { get; set; }
        [BookmarkMapping("InterestDateTwentythird2")]
        public string InterestDateTwentythird2 { get; set; }
        [BookmarkMapping("InterestDateTwentyfourth2")]
        public string InterestDateTwentyfourth2 { get; set; }
        [BookmarkMapping("InterestAmountFirst2")]
        public decimal InterestAmountFirst2 { get; set; }
        [BookmarkMapping("InterestAmountSecond2")]
        public decimal InterestAmountSecond2 { get; set; }
        [BookmarkMapping("InterestAmountThird2")]
        public decimal InterestAmountThird2 { get; set; }
        [BookmarkMapping("InterestAmountFourth2")]
        public decimal InterestAmountFourth2 { get; set; }
        [BookmarkMapping("InterestAmountFifth2")]
        public decimal InterestAmountFifth2 { get; set; }
        [BookmarkMapping("InterestAmountSixth2")]
        public decimal InterestAmountSixth2 { get; set; }
        [BookmarkMapping("InterestAmountSeventh2")]
        public decimal InterestAmountSeventh2 { get; set; }
        [BookmarkMapping("InterestAmountEighth2")]
        public decimal InterestAmountEighth2 { get; set; }
        [BookmarkMapping("InterestAmountNineth2")]
        public decimal InterestAmountNineth2 { get; set; }
        [BookmarkMapping("InterestAmountTenth2")]
        public decimal InterestAmountTenth2 { get; set; }
        [BookmarkMapping("InterestAmountEleventh2")]
        public decimal InterestAmountEleventh2 { get; set; }
        [BookmarkMapping("InterestAmountTwelfth2")]
        public decimal InterestAmountTwelfth2 { get; set; }
        [BookmarkMapping("InterestAmountThirteenth2")]
        public decimal InterestAmountThirteenth2 { get; set; }
        [BookmarkMapping("InterestAmountFourteenth2")]
        public decimal InterestAmountFourteenth2 { get; set; }
        [BookmarkMapping("InterestAmountFifteenth2")]
        public decimal InterestAmountFifteenth2 { get; set; }
        [BookmarkMapping("InterestAmountSixteenth2")]
        public decimal InterestAmountSixteenth2 { get; set; }
        [BookmarkMapping("InterestAmountSeventeenth2")]
        public decimal InterestAmountSeventeenth2 { get; set; }
        [BookmarkMapping("InterestAmountEighteenth2")]
        public decimal InterestAmountEighteenth2 { get; set; }
        [BookmarkMapping("InterestAmountNineteenth2")]
        public decimal InterestAmountNineteenth2 { get; set; }
        [BookmarkMapping("InterestAmountTwentieth2")]
        public decimal InterestAmountTwentieth2 { get; set; }
        [BookmarkMapping("InterestAmountTwentyfirst2")]
        public decimal InterestAmountTwentyfirst2 { get; set; }
        [BookmarkMapping("InterestAmountTwentysecond2")]
        public decimal InterestAmountTwentysecond2 { get; set; }
        [BookmarkMapping("InterestAmountTwentythird2")]
        public decimal InterestAmountTwentythird2 { get; set; }
        [BookmarkMapping("InterestAmountTwentyfourth2")]
        public decimal InterestAmountTwentyfourth2 { get; set; }
        [BookmarkMapping("InterestTotalAmountFirst1")]
        public decimal InterestTotalAmountFirst1 { get; set; }
        [BookmarkMapping("InterestTotalAmountFirst2")]
        public decimal InterestTotalAmountFirst2 { get; set; }
        [BookmarkMapping("InterestTotalAmountSecond1")]
        public string InterestTotalAmountSecond1 { get; set; }
        [BookmarkMapping("InterestWordingFirst1")]
        public string InterestWordingFirst1 { get; set; }
        [BookmarkMapping("InterestWordingSecond1")]
        public string InterestWordingSecond1 { get; set; }
        [BookmarkMapping("InterestWordingThird1")]
        public string InterestWordingThird1 { get; set; }
        [BookmarkMapping("InterestWordingFourth1")]
        public string InterestWordingFourth1 { get; set; }
        [BookmarkMapping("InterestWordingFifth1")]
        public string InterestWordingFifth1 { get; set; }
        [BookmarkMapping("InterestWordingSixth1")]
        public string InterestWordingSixth1 { get; set; }
        [BookmarkMapping("InterestWordingSeventh1")]
        public string InterestWordingSeventh1 { get; set; }
        [BookmarkMapping("InterestWordingEighth1")]
        public string InterestWordingEighth1 { get; set; }
        [BookmarkMapping("InterestWordingNineth1")]
        public string InterestWordingNineth1 { get; set; }
        [BookmarkMapping("InterestWordingTenth1")]
        public string InterestWordingTenth1 { get; set; }
        [BookmarkMapping("InterestWordingEleventh1")]
        public string InterestWordingEleventh1 { get; set; }
        [BookmarkMapping("InterestWordingTwelfth1")]
        public string InterestWordingTwelfth1 { get; set; }
        [BookmarkMapping("InterestWordingThirteenth1")]
        public string InterestWordingThirteenth1 { get; set; }
        [BookmarkMapping("InterestWordingFourteenth1")]
        public string InterestWordingFourteenth1 { get; set; }
        [BookmarkMapping("InterestWordingFifteenth1")]
        public string InterestWordingFifteenth1 { get; set; }
        [BookmarkMapping("InterestWordingSixteenth1")]
        public string InterestWordingSixteenth1 { get; set; }
        [BookmarkMapping("InterestWordingSeventeenth1")]
        public string InterestWordingSeventeenth1 { get; set; }
        [BookmarkMapping("InterestWordingEighteenth1")]
        public string InterestWordingEighteenth1 { get; set; }
        [BookmarkMapping("InterestWordingNineteenth1")]
        public string InterestWordingNineteenth1 { get; set; }
        [BookmarkMapping("InterestWordingTwentieth1")]
        public string InterestWordingTwentieth1 { get; set; }
        [BookmarkMapping("InterestWordingTwentyfirst1")]
        public string InterestWordingTwentyfirst1 { get; set; }
        [BookmarkMapping("InterestWordingTwentysecond1")]
        public string InterestWordingTwentysecond1 { get; set; }
        [BookmarkMapping("InterestWordingTwentythird1")]
        public string InterestWordingTwentythird1 { get; set; }
        [BookmarkMapping("InterestWordingTwentyfourth1")]
        public string InterestWordingTwentyfourth1 { get; set; }

    }
}
