using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class IntercompanyInvoiceTableItemView : ViewCompanyBaseEntity
	{
		public string IntercompanyInvoiceTableGUID { get; set; }
		public string CNReasonGUID { get; set; }
		public string CreditAppTableGUID { get; set; }
		public string CurrencyGUID { get; set; }
		public string CustomerName { get; set; }
		public string CustomerTableGUID { get; set; }
		public string Dimension1GUID { get; set; }
		public string Dimension2GUID { get; set; }
		public string Dimension3GUID { get; set; }
		public string Dimension4GUID { get; set; }
		public string Dimension5GUID { get; set; }
		public string DocumentId { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string DueDate { get; set; }
		public decimal ExchangeRate { get; set; }
		public string IntercompanyGUID { get; set; }
		public string IntercompanyInvoiceId { get; set; }
		public string InvoiceAddressTransGUID { get; set; }
		public decimal InvoiceAmount { get; set; }
		public string InvoiceRevenueTypeGUID { get; set; }
		public string InvoiceText { get; set; }
		public string InvoiceTypeGUID { get; set; }
		public string IssuedDate { get; set; }
		public decimal OrigInvoiceAmount { get; set; }
		public string OrigInvoiceId { get; set; }
		public decimal OrigTaxInvoiceAmount { get; set; }
		public string OrigTaxInvoiceId { get; set; }
		public int ProductType { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public string TaxBranchId { get; set; }
		public string TaxBranchName { get; set; }
		public string TaxTableGUID { get; set; }
		public string WithholdingTaxTableGUID { get; set; }
		public string Customer_Values { get; set; }
		public string InvoiceType_Values { get; set; }
		public string InterCompany_Values { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string Currency_Values { get; set; }
		public string InvoiceRevenueType_Values { get; set; }
		public string TaxTable_Values { get; set; }
		public string WithholdingTaxTable_Values { get; set; }
		public string CNReason_Values { get; set; }
		public string Dimension1_Values { get; set; }
		public string Dimension2_Values { get; set; }
		public string Dimension3_Values { get; set; }
		public string Dimension4_Values { get; set; }
		public string Dimension5_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string RefId { get; set; }
		public decimal Balance { get; set; }
		public decimal SettleAmount { get; set; }
		public string FeeLedgerAccount { get; set; }
		public string UnboundInvoiceAddress { get; set; }
		public string InvoiceAddressTrans_Values { get; set; }
	}
}
