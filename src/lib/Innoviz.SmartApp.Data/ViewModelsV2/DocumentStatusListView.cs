using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class DocumentStatusListView : ViewCompanyBaseEntity
	{
		public string DocumentStatusGUID { get; set; }
		public string StatusId { get; set; }
		public string Description { get; set; }
		public string DocumentProcessGUID { get; set; }


	}
}
