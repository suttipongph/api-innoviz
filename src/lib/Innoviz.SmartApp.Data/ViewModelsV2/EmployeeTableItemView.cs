using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class EmployeeTableItemView : ViewCompanyBaseEntity
	{
		public string EmployeeTableGUID { get; set; }
		public string CollectionGroupGUID { get; set; }
		public string Dimension1GUID { get; set; }
		public string Dimension2GUID { get; set; }
		public string Dimension3GUID { get; set; }
		public string Dimension4GUID { get; set; }
		public string Dimension5GUID { get; set; }
		public string EmployeeId { get; set; }
		public string EmplTeamGUID { get; set; }
		public bool AssistMD { get; set; }
		public bool InActive { get; set; }
		public string Name { get; set; }
		public string Signature { get; set; }
		public string UserId { get; set; }
		public string SysuserTable_UserName { get; set; }
		public string DepartmentGUID { get; set; }
		public string BusinessUnitGUID { get; set; }
		public string ReportToEmployeeTableGUID { get; set; }
		public string Position { get; set; }
	}
}
