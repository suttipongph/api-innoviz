using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CalendarNonWorkingDateListView : ViewCompanyBaseEntity
	{
		public string CalendarNonWorkingDateGUID { get; set; }
		public string CalendarGroupGUID { get; set; }
		public string CalendarDate { get; set; }
		public string Description { get; set; }
		public int HolidayType { get; set; }
		public string CalendarGroup_Values { get; set; }
		public string CalendarGroup_calendarGroupId { get; set; }
	}
}
