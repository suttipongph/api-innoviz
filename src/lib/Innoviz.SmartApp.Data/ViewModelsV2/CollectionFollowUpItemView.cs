using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CollectionFollowUpItemView : ViewCompanyBaseEntity
	{
		public string CollectionFollowUpGUID { get; set; }
		public string BuyerTableGUID { get; set; }
		public decimal ChequeAmount { get; set; }
		public string ChequeBankAccNo { get; set; }
		public string ChequeBankGroupGUID { get; set; }
		public string ChequeBranch { get; set; }
		public int ChequeCollectionResult { get; set; }
		public string ChequeDate { get; set; }
		public string ChequeNo { get; set; }
		public decimal CollectionAmount { get; set; }
		public string CollectionDate { get; set; }
		public int CollectionFollowUpResult { get; set; }
		public string CreditAppLineGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public string Description { get; set; }
		public string DocumentId { get; set; }
		public string MethodOfPaymentGUID { get; set; }
		public string NewCollectionDate { get; set; }
		public int ReceivedFrom { get; set; }
		public string RecipientName { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public string Remark { get; set; }
		public string RefId { get; set; }
		public string CustomerTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string CreditAppLine_Values { get; set; }
		public string MethodOfPayment_PaymentType { get; set; }
	}
}
