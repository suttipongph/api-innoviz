using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BranchItemView : ViewCompanyBaseEntity
	{
		public string BranchGUID { get; set; }
		public string ApplicationNumberSeqGUID { get; set; }
		public string BranchId { get; set; }
		public string Name { get; set; }
		public string TaxBranchGUID { get; set; }
		public string TaxBranch_Values { get; set; }
	}
}
