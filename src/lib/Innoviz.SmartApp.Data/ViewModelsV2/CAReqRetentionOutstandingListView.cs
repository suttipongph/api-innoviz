using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CAReqRetentionOutstandingListView : ViewCompanyBaseEntity
	{
		public string CAReqRetentionOutstandingGUID { get; set; }
		public int ProductType { get; set; }
		public string CreditAppTableGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public decimal MaximumRetention { get; set; }
		public decimal AccumRetentionAmount { get; set; }
		public decimal RemainingAmount { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string CreditAppTable_CreditAppId { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string CreditAppRequestTableGUID { get; set; }
	}
}
