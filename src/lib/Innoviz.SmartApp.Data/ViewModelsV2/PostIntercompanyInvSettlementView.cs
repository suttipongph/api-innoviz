﻿using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class PostIntercompanyInvSettlementView
    {
        public string IntercompanyInvoiceSettlementGUID { get; set; }
        public string DocumentStatus_Values { get; set; }
        public string IntercompanyInvoice_Values { get; set; }
        public string MethodOfPayment_Values { get; set; }
        public decimal SettleInvoiceAmount { get; set; }
        public string TransDate { get; set; }
    }
    public class PostIntercompanyInvSettlementResultView : ResultBaseEntity
    {
    }
}

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class PostIntercompanyInvSettlementViewMap
    {
        public StagingTableIntercoInvSettle StagingTableIntercoInvSettle { get; set; }
        public IntercompanyInvoiceSettlement IntercompanyInvoiceSettlement { get; set; }
    }
}
