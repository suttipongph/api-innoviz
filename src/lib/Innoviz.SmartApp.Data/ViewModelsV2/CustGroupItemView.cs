using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CustGroupItemView : ViewCompanyBaseEntity
	{
		public string CustGroupGUID { get; set; }
		public string CustGroupId { get; set; }
		public string Description { get; set; }
		public string NumberSeqTableGUID { get; set; }
	}
}
