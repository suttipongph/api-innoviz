using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ConsortiumTransListView : ViewCompanyBaseEntity
	{
		public string ConsortiumTransGUID { get; set; }
		public int Ordering { get; set; }
		public string AuthorizedPersonTypeGUID { get; set; }
		public string CustomerName { get; set; }
		public string OperatedBy { get; set; }
		public string AuthorizedPersonType_Values { get; set; }
		public string AuthorizedPersonType_AuthorizedPersonTypeId { get; set; }
		public string RefGUID { get; set; }
	}
}
