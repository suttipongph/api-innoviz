using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BusinessUnitListView : ViewCompanyBaseEntity
	{
		public string BusinessUnitGUID { get; set; }
		public string BusinessUnitId { get; set; }
		public string Description { get; set; }
		public string ParentBusinessUnitGUID { get; set; }
		public string ParentBusinessUnit_Values { get; set; }
		public string ParentBusinessUnit_BusinessUnitId { get; set; }
	}
}
