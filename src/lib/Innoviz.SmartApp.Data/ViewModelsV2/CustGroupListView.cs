using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CustGroupListView : ViewCompanyBaseEntity
	{
		public string CustGroupGUID { get; set; }
		public string CustGroupId { get; set; }
		public string Description { get; set; }
		public string NumberSeqTableGUID { get; set; }
		public string NumberSeqTable_Value { get; set; }
		public string NumberSeqTable_Description { get; set; }
		public string NumberSeqCode { get; set; }
	}
}
