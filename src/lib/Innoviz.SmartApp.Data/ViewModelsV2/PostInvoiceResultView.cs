﻿using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class PostInvoiceResultView
    {
        public List<InvoiceTable> InvoiceTables { get; set; } = new List<InvoiceTable>();
        public List<InvoiceLine> InvoiceLines { get; set; } = new List<InvoiceLine>();
        public List<CustTrans> CustTranses { get; set; } = new List<CustTrans>();
        public List<CreditAppTrans> CreditAppTranses { get; set; } = new List<CreditAppTrans>();
        public List<TaxInvoiceTable> TaxInvoiceTables { get; set; } = new List<TaxInvoiceTable>();
        public List<TaxInvoiceLine> TaxInvoiceLines { get; set; } = new List<TaxInvoiceLine>();
        public List<ProcessTrans> ProcessTranses { get; set; } = new List<ProcessTrans>();
        public List<RetentionTrans> RetentionTranses { get; set; } = new List<RetentionTrans>();

    }
}
