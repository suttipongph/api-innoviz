using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CAReqCreditOutStandingListView : ViewCompanyBaseEntity
	{
		public string CAReqCreditOutStandingGUID { get; set; }
		public int ProductType { get; set; }
		public decimal ReserveToBeRefund { get; set; }
		public string CreditLimitTypeGUID { get; set; }
		public string CreditAppTableGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public string AsOfDate { get; set; }
		public decimal ApprovedCreditLimit { get; set; }
		public decimal CreditLimitBalance { get; set; }
		public decimal ARBalance { get; set; }
		public decimal AccumRetentionAmount { get; set; }
		public string CreditAppRequestTableGUID { get; set; }
		public string CreditLimitType_Values { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CreditLimitType_CreditLimitTypeId { get; set; }
		public string CreditAppTable_CreditAppId { get; set; }
		public string CustomerTable_CustomerId { get; set; }
	}
}
