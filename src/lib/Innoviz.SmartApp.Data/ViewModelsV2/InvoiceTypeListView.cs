using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class InvoiceTypeListView : ViewCompanyBaseEntity
	{
		public string InvoiceTypeGUID { get; set; }
		public bool DirectReceipt { get; set; }
		public bool ValidateDirectReceiveByCA { get; set; }
		public bool ProductInvoice { get; set; }
		public string Description { get; set; }
		public int ProductType { get; set; }
		public string InvoiceTypeId { get; set; }
		public int SuspenseInvoiceType { get; set; }
		public string AutoGenInvoiceRevenueTypeGUID { get; set; }
		public string AutoGenInvoiceRevenueType_values { get; set; }
	}
}
