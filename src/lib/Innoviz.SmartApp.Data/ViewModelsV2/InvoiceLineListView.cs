using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class InvoiceLineListView : ViewBranchCompanyBaseEntity
	{
		public string InvoiceTableGUID { get; set; }
		public string InvoiceLineGUID { get; set; }
		public int LineNum { get; set; }
		public string InvoiceRevenueTypeGUID { get; set; }
		public string InvoiceText { get; set; }
		public decimal TotalAmountBeforeTax { get; set; }
		public decimal TaxAmount { get; set; }
		public decimal TotalAmount { get; set; }
		public string TaxTableGUID { get; set; }
		public string InvoiceRevenueType_RevenueTypeId { get; set; }
		public string InvoiceRevenueType_Values { get; set; }
		public string TaxTable_taxTabled { get; set; }
		public string TaxTable_Values { get; set; }
	}
}
