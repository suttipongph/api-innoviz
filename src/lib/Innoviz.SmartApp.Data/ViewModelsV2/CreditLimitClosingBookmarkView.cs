﻿using Innoviz.SmartApp.Core.Attributes;
using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
   public class CreditLimitClosingBookmarkView
    {
        [BookmarkMapping("CANumber")]
        public string CANumber { get; set; }
        [BookmarkMapping("RefCANumber")]
        public string RefCANumber { get; set; }
        [BookmarkMapping("CAStatus")]
        public string CAStatus { get; set; }
        [BookmarkMapping("CACreate")]
        public DateTime CACreate { get; set; }
        [BookmarkMapping("CACreateDate")]
        public string CACreateDate { get; set; }
        [BookmarkMapping("CustomerName1")]
        public string CustomerName1 { get; set; }
        [BookmarkMapping("CustomerName2")]
        public string CustomerName2 { get; set; }
        [BookmarkMapping("CustomerCode")]
        public string CustomerCode { get; set; }
        [BookmarkMapping("SalesResp")]
        public string SalesResp { get; set; }
        [BookmarkMapping("KYCHeader")]
        public string KYCHeader { get; set; }
        [BookmarkMapping("CreditScoring1")]
        public string CreditScoring1 { get; set; }
        [BookmarkMapping("CreditScoring2")]
        public string CreditScoring2 { get; set; }
        [BookmarkMapping("CADesp")]
        public string CADesp { get; set; }
        [BookmarkMapping("CustCompanyRegistrationID")]
        public string CustCompanyRegistrationID { get; set; }
        [BookmarkMapping("CustomerAddress")]
        public string CustomerAddress { get; set; }
        [BookmarkMapping("CustComEstablished")]
        public DateTime? CustComEstablished { get; set; }
        [BookmarkMapping("CustComEstablishedDate")]
        public string CustComEstablishedDate { get; set; }
        [BookmarkMapping("CustBusinessType")]
        public string CustBusinessType { get; set; }
        [BookmarkMapping("CustBlackListStatus")]
        public string CustBlackListStatus { get; set; }
        [BookmarkMapping("TotalApprovedCreditLimit")]
        public decimal TotalApprovedCreditLimit { get; set; }
        [BookmarkMapping("TotalAROutstanding")]
        public decimal TotalAROutstanding { get; set; }
        [BookmarkMapping("MarketingComment")]
        public string MarketingComment { get; set; }
        [BookmarkMapping("CreditComment")]
        public string CreditComment { get; set; }
        [BookmarkMapping("ApproverComment")]
        public string ApproverComment { get; set; }
        [BookmarkMapping("CARemark")]
        public string CARemark { get; set; }
        [BookmarkMapping("ReasonRemark")]
        public string ReasonRemark { get; set; }        
    }
}
