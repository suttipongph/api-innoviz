using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ReceiptTableItemView : ViewBranchCompanyBaseEntity
	{
		public string ReceiptTableGUID { get; set; }
		public string ChequeBankGroupGUID { get; set; }
		public string ChequeBranch { get; set; }
		public string ChequeDate { get; set; }
		public string ChequeNo { get; set; }
		public string CurrencyGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public string DocumentStatusGUID { get; set; }
		public decimal ExchangeRate { get; set; }
		public string InvoiceSettlementDetailGUID { get; set; }
		public string MethodOfPaymentGUID { get; set; }
		public decimal OverUnderAmount { get; set; }
		public string ReceiptAddress1 { get; set; }
		public string ReceiptAddress2 { get; set; }
		public string ReceiptDate { get; set; }
		public string ReceiptId { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public string Remark { get; set; }
		public decimal SettleAmount { get; set; }
		public decimal SettleAmountMST { get; set; }
		public decimal SettleBaseAmount { get; set; }
		public decimal SettleBaseAmountMST { get; set; }
		public decimal SettleTaxAmount { get; set; }
		public decimal SettleTaxAmountMST { get; set; }
		public string TransDate { get; set; }
		public string CustomerTable_Values { get; set; }
		public string ReceiptTempTable_Values { get; set; }
		public string Currency_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string Methodopayment_Values { get; set; }
		public string InvoiceSettlementDetail_Values { get; set; }

		public string RefId { get; set; }
		public string DocumentStatus_Description { get; set; }
		public string ReceiptAddress_Values { get; set; }
	}
	public class PrintReceiptReportView : ViewReportBaseEntity
	{
		public string ReceiptTableGUID { get; set; }
		public string PrintReceiptGUID { get; set; }
		public string CustomerId { get; set; }
		public string ReceiptDate { get; set; }
		public string ReceiptId { get; set; }
		public decimal SettleAmount { get; set; }
		public decimal SettleBaseAmount { get; set; }
		public decimal SettleTaxAmount { get; set; }
		public string TransDate { get; set; }
		public bool IsCopy { get; set; }

	}
}
