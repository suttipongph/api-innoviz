using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BusinessCollateralAgmLineItemView : ViewCompanyBaseEntity
	{
		public string BusinessCollateralAgmLineGUID { get; set; }
		public string AccountNumber { get; set; }
		public string BankGroupGUID { get; set; }
		public string BankTypeGUID { get; set; }
		public string BusinessCollateralAgmTableGUID { get; set; }
		public string BusinessCollateralSubTypeGUID { get; set; }
		public string BusinessCollateralTypeGUID { get; set; }
		public decimal BusinessCollateralValue { get; set; }
		public string BuyerName { get; set; }
		public string BuyerTableGUID { get; set; }
		public string BuyerTaxIdentificationId { get; set; }
		public decimal CapitalValuation { get; set; }
		public string ChassisNumber { get; set; }
		public string CreditAppReqBusinessCollateralGUID { get; set; }
		public string DateOfValuation { get; set; }
		public string Description { get; set; }
		public decimal GuaranteeAmount { get; set; }
		public string Lessee { get; set; }
		public string Lessor { get; set; }
		public int LineNum { get; set; }
		public string MachineNumber { get; set; }
		public string MachineRegisteredStatus { get; set; }
		public string Ownership { get; set; }
		public int PreferentialCreditorNumber { get; set; }
		public string ProjectName { get; set; }
		public decimal Quantity { get; set; }
		public string RefAgreementDate { get; set; }
		public string RefAgreementId { get; set; }
		public string RegisteredPlace { get; set; }
		public string RegistrationPlateNumber { get; set; }
		public string TitleDeedDistrict { get; set; }
		public string TitleDeedNumber { get; set; }
		public string TitleDeedProvince { get; set; }
		public string TitleDeedSubDistrict { get; set; }
		public string Unit { get; set; }
		public string ValuationCommittee { get; set; }
		public string BusinessCollateralAgmTable_BusinessCollateralAgmId { get; set; }
		public string BusinessCollateralAgmTable_InternalBusinessCollateralAgmId { get; set; }
		public string businessCollateralAgmTable_CreditAppRequestTableGUID { get; set; }
		public string BusinessCollateralAgmTable_Values { get; set; }
		public int BusinessCollateralAgmTable_AgreementDocType { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string Product { get; set; }
		public string OriginalBusinessCollateralAgreementLineGUID { get; set; }
		public string OriginalBusinessCollateralAgreementTableGUID { get; set; }
		public int BusinessCollateralAgmTable_ProductType { get; set; }
		public bool Cancelled { get; set; }
		public string BusinessCollateralStatusGUID { get; set; }
		public string BusinessCollateralStatus_Values { get; set; }
		public string AttachmentText { get; set; }
	}
}
