using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class GuarantorAgreementLineAffiliateItemView : ViewCompanyBaseEntity
	{
		public string GuarantorAgreementLineAffiliateGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public string GuarantorAgreementLineGUID { get; set; }
		public string MainAgreementTableGUID { get; set; }
		public int Ordering { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string GuarantorAgreementLine_Values { get; set; }
		public string Customer_CustomerGUID { get; set; }
		public string MainAgreement_ProductType { get; set; }
		public string GuarantorAgreementTable_ParentCompanyGUID { get; set; }

	}
}
