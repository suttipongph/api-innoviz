using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class NationalityItemView : ViewCompanyBaseEntity
	{
		public string NationalityGUID { get; set; }
		public string Description { get; set; }
		public string NationalityId { get; set; }
	}
}
