using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class RelatedPersonTableItemView : ViewCompanyBaseEntity
	{
		public string RelatedPersonTableGUID { get; set; }
		public string BackgroundSummary { get; set; }
		public string CompanyName { get; set; }
		public string OperatedBy { get; set; }
		public string DateOfBirth { get; set; }
		public string DateOfEstablishment { get; set; }
		public string DateOfExpiry { get; set; }
		public string DateOfIssue { get; set; }
		public string Email { get; set; }
		public string Extension { get; set; }
		public string Fax { get; set; }
		public string GenderGUID { get; set; }
		public int IdentificationType { get; set; }
		public decimal Income { get; set; }
		public string IssuedBy { get; set; }
		public string LineId { get; set; }
		public string MaritalStatusGUID { get; set; }
		public string Mobile { get; set; }
		public string Name { get; set; }
		public string NationalityGUID { get; set; }
		public string OccupationGUID { get; set; }
		public decimal OtherIncome { get; set; }
		public string OtherSourceIncome { get; set; }
		public decimal PaidUpCapital { get; set; }
		public string PassportId { get; set; }
		public string Phone { get; set; }
		public string Position { get; set; }
		public string RaceGUID { get; set; }
		public int RecordType { get; set; }
		public decimal RegisteredCapital { get; set; }
		public string RegistrationTypeGUID { get; set; }
		public string RelatedPersonId { get; set; }
		public string SpouseName { get; set; }
		public string TaxId { get; set; }
		public int WorkExperienceMonth { get; set; }
		public int WorkExperienceYear { get; set; }
		public string WorkPermitId { get; set; }
		public string Nationality_NationalityId { get; set; }
		public string Race_RaceId { get; set; }
		public int Age { get; set; }
		public string CustomerTable_customerTableGUID { get; set; }
		#region GetRelatedPersonTableByCreditAppTableDropDown
		public string CreditAppRequestTable_CreditAppRequestTableGUID { get; set; }
		public int Reference_RefType { get; set; }
		public bool Reference_Inactive { get; set; }
		public string Nationality_Values { get; set; }
		public string Race_Values { get; set; }
		#endregion GetRelatedPersonTableByCreditAppTableDropDown

	}
}
