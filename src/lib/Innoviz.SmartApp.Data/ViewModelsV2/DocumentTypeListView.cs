using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class DocumentTypeListView : ViewCompanyBaseEntity
	{
		public string DocumentTypeGUID { get; set; }
		public string DocumentTypeId { get; set; }
		public string Description { get; set; }
	}
}
