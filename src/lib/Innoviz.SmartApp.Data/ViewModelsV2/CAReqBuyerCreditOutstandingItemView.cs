using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CAReqBuyerCreditOutstandingItemView : ViewCompanyBaseEntity
	{
		public string CAReqBuyerCreditOutstandingGUID { get; set; }
		public decimal ApprovedCreditLimitLine { get; set; }
		public decimal ARBalance { get; set; }
		public string AssignmentMethodGUID { get; set; }
		public string BillingResponsibleByGUID { get; set; }
		public string BuyerTableGUID { get; set; }
		public string CreditAppLineGUID { get; set; }
		public string CreditAppRequestTableGUID { get; set; }
		public string CreditAppTableGUID { get; set; }
		public int LineNum { get; set; }
		public decimal MaxPurchasePct { get; set; }
		public string MethodOfPaymentGUID { get; set; }
		public int ProductType { get; set; }
		public string Status { get; set; }
	}
}
