using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ProjectProgressTableListView : ViewCompanyBaseEntity
	{
		public string ProjectProgressTableGUID { get; set; }
		public string ProjectProgressId { get; set; }
		public string TransDate { get; set; }
		public string Description { get; set; }
		public string WithdrawalTableGUID { get; set; }
	}
}
