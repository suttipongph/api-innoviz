﻿
using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class ServiceFeeTransCalculateFieldView : ResultBaseEntity
    {
        public decimal AmountBeforeTax { get; set; }
        public decimal AmountIncludeTax { get; set; }
        public decimal SettleInvoiceAmount { get; set; }
        public decimal SettleWHTAmount { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal WHTAmount { get; set; }
        public string TaxTableGUID { get; set; }
        public string WithholdingTaxTableGUID { get; set; }
    }
}
