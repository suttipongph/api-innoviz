using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class NumberSeqSetupByProductTypeItemView : ViewCompanyBaseEntity
	{
		public string NumberSeqSetupByProductTypeGUID { get; set; }
		public string CreditAppNumberSeqGUID { get; set; }
		public string CreditAppRequestNumberSeqGUID { get; set; }
		public string GuarantorAgreementNumberSeqGUID { get; set; }
		public string InternalGuarantorAgreementNumberSeqGUID { get; set; }
		public string InternalMainAgreementNumberSeqGUID { get; set; }
		public string MainAgreementNumberSeqGUID { get; set; }
		public string TaxInvoiceNumberSeqGUID { get; set; }
		public string TaxCreditNoteNumberSeqGUID { get; set; }
		public string ReceiptNumberSeqGUID { get; set; }
		public int ProductType { get; set; }
	}
}
