﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class InterestCalcAmountView
    {
        public decimal InterestCalcAmount { get; set; }
        public int InterestDay { get; set; }
    }
}
