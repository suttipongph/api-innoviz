using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BookmarkDocumentTemplateTableItemView : ViewCompanyBaseEntity
	{
		public string BookmarkDocumentTemplateTableGUID { get; set; }
		public int BookmarkDocumentRefType { get; set; }
		public string BookmarkDocumentTemplateId { get; set; }
		public string Description { get; set; }
		public string RefGUID { get; set; }
		public string RefType { get; set; }
	}
	public class BookmarkDocumentTemplateTableItemViewResult : ResultBaseEntity
    {

    }
}
