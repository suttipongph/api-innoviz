using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ConsortiumLineListView : ViewCompanyBaseEntity
	{
		public string ConsortiumLineGUID { get; set; }
		public string CustomerName { get; set; }
		public string OperatedBy { get; set; }
		public string Position { get; set; }
		public decimal ProportionOfShareholderPct { get; set; }
		public bool IsMain { get; set; }
		public string ConsortiumTableGUID { get; set; }
		public int Ordering { get; set; }
	}
}
