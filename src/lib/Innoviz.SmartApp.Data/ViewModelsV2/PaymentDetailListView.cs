using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class PaymentDetailListView : ViewCompanyBaseEntity
	{
		public string PaymentDetailGUID { get; set; }
		public int PaidToType { get; set; }
		public string InvoiceTypeGUID { get; set; }
		public bool SuspenseTransfer { get; set; }
		public string CustomerTableGUID { get; set; }
		public string VendorTableGUID { get; set; }
		public decimal PaymentAmount { get; set; }
		public string InvoiceType_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string VendorTable_Values { get; set; }
		public string InvoiceType_InvoiceTypeId { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string VendorTable_VendorId { get; set; }
		public string RefGUID { get; set; }
	}
}
