using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class AssignmentAgreementTableListView : ViewCompanyBaseEntity
	{
		public string AssignmentAgreementTableGUID { get; set; }
		public string InternalAssignmentAgreementId { get; set; }
		public string AssignmentAgreementId { get; set; }
		public string BuyerTableGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public string AssignmentMethodGUID { get; set; }
		public string AgreementDate { get; set; }
		public decimal AssignmentAgreementAmount { get; set; }
		public string DocumentStatusGUID { get; set; }
		public int AgreementDocType { get; set; }
		public string AssignmentMethod_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string RefAssignmentAgreementTableGUID { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string AssignmentMethod_AssignmentMethodId { get; set; }
		public string AssignmentProductDescription { get; set; }
		public string ToWhomConcern { get; set; }
		public string CancelAuthorityPersonID { get; set; }
		public string CancelAuthorityPersonName { get; set; }
		public string CancelAuthorityPersonAddress { get; set; }
		public string RefCreditAppRequestTableGUID { get; set; }
		public string CreditAppReqAssignmentGUID { get; set; }
		public string CreditAppRequestTable_Values { get; set; }
		public string CreditAppRequestTable_CreditAppRequestId { get; set; }
		public string Description { get; set; }

	}
}
