using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class GuarantorAgreementTableItemView : ViewCompanyBaseEntity
	{
		public string GuarantorAgreementTableGUID { get; set; }
		public bool Affiliate { get; set; }
		public string AgreementDate { get; set; }
		public int AgreementDocType { get; set; }
		public int AgreementExtension { get; set; }
		public int AgreementYear { get; set; }
		public string Description { get; set; }
		public string DocumentReasonGUID { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string ExpiryDate { get; set; }
		public string GuarantorAgreementId { get; set; }
		public string InternalGuarantorAgreementId { get; set; }
		public string MainAgreementTableGUID { get; set; }
		public string ParentCompanyGUID { get; set; }
		public string RefGuarantorAgreementTableGUID { get; set; }
		public string SigningDate { get; set; }
		public string WitnessCompany1GUID { get; set; }
		public string WitnessCompany2GUID { get; set; }
		public string WitnessCustomer1 { get; set; }
		public string WitnessCustomer2 { get; set; }
		public string MainAgreementTable_MainAgreementId { get; set; }
		public string DocumentStatus_Values { get; set; }
		public int creditApp_ProductType { get; set; }
		public string company_CompanyId { get; set; }
		public int HaveLine { get; set; }
		public string GuarantorAgreementTable_RefValue { get; set; }
		public string GuarantorAgreementTable_RefInternalValue { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public bool IsBookmarkComplete { get; set; }
		public string MainAgreement_ConsortiumGUID { get; set; }
		public string CustomerTable_CustomerTableGUID { get; set; }
		public string CustomerTable_Value { get; set; }
		public string CreditAppRequestTable_CreditAppRequestTableGUID { get; set; }
		public string CreditAppTable_CreditAppTableGUID { get; set; }
		public string GuarantorTransTable_RefGUID { get; set; }
		public bool GuarantorTransTable_InActive { get; set; }
		public string Remark { get; set; }
		public string DocumentReason_Values { get; set; }
		public string MainAgreementTable_StatusId { get; set; }
		public int MainAgreementTable_ProductType { get; set; }
		public NotificationResponse Notification { get; set; }
		public string MainAgreementTable_CustomerName { get; set; }
	}
}
