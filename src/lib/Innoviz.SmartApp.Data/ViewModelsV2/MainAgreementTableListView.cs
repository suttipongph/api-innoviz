using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class MainAgreementTableListView : ViewCompanyBaseEntity
	{
		public string MainAgreementTableGUID { get; set; }
		public string InternalMainAgreementId { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string MainAgreementId { get; set; }
		public string AgreementDate { get; set; }
		public string CreditAppTableGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public string BuyerTableGUID { get; set; }
		public int ProductType { get; set; }
		public int AgreementDocType { get; set; }
		public string CreditLimitTypeGUID { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string CreditLimitType_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string CreditAppTable_CreditAppId { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string CreditLimitType_CreditLimitTypeId { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string RefMainAgreementTableGUID { get; set; }
		public bool CreditLimitType_Revolving { get; set; }
		public string Description { get; set; }
	}
}
