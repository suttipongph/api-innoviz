﻿using System;
using System.Collections.Generic;
using System.Text;
using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class PrintPurchaseView
    {
        public string PurchaseTableGUID { get; set; }
        public string PurchaseId { get; set; }
        public string PurchaseDate { get; set; }
        public string DocumentStatus { get; set; }
        public string CustomerId { get; set; }
        public bool Rollbill { get; set; }
        public bool AdditionalPurchase { get; set; }
    }
    public class PrintPurchaseReporttView : ViewReportBaseEntity
    {
        public string PurchaseTableGUID { get; set; }
        public string PurchaseId { get; set; }
        public string PurchaseDate { get; set; }
        public string DocumentStatus { get; set; }
        public string CustomerId { get; set; }
        public bool Rollbill { get; set; }
        public bool AdditionalPurchase { get; set; }
    }
}
