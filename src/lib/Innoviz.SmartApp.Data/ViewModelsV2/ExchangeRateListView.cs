using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ExchangeRateListView : ViewDateEffectiveBaseEntity
	{
		public string ExchangeRateGUID { get; set; }
		public string CurrencyGUID { get; set; }
		public string HomeCurrencyGUID { get; set; }
		public decimal Rate { get; set; }
		public string Currency_CurrencyId { get; set; }
		public string HomeCurrency_CurrencyId { get; set; }
	}
}
