using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ApplicationTableItemView : ViewCompanyBaseEntity
	{
		public string ApplicationTableGUID { get; set; }
		public string AgreementTypeGUID { get; set; }
		public string ApplicationDate { get; set; }
		public string ApplicationId { get; set; }
		public string BillingAddressGUID { get; set; }
		public int CalculationType { get; set; }
		public string CreditResultGUID { get; set; }
		public string CreditResultNotes { get; set; }
		public string CurrencyGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public string DeliveryAddressGUID { get; set; }
		public string Dimension1GUID { get; set; }
		public string Dimension2GUID { get; set; }
		public string Dimension3GUID { get; set; }
		public string Dimension4GUID { get; set; }
		public string Dimension5GUID { get; set; }
		public string DocumentReasonGUID { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string ExpectedExecuteDate { get; set; }
		public string ExpirationDate { get; set; }
		public string FlexInfo1 { get; set; }
		public string FlexInfo2 { get; set; }
		public string FlexSetupGUID1 { get; set; }
		public string FlexSetupGUID2 { get; set; }
		public string FollowupDate { get; set; }
		public string IntroducedByGUID { get; set; }
		public string InvoiceAddressGUID { get; set; }
		public string LanguageGUID { get; set; }
		public string LeaseSubTypeGUID { get; set; }
		public string LeaseTypeGUID { get; set; }
		public string MailingInvoiceAddressGUID { get; set; }
		public string MailingReceiptAddressGUID { get; set; }
		public string NCBAccountStatusGUID { get; set; }
		public string NCBNotes { get; set; }
		public string OriginalAgreementGUID { get; set; }
		public string PaymentFrequencyGUID { get; set; }
		public int ProcessInstanceId { get; set; }
		public string PropertyAddressGUID { get; set; }
		public string ProspectTableGUID { get; set; }
		public string ReasonRemark { get; set; }
		public string ReceiptAddressGUID { get; set; }
		public int RefAgreementExtension { get; set; }
		public string RegisterAddressGUID { get; set; }
		public string Remark { get; set; }
		public string ResponsibleBy { get; set; }
		public bool SalesLeaseBack { get; set; }
		public string WorkingAddressGUID { get; set; }
		public string DocumentStatus_StatusId { get; set; }
	}
}
