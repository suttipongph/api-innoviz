using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class NCBTransListView : ViewCompanyBaseEntity
	{
		public string NCBTransGUID { get; set; }
		public string CreditTypeGUID { get; set; }
		public decimal CreditLimit { get; set; }
		public decimal OutstandingAR { get; set; }
		public decimal MonthlyRepayment { get; set; }
		public string EndDate { get; set; }
		public string BankGroupGUID { get; set; }
		public string NCBAccountStatusGUID { get; set; }
		public string CreditType_Values { get; set; }
		public string BankGroup_Values { get; set; }
		public string NCBAccountStatus_Values { get; set; }
		public string CreditType_CreditTypeId { get; set; }
		public string BankGroup_BankGroupId { get; set; }
		public string NcbAccountStatus_NCBAccStatusId { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
	}
}
