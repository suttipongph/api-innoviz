using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class NumberSeqTableItemView : ViewCompanyBaseEntity
	{
		public string NumberSeqTableGUID { get; set; }
		public string Description { get; set; }
		public int Largest { get; set; }
		public bool Manual { get; set; }
		public int Next { get; set; }
		public string NumberSeqCode { get; set; }
		public int Smallest { get; set; }
		public string NumberSeqTable_Values { get; set; }
		public string NumberSeqParameter_ReferenceId { get; set; }
	}
}
