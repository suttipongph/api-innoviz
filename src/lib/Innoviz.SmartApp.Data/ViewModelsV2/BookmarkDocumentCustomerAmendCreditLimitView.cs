﻿using Innoviz.SmartApp.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class BookmarkDocumentCustomerAmendCreditLimitView 
    {
        [BookmarkMapping("RefCANumber")]
        public string OriginalCreditAppTableId { get; set; }

        [BookmarkMapping("CANumber")]
        public string QCreditAppRequestTable_CreditAppRequestId { get; set; }
        public string QCreditAppRequestTable_RefCreditAppTableGUID { get; set; }
        public string QCreditAppRequestTable_DocumentStatusGUID { get; set; }
        [BookmarkMapping("CACreateDate")]
        public string QCreditAppRequestTable_RequestDate { get; set; }
        public string QCreditAppRequestTable_CustomerTableGUID { get; set; }
        [BookmarkMapping("CARemark")]
        public string QCreditAppRequestTable_Remark { get; set; }
        [BookmarkMapping("ProductType")]
        public string QCreditAppRequestTable_ProductType { get; set; }

        public string QCreditAppRequestTable_ProductSubTypeGUID { get; set; }

        public string QCreditAppRequestTable_CreditLimitTypeGUID { get; set; }
        [BookmarkMapping("CreditLimitLife")]
        public string QCreditAppRequestTable_CreditLimitExpiration { get; set; }
        [BookmarkMapping("CreditPeriodStartDate")]
        public string QCreditAppRequestTable_StartDate { get; set; }
        [BookmarkMapping("CreditPeriodEndDate")]
        public string QCreditAppRequestTable_ExpiryDate { get; set; }
        [BookmarkMapping("ApprovedCreditLimitAmt")]
        public string QCreditAppRequestTable_ApprovedCreditLimitRequest { get; set; } 
        [BookmarkMapping("CreditLimitReqAmt")]
        public string QCreditAppRequestTable_CreditLimitRequest { get; set; }

        public string QCreditAppRequestTable_InterestTypeGUID { get; set; }
        [BookmarkMapping("InterestAdj")]
        public string QCreditAppRequestTable_InterestAdjustment { get; set; }
        [BookmarkMapping("TotalInterest")]
        public decimal QCreditAppRequestTable_TotalInterestPct { get; set; }
        [BookmarkMapping("MaxPurch")]
        public string QCreditAppRequestTable_MaxPurchasePct { get; set; }
        [BookmarkMapping("MaxRetenPerc")]
        public string QCreditAppRequestTable_MaxRetentionPct { get; set; }
        [BookmarkMapping("MaxRetenAmt")]
        public string QCreditAppRequestTable_MaxRetentionAmount { get; set; }
        [BookmarkMapping("CACondition")]
        public string QCreditAppRequestTable_CACondition { get; set; }
        public string QCreditAppRequestTable_PDCBankGUID { get; set; }
        [BookmarkMapping("CreditLimitFee")]
        public string QCreditAppRequestTable_CreditRequestFeePct { get; set; }
        [BookmarkMapping("CreditLimitFeeAmt")]
        public string QCreditAppRequestTable_CreditRequestFeeAmount { get; set; }
        [BookmarkMapping("FactoringPurchaseFee")]
        public string QCreditAppRequestTable_PurchaseFeePct { get; set; }
        [BookmarkMapping("FactoringPurchaseFeeCalBase")]
        public string QCreditAppRequestTable_PurchaseFeeCalculateBase { get; set; }
        public string QCreditAppRequestTable_RegisteredAddressGUID { get; set; }
        public string QCreditAppRequestTable_IntroducedByGUID { get; set; }
        [BookmarkMapping("CustFinRevenuePerMonth1")]
        public string QCreditAppRequestTable_SalesAvgPerMonth { get; set; }
        [BookmarkMapping("MarketingComment")]
        public string QCreditAppRequestTable_MarketingComment { get; set; }
        [BookmarkMapping("CreditComment")]
        public string QCreditAppRequestTable_CreditComment { get; set; }
        [BookmarkMapping("ApproverComment")]
        public string QCreditAppRequestTable_ApproverComment { get; set; }
        [BookmarkMapping("CreditAllBuyer")]
        public string QCreditAppRequestTable_CustomerCreditLimit { get; set; }
        [BookmarkMapping("CreditBueroCheckDate")]
        public string QCreditAppRequestTable_NCBCheckedDate { get; set; }
        [BookmarkMapping("CADesp")]
        public string QCreditAppRequestTable_Description { get; set; }

        [BookmarkMapping("CAStatus")]
        public string DocumentStatus_Status { get; set; }
        [BookmarkMapping("KYCHeader")]
        public string KYCSetup_KYC { get; set; }
        [BookmarkMapping("CreditScoring")]
        public string CreditScoring_CreditScoring { get; set; }

        [BookmarkMapping("ProductSubType")]
        public string ProductSubType_ProductSubType { get; set; }
        [BookmarkMapping("CreditLimitType")]
        public string CreditLimitType_CreditLimitType { get; set; }
        [BookmarkMapping("CAInterestType")]
        public string InterestType_InterestType { get; set; }
        [BookmarkMapping("CustomerAddress")]
        public string AddressTrans_RegisteredAddress { get; set; }
        [BookmarkMapping("CustBusinessType")]
        public string BusinessType_BusinessType { get; set; }
        [BookmarkMapping("Channel")]
        public string IntroducedBy_IntroducedBy { get; set; }
        public string QOriginalCreditAppRequest_OriginalCreditAppRequestId { get; set; }
        public string QOriginalCreditAppRequest_ApprovedCreditLimitRequest { get; set; }
        public string QOriginalCreditAppRequest_CreditLimitRequest { get; set; }
        [BookmarkMapping("CustomerCode")]
        public string QCustomer_CustomerId { get; set; }
        [BookmarkMapping("CustomerName1", "CustomerName2")]
        public string QCustomer_CustomerName { get; set; }
        public string QCustomer_ResponsibleByGUID { get; set; }
        public string QCustomer_IdentificationType { get; set; }
        public string QCustomer_TaxID { get; set; }
        public string QCustomer_PassportID { get; set; }
        public string QCustomer_RecordType { get; set; }
        public string QCustomer_DateOfEstablish { get; set; }
        public string QCustomer_DateOfBirth { get; set; }
        public string QCustomer_BusinessTypeGUID { get; set; }
        public string QCustomer_IntroducedByGUID { get; set; }
        [BookmarkMapping("SalesResp")]
        public string QCustomer_ResponsibleBy { get; set; }

        [BookmarkMapping("CustPDCBankName")]
        public string QPDCBank_CustPDCBankName { get; set; }
        [BookmarkMapping("CustPDCBankBranch")]
        public string QPDCBank_CustPDCBankBranch { get; set; }
        [BookmarkMapping("CustPDCBankAccountType")]
        public string QPDCBank_CustPDCBankType { get; set; }
        [BookmarkMapping("CustPDCBankAccountNumber")]
        public string QPDCBank_CustPDCBankAccountName { get; set; }

        [BookmarkMapping("ServiceFeeDesc01")]
        public string QServiceFeeConditionTrans_ServicefeeTypeId_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt01")]
        public string QServiceFeeConditionTrans_AmountBeforeTax_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark01")]
        public string QServiceFeeConditionTrans_Description_1 { get; set; }


        [BookmarkMapping("ServiceFeeDesc02")]
        public string QServiceFeeConditionTrans_ServicefeeTypeId_2 { get; set; }
        [BookmarkMapping("ServiceFeeAmt02")]
        public string QServiceFeeConditionTrans_AmountBeforeTax_2 { get; set; }
        [BookmarkMapping("ServiceFeeRemark02")]
        public string QServiceFeeConditionTrans_Description_2 { get; set; }


        [BookmarkMapping("ServiceFeeDesc03")]
        public string QServiceFeeConditionTrans_ServicefeeTypeId_3 { get; set; }
        [BookmarkMapping("ServiceFeeAmt03")]
        public string QServiceFeeConditionTrans_AmountBeforeTax_3 { get; set; }
        [BookmarkMapping("ServiceFeeRemark03")]
        public string QServiceFeeConditionTrans_Description_3 { get; set; }


        [BookmarkMapping("ServiceFeeDesc04")]
        public string QServiceFeeConditionTrans_ServicefeeTypeId_4 { get; set; }
        [BookmarkMapping("ServiceFeeAmt04")]
        public string QServiceFeeConditionTrans_AmountBeforeTax_4 { get; set; }
        [BookmarkMapping("ServiceFeeRemark04")]
        public string QServiceFeeConditionTrans_Description_4 { get; set; }


        [BookmarkMapping("ServiceFeeDesc05")]
        public string QServiceFeeConditionTrans_ServicefeeTypeId_5 { get; set; }
        [BookmarkMapping("ServiceFeeAmt05")]
        public string QServiceFeeConditionTrans_AmountBeforeTax_5 { get; set; }
        [BookmarkMapping("ServiceFeeRemark05")]
        public string QServiceFeeConditionTrans_Description_5 { get; set; }


        [BookmarkMapping("ServiceFeeDesc06")]
        public string QServiceFeeConditionTrans_ServicefeeTypeId_6 { get; set; }
        [BookmarkMapping("ServiceFeeAmt06")]
        public string QServiceFeeConditionTrans_AmountBeforeTax_6 { get; set; }
        [BookmarkMapping("ServiceFeeRemark06")]
        public string QServiceFeeConditionTrans_Description_6 { get; set; }


        [BookmarkMapping("ServiceFeeDesc07")]
        public string QServiceFeeConditionTrans_ServicefeeTypeId_7 { get; set; }
        [BookmarkMapping("ServiceFeeAmt07")]
        public string QServiceFeeConditionTrans_AmountBeforeTax_7 { get; set; }
        [BookmarkMapping("ServiceFeeRemark07")]
        public string QServiceFeeConditionTrans_Description_7 { get; set; }


        [BookmarkMapping("ServiceFeeDesc08")]
        public string QServiceFeeConditionTrans_ServicefeeTypeId_8 { get; set; }
        [BookmarkMapping("ServiceFeeAmt08")]
        public string QServiceFeeConditionTrans_AmountBeforeTax_8 { get; set; }
        [BookmarkMapping("ServiceFeeRemark08")]
        public string QServiceFeeConditionTrans_Description_8 { get; set; }


        [BookmarkMapping("ServiceFeeDesc09")]
        public string QServiceFeeConditionTrans_ServicefeeTypeId_9 { get; set; }
        [BookmarkMapping("ServiceFeeAmt09")]
        public string QServiceFeeConditionTrans_AmountBeforeTax_9 { get; set; }
        [BookmarkMapping("ServiceFeeRemark09")]
        public string QServiceFeeConditionTrans_Description_9 { get; set; }


        [BookmarkMapping("ServiceFeeDesc10")]
        public string QServiceFeeConditionTrans_ServicefeeTypeId_10 { get; set; }
        [BookmarkMapping("ServiceFeeAmt10")]
        public string QServiceFeeConditionTrans_AmountBeforeTax_10 { get; set; }
        [BookmarkMapping("ServiceFeeRemark10")]
        public string QServiceFeeConditionTrans_Description_10 { get; set; }

        [BookmarkMapping("SumServiceFeeAmt")]
        public string QSumServiceFeeConditionTrans_ServiceFeeAmt { get; set; }



        [BookmarkMapping("GuarantorNameFirst1")]
        public string QGuarantorTrans_RelatedPersonName_1 { get; set; }
        [BookmarkMapping("GuarantorPositionFirst1")]
        public string QGuarantorTrans_GuarantorType_1 { get; set; }



        [BookmarkMapping("GuarantorNameSecond1")]
        public string QGuarantorTrans_RelatedPersonName_2 { get; set; }
        [BookmarkMapping("GuarantorPositionSecond1")]
        public string QGuarantorTrans_GuarantorType_2 { get; set; }



        [BookmarkMapping("GuarantorNameThird1")]
        public string QGuarantorTrans_RelatedPersonName_3 { get; set; }
        [BookmarkMapping("GuarantorPositionThird1")]
        public string QGuarantorTrans_GuarantorType_3 { get; set; }



        [BookmarkMapping("GuarantorNameFourth1")]
        public string QGuarantorTrans_RelatedPersonName_4 { get; set; }
        [BookmarkMapping("GuarantorPositionFourth1")]
        public string QGuarantorTrans_GuarantorType_4 { get; set; }



        [BookmarkMapping("GuarantorNameFifth1")]
        public string QGuarantorTrans_RelatedPersonName_5 { get; set; }
        [BookmarkMapping("GuarantorPositionFifth1")]
        public string QGuarantorTrans_GuarantorType_5 { get; set; }



        [BookmarkMapping("GuarantorNameSixth1")]
        public string QGuarantorTrans_RelatedPersonName_6 { get; set; }
        [BookmarkMapping("GuarantorPositionSixth1")]
        public string QGuarantorTrans_GuarantorType_6 { get; set; }



        [BookmarkMapping("GuarantorNameSeventh1")]
        public string QGuarantorTrans_RelatedPersonName_7 { get; set; }
        [BookmarkMapping("GuarantorPositionSeventh1")]
        public string QGuarantorTrans_GuarantorType_7 { get; set; }



        [BookmarkMapping("GuarantorNameEighth1")]
        public string QGuarantorTrans_RelatedPersonName_8 { get; set; }
        [BookmarkMapping("GuarantorPositionEighth1")]
        public string QGuarantorTrans_GuarantorType_8 { get; set; }


        [BookmarkMapping("BusinessCollateralNewFirst1")]
        public string QCreditAppReqBusinessCollateral_IsNew_1 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeFirst1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralType_1 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeFirst1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralSubType_1 { get; set; }
        [BookmarkMapping("BusinessCollateralDescFirst1")]
        public string QCreditAppReqBusinessCollateral_Description_1 { get; set; }
        [BookmarkMapping("BusinessCollateralValueFirst1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralValue_1 { get; set; }


        [BookmarkMapping("BusinessCollateralNewSecond1")]
        public string QCreditAppReqBusinessCollateral_IsNew_2 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeSecond1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralType_2 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeSecond1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralSubType_2 { get; set; }
        [BookmarkMapping("BusinessCollateralDescSecond1")]
        public string QCreditAppReqBusinessCollateral_Description_2 { get; set; }
        [BookmarkMapping("BusinessCollateralValueSecond1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralValue_2 { get; set; }


        [BookmarkMapping("BusinessCollateralNewThird1")]
        public string QCreditAppReqBusinessCollateral_IsNew_3 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeThird1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralType_3 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeThird1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralSubType_3 { get; set; }
        [BookmarkMapping("BusinessCollateralDescThird1")]
        public string QCreditAppReqBusinessCollateral_Description_3 { get; set; }
        [BookmarkMapping("BusinessCollateralValueThird1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralValue_3 { get; set; }


        [BookmarkMapping("BusinessCollateralNewFourth1")]
        public string QCreditAppReqBusinessCollateral_IsNew_4 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeFourth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralType_4 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeFourth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralSubType_4 { get; set; }
        [BookmarkMapping("BusinessCollateralDescFourth1")]
        public string QCreditAppReqBusinessCollateral_Description_4 { get; set; }
        [BookmarkMapping("BusinessCollateralValueFourth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralValue_4 { get; set; }


        [BookmarkMapping("BusinessCollateralNewFifth1")]
        public string QCreditAppReqBusinessCollateral_IsNew_5 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeFifth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralType_5 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeFifth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralSubType_5 { get; set; }
        [BookmarkMapping("BusinessCollateralDescFifth1")]
        public string QCreditAppReqBusinessCollateral_Description_5 { get; set; }
        [BookmarkMapping("BusinessCollateralValueFifth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralValue_5 { get; set; }


        [BookmarkMapping("BusinessCollateralNewSixth1")]
        public string QCreditAppReqBusinessCollateral_IsNew_6 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeSixth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralType_6 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeSixth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralSubType_6 { get; set; }
        [BookmarkMapping("BusinessCollateralDescSixth1")]
        public string QCreditAppReqBusinessCollateral_Description_6 { get; set; }
        [BookmarkMapping("BusinessCollateralValueSixth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralValue_6 { get; set; }


        [BookmarkMapping("BusinessCollateralNewSeventh1")]
        public string QCreditAppReqBusinessCollateral_IsNew_7 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeSeventh1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralType_7 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeSeventh1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralSubType_7 { get; set; }
        [BookmarkMapping("BusinessCollateralDescSeventh1")]
        public string QCreditAppReqBusinessCollateral_Description_7 { get; set; }
        [BookmarkMapping("BusinessCollateralValueSeventh1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralValue_7 { get; set; }


        [BookmarkMapping("BusinessCollateralNewEighth1")]
        public string QCreditAppReqBusinessCollateral_IsNew_8 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeEighth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralType_8 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeEighth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralSubType_8 { get; set; }
        [BookmarkMapping("BusinessCollateralDescEighth1")]
        public string QCreditAppReqBusinessCollateral_Description_8 { get; set; }
        [BookmarkMapping("BusinessCollateralValueEighth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralValue_8 { get; set; }


        [BookmarkMapping("BusinessCollateralNewNineth1")]
        public string QCreditAppReqBusinessCollateral_IsNew_9 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeNineth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralType_9 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeNineth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralSubType_9 { get; set; }
        [BookmarkMapping("BusinessCollateralDescNineth1")]
        public string QCreditAppReqBusinessCollateral_Description_9 { get; set; }
        [BookmarkMapping("BusinessCollateralValueNineth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralValue_9 { get; set; }


        [BookmarkMapping("BusinessCollateralNewTenth1")]
        public string QCreditAppReqBusinessCollateral_IsNew_10 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeTenth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralType_10 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeTenth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralSubType_10 { get; set; }
        [BookmarkMapping("BusinessCollateralDescTenth1")]
        public string QCreditAppReqBusinessCollateral_Description_10 { get; set; }
        [BookmarkMapping("BusinessCollateralValueTenth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralValue_10 { get; set; }


        [BookmarkMapping("BusinessCollateralNewEleventh1")]
        public string QCreditAppReqBusinessCollateral_IsNew_11 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeEleventh1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralType_11 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeEleventh1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralSubType_11 { get; set; }
        [BookmarkMapping("BusinessCollateralDescEleventh1")]
        public string QCreditAppReqBusinessCollateral_Description_11 { get; set; }
        [BookmarkMapping("BusinessCollateralValueEleventh1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralValue_11 { get; set; }


        [BookmarkMapping("BusinessCollateralNewTwelfth1")]
        public string QCreditAppReqBusinessCollateral_IsNew_12 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeTwelfth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralType_12 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeTwelfth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralSubType_12 { get; set; }
        [BookmarkMapping("BusinessCollateralDescTwelfth1")]
        public string QCreditAppReqBusinessCollateral_Description_12 { get; set; }
        [BookmarkMapping("BusinessCollateralValueTwelfth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralValue_12 { get; set; }


        [BookmarkMapping("BusinessCollateralNewThirteenth1")]
        public string QCreditAppReqBusinessCollateral_IsNew_13 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeThirteenth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralType_13 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeThirteenth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralSubType_13 { get; set; }
        [BookmarkMapping("BusinessCollateralDescThirteenth1")]
        public string QCreditAppReqBusinessCollateral_Description_13 { get; set; }
        [BookmarkMapping("BusinessCollateralValueThirteenth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralValue_13 { get; set; }


        [BookmarkMapping("BusinessCollateralNewFourteenth1")]
        public string QCreditAppReqBusinessCollateral_IsNew_14 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeFourteenth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralType_14 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeFourteenth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralSubType_14 { get; set; }
        [BookmarkMapping("BusinessCollateralDescFourteenth1")]
        public string QCreditAppReqBusinessCollateral_Description_14 { get; set; }
        [BookmarkMapping("BusinessCollateralValueFourteenth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralValue_14 { get; set; }


        [BookmarkMapping("BusinessCollateralNewFifteenth1")]
        public string QCreditAppReqBusinessCollateral_IsNew_15 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeFifteenth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralType_15 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeFifteenth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralSubType_15 { get; set; }
        [BookmarkMapping("BusinessCollateralDescFifteenth1")]
        public string QCreditAppReqBusinessCollateral_Description_15 { get; set; }
        [BookmarkMapping("BusinessCollateralValueFifteenth1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralValue_15 { get; set; }

        [BookmarkMapping("SumBusinessCollateralValue")]
        public decimal QSumCreditAppReqBusinessCollateral_SumBusinessCollateralValue { get; set; }
        [BookmarkMapping("CustBankName")]
        public string QBankAccountControl_Instituetion { get; set; }
        [BookmarkMapping("CustBankBranch")]
        public string QBankAccountControl_BankBranch { get; set; }
        [BookmarkMapping("CustBankAccountType")]
        public string QBankAccountControl_AccountType { get; set; }
        [BookmarkMapping("CustBankAccountNumber")]
        public string QBankAccountControl_BankAccountName { get; set; }


        [BookmarkMapping("CustAuthorizedPersonNameFirst1")]
        public string QAuthorizedPersonTrans_AuthorizedPersonName_1 { get; set; }
        public string QAuthorizedPersonTrans_DateOfBirth_1 { get; set; }


        [BookmarkMapping("CustAuthorizedPersonNameSecond1")]
        public string QAuthorizedPersonTrans_AuthorizedPersonName_2 { get; set; }
        public string QAuthorizedPersonTrans_DateOfBirth_2 { get; set; }


        [BookmarkMapping("CustAuthorizedPersonNameThird1")]
        public string QAuthorizedPersonTrans_AuthorizedPersonName_3 { get; set; }
        public string QAuthorizedPersonTrans_DateOfBirth_3 { get; set; }


        [BookmarkMapping("CustAuthorizedPersonNameFourth1")]
        public string QAuthorizedPersonTrans_AuthorizedPersonName_4 { get; set; }
        public string QAuthorizedPersonTrans_DateOfBirth_4 { get; set; }


        [BookmarkMapping("CustAuthorizedPersonNameFifth1")]
        public string QAuthorizedPersonTrans_AuthorizedPersonName_5 { get; set; }
        public string QAuthorizedPersonTrans_DateOfBirth_5 { get; set; }


        [BookmarkMapping("CustAuthorizedPersonNameSixth1")]
        public string QAuthorizedPersonTrans_AuthorizedPersonName_6 { get; set; }
        public string QAuthorizedPersonTrans_DateOfBirth_6 { get; set; }


        [BookmarkMapping("CustAuthorizedPersonNameSeventh1")]
        public string QAuthorizedPersonTrans_AuthorizedPersonName_7 { get; set; }
        public string QAuthorizedPersonTrans_DateOfBirth_7 { get; set; }


        [BookmarkMapping("CustAuthorizedPersonNameEighth1")]
        public string QAuthorizedPersonTrans_AuthorizedPersonName_8 { get; set; }
        public string QAuthorizedPersonTrans_DateOfBirth_8 { get; set; }


        [BookmarkMapping("CustShareHolderPersonNameFirst1")]
        public string QOwnerTrans_ShareHolderPersonName_1 { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentFirst1")]
        public string QOwnerTrans_PropotionOfShareholderPct_1 { get; set; }


        [BookmarkMapping("CustShareHolderPersonNameSecond1")]
        public string QOwnerTrans_ShareHolderPersonName_2 { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentSecond1")]
        public string QOwnerTrans_PropotionOfShareholderPct_2 { get; set; }


        [BookmarkMapping("CustShareHolderPersonNameThird1")]
        public string QOwnerTrans_ShareHolderPersonName_3 { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentThird1")]
        public string QOwnerTrans_PropotionOfShareholderPct_3 { get; set; }


        [BookmarkMapping("CustShareHolderPersonNameFourth1")]
        public string QOwnerTrans_ShareHolderPersonName_4 { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentFourth1")]
        public string QOwnerTrans_PropotionOfShareholderPct_4 { get; set; }


        [BookmarkMapping("CustShareHolderPersonNameFifth1")]
        public string QOwnerTrans_ShareHolderPersonName_5 { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentFifth1")]
        public string QOwnerTrans_PropotionOfShareholderPct_5 { get; set; }


        [BookmarkMapping("CustShareHolderPersonNameSixth1")]
        public string QOwnerTrans_ShareHolderPersonName_6 { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentSixth1")]
        public string QOwnerTrans_PropotionOfShareholderPct_6 { get; set; }


        [BookmarkMapping("CustShareHolderPersonNameSeventh1")]
        public string QOwnerTrans_ShareHolderPersonName_7 { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentSeventh1")]
        public string QOwnerTrans_PropotionOfShareholderPct_7 { get; set; }


        [BookmarkMapping("CustShareHolderPersonNameEighth1")]
        public string QOwnerTrans_ShareHolderPersonName_8 { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentEighth1")]
        public string QOwnerTrans_PropotionOfShareholderPct_8 { get; set; }



        [BookmarkMapping("CustFinYearFirst1", "CustFinYearFirst2")]
        public int QCreditAppReqLineFin_Year_1 { get; set; }
        [BookmarkMapping("CustFinRegisteredCapitalFirst1")]
        public string QCreditAppReqLineFin_RegisteredCapital_1 { get; set; }
        [BookmarkMapping("CustFinPaidCapitalFirst1")]
        public string QCreditAppReqLineFin_PaidCapital_1 { get; set; }
        [BookmarkMapping("CustFinTotalAssetFirst1")]
        public string QCreditAppReqLineFin_TotalAsset_1 { get; set; }
        [BookmarkMapping("CustFinTotalLiabilityFirst1")]
        public string QCreditAppReqLineFin_TotalLiability_1 { get; set; }
        [BookmarkMapping("CustFinTotalEquityFirst1")]
        public string QCreditAppReqLineFin_TotalEquity_1 { get; set; }
        [BookmarkMapping("CustFinTotalRevenueFirst1")]
        public string QCreditAppReqLineFin_TotalRevenue_1 { get; set; }
        [BookmarkMapping("CustFinTotalCOGSFirst1")]
        public string QCreditAppReqLineFin_TotalCOGS_1 { get; set; }
        [BookmarkMapping("CustFinTotalGrossProfitFirst1")]
        public string QCreditAppReqLineFin_TotalGrossProfit_1 { get; set; }
        [BookmarkMapping("CustFinTotalOperExpFirst1")]
        public string QCreditAppReqLineFin_TotalOperExpFirst_1 { get; set; }
        [BookmarkMapping("CustFinTotalNetProfitFirst1")]
        public string QCreditAppReqLineFin_TotalNetProfitFirst_1 { get; set; }
        [BookmarkMapping("CustFinNetProfitPercent1")]
        public string QCreditAppReqLineFin_NetProfitPercent_1 { get; set; }
        [BookmarkMapping("CustFinlDE1")]
        public string QCreditAppReqLineFin_lDE_1 { get; set; }
        [BookmarkMapping("CustFinQuickRatio1")]
        public string QCreditAppReqLineFin_QuickRatio_1 { get; set; }
        [BookmarkMapping("CustFinIntCoverageRatio1")]
        public string QCreditAppReqLineFin_IntCoverageRatio_1 { get; set; }



        [BookmarkMapping("CustFinYearSecond1", "CustFinYearSecond2")]
        public int QCreditAppReqLineFin_Year_2 { get; set; }
        [BookmarkMapping("CustFinRegisteredCapitalSecond1")]
        public string QCreditAppReqLineFin_RegisteredCapital_2 { get; set; }
        [BookmarkMapping("CustFinPaidCapitalSecond1")]
        public string QCreditAppReqLineFin_PaidCapital_2 { get; set; }
        [BookmarkMapping("CustFinTotalAssetSecond1")]
        public string QCreditAppReqLineFin_TotalAsset_2 { get; set; }
        [BookmarkMapping("CustFinTotalLiabilitySecond1")]
        public string QCreditAppReqLineFin_TotalLiability_2 { get; set; }
        [BookmarkMapping("CustFinTotalEquitySecond1")]
        public string QCreditAppReqLineFin_TotalEquity_2 { get; set; }
        [BookmarkMapping("CustFinTotalRevenueSecond1")]
        public string QCreditAppReqLineFin_TotalRevenue_2 { get; set; }
        [BookmarkMapping("CustFinTotalCOGSSecond1")]
        public string QCreditAppReqLineFin_TotalCOGS_2 { get; set; }
        [BookmarkMapping("CustFinTotalGrossProfitSecond1")]
        public string QCreditAppReqLineFin_TotalGrossProfit_2 { get; set; }
        [BookmarkMapping("CustFinTotalOperExpSecond1")]
        public string QCreditAppReqLineFin_TotalOperExpFirst_2 { get; set; }
        [BookmarkMapping("CustFinTotalNetProfitSecond1")]
        public string QCreditAppReqLineFin_TotalNetProfitFirst_2 { get; set; }



        [BookmarkMapping("CustFinYearThird1", "CustFinYearThird2")]
        public int QCreditAppReqLineFin_Year_3 { get; set; }
        [BookmarkMapping("CustFinRegisteredCapitalThird1")]
        public string QCreditAppReqLineFin_RegisteredCapital_3 { get; set; }
        [BookmarkMapping("CustFinPaidCapitalThird1")]
        public string QCreditAppReqLineFin_PaidCapital_3 { get; set; }
        [BookmarkMapping("CustFinTotalAssetThird1")]
        public string QCreditAppReqLineFin_TotalAsset_3 { get; set; }
        [BookmarkMapping("CustFinTotalLiabilityThird1")]
        public string QCreditAppReqLineFin_TotalLiability_3 { get; set; }
        [BookmarkMapping("CustFinTotalEquityThird1")]
        public string QCreditAppReqLineFin_TotalEquity_3 { get; set; }
        [BookmarkMapping("CustFinTotalRevenueThird1")]
        public string QCreditAppReqLineFin_TotalRevenue_3 { get; set; }
        [BookmarkMapping("CustFinTotalCOGSThird1")]
        public string QCreditAppReqLineFin_TotalCOGS_3 { get; set; }
        [BookmarkMapping("CustFinTotalGrossProfitThird1")]
        public string QCreditAppReqLineFin_TotalGrossProfit_3 { get; set; }
        [BookmarkMapping("CustFinTotalOperExpThird1")]
        public string QCreditAppReqLineFin_TotalOperExpFirst_3 { get; set; }
        [BookmarkMapping("CustFinTotalNetProfitThird1")]
        public string QCreditAppReqLineFin_TotalNetProfitFirst_3 { get; set; }



        [BookmarkMapping("CustFinYearFourth1", "CustFinYearFourth2")]
        public int QCreditAppReqLineFin_Year_4 { get; set; }
        [BookmarkMapping("CustFinRegisteredCapitalFourth1")]
        public string QCreditAppReqLineFin_RegisteredCapital_4 { get; set; }
        [BookmarkMapping("CustFinPaidCapitalFourth1")]
        public string QCreditAppReqLineFin_PaidCapital_4 { get; set; }
        [BookmarkMapping("CustFinTotalAssetFourth1")]
        public string QCreditAppReqLineFin_TotalAsset_4 { get; set; }
        [BookmarkMapping("CustFinTotalLiabilityFourth1")]
        public string QCreditAppReqLineFin_TotalLiability_4 { get; set; }
        [BookmarkMapping("CustFinTotalEquityFourth1")]
        public string QCreditAppReqLineFin_TotalEquity_4 { get; set; }
        [BookmarkMapping("CustFinTotalRevenueFourth1")]
        public string QCreditAppReqLineFin_TotalRevenue_4 { get; set; }
        [BookmarkMapping("CustFinTotalCOGSFourth1")]
        public string QCreditAppReqLineFin_TotalCOGS_4 { get; set; }
        [BookmarkMapping("CustFinTotalGrossProfitFourth1")]
        public string QCreditAppReqLineFin_TotalGrossProfit_4 { get; set; }
        [BookmarkMapping("CustFinTotalOperExpFourth1")]
        public string QCreditAppReqLineFin_TotalOperExpFirst_4 { get; set; }
        [BookmarkMapping("CustFinTotalNetProfitFourth1")]
        public string QCreditAppReqLineFin_TotalNetProfitFirst_4 { get; set; }



        [BookmarkMapping("CustFinYearFifth1", "CustFinYearFifth2")]
        public int QCreditAppReqLineFin_Year_5 { get; set; }
        [BookmarkMapping("CustFinRegisteredCapitalFifth1")]
        public string QCreditAppReqLineFin_RegisteredCapital_5 { get; set; }
        [BookmarkMapping("CustFinPaidCapitalFifth1")]
        public string QCreditAppReqLineFin_PaidCapital_5 { get; set; }
        [BookmarkMapping("CustFinTotalAssetFifth1")]
        public string QCreditAppReqLineFin_TotalAsset_5 { get; set; }
        [BookmarkMapping("CustFinTotalLiabilityFifth1")]
        public string QCreditAppReqLineFin_TotalLiability_5 { get; set; }
        [BookmarkMapping("CustFinTotalEquityFifth1")]
        public string QCreditAppReqLineFin_TotalEquity_5 { get; set; }
        [BookmarkMapping("CustFinTotalRevenueFifth1")]
        public string QCreditAppReqLineFin_TotalRevenue_5 { get; set; }
        [BookmarkMapping("CustFinTotalCOGSFifth1")]
        public string QCreditAppReqLineFin_TotalCOGS_5 { get; set; }
        [BookmarkMapping("CustFinTotalGrossProfitFifth1")]
        public string QCreditAppReqLineFin_TotalGrossProfit_5 { get; set; }
        [BookmarkMapping("CustFinTotalOperExpFifth1")]
        public string QCreditAppReqLineFin_TotalOperExpFirst_5 { get; set; }
        [BookmarkMapping("CustFinTotalNetProfitFifth1")]
        public string QCreditAppReqLineFin_TotalNetProfitFirst_5 { get; set; }



        [BookmarkMapping("Month1")]
        public string QTaxReportTrans_TaxMonth_1 { get; set; }
        [BookmarkMapping("VATAmountMonth1")]
        public string QTaxReportTrans_Amount_1 { get; set; }


        [BookmarkMapping("Month2")]
        public string QTaxReportTrans_TaxMonth_2 { get; set; }
        [BookmarkMapping("VATAmountMonth2")]
        public string QTaxReportTrans_Amount_2 { get; set; }


        [BookmarkMapping("Month3")]
        public string QTaxReportTrans_TaxMonth_3 { get; set; }
        [BookmarkMapping("VATAmountMonth3")]
        public string QTaxReportTrans_Amount_3 { get; set; }


        [BookmarkMapping("Month4")]
        public string QTaxReportTrans_TaxMonth_4 { get; set; }
        [BookmarkMapping("VATAmountMonth4")]
        public string QTaxReportTrans_Amount_4 { get; set; }


        [BookmarkMapping("Month5")]
        public string QTaxReportTrans_TaxMonth_5 { get; set; }
        [BookmarkMapping("VATAmountMonth5")]
        public string QTaxReportTrans_Amount_5 { get; set; }


        [BookmarkMapping("Month6")]
        public string QTaxReportTrans_TaxMonth_6 { get; set; }
        [BookmarkMapping("VATAmountMonth6")]
        public string QTaxReportTrans_Amount_6 { get; set; }


        [BookmarkMapping("Month7")]
        public string QTaxReportTrans_TaxMonth_7 { get; set; }
        [BookmarkMapping("VATAmountMonth7")]
        public string QTaxReportTrans_Amount_7 { get; set; }


        [BookmarkMapping("Month8")]
        public string QTaxReportTrans_TaxMonth_8 { get; set; }
        [BookmarkMapping("VATAmountMonth8")]
        public string QTaxReportTrans_Amount_8 { get; set; }


        [BookmarkMapping("Month9")]
        public string QTaxReportTrans_TaxMonth_9 { get; set; }
        [BookmarkMapping("VATAmountMonth9")]
        public string QTaxReportTrans_Amount_9 { get; set; }


        [BookmarkMapping("Month10")]
        public string QTaxReportTrans_TaxMonth_10 { get; set; }
        [BookmarkMapping("VATAmountMonth10")]
        public string QTaxReportTrans_Amount_10 { get; set; }


        [BookmarkMapping("Month11")]
        public string QTaxReportTrans_TaxMonth_11 { get; set; }
        [BookmarkMapping("VATAmountMonth11")]
        public string QTaxReportTrans_Amount_11 { get; set; }


        [BookmarkMapping("Month12")]
        public string QTaxReportTrans_TaxMonth_12 { get; set; }
        [BookmarkMapping("VATAmountMonth12")]
        public string QTaxReportTrans_Amount_12 { get; set; }


        [BookmarkMapping("OutputVATStart")]
        public string QTaxReportStart_TaxMonth { get; set; }
        [BookmarkMapping("OutputVATEnd")]
        public string QTaxReportEnd_TaxMonth { get; set; }
        [BookmarkMapping("TotalOutputVAT")]
        public string QTaxReportTransSum_SumTaxReport { get; set; }
        public int QTaxReportTransSum_TaxReportCOUNT { get; set; }


        [BookmarkMapping("FinInstitutionFirst1")]
        public string QNCBTrans_Institution_1 { get; set; }
        [BookmarkMapping("LoanTypeFirst1")]
        public string QNCBTrans_LoanType_1 { get; set; }
        [BookmarkMapping("LoanLimitFirst1")]
        public string QNCBTrans_LoanLimit_1 { get; set; }


        [BookmarkMapping("FinInstitutionSecond1")]
        public string QNCBTrans_Institution_2 { get; set; }
        [BookmarkMapping("LoanTypeSecond1")]
        public string QNCBTrans_LoanType_2 { get; set; }
        [BookmarkMapping("LoanLimitSecond1")]
        public string QNCBTrans_LoanLimit_2 { get; set; }


        [BookmarkMapping("FinInstitutionThird1")]
        public string QNCBTrans_Institution_3 { get; set; }
        [BookmarkMapping("LoanTypeThird1")]
        public string QNCBTrans_LoanType_3 { get; set; }
        [BookmarkMapping("LoanLimitThird1")]
        public string QNCBTrans_LoanLimit_3 { get; set; }


        [BookmarkMapping("FinInstitutionFourth1")]
        public string QNCBTrans_Institution_4 { get; set; }
        [BookmarkMapping("LoanTypeFourth1")]
        public string QNCBTrans_LoanType_4 { get; set; }
        [BookmarkMapping("LoanLimitFourth1")]
        public string QNCBTrans_LoanLimit_4 { get; set; }


        [BookmarkMapping("FinInstitutionFifth1")]
        public string QNCBTrans_Institution_5 { get; set; }
        [BookmarkMapping("LoanTypeFifth1")]
        public string QNCBTrans_LoanType_5 { get; set; }
        [BookmarkMapping("LoanLimitFifth1")]
        public string QNCBTrans_LoanLimit_5 { get; set; }


        [BookmarkMapping("FinInstitutionSixth1")]
        public string QNCBTrans_Institution_6 { get; set; }
        [BookmarkMapping("LoanTypeSixth1")]
        public string QNCBTrans_LoanType_6 { get; set; }
        [BookmarkMapping("LoanLimitSixth1")]
        public string QNCBTrans_LoanLimit_6 { get; set; }


        [BookmarkMapping("FinInstitutionSeventh1")]
        public string QNCBTrans_Institution_7 { get; set; }
        [BookmarkMapping("LoanTypeSeventh1")]
        public string QNCBTrans_LoanType_7 { get; set; }
        [BookmarkMapping("LoanLimitSeventh1")]
        public string QNCBTrans_LoanLimit_7 { get; set; }


        [BookmarkMapping("FinInstitutionEighth1")]
        public string QNCBTrans_Institution_8 { get; set; }
        [BookmarkMapping("LoanTypeEighth1")]
        public string QNCBTrans_LoanType_8 { get; set; }
        [BookmarkMapping("LoanLimitEighth1")]
        public string QNCBTrans_LoanLimit_8 { get; set; }


        public string QNCBTransSum_LoanLimit { get; set; }
        [BookmarkMapping("TotalLoanLimit")]
        public string QNCBTransSum_TotalLoanLimit { get; set; }


        [BookmarkMapping("ApprovedCreditAsofDate")]
        public string QCAReqCreditOutStanding_AsOfDate { get; set; }
        public int QCAReqCreditOutStanding_ProductType { get; set; }
        public string QCAReqCreditOutStanding_CreditLimitTypeId { get; set; }
        public string QCAReqCreditOutStanding_CreditLimitTypeDesc { get; set; }
        public string QCAReqCreditOutStanding_ApprovedCreditLimit { get; set; }
        public string QCAReqCreditOutStanding_ARBalance { get; set; }
        public string QCAReqCreditOutStanding_CreditLimitBalance { get; set; }

        [BookmarkMapping("TotalCreditLimit")]
        public string QCAReqCreditOutStandingSum_SumApprovedCreditLimit { get; set; }
        [BookmarkMapping("TotalAROutstanding")]
        public string QCAReqCreditOutStandingSum_SumARBalance { get; set; }
        [BookmarkMapping("TotalCreditLimitRemaining")]
        public string QCAReqCreditOutStandingSum_SumCreditLimitBalance { get; set; }
        [BookmarkMapping("TotalReserveOutstanding")]
        public string QCAReqCreditOutStandingSum_SumReserveToBeRefund { get; set; }
        [BookmarkMapping("TotalRetentionOutstanding")]
        public string QCAReqCreditOutStandingSum_SumAccumRetentionAmount { get; set; }


        [BookmarkMapping("ApproverNameFirst1")]
        public string QActionHistory_ApproverName_1 { get; set; }
        [BookmarkMapping("ApproverReasonFirst1")]
        public string QActionHistory_Comment_1 { get; set; }
        [BookmarkMapping("ApprovedDateFirst1")]
        public string QActionHistory_CreatedDateTime_1 { get; set; }


        [BookmarkMapping("ApproverNameSecond1")]
        public string QActionHistory_ApproverName_2 { get; set; }
        [BookmarkMapping("ApproverReasonSecond1")]
        public string QActionHistory_Comment_2 { get; set; }
        [BookmarkMapping("ApprovedDateSecond1")]
        public string QActionHistory_CreatedDateTime_2 { get; set; }


        [BookmarkMapping("ApproverNameThird1")]
        public string QActionHistory_ApproverName_3 { get; set; }
        [BookmarkMapping("ApproverReasonThird1")]
        public string QActionHistory_Comment_3 { get; set; }
        [BookmarkMapping("ApprovedDateThird1")]
        public string QActionHistory_CreatedDateTime_3 { get; set; }


        [BookmarkMapping("ApproverNameFourth1")]
        public string QActionHistory_ApproverName_4 { get; set; }
        [BookmarkMapping("ApproverReasonFourth1")]
        public string QActionHistory_Comment_4 { get; set; }
        [BookmarkMapping("ApprovedDateFourth1")]
        public string QActionHistory_CreatedDateTime_4 { get; set; }


        [BookmarkMapping("ApproverNameFifth1")]
        public string QActionHistory_ApproverName_5 { get; set; }
        [BookmarkMapping("ApproverReasonFifth1")]
        public string QActionHistory_Comment_5 { get; set; }
        [BookmarkMapping("ApprovedDateFifth1")]
        public string QActionHistory_CreatedDateTime_5 { get; set; }





        public bool QCreditAppReqAssignment_IsNew_1 { get; set; }


        [BookmarkMapping("AssignmentBuyerFirst1")]
        public string QCreditAppReqAssignment_AssignmentBuyer_1 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdFirst1")]
        public string QCreditAppReqAssignment_RefAgreementId_1 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountFirst1")]
        public string QCreditAppReqAssignment_BuyerAgreementAmount_1 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountFirst1")]
        public string QCreditAppReqAssignment_AssignmentAgreementAmount_1 { get; set; }
        [BookmarkMapping("AssignmentAgreementDateFirst1")]
        public string QCreditAppReqAssignment_AssignmentAgreementDate_1 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountFirst1")]
        public string QCreditAppReqAssignment_RemainingAmount_1 { get; set; }


        public bool QCreditAppReqAssignment_IsNew_2 { get; set; }
        [BookmarkMapping("AssignmentBuyerSecond1")]
        public string QCreditAppReqAssignment_AssignmentBuyer_2 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdSecond1")]
        public string QCreditAppReqAssignment_RefAgreementId_2 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountSecond1")]
        public string QCreditAppReqAssignment_BuyerAgreementAmount_2 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountSecond1")]
        public string QCreditAppReqAssignment_AssignmentAgreementAmount_2 { get; set; }
        [BookmarkMapping("AssignmentAgreementDateSecond1")]
        public string QCreditAppReqAssignment_AssignmentAgreementDate_2 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountSecond1")]
        public string QCreditAppReqAssignment_RemainingAmount_2 { get; set; }


        public bool QCreditAppReqAssignment_IsNew_3 { get; set; }
        [BookmarkMapping("AssignmentBuyerThird1")]
        public string QCreditAppReqAssignment_AssignmentBuyer_3 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdThird1")]
        public string QCreditAppReqAssignment_RefAgreementId_3 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountThird1")]
        public string QCreditAppReqAssignment_BuyerAgreementAmount_3 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountThird1")]
        public string QCreditAppReqAssignment_AssignmentAgreementAmount_3 { get; set; }
        [BookmarkMapping("AssignmentAgreementDateThird1")]
        public string QCreditAppReqAssignment_AssignmentAgreementDate_3 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountThird1")]
        public string QCreditAppReqAssignment_RemainingAmount_3 { get; set; }


        public bool QCreditAppReqAssignment_IsNew_4 { get; set; }
        [BookmarkMapping("AssignmentBuyerFourth1")]
        public string QCreditAppReqAssignment_AssignmentBuyer_4 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdFourth1")]
        public string QCreditAppReqAssignment_RefAgreementId_4 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountFourth1")]
        public string QCreditAppReqAssignment_BuyerAgreementAmount_4 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountFourth1")]
        public string QCreditAppReqAssignment_AssignmentAgreementAmount_4 { get; set; }
        [BookmarkMapping("AssignmentAgreementDateFourth1")]
        public string QCreditAppReqAssignment_AssignmentAgreementDate_4 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountFourth1")]
        public string QCreditAppReqAssignment_RemainingAmount_4 { get; set; }


        public bool QCreditAppReqAssignment_IsNew_5 { get; set; }
        [BookmarkMapping("AssignmentBuyerFifth1")]
        public string QCreditAppReqAssignment_AssignmentBuyer_5 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdFifth1")]
        public string QCreditAppReqAssignment_RefAgreementId_5 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountFifth1")]
        public string QCreditAppReqAssignment_BuyerAgreementAmount_5 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountFifth1")]
        public string QCreditAppReqAssignment_AssignmentAgreementAmount_5 { get; set; }
        [BookmarkMapping("AssignmentAgreementDateFifth1")]
        public string QCreditAppReqAssignment_AssignmentAgreementDate_5 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountFifth1")]
        public string QCreditAppReqAssignment_RemainingAmount_5 { get; set; }


        public bool QCreditAppReqAssignment_IsNew_6 { get; set; }
        [BookmarkMapping("AssignmentBuyerSixth1")]
        public string QCreditAppReqAssignment_AssignmentBuyer_6 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdSixth1")]
        public string QCreditAppReqAssignment_RefAgreementId_6 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountSixth1")]
        public string QCreditAppReqAssignment_BuyerAgreementAmount_6 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountSixth1")]
        public string QCreditAppReqAssignment_AssignmentAgreementAmount_6 { get; set; }
        [BookmarkMapping("AssignmentAgreementDateSixth1")]
        public string QCreditAppReqAssignment_AssignmentAgreementDate_6 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountSixth1")]
        public string QCreditAppReqAssignment_RemainingAmount_6 { get; set; }


        public bool QCreditAppReqAssignment_IsNew_7 { get; set; }
        [BookmarkMapping("AssignmentBuyerSeventh1")]
        public string QCreditAppReqAssignment_AssignmentBuyer_7 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdSeventh1")]
        public string QCreditAppReqAssignment_RefAgreementId_7 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountSeventh1")]
        public string QCreditAppReqAssignment_BuyerAgreementAmount_7 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountSeventh1")]
        public string QCreditAppReqAssignment_AssignmentAgreementAmount_7 { get; set; }
        [BookmarkMapping("AssignmentAgreementDateSeventh1")]
        public string QCreditAppReqAssignment_AssignmentAgreementDate_7 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountSeventh1")]
        public string QCreditAppReqAssignment_RemainingAmount_7 { get; set; }


        public bool QCreditAppReqAssignment_IsNew_8 { get; set; }
        [BookmarkMapping("AssignmentBuyerEighth1")]
        public string QCreditAppReqAssignment_AssignmentBuyer_8 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdEighth1")]
        public string QCreditAppReqAssignment_RefAgreementId_8 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountEighth1")]
        public string QCreditAppReqAssignment_BuyerAgreementAmount_8 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountEighth1")]
        public string QCreditAppReqAssignment_AssignmentAgreementAmount_8 { get; set; }
        [BookmarkMapping("AssignmentAgreementDateEighth1")]
        public string QCreditAppReqAssignment_AssignmentAgreementDate_8 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountEighth1")]
        public string QCreditAppReqAssignment_RemainingAmount_8 { get; set; }


        public bool QCreditAppReqAssignment_IsNew_9 { get; set; }
        [BookmarkMapping("AssignmentBuyerNineth1")]
        public string QCreditAppReqAssignment_AssignmentBuyer_9 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdNineth1")]
        public string QCreditAppReqAssignment_RefAgreementId_9 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountNineth1")]
        public string QCreditAppReqAssignment_BuyerAgreementAmount_9 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountNineth1")]
        public string QCreditAppReqAssignment_AssignmentAgreementAmount_9 { get; set; }
        [BookmarkMapping("AssignmentAgreementDateNineth1")]
        public string QCreditAppReqAssignment_AssignmentAgreementDate_9 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountNineth1")]
        public string QCreditAppReqAssignment_RemainingAmount_9 { get; set; }


        public bool QCreditAppReqAssignment_IsNew_10 { get; set; }
        [BookmarkMapping("AssignmentBuyerTenth1")]
        public string QCreditAppReqAssignment_AssignmentBuyer_10 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdTenth1")]
        public string QCreditAppReqAssignment_RefAgreementId_10 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountTenth1")]
        public string QCreditAppReqAssignment_BuyerAgreementAmount_10 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountTenth1")]
        public string QCreditAppReqAssignment_AssignmentAgreementAmount_10 { get; set; }
        [BookmarkMapping("AssignmentAgreementDateTenth1")]
        public string QCreditAppReqAssignment_AssignmentAgreementDate_10 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountTenth1")]
        public string QCreditAppReqAssignment_RemainingAmount_10 { get; set; }

        [BookmarkMapping("SumAssignmentBuyerAgreementAmount")]
        public string QSumCreditAppReqAssignment_SumBuyerAgreementAmount { get; set; }
        [BookmarkMapping("SumAssignmentAgreementAmount")]
        public string QSumCreditAppReqAssignment_SumAssignmentAgreementAmount { get; set; }
        [BookmarkMapping("SumAssignmentRemainingAmount")]
        public string QSumCreditAppReqAssignment_SumSumRemainingAmount { get; set; }

        [BookmarkMapping("AssignmentNewFirst1")]
        public string QVariable_IsNew1 { get; set; }
        [BookmarkMapping("AssignmentNewSecond1")]
        public string QVariable_IsNew2 { get; set; }
        [BookmarkMapping("AssignmentNewThird1")]
        public string QVariable_IsNew3 { get; set; }
        [BookmarkMapping("AssignmentNewFourth1")]
        public string QVariable_IsNew4 { get; set; }
        [BookmarkMapping("AssignmentNewFifth1")]
        public string QVariable_IsNew5 { get; set; }
        [BookmarkMapping("AssignmentNewSixth1")]
        public string QVariable_IsNew6 { get; set; }
        [BookmarkMapping("AssignmentNewSeventh1")]
        public string QVariable_IsNew7 { get; set; }
        [BookmarkMapping("AssignmentNewEighth1")]
        public string QVariable_IsNew8 { get; set; }
        [BookmarkMapping("AssignmentNewNineth1")]
        public string QVariable_IsNew9 { get; set; }
        [BookmarkMapping("AssignmentNewTenth1")]
        public string QVariable_IsNew10 { get; set; }
        [BookmarkMapping("CustCompanyRegistrationID")]
        public string QVariable_CustCompanyRegistrationID { get; set; }
        [BookmarkMapping("CustComEstablishedDate")]
        public string QVariable_CustComEstablishedDate { get; set; }

        [BookmarkMapping("CustAuthorizedPersonAgeFirst1")]
        public int QVariable_AuthorizedPersonTrans1Age { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeSecond1")]
        public int QVariable_AuthorizedPersonTrans2Age { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeThird1")]
        public int QVariable_AuthorizedPersonTrans3Age { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeFourth1")]
        public int QVariable_AuthorizedPersonTrans4Age { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeFifth1")]
        public int QVariable_AuthorizedPersonTrans5Age { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeSixth1")]
        public int QVariable_AuthorizedPersonTrans6Age { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeSeventh1")]
        public int QVariable_AuthorizedPersonTrans7Age { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeEighth1")]
        public int QVariable_AuthorizedPersonTrans8Age { get; set; }
        [BookmarkMapping("AverageOutputVATPerMonth")]
        public string QVariable_AverageOutputVATPerMonth { get; set; }
        [BookmarkMapping("NewCreditLimitAmt")]
        public string QVariable_NewCreditLimitAmt { get; set; }

        [BookmarkMapping("CrditLimitAmtStart1")]
        public string QVariable_CrditLimitAmtStart1 { get; set; }
        [BookmarkMapping("AROutstandingAmtStart")]
        public string QVariable_AROutstandingAmtStart { get; set; }
        [BookmarkMapping("CreditLimitRemainingStart1")]
        public string QVariable_CreditLimitRemainingStart1 { get; set; }
        [BookmarkMapping("ReserveOutstandingStart1")]
        public string QVariable_ReserveOutstandingStart1 { get; set; }
        [BookmarkMapping("RetentionOutstandingStart1")]
        public string QVariable_RetentionOutstandingStart1 { get; set; }

        [BookmarkMapping("CrditLimitAmtFirst1")]
        public string QVariable_CrditLimitAmtFirst1 { get; set; }
        [BookmarkMapping("AROutstandingAmtFirst")]
        public string QVariable_AROutstandingAmtFirst { get; set; }
        [BookmarkMapping("CreditLimitRemainingFirst1")]
        public string QVariable_CreditLimitRemainingFirst1 { get; set; }
        [BookmarkMapping("ReserveOutstandingFirst1")]
        public string QVariable_ReserveOutstandingFirst1 { get; set; }
        [BookmarkMapping("RetentionOutstandingFirst1")]
        public string QVariable_RetentionOutstandingFirst1 { get; set; }

        [BookmarkMapping("CrditLimitAmtSecond1")]
        public string QVariable_CrditLimitAmtSecond1 { get; set; }
        [BookmarkMapping("AROutstandingAmtSecond")]
        public string QVariable_AROutstandingAmtSecond { get; set; }
        [BookmarkMapping("CreditLimitRemainingSecond1")]
        public string QVariable_CreditLimitRemainingSecond1 { get; set; }
        [BookmarkMapping("ReserveOutstandingSecond1")]
        public string QVariable_ReserveOutstandingSecond1 { get; set; }
        [BookmarkMapping("RetentionOutstandingSecond1")]
        public string QVariable_RetentionOutstandingSecond1 { get; set; }

        [BookmarkMapping("CrditLimitAmtThird1")]
        public string QVariable_CrditLimitAmtThird1 { get; set; }
        [BookmarkMapping("AROutstandingAmtThird")]
        public string QVariable_AROutstandingAmtThird { get; set; }
        [BookmarkMapping("CreditLimitRemainingThird1")]
        public string QVariable_CreditLimitRemainingThird1 { get; set; }
        [BookmarkMapping("ReserveOutstandingThird1")]
        public string QVariable_ReserveOutstandingThird1 { get; set; }
        [BookmarkMapping("RetentionOutstandingThird1")]
        public string QVariable_RetentionOutstandingThird1 { get; set; }

        [BookmarkMapping("CrditLimitAmtFourth1")]
        public string QVariable_CrditLimitAmtFourth1 { get; set; }
        [BookmarkMapping("AROutstandingAmtFourth")]
        public string QVariable_AROutstandingAmtFourth { get; set; }
        [BookmarkMapping("CreditLimitRemainingFourth1")]
        public string QVariable_CreditLimitRemainingFourth1 { get; set; }
        [BookmarkMapping("ReserveOutstandingFourth1")]
        public string QVariable_ReserveOutstandingFourth1 { get; set; }
        [BookmarkMapping("RetentionOutstandingFourth1")]
        public string QVariable_RetentionOutstandingFourth1 { get; set; }

        [BookmarkMapping("CrditLimitAmtFifth1")]
        public string QVariable_CrditLimitAmtFifth1 { get; set; }
        [BookmarkMapping("AROutstandingAmtFifth")]
        public string QVariable_AROutstandingAmtFifth { get; set; }
        [BookmarkMapping("CreditLimitRemainingFifth1")]
        public string QVariable_CreditLimitRemainingFifth1 { get; set; }
        [BookmarkMapping("ReserveOutstandingFifth1")]
        public string QVariable_ReserveOutstandingFifth1 { get; set; }
        [BookmarkMapping("RetentionOutstandingFifth1")]
        public string QVariable_RetentionOutstandingFifth1 { get; set; }

        [BookmarkMapping("AROutstandingAmtSixth")]
        public string QVariable_CrditLimitAmtSixth1 { get; set; }
        [BookmarkMapping("CreditLimitRemainingSixth")]
        public string QVariable_AROutstandingAmtSixth { get; set; }
        [BookmarkMapping("CreditLimitRemainingSixth1")]
        public string QVariable_CreditLimitRemainingSixth1 { get; set; }
        [BookmarkMapping("ReserveOutstandingSixth1")]
        public string QVariable_ReserveOutstandingSixth1 { get; set; }
        [BookmarkMapping("RetentionOutstandingSixth1")]
        public string QVariable_RetentionOutstandingSixth1 { get; set; }

        [BookmarkMapping("CrditLimitAmtSeventh1")]
        public string QVariable_CrditLimitAmtSeventh1 { get; set; }
        [BookmarkMapping("AROutstandingAmtSeventh")]
        public string QVariable_AROutstandingAmtSeventh { get; set; }
        [BookmarkMapping("CreditLimitRemainingSeventh1")]
        public string QVariable_CreditLimitRemainingSeventh1 { get; set; }
        [BookmarkMapping("ReserveOutstandingSeventh1")]
        public string QVariable_ReserveOutstandingSeventh1 { get; set; }
        [BookmarkMapping("RetentionOutstandingSeventh1")]
        public string QVariable_RetentionOutstandingSeventh1 { get; set; }

        [BookmarkMapping("CrditLimitAmtEighth1")]
        public string QVariable_CrditLimitAmtEighth1 { get; set; }
        [BookmarkMapping("AROutstandingAmtEighth")]
        public string QVariable_AROutstandingAmtEighth { get; set; }
        [BookmarkMapping("CreditLimitRemainingEighth1")]
        public string QVariable_CreditLimitRemainingEighth1 { get; set; }
        [BookmarkMapping("ReserveOutstandingEighth1")]
        public string QVariable_ReserveOutstandingEighth1 { get; set; }
        [BookmarkMapping("RetentionOutstandingEighth1")]
        public string QVariable_RetentionOutstandingEighth1 { get; set; }

        [BookmarkMapping("CrditLimitAmtNineth1")]
        public string QVariable_CrditLimitAmtNineth1 { get; set; }
        [BookmarkMapping("AROutstandingAmtNineth")]
        public string QVariable_AROutstandingAmtNineth { get; set; }
        [BookmarkMapping("CreditLimitRemainingNineth1")]
        public string QVariable_CreditLimitRemainingNineth1 { get; set; }
        [BookmarkMapping("ReserveOutstandingNineth1")]
        public string QVariable_ReserveOutstandingNineth1 { get; set; }
        [BookmarkMapping("RetentionOutstandingNineth1")]
        public string QVariable_RetentionOutstandingNineth1 { get; set; }

        [BookmarkMapping("CrditLimitAmtTenth1")]
        public string QVariable_CrditLimitAmtTenth1 { get; set; }
        [BookmarkMapping("AROutstandingAmtTenth")]
        public string QVariable_AROutstandingAmtTenth { get; set; }
        [BookmarkMapping("CreditLimitRemainingTenth1")]
        public string QVariable_CreditLimitRemainingTenth1 { get; set; }
        [BookmarkMapping("ReserveOutstandingTenth1")]
        public string QVariable_ReserveOutstandingTenth1 { get; set; }
        [BookmarkMapping("RetentionOutstandingTenth1")]
        public string QVariable_RetentionOutstandingTenth1 { get; set; }

        [BookmarkMapping("RetenDeductMethod01")]
        public string QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionDeductionMethod_Row_1 { get; set; }
        [BookmarkMapping("RetenCalBase01")]
        public string QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionCalculateBase_Row_1 { get; set; }
        [BookmarkMapping("RetenPerc01")]
        public decimal QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionPct_Row_1 { get; set; }
        [BookmarkMapping("RetenAmt01")]
        public decimal QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionAmount_Row_1 { get; set; }

        [BookmarkMapping("RetenDeductMethod02")]
        public string QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionDeductionMethod_Row_2 { get; set; }
        [BookmarkMapping("RetenCalBase02")]
        public string QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionCalculateBase_Row_2 { get; set; }
        [BookmarkMapping("RetenPerc02")]
        public decimal QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionPct_Row_2 { get; set; }
        [BookmarkMapping("RetenAmt02")]
        public decimal QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionAmount_Row_2 { get; set; }

        [BookmarkMapping("RetenDeductMethod03")]
        public string QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionDeductionMethod_Row_3 { get; set; }
        [BookmarkMapping("RetenCalBase03")]
        public string QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionCalculateBase_Row_3 { get; set; }
        [BookmarkMapping("RetenPerc03")]
        public decimal QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionPct_Row_3 { get; set; }
        [BookmarkMapping("RetenAmt03")]
        public decimal QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionAmount_Row_3 { get; set; }


    }
    public class QCreditAppRequestTableCrediLimit
    {
        public string QCreditAppRequestTable_CreditAppRequestId { get; set; }
        public Guid? QCreditAppRequestTable_RefCreditAppTableGUID { get; set; }
        public Guid? QCreditAppRequestTable_DocumentStatusGUID { get; set; }
        public DateTime? QCreditAppRequestTable_RequestDate { get; set; }
        public Guid? QCreditAppRequestTable_CustomerTableGUID { get; set; }
        public string QCreditAppRequestTable_Remark { get; set; }
        public int QCreditAppRequestTable_ProductType { get; set; }
        public Guid? QCreditAppRequestTable_ProductSubTypeGUID { get; set; }
        public Guid? QCreditAppRequestTable_CreditLimitTypeGUID { get; set; }
        public int QCreditAppRequestTable_CreditLimitExpiration { get; set; }
        public DateTime? QCreditAppRequestTable_StartDate { get; set; }
        public DateTime? QCreditAppRequestTable_ExpiryDate { get; set; }
        public decimal QCreditAppRequestTable_ApprovedCreditLimitRequest { get; set; }
        public decimal QCreditAppRequestTable_CreditLimitRequest { get; set; }
        public Guid? QCreditAppRequestTable_InterestTypeGUID { get; set; }
        public decimal QCreditAppRequestTable_InterestAdjustment { get; set; }
        public decimal QCreditAppRequestTable_TotalInterestPct { get; set; }
        public decimal QCreditAppRequestTable_MaxPurchasePct { get; set; }
        public decimal QCreditAppRequestTable_MaxRetentionPct { get; set; }
        public decimal QCreditAppRequestTable_MaxRetentionAmount { get; set; }
        public string QCreditAppRequestTable_CACondition { get; set; }
        public Guid? QCreditAppRequestTable_PDCBankGUID { get; set; }
        public decimal QCreditAppRequestTable_CreditRequestFeePct { get; set; }
        public decimal QCreditAppRequestTable_CreditRequestFeeAmount { get; set; }
        public decimal QCreditAppRequestTable_PurchaseFeePct { get; set; }
        public int QCreditAppRequestTable_PurchaseFeeCalculateBase { get; set; }
        public Guid? QCreditAppRequestTable_RegisteredAddressGUID { get; set; }
        public Guid? QCreditAppRequestTable_IntroducedByGUID { get; set; }
        public decimal QCreditAppRequestTable_SalesAvgPerMonth { get; set; }
        public string QCreditAppRequestTable_MarketingComment { get; set; }
        public string QCreditAppRequestTable_CreditComment { get; set; }
        public string QCreditAppRequestTable_ApproverComment { get; set; }
        public decimal QCreditAppRequestTable_CustomerCreditLimit { get; set; }
        public DateTime? QCreditAppRequestTable_NCBCheckedDate { get; set; }
        public string QCreditAppRequestTable_Description { get; set; }
        public string QCreditAppRequestTable_OriginalCreditAppTableId { get; set; }
        public decimal QCreditAppRequestTable_TotalInterestCA { get; set; }

        public string DocumentStatus_Status { get; set; }
        public string KYCSetup_KYC { get; set; }
        public string CreditScoring_CreditScoring { get; set; }
        public string ProductSubType_ProductSubType { get; set; }
        public string CreditLimitType_CreditLimitType { get; set; }
        public string InterestType_InterestType { get; set; }
        public string AddressTrans_RegisteredAddress { get; set; }
        public string BusinessType_BusinessType { get; set; }
        public string IntroducedBy_IntroducedBy { get; set; }

        // Customer
        public string QCustomer_CustomerId { get; set; }
        public string QCustomer_CustomerName { get; set; }
        public Guid? QCustomer_ResponsibleByGUID { get; set; }
        public int QCustomer_IdentificationType { get; set; }
        public string QCustomer_TaxID { get; set; }
        public string QCustomer_PassportID { get; set; }
        public int QCustomer_RecordType { get; set; }
        public DateTime? QCustomer_DateOfEstablish { get; set; }
        public DateTime? QCustomer_DateOfBirth { get; set; }
        public Guid? QCustomer_BusinessTypeGUID { get; set; }
        public Guid? QCustomer_IntroducedByGUID { get; set; }
        public string QCustomer_ResponsibleBy { get; set; }

        //QOriginalCreditAppRequest
        public string QOriginalCreditAppRequest_OriginalCreditAppRequestId { get; set; }
        public decimal QOriginalCreditAppRequest_ApprovedCreditLimitRequest { get; set; }
        public decimal QOriginalCreditAppRequest_CreditLimitRequest { get; set; }

        // QBankAccountControl
        public string QBankAccountControl_Instituetion { get; set; }
        public string QBankAccountControl_BankBranch { get; set; }
        public string QBankAccountControl_AccountType { get; set; }
        public string QBankAccountControl_BankAccountName { get; set; }

        // QPDCBank
        public string QPDCBank_CustPDCBankName { get; set; }
        public string QPDCBank_CustPDCBankBranch { get; set; }
        public string QPDCBank_CustPDCBankType { get; set; }
        public string QPDCBank_CustPDCBankAccountName { get; set; }
    }

    public class QOriginalCreditAppRequestCrediLimit
    {
        public string QOriginalCreditAppRequest_OriginalCreditAppRequestId { get; set; }
        public string QOriginalCreditAppRequest_ApprovedCreditLimitRequest { get; set; }
        public string QOriginalCreditAppRequest_CreditLimitRequest { get; set; }
    }
    public class QCustomerCrediLimit
    {
        public string QCustomer_CustomerId { get; set; }
        public string QCustomer_CustomerName { get; set; }
        public Guid? QCustomer_ResponsibleByGUID { get; set; }
        public int QCustomer_IdentificationType { get; set; }
        public string QCustomer_TaxID { get; set; }
        public string QCustomer_PassportID { get; set; }
        public int QCustomer_RecordType { get; set; }
        public DateTime? QCustomer_DateOfEstablish { get; set; }
        public DateTime? QCustomer_DateOfBirth { get; set; }
        public Guid? QCustomer_BusinessTypeGUID { get; set; }
        public Guid? QCustomer_IntroducedByGUID { get; set; }
        public string QCustomer_ResponsibleBy { get; set; }

    }
    public class QPDCBankCrediLimit
    {
        public string QPDCBank_CustPDCBankName { get; set; }
        public string QPDCBank_CustPDCBankBranch { get; set; }
        public string QPDCBank_CustPDCBankType { get; set; }
        public string QPDCBank_CustPDCBankAccountName { get; set; }
    }
    public class QServiceFeeConditionTransCrediLimit
    {
        public string QServiceFeeConditionTrans_Description { get; set; }
        public decimal QServiceFeeConditionTrans_AmountBeforeTax { get; set; }
        public string QServiceFeeConditionTrans_ServicefeeTypeId { get; set; }
        public string QServiceFeeConditionTrans_ServiceFeeDesc { get; set; }
        public int QServiceFeeConditionTrans_Row { get; set; }
    }
    public class QSumServiceFeeConditionTransCrediLimit
    {
        public decimal QSumServiceFeeConditionTrans_ServiceFeeAmt { get; set; }

    }
    public class QGuarantorTransCrediLimit
    {
        public string QGuarantorTrans_RelatedPersonName { get; set; }
        public string QGuarantorTrans_GuarantorType { get; set; }
        public int QGuarantorTrans_Row { get; set; }
    }
    public class QCreditAppReqBusinessCollateralCrediLimit
    {
        public string QCreditAppReqBusinessCollateral_IsNew { get; set; }
        public string QCreditAppReqBusinessCollateral_BusinessCollateralType { get; set; }
        public string QCreditAppReqBusinessCollateral_BusinessCollateralSubType { get; set; }
        public string QCreditAppReqBusinessCollateral_Description { get; set; }
        public decimal QCreditAppReqBusinessCollateral_BusinessCollateralValue { get; set; }
        public int QCreditAppReqBusinessCollateral_Row { get; set; }
        public DateTime QCreditAppReqBusinessCollateral_CreateDateTime { get; set; }
    }
    public class QSumCreditAppReqBusinessCollateralCrediLimit
    {
        public decimal QSumCreditAppReqBusinessCollateral_SumBusinessCollateralValue { get; set; }
    }
    public class QBankAccountControlCrediLimit
    {
        public string QBankAccountControl_Instituetion { get; set; }
        public string QBankAccountControl_BankBranch { get; set; }
        public string QBankAccountControl_AccountType { get; set; }
        public string QBankAccountControl_BankAccountName { get; set; }
    }
    public class QAuthorizedPersonTransCrediLimit
    {
        public string QAuthorizedPersonTrans_AuthorizedPersonName { get; set; }
        public DateTime? QAuthorizedPersonTrans_DateOfBirth { get; set; }
        public int QAuthorizedPersonTrans_Row { get; set; }
    }
    public class QOwnerTransCrediLimit
    {
        public string QOwnerTrans_ShareHolderPersonName { get; set; }
        public decimal QOwnerTrans_PropotionOfShareholderPct { get; set; }
        public int QOwnerTrans_Row { get; set; }
    }
    public class QCreditAppReqLineFinCrediLimit
    {
        public int QCreditAppReqLineFin_Year { get; set; }
        public int QCreditAppReqLineFin_Row { get; set; }
        public decimal QCreditAppReqLineFin_RegisteredCapital { get; set; }
        public decimal QCreditAppReqLineFin_PaidCapital { get; set; }
        public decimal QCreditAppReqLineFin_TotalAsset { get; set; }
        public decimal QCreditAppReqLineFin_TotalLiability { get; set; }
        public decimal QCreditAppReqLineFin_TotalEquity { get; set; }
        public decimal QCreditAppReqLineFin_TotalRevenue { get; set; }
        public decimal QCreditAppReqLineFin_TotalCOGS { get; set; }
        public decimal QCreditAppReqLineFin_TotalGrossProfit { get; set; }
        public decimal QCreditAppReqLineFin_TotalOperExpFirst { get; set; }
        public decimal QCreditAppReqLineFin_TotalNetProfitFirst { get; set; }
        public decimal QCreditAppReqLineFin_NetProfitPercent { get; set; }
        public decimal QCreditAppReqLineFin_lDE { get; set; }
        public decimal QCreditAppReqLineFin_QuickRatio { get; set; }
        public decimal QCreditAppReqLineFin_IntCoverageRatio { get; set; }
    }
    public class QTaxReportTransCrediLimit
    {
        public string QTaxReportTrans_TaxMonth { get; set; }
        public decimal QTaxReportTrans_Amount { get; set; }
        public int QTaxReportTrans_Row { get; set; }
    }
    public class QTaxReportStartCrediLimit
    {
        public string QTaxReportStart_TaxMonth { get; set; }
    }
    public class QTaxReportEndCrediLimit
    {
        public string QTaxReportEnd_TaxMonth { get; set; }
    }
    public class QTaxReportTransSumCrediLimit
    {
        public decimal QTaxReportTransSum_SumTaxReport { get; set; }
        public int QTaxReportTransSum_TaxReportCOUNT { get; set; }
    }
    public class QNCBTransCrediLimit
    {
        public string QNCBTrans_Institution { get; set; }
        public string QNCBTrans_LoanType { get; set; }
        public decimal QNCBTrans_LoanLimit { get; set; }
        public int QNCBTrans_Row { get; set; }
    }
    public class QNCBTransSumCrediLimit
    {
        public decimal QNCBTransSum_LoanLimit { get; set; }
        public decimal QNCBTransSum_TotalLoanLimit { get; set; }
    }
    public class QCAReqCreditOutStandingCrediLimit
    {
        public DateTime? QCAReqCreditOutStanding_AsOfDate { get; set; }
        public int QCAReqCreditOutStanding_ProductType { get; set; }
        public string QCAReqCreditOutStanding_CreditLimitTypeId { get; set; }
        public string QCAReqCreditOutStanding_CreditLimitTypeDesc { get; set; }
        public decimal QCAReqCreditOutStanding_ApprovedCreditLimit { get; set; }
        public decimal QCAReqCreditOutStanding_ARBalance { get; set; }
        public decimal QCAReqCreditOutStanding_CreditLimitBalance { get; set; }
        public decimal QCAReqCreditOutStanding_ReserveToBeRefund { get; set; }
        public decimal QCAReqCreditOutStanding_AccumRetentionAmount { get; set; }
        public int QCAReqCreditOutStanding_Row { get; set; }
    }
    public class QCAReqCreditOutStandingSumCrediLimit
    {
        public decimal QCAReqCreditOutStandingSum_SumApprovedCreditLimit { get; set; }
        public decimal QCAReqCreditOutStandingSum_SumARBalance { get; set; }
        public decimal QCAReqCreditOutStandingSum_SumCreditLimitBalance { get; set; }
        public decimal QCAReqCreditOutStandingSum_SumReserveToBeRefund { get; set; }
        public decimal QCAReqCreditOutStandingSum_SumAccumRetentionAmount { get; set; }
    }
    public class QActionHistoryCrediLimit
    {
        public string QActionHistory_ApproverName { get; set; }
        public string QActionHistory_Comment { get; set; }
        public DateTime? QActionHistory_CreatedDateTime { get; set; }
        public int QActionHistory_Row { get; set; }
    }
    public class QCreditAppReqAssignmentCrediLimit
    {
        public bool QCreditAppReqAssignment_IsNew { get; set; }
        public string QCreditAppReqAssignment_AssignmentBuyer { get; set; }
        public string QCreditAppReqAssignment_RefAgreementId { get; set; }
        public decimal QCreditAppReqAssignment_BuyerAgreementAmount { get; set; }
        public decimal QCreditAppReqAssignment_AssignmentAgreementAmount { get; set; }
        public DateTime? QCreditAppReqAssignment_AssignmentAgreementDate { get; set; }
        public decimal QCreditAppReqAssignment_RemainingAmount { get; set; }
        public int QCreditAppReqAssignment_Row { get; set; }
    }
    public class QSumCreditAppReqAssignmentCrediLimit
    {
        public decimal QSumCreditAppReqAssignment_SumBuyerAgreementAmount { get; set; }
        public decimal QSumCreditAppReqAssignment_SumAssignmentAgreementAmount { get; set; }
        public decimal QSumCreditAppReqAssignment_SumSumRemainingAmount { get; set; }
    }
    public class QRetentionConditionTransCreditAppReqAssignmentCrediLimit
    {
        public decimal? QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionDeductionMethod { get; set; }
        public decimal? QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionCalculateBase { get; set; }
        public decimal QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionPct { get; set; }
        public decimal QRetentionConditionTransCreditAppReqAssignmentCrediLimit_RetentionAmount { get; set; }
    }
    public class QVariableCrediLimit
    {
        public string QVariable_IsNew1 { get; set; }
        public string QVariable_IsNew2 { get; set; }
        public string QVariable_IsNew3 { get; set; }
        public string QVariable_IsNew4 { get; set; }
        public string QVariable_IsNew5 { get; set; }
        public string QVariable_IsNew6 { get; set; }
        public string QVariable_IsNew7 { get; set; }
        public string QVariable_IsNew8 { get; set; }
        public string QVariable_IsNew9 { get; set; }
        public string QVariable_IsNew10 { get; set; }

        public string QVariable_CustCompanyRegistrationID { get; set; }
        public string QVariable_CustComEstablishedDate { get; set; }

        public string QVariable_AuthorizedPersonTrans1Age { get; set; }
        public string QVariable_AuthorizedPersonTrans2Age { get; set; }
        public string QVariable_AuthorizedPersonTrans3Age { get; set; }
        public string QVariable_AuthorizedPersonTrans4Age { get; set; }
        public string QVariable_AuthorizedPersonTrans5Age { get; set; }
        public string QVariable_AuthorizedPersonTrans6Age { get; set; }
        public string QVariable_AuthorizedPersonTrans7Age { get; set; }
        public string QVariable_AuthorizedPersonTrans8Age { get; set; }

        public string QVariable_AverageOutputVATPerMonth { get; set; }
        public string QVariable_NewCreditLimitAmt { get; set; }

        public string QVariable_CrditLimitAmtFirst1 { get; set; }
        public string QVariable_AROutstandingAmtFirst { get; set; }
        public string QVariable_CreditLimitRemainingFirst1 { get; set; }
        public string QVariable_ReserveOutstandingFirst1 { get; set; }
        public string QVariable_RetentionOutstandingFirst1 { get; set; }
        public string QVariable_CrditLimitAmtSecond1 { get; set; }
        public string QVariable_AROutstandingAmtSecond { get; set; }
        public string QVariable_CreditLimitRemainingSecond1 { get; set; }
        public string QVariable_ReserveOutstandingSecond1 { get; set; }
        public string QVariable_RetentionOutstandingSecond1 { get; set; }
        public string QVariable_CrditLimitAmtThird1 { get; set; }
        public string QVariable_AROutstandingAmtThird { get; set; }
        public string QVariable_CreditLimitRemainingThird1 { get; set; }
        public string QVariable_ReserveOutstandingThird1 { get; set; }
        public string QVariable_RetentionOutstandingThird1 { get; set; }

        public string QVariable_CrditLimitAmtFourth1 { get; set; }
        public string QVariable_AROutstandingAmtFourth { get; set; }
        public string QVariable_CreditLimitRemainingFourth1 { get; set; }
        public string QVariable_ReserveOutstandingFourth1 { get; set; }
        public string QVariable_RetentionOutstandingFourth1 { get; set; }


        public string QVariable_CrditLimitAmtFifth1 { get; set; }
        public string QVariable_AROutstandingAmtFifth { get; set; }
        public string QVariable_CreditLimitRemainingFifth1 { get; set; }
        public string QVariable_ReserveOutstandingFifth1 { get; set; }
        public string QVariable_RetentionOutstandingFifth1 { get; set; }


        public string QVariable_CrditLimitAmtSixth1 { get; set; }
        public string QVariable_AROutstandingAmtSixth { get; set; }
        public string QVariable_CreditLimitRemainingSixth1 { get; set; }
        public string QVariable_ReserveOutstandingSixth1 { get; set; }
        public string QVariable_RetentionOutstandingSixth1 { get; set; }


        public string QVariable_CrditLimitAmtSeventh1 { get; set; }
        public string QVariable_AROutstandingAmtSeventh { get; set; }
        public string QVariable_CreditLimitRemainingSeventh1 { get; set; }
        public string QVariable_ReserveOutstandingSeventh1 { get; set; }
        public string QVariable_RetentionOutstandingSeventh1 { get; set; }


        public string QVariable_CrditLimitAmtEighth1 { get; set; }
        public string QVariable_AROutstandingAmtEighth { get; set; }
        public string QVariable_CreditLimitRemainingEighth1 { get; set; }
        public string QVariable_ReserveOutstandingEighth1 { get; set; }
        public string QVariable_RetentionOutstandingEighth1 { get; set; }

        public string QVariable_CrditLimitAmtNineth1 { get; set; }
        public string QVariable_AROutstandingAmtNineth { get; set; }
        public string QVariable_CreditLimitRemainingNineth1 { get; set; }
        public string QVariable_ReserveOutstandingNineth1 { get; set; }
        public string QVariable_RetentionOutstandingNineth1 { get; set; }


        public string QVariable_CrditLimitAmtTenth1 { get; set; }
        public string QVariable_AROutstandingAmtTenth { get; set; }
        public string QVariable_CreditLimitRemainingTenth1 { get; set; }
        public string QVariable_ReserveOutstandingTenth1 { get; set; }
        public string QVariable_RetentionOutstandingTenth1 { get; set; }
    }
}
