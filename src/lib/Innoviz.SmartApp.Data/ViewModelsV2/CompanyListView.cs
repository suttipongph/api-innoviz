using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CompanyListView : ViewCompanyBaseEntity
	{
        public string Name { get; set; }
        public string SecondName { get; set; }
        public string AltName { get; set; }
        public string DefaultBranchGUID { get; set; }
        public string Branch_Values { get; set; }
        public string Branch_BranchId { get; set; }
        public string TaxId { get; set; }
    }
}
