using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class GenderItemView : ViewCompanyBaseEntity
	{
		public string GenderGUID { get; set; }
		public string Description { get; set; }
		public string GenderId { get; set; }
	}
}
