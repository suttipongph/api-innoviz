using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ReceiptTempTableListView : ViewCompanyBaseEntity
	{
		public string ReceiptTempTableGUID { get; set; }
		public string ReceiptTempId { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string ReceiptDate { get; set; }
		public string TransDate { get; set; }
		public int ProductType { get; set; }
		public int ReceiptTempRefType { get; set; }
		public int ReceivedFrom { get; set; }
		public string CustomerTableGUID { get; set; }
		public string BuyerTableGUID { get; set; }
		public decimal ReceiptAmount { get; set; }
		public string CurrencyGUID { get; set; }
		public string CustomerTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string Currency_Values { get; set; }

		public string CustomerTable_CustomerId { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string Currency_CurrencyId { get; set; }
		public string DocumentStatus_StatusId { get; set; }
	}
}
