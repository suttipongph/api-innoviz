﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
   public class CopyBuyerAgreementTransView
    {
        public string CustomerTableGUID { get; set; }
        public string BuyerTableGUID { get; set; }
        public string BuyerAgreementTableGUID { get; set; }
        public int RefType { get; set; }
        public string RefGUID { get; set; }        
        public string[] ResultLabel { get; set; }
    }
}
