using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CAReqAssignmentOutstandingItemView : ViewCompanyBaseEntity
	{
		public string CAReqAssignmentOutstandingGUID { get; set; }
		public decimal AssignmentAgreementAmount { get; set; }
		public string AssignmentAgreementTableGUID { get; set; }
		public string BuyerTableGUID { get; set; }
		public string CreditAppRequestTableGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public decimal RemainingAmount { get; set; }
		public decimal SettleAmount { get; set; }
	}
}
