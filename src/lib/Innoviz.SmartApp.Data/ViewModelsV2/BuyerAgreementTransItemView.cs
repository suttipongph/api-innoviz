using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BuyerAgreementTransItemView : ViewCompanyBaseEntity
	{
		public string BuyerAgreementTransGUID { get; set; }
		public string BuyerAgreementLineGUID { get; set; }
		public string BuyerAgreementTableGUID { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public string RefId { get; set; }
		public string BuyerAgreementLine_DueDate { get; set; }
		public decimal BuyerAgreementLine_Amount { get; set; }
		public string BuyerAgreementTable_BuyerAgreementId { get; set; }
		public string BuyerAgreementLine_Description { get; set; }
	}
}
