using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CustomerCreditLimitByProductListView : ViewCompanyBaseEntity
	{
		public string CustomerCreditLimitByProductGUID { get; set; }
		public int ProductType { get; set; }
		public decimal CreditLimit { get; set; }
		public string CustomerTableGUID { get; set; }
		public decimal CreditLimitBalance { get; set; }
		
	}
}
