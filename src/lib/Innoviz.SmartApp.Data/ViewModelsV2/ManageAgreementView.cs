﻿using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class ManageAgreementView : ResultBaseEntity
    {
        public string ManageAgreementGUID { get; set; }
        public string AgreementId { get; set; }
        public string DocumentStatusGUID { get; set; }
        public int DocumentProcessStatus { get; set; }
        public int AgreementDocType { get; set; }
        public int ManageAgreementAction { get; set; }
        public string Label { get; set; }
        public int RefType { get; set; }
        public string RefGUID { get; set; }
        public string DocumentReasonGUID { get; set; }
        public string ReasonRemark { get; set; }
        public string SigningDate { get; set; }
        public int ProductType { get; set; }
        public string InternalAgreementId { get; set; }
        public string Description { get; set; }
        public bool Affiliate { get; set; }
        public string AgreementDate { get; set; }
        public decimal Amount { get; set; }
        public string AssignmentMethodId { get; set; }
        public string BuyerId { get; set; }
        public string BuyerName { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string ExpiryDate { get; set; }
        public string CustomerTable_Values { get; set; }
        public string BuyerTable_Values { get; set; }
    }

    public class AssignmentAgreementManageAgreementView
    {
        public AssignmentAgreementTable AssignmentAgreementTable { get; set; }
        public PostInvoiceResultView PostInvoiceResultView { get; set; }
        public List<IntercompanyInvoiceTable> IntercompanyInvoiceTable { get; set; } = new List<IntercompanyInvoiceTable>();
        public AssignmentAgreementTable RefAssignmentAgreementTable { get; set; }
    }
    public class MainAgreementManageAgreementView
    {
        public List<MainAgreementTable> MainAgreementTables { get; set; } = new List<MainAgreementTable>();
        public List<GuarantorAgreementTable> GuarantorAgreementTables { get; set; } = new List<GuarantorAgreementTable>();
        public PostInvoiceResultView PostInvoiceResultView { get; set; }
        public List<IntercompanyInvoiceTable> IntercompanyInvoiceTable { get; set; } = new List<IntercompanyInvoiceTable>();
    }
    public class BusinessCollateralAgreementManageAgreementView
    {
        public List<BusinessCollateralAgmTable> BusinessCollateralAgmTables { get; set; } = new List<BusinessCollateralAgmTable>();
        public List<CustBusinessCollateral> CreateCustBusinessCollaterals { get; set; } = new List<CustBusinessCollateral>();
        public List<CustBusinessCollateral> UpdateCustBusinessCollaterals { get; set; } = new List<CustBusinessCollateral>();
        public PostInvoiceResultView PostInvoiceResultView { get; set; }
        public List<IntercompanyInvoiceTable> IntercompanyInvoiceTable { get; set; } = new List<IntercompanyInvoiceTable>();
        public List<BusinessCollateralAgmLine> BusinessCollateralAgmLines { get; set; } = new List<BusinessCollateralAgmLine>();
    } 
    public class GuarantorAgreementManageAgreementView
    {
        public List<GuarantorAgreementTable> GuarantorAgreementTables { get; set; } = new List<GuarantorAgreementTable>();
        public PostInvoiceResultView PostInvoiceResultView { get; set; }
    }

    public class ManageAssignAgreementValidationResult
    {
        public bool IsValid { get; set; }
        public bool HasLine { get; set; }
        public int RefType { get; set; }
        public string RefGUID { get; set; }
    }
}
