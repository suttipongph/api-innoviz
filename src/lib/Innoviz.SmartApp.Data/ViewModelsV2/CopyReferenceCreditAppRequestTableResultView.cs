﻿using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class CopyReferenceCreditAppRequestTableResultView
    {
        public List<AuthorizedPersonTrans> AuthorizedPersonTrans { get; set; }
        public List<GuarantorTrans> GuarantorTrans { get; set; }
        public List<OwnerTrans> OwnerTrans { get; set; }
        public List<CAReqRetentionOutstanding> CAReqRetentionOutstandings { get; set; }
        public List<CAReqCreditOutStanding> CAReqCreditOutStandings { get; set; }
        public List<CAReqAssignmentOutstanding> CAReqAssignmentOutstandings { get; set; }
        public List<CAReqBuyerCreditOutstanding> CAReqBuyerCreditOutstandings { get; set; }
    }
}
