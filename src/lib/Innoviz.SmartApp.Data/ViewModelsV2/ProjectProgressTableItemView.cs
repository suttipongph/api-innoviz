using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ProjectProgressTableItemView : ViewCompanyBaseEntity
	{
		public string ProjectProgressTableGUID { get; set; }
		public string Description { get; set; }
		public string ProjectProgressId { get; set; }
		public string Remark { get; set; }
		public string TransDate { get; set; }
		public string WithdrawalTableGUID { get; set; }
		public string RefId { get; set; }
		public string Withdrawal_Value { get; set; }
	}
}
