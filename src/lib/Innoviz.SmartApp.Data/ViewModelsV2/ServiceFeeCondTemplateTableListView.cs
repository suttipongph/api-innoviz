using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ServiceFeeCondTemplateTableListView : ViewCompanyBaseEntity
	{
		public string ServiceFeeCondTemplateTableGUID { get; set; }
		public int ProductType { get; set; }
		public string ServiceFeeCondTemplateId { get; set; }
		public string Description { get; set; }
	}
}
