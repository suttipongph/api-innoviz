﻿using Innoviz.SmartApp.Core.ViewModels;
using NJsonSchema.Annotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    [JsonSchemaFlatten]
    public class AttachmentItemView : ViewBaseEntity
    {
        public FileInformation FileInfo { get; set; }
        public string AttachmentGUID { get; set; }
        public int RefType { get; set; }
        public string RefGUID { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileDescription { get; set; }
        public string ContentType { get; set; }
        public string Base64Data { get; set; }
        public string RefId { get; set; }
        public int RefId_Int { get; set; }
        public string RefId_DateTime { get; set; }
    }
}
