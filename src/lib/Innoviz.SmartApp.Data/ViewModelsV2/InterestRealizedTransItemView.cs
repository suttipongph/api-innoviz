using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class InterestRealizedTransItemView : ViewCompanyBaseEntity
	{
		public string InterestRealizedTransGUID { get; set; }
		public decimal AccInterestAmount { get; set; }
		public string AccountingDate { get; set; }
		public bool Accrued { get; set; }
		public string DocumentId { get; set; }
		public string EndDate { get; set; }
		public int InterestDay { get; set; }
		public decimal InterestPerDay { get; set; }
		public int LineNum { get; set; }
		public int ProductType { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public string StartDate { get; set; }
		public string RefID { get; set; }
		public bool Cancelled { get; set; }
		public string RefProcessTransGUID { get; set; }
		public string ProcessTrans_Value { get; set; }
		public string Dimension1GUID { get; set; }
		public string Dimension2GUID { get; set; }
		public string Dimension3GUID { get; set; }
		public string Dimension4GUID { get; set; }
		public string Dimension5GUID { get; set; }
		public string Dimension1_Values { get; set; }
		public string Dimension2_Values { get; set; }
		public string Dimension3_Values { get; set; }
		public string Dimension4_Values { get; set; }
		public string Dimension5_Values { get; set; }
		public string ProcessTrans_TransDate { get; set; }
	}
}
