using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class KYCSetupListView : ViewCompanyBaseEntity
	{
		public string KYCSetupGUID { get; set; }
		public string KYCId { get; set; }
		public string Description { get; set; }
	}
}
