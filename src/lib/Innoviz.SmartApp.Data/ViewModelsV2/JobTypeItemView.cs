using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class JobTypeItemView : ViewCompanyBaseEntity
	{
		public string JobTypeGUID { get; set; }
		public string Description { get; set; }
		public string JobTypeId { get; set; }
		public int ShowDocConVerifyType { get; set; }
		public bool Assignment { get; set; }

	}
}
