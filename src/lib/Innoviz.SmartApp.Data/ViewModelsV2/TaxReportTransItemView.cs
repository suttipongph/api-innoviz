using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class TaxReportTransItemView : ViewCompanyBaseEntity
	{
		public string TaxReportTransGUID { get; set; }
		public decimal Amount { get; set; }
		public string Description { get; set; }
		public int Month { get; set; }
		public string RefGUID { get; set; }
		public string RefId { get; set; }
		public int RefType { get; set; }
		public int Year { get; set; }
	}
}
