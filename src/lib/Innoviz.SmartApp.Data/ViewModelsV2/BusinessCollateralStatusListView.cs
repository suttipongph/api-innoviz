using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BusinessCollateralStatusListView : ViewCompanyBaseEntity
	{
		public string BusinessCollateralStatusGUID { get; set; }
		public string BusinessCollateralStatusId { get; set; }
		public string Description { get; set; }
	}
}
