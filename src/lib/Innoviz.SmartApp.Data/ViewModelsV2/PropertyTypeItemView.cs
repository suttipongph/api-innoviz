using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class PropertyTypeItemView : ViewCompanyBaseEntity
	{
		public string PropertyTypeGUID { get; set; }
		public string Description { get; set; }
		public string PropertyTypeId { get; set; }
	}
}
