using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CustBankListView : ViewCompanyBaseEntity
	{
		public string CustBankGUID { get; set; }
		public string BankGroupGUID { get; set; }
		public string BankTypeGUID { get; set; }
		public string AccountNumber { get; set; }
		public string BankAccountName { get; set; }
		public bool BankAccountControl { get; set; }
		public bool PDC { get; set; }
		public bool Primary { get; set; }
		public bool InActive { get; set; }
		public string BankGroup_Values { get; set; }
		public string BankType_Values { get; set; }
		public string BankGroup_BankGroupId { get; set; }
		public string BankType_BankTypeId { get; set; }
		public string CustomerTableGUID { get; set; }
	}
}
