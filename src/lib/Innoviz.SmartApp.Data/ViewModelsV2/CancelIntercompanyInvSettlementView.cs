﻿using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class CancelIntercompanyInvSettlementView
    {
        public string CancelDate { get; set; }
        public string CNReasonGUID { get; set; }
        public string DocumentStatus_Values { get; set; }
        public string IntercompanyInvoice_Values { get; set; }
        public string IntercompanyInvoiceSettlementGUID { get; set; }
        public string IntercompanyInvoiceSettlementId { get; set; }
        public string MethodOfPayment_Values { get; set; }
        public decimal OrigInvoiceAmount { get; set; }
        public string OrigInvoiceId { get; set; }
        public decimal OrigTaxInvoiceAmount { get; set; }
        public string OrigTaxInvoiceId { get; set; }
        public decimal SettleAmount { get; set; }
        public decimal SettleInvoiceAmount { get; set; }
        public decimal SettleWHTAmount { get; set; }
    }
    public class CancelIntercompanyInvSettlementResultView : ResultBaseEntity
    {
    }
}

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class CancelIntercompanyInvSettlementViewMap
    {
        public StagingTableIntercoInvSettle StagingTableIntercoInvSettle { get; set; }
        public IntercompanyInvoiceSettlement CreateIntercompanyInvoiceSettlement { get; set; }
        public IntercompanyInvoiceSettlement UpdateIntercompanyInvoiceSettlement { get; set; }
    }
}
