﻿using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class PostReceiptTempParamView
    {
        public string ReceiptTempTableGUID { get; set; }
        public string ReceiptTempTable_Values { get; set; }
        public string ReceiptDate { get; set; }
        public string TransDate { get; set; }
        public int ProductType { get; set; }
        public int ReceivedFrom { get; set; }
        public string CustomerTable_Values { get; set; }
        public string BuyerTable_Values { get; set; }
        public decimal ReceiptAmount { get; set; }
        public decimal SettleFeeAmount { get; set; }
        public decimal SettleAmount { get; set; }
        public string SuspenseInvoiceType_Values { get; set; }
        public decimal SuspenseAmount { get; set; }
        public string CreditAppTable_Values { get; set; }
    }
    public class PostReceiptTempResultView : ResultBaseEntity
    {

    }
}
namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class PostReceiptTempResultViewMap
    {
        public ReceiptTempTable ReceiptTempTable { get; set; }
        public List<ReceiptTempTable> NewReceiptTempTable { get; set; }
        public List<InvoiceTable> InvoiceTable { get; set; }
        public List<InvoiceLine> InvoiceLine { get; set; }
        public List<ProcessTrans> ProcessTrans { get; set; }
        public List<CustTrans> CustTransCreate { get; set; }
        public List<CustTrans> CustTransUpdate { get; set; }
        public List<CreditAppTrans> CreditAppTrans { get; set; }
        public List<TaxInvoiceTable> TaxInvoiceTable { get; set; }
        public List<TaxInvoiceLine> TaxInvoiceLine { get; set; }
        public List<RetentionTrans> RetentionTrans { get; set; }
        public List<InterestRealizedTrans> InterestRealizedTrans { get; set; }
        public BuyerReceiptTable BuyerReceiptTable { get; set; }
        public List<AssignmentAgreementSettle> AssignmentAgreementSettle { get; set; }
        public List<ReceiptTempPaymDetail> ReceiptTempPaymDetail { get; set; }
        public List<InvoiceSettlementDetail> InvoiceSettlementDetail { get; set; }
        public List<ReceiptTable> ReceiptTable { get; set; }
        public List<ReceiptLine> ReceiptLine { get; set; }
        public List<PaymentHistory> PaymentHistory { get; set; }
        public List<IntercompanyInvoiceTable> IntercompanyInvoiceTable { get; set; }
        // R07
        public List<ProductSettledTrans> ProductSettledTrans { get; set; }
    }
    public class PostReceiptTempParamViewMap
    {
        public ReceiptTempTable ReceiptTempTable { get; set; }
        public List<DocumentStatus> DocumentStatus { get; set; }
        public List<ReceiptTempPaymDetail> ReceiptTempPaymDetail { get; set; }
        public List<InvoiceSettlementDetail> InvoiceSettlementDetail { get; set; }
        public List<ServiceFeeTrans> ServiceFeeTrans { get; set; }
        public CompanyParameter CompanyParameter { get; set; }
        public List<InvoiceType> InvoiceType { get; set; }
        public List<CustTrans> CustTrans { get; set; }
        public List<InvoiceTable> InvoiceTable { get; set; }
        public Dictionary<Guid, List<CustTrans>> CustTransCalcPDCInterestOutstanding { get; set; }
        public List<ProductSettledTrans> ProductSettledTrans { get; set; }
        public List<WithdrawalLine> WithdrawalLine { get; set; }
        public List<PurchaseLine> PurchaseLine { get; set; }
        public Dictionary<Guid, decimal> SumInterestCalcAmount { get; set; }
        public NumberSeqTable ReceiptTempNumberSeqTable { get; set; }
        public NumberSeqTable BuyerReceiptNumberSeqTable { get; set; }
    }
    public class SettlePDCInterestOutstandingPostReceiptTempView
    {
        public List<InvoiceTable> InvoiceTablePDC { get; set; }
        public List<InvoiceLine> InvoiceLinePDC { get; set; }
        public List<CustTrans> CustTransPDC { get; set; }
        public List<InvoiceSettlementDetail> InvoiceSettlementDetailPDC { get; set; }
        public ReceiptTempTable ReceiptTempPDC { get; set; }
        public InvoiceTable InvoiceTableSumIntCalcAmount { get; set; }
        public InvoiceLine InvoiceLineSumIntCalcAmount { get; set; }
    }
}
