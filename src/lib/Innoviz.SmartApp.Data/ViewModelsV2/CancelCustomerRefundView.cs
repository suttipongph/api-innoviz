﻿using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class CancelCustomerRefundView
    {
        public string CustomerRefundTableGUID { get; set; }
        public string CustomerTable_Values { get; set; }
        public string CustomerRefundTable_Values { get; set; }
        public string DocumentReasonGUID { get; set; }
        public string DocumentStatus_Values { get; set; }
        public decimal NetAmount { get; set; }
        public decimal PaymentAmount { get; set; }
        public decimal RetentionAmount { get; set; }
        public decimal ServiceFeeAmount { get; set; }
        public decimal SettleAmount { get; set; }
        public decimal SuspenseAmount { get; set; }
        public string TransDate { get; set; }
    }
    public class CancelCustomerRefundResultView : ResultBaseEntity
    {
    }
}

