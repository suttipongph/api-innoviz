using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class TaxInvoiceTableItemView : ViewBranchCompanyBaseEntity
	{
		public string TaxInvoiceTableGUID { get; set; }
		public string CurrencyGUID { get; set; }
		public string CustomerName { get; set; }
		public string CustomerTableGUID { get; set; }
		public string Dimension1GUID { get; set; }
		public string Dimension2GUID { get; set; }
		public string Dimension3GUID { get; set; }
		public string Dimension4GUID { get; set; }
		public string Dimension5GUID { get; set; }
		public string DocumentId { get; set; }
		public string CNReasonGUID { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string DueDate { get; set; }
		public decimal ExchangeRate { get; set; }
		public string InvoiceAddress1 { get; set; }
		public string InvoiceAddress2 { get; set; }
		public decimal InvoiceAmount { get; set; }
		public decimal InvoiceAmountBeforeTax { get; set; }
		public decimal InvoiceAmountBeforeTaxMST { get; set; }
		public decimal InvoiceAmountMST { get; set; }
		public string InvoiceTableGUID { get; set; }
		public string InvoiceTypeGUID { get; set; }
		public string IssuedDate { get; set; }
		public string MailingInvoiceAddress1 { get; set; }
		public string MailingInvoiceAddress2 { get; set; }
		public string MethodOfPaymentGUID { get; set; }
		public decimal OrigTaxInvoiceAmount { get; set; }
		public string OrigTaxInvoiceId { get; set; }
		public string TaxInvoiceRefGUID { get; set; }
		public string RefTaxInvoiceGUID { get; set; }
		public int TaxInvoiceRefType { get; set; }
		public string Remark { get; set; }
		public decimal TaxAmount { get; set; }
		public decimal TaxAmountMST { get; set; }
		public string TaxBranchId { get; set; }
		public string TaxBranchName { get; set; }
		public string TaxInvoiceId { get; set; }
		public string TaxInvoiceTable_Values { get; set; }
		public string Currency_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string Dimension1_Values { get; set; }
		public string Dimension2_Values { get; set; }
		public string Dimension3_Values { get; set; }
		public string Dimension4_Values { get; set; }
		public string Dimension5_Values { get; set; }
		public string CNReason_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string InvoiceTable_Values { get; set; }
		public string InvoiceType_Values { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public string Ref_Values { get; set; }
		public string RefTaxInvoice_Values { get; set; }
		public string MailingInvoiceAddress_Values { get; set; }
		public string InvoiceAddress_Values { get; set; }
	}
}
