﻿using System;
using System.Collections.Generic;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Core.ViewModels
{
    public class InvoiceOutstandingListView : ViewBranchCompanyBaseEntity
    {
        public decimal BalanceAmount { get; set; }
        public string BuyerAgreementTableGUID { get; set; }
        public string BuyerInvoiceTableGUID { get; set; }
        public string BuyerTableGUID { get; set; }
        public string DocumentId { get; set; }
        public string DueDate { get; set; }
        public string RefGUID { get; set; }
        public int RefType { get; set; }
        public decimal InvoiceAmount { get; set; }
        public string InvoiceTableGUID { get; set; }
        public string InvoiceTypeGUID { get; set; }
        public bool ProductInvoice { get; set; }
        public int ProductType { get; set; }
        public string InvoiceTable_Values { get; set; }
        public string InvoiceTable_InvoiceId { get; set; }
        public string InvoiceTable_CustomerTableGUID { get; set; }
        public int SuspenseInvoiceType { get; set; }
        public string InvoiceType_Values { get; set; }
        public string InvoiceType_InvoiceTypeId { get; set; }
        public string BuyerInvoiceTable_Values { get; set; }
        public string BuyerInvoiceTable_BuyerInvoiceId { get; set; }
        public string BuyerAgreementTable_Values { get; set; }
        public string BuyerAgreementTable_BuyerAgreementId { get; set; }
        public string BuyerTable_Values { get; set; }
        public string BuyerTable_BuyerId { get; set; }
        public string CreditAppTable_Values { get; set; }
        public string Currency_Values { get; set; }
        public string RefReceiptTempPaymDetailGUID { get; set; }
        public decimal MaxSettleAmount { get; set; }
        public bool ExitsInvoice { get; set; }
    }
}
