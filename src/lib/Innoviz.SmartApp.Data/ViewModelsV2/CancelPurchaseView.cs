﻿using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class CancelPurchaseView
    {
        public string PurchaseTableGUID { get; set; }
        public string Purchase_Values { get; set; }
        public string CustomerTable_Values { get; set; }
        public string DocumentReasonGUID { get; set; }
        public string DocumentStatus_Values { get; set; }
        public string PurchaseDate { get; set; }
        public string ReasonRemark { get; set; }
        public bool AdditionalPurchase { get; set; }
        public bool Rollbill { get; set; }

    }
    public class CancelPurchaseResultView : ResultBaseEntity
    {
    }
}
