using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CollectionGroupItemView : ViewCompanyBaseEntity
	{
		public string CollectionGroupGUID { get; set; }
		public string AgreementBranchId { get; set; }
		public int AgreementFromOverdueDay { get; set; }
		public string AgreementLeaseTypeId { get; set; }
		public int AgreementToOverdueDay { get; set; }
		public string CollectionGroupId { get; set; }
		public string CustGroupId { get; set; }
		public string CustTerritoryId { get; set; }
		public string Description { get; set; }
	}
}
