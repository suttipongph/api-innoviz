using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class PurchaseLineItemView : ViewCompanyBaseEntity
	{
		public string PurchaseLineGUID { get; set; }
		public string AssignmentAgreementTableGUID { get; set; }
		public decimal AssignmentAmount { get; set; }
		public string BillingDate { get; set; }
		public decimal BillingFeeAmount { get; set; }
		public string BuyerAgreementTableGUID { get; set; }
		public decimal BuyerInvoiceAmount { get; set; }
		public string BuyerInvoiceTableGUID { get; set; }
		public string BuyerPDCTableGUID { get; set; }
		public string BuyerTableGUID { get; set; }
		public bool ClosedForAdditionalPurchase { get; set; }
		public string CollectionDate { get; set; }
		public string CreditAppLineGUID  { get; set; }
		public string CustomerPDCTableGUID { get; set; }
		public string Dimension1GUID { get; set; }
		public string Dimension2GUID { get; set; }
		public string Dimension3GUID { get; set; }
		public string Dimension4GUID { get; set; }
		public string Dimension5GUID { get; set; }
		public string DueDate { get; set; }
		public decimal InterestAmount { get; set; }
		public string InterestDate { get; set; }
		public int InterestDay { get; set; }
		public decimal InterestRefundAmount { get; set; }
		public int LineNum { get; set; }
		public decimal LinePurchaseAmount { get; set; }
		public decimal LinePurchasePct { get; set; }
		public string MethodOfPaymentGUID { get; set; }
		public decimal NetPurchaseAmount { get; set; }
		public int NumberOfRollbill { get; set; }
		public string OriginalInterestDate { get; set; }
		public string OriginalPurchaseLineGUID { get; set; }
		public decimal OutstandingBuyerInvoiceAmount  { get; set; }
		public decimal PurchaseAmount { get; set; }
		public decimal PurchaseFeeAmount { get; set; }
		public int PurchaseFeeCalculateBase { get; set; }
		public decimal PurchaseFeePct { get; set; }
		public string PurchaseLineInvoiceTableGUID  { get; set; }
		public decimal PurchasePct { get; set; }
		public string PurchaseTableGUID { get; set; }
		public decimal ReceiptFeeAmount { get; set; }
		public string RefPurchaseLineGUID { get; set; }
		public decimal ReserveAmount { get; set; }
		public decimal RetentionAmount { get; set; }
		public decimal RollbillInterestAmount { get; set; }
		public int RollbillInterestDay { get; set; }
		public decimal RollbillInterestPct { get; set; }
		public string RollbillPurchaseLineGUID { get; set; }
		public decimal SettleBillingFeeAmount { get; set; }
		public decimal SettleInterestAmount { get; set; }
		public decimal SettlePurchaseFeeAmount { get; set; }
		public decimal SettleReceiptFeeAmount { get; set; }
		public decimal SettleRollBillInterestAmount { get; set; }
		public string PurchaseTable_Values { get; set; }
		public string PurchaseTable_CustomerTableGUID { get; set; }
		public string PurchaseTable_CreditAppTableGUID { get; set; }
		public string PurchaseTable_PurchaseDate { get; set; }
		public bool CreditLitmitType_ValidateBuyerAgreement { get; set; }
		public string CreditAppLine_Values { get; set; }
		public decimal CreditAppLine_MaxPurchasePct { get; set; }
		public string PurchaseTable_PurchaseId { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public string RefPurchaseLine_Values { get; set; }
		public string PurchaseLineInvoiceTable_Values { get; set; }
		public bool PurchaseTable_AdditionalPurchase { get; set; }
		public bool RollBill { get; set; }
		public decimal RetentionCalculateBase { get; set; }
		public decimal RetentionPct { get; set; }
		public string CustomerPDCDate { get; set; }
		public bool AssignmentMethod_ValidateAssignmentBalance { get; set; }
		public string AssignmentMethod_AssignmentMethodGUID { get; set; }
	}
}
