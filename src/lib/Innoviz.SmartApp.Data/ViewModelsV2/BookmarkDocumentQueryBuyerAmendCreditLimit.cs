﻿using Innoviz.SmartApp.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
   public class QueryBuyerAmendCreditLimit
    {

        public Guid? QCAReqLineBuyerAmendCL_CreditAppRequestTableGUID { get; set; }
        public Guid? QCAReqLineBuyerAmendCL_BuyerTableGUID { get; set; }
        [BookmarkMapping("BuyerName1st_1")]
        public string QCAReqLineBuyerAmendCL_BuyerName { get; set; }
        public Guid? QCAReqLineBuyerAmendCL_BusinessSegmentGUID { get; set; }
        [BookmarkMapping("BusinessSegment1st_2")]
        public string QCAReqLineBuyerAmendCL_BusinessSegment { get; set; }
        [BookmarkMapping("BuyerTaxID1st_1")]
        public string QCAReqLineBuyerAmendCL_TaxIdPassportId { get; set; }
        public Guid? QCAReqLineBuyerAmendCL_InvoiceAddressGUID { get; set; }
        [BookmarkMapping("BuyerAddress1st_1")]
        public string QCAReqLineBuyerAmendCL_InvoiceAddress { get; set; }
        [BookmarkMapping("BuyerCompanyRegisteredDate1st_1")]
        public DateTime? QCAReqLineBuyerAmendCL_DateOfEstablish { get; set; }

        public Guid? QCAReqLineBuyerAmendCL_LineOfBusinessGUID { get; set; }
        [BookmarkMapping("BuyerLineOfBusiness1st_1")]
        public string QCAReqLineBuyerAmendCL_LineOfBusiness { get; set; }
        public Guid? QCAReqLineBuyerAmendCL_CreditScoringGUID { get; set; }
        [BookmarkMapping("CreditScoreHeaderAmend","BuyerCreditScoring1st_1")] //R02
        public string QCAReqLineBuyerAmendCL_CreditScoring { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer1st_2")]
        public decimal QCAReqLineBuyerAmendCL_BuyerCreditLimit { get; set; }
        [BookmarkMapping("AROutstandingThisCustomer")]
        public decimal QCAReqLineBuyerAmendCL_CustomerBuyerOutstanding { get; set; }
        [BookmarkMapping("AROutstandingAllCustomer")]
        public decimal QCAReqLineBuyerAmendCL_AllCustomerBuyerOutstanding { get; set; }
        [BookmarkMapping("FactoringPurchaseFee1st_1")]
        public decimal QCAReqLineBuyerAmendCL_PurchaseFeePct { get; set; }
        //[BookmarkMapping("FactoringPurchaseFeeCalBase1st_1")]
        //public decimal QCAReqLineBuyerAmendCL_PurchaseFeeCalculateBase { get; set; }
        public Guid? QCAReqLineBuyerAmendCL_BillingResponsibleByGUID { get; set; }
        //[BookmarkMapping("BillingResponsible1st_2")]
        //public string QCAReqLineBuyerAmendCL_BillingResponsibleBy { get; set; }
        public bool QCAReqLineBuyerAmendCL_AcceptanceDocument { get; set; }

        public Guid? QCAReqLineBuyerAmendCL_BillingContactPersonGUID { get; set; }
        public Guid? QCAReqLineBuyerAmendCL_BillingAddressGUID { get; set; }
        [BookmarkMapping("BuyerBillingAddress1st_1")]
        public string QCAReqLineBuyerAmendCL_BillingAddress { get; set; }
        [BookmarkMapping("ReceiptDescriptiong1st_1")]
        public string QCAReqLineBuyerAmendCL_ReceiptDescription { get; set; }
        public Guid? QCAReqLineBuyerAmendCL_MethodOfPaymentGUID { get; set; }
        [BookmarkMapping("PaymentMethod1st_2")]
        public string QCAReqLineBuyerAmendCL_MethodOfPayment { get; set; }
        public Guid? QCAReqLineBuyerAmendCL_ReceiptContactPersonGUID { get; set; }
        public Guid? QCAReqLineBuyerAmendCL_ReceiptAddressGUID { get; set; }
        [BookmarkMapping("BuyerReceiptAddress1st_1")]
        public string QCAReqLineBuyerAmendCL_ReceiptAddress { get; set; }
        [BookmarkMapping("MarketingComment")]
        public string QCAReqLineBuyerAmendCL_MarketingComment { get; set; }
        [BookmarkMapping("CreditComment")]
        public string QCAReqLineBuyerAmendCL_CreditComment { get; set; }
        [BookmarkMapping("ApproverComment")]
        public string QCAReqLineBuyerAmendCL_ApproverComment { get; set; }
        [BookmarkMapping("NewBuyerCreditLimit")]
        public decimal QCAReqLineBuyerAmendCL_CreditLimitLineRequest { get; set; }
        [BookmarkMapping("BillingDescriptiong1st_1")]
        public string QCAReqLineBuyerAmendCL_BillingDescription { get; set; }
        [BookmarkMapping("BuyerBlacklistStatus1st_1")]
        public string QCAReqLineBuyerAmendCL_BlacklistStatus { get; set; }

        //////////////////////////////////////////////////////////////////
        [BookmarkMapping("FactoringPurchaseFeeCalBase1st_1")]
        public string QCAReqTableBuyerAmendCL_PurchaseFeeCalculateBase { get; set; }
        [BookmarkMapping("CANumber")]
        public string QCAReqTableBuyerAmendCL_CreditAppRequestId { get; set; }
        public Guid? QCAReqTableBuyerAmendCL_RefCreditAppTableGUID { get; set; }
        [BookmarkMapping("RefCANumber")]
        public string QCAReqTableBuyerAmendCL_OriginalCreditAppTableId { get; set; }
        public Guid? QCAReqTableBuyerAmendCL_DocumentStatusGUID { get; set; }
        [BookmarkMapping("CAStatus")]
        public string QCAReqTableBuyerAmendCL_Status { get; set; }
        [BookmarkMapping("CACreateDate")]
        public DateTime? QCAReqTableBuyerAmendCL_RequestDate { get; set; }
        public Guid? QCAReqTableBuyerAmendCL_CreditScoringGUID { get; set; }

        public string QCAReqTableBuyerAmendCL_CreditScoring { get; set; }
        
        [BookmarkMapping("PurchaseConditionCust")]
        public string QCAReqLineAmendBuyerAmendCL_CACondition { get; set; }
        
        [BookmarkMapping("CADesp")]

        public string QCAReqTableBuyerAmendCL_Description { get; set; }
        public Guid? QCAReqTableBuyerAmendCL_DocumentReasonGUID { get; set; }
        public string QCAReqTableBuyerAmendCL_Purpose { get; set; }
        [BookmarkMapping("CARemark")]
        public string QCAReqTableBuyerAmendCL_Remark { get; set; }
        public Guid? QCAReqLineBuyerAmendCL_CustomerTableGUID { get; set; }
        [BookmarkMapping("CustomerCode")]
        public string QCAReqLineBuyerAmendCL_CustomerId { get; set; }
        [BookmarkMapping("CustomerName1")]
        public string QCAReqLineBuyerAmendCL_CustomerName { get; set; }
        public Guid? QCAReqLineBuyerAmendCL_ResponsibleByGUID { get; set; }
        [BookmarkMapping("SalesResp")]
        public string QCAReqLineBuyerAmendCL_ResponsibleBy { get; set; }

        //////////////////////////////////////////////////////////////////

        [BookmarkMapping("ApprovedBuyerCreditLimit")]
        public decimal QCAReqLineAmendBuyerAmendCL_OriginalCreditLimitLineRequest { get; set; }


        //////////////////////////////////////////////////////////////////


        [BookmarkMapping("BuyerBillingContactPerson1st_1")]
        public string QBillingContactPersonBuyerAmendCL_Name { get; set; }
        [BookmarkMapping("BuyerBillingContactPersonTel1st_1")]
        public string QBillingContactPersonBuyerAmendCL_Phone { get; set; }
        [BookmarkMapping("BuyerBillingContactPersonPosition1st_1")]
        public string QBillingContactPersonBuyerAmendCL_Position { get; set; }
        public string QBillingContactPersonBuyerAmendCL_ContactPersonTrans { get; set; }
        /////////////////////////////////////////////////////////////////////
        [BookmarkMapping("BuyerReceiptContactPerson1st_1")]
        public string QReceiptContactPersonBuyerAmendCL_Name { get; set; }
        [BookmarkMapping("BuyerReceiptContactPersonTel1st_1")]
        public string QReceiptContactPersonBuyerAmendCL_Phone { get; set; }
        [BookmarkMapping("BuyerReceiptContactPersonPosition1st_1")]
        public string QReceiptContactPersonBuyerAmendCL_Position { get; set; }
        public string QReceiptContactPersonBuyerAmendCL_ContactPersonTrans { get; set; }
        /////////////////////////////////////////////////////////////////////
        [BookmarkMapping("BuyerFinYearFirst1_1st_1", "BuyerFinYearFirst2_1st_1")]
        public int QCreditAppReqLineFinBuyerAmendCL_Year_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalFirst1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_RegisteredCapital_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalFirst1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_PaidCapital_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetFirst1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalAsset_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilityFirst1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalLiability_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquityFirst1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalEquity_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueFirst1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalRevenue_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSFirst1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalCOGS_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitFirst1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalGrossProfit_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpFirst1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalOperExpFirst_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitFirst1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalNetProfitFirst_1 { get; set; }
        [BookmarkMapping("BuyerFinNetProfitPercent1_1st_1")]

        public decimal QCreditAppReqLineFinBuyerAmendCL_NetProfitPercent_1 { get; set; }
        [BookmarkMapping("BuyerFinlDE1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_lDE_1 { get; set; }
        [BookmarkMapping("BuyerFinQuickRatio1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_QuickRatio_1 { get; set; }
        [BookmarkMapping("BuyerFinIntCoverageRatio1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_IntCoverageRatio_1 { get; set; }

        /////////////
        [BookmarkMapping("BuyerFinYearSecond1_1st_1", "BuyerFinYearSecond2_1st_1")]
        public int QCreditAppReqLineFinBuyerAmendCL_Year_2 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalSecond1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_RegisteredCapital_2 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalSecond1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_PaidCapital_2 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetSecond1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalAsset_2 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilitySecond1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalLiability_2 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquitySecond1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalEquity_2 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueSecond1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalRevenue_2 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSSecond1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalCOGS_2 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitSecond1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalGrossProfit_2 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpSecond1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalOperExpFirst_2 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitSecond1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalNetProfitFirst_2 { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendCL_NetProfitPercent_2 { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendCL_lDE_2 { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendCL_QuickRatio_2 { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendCL_IntCoverageRatio_2 { get; set; }
        ////////////
        [BookmarkMapping("BuyerFinYearThird1_1st_1", "BuyerFinYearThird2_1st_1")]
        public int QCreditAppReqLineFinBuyerAmendCL_Year_3 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalThird1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_RegisteredCapital_3 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalThird1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_PaidCapital_3 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetThird1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalAsset_3 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilityThird1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalLiability_3 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquityThird1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalEquity_3 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueThird1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalRevenue_3 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSThird1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalCOGS_3 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitThird1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalGrossProfit_3 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpThird1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalOperExpFirst_3 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitThird1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalNetProfitFirst_3 { get; set; }

        public decimal QCreditAppReqLineFinBuyerAmendCL_NetProfitPercent_3 { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendCL_lDE_3 { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendCL_QuickRatio_3 { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendCL_IntCoverageRatio_3 { get; set; }

        ///////////////////////////////////////////////////////////////////////////////////
        [BookmarkMapping("BillingDocumentFirst1_1st_1")]
        public string QBillingDocumentBuyerAmendCL_DocumentTypeDescription_1 { get; set; }
        [BookmarkMapping("BillingDocumentSecond1_1st_1")]
        public string QBillingDocumentBuyerAmendCL_DocumentTypeDescription_2 { get; set; }
        [BookmarkMapping("BillingDocumentThird1_1st_1")]
        public string QBillingDocumentBuyerAmendCL_DocumentTypeDescription_3 { get; set; }
        [BookmarkMapping("BillingDocumentFourth_1st_1")]
        public string QBillingDocumentBuyerAmendCL_DocumentTypeDescription_4 { get; set; }
        [BookmarkMapping("BillingDocumentFifth_1st_1")]
        public string QBillingDocumentBuyerAmendCL_DocumentTypeDescription_5 { get; set; }
        [BookmarkMapping("BillingDocumentSixth1_1st_1")]
        public string QBillingDocumentBuyerAmendCL_DocumentTypeDescription_6 { get; set; }
        [BookmarkMapping("BillingDocumentSeventh_1st_1")]
        public string QBillingDocumentBuyerAmendCL_DocumentTypeDescription_7 { get; set; }
        [BookmarkMapping("BillingDocumentEigth1_1st_1")]
        public string QBillingDocumentBuyerAmendCL_DocumentTypeDescription_8 { get; set; }
        [BookmarkMapping("BillingDocumentNineth1_1st_1")]
        public string QBillingDocumentBuyerAmendCL_DocumentTypeDescription_9 { get; set; }
        [BookmarkMapping("BillingDocumentTenth_1st_1")]
        public string QBillingDocumentBuyerAmendCL_DocumentTypeDescription_10 { get; set; }
        ///////////////////////////////////////////////////////////////////////////////////
        [BookmarkMapping("ReceiptDocumentFirst1_1st_1")]
        public string QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSecond1_1st_1")]
        public string QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription_2 { get; set; }
        [BookmarkMapping("ReceiptDocumentThird1_1st_1")]
        public string QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription_3 { get; set; }
        [BookmarkMapping("ReceiptDocumentFourth_1st_1")]
        public string QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription_4 { get; set; }
        [BookmarkMapping("ReceiptDocumentFifth_1st_1")]
        public string QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription_5 { get; set; }
        [BookmarkMapping("ReceiptDocumentSixth1_1st_1")]
        public string QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription_6 { get; set; }
        [BookmarkMapping("ReceiptDocumentSeventh_1st_1")]
        public string QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription_7 { get; set; }
        [BookmarkMapping("ReceiptDocumentEigth1_1st_1")]
        public string QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription_8 { get; set; }
        [BookmarkMapping("ReceiptDocumentNineth1_1st_1")]
        public string QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription_9 { get; set; }
        [BookmarkMapping("ReceiptDocumentTenth_1st_1")]
        public string QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription_10 { get; set; }
        ///////////////////////////////////////////////////////////////////////////////////

        public int QServiceFeeConditionLineBuyerAmendCL_Ordering_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark01")]
        public string QServiceFeeConditionLineBuyerAmendCL_Description_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt01")]
        public decimal QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc01")]
        public string QServiceFeeConditionLineBuyerAmendCL_ServicefeeType_1 { get; set; }
        ///////////////
        public int QServiceFeeConditionLineBuyerAmendCL_Ordering_2 { get; set; }
        [BookmarkMapping("ServiceFeeRemark02")]
        public string QServiceFeeConditionLineBuyerAmendCL_Description_2 { get; set; }
        [BookmarkMapping("ServiceFeeAmt02")]
        public decimal QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax_2 { get; set; }
        [BookmarkMapping("ServiceFeeDesc02")]
        public string QServiceFeeConditionLineBuyerAmendCL_ServicefeeType_2 { get; set; }
        ///////////////
        public int QServiceFeeConditionLineBuyerAmendCL_Ordering_3 { get; set; }
        [BookmarkMapping("ServiceFeeRemark03")]
        public string QServiceFeeConditionLineBuyerAmendCL_Description_3 { get; set; }
        [BookmarkMapping("ServiceFeeAmt03")]
        public decimal QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax_3 { get; set; }
        [BookmarkMapping("ServiceFeeDesc03")]
        public string QServiceFeeConditionLineBuyerAmendCL_ServicefeeType_3 { get; set; }
        ///////////////
        public int QServiceFeeConditionLineBuyerAmendCL_Ordering_4 { get; set; }
        [BookmarkMapping("ServiceFeeRemark04")]
        public string QServiceFeeConditionLineBuyerAmendCL_Description_4 { get; set; }
        [BookmarkMapping("ServiceFeeAmt04")]
        public decimal QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax_4 { get; set; }
        [BookmarkMapping("ServiceFeeDesc04")]
        public string QServiceFeeConditionLineBuyerAmendCL_ServicefeeType_4 { get; set; }
        ///////////////
        public int QServiceFeeConditionLineBuyerAmendCL_Ordering_5 { get; set; }
        [BookmarkMapping("ServiceFeeRemark05")]
        public string QServiceFeeConditionLineBuyerAmendCL_Description_5 { get; set; }
        [BookmarkMapping("ServiceFeeAmt05")]
        public decimal QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax_5 { get; set; }
        [BookmarkMapping("ServiceFeeDesc05")]
        public string QServiceFeeConditionLineBuyerAmendCL_ServicefeeType_5 { get; set; }
        ///////////////

        public int QServiceFeeConditionLineBuyerAmendCL_Ordering_6 { get; set; }
        [BookmarkMapping("ServiceFeeRemark06")]
        public string QServiceFeeConditionLineBuyerAmendCL_Description_6 { get; set; }
        [BookmarkMapping("ServiceFeeAmt06")]
        public decimal QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax_6 { get; set; }
        [BookmarkMapping("ServiceFeeDesc06")]
        public string QServiceFeeConditionLineBuyerAmendCL_ServicefeeType_6 { get; set; }
        ///////////////

        public int QServiceFeeConditionLineBuyerAmendCL_Ordering_7 { get; set; }
        [BookmarkMapping("ServiceFeeRemark07")]
        public string QServiceFeeConditionLineBuyerAmendCL_Description_7 { get; set; }
        [BookmarkMapping("ServiceFeeAmt07")]
        public decimal QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax_7 { get; set; }
        [BookmarkMapping("ServiceFeeDesc07")]
        public string QServiceFeeConditionLineBuyerAmendCL_ServicefeeType_7 { get; set; }
        ///////////////

        public int QServiceFeeConditionLineBuyerAmendCL_Ordering_8 { get; set; }
        [BookmarkMapping("ServiceFeeRemark08")]
        public string QServiceFeeConditionLineBuyerAmendCL_Description_8 { get; set; }
        [BookmarkMapping("ServiceFeeAmt08")]
        public decimal QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax_8 { get; set; }
        [BookmarkMapping("ServiceFeeDesc08")]
        public string QServiceFeeConditionLineBuyerAmendCL_ServicefeeType_8 { get; set; }
        ///////////////

        public int QServiceFeeConditionLineBuyerAmendCL_Ordering_9 { get; set; }
        [BookmarkMapping("ServiceFeeRemark09")]
        public string QServiceFeeConditionLineBuyerAmendCL_Description_9 { get; set; }
        [BookmarkMapping("ServiceFeeAmt09")]
        public decimal QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax_9 { get; set; }
        [BookmarkMapping("ServiceFeeDesc09")]
        public string QServiceFeeConditionLineBuyerAmendCL_ServicefeeType_9 { get; set; }
        ///////////////
        public int QServiceFeeConditionLineBuyerAmendCL_Ordering_10 { get; set; }
        [BookmarkMapping("ServiceFeeRemark10")]
        public string QServiceFeeConditionLineBuyerAmendCL_Description_10 { get; set; }
        [BookmarkMapping("ServiceFeeAmt10")]
        public decimal QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax_10 { get; set; }
        [BookmarkMapping("ServiceFeeDesc10")]
        public string QServiceFeeConditionLineBuyerAmendCL_ServicefeeType_10 { get; set; }
        /////////////// ////////////// ////////////// ////////////// ////////////// ////////////// //////////////
        [BookmarkMapping("SumServiceFeeAmt")]
        public decimal QSumServiceFeeConditionLineBuyerAmendCL_SumServiceFeeAmt { get; set; }
        ////////////// ////////////// ////////////// ////////////// ////////////// ////////////// //////////////
        [BookmarkMapping("ApproverReasonFirst1")]
        public string QActionHistoryBuyerAmendCL_Comment_1 { get; set; }
        [BookmarkMapping("ApprovedDateFirst1")]
        public DateTime? QActionHistoryBuyerAmendCL_CreatedDateTime_1 { get; set; }
        public string QActionHistoryBuyerAmendCL_ActionName_1 { get; set; }
        [BookmarkMapping("ApproverNameFirst1")]
        public string QActionHistoryBuyerAmendCL_ApproverName_1 { get; set; }
        //////////////
        [BookmarkMapping("ApproverReasonSecond1")]
        public string QActionHistoryBuyerAmendCL_Comment_2 { get; set; }
        [BookmarkMapping("ApprovedDateSecond1")]
        public DateTime? QActionHistoryBuyerAmendCL_CreatedDateTime_2 { get; set; }
        public string QActionHistoryBuyerAmendCL_ActionName_2 { get; set; }
        [BookmarkMapping("ApproverNameSecond1")]
        public string QActionHistoryBuyerAmendCL_ApproverName_2 { get; set; }
        //////////////
        [BookmarkMapping("ApproverReasonThird1")]
        public string QActionHistoryBuyerAmendCL_Comment_3 { get; set; }
        [BookmarkMapping("ApprovedDateThird1")]
        public DateTime? QActionHistoryBuyerAmendCL_CreatedDateTime_3 { get; set; }
        public string QActionHistoryBuyerAmendCL_ActionName_3 { get; set; }
        [BookmarkMapping("ApproverNameThird1")]
        public string QActionHistoryBuyerAmendCL_ApproverName_3 { get; set; }
        //////////////
        [BookmarkMapping("ApproverReasonFourth1")]
        public string QActionHistoryBuyerAmendCL_Comment_4 { get; set; }
        [BookmarkMapping("ApprovedDateFourth1")]
        public DateTime? QActionHistoryBuyerAmendCL_CreatedDateTime_4 { get; set; }
        public string QActionHistoryBuyerAmendCL_ActionName_4 { get; set; }
        [BookmarkMapping("ApproverNameFourth1")]
        public string QActionHistoryBuyerAmendCL_ApproverName_4 { get; set; }
        //////////////
        [BookmarkMapping("ApproverReasonFifth1")]
        public string QActionHistoryBuyerAmendCL_Comment_5 { get; set; }
        [BookmarkMapping("ApprovedDateFifth1")]
        public DateTime? QActionHistoryBuyerAmendCL_CreatedDateTime_5 { get; set; }
        public string QActionHistoryBuyerAmendCL_ActionName_5 { get; set; }
        [BookmarkMapping("ApproverNameFifth1")]
        public string QActionHistoryBuyerAmendCL_ApproverName_5 { get; set; }
        //////////////

        public string QActionHistoryBuyerAmendCL_Comment_6 { get; set; }
        public DateTime? QActionHistoryBuyerAmendCL_CreatedDateTime_6 { get; set; }
        public string QActionHistoryBuyerAmendCL_ActionName_6 { get; set; }
        public string QActionHistoryBuyerAmendCL_ApproverName_6 { get; set; }
        //////////////

        public string QActionHistoryBuyerAmendCL_Comment_7 { get; set; }
        public DateTime? QActionHistoryBuyerAmendCL_CreatedDateTime_7 { get; set; }
        public string QActionHistoryBuyerAmendCL_ActionName_7 { get; set; }
        public string QActionHistoryBuyerAmendCL_ApproverName_7 { get; set; }
        //////////////

        public string QActionHistoryBuyerAmendCL_Comment_8 { get; set; }
        public DateTime? QActionHistoryBuyerAmendCL_CreatedDateTime_8 { get; set; }
        public string QActionHistoryBuyerAmendCL_ActionName_8 { get; set; }
        public string QActionHistoryBuyerAmendCL_ApproverName_8 { get; set; }
        //////////////
        [BookmarkMapping("BuyerCreditLimitRequest")]
        public decimal QueryBuyerAmendCreditLimit_BuyerCreditLimitRequest { get; set; }
        [BookmarkMapping("AcceptanceDocument1st_1")]
        public string QueryBuyerAmendCreditLimit_AcceptanceDocument { get; set; }
        //R02
        [BookmarkMapping("KYCHeaderAmend")]
        public string QCAReqLineBuyerAmendCL_KYCSetup { get; set; }
        [BookmarkMapping("BillingResponsible1st_2")]
        public string QOriginalCreditAppRequestLine_BillingResponsibleBy { get; set; }

    }
    public class QCAReqLineBuyerAmendCL
    {
        public Guid QCAReqLineBuyerAmendCL_CreditAppRequestLineGUID { get; set; }
        public Guid? QCAReqLineBuyerAmendCL_RefCreditAppLineGUID { get; set; }
        public Guid QCAReqLineBuyerAmendCL_CreditAppRequestTableGUID { get; set; }
        public Guid? QCAReqLineBuyerAmendCL_BuyerTableGUID { get; set; }
        public string QCAReqLineBuyerAmendCL_BuyerName { get; set; }
        public Guid? QCAReqLineBuyerAmendCL_BusinessSegmentGUID { get; set; }
        public string QCAReqLineBuyerAmendCL_BusinessSegment { get; set; }
        public int QCAReqLineBuyerAmendCL_IdentificationType { get; set; }
        public string QCAReqLineBuyerAmendCL_TaxId { get; set; }
        public string QCAReqLineBuyerAmendCL_PassportId { get; set; }
        public Guid? QCAReqLineBuyerAmendCL_InvoiceAddressGUID { get; set; }
        public string QCAReqLineBuyerAmendCL_InvoiceAddress { get; set; }
        public DateTime? QCAReqLineBuyerAmendCL_DateOfEstablish { get; set; }

        public Guid? QCAReqLineBuyerAmendCL_LineOfBusinessGUID { get; set; }
        public string QCAReqLineBuyerAmendCL_LineOfBusiness { get; set; }
        public Guid? QCAReqLineBuyerAmendCL_CreditScoringGUID { get; set; }
        public string QCAReqLineBuyerAmendCL_CreditScoring { get; set; }
        public decimal QCAReqLineBuyerAmendCL_BuyerCreditLimit { get; set; }
        public decimal QCAReqLineBuyerAmendCL_CustomerBuyerOutstanding { get; set; }
        public decimal QCAReqLineBuyerAmendCL_AllCustomerBuyerOutstanding { get; set; }
        public decimal QCAReqLineBuyerAmendCL_PurchaseFeePct { get; set; }
        public int QCAReqLineBuyerAmendCL_PurchaseFeeCalculateBase { get; set; }
        public Guid? QCAReqLineBuyerAmendCL_BillingResponsibleByGUID { get; set; }
        public string QCAReqLineBuyerAmendCL_BillingResponsibleBy { get; set; }
        public bool QCAReqLineBuyerAmendCL_AcceptanceDocument { get; set; }

        public Guid? QCAReqLineBuyerAmendCL_BillingContactPersonGUID { get; set; }

        public Guid? QCAReqLineBuyerAmendCL_BillingAddressGUID { get; set; }
        public string QCAReqLineBuyerAmendCL_BillingAddress { get; set; }
        public Guid? QCAReqLineBuyerAmendCL_MethodOfPaymentGUID { get; set; }
        public string QCAReqLineBuyerAmendCL_MethodOfPayment { get; set; }
        public string QCAReqLineBuyerAmendCL_ReceiptDescription { get; set; }
        public Guid? QCAReqLineBuyerAmendCL_ReceiptContactPersonGUID { get; set; }
        public Guid? QCAReqLineBuyerAmendCL_ReceiptAddressGUID { get; set; }
        public string QCAReqLineBuyerAmendCL_ReceiptAddress { get; set; }
        public string QCAReqLineBuyerAmendCL_MarketingComment { get; set; }
        public string QCAReqLineBuyerAmendCL_CreditComment { get; set; }
        public string QCAReqLineBuyerAmendCL_ApproverComment { get; set; }
        public decimal QCAReqLineBuyerAmendCL_CreditLimitLineRequest { get; set; }
        public string QCAReqLineBuyerAmendCL_BillingDescription { get; set; }
        public string QCAReqLineBuyerAmendCL_BlacklistStatus { get; set; }
        //R02
        public string QCAReqLineBuyerAmendCL_KYCSetup { get; set; }
    }
    public class QCAReqTableBuyerAmendCL
    {
        public decimal QCAReqTableBuyerAmendCL_PurchaseFeeCalculateBase { get; set; }
        public string QCAReqTableBuyerAmendCL_CreditAppRequestId { get; set; }
        public Guid QCAReqTableBuyerAmendCL_CrediAppReqGUID { get;set;}
        public Guid? QCAReqTableBuyerAmendCL_RefCreditAppTableGUID { get; set; }
        public string QCAReqTableBuyerAmendCL_OriginalCreditAppTableId { get; set; }
        public Guid? QCAReqTableBuyerAmendCL_DocumentStatusGUID { get; set; }
        public string QCAReqTableBuyerAmendCL_Status { get; set; }
        public DateTime? QCAReqTableBuyerAmendCL_RequestDate { get; set; }
        public string QCAReqTableBuyerAmendCL_Description { get; set; }
        public string QCAReqTableBuyerAmendCL_Remark { get; set; }
        public Guid? QCAReqLineBuyerAmendCL_CustomerTableGUID { get; set; }
        public string QCAReqLineBuyerAmendCL_CustomerId { get; set; }
        public string QCAReqLineBuyerAmendCL_CustomerName { get; set; }
        public Guid? QCAReqLineBuyerAmendCL_ResponsibleByGUID { get; set; }
        public string QCAReqLineBuyerAmendCL_ResponsibleBy { get; set; }
        public string QCAReqTableBuyerAmendCL_CreditScoring { get; set; }
        public string QCAReqLineAmendBuyerAmendCL_CACondition { get; set; }
        public Guid QCAReqTableBuyerAmendCL_RefCreditAppRequestTableGUID { get; set; }
        

    }
    public class QCAReqLineAmendBuyerAmendCL
    {
        public decimal QCAReqLineAmendBuyerAmendCL_OriginalCreditLimitLineRequest { get; set; }
    }
    public class QBillingContactPersonBuyerAmendCL
    {
        public string QBillingContactPersonBuyerAmendCL_Name { get; set; }
        public string QBillingContactPersonBuyerAmendCL_Phone { get; set; }
        public string QBillingContactPersonBuyerAmendCL_Position { get; set; }
        public string QBillingContactPersonBuyerAmendCL_ContactPersonTrans { get; set; }
    }
    public class QReceiptContactPersonBuyerAmendCL
    {
        public string QReceiptContactPersonBuyerAmendCL_Name { get; set; }
        public string QReceiptContactPersonBuyerAmendCL_Phone { get; set; }
        public string QReceiptContactPersonBuyerAmendCL_Position { get; set; }
        public string QReceiptContactPersonBuyerAmendCL_ContactPersonTrans { get; set; }
    }
    public class QCreditAppReqLineFinBuyerAmendCL
    {
        public int QCreditAppReqLineFinBuyerAmendCL_Year { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendCL_RegisteredCapital { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendCL_PaidCapital { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalAsset { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalLiability { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalEquity { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalRevenue { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalCOGS { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalGrossProfit { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalOperExpFirst { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendCL_TotalNetProfitFirst { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendCL_NetProfitPercent { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendCL_lDE { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendCL_QuickRatio { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendCL_IntCoverageRatio { get; set; }

        public int QCreditAppReqLineFinBuyerAmendCL_Row { get; set; }
    }
    public class QBillingDocumentBuyerAmendCL
    {
        public string QBillingDocumentBuyerAmendCL_DocumentTypeDescription { get; set; }
        public int QBillingDocumentBuyerAmendCL_Row { get; set; }
    }
    public class QReceiptDocumentConditionBuyerAmendCL
    {
        public string QReceiptDocumentConditionBuyerAmendCL_DocumentTypeDescription { get; set; }
        public int QReceiptDocumentConditionBuyerAmendCL_Row { get; set; }
    }
    public class QServiceFeeConditionLineBuyerAmendCL
    {
        public int QServiceFeeConditionLineBuyerAmendCL_Ordering { get; set; }
        public string QServiceFeeConditionLineBuyerAmendCL_Description { get; set; }
        public decimal QServiceFeeConditionLineBuyerAmendCL_AmountBeforeTax { get; set; }
        public string QServiceFeeConditionLineBuyerAmendCL_ServicefeeType { get; set; }
        public int QServiceFeeConditionLineBuyerAmendCL_Row { get; set; }
    }
    public class QSumServiceFeeConditionLineBuyerAmendCL
    {
        public decimal QSumServiceFeeConditionLineBuyerAmendCL_SumServiceFeeAmt { get; set; }
    }
    public class QActionHistoryBuyerAmendCL
    {
        public string QActionHistoryBuyerAmendCL_Comment { get; set; }
        public DateTime? QActionHistoryBuyerAmendCL_CreatedDateTime { get; set; }
        public string QActionHistoryBuyerAmendCL_ActionName { get; set; }
        public string QActionHistoryBuyerAmendCL_ApproverName { get; set; }
        public int QActionHistoryBuyerAmendCL_Row { get; set; }
    }
    public class QOriginalCreditAppRequestLine
    {
        public Guid? QOriginalCreditAppRequestLine_BillingResponsibleByGUID { get; set; }
        public string QOriginalCreditAppRequestLine_BillingResponsibleBy { get; set; }
        public Guid QOriginalCreditAppRequestLine_CreditAppRequestLineGUID { get; set; }
        public Guid? QOriginalCreditAppRequestLine_RefCreditLineGUID { get; set; }

    }
}
