﻿using Innoviz.SmartApp.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class BookmarkDocumentQueryBuyerAmendInformation
    {
        [BookmarkMapping("CANumber")]
        public string QCreditAppRequestTableBuyerAmendInfo_CreditAppRequestId { get; set; }
        [BookmarkMapping("CADesp1")]
        public string QCreditAppRequestTableBuyerAmendInfo_Description { get; set; }
        public Guid? QCreditAppRequestTableBuyerAmendInfo_RefCreditAppTableGUID { get; set; }
        [BookmarkMapping("RefCANumber")]
        public string QCreditAppRequestTableBuyerAmendInfo_OriginalCreditAppTableId { get; set; }
        public Guid? QCreditAppRequestTableBuyerAmendInfo_DocumentStatusGUID { get; set; }
        [BookmarkMapping("CAStatus")]
        public string QCreditAppRequestTableBuyerAmendInfo_Status { get; set; }
        [BookmarkMapping("CACreateDate")]
        public DateTime? QCreditAppRequestTableBuyerAmendInfo_RequestDate { get; set; }
        public Guid? QCreditAppRequestTableBuyerAmendInfo_CustomerTableGUID { get; set; }
        [BookmarkMapping("CARemark1")]
        public string QCreditAppRequestTableBuyerAmendInfo_Remark { get; set; }


        ////////////
        [BookmarkMapping("CustomerCode")]
        public string QCustomerBuyerAmendInfo_CustomerId { get; set; }
        [BookmarkMapping("CustomerName1")]

        public string QCustomerBuyerAmendInfo_CustomerName { get; set; }

        public Guid? QCustomerBuyerAmendInfo_ResponsibleByGUID { get; set; }
        [BookmarkMapping("SalesResp")]

        public string QCustomerBuyerAmendInfo_ResponsibleBy { get; set; }

        //////////


        public Guid? QCreditAppRequestLineBuyerAmendInfo_BusinessSegmentGUID { get; set; }
        [BookmarkMapping("BusinessSegment1st")]
        public string QCreditAppRequestLineBuyerAmendInfo_BusinessSegment { get; set; }
        [BookmarkMapping("BuyerName1st")]
        public string QCreditAppRequestLineBuyerAmendInfo_BuyerName { get; set; }
        [BookmarkMapping("BuyerTaxID1st")]
        public string QCreditAppRequestLineBuyerAmendInfo_TaxId { get; set; }
        public Guid? QCreditAppRequestLineBuyerAmendInfo_InvoiceAddressGUID { get; set; }
        [BookmarkMapping("BuyerAddress1st")]
        public string QCreditAppRequestLineBuyerAmendInfo_InvoiceAddress { get; set; }
        [BookmarkMapping("BuyerCompanyRegisteredDate1st")]
        public DateTime? QCreditAppRequestLineBuyerAmendInfo_DateOfEstablish { get; set; }
        public Guid? QCreditAppRequestLineBuyerAmendInfo_LineOfBusinessGUID { get; set; }
        [BookmarkMapping("BuyerLineOfBusiness1st")]
        public string QCreditAppRequestLineBuyerAmendInfo_LineOfBusiness { get; set; }
        public Guid? QCreditAppRequestLineBuyerAmendInfo_BlacklistStatusGUID { get; set; }
        [BookmarkMapping("BuyerBlacklistStatus1st")]
        public string QCreditAppRequestLineBuyerAmendInfo_BlacklistStatus { get; set; }
        public Guid? QCreditAppRequestLineBuyerAmendInfo_CreditScoringGUID { get; set; }
        [BookmarkMapping("BuyerCreditScoring1st", "CreditScoreHeaderAmend")]
        public string QCreditAppRequestLineBuyerAmendInfo_CreditScoring { get; set; }
        [BookmarkMapping("NewPurchaseRate1st")]
        public decimal? QCreditAppRequestLineBuyerAmendInfo_MaxPurchasePct { get; set; }
        [BookmarkMapping("NewPurchaseFeeRate1st")]
        public decimal? QCreditAppRequestLineBuyerAmendInfo_PurchaseFeePct { get; set; }
        [BookmarkMapping("NewPurchaseFeeCalBase1st")]
        public string QCreditAppRequestLineBuyerAmendInfo_PurchaseFeeCalculateBase { get; set; }
        public Guid? QCreditAppRequestLineBuyerAmendInfo_BillingAddressGUID { get; set; }
        [BookmarkMapping("NewBillingAddress1st")]
        public string QCreditAppRequestLineBuyerAmendInfo_BillingAddress { get; set; }
        [BookmarkMapping("NewReceiptRemark1st")]
        public string QCreditAppRequestLineBuyerAmendInfo_ReceiptRemark { get; set; }
        [BookmarkMapping("MarketingComment1st")]
        public string QCreditAppRequestLineBuyerAmendInfo_MarketingComment { get; set; }
        [BookmarkMapping("CreditComment1st")]
        public string QCreditAppRequestLineBuyerAmendInfo_CreditComment { get; set; }
        [BookmarkMapping("ApproverComment1st")]
        public string QCreditAppRequestLineBuyerAmendInfo_ApproverComment { get; set; }
        [BookmarkMapping("NewReceiptAddress1st")]
        public string QCreditAppRequestLineBuyerAmendInfo_ReceiptAddress { get; set; }
        public Guid? QCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID { get; set; }

        ////////////////////////////////////////
        ///
        [BookmarkMapping("OldPurchaseRate1st")]
        public decimal QCreditAppRequestLineAmendBuyerAmendInfo_OriginalMaxPurchasePct { get; set; }
        [BookmarkMapping("OldPurchaseFeeRate1st")]
        public decimal QCreditAppRequestLineAmendBuyerAmendInfo_OriginalPurchaseFeePct { get; set; }
        [BookmarkMapping("OldPurchaseFeeCalBase1st")]
        public string QCreditAppRequestLineAmendBuyerAmendInfo_OriginalPurchaseFeeCalculateBase { get; set; }
        public Guid? QCreditAppRequestLineAmendBuyerAmendInfo_OriginalBillingAddressGUID { get; set; }
        [BookmarkMapping("OldBillingAddress1st")]
        public string QCreditAppRequestLineAmendBuyerAmendInfo_OriginalBillingAddress { get; set; }
        public Guid? QCreditAppRequestLineAmendBuyerAmendInfo_OriginalReceiptAddressGUID { get; set; }
        [BookmarkMapping("OldReceiptAddress1st")]
        public string QCreditAppRequestLineAmendBuyerAmendInfo_OriginalReceiptAddress { get; set; }
        [BookmarkMapping("OldReceiptRemark1st")]
        public string QCreditAppRequestLineAmendBuyerAmendInfo_OriginalReceiptRemark { get; set; }
        /////////////////////////////////////////
        [BookmarkMapping("NewBillingDocConDescription1stDoc_1st")]
        public string QBillingDocumentBuyerAmendInfo_DocumentType_1 { get; set; }
        public bool QBillingDocumentBuyerAmendInfo_Inactive_1 { get; set; }
        public int QBillingDocumentBuyerAmendInfo_Row_1 { get; set; }
        [BookmarkMapping("NewBillingDocConDescription2ndDoc_1st")]
        public string QBillingDocumentBuyerAmendInfo_DocumentType_2 { get; set; }
        public bool QBillingDocumentBuyerAmendInfo_Inactive_2 { get; set; }
        public int QBillingDocumentBuyerAmendInfo_Row_2 { get; set; }
        [BookmarkMapping("NewBillingDocConDescription3rdDoc_1st")]
        public string QBillingDocumentBuyerAmendInfo_DocumentType_3 { get; set; }
        public bool QBillingDocumentBuyerAmendInfo_Inactive_3 { get; set; }
        public int QBillingDocumentBuyerAmendInfo_Row_3 { get; set; }
        [BookmarkMapping("NewBillingDocConDescription4thDoc_1st")]
        public string QBillingDocumentBuyerAmendInfo_DocumentType_4 { get; set; }
        public bool QBillingDocumentBuyerAmendInfo_Inactive_4 { get; set; }
        public int QBillingDocumentBuyerAmendInfo_Row_4 { get; set; }
        [BookmarkMapping("NewBillingDocConDescription5thDoc_1st")]
        public string QBillingDocumentBuyerAmendInfo_DocumentType_5 { get; set; }
        public bool QBillingDocumentBuyerAmendInfo_Inactive_5 { get; set; }
        public int QBillingDocumentBuyerAmendInfo_Row_5 { get; set; }
        [BookmarkMapping("NewBillingDocConDescription6thDoc_1st")]
        public string QBillingDocumentBuyerAmendInfo_DocumentType_6 { get; set; }
        public bool QBillingDocumentBuyerAmendInfo_Inactive_6 { get; set; }
        public int QBillingDocumentBuyerAmendInfo_Row_6 { get; set; }
        [BookmarkMapping("NewBillingDocConDescription7thDoc_1st")]
        public string QBillingDocumentBuyerAmendInfo_DocumentType_7 { get; set; }
        public bool QBillingDocumentBuyerAmendInfo_Inactive_7 { get; set; }
        public int QBillingDocumentBuyerAmendInfo_Row_7 { get; set; }
        [BookmarkMapping("NewBillingDocConDescription8thDoc_1st")]
        public string QBillingDocumentBuyerAmendInfo_DocumentType_8 { get; set; }
        public bool QBillingDocumentBuyerAmendInfo_Inactive_8 { get; set; }
        public int QBillingDocumentBuyerAmendInfo_Row_8 { get; set; }
        [BookmarkMapping("NewBillingDocConDescription9thDoc_1st")]
        public string QBillingDocumentBuyerAmendInfo_DocumentType_9 { get; set; }
        public bool QBillingDocumentBuyerAmendInfo_Inactive_9 { get; set; }
        public int QBillingDocumentBuyerAmendInfo_Row_9 { get; set; }
        [BookmarkMapping("NewBillingDocConDescription10thDoc_1st")]
        public string QBillingDocumentBuyerAmendInfo_DocumentType_10 { get; set; }
        public bool QBillingDocumentBuyerAmendInfo_Inactive_10 { get; set; }
        public int QBillingDocumentBuyerAmendInfo_Row_10 { get; set; }
        /////////////////////////////////////////
        [BookmarkMapping("NewReceiptDocConDescription1stDoc_1st")]
        public string QReceiptDocumentBuyerAmendInfo_DocumentType_1 { get; set; }
        public bool QReceiptDocumentBuyerAmendInfo_Inactive_1 { get; set; }
        public int QReceiptDocumentBuyerAmendInfo_Row_1 { get; set; }
        [BookmarkMapping("NewReceiptDocConDescription2ndDoc_1st")]
        public string QReceiptDocumentBuyerAmendInfo_DocumentType_2 { get; set; }
        public bool QReceiptDocumentBuyerAmendInfo_Inactive_2 { get; set; }
        public int QReceiptDocumentBuyerAmendInfo_Row_2 { get; set; }
        [BookmarkMapping("NewReceiptDocConDescription3rdDoc_1st")]
        public string QReceiptDocumentBuyerAmendInfo_DocumentType_3 { get; set; }
        public bool QReceiptDocumentBuyerAmendInfo_Inactive_3 { get; set; }
        public int QReceiptDocumentBuyerAmendInfo_Row_3 { get; set; }
        [BookmarkMapping("NewReceiptDocConDescription4thDoc_1st")]
        public string QReceiptDocumentBuyerAmendInfo_DocumentType_4 { get; set; }
        public bool QReceiptDocumentBuyerAmendInfo_Inactive_4 { get; set; }
        public int QReceiptDocumentBuyerAmendInfo_Row_4 { get; set; }
        [BookmarkMapping("NewReceiptDocConDescription5thDoc_1st")]
        public string QReceiptDocumentBuyerAmendInfo_DocumentType_5 { get; set; }
        public bool QReceiptDocumentBuyerAmendInfo_Inactive_5 { get; set; }
        public int QReceiptDocumentBuyerAmendInfo_Row_5 { get; set; }
        [BookmarkMapping("NewReceiptDocConDescription6thDoc_1st")]
        public string QReceiptDocumentBuyerAmendInfo_DocumentType_6 { get; set; }
        public bool QReceiptDocumentBuyerAmendInfo_Inactive_6 { get; set; }
        public int QReceiptDocumentBuyerAmendInfo_Row_6 { get; set; }
        [BookmarkMapping("NewReceiptDocConDescription7thDoc_1st")]
        public string QReceiptDocumentBuyerAmendInfo_DocumentType_7 { get; set; }
        public bool QReceiptDocumentBuyerAmendInfo_Inactive_7 { get; set; }
        public int QReceiptDocumentBuyerAmendInfo_Row_7 { get; set; }
        [BookmarkMapping("NewReceiptDocConDescription8thDoc_1st")]
        public string QReceiptDocumentBuyerAmendInfo_DocumentType_8 { get; set; }
        public bool QReceiptDocumentBuyerAmendInfo_Inactive_8 { get; set; }
        public int QReceiptDocumentBuyerAmendInfo_Row_8 { get; set; }
        [BookmarkMapping("NewReceiptDocConDescription9thDoc_1st")]
        public string QReceiptDocumentBuyerAmendInfo_DocumentType_9 { get; set; }
        public bool QReceiptDocumentBuyerAmendInfo_Inactive_9 { get; set; }
        public int QReceiptDocumentBuyerAmendInfo_Row_9 { get; set; }
        [BookmarkMapping("NewReceiptDocConDescription10thDoc_1st")]
        public string QReceiptDocumentBuyerAmendInfo_DocumentType_10 { get; set; }
        public bool QReceiptDocumentBuyerAmendInfo_Inactive_10 { get; set; }
        public int QReceiptDocumentBuyerAmendInfo_Row_10 { get; set; }
        /////////////////////////////////////////
        [BookmarkMapping("BuyerFinYearFirst1_1st_1", "BuyerFinYearFirst2_1st_1")]
        public int QCreditAppReqLineFinBuyerAmendInfo_Year_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalFirst1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_RegisteredCapital_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalFirst1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_PaidCapital_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetFirst1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalAsset_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilityFirst1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalLiability_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquityFirst1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalEquity_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueFirst1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalRevenue_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSFirst1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalCOGS_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitFirst1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalGrossProfit_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpFirst1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalOperExpFirst_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitFirst1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalNetProfitFirst_1 { get; set; }
        [BookmarkMapping( "BuyerFinNetProfitPercent1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_NetProfitPercent_1 { get; set; }
        [BookmarkMapping("BuyerFinlDE1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_lDE_1 { get; set; }
        [BookmarkMapping( "BuyerFinQuickRatio1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_QuickRatio_1 { get; set; }
        [BookmarkMapping("BuyerFinIntCoverageRatio1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_IntCoverageRatio_1 { get; set; }
        public int QCreditAppReqLineFinBuyerAmendInfo_Row_1 { get; set; }
        /////////////////////////////////////////
        [BookmarkMapping("BuyerFinYearSecond1_1st_1", "BuyerFinYearSecond2_1st_1")]
        public int QCreditAppReqLineFinBuyerAmendInfo_Year_2 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalSecond1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_RegisteredCapital_2 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalSecond1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_PaidCapital_2 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetSecond1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalAsset_2 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilitySecond1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalLiability_2 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquitySecond1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalEquity_2 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueSecond1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalRevenue_2 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSSecond1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalCOGS_2 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitSecond1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalGrossProfit_2 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpSecond1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalOperExpFirst_2 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitSecond1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalNetProfitFirst_2 { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendInfo_NetProfitPercent_2 { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendInfo_lDE_2 { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendInfo_QuickRatio_2 { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendInfo_IntCoverageRatio_2 { get; set; }
        public int QCreditAppReqLineFinBuyerAmendInfo_Row_2 { get; set; }

        //////////////////////////////////////////
        [BookmarkMapping("BuyerFinYearThird1_1st_1", "BuyerFinYearThird2_1st_1")]
        public int QCreditAppReqLineFinBuyerAmendInfo_Year_3 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalThird1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_RegisteredCapital_3 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalThird1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_PaidCapital_3 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetThird1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalAsset_3 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilityThird1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalLiability_3 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquityThird1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalEquity_3 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueThird1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalRevenue_3 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSThird1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalCOGS_3 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitThird1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalGrossProfit_3 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpThird1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalOperExpFirst_3 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitThird1_1st_1")]
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalNetProfitFirst_3 { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendInfo_NetProfitPercent_3 { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendInfo_lDE_3 { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendInfo_QuickRatio_3 { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendInfo_IntCoverageRatio_3 { get; set; }
        public int QCreditAppReqLineFinBuyerAmendInfo_Row_3 { get; set; }
        //////////

        public string BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive1 { get; set; }

        public string BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive2 { get; set; }

        public string BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive3 { get; set; }

        public string BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive4 { get; set; }

        public string BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive5 { get; set; }

        public string BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive6 { get; set; }

        public string BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive7 { get; set; }

        public string BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive8 { get; set; }

        public string BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive9 { get; set; }

        public string BookmarkDocumentQueryBuyerAmendInformation_NewBillingDocConInactive10 { get; set; }

        /////////////////////////\
        public string BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive1 { get; set; }
        public string BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive2 { get; set; }
        public string BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive3 { get; set; }
        public string BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive4 { get; set; }
        public string BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive5 { get; set; }
        public string BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive6 { get; set; }
        public string BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive7 { get; set; }
        public string BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive8 { get; set; }
        public string BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive9 { get; set; }
        public string BookmarkDocumentQueryBuyerAmendInformation_NewReceiptDocConInactive10 { get; set; }
        [BookmarkMapping("OldAssignment")]
        public string QCreditAppRequestLineAmendBuyerAmendInfo_OldAssignment { get; set; }
        [BookmarkMapping("OldMethodOfPayment")]
        public string QCreditAppRequestLineAmendBuyerAmendInfo_OriginalMethodOfPayment { get; set; }
        [BookmarkMapping("KYCHeaderAmend")]
        public string QCreditAppRequestLineBuyerAmendInfo_KYCHeaderAmend { get; set; }
        [BookmarkMapping("NewAssignment")]
        public string QCreditAppRequestLineBuyerAmendInfo_NewAssignment { get; set; }
        [BookmarkMapping("NewMethodOfPayment")]
        public string QCreditAppRequestLineBuyerAmendInfo_NewMethodOfPayment { get; set; }
        [BookmarkMapping("SumBusinessCollateralValue")]
        public decimal QSumCreditAppReqBusinessCollateral_SumBusinessCollateralValue { get; set; }
        [BookmarkMapping("SumAssignmentBuyerAgreementAmount")]

        public decimal QSumCreditAppReqAssignment_SumBuyerAgreementAmount { get; set; }
        [BookmarkMapping("SumAssignmentAgreementAmount")]

        public decimal QSumCreditAppReqAssignment_SumAssignmentAgreementAmount { get; set; }
        [BookmarkMapping("SumRemainingAmount")]

        public decimal QSumCreditAppReqAssignment_SumRemainingAmount { get; set; }


        #region QCreditAppReqAssignment
        [BookmarkMapping("AssignmentBuyer1")]
        public string QCreditAppReqAssignment_AssignmentBuyer_1 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId1")]
        public string QCreditAppReqAssignment_AssignmentRefAgreementId_1 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount1")]
        public decimal QCreditAppReqAssignment_AssignmentBuyerAgreementAmount_1 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount1")]
        public decimal QCreditAppReqAssignment_AssignmentAgreementAmount_1 { get; set; }
        [BookmarkMapping("AssignmentAgreementDate1")]
        public DateTime? QCreditAppReqAssignment_AssignmentAgreementDate_1 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount1")]
        public decimal QCreditAppReqAssignment_RemainingAmount_1 { get; set; }

        [BookmarkMapping("AssignmentBuyer2")]
        public string QCreditAppReqAssignment_AssignmentBuyer_2 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId2")]
        public string QCreditAppReqAssignment_AssignmentRefAgreementId_2 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount2")]
        public decimal QCreditAppReqAssignment_AssignmentBuyerAgreementAmount_2 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount2")]
        public decimal QCreditAppReqAssignment_AssignmentAgreementAmount_2 { get; set; }
        [BookmarkMapping("AssignmentAgreementDate2")]
        public DateTime? QCreditAppReqAssignment_AssignmentAgreementDate_2 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount2")]
        public decimal QCreditAppReqAssignment_RemainingAmount_2 { get; set; }

        [BookmarkMapping("AssignmentBuyer3")]
        public string QCreditAppReqAssignment_AssignmentBuyer_3 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId3")]
        public string QCreditAppReqAssignment_AssignmentRefAgreementId_3 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount3")]
        public decimal QCreditAppReqAssignment_AssignmentBuyerAgreementAmount_3 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount3")]
        public decimal QCreditAppReqAssignment_AssignmentAgreementAmount_3 { get; set; }
        [BookmarkMapping("AssignmentAgreementDate3")]
        public DateTime? QCreditAppReqAssignment_AssignmentAgreementDate_3 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount3")]
        public decimal QCreditAppReqAssignment_RemainingAmount_3 { get; set; }


        [BookmarkMapping("AssignmentBuyer4")]
        public string QCreditAppReqAssignment_AssignmentBuyer_4 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId4")]
        public string QCreditAppReqAssignment_AssignmentRefAgreementId_4 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount4")]
        public decimal QCreditAppReqAssignment_AssignmentBuyerAgreementAmount_4 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount4")]
        public decimal QCreditAppReqAssignment_AssignmentAgreementAmount_4 { get; set; }
        [BookmarkMapping("AssignmentAgreementDate4")]
        public DateTime? QCreditAppReqAssignment_AssignmentAgreementDate_4 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount4")]
        public decimal QCreditAppReqAssignment_RemainingAmount_4 { get; set; }

        [BookmarkMapping("AssignmentBuyer5")]
        public string QCreditAppReqAssignment_AssignmentBuyer_5 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId5")]
        public string QCreditAppReqAssignment_AssignmentRefAgreementId_5 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount5")]
        public decimal QCreditAppReqAssignment_AssignmentBuyerAgreementAmount_5 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount5")]
        public decimal QCreditAppReqAssignment_AssignmentAgreementAmount_5 { get; set; }
        [BookmarkMapping("AssignmentAgreementDate5")]
        public DateTime? QCreditAppReqAssignment_AssignmentAgreementDate_5 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount5")]
        public decimal QCreditAppReqAssignment_RemainingAmount_5 { get; set; }

        [BookmarkMapping("AssignmentBuyer6")]
        public string QCreditAppReqAssignment_AssignmentBuyer_6 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId6")]
        public string QCreditAppReqAssignment_AssignmentRefAgreementId_6 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount6")]
        public decimal QCreditAppReqAssignment_AssignmentBuyerAgreementAmount_6 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount6")]
        public decimal QCreditAppReqAssignment_AssignmentAgreementAmount_6 { get; set; }
        [BookmarkMapping("AssignmentAgreementDate6")]
        public DateTime? QCreditAppReqAssignment_AssignmentAgreementDate_6 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount6")]
        public decimal QCreditAppReqAssignment_RemainingAmount_6 { get; set; }

        [BookmarkMapping("AssignmentBuyer7")]
        public string QCreditAppReqAssignment_AssignmentBuyer_7 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId7")]
        public string QCreditAppReqAssignment_AssignmentRefAgreementId_7 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount7")]
        public decimal QCreditAppReqAssignment_AssignmentBuyerAgreementAmount_7 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount7")]
        public decimal QCreditAppReqAssignment_AssignmentAgreementAmount_7 { get; set; }
        [BookmarkMapping("AssignmentAgreementDate7")]
        public DateTime? QCreditAppReqAssignment_AssignmentAgreementDate_7 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount7")]
        public decimal QCreditAppReqAssignment_RemainingAmount_7 { get; set; }

        [BookmarkMapping("AssignmentBuyer8")]
        public string QCreditAppReqAssignment_AssignmentBuyer_8 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId8")]
        public string QCreditAppReqAssignment_AssignmentRefAgreementId_8 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount8")]
        public decimal QCreditAppReqAssignment_AssignmentBuyerAgreementAmount_8 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount8")]
        public decimal QCreditAppReqAssignment_AssignmentAgreementAmount_8 { get; set; }
        [BookmarkMapping("AssignmentAgreementDate8")]
        public DateTime? QCreditAppReqAssignment_AssignmentAgreementDate_8 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount8")]
        public decimal QCreditAppReqAssignment_RemainingAmount_8 { get; set; }

        [BookmarkMapping("AssignmentBuyer9")]
        public string QCreditAppReqAssignment_AssignmentBuyer_9 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId9")]
        public string QCreditAppReqAssignment_AssignmentRefAgreementId_9 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount9")]
        public decimal QCreditAppReqAssignment_AssignmentBuyerAgreementAmount_9 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount9")]
        public decimal QCreditAppReqAssignment_AssignmentAgreementAmount_9 { get; set; }
        [BookmarkMapping("AssignmentAgreementDate9")]
        public DateTime? QCreditAppReqAssignment_AssignmentAgreementDate_9 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount9")]
        public decimal QCreditAppReqAssignment_RemainingAmount_9 { get; set; }

        [BookmarkMapping("AssignmentBuyer10")]
        public string QCreditAppReqAssignment_AssignmentBuyer_10 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId10")]
        public string QCreditAppReqAssignment_AssignmentRefAgreementId_10 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount10")]
        public decimal QCreditAppReqAssignment_AssignmentBuyerAgreementAmount_10 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount10")]
        public decimal QCreditAppReqAssignment_AssignmentAgreementAmount_10 { get; set; }
        [BookmarkMapping("AssignmentAgreementDate10")]
        public DateTime? QCreditAppReqAssignment_AssignmentAgreementDate_10 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount10")]
        public decimal QCreditAppReqAssignment_RemainingAmount_10 { get; set; }


        #endregion QCreditAppReqAssignment

        #region QCreditAppReqBusinessCollateral
        [BookmarkMapping("BusinessCollateralType1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralType_1 { get; set; }
        [BookmarkMapping("BusinessCollateralSubType1")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralSubType_1 { get; set; }
        [BookmarkMapping("BusinessCollateralDesc1")]
        public string QCreditAppReqBusinessCollateral_Description_1 { get; set; }
        [BookmarkMapping("BusinessCollateralValue1")]
        public decimal QCreditAppReqBusinessCollateral_BusinessCollateralValue_1 { get; set; }

        [BookmarkMapping("BusinessCollateralType2")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralType_2 { get; set; }
        [BookmarkMapping("BusinessCollateralSubType2")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralSubType_2 { get; set; }
        [BookmarkMapping("BusinessCollateralDesc2")]
        public string QCreditAppReqBusinessCollateral_Description_2 { get; set; }
        [BookmarkMapping("BusinessCollateralValue2")]
        public decimal QCreditAppReqBusinessCollateral_BusinessCollateralValue_2 { get; set; }

        [BookmarkMapping("BusinessCollateralType3")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralType_3 { get; set; }
        [BookmarkMapping("BusinessCollateralSubType3")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralSubType_3 { get; set; }
        [BookmarkMapping("BusinessCollateralDesc3")]
        public string QCreditAppReqBusinessCollateral_Description_3 { get; set; }
        [BookmarkMapping("BusinessCollateralValue3")]
        public decimal QCreditAppReqBusinessCollateral_BusinessCollateralValue_3 { get; set; }

        [BookmarkMapping("BusinessCollateralType4")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralType_4 { get; set; }
        [BookmarkMapping("BusinessCollateralSubType4")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralSubType_4 { get; set; }
        [BookmarkMapping("BusinessCollateralDesc4")]
        public string QCreditAppReqBusinessCollateral_Description_4 { get; set; }
        [BookmarkMapping("BusinessCollateralValue4")]
        public decimal QCreditAppReqBusinessCollateral_BusinessCollateralValue_4 { get; set; }

        [BookmarkMapping("BusinessCollateralType5")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralType_5 { get; set; }
        [BookmarkMapping("BusinessCollateralSubType5")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralSubType_5 { get; set; }
        [BookmarkMapping("BusinessCollateralDesc5")]
        public string QCreditAppReqBusinessCollateral_Description_5 { get; set; }
        [BookmarkMapping("BusinessCollateralValue5")]
        public decimal QCreditAppReqBusinessCollateral_BusinessCollateralValue_5 { get; set; }

        [BookmarkMapping("BusinessCollateralType6")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralType_6 { get; set; }
        [BookmarkMapping("BusinessCollateralSubType6")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralSubType_6 { get; set; }
        [BookmarkMapping("BusinessCollateralDesc6")]
        public string QCreditAppReqBusinessCollateral_Description_6 { get; set; }
        [BookmarkMapping("BusinessCollateralValue6")]
        public decimal QCreditAppReqBusinessCollateral_BusinessCollateralValue_6 { get; set; }

        [BookmarkMapping("BusinessCollateralType7")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralType_7 { get; set; }
        [BookmarkMapping("BusinessCollateralSubType7")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralSubType_7 { get; set; }
        [BookmarkMapping("BusinessCollateralDesc7")]
        public string QCreditAppReqBusinessCollateral_Description_7 { get; set; }
        [BookmarkMapping("BusinessCollateralValue7")]
        public decimal QCreditAppReqBusinessCollateral_BusinessCollateralValue_7 { get; set; }

        [BookmarkMapping("BusinessCollateralType8")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralType_8 { get; set; }
        [BookmarkMapping("BusinessCollateralSubType8")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralSubType_8 { get; set; }
        [BookmarkMapping("BusinessCollateralDesc8")]
        public string QCreditAppReqBusinessCollateral_Description_8 { get; set; }
        [BookmarkMapping("BusinessCollateralValue8")]
        public decimal QCreditAppReqBusinessCollateral_BusinessCollateralValue_8 { get; set; }

        [BookmarkMapping("BusinessCollateralType9")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralType_9 { get; set; }
        [BookmarkMapping("BusinessCollateralSubType9")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralSubType_9 { get; set; }
        [BookmarkMapping("BusinessCollateralDesc9")]
        public string QCreditAppReqBusinessCollateral_Description_9 { get; set; }
        [BookmarkMapping("BusinessCollateralValue9")]
        public decimal QCreditAppReqBusinessCollateral_BusinessCollateralValue_9 { get; set; }

        [BookmarkMapping("BusinessCollateralType10")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralType_10 { get; set; }
        [BookmarkMapping("BusinessCollateralSubType10")]
        public string QCreditAppReqBusinessCollateral_BusinessCollateralSubType_10 { get; set; }
        [BookmarkMapping("BusinessCollateralDesc10")]
        public string QCreditAppReqBusinessCollateral_Description_10 { get; set; }
        [BookmarkMapping("BusinessCollateralValue10")]
        public decimal QCreditAppReqBusinessCollateral_BusinessCollateralValue_10 { get; set; }

        #endregion QCreditAppReqBusinessCollateral

        #region QBillingDocumentOld
        [BookmarkMapping("OldBillingDocConDescriptionDoc_1")]
        public string QBillingDocumentOld_DocumentType_1 { get; set; }
        [BookmarkMapping("OldBillingDocConDescriptionDoc_2")]
        public string QBillingDocumentOld_DocumentType_2 { get; set; }
        [BookmarkMapping("OldBillingDocConDescriptionDoc_3")]
        public string QBillingDocumentOld_DocumentType_3 { get; set; }
        [BookmarkMapping("OldBillingDocConDescriptionDoc_4")]
        public string QBillingDocumentOld_DocumentType_4 { get; set; }
        [BookmarkMapping("OldBillingDocConDescriptionDoc_5")]
        public string QBillingDocumentOld_DocumentType_5 { get; set; }

        public string QBillingDocumentOld_DocumentType_6 { get; set; }
        public string QBillingDocumentOld_DocumentType_7 { get; set; }
        public string QBillingDocumentOld_DocumentType_8 { get; set; }
        public string QBillingDocumentOld_DocumentType_9 { get; set; }
        public string QBillingDocumentOld_DocumentType_10 { get; set; }
        #endregion QBillingDocumentOld

        #region QReceiptDocumentOld
        [BookmarkMapping("OldReceiptDocConDescriptionDoc_1")]
        public string QReceiptDocumentOld_DocumentType_1 { get; set; }
        [BookmarkMapping("OldReceiptDocConDescriptionDoc_2")]
        public string QReceiptDocumentOld_DocumentType_2 { get; set; }
        [BookmarkMapping("OldReceiptDocConDescriptionDoc_3")]
        public string QReceiptDocumentOld_DocumentType_3 { get; set; }
        [BookmarkMapping("OldReceiptDocConDescriptionDoc_4")]
        public string QReceiptDocumentOld_DocumentType_4 { get; set; }
        [BookmarkMapping("OldReceiptDocConDescriptionDoc_5")]
        public string QReceiptDocumentOld_DocumentType_5 { get; set; }
        public string QReceiptDocumentOld_DocumentType_6 { get; set; }
        public string QReceiptDocumentOld_DocumentType_7 { get; set; }
        public string QReceiptDocumentOld_DocumentType_8 { get; set; }
        public string QReceiptDocumentOld_DocumentType_9 { get; set; }
        public string QReceiptDocumentOld_DocumentType_10 { get; set; }
        #endregion QReceiptDocumentOld

        [BookmarkMapping("AssignmentNew1")]
        public string  Variable_AssignmentNew1 { get; set; }
        [BookmarkMapping("AssignmentNew2")]
        public string Variable_AssignmentNew2 { get; set; }
        [BookmarkMapping("AssignmentNew3")]
        public string Variable_AssignmentNew3 { get; set; }
        [BookmarkMapping("AssignmentNew4")]
        public string Variable_AssignmentNew4 { get; set; }
        [BookmarkMapping("AssignmentNew5")]
        public string Variable_AssignmentNew5 { get; set; }
        [BookmarkMapping("AssignmentNew6")]
        public string Variable_AssignmentNew6 { get; set; }
        [BookmarkMapping("AssignmentNew7")]
        public string Variable_AssignmentNew7 { get; set; }
        [BookmarkMapping("AssignmentNew8")]
        public string Variable_AssignmentNew8 { get; set; }
        [BookmarkMapping("AssignmentNew9")]
        public string Variable_AssignmentNew9 { get; set; }
        [BookmarkMapping("AssignmentNew10")]
        public string Variable_AssignmentNew10 { get; set; }
        [BookmarkMapping("BusinessCollateralNew1")]
        public string Variable_BusinessCollateralIsNew1 { get; set; }
        [BookmarkMapping("BusinessCollateralNew2")]
        public string Variable_BusinessCollateralIsNew2 { get; set; }
        [BookmarkMapping("BusinessCollateralNew3")]
        public string Variable_BusinessCollateralIsNew3 { get; set; }
        [BookmarkMapping("BusinessCollateralNew4")]
        public string Variable_BusinessCollateralIsNew4 { get; set; }
        [BookmarkMapping("BusinessCollateralNew5")]
        public string Variable_BusinessCollateralIsNew5 { get; set; }
        [BookmarkMapping("BusinessCollateralNew6")]
        public string Variable_BusinessCollateralIsNew6 { get; set; }
        [BookmarkMapping("BusinessCollateralNew7")]
        public string Variable_BusinessCollateralIsNew7 { get; set; }
        [BookmarkMapping("BusinessCollateralNew8")]
        public string Variable_BusinessCollateralIsNew8 { get; set; }
        [BookmarkMapping("BusinessCollateralNew9")]
        public string Variable_BusinessCollateralIsNew9 { get; set; }
        [BookmarkMapping("BusinessCollateralNew10")]
        public string Variable_BusinessCollateralIsNew10 { get; set; }
        [BookmarkMapping("BuyerAverageSalesPerMonth")]
        public decimal Variable_BuyerAverageSalesPerMonth { get; set; }

        
    }
    public class QCreditAppRequestTableBuyerAmendInfo
    {
        public string QCreditAppRequestTableBuyerAmendInfo_CreditAppRequestId { get; set; }
        public string QCreditAppRequestTableBuyerAmendInfo_Description { get; set; }
        public Guid? QCreditAppRequestTableBuyerAmendInfo_RefCreditAppTableGUID { get; set; }
        public string QCreditAppRequestTableBuyerAmendInfo_OriginalCreditAppTableId { get; set; }
        public Guid QCreditAppRequestTableBuyerAmendInfo_OriginalRefCreditAppRequestTableGUID { get; set; }
        public Guid? QCreditAppRequestTableBuyerAmendInfo_DocumentStatusGUID { get; set; }
        public string QCreditAppRequestTableBuyerAmendInfo_Status { get; set; }
        public DateTime? QCreditAppRequestTableBuyerAmendInfo_RequestDate { get; set; }
        public Guid? QCreditAppRequestTableBuyerAmendInfo_CustomerTableGUID { get; set; }
        public string QCreditAppRequestTableBuyerAmendInfo_Remark { get; set; }
    }
    public class QCustomerBuyerAmendInfo
    {
        public string QCustomerBuyerAmendInfo_CustomerId { get; set; }

        public string QCustomerBuyerAmendInfo_CustomerName { get; set; }

        public Guid? QCustomerBuyerAmendInfo_ResponsibleByGUID { get; set; }

        public string QCustomerBuyerAmendInfo_ResponsibleBy { get; set; }
    }
    public class QCreditAppRequestLineBuyerAmendInfo
    {
        public Guid? QCreditAppRequestLineBuyerAmendInfo_BusinessSegmentGUID { get; set; }
        public string QCreditAppRequestLineBuyerAmendInfo_BusinessSegment { get; set; }
        public string QCreditAppRequestLineBuyerAmendInfo_BuyerName { get; set; }
        public Guid? QCreditAppRequestTableBuyerAmendInfo_BuyerTableGUID { get; set; }

        public string QCreditAppRequestLineBuyerAmendInfo_TaxId { get; set; }
        public Guid? QCreditAppRequestLineBuyerAmendInfo_InvoiceAddressGUID { get; set; }
        public string QCreditAppRequestLineBuyerAmendInfo_InvoiceAddress { get; set; }
        public DateTime? QCreditAppRequestLineBuyerAmendInfo_DateOfEstablish { get; set; }
        public Guid? QCreditAppRequestLineBuyerAmendInfo_LineOfBusinessGUID { get; set; }
        public string QCreditAppRequestLineBuyerAmendInfo_LineOfBusiness { get; set; }
        public Guid? QCreditAppRequestLineBuyerAmendInfo_BlacklistStatusGUID { get; set; }
        public string QCreditAppRequestLineBuyerAmendInfo_BlacklistStatus { get; set; }
        public Guid? QCreditAppRequestLineBuyerAmendInfo_CreditScoringGUID { get; set; }
        public string QCreditAppRequestLineBuyerAmendInfo_CreditScoring { get; set; }
        public decimal? QCreditAppRequestLineBuyerAmendInfo_MaxPurchasePct { get; set; }
        public decimal? QCreditAppRequestLineBuyerAmendInfo_PurchaseFeePct { get; set; }
        public int QCreditAppRequestLineBuyerAmendInfo_PurchaseFeeCalculateBase { get; set; }
        public Guid? QCreditAppRequestLineBuyerAmendInfo_BillingAddressGUID { get; set; }
        public string QCreditAppRequestLineBuyerAmendInfo_BillingAddress { get; set; }
        public string QCreditAppRequestLineBuyerAmendInfo_ReceiptRemark { get; set; }
        public string QCreditAppRequestLineBuyerAmendInfo_MarketingComment { get; set; }
        public string QCreditAppRequestLineBuyerAmendInfo_CreditComment { get; set; }
        public string QCreditAppRequestLineBuyerAmendInfo_ApproverComment { get; set; }
        public string QCreditAppRequestLineBuyerAmendInfo_ReceiptAddress { get; set; }
        public Guid? QCreditAppRequestLineBuyerAmendInfo_CreditAppRequestLineGUID { get; set; }
        public string QCreditAppRequestLineBuyerAmendInfo_KYCHeaderAmend { get; set; }
        public string QCreditAppRequestLineBuyerAmendInfo_NewAssignment { get; set; }
        public string QCreditAppRequestLineBuyerAmendInfo_NewMethodOfPayment { get; set; }

    }
    public class QCreditAppRequestLineAmendBuyerAmendInfo
    {
        public decimal QCreditAppRequestLineAmendBuyerAmendInfo_OriginalMaxPurchasePct { get; set; }
        public decimal QCreditAppRequestLineAmendBuyerAmendInfo_OriginalPurchaseFeePct { get; set; }
        public int QCreditAppRequestLineAmendBuyerAmendInfo_OriginalPurchaseFeeCalculateBase { get; set; }
        public Guid? QCreditAppRequestLineAmendBuyerAmendInfo_OriginalBillingAddressGUID { get; set; }
        public string QCreditAppRequestLineAmendBuyerAmendInfo_OriginalBillingAddress { get; set; }
        public Guid? QCreditAppRequestLineAmendBuyerAmendInfo_OriginalReceiptAddressGUID { get; set; }
        public string QCreditAppRequestLineAmendBuyerAmendInfo_OriginalReceiptAddress { get; set; }
        public string QCreditAppRequestLineAmendBuyerAmendInfo_OriginalReceiptRemark { get; set; }
        public string QCreditAppRequestLineAmendBuyerAmendInfo_OldAssignment { get; set; }
        public string QCreditAppRequestLineAmendBuyerAmendInfo_OriginalMethodOfPayment { get; set; }
    }
    public class QBillingDocumentBuyerAmendInfo
    {
        public string QBillingDocumentBuyerAmendInfo_DocumentType { get; set; }
        public bool QBillingDocumentBuyerAmendInfo_Inactive { get; set; }
        public int QBillingDocumentBuyerAmendInfo_Row { get; set; }
    }
    public class QReceiptDocumentBuyerAmendInfo
    {
        public string QReceiptDocumentBuyerAmendInfo_DocumentType { get; set; }
        public bool QReceiptDocumentBuyerAmendInfo_Inactive { get; set; }
        public int QReceiptDocumentBuyerAmendInfo_Row { get; set; }

    }
    public class QCreditAppReqLineFinBuyerAmendInfo
    {

        public int QCreditAppReqLineFinBuyerAmendInfo_Year { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendInfo_RegisteredCapital { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendInfo_PaidCapital { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalAsset { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalLiability { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalEquity { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalRevenue { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalCOGS { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalGrossProfit { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalOperExpFirst { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendInfo_TotalNetProfitFirst { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendInfo_NetProfitPercent { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendInfo_lDE { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendInfo_QuickRatio { get; set; }
        public decimal QCreditAppReqLineFinBuyerAmendInfo_IntCoverageRatio { get; set; }
        public int QCreditAppReqLineFinBuyerAmendInfo_Row { get; set; }
    }
    public class QCreditAppReqAssignmentBuyerAmendInformation
    {

        public bool QCreditAppReqAssignment_IsNew { get; set; }
        public Guid QCreditAppReqAssignment_BuyerTableGUID { get; set; }
        public string QCreditAppReqAssignment_AssignmentBuyer { get; set; }
        public Guid? QCreditAppReqAssignment_BuyerAgreementTableGUID { get; set; }
        public string QCreditAppReqAssignment_AssignmentRefAgreementId { get; set; }
        public decimal QCreditAppReqAssignment_AssignmentBuyerAgreementAmount { get; set; }
        public decimal QCreditAppReqAssignment_AssignmentAgreementAmount { get; set; }
        public Guid? QCreditAppReqAssignment_AssignmentAgreementTableGUID { get; set; }
        public DateTime? QCreditAppReqAssignment_AssignmentAgreementDate { get; set; }
        public decimal QCreditAppReqAssignment_RemainingAmount { get; set; }
        public int QCreditAppReqAssignment_Row { get; set; }

    }
    public class QSumCreditAppReqAssignmentBuyerAmendInformation
    {
        public decimal QSumCreditAppReqAssignment_SumBuyerAgreementAmount { get; set; }
        public decimal QSumCreditAppReqAssignment_SumAssignmentAgreementAmount { get; set; }
        public decimal QSumCreditAppReqAssignment_SumRemainingAmount { get; set; }

    }
    public class QCreditAppReqBusinessCollateralBuyerAmendInformation
    {
        public bool QCreditAppReqBusinessCollateral_IsNew { get; set; }
        public Guid QCreditAppReqBusinessCollateral_BusinessCollateralTypeGUID { get; set; }
        public string QCreditAppReqBusinessCollateral_BusinessCollateralType { get; set; }
        public Guid QCreditAppReqBusinessCollateral_BusinessCollateralSubTypeGUID { get; set; }
        public string QCreditAppReqBusinessCollateral_BusinessCollateralSubType { get; set; }
        public string QCreditAppReqBusinessCollateral_Description { get; set; }
        public decimal QCreditAppReqBusinessCollateral_BusinessCollateralValue { get; set; }
    }
    public class QSumCreditAppReqBusinessCollateralBuyerAmendInformation
    { 
        public decimal QSumCreditAppReqBusinessCollateral_SumBusinessCollateralValue { get; set; }
    }
    public class QOriginalRefCreditAppRequestLineBuyerAmendInformation
    {
        public Guid QOriginalRefCreditAppRequestLine_CreditAppRequestLineGUID { get; set; }
    }
    public class QBillingDocumentOldBuyerAmendInformation
    {
        public string QBillingDocumentOld_DocumentType { get; set; }
        public bool QBillingDocumentOld_Inactive { get; set; }
    }
    public class QReceiptDocumentOldBuyerAmendInformation
    {
        public string QReceiptDocumentOld_DocumentType { get; set; }
        public bool QReceiptDocumentOld_Inactive { get; set; }
    }


}

