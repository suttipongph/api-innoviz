using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BookmarkDocumentTransListView : ViewCompanyBaseEntity
	{
		public string BookmarkDocumentTransGUID { get; set; }
		public string BookmarkDocumentGUID { get; set; }
		public string DocumentTemplateTableGUID { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string ReferenceExternalId { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string BookmarkDocument_Values { get; set; }
		public string DocumentTemplateTable_Values { get; set; }
		public string BookmarkDocument_BookmarkDocumentId { get; set; }
		public string DocumentTemplateTable_BookmarkDocumentTemplateId { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string DocumentStatus_Description { get; set; }
		public string DocumentTemplateTable_TemplateId { get; set; }
		public int BookmarkDocument_BookmarkDocumentRefType { get; set; }
		public int BookmarkDocument_DocumentTemplateType { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public string BookmarkDocumentId { get; set; }
	}
}
