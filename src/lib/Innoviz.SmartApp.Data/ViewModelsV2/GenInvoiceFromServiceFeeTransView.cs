﻿using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class GenInvoiceFromServiceFeeTransParamView
    {
        public List<ServiceFeeTrans> ServiceFeeTrans{ get; set; }
        public DateTime IssuedDate { get; set; }
        public Guid CustomerTableGUID {get;set;}
        public ProductType ProductType { get; set; }
        public string DocumentId { get; set; }
        public Guid? CreditAppTableGUID { get; set; }
    }
    public class GenInvoiceFromServiceFeeTransResultView
    {
        public List<InvoiceTable> InvoiceTable { get; set; } = new List<InvoiceTable>();
        public List<InvoiceLine> InvoiceLine { get; set; } = new List<InvoiceLine>();
        public List<IntercompanyInvoiceTable> IntercompanyInvoiceTable { get; set; } = new List<IntercompanyInvoiceTable>(); 
    }
}
