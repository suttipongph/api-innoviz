﻿using Innoviz.SmartApp.Core.Attributes;
using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
   public class BuyerReviewCreditLimitBookmarkView : ViewCompanyBaseEntity
    {
        [BookmarkMapping("CANumber")]
        public string CANumber { get; set; }
        [BookmarkMapping("RefCANumber")]
        public string RefCANumber { get; set; }
        [BookmarkMapping("CAStatus")]
        public string CAStatus { get; set; }
        [BookmarkMapping("CACreateDate")]
        public DateTime CACreateDate { get; set; }

        [BookmarkMapping("CustomerName1")]
        public string CustomerName1 { get; set; }
        [BookmarkMapping("CustomerCode")]
        public string CustomerCode { get; set; }
        [BookmarkMapping("SalesResp")]
        public string SalesResp { get; set; }
        [BookmarkMapping("KYCHeader")]
        public string KYCHeader { get; set; }
        [BookmarkMapping("CreditScoring")]
        public string CreditScoring { get; set; }
        [BookmarkMapping("CADesp")]
        public string CADesp { get; set; }
        [BookmarkMapping("CARemark")]
        public string CARemark { get; set; }
        [BookmarkMapping("MarketingComment1st")]
        public string MarketingComment1st { get; set; }
        [BookmarkMapping("CreditComment1st")]
        public string CreditComment1st { get; set; }
        [BookmarkMapping("ApproverComment1st")]
        public string ApproverComment1st { get; set; }
        [BookmarkMapping("AcceptanceDocument1st_1")]
        public string AcceptanceDocument1st_1 { get; set; }
        [BookmarkMapping("AssignmentCondition1st_2")]
        public string AssignmentCondition1st_2 { get; set; }
        [BookmarkMapping("BillingDescriptiong1st_1")]
        public string BillingDescriptiong1st_1 { get; set; }
        [BookmarkMapping("BillingResponsible1st_2")]
        public string BillingResponsible1st_2 { get; set; }
        [BookmarkMapping("BusinessSegment1st_2")]
        public string BusinessSegment1st_2 { get; set; }
        [BookmarkMapping("BuyerAddress1st_1")]
        public string BuyerAddress1st_1 { get; set; }
        [BookmarkMapping("BuyerReceiptAddress1st_1")]
        public string BuyerReceiptAddress1st_1 { get; set; }
        [BookmarkMapping("BuyerBillingAddress1st_1")]
        public string BuyerBillingAddress1st_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPerson1st_1")]
        public string BuyerBillingContactPerson1st_1 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPerson1st_1")]
        public string BuyerReceiptContactPerson1st_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPersonPosition1st_1")]
        public string BuyerBillingContactPersonPosition1st_1 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPersonPosition1st_1")]
        public string BuyerReceiptContactPersonPosition1st_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPersonTel1st_1")]
        public string BuyerBillingContactPersonTel1st_1 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPersonTel1st_1")]
        public string BuyerReceiptContactPersonTel1st_1 { get; set; }
        [BookmarkMapping("BuyerBlacklistStatus1st_1")]
        public string BuyerBlacklistStatus1st_1 { get; set; }
        [BookmarkMapping("FactoringPurchaseFee1st_1")]
        public decimal FactoringPurchaseFee1st_1 { get; set; }
        [BookmarkMapping("FactoringPurchaseFeeCalBase1st_1")]
        public string FactoringPurchaseFeeCalBase1st_1 { get; set; }
        [BookmarkMapping("PaymentMethod1st_2")]
        public string PaymentMethod1st_2 { get; set; }
        [BookmarkMapping("PurchasePercent1st_2")]
        public decimal PurchasePercent1st_2 { get; set; }
        [BookmarkMapping("ReceiptDescriptiong1st_1")]
        public string ReceiptDescriptiong1st_1 { get; set; }
        [BookmarkMapping("PurchaseConditionCust")]
        public string PurchaseConditionCust { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer1st_2")]
        public decimal BuyerCreditLimitAllCustomer1st_2 { get; set; }
        [BookmarkMapping("BuyerName1st_1")]
        public string BuyerName1st_1 { get; set; }
        [BookmarkMapping("BuyerTaxID1st_1")]
        public string BuyerTaxID1st_1 { get; set; }
        [BookmarkMapping("BuyerCompanyRegisteredDate1st_1")]
        public DateTime? BuyerCompanyRegisteredDate1st_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest1st_2")]
        public decimal BuyerCreditLimitRequest1st_2 { get; set; }
        [BookmarkMapping("BuyerCreditScoring1st_1")]
        public string BuyerCreditScoring1st_1 { get; set; }
        [BookmarkMapping("BuyerFinRevenuePerMonth1_1st_1")]
        public decimal BuyerFinRevenuePerMonth1_1st_1 { get; set; }
        [BookmarkMapping("BuyerLineOfBusiness1st_1")]
        public string BuyerLineOfBusiness1st_1 { get; set; }
        [BookmarkMapping("CustPDCBankAccountNumber1st_1")]
        public string CustPDCBankAccountNumber1st_1 { get; set; }
        [BookmarkMapping("CustPDCBankAccountType1st_1")]
        public string CustPDCBankAccountType1st_1 { get; set; }
        [BookmarkMapping("CustPDCBankBranch1st_1")]
        public string CustPDCBankBranch1st_1 { get; set; }
        [BookmarkMapping("CustPDCBankName1st_1")]
        public string CustPDCBankName1st_1 { get; set; }
        [BookmarkMapping("BillingDocumentFirst1_1st_1")]
        public string BillingDocumentFirst1_1st_1 { get; set; }
        [BookmarkMapping("BillingDocumentSecond1_1st_1")]
        public string BillingDocumentSecond1_1st_1 { get; set; }
        [BookmarkMapping("BillingDocumentThird1_1st_1")]
        public string BillingDocumentThird1_1st_1 { get; set; }
        [BookmarkMapping("BillingDocumentFourth_1st_1")]
        public string BillingDocumentFourth_1st_1 { get; set; }
        [BookmarkMapping("BillingDocumentFifth_1st_1")]
        public string BillingDocumentFifth_1st_1 { get; set; }
        [BookmarkMapping("BillingDocumentSixth1_1st_1")]
        public string BillingDocumentSixth1_1st_1 { get; set; }
        [BookmarkMapping("BillingDocumentSeventh_1st_1")]
        public string BillingDocumentSeventh_1st_1 { get; set; }
        [BookmarkMapping("BillingDocumentEigth1_1st_1")]
        public string BillingDocumentEigth1_1st_1 { get; set; }
        [BookmarkMapping("BillingDocumentNineth1_1st_1")]
        public string BillingDocumentNineth1_1st_1 { get; set; }
        [BookmarkMapping("BillingDocumentTenth_1st_1")]
        public string BillingDocumentTenth_1st_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFirst1_1st_1")]
        public string ReceiptDocumentFirst1_1st_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSecond1_1st_1")]
        public string ReceiptDocumentSecond1_1st_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentThird1_1st_1")]
        public string ReceiptDocumentThird1_1st_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFourth_1st_1")]
        public string ReceiptDocumentFourth_1st_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFifth_1st_1")]
        public string ReceiptDocumentFifth_1st_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSixth1_1st_1")]
        public string ReceiptDocumentSixth1_1st_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSeventh_1st_1")]
        public string ReceiptDocumentSeventh_1st_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentEigth1_1st_1")]
        public string ReceiptDocumentEigth1_1st_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentNineth1_1st_1")]
        public string ReceiptDocumentNineth1_1st_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentTenth_1st_1")]
        public string ReceiptDocumentTenth_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalFirst1_1st_1")]
        public decimal BuyerFinRegisteredCapitalFirst1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalSecond1_1st_1")]
        public decimal BuyerFinRegisteredCapitalSecond1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalThird1_1st_1")]
        public decimal BuyerFinRegisteredCapitalThird1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalFirst1_1st_1")]
        public decimal BuyerFinPaidCapitalFirst1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalSecond1_1st_1")]
        public decimal BuyerFinPaidCapitalSecond1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalThird1_1st_1")]
        public decimal BuyerFinPaidCapitalThird1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetFirst1_1st_1")]
        public decimal BuyerFinTotalAssetFirst1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetSecond1_1st_1")]
        public decimal BuyerFinTotalAssetSecond1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetThird1_1st_1")]
        public decimal BuyerFinTotalAssetThird1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilityFirst1_1st_1")]
        public decimal BuyerFinTotalLiabilityFirst1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilitySecond1_1st_1")]
        public decimal BuyerFinTotalLiabilitySecond1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilityThird1_1st_1")]
        public decimal BuyerFinTotalLiabilityThird1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquityFirst1_1st_1")]
        public decimal BuyerFinTotalEquityFirst1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquitySecond1_1st_1")]
        public decimal BuyerFinTotalEquitySecond1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquityThird1_1st_1")]
        public decimal BuyerFinTotalEquityThird1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueFirst1_1st_1")]
        public decimal BuyerFinTotalRevenueFirst1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueSecond1_1st_1")]
        public decimal BuyerFinTotalRevenueSecond1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueThird1_1st_1")]
        public decimal BuyerFinTotalRevenueThird1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSFirst1_1st_1")]
        public decimal BuyerFinTotalCOGSFirst1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSSecond1_1st_1")]
        public decimal BuyerFinTotalCOGSSecond1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSThird1_1st_1")]
        public decimal BuyerFinTotalCOGSThird1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitFirst1_1st_1")]
        public decimal BuyerFinTotalGrossProfitFirst1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitSecond1_1st_1")]
        public decimal BuyerFinTotalGrossProfitSecond1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitThird1_1st_1")]
        public decimal BuyerFinTotalGrossProfitThird1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpFirst1_1st_1")]
        public decimal BuyerFinTotalOperExpFirst1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpSecond1_1st_1")]
        public decimal BuyerFinTotalOperExpSecond1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpThird1_1st_1")]
        public decimal BuyerFinTotalOperExpThird1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitFirst1_1st_1")]
        public decimal BuyerFinTotalNetProfitFirst1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitSecond1_1st_1")]
        public decimal BuyerFinTotalNetProfitSecond1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitThird1_1st_1")]
        public decimal BuyerFinTotalNetProfitThird1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinNetProfitPercent1_1st_1")]
        public decimal BuyerFinNetProfitPercent1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinlDE1_1st_1")]
        public decimal BuyerFinlDE1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinQuickRatio1_1st_1")]
        public decimal BuyerFinQuickRatio1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinIntCoverageRatio1_1st_1")]
        public decimal BuyerFinIntCoverageRatio1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinYearFirst1_1st_1")]
        public int BuyerFinYearFirst1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinYearFirst2_1st_1")]
        public int BuyerFinYearFirst2_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinYearSecond1_1st_1")]
        public int BuyerFinYearSecond1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinYearSecond2_1st_1")]
        public int BuyerFinYearSecond2_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinYearThird1_1st_1")]
        public int BuyerFinYearThird1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinYearThird2_1st_1")]
        public int BuyerFinYearThird2_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt01_1st_1")]
        public decimal ServiceFeeAmt01_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc01_1st_1")]
        public string ServiceFeeDesc01_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark01_1st_1")]
        public string ServiceFeeRemark01_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt02_1st_1")]
        public decimal ServiceFeeAmt02_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc02_1st_1")]
        public string ServiceFeeDesc02_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark02_1st_1")]
        public string ServiceFeeRemark02_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt03_1st_1")]
        public decimal ServiceFeeAmt03_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc03_1st_1")]
        public string ServiceFeeDesc03_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark03_1st_1")]
        public string ServiceFeeRemark03_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt04_1st_1")]
        public decimal ServiceFeeAmt04_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc04_1st_1")]
        public string ServiceFeeDesc04_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark04_1st_1")]
        public string ServiceFeeRemark04_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt05_1st_1")]
        public decimal ServiceFeeAmt05_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc05_1st_1")]
        public string ServiceFeeDesc05_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark05_1st_1")]
        public string ServiceFeeRemark05_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt06_1st_1")]
        public decimal ServiceFeeAmt06_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc06_1st_1")]
        public string ServiceFeeDesc06_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark06_1st_1")]
        public string ServiceFeeRemark06_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt07_1st_1")]
        public decimal ServiceFeeAmt07_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc07_1st_1")]
        public string ServiceFeeDesc07_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark07_1st_1")]
        public string ServiceFeeRemark07_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt08_1st_1")]
        public decimal ServiceFeeAmt08_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc08_1st_1")]
        public string ServiceFeeDesc08_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark08_1st_1")]
        public string ServiceFeeRemark08_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt09_1st_1")]
        public decimal ServiceFeeAmt09_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc09_1st_1")]
        public string ServiceFeeDesc09_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark09_1st_1")]
        public string ServiceFeeRemark09_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt10_1st_1")]
        public decimal ServiceFeeAmt10_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc10_1st_1")]
        public string ServiceFeeDesc10_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark10_1st_1")]
        public string ServiceFeeRemark10_1st_1 { get; set; }
        [BookmarkMapping("SumServiceFeeAmt_1st_1")]
        public decimal SumServiceFeeAmt_1st_1 { get; set; }
        //R03
        [BookmarkMapping("AllCustBuyerOutsatanding")]
        public decimal AllCustBuyerOutsatanding { get; set; }
        //R04
        [BookmarkMapping("CustBuyerOutsatanding")]
        public decimal CustomerBuyerOutstanding { get; set; }

        public Guid RefCreditAppTableGUID { get; set; }
        public Guid RefCreditAppLineGUID { get; set; }
        public Guid CreditAppRequestLineGUID { get; set; }
    }

    public class ServiceFeeBookmarkView
    {
        public decimal AmountBeforeTax { get; set; }
        public string ServiceFeeTypeDescription { get; set; }
        public string Description { get; set; }
    }

    public class CreditAppReqLineFinBookmarkView 
    {
        public int Year { get; set; }
        public decimal BuyerFinRegisteredCapital { get; set; }
        public decimal BuyerFinPaidCapital { get; set; }
        public decimal BuyerFinTotalAsset { get; set; }
        public decimal BuyerFinTotalLiability { get; set; }
        public decimal BuyerFinTotalEquity { get; set; }
        public decimal BuyerFinTotalRevenue { get; set; }
        public decimal BuyerFinTotalCOGS { get; set; }
        public decimal BuyerFinTotalGrossProfit { get; set; }
        public decimal BuyerFinTotalOperExp { get; set; }
        public decimal BuyerFinTotalNetProfit { get; set; }
        public decimal BuyerFinNetProfitPercent { get; set; }
        public decimal BuyerFinlDERatio { get; set; }
        public decimal BuyerFinQuickRatio { get; set; }
        public decimal BuyerFinIntCoverageRatio { get; set; }
    }

    public class BillingDocumentBookmarkView
    {
        public int Row { get; set; }
        public string BillingDocument { get; set; }
    }

    public class ReceiptDocumentBookmarkView
    {
        public int Row { get; set; }
        public string ReceiptDocument { get; set; }
    }

    //R03
    public class QOriginalCreditAppLine
    {
        public string InvoiceAddress { get; set; }
        public decimal MaxPurchasePct { get; set; }
        public string AssignmentMethodRemark { get; set; }
        public decimal PurchaseFeePct { get; set; }
        public int PurchaseFeeCalculateBase { get; set; }
        public string BillingResponsibleByDescription { get; set; }
        public bool AcceptanceDocument { get; set; }
        public string BillingDescription { get; set; }
        public string BillingAddress { get; set; }
        public string LineCondition { get; set; }
        public string ReceiptDescription { get; set; }
        public string MethodOfPaymentDescription { get; set; }
        public string ReceiptAddress { get; set; }
        public string ReceiptContactName { get; set; }
        public string ReceiptContactPhone { get; set; }
        public string ReceiptContactPosition { get; set; }
        public string BillingContactName { get; set; }
        public string BillingContactPhone { get; set; }
        public string BillingContactPosition { get; set; }
    }
    //R03
}
