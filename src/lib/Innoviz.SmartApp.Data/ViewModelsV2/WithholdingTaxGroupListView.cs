using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class WithholdingTaxGroupListView : ViewCompanyBaseEntity
	{
		public string WithholdingTaxGroupGUID { get; set; }
		public string Description { get; set; }
		public string WHTGroupId { get; set; }
	}
}
