using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class InterestRealizedTransListView : ViewCompanyBaseEntity
	{
		public string InterestRealizedTransGUID { get; set; }
		public int LineNum { get; set; }
		public string StartDate { get; set; }
		public string EndDate { get; set; }
		public string AccountingDate { get; set; }
		public int InterestDay { get; set; }
		public decimal AccInterestAmount { get; set; }
		public string DocumentId { get; set; }
		public string RefGUID { get; set; }
		public bool Cancelled { get; set; }
		public string RefProcessTransGUID { get; set; }
		public string ProcessTrans_Value { get; set; }

	}
}
