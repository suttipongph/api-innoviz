using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class FreeTextInvoiceTableListView : ViewCompanyBaseEntity
	{
		public string FreeTextInvoiceTableGUID { get; set; }
		public string FreeTextInvoiceId { get; set; }
		public string CustomerTableGUID { get; set; }
		public string CreditAppTableGUID { get; set; }
		public string IssuedDate { get; set; }
		public string InvoiceTypeGUID { get; set; }
		public string CustomerTable_Values { get; set; }
		public string InvoiceType_Values { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string InvoiceType_InvoiceTypeId { get; set; }
		public string CreditAppTable_CreditAppId { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string DocumentStatusGUID { get; set; }
		public decimal InvoiceAmount { get; set; }
	}
}
