using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class InvoiceTableListView : ViewBranchCompanyBaseEntity
	{
		public string InvoiceTableGUID { get; set; }
		public string InvoiceId { get; set; }
		public string CustomerTableGUID { get; set; }
		public string CreditAppTableGUID { get; set; }
		public string IssuedDate { get; set; }
		public string InvoiceTypeGUID { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string DocumentStatus_DocumentId { get; set; }
		public string BuyerTableGUID { get; set; }
		public string BuyerInvoiceTableGUID { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string InvoiceType_Values { get; set; }
		public string InvoiceType_InvoiceTypeId { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string CreditAppTable_CreditAppId { get; set; }
		public string BuyerTable_Values { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string BuyerInvoiceTable_Values { get; set; }
		public string BuyerInvoiceTable_BuyerInvoiceId { get; set; }
		public string DueDate { get; set; }
		public string RefGUID { get; set; }
		public string DocumentId { get; set; }

		#region UnboundField
		public int CustTransStatus { get; set; }
		public int RefType { get; set; }
		#endregion
	}
}
