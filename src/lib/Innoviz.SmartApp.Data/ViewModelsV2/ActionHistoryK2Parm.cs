﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class ActionHistoryK2Parm
    {
        public string RefGUID { get; set; }
        public string ActionName { get; set; }
        public string ActivityName { get; set; }
    }
}
