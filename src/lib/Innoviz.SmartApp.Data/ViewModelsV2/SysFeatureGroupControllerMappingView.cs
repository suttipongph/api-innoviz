﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class SysFeatureGroupControllerMappingView
    {
        public string GroupId { get; set; }
        public List<SysFeatureTableView> SysFeatureTables { get; set; }
        public string ControllerName { get; set; }
        public string FeatureType { get; set; }
    }
    public class SysFeatureTableView
    {
        public string Path { get; set; }
        public string FeatureId { get; set; }
        public string ParentFeatureId { get; set; }
    }
    public class BuildRouteAttributeView
    {
        public StringBuilder StringBuilder { get; set; }
        public int GroupIdStartIndex { get; set; }
    }
}
