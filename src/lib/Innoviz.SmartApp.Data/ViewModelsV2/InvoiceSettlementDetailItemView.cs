using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class InvoiceSettlementDetailItemView : ViewCompanyBaseEntity
	{
		public string InvoiceSettlementDetailGUID { get; set; }
		public string AssignmentAgreementTableGUID { get; set; }
		public decimal BalanceAmount { get; set; }
		public string BuyerAgreementTableGUID { get; set; }
		public string DocumentId { get; set; }
		public int InterestCalculationMethod { get; set; }
		public decimal InvoiceAmount { get; set; }
		public string InvoiceDueDate { get; set; }
		public string InvoiceTableGUID { get; set; }
		public string InvoiceTypeGUID { get; set; }
		public int ProductType { get; set; }
		public decimal PurchaseAmount { get; set; }
		public decimal PurchaseAmountBalance { get; set; }
		public string RefGUID { get; set; }
		public string RefReceiptTempPaymDetailGUID { get; set; }
		public int RefType { get; set; }
		public string RefId { get; set; }
		public decimal RetentionAmountAccum { get; set; }
		public decimal SettleAmount { get; set; }
		public decimal SettleAssignmentAmount { get; set; }
		public decimal SettleInvoiceAmount { get; set; }
		public decimal SettlePurchaseAmount { get; set; }
		public decimal SettleReserveAmount { get; set; }
		public decimal SettleTaxAmount { get; set; }
		public int SuspenseInvoiceType { get; set; }
		public decimal WHTAmount { get; set; }
		public bool WHTSlipReceivedByBuyer { get; set; }
		public string InvoiceType_Values { get; set; }
		public string BuyerAgreementTable_Values { get; set; }
		public string AssignmentAgreementTable_Values { get; set; }
		#region InvoiceTable
		public string InvoiceTable_Values { get; set; }
		public string Currency_Values { get; set; }
		public string CreditAppTable_Values { get; set; }
		public decimal InvoiceTable_WHTBalance { get; set; }
		public decimal InvoiceTable_SettleTaxBalance { get; set; }
		public decimal InvoiceTable_WHTAmount { get; set; }
		public int? InvoiceTable_ProductType { get; set; }
		public bool InvoiceTable_ProductInvoice { get; set; }
		#endregion InvoiceTable
		public int ReceivedFrom { get; set; }
		public bool WHTSlipReceivedByCustomer { get; set; }
        #region Unbound 
		public decimal MaxSettleAmount { get; set; }
		#endregion Unbound
		public string InvoiceSettlementDetail_Values { get; set; }
	}
}
