using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class VerificationTransItemView : ViewCompanyBaseEntity
	{
		public string VerificationTransGUID { get; set; }
		public string DocumentId { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public string VerificationTableGUID { get; set; }
		public string RefId { get; set; }
	}
}
