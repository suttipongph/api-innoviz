using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class DocumentTemplateTableListView : ViewCompanyBaseEntity
	{
		public string DocumentTemplateTableGUID { get; set; }
		public string Description { get; set; }
		public int DocumentTemplateType { get; set; }
		public string TemplateId { get; set; }
	}
}
