﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class DemoWorkflowParamView
    {
        public string DemoGUID { get; set; }
        public string DemoId { get; set; }
        public string RowData { get; set; }
    }
}
