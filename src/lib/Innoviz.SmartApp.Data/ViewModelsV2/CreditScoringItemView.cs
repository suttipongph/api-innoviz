using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CreditScoringItemView : ViewCompanyBaseEntity
	{
		public string CreditScoringGUID { get; set; }
		public string CreditScoringId { get; set; }
		public string Description { get; set; }
	}
}
