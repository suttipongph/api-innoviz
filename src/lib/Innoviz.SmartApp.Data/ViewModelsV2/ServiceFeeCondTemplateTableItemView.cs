using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ServiceFeeCondTemplateTableItemView : ViewCompanyBaseEntity
	{
		public string ServiceFeeCondTemplateTableGUID { get; set; }
		public string Description { get; set; }
		public int ProductType { get; set; }
		public string ServiceFeeCondTemplateId { get; set; }
	}
}
