﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class UpdateStatusAmendBuyerInfoView
    {
        public string UpdateStatusAmendBuyerInfoGUID { get; set; }
        public int ApprovalDecision { get; set; }
        public string ApprovedDate { get; set; }
        public string CreditAppRequestId { get; set; }
        public int CreditAppRequestType { get; set; }
        public string CustomerId { get; set; }
        public string Description { get; set; }
        public string CreditAppRequestTableGUID { get; set; }
    }
    public class UpdateStatusAmendBuyerInfoResultView: ResultBaseEntity
    {

    }
}
