﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
  public  class PrintDocumentReturnLabelLetterView
    {
        public string printDocumentReturnLabelLetterGUID { get; set; }
        public string address { get; set; }
        public string contactPersonName { get; set; }
        public int contactTo { get; set; }
        public string documentReturnId { get; set; }
        public string DocumentReturnTableGUID { get; set; }
    }
    public class PrintDocumentReturnLabelLetterViewReport : ViewReportBaseEntity
    {
        public string printDocumentReturnLabelLetterGUID { get; set; }
        public string address { get; set; }
        public string contactPersonName { get; set; }
        public string contactTo { get; set; }
        public string documentReturnId { get; set; }

        public string DocumentReturnTableGUID { get; set; }
    }
}
