﻿using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class GenStagingTableParameter
    {
        public List<int> ProcessTransType { get; set; }
        public List<int> StagingBatchStatus { get; set; }
        public List<Guid> RefGUID { get; set; }

        #region not required
        public string StagingBatchId { get; set; }
        public Guid DefaultCompanyGUID { get; set; }
        public string CompanyTaxBranchId { get; set; }
        public Company Company { get; set; }
        public CompanyParameter CompanyParameter { get; set; }
        public List<LedgerDimension> LedgerDimension { get; set; }
        public List<InvoiceType> InvoiceType { get; set; }
        public List<InvoiceRevenueType> InvoiceRevenueType { get; set; }
        public List<MethodOfPayment> MethodOfPayment { get; set; }
        public List<TaxTable> TaxTable { get; set; }
        public List<VendorTable> VendorTable { get; set; }
        public List<VendBank> VendPrimaryBank { get; set; }
        public List<AddressTrans> VendPrimaryAddressTrans { get; set; }
        public List<ReceiptTempTable> ReceiptTempTableFilter { get; set; }
        public List<PaymentDetail> PaymentDetailFilter { get; set; }
        public List<CustomerRefundTable> CustomerRefundTableFilter { get; set; }
        public List<PaymentHistory> PaymentHistoryFilter { get; set; }
        public List<InvoiceSettlementDetail> InvoiceSettlementDetailFilter { get; set; }

        public string UnearnedInterestLedgerAccount { get; set; }
        public string InterestLedgerAccount { get; set; }
        public List<InterestAccountbyProductType> InterestAccountbyProductType { get; set; }
        public List<StagingTransText> StagingTransText { get; set; }
        public List<int> ProcessTransTypeFilter { get; set; }
        public List<ProcessTrans> ProcessTransFilter { get; set; }
        public List<ProcessTrans> ProcessTransFilter_InvoicePayment { get; set; }
        public List<ProcessTransVendorPaymTrans> ProcessTransVendorPaymTrans { get; set; }
        public List<ProcessTransVendorPaymTrans> ProcessTransVendorPaymTransNoError { get; set; }
        #endregion
    }

    public class GenStagingTableResult
    {
        public List<GenStagingTableErrorList> GenStagingTableErrorList { get; set; } = new List<GenStagingTableErrorList>();
        public string StagingBatchId { get; set; }
        public List<StagingTable> StagingTable { get; set; } = new List<StagingTable>();
        public List<StagingTableVendorInfo> StagingTableVendorInfo { get; set; } = new List<StagingTableVendorInfo>();
        public List<ProcessTrans> ProcessTrans { get; set; } = new List<ProcessTrans>();
    }

    public class GenStagingTableErrorList
    {
        public string TableName { get; set; }
        public string FieldName { get; set; }
        public string Reference { get; set; }
        public string RefValue { get; set; }
        public object[] RefValueArgs { get; set; }
        public int? ProcessTransType { get; set; }
        public string ErrorCode { get; set; }
        public object[] ErrorCodeArgs { get; set; }
        public Guid? RefGUID { get; set; }
        public ProductType? ProductType { get; set; }
        public Guid ErrorKey { get; set; } = Guid.NewGuid();
    }

    public class InterestAccountbyProductType
    {
        public int ProductType { get; set; }
        public string UnearnedInterestLedgerAccount { get; set; }
        public string InterestLedgerAccount { get; set; }
    }

    public class ProcessTransVendorPaymTrans
    {
        public ProcessTrans ProcessTrans { get; set; } = new ProcessTrans();
        public VendorPaymentTrans VendorPaymentTrans { get; set; } = new VendorPaymentTrans();
    }

    public class QueryForGenStagingPaymentDetail
    {
        public ProcessTrans ProcessTrans { get; set; }
        public PaymentDetail PaymentDetail { get; set; }
        public string DimensionCode1 { get; set; }
        public string DimensionCode2 { get; set; }
        public string DimensionCode3 { get; set; }
        public string DimensionCode4 { get; set; }
        public string DimensionCode5 { get; set; }
    }
}
