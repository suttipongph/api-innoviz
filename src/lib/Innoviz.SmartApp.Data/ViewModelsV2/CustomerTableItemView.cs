using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CustomerTableItemView : ViewCompanyBaseEntity
	{
		public string CustomerTableGUID { get; set; }
		public int Age { get; set; }
		public string AltName { get; set; }
		public string BlacklistStatusGUID { get; set; }
		public string BOTRating { get; set; }
		public string BusinessSegmentGUID { get; set; }
		public string BusinessSizeGUID { get; set; }
		public string BusinessTypeGUID { get; set; }
		public string CompanyName { get; set; }
		public string CompanyWebsite { get; set; }
		public string CreditScoringGUID { get; set; }
		public string CurrencyGUID { get; set; }
		public string CustGroupGUID { get; set; }
		public string CustomerId { get; set; }
		public string DateOfBirth { get; set; }
		public string DateOfEstablish { get; set; }
		public string DateOfExpiry { get; set; }
		public string DateOfIssue { get; set; }
		public string Dimension1GUID { get; set; }
		public string Dimension2GUID { get; set; }
		public string Dimension3GUID { get; set; }
		public string Dimension4GUID { get; set; }
		public string Dimension5GUID { get; set; }
		public string DocumentStatusGUID { get; set; }
		public int DueDay { get; set; }
		public string ExposureGroupGUID { get; set; }
		public decimal FixedAsset { get; set; }
		public string GenderGUID { get; set; }
		public string GradeClassificationGUID { get; set; }
		public int IdentificationType { get; set; }
		public decimal Income { get; set; }
		public string IntroducedByGUID { get; set; }
		public string IntroducedByRemark { get; set; }
		public int InvoiceIssuingDay { get; set; }
		public string IssuedBy { get; set; }
		public string KYCSetupGUID { get; set; }
		public int Labor { get; set; }
		public string LineOfBusinessGUID { get; set; }
		public string MaritalStatusGUID { get; set; }
		public string MethodOfPaymentGUID { get; set; }
		public string Name { get; set; }
		public string NationalityGUID { get; set; }
		public string NCBAccountStatusGUID { get; set; }
		public string OccupationGUID { get; set; }
		public decimal OtherIncome { get; set; }
		public string OtherSourceIncome { get; set; }
		public decimal PaidUpCapital { get; set; }
		public string ParentCompanyGUID { get; set; }
		public string PassportID { get; set; }
		public string Position { get; set; }
		public decimal PrivateARPct { get; set; }
		public decimal PublicARPct { get; set; }
		public string RaceGUID { get; set; }
		public int RecordType { get; set; }
		public string ReferenceId { get; set; }
		public decimal RegisteredCapital { get; set; }
		public string RegistrationTypeGUID { get; set; }
		public string ResponsibleByGUID { get; set; }
		public string SigningCondition { get; set; }
		public string SpouseName { get; set; }
		public string TaxID { get; set; }
		public string TerritoryGUID { get; set; }
		public string VendorTableGUID { get; set; }
		public string WithholdingTaxGroupGUID { get; set; }
		public int WorkExperienceMonth { get; set; }
		public int WorkExperienceYear { get; set; }
		public string WorkPermitID { get; set; }
		public string LineOfBusiness_Values { get; set; }
		public string IntroducedBy_Values { get; set; }
		public string BusinessType_Values { get; set; }

		public string DocumentStatus_StatusId { get; set; }
		public string GuarantorAgreement_GuarantorAgreementGUID { get; set; }
		public bool IsPassportIdDuplicate { get; set; }
		public bool IsTaxIdDuplicate { get; set; }
		public string PassportDuplicateEmployeeName { get; set; }
	}
}
