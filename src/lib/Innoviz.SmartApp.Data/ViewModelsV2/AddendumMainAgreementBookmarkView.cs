﻿using Innoviz.SmartApp.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class AddendumMainAgreementBookmarkView
    {
        public DateTime AgreementDate { get; set; }
        public DateTime RequestDate { get; set; }
        public Guid CreditAppRequestTableGUID { get; set; }
        public string InterestTypeId { get; set; }
        public decimal InterestAdjustment { get; set; }
        public decimal InterestValue { get; set; }

        [BookmarkMapping("ApprovedDate1")]
        public string ApprovedDate1 { get; set; }
        [BookmarkMapping("AuthorityPerson1st_1")]
        public string AuthorityPerson1st_1 { get; set; }

        [BookmarkMapping("AuthorityPerson2nd_1")]
        public string AuthorityPerson2nd_1 { get; set; }

        [BookmarkMapping("AuthorityPerson3rd_1")]
        public string AuthorityPerson3rd_1 { get; set; }

        [BookmarkMapping("PositionLIT1")]
        public string PositionLIT1 { get; set; }

        [BookmarkMapping("AuthorizedCust1st_1")]
        public string AuthorizedCust1st_1 { get; set; }

        [BookmarkMapping("AuthorizedCust1st_2")]
        public string AuthorizedCust1st_2 { get; set; }

        [BookmarkMapping("AuthorizedCust2nd_1")]
        public string AuthorizedCust2nd_1 { get; set; }

        [BookmarkMapping("AuthorizedCust2nd_2")]
        public string AuthorizedCust2nd_2 { get; set; }

        [BookmarkMapping("AuthorizedCust3rd_1")]
        public string AuthorizedCust3rd_1 { get; set; }

        [BookmarkMapping("AuthorizedCust3rd_2")]
        public string AuthorizedCust3rd_2 { get; set; }

        [BookmarkMapping("AuthorizedCustNew1st_1")]
        public string AuthorizedCustNew1st_1 { get; set; }

        [BookmarkMapping("AuthorizedCustNew1st_2")]
        public string AuthorizedCustNew1st_2 { get; set; }

        [BookmarkMapping("AuthorizedCustNew2nd_1")]
        public string AuthorizedCustNew2nd_1 { get; set; }

        [BookmarkMapping("AuthorizedCustNew2nd_2")]
        public string AuthorizedCustNew2nd_2 { get; set; }

        [BookmarkMapping("AuthorizedCustNew3rd_1")]
        public string AuthorizedCustNew3rd_1 { get; set; }

        [BookmarkMapping("AuthorizedCustNew3rd_2")]
        public string AuthorizedCustNew3rd_2 { get; set; }

        [BookmarkMapping("AuthorizedCustNew4th_1")]
        public string AuthorizedCustNew4th_1 { get; set; }

        [BookmarkMapping("AuthorizedCustNew4th_2")]
        public string AuthorizedCustNew4th_2 { get; set; }

        [BookmarkMapping("AuthorizedCustNew5th_1")]
        public string AuthorizedCustNew5th_1 { get; set; }

        [BookmarkMapping("AuthorizedCustNew5th_2")]
        public string AuthorizedCustNew5th_2 { get; set; }

        [BookmarkMapping("AuthorizedCustNew6th_1")]
        public string AuthorizedCustNew6th_1 { get; set; }

        [BookmarkMapping("AuthorizedCustNew6th_2")]
        public string AuthorizedCustNew6th_2 { get; set; }

        [BookmarkMapping("AuthorizedCustOld1st_1")]
        public string AuthorizedCustOld1st_1 { get; set; }

        [BookmarkMapping("AuthorizedCustOld1st_2")]
        public string AuthorizedCustOld1st_2 { get; set; }

        [BookmarkMapping("AuthorizedCustOld2nd_1")]
        public string AuthorizedCustOld2nd_1 { get; set; }

        [BookmarkMapping("AuthorizedCustOld2nd_2")]
        public string AuthorizedCustOld2nd_2 { get; set; }

        [BookmarkMapping("AuthorizedCustOld3rd_1")]
        public string AuthorizedCustOld3rd_1 { get; set; }

        [BookmarkMapping("AuthorizedCustOld3rd_2")]
        public string AuthorizedCustOld3rd_2 { get; set; }

        [BookmarkMapping("AuthorizedCustOld4th_1")]
        public string AuthorizedCustOld4th_1 { get; set; }

        [BookmarkMapping("AuthorizedCustOld4th_2")]
        public string AuthorizedCustOld4th_2 { get; set; }

        [BookmarkMapping("AuthorizedCustOld5th_1")]
        public string AuthorizedCustOld5th_1 { get; set; }

        [BookmarkMapping("AuthorizedCustOld5th_2")]
        public string AuthorizedCustOld5th_2 { get; set; }

        [BookmarkMapping("AuthorizedCustOld6th_1")]
        public string AuthorizedCustOld6th_1 { get; set; }

        [BookmarkMapping("AuthorizedCustOld6th_2")]
        public string AuthorizedCustOld6th_2 { get; set; }

        [BookmarkMapping("ContractDate1")]
        public string ContractDate1 { get; set; }

        [BookmarkMapping("ContractDate2")]
        public string ContractDate2 { get; set; }

        [BookmarkMapping("ContractDate3")]
        public string ContractDate3 { get; set; }

        [BookmarkMapping("ContractDate4")]
        public string ContractDate4 { get; set; }

        [BookmarkMapping("ContractDate5")]
        public string ContractDate5 { get; set; }

        [BookmarkMapping("ContractDate6")]
        public string ContractDate6 { get; set; }

        [BookmarkMapping("ContractDate7")]
        public string ContractDate7 { get; set; }

        [BookmarkMapping("ContractDate8")]
        public string ContractDate8 { get; set; }

        [BookmarkMapping("ContractDate9")]
        public string ContractDate9 { get; set; }

        [BookmarkMapping("ContractDate10")]
        public string ContractDate10 { get; set; }

        [BookmarkMapping("ContractNo1")]
        public string ContractNo1 { get; set; }

        [BookmarkMapping("ContractNo2")]
        public string ContractNo2 { get; set; }

        [BookmarkMapping("ContractNo3")]
        public string ContractNo3 { get; set; }

        [BookmarkMapping("ContractNo4")]
        public string ContractNo4 { get; set; }

        [BookmarkMapping("ContractNo5")]
        public string ContractNo5 { get; set; }

        [BookmarkMapping("ContractNo6")]
        public string ContractNo6 { get; set; }

        [BookmarkMapping("ContractNo7")]
        public string ContractNo7 { get; set; }

        [BookmarkMapping("ContractNo8")]
        public string ContractNo8 { get; set; }

        [BookmarkMapping("ContractNo9")]
        public string ContractNo9 { get; set; }

        [BookmarkMapping("ContractNo10")]
        public string ContractNo10 { get; set; }

        [BookmarkMapping("CustName1")]
        public string CustName1 { get; set; }

        [BookmarkMapping("CustName2")]
        public string CustName2 { get; set; }

        [BookmarkMapping("CustName3")]
        public string CustName3 { get; set; }

        [BookmarkMapping("CustName4")]
        public string CustName4 { get; set; }

        [BookmarkMapping("CustName5")]
        public string CustName5 { get; set; }

        [BookmarkMapping("CustName6")]
        public string CustName6 { get; set; }

        [BookmarkMapping("CustName7")]
        public string CustName7 { get; set; }

        [BookmarkMapping("CustName8")]
        public string CustName8 { get; set; }

        [BookmarkMapping("CustName9")]
        public string CustName9 { get; set; }

        [BookmarkMapping("CustName10")]
        public string CustName10 { get; set; }

        [BookmarkMapping("CustName11")]
        public string CustName11 { get; set; }

        [BookmarkMapping("CustName12")]
        public string CustName12 { get; set; }

        [BookmarkMapping("CustName13")]
        public string CustName13 { get; set; }

        [BookmarkMapping("CustName14")]
        public string CustName14 { get; set; }

        [BookmarkMapping("CustName15")]
        public string CustName15 { get; set; }

        [BookmarkMapping("CustName16")]
        public string CustName16 { get; set; }

        [BookmarkMapping("CustName17")]
        public string CustName17 { get; set; }

        [BookmarkMapping("CustName18")]
        public string CustName18 { get; set; }

        [BookmarkMapping("CustName19")]
        public string CustName19 { get; set; }

        [BookmarkMapping("CustName20")]
        public string CustName20 { get; set; }

        [BookmarkMapping("CustName21")]
        public string CustName21 { get; set; }

        [BookmarkMapping("CustName22")]
        public string CustName22 { get; set; }

        [BookmarkMapping("CustName23")]
        public string CustName23 { get; set; }

        [BookmarkMapping("CustName24")]
        public string CustName24 { get; set; }

        [BookmarkMapping("CustName25")]
        public string CustName25 { get; set; }

        [BookmarkMapping("CustName26")]
        public string CustName26 { get; set; }

        [BookmarkMapping("CustName27")]
        public string CustName27 { get; set; }

        [BookmarkMapping("CustName28")]
        public string CustName28 { get; set; }

        [BookmarkMapping("CustName29")]
        public string CustName29 { get; set; }

        [BookmarkMapping("CustName30")]
        public string CustName30 { get; set; }

        [BookmarkMapping("CustName31")]
        public string CustName31 { get; set; }

        [BookmarkMapping("CustName32")]
        public string CustName32 { get; set; }

        [BookmarkMapping("CustName33")]
        public string CustName33 { get; set; }

        [BookmarkMapping("CustName34")]
        public string CustName34 { get; set; }

        [BookmarkMapping("CustName35")]
        public string CustName35 { get; set; }

        [BookmarkMapping("CustName36")]
        public string CustName36 { get; set; }

        [BookmarkMapping("CustName37")]
        public string CustName37 { get; set; }

        [BookmarkMapping("CustName38")]
        public string CustName38 { get; set; }

        [BookmarkMapping("CustName39")]
        public string CustName39 { get; set; }

        [BookmarkMapping("CustName40")]
        public string CustName40 { get; set; }

        [BookmarkMapping("CustName41")]
        public string CustName41 { get; set; }

        [BookmarkMapping("CustName42")]
        public string CustName42 { get; set; }

        [BookmarkMapping("CustName43")]
        public string CustName43 { get; set; }

        [BookmarkMapping("CustName44")]
        public string CustName44 { get; set; }

        [BookmarkMapping("CustName45")]
        public string CustName45 { get; set; }

        [BookmarkMapping("CustName46")]
        public string CustName46 { get; set; }

        [BookmarkMapping("CustName47")]
        public string CustName47 { get; set; }

        [BookmarkMapping("CustName48")]
        public string CustName48 { get; set; }

        [BookmarkMapping("CustName49")]
        public string CustName49 { get; set; }

        [BookmarkMapping("CustName50")]
        public string CustName50 { get; set; }

        [BookmarkMapping("DateOfCA1")]
        public string DateOfCA1 { get; set; }
        [BookmarkMapping("DateOfCA2")]
        public string DateOfCA2 { get; set; }
        [BookmarkMapping("DateOfCA3")]
        public string DateOfCA3 { get; set; }
        [BookmarkMapping("DateOfCA4")]
        public string DateOfCA4 { get; set; }
        [BookmarkMapping("DateOfCA5")]
        public string DateOfCA5 { get; set; }
        [BookmarkMapping("DateOfCA6")]
        public string DateOfCA6 { get; set; }

        [BookmarkMapping("NewInterestRate1")]
        public string NewInterestRate1 { get; set; }

        [BookmarkMapping("OldInterestRate1")]
        public string OldInterestRate1 { get; set; }

        [BookmarkMapping("ManualNo1")]
        public string ManualNo1 { get; set; }

        [BookmarkMapping("ApprovedDate2")]
        public string ApprovedDate2 { get; set; }
        [BookmarkMapping("ApprovedDate3")]
        public string ApprovedDate3 { get; set; }
        [BookmarkMapping("ApprovedDate4")]
        public string ApprovedDate4 { get; set; }
        [BookmarkMapping("ApprovedDate5")]
        public string ApprovedDate5 { get; set; }
        [BookmarkMapping("ApprovedDate6")]
        public string ApprovedDate6 { get; set; }
    }
}
