using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ServiceFeeConditionTransListView : ViewCompanyBaseEntity
	{
		public string ServiceFeeConditionTransGUID { get; set; }
		public int Ordering { get; set; }
		public string InvoiceRevenueTypeGUID { get; set; }
		public string Description { get; set; }
		public decimal AmountBeforeTax { get; set; }
		public bool Inactive { get; set; }
		public string InvoiceRevenueType_Values { get; set; }
		public string InvoiceRevenueType_revenueTypeId { get; set; }
		public string RefGUID { get; set; }
		public int InvoiceRevenueType_ProductType { get; set; }
		public int InvoiceRevenueType_ServiceFeeCategory { get; set; }
	}
}
