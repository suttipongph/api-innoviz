using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class StagingTransTextListView : ViewCompanyBaseEntity
	{
		public string StagingTransTextGUID { get; set; }
		public int ProcessTransType { get; set; }
		public string TransText { get; set; }
	}
}
