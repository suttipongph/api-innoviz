using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class AgreementTableInfoListView : ViewCompanyBaseEntity
	{
		public string AgreementTableInfoGUID { get; set; }
	}
}
