using Innoviz.SmartApp.Core.ViewModels;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ImportFinancialStatementResultView : ResultBaseEntity
	{
		public string FinancialStatementTransGUID { get; set; }
		public decimal Amount { get; set; }
		public string Description { get; set; }
		public int Ordering { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public int Year { get; set; }
		public string RefId { get; set; }
	}
}
