using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class DocumentReturnMethodListView : ViewCompanyBaseEntity
	{
		public string DocumentReturnMethodGUID { get; set; }
		public string DocumentReturnMethodId { get; set; }
		public string Description { get; set; }
	}
}
