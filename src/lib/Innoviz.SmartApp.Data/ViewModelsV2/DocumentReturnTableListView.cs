using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class DocumentReturnTableListView : ViewCompanyBaseEntity
	{
		public string DocumentReturnTableGUID { get; set; }
		public string DocumentReturnId { get; set; }
		public string RequestorGUID { get; set; }
		public int ContactTo { get; set; }
		public string ContactPersonName { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string DocumentReturnMethodGUID { get; set; }
		public string ExpectedReturnDate { get; set; }
		public string EmployeeTable_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string DocumentReturnMethod_Values { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string DocumentReturnMethod_DocumentReturnMethodId { get; set; }
		public string EmployeeTable_EmployeeTableId { get; set; }
	}
}
