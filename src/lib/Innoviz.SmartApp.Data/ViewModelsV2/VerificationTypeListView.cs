using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class VerificationTypeListView : ViewCompanyBaseEntity
	{
		public string VerificationTypeGUID { get; set; }
		public string VerificationTypeId { get; set; }
		public string Description { get; set; }
		public int VerifyType { get; set; }
	}
}
