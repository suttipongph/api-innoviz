using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class NCBAccountStatusItemView : ViewCompanyBaseEntity
	{
		public string NCBAccountStatusGUID { get; set; }
		public string Code { get; set; }
		public string Description { get; set; }
		public string NCBAccStatusId { get; set; }
	}
}
