using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ServiceFeeConditionTransItemView : ViewCompanyBaseEntity
	{
		public string ServiceFeeConditionTransGUID { get; set; }
		public decimal AmountBeforeTax { get; set; }
		public string CreditAppRequestTableGUID { get; set; }
		public string Description { get; set; }
		public bool Inactive { get; set; }
		public string InvoiceRevenueTypeGUID { get; set; }
		public int Ordering { get; set; }
		public string RefGUID { get; set; }
		public string RefServiceFeeConditionTransGUID { get; set; }
		public int RefType { get; set; }
		public string RefId { get; set; }
		public string CreditAppRequestTable_CreditAppRequestId { get; set; }
		public int InvoiceRevenueType_ProductType { get; set; }
		public int InvoiceRevenueType_ServiceFeeCategory { get; set; }
		public string InvoiceRevenueType_FeeTaxGUID { get; set; }
		public string InvoiceRevenueType_FeeWHTGUID { get; set; }
	}
}
