using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BusinessSizeListView : ViewCompanyBaseEntity
	{
		public string BusinessSizeGUID { get; set; }
		public string BusinessSizeId { get; set; }
		public string Description { get; set; }
	}
}
