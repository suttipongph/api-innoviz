using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ConsortiumLineItemView : ViewCompanyBaseEntity
	{
		public string ConsortiumLineGUID { get; set; }
		public string Address { get; set; }
		public string AuthorizedPersonTypeGUID { get; set; }
		public string ConsortiumTableGUID { get; set; }
		public string CustomerName { get; set; }
		public bool IsMain { get; set; }
		public string OperatedBy { get; set; }
		public string Position { get; set; }
		public decimal ProportionOfShareholderPct { get; set; }
		public string Remark { get; set; }
		public string ConsortiumTable_Values { get; set; }
		public int Ordering { get; set; }
	}
}
