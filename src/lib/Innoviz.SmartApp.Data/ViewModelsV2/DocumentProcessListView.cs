using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class DocumentProcessListView : ViewBaseEntity
	{
		public string DocumentProcessGUID { get; set; }
		public string ProcessId { get; set; }
		public string Description { get; set; }

	}
}
