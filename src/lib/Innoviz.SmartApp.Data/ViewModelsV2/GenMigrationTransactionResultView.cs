﻿using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class GenMigrationTransactionResultView : ResultBaseEntity
    {
        public List<ProductSubTypeItemView> ProductSubType { get; set; } = new List<ProductSubTypeItemView>();
        public List<CreditLimitTypeItemView> CreditLimitType { get; set; } = new List<CreditLimitTypeItemView>();
        public List<AddressTransItemView> AddressTrans { get; set; } = new List<AddressTransItemView>();
        public List<CreditAppRequestTableItemView> CreditAppRequestTable { get; set; } = new List<CreditAppRequestTableItemView>();
        public List<CreditAppReqBusinessCollateralItemView> CreditAppReqBusinessCollateral { get; set; } = new List<CreditAppReqBusinessCollateralItemView>();
        public List<CreditAppTableItemView> CreditAppTable { get; set; } = new List<CreditAppTableItemView>();
        public List<MainAgreementTableItemView> MainAgreementTable { get; set; } = new List<MainAgreementTableItemView>();
        public List<BusinessCollateralAgmTableItemView> BusinessCollateralAgmTable { get; set; } = new List<BusinessCollateralAgmTableItemView>();
        public List<BusinessCollateralAgmLineItemView> BusinessCollateralAgmLine { get; set; } = new List<BusinessCollateralAgmLineItemView>();
        public List<ServiceFeeTransItemView> ServiceFeeTrans { get; set; } = new List<ServiceFeeTransItemView>();
        public List<MessengerJobTableItemView> MessengerJobTable { get; set; } = new List<MessengerJobTableItemView>();
        public List<CustomerRefundTableItemView> CustomerRefundTable { get; set; } = new List<CustomerRefundTableItemView>();
        public List<PaymentDetailItemView> PaymentDetail { get; set; } = new List<PaymentDetailItemView>();
        public List<InvoiceTableItemView> InvoiceTable { get; set; } = new List<InvoiceTableItemView>();
        public List<InvoiceSettlementDetailItemView> InvoiceSettlementDetail { get; set; } = new List<InvoiceSettlementDetailItemView>();
        public List<ReceiptTempTableItemView> ReceiptTempTable { get; set; } = new List<ReceiptTempTableItemView>();
        public List<ProcessTransItemView> ProcessTrans { get; set; } = new List<ProcessTransItemView>();
    }
}
