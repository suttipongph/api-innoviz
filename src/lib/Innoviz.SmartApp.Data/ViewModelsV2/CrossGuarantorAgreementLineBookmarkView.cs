using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class CrossGuarantorAgreementLineBookmarkView
    {
        public string GuarantorName1st_1 { get; set; }
        public string GuarantorName1st_2 { get; set; }
        public string GuarantorName1st_3 { get; set; }
        public DateTime? GuarantorAge1st_DateOfBirth { get; set; }
        public string GuarantorRace1st_1 { get; set; }
        public string GuarantorNationality1st_1 { get; set; }
        public string GuarantorAddress1st_1 { get; set; }
        public string GuarantorTaxID1st_1 { get; set; }
        public DateTime? GuarantorDateregis1st_1 { get; set; }
        public string RelatedName1st { get; set; }
        public string RelatedAddress1st { get; set; }
        public string RelatedContractNo1st { get; set; }
        public DateTime? RelatedContractDate1st { get; set; }
        public string RelatedName2nd { get; set; }
        public string RelatedAddress2nd { get; set; }
        public string RelatedContractNo2nd { get; set; }
        public DateTime? RelatedContractDate2nd { get; set; }
        public string RelatedName3rd { get; set; }
        public string RelatedAddress3rd { get; set; }
        public string RelatedContractNo3rd { get; set; }
        public DateTime? RelatedContractDate3rd { get; set; }
        public string RelatedName4th { get; set; }
        public string RelatedAddress4th { get; set; }
        public string RelatedContractNo4th { get; set; }
        public DateTime? RelatedContractDate4th { get; set; }
        public string RelatedName5th { get; set; }
        public string RelatedAddress5th { get; set; }
        public string RelatedContractNo5th { get; set; }
        public DateTime? RelatedContractDate5th { get; set; }
        public string RelatedName6th { get; set; }
        public string RelatedAddress6th { get; set; }
        public string RelatedContractNo6th { get; set; }
        public DateTime? RelatedContractDate6th { get; set; }
        public string RelatedName7th { get; set; }
        public string RelatedAddress7th { get; set; }
        public string RelatedContractNo7th { get; set; }
        public DateTime? RelatedContractDate7th { get; set; }
        public string RelatedName8th { get; set; }
        public string RelatedAddress8th { get; set; }
        public string RelatedContractNo8th { get; set; }
        public DateTime? RelatedContractDate8th { get; set; }
        public decimal MaximumGuaranteeAmount { get; set; }

        public decimal ApprovedCreditLimit { get; set; }
        public string AffiliateCustomerTaxId1st { get; set; }
        public string AffiliateCustomerTaxId2nd { get; set; }
        public string AffiliateCustomerTaxId3rd { get; set; }
        public string AffiliateCustomerTaxId4th { get; set; }
        public string AffiliateCustomerTaxId5th { get; set; }
        public string AffiliateCustomerTaxId6th { get; set; }
        public string AffiliateCustomerTaxId7th { get; set; }
        public string AffiliateCustomerTaxId8th { get; set; }
        public string GuarantotOperatedBy { get; set; }
        public string GuarantorName { get; set; }
        public Guid GuarantorAgreementLineGUID { get; set; }
    }
}