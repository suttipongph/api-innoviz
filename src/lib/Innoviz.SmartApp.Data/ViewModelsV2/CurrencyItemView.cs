using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CurrencyItemView : ViewCompanyBaseEntity
	{
		public string CurrencyGUID { get; set; }
		public string CurrencyId { get; set; }
		public string Name { get; set; }
		public decimal ExchangeRate_Rate { get; set; }
	}
}
