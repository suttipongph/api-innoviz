using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BillingResponsibleByItemView : ViewCompanyBaseEntity
	{
		public string BillingResponsibleByGUID { get; set; }
		public int BillingBy { get; set; }
		public string BillingResponsibleById { get; set; }
		public string Description { get; set; }
	}
}
