﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
   public class GenAssignmentAgmNoticeOfCancelView : ResultBaseEntity
    {
        public string AssignmentAgreementTableGUID { get; set; }
        public string NoticeOfCancellationInternalAssignmentAgreementId { get; set; }
        public string CancelDate { get; set; }
        public string Description { get; set; }
        public string DocumentReasonGUID { get; set; }
        public string ReasonRemark { get; set; }
        public string ResultLabel { get; set; }
        public AssignmentAgreementTableItemView AssignmentAgreementTableItemView { get; set; }
        public string AssignmentProductDescription { get; set; }
        public string ToWhomConcern { get; set; }
        public string CancelAuthorityPersonID { get; set; }
        public string CancelAuthorityPersonName { get; set; }
        public string CancelAuthorityPersonAddress { get; set; }
    }
}
