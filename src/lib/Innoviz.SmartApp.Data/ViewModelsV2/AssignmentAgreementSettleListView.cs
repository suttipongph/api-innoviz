using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class AssignmentAgreementSettleListView : ViewCompanyBaseEntity
	{
		public string AssignmentAgreementSettleGUID { get; set; }
		public string SettledDate { get; set; }
		public decimal SettledAmount { get; set; }
		public string BuyerAgreementTableGUID { get; set; }
		public string BuyerAgreementTable_Values { get; set; }
		public string RefGUID { get; set; }
		public string AssignmentAgreementTableGUID { get; set; }
	}
}
