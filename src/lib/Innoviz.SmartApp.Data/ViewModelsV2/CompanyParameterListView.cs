using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CompanyParameterListView : ViewCompanyBaseEntity
	{
		public string CompanyParameterGUID { get; set; }
	}
}
