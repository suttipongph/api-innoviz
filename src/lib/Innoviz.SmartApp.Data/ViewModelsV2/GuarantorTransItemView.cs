using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class GuarantorTransItemView : ViewCompanyBaseEntity
	{
		public string GuarantorTransGUID { get; set; }
		public bool Affiliate { get; set; }
		public string GuarantorTypeGUID { get; set; }
		public bool InActive { get; set; }
		public int Ordering { get; set; }
		public string RefGuarantorTransGUID { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public string RelatedPersonTableGUID { get; set; }
		public int Age { get; set; }
		public string RelatedPersonTable_BackgroundSummary { get; set; }
		public string RelatedPersonTable_DateOfBirth { get; set; }
		public string RelatedPersonTable_Email { get; set; }
		public string RelatedPersonTable_Extension { get; set; }
		public string RelatedPersonTable_Fax { get; set; }
		public int RelatedPersonTable_IdentificationType { get; set; }
		public string RelatedPersonTable_LineId { get; set; }
		public string RelatedPersonTable_Mobile { get; set; }
		public string RelatedPersonTable_Name { get; set; }
		public string RelatedPersonTable_NationalityId { get; set; }
		public string RelatedPersonTable_PassportId { get; set; }
		public string RelatedPersonTable_Phone { get; set; }
		public string RelatedPersonTable_RaceId { get; set; }
		public string RefID { get; set; }
		public string RelatedPersonTable_TaxId { get; set; }
		public string RelatedPersonTable_WorkPermitId { get; set; }
		public string RelatedPersonTable_RelatedPersonId { get; set; }
		public string RelatedPersonTable_Address1 { get; set; }
		public string RelatedPersonTable_Address2 { get; set; }
		public string RelatedPersonTable_DateOfIssue { get; set; }
		public string GuarantorAgreement_GuarantorAgreementGUID { get; set; }
		public string RelatedPersonTable_RaceGUID { get; set; }
		public string RelatedPersonTable_NationalityGUID { get; set; }
		public string CreditAppTable_CreditAppTableGUID { get; set; }
		public string RelatedPersonTable_OperatedBy { get; set; }
		public decimal MaximumGuaranteeAmount { get; set; }

	}
}
