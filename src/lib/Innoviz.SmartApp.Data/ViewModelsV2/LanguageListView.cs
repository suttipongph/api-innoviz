using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class LanguageListView : ViewCompanyBaseEntity
	{
		public string LanguageGUID { get; set; }
	}
}
