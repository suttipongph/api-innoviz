﻿using Innoviz.SmartApp.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class BookmarkDocumentQueryCreditApplicationSharedAndPF
    {
        [BookmarkMapping("CANumber")]
        public string QCreditAppRequestTable_CreditAppRequestId { get; set; }
        [BookmarkMapping("RefCANumber")]
        public string QCreditAppRequestTable_OriginalCreditAppTableId { get; set; }
        [BookmarkMapping("CAStatus")]
        public string QCreditAppRequestTable_Status { get; set; }
        [BookmarkMapping("CACreateDate")]
        public string QCreditAppRequestTable_RequestDate { get; set; }
        [BookmarkMapping("CustomerName1", "CustomerName2")]
        public string QCreditAppRequestTable_CustomerName { get; set; }
        [BookmarkMapping("CustomerCode")]
        public string QCreditAppRequestTable_CustomerId { get; set; }
        [BookmarkMapping("SalesResp")]
        public string QCreditAppRequestTable_ResponsibleBy { get; set; }
        [BookmarkMapping("KYCHeader")]
        public string QCreditAppRequestTable_KYC { get; set; }
        [BookmarkMapping("CreditScoring")]
        public string QCreditAppRequestTable_CreditScoring { get; set; }
        [BookmarkMapping("CADesp")]
        public string QCreditAppRequestTable_Description { get; set; }
        [BookmarkMapping("Purpose")]
        public string QCreditAppRequestTable_Purpose { get; set; }
        [BookmarkMapping("CARemark")]
        public string QCreditAppRequestTable_CARemark { get; set; }
        [BookmarkMapping("ProductType")]
        public string QCreditAppRequestTable_ProductType { get; set; }
        [BookmarkMapping("ProductSubType")]
        public string QCreditAppRequestTable_ProductSubType { get; set; }
        [BookmarkMapping("CreditLimitType")]
        public string QCreditAppRequestTable_CreditLimitTypeId { get; set; }
        [BookmarkMapping("CreditLimitLife")]
        public string QCreditAppRequestTable_CreditLimitExpiration { get; set; }
        [BookmarkMapping("CreditPeriodStartDate")]
        public string QCreditAppRequestTable_StartDate { get; set; }
        [BookmarkMapping("CreditPeriodEndDate")]
        public string QCreditAppRequestTable_ExpiryDate { get; set; }
        [BookmarkMapping("CreditLimitReqAmt")]
        public decimal QCreditAppRequestTable_CreditLimitRequest { get; set; }
        [BookmarkMapping("CreditAllBuyer")]
        public decimal QCreditAppRequestTable_CustomerCreditLimit { get; set; }
        [BookmarkMapping("CAInterestType")]
        public string QCreditAppRequestTable_InterestType { get; set; }
        [BookmarkMapping("InterestAdj")]
        public decimal QCreditAppRequestTable_InterestAdjustment { get; set; }
        [BookmarkMapping("TotalInterest")]
        public decimal Variable_NewInterestRate { get; set; }
        [BookmarkMapping("MaxPurch")]
        public decimal QCreditAppRequestTable_MaxPurchasePct { get; set; }
        [BookmarkMapping("MaxRetenPerc")]
        public decimal QCreditAppRequestTable_MaxRetentionPct { get; set; }
        [BookmarkMapping("MaxRetenAmt")]
        public decimal QCreditAppRequestTable_MaxRetentionAmount { get; set; }
        [BookmarkMapping("CustPDCBankName")]
        public string QCreditAppRequestTable_CustPDCBankName { get; set; }
        [BookmarkMapping("CustPDCBankBranch")]
        public string QCreditAppRequestTable_CustPDCBankBranch { get; set; }
        [BookmarkMapping("CustPDCBankAccountType")]
        public string QCreditAppRequestTable_CustPDCBankType { get; set; }
        [BookmarkMapping("CustPDCBankAccountNumber")]
        public string QCreditAppRequestTable_CustPDCBankAccountName { get; set; }
        [BookmarkMapping("CreditLimitFee")]
        public decimal QCreditAppRequestTable_CreditRequestFeePct { get; set; }
        [BookmarkMapping("CreditLimitFeeAmt")]
        public decimal QCreditAppRequestTable_CreditRequestFeeAmount { get; set; }
        [BookmarkMapping("FactoringPurchaseFee")]
        public decimal QCreditAppRequestTable_PurchaseFeePct { get; set; }
        [BookmarkMapping("FactoringPurchaseFeeCalBase")]
        public string QCreditAppRequestTable_PurchaseFeeCalculateBase { get; set; }
        [BookmarkMapping("CACondition")]
        public string QCreditAppRequestTable_CACondition { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("RetenDeductMethod01")]
        public string QRetentionConditionTrans1_RetentionDeductionMethod { get; set; }
        [BookmarkMapping("RetenCalBase01")]
        public string QRetentionConditionTrans1_RetentionCalculateBase { get; set; }
        [BookmarkMapping("RetenPerc01")]
        public decimal QRetentionConditionTrans1_RetentionPct { get; set; }
        [BookmarkMapping("RetenAmt01")]
        public decimal tentionConditionTrans1_RetentionAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("RetenDeductMethod02")]
        public string QRetentionConditionTrans2_RetentionDeductionMethod { get; set; }
        [BookmarkMapping("RetenCalBase02")]
        public string QRetentionConditionTrans2_RetentionCalculateBase { get; set; }
        [BookmarkMapping("RetenPerc02")]
        public decimal QRetentionConditionTrans2_RetentionPct { get; set; }
        [BookmarkMapping("RetenAmt02")]
        public decimal tentionConditionTrans2_RetentionAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("RetenDeductMethod03")]
        public string QRetentionConditionTrans3_RetentionDeductionMethod { get; set; }
        [BookmarkMapping("RetenCalBase03")]
        public string QRetentionConditionTrans3_RetentionCalculateBase { get; set; }
        [BookmarkMapping("RetenPerc03")]
        public decimal QRetentionConditionTrans3_RetentionPct { get; set; }
        [BookmarkMapping("RetenAmt03")]
        public decimal tentionConditionTrans3_RetentionAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ServiceFeeDesc01")]
        public string QServiceFeeConditionTrans1_ServicefeeTypeId { get; set; }
        [BookmarkMapping("ServiceFeeAmt01")]
        public decimal QServiceFeeConditionTrans1_AmountBeforeTax { get; set; }
        [BookmarkMapping("ServiceFeeRemark01")]
        public string QServiceFeeConditionTrans1_Description { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ServiceFeeDesc02")]
        public string QServiceFeeConditionTrans2_ServicefeeTypeId { get; set; }
        [BookmarkMapping("ServiceFeeAmt02")]
        public decimal QServiceFeeConditionTrans2_AmountBeforeTax { get; set; }
        [BookmarkMapping("ServiceFeeRemark02")]
        public string QServiceFeeConditionTrans2_Description { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ServiceFeeDesc03")]
        public string QServiceFeeConditionTrans3_ServicefeeTypeId { get; set; }
        [BookmarkMapping("ServiceFeeAmt03")]
        public decimal QServiceFeeConditionTrans3_AmountBeforeTax { get; set; }
        [BookmarkMapping("ServiceFeeRemark03")]
        public string QServiceFeeConditionTrans3_Description { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ServiceFeeDesc04")]
        public string QServiceFeeConditionTrans4_ServicefeeTypeId { get; set; }
        [BookmarkMapping("ServiceFeeAmt04")]
        public decimal QServiceFeeConditionTrans4_AmountBeforeTax { get; set; }
        [BookmarkMapping("ServiceFeeRemark04")]
        public string QServiceFeeConditionTrans4_Description { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ServiceFeeDesc05")]
        public string QServiceFeeConditionTrans5_ServicefeeTypeId { get; set; }
        [BookmarkMapping("ServiceFeeAmt05")]
        public decimal QServiceFeeConditionTrans5_AmountBeforeTax { get; set; }
        [BookmarkMapping("ServiceFeeRemark05")]
        public string QServiceFeeConditionTrans5_Description { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ServiceFeeDesc06")]
        public string QServiceFeeConditionTrans6_ServicefeeTypeId { get; set; }
        [BookmarkMapping("ServiceFeeAmt06")]
        public decimal QServiceFeeConditionTrans6_AmountBeforeTax { get; set; }
        [BookmarkMapping("ServiceFeeRemark06")]
        public string QServiceFeeConditionTrans6_Description { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ServiceFeeDesc07")]
        public string QServiceFeeConditionTrans7_ServicefeeTypeId { get; set; }
        [BookmarkMapping("ServiceFeeAmt07")]
        public decimal QServiceFeeConditionTrans7_AmountBeforeTax { get; set; }
        [BookmarkMapping("ServiceFeeRemark07")]
        public string QServiceFeeConditionTrans7_Description { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ServiceFeeDesc08")]
        public string QServiceFeeConditionTrans8_ServicefeeTypeId { get; set; }
        [BookmarkMapping("ServiceFeeAmt08")]
        public decimal QServiceFeeConditionTrans8_AmountBeforeTax { get; set; }
        [BookmarkMapping("ServiceFeeRemark08")]
        public string QServiceFeeConditionTrans8_Description { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ServiceFeeDesc09")]
        public string QServiceFeeConditionTrans9_ServicefeeTypeId { get; set; }
        [BookmarkMapping("ServiceFeeAmt09")]
        public decimal QServiceFeeConditionTrans9_AmountBeforeTax { get; set; }
        [BookmarkMapping("ServiceFeeRemark09")]
        public string QServiceFeeConditionTrans9_Description { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ServiceFeeDesc10")]
        public string QServiceFeeConditionTrans10_ServicefeeTypeId { get; set; }
        [BookmarkMapping("ServiceFeeAmt10")]
        public decimal QServiceFeeConditionTrans10_AmountBeforeTax { get; set; }
        [BookmarkMapping("ServiceFeeRemark10")]
        public string QServiceFeeConditionTrans10_Description { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("GuarantorNameFirst1")]
        public string QGuarantorTrans1_RelatedPersonName { get; set; }
        [BookmarkMapping("GuarantorPositionFirst1")]
        public string QGuarantorTrans1_GuarantorType { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("GuarantorNameSecond1")]
        public string QGuarantorTrans2_RelatedPersonName { get; set; }
        [BookmarkMapping("GuarantorPositionSecond1")]
        public string QGuarantorTrans2_GuarantorType { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("GuarantorNameThird1")]
        public string QGuarantorTrans3_RelatedPersonName { get; set; }
        [BookmarkMapping("GuarantorPositionThird1")]
        public string QGuarantorTrans3_GuarantorType { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("GuarantorNameFourth1")]
        public string QGuarantorTrans4_RelatedPersonName { get; set; }
        [BookmarkMapping("GuarantorPositionFourth1")]
        public string QGuarantorTrans4_GuarantorType { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("GuarantorNameFifth1")]
        public string QGuarantorTrans5_RelatedPersonName { get; set; }
        [BookmarkMapping("GuarantorPositionFifth1")]
        public string QGuarantorTrans5_GuarantorType { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("GuarantorNameSixth1")]
        public string QGuarantorTrans6_RelatedPersonName { get; set; }
        [BookmarkMapping("GuarantorPositionSixth1")]
        public string QGuarantorTrans6_GuarantorType { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("GuarantorNameSeventh1")]
        public string QGuarantorTrans7_RelatedPersonName { get; set; }
        [BookmarkMapping("GuarantorPositionSeventh1")]
        public string QGuarantorTrans7_GuarantorType { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("GuarantorNameEighth1")]
        public string QGuarantorTrans8_RelatedPersonName { get; set; }
        [BookmarkMapping("GuarantorPositionEighth1")]
        public string QGuarantorTrans8_GuarantorType { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("AssignmentNewFirst1")]
        public string Variable_IsNew1 { get; set; }
        [BookmarkMapping("AssignmentBuyerFirst1")]
        public string QCreditAppReqAssignment1_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdFirst1")]
        public string QCreditAppReqAssignment1_RefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountFirst1")]
        public decimal QCreditAppReqAssignment1_BuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountFirst1")]
        public decimal QCreditAppReqAssignment1_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDateFirst1")]
        public string QCreditAppReqAssignment1_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountFirst1")]
        public decimal QCreditAppReqAssignment1_RemainingAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("AssignmentNewSecond1")]
        public string Variable_IsNew2 { get; set; }
        [BookmarkMapping("AssignmentBuyerSecond1")]
        public string QCreditAppReqAssignment2_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdSecond1")]
        public string QCreditAppReqAssignment2_RefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountSecond1")]
        public decimal QCreditAppReqAssignment2_BuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountSecond1")]
        public decimal QCreditAppReqAssignment2_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDateSecond1")]
        public string QCreditAppReqAssignment2_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountSecond1")]
        public decimal QCreditAppReqAssignment2_RemainingAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("AssignmentNewThird1")]
        public string Variable_IsNew3 { get; set; }
        [BookmarkMapping("AssignmentBuyerThird1")]
        public string QCreditAppReqAssignment3_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdThird1")]
        public string QCreditAppReqAssignment3_RefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountThird1")]
        public decimal QCreditAppReqAssignment3_BuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountThird1")]
        public decimal QCreditAppReqAssignment3_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDateThird1")]
        public string QCreditAppReqAssignment3_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountThird1")]
        public decimal QCreditAppReqAssignment3_RemainingAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("AssignmentNewFourth1")]
        public string Variable_IsNew4 { get; set; }
        [BookmarkMapping("AssignmentBuyerFourth1")]
        public string QCreditAppReqAssignment4_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdFourth1")]
        public string QCreditAppReqAssignment4_RefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountFourth1")]
        public decimal QCreditAppReqAssignment4_BuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountFourth1")]
        public decimal QCreditAppReqAssignment4_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDateFourth1")]
        public string QCreditAppReqAssignment4_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountFourth1")]
        public decimal QCreditAppReqAssignment4_RemainingAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("AssignmentNewFifth1")]
        public string Variable_IsNew5 { get; set; }
        [BookmarkMapping("AssignmentBuyerFifth1")]
        public string QCreditAppReqAssignment5_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdFifth1")]
        public string QCreditAppReqAssignment5_RefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountFifth1")]
        public decimal QCreditAppReqAssignment5_BuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountFifth1")]
        public decimal QCreditAppReqAssignment5_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDateFifth1")]
        public string QCreditAppReqAssignment5_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountFifth1")]
        public decimal QCreditAppReqAssignment5_RemainingAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("AssignmentNewSixth1")]
        public string Variable_IsNew6 { get; set; }
        [BookmarkMapping("AssignmentBuyerSixth1")]
        public string QCreditAppReqAssignment6_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdSixth1")]
        public string QCreditAppReqAssignment6_RefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountSixth1")]
        public decimal QCreditAppReqAssignment6_BuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountSixth1")]
        public decimal QCreditAppReqAssignment6_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDateSixth1")]
        public string QCreditAppReqAssignment6_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountSixth1")]
        public decimal QCreditAppReqAssignment6_RemainingAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("AssignmentNewSeventh1")]
        public string Variable_IsNew7 { get; set; }
        [BookmarkMapping("AssignmentBuyerSeventh1")]
        public string QCreditAppReqAssignment7_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdSeventh1")]
        public string QCreditAppReqAssignment7_RefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountSeventh1")]
        public decimal QCreditAppReqAssignment7_BuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountSeventh1")]
        public decimal QCreditAppReqAssignment7_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDateSeventh1")]
        public string QCreditAppReqAssignment7_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountSeventh1")]
        public decimal QCreditAppReqAssignment7_RemainingAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("AssignmentNewEighth1")]
        public string Variable_IsNew8 { get; set; }
        [BookmarkMapping("AssignmentBuyerEighth1")]
        public string QCreditAppReqAssignment8_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdEighth1")]
        public string QCreditAppReqAssignment8_RefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountEighth1")]
        public decimal QCreditAppReqAssignment8_BuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountEighth1")]
        public decimal QCreditAppReqAssignment8_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDateEighth1")]
        public string QCreditAppReqAssignment8_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountEighth1")]
        public decimal QCreditAppReqAssignment8_RemainingAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("AssignmentNewNineth1")]
        public string Variable_IsNew9 { get; set; }
        [BookmarkMapping("AssignmentBuyerNineth1")]
        public string QCreditAppReqAssignment9_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdNineth1")]
        public string QCreditAppReqAssignment9_RefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountNineth1")]
        public decimal QCreditAppReqAssignment9_BuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountNineth1")]
        public decimal QCreditAppReqAssignment9_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDateNineth1")]
        public string QCreditAppReqAssignment9_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountNineth1")]
        public decimal QCreditAppReqAssignment9_RemainingAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("AssignmentNewTenth1")]
        public string Variable_IsNew10 { get; set; }
        [BookmarkMapping("AssignmentBuyerTenth1")]
        public string QCreditAppReqAssignment10_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdTenth1")]
        public string QCreditAppReqAssignment10_RefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountTenth1")]
        public decimal QCreditAppReqAssignment10_BuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountTenth1")]
        public decimal QCreditAppReqAssignment10_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDateTenth1")]
        public string QCreditAppReqAssignment10_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountTenth1")]
        public decimal QCreditAppReqAssignment10_RemainingAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("SumAssignmentBuyerAgreementAmount")]
        public decimal QSumCreditAppReqAssignment_SumBuyerAgreementAmount { get; set; }
        [BookmarkMapping("SumAssignmentAgreementAmount")]
        public decimal QSumCreditAppReqAssignment_SumAssignmentAgreementAmount { get; set; }
        [BookmarkMapping("SumAssignmentRemainingAmount")]
        public decimal QSumCreditAppReqAssignment_SumRemainingAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewFirst1")]
        public string QCreditAppReqBusinessCollateral1_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeFirst1")]
        public string QCreditAppReqBusinessCollateral1_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeFirst1")]
        public string QCreditAppReqBusinessCollateral1_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescFirst1")]
        public string QCreditAppReqBusinessCollateral1_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueFirst1")]
        public decimal QCreditAppReqBusinessCollateral1_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewSecond1")]
        public string QCreditAppReqBusinessCollateral2_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeSecond1")]
        public string QCreditAppReqBusinessCollateral2_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeSecond1")]
        public string QCreditAppReqBusinessCollateral2_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescSecond1")]
        public string QCreditAppReqBusinessCollateral2_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueSecond1")]
        public decimal QCreditAppReqBusinessCollateral2_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewThird1")]
        public string QCreditAppReqBusinessCollateral3_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeThird1")]
        public string QCreditAppReqBusinessCollateral3_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeThird1")]
        public string QCreditAppReqBusinessCollateral3_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescThird1")]
        public string QCreditAppReqBusinessCollateral3_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueThird1")]
        public decimal QCreditAppReqBusinessCollateral3_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewFourth1")]
        public string QCreditAppReqBusinessCollateral4_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeFourth1")]
        public string QCreditAppReqBusinessCollateral4_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeFourth1")]
        public string QCreditAppReqBusinessCollateral4_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescFourth1")]
        public string QCreditAppReqBusinessCollateral4_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueFourth1")]
        public decimal QCreditAppReqBusinessCollateral4_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewFifth1")]
        public string QCreditAppReqBusinessCollateral5_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeFifth1")]
        public string QCreditAppReqBusinessCollateral5_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeFifth1")]
        public string QCreditAppReqBusinessCollateral5_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescFifth1")]
        public string QCreditAppReqBusinessCollateral5_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueFifth1")]
        public decimal QCreditAppReqBusinessCollateral5_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewSixth1")]
        public string QCreditAppReqBusinessCollateral6_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeSixth1")]
        public string QCreditAppReqBusinessCollateral6_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeSixth1")]
        public string QCreditAppReqBusinessCollateral6_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescSixth1")]
        public string QCreditAppReqBusinessCollateral6_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueSixth1")]
        public decimal QCreditAppReqBusinessCollateral6_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewSeventh1")]
        public string QCreditAppReqBusinessCollateral7_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeSeventh1")]
        public string QCreditAppReqBusinessCollateral7_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeSeventh1")]
        public string QCreditAppReqBusinessCollateral7_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescSeventh1")]
        public string QCreditAppReqBusinessCollateral7_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueSeventh1")]
        public decimal QCreditAppReqBusinessCollateral7_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewEighth1")]
        public string QCreditAppReqBusinessCollateral8_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeEighth1")]
        public string QCreditAppReqBusinessCollateral8_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeEighth1")]
        public string QCreditAppReqBusinessCollateral8_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescEighth1")]
        public string QCreditAppReqBusinessCollateral8_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueEighth1")]
        public decimal QCreditAppReqBusinessCollateral8_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewNineth1")]
        public string QCreditAppReqBusinessCollateral9_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeNineth1")]
        public string QCreditAppReqBusinessCollateral9_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeNineth1")]
        public string QCreditAppReqBusinessCollateral9_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescNineth1")]
        public string QCreditAppReqBusinessCollateral9_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueNineth1")]
        public decimal QCreditAppReqBusinessCollateral9_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewTenth1")]
        public string QCreditAppReqBusinessCollateral10_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeTenth1")]
        public string QCreditAppReqBusinessCollateral10_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeTenth1")]
        public string QCreditAppReqBusinessCollateral10_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescTenth1")]
        public string QCreditAppReqBusinessCollateral10_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueTenth1")]
        public decimal QCreditAppReqBusinessCollateral10_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewEleventh1")]
        public string QCreditAppReqBusinessCollateral11_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeEleventh1")]
        public string QCreditAppReqBusinessCollateral11_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeEleventh1")]
        public string QCreditAppReqBusinessCollateral11_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescEleventh1")]
        public string QCreditAppReqBusinessCollateral11_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueEleventh1")]
        public decimal QCreditAppReqBusinessCollateral11_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewTwelfth1")]
        public string QCreditAppReqBusinessCollateral12_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeTwelfth1")]
        public string QCreditAppReqBusinessCollateral12_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeTwelfth1")]
        public string QCreditAppReqBusinessCollateral12_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescTwelfth1")]
        public string QCreditAppReqBusinessCollateral12_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueTwelfth1")]
        public decimal QCreditAppReqBusinessCollateral12_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewThirteenth1")]
        public string QCreditAppReqBusinessCollateral13_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeThirteenth1")]
        public string QCreditAppReqBusinessCollateral13_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeThirteenth1")]
        public string QCreditAppReqBusinessCollateral13_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescThirteenth1")]
        public string QCreditAppReqBusinessCollateral13_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueThirteenth1")]
        public decimal QCreditAppReqBusinessCollateral13_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewFourteenth1")]
        public string QCreditAppReqBusinessCollateral14_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeFourteenth1")]
        public string QCreditAppReqBusinessCollateral14_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeFourteenth1")]
        public string QCreditAppReqBusinessCollateral14_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescFourteenth1")]
        public string QCreditAppReqBusinessCollateral14_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueFourteenth1")]
        public decimal QCreditAppReqBusinessCollateral14_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewFifteenth1")]
        public string QCreditAppReqBusinessCollateral15_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeFifteenth1")]
        public string QCreditAppReqBusinessCollateral15_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeFifteenth1")]
        public string QCreditAppReqBusinessCollateral15_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescFifteenth1")]
        public string QCreditAppReqBusinessCollateral15_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueFifteenth1")]
        public decimal QCreditAppReqBusinessCollateral15_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("SumBusinessCollateralValue")]
        public decimal QSumCreditAppReqBusinessCollateral_SumBusinessCollateralValue { get; set; }
        [BookmarkMapping("CustCompanyRegistrationID")]
        public string Variable_CustCompanyRegistrationID { get; set; }
        [BookmarkMapping("CustomerAddress")]
        public string QCreditAppRequestTable_RegisteredAddress { get; set; }
        [BookmarkMapping("CustComEstablishedDate")]
        public string Variable_CustComEstablishedDate { get; set; }
        [BookmarkMapping("CustBusinessType")]
        public string QCreditAppRequestTable_BusinessType { get; set; }
        [BookmarkMapping("Channel")]
        public string QCreditAppRequestTable_IntroducedBy { get; set; }
        [BookmarkMapping("CustBankName")]
        public string QCreditAppRequestTable_Instituetion { get; set; }
        [BookmarkMapping("CustBankBranch")]
        public string QCreditAppRequestTable_BankBranch { get; set; }
        [BookmarkMapping("CustBankAccountType")]
        public string QCreditAppRequestTable_BankType { get; set; }
        [BookmarkMapping("CustBankAccountNumber")]
        public string QCreditAppRequestTable_BankAccountName { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustAuthorizedPersonNameFirst1")]
        public string QAuthorizedPersonTrans1_AuthorizedPersonName { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeFirst1")]
        public string Variable_AuthorizedPersonTrans1Age { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustAuthorizedPersonNameSecond1")]
        public string QAuthorizedPersonTrans2_AuthorizedPersonName { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeSecond1")]
        public string Variable_AuthorizedPersonTrans2Age { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustAuthorizedPersonNameThird1")]
        public string QAuthorizedPersonTrans3_AuthorizedPersonName { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeThird1")]
        public string Variable_AuthorizedPersonTrans3Age { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustAuthorizedPersonNameFourth1")]
        public string QAuthorizedPersonTrans4_AuthorizedPersonName { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeFourth1")]
        public string Variable_AuthorizedPersonTrans4Age { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustAuthorizedPersonNameFifth1")]
        public string QAuthorizedPersonTrans5_AuthorizedPersonName { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeFifth1")]
        public string Variable_AuthorizedPersonTrans5Age { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustAuthorizedPersonNameSixth1")]
        public string QAuthorizedPersonTrans6_AuthorizedPersonName { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeSixth1")]
        public string Variable_AuthorizedPersonTrans6Age { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustAuthorizedPersonNameSeventh1")]
        public string QAuthorizedPersonTrans7_AuthorizedPersonName { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeSeventh1")]
        public string Variable_AuthorizedPersonTrans7Age { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustAuthorizedPersonNameEighth1")]
        public string QAuthorizedPersonTrans8_AuthorizedPersonName { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeEighth1")]
        public string Variable_AuthorizedPersonTrans8Age { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustShareHolderPersonNameFirst1")]
        public string QOwnerTrans1_ShareHolderPersonName { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentFirst1")]
        public decimal QOwnerTrans1_PropotionOfShareholderPct { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustShareHolderPersonNameSecond1")]
        public string QOwnerTrans2_ShareHolderPersonName { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentSecond1")]
        public decimal QOwnerTrans2_PropotionOfShareholderPct { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustShareHolderPersonNameThird1")]
        public string QOwnerTrans3_ShareHolderPersonName { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentThird1")]
        public decimal QOwnerTrans3_PropotionOfShareholderPct { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustShareHolderPersonNameFourth1")]
        public string QOwnerTrans4_ShareHolderPersonName { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentFourth1")]
        public decimal QOwnerTrans4_PropotionOfShareholderPct { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustShareHolderPersonNameFifth1")]
        public string QOwnerTrans5_ShareHolderPersonName { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentFifth1")]
        public decimal QOwnerTrans5_PropotionOfShareholderPct { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustShareHolderPersonNameSixth1")]
        public string QOwnerTrans6_ShareHolderPersonName { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentSixth1")]
        public decimal QOwnerTrans6_PropotionOfShareholderPct { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustShareHolderPersonNameSeventh1")]
        public string QOwnerTrans7_ShareHolderPersonName { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentSeventh1")]
        public decimal QOwnerTrans7_PropotionOfShareholderPct { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustShareHolderPersonNameEighth1")]
        public string QOwnerTrans8_ShareHolderPersonName { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentEighth1")]
        public decimal QOwnerTrans8_PropotionOfShareholderPct { get; set; }
        //-----------------------------------------------------------------------------------

        [BookmarkMapping("CustFinRevenuePerMonth1")]
        public decimal QCreditAppRequestTable_SalesAvgPerMonth { get; set; }
        [BookmarkMapping("OutputVATStart")]
        public string QTaxReportStart_TaxMonth { get; set; }
        [BookmarkMapping("OutputVATEnd")]
        public string QTaxReportEnd_TaxMonth { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("Month1")]
        public string QTaxReportTrans1_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth1")]
        public decimal QTaxReportTrans1_Amount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("Month2")]
        public string QTaxReportTrans2_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth2")]
        public decimal QTaxReportTrans2_Amount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("Month3")]
        public string QTaxReportTrans3_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth3")]
        public decimal QTaxReportTrans3_Amount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("Month4")]
        public string QTaxReportTrans4_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth4")]
        public decimal QTaxReportTrans4_Amount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("Month5")]
        public string QTaxReportTrans5_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth5")]
        public decimal QTaxReportTrans5_Amount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("Month6")]
        public string QTaxReportTrans6_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth6")]
        public decimal QTaxReportTrans6_Amount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("Month7")]
        public string QTaxReportTrans7_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth7")]
        public decimal QTaxReportTrans7_Amount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("Month8")]
        public string QTaxReportTrans8_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth8")]
        public decimal QTaxReportTrans8_Amount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("Month9")]
        public string QTaxReportTrans9_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth9")]
        public decimal QTaxReportTrans9_Amount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("Month10")]
        public string QTaxReportTrans10_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth10")]
        public decimal QTaxReportTrans10_Amount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("Month11")]
        public string QTaxReportTrans11_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth11")]
        public decimal QTaxReportTrans11_Amount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("Month12")]
        public string QTaxReportTrans12_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth12")]
        public decimal QTaxReportTrans12_Amount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("TotalOutputVAT")]
        public decimal QTaxReportTransSum_SumTaxReport { get; set; }
        [BookmarkMapping("AverageOutputVATPerMonth")]
        public decimal Variable_AverageOutputVATPerMonth { get; set; }
        [BookmarkMapping("ApprovedCreditAsofDate")]
        public string QCreditAppRequestTable_AsOfDate { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CrditLimitAmtFirst1")]
        public decimal Variable_CrditLimitAmtFirst1 { get; set; }
        [BookmarkMapping("AROutstandingAmtFirst")]
        public decimal Variable_AROutstandingAmtFirst { get; set; }
        [BookmarkMapping("CreditLimitRemainingFirst1")]
        public decimal Variable_CreditLimitRemainingFirst1 { get; set; }
        [BookmarkMapping("ReserveOutstandingFirst1")]
        public decimal Variable_ReserveOutstandingFirst1 { get; set; }
        [BookmarkMapping("RetentionOutstandingFirst1")]
        public decimal Variable_RetentionOutstandingFirst1 { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CrditLimitAmtSecond1")]
        public decimal Variable_CrditLimitAmtSecond1 { get; set; }
        [BookmarkMapping("AROutstandingAmtSecond")]
        public decimal Variable_AROutstandingAmtSecond { get; set; }
        [BookmarkMapping("CreditLimitRemainingSecond1")]
        public decimal Variable_CreditLimitRemainingSecond1 { get; set; }
        [BookmarkMapping("ReserveOutstandingSecond1")]
        public decimal Variable_ReserveOutstandingSecond1 { get; set; }
        [BookmarkMapping("RetentionOutstandingSecond1")]
        public decimal Variable_RetentionOutstandingSecond1 { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CrditLimitAmtThird1")]
        public decimal Variable_CrditLimitAmtThird1 { get; set; }
        [BookmarkMapping("AROutstandingAmtThird")]
        public decimal Variable_AROutstandingAmtThird { get; set; }
        [BookmarkMapping("CreditLimitRemainingThird1")]
        public decimal Variable_CreditLimitRemainingThird1 { get; set; }
        [BookmarkMapping("ReserveOutstandingThird1")]
        public decimal Variable_ReserveOutstandingThird1 { get; set; }
        [BookmarkMapping("RetentionOutstandingThird1")]
        public decimal Variable_RetentionOutstandingThird1 { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CrditLimitAmtFourth1")]
        public decimal Variable_CrditLimitAmtFourth1 { get; set; }
        [BookmarkMapping("AROutstandingAmtFourth")]
        public decimal Variable_AROutstandingAmtFourth { get; set; }
        [BookmarkMapping("CreditLimitRemainingFourth1")]
        public decimal Variable_CreditLimitRemainingFourth1 { get; set; }
        [BookmarkMapping("ReserveOutstandingFourth1")]
        public decimal Variable_ReserveOutstandingFourth1 { get; set; }
        [BookmarkMapping("RetentionOutstandingFourth1")]
        public decimal Variable_RetentionOutstandingFourth1 { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CrditLimitAmtFifth1")]
        public decimal Variable_CrditLimitAmtFifth1 { get; set; }
        [BookmarkMapping("AROutstandingAmtFifth")]
        public decimal Variable_AROutstandingAmtFifth { get; set; }
        [BookmarkMapping("CreditLimitRemainingFifth1")]
        public decimal Variable_CreditLimitRemainingFifth1 { get; set; }
        [BookmarkMapping("ReserveOutstandingFifth1")]
        public decimal Variable_ReserveOutstandingFifth1 { get; set; }
        [BookmarkMapping("RetentionOutstandingFifth1")]
        public decimal Variable_RetentionOutstandingFifth1 { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CrditLimitAmtSixth1")]
        public decimal Variable_CrditLimitAmtSixth1 { get; set; }
        [BookmarkMapping("AROutstandingAmtSixth")]
        public decimal Variable_AROutstandingAmtSixth { get; set; }
        [BookmarkMapping("CreditLimitRemainingSixth1")]
        public decimal Variable_CreditLimitRemainingSixth1 { get; set; }
        [BookmarkMapping("ReserveOutstandingSixth1")]
        public decimal Variable_ReserveOutstandingSixth1 { get; set; }
        [BookmarkMapping("RetentionOutstandingSixth1")]
        public decimal Variable_RetentionOutstandingSixth1 { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CrditLimitAmtSeventh1")]
        public decimal Variable_CrditLimitAmtSeventh1 { get; set; }
        [BookmarkMapping("AROutstandingAmtSeventh")]
        public decimal Variable_AROutstandingAmtSeventh { get; set; }
        [BookmarkMapping("CreditLimitRemainingSeventh1")]
        public decimal Variable_CreditLimitRemainingSeventh1 { get; set; }
        [BookmarkMapping("ReserveOutstandingSeventh1")]
        public decimal Variable_ReserveOutstandingSeventh1 { get; set; }
        [BookmarkMapping("RetentionOutstandingSeventh1")]
        public decimal Variable_RetentionOutstandingSeventh1 { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CrditLimitAmtEighth1")]
        public decimal Variable_CrditLimitAmtEighth1 { get; set; }
        [BookmarkMapping("AROutstandingAmtEighth")]
        public decimal Variable_AROutstandingAmtEighth { get; set; }
        [BookmarkMapping("CreditLimitRemainingEighth1")]
        public decimal Variable_CreditLimitRemainingEighth1 { get; set; }
        [BookmarkMapping("ReserveOutstandingEighth1")]
        public decimal Variable_ReserveOutstandingEighth1 { get; set; }
        [BookmarkMapping("RetentionOutstandingEighth1")]
        public decimal Variable_RetentionOutstandingEighth1 { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CrditLimitAmtNineth1")]
        public decimal Variable_CrditLimitAmtNineth1 { get; set; }
        [BookmarkMapping("AROutstandingAmtNineth")]
        public decimal Variable_AROutstandingAmtNineth { get; set; }
        [BookmarkMapping("CreditLimitRemainingNineth1")]
        public decimal Variable_CreditLimitRemainingNineth1 { get; set; }
        [BookmarkMapping("ReserveOutstandingNineth1")]
        public decimal Variable_ReserveOutstandingNineth1 { get; set; }
        [BookmarkMapping("RetentionOutstandingNineth1")]
        public decimal Variable_RetentionOutstandingNineth1 { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CrditLimitAmtTenth1")]
        public decimal Variable_CrditLimitAmtTenth1 { get; set; }
        [BookmarkMapping("AROutstandingAmtTenth")]
        public decimal Variable_AROutstandingAmtTenth { get; set; }
        [BookmarkMapping("CreditLimitRemainingTenth1")]
        public decimal Variable_CreditLimitRemainingTenth1 { get; set; }
        [BookmarkMapping("ReserveOutstandingTenth1")]
        public decimal Variable_ReserveOutstandingTenth1 { get; set; }
        [BookmarkMapping("RetentionOutstandingTenth1")]
        public decimal Variable_RetentionOutstandingTenth1 { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("TotalCreditLimit")]
        public decimal QCAReqCreditOutStandingSum_SumApprovedCreditLimit { get; set; }
        [BookmarkMapping("TotalAROutstanding")]
        public decimal QCAReqCreditOutStandingSum_SumARBalance { get; set; }
        [BookmarkMapping("TotalCreditLimitRemaining")]
        public decimal QCAReqCreditOutStandingSum_SumCreditLimitBalance { get; set; }
        [BookmarkMapping("TotalReserveOutstanding")]
        public decimal QCAReqCreditOutStandingSum_SumReserveToBeRefund { get; set; }
        [BookmarkMapping("TotalRetentionOutstanding")]
        public decimal QCAReqCreditOutStandingSum_SumAccumRetentionAmount { get; set; }
        [BookmarkMapping("CreditBueroCheckDate")]
        public string QCreditAppRequestTable_NCBCheckedDate { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("FinInstitutionFirst1")]
        public string QNCBTrans1_Institution { get; set; }
        [BookmarkMapping("LoanTypeFirst1")]
        public string QNCBTrans1_LoanType { get; set; }
        [BookmarkMapping("LoanLimitFirst1")]
        public decimal QNCBTrans1_LoanLimit { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("FinInstitutionSecond1")]
        public string QNCBTrans2_Institution { get; set; }
        [BookmarkMapping("LoanTypeSecond1")]
        public string QNCBTrans2_LoanType { get; set; }
        [BookmarkMapping("LoanLimitSecond1")]
        public decimal QNCBTrans2_LoanLimit { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("FinInstitutionThird1")]
        public string QNCBTrans3_Institution { get; set; }
        [BookmarkMapping("LoanTypeThird1")]
        public string QNCBTrans3_LoanType { get; set; }
        [BookmarkMapping("LoanLimitThird1")]
        public decimal QNCBTrans3_LoanLimit { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("FinInstitutionFourth1")]
        public string QNCBTrans4_Institution { get; set; }
        [BookmarkMapping("LoanTypeFourth1")]
        public string QNCBTrans4_LoanType { get; set; }
        [BookmarkMapping("LoanLimitFourth1")]
        public decimal QNCBTrans4_LoanLimit { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("FinInstitutionFifth1")]
        public string QNCBTrans5_Institution { get; set; }
        [BookmarkMapping("LoanTypeFifth1")]
        public string QNCBTrans5_LoanType { get; set; }
        [BookmarkMapping("LoanLimitFifth1")]
        public decimal QNCBTrans5_LoanLimit { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("FinInstitutionSixth1")]
        public string QNCBTrans6_Institution { get; set; }
        [BookmarkMapping("LoanTypeSixth1")]
        public string QNCBTrans6_LoanType { get; set; }
        [BookmarkMapping("LoanLimitSixth1")]
        public decimal QNCBTrans6_LoanLimit { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("FinInstitutionSeventh1")]
        public string QNCBTrans7_Institution { get; set; }
        [BookmarkMapping("LoanTypeSeventh1")]
        public string QNCBTrans7_LoanType { get; set; }
        [BookmarkMapping("LoanLimitSeventh1")]
        public decimal QNCBTrans7_LoanLimit { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("FinInstitutionEighth1")]
        public string QNCBTrans8_Institution { get; set; }
        [BookmarkMapping("LoanTypeEighth1")]
        public string QNCBTrans8_LoanType { get; set; }
        [BookmarkMapping("LoanLimitEighth1")]
        public decimal QNCBTrans8_LoanLimit { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("TotalLoanLimit")]
        public decimal QNCBTransSum_TotalLoanLimit { get; set; }
        [BookmarkMapping("MarketingComment")]
        public string QCreditAppRequestTable_MarketingComment { get; set; }
        [BookmarkMapping("ApproverComment")]
        public string QCreditAppRequestTable_ApproverComment { get; set; }
        [BookmarkMapping("CreditComment")]
        public string QCreditAppRequestTable_CreditComment { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ApproverNameFirst1")]
        public string QActionHistory1_ApproverName { get; set; }
        [BookmarkMapping("ApproverReasonFirst1")]
        public string QActionHistory1_Comment { get; set; }
        [BookmarkMapping("ApprovedDateFirst1")]
        public string QActionHistory1_CreatedDateTime { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ApproverNameSecond1")]
        public string QActionHistory2_ApproverName { get; set; }
        [BookmarkMapping("ApproverReasonSecond1")]
        public string QActionHistory2_Comment { get; set; }
        [BookmarkMapping("ApprovedDateSecond1")]
        public string QActionHistory2_CreatedDateTime { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ApproverNameThird1")]
        public string QActionHistory3_ApproverName { get; set; }
        [BookmarkMapping("ApproverReasonThird1")]
        public string QActionHistory3_Comment { get; set; }
        [BookmarkMapping("ApprovedDateThird1")]
        public string QActionHistory3_CreatedDateTime { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ApproverNameFourth1")]
        public string QActionHistory4_ApproverName { get; set; }
        [BookmarkMapping("ApproverReasonFourth1")]
        public string QActionHistory4_Comment { get; set; }
        [BookmarkMapping("ApprovedDateFourth1")]
        public string QActionHistory4_CreatedDateTime { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ApproverNameFifth1")]
        public string QActionHistory5_ApproverName { get; set; }
        [BookmarkMapping("ApproverReasonFifth1")]
        public string QActionHistory5_Comment { get; set; }
        [BookmarkMapping("ApprovedDateFifth1")]
        public string QActionHistory5_CreatedDateTime { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CreditTermNumberOfDay")]
        public string QCreditAppRequestTableCAPF_NumberOfDays { get; set; }
        public string QCreditAppRequestTableCAPF_CreditTermId { get; set; }
        public string QCreditAppRequestTableCAPF_CreditAppRequestId { get; set; }
        public decimal QCreditAppRequestTableCAPF_SalesAvgPerMonth { get; set; }
        [BookmarkMapping("CreditReq")]
        public decimal QCreditAppRequestTableCAPF_CreditLimitRequest { get; set; }
        ////

        public Guid? QCreditAppRequestLineCAPF_CreditAppRequestLineGUID { get; set; }
        public Guid? QCreditAppRequestLineCAPF_BuyerTableGUID { get; set; }
        public Guid? QCreditAppRequestLineCAPF_InvoiceAddressGUID { get; set; }

        public decimal QCreditAppRequestLineCAPF_CreditLimitLineRequest { get; set; }
        public string QCreditAppRequestLineCAPF_InvoiceAddress { get; set; }
        public string QCreditAppRequestLineCAPF_CreditScoring { get; set; }
        public string QCreditAppRequestLineCAPF_BlacklistStatus { get; set; }


        /// 
        [BookmarkMapping("BuyName")]
        public string QBuyerTableCAPF_BuyerName { get; set; }
        [BookmarkMapping("BuyID")]
        public string QBuyerTableCAPF_BuyerId { get; set; }
        public string QBuyerTableCAPF_TaxId { get; set; }
        public DateTime? QCreditAppRequestLineCAPF_DateOfEstablish { get; set; }

        public string QBuyerTableCAPF_BusinessSegmentDesc { get; set; }
        public string QBuyerTableCAPF_LineOfBusinessDesc { get; set; }

        //
        public Guid? QBuyerAgreementCAPF_BuyerAgreementTableGUID { get; set; }
        [BookmarkMapping("ProjConNum1")]
        public string QBuyerAgreementCAPF_BuyerAgreementId { get; set; }
        [BookmarkMapping("ProjectConName")]
        public string QBuyerAgreementCAPF_BuyerAgreement { get; set; }
        [BookmarkMapping("ProjectConStartDate")]
        public DateTime? QBuyerAgreementCAPF_StartDate { get; set; }
        [BookmarkMapping("ProjectConEndDate")]
        public DateTime? QBuyerAgreementCAPF_EndDate { get; set; }
        [BookmarkMapping("ProjectConDescription")]
        public string QBuyerAgreementCAPF_Remark { get; set; }
        [BookmarkMapping("Penalty")]
        public string QBuyerAgreementCAPF_Penalty { get; set; }
        //
        [BookmarkMapping("ProjConPaymentPeriod1st_1")]
        public int QBuyerAgreementLineCAPF_Period_1 { get; set; }
        [BookmarkMapping("ProjConPaymentDes1st_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_1 { get; set; }
        [BookmarkMapping("ProjConPayementAmt1st_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_1 { get; set; }
        [BookmarkMapping("ProjConDueDate1st_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_1 { get; set; }

        [BookmarkMapping("ProjConPaymentPeriod2nd_1")]
        public int QBuyerAgreementLineCAPF_Period_2 { get; set; }
        [BookmarkMapping("ProjConPaymentDes2nd_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_2 { get; set; }
        [BookmarkMapping("ProjConPayementAmt2nd_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_2 { get; set; }
        [BookmarkMapping("ProjConDueDate2nd_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_2 { get; set; }

        [BookmarkMapping("ProjConPaymentPeriod3rd_1")]
        public int QBuyerAgreementLineCAPF_Period_3 { get; set; }
        [BookmarkMapping("ProjConPaymentDes3rd_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_3 { get; set; }
        [BookmarkMapping("ProjConPayementAmt3rd_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_3 { get; set; }
        [BookmarkMapping("ProjConDueDate3rd_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_3 { get; set; }
        [BookmarkMapping("ProjConPaymentPeriod4th_1")]
        public int QBuyerAgreementLineCAPF_Period_4 { get; set; }
        [BookmarkMapping("ProjConPaymentDes4th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_4 { get; set; }
        [BookmarkMapping("ProjConPayementAmt4th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_4 { get; set; }
        [BookmarkMapping("ProjConDueDate4th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_4 { get; set; }
        [BookmarkMapping("ProjConPaymentPeriod5th_1")]
        public int QBuyerAgreementLineCAPF_Period_5 { get; set; }
        [BookmarkMapping("ProjConPaymentDes5th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_5 { get; set; }
        [BookmarkMapping("ProjConPayementAmt5th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_5 { get; set; }
        [BookmarkMapping("ProjConDueDate5th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_5 { get; set; }
        [BookmarkMapping("ProjConPaymentPeriod6th_1")]
        public int QBuyerAgreementLineCAPF_Period_6 { get; set; }
        [BookmarkMapping("ProjConPaymentDes6th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_6 { get; set; }
        [BookmarkMapping("ProjConPayementAmt6th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_6 { get; set; }
        [BookmarkMapping("ProjConDueDate6th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_6 { get; set; }
        [BookmarkMapping("ProjConPaymentPeriod7th_1")]
        public int QBuyerAgreementLineCAPF_Period_7 { get; set; }
        [BookmarkMapping("ProjConPaymentDes7th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_7 { get; set; }
        [BookmarkMapping("ProjConPayementAmt7th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_7 { get; set; }
        [BookmarkMapping("ProjConDueDate7th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_7 { get; set; }
        [BookmarkMapping("ProjConPaymentPeriod8th_1")]
        public int QBuyerAgreementLineCAPF_Period_8 { get; set; }
        [BookmarkMapping("ProjConPaymentDes8th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_8 { get; set; }
        [BookmarkMapping("ProjConPayementAmt8th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_8 { get; set; }
        [BookmarkMapping("ProjConDueDate8th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_8 { get; set; }
        [BookmarkMapping("ProjConPaymentPeriod9th_1")]
        public int QBuyerAgreementLineCAPF_Period_9 { get; set; }
        [BookmarkMapping("ProjConPaymentDes9th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_9 { get; set; }
        [BookmarkMapping("ProjConPayementAmt9th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_9 { get; set; }
        [BookmarkMapping("ProjConDueDate9th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_9 { get; set; }
        [BookmarkMapping("ProjConPaymentPeriod10th_1")]
        public int QBuyerAgreementLineCAPF_Period_10 { get; set; }
        [BookmarkMapping("ProjConPaymentDes10th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_10 { get; set; }
        [BookmarkMapping("ProjConPayementAmt10th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_10 { get; set; }
        [BookmarkMapping("ProjConDueDate10th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_10 { get; set; }

        [BookmarkMapping("ProjConPaymentPeriod11th_1")]
        public int QBuyerAgreementLineCAPF_Period_11 { get; set; }
        [BookmarkMapping("ProjConPaymentDes11th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_11 { get; set; }
        [BookmarkMapping("ProjConPayementAmt11th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_11 { get; set; }
        [BookmarkMapping("ProjConDueDate11th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_11 { get; set; }
        [BookmarkMapping("ProjConPaymentPeriod12th_1")]
        public int QBuyerAgreementLineCAPF_Period_12 { get; set; }
        [BookmarkMapping("ProjConPaymentDes12th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_12 { get; set; }
        [BookmarkMapping("ProjConPayementAmt12th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_12 { get; set; }
        [BookmarkMapping("ProjConDueDate12th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_12 { get; set; }

        [BookmarkMapping("ProjConPaymentPeriod13th_1")]
        public int QBuyerAgreementLineCAPF_Period_13 { get; set; }
        [BookmarkMapping("ProjConPaymentDes13th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_13 { get; set; }
        [BookmarkMapping("ProjConPayementAmt13th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_13 { get; set; }
        [BookmarkMapping("ProjConDueDate13th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_13 { get; set; }
        [BookmarkMapping("ProjConPaymentPeriod14th_1")]
        public int QBuyerAgreementLineCAPF_Period_14 { get; set; }
        [BookmarkMapping("ProjConPaymentDes14th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_14 { get; set; }
        [BookmarkMapping("ProjConPayementAmt14th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_14 { get; set; }
        [BookmarkMapping("ProjConDueDate14th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_14 { get; set; }

        [BookmarkMapping("ProjConPaymentPeriod15th_1")]
        public int QBuyerAgreementLineCAPF_Period_15 { get; set; }
        [BookmarkMapping("ProjConPaymentDes15th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_15 { get; set; }
        [BookmarkMapping("ProjConPayementAmt15th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_15 { get; set; }
        [BookmarkMapping("ProjConDueDate15th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_15 { get; set; }
        [BookmarkMapping("ProjConPaymentPeriod16th_1")]
        public int QBuyerAgreementLineCAPF_Period_16 { get; set; }
        [BookmarkMapping("ProjConPaymentDes16th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_16 { get; set; }
        [BookmarkMapping("ProjConPayementAmt16th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_16 { get; set; }
        [BookmarkMapping("ProjConDueDate16th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_16 { get; set; }

        [BookmarkMapping("ProjConPaymentPeriod17th_1")]
        public int QBuyerAgreementLineCAPF_Period_17 { get; set; }
        [BookmarkMapping("ProjConPaymentDes17th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_17 { get; set; }
        [BookmarkMapping("ProjConPayementAmt17th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_17 { get; set; }
        [BookmarkMapping("ProjConDueDate17th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_17 { get; set; }
        [BookmarkMapping("ProjConPaymentPeriod18th_1")]
        public int QBuyerAgreementLineCAPF_Period_18 { get; set; }
        [BookmarkMapping("ProjConPaymentDes18th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_18 { get; set; }
        [BookmarkMapping("ProjConPayementAmt18th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_18 { get; set; }
        [BookmarkMapping("ProjConDueDate18th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_18 { get; set; }

        [BookmarkMapping("ProjConPaymentPeriod19th_1")]
        public int QBuyerAgreementLineCAPF_Period_19 { get; set; }
        [BookmarkMapping("ProjConPaymentDes19th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_19 { get; set; }
        [BookmarkMapping("ProjConPayementAmt19th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_19 { get; set; }
        [BookmarkMapping("ProjConDueDate19th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_19 { get; set; }

        [BookmarkMapping("ProjConPaymentPeriod20th_1")]
        public int QBuyerAgreementLineCAPF_Period_20 { get; set; }
        [BookmarkMapping("ProjConPaymentDes20th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_20 { get; set; }
        [BookmarkMapping("ProjConPayementAmt20th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_20 { get; set; }
        [BookmarkMapping("ProjConDueDate20th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_20 { get; set; }

        [BookmarkMapping("ProjectConValue")]
        public decimal QSumBuyerAgreementLineCAPF_SumBuyerAgreementAmount { get; set; }
        [BookmarkMapping("LoanRequest")]
        public decimal QSumCreditAppRequestLine_SumCreditLimitLineRequest { get; set; }

        [BookmarkMapping("SUMProjConPayementAmt")]
        public decimal QSum10BuyerAgreementLine_SumBuyerAgreementAmount { get; set; }
        [BookmarkMapping("BusinessSegment")]
        public string QCreditAppRequestLine_BusinessSegmentDesc { get; set; }
        [BookmarkMapping("BuyerName")]
        public string QCreditAppRequestLine_BuyerName { get; set; }
        [BookmarkMapping("BuyerAddress")]
        public string QCreditAppRequestLine_InvoiceAddress { get; set; }
        [BookmarkMapping("BuyerBusinessType")]
        public string QCreditAppRequestLine_LineOfBusinessDesc { get; set; }
        [BookmarkMapping("BuyerBlacklistStatus")]
        public string QCreditAppRequestLine_BlacklistStatusDesc { get; set; }
        [BookmarkMapping("BuyerRegisNum")]
        public string QCreditAppRequestLine_TaxId { get; set; }
        [BookmarkMapping("BuyerFoundingDate")]
        public string QCreditAppRequestLine_DateOfEstablish { get; set; }
        [BookmarkMapping("BuyerCreditScore")]
        public string QCreditAppRequestLine_CreditScoringDesc { get; set; }
        [BookmarkMapping("BuyerFINStateYear_1", "BuyerInComeStateYear_1")]
        public int QCreditAppReqLineFin1_Year { get; set; }
        [BookmarkMapping("BuyerFINStateShareholder_1")]
        public decimal QCreditAppReqLineFin1_RegisteredCapital { get; set; }
        [BookmarkMapping("BuyerFINStatePaidShareHolder_1")]
        public decimal QCreditAppReqLineFin1_PaidCapital { get; set; }
        [BookmarkMapping("BuyerFINStateAssets_1")]
        public decimal QCreditAppReqLineFin1_TotalAsset { get; set; }
        [BookmarkMapping("BuyerFINStateLiability_1")]
        public decimal QCreditAppReqLineFin1_TotalLiability { get; set; }
        [BookmarkMapping("BuyerFINStateEquiry_1")]
        public decimal QCreditAppReqLineFin1_TotalEquity { get; set; }
        [BookmarkMapping("BuyerInComeStateRev_1")]
        public decimal QCreditAppReqLineFin1_TotalRevenue { get; set; }
        [BookmarkMapping("BuyerInComeStateCost_1")]
        public decimal QCreditAppReqLineFin1_TotalCOGS { get; set; }
        [BookmarkMapping("BuyerInComeStateGrossProfit_1")]
        public decimal QCreditAppReqLineFin1_TotalGrossProfit { get; set; }
        [BookmarkMapping("BuyerInComeStateExp_1")]
        public decimal QCreditAppReqLineFin1_TotalOperExpFirst { get; set; }
        [BookmarkMapping("BuyerInComeStateNetProfit_1")]
        public decimal QCreditAppReqLineFin1_TotalNetProfitFirst { get; set; }
        [BookmarkMapping("BuyerFINStateYear_2" , "BuyerInComeStateYear_2")]
        public int QCreditAppReqLineFin2_Year { get; set; }
        [BookmarkMapping("BuyerFINStateShareholder_2")]
        public decimal QCreditAppReqLineFin2_RegisteredCapital { get; set; }
        [BookmarkMapping("BuyerFINStatePaidShareHolder_2")]
        public decimal QCreditAppReqLineFin2_PaidCapital { get; set; }
        [BookmarkMapping("BuyerFINStateAssets_2")]
        public decimal QCreditAppReqLineFin2_TotalAsset { get; set; }
        [BookmarkMapping("BuyerFINStateLiability_2")]
        public decimal QCreditAppReqLineFin2_TotalLiability { get; set; }
        [BookmarkMapping("BuyerFINStateEquiry_2")]
        public decimal QCreditAppReqLineFin2_TotalEquity { get; set; }
        [BookmarkMapping("BuyerInComeStateRev_2")]
        public decimal QCreditAppReqLineFin2_TotalRevenue { get; set; }
        [BookmarkMapping("BuyerInComeStateCost_2")]
        public decimal QCreditAppReqLineFin2_TotalCOGS { get; set; }
        [BookmarkMapping("BuyerInComeStateGrossProfit_2")]
        public decimal QCreditAppReqLineFin2_TotalGrossProfit { get; set; }
        [BookmarkMapping("BuyerInComeStateExp_2")]
        public decimal QCreditAppReqLineFin2_TotalOperExpFirst { get; set; }
        [BookmarkMapping("BuyerInComeStateNetProfit_2")]
        public decimal QCreditAppReqLineFin2_TotalNetProfitFirst { get; set; }
        [BookmarkMapping("BuyerFINStateYear_3" , "BuyerInComeStateYear_3")]
        public int QCreditAppReqLineFin3_Year { get; set; }
        [BookmarkMapping("BuyerFINStateShareholder_3")]
        public decimal QCreditAppReqLineFin3_RegisteredCapital { get; set; }
        [BookmarkMapping("BuyerFINStatePaidShareHolder_3")]
        public decimal QCreditAppReqLineFin3_PaidCapital { get; set; }
        [BookmarkMapping("BuyerFINStateAssets_3")]
        public decimal QCreditAppReqLineFin3_TotalAsset { get; set; }
        [BookmarkMapping("BuyerFINStateLiability_3")]
        public decimal QCreditAppReqLineFin3_TotalLiability { get; set; }
        [BookmarkMapping("BuyerFINStateEquiry_3")]
        public decimal QCreditAppReqLineFin3_TotalEquity { get; set; }
        [BookmarkMapping("BuyerInComeStateRev_3")]
        public decimal QCreditAppReqLineFin3_TotalRevenue { get; set; }
        [BookmarkMapping("BuyerInComeStateCost_3")]
        public decimal QCreditAppReqLineFin3_TotalCOGS { get; set; }
        [BookmarkMapping("BuyerInComeStateGrossProfit_3")]
        public decimal QCreditAppReqLineFin3_TotalGrossProfit { get; set; }
        [BookmarkMapping("BuyerInComeStateExp_3")]
        public decimal QCreditAppReqLineFin3_TotalOperExpFirst { get; set; }
        [BookmarkMapping("BuyerInComeStateNetProfit_3")]
        public decimal QCreditAppReqLineFin3_TotalNetProfitFirst { get; set; }
        [BookmarkMapping("BuyerNetProfit")]
        public decimal QCreditAppReqLineFin1_NetProfitPercent { get; set; }
        [BookmarkMapping("BuyerDE")]
        public decimal QCreditAppReqLineFin1_lDE { get; set; }
        [BookmarkMapping("BuyerCurrentRatio")]
        public decimal QCreditAppReqLineFin1_QuickRatio { get; set; }
        [BookmarkMapping("BuyerInterestCoverageRatio")]
        public decimal QCreditAppReqLineFin1_IntCoverageRatio { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("SumServiceFeeAmt")]
        public decimal QSumServiceFeeConditionTrans_ServiceFeeAmt { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustFinYearFirst1", "CustFinYearFirst2")]
        public int QCCreditAppReqLineFin1_Year { get; set; }
        [BookmarkMapping("CustFinRegisteredCapitalFirst1")]
        public decimal QCCreditAppReqLineFin1_RegisteredCapital { get; set; }
        [BookmarkMapping("CustFinPaidCapitalFirst1")]
        public decimal QCCreditAppReqLineFin1_PaidCapital { get; set; }
        [BookmarkMapping("CustFinTotalAssetFirst1")]
        public decimal QCCreditAppReqLineFin1_TotalAsset { get; set; }
        [BookmarkMapping("CustFinTotalLiabilityFirst1")]
        public decimal QCCreditAppReqLineFin1_TotalLiability { get; set; }
        [BookmarkMapping("CustFinTotalEquityFirst1")]
        public decimal QCCreditAppReqLineFin1_TotalEquity { get; set; }
        [BookmarkMapping("CustFinTotalRevenueFirst1")]
        public decimal QCCreditAppReqLineFin1_TotalRevenue { get; set; }
        [BookmarkMapping("CustFinTotalCOGSFirst1")]
        public decimal QCCreditAppReqLineFin1_TotalCOGS { get; set; }
        [BookmarkMapping("CustFinTotalGrossProfitFirst1")]
        public decimal QCCreditAppReqLineFin1_TotalGrossProfit { get; set; }
        [BookmarkMapping("CustFinIntCoverageRatio1")]
        public decimal QCCreditAppReqLineFin1_IntCoverageRatio { get; set; }
        [BookmarkMapping("CustFinTotalOperExpFirst1")]
        public decimal QCCreditAppReqLineFin1_TotalOperExpFirst { get; set; }
        [BookmarkMapping("CustFinTotalNetProfitFirst1")]
        public decimal QCCreditAppReqLineFin1_TotalNetProfitFirst { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustFinYearSecond1", "CustFinYearSecond2")]
        public int QCCreditAppReqLineFin2_Year { get; set; }
        [BookmarkMapping("CustFinRegisteredCapitalSecond1")]
        public decimal QCCreditAppReqLineFin2_RegisteredCapital { get; set; }
        [BookmarkMapping("CustFinPaidCapitalSecond1")]
        public decimal QCCreditAppReqLineFin2_PaidCapital { get; set; }
        [BookmarkMapping("CustFinTotalAssetSecond1")]
        public decimal QCCreditAppReqLineFin2_TotalAsset { get; set; }
        [BookmarkMapping("CustFinTotalLiabilitySecond1")]
        public decimal QCCreditAppReqLineFin2_TotalLiability { get; set; }
        [BookmarkMapping("CustFinTotalEquitySecond1")]
        public decimal QCCreditAppReqLineFin2_TotalEquity { get; set; }
        [BookmarkMapping("CustFinTotalRevenueSecond1")]
        public decimal QCCreditAppReqLineFin2_TotalRevenue { get; set; }
        [BookmarkMapping("CustFinTotalCOGSSecond1")]
        public decimal QCCreditAppReqLineFin2_TotalCOGS { get; set; }
        [BookmarkMapping("CustFinTotalGrossProfitSecond1")]
        public decimal QCCreditAppReqLineFin2_TotalGrossProfit { get; set; }
        [BookmarkMapping("CustFinTotalOperExpSecond1")]
        public decimal QCCreditAppReqLineFin2_TotalOperExpSecond { get; set; }
        [BookmarkMapping("CustFinTotalNetProfitSecond1")]
        public decimal QCCreditAppReqLineFin2_TotalNetProfitSecond { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustFinYearThird1", "CustFinYearThird2")]
        public int QCCreditAppReqLineFin3_Year { get; set; }
        [BookmarkMapping("CustFinRegisteredCapitalThird1")]
        public decimal QCCreditAppReqLineFin3_RegisteredCapital { get; set; }
        [BookmarkMapping("CustFinPaidCapitalThird1")]
        public decimal QCCreditAppReqLineFin3_PaidCapital { get; set; }
        [BookmarkMapping("CustFinTotalAssetThird1")]
        public decimal QCCreditAppReqLineFin3_TotalAsset { get; set; }
        [BookmarkMapping("CustFinTotalLiabilityThird1")]
        public decimal QCCreditAppReqLineFin3_TotalLiability { get; set; }
        [BookmarkMapping("CustFinTotalEquityThird1")]
        public decimal QCCreditAppReqLineFin3_TotalEquity { get; set; }
        [BookmarkMapping("CustFinTotalRevenueThird1")]
        public decimal QCCreditAppReqLineFin3_TotalRevenue { get; set; }
        [BookmarkMapping("CustFinTotalCOGSThird1")]
        public decimal QCCreditAppReqLineFin3_TotalCOGS { get; set; }
        [BookmarkMapping("CustFinTotalGrossProfitThird1")]
        public decimal QCCreditAppReqLineFin3_TotalGrossProfit { get; set; }
        [BookmarkMapping("CustFinTotalOperExpThird1")]
        public decimal QCCreditAppReqLineFin3_TotalOperExpThird { get; set; }
        [BookmarkMapping("CustFinTotalNetProfitThird1")]
        public decimal QCCreditAppReqLineFin3_TotalNetProfitThird { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustFinYearFourth1", "CustFinYearFourth2")]
        public int QCCreditAppReqLineFin4_Year { get; set; }
        [BookmarkMapping("CustFinRegisteredCapitalFourth1")]
        public decimal QCCreditAppReqLineFin4_RegisteredCapital { get; set; }
        [BookmarkMapping("CustFinPaidCapitalFourth1")]
        public decimal QCCreditAppReqLineFin4_PaidCapital { get; set; }
        [BookmarkMapping("CustFinTotalAssetFourth1")]
        public decimal QCCreditAppReqLineFin4_TotalAsset { get; set; }
        [BookmarkMapping("CustFinTotalLiabilityFourth1")]
        public decimal QCCreditAppReqLineFin4_TotalLiability { get; set; }
        [BookmarkMapping("CustFinTotalEquityFourth1")]
        public decimal QCCreditAppReqLineFin4_TotalEquity { get; set; }
        [BookmarkMapping("CustFinTotalRevenueFourth1")]
        public decimal QCCreditAppReqLineFin4_TotalRevenue { get; set; }
        [BookmarkMapping("CustFinTotalCOGSFourth1")]
        public decimal QCCreditAppReqLineFin4_TotalCOGS { get; set; }
        [BookmarkMapping("CustFinTotalGrossProfitFourth1")]
        public decimal QCCreditAppReqLineFin4_TotalGrossProfit { get; set; }
        [BookmarkMapping("CustFinTotalOperExpFourth1")]
        public decimal QCCreditAppReqLineFin4_TotalOperExpFourth { get; set; }
        [BookmarkMapping("CustFinTotalNetProfitFourth1")]
        public decimal QCCreditAppReqLineFin4_TotalNetProfitFourth { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustFinYearFifth1", "CustFinYearFifth2")]
        public int QCCreditAppReqLineFin5_Year { get; set; }
        [BookmarkMapping("CustFinRegisteredCapitalFifth1")]
        public decimal QCCreditAppReqLineFin5_RegisteredCapital { get; set; }
        [BookmarkMapping("CustFinPaidCapitalFifth1")]
        public decimal QCCreditAppReqLineFin5_PaidCapital { get; set; }
        [BookmarkMapping("CustFinTotalAssetFifth1")]
        public decimal QCCreditAppReqLineFin5_TotalAsset { get; set; }
        [BookmarkMapping("CustFinTotalLiabilityFifth1")]
        public decimal QCCreditAppReqLineFin5_TotalLiability { get; set; }
        [BookmarkMapping("CustFinTotalEquityFifth1")]
        public decimal QCCreditAppReqLineFin5_TotalEquity { get; set; }
        [BookmarkMapping("CustFinTotalRevenueFifth1")]
        public decimal QCCreditAppReqLineFin5_TotalRevenue { get; set; }
        [BookmarkMapping("CustFinTotalCOGSFifth1")]
        public decimal QCCreditAppReqLineFin5_TotalCOGS { get; set; }
        [BookmarkMapping("CustFinTotalGrossProfitFifth1")]
        public decimal QCCreditAppReqLineFin5_TotalGrossProfit { get; set; }
        [BookmarkMapping("CustFinTotalOperExpFifth1")]
        public decimal QCCreditAppReqLineFin5_TotalOperExpFifth { get; set; }
        [BookmarkMapping("CustFinTotalNetProfitFifth1")]
        public decimal QCCreditAppReqLineFin5_TotalNetProfitFifth { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustFinNetProfitPercent1")]
        public decimal QCCreditAppReqLineFin1_NetProfitPercent { get; set; }
        [BookmarkMapping("CustFinlDE1")]
        public decimal QCCreditAppReqLineFin1_lDE { get; set; }
        [BookmarkMapping("CustFinQuickRatio1")]
        public decimal QCCreditAppReqLineFin1_QuickRatio { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CrditLimitAmtStart1")]
        public decimal QCAReqCreditOutStanding1_ApprovedCreditLimit { get; set; }
        [BookmarkMapping("AROutstandingAmtStart")]
        public decimal QCAReqCreditOutStanding1_ARBalance { get; set; }
        [BookmarkMapping("CreditLimitRemainingStart1")]
        public decimal QCAReqCreditOutStanding1_CreditLimitBalance { get; set; }
        [BookmarkMapping("ReserveOutstandingStart1")]
        public decimal QCAReqCreditOutStanding1_ReserveToBeRefund { get; set; }
        [BookmarkMapping("RetentionOutstandingStart1")]
        public decimal QCAReqCreditOutStanding1_AccumRetentionAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("FactoringPurchaseCondition")]
        public string QCreditAppRequestTable_PurchaseWithdrawalCondition { get; set; }
        [BookmarkMapping("AssignmentCondition")]
        public string QCreditAppRequestTable_AssignmentMethod { get; set; }
    }
}
