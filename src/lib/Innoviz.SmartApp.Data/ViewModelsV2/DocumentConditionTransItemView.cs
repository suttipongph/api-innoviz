using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class DocumentConditionTransItemView : ViewCompanyBaseEntity
	{
		public string DocumentConditionTransGUID { get; set; }
		public int DocConVerifyType { get; set; }
		public string DocumentTypeGUID { get; set; }
		public bool Mandatory { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public string RefId { get; set; }
		public string RefDocumentConditionTransGUID { get; set; }
		public bool Inactive { get; set; }
	}
}
