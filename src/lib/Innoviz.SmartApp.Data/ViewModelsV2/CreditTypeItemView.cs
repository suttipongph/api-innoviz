using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CreditTypeItemView : ViewCompanyBaseEntity
	{
		public string CreditTypeGUID { get; set; }
		public string CreditTypeId { get; set; }
		public string Description { get; set; }
	}
}
