using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ProcessTransListView : ViewCompanyBaseEntity
	{
		public string ProcessTransGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public int StagingBatchStatus { get; set; }
		public string CreditAppTableGUID { get; set; }
		public int ProductType { get; set; }
		public string TransDate { get; set; }
		public int ProcessTransType { get; set; }
		public decimal Amount { get; set; }
		public decimal TaxAmount { get; set; }
		public bool Revert { get; set; }
		public string StagingBatchId { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string InvoiceTableGUID { get; set; }
		public string InvoiceTable_Values { get; set; }
		public string RefId { get; set; }
		public string RefType { get; set; }
		public string InvoiceTable_InvoiceId { get; set; }
		public string CreditAppTable_CreditappId { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string RefGUID { get; set; }
	}
}
