using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ApplicationTableListView : ViewCompanyBaseEntity
	{
		public string ApplicationTableGUID { get; set; }
	}
}
