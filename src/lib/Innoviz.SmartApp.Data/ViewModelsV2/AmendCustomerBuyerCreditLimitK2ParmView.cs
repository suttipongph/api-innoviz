﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class AmendCustomerBuyerCreditLimitK2ParmView
    {
        public string ParmDocGUID { get; set; }
        public string CompanyGUID { get; set; }
        public string WorkflowName { get; set; }
        public string Owner { get; set; }
        public string OwnerBusinessUnitGUID { get; set; }
    }
}
