using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class IntercompanyItemView : ViewCompanyBaseEntity
	{
		public string IntercompanyGUID { get; set; }
		public string Description { get; set; }
		public string IntercompanyId { get; set; }
	}
}
