using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class RetentionConditionTransListView : ViewCompanyBaseEntity
	{
		public string RetentionConditionTransGUID { get; set; }
		public int ProductType { get; set; }
		public int RetentionDeductionMethod { get; set; }
		public int RetentionCalculateBase { get; set; }
		public decimal RetentionPct { get; set; }
		public decimal RetentionAmount { get; set; }
		public string RefGUID { get; set; }
	}
}
