using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class WithholdingTaxTableListView : ViewCompanyBaseEntity
	{
		public string WithholdingTaxTableGUID { get; set; }
		public string WHTCode { get; set; }
		public string Description { get; set; }
	}
}
