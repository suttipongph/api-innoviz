using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class InquiryPurchaseLineOutstandingListView : ViewCompanyBaseEntity
	{
		public string PurchaseLineGUID { get; set; }
		public string PurchaseTable_Values { get; set; }
		public string PurchaseTableGUID { get; set; }
		public string PurchaseTable_PurchaseId { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CustomerTableGUID { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string BuyerTable_Values { get; set; }
		public string BuyerTableGUID { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public decimal InvoiceAmount { get; set; }
		public decimal PurchaseAmount { get; set; }
		public decimal Outstanding { get; set; }
		public string DueDate { get; set; }
		public string BillingDate { get; set; }
		public string CollectionDate { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public string MethodOfPaymentGUID { get; set; }
		public string MethodOfPayment_MethodOfPaymentId { get; set; }
	}
}
