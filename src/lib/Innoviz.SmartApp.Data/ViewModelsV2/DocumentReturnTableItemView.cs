using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class DocumentReturnTableItemView : ViewCompanyBaseEntity
	{
		public string DocumentReturnTableGUID { get; set; }
		public string ActualReturnDate { get; set; }
		public string Address { get; set; }
		public string BuyerTableGUID { get; set; }
		public string ContactPersonName { get; set; }
		public int ContactTo { get; set; }
		public string CustomerTableGUID { get; set; }
		public string DocumentReturnId { get; set; }
		public string DocumentReturnMethodGUID { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string ExpectedReturnDate { get; set; }
		public string PostAcknowledgementNo { get; set; }
		public string PostNumber { get; set; }
		public string ReceivedBy { get; set; }
		public string Remark { get; set; }
		public string RequestorGUID { get; set; }
		public string ReturnByGUID { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string OriginalDocumentStatus { get; set; }
		public string OriginalDocumentStatusGUID { get; set; }
	public NotificationResponse Notification { get; set; } = new NotificationResponse();

	}
}
