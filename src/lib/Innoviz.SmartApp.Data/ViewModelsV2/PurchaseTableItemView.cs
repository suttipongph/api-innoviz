using Innoviz.SmartApp.Core.ViewModels;
using NJsonSchema.Annotations;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	[JsonSchemaFlatten]
	public class PurchaseTableItemView : ViewCompanyBaseEntity
	{
		public string PurchaseTableGUID { get; set; }
		public bool AdditionalPurchase { get; set; }
		public string CreditAppTableGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public string Description { get; set; }
		public bool DiffChequeIssuedName { get; set; }
		public string Dimension1GUID { get; set; }
		public string Dimension2GUID { get; set; }
		public string Dimension3GUID { get; set; }
		public string Dimension4GUID { get; set; }
		public string Dimension5GUID { get; set; }
		public string DocumentStatusGUID { get; set; }
		public decimal OverCreditBuyer { get; set; }
		public decimal OverCreditCA { get; set; }
		public decimal OverCreditCALine { get; set; }
		public int ProductType { get; set; }
		public string PurchaseDate { get; set; }
		public string PurchaseId { get; set; }
		public string ReceiptTempTableGUID { get; set; }
		public int RetentionCalculateBase { get; set; }
		public decimal RetentionFixedAmount { get; set; }
		public decimal RetentionPct { get; set; }
		public bool Rollbill { get; set; }
		public decimal SumPurchaseAmount { get; set; }
		public decimal TotalInterestPct { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string DocumentStatus_Description { get; set; }
		public decimal NetPaid { get; set; }
		public int ProcessInstanceId { get; set; }
		public string DocumentReasonGUID { get; set; }
		public string Remark { get; set; }
		public string OperReportSignatureGUID { get; set; }
		public string DocumentReason_Values { get; set; }
	}
}
