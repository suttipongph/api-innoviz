using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ProductSettledTransListView : ViewCompanyBaseEntity
	{
		public string ProductSettledTransGUID { get; set; }
		public string InvoiceSettlementDetailGUID { get; set; }
		public string DocumentId { get; set; }
		public int ProductType { get; set; }
		public string InterestDate { get; set; }
		public string SettledDate { get; set; }
		public int InterestDay { get; set; }
		public decimal SettledPurchaseAmount { get; set; }
		public decimal SettledReserveAmount { get; set; }
		public decimal SettledInvoiceAmount { get; set; }
		public decimal InterestCalcAmount { get; set; }
		public string InvoiceSettlementDetail_Values { get; set; }
		public int RefType { get; set; }
		public string OriginalRefGUID { get; set; }
		public string WithdrawalTable_WithdrawalTableGUID { get; set; }
	}
}
