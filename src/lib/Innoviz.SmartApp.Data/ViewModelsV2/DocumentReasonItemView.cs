using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class DocumentReasonItemView : ViewCompanyBaseEntity
	{
		public string DocumentReasonGUID { get; set; }
		public string Description { get; set; }
		public string ReasonId { get; set; }
		public int RefType { get; set; }
	}
}
