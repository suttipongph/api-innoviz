using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class AssignmentMethodItemView : ViewCompanyBaseEntity
	{
		public string AssignmentMethodGUID { get; set; }
		public string AssignmentMethodId { get; set; }
		public string Description { get; set; }
		public bool ValidateAssignmentBalance { get; set; }
	}
}
