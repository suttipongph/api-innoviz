using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class NCBTransItemView : ViewCompanyBaseEntity
	{
		public string NCBTransGUID { get; set; }
		public string BankGroupGUID { get; set; }
		public decimal CreditLimit { get; set; }
		public string CreditTypeGUID { get; set; }
		public string EndDate { get; set; }
		public decimal MonthlyRepayment { get; set; }
		public string NCBAccountStatusGUID { get; set; }
		public decimal OutstandingAR { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public string RefId { get; set; }
	}
}
