﻿using System;
using System.Collections.Generic;
using System.Text;
using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class UpdateExpectedSigningDateView : ResultBaseEntity
    {
        public string CreditAppTableGUID { get; set; }
        public string CreditAppId { get; set; }
        public string CustomerTable_Values { get; set; }
        public string DocumentReasonGUID { get; set; }
        public string ExpectedAgmSigningDate { get; set; }
        public string ReasonRemark { get; set; }
    }
}
