using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class LedgerFiscalPeriodListView : ViewCompanyBaseEntity
	{
		public string LedgerFiscalPeriodGUID { get; set; }
		public string EndDate { get; set; }
		public string LedgerFiscalYearGUID { get; set; }
		public int PeriodStatus { get; set; }
		public string StartDate { get; set; }
	}
}
