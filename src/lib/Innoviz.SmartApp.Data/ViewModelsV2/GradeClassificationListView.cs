using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class GradeClassificationListView : ViewCompanyBaseEntity
	{
		public string GradeClassificationGUID { get; set; }
		public string GradeClassificationId { get; set; }
		public string Description { get; set; }
	}
}
