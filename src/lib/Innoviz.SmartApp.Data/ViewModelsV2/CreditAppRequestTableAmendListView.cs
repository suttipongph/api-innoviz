using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CreditAppRequestTableAmendListView : ViewCompanyBaseEntity
	{
		public string CreditAppRequestTableAmendGUID { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string DocumentStatus_Description { get; set; }
		public string CreditAppRequestTable_RequestDate { get; set; }
		public string CreditAppRequestTable_CreditAppRequestId { get; set; }
		public string CreditAppRequestTable_Description { get; set; }
		public int CreditAppRequestTable_CreditAppRequestType { get; set; }
		public string CreditAppRequestTable_RefCreditAppTableGUID { get; set; }

	}
}
