using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ParentCompanyListView : ViewCompanyBaseEntity
	{
		public string ParentCompanyGUID { get; set; }
		public string ParentCompanyId { get; set; }
		public string Description { get; set; }
	}
}
