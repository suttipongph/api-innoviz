﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class CancelCreditAppRequestParamView
    {
        public string CreditAppRequestGUID { get; set; }
        public string DocumentReasonGUID { get; set; }
        public string ReasonRemark { get; set; }
        public string RefLabel { get; set; }
    }
    public class CancelCreditAppRequestResultView: ResultBaseEntity
    {
        public string CreditAppRequestGUID { get; set; }
        public string CreditAppRequestId { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string DocumentReasonGUID { get; set; }
        public string ReasonRemark { get; set; }
        public string Description { get; set; }
        public string DocumentStatus_Description { get; set; }
    }
}
