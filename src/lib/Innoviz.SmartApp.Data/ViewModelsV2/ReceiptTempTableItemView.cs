using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ReceiptTempTableItemView : ViewCompanyBaseEntity
	{
		public string ReceiptTempTableGUID { get; set; }
		public string BuyerReceiptTableGUID { get; set; }
		public string BuyerTableGUID { get; set; }
		public string CreditAppTableGUID { get; set; }
		public string CurrencyGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public string Dimension1GUID { get; set; }
		public string Dimension2GUID { get; set; }
		public string Dimension3GUID { get; set; }
		public string Dimension4GUID { get; set; }
		public string Dimension5GUID { get; set; }
		public string DocumentReasonGUID { get; set; }
		public string DocumentStatusGUID { get; set; }
		public decimal ExchangeRate { get; set; }
		public string MainReceiptTempGUID { get; set; }
		public int ProductType { get; set; }
		public decimal ReceiptAmount { get; set; }
		public string ReceiptDate { get; set; }
		public string ReceiptTempId { get; set; }
		public int ReceiptTempRefType { get; set; }
		public int ReceivedFrom { get; set; }
		public string RefReceiptTempGUID { get; set; }
		public string Remark { get; set; }
		public decimal SettleAmount { get; set; }
		public decimal SettleFeeAmount { get; set; }
		public decimal SuspenseAmount { get; set; }
		public string SuspenseInvoiceTypeGUID { get; set; }
		public string TransDate { get; set; }
		public string DocumentReason_Values { get; set; }
		public string MainReceiptTemp_Values { get; set; }
		public string RefReceiptTemp_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public bool InvoiceType_ValidateDirectReceiveByCA { get; set; }
		public string DocumentStatus_StatusId { get; set; }

		public string CustomerTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string SuspenseInvoiceType_Values { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string Process_Id { get; set; }
	}
}
