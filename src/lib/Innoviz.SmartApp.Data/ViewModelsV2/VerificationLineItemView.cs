using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class VerificationLineItemView : ViewCompanyBaseEntity
	{
		public string VerificationLineGUID { get; set; }
		public string VerificationID { get; set; }
		public bool Pass { get; set; }
		public string Remark { get; set; }
		public string VendorTableGUID { get; set; }
		public string VerificationTableGUID { get; set; }
		public string VerificationTypeGUID { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string VerificationTable_Values { get; set; }
	}
}
