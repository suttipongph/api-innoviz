using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CollectionGroupListView : ViewCompanyBaseEntity
	{
		public string CollectionGroupGUID { get; set; }
	}
}
