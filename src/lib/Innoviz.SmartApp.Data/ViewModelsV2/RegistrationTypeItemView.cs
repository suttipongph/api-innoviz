using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class RegistrationTypeItemView : ViewCompanyBaseEntity
	{
		public string RegistrationTypeGUID { get; set; }
		public string Description { get; set; }
		public string RegistrationTypeId { get; set; }
	}
}
