using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class MessengerJobTableListView : ViewCompanyBaseEntity
	{
		public string MessengerJobTableGUID { get; set; }
		public string JobId { get; set; }
		public string BuyerTableGUID { get; set; }
		public string JobEndTime { get; set; }
		public string ContactName { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string JobDate { get; set; }
		public string JobStartTime { get; set; }
		public string JobTypeGUID { get; set; }
		public string MessengerTableGUID { get; set; }
		public int Priority { get; set; }
		public int Result { get; set; }
		public string RequestorGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public string CustomerTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string JobType_Values { get; set; }
		public string MessengerTable_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string EmployeeTable_Values { get; set; }
		public int RefType { get; set; }
		public string RefGUID { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string EmployeeTable_EmployeeId { get; set; }
		public string MessengerTable_MessengerId { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string JobType_JobTypeId { get; set; }

	}
}
