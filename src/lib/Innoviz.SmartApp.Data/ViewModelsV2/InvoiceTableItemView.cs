using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class InvoiceTableItemView : ViewBranchCompanyBaseEntity
	{
		public string InvoiceTableGUID { get; set; }
		public int AccountingPeriod { get; set; }
		public string BuyerAgreementTableGUID { get; set; }
		public string BuyerInvoiceTableGUID { get; set; }
		public string BuyerTableGUID { get; set; }
		public string CNReasonGUID { get; set; }
		public string CreditAppTableGUID { get; set; }
		public string CurrencyGUID { get; set; }
		public string CustomerName { get; set; }
		public string CustomerTableGUID { get; set; }
		public string Dimension1GUID { get; set; }
		public string Dimension2GUID { get; set; }
		public string Dimension3GUID { get; set; }
		public string Dimension4GUID { get; set; }
		public string Dimension5GUID { get; set; }
		public string DocumentId { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string DueDate { get; set; }
		public decimal ExchangeRate { get; set; }
		public string InvoiceAddress1 { get; set; }
		public string InvoiceAddress2 { get; set; }
		public decimal InvoiceAmount { get; set; }
		public decimal InvoiceAmountBeforeTax { get; set; }
		public decimal InvoiceAmountBeforeTaxMST { get; set; }
		public decimal InvoiceAmountMST { get; set; }
		public string InvoiceId { get; set; }
		public string InvoiceTypeGUID { get; set; }
		public string IssuedDate { get; set; }
		public string MailingInvoiceAddress1 { get; set; }
		public string MailingInvoiceAddress2 { get; set; }
		public int MarketingPeriod { get; set; }
		public string MethodOfPaymentGUID { get; set; }
		public decimal OrigInvoiceAmount { get; set; }
		public string OrigInvoiceId { get; set; }
		public decimal OrigTaxInvoiceAmount { get; set; }
		public string OrigTaxInvoiceId { get; set; }
		public bool ProductInvoice { get; set; }
		public int ProductType { get; set; }
		public string RefGUID { get; set; }
		public string RefInvoiceGUID { get; set; }
		public string RefTaxInvoiceGUID { get; set; }
		public int RefType { get; set; }
		public string Remark { get; set; }
		public int SuspenseInvoiceType { get; set; }
		public decimal TaxAmount { get; set; }
		public decimal TaxAmountMST { get; set; }
		public string TaxBranchId { get; set; }
		public string TaxBranchName { get; set; }
		public decimal WHTAmount { get; set; }
		public decimal WHTBaseAmount { get; set; }
		public string RefId { get; set; }
		public string ReceiptTempTableGUID { get; set; }

		#region UnboundField
		public string UnboundInvoiceAddress { get; set; }
		public string UnboundMailingAddress { get; set; }
		public int CustTransStatus { get; set; }
		public string CreditAppRequestTable_CreditAppRequestTableGUID { get; set; }
		public string ReferenceID { get; set; }
		#endregion
	}
}
