using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CalendarNonWorkingDateItemView : ViewCompanyBaseEntity
	{
		public string CalendarNonWorkingDateGUID { get; set; }
		public string CalendarDate { get; set; }
		public string CalendarGroupGUID { get; set; }
		public string Description { get; set; }
		public int HolidayType { get; set; }
	}
}
