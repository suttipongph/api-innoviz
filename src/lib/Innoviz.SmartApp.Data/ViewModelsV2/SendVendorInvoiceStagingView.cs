﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class SendVendorInvoiceStagingParamView
    {
        public string DocumentId { get; set; }
        public int Number { get; set; }
        public int RefType { get; set; }
        public string RefGUID { get; set; }
        public string CallerTableLabel { get; set; }
    }
    public class SendVendorInvoiceStagingResultView: ResultBaseEntity
    {

    }
}
namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class SendVendorInvoiceStagingResultViewMap
    {

    }
}
