﻿using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class GenIntercompanySettlementStagingView
    {
        public StagingTableIntercoInvSettle StagingTableIntercoInvSettle { get; set; }
        public string IntercompanyInvoiceSettlementId { get; set; }
    }
}
