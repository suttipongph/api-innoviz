using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BusinessCollateralAgmLineListView : ViewCompanyBaseEntity
	{
		public string BusinessCollateralAgmLineGUID { get; set; }
		public string BusinessCollateralAgmTableGUID { get; set; }
		public int LineNum { get; set; }
		public string BusinessCollateralTypeGUID { get; set; }
		public string BusinessCollateralSubTypeGUID { get; set; }
		public string Description { get; set; }
		public decimal BusinessCollateralValue { get; set; }
		public string BuyerTableGUID { get; set; }
		public string BusinessCollateralType_Values { get; set; }
		public string BusinessCollateralSubType_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string BusinessCollateralType_BusinessCollateralTypeId { get; set; }
		public string BusinessCollateralSubType_BusinessCollateralSubTypeId { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public bool Cancelled { get; set; }
	}
}
