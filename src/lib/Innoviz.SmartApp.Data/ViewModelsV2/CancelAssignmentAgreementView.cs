﻿using Innoviz.SmartApp.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class CancelAssignmentAgreementView
    {
        [BookmarkMapping("AssignmentCancelDate1")]
        public string AssignmentCancelDate1 { get; set; }
        [BookmarkMapping("AssignmentDate1")]
        public string AssignmentDate1 { get; set; }
        [BookmarkMapping("AssignmentDate2")]
        public string AssignmentDate2 { get; set; }
        [BookmarkMapping("AssignmentDate3")]
        public string AssignmentDate3 { get; set; }
        [BookmarkMapping("AssignmentManualNo1")]
        public string AssignmentManualNo1 { get; set; }
        [BookmarkMapping("AssignmentNo3")]
        public string AssignmentNo3 { get; set; }
        [BookmarkMapping("AssignmentTextCancel1")]
        public string AssignmentTextCancel1 { get; set; }
        [BookmarkMapping("ManualNo1")]
        public string ManualNo1 { get; set; }
        [BookmarkMapping("CustName1")]
        public string CustName1 { get; set; }
        [BookmarkMapping("Dear1")]
        public string Dear1 { get; set; }
        [BookmarkMapping("Dear2")]
        public string Dear2 { get; set; }
        [BookmarkMapping("TextDateAssignment1")]
        public string TextDateAssignment1 { get; set; }
        [BookmarkMapping("TextDetailinvoice")]
        public string TextDetailinvoice { get; set; }
        [BookmarkMapping("CompanyName1")]
        public string CompanyName1 { get; set; }
        [BookmarkMapping("CompanyName2")]
        public string CompanyName2 { get; set; }
        [BookmarkMapping("CustName2")]
        public string CustName2 { get; set; }
        [BookmarkMapping("TextDateAssignment2")]
        public string TextDateAssignment2 { get; set; }
        [BookmarkMapping("CompanyName3")]
        public string CompanyName3 { get; set; }
        [BookmarkMapping("CompanyName4")]
        public string CompanyName4 { get; set; }
        [BookmarkMapping("CustName3")]
        public string CustName3 { get; set; }
        [BookmarkMapping("CustName4")]
        public string CustName4 { get; set; }
        [BookmarkMapping("CustBankName1")]
        public string CustBankName1 { get; set; }
        [BookmarkMapping("CustBankBranch1")]
        public string CustBankBranch1 { get; set; }
        [BookmarkMapping("CustBankType1")]
        public string CustBankType1 { get; set; }
        [BookmarkMapping("MessengerAddress1")]
        public string MessengerAddress1 { get; set; }
        [BookmarkMapping("MessengerID1")]
        public string MessengerID1 { get; set; }
        [BookmarkMapping("MessengerName1")]
        public string MessengerName1 { get; set; }
        [BookmarkMapping("CustBankNo1")]
        public string CustBankNo1 { get; set; }
        [BookmarkMapping("PositionReceive1")]
        public string PositionReceive1 { get; set; }
        [BookmarkMapping("CompanyName5")]
        public string CompanyName5 { get; set; }
        [BookmarkMapping("Dear3")]
        public string Dear3 { get; set; }
        [BookmarkMapping("AuthorityPersonFirst1")]
        public string AuthorityPersonFirst1 { get; set; }
        [BookmarkMapping("AuthorityPersonSecond1")]
        public string AuthorityPersonSecond1 { get; set; }
        [BookmarkMapping("TextDetailBuyer1")]
        public string TextDetailBuyer1 { get; set; }
        [BookmarkMapping("PositionLIT1")]
        public string PositionLIT1 { get; set; }
        [BookmarkMapping("ReceivedName1", "ReceivedName2", "ReceivedName3")]
        public string ReceivedName { get; set; }
        [BookmarkMapping("CustName5")]
        public string CustName { get; set; }

        // variable
        public string AgreementCancelDate { get; set; }
        public string AgreementDate { get; set; }
    }
}
