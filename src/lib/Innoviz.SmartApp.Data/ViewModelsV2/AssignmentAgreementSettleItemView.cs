using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class AssignmentAgreementSettleItemView : ViewCompanyBaseEntity
	{
		public string AssignmentAgreementSettleGUID { get; set; }
		public string AssignmentAgreementTableGUID { get; set; }
		public string BuyerAgreementTableGUID { get; set; }
		public string DocumentReasonGUID { get; set; }
		public string RefAssignmentAgreementSettleGUID { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public string RefId { get; set; }
		public decimal SettledAmount { get; set; }
		public string SettledDate { get; set; }
		public string AssignmentAgreementTable_Value { get; set; }
		public string AssignmentAgreementTable_AssignmentAgreementId { get; set; }
		public string BuyerTable_Value { get; set; }
		public string DocumentReason_ReasonId { get; set; }
		public string RefAssignTable_Value { get; set; }
		public string DocumentId { get; set; }
	
		public string InvoiceTableGUID { get; set; }
		public string InvoiceTable_Values { get; set; }
	}
}
