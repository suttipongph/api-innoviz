using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class NumberSeqParameterListView : ViewCompanyBaseEntity
	{
		public string NumberSeqParameterGUID { get; set; }
		public string NumberSeqTableGUID { get; set; }
		public string ReferenceId { get; set; }
		public string NumberSeqTable_Values { get; set; }
	}
}
