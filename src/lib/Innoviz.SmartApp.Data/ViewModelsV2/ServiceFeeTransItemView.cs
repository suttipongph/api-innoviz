using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class ServiceFeeTransItemView : ViewCompanyBaseEntity
    {
        public string ServiceFeeTransGUID { get; set; }
        public decimal AmountBeforeTax { get; set; }
        public decimal AmountIncludeTax { get; set; }
        public string CNReasonGUID { get; set; }
        public string Description { get; set; }
        public string Dimension1GUID { get; set; }
        public string Dimension2GUID { get; set; }
        public string Dimension3GUID { get; set; }
        public string Dimension4GUID { get; set; }
        public string Dimension5GUID { get; set; }
        public decimal FeeAmount { get; set; }
        public bool IncludeTax { get; set; }
        public string InvoiceRevenueTypeGUID { get; set; }
        public int Ordering { get; set; }
        public decimal OrigInvoiceAmount { get; set; }
        public string OrigInvoiceId { get; set; }
        public decimal OrigTaxInvoiceAmount { get; set; }
        public string OrigTaxInvoiceId { get; set; }
        public string RefGUID { get; set; }
        public int RefType { get; set; }
        public decimal SettleAmount { get; set; }
        public decimal SettleInvoiceAmount { get; set; }
        public decimal SettleWHTAmount { get; set; }
        public decimal TaxAmount { get; set; }
        public string TaxTableGUID { get; set; }
        public decimal WHTAmount { get; set; }
        public string WithholdingTaxTableGUID { get; set; }
        public string RefId { get; set; }
        public int ProductType { get; set; }
        public string TaxDate { get; set; }
        public bool WHTSlipReceivedByCustomer { get; set; }
        public string InvoiceRevenueType_InterompanyTableGUID { get; set; }
        public decimal TaxValue { get; set; }
        public decimal WHTaxValue { get; set; }
    }
}
