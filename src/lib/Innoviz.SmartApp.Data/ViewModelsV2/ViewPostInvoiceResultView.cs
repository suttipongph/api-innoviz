﻿using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class ViewPostInvoiceResultView
    {
        public InvoiceTable InvoiceTable { get; set; }
        public List<InvoiceLine> InvoiceLines { get; set; } = new List<InvoiceLine>();
        public CustTrans CustTrans { get; set; }
        public CreditAppTrans CreditAppTrans { get; set; }
        public TaxInvoiceTable TaxInvoiceTable { get; set; }
        public List<TaxInvoiceLine> TaxInvoiceLines { get; set; } = new List<TaxInvoiceLine>();
        public ProcessTrans ProcessTrans { get; set; }
        public RetentionTrans RetentionTrans { get; set; }

    }
}
