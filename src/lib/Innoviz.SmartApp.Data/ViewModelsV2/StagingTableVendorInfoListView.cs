using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class StagingTableVendorInfoListView : ViewCompanyBaseEntity
	{
		public string StagingTableVendorInfoGUID { get; set; }
		public int RecordType { get; set; }
		public string VendorId { get; set; }
		public string Name { get; set; }
		public string CurrencyId { get; set; }
		public string VendGroupId { get; set; }
		public string BankAccountName { get; set; }
		public string BankAccount { get; set; }
		public string BankGroupId { get; set; }
		public string ProcessTransGUID { get; set; }
	}
}
