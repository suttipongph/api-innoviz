﻿using Innoviz.SmartApp.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class CBCANoticeOfCancellation
    {
        public DateTime CBCANoticeOfCancellation_AgreementDate { set; get; }
        public string CBCANoticeOfCancellation_DocumentReasonId { set; get; }
        public string CBCANoticeOfCancellation_DocumentReasonDesc { set; get; }
        public string CBCANoticeOfCancellation_Remark { set; get; }
        public string CBCANoticeOfCancellation_DocumentReasonGUID { set; get; }
        public string CBCANoticeOfCancellation_CustomerAddress { set; get; }
        public string CBCANoticeOfCancellation_CustomerName { set; get; }
        public string CBCANoticeOfCancellation_DBDRegistrationId { set; get; }
        public DateTime? CBCANoticeOfCancellation_DBDRegistrationDate { set; get; }
    }
    public class CBCAQBookmarkDocumentTrans
    {
        public string CBCAQBookmarkDocumentTrans_ReferenceExternalId { set; get; }
        public string CBCAQBookmarkDocumentTrans_ReferenceExternalDate { set; get; }
    }
    public class CBCAAgreementTableInfo
    {
        public string CBCAAgreementTableInfo_AuthorizedPersonCompany1 { set; get; }
        public string CBCAAgreementTableInfo_AuthorizedPersonCompany2 { set; get; }
        public string CBCAAgreementTableInfo_AuthorizedPersonCompany3 { set; get; }
        public string CBCAAgreementTableInfo_AuthorizedPersonTransCustomer1GUID { set; get; }
        public string CBCAAgreementTableInfo_AuthorizedPersonTransCustomer2GUID { set; get; }
        public string CBCAAgreementTableInfo_AuthorizedPersonTransCustomer3GUID { set; get; }
        public string CBCAAgreementTableInfo_WitnessCompany1 { set; get; }
        public string CBCAAgreementTableInfo_WitnessCompany2 { set; get; }
        public string CBCAAgreementTableInfo_CompanyName { set; get; }
        public string CBCAAgreementTableInfo_CompanyAddress { set; get; }

    }
    public class CBCAQBusinessCollateralAgm
    {
        public string CBCAQBusinessCollateralAgm_DBDRegistrationId { set; get; }
        public string CBCAQBusinessCollateralAgm_DBDRegistrationDate { set; get; }
    }

    public class CBCAQBusinessCollateralAgmLine
    {
        public int CBCAQBusinessCollateralAgmLine_LineNum { set; get; }
        public Guid CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeGUID { set; get; }
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc { set; get; }
        public Guid CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeGUID { set; get; }
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc { set; get; }
        public string CBCAQBusinessCollateralAgmLine_Description { set; get; }
        public decimal CBCAQBusinessCollateralAgmLine_BusinessCollateralValue { set; get; }
    }

    public class BookmarkCancelBusinessCollateralAgreement
    {
        [BookmarkMapping("BCCancelDate1", "BCCancelDate2")]
        public string CBCANoticeOfCancellation_AgreementDate { set; get; }
        public string CBCANoticeOfCancellation_DocumentReasonGUID { set; get; }
        public string CBCANoticeOfCancellation_DocumentReasonId { set; get; }
        [BookmarkMapping("AuthorizedCompanyFirst1")]
        public string CBCAAgreementTableInfo_AuthorizedPersonCompany1 { set; get; }
        [BookmarkMapping("AuthorizedCompanySecond1")]
        public string CBCAAgreementTableInfo_AuthorizedPersonCompany2 { set; get; }
        [BookmarkMapping("AuthorizedCompanyThird1")]
        public string CBCAAgreementTableInfo_AuthorizedPersonCompany3 { set; get; }
        public string CBCAAgreementTableInfo_AuthorizedPersonTransCustomer1GUID { set; get; }
        public string CBCAAgreementTableInfo_AuthorizedPersonTransCustomer2GUID { set; get; }
        public string CBCAAgreementTableInfo_AuthorizedPersonTransCustomer3GUID { set; get; }
        [BookmarkMapping("WitnessFirst1")]
        public string CBCAAgreementTableInfo_WitnessCompany1 { set; get; }
        [BookmarkMapping("WitnessSecond1")]
        public string CBCAAgreementTableInfo_WitnessCompany2 { set; get; }
        [BookmarkMapping("CustAddress1")]

        public string CBCANoticeOfCancellation_CustomerAddress { set; get; }
        [BookmarkMapping("CustName1", "CustName2", "CustName3")]
        public string CBCANoticeOfCancellation_CustomerName { set; get; }
        [BookmarkMapping("BCCancelDBBNo1")]
        public string CBCANoticeOfCancellation_DBDRegistrationId { set; get; }
        [BookmarkMapping("BCCancelDBDDate1")]
        public string CBCANoticeOfCancellation_DBDRegistrationDate { set; get; }

        [BookmarkMapping("BCCancelReasonFirst")]
        public string BCCancelReasonFirst { set; get; }
        [BookmarkMapping("BCCancelReasonFSecond")]
        public string BCCancelReasonFSecond { set; get; }
        [BookmarkMapping("BCCancelReasonThird")]

        public string BCCancelReasonThird { set; get; }
        [BookmarkMapping("BCCancelReasonForth")]
        public string BCCancelReasonForth { set; get; }
        [BookmarkMapping("BCCancelReasonFifth")]
        public string BCCancelReasonFifth { set; get; }
        [BookmarkMapping("BCCancelTextNo1", "BCCancelTextNo2")]
        public string ReferenceExternalId { set; get; }
        [BookmarkMapping("AuthorizedCustFirst1")]
        public string AuthorizedPersonCustomer1_Name { set; get; }
        [BookmarkMapping("AuthorizedCustSecond1")]
        public string AuthorizedPersonCustomer2_Name { set; get; }
        [BookmarkMapping("AuthorizedCustThird1")]
        public string AuthorizedPersonCustomer3_Name { set; get; }


        [BookmarkMapping("ExternalDate1")]
        public string CBCAQBookmarkDocumentTrans_ReferenceExternalDate { set; get; }

        [BookmarkMapping("ReasonCancel")]
        public string CBCANoticeOfCancellation_DocumentReasonDesc { set; get; }
        [BookmarkMapping("RemarkCancel")]
        public string CBCANoticeOfCancellation_Remark { set; get; }



        #region line
        [BookmarkMapping("Line1")]
        public int CBCAQBusinessCollateralAgmLine_LineNum_1 { set; get; }
        [BookmarkMapping("Type1")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_1 { set; get; }
        [BookmarkMapping("SubType1")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_1 { set; get; }
        [BookmarkMapping("Description1")]
        public string CBCAQBusinessCollateralAgmLine_Description_1 { set; get; }
        [BookmarkMapping("Businesscolleteral1")]
        public decimal CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_1 { set; get; }


        [BookmarkMapping("Line2")]
        public int CBCAQBusinessCollateralAgmLine_LineNum_2 { set; get; }
        [BookmarkMapping("Type2")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_2 { set; get; }
        [BookmarkMapping("SubType2")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_2 { set; get; }
        [BookmarkMapping("Description2")]
        public string CBCAQBusinessCollateralAgmLine_Description_2 { set; get; }
        [BookmarkMapping("Businesscolleteral2")]
        public decimal CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_2 { set; get; }

        [BookmarkMapping("Line3Line3")]
        public int CBCAQBusinessCollateralAgmLine_LineNum_3 { set; get; }
        [BookmarkMapping("Type3")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_3 { set; get; }
        [BookmarkMapping("SubType3")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_3 { set; get; }
        [BookmarkMapping("Description3")]
        public string CBCAQBusinessCollateralAgmLine_Description_3 { set; get; }
        [BookmarkMapping("Businesscolleteral3")]
        public decimal CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_3 { set; get; }


        [BookmarkMapping("Line4")]
        public int CBCAQBusinessCollateralAgmLine_LineNum_4 { set; get; }
        [BookmarkMapping("Type4")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_4 { set; get; }
        [BookmarkMapping("SubType4")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_4 { set; get; }
        [BookmarkMapping("Description4")]
        public string CBCAQBusinessCollateralAgmLine_Description_4 { set; get; }
        [BookmarkMapping("Businesscolleteral4")]
        public decimal CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_4 { set; get; }


        [BookmarkMapping("Line5")]
        public int CBCAQBusinessCollateralAgmLine_LineNum_5 { set; get; }
        [BookmarkMapping("Type5")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_5 { set; get; }
        [BookmarkMapping("SubType5")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_5 { set; get; }
        [BookmarkMapping("Description5")]
        public string CBCAQBusinessCollateralAgmLine_Description_5 { set; get; }
        [BookmarkMapping("Businesscolleteral5")]
        public decimal CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_5 { set; get; }


        [BookmarkMapping("Line6")]
        public int CBCAQBusinessCollateralAgmLine_LineNum_6 { set; get; }
        [BookmarkMapping("Type6")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_6 { set; get; }
        [BookmarkMapping("SubType6")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_6 { set; get; }
        [BookmarkMapping("Description6")]
        public string CBCAQBusinessCollateralAgmLine_Description_6 { set; get; }
        [BookmarkMapping("Businesscolleteral6")]
        public decimal CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_6 { set; get; }


        [BookmarkMapping("Line7")]
        public int CBCAQBusinessCollateralAgmLine_LineNum_7 { set; get; }
        [BookmarkMapping("Type7")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_7 { set; get; }
        [BookmarkMapping("SubType7")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_7 { set; get; }
        [BookmarkMapping("Description7")]
        public string CBCAQBusinessCollateralAgmLine_Description_7 { set; get; }
        [BookmarkMapping("Businesscolleteral7")]
        public decimal CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_7 { set; get; }

        [BookmarkMapping("Line8")]
        public int CBCAQBusinessCollateralAgmLine_LineNum_8 { set; get; }
        [BookmarkMapping("Type8")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_8 { set; get; }
        [BookmarkMapping("SubType8")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_8 { set; get; }
        [BookmarkMapping("Description8")]
        public string CBCAQBusinessCollateralAgmLine_Description_8 { set; get; }
        [BookmarkMapping("Businesscolleteral8")]
        public decimal CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_8 { set; get; }


        [BookmarkMapping("Line9")]
        public int CBCAQBusinessCollateralAgmLine_LineNum_9 { set; get; }
        [BookmarkMapping("Type9")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_9 { set; get; }
        [BookmarkMapping("SubType9")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_9 { set; get; }
        [BookmarkMapping("Description9")]
        public string CBCAQBusinessCollateralAgmLine_Description_9 { set; get; }
        [BookmarkMapping("Businesscolleteral9")]
        public decimal CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_9 { set; get; }


        [BookmarkMapping("Line10")]
        public int CBCAQBusinessCollateralAgmLine_LineNum_10 { set; get; }
        [BookmarkMapping("Type10")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_10 { set; get; }
        [BookmarkMapping("SubType10")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_10 { set; get; }
        [BookmarkMapping("Description10")]
        public string CBCAQBusinessCollateralAgmLine_Description_10 { set; get; }
        [BookmarkMapping("Businesscolleteral10")]
        public decimal CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_10 { set; get; }


        [BookmarkMapping("Line11")]
        public int CBCAQBusinessCollateralAgmLine_LineNum_11 { set; get; }
        [BookmarkMapping("Type11")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_11 { set; get; }
        [BookmarkMapping("SubType11")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_11 { set; get; }
        [BookmarkMapping("Description11")]
        public string CBCAQBusinessCollateralAgmLine_Description_11 { set; get; }
        [BookmarkMapping("Businesscolleteral11")]
        public decimal CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_11 { set; get; }


        [BookmarkMapping("Line12")]
        public int CBCAQBusinessCollateralAgmLine_LineNum_12 { set; get; }
        [BookmarkMapping("Type12")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_12 { set; get; }
        [BookmarkMapping("SubType12")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_12 { set; get; }
        [BookmarkMapping("Description12")]
        public string CBCAQBusinessCollateralAgmLine_Description_12 { set; get; }
        [BookmarkMapping("Businesscolleteral12")]
        public decimal CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_12 { set; get; }


        [BookmarkMapping("Line13")]
        public int CBCAQBusinessCollateralAgmLine_LineNum_13 { set; get; }
        [BookmarkMapping("Type13")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_13 { set; get; }
        [BookmarkMapping("SubType13")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_13 { set; get; }
        [BookmarkMapping("Description13")]
        public string CBCAQBusinessCollateralAgmLine_Description_13 { set; get; }
        [BookmarkMapping("Businesscolleteral13")]
        public decimal CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_13 { set; get; }


        [BookmarkMapping("Line14")]
        public int CBCAQBusinessCollateralAgmLine_LineNum_14 { set; get; }
        [BookmarkMapping("Type14")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_14 { set; get; }
        [BookmarkMapping("SubType14")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_14 { set; get; }
        [BookmarkMapping("Description14")]
        public string CBCAQBusinessCollateralAgmLine_Description_14 { set; get; }
        [BookmarkMapping("Businesscolleteral14")]
        public decimal CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_14 { set; get; }


        [BookmarkMapping("Line15")]
        public int CBCAQBusinessCollateralAgmLine_LineNum_15 { set; get; }
        [BookmarkMapping("Type15")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_15 { set; get; }
        [BookmarkMapping("SubType15")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_15 { set; get; }
        [BookmarkMapping("Description15")]
        public string CBCAQBusinessCollateralAgmLine_Description_15 { set; get; }
        [BookmarkMapping("Businesscolleteral15")]
        public decimal CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_15 { set; get; }


        [BookmarkMapping("Line16")]
        public int CBCAQBusinessCollateralAgmLine_LineNum_16 { set; get; }
        [BookmarkMapping("Type16")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_16 { set; get; }
        [BookmarkMapping("SubType16")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_16 { set; get; }
        [BookmarkMapping("Description16")]
        public string CBCAQBusinessCollateralAgmLine_Description_16 { set; get; }
        [BookmarkMapping("Businesscolleteral16")]
        public decimal CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_16 { set; get; }


        [BookmarkMapping("Line17")]
        public int CBCAQBusinessCollateralAgmLine_LineNum_17 { set; get; }
        [BookmarkMapping("Type17")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_17 { set; get; }
        [BookmarkMapping("SubType17")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_17 { set; get; }
        [BookmarkMapping("Description17")]
        public string CBCAQBusinessCollateralAgmLine_Description_17 { set; get; }
        [BookmarkMapping("Businesscolleteral17")]
        public decimal CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_17 { set; get; }


        [BookmarkMapping("Line18")]
        public int CBCAQBusinessCollateralAgmLine_LineNum_18 { set; get; }
        [BookmarkMapping("Type18")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_18 { set; get; }
        [BookmarkMapping("SubType18")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_18 { set; get; }
        [BookmarkMapping("Description18")]
        public string CBCAQBusinessCollateralAgmLine_Description_18 { set; get; }
        [BookmarkMapping("Businesscolleteral18")]
        public decimal CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_18 { set; get; }


        [BookmarkMapping("Line19")]
        public int CBCAQBusinessCollateralAgmLine_LineNum_19 { set; get; }
        [BookmarkMapping("Type19")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_19 { set; get; }
        [BookmarkMapping("SubType19")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_19 { set; get; }
        [BookmarkMapping("Description19")]
        public string CBCAQBusinessCollateralAgmLine_Description_19 { set; get; }
        [BookmarkMapping("Businesscolleteral19")]
        public decimal CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_19 { set; get; }


        [BookmarkMapping("Line20")]
        public int CBCAQBusinessCollateralAgmLine_LineNum_20 { set; get; }
        [BookmarkMapping("Type20")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralTypeDesc_20 { set; get; }
        [BookmarkMapping("SubType20")]
        public string CBCAQBusinessCollateralAgmLine_BusinessCollateralSubTypeDesc_20 { set; get; }
        [BookmarkMapping("Description20")]
        public string CBCAQBusinessCollateralAgmLine_Description_20 { set; get; }
        [BookmarkMapping("Businesscolleteral20")]
        public decimal CBCAQBusinessCollateralAgmLine_BusinessCollateralValue_20 { set; get; }
        #endregion line

    }
}
