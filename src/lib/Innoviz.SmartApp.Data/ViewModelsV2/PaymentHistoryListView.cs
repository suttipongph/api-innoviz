using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class PaymentHistoryListView : ViewBranchCompanyBaseEntity
	{
		public string PaymentHistoryGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public decimal PaymentAmount { get; set; }
		public decimal WHTAmount { get; set; }
		public string CreditAppTableGUID { get; set; }
		public int ProductType { get; set; }
		public string InvoiceTableGUID { get; set; }
		public string PaymentDate { get; set; }
		public string ReceivedDate { get; set; }
		public int ReceivedFrom { get; set; }
		public decimal PaymentBaseAmount { get; set; }
		public decimal PaymentTaxAmount { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string InvoiceTable_Values { get; set; }
		public bool WHTSlipReceivedByCustomer { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string CreditAppTable_CreditAppTableId { get; set; }
		public string InvoiceTable_InvoiceTableId { get; set; }
	}
}
