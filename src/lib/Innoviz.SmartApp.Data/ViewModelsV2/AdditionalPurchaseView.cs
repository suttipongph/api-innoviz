﻿using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class AdditionalPurchaseParamView
    {
        public string Description { get; set; }
        public string DueDate { get; set; }
        public decimal LinePurchaseAmount { get; set; }
        public decimal LinePurchasePct { get; set; }
        public decimal PurchaseAmount { get; set; }
        public string PurchaseDate { get; set; }
        public decimal PurchasePct { get; set; }
        public string RefDueDate { get; set; }
        public decimal RefPurchaseAmount { get; set; }
        public string RefPurchaseDate { get; set; }
        public string RefPurchaseId { get; set; }
        public decimal RefPurchasePct { get; set; }
        public string PurchaseLineGUID { get; set; }
        public decimal RefBuyerInvoiceAmount { get; set; }
    }
    public class AdditionalPurchaseResultView : ResultBaseEntity
    {
    }
}
namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class AdditionalPurchaseResultViewMap
    {
        public List<PurchaseLine> PurchaseLine { get; set; }
        public PurchaseTable PurchaseTable { get; set; }
    }
}
