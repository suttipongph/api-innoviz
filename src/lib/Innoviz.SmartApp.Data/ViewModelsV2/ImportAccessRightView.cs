﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class ImportAccessRightView
    {
        public FileInformation FileInfo { get; set; }
        public string RoleGUID { get; set; }
        public string PathMenu { get; set; }
        public string GroupId { get; set; }
        public int Action { get; set; }
        public int Read { get; set; }
        public int Update { get; set; }
        public int Create { get; set; }
        public int Delete { get; set; }
        public static IReadOnlyDictionary<string, string> GetMapper()
        {
            // ("ExcelColumnName", "ModelColumnName")
            IDictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("pathMenu", "PathMenu");
            dict.Add("action", "Action");
            dict.Add("read", "Read");
            dict.Add("update", "Update");
            dict.Add("create", "Create");
            dict.Add("delete", "Delete");
            dict.Add("reference", "GroupId");
            return new ReadOnlyDictionary<string, string>(dict);
        }
    }
    public class ImportAccessRightResultView: ResultBaseEntity { }
}
