using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CustBusinessCollateralListView : ViewCompanyBaseEntity
	{
		public string CustBusinessCollateralGUID { get; set; }
		public string CustBusinessCollateralId { get; set; }
		public string Description { get; set; }
		public string DBDRegistrationDate { get; set; }
		public string BusinessCollateralTypeGUID { get; set; }
		public string BusinessCollateralSubTypeGUID { get; set; }
		public string BuyerName { get; set; }
		public bool Cancelled { get; set; }
		public decimal BusinessCollateralValue { get; set; }
		public string CreditAppRequestTableGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public string CreditAppRequestTable_Values { get; set; }
		public string BusinessCollateralType_Values { get; set; }
		public string BusinessCollateralSubType_Values { get; set; }
		public string CreditAppRequestTable_CreditAppRequestId { get; set; }
		public string BusinessCollateralType_BusinessCollateralTypeId { get; set; }
		public string BusinessCollateralSubType_BusinessCollateralSubTypeId { get; set; }

	}
}
