﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class CalcSettleInvoiceAmountResultView
    {
        public decimal BalanceAmount { get; set; }
        public decimal WHTBalance { get; set; }
        public decimal SettleTaxBalance { get; set; }
        public decimal MaxSettleAmount { get; set; }
        public decimal WHTAmount { get; set; }
        public decimal SettleInvoiceAmount { get; set; }
        public decimal SettleTaxAmount { get; set; }
        public decimal SettleAmount { get; set; }

    }
}
