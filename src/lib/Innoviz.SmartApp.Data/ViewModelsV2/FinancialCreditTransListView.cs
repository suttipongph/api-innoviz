using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class FinancialCreditTransListView : ViewCompanyBaseEntity
	{
		public string FinancialCreditTransGUID { get; set; }
		public string BankGroupGUID { get; set; }
		public string CreditTypeGUID { get; set; }
		public decimal Amount { get; set; }
		public string BankGroup_Values { get; set; }
		public string CreditType_Values { get; set; }
		public string BankGroup_BankGroupId { get; set; }
		public string CreditType_CreditTypeId { get; set; }
		public string RefGUID { get; set; }
	}
}
