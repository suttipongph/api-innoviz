using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class AgreementTableInfoTextListView : ViewCompanyBaseEntity
	{
		public string AgreementTableInfoTextGUID { get; set; }
	}
}
