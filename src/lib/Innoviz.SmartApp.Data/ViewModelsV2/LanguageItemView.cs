using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class LanguageItemView : ViewCompanyBaseEntity
	{
		public string LanguageGUID { get; set; }
		public string LanguageId { get; set; }
		public string Name { get; set; }
	}
}
