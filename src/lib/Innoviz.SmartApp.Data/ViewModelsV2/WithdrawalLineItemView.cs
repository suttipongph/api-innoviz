using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class WithdrawalLineItemView : ViewCompanyBaseEntity
	{
		public string WithdrawalLineGUID { get; set; }
		public string BillingDate { get; set; }
		public string BuyerPDCTableGUID { get; set; }
		public bool ClosedForTermExtension { get; set; }
		public string CollectionDate { get; set; }
		public string CustomerPDCTableGUID { get; set; }
		public string Dimension1GUID { get; set; }
		public string Dimension2GUID { get; set; }
		public string Dimension3GUID { get; set; }
		public string Dimension4GUID { get; set; }
		public string Dimension5GUID { get; set; }
		public string DueDate { get; set; }
		public decimal InterestAmount { get; set; }
		public string InterestDate { get; set; }
		public int InterestDay { get; set; }
		public string StartDate { get; set; }
		public string WithdrawalLineInvoiceTableGUID { get; set; }
		public int WithdrawalLineType { get; set; }
		public string WithdrawalTableGUID { get; set; }
		public string WithdrawalTable_CustomerTableGUID { get; set; }
		public string WithdrawalTable_BuyerTableGUID { get; set; }
		public string WithdrawalTable_WithdrawalDate { get; set; }
		public string WithdrawalTable_Values { get; set; }
		public string InvoiceTable_Values { get; set; }
		public int LineNum { get; set; }
		public string CustomerPDCDate { get; set; }
		public decimal CustomerPDCAmount { get; set; }
		public string WithdrawalTable_StatusId { get; set; }
	}
}
