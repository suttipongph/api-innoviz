using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BusinessUnitItemView : ViewCompanyBaseEntity
	{
		public string BusinessUnitGUID { get; set; }
		public string BusinessUnitId { get; set; }
		public string Description { get; set; }
		public string ParentBusinessUnitGUID { get; set; }
	}
}
