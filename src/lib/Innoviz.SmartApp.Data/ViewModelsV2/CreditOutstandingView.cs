﻿using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class CreditOutstandingView : ViewCompanyBaseEntity
    {
        public string CreditAppTableGUID { get; set; }
        public int ProductType { get; set; }
        public string CreditLimitTypeGUID { get; set; }
        public decimal ApprovedCreditLimit { get; set; }
        public string CustomerTableGUID { get; set; }
        public string BuyerTableGUID { get; set; }
        public string AsOfDate { get; set; }
        public string ExpiryDate { get; set; }
        public decimal CreditLimitBalance { get; set; }
        public decimal ARBalance { get; set; }
        public decimal ReserveToBeRefund { get; set; }
        public decimal AccumRetentionAmount { get; set; }
        public string ParentCreditLimitType { get; set; }
        public string CreditAppTable_Values { get; set; }
        public string CreditAppTable_CreditAppId { get; set; }
        public string CreditLimitType_Values { get; set; }
        public string CreditLimitType_CreditLimitTypeId { get; set; }
        public string CustomerTable_Values { get; set; }
        public string CustomerTable_CustomerId { get; set; }
        public int RefType { get; set; }
    }
}
