﻿using System;
using System.Collections.Generic;
using System.Text;
using Innoviz.SmartApp.Core.Attributes;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class CrossGuarantorAgreementBookmarkView
    {
        [BookmarkMapping("GuarantorAgreementNo1")]
        public string GuarantorAgreementNo1 { get; set; }
        [BookmarkMapping("GuarantorAgreementNo2")]
        public string GuarantorAgreementNo2 { get; set; }
        [BookmarkMapping("GuarantorAgreementNo3")]
        public string GuarantorAgreementNo3 { get; set; }
        [BookmarkMapping("GuarantorAgreementNo4")]
        public string GuarantorAgreementNo4 { get; set; }
        [BookmarkMapping("GuarantorAgreementNo5")]
        public string GuarantorAgreementNo5 { get; set; }
        [BookmarkMapping("GuarantorAgreementNo6")]
        public string GuarantorAgreementNo6 { get; set; }
        [BookmarkMapping("GuarantorAgreementNo7")]
        public string GuarantorAgreementNo7 { get; set; }
        [BookmarkMapping("GuarantorAgreementNo8")]
        public string GuarantorAgreementNo8 { get; set; }
        [BookmarkMapping("GuarantorAgreementDate1")]
        public string GuarantorAgreementDate1 { get; set; }
        [BookmarkMapping("GuarantorAgreementDate2")]
        public string GuarantorAgreementDate2 { get; set; }
        [BookmarkMapping("GuarantorAgreementDate3")]
        public string GuarantorAgreementDate3 { get; set; }
        [BookmarkMapping("GuarantorAgreementDate4")]
        public string GuarantorAgreementDate4 { get; set; }
        [BookmarkMapping("GuarantorAgreementDate5")]
        public string GuarantorAgreementDate5 { get; set; }
        [BookmarkMapping("GuarantorAgreementDate6")]
        public string GuarantorAgreementDate6 { get; set; }
        [BookmarkMapping("GuarantorAgreementDate7")]
        public string GuarantorAgreementDate7 { get; set; }
        [BookmarkMapping("GuarantorAgreementDate8")]
        public string GuarantorAgreementDate8 { get; set; }

        public DateTime? qGuarantorAgreementDate1 { get; set; }
        public DateTime? qGuarantorAgreementDate2 { get; set; }
        public DateTime? qGuarantorAgreementDate3 { get; set; }
        public DateTime? qGuarantorAgreementDate4 { get; set; }
        public DateTime? qGuarantorAgreementDate5 { get; set; }
        public DateTime? qGuarantorAgreementDate6 { get; set; }
        public DateTime? qGuarantorAgreementDate7 { get; set; }
        public DateTime? qGuarantorAgreementDate8 { get; set; }


        [BookmarkMapping("GuarantorName1st_1")]
        public string GuarantorName1st_1 { get; set; }
        [BookmarkMapping("GuarantorName1st_2")]
        public string GuarantorName1st_2 { get; set; }
        [BookmarkMapping("GuarantorName1st_3")]
        public string GuarantorName1st_3 { get; set; }
        [BookmarkMapping("GuarantorAge1st_1")]
        public string GuarantorAge1st_1 { get; set; }
        [BookmarkMapping("GuarantorRace1st_1")]
        public string GuarantorRace1st_1 { get; set; }
        [BookmarkMapping("GuarantorNationality1st_1")]
        public string GuarantorNationality1st_1 { get; set; }
        [BookmarkMapping("GuarantorAddress1st_1")]
        public string GuarantorAddress1st_1 { get; set; }
        [BookmarkMapping("GuarantorTaxID1st_1")]
        public string GuarantorTaxID1st_1 { get; set; }
        public DateTime? qGuarantorDateregis1st_1 { get; set; }
        [BookmarkMapping("GuarantorDateregis1st_1")]
        public string GuarantorDateregis1st_1 { get; set; }
        [BookmarkMapping("RelatedName1st_Of1st_1")]
        public string RelatedName1st_Of1st_1 { get; set; }
        [BookmarkMapping("RelatedAddress1st_Of1st_1")]
        public string RelatedAddress1st_Of1st_1 { get; set; }
        [BookmarkMapping("RelatedContractNo1st_Of1st_1")]
        public string RelatedContractNo1st_Of1st_1 { get; set; }
        [BookmarkMapping("RelatedContractDate1st_Of1st_1")]
        public string RelatedContractDate1st_Of1st_1 { get; set; }
        public DateTime? qRelatedContractDate1st_Of1st_1 { get; set; }

        [BookmarkMapping("RelatedName2nd_Of1st_1")]
        public string RelatedName2nd_Of1st_1 { get; set; }
        [BookmarkMapping("RelatedAddress2nd_Of1st_1")]
        public string RelatedAddress2nd_Of1st_1 { get; set; }
        [BookmarkMapping("RelatedContractNo2nd_Of1st_1")]
        public string RelatedContractNo2nd_Of1st_1 { get; set; }
        [BookmarkMapping("RelatedContractDate2nd_Of1st_1")]
        public string RelatedContractDate2nd_Of1st_1 { get; set; }
        public DateTime? qRelatedContractDate2nd_Of1st_1 { get; set; }

        [BookmarkMapping("RelatedName3rd_Of1st_1")]
        public string RelatedName3rd_Of1st_1 { get; set; }
        [BookmarkMapping("RelatedAddress3rd_Of1st_1")]
        public string RelatedAddress3rd_Of1st_1 { get; set; }
        [BookmarkMapping("RelatedContractNo3rd_Of1st_1")]
        public string RelatedContractNo3rd_Of1st_1 { get; set; }
        [BookmarkMapping("RelatedContractDate3rd_Of1st_1")]
        public string RelatedContractDate3rd_Of1st_1 { get; set; }
        public DateTime? qRelatedContractDate3rd_Of1st_1 { get; set; }

        [BookmarkMapping("RelatedName4th_Of1st_1")]
        public string RelatedName4th_Of1st_1 { get; set; }
        [BookmarkMapping("RelatedAddress4th_Of1st_1")]
        public string RelatedAddress4th_Of1st_1 { get; set; }
        [BookmarkMapping("RelatedContractNo4th_Of1st_1")]
        public string RelatedContractNo4th_Of1st_1 { get; set; }
        [BookmarkMapping("RelatedContractDate4th_Of1st_1")]
        public string RelatedContractDate4th_Of1st_1 { get; set; }
        public DateTime? qRelatedContractDate4th_Of1st_1 { get; set; }

        [BookmarkMapping("RelatedName5th_Of1st_1")]
        public string RelatedName5th_Of1st_1 { get; set; }
        [BookmarkMapping("RelatedAddress5th_Of1st_1")]
        public string RelatedAddress5th_Of1st_1 { get; set; }
        [BookmarkMapping("RelatedContractNo5th_Of1st_1")]
        public string RelatedContractNo5th_Of1st_1 { get; set; }
        [BookmarkMapping("RelatedContractDate5th_Of1st_1")]
        public string RelatedContractDate5th_Of1st_1 { get; set; }
        public DateTime? qRelatedContractDate5th_Of1st_1 { get; set; }

        [BookmarkMapping("RelatedName6th_Of1st_1")]
        public string RelatedName6th_Of1st_1 { get; set; }
        [BookmarkMapping("RelatedAddress6th_Of1st_1")]
        public string RelatedAddress6th_Of1st_1 { get; set; }
        [BookmarkMapping("RelatedContractNo6th_Of1st_1")]
        public string RelatedContractNo6th_Of1st_1 { get; set; }
        [BookmarkMapping("RelatedContractDate6th_Of1st_1")]
        public string RelatedContractDate6th_Of1st_1 { get; set; }
        public DateTime? qRelatedContractDate6th_Of1st_1 { get; set; }

        [BookmarkMapping("RelatedName7th_Of1st_1")]
        public string RelatedName7th_Of1st_1 { get; set; }
        [BookmarkMapping("RelatedAddress7th_Of1st_1")]
        public string RelatedAddress7th_Of1st_1 { get; set; }
        [BookmarkMapping("RelatedContractNo7th_Of1st_1")]
        public string RelatedContractNo7th_Of1st_1 { get; set; }
        [BookmarkMapping("RelatedContractDate7th_Of1st_1")]
        public string RelatedContractDate7th_Of1st_1 { get; set; }
        public DateTime? qRelatedContractDate7th_Of1st_1 { get; set; }

        [BookmarkMapping("RelatedName8th_Of1st_1")]
        public string RelatedName8th_Of1st_1 { get; set; }
        [BookmarkMapping("RelatedAddress8th_Of1st_1")]
        public string RelatedAddress8th_Of1st_1 { get; set; }
        [BookmarkMapping("RelatedContractNo8th_Of1st_1")]
        public string RelatedContractNo8th_Of1st_1 { get; set; }
        [BookmarkMapping("RelatedContractDate8th_Of1st_1")]
        public string RelatedContractDate8th_Of1st_1 { get; set; }
        public DateTime? qRelatedContractDate8th_Of1st_1 { get; set; }

        [BookmarkMapping("GuarantorName2nd_1")]
        public string GuarantorName2nd_1 { get; set; }
        [BookmarkMapping("GuarantorName2nd_2")]
        public string GuarantorName2nd_2 { get; set; }
        [BookmarkMapping("GuarantorAge2nd_1")]
        public string GuarantorAge2nd_1 { get; set; }
        [BookmarkMapping("GuarantorRace2nd_1")]
        public string GuarantorRace2nd_1 { get; set; }
        [BookmarkMapping("GuarantorNationality2nd_1")]
        public string GuarantorNationality2nd_1 { get; set; }
        [BookmarkMapping("GuarantorAddress2nd_1")]
        public string GuarantorAddress2nd_1 { get; set; }
        [BookmarkMapping("GuarantorTaxID2nd_1")]
        public string GuarantorTaxID2nd_1 { get; set; }
        [BookmarkMapping("GuarantorDateregis2nd_1")]
        public string GuarantorDateregis2nd_1 { get; set; }
        public DateTime? qGuarantorDateregis2nd_1 { get; set; }
        [BookmarkMapping("RelatedName1st_Of2nd_1")]
        public string RelatedName1st_Of2nd_1 { get; set; }
        [BookmarkMapping("RelatedAddress1st_Of2nd_1")]
        public string RelatedAddress1st_Of2nd_1 { get; set; }
        [BookmarkMapping("RelatedContractNo1st_Of2nd_1")]
        public string RelatedContractNo1st_Of2nd_1 { get; set; }
        [BookmarkMapping("RelatedContractDate1st_Of2nd_1")]
        public string RelatedContractDate1st_Of2nd_1 { get; set; }
        public DateTime? qRelatedContractDate1st_Of2nd_1 { get; set; }

        [BookmarkMapping("RelatedName2nd_Of2nd_1")]
        public string RelatedName2nd_Of2nd_1 { get; set; }
        [BookmarkMapping("RelatedAddress2nd_Of2nd_1")]
        public string RelatedAddress2nd_Of2nd_1 { get; set; }
        [BookmarkMapping("RelatedContractNo2nd_Of2nd_1")]
        public string RelatedContractNo2nd_Of2nd_1 { get; set; }
        [BookmarkMapping("RelatedContractDate2nd_Of2nd_1")]
        public string RelatedContractDate2nd_Of2nd_1 { get; set; }
        public DateTime? qRelatedContractDate2nd_Of2nd_1 { get; set; }

        [BookmarkMapping("RelatedName3rd_Of2nd_1")]
        public string RelatedName3rd_Of2nd_1 { get; set; }
        [BookmarkMapping("RelatedAddress3rd_Of2nd_1")]
        public string RelatedAddress3rd_Of2nd_1 { get; set; }
        [BookmarkMapping("RelatedContractNo3rd_Of2nd_1")]
        public string RelatedContractNo3rd_Of2nd_1 { get; set; }
        [BookmarkMapping("RelatedContractDate3rd_Of2nd_1")]
        public string RelatedContractDate3rd_Of2nd_1 { get; set; }
        public DateTime? qRelatedContractDate3rd_Of2nd_1 { get; set; }

        [BookmarkMapping("RelatedName4th_Of2nd_1")]
        public string RelatedName4th_Of2nd_1 { get; set; }
        [BookmarkMapping("RelatedAddress4th_Of2nd_1")]
        public string RelatedAddress4th_Of2nd_1 { get; set; }
        [BookmarkMapping("RelatedContractNo4th_Of2nd_1")]
        public string RelatedContractNo4th_Of2nd_1 { get; set; }
        [BookmarkMapping("RelatedContractDate4th_Of2nd_1")]
        public string RelatedContractDate4th_Of2nd_1 { get; set; }
        public DateTime? qRelatedContractDate4th_Of2nd_1 { get; set; }

        [BookmarkMapping("RelatedName5th_Of2nd_1")]
        public string RelatedName5th_Of2nd_1 { get; set; }
        [BookmarkMapping("RelatedAddress5th_Of2nd_1")]
        public string RelatedAddress5th_Of2nd_1 { get; set; }
        [BookmarkMapping("RelatedContractNo5th_Of2nd_1")]
        public string RelatedContractNo5th_Of2nd_1 { get; set; }
        [BookmarkMapping("RelatedContractDate5th_Of2nd_1")]
        public string RelatedContractDate5th_Of2nd_1 { get; set; }
        public DateTime? qRelatedContractDate5th_Of2nd_1 { get; set; }

        [BookmarkMapping("RelatedName6th_Of2nd_1")]
        public string RelatedName6th_Of2nd_1 { get; set; }
        [BookmarkMapping("RelatedAddress6th_Of2nd_1")]
        public string RelatedAddress6th_Of2nd_1 { get; set; }
        [BookmarkMapping("RelatedContractNo6th_Of2nd_1")]
        public string RelatedContractNo6th_Of2nd_1 { get; set; }
        [BookmarkMapping("RelatedContractDate6th_Of2nd_1")]
        public string RelatedContractDate6th_Of2nd_1 { get; set; }
        public DateTime? qRelatedContractDate6th_Of2nd_1 { get; set; }

        [BookmarkMapping("RelatedName7th_Of2nd_1")]
        public string RelatedName7th_Of2nd_1 { get; set; }
        [BookmarkMapping("RelatedAddress7th_Of2nd_1")]
        public string RelatedAddress7th_Of2nd_1 { get; set; }
        [BookmarkMapping("RelatedContractNo7th_Of2nd_1")]
        public string RelatedContractNo7th_Of2nd_1 { get; set; }
        [BookmarkMapping("RelatedContractDate7th_Of2nd_1")]
        public string RelatedContractDate7th_Of2nd_1 { get; set; }
        public DateTime? qRelatedContractDate7th_Of2nd_1 { get; set; }
        [BookmarkMapping("RelatedName8th_Of2nd_1")]
        public string RelatedName8th_Of2nd_1 { get; set; }
        [BookmarkMapping("RelatedAddress8th_Of2nd_1")]
        public string RelatedAddress8th_Of2nd_1 { get; set; }
        [BookmarkMapping("RelatedContractNo8th_Of2nd_1")]
        public string RelatedContractNo8th_Of2nd_1 { get; set; }
        [BookmarkMapping("RelatedContractDate8th_Of2nd_1")]
        public string RelatedContractDate8th_Of2nd_1 { get; set; }
        public DateTime? qRelatedContractDate8th_Of2nd_1 { get; set; }
        [BookmarkMapping("GuarantorName3rd_1")]
        public string GuarantorName3rd_1 { get; set; }
        [BookmarkMapping("GuarantorName3rd_2")]
        public string GuarantorName3rd_2 { get; set; }
        [BookmarkMapping("GuarantorAge3rd_1")]
        public string GuarantorAge3rd_1 { get; set; }
        [BookmarkMapping("GuarantorRace3rd_1")]
        public string GuarantorRace3rd_1 { get; set; }
        [BookmarkMapping("GuarantorNationality3rd_1")]
        public string GuarantorNationality3rd_1 { get; set; }
        [BookmarkMapping("GuarantorAddress3rd_1")]
        public string GuarantorAddress3rd_1 { get; set; }
        [BookmarkMapping("GuarantorTaxID3rd_1")]
        public string GuarantorTaxID3rd_1 { get; set; }
        [BookmarkMapping("GuarantorDateregis3rd_1")]
        public string GuarantorDateregis3rd_1 { get; set; }

        public DateTime? qGuarantorDateregis3rd_1 { get; set; }

        [BookmarkMapping("RelatedName1st_Of3rd_1")]
        public string RelatedName1st_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedAddress1st_Of3rd_1")]
        public string RelatedAddress1st_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedContractNo1st_Of3rd_1")]
        public string RelatedContractNo1st_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedContractDate1st_Of3rd_1")]
        public string RelatedContractDate1st_Of3rd_1 { get; set; }
        public DateTime? qRelatedContractDate1st_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedName2nd_Of3rd_1")]
        public string RelatedName2nd_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedAddress2nd_Of3rd_1")]
        public string RelatedAddress2nd_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedContractNo2nd_Of3rd_1")]
        public string RelatedContractNo2nd_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedContractDate2nd_Of3rd_1")]
        public string RelatedContractDate2nd_Of3rd_1 { get; set; }
        public DateTime? qRelatedContractDate2nd_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedName3rd_Of3rd_1")]
        public string RelatedName3rd_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedAddress3rd_Of3rd_1")]
        public string RelatedAddress3rd_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedContractNo3rd_Of3rd_1")]
        public string RelatedContractNo3rd_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedContractDate3rd_Of3rd_1")]
        public string RelatedContractDate3rd_Of3rd_1 { get; set; }
        public DateTime? qRelatedContractDate3rd_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedName4th_Of3rd_1")]
        public string RelatedName4th_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedAddress4th_Of3rd_1")]
        public string RelatedAddress4th_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedContractNo4th_Of3rd_1")]
        public string RelatedContractNo4th_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedContractDate4th_Of3rd_1")]
        public string RelatedContractDate4th_Of3rd_1 { get; set; }
        public DateTime? qRelatedContractDate4th_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedName5th_Of3rd_1")]
        public string RelatedName5th_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedAddress5th_Of3rd_1")]
        public string RelatedAddress5th_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedContractNo5th_Of3rd_1")]
        public string RelatedContractNo5th_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedContractDate5th_Of3rd_1")]
        public string RelatedContractDate5th_Of3rd_1 { get; set; }
        public DateTime? qRelatedContractDate5th_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedName6th_Of3rd_1")]
        public string RelatedName6th_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedAddress6th_Of3rd_1")]
        public string RelatedAddress6th_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedContractNo6th_Of3rd_1")]
        public string RelatedContractNo6th_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedContractDate6th_Of3rd_1")]
        public string RelatedContractDate6th_Of3rd_1 { get; set; }
        public DateTime? qRelatedContractDate6th_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedName7th_Of3rd_1")]
        public string RelatedName7th_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedAddress7th_Of3rd_1")]
        public string RelatedAddress7th_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedContractNo7th_Of3rd_1")]
        public string RelatedContractNo7th_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedContractDate7th_Of3rd_1")]
        public string RelatedContractDate7th_Of3rd_1 { get; set; }
        public DateTime? qRelatedContractDate7th_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedName8th_Of3rd_1")]
        public string RelatedName8th_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedAddress8th_Of3rd_1")]
        public string RelatedAddress8th_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedContractNo8th_Of3rd_1")]
        public string RelatedContractNo8th_Of3rd_1 { get; set; }
        [BookmarkMapping("RelatedContractDate8th_Of3rd_1")]
        public string RelatedContractDate8th_Of3rd_1 { get; set; }
        public DateTime? qRelatedContractDate8th_Of3rd_1 { get; set; }
        [BookmarkMapping("GuarantorName4th_1")]
        public string GuarantorName4th_1 { get; set; }
        [BookmarkMapping("GuarantorName4th_2")]
        public string GuarantorName4th_2 { get; set; }
        [BookmarkMapping("GuarantorAge4th_1")]
        public string GuarantorAge4th_1 { get; set; }
        [BookmarkMapping("GuarantorRace4th_1")]
        public string GuarantorRace4th_1 { get; set; }
        [BookmarkMapping("GuarantorNationality4th_1")]
        public string GuarantorNationality4th_1 { get; set; }
        [BookmarkMapping("GuarantorAddress4th_1")]
        public string GuarantorAddress4th_1 { get; set; }
        [BookmarkMapping("GuarantorTaxID4th_1")]
        public string GuarantorTaxID4th_1 { get; set; }
        [BookmarkMapping("GuarantorDateregis4th_1")]
        public string GuarantorDateregis4th_1 { get; set; }
        public DateTime? qGuarantorDateregis4th_1 { get; set; }

        [BookmarkMapping("RelatedName1st_Of4th_1")]
        public string RelatedName1st_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedAddress1st_Of4th_1")]
        public string RelatedAddress1st_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo1st_Of4th_1")]
        public string RelatedContractNo1st_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate1st_Of4th_1")]
        public string RelatedContractDate1st_Of4th_1 { get; set; }
        public DateTime? qRelatedContractDate1st_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedName2nd_Of4th_1")]
        public string RelatedName2nd_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedAddress2nd_Of4th_1")]
        public string RelatedAddress2nd_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo2nd_Of4th_1")]
        public string RelatedContractNo2nd_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate2nd_Of4th_1")]
        public string RelatedContractDate2nd_Of4th_1 { get; set; }
        public DateTime? qRelatedContractDate2nd_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedName3rd_Of4th_1")]
        public string RelatedName3rd_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedAddress3rd_Of4th_1")]
        public string RelatedAddress3rd_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo3rd_Of4th_1")]
        public string RelatedContractNo3rd_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate3rd_Of4th_1")]
        public string RelatedContractDate3rd_Of4th_1 { get; set; }
        public DateTime? qRelatedContractDate3rd_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedName4th_Of4th_1")]
        public string RelatedName4th_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedAddress4th_Of4th_1")]
        public string RelatedAddress4th_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo4th_Of4th_1")]
        public string RelatedContractNo4th_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate4th_Of4th_1")]
        public string RelatedContractDate4th_Of4th_1 { get; set; }
        public DateTime? qRelatedContractDate4th_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedName5th_Of4th_1")]
        public string RelatedName5th_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedAddress5th_Of4th_1")]
        public string RelatedAddress5th_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo5th_Of4th_1")]
        public string RelatedContractNo5th_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate5th_Of4th_1")]
        public string RelatedContractDate5th_Of4th_1 { get; set; }
        public DateTime? qRelatedContractDate5th_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedName6th_Of4th_1")]
        public string RelatedName6th_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedAddress6th_Of4th_1")]
        public string RelatedAddress6th_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo6th_Of4th_1")]
        public string RelatedContractNo6th_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate6th_Of4th_1")]
        public string RelatedContractDate6th_Of4th_1 { get; set; }
        public DateTime? qRelatedContractDate6th_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedName7th_Of4th_1")]
        public string RelatedName7th_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedAddress7th_Of4th_1")]
        public string RelatedAddress7th_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo7th_Of4th_1")]
        public string RelatedContractNo7th_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate7th_Of4th_1")]
        public string RelatedContractDate7th_Of4th_1 { get; set; }
        public DateTime? qRelatedContractDate7th_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedName8th_Of4th_1")]
        public string RelatedName8th_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedAddress8th_Of4th_1")]
        public string RelatedAddress8th_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo8th_Of4th_1")]
        public string RelatedContractNo8th_Of4th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate8th_Of4th_1")]
        public string RelatedContractDate8th_Of4th_1 { get; set; }
        public DateTime? qRelatedContractDate8th_Of4th_1 { get; set; }
        [BookmarkMapping("GuarantorName5th_1")]
        public string GuarantorName5th_1 { get; set; }
        [BookmarkMapping("GuarantorName5th_2")]
        public string GuarantorName5th_2 { get; set; }
        [BookmarkMapping("GuarantorAge5th_1")]
        public string GuarantorAge5th_1 { get; set; }
        [BookmarkMapping("GuarantorRace5th_1")]
        public string GuarantorRace5th_1 { get; set; }
        [BookmarkMapping("GuarantorNationality5th_1")]
        public string GuarantorNationality5th_1 { get; set; }
        [BookmarkMapping("GuarantorAddress5th_1")]
        public string GuarantorAddress5th_1 { get; set; }
        [BookmarkMapping("GuarantorTaxID5th_1")]
        public string GuarantorTaxID5th_1 { get; set; }
        [BookmarkMapping("GuarantorDateregis5th_1")]
        public string GuarantorDateregis5th_1 { get; set; }
        public DateTime? qGuarantorDateregis5th_1 { get; set; }

        [BookmarkMapping("RelatedName1st_Of5th_1")]
        public string RelatedName1st_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedAddress1st_Of5th_1")]
        public string RelatedAddress1st_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo1st_Of5th_1")]
        public string RelatedContractNo1st_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate1st_Of5th_1")]
        public string RelatedContractDate1st_Of5th_1 { get; set; }
        public DateTime? qRelatedContractDate1st_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedName2nd_Of5th_1")]
        public string RelatedName2nd_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedAddress2nd_Of5th_1")]
        public string RelatedAddress2nd_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo2nd_Of5th_1")]
        public string RelatedContractNo2nd_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate2nd_Of5th_1")]
        public string RelatedContractDate2nd_Of5th_1 { get; set; }
        public DateTime? qRelatedContractDate2nd_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedName3rd_Of5th_1")]
        public string RelatedName3rd_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedAddress3rd_Of5th_1")]
        public string RelatedAddress3rd_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo3rd_Of5th_1")]
        public string RelatedContractNo3rd_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate3rd_Of5th_1")]
        public string RelatedContractDate3rd_Of5th_1 { get; set; }
        public DateTime? qRelatedContractDate3rd_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedName4th_Of5th_1")]
        public string RelatedName4th_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedAddress4th_Of5th_1")]
        public string RelatedAddress4th_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo4th_Of5th_1")]
        public string RelatedContractNo4th_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate4th_Of5th_1")]
        public string RelatedContractDate4th_Of5th_1 { get; set; }
        public DateTime? qRelatedContractDate4th_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedName5th_Of5th_1")]
        public string RelatedName5th_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedAddress5th_Of5th_1")]
        public string RelatedAddress5th_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo5th_Of5th_1")]
        public string RelatedContractNo5th_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate5th_Of5th_1")]
        public string RelatedContractDate5th_Of5th_1 { get; set; }
        public DateTime? qRelatedContractDate5th_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedName6th_Of5th_1")]
        public string RelatedName6th_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedAddress6th_Of5th_1")]
        public string RelatedAddress6th_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo6th_Of5th_1")]
        public string RelatedContractNo6th_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate6th_Of5th_1")]
        public string RelatedContractDate6th_Of5th_1 { get; set; }
        public DateTime? qRelatedContractDate6th_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedName7th_Of5th_1")]
        public string RelatedName7th_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedAddress7th_Of5th_1")]
        public string RelatedAddress7th_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo7th_Of5th_1")]
        public string RelatedContractNo7th_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate7th_Of5th_1")]
        public string RelatedContractDate7th_Of5th_1 { get; set; }
        public DateTime? qRelatedContractDate7th_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedName8th_Of5th_1")]
        public string RelatedName8th_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedAddress8th_Of5th_1")]
        public string RelatedAddress8th_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo8th_Of5th_1")]
        public string RelatedContractNo8th_Of5th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate8th_Of5th_1")]
        public string RelatedContractDate8th_Of5th_1 { get; set; }
        public DateTime? qRelatedContractDate8th_Of5th_1 { get; set; }
        [BookmarkMapping("GuarantorName6th_1")]
        public string GuarantorName6th_1 { get; set; }
        [BookmarkMapping("GuarantorName6th_2")]
        public string GuarantorName6th_2 { get; set; }
        [BookmarkMapping("GuarantorAge6th_1")]
        public string GuarantorAge6th_1 { get; set; }
        [BookmarkMapping("GuarantorRace6th_1")]
        public string GuarantorRace6th_1 { get; set; }
        [BookmarkMapping("GuarantorNationality6th_1")]
        public string GuarantorNationality6th_1 { get; set; }
        [BookmarkMapping("GuarantorAddress6th_1")]
        public string GuarantorAddress6th_1 { get; set; }
        [BookmarkMapping("GuarantorTaxID6th_1")]
        public string GuarantorTaxID6th_1 { get; set; }
        [BookmarkMapping("GuarantorDateregis6th_1")]
        public string GuarantorDateregis6th_1 { get; set; }
        public DateTime? qGuarantorDateregis6th_1 { get; set; }

        [BookmarkMapping("RelatedName1st_Of6th_1")]
        public string RelatedName1st_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedAddress1st_Of6th_1")]
        public string RelatedAddress1st_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo1st_Of6th_1")]
        public string RelatedContractNo1st_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate1st_Of6th_1")]
        public string RelatedContractDate1st_Of6th_1 { get; set; }
        public DateTime? qRelatedContractDate1st_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedName2nd_Of6th_1")]
        public string RelatedName2nd_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedAddress2nd_Of6th_1")]
        public string RelatedAddress2nd_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo2nd_Of6th_1")]
        public string RelatedContractNo2nd_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate2nd_Of6th_1")]
        public string RelatedContractDate2nd_Of6th_1 { get; set; }
        public DateTime? qRelatedContractDate2nd_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedName3rd_Of6th_1")]
        public string RelatedName3rd_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedAddress3rd_Of6th_1")]
        public string RelatedAddress3rd_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo3rd_Of6th_1")]
        public string RelatedContractNo3rd_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate3rd_Of6th_1")]
        public string RelatedContractDate3rd_Of6th_1 { get; set; }
        public DateTime? qRelatedContractDate3rd_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedName4th_Of6th_1")]
        public string RelatedName4th_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedAddress4th_Of6th_1")]
        public string RelatedAddress4th_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo4th_Of6th_1")]
        public string RelatedContractNo4th_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate4th_Of6th_1")]
        public string RelatedContractDate4th_Of6th_1 { get; set; }
        public DateTime? qRelatedContractDate4th_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedName5th_Of6th_1")]
        public string RelatedName5th_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedAddress5th_Of6th_1")]
        public string RelatedAddress5th_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo5th_Of6th_1")]
        public string RelatedContractNo5th_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate5th_Of6th_1")]
        public string RelatedContractDate5th_Of6th_1 { get; set; }
        public DateTime? qRelatedContractDate5th_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedName6th_Of6th_1")]
        public string RelatedName6th_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedAddress6th_Of6th_1")]
        public string RelatedAddress6th_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo6th_Of6th_1")]
        public string RelatedContractNo6th_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate6th_Of6th_1")]
        public string RelatedContractDate6th_Of6th_1 { get; set; }
        public DateTime? qRelatedContractDate6th_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedName7th_Of6th_1")]
        public string RelatedName7th_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedAddress7th_Of6th_1")]
        public string RelatedAddress7th_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo7th_Of6th_1")]
        public string RelatedContractNo7th_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate7th_Of6th_1")]
        public string RelatedContractDate7th_Of6th_1 { get; set; }
        public DateTime? qRelatedContractDate7th_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedName8th_Of6th_1")]
        public string RelatedName8th_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedAddress8th_Of6th_1")]
        public string RelatedAddress8th_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo8th_Of6th_1")]
        public string RelatedContractNo8th_Of6th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate8th_Of6th_1")]
        public string RelatedContractDate8th_Of6th_1 { get; set; }
        public DateTime? qRelatedContractDate8th_Of6th_1 { get; set; }
        [BookmarkMapping("GuarantorName7th_1")]
        public string GuarantorName7th_1 { get; set; }
        [BookmarkMapping("GuarantorName7th_2")]
        public string GuarantorName7th_2 { get; set; }
        [BookmarkMapping("GuarantorAge7th_1")]
        public string GuarantorAge7th_1 { get; set; }
        [BookmarkMapping("GuarantorRace7th_1")]
        public string GuarantorRace7th_1 { get; set; }
        [BookmarkMapping("GuarantorNationality7th_1")]
        public string GuarantorNationality7th_1 { get; set; }
        [BookmarkMapping("GuarantorAddress7th_1")]
        public string GuarantorAddress7th_1 { get; set; }
        [BookmarkMapping("GuarantorTaxID7th_1")]
        public string GuarantorTaxID7th_1 { get; set; }
        [BookmarkMapping("GuarantorDateregis7th_1")]
        public string GuarantorDateregis7th_1 { get; set; }
        public DateTime? qGuarantorDateregis7th_1 { get; set; }

        [BookmarkMapping("RelatedName1st_Of7th_1")]
        public string RelatedName1st_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedAddress1st_Of7th_1")]
        public string RelatedAddress1st_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo1st_Of7th_1")]
        public string RelatedContractNo1st_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate1st_Of7th_1")]
        public string RelatedContractDate1st_Of7th_1 { get; set; }
        public DateTime? qRelatedContractDate1st_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedName2nd_Of7th_1")]
        public string RelatedName2nd_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedAddress2nd_Of7th_1")]
        public string RelatedAddress2nd_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo2nd_Of7th_1")]
        public string RelatedContractNo2nd_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate2nd_Of7th_1")]
        public string RelatedContractDate2nd_Of7th_1 { get; set; }
        public DateTime? qRelatedContractDate2nd_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedName3rd_Of7th_1")]
        public string RelatedName3rd_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedAddress3rd_Of7th_1")]
        public string RelatedAddress3rd_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo3rd_Of7th_1")]
        public string RelatedContractNo3rd_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate3rd_Of7th_1")]
        public string RelatedContractDate3rd_Of7th_1 { get; set; }
        public DateTime? qRelatedContractDate3rd_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedName4th_Of7th_1")]
        public string RelatedName4th_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedAddress4th_Of7th_1")]
        public string RelatedAddress4th_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo4th_Of7th_1")]
        public string RelatedContractNo4th_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate4th_Of7th_1")]
        public string RelatedContractDate4th_Of7th_1 { get; set; }
        public DateTime? qRelatedContractDate4th_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedName5th_Of7th_1")]
        public string RelatedName5th_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedAddress5th_Of7th_1")]
        public string RelatedAddress5th_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo5th_Of7th_1")]
        public string RelatedContractNo5th_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate5th_Of7th_1")]
        public string RelatedContractDate5th_Of7th_1 { get; set; }
        public DateTime? qRelatedContractDate5th_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedName6th_Of7th_1")]
        public string RelatedName6th_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedAddress6th_Of7th_1")]
        public string RelatedAddress6th_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo6th_Of7th_1")]
        public string RelatedContractNo6th_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate6th_Of7th_1")]
        public string RelatedContractDate6th_Of7th_1 { get; set; }
        public DateTime? qRelatedContractDate6th_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedName7th_Of7th_1")]
        public string RelatedName7th_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedAddress7th_Of7th_1")]
        public string RelatedAddress7th_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo7th_Of7th_1")]
        public string RelatedContractNo7th_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate7th_Of7th_1")]
        public string RelatedContractDate7th_Of7th_1 { get; set; }
        public DateTime? qRelatedContractDate7th_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedName8th_Of7th_1")]
        public string RelatedName8th_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedAddress8th_Of7th_1")]
        public string RelatedAddress8th_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo8th_Of7th_1")]
        public string RelatedContractNo8th_Of7th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate8th_Of7th_1")]
        public string RelatedContractDate8th_Of7th_1 { get; set; }
        public DateTime? qRelatedContractDate8th_Of7th_1 { get; set; }
        [BookmarkMapping("GuarantorName8th_1")]
        public string GuarantorName8th_1 { get; set; }
        [BookmarkMapping("GuarantorName8th_2")]
        public string GuarantorName8th_2 { get; set; }
        [BookmarkMapping("GuarantorAge8th_1")]
        public string GuarantorAge8th_1 { get; set; }
        [BookmarkMapping("GuarantorRace8th_1")]
        public string GuarantorRace8th_1 { get; set; }
        [BookmarkMapping("GuarantorNationality8th_1")]
        public string GuarantorNationality8th_1 { get; set; }
        [BookmarkMapping("GuarantorAddress8th_1")]
        public string GuarantorAddress8th_1 { get; set; }
        [BookmarkMapping("GuarantorTaxID8th_1")]
        public string GuarantorTaxID8th_1 { get; set; }
        [BookmarkMapping("GuarantorDateregis8th_1")]
        public string GuarantorDateregis8th_1 { get; set; }
        public DateTime? qGuarantorDateregis8th_1 { get; set; }

        [BookmarkMapping("RelatedName1st_Of8th_1")]
        public string RelatedName1st_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedAddress1st_Of8th_1")]
        public string RelatedAddress1st_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo1st_Of8th_1")]
        public string RelatedContractNo1st_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate1st_Of8th_1")]
        public string RelatedContractDate1st_Of8th_1 { get; set; }
        public DateTime? qRelatedContractDate1st_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedName2nd_Of8th_1")]
        public string RelatedName2nd_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedAddress2nd_Of8th_1")]
        public string RelatedAddress2nd_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo2nd_Of8th_1")]
        public string RelatedContractNo2nd_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate2nd_Of8th_1")]
        public string RelatedContractDate2nd_Of8th_1 { get; set; }
        public DateTime? qRelatedContractDate2nd_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedName3rd_Of8th_1")]
        public string RelatedName3rd_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedAddress3rd_Of8th_1")]
        public string RelatedAddress3rd_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo3rd_Of8th_1")]
        public string RelatedContractNo3rd_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate3rd_Of8th_1")]
        public string RelatedContractDate3rd_Of8th_1 { get; set; }
        public DateTime? qRelatedContractDate3rd_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedName4th_Of8th_1")]
        public string RelatedName4th_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedAddress4th_Of8th_1")]
        public string RelatedAddress4th_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo4th_Of8th_1")]
        public string RelatedContractNo4th_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate4th_Of8th_1")]
        public string RelatedContractDate4th_Of8th_1 { get; set; }
        public DateTime? qRelatedContractDate4th_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedName5th_Of8th_1")]
        public string RelatedName5th_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedAddress5th_Of8th_1")]
        public string RelatedAddress5th_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo5th_Of8th_1")]
        public string RelatedContractNo5th_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate5th_Of8th_1")]
        public string RelatedContractDate5th_Of8th_1 { get; set; }
        public DateTime? qRelatedContractDate5th_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedName6th_Of8th_1")]
        public string RelatedName6th_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedAddress6th_Of8th_1")]
        public string RelatedAddress6th_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo6th_Of8th_1")]
        public string RelatedContractNo6th_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate6th_Of8th_1")]
        public string RelatedContractDate6th_Of8th_1 { get; set; }
        public DateTime? qRelatedContractDate6th_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedName7th_Of8th_1")]
        public string RelatedName7th_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedAddress7th_Of8th_1")]
        public string RelatedAddress7th_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo7th_Of8th_1")]
        public string RelatedContractNo7th_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate7th_Of8th_1")]
        public string RelatedContractDate7th_Of8th_1 { get; set; }
        public DateTime? qRelatedContractDate7th_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedName8th_Of8th_1")]
        public string RelatedName8th_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedAddress8th_Of8th_1")]
        public string RelatedAddress8th_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedContractNo8th_Of8th_1")]
        public string RelatedContractNo8th_Of8th_1 { get; set; }
        [BookmarkMapping("RelatedContractDate8th_Of8th_1")]
        public string RelatedContractDate8th_Of8th_1 { get; set; }
        public DateTime? qRelatedContractDate8th_Of8th_1 { get; set; }
        [BookmarkMapping("GuarantorYear1")]
        public int GuarantorYear1 { get; set; }
        [BookmarkMapping("GuarantorYear2")]
        public int GuarantorYear2 { get; set; }
        [BookmarkMapping("GuarantorYear3")]
        public int GuarantorYear3 { get; set; }
        [BookmarkMapping("GuarantorYear4")]
        public int GuarantorYear4 { get; set; }
        [BookmarkMapping("GuarantorYear5")]
        public int GuarantorYear5 { get; set; }
        [BookmarkMapping("GuarantorYear6")]
        public int GuarantorYear6 { get; set; }
        [BookmarkMapping("GuarantorYear7")]
        public int GuarantorYear7 { get; set; }
        [BookmarkMapping("GuarantorYear8")]
        public int GuarantorYear8 { get; set; }
        [BookmarkMapping("WitnessFirst1")]
        public string WitnessFirst1 { get; set; }
        [BookmarkMapping("WitnessFirst2")]
        public string WitnessFirst2 { get; set; }
        [BookmarkMapping("WitnessFirst3")]
        public string WitnessFirst3 { get; set; }
        [BookmarkMapping("WitnessFirst4")]
        public string WitnessFirst4 { get; set; }
        [BookmarkMapping("WitnessFirst5")]
        public string WitnessFirst5 { get; set; }
        [BookmarkMapping("WitnessFirst6")]
        public string WitnessFirst6 { get; set; }
        [BookmarkMapping("WitnessFirst7")]
        public string WitnessFirst7 { get; set; }
        [BookmarkMapping("WitnessFirst8")]
        public string WitnessFirst8 { get; set; }
        [BookmarkMapping("WitnessSecond1")]
        public string WitnessSecond1 { get; set; }
        [BookmarkMapping("WitnessSecond2")]
        public string WitnessSecond2 { get; set; }
        [BookmarkMapping("WitnessSecond3")]
        public string WitnessSecond3 { get; set; }
        [BookmarkMapping("WitnessSecond4")]
        public string WitnessSecond4 { get; set; }
        [BookmarkMapping("WitnessSecond5")]
        public string WitnessSecond5 { get; set; }
        [BookmarkMapping("WitnessSecond6")]
        public string WitnessSecond6 { get; set; }
        [BookmarkMapping("WitnessSecond7")]
        public string WitnessSecond7 { get; set; }
        [BookmarkMapping("WitnessSecond8")]
        public string WitnessSecond8 { get; set; }
        [BookmarkMapping("MaxGuaranteeAmt1st_1")]
        public decimal MaxGuaranteeAmt1st_1 { get; set; }
        [BookmarkMapping("MaxGuaranteeTxt1st_1")]
        public string MaxGuaranteeTxt1st_1 { get; set; }
        [BookmarkMapping("MaxGuaranteeAmt2nd_1")]
        public decimal MaxGuaranteeAmt2nd_1 { get; set; }
        [BookmarkMapping("MaxGuaranteeTxt2nd_1")]
        public string MaxGuaranteeTxt2nd_1 { get; set; }
        [BookmarkMapping("MaxGuaranteeAmt3rd_1")]
        public decimal MaxGuaranteeAmt3rd_1 { get; set; }
        [BookmarkMapping("MaxGuaranteeTxt3rd_1")]
        public string MaxGuaranteeTxt3rd_1 { get; set; }
        [BookmarkMapping("MaxGuaranteeAmt4th_1")]
        public decimal MaxGuaranteeAmt4th_1 { get; set; }
        [BookmarkMapping("MaxGuaranteeTxt4th_1")]
        public string MaxGuaranteeTxt4th_1 { get; set; }
        [BookmarkMapping("MaxGuaranteeAmt5th_1")]
        public decimal MaxGuaranteeAmt5th_1 { get; set; }
        [BookmarkMapping("MaxGuaranteeTxt5th_1")]
        public string MaxGuaranteeTxt5th_1 { get; set; }
        [BookmarkMapping("MaxGuaranteeAmt6th_1")]
        public decimal MaxGuaranteeAmt6th_1 { get; set; }
        [BookmarkMapping("MaxGuaranteeTxt6th_1")]
        public string MaxGuaranteeTxt6th_1 { get; set; }
        [BookmarkMapping("MaxGuaranteeAmt7th_1")]
        public decimal MaxGuaranteeAmt7th_1 { get; set; }
        [BookmarkMapping("MaxGuaranteeTxt7th_1")]
        public string MaxGuaranteeTxt7th_1 { get; set; }
        [BookmarkMapping("MaxGuaranteeAmt8th_1")]
        public decimal MaxGuaranteeAmt8th_1 { get; set; }
        [BookmarkMapping("MaxGuaranteeTxt8th_1")]
        public string MaxGuaranteeTxt8th_1 { get; set; }
        [BookmarkMapping("ApprovedCreditLimitAmt1st_1")]
        public decimal ApprovedCreditLimitAmt1st_1 { get; set; }
        [BookmarkMapping("ApprovedCreditLimitTxt1st_1")]
        public string ApprovedCreditLimitTxt1st_1 { get; set; }
        [BookmarkMapping("ApprovedCreditLimitAmt2nd_1")]
        public decimal ApprovedCreditLimitAmt2nd_1 { get; set; }
        [BookmarkMapping("ApprovedCreditLimitTxt2nd_1")]
        public string ApprovedCreditLimitTxt2nd_1 { get; set; }
        [BookmarkMapping("ApprovedCreditLimitAmt3rd_1")]
        public decimal ApprovedCreditLimitAmt3rd_1 { get; set; }
        [BookmarkMapping("ApprovedCreditLimitTxt3rd_1")]
        public string ApprovedCreditLimitTxt3rd_1 { get; set; }
        [BookmarkMapping("ApprovedCreditLimitAmt4th_1")]
        public decimal ApprovedCreditLimitAmt4th_1 { get; set; }
        [BookmarkMapping("ApprovedCreditLimitTxt4th_1")]
        public string ApprovedCreditLimitTxt4th_1 { get; set; }
        [BookmarkMapping("ApprovedCreditLimitAmt5th_1")]
        public decimal ApprovedCreditLimitAmt5th_1 { get; set; }
        [BookmarkMapping("ApprovedCreditLimitTxt5th_1")]
        public string ApprovedCreditLimitTxt5th_1 { get; set; }
        [BookmarkMapping("ApprovedCreditLimitAmt6th_1")]
        public decimal ApprovedCreditLimitAmt6th_1 { get; set; }
        [BookmarkMapping("ApprovedCreditLimitTxt6th_1")]
        public string ApprovedCreditLimitTxt6th_1 { get; set; }
        [BookmarkMapping("ApprovedCreditLimitAmt7th_1")]
        public decimal ApprovedCreditLimitAmt7th_1 { get; set; }
        [BookmarkMapping("ApprovedCreditLimitTxt7th_1")]
        public string ApprovedCreditLimitTxt7th_1 { get; set; }
        [BookmarkMapping("ApprovedCreditLimitAmt8th_1")]
        public decimal ApprovedCreditLimitAmt8th_1 { get; set; }
        [BookmarkMapping("ApprovedCreditLimitTxt8th_1")]
        public string ApprovedCreditLimitTxt8th_1 { get; set; }
        public decimal ApprovedCreditLimit { get; set; }

        [BookmarkMapping("GuarantorName2nd_3")]
        public string GuarantorName2nd_3 { get; set; }
        [BookmarkMapping("GuarantorName3rd_3")]
        public string GuarantorName3rd_3 { get; set; }
        [BookmarkMapping("GuarantorName4th_3")]
        public string GuarantorName4th_3 { get; set; }
        [BookmarkMapping("GuarantorName5th_3")]
        public string GuarantorName5th_3 { get; set; }
        [BookmarkMapping("GuarantorName6th_3")]
        public string GuarantorName6th_3 { get; set; }
        [BookmarkMapping("GuarantorName7th_3")]
        public string GuarantorName7th_3 { get; set; }
        [BookmarkMapping("GuarantorName8th_3")]
        public string GuarantorName8th_3 { get; set; }

        [BookmarkMapping("GuarantorCrossTaxID1st_Of1st_1")]
        public string GuarantorCrossTaxID1st_Of1st_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID2nd_Of1st_1")]
        public string GuarantorCrossTaxID2nd_Of1st_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID3rd_Of1st_1")]
        public string GuarantorCrossTaxID3rd_Of1st_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID4th_Of1st_1")]
        public string GuarantorCrossTaxID4th_Of1st_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID5th_Of1st_1")]
        public string GuarantorCrossTaxID5th_Of1st_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID6th_Of1st_1")]
        public string GuarantorCrossTaxID6th_Of1st_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID7th_Of1st_1")]
        public string GuarantorCrossTaxID7th_Of1st_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID8th_Of1st_1")]
        public string GuarantorCrossTaxID8th_Of1st_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID1st_Of2nd_1")]
        public string GuarantorCrossTaxID1st_Of2nd_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID2nd_Of2nd_1")]
        public string GuarantorCrossTaxID2nd_Of2nd_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID3rd_Of2nd_1")]
        public string GuarantorCrossTaxID3rd_Of2nd_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID4th_Of2nd_1")]
        public string GuarantorCrossTaxID4th_Of2nd_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID5th_Of2nd_1")]
        public string GuarantorCrossTaxID5th_Of2nd_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID6th_Of2nd_1")]
        public string GuarantorCrossTaxID6th_Of2nd_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID7th_Of2nd_1")]
        public string GuarantorCrossTaxID7th_Of2nd_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID8th_Of2nd_1")]
        public string GuarantorCrossTaxID8th_Of2nd_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID1st_Of3rd_1")]
        public string GuarantorCrossTaxID1st_Of3rd_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID2nd_Of3rd_1")]
        public string GuarantorCrossTaxID2nd_Of3rd_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID3rd_Of3rd_1")]
        public string GuarantorCrossTaxID3rd_Of3rd_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID4th_Of3rd_1")]
        public string GuarantorCrossTaxID4th_Of3rd_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID5th_Of3rd_1")]
        public string GuarantorCrossTaxID5th_Of3rd_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID6th_Of3rd_1")]
        public string GuarantorCrossTaxID6th_Of3rd_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID7th_Of3rd_1")]
        public string GuarantorCrossTaxID7th_Of3rd_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID8th_Of3rd_1")]
        public string GuarantorCrossTaxID8th_Of3rd_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID1st_Of4th_1")]
        public string GuarantorCrossTaxID1st_Of4th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID2nd_Of4th_1")]
        public string GuarantorCrossTaxID2nd_Of4th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID3rd_Of4th_1")]
        public string GuarantorCrossTaxID3rd_Of4th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID4th_Of4th_1")]
        public string GuarantorCrossTaxID4th_Of4th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID5th_Of4th_1")]
        public string GuarantorCrossTaxID5th_Of4th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID6th_Of4th_1")]
        public string GuarantorCrossTaxID6th_Of4th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID7th_Of4th_1")]
        public string GuarantorCrossTaxID7th_Of4th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID8th_Of4th_1")]
        public string GuarantorCrossTaxID8th_Of4th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID1st_Of5th_1")]
        public string GuarantorCrossTaxID1st_Of5th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID2nd_Of5th_1")]
        public string GuarantorCrossTaxID2nd_Of5th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID3rd_Of5th_1")]
        public string GuarantorCrossTaxID3rd_Of5th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID4th_Of5th_1")]
        public string GuarantorCrossTaxID4th_Of5th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID5th_Of5th_1")]
        public string GuarantorCrossTaxID5th_Of5th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID6th_Of5th_1")]
        public string GuarantorCrossTaxID6th_Of5th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID7th_Of5th_1")]
        public string GuarantorCrossTaxID7th_Of5th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID8th_Of5th_1")]
        public string GuarantorCrossTaxID8th_Of5th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID1st_Of6th_1")]
        public string GuarantorCrossTaxID1st_Of6th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID2nd_Of6th_1")]
        public string GuarantorCrossTaxID2nd_Of6th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID3rd_Of6th_1")]
        public string GuarantorCrossTaxID3rd_Of6th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID4th_Of6th_1")]
        public string GuarantorCrossTaxID4th_Of6th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID5th_Of6th_1")]
        public string GuarantorCrossTaxID5th_Of6th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID6th_Of6th_1")]
        public string GuarantorCrossTaxID6th_Of6th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID7th_Of6th_1")]
        public string GuarantorCrossTaxID7th_Of6th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID8th_Of6th_1")]
        public string GuarantorCrossTaxID8th_Of6th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID1st_Of7th_1")]
        public string GuarantorCrossTaxID1st_Of7th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID2nd_Of7th_1")]
        public string GuarantorCrossTaxID2nd_Of7th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID3rd_Of7th_1")]
        public string GuarantorCrossTaxID3rd_Of7th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID4th_Of7th_1")]
        public string GuarantorCrossTaxID4th_Of7th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID5th_Of7th_1")]
        public string GuarantorCrossTaxID5th_Of7th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID6th_Of7th_1")]
        public string GuarantorCrossTaxID6th_Of7th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID7th_Of7th_1")]
        public string GuarantorCrossTaxID7th_Of7th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID8th_Of7th_1")]
        public string GuarantorCrossTaxID8th_Of7th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID1st_Of8th_1")]
        public string GuarantorCrossTaxID1st_Of8th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID2nd_Of8th_1")]
        public string GuarantorCrossTaxID2nd_Of8th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID3rd_Of8th_1")]
        public string GuarantorCrossTaxID3rd_Of8th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID4th_Of8th_1")]
        public string GuarantorCrossTaxID4th_Of8th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID5th_Of8th_1")]
        public string GuarantorCrossTaxID5th_Of8th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID6th_Of8th_1")]
        public string GuarantorCrossTaxID6th_Of8th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID7th_Of8th_1")]
        public string GuarantorCrossTaxID7th_Of8th_1 { get; set; }
        [BookmarkMapping("GuarantorCrossTaxID8th_Of8th_1")]
        public string GuarantorCrossTaxID8th_Of8th_1 { get; set; }
        [BookmarkMapping("GuarantotOperatedBy1st_1")]
        public string GuarantotOperatedBy1st_1 { get; set; }
        [BookmarkMapping("GuarantotOperatedBy1st_2")]
        public string GuarantotOperatedBy1st_2 { get; set; }
        [BookmarkMapping("GuarantotOperatedBy2nd_1")]
        public string GuarantotOperatedBy2nd_1 { get; set; }
        [BookmarkMapping("GuarantotOperatedBy2nd_2")]
        public string GuarantotOperatedBy2nd_2 { get; set; }
        [BookmarkMapping("GuarantotOperatedBy3rd_1")]
        public string GuarantotOperatedBy3rd_1 { get; set; }
        [BookmarkMapping("GuarantotOperatedBy3rd_2")]
        public string GuarantotOperatedBy3rd_2 { get; set; }
        [BookmarkMapping("GuarantotOperatedBy4th_1")]
        public string GuarantotOperatedBy4th_1 { get; set; }
        [BookmarkMapping("GuarantotOperatedBy4th_2")]
        public string GuarantotOperatedBy4th_2 { get; set; }
        [BookmarkMapping("GuarantotOperatedBy5th_1")]
        public string GuarantotOperatedBy5th_1 { get; set; }
        [BookmarkMapping("GuarantotOperatedBy5th_2")]
        public string GuarantotOperatedBy5th_2 { get; set; }
        [BookmarkMapping("GuarantotOperatedBy6th_1")]
        public string GuarantotOperatedBy6th_1 { get; set; }
        [BookmarkMapping("GuarantotOperatedBy6th_2")]
        public string GuarantotOperatedBy6th_2 { get; set; }
        [BookmarkMapping("GuarantotOperatedBy7th_1")]
        public string GuarantotOperatedBy7th_1 { get; set; }
        [BookmarkMapping("GuarantotOperatedBy7th_2")]
        public string GuarantotOperatedBy7th_2 { get; set; }
        [BookmarkMapping("GuarantotOperatedBy8th_1")]
        public string GuarantotOperatedBy8th_1 { get; set; }
        [BookmarkMapping("GuarantotOperatedBy8th_2")]
        public string GuarantotOperatedBy8th_2 { get; set; }

        public DateTime? qGuarantorDateOfBirth1st_1 { get; set; }
        public DateTime? qGuarantorDateOfBirth2nd_1 { get; set; }
        public DateTime? qGuarantorDateOfBirth3rd_1 { get; set; }
        public DateTime? qGuarantorDateOfBirth4th_1 { get; set; }
        public DateTime? qGuarantorDateOfBirth5th_1 { get; set; }
        public DateTime? qGuarantorDateOfBirth6th_1 { get; set; }
        public DateTime? qGuarantorDateOfBirth7th_1 { get; set; }
        public DateTime? qGuarantorDateOfBirth8th_1 { get; set; }

        // R04
        [BookmarkMapping("WritingAddress1th")]
        public string WritingAddress1th { get; set; }
        [BookmarkMapping("WritingAddress2nd")]
        public string WritingAddress2nd { get; set; }
        [BookmarkMapping("WritingAddress3rd")]
        public string WritingAddress3rd { get; set; }
        [BookmarkMapping("WritingAddress4th")]
        public string WritingAddress4th { get; set; }
        [BookmarkMapping("WritingAddress5th")]
        public string WritingAddress5th { get; set; }
        [BookmarkMapping("WritingAddress6th")]
        public string WritingAddress6th { get; set; }
        [BookmarkMapping("WritingAddress7th")]
        public string WritingAddress7th { get; set; }
        [BookmarkMapping("WritingAddress8th")]
        public string WritingAddress8th { get; set; }
        [BookmarkMapping("SumLineAffliliate1st")]
        public string SumLineAffliliate1st { get; set; }
        [BookmarkMapping("SumLineAffliliate2nd")]
        public string SumLineAffliliate2nd { get; set; }
        [BookmarkMapping("SumLineAffliliate3rd")]
        public string SumLineAffliliate3rd { get; set; }
        [BookmarkMapping("SumLineAffliliate4th")]
        public string SumLineAffliliate4th { get; set; }
        [BookmarkMapping("SumLineAffliliate5th")]
        public string SumLineAffliliate5th { get; set; }
        [BookmarkMapping("SumLineAffliliate6th")]
        public string SumLineAffliliate6th { get; set; }
        [BookmarkMapping("SumLineAffliliate7th")]
        public string SumLineAffliliate7th { get; set; }
        [BookmarkMapping("SumLineAffliliate8th")]
        public string SumLineAffliliate8th { get; set; }

        public int QSumLineAffliliate1st { get; set; }
        public int QSumLineAffliliate2nd { get; set; }
        public int QSumLineAffliliate3rd { get; set; }
        public int QSumLineAffliliate4th { get; set; }
        public int QSumLineAffliliate5th { get; set; }
        public int QSumLineAffliliate6th { get; set; }
        public int QSumLineAffliliate7th { get; set; }
        public int QSumLineAffliliate8th { get; set; }
    }
}
