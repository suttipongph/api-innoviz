using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BuyerAgreementLineListView : ViewCompanyBaseEntity
	{
		public string BuyerAgreementLineGUID { get; set; }
		public int Period { get; set; }
		public string Description { get; set; }
		public decimal Amount { get; set; }
		public string BuyerAgreementTableGUID { get; set; }

	}
}
