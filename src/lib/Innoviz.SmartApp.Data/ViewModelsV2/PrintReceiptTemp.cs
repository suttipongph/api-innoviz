﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
     public  class PrintReceiptTemp
    {
        public string ReceiptTempTableGUID { get; set; }
        public string ReceiptTempId { get; set; }
        public string ReceiptDate { get; set; }
        public string TransDate { get; set; }
        public int ProductType { get; set; }
        public int ReceivedFrom { get; set; }
        public string CustomerId { get; set; }
        public string BuyerId { get; set; }
        public decimal ReceiptAmount { get; set; }
        public string SuspenseInvoiceTypeGUID { get; set; }
        public decimal SettleFeeAmount { get; set; }
        public decimal SettleAmount { get; set; }
        public decimal SuspenseAmount { get; set; }
        public string CreditAppTableGUID { get; set; }
        public string SuspenseInvoiceType_Values { get; set; }
        public string CreditAppTable_Values { get; set; }

    }
    public class PrintReceiptTempReportView : ViewReportBaseEntity
    {

        public string ReceiptTempTableGUID { get; set; }
        public string ReceiptTempId { get; set; }
        public string ReceiptDate { get; set; }
        public string TransDate { get; set; }
        public string ProductType { get; set; }
        public string ReceivedFrom { get; set; }
        public string CustomerId { get; set; }
        public string BuyerId { get; set; }
        public string ReceiptAmount { get; set; }
        public string SuspenseInvoiceTypeGUID { get; set; }
        public string SettleFeeAmount { get; set; }
        public string SettleAmount { get; set; }
        public string SuspenseAmount { get; set; }
        public string CreditAppTableGUID { get; set; }
        public string SuspenseInvoiceType_Values { get; set; }
        public string CreditAppTable_Values { get; set; }
    }
}

