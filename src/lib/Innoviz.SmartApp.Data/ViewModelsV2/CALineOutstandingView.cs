﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CALineOutstandingView : ViewCompanyBaseEntity
	{
		public string CreditAppTableGUID { get; set; }
		public string CreditAppId { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string CreditLimitTypeGUID { get; set; }
		public string CreditLimitTypeId { get; set; }
		public string CreditLimitType_Values { get; set; }
		public string CustomerTableGUID { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CustomerId { get; set; }
		public string BuyerTableGUID { get; set; }
		public string BuyerTable_Values { get; set; }
		public string BuyerId { get; set; }
		public int LineNum { get; set; }
		public int ProductType { get; set; }
		public decimal ApprovedCreditLimitLine { get; set; }
		public decimal CreditLimitBalance { get; set; }
		public decimal ARBalance { get; set; }
		public string StartDate { get; set; }
		public string ExpiryDate { get; set; }
		public string StartDate_Values { get; set; }
		public string ExpiryDate_Values { get; set; }
		public decimal CreditDeductAmount { get; set; }
		public string CreditAppLineGUID { get; set; }
		public decimal MaxPurchasePct { get; set; }
		public string AssignmentMethodGUID { get; set; }
		public string BillingResponsibleByGUID { get; set; }
		public string MethodOfPaymentGUID { get; set; }
		public string Status { get; set; }
		public string CreditAppRequestTableGUID { get; set; }
		public int RefType { get; set; }
	}
}
