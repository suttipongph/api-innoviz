using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class IntercompanyInvoiceSettlementItemView : ViewCompanyBaseEntity
	{
		public string IntercompanyInvoiceSettlementGUID { get; set; }
		public string CNReasonGUID { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string IntercompanyInvoiceSettlementId { get; set; }
		public string IntercompanyInvoiceTableGUID { get; set; }
		public string MethodOfPaymentGUID { get; set; }
		public decimal OrigInvoiceAmount { get; set; }
		public string OrigInvoiceId { get; set; }
		public decimal OrigTaxInvoiceAmount { get; set; }
		public string OrigTaxInvoiceId { get; set; }
		public string RefIntercompanyInvoiceSettlementGUID { get; set; }
		public decimal SettleAmount { get; set; }
		public decimal SettleInvoiceAmount { get; set; }
		public decimal SettleWHTAmount { get; set; }
		
	
		public string TransDate { get; set; }
		public bool WHTSlipReceivedByCustomer { get; set; }
		public string IntercompanyInvoice_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public string IntercomapnyInvoice_IssuedDate { get; set; }
		public decimal IntercomapnyInvoice_InvoiceAmount { get; set; }
		public string CNReason_Values { get; set; }
		public string RefIntercompanyInvoiceSettlement_IntercompanyInvoiceSettlementId { get; set; }

	}
}
