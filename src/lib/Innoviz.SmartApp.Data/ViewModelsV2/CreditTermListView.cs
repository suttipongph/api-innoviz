using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CreditTermListView : ViewCompanyBaseEntity
	{
		public string CreditTermGUID { get; set; }
		public string CreditTermId { get; set; }
		public string Description { get; set; }
		public int NumberOfDays { get; set; }
	}
}
