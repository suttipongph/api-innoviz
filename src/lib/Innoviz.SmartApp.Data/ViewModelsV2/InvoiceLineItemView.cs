using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class InvoiceLineItemView : ViewBranchCompanyBaseEntity
	{
		public string InvoiceLineGUID { get; set; }
		public string Dimension1GUID { get; set; }
		public string Dimension2GUID { get; set; }
		public string Dimension3GUID { get; set; }
		public string Dimension4GUID { get; set; }
		public string Dimension5GUID { get; set; }
		public string InvoiceRevenueTypeGUID { get; set; }
		public string InvoiceTableGUID { get; set; }
		public string InvoiceText { get; set; }
		public int LineNum { get; set; }
		public string ProdUnitGUID { get; set; }
		public decimal Qty { get; set; }
		public decimal TaxAmount { get; set; }
		public string TaxTableGUID { get; set; }
		public decimal TotalAmount { get; set; }
		public decimal TotalAmountBeforeTax { get; set; }
		public decimal UnitPrice { get; set; }
		public decimal WHTAmount { get; set; }
		public decimal WHTBaseAmount { get; set; }
		public string WithholdingTaxTableGUID { get; set; }
		public string TaxTable_PaymentTaxTableGUID { get; set; }
	}
}
