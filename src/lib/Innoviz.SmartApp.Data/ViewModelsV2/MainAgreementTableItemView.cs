using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class MainAgreementTableItemView : ViewCompanyBaseEntity
	{
		public string MainAgreementTableGUID { get; set; }
		public string AgreementDate { get; set; }
		public int AgreementDocType { get; set; }
		public int Agreementextension { get; set; }
		public int AgreementYear { get; set; }
		public decimal ApprovedCreditLimit { get; set; }
		public decimal ApprovedCreditLimitLine { get; set; }
		public string BuyerAgreementDescription { get; set; }
		public string BuyerAgreementReferenceId { get; set; }
		public string BuyerName { get; set; }
		public string BuyerTableGUID { get; set; }
		public string ConsortiumTableGUID { get; set; }
		public string CreditAppRequestTableGUID { get; set; }
		public string CreditAppTableGUID { get; set; }
		public string CreditLimitTypeGUID { get; set; }
		public string CustomerAltName { get; set; }
		public string CustomerName { get; set; }
		public string CustomerTableGUID { get; set; }
		public string Description { get; set; }
		public string DocumentReasonGUID { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string ExpiryDate { get; set; }
		public string InternalMainAgreementId { get; set; }
		public string MainAgreementId { get; set; }
		public decimal MaxInterestPct { get; set; }
		public int ProductType { get; set; }
		public string RefMainAgreementId { get; set; }
		public string RefMainAgreementTableGUID { get; set; }
		public string Remark { get; set; }
		public string SigningDate { get; set; }
		public string StartDate { get; set; }
		public decimal TotalInterestPct { get; set; }
		public string WithdrawalTable_DueDate { get; set; }
		public string WithdrawalTableGUID { get; set; }
		public string CustomerTable_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string CreditLimitType_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string CreditLimitType_CreditLimitTypeId { get; set; }
		public string CreditAppRequestTable_Values { get; set; }
		public string ConsortiumTable_Values { get; set; }
		public string MainAgreementTable_Values { get; set; }
		public bool? Revolving { get; set; }
		public int? ServiceFeeCategory { get; set; }
		public string DocumentReason_Values { get; set; }
		public bool CreditLimitType_Revolving { get; set; }
	}
}
