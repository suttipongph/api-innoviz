using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CollectionFollowUpListView : ViewCompanyBaseEntity
	{
		public string CollectionFollowUpGUID { get; set; }
		public string CollectionDate { get; set; }
		public string Description { get; set; }
		public int ReceivedFrom { get; set; }
		public string MethodOfPaymentGUID { get; set; }
		public decimal CollectionAmount { get; set; }
		public int CollectionFollowUpResult { get; set; }
		public string NewCollectionDate { get; set; }
		public string CustomerTableGUID { get; set; }
		public string BuyerTableGUID { get; set; }
		public string CustomerTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string MethodOfPayment_MethodOfPaymentId { get; set; }
		public string RefGUID { get; set; }
	}
}
