using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class RegistrationTypeListView : ViewCompanyBaseEntity
	{
		public string RegistrationTypeGUID { get; set; }
		public string RegistrationTypeId { get; set; }
		public string Description { get; set; }
	}
}
