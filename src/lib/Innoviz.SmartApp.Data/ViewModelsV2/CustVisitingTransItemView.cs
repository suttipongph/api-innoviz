using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CustVisitingTransItemView : ViewCompanyBaseEntity
	{
		public string CustVisitingTransGUID { get; set; }
		public string CustVisitingDate { get; set; }
		public string CustVisitingMemo { get; set; }
		public string Description { get; set; }
		public string RefGUID { get; set; }
		public string RefId { get; set; }
		public int RefType { get; set; }
		public string ResponsibleByGUID { get; set; }
	}
}
