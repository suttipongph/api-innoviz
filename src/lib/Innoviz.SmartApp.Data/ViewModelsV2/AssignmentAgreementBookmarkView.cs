﻿using Innoviz.SmartApp.Core.Attributes;
using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class AssignmentAgreementBookmarkView : ViewCompanyBaseEntity
    {
        public DateTime AssignmentDate { get; set; }
        public DateTime? CustDateRegis { get; set; }
        [BookmarkMapping("AssignmentDate1")]
        public string AssignmentDate1 { get; set; }
        [BookmarkMapping("AssignmentDate2")]
        public string AssignmentDate2 { get; set; }
        [BookmarkMapping("AssignmentDate3")]
        public string AssignmentDate3 { get; set; }
        [BookmarkMapping("AssignmentDate4")]
        public string AssignmentDate4 { get; set; }
        [BookmarkMapping("AssignmentDate5")]
        public string AssignmentDate5 { get; set; }


        [BookmarkMapping("AssignmentManualDate1")]
        public string AssignmentManualDate1 { get; set; }
        [BookmarkMapping("AssignmentManualDate2")]
        public string AssignmentManualDate2 { get; set; }
        [BookmarkMapping("AssignmentManualDate3")]
        public string AssignmentManualDate3 { get; set; }


        [BookmarkMapping("AssignmentManualNo1")]
        public string AssignmentManualNo1 { get; set; }
        [BookmarkMapping("AssignmentManualNo2")]
        public string AssignmentManualNo2 { get; set; }


        [BookmarkMapping("AssignmentMemoManualDate1")]
        public string AssignmentMemoManualDate1 { get; set; }
        [BookmarkMapping("AssignmentMemoManualDate2")]
        public string AssignmentMemoManualDate2 { get; set; }


        [BookmarkMapping("AssignmentMemoManualNo1")]
        public string AssignmentMemoManualNo1 { get; set; }
        [BookmarkMapping("AssignmentMemoManualNo2")]
        public string AssignmentMemoManualNo2 { get; set; }


        [BookmarkMapping("AssignmentNo1")]
        public string AssignmentNo1 { get; set; }
        [BookmarkMapping("AssignmentPositionLIT1")]
        public string AssignmentPositionLIT1 { get; set; }


        [BookmarkMapping("AssignmentTextFirst1")]
        public string AssignmentTextFirst1 { get; set; }
        [BookmarkMapping("AssignmentTextFirst2")]
        public string AssignmentTextFirst2 { get; set; }
        [BookmarkMapping("AssignmentTextFirst3")]
        public string AssignmentTextFirst3 { get; set; }
        [BookmarkMapping("AssignmentTextFirst4")]
        public string AssignmentTextFirst4 { get; set; }


        [BookmarkMapping("AssignmentTextFromMasterBuyer")]
        public string AssignmentTextFromMasterBuyer { get; set; }


        [BookmarkMapping("AuthorityPersonFirst1")]
        public string AuthorityPersonFirst1 { get; set; }
        [BookmarkMapping("AuthorityPersonFirst2")]
        public string AuthorityPersonFirst2 { get; set; }
        [BookmarkMapping("AuthorityPersonFirst3")]
        public string AuthorityPersonFirst3 { get; set; }
        [BookmarkMapping("AuthorityPersonFirst4")]
        public string AuthorityPersonFirst4 { get; set; }
        [BookmarkMapping("AuthorityPersonFirst5")]
        public string AuthorityPersonFirst5 { get; set; }


        [BookmarkMapping("AuthorityPersonSecond1")]
        public string AuthorityPersonSecond1 { get; set; }
        [BookmarkMapping("AuthorityPersonSecond2")]
        public string AuthorityPersonSecond2 { get; set; }
        [BookmarkMapping("AuthorityPersonSecond3")]
        public string AuthorityPersonSecond3 { get; set; }
        [BookmarkMapping("AuthorityPersonSecond4")]
        public string AuthorityPersonSecond4 { get; set; }
        [BookmarkMapping("AuthorityPersonSecond5")]
        public string AuthorityPersonSecond5 { get; set; }


        [BookmarkMapping("AuthorityPersonThird1")]
        public string AuthorityPersonThird1 { get; set; }
        [BookmarkMapping("AuthorityPersonThird2")]
        public string AuthorityPersonThird2 { get; set; }
        [BookmarkMapping("AuthorityPersonThird3")]
        public string AuthorityPersonThird3 { get; set; }
        [BookmarkMapping("AuthorityPersonThird4")]
        public string AuthorityPersonThird4 { get; set; }
        [BookmarkMapping("AuthorityPersonThird5")]
        public string AuthorityPersonThird5 { get; set; }


        [BookmarkMapping("AuthorizedCustFirst1")]
        public string AuthorizedCustFirst1 { get; set; }
        [BookmarkMapping("AuthorizedCustFirst2")]
        public string AuthorizedCustFirst2 { get; set; }
        [BookmarkMapping("AuthorizedCustFirst3")]
        public string AuthorizedCustFirst3 { get; set; }
        [BookmarkMapping("AuthorizedCustFirst4")]
        public string AuthorizedCustFirst4 { get; set; }
        [BookmarkMapping("AuthorizedCustFirst5")]
        public string AuthorizedCustFirst5 { get; set; }
        [BookmarkMapping("AuthorizedCustFirst6")]
        public string AuthorizedCustFirst6 { get; set; }
        [BookmarkMapping("AuthorizedCustFirst7")]
        public string AuthorizedCustFirst7 { get; set; }
        [BookmarkMapping("AuthorizedCustFirst8")]
        public string AuthorizedCustFirst8 { get; set; }
        [BookmarkMapping("AuthorizedCustFirst9")]
        public string AuthorizedCustFirst9 { get; set; }
        [BookmarkMapping("AuthorizedCustFirst10")]
        public string AuthorizedCustFirst10 { get; set; }


        [BookmarkMapping("AuthorizedCustSecond1")]
        public string AuthorizedCustSecond1 { get; set; }
        [BookmarkMapping("AuthorizedCustSecond2")]
        public string AuthorizedCustSecond2 { get; set; }
        [BookmarkMapping("AuthorizedCustSecond3")]
        public string AuthorizedCustSecond3 { get; set; }
        [BookmarkMapping("AuthorizedCustSecond4")]
        public string AuthorizedCustSecond4 { get; set; }
        [BookmarkMapping("AuthorizedCustSecond5")]
        public string AuthorizedCustSecond5 { get; set; }
        [BookmarkMapping("AuthorizedCustSecond6")]
        public string AuthorizedCustSecond6 { get; set; }
        [BookmarkMapping("AuthorizedCustSecond7")]
        public string AuthorizedCustSecond7 { get; set; }
        [BookmarkMapping("AuthorizedCustSecond8")]
        public string AuthorizedCustSecond8 { get; set; }
        [BookmarkMapping("AuthorizedCustSecond9")]
        public string AuthorizedCustSecond9 { get; set; }
        [BookmarkMapping("AuthorizedCustSecond10")]
        public string AuthorizedCustSecond10 { get; set; }


        [BookmarkMapping("AuthorizedCustThird1")]
        public string AuthorizedCustThird1 { get; set; }
        [BookmarkMapping("AuthorizedCustThird2")]
        public string AuthorizedCustThird2 { get; set; }
        [BookmarkMapping("AuthorizedCustThird3")]
        public string AuthorizedCustThird3 { get; set; }
        [BookmarkMapping("AuthorizedCustThird4")]
        public string AuthorizedCustThird4 { get; set; }
        [BookmarkMapping("AuthorizedCustThird5")]
        public string AuthorizedCustThird5 { get; set; }
        [BookmarkMapping("AuthorizedCustThird6")]
        public string AuthorizedCustThird6 { get; set; }
        [BookmarkMapping("AuthorizedCustThird7")]
        public string AuthorizedCustThird7 { get; set; }
        [BookmarkMapping("AuthorizedCustThird8")]
        public string AuthorizedCustThird8 { get; set; }
        [BookmarkMapping("AuthorizedCustThird9")]
        public string AuthorizedCustThird9 { get; set; }
        [BookmarkMapping("AuthorizedCustThird10")]
        public string AuthorizedCustThird10 { get; set; }
        
        [BookmarkMapping("CustAddress1")]
        public string CustAddress1 { get; set; }


        [BookmarkMapping("CustBankBranch1")]
        public string CustBankBranch1 { get; set; }
        [BookmarkMapping("CustBankBranch2")]
        public string CustBankBranch2 { get; set; }
        [BookmarkMapping("CustBankBranch3")]
        public string CustBankBranch3 { get; set; }


        [BookmarkMapping("CustBankName1")]
        public string CustBankName1 { get; set; }
        [BookmarkMapping("CustBankName2")]
        public string CustBankName2 { get; set; }
        [BookmarkMapping("CustBankName3")]
        public string CustBankName3 { get; set; }


        [BookmarkMapping("CustBankNo1")]
        public string CustBankNo1 { get; set; }
        [BookmarkMapping("CustBankNo2")]
        public string CustBankNo2 { get; set; }


        [BookmarkMapping("CustBankType1")]
        public string CustBankType1 { get; set; }
        [BookmarkMapping("CustDateRegis1")]
        public string CustDateRegis1 { get; set; }


        [BookmarkMapping("CustName1")]
        public string CustName1 { get; set; }
        [BookmarkMapping("CustName2")]
        public string CustName2 { get; set; }
        [BookmarkMapping("CustName3")]
        public string CustName3 { get; set; }
        [BookmarkMapping("CustName4")]
        public string CustName4 { get; set; }
        [BookmarkMapping("CustName5")]
        public string CustName5 { get; set; }
        [BookmarkMapping("CustName6")]
        public string CustName6 { get; set; }
        [BookmarkMapping("CustName7")]
        public string CustName7 { get; set; }
        [BookmarkMapping("CustName8")]
        public string CustName8 { get; set; }
        [BookmarkMapping("CustName9")]
        public string CustName9 { get; set; }
        [BookmarkMapping("CustName10")]
        public string CustName10 { get; set; }
        [BookmarkMapping("CustName11")]
        public string CustName11 { get; set; }
        [BookmarkMapping("CustName12")]
        public string CustName12 { get; set; }
        [BookmarkMapping("CustName13")]
        public string CustName13 { get; set; }
        [BookmarkMapping("CustName14")]
        public string CustName14 { get; set; }


        [BookmarkMapping("CustTaxID1")]
        public string CustTaxID1 { get; set; }
        [BookmarkMapping("FaxNumber1")]
        public string FaxNumber1 { get; set; }
        [BookmarkMapping("InterestRate1")]
        public string InterestRate1 { get; set; }


        [BookmarkMapping("LSAssignmentTextDetail1")]
        public string LSAssignmentTextDetail1 { get; set; }
        [BookmarkMapping("LSAssignmentTextNoFirst1")]
        public string LSAssignmentTextNoFirst1 { get; set; }
        [BookmarkMapping("LSAssignmentTextNoFirst2")]
        public string LSAssignmentTextNoFirst2 { get; set; }


        [BookmarkMapping("PositionCust1")]
        public string PositionCust1 { get; set; }
        [BookmarkMapping("PositionCust2")]
        public string PositionCust2 { get; set; }


        [BookmarkMapping("PositionLIT1")]
        public string PositionLIT1 { get; set; }
        [BookmarkMapping("PositionLIT2")]
        public string PositionLIT2 { get; set; }
        [BookmarkMapping("PositionLIT3")]
        public string PositionLIT3 { get; set; }
        [BookmarkMapping("PositionLIT4")]
        public string PositionLIT4 { get; set; }
        [BookmarkMapping("PositionLIT5")]
        public string PositionLIT5 { get; set; }


        [BookmarkMapping("ReceivedName1")]
        public string ReceivedName1 { get; set; }
        [BookmarkMapping("ReceivedName2")]
        public string ReceivedName2 { get; set; }
        [BookmarkMapping("ReceivedName3")]
        public string ReceivedName3 { get; set; }
        [BookmarkMapping("ReceivedName4")]
        public string ReceivedName4 { get; set; }
        [BookmarkMapping("ReceivedName5")]
        public string ReceivedName5 { get; set; }


        [BookmarkMapping("Dear1")]
        public string Dear1 { get; set; }
        [BookmarkMapping("Dear2")]
        public string Dear2 { get; set; }
        [BookmarkMapping("Dear3")]
        public string Dear3 { get; set; }
        [BookmarkMapping("Dear4")]
        public string Dear4 { get; set; }
        [BookmarkMapping("Dear5")]
        public string Dear5 { get; set; }


        [BookmarkMapping("TelNumber1")]
        public string TelNumber1 { get; set; }
        [BookmarkMapping("TextAuthorizationDate1")]
        public string TextAuthorizationDate1 { get; set; }
        [BookmarkMapping("TextProvinceRegis1")]
        public string TextProvinceRegis1 { get; set; }


        [BookmarkMapping("WitnessFirst1")]
        public string WitnessFirst1 { get; set; }
        [BookmarkMapping("WitnessSecond1")]
        public string WitnessSecond1 { get; set; }
        [BookmarkMapping("AssignmentTextSecond1")]
        public string AssignmentTextSecond1 { get; set; }
        [BookmarkMapping("AssignmentTextSecond2")]
        public string AssignmentTextSecond2 { get; set; }
        [BookmarkMapping("AssignmentTextSecond3")]
        public string AssignmentTextSecond3 { get; set; }
        [BookmarkMapping("AssignmentTextThird1")]
        public string AssignmentTextThird1 { get; set; }
        [BookmarkMapping("AssignmentTextThird2")]
        public string AssignmentTextThird2 { get; set; }
        [BookmarkMapping("AssignmentTextThird3")]
        public string AssignmentTextThird3 { get; set; }
        [BookmarkMapping("AssignmentTextFour1")]
        public string AssignmentTextFour1 { get; set; }
        [BookmarkMapping("AssignmentTextFour2")]
        public string AssignmentTextFour2 { get; set; }
        [BookmarkMapping("AssignmentTextFour3")]
        public string AssignmentTextFour3 { get; set; }
    }
}
