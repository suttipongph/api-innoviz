using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class JobChequeListView : ViewCompanyBaseEntity
	{
		public string JobChequeGUID { get; set; }
		public int ChequeSource { get; set; }
		public string CollectionFollowUpGUID { get; set; }
		public string ChequeTableGUID { get; set; }
		public string ChequeBankGroupGUID { get; set; }
		public string ChequeBranch { get; set; }
		public string ChequeDate { get; set; }
		public string ChequeNo { get; set; }
		public decimal Amount { get; set; }
		public string CollectionFollowUp_Values { get; set; }
		public string ChequeTable_Values { get; set; }
		public string BankGroup_Values { get; set; }
		public string MessengerJobTableGUID { get; set; }
	}
}
