﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class GetCreditApplicationRequestEmailContentK2ResultView
    {
		public Guid CreditAppRequestTableGUID { get; set; }
		public int CreditAppRequestType { get; set; }
		public Guid InterestTypeGUID { get; set; }
		public DateTime? RequestDate { get; set; }
		public string ParmCustomerName { get; set; }
		public string ParmCreditRequestFeeAmount { get; set; }
		public string ParmMaxRetentionAmount { get; set; }
		public string ParmProductTypeDescription { get; set; }
		public string ParmApprovedCreditLimitRequest { get; set; }
		public string ParmAuthorizedPersonFirst { get; set; }
		public string ParmAuthorizedPersonName { get; set; }
		public string ParmGuarantorName { get; set; }
		public string ParmTotalInterestPct { get; set; }
		public decimal InterestAdjustmentPct { get; set; }
		public int ParmNumberOfCreditAppRequestLine { get; set; }
		public string ParmBuyerNameCreditLimit { get; set; }
		public string ParmOriginalCreditLimit { get; set; }
		public Decimal OriginalCreditLimit { get; set; }
		public string ParmCreditLimitChange { get; set; }
		public string ParmCreditRequestFeeAmountChange { get; set; }
		public string ParmAmendRate { get; set; }
		public string ParmAmendAuthorizedPerson { get; set; }
		public string ParmAmendGuarantor { get; set; }
		public string ParmOriginalMaxRetentionAmount { get; set; }
		public string ParmOriginalTotalInterestPct { get; set; }
		public string ParmCreditRequestFeePctChange { get; set; }
		public string ParmMaxRetentionAmountChange { get; set; }
		public string ParmTotalIntestPctChange { get; set; }
		public string ParmOriginalAuthorizedPersonName { get; set; }
		public string ParmOriginalGuarantorName { get; set; }
		public int OrderingAuthorizedPersonName { get; set; }
		public int OrderingGuarantorName { get; set; }
		public int OrderingOriginalAuthorizedPersonName { get; set; }
		public int OrderingOriginalGuarantorName { get; set; }
		public int LineNumCreditAppRequestLine { get; set; }
		public string ParmMaxPurchasePct { get; set; }
		public string ParmAssignmentMethod { get; set; }
		public string ParmBillingResponsibleBy { get; set; }
		public string ParmMethodOfPayment { get; set; }
		public string ParmBuyerName { get; set; }
		public string ParmMainAgreementId { get; set; }
		public string ParmMainAgreementDate { get; set; }
		public DateTime? MainAgreementDate { get; set; }
		public string ParmMarketingEmpName { get; set; }
		public string ParmMarketingEmpPosition { get; set; }
		public string ParmMarketingEmpPhone { get; set; }
		public Guid? RefCreditAppTableGUID { get; set; }
		public string ParmOriginalCreditRequestFeeAmount { get; set; }
	}
}
