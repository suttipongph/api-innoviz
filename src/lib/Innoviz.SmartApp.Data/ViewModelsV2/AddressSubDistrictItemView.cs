using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class AddressSubDistrictItemView : ViewCompanyBaseEntity
	{
		public string AddressSubDistrictGUID { get; set; }
		public string AddressDistrictGUID { get; set; }
		public string addressProvinceGUID { get; set; }
		public string Name { get; set; }
		public string SubDistrictId { get; set; }
		public string addressDistrict_DistrictId { get; set; }
		public string addressDistrict_Name { get; set; }
		public string addressProvince_ProvinceId { get; set; }
	}
}
