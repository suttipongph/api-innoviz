using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ServiceFeeCondTemplateLineListView : ViewCompanyBaseEntity
	{
		public string ServiceFeeCondTemplateLineGUID { get; set; }
		public int Ordering { get; set; }
		public string InvoiceRevenueTypeGUID { get; set; }
		public string Description { get; set; }
		public decimal AmountBeforeTax { get; set; }
		public string InvoiceRevenueType_Values { get; set; }
		public string ServiceFeeCondTemplateTableGUID { get; set; }
	}
}
