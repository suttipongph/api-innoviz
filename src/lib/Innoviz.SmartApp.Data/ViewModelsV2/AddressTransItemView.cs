using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class AddressTransItemView : ViewCompanyBaseEntity
	{
		public string AddressTransGUID { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string AddressCountryGUID { get; set; }
		public string AddressDistrictGUID { get; set; }
		public string AddressPostalCodeGUID { get; set; }
		public string AddressProvinceGUID { get; set; }
		public string AddressSubDistrictGUID { get; set; }
		public bool CurrentAddress { get; set; }
		public bool IsTax { get; set; }
		public string Name { get; set; }
		public string OwnershipGUID { get; set; }
		public bool Primary { get; set; }
		public string PropertyTypeGUID { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public string TaxBranchId { get; set; }
		public string TaxBranchName { get; set; }
		public string RefId { get; set; }
		public string AddressSubDistrict_Name { get; set; }
		public string AddressDistrict_Name { get; set; }
		public string AddressProvince_Name { get; set; }
		public string AddressCountry_Name { get; set; }
		public string AddressPostalCode_PostalCode { get; set; }
		public string UnboundAddress { get; set; }
		public string AltAddress { get; set; }

	}
}
