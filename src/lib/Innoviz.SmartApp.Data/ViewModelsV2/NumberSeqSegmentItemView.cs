using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class NumberSeqSegmentItemView : ViewCompanyBaseEntity
	{
		public string NumberSeqSegmentGUID { get; set; }
		public string NumberSeqTableGUID { get; set; }
		public int Ordering { get; set; }
		public int SegmentType { get; set; }
		public string SegmentValue { get; set; }
		public string NumberSeqTable_Values { get; set; }
		
	}
}
