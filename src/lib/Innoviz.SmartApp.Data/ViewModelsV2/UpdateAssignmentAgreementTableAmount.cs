﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class UpdateAssignmentAgreementTableAmountView
    {
        public string DocumentReasonGUID { get; set; }
        public decimal NewAssignmentAgreementAmount { get; set; }
        public string AssignmentAgreementTableGUID { get; set; }
        public string InternalAssignmentAgreementId { get; set; }
        public string Description { get; set; }
    }
    public class UpdateAssignmentAgreementTableAmountResult : ResultBaseEntity
    {
    }
 
}
