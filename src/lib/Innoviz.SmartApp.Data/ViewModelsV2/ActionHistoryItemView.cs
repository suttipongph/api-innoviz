﻿using Innoviz.SmartApp.Core.ViewModels;
using NJsonSchema.Annotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    [JsonSchemaFlatten]
    public class ActionHistoryItemView : ViewCompanyBaseEntity
    {
        public string ActionHistoryGUID { get; set; }
        public string SerialNo { get; set; }
        public string WorkflowName { get; set; }
        public string ActivityName { get; set; }
        public string ActionName { get; set; }
        public string Comment { get; set; }
        public string RefGUID { get; set; }
    }
}
