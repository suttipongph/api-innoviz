using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CreditAppRequestTableAmendItemView : ViewCompanyBaseEntity
	{
		public string CreditAppRequestTableAmendGUID { get; set; }
		public bool AmendAuthorizedPerson { get; set; }
		public bool AmendGuarantor { get; set; }
		public bool AmendRate { get; set; }
		public string CreditAppRequestTableGUID { get; set; }
		public decimal OriginalCreditLimit { get; set; }
		public decimal OriginalCreditLimitRequestFeeAmount { get; set; }
		public decimal OriginalInterestAdjustmentPct { get; set; }
		public string OriginalInterestTypeGUID { get; set; }
		public string OriginalInterestType_Values { get; set; }
		public decimal OriginalMaxPurchasePct { get; set; }
		public decimal OriginalMaxRetentionAmount { get; set; }
		public decimal OriginalMaxRetentionPct { get; set; }
		public int OriginalPurchaseFeeCalculateBase { get; set; }
		public decimal OriginalPurchaseFeePct { get; set; }
		public decimal OriginalTotalInterestPct { get; set; }
		public string CNReasonGUID { get; set; }
		public decimal OrigInvoiceAmount { get; set; }
		public string OrigInvoiceId { get; set; }
		public decimal OrigTaxInvoiceAmount { get; set; }
		public string OrigTaxInvoiceId { get; set; }
		#region Unbound
		public string CustomerTable_Values { get; set; }
		public string CreditAppRequestTable_CustomerTableGUID { get; set; }
		public string RefCreditAppTable_Values { get; set; }
		public string RefCreditAppTableGUID { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string CreditAppRequestTable_CreditAppRequestId { get; set; }
		public string CreditAppRequestTable_Description { get; set; }
		public int CreditAppRequestTable_CreditAppRequestType { get; set; }
		public string CreditAppRequestTable_RequestDate { get; set; }
		public string CreditAppRequestTable_DocumentStatusGUID { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public decimal CustomerTable_CustomerId { get; set; }
		public decimal CreditAppRequestTable_CustomerCreditLimit { get; set; }
		public string CreditAppRequestTable_StartDate { get; set; }
		public string CreditAppRequestTable_ExpiryDate { get; set; }
		public string CreditAppRequestTable_RefCreditAppId { get; set; }
		public string CreditAppRequestTable_Remark { get; set; }
		public decimal CreditAppRequestTable_NewCreditLimit { get; set; }
		public decimal CreditAppRequestTable_CreditLimitRequestFeePct { get; set; }
		public decimal CreditAppRequestTable_CreditLimitRequestFeeAmount { get; set; }
		public decimal CreditAppRequestTable_MaxRetentionPct { get; set; }
		public decimal CreditAppRequestTable_MaxRetentionAmount { get; set; }
		public string CreditAppRequestTable_NewInterestTypeGUID { get; set; }
		public decimal CreditAppRequestTable_NewInterestAdjustmentPct { get; set; }
		public decimal CreditAppRequestTable_NewTotalInterestPct { get; set; }
		public decimal CreditAppRequestTable_NewCreditLimitRequestFeePct { get; set; }
		public decimal CreditAppRequestTable_NewMaxRetentionPct { get; set; }
		public decimal CreditAppRequestTable_NewMaxRetentionAmount { get; set; }
		public decimal CreditAppRequestTable_NewMaxPurchasePct { get; set; }
		public decimal CreditAppRequestTable_NewPurchaseFeePct { get; set; }
		public int CreditAppRequestTable_NewPurchaseFeeCalculateBase { get; set; }
		public string CreditAppRequestTable_ApprovedDate { get; set; }
		public string CreditAppRequestTable_MarketingComment { get; set; }
		public string CreditAppRequestTable_CreditComment { get; set; }
		public string CreditAppRequestTable_ApproverComment { get; set; }
		public decimal CreditAppRequestTable_ApprovedCreditLimitRequest { get; set; }
		public string CreditAppRequestTable_CACondition { get; set; }
		public string CreditAppRequestTable_SigningCondition { get; set; }
		public string CreditAppRequestTable_FinancialCreditCheckedDate { get; set; }
		public string CreditAppRequestTable_FinancialCheckedBy { get; set; }
		public string CreditAppRequestTable_NCBCheckedDate { get; set; }
		public string CreditAppRequestTable_NCBCheckedBy { get; set; }
		public string CreditAppRequestTable_BlacklistStatusGUID { get; set; }
		public string CreditAppRequestTable_KYCGUID { get; set; }
		public string CreditAppRequestTable_CreditScoringGUID { get; set; }
		public decimal CreditAppRequestTable_NewCreditLimitRequestFeeAmount { get; set; }
		public string CreditAppTable_InactiveDate { get; set; }

		#endregion
		public int ProcessInstanceId { get; set; }
	}
}
