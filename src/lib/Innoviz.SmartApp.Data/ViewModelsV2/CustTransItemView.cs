using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CustTransItemView : ViewBranchCompanyBaseEntity
	{
		public string CustTransGUID { get; set; }
		public string BuyerTableGUID { get; set; }
		public string CreditAppTableGUID { get; set; }
		public string CurrencyGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public int CustTransStatus { get; set; }
		public string Description { get; set; }
		public string DueDate { get; set; }
		public decimal ExchangeRate { get; set; }
		public string InvoiceTableGUID { get; set; }
		public string InvoiceTypeGUID { get; set; }
		public string LastSettleDate { get; set; }
		public int ProductType { get; set; }
		public decimal SettleAmount { get; set; }
		public decimal SettleAmountMST { get; set; }
		public decimal TransAmount { get; set; }
		public decimal TransAmountMST { get; set; }
		public string TransDate { get; set; }
		public string InvoiceTable_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string InvoiceType_Values { get; set; }
		public string Currency_Values { get; set; }
		public string CreditApp_Value { get; set; }
		public string CreditAppLineGUID { get; set; }
		public string CreditAppLine_Value { get; set; }
	}
}
