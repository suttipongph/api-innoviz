using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BookmarkDocumentListView : ViewCompanyBaseEntity
	{
		public string BookmarkDocumentGUID { get; set; }
		public string BookmarkDocumentId { get; set; }
		public string Description { get; set; }
		public int DocumentTemplateType { get; set; }
		public int BookmarkDocumentRefType { get; set; }
	}
}
