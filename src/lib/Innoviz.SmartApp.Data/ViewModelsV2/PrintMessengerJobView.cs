﻿using System;
using System.Collections.Generic;
using System.Text;
using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class PrintMessengerJobView
    {
        public string MessengerJobTableGUID { get; set; }
        public string MessengerJobTable_Values { get; set; }
        public string DocumentStatusGUID { get; set; }
        public string DocumentStatus_Values { get; set; }
        public string JobDate { get; set; }
        public string JobTime { get; set; }
        public string JobTypeGUID { get; set; }
        public string JobType_Values { get; set; }
        public string MessengerTableGUID { get; set; }
        public string MessengerTable_Values { get; set; }
        public string JobDetail { get; set; }
        public int ContactTo { get; set; }
        public string ContactPersonName { get; set; }
        public string JobEndTime { get; set; }
        
    }
    public class PrintMessengerJobReportView : ViewReportBaseEntity
    {
        public string MessengerJobTableGUID { get; set; }
        public string DocumentStatusGUID { get; set; }
        public string DocumentStatus_Values { get; set; }
        public string JobDate { get; set; }
        public string JobTime { get; set; }
        public string JobTypeGUID { get; set; }
        public string JobType_Values { get; set; }
        public string MessengerTableGUID { get; set; }
        public string MessengerTable_Values { get; set; }
        public string JobDetail { get; set; }
        public int ContactTo { get; set; }
        public string ContactPersonName { get; set; }
    }
}

