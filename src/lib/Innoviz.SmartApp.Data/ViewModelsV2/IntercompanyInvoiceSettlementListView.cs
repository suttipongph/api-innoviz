using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class IntercompanyInvoiceSettlementListView : ViewCompanyBaseEntity
	{
		public string IntercompanyInvoiceSettlementGUID { get; set; }
		public string TransDate { get; set; }
		public string DocumentStatusGUID { get; set; }
		public bool WHTSlipReceivedByCustomer { get; set; }
		public decimal SettleAmount { get; set; }
		public decimal SettleWHTAmount { get; set; }
		public decimal SettleInvoiceAmount { get; set; }
	
		
		public string DocumentStatus_Values { get; set; }
		public string IntercompanyInvoiceTableGUID { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string IntercompanyInvoiceSettlementId { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public string MethodOfPaymentGUID { get; set; }
		public string MethodOfPayment_MethodOfPaymentId { get; set; }
		
	}
}
