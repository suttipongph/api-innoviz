using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class GradeClassificationItemView : ViewCompanyBaseEntity
	{
		public string GradeClassificationGUID { get; set; }
		public string Description { get; set; }
		public string GradeClassificationId { get; set; }
	}
}
