using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class WithdrawalLineListView : ViewCompanyBaseEntity
	{
		public string WithdrawalLineGUID { get; set; }
		public int WithdrawalLineType { get; set; }
		public string StartDate { get; set; }
		public string DueDate { get; set; }
		public string InterestDate { get; set; }
		public int InterestDay { get; set; }
		public decimal InterestAmount { get; set; }
		public string WithdrawalTableGUID { get; set; }
		public int LineNum { get; set; }
	}
}
