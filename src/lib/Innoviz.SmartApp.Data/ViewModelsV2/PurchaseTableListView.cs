using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class PurchaseTableListView : ViewCompanyBaseEntity
	{
		public string PurchaseTableGUID { get; set; }
		public string PurchaseId { get; set; }
		public string Description { get; set; }
		public string CustomerTableGUID { get; set; }
		public string PurchaseDate { get; set; }
		public string CreditAppTableGUID { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public bool RollBill { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string DocumentStatus_StatusId { get; set; }
	}
}
