﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class SysFeatureGroupRoleView : ViewBaseEntity
    {
        public string SysFeatureGroupRoleGUID { get; set; }
        public string SysFeatureGroupGUID { get; set; }
        public string GroupId { get; set; }
        public int Create { get; set; }
        public int Read { get; set; }
        public int Update { get; set; }
        public int Delete { get; set; }
        public int Action { get; set; }
        public string RoleGUID { get; set; }
        public string SysRoleTable_CompanyGUID { get; set; }
        public string SysRoleTable_DisplayName { get; set; }
        public int SysRoleTable_SiteLoginType { get; set; }
        public int SysFeatureGroup_FeatureType { get; set; }
    }
}
