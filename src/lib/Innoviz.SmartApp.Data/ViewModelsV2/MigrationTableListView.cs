﻿using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class MigrationTableListView : ViewBaseEntity
    {
        public string MigrationTableGUID { get; set; }
        public string GroupName { get; set; }
        public int GroupOrder { get; set; }
        public string MigrationName { get; set; }
        public int MigrationOrder { get; set; }
        public string MigrationResult { get; set; }
        public string PackageName { get; set; }
    }
}
