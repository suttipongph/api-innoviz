using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class TaxInvoiceLineItemView : ViewBranchCompanyBaseEntity
	{
		public string TaxInvoiceLineGUID { get; set; }
		public string Dimension1GUID { get; set; }
		public string Dimension2GUID { get; set; }
		public string Dimension3GUID { get; set; }
		public string Dimension4GUID { get; set; }
		public string Dimension5GUID { get; set; }
		public string InvoiceRevenueTypeGUID { get; set; }
		public string InvoiceText { get; set; }
		public int LineNum { get; set; }
		public decimal Qty { get; set; }
		public decimal TaxAmount { get; set; }
		public string TaxInvoiceTableGUID { get; set; }
		public string TaxTableGUID { get; set; }
		public decimal TotalAmount { get; set; }
		public decimal TotalAmountBeforeTax { get; set; }
		public decimal UnitPrice { get; set; }
		public string TaxInvoiceTable_Values { get; set; }
		public string InvoiceRevenueType_Values { get; set; }
		public string ProdUnit_Values { get; set; }
		public string TaxTable_Values { get; set; }
		public string Dimension1_Values { get; set; }
		public string Dimension2_Values { get; set; }
		public string Dimension3_Values { get; set; }
		public string Dimension4_Values { get; set; }
		public string Dimension5_Values { get; set; }
		public string ProdUnitGUID { get; set; }
	}
}
