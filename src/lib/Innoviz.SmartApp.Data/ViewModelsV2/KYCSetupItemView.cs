using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class KYCSetupItemView : ViewCompanyBaseEntity
	{
		public string KYCSetupGUID { get; set; }
		public string Description { get; set; }
		public string KYCId { get; set; }
	}
}
