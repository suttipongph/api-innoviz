﻿using Innoviz.SmartApp.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
 public class BookmarkDocumentQueryCreditApplicationPF
    {
        [BookmarkMapping("CreditTermNumberOfDay")]
        public string QCreditAppRequestTableCAPF_NumberOfDays { get; set; }
        public string QCreditAppRequestTableCAPF_CreditTermId { get; set; }
        public string QCreditAppRequestTableCAPF_CreditAppRequestId { get; set; }
        public decimal QCreditAppRequestTableCAPF_SalesAvgPerMonth { get; set; }
        [BookmarkMapping("CreditReq")]
        public decimal QCreditAppRequestTableCAPF_CreditLimitRequest { get; set; }
        ////
        
        public Guid? QCreditAppRequestLineCAPF_CreditAppRequestLineGUID { get; set; }
        public Guid? QCreditAppRequestLineCAPF_BuyerTableGUID { get; set; }
        public Guid? QCreditAppRequestLineCAPF_InvoiceAddressGUID { get; set; }

        public decimal QCreditAppRequestLineCAPF_CreditLimitLineRequest { get; set; }
        public string QCreditAppRequestLineCAPF_InvoiceAddress { get; set; }
        public string QCreditAppRequestLineCAPF_CreditScoring { get; set; }
        public string QCreditAppRequestLineCAPF_BlacklistStatus { get; set; }


        /// 
        [BookmarkMapping("BuyName")]
        public string QBuyerTableCAPF_BuyerName { get; set; }
        [BookmarkMapping("BuyID")]
        public string QBuyerTableCAPF_BuyerId { get; set; }
        public string QBuyerTableCAPF_TaxId { get; set; }
        public DateTime? QCreditAppRequestLineCAPF_DateOfEstablish { get; set; }

        public string QBuyerTableCAPF_BusinessSegmentDesc { get; set; }
        public string QBuyerTableCAPF_LineOfBusinessDesc { get; set; }

        //
        public Guid? QBuyerAgreementCAPF_BuyerAgreementTableGUID { get; set; }
        [BookmarkMapping("ProjConNum1")]
        public string QBuyerAgreementCAPF_BuyerAgreementId { get; set; }
        [BookmarkMapping("ProjectConName")]
        public string QBuyerAgreementCAPF_BuyerAgreement { get; set; }
        [BookmarkMapping("ProjectConStartDate")]
        public DateTime? QBuyerAgreementCAPF_StartDate { get; set; }
        [BookmarkMapping("ProjectConEndDate")]
        public DateTime? QBuyerAgreementCAPF_EndDate { get; set; }
        [BookmarkMapping("ProjectConDescription")]
        public string QBuyerAgreementCAPF_Remark { get; set; }
        [BookmarkMapping("Penalty")]
        public string QBuyerAgreementCAPF_Penalty { get; set; }
        //
        [BookmarkMapping("ProjConPaymentPeriod1st_1")]
        public int QBuyerAgreementLineCAPF_Period_1 { get; set; }
        [BookmarkMapping("ProjConPaymentDes1st_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_1 { get; set; }
        [BookmarkMapping("ProjConPayementAmt1st_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_1 { get; set; }
        [BookmarkMapping("ProjConDueDate1st_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_1 { get; set; }

        [BookmarkMapping("ProjConPaymentPeriod2nd_1")]
        public int QBuyerAgreementLineCAPF_Period_2 { get; set; }
        [BookmarkMapping("ProjConPaymentDes2nd_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_2 { get; set; }
        [BookmarkMapping("ProjConPayementAmt2nd_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_2 { get; set; }
        [BookmarkMapping("ProjConDueDate2nd_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_2 { get; set; }

        [BookmarkMapping("ProjConPaymentPeriod3rd_1")]
        public int QBuyerAgreementLineCAPF_Period_3 { get; set; }
        [BookmarkMapping("ProjConPaymentDes3rd_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_3 { get; set; }
        [BookmarkMapping("ProjConPayementAmt3rd_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_3 { get; set; }
        [BookmarkMapping("ProjConDueDate3rd_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_3 { get; set; }
        [BookmarkMapping("ProjConPaymentPeriod4th_1")]
        public int QBuyerAgreementLineCAPF_Period_4 { get; set; }
        [BookmarkMapping("ProjConPaymentDes4th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_4 { get; set; }
        [BookmarkMapping("ProjConPayementAmt4th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_4 { get; set; }
        [BookmarkMapping("ProjConDueDate4th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_4 { get; set; }
        [BookmarkMapping("ProjConPaymentPeriod5th_1")]
        public int QBuyerAgreementLineCAPF_Period_5 { get; set; }
        [BookmarkMapping("ProjConPaymentDes5th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_5 { get; set; }
        [BookmarkMapping("ProjConPayementAmt5th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_5 { get; set; }
        [BookmarkMapping("ProjConDueDate5th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_5 { get; set; }
        [BookmarkMapping("ProjConPaymentPeriod6th_1")]
        public int QBuyerAgreementLineCAPF_Period_6 { get; set; }
        [BookmarkMapping("ProjConPaymentDes6th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_6 { get; set; }
        [BookmarkMapping("ProjConPayementAmt6th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_6 { get; set; }
        [BookmarkMapping("ProjConDueDate6th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_6 { get; set; }
        [BookmarkMapping("ProjConPaymentPeriod7th_1")]
        public int QBuyerAgreementLineCAPF_Period_7 { get; set; }
        [BookmarkMapping("ProjConPaymentDes7th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_7 { get; set; }
        [BookmarkMapping("ProjConPayementAmt7th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_7 { get; set; }
        [BookmarkMapping("ProjConDueDate7th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_7 { get; set; }
        [BookmarkMapping("ProjConPaymentPeriod8th_1")]
        public int QBuyerAgreementLineCAPF_Period_8 { get; set; }
        [BookmarkMapping("ProjConPaymentDes8th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_8 { get; set; }
        [BookmarkMapping("ProjConPayementAmt8th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_8 { get; set; }
        [BookmarkMapping("ProjConDueDate8th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_8 { get; set; }
        [BookmarkMapping("ProjConPaymentPeriod9th_1")]
        public int QBuyerAgreementLineCAPF_Period_9 { get; set; }
        [BookmarkMapping("ProjConPaymentDes9th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_9 { get; set; }
        [BookmarkMapping("ProjConPayementAmt9th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_9 { get; set; }
        [BookmarkMapping("ProjConDueDate9th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_9 { get; set; }
        [BookmarkMapping("ProjConPaymentPeriod10th_1")]
        public int QBuyerAgreementLineCAPF_Period_10 { get; set; }
        [BookmarkMapping("ProjConPaymentDes10th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_10 { get; set; }
        [BookmarkMapping("ProjConPayementAmt10th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_10 { get; set; }
        [BookmarkMapping("ProjConDueDate10th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_10 { get; set; }

        [BookmarkMapping("ProjConPaymentPeriod11th_1")]
        public int QBuyerAgreementLineCAPF_Period_11 { get; set; }
        [BookmarkMapping("ProjConPaymentDes11th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_11 { get; set; }
        [BookmarkMapping("ProjConPayementAmt11th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_11 { get; set; }
        [BookmarkMapping("ProjConDueDate11th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_11 { get; set; }
        [BookmarkMapping("ProjConPaymentPeriod12th_1")]
        public int QBuyerAgreementLineCAPF_Period_12 { get; set; }
        [BookmarkMapping("ProjConPaymentDes12th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_12 { get; set; }
        [BookmarkMapping("ProjConPayementAmt12th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_12 { get; set; }
        [BookmarkMapping("ProjConDueDate12th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_12 { get; set; }

        [BookmarkMapping("ProjConPaymentPeriod13th_1")]
        public int QBuyerAgreementLineCAPF_Period_13 { get; set; }
        [BookmarkMapping("ProjConPaymentDes13th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_13 { get; set; }
        [BookmarkMapping("ProjConPayementAmt13th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_13 { get; set; }
        [BookmarkMapping("ProjConDueDate13th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_13 { get; set; }
        [BookmarkMapping("ProjConPaymentPeriod14th_1")]
        public int QBuyerAgreementLineCAPF_Period_14 { get; set; }
        [BookmarkMapping("ProjConPaymentDes14th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_14 { get; set; }
        [BookmarkMapping("ProjConPayementAmt14th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_14 { get; set; }
        [BookmarkMapping("ProjConDueDate14th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_14 { get; set; }

        [BookmarkMapping("ProjConPaymentPeriod15th_1")]
        public int QBuyerAgreementLineCAPF_Period_15 { get; set; }
        [BookmarkMapping("ProjConPaymentDes15th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_15 { get; set; }
        [BookmarkMapping("ProjConPayementAmt15th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_15 { get; set; }
        [BookmarkMapping("ProjConDueDate15th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_15 { get; set; }
        [BookmarkMapping("ProjConPaymentPeriod16th_1")]
        public int QBuyerAgreementLineCAPF_Period_16 { get; set; }
        [BookmarkMapping("ProjConPaymentDes16th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_16 { get; set; }
        [BookmarkMapping("ProjConPayementAmt16th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_16 { get; set; }
        [BookmarkMapping("ProjConDueDate16th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_16 { get; set; }

        [BookmarkMapping("ProjConPaymentPeriod17th_1")]
        public int QBuyerAgreementLineCAPF_Period_17 { get; set; }
        [BookmarkMapping("ProjConPaymentDes17th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_17 { get; set; }
        [BookmarkMapping("ProjConPayementAmt17th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_17 { get; set; }
        [BookmarkMapping("ProjConDueDate17th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_17 { get; set; }
        [BookmarkMapping("ProjConPaymentPeriod18th_1")]
        public int QBuyerAgreementLineCAPF_Period_18 { get; set; }
        [BookmarkMapping("ProjConPaymentDes18th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_18 { get; set; }
        [BookmarkMapping("ProjConPayementAmt18th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_18 { get; set; }
        [BookmarkMapping("ProjConDueDate18th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_18 { get; set; }

        [BookmarkMapping("ProjConPaymentPeriod19th_1")]
        public int QBuyerAgreementLineCAPF_Period_19 { get; set; }
        [BookmarkMapping("ProjConPaymentDes19th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_19 { get; set; }
        [BookmarkMapping("ProjConPayementAmt19th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_19 { get; set; }
        [BookmarkMapping("ProjConDueDate19th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_19 { get; set; }

        [BookmarkMapping("ProjConPaymentPeriod20th_1")]
        public int QBuyerAgreementLineCAPF_Period_20 { get; set; }
        [BookmarkMapping("ProjConPaymentDes20th_1")]
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_20 { get; set; }
        [BookmarkMapping("ProjConPayementAmt20th_1")]
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_20 { get; set; }
        [BookmarkMapping("ProjConDueDate20th_1")]
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_20 { get; set; }

        [BookmarkMapping("ProjectConValue")]
        public decimal QSumBuyerAgreementLineCAPF_SumBuyerAgreementAmount { get; set; }
        [BookmarkMapping("LoanRequest")]
        public decimal QSumCreditAppRequestLine_SumCreditLimitLineRequest { get; set; }
        [BookmarkMapping("SUMProjConPayementAmt")]
        public decimal QSum10BuyerAgreementLine_SumBuyerAgreementAmount { get; set; }
        [BookmarkMapping("BusinessSegment")]
        public string QCreditAppRequestLine_BusinessSegmentDesc { get; set; }
        [BookmarkMapping("BuyerName")]
        public string QCreditAppRequestLine_BuyerName { get; set; }
        [BookmarkMapping("BuyerAddress")]
        public string QCreditAppRequestLine_InvoiceAddress { get; set; }
        [BookmarkMapping("BuyerBusinessType")]
        public string QCreditAppRequestLine_LineOfBusinessDesc { get; set; }
        [BookmarkMapping("BuyerBlacklistStatus")]
        public string QCreditAppRequestLine_BlacklistStatusDesc { get; set; }
        [BookmarkMapping("BuyerRegisNum")]
        public string QCreditAppRequestLine_TaxId { get; set; }
        [BookmarkMapping("BuyerFoundingDate")]
        public string QCreditAppRequestLine_DateOfEstablish { get; set; }
        [BookmarkMapping("BuyerCreditScore")]
        public string QCreditAppRequestLine_CreditScoringDesc { get; set; }
        [BookmarkMapping("BuyerFINStateYear_1" , "BuyerInComeStateYear_1")]
        public int QCreditAppReqLineFin1_Year { get; set; }
        [BookmarkMapping("BuyerFINStateShareholder_1")]
        public decimal QCreditAppReqLineFin1_RegisteredCapital { get; set; }
        [BookmarkMapping("BuyerFINStatePaidShareHolder_1")]
        public decimal QCreditAppReqLineFin1_PaidCapital { get; set; }
        [BookmarkMapping("BuyerFINStateAssets_1")]
        public decimal QCreditAppReqLineFin1_TotalAsset { get; set; }
        [BookmarkMapping("BuyerFINStateLiability_1")]
        public decimal QCreditAppReqLineFin1_TotalLiability { get; set; }
        [BookmarkMapping("BuyerFINStateEquiry_1")]
        public decimal QCreditAppReqLineFin1_TotalEquity { get; set; }
        [BookmarkMapping("BuyerInComeStateRev_1")]
        public decimal QCreditAppReqLineFin1_TotalRevenue { get; set; }
        [BookmarkMapping("BuyerInComeStateCost_1")]
        public decimal QCreditAppReqLineFin1_TotalCOGS { get; set; }
        [BookmarkMapping("BuyerInComeStateGrossProfit_1")]
        public decimal QCreditAppReqLineFin1_TotalGrossProfit { get; set; }
        [BookmarkMapping("BuyerInComeStateExp_1")]
        public decimal QCreditAppReqLineFin1_TotalOperExpFirst { get; set; }
        [BookmarkMapping("BuyerInComeStateNetProfit_1")]
        public decimal QCreditAppReqLineFin1_TotalNetProfitFirst { get; set; }
        [BookmarkMapping("BuyerFINStateYear_2" , "BuyerInComeStateYear_2")]
        public int QCreditAppReqLineFin2_Year { get; set; }
        [BookmarkMapping("BuyerFINStateShareholder_2")]
        public decimal QCreditAppReqLineFin2_RegisteredCapital { get; set; }
        [BookmarkMapping("BuyerFINStatePaidShareHolder_2")]
        public decimal QCreditAppReqLineFin2_PaidCapital { get; set; }
        [BookmarkMapping("BuyerFINStateAssets_2")]
        public decimal QCreditAppReqLineFin2_TotalAsset { get; set; }
        [BookmarkMapping("BuyerFINStateLiability_2")]
        public decimal QCreditAppReqLineFin2_TotalLiability { get; set; }
        [BookmarkMapping("BuyerFINStateEquiry_2")]
        public decimal QCreditAppReqLineFin2_TotalEquity { get; set; }
        [BookmarkMapping("BuyerInComeStateRev_2")]
        public decimal QCreditAppReqLineFin2_TotalRevenue { get; set; }
        [BookmarkMapping("BuyerInComeStateCost_2")]
        public decimal QCreditAppReqLineFin2_TotalCOGS { get; set; }
        [BookmarkMapping("BuyerInComeStateGrossProfit_2")]
        public decimal QCreditAppReqLineFin2_TotalGrossProfit { get; set; }
        [BookmarkMapping("BuyerInComeStateExp_2")]
        public decimal QCreditAppReqLineFin2_TotalOperExpFirst { get; set; }
        [BookmarkMapping("BuyerInComeStateNetProfit_2")]
        public decimal QCreditAppReqLineFin2_TotalNetProfitFirst { get; set; }
        [BookmarkMapping("BuyerFINStateYear_3" , "BuyerInComeStateYear_3")]
        public int QCreditAppReqLineFin3_Year { get; set; }
        [BookmarkMapping("BuyerFINStateShareholder_3")]
        public decimal QCreditAppReqLineFin3_RegisteredCapital { get; set; }
        [BookmarkMapping("BuyerFINStatePaidShareHolder_3")]
        public decimal QCreditAppReqLineFin3_PaidCapital { get; set; }
        [BookmarkMapping("BuyerFINStateAssets_3")]
        public decimal QCreditAppReqLineFin3_TotalAsset { get; set; }
        [BookmarkMapping("BuyerFINStateLiability_3")]
        public decimal QCreditAppReqLineFin3_TotalLiability { get; set; }
        [BookmarkMapping("BuyerFINStateEquiry_3")]
        public decimal QCreditAppReqLineFin3_TotalEquity { get; set; }
        [BookmarkMapping("BuyerInComeStateRev_3")]
        public decimal QCreditAppReqLineFin3_TotalRevenue { get; set; }
        [BookmarkMapping("BuyerInComeStateCost_3")]
        public decimal QCreditAppReqLineFin3_TotalCOGS { get; set; }
        [BookmarkMapping("BuyerInComeStateGrossProfit_3")]
        public decimal QCreditAppReqLineFin3_TotalGrossProfit { get; set; }
        [BookmarkMapping("BuyerInComeStateExp_3")]
        public decimal QCreditAppReqLineFin3_TotalOperExpFirst { get; set; }
        [BookmarkMapping("BuyerInComeStateNetProfit_3")]
        public decimal QCreditAppReqLineFin3_TotalNetProfitFirst { get; set; }
        [BookmarkMapping("BuyerNetProfit")]
        public decimal QCreditAppReqLineFin1_NetProfitPercent { get; set; }
        [BookmarkMapping("BuyerDE")]
        public decimal QCreditAppReqLineFin1_lDE { get; set; }
        [BookmarkMapping("BuyerCurrentRatio")]
        public decimal QCreditAppReqLineFin1_QuickRatio { get; set; }
        [BookmarkMapping("BuyerInterestCoverageRatio")]
        public decimal QCreditAppReqLineFin1_IntCoverageRatio { get; set; }

    }
    public class QCreditAppRequestTableCAPF
    {
        public string QCreditAppRequestTableCAPF_CreditTermId { get; set; }
        public string QCreditAppRequestTableCAPF_CreditAppRequestId { get; set; }
        public decimal QCreditAppRequestTableCAPF_SalesAvgPerMonth { get; set; }
        public decimal QCreditAppRequestTableCAPF_CreditLimitRequest { get; set; }
        public string QCreditAppRequestTableCAPF_NumberOfDays { get; set; }
    }
    public class QCreditAppRequestLineCAPF
    {
        public Guid? QCreditAppRequestLineCAPF_CreditAppRequestLineGUID { get; set; }
        public Guid? QCreditAppRequestLineCAPF_BuyerTableGUID { get; set; }
        public Guid? QCreditAppRequestLineCAPF_InvoiceAddressGUID { get; set; }
        public Guid? QCreditAppRequestLineCAPF_AssignmentMethodGUID { get; set; }
        public string QCreditAppRequestLineCAPF_AssignmentMethodDesc { get; set; }
        public Guid? QCreditAppRequestLineCAPF_LineOfBusinessGUID { get; set; }
        public string QCreditAppRequestLineCAPF_LineOfBusinessDesc { get; set; }
        public Guid? QCreditAppRequestLineCAPF_BlacklistStatusGUID { get; set; }
        public string QCreditAppRequestLineCAPF_BlacklistStatusDesc { get; set; }
        public Guid? QCreditAppRequestLineCAPF_CreditScoringGUID { get; set; }
        public string QCreditAppRequestLineCAPF_CreditScoringDesc { get; set; }
        public decimal QCreditAppRequestLineCAPF_CreditLimitLineRequest { get; set; }
        public string QCreditAppRequestLineCAPF_PaymentCondition { get; set; }
        public string QCreditAppRequestLineCAPF_InvoiceAddress { get; set; }
        public string QCreditAppRequestLineCAPF_CreditScoring { get; set; }
        public string QCreditAppRequestLineCAPF_BlacklistStatus { get; set; }
        public string QCreditAppRequestLineCAPF_BuyerName { get; set; }
        public string QCreditAppRequestLineCAPF_BuyerId { get; set; }
        public string QCreditAppRequestLineCAPF_TaxId { get; set; }
        public string QCreditAppRequestLineCAPF_DateOfEstablish { get; set; }
        public string QCreditAppRequestLineCAPF_BusinessSegmentGUID { get; set; }
        public string QCreditAppRequestLineCAPF_BusinessSegmentDesc { get; set; }
    }
    public class QBuyerTableCAPF
    {
        public string QBuyerTableCAPF_BuyerName { get; set; }
        public string QBuyerTableCAPF_BuyerId { get; set; }
        public string QBuyerTableCAPF_TaxId { get; set; }
        public DateTime? QCreditAppRequestLineCAPF_DateOfEstablish { get; set; }

        public string QBuyerTableCAPF_BusinessSegmentDesc { get; set; }
        public string QBuyerTableCAPF_LineOfBusinessDesc { get; set; }
    }
    public class QBuyerAgreementCAPF
    {
        public Guid? QBuyerAgreementCAPF_BuyerAgreementTableGUID { get; set; }
        public string QBuyerAgreementCAPF_BuyerAgreementId { get; set; }
        public string QBuyerAgreementCAPF_BuyerAgreement { get; set; }
        public DateTime? QBuyerAgreementCAPF_StartDate { get; set; }
        public DateTime? QBuyerAgreementCAPF_EndDate { get; set; }
        public string QBuyerAgreementCAPF_Remark { get; set; }
        public string QBuyerAgreementCAPF_Penalty { get; set; }
    }
    public class QBuyerAgreementLineCAPF
    {
        public int QBuyerAgreementLineCAPF_Period { get; set; }
        public string QBuyerAgreementLineCAPF_BuyerAgreementLineDesc { get; set; }
        public decimal QBuyerAgreementLineCAPF_BuyerAgreementLineAmount { get; set; }
        public DateTime? QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate { get; set; }
        public int QBuyerAgreementLineCAPF_Row { get; set; }
    }
    public class QSumBuyerAgreementLineCAPF
    {
        public decimal QSumBuyerAgreementLineCAPF_SumBuyerAgreementAmount { get; set; }
    }
    public class QSumCreditAppRequestLineCAPF
    {
        public decimal QSumCreditAppRequestLine_SumCreditLimitLineRequest { get; set; }
    }
}
