using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using System.Collections.Generic;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class UpdateAssignmentAgreementSettleResultView : ResultBaseEntity
	{
		public AssignmentAgreementSettle RevertAssignmentAgreementSettle { get; set; } = new AssignmentAgreementSettle();
		public AssignmentAgreementSettle NewSettledAmount { get; set; } = new AssignmentAgreementSettle();
	}
}
