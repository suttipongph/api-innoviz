using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class TaxTableItemView : ViewCompanyBaseEntity
	{
		public string TaxTableGUID { get; set; }
		public string Description { get; set; }
		public string InputTaxLedgerAccount { get; set; }
		public string OutputTaxLedgerAccount { get; set; }
		public string PaymentTaxTableGUID { get; set; }
		public string TaxCode { get; set; }
	}
}
