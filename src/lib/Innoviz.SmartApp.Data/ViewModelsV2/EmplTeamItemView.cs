using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class EmplTeamItemView : ViewCompanyBaseEntity
	{
		public string EmplTeamGUID { get; set; }
		public string Name { get; set; }
		public string TeamId { get; set; }
	}
}
