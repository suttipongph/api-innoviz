using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CreditAppReqAssignmentListView : ViewCompanyBaseEntity
	{
		public string CreditAppReqAssignmentGUID { get; set; }
		public bool IsNew { get; set; }
		public string AssignmentAgreementTableGUID { get; set; }
		public string BuyerTableGUID { get; set; }
		public decimal AssignmentAgreementAmount { get; set; }
		public string AssignmentMethodGUID { get; set; }
		public string BuyerAgreementTableGUID { get; set; }
		public decimal BuyerAgreementAmount { get; set; }
		public decimal RemainingAmount { get; set; }
		public string CreditAppRequestTableGUID { get; set; }
		public string AssignmentAgreementTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string AssignmentMethod_Values { get; set; }
		public string BuyerAgreementTable_Values { get; set; }
		public string AssignmentAgreementTable_InternalAssignmentAgreementId { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string AssignmentMethod_AssignmentMethodId { get; set; }
		public string BuyerAgreementTable_BuyerAgreementId { get; set; }
	}
}
