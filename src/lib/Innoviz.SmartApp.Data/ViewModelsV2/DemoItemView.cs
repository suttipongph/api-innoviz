﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class DemoItemView
    {
        public string DemoGUID { get; set; }
        public string DemoID { get; set; }
        public string Description { get; set; }
        public string DocumentProcessGUID { get; set; }
        public string DocumentStatusGUID { get; set; }
        public string EmployeeTableGUID { get; set; }
        public string DemoDate { get; set; }
        public decimal DemoAmount { get; set; }
        public int DemoPeriod { get; set; }
        public bool DemoFact { get; set; }
        public string DemoDocument { get; set; }
    }
}
