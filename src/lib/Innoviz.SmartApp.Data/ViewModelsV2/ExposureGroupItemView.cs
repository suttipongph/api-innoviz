using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ExposureGroupItemView : ViewCompanyBaseEntity
	{
		public string ExposureGroupGUID { get; set; }
		public string Description { get; set; }
		public string ExposureGroupId { get; set; }
	}
}
