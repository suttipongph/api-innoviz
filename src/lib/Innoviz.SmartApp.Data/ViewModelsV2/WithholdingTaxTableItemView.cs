using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class WithholdingTaxTableItemView : ViewCompanyBaseEntity
	{
		public string WithholdingTaxTableGUID { get; set; }
		public string Description { get; set; }
		public string LedgerAccount { get; set; }
		public string WHTCode { get; set; }
	}
}
