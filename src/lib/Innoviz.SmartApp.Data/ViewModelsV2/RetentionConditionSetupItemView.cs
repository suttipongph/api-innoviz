using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class RetentionConditionSetupItemView : ViewCompanyBaseEntity
	{
		public string RetentionConditionSetupGUID { get; set; }
		public int ProductType { get; set; }
		public int RetentionCalculateBase { get; set; }
		public int RetentionDeductionMethod { get; set; }
	}
}
