using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class NumberSeqSetupByProductTypeListView : ViewCompanyBaseEntity
	{
		public string NumberSeqSetupByProductTypeGUID { get; set; }
		public int ProductType { get; set; }
		public string CreditAppRequestNumberSeqGUID { get; set; }
		public string CreditAppNumberSeqGUID { get; set; }
		public string InternalMainAgreementNumberSeqGUID { get; set; }
		public string MainAgreementNumberSeqGUID { get; set; }
		public string InternalGuarantorAgreementNumberSeqGUID { get; set; }
		public string GuarantorAgreementNumberSeqGUID { get; set; }
		public string TaxInvoiceNumberSeqGUID { get; set; }
		public string TaxCreditNoteNumberSeqGUID { get; set; }
		public string ReceiptNumberSeqGUID { get; set; }
		public string CreditAppRequestNumber_Values { get; set; }
		public string CreditAppNumberSeq_Values { get; set; }
		public string InternalMainAgreementNumber_Values { get; set; }
		public string MainAgreementNumber_Values { get; set; }
		public string InternalGuarantorAgreementNumber_Values { get; set; }
		public string GuarantorAgreementNumber_Values { get; set; }
		public string TaxInvoiceNumberSeq_Values { get; set; }
		public string TaxCreditNoteNumberSeq_Values { get; set; }
		public string ReceiptNumberSeq_Values { get; set; }
		public string NumberSeqTableGUID { get; set; }
		public string NumberSeqCode { get; set; }
	}
}
