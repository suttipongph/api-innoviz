using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BuyerReceiptTableListView : ViewCompanyBaseEntity
	{
		public string BuyerReceiptTableGUID { get; set; }
		public string BuyerReceiptId { get; set; }
		public string BuyerReceiptDate { get; set; }
		public string BuyerTableGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public decimal BuyerReceiptAmount { get; set; }
		public bool Cancel { get; set; }
		public string BuyerTable_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string BuyerTable_BuyerId { get; set; }
	}
}
