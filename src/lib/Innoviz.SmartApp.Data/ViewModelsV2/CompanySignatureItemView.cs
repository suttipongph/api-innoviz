using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CompanySignatureItemView : ViewCompanyBaseEntity
	{
		public string CompanySignatureGUID { get; set; }
		public string EmployeeTableGUID { get; set; }
		public int Ordering { get; set; }
		public int RefType { get; set; }
		public string Branch_Values { get; set; }
		public string EmployeeTable_EmployeeId { get; set; }
		public string EmployeeTable_Name { get; set; }
		public string BranchGUID { get; set; }
		public string DefaultBranchGUID { get; set; }
	}
}
