using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class ServiceFeeTransListView : ViewCompanyBaseEntity
    {
        public string ServiceFeeTransGUID { get; set; }
        public int Ordering { get; set; }
        public string Description { get; set; }
        public string InvoiceRevenueTypeGUID { get; set; }
        public bool IncludeTax { get; set; }
        public decimal FeeAmount { get; set; }
        public decimal AmountBeforeTax { get; set; }
        public string InvoiceRevenueType_Values { get; set; }
        public string InvoiceRevenueType_RevenueTypeId { get; set; }
        public string RefGUID { get; set; }
    }
}
