using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BuyerAgreementTableItemView : ViewCompanyBaseEntity
	{
		public string BuyerAgreementTableGUID { get; set; }
		public string BuyerAgreementId { get; set; }
		public string BuyerTableGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public string Description { get; set; }
		public string EndDate { get; set; }
		public decimal MaximumCreditLimit { get; set; }
		public string Penalty { get; set; }
		public string ReferenceAgreementID { get; set; }
		public string Remark { get; set; }
		public string StartDate { get; set; }
		public string BuyerAgreementTable_Values { get; set; }
		public string CreditAppRequestLine_CreditAppRequestLineGUID { get; set; }
		public string BuyerAgreementTable_Description { get; set; }
		public string BuyerAgreementTable_ReferenceAgreementID { get; set; }
		public decimal BuyerAgreementAmount { get; set; }
		public string BuyerAgreementTrans_RefGUID { get; set; }
		public string CreditAppTable_CreditAppTableGUID { get; set; }
		public string BuyerAgreementLine_Description { get; set; }
		public int BuyerAgreementTrans_RefType { get; set; }

	}
}
