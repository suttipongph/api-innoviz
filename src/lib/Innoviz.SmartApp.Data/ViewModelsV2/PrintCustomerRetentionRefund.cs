﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
  public  class PrintCustomerRetentionRefund
    {
        public string printCustomerRefundRetentionGUID { get; set; }
        public string customerId { get; set; }
        public string customerRefundId { get; set; }
        public string documentStatus { get; set; }
        public decimal netAmount { get; set; }
        public decimal paymentAmount { get; set; }
        public decimal retentionAmount { get; set; }
        public decimal serviceFeeAmount { get; set; }
        public decimal settleAmount { get; set; }
        public decimal suspenseAmount { get; set; }
        public string transDate { get; set; }
        public string CustomerRefundTableGUID { get; set; }
    }
    public class PrintCustomerRetentionRefundViewReport : ViewReportBaseEntity
    {
        public string printCustomerRefundRetentionGUID { get; set; }
        public string customerId { get; set; }
        public string customerRefundId { get; set; }
        public string documentStatus { get; set; }
        public decimal netAmount { get; set; }
        public decimal paymentAmount { get; set; }
        public decimal retentionAmount { get; set; }
        public decimal serviceFeeAmount { get; set; }
        public decimal settleAmount { get; set; }
        public decimal suspenseAmount { get; set; }
        public string transDate { get; set; }
        public string CustomerRefundTableGUID { get; set; }
    }
}
