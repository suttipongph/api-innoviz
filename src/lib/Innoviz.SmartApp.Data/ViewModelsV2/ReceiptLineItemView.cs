using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using System;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ReceiptLineItemView : ViewBranchCompanyBaseEntity
	{
		public string ReceiptLineGUID { get; set; }
		public string DueDate { get; set; }
		public decimal InvoiceAmount { get; set; }
		public string InvoiceTableGUID { get; set; }
		public int LineNum { get; set; }
		public string ReceiptTableGUID { get; set; }
		public decimal SettleAmount { get; set; }
		public decimal SettleAmountMST { get; set; }
		public decimal SettleBaseAmount { get; set; }
		public decimal SettleBaseAmountMST { get; set; }
		public decimal SettleTaxAmount { get; set; }
		public decimal SettleTaxAmountMST { get; set; }
		public decimal TaxAmount { get; set; }
		public string TaxTableGUID { get; set; }
		public decimal WHTAmount { get; set; }
		public string WithholdingTaxTableGUID { get; set; }
		public string ReceiptTable_Values { get; set; }
		public string InvoiceTable_Values { get; set; }
		public string TaxTable_Code { get; set; }
		public string WithholdingTaxTable_Code { get; set; }

	}
}
