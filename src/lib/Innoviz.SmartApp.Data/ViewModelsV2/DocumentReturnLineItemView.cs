using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class DocumentReturnLineItemView : ViewCompanyBaseEntity
	{
		public string DocumentReturnLineGUID { get; set; }
		public string Address { get; set; }
		public string BuyerTableGUID { get; set; }
		public string ContactPersonName { get; set; }
		public string DocumentNo { get; set; }
		public string DocumentReturnTableGUID { get; set; }
		public string DocumentTypeGUID { get; set; }
		public int LineNum { get; set; }
		public string Remark { get; set; }
		public int DocumentReturnTable_ContactTo { get; set; }
		public string DocumentReturnTable_DocumentStatus_StatusId { get; set; }
		public string DocumentReturnTable_DocumentReturnId { get; set; }
	}
}
