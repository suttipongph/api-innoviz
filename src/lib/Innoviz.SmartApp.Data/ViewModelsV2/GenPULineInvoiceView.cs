﻿using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class GenPULineInvoiceView
    {

    }
    public class GenPULineInvoiceResultView : ResultBaseEntity
    {
        public List<InvoiceTable> InvoiceTable { get; set; }
        public List<InvoiceLine> InvoiceLine { get; set; }
        public List<CustTrans> CustTrans { get; set; }
        public List<ProcessTrans> ProcessTrans { get; set; }
        public List<RetentionTrans> RetentionTrans { get; set; }
        public List<TaxInvoiceTable> TaxInvoiceTable { get; set; }
        public List<TaxInvoiceLine> TaxInvoiceLine { get; set; }
        public List<CreditAppTrans> CreditAppTrans { get; set; }
        public List<PurchaseLine> PurchaseLine { get; set; }
    }
}

