﻿using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class PurchaseLineRollBillInitialDataParm
    {
        public string PurchaseLineGUID { get; set; }
        public string PurchaseTableGUID { get; set; }
    }
}
