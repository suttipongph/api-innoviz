﻿using Innoviz.SmartApp.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class QBusinessCollateralAgmExtend
    {
        public string QBusinessCollateralAgmExtend_BusinessCollateralAgmId { get; set; }
        public Guid? QBusinessCollateralAgmExtend_RefBusinessCollateralAgmTableGUID { get; set; }
        public string QBusinessCollateralAgmExtend_Name { get; set; }
        public string QBusinessCollateralAgmExtend_TaxID { get; set; }
        public string QBusinessCollateralAgmExtend_CompanyGUID { get; set; }
    }
    public class QAgreementTableInfo
    {
        public string QAgreementTableInfo_CustomerAddress { get; set; }
        public string QAgreementTableInfo_CompanyWitness1 { get; set; }
        public string QAgreementTableInfo_CompanyWitness2 { get; set; }
        public string QAgreementTableInfo_CompanyAuthorizedPerson1 { get; set; }
        public string QAgreementTableInfo_CompanyAuthorizedPerson2 { get; set; }
        public string QAgreementTableInfo_CompanyAuthorizedPerson3 { get; set; }
        public string QAgreementTableInfo_AuthorizedPersonTransCustomer1GUID { get; set; }
        public string QAgreementTableInfo_AuthorizedPersonTransCustomer2GUID { get; set; }
        public string QAgreementTableInfo_AuthorizedPersonTransCustomer3GUID { get; set; }
    }
    public class QCustomerAuthorizedPerson
    {
        public string QCustomerAuthorizedPerson1_Name { get; set; }
        public string QCustomerAuthorizedPerson2_Name { get; set; }
        public string QCustomerAuthorizedPerson3_Name { get; set; }
    }
    public class QBusinessCollateralAgmOriginal
    {
        public string QBusinessCollateralAgmOriginal_BusinessCollateralAgmId { get; set; }
        public decimal QBusinessCollateralAgmOriginal_TotalBusinessCollateralValue { get; set; }
        public DateTime? QBusinessCollateralAgmOriginal_AgreementDate { get; set; }
    }
    public class QBusinessCollateralAgmExtendREF
    {
        public DateTime? QBusinessCollateralAgmExten_AgreementDate { get; set; }
        public int QBusinessCollateralAgmExtend_AgreementExtension { get; set; }
        public decimal QBusinessCollateralAgmExtend_TotalBusinessCollateralValue { get; set; }

    }
    public class QBusinessCollateralAgreement
    {
        #region lession1 BusinessCollateralAgmExtend
        [BookmarkMapping("ContractCollateralNew1")]
        public string QBusinessCollateralAgmExtend_BusinessCollateralAgmId { get; set; }
        public string QBusinessCollateralAgmExtend_RefBusinessCollateralAgmTableGUID { get; set; }
        [BookmarkMapping("CustName1", "CustName2", "CustName3", "CustName4", "CustName5")]
        public string QBusinessCollateralAgmExtend_Name { get; set; }
        [BookmarkMapping("CustTaxID1", "CustTaxID2")]
        public string QBusinessCollateralAgmExtend_TaxID { get; set; }
        #endregion  lession1 BusinessCollateralAgmExtend

        #region lession 2 AgreementTableInfo
        [BookmarkMapping("CustAddress1", "CustAddress2")]
        public string QAgreementTableInfo_CustomerAddress { get; set; }
        [BookmarkMapping("WitnessFirst1", "WitnessFirst2", "WitnessFirst3")]
        public string QAgreementTableInfo_CompanyWitness1 { get; set; }
        [BookmarkMapping("WitnessSecond1", "WitnessSecond2", "WitnessSecond3")]
        public string QAgreementTableInfo_CompanyWitness2 { get; set; }
        [BookmarkMapping("AuthorityPersonFirst1", "AuthorityPersonFirst2", "AuthorityPersonFirst3")]
        public string QAgreementTableInfo_CompanyAuthorizedPerson1 { get; set; }
        [BookmarkMapping("AuthorityPersonSecond1", "AuthorityPersonSecond2", "AuthorityPersonSecond3")]
        public string QAgreementTableInfo_CompanyAuthorizedPerson2 { get; set; }
        [BookmarkMapping("AuthorityPersonThird1", "AuthorityPersonThird2", "AuthorityPersonThird3")]
        public string QAgreementTableInfo_CompanyAuthorizedPerson3 { get; set; }
        public string QAgreementTableInfo_AuthorizedPersonTransCustomer1GUID { get; set; }
        public string QAgreementTableInfo_AuthorizedPersonTransCustomer2GUID { get; set; }
        public string QAgreementTableInfo_AuthorizedPersonTransCustomer3GUID { get; set; }
        #endregion lession 2 AgreementTableInfo

        #region lession 3 AuthorizedPerson
        [BookmarkMapping("AuthorizedCustFirst1", "AuthorizedCustFirst2", "AuthorizedCustFirst3")]
        public string QCustomerAuthorizedPerson1_Name { get; set; }
        [BookmarkMapping("AuthorizedCustSecond1", "AuthorizedCustSecond2", "AuthorizedCustSecond3")]
        public string QCustomerAuthorizedPerson2_Name { get; set; }
        [BookmarkMapping("AuthorizedCustThird1", "AuthorizedCustThird2", "AuthorizedCustThird3")]
        public string QCustomerAuthorizedPerson3_Name { get; set; }
        #endregion lession 3 AuthorizedPerson

        #region lession 4 BusinessCollateralAgmOriginal
        [BookmarkMapping("ContractCollateralNo1", "ContractCollateralNo2", "ContractCollateralNo3", "ContractCollateralNo4", "ContractCollateralNo5", "ContractCollateralNo6", "ContractCollateralNo7", "ContractCollateralNo8", "ContractCollateralNo9", "ContractCollateralNo10", "ContractCollateralNo11", "ContractCollateralNo12", "ContractCollateralNo13", "ContractCollateralNo14", "ContractCollateralNo15", "ContractCollateralNo16", "ContractCollateralNo17", "ContractCollateralNo18", "ContractCollateralNo19", "ContractCollateralNo20", "ContractCollateralNo21", "ContractCollateralNo22")]
        public string QBusinessCollateralAgmOriginal_BusinessCollateralAgmId { get; set; }
        [BookmarkMapping("CollateralARAmtOriginal1")]
        public decimal QBusinessCollateralAgmExtend_TotalBusinessCollateralValue { get; set; }
        [BookmarkMapping("AccumTotalCollateralAmtOriginal")]
        public decimal QBusinessCollateralAgmOriginal_TotalBusinessCollateralValue { get; set; }
       



        #endregion lession 4 BusinessCollateralAgmOriginal

        #region lession 5 BusinessCollateralAgmExtend
        [BookmarkMapping("CollateralARAmTxt1st_1")]

        public decimal QBusinessCollateralAgmExtend1_TotalBusinessCollateralValue { get; set; }
        [BookmarkMapping("CollateralARAmTxt2nd_1")]

        public decimal QBusinessCollateralAgmExtend2_TotalBusinessCollateralValue { get; set; }
        [BookmarkMapping("CollateralARAmTxt3rd_1")]

        public decimal QBusinessCollateralAgmExtend3_TotalBusinessCollateralValue { get; set; }
        [BookmarkMapping("CollateralARAmTxt4th_1")]

        public decimal QBusinessCollateralAgmExtend4_TotalBusinessCollateralValue { get; set; }
        [BookmarkMapping("CollateralARAmTxt5th_1")]

        public decimal QBusinessCollateralAgmExtend5_TotalBusinessCollateralValue { get; set; }
        [BookmarkMapping("CollateralARAmTxt6th_1")]

        public decimal QBusinessCollateralAgmExtend6_TotalBusinessCollateralValue { get; set; }
        [BookmarkMapping("CollateralARAmTxt7th_1")]

        public decimal QBusinessCollateralAgmExtend7_TotalBusinessCollateralValue { get; set; }
        [BookmarkMapping("CollateralARAmTxt8th_1")]

        public decimal QBusinessCollateralAgmExtend8_TotalBusinessCollateralValue { get; set; }
        [BookmarkMapping("CollateralARAmTxt9th_1")]

        public decimal QBusinessCollateralAgmExtend9_TotalBusinessCollateralValue { get; set; }
        [BookmarkMapping("CollateralARAmTxt10th_1")]

        public decimal QBusinessCollateralAgmExtend10_TotalBusinessCollateralValue { get; set; }
        #endregion lession 5 BusinessCollateralAgmLine

        #region lession 6 BusinessCollateralAgmExtend

        [BookmarkMapping("IssueNoFirst1", "IssueNoFirst2", "IssueNoFirst3", "IssueNoFirst4")]

        public int QBusinessCollateralAgmExtend1_AgreementExtension { get; set; }
        [BookmarkMapping("IssueNoSecond1", "IssueNoSecond2", "IssueNoSecond3")]

        public int QBusinessCollateralAgmExtend2_AgreementExtension { get; set; }
        [BookmarkMapping("IssueNoThird1", "IssueNoThird2", "IssueNoThird3")]

        public int QBusinessCollateralAgmExtend3_AgreementExtension { get; set; }
        [BookmarkMapping("IssueNoFourth1", "IssueNoFourth2", "IssueNoFourth3")]

        public int QBusinessCollateralAgmExtend4_AgreementExtension { get; set; }
        [BookmarkMapping("IssueNoFifth1", "IssueNoFifth2", "IssueNoFifth3")]

        public int QBusinessCollateralAgmExtend5_AgreementExtension { get; set; }
        [BookmarkMapping("IssueNoSixth1", "IssueNoSixth2", "IssueNoSixth3")]

        public int QBusinessCollateralAgmExtend6_AgreementExtension { get; set; }
        [BookmarkMapping("IssueNoSeventh1", "IssueNoSeventh2", "IssueNoSeventh3")]

        public int QBusinessCollateralAgmExtend7_AgreementExtension { get; set; }
        [BookmarkMapping("IssueNoEighth1", "IssueNoEighth2", "IssueNoEighth3")]

        public int QBusinessCollateralAgmExtend8_AgreementExtension { get; set; }
        [BookmarkMapping("IssueNoNineth1", "IssueNoNineth2", "IssueNoNineth3")]

        public int QBusinessCollateralAgmExtend9_AgreementExtension { get; set; }
        [BookmarkMapping("IssueNoTenth1", "IssueNoTenth2", "IssueNoTenth3")]

        public int QBusinessCollateralAgmExtend10_AgreementExtension { get; set; }
        #endregion lession 6 BusinessCollateralAgmExtend

        #region variable


        [BookmarkMapping("AccumTotalCollateralAmtTxtOriginal", "CollateralARAmtOriginalText1")]
        public string Variable_OriginalText { get; set; }
        [BookmarkMapping("CollateralARAmTxtTxt1st_1")]
        public string Variable_AmountText1 { get; set; }
        [BookmarkMapping("CollateralARAmTxtTxt2nd_1")]
        public string Variable_AmountText2 { get; set; }
        [BookmarkMapping("CollateralARAmTxtTxt3rd_1")]
        public string Variable_AmountText3 { get; set; }
        [BookmarkMapping("CollateralARAmTxtTxt4th_1")]
        public string Variable_AmountText4 { get; set; }
        [BookmarkMapping("CollateralARAmTxtTxt5th_1")]
        public string Variable_AmountText5 { get; set; }
        [BookmarkMapping("CollateralARAmTxtTxt6th_1")]
        public string Variable_AmountText6 { get; set; }
        [BookmarkMapping("CollateralARAmTxtTxt7th_1")]
        public string Variable_AmountText7 { get; set; }
        [BookmarkMapping("CollateralARAmTxtTxt8th_1")]
        public string Variable_AmountText8 { get; set; }
        [BookmarkMapping("CollateralARAmTxtTxt9th_1")]
        public string Variable_AmountText9 { get; set; }
        [BookmarkMapping("CollateralARAmTxtTxt10th_1")]
        public string Variable_AmountText10 { get; set; }


        [BookmarkMapping("AccumTotalCollateralAmtExt1st")]
        public decimal Variable_AccumExt1 { get; set; }
        [BookmarkMapping("AccumTotalCollateralAmtExt2nd")]
        public decimal Variable_AccumExt2 { get; set; }
        [BookmarkMapping("AccumTotalCollateralAmtExt3rd")]
        public decimal Variable_AccumExt3 { get; set; }
        [BookmarkMapping("AccumTotalCollateralAmtExt4th")]
        public decimal Variable_AccumExt4 { get; set; }
        [BookmarkMapping("AccumTotalCollateralAmtExt5th")]
        public decimal Variable_AccumExt5 { get; set; }
        [BookmarkMapping("AccumTotalCollateralAmtExt6th")]
        public decimal Variable_AccumExt6 { get; set; }
        [BookmarkMapping("AccumTotalCollateralAmtExt7th")]
        public decimal Variable_AccumExt7 { get; set; }
        [BookmarkMapping("AccumTotalCollateralAmtExt8th")]
        public decimal Variable_AccumExt8 { get; set; }
        [BookmarkMapping("AccumTotalCollateralAmtExt9th")]
        public decimal Variable_AccumExt9 { get; set; }
        [BookmarkMapping("AccumTotalCollateralAmtExt10th")]
        public decimal Variable_AccumExt10 { get; set; }



        [BookmarkMapping("AccumTotalCollateralAmtTxtExt1st")]
        public string Variable_AccumExtText1 { get; set; }
        [BookmarkMapping("AccumTotalCollateralAmtTxtExt2nd")]
        public string Variable_AccumExtText2 { get; set; }
        [BookmarkMapping("AccumTotalCollateralAmtTxtExt3rd")]
        public string Variable_AccumExtText3 { get; set; }
        [BookmarkMapping("AccumTotalCollateralAmtTxtExt4th")]
        public string Variable_AccumExtText4 { get; set; }
        [BookmarkMapping("AccumTotalCollateralAmtTxtExt5th")]
        public string Variable_AccumExtText5 { get; set; }
        [BookmarkMapping("AccumTotalCollateralAmtTxtExt6th")]
        public string Variable_AccumExtText6 { get; set; }
        [BookmarkMapping("AccumTotalCollateralAmtTxtExt7th")]
        public string Variable_AccumExtText7 { get; set; }
        [BookmarkMapping("AccumTotalCollateralAmtTxtExt8th")]
        public string Variable_AccumExtText8 { get; set; }
        [BookmarkMapping("AccumTotalCollateralAmtTxtExt9th")]
        public string Variable_AccumExtText9 { get; set; }
        [BookmarkMapping("AccumTotalCollateralAmtTxtExt10th")]
        public string Variable_AccumExtText10 { get; set; }
        [BookmarkMapping("CollateralDate1", "CollateralDate2", "CollateralDate3", "CollateralDate4", "CollateralDate5", "CollateralDate6", "CollateralDate7", "CollateralDate8", "CollateralDate9", "CollateralDate10", "CollateralDate11", "CollateralDate12", "CollateralDate13", "CollateralDate14", "CollateralDate15", "CollateralDate16", "CollateralDate17", "CollateralDate18")]
        public string Variable_AgmDate { get; set; }


        [BookmarkMapping("CurrentDateFirst1", "CurrentDateFirst2", "CurrentDateFirst3")]
        public string Variable_AgmDateExt1 { get; set; }
        [BookmarkMapping("CurrentDateSecond1", "CurrentDateSecond2", "CurrentDateSecond3")]
        public string Variable_AgmDateExt2 { get; set; }
        [BookmarkMapping("CurrentDateThird1", "CurrentDateThird2", "CurrentDateThird3")]
        public string Variable_AgmDateExt3 { get; set; }
        [BookmarkMapping("CurrentDateFourth1", "CurrentDateFourth2", "CurrentDateFourth3")]
        public string Variable_AgmDateExt4 { get; set; }
        [BookmarkMapping("CurrentDateFifth1", "CurrentDateFifth2", "CurrentDateFifth3")]
        public string Variable_AgmDateExt5 { get; set; }
        [BookmarkMapping("CurrentDateSixth1", "CurrentDateSixth2", "CurrentDateSixth3")]
        public string Variable_AgmDateExt6 { get; set; }
        [BookmarkMapping("CurrentDateSeventh1", "CurrentDateSeventh2", "CurrentDateSeventh3")]
        public string Variable_AgmDateExt7 { get; set; }
        [BookmarkMapping("CurrentDateEighth1", "CurrentDateEighth2", "CurrentDateEighth3")]
        public string Variable_AgmDateExt8 { get; set; }
        [BookmarkMapping("CurrentDateNineth1", "CurrentDateNineth2", "CurrentDateNineth3")]
        public string Variable_AgmDateExt9 { get; set; }
        [BookmarkMapping("CurrentDateTenth1", "CurrentDateTenth2", "CurrentDateTenth3")]
        public string Variable_AgmDateExt10 { get; set; }

        #endregion variable

        #region Company
        [BookmarkMapping("CompanyName1", "CompanyName2", "CompanyName3", "CompanyName4", "CompanyName5", "CompanyName6", "CompanyName7", "CompanyName8")]
        public string QCompany_Name { get; set; }
        [BookmarkMapping("CompanyAddress1", "CompanyAddress2", "CompanyAddress3", "CompanyAddress4")]
        public string QCompany_Address { get; set; }
        #endregion Company
    }
    public class QCompany
    {
        public string QCompany_Name { get; set; }
        public string QCompany_Address { get; set; }

    }
}
