﻿using System;
using System.Collections.Generic;
using System.Text;
using Innoviz.SmartApp.Core.Attributes;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class QGuarantorAgreementTable
    {

        public string QGuarantorAgreementTable_GuarantorAgreementId { get; set; }
        public string QGuarantorAgreementTable_AgreementDate { get; set; }
        public string QGuarantorAgreementTable_AgreementYear { get; set; }
        public string QGuarantorAgreementTable_MainAgreementTableGUID { get; set; }
        public string QGuarantorAgreementTable_CustomerAddress { get; set; }
        public string QGuarantorAgreementTable_CompanyWitness1 { get; set; }
        public string QGuarantorAgreementTable_CompanyWitness2 { get; set; }
        public string QGuarantorAgreementTable_CompanyGUID { get; set; }
        public decimal QGuarantorAgreementTable_ApprovedCreditLimit { get; set; }
        public string QGuarantorAgreementTable_CompanyName { get; set; }
        public string QGuarantorAgreementTable_CompanyAddress { get; set; }
        public string QGuarantorAgreementTable_MainAgreementId { get; set; }
        public DateTime QGuarantorAgreementTable_MainAgreementDate { get; set; }
        public string QGuarantorAgreementTable_MainCustomerName { get; set; }
        public string QGuarantorAgreementTable_MainAgreementAddress { get; set; }
        public string QGuarantorAgreementTable_Description { get; set; }
    }
    public class QGuarantorAgreementInfo
    {
        public string QGuarantorAgreementInfo_CompanyWitness1 { get; set; }
        public string QGuarantorAgreementInfo_CompanyWitness2 { get; set; }
    }
    public class QBookmarkDocumentTrans
    {
        public int EnumDocTemType { get; set; }
    }
    public class QCompany_GuarantorAgreement
    {
        public string QCompany_GuarantorAgreement_Name { get; set; }
        public string QCompany_GuarantorAgreement_Address { get; set; }
    }
    public class QMainAgreementTable
    {
        public string QMainAgreementTable_GuarantorAgreement_MainAgreementId { get; set; }
        public string QMainAgreementTable_GuarantorAgreement_MainAgreementDate { get; set; }
        public string QMainAgreementTable_GuarantorAgreement_ApprovedCreditLimit { get; set; }
        public string QMainAgreementTable_GuarantorAgreement_MainCustomerName { get; set; }
    }
    public class QMainAgreementtInfo
    {
        public string QMainAgreementtInfo_MainAgreementAddress { get; set; }
    }
    public class QConsortiumTable
    {
        public string QConsortiumTable_Description { get; set; }
    }
    public class QGuarantorAgreementLine1
    {
        public string QGuarantorAgreementLine1_GuarantorAgreement_Name { get; set; }
        public string QGuarantorAgreementLine1_GuarantorAgreement_Age { get; set; }
        public string QGuarantorAgreementLine1_GuarantorAgreement_Race { get; set; }
        public string QGuarantorAgreementLine1_GuarantorAgreement_Nationality { get; set; }
        public string QGuarantorAgreementLine1_GuarantorAgreement_Address { get; set; }
        public string QGuarantorAgreementLine1_GuarantorAgreement_TaxId { get; set; }
        public string QGuarantorAgreementLine1_GuarantorAgreement_WorkPermitId { get; set; }
        public string QGuarantorAgreementLine1_GuarantorAgreement_DOB { get; set; }
        public string QGuarantorAgreementLine1_GuarantorAgreement_PassportId { get; set; }
        public string QGuarantorAgreementLine1_GuarantorAgreement_DateOfIssue { get; set; }
    }
    public class QGuarantorAgreementLine2
    {
        public string QGuarantorAgreementLine2_GuarantorAgreement_Name { get; set; }
        public string QGuarantorAgreementLine2_GuarantorAgreement_Age { get; set; }
        public string QGuarantorAgreementLine2_GuarantorAgreement_Race { get; set; }
        public string QGuarantorAgreementLine2_GuarantorAgreement_Nationality { get; set; }
        public string QGuarantorAgreementLine2_GuarantorAgreement_Address { get; set; }
        public string QGuarantorAgreementLine2_GuarantorAgreement_TaxId { get; set; }
        public string QGuarantorAgreementLine2_GuarantorAgreement_WorkPermitId { get; set; }
        public string QGuarantorAgreementLine2_GuarantorAgreement_DOB { get; set; }
        public string QGuarantorAgreementLine2_GuarantorAgreement_PassportId { get; set; }
        public string QGuarantorAgreementLine2_GuarantorAgreement_DateOfIssue { get; set; }
    }
    public class QGuarantorAgreementLine3
    {
        public string QGuarantorAgreementLine3_GuarantorAgreement_Name { get; set; }
        public string QGuarantorAgreementLine3_GuarantorAgreement_Age { get; set; }
        public string QGuarantorAgreementLine3_GuarantorAgreement_Race { get; set; }
        public string QGuarantorAgreementLine3_GuarantorAgreement_Nationality { get; set; }
        public string QGuarantorAgreementLine3_GuarantorAgreement_Address { get; set; }
        public string QGuarantorAgreementLine3_GuarantorAgreement_TaxId { get; set; }
        public string QGuarantorAgreementLine3_GuarantorAgreement_WorkPermitId { get; set; }
        public string QGuarantorAgreementLine3_GuarantorAgreement_DOB { get; set; }
        public string QGuarantorAgreementLine3_GuarantorAgreement_PassportId { get; set; }
        public string QGuarantorAgreementLine3_GuarantorAgreement_DateOfIssue { get; set; }
    }
    public class QGuarantorAgreementLine4
    {
        public string QGuarantorAgreementLine4_GuarantorAgreement_Name { get; set; }
        public string QGuarantorAgreementLine4_GuarantorAgreement_Age { get; set; }
        public string QGuarantorAgreementLine4_GuarantorAgreement_Race { get; set; }
        public string QGuarantorAgreementLine4_GuarantorAgreement_Nationality { get; set; }
        public string QGuarantorAgreementLine4_GuarantorAgreement_Address { get; set; }
        public string QGuarantorAgreementLine4_GuarantorAgreement_TaxId { get; set; }
        public string QGuarantorAgreementLine4_GuarantorAgreement_WorkPermitId { get; set; }
        public string QGuarantorAgreementLine4_GuarantorAgreement_DOB { get; set; }
        public string QGuarantorAgreementLine4_GuarantorAgreement_PassportId { get; set; }
        public string QGuarantorAgreementLine4_GuarantorAgreement_DateOfIssue { get; set; }
    }
    public class QGuarantorAgreementLine5
    {
        public string QGuarantorAgreementLine5_GuarantorAgreement_Name { get; set; }
        public string QGuarantorAgreementLine5_GuarantorAgreement_Age { get; set; }
        public string QGuarantorAgreementLine5_GuarantorAgreement_Race { get; set; }
        public string QGuarantorAgreementLine5_GuarantorAgreement_Nationality { get; set; }
        public string QGuarantorAgreementLine5_GuarantorAgreement_Address { get; set; }
        public string QGuarantorAgreementLine5_GuarantorAgreement_TaxId { get; set; }
        public string QGuarantorAgreementLine5_GuarantorAgreement_WorkPermitId { get; set; }
        public string QGuarantorAgreementLine5_GuarantorAgreement_DOB { get; set; }
        public string QGuarantorAgreementLine5_GuarantorAgreement_PassportId { get; set; }
        public string QGuarantorAgreementLine5_GuarantorAgreement_DateOfIssue { get; set; }
    }
    public class QGuarantorAgreementLine6
    {
        public string QGuarantorAgreementLine6_GuarantorAgreement_Name { get; set; }
        public string QGuarantorAgreementLine6_GuarantorAgreement_Age { get; set; }
        public string QGuarantorAgreementLine6_GuarantorAgreement_Race { get; set; }
        public string QGuarantorAgreementLine6_GuarantorAgreement_Nationality { get; set; }
        public string QGuarantorAgreementLine6_GuarantorAgreement_Address { get; set; }
        public string QGuarantorAgreementLine6_GuarantorAgreement_TaxId { get; set; }
        public string QGuarantorAgreementLine6_GuarantorAgreement_WorkPermitId { get; set; }
        public string QGuarantorAgreementLine6_GuarantorAgreement_DOB { get; set; }
        public string QGuarantorAgreementLine6_GuarantorAgreement_PassportId { get; set; }
        public string QGuarantorAgreementLine6_GuarantorAgreement_DateOfIssue { get; set; }
    }
    public class QGuarantorAgreementLine7
    {
        public string QGuarantorAgreementLine7_GuarantorAgreement_Name { get; set; }
        public string QGuarantorAgreementLine7_GuarantorAgreement_Age { get; set; }
        public string QGuarantorAgreementLine7_GuarantorAgreement_Race { get; set; }
        public string QGuarantorAgreementLine7_GuarantorAgreement_Nationality { get; set; }
        public string QGuarantorAgreementLine7_GuarantorAgreement_Address { get; set; }
        public string QGuarantorAgreementLine7_GuarantorAgreement_TaxId { get; set; }
        public string QGuarantorAgreementLine7_GuarantorAgreement_WorkPermitId { get; set; }
        public string QGuarantorAgreementLine7_GuarantorAgreement_DOB { get; set; }
        public string QGuarantorAgreementLine7_GuarantorAgreement_PassportId { get; set; }
        public string QGuarantorAgreementLine7_GuarantorAgreement_DateOfIssue { get; set; }
    }
    public class QGuarantorAgreementLine8
    {
        public string QGuarantorAgreementLine8_GuarantorAgreement_Name { get; set; }
        public string QGuarantorAgreementLine8_GuarantorAgreement_Age { get; set; }
        public string QGuarantorAgreementLine8_GuarantorAgreement_Race { get; set; }
        public string QGuarantorAgreementLine8_GuarantorAgreement_Nationality { get; set; }
        public string QGuarantorAgreementLine8_GuarantorAgreement_Address { get; set; }
        public string QGuarantorAgreementLine8_GuarantorAgreement_TaxId { get; set; }
        public string QGuarantorAgreementLine8_GuarantorAgreement_WorkPermitId { get; set; }
        public string QGuarantorAgreementLine8_GuarantorAgreement_DOB { get; set; }
        public string QGuarantorAgreementLine8_GuarantorAgreement_PassportId { get; set; }
        public string QGuarantorAgreementLine8_GuarantorAgreement_DateOfIssue { get; set; }
    }
    public class QGuarantorAgreementLine
    {
        public string Name { get; set; }
        public string TaxId { get; set; }
        public string PassportId { get; set; }
        public string WorkPermitId { get; set; }
        public string DateOfIssue { get; set; }
        public DateTime? DOB { get; set; }
        public int Age { get; set; }
        public string NationalityGUID { get; set; }
        public string Nationality { get; set; }
        public string RaceGUID { get; set; }
        public string Race { get; set; }
        public string Address { get; set; }
        public string Ordering { get; set; }
        public string RelatedPersonTableGUID { get; set; }
        public int IdentificationType { get; set; }
        public int RecordType { get; set; }
        public string GuaranteeLand { get; set; }
        public string OperatedBy { get; set; }
        public int Row { get; set; }
    }
    public class QGuarantorAgreementLineCOUNT
    {
        public int Count { get; set; }
    }
    public class QConsortiumTrans1
    {
        public string QConsortiumTrans1_GuarantorAgreement_CustomerName { get; set; }
        public string QConsortiumTrans1_GuarantorAgreement_Address { get; set; }
        public string QConsortiumTrans1_GuarantorAgreement_ConsName { get; set; }
        public int Ordering { get; set; }
    }
    public class QConsortiumTrans2
    {
        public string QConsortiumTrans2_GuarantorAgreement_CustomerName { get; set; }
        public string QConsortiumTrans2_GuarantorAgreement_Address { get; set; }
    }
    public class QConsortiumTrans3
    {
        public string QConsortiumTrans3_GuarantorAgreement_CustomerName { get; set; }
        public string QConsortiumTrans3_GuarantorAgreement_Address { get; set; }
    }
    public class QJointVentureTrans1
    {
        public string QJointVentureTrans1_GuarantorAgreement_Name { get; set; }
        public string QJointVentureTrans1_GuarantorAgreement_Address { get; set; }
    }
    public class QJointVentureTrans2
    {
        public string QJointVentureTrans2_GuarantorAgreement_Name { get; set; }
        public string QJointVentureTrans2_GuarantorAgreement_Address { get; set; }
    }
    public class QJointVentureTrans3
    {
        public string QJointVentureTrans3_GuarantorAgreement_Name { get; set; }
        public string QJointVentureTrans3_GuarantorAgreement_Address { get; set; }
    }
    public class BookmaskDocumentGuarantorAgreement_Variable
    {
        public string Variable_GuarantorAgreementNo1 { get; set; }
        public string Variable_GuarantorAgreementNo2 { get; set; }
        public string Variable_GuarantorAgreementNo3 { get; set; }
        public string Variable_GuarantorAgreementNo4 { get; set; }
        public string Variable_GuarantorAgreementNo5 { get; set; }
        public string Variable_GuarantorAgreementNo6 { get; set; }
        public string Variable_GuarantorAgreementNo7 { get; set; }
        public string Variable_GuarantorAgreementNo8 { get; set; }
        public string Variable_ContractDate { get; set; }
        public string Variable_GuarantorBirthday1 { get; set; }
        public string Variable_GuarantorBirthday2 { get; set; }
        public string Variable_GuarantorBirthday3 { get; set; }
        public string Variable_GuarantorBirthday4 { get; set; }
        public string Variable_GuarantorBirthday5 { get; set; }
        public string Variable_GuarantorBirthday6 { get; set; }
        public string Variable_GuarantorBirthday7{ get; set; }
        public string Variable_GuarantorBirthday8 { get; set; }
        public string Variable_MainApprovedCreditLimit { get; set; }
    }
    public class BookmaskDocumentGuarantorAgreement
    {
        [BookmarkMapping("GuarantorAgreementNo1")]
        public string Variable_GuarantorAgreementNo1 { get; set; }
        [BookmarkMapping("GuarantorAgreementNo2")]
        public string Variable_GuarantorAgreementNo2 { get; set; }
        [BookmarkMapping("GuarantorAgreementNo3")]
        public string Variable_GuarantorAgreementNo3 { get; set; }
        [BookmarkMapping("GuarantorAgreementNo4")]
        public string Variable_GuarantorAgreementNo4 { get; set; }
        [BookmarkMapping("GuarantorAgreementNo5")]
        public string Variable_GuarantorAgreementNo5 { get; set; }
        [BookmarkMapping("GuarantorAgreementNo6")]
        public string Variable_GuarantorAgreementNo6 { get; set; }
        [BookmarkMapping("GuarantorAgreementNo7")]
        public string Variable_GuarantorAgreementNo7 { get; set; }
        [BookmarkMapping("GuarantorAgreementNo8")]
        public string Variable_GuarantorAgreementNo8 { get; set; }
        [BookmarkMapping("CompanyName1", "CompanyName2", "CompanyName3", "CompanyName4", "CompanyName5", "CompanyName6", "CompanyName7", "CompanyName8", "CompanyName9", "CompanyName10", "CompanyName11", "CompanyName12", "CompanyName13", "CompanyName14", "CompanyName15", "CompanyName16")]
        public string QCompany_Name { get; set; }
        [BookmarkMapping("CompanyAddress1", "CompanyAddress2", "CompanyAddress3", "CompanyAddress4", "CompanyAddress5", "CompanyAddress6", "CompanyAddress7", "CompanyAddress8")]
        public string QCompany_Address { get; set; }
        [BookmarkMapping("GuarantorAgreementDate1", "GuarantorAgreementDate2", "GuarantorAgreementDate3", "GuarantorAgreementDate4", "GuarantorAgreementDate5", "GuarantorAgreementDate6", "GuarantorAgreementDate7", "GuarantorAgreementDate8", "GuarantorAgreementDate9", "GuarantorAgreementDate10", "GuarantorAgreementDate11", "GuarantorAgreementDate12", "GuarantorAgreementDate13", "GuarantorAgreementDate14", "GuarantorAgreementDate15", "GuarantorAgreementDate16")]
        public string QGuarantorAgreementTable_AgreementDate { get; set; }
        [BookmarkMapping("GuarantorYear1", "GuarantorYear2", "GuarantorYear3", "GuarantorYear4", "GuarantorYear5", "GuarantorYear6", "GuarantorYear7", "GuarantorYear8")]
        public string QGuarantorAgreementTable_AgreementYear { get; set; }
        [BookmarkMapping("ContractNo", "ContractNo1", "ContractNo2", "ContractNo3", "ContractNo4", "ContractNo5", "ContractNo6", "ContractNo7", "ContractNo8", "ContractNo10", "ContractNo11", "ContractNo12", "ContractNo13", "ContractNo14", "ContractNo15", "ContractNo16")]
        public string QMainAgreementTable_MainAgreementId { get; set; }
        [BookmarkMapping("ContractDate", "ContractDate1", "ContractDate2", "ContractDate3", "ContractDate4", "ContractDate5", "ContractDate6", "ContractDate7", "ContractDate8", "ContractDate10", "ContractDate11", "ContractDate12", "ContractDate13", "ContractDate14", "ContractDate15", "ContractDate16")]
        public string Variable_ContractDate { get; set; }
        [BookmarkMapping("CreditLimitAmt1", "CreditLimitAmt2", "CreditLimitAmt3", "CreditLimitAmt4", "CreditLimitAmt5", "CreditLimitAmt6", "CreditLimitAmt7", "CreditLimitAmt8", "CreditLimitAmt1d", "CreditLimitAmt2d", "CreditLimitAmt3d", "CreditLimitAmt4d", "CreditLimitAmt5d", "CreditLimitAmt6d", "CreditLimitAmt7d", "CreditLimitAmt8d")]
        public decimal QMainAgreementTable_ApprovedCreditLimit { get; set; }
        [BookmarkMapping("CreditLimitAmt1TH", "CreditLimitAmt2TH", "CreditLimitAmt3TH", "CreditLimitAmt4TH", "CreditLimitAmt5TH", "CreditLimitAmt6TH", "CreditLimitAmt7TH", "CreditLimitAmt8TH")]
        public string THApprovedCreditLimit { get; set; }
        [BookmarkMapping("CreditLimitAmtText1", "CreditLimitAmtText2", "CreditLimitAmtText3", "CreditLimitAmtText4", "CreditLimitAmtText5", "CreditLimitAmtText6", "CreditLimitAmtText7", "CreditLimitAmtText8", "CreditLimitAmtText1d", "CreditLimitAmtText2d", "CreditLimitAmtText3d", "CreditLimitAmtText4d", "CreditLimitAmtText5d", "CreditLimitAmtText6d", "CreditLimitAmtText7d", "CreditLimitAmtText8d")]
        public string Variable_MainApprovedCreditLimit { get; set; }
        [BookmarkMapping("CompanyWitnessFirst1", "CompanyWitnessFirst2", "CompanyWitnessFirst3", "CompanyWitnessFirst4", "CompanyWitnessFirst5", "CompanyWitnessFirst6", "CompanyWitnessFirst7", "CompanyWitnessFirst8")]
        public string QGuarantorAgreementTable_CompanyWitness1 { get; set; }
        [BookmarkMapping("CompanyWitnessSecond1", "CompanyWitnessSecond2", "CompanyWitnessSecond3", "CompanyWitnessSecond4", "CompanyWitnessSecond5", "CompanyWitnessSecond6", "CompanyWitnessSecond7", "CompanyWitnessSecond8")]
        public string QGuarantorAgreementTable_CompanyWitness2 { get; set; }
        [BookmarkMapping("CustName1", "CustName2", "CustName3", "CustName4", "CustName5", "CustName6", "CustName7", "CustName8", "CustName9", "CustName10", "CustName11", "CustName12", "CustName13", "CustName14", "CustName15", "CustName16")]
        public string QGuarantorAgreementTable_MainCustomerName { get; set; }
        [BookmarkMapping("CustAddress1", "CustAddress2", "CustAddress3", "CustAddress4", "CustAddress5", "CustAddress6", "CustAddress7", "CustAddress8")]
        public string QGuarantorAgreementTable_CustomerAddress { get; set; }
        [BookmarkMapping("GuarantorName1st_1", "GuarantorName1st_2")]
        public string QGuarantorAgreementLine1_Name { get; set; }
        [BookmarkMapping("GuarantorAge1st_1")]
        public int QGuarantorAgreementLine1_Age { get; set; }
        [BookmarkMapping("GuarantorRace1st_1")]
        public string QGuarantorAgreementLine1_Race { get; set; }
        [BookmarkMapping("GuarantorNationality1st_1")]
        public string QGuarantorAgreementLine1_Nationality { get; set; }
        [BookmarkMapping("GuarantorAddress1st_1")]
        public string QGuarantorAgreementLine1_Address { get; set; }
        [BookmarkMapping("GuarantorTaxID1st_1")]
        public string QGuarantorAgreementLine1_TaxId { get; set; }
        [BookmarkMapping("GuarantorBirthday1st_1")]
        public string Variable_GuarantorBirthday1 { get; set; }
        [BookmarkMapping("PassportID1st_1")]
        public string QGuarantorAgreementLine1_PassportId { get; set; }
        [BookmarkMapping("WorkPermitID1st_1")]
        public string QGuarantorAgreementLine1_WorkPermitId { get; set; }
        [BookmarkMapping("ConsortiumName1st_1", "ConsortiumName2nd_1", "ConsortiumName3rd_1", "ConsortiumName4th_1", "ConsortiumName5th_1", "ConsortiumName6th_1", "ConsortiumName7th_1", "ConsortiumName8th_1")]
        public string QConsortiumTrans1_ConsName { get; set; }
        [BookmarkMapping("ConsCustNameFirst1st_1", "ConsCustNameFirst2nd_1", "ConsCustNameFirst3rd_1", "ConsCustNameFirst4th_1", "ConsCustNameFirst5th_1", "ConsCustNameFirst6th_1", "ConsCustNameFirst7th_1", "ConsCustNameFirst8th_1")]
        public string QConsortiumTrans1_CustomerName { get; set; }
        [BookmarkMapping("ConsCustAddressFirst1st_1", "ConsCustAddressFirst2nd_1", "ConsCustAddressFirst3rd_1", "ConsCustAddressFirst4th_1", "ConsCustAddressFirst5th_1", "ConsCustAddressFirst6th_1", "ConsCustAddressFirst7th_1", "ConsCustAddressFirst8th_1")]
        public string QConsortiumTrans1_Address { get; set; }
        [BookmarkMapping("ConsCustNameSecond1st_1", "ConsCustNameSecond2nd_1", "ConsCustNameSecond3rd_1", "ConsCustNameSecond4th_1", "ConsCustNameSecond5th_1", "ConsCustNameSecond6th_1", "ConsCustNameSecond7th_1", "ConsCustNameSecond8th_1")]
        public string QConsortiumTrans2_CustomerName { get; set; }
        [BookmarkMapping("ConsCustAddressSecond1st_1", "ConsCustAddressSecond2nd_1", "ConsCustAddressSecond3rd_1", "ConsCustAddressSecond4th_1", "ConsCustAddressSecond5th_1", "ConsCustAddressSecond6th_1", "ConsCustAddressSecond7th_1", "ConsCustAddressSecond8th_1")]
        public string QConsortiumTrans2_Address { get; set; }
        [BookmarkMapping("ConsCustNameThird1st_1", "ConsCustNameThird2nd_1", "ConsCustNameThird3rd_1", "ConsCustNameThird4th_1", "ConsCustNameThird5th_1", "ConsCustNameThird6th_1", "ConsCustNameThird7th_1", "ConsCustNameThird8th_1")]
        public string QConsortiumTrans3_CustomerName { get; set; }
        [BookmarkMapping("ConsCustAddressThird1st_1", "ConsCustAddressThird2nd_1", "ConsCustAddressThird3rd_1", "ConsCustAddressThird4th_1", "ConsCustAddressThird5th_1", "ConsCustAddressThird6th_1", "ConsCustAddressThird7th_1", "ConsCustAddressThird8th_1")]
        public string QConsortiumTrans3_Address { get; set; }
        [BookmarkMapping("JointNameFirst1st_1", "JointNameFirst2nd_1", "JointNameFirst3rd_1", "JointNameFirst4th_1", "JointNameFirst5th_1", "JointNameFirst6th_1", "JointNameFirst7th_1", "JointNameFirst8th_1")]
        public string QJointVentureTrans1_Name { get; set; }
        [BookmarkMapping("JointAddressFirst1st_1", "JointAddressFirst2nd_1", "JointAddressFirst3rd_1", "JointAddressFirst4th_1", "JointAddressFirst5th_1", "JointAddressFirst6th_1", "JointAddressFirst7th_1", "JointAddressFirst8th_1")]
        public string QJointVentureTrans1_Address { get; set; }
        [BookmarkMapping("JointNameSecond1st_1", "JointNameSecond2nd_1", "JointNameSecond3rd_1", "JointNameSecond4th_1", "JointNameSecond5th_1", "JointNameSecond6th_1", "JointNameSecond7th_1", "JointNameSecond8th_1")]
        public string QJointVentureTrans2_Name { get; set; }
        [BookmarkMapping("JointAddressSecond1st_1", "JointAddressSecond2nd_1", "JointAddressSecond3rd_1", "JointAddressSecond4th_1", "JointAddressSecond5th_1", "JointAddressSecond6th_1", "JointAddressSecond7th_1", "JointAddressSecond8th_1")]
        public string QJointVentureTrans2_Address { get; set; }
        [BookmarkMapping("JointNameThird1st_1", "JointNameThird2nd_1", "JointNameThird3rd_1", "JointNameThird4th_1", "JointNameThird5th_1", "JointNameThird6th_1", "JointNameThird7th_1", "JointNameThird8th_1")]
        public string QJointVentureTrans3_Name { get; set; }
        [BookmarkMapping("JointAddressThird1st_1", "JointAddressThird2nd_1", "JointAddressThird3rd_1", "JointAddressThird4th_1", "JointAddressThird5th_1", "JointAddressThird6th_1", "JointAddressThird7th_1", "JointAddressThird8th_1")]
        public string QJointVentureTrans3_Address { get; set; }
        //------------------------------------------------------------------
        [BookmarkMapping("GuarantorName2nd_1", "GuarantorName2nd_2")]
        public string QGuarantorAgreementLine2_Name { get; set; }
        [BookmarkMapping("GuarantorAge2nd_1")]
        public int QGuarantorAgreementLine2_Age { get; set; }
        [BookmarkMapping("GuarantorRace2nd_1")]
        public string QGuarantorAgreementLine2_Race { get; set; }
        [BookmarkMapping("GuarantorNationality2nd_1")]
        public string QGuarantorAgreementLine2_Nationality { get; set; }
        [BookmarkMapping("GuarantorAddress2nd_1")]
        public string QGuarantorAgreementLine2_Address { get; set; }
        [BookmarkMapping("GuarantorTaxID2nd_1")]
        public string QGuarantorAgreementLine2_TaxId { get; set; }
        [BookmarkMapping("GuarantorBirthday2nd_1")]
        public string Variable_GuarantorBirthday2 { get; set; }
        [BookmarkMapping("PassportID2nd_1")]
        public string QGuarantorAgreementLine2_PassportId { get; set; }
        [BookmarkMapping("WorkPermitID2nd_1")]
        public string QGuarantorAgreementLine2_WorkPermitId { get; set; }
        //------------------------------------------------------------------
        [BookmarkMapping("GuarantorName3rd_1", "GuarantorName3rd_2")]
        public string QGuarantorAgreementLine3_Name { get; set; }
        [BookmarkMapping("GuarantorAge3rd_1")]
        public int QGuarantorAgreementLine3_Age { get; set; }
        [BookmarkMapping("GuarantorRace3rd_1")]
        public string QGuarantorAgreementLine3_Race { get; set; }
        [BookmarkMapping("GuarantorNationality3rd_1")]
        public string QGuarantorAgreementLine3_Nationality { get; set; }
        [BookmarkMapping("GuarantorAddress3rd_1")]
        public string QGuarantorAgreementLine3_Address { get; set; }
        [BookmarkMapping("GuarantorTaxID3rd_1")]
        public string QGuarantorAgreementLine3_TaxId { get; set; }
        [BookmarkMapping("GuarantorBirthday3rd_1")]
        public string Variable_GuarantorBirthday3 { get; set; }
        [BookmarkMapping("PassportID3rd_1")]
        public string QGuarantorAgreementLine3_PassportId { get; set; }
        [BookmarkMapping("WorkPermitID3rd_1")]
        public string QGuarantorAgreementLine3_WorkPermitId { get; set; }
        //------------------------------------------------------------------
        [BookmarkMapping("GuarantorName4th_1", "GuarantorName4th_2")]
        public string QGuarantorAgreementLine4_Name { get; set; }
        [BookmarkMapping("GuarantorAge4th_1")]
        public int QGuarantorAgreementLine4_Age { get; set; }
        [BookmarkMapping("GuarantorRace4th_1")]
        public string QGuarantorAgreementLine4_Race { get; set; }
        [BookmarkMapping("GuarantorNationality4th_1")]
        public string QGuarantorAgreementLine4_Nationality { get; set; }
        [BookmarkMapping("GuarantorAddress4th_1")]
        public string QGuarantorAgreementLine4_Address { get; set; }
        [BookmarkMapping("GuarantorTaxID4th_1")]
        public string QGuarantorAgreementLine4_TaxId { get; set; }
        [BookmarkMapping("GuarantorBirthday4th_1")]
        //public string QGuarantorAgreementLine4_DOB { get; set; }
        public string Variable_GuarantorBirthday4 { get; set; }
        [BookmarkMapping("PassportID4th_1")]
        public string QGuarantorAgreementLine4_PassportId { get; set; }
        [BookmarkMapping("WorkPermitID4th_1")]
        public string QGuarantorAgreementLine4_WorkPermitId { get; set; }
        //------------------------------------------------------------------
        [BookmarkMapping("GuarantorName5th_1", "GuarantorName5th_2")]
        public string QGuarantorAgreementLine5_Name { get; set; }
        [BookmarkMapping("GuarantorAge5th_1")]
        public int QGuarantorAgreementLine5_Age { get; set; }
        [BookmarkMapping("GuarantorRace5th_1")]
        public string QGuarantorAgreementLine5_Race { get; set; }
        [BookmarkMapping("GuarantorNationality5th_1")]
        public string QGuarantorAgreementLine5_Nationality { get; set; }
        [BookmarkMapping("GuarantorAddress5th_1")]
        public string QGuarantorAgreementLine5_Address { get; set; }
        [BookmarkMapping("GuarantorTaxID5th_1")]
        public string QGuarantorAgreementLine5_TaxId { get; set; }
        [BookmarkMapping("GuarantorBirthday5th_1")]
        public string Variable_GuarantorBirthday5 { get; set; }
        [BookmarkMapping("PassportID5th_1")]
        public string QGuarantorAgreementLine5_PassportId { get; set; }
        [BookmarkMapping("WorkPermitID5th_1")]
        public string QGuarantorAgreementLine5_WorkPermitId { get; set; }
        //------------------------------------------------------------------
        [BookmarkMapping("GuarantorName6th_1", "GuarantorName6th_2")]
        public string QGuarantorAgreementLine6_Name { get; set; }
        [BookmarkMapping("GuarantorAge6th_1")]
        public int QGuarantorAgreementLine6_Age { get; set; }
        [BookmarkMapping("GuarantorRace6th_1")]
        public string QGuarantorAgreementLine6_Race { get; set; }
        [BookmarkMapping("GuarantorNationality6th_1")]
        public string QGuarantorAgreementLine6_Nationality { get; set; }
        [BookmarkMapping("GuarantorAddress6th_1")]
        public string QGuarantorAgreementLine6_Address { get; set; }
        [BookmarkMapping("GuarantorTaxID6th_1")]
        public string QGuarantorAgreementLine6_TaxId { get; set; }
        [BookmarkMapping("GuarantorBirthday6th_1")]
        public string Variable_GuarantorBirthday6 { get; set; }
        [BookmarkMapping("PassportID6th_1")]
        public string QGuarantorAgreementLine6_PassportId { get; set; }
        [BookmarkMapping("WorkPermitID6th_1")]
        public string QGuarantorAgreementLine6_WorkPermitId { get; set; }
        //------------------------------------------------------------------
        [BookmarkMapping("GuarantorName7th_1", "GuarantorName7th_2")]
        public string QGuarantorAgreementLine7_Name { get; set; }
        [BookmarkMapping("GuarantorAge7th_1")]
        public int QGuarantorAgreementLine7_Age { get; set; }
        [BookmarkMapping("GuarantorRace7th_1")]
        public string QGuarantorAgreementLine7_Race { get; set; }
        [BookmarkMapping("GuarantorNationality7th_1")]
        public string QGuarantorAgreementLine7_Nationality { get; set; }
        [BookmarkMapping("GuarantorAddress7th_1")]
        public string QGuarantorAgreementLine7_Address { get; set; }
        [BookmarkMapping("GuarantorTaxID7th_1")]
        public string QGuarantorAgreementLine7_TaxId { get; set; }
        [BookmarkMapping("GuarantorBirthday7th_1")]
        public string Variable_GuarantorBirthday7 { get; set; }
        [BookmarkMapping("PassportID7th_1")]
        public string QGuarantorAgreementLine7_PassportId { get; set; }
        [BookmarkMapping("WorkPermitID7th_1")]
        public string QGuarantorAgreementLine7_WorkPermitId { get; set; }
        //------------------------------------------------------------------
        [BookmarkMapping("GuarantorName8th_1", "GuarantorName8th_2")]
        public string QGuarantorAgreementLine8_Name { get; set; }
        [BookmarkMapping("GuarantorAge8th_1")]
        public int QGuarantorAgreementLine8_Age { get; set; }
        [BookmarkMapping("GuarantorRace8th_1")]
        public string QGuarantorAgreementLine8_Race { get; set; }
        [BookmarkMapping("GuarantorNationality8th_1")]
        public string QGuarantorAgreementLine8_Nationality { get; set; }
        [BookmarkMapping("GuarantorAddress8th_1")]
        public string QGuarantorAgreementLine8_Address { get; set; }
        [BookmarkMapping("GuarantorTaxID8th_1")]
        public string QGuarantorAgreementLine8_TaxId { get; set; }
        [BookmarkMapping("GuarantorBirthday8th_1")]
        public string Variable_GuarantorBirthday8 { get; set; }
        [BookmarkMapping("PassportID8th_1")]
        public string QGuarantorAgreementLine8_PassportId { get; set; }
        [BookmarkMapping("WorkPermitID8th_1")]
        public string QGuarantorAgreementLine8_WorkPermitId { get; set; }
        //------------------------------------------------------------------
        [BookmarkMapping("TextLand1")]
        public string QGuarantorAgreementLine1_GuaranteeLand { get; set; }
        [BookmarkMapping("TextLand2")]
        public string QGuarantorAgreementLine2_GuaranteeLand { get; set; }
        [BookmarkMapping("TextLand3")]
        public string QGuarantorAgreementLine3_GuaranteeLand { get; set; }
        [BookmarkMapping("TextLand4")]
        public string QGuarantorAgreementLine4_GuaranteeLand { get; set; }
        [BookmarkMapping("TextLand5")]
        public string QGuarantorAgreementLine5_GuaranteeLand { get; set; }
        [BookmarkMapping("TextLand6")]
        public string QGuarantorAgreementLine6_GuaranteeLand { get; set; }
        [BookmarkMapping("TextLand7")]
        public string QGuarantorAgreementLine7_GuaranteeLand { get; set; }
        [BookmarkMapping("TextLand8")]
        public string QGuarantorAgreementLine8_GuaranteeLand { get; set; }
        //------------------------------------------------------------------
        [BookmarkMapping("GuarantorOperatedBy1st_1", "GuarantorOperatedBy1st_2")]
        public string QGuarantorAgreementLine1_OperatedBy { get; set; }
        [BookmarkMapping("GuarantorOperatedBy2nd_1", "GuarantorOperatedBy2nd_2")]
        public string QGuarantorAgreementLine2_OperatedBy { get; set; }
        [BookmarkMapping("GuarantorOperatedBy3rd_1", "GuarantorOperatedBy3rd_2")]
        public string QGuarantorAgreementLine3_OperatedBy { get; set; }
        [BookmarkMapping("GuarantorOperatedBy4th_1", "GuarantorOperatedBy4th_2")]
        public string QGuarantorAgreementLine4_OperatedBy { get; set; }
        [BookmarkMapping("GuarantorOperatedBy5th_1", "GuarantorOperatedBy5th_2")]
        public string QGuarantorAgreementLine5_OperatedBy { get; set; }
        [BookmarkMapping("GuarantorOperatedBy6th_1", "GuarantorOperatedBy6th_2")]
        public string QGuarantorAgreementLine6_OperatedBy { get; set; }
        [BookmarkMapping("GuarantorOperatedBy7th_1", "GuarantorOperatedBy7th_2")]
        public string QGuarantorAgreementLine7_OperatedBy { get; set; }
        [BookmarkMapping("GuarantorOperatedBy8th_1", "GuarantorOperatedBy8th_2")]
        public string QGuarantorAgreementLine8_OperatedBy { get; set; }

        //------------------------------------------------------------------
    }
}
