using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BuyerAgreementTableListView : ViewCompanyBaseEntity
	{
		public string BuyerAgreementTableGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public string BuyerAgreementId { get; set; }
		public string ReferenceAgreementID { get; set; }
		public string Description { get; set; }
		public string StartDate { get; set; }
		public string EndDate { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public decimal buyerAgreementAmount { get; set; }
		public string BuyerTableGUID { get; set; }

	}
}
