using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class WithholdingTaxValueItemView : ViewDateEffectiveBaseEntity
	{
		public string WithholdingTaxValueGUID { get; set; }
		public decimal Value { get; set; }
		public string WithholdingTaxTableGUID { get; set; }
		public string WithholdingTaxTable_Values { get; set; }
	}
}
