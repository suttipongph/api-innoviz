using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ConsortiumTableItemView : ViewCompanyBaseEntity
	{
		public string ConsortiumTableGUID { get; set; }
		public string ConsortiumId { get; set; }
		public string Description { get; set; }
		public string DocumentStatusGUID { get; set; }
	}
}
