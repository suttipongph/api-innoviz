﻿using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class PrintSetBookDocTransParm
	{
		public int RefType{ get; set; }
		public string RefGUID { get; set; }
	}
}
