using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class GuarantorAgreementLineItemView : ViewCompanyBaseEntity
	{
		public string GuarantorAgreementLineGUID { get; set; }
		public int Age { get; set; }
		public string DateOfBirth { get; set; }
		public string DateOfIssue { get; set; }
		public string GuaranteeLand { get; set; }
		public string GuarantorAgreementTableGUID { get; set; }
		public string GuarantorTransGUID { get; set; }
		public string Name { get; set; }
		public string NationalityGUID { get; set; }
		public string OperatedBy { get; set; }
		public int Ordering { get; set; }
		public string PassportId { get; set; }
		public string PrimaryAddress1 { get; set; }
		public string PrimaryAddress2 { get; set; }
		public string RaceGUID { get; set; }
		public string TaxId { get; set; }
		public string WorkPermitId { get; set; }
		public string GuarantorAgreementTable_InternalGuarantorAgreementId { get; set; }
		public bool Affiliate { get; set; }
		public string MainAgreement_CreditAppTableGUID { get; set; }
		public int Mainagreement_AgreementDoctype { get; set; }
		public bool GuarantorTrans_Affiliate { get; set; }
		public int GuarantorTrans_Ordering { get; set; }
		public string GuarantorTrans_GuarantorTransGUID { get; set; }
		public string RelatedPersonTable_Name { get; set; }
		public string RelatedPersonTable_TaxId { get; set; }
		public string RelatedPersonTable_PassportId { get; set; }
		public string RelatedPersonTable_WorkPermitId { get; set; }
		public string RelatedPersonTable_DateOfIssue { get; set; }
		public string RelatedPersonTable_DateOfBirth { get; set; }
		public string RelatedPersonTable_NationalityGUID { get; set; }
		public string RelatedPersonTable_RaceGUID { get; set; }
		public string RelatedPersonTable_Address1 { get; set; }
		public string RelatedPersonTable_Address2 { get; set; }
		public string GuarantorTransTable_RefGUID { get; set; }
		public bool GuarantorTransTable_InActive { get; set; }
		public string CreditAppRequestTable_creditAppRequestTableGUID { get; set; }
		public decimal MaximumGuaranteeAmount { get; set; }
		public string CreditAppTableGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public decimal ApprovedCreditLimit { get; set; }
		public NotificationResponse Notification { get; set; } = new NotificationResponse();
		public string DocumentStatus_StatusId { get; set; } 



	}
}
