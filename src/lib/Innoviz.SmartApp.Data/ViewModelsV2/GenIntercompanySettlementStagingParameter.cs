﻿using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class GenIntercompanySettlementStagingParameter
    {
        public List<IntercompanyInvoiceSettlement> IntercompanyInvoiceSettlement { get; set; }
    }
}
