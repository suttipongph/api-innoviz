using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class AuthorizedPersonTypeItemView : ViewCompanyBaseEntity
	{
		public string AuthorizedPersonTypeGUID { get; set; }
		public string AuthorizedPersonTypeId { get; set; }
		public string Description { get; set; }
	}
}
