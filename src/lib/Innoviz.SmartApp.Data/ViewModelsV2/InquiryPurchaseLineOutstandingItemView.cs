using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class InquiryPurchaseLineOutstandingItemView : ViewCompanyBaseEntity
	{
		public string PurchaseLineGUID { get; set; }
		public string PurchaseTable_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string BuyerInvoiceTable_BuyerInvoiceId { get; set; }
		public decimal InvoiceAmount { get; set; }
		public decimal PurchaseAmount { get; set; }
		public decimal Outstanding { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public string DueDate { get; set; }
		public string ChequeTable_ChequeDate { get; set; }
		public string BillingDate { get; set; }
		public string CollectionDate { get; set; }
		public string CreditAppTableBankAccountControl_BankAccountName { get; set; }
		public string CreditAppTablePDCBank_BankAccountName { get; set; }
		public int LineNum { get; set; }
		public string CreditAppLine_Values { get; set; }
		public string AssignmentAgreementTableGUID { get; set; }
		public string BuyerAgreementTableGUID { get; set; }
		public string MethodOfPaymentGUID { get; set; }
	}
}
