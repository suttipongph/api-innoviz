using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class AddressSubDistrictListView : ViewCompanyBaseEntity
	{
		public string AddressSubDistrictGUID { get; set; }
		public string AddressDistrictGUID { get; set; }
		public string AddressProvinceGUID { get; set; }
		public string Name { get; set; }
		public string SubDistrictId { get; set; }
		public string addressDistrict_DistrictId { get; set; }
		public string addressDistrict_Name { get; set; }
		public string addressProvince_ProvinceId { get; set; }
		public string AddressProvince_Values { get; set; }
		public string AddressDistrict_Values { get; set; }

	}
}
