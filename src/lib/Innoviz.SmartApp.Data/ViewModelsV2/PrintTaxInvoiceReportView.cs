﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class PrintTaxInvoiceReportView : ViewReportBaseEntity
    {
        public string TaxInvoiceTableGUID { get; set; }
        public string PrintTaxInvoiceGUID { get; set; }
        public string CustomerId { get; set; }
        public string DocumentId { get; set; }
        public string DueDate { get; set; }
        public decimal InvoiceAmount { get; set; }
        public decimal InvoiceAmountBeforeTax { get; set; }
        public string InvoiceId { get; set; }
        public string InvoiceTypeId { get; set; }
        public string IssuedDate { get; set; }
        public decimal TaxAmount { get; set; }
        public string TaxInvoiceId { get; set; }
        public bool IsCopy { get; set; }
    }
}
