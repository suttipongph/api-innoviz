using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class OwnershipListView : ViewCompanyBaseEntity
	{
		public string OwnershipGUID { get; set; }
		public string OwnershipId { get; set; }
		public string Description { get; set; }
	}
}
