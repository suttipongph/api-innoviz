﻿using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ReportAgingView : ViewReportBaseEntity
	{
		public string AsOfDate { get; set; }
		public string[] CustomerTableGUID { get; set; }
		public string[] BuyerTableGUID { get; set; }
		public int ProductType { get; set; }
		public int[] RefType { get; set; }
		public int SuspenseInvoiceType { get; set; }
		public string AgingReportSetup_Description_LineNum1 { get; set; }
		public string AgingReportSetup_Description_LineNum2 { get; set; }
		public string AgingReportSetup_Description_LineNum3 { get; set; }
		public string AgingReportSetup_Description_LineNum4 { get; set; }
		public string AgingReportSetup_Description_LineNum5 { get; set; }
		public string AgingReportSetup_Description_LineNum6 { get; set; }
	}
}
