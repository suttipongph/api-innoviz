using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class IntroducedByListView : ViewCompanyBaseEntity
	{
		public string IntroducedByGUID { get; set; }
		public string Description { get; set; }
		public string IntroducedById { get; set; }
		public string VendorTableGUID { get; set; }
		public string VendorTable_VendorId { get; set; }
		public string VendorTable_Name { get; set; }
		public string VendorTable_Values { get; set; }
	}
}
