using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BuyerReceiptTableItemView : ViewCompanyBaseEntity
	{
		public string BuyerReceiptTableGUID { get; set; }
		public string AssignmentAgreementTableGUID { get; set; }
		public string BuyerAgreementTableGUID { get; set; }
		public string BuyerReceiptAddress { get; set; }
		public decimal BuyerReceiptAmount { get; set; }
		public string BuyerReceiptDate { get; set; }
		public string BuyerReceiptId { get; set; }
		public string BuyerTableGUID { get; set; }
		public bool Cancel { get; set; }
		public string ChequeDate { get; set; }
		public string ChequeNo { get; set; }
		public string CustomerTableGUID { get; set; }
		public string MethodOfPaymentGUID { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public string Remark { get; set; }
		public bool ShowRemarkOnly { get; set; }
		public string BuyerTable_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string AssignmentAgreementTable_Values { get; set; }
		public string BuyerAgreementTable_Values { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public string CompanyBankId { get; set; }
		public string RefId { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string BuyerTable_BuyerId { get; set; }
	}
}
