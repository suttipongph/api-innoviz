﻿using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class OverCreditBuyerView
	{
		public string BuyerTableGUID { get; set; }
		public bool isBuyerCreditLimitByProductExist {get; set; }
		public decimal OverCredit { get; set; }	
		public decimal SumPurchaseLine { get; set; }
		public decimal CreditLimitBalance { get; set; }
	}
}
