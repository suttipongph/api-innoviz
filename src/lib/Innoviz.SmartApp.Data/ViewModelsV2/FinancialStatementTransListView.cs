using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class FinancialStatementTransListView : ViewCompanyBaseEntity
	{
		public string FinancialStatementTransGUID { get; set; }
		public int Year { get; set; }
		public int Ordering { get; set; }
		public string Description { get; set; }
		public decimal Amount { get; set; }
		public string RefGUID { get; set; }
	}
}
