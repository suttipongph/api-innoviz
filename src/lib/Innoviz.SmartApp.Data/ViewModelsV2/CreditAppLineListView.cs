using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CreditAppLineListView : ViewCompanyBaseEntity
	{
		public string CreditAppLineGUID { get; set; }
		public string CreditAppTableGUID { get; set; }
		public int LineNum { get; set; }
		public string ReviewDate { get; set; }
		public string BuyerTableGUID { get; set; }
		public string BusinessSegmentGUID { get; set; }
		public decimal ApprovedCreditLimitLine { get; set; }
		public string BuyerTable_Values { get; set; }
		public string BusinessSegment_Values { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string BusinessSegment_BusinessSegmentId { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string CreditAppTable_CreditAppId { get; set; }

		public string CustomerTable_Values { get; set; }
		public string ExpiryDate { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string CustomerTable_CustomerTableGUID { get; set; }
		public int ProductType { get; set; }
	}
}
