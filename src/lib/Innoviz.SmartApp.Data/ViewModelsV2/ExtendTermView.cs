﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class ExtendTermParamView
    {
        public string WithdrawalTableGUID { get; set; }
        public string WithdrawalId { get; set; }
        public string DocumentStatus { get; set; }
        public string DocumentStatusGUID { get; set; }
        public string CustomerId { get; set; }
        public string BuyerId { get; set; }
        public bool TermExtension { get; set; }
        public int NumberOfExtension { get; set; }
        public string WithdrawalDate { get; set; }
        public string DueDate { get; set; }
        public string CreditTermId { get; set; }
        public decimal TotalInterestPct { get; set; }
        public decimal WithdrawalAmount { get; set; }
        public decimal Outstanding { get; set; }
        public string ExtendWithdrawalDate { get; set; }
        public string ExtendCreditTermGUID { get; set; }
        public string Description { get; set; }
        public string CompanyGUID { get; set; }
    } 
    public class ExtendTermResultView: ResultBaseEntity
    {
        public string WithdrawalTableGUID { get; set; }
    }


}
