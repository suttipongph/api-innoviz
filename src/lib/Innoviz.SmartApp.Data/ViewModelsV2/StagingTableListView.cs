using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class StagingTableListView : ViewCompanyBaseEntity
	{
		public string StagingTableGUID { get; set; }
		public string StagingBatchId { get; set; }
		public string InterfaceStagingBatchId { get; set; }
		public int InterfaceStatus { get; set; }
		public int RefType { get; set; }
		public string TransDate { get; set; }
		public int ProcessTransType { get; set; }
		public int AccountType { get; set; }
		public string AccountNum { get; set; }
		public decimal AmountMST { get; set; }
		public string ProcessTransGUID { get; set; }
		public string ProcessTrans_Values { get; set; }
		public string RefId { get; set; }
		public string RefGUID { get; set; }
		public int SourceRefType { get; set; }
		public string SourceRefId { get; set; }
		public string ProcessTrans_ProcessTransId { get; set; }
		public string InvoiceTable_Values{ get; set; }
		public string InvoiceTableGUID { get; set; }
		public string InvoiceTable_invoiceId { get; set; }
	}
}
