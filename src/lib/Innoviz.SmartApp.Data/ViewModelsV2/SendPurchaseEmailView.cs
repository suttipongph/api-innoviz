﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class SendPurchaseEmailView : ResultBaseEntity
    {
        public WorkflowInstance WorkflowInstance { get; set; }
        public string PurchaseTableGUID { get; set; }
        public bool AdditionalPurchase { get; set; }
        public string CustomerId { get; set; }
        public string DocumentStatus { get; set; }
        public string PurchaseDate { get; set; }
        public string PurchaseId { get; set; }

        // workflow
        public string ParmFolio { get; set; }
        public string ProcName { get; set; }
        public string ParmCompany { get; set; }
        public string ParmDocGUID { get; set; }
        public string ParmDocID { get; set; }
        public string ParmCustomerEmail { get; set; }
        public string ParmMarketingUser { get; set; }
        public string ParmSubmitPurchaseUser { get; set; }
        public string ParmPurchaseDate { get; set; }
        public string ParmCustomerName { get; set; }
        public string ParmPurchaseId { get; set; }

        // Start workflow parameters
        public string ActivityName { get; set; }
        public string ActionName { get; set; }

        public byte[] RowVersion { get; set; }
    }
}
