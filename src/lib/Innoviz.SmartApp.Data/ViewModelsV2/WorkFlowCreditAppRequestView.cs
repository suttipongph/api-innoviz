﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class WorkFlowCreditAppRequestView : ViewCompanyBaseEntity
    {
        public WorkflowInstance workflowInstance { get; set; }
        public string WorkFlowCreditAppRequestGUID { get; set; }
        public string ActionName { get; set; }
        public string ActionRemark { get; set; }
        public string AssistMD { get; set; }
        public string CreditAppRequestId { get; set; }
        public int CreditAppRequestType { get; set; }
        public string Description { get; set; }
        public string DocumentStatus_StatusId { get; set; }
        public string CreditAppRequestTableGUID { get; set; }
        public string CustomerTable_Values { get; set; }
        public string MarketingHead { get; set; }
        public int ProductType { get; set; }
        public string ParmProductType { get; set; }
        public string RefCreditAppTableGUID { get; set; }
        public string ParmFolio { get; set; }
        public int ProcessInstanceId { get; set; }
        public string CustomerEmail { get; set; }
        
    }
}
