﻿using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class UpdateClosingResultView
    {
        public string CreditAppRequestTableGUID { get; set; }
        public int ApprovalDecision { get; set; }
        public string CreditAppRequestTable_Values { get; set; }
        public string DocumentRemark { get; set; }
        public string DocumentReasonGUID { get; set; }
        public string ClosingDate { get; set; }
    }
    public class UpdateClosingResultResultView : ResultBaseEntity
    {
    }
}


