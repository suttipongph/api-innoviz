using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class GuarantorTypeListView : ViewCompanyBaseEntity
	{
		public string GuarantorTypeGUID { get; set; }
		public string GuarantorTypeId { get; set; }
		public string Description { get; set; }
	}
}
