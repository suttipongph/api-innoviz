using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class GuarantorTypeItemView : ViewCompanyBaseEntity
	{
		public string GuarantorTypeGUID { get; set; }
		public string Description { get; set; }
		public string GuarantorTypeId { get; set; }
	}
}
