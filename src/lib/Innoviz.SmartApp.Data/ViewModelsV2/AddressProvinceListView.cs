using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class AddressProvinceListView : ViewCompanyBaseEntity
	{
		public string AddressProvinceGUID { get; set; }
		public string AddressCountryGUID { get; set; }
		public string Name { get; set; }
		public string ProvinceId { get; set; }
		public string CountryId { get; set; }
		public string AddressCountry_Name { get; set; }
	}
}
