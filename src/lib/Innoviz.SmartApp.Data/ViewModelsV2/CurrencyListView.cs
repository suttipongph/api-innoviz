using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CurrencyListView : ViewCompanyBaseEntity
	{
		public string CurrencyGUID { get; set; }
		public string CurrencyId { get; set; }
		public string Name { get; set; }
	}
}
