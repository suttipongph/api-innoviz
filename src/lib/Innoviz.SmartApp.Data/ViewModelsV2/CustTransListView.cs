using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CustTransListView : ViewBranchCompanyBaseEntity
	{
		public string CustTransGUID { get; set; }
		public string InvoiceTableGUID { get; set; }
		public int CustTransStatus { get; set; }
		public string CustomerTableGUID { get; set; }
		public string BuyerTableGUID { get; set; }
		public int ProductType { get; set; }
		public string InvoiceTypeGUID { get; set; }
		public string DueDate { get; set; }
		public string CurrencyGUID { get; set; }
		public decimal TransAmount { get; set; }
		public decimal SettleAmount { get; set; }
		public string InvoiceTable_Values { get; set; }
		public string InvoiceTable_InvoiceId { get; set; }
		public string CustomerTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string InvoiceType_Values { get; set; }
		public string Currency_Values { get; set; }
		public string CustomerId { get; set; }
		public string CustomerName { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string InvoiceType_InvoiceTypeId { get; set; }
		public string Currency_CurrencyId { get; set; }
		public string CreditApp_CreditAppId { get; set; }
	}
}
