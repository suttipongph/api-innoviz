using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BankGroupItemView : ViewCompanyBaseEntity
	{
		public string BankGroupGUID { get; set; }
		public string Alias { get; set; }
		public string BankGroupId { get; set; }
		public string Description { get; set; }
	}
}
