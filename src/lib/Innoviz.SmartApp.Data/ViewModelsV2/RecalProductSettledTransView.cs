﻿using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class RecalProductSettledTransView : ViewCompanyBaseEntity
    {
        public string ProductSettledTransGUID { get; set; }
        public string DocumentId { get; set; }
        public string InvoiceSettlementDetailGUID { get; set; }
        public string InvoiceTable_Values { get; set; }
    }

public class RecalProductSettledTransResultView : ResultBaseEntity
    {

    }
}

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class RecalProductSettledTransViewMap
    {
        public ProductSettledTrans ProductSettledTrans { get; set; }
    }
}