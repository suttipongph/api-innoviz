using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BuyerAgreementLineItemView : ViewCompanyBaseEntity
	{
		public string BuyerAgreementLineGUID { get; set; }
		public bool AlreadyInvoiced { get; set; }
		public decimal Amount { get; set; }
		public string BuyerAgreementTableGUID { get; set; }
		public string Description { get; set; }
		public string DueDate { get; set; }
		public int Period { get; set; }
		public string Remark { get; set; }
		public string BuyerAgreementTable_Values { get; set; }
	}
}
