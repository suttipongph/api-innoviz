using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CreditAppReqBusinessCollateralListView : ViewCompanyBaseEntity
	{
		public string CreditAppReqBusinessCollateralGUID { get; set; }
		public bool IsNew { get; set; }
		public decimal BusinessCollateralValue { get; set; }
		public decimal CapitalValuation { get; set; }
		public string CustBusinessCollateralGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public string CreditAppRequestTableGUID { get; set; }
		public string BusinessCollateralTypeGUID { get; set; }
		public string BusinessCollateralSubTypeGUID { get; set; }
		public string Description { get; set; }
		public string RefAgreementId { get; set; }
		public string RefAgreementDate { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CreditAppRequestTable_Values { get; set; }
		public string CustBusinessCollateral_Values { get; set; }
		public string BusinessCollateralType_Values { get; set; }
		public string BusinessCollateralSubType_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string CreditAppRequestTable_CreditAppRequestId { get; set; }
		public string CustBusinessCollateral_CustBusinessCollateralId { get; set; }
		public string BusinessCollateralType_BusinessCollateralTypeId { get; set; }
		public string BusinessCollateralSubType_BusinessCollateralSubTypeId { get; set; }
	}
}
