using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ExposureGroupByProductListView : ViewCompanyBaseEntity
	{
		public string ExposureGroupByProductGUID { get; set; }
		public string ExposureGroupGUID { get; set; }
		public int ProductType { get; set; }
		public decimal ExposureAmount { get; set; }
		public string DocumentConditionTemplateTable_Values { get; set; }
		public string ExposureGroup_ExposureGroupId { get; set; }
	}
}
