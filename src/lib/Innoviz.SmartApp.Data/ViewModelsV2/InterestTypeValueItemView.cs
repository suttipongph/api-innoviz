using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class InterestTypeValueItemView : ViewDateEffectiveBaseEntity
	{
		public string InterestTypeValueGUID { get; set; }
		public string InterestTypeGUID { get; set; }
		public decimal Value { get; set; }
		public string InterestType_Values { get; set; }

	}
}
