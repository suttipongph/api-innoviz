using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CustBankItemView : ViewCompanyBaseEntity
	{
		public string CustBankGUID { get; set; }
		public string AccountNumber { get; set; }
		public bool BankAccountControl { get; set; }
		public string BankAccountName { get; set; }
		public string BankBranch { get; set; }
		public string BankGroupGUID { get; set; }
		public string BankTypeGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public bool InActive { get; set; }
		public bool PDC { get; set; }
		public bool Primary { get; set; }
		public string CustomerTable_Values { get; set; }
	}
}
