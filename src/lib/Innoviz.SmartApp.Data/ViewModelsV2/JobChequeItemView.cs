using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class JobChequeItemView : ViewCompanyBaseEntity
	{
		public string JobChequeGUID { get; set; }
		public decimal Amount { get; set; }
		public string ChequeBankAccNo { get; set; }
		public string ChequeBankGroupGUID { get; set; }
		public string ChequeBranch { get; set; }
		public string ChequeDate { get; set; }
		public string ChequeNo { get; set; }
		public int ChequeSource { get; set; }
		public string ChequeTableGUID { get; set; }
		public string CollectionFollowUpGUID { get; set; }
		public string MessengerJobTableGUID { get; set; }
		public string RecipientName { get; set; }
		public string MessengerJobTable_CustomerTableGUID { get; set; }
		public string MessengerJobTable_BuyerTableGUID { get; set; }
		public string MessengerJobTable_RefGUID { get; set; }
	}
}
