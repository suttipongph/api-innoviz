﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class UpdateVerificationStatusParamView
    {
        public string updateVerificationStatusGUID { get; set; }
        public string documentReasonGUID { get; set; }
        public string documentStatusGUID { get; set; }
        public string originalDocumentStatusGUID { get; set; }
        public string reasonRemark { get; set; }
    }
    public class UpdateVerificationStatusView : ResultBaseEntity
    {
        public string updateVerificationStatusGUID { get; set; }
        public string documentReasonGUID { get; set; }
        public string documentStatusGUID { get; set; }
        public string originalDocumentStatusGUID { get; set; }
        public string reasonRemark { get; set; }
        public string verificationTableGUID { get; set; }
        public string verificationId { get; set; }
        public string documentStatus_Values { get; set; }
    }
}
