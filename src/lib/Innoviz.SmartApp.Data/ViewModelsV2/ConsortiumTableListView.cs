using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ConsortiumTableListView : ViewCompanyBaseEntity
	{
		public string ConsortiumTableGUID { get; set; }
		public string ConsortiumId { get; set; }
		public string Description { get; set; }
	}
}
