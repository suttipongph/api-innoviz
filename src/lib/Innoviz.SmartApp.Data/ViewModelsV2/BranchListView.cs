using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BranchListView : ViewCompanyBaseEntity
	{
		public string BranchGUID { get; set; }
		public string BranchId { get; set; }
		public string TaxBranchGUID { get; set; }
		public string Name { get; set; }
	}
}
