using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ProductSubTypeItemView : ViewCompanyBaseEntity
	{
		public string ProductSubTypeGUID { get; set; }
		public string Description { get; set; }
		public int GuarantorAgreementYear { get; set; }
		public decimal MaxInterestFeePct  { get; set; }
		public string ProductSubTypeId { get; set; }
		public int ProductType { get; set; }
		public int CalcInterestMethod { get; set; }
		public int CalcInterestDayMethod { get; set; }
	}
}
