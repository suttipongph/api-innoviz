using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CreditAppTableListView : ViewCompanyBaseEntity
	{
		public string CreditAppTableGUID { get; set; }
		public string CreditAppId { get; set; }
		public string ExpiryDate { get; set; }
		public string CustomerTableGUID { get; set; }
		public int ProductType { get; set; }
		public string CreditLimitTypeGUID { get; set; }
		public string ApprovedDate { get; set; }
		public string RefCreditAppRequestTableGUID { get; set; }
		public decimal ApprovedCreditLimit { get; set; }
		public string InactiveDate { get; set; }
		public string StartDate { get; set; }
		public string CreditLimitType_Values { get; set; }
		public string CreditAppRequestTable_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string RefCreditAppRequestTable_Values { get; set; }
		public string RefCreditAppRequestTable_CreditAppRequestId { get; set; }
		public string CreditLimitType_CreditLimitTypeId { get; set; }
		public string CustomerTable_CustomerId { get; set; }


	}
}
