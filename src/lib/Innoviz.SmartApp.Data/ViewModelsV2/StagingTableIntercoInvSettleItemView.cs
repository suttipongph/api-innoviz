using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class StagingTableIntercoInvSettleItemView : ViewCompanyBaseEntity
	{
		public string StagingTableIntercoInvSettleGUID { get; set; }
		public string Address1 { get; set; }
		public string AltName { get; set; }
		public string CNReasonId { get; set; }
		public string CompanyTaxBranchId { get; set; }
		public string CountryId { get; set; }
		public string CurrencyId { get; set; }
		public string CustFaxValue { get; set; }
		public string CustGroupId { get; set; }
		public string CustomerId { get; set; }
		public string CustPhoneValue { get; set; }
		public string DimensionCode1 { get; set; }
		public string DimensionCode2 { get; set; }
		public string DimensionCode3 { get; set; }
		public string DimensionCode4 { get; set; }
		public string DimensionCode5 { get; set; }
		public string DistrictId { get; set; }
		public string DueDate { get; set; }
		public string FeeLedgerAccount { get; set; }
		public string IntercompanyInvoiceSettlementGUID { get; set; }
		public string IntercompanyInvoiceSettlementId { get; set; }
		public string InterfaceStagingBatchId { get; set; }
		public int InterfaceStatus { get; set; }
		public string InvoiceDate { get; set; }
		public string InvoiceText { get; set; }
		public string MethodOfPaymentId { get; set; }
		public string Name { get; set; }
		public decimal OrigInvoiceAmount { get; set; }
		public string OrigInvoiceId { get; set; }
		public string PostalCode { get; set; }
		public string ProvinceId { get; set; }
		public int RecordType { get; set; }
		public decimal SettleInvoiceAmount { get; set; }
		public string SubDistrictId { get; set; }
		public string TaxBranchId { get; set; }
		public string TaxCode { get; set; }
		public string TaxId { get; set; }

		public string StagingTableIntercoInvSettleCompanyId { get; set; }
	}
}
