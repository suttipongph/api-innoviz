﻿using Innoviz.SmartApp.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class MainAgreementJVBookmarkView
    {
        [BookmarkMapping("JointAddressFirst1")]
        public string JointAddressFirst1 { get; set; }
        [BookmarkMapping("JointNameFirst1")]
        public string JointNameFirst1 { get; set; }
        [BookmarkMapping("JointAddressSecond1")]
        public string JointAddressSecond1 { get; set; }
        [BookmarkMapping("JointNameSecond1")]
        public string JointNameSecond1 { get; set; }
        [BookmarkMapping("JointAddressThird1")]
        public string JointAddressThird1 { get; set; }
        [BookmarkMapping("JointNameThird1")]
        public string JointNameThird1 { get; set; }
        [BookmarkMapping("JointAddressFourth1")]
        public string JointAddressFourth1 { get; set; }
        [BookmarkMapping("JointNameFourth1")]
        public string JointNameFourth1 { get; set; }
        [BookmarkMapping("PositionJointFirst1")]
        public string PositionJointFirst1 { get; set; }
        [BookmarkMapping("PositionJointSecond1")]
        public string PositionJointSecond1 { get; set; }
        [BookmarkMapping("PositionJointThird1")]
        public string PositionJointThird1 { get; set; }
        [BookmarkMapping("PositionJointFourth1")]
        public string PositionJointFourth1 { get; set; }
        [BookmarkMapping("JointAddressFirst2")]
        public string JointAddressFirst2 { get; set; }
        [BookmarkMapping("JointNameFirst2")]
        public string JointNameFirst2 { get; set; }
        [BookmarkMapping("JointAddressSecond2")]
        public string JointAddressSecond2 { get; set; }
        [BookmarkMapping("JointNameSecond2")]
        public string JointNameSecond2 { get; set; }
        [BookmarkMapping("JointAddressThird2")]
        public string JointAddressThird2 { get; set; }
        [BookmarkMapping("JointNameThird2")]
        public string JointNameThird2 { get; set; }
        [BookmarkMapping("JointAddressFourth2")]
        public string JointAddressFourth2 { get; set; }
        [BookmarkMapping("JointNameFourth2")]
        public string JointNameFourth2 { get; set; }
        [BookmarkMapping("PositionJointFirst2")]
        public string PositionJointFirst2 { get; set; }
        [BookmarkMapping("PositionJointSecond2")]
        public string PositionJointSecond2 { get; set; }
        [BookmarkMapping("PositionJointThird2")]
        public string PositionJointThird2 { get; set; }
        [BookmarkMapping("PositionJointFourth2")]
        public string PositionJointFourth2 { get; set; }
        [BookmarkMapping("JointAddressFirst3")]
        public string JointAddressFirst3 { get; set; }
        [BookmarkMapping("JointNameFirst3")]
        public string JointNameFirst3 { get; set; }
        [BookmarkMapping("JointAddressSecond3")]
        public string JointAddressSecond3 { get; set; }
        [BookmarkMapping("JointNameSecond3")]
        public string JointNameSecond3 { get; set; }
        [BookmarkMapping("JointAddressThird3")]
        public string JointAddressThird3 { get; set; }
        [BookmarkMapping("JointNameThird3")]
        public string JointNameThird3 { get; set; }
        [BookmarkMapping("JointAddressFourth3")]
        public string JointAddressFourth3 { get; set; }
        [BookmarkMapping("JointNameFourth3")]
        public string JointNameFourth3 { get; set; }
        [BookmarkMapping("PositionJointFirst3")]
        public string PositionJointFirst3 { get; set; }
        [BookmarkMapping("PositionJointSecond3")]
        public string PositionJointSecond3 { get; set; }
        [BookmarkMapping("PositionJointThird3")]
        public string PositionJointThird3 { get; set; }
        [BookmarkMapping("PositionJointFourth3")]
        public string PositionJointFourth3 { get; set; }
        [BookmarkMapping("JointAddressFirst4")]
        public string JointAddressFirst4 { get; set; }
        [BookmarkMapping("JointNameFirst4")]
        public string JointNameFirst4 { get; set; }
        [BookmarkMapping("JointAddressSecond4")]
        public string JointAddressSecond4 { get; set; }
        [BookmarkMapping("JointNameSecond4")]
        public string JointNameSecond4 { get; set; }
        [BookmarkMapping("JointAddressThird4")]
        public string JointAddressThird4 { get; set; }
        [BookmarkMapping("JointNameThird4")]
        public string JointNameThird4 { get; set; }
        [BookmarkMapping("JointAddressFourth4")]
        public string JointAddressFourth4 { get; set; }
        [BookmarkMapping("JointNameFourth4")]
        public string JointNameFourth4 { get; set; }
        [BookmarkMapping("PositionJointFirst4")]
        public string PositionJointFirst4 { get; set; }
        [BookmarkMapping("PositionJointSecond4")]
        public string PositionJointSecond4 { get; set; }
        [BookmarkMapping("PositionJointThird4")]
        public string PositionJointThird4 { get; set; }
        [BookmarkMapping("PositionJointFourth4")]
        public string PositionJointFourth4 { get; set; }
        [BookmarkMapping("TextJVDetail1")]
        public string TextJVDetail1 { get; set; }
        [BookmarkMapping("OperatedbyFirst1")]
        public string OperatedbyFirst1 { get; set; }
        [BookmarkMapping("OperatedbySecond1")]
        public string OperatedbySecond1 { get; set; }
        [BookmarkMapping("OperatedbyThird1")]
        public string OperatedbyThird1 { get; set; }
        [BookmarkMapping("OperatedbyFourth1")]
        public string OperatedbyFourth1 { get; set; }
        public string JointAddress { get; set; }
        public string JointName { get; set; }
        public string PositionJoint { get; set; }
        public string OperatedBy { get; set; }
    }
}
