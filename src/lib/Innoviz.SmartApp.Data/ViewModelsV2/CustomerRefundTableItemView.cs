using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CustomerRefundTableItemView : ViewCompanyBaseEntity
	{
		public string CustomerRefundTableGUID { get; set; }
		public string CreditAppTableGUID { get; set; }
		public string CurrencyGUID { get; set; }
		public string CustomerRefundId { get; set; }
		public string CustomerTableGUID { get; set; }
		public string Description { get; set; }
		public string Dimension1GUID { get; set; }
		public string Dimension2GUID { get; set; }
		public string Dimension3GUID { get; set; }
		public string Dimension4GUID { get; set; }
		public string Dimension5GUID { get; set; }
		public string DocumentReasonGUID { get; set; }
		public string DocumentReason_Values { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public decimal ExchangeRate { get; set; }
		public string OperReportSignatureGUID { get; set; }
		public decimal PaymentAmount { get; set; }
		public int ProductType { get; set; }
		public decimal RetentionAmount { get; set; }
		public int RetentionCalculateBase { get; set; }
		public decimal RetentionPct { get; set; }
		public decimal ServiceFeeAmount { get; set; }
		public decimal SettleAmount { get; set; }
		public decimal SuspenseAmount { get; set; }
		public int SuspenseInvoiceType { get; set; }
		public string TransDate { get; set; }
		public decimal NetAmount { get; set; }
		public string Remark { get; set; }
	}
}
