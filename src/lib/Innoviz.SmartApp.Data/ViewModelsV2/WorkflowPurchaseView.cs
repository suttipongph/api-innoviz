﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class WorkflowPurchaseView : ViewCompanyBaseEntity
    {
        public WorkflowInstance WorkflowInstance { get; set; }
        public string PurchaseTableGUID { get; set; }
        public string ActionName { get; set; }
        public string ActionRemark { get; set; }
        public string AssistMD { get; set; }
        public string ParmFolio { get; set; }
        public int ProcessInstanceId { get; set; }

        public decimal NetPaid { get; set; }
        public bool ParmRollbill { get; set; }
        public decimal ParmSumPurchaseAmount { get; set; }
        public decimal ParmOverCreditBuyer { get; set; }
        public decimal ParmOverCreditCA { get; set; }
        public decimal ParmOverCreditCALine { get; set; }
        public bool ParmDiffChequeIssuedName { get; set; }
        public string ParmCreditAppTableGUID { get; set; }
        public string ParmProductType { get; set; }
        public string ParmDocID { get; set; }
        public string ParmDocumentReasonGUID { get; set; }

        public string CreditApplicationId { get; set; }
        public string CustomerId { get; set; }
        public string PurchaseId { get; set; }
        public string DocumentStatus_StatusId { get; set; }
        public bool Rollbill { get; set; }
        public string DocumentReasonGUID { get; set; }
    }
}

