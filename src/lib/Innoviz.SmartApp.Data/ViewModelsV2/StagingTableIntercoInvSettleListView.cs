using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class StagingTableIntercoInvSettleListView : ViewCompanyBaseEntity
	{
		public string StagingTableIntercoInvSettleGUID { get; set; }
		public string IntercompanyInvoiceSettlementId { get; set; }
		public int InterfaceStatus { get; set; }
		public string InvoiceDate { get; set; }
		public string DueDate { get; set; }
		public string MethodOfPaymentId { get; set; }
		public decimal SettleInvoiceAmount { get; set; }
		public string FeeLedgerAccount { get; set; }
		public string CustomerId { get; set; }
		public string Name { get; set; }
		public string InterfaceStagingBatchId { get; set; }
		public string IntercompanyInvoiceSettlementGUID { get; set; }
	}
}
