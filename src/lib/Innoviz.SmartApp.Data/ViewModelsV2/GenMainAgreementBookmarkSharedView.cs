using System;
using Innoviz.SmartApp.Core.Attributes;
using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class GenMainAgreementBookmarkSharedView
    {
        [BookmarkMapping("ContAmountFirst1")]
        public decimal ContAmountFirst1 { get; set; }

        [BookmarkMapping("ContAmountSecond1")]
        public string ContAmountSecond1 { get; set; }

        public DateTime AgreementDate { get; set; }

        [BookmarkMapping("AgreementDate1")]
        public string AgreementDate1 { get; set; }

        [BookmarkMapping("AgreementDate2")]
        public string AgreementDate2 { get; set; }

        [BookmarkMapping("AgreementDate3")]
        public string AgreementDate3 { get; set; }

        [BookmarkMapping("AgreementDate4")]
        public string AgreementDate4 { get; set; }

        [BookmarkMapping("AgreementDate5")]
        public string AgreementDate5 { get; set; }

        [BookmarkMapping("AgreementNo1")]
        public string AgreementNo1 { get; set; }

        [BookmarkMapping("AgreementNo2")]
        public string AgreementNo2 { get; set; }

        [BookmarkMapping("AgreementNo3")]
        public string AgreementNo3 { get; set; }

        [BookmarkMapping("AgreementNo4")]
        public string AgreementNo4 { get; set; }

        [BookmarkMapping("AgreementNo5")]
        public string AgreementNo5 { get; set; }

        [BookmarkMapping("InterestRate1")]
        public decimal? InterestRate1 { get; set; }

        [BookmarkMapping("AliasNameCust1")]
        public string AliasNameCust1 { get; set; }

        [BookmarkMapping("AliasNameCust2")]
        public string AliasNameCust2 { get; set; }

        [BookmarkMapping("AliasNameCust3")]
        public string AliasNameCust3 { get; set; }

        [BookmarkMapping("AliasNameCust4")]
        public string AliasNameCust4 { get; set; }

        [BookmarkMapping("AliasNameCust5")]
        public string AliasNameCust5 { get; set; }

        [BookmarkMapping("AliasNameCust6")]
        public string AliasNameCust6 { get; set; }

        [BookmarkMapping("AliasNameCust7")]
        public string AliasNameCust7 { get; set; }

        [BookmarkMapping("AliasNameCust8")]
        public string AliasNameCust8 { get; set; }

        [BookmarkMapping("CustName1")]
        public string CustName1 { get; set; }

        [BookmarkMapping("CustName2")]
        public string CustName2 { get; set; }

        [BookmarkMapping("CustName3")]
        public string CustName3 { get; set; }

        [BookmarkMapping("CustName4")]
        public string CustName4 { get; set; }

        [BookmarkMapping("CustName5")]
        public string CustName5 { get; set; }

        [BookmarkMapping("CustName6")]
        public string CustName6 { get; set; }

        [BookmarkMapping("CustName7")]
        public string CustName7 { get; set; }

        [BookmarkMapping("CustName8")]
        public string CustName8 { get; set; }

        [BookmarkMapping("CustTaxID1")]
        public string CustTaxID1 { get; set; }

        [BookmarkMapping("CompanyWitnessFirst1")]
        public string CompanyWitnessFirst1 { get; set; }

        [BookmarkMapping("CompanyWitnessSecond1")]
        public string CompanyWitnessSecond1 { get; set; }

        [BookmarkMapping("Text1")]
        public string Text1 { get; set; }

        [BookmarkMapping("Text2")]
        public string Text2 { get; set; }

        [BookmarkMapping("Text3")]
        public string Text3 { get; set; }

        [BookmarkMapping("Text4")]
        public string Text4 { get; set; }

        [BookmarkMapping("AuthorityPersonFirst1")]
        public string AuthorityPersonFirst1 { get; set; }

        [BookmarkMapping("AuthorityPersonFirst2")]
        public string AuthorityPersonFirst2 { get; set; }

        [BookmarkMapping("AuthorityPersonSecond1")]
        public string AuthorityPersonSecond1 { get; set; }

        [BookmarkMapping("AuthorityPersonSecond2")]
        public string AuthorityPersonSecond2 { get; set; }

        [BookmarkMapping("AuthorityPersonThird1")]
        public string AuthorityPersonThird1 { get; set; }

        [BookmarkMapping("AuthorityPersonThird2")]
        public string AuthorityPersonThird2 { get; set; }

        [BookmarkMapping("AuthorizedBuyerFirst1")]
        public string AuthorizedBuyerFirst1 { get; set; }

        [BookmarkMapping("AuthorizedBuyerFirst2")]
        public string AuthorizedBuyerFirst2 { get; set; }

        [BookmarkMapping("AuthorizedBuyerSecond1")]
        public string AuthorizedBuyerSecond1 { get; set; }

        [BookmarkMapping("AuthorizedBuyerSecond2")]
        public string AuthorizedBuyerSecond2 { get; set; }

        [BookmarkMapping("AuthorizedBuyerThird1")]
        public string AuthorizedBuyerThird1 { get; set; }

        [BookmarkMapping("AuthorizedBuyerThird2")]
        public string AuthorizedBuyerThird2 { get; set; }

        [BookmarkMapping("AuthorizedCustFirst1")]
        public string AuthorizedCustFirst1 { get; set; }

        [BookmarkMapping("AuthorizedCustFirst2")]
        public string AuthorizedCustFirst2 { get; set; }

        [BookmarkMapping("AuthorizedCustSecond1")]
        public string AuthorizedCustSecond1 { get; set; }

        [BookmarkMapping("AuthorizedCustSecond2")]
        public string AuthorizedCustSecond2 { get; set; }

        [BookmarkMapping("AuthorizedCustThird1")]
        public string AuthorizedCustThird1 { get; set; }

        [BookmarkMapping("AuthorizedCustThird2")]
        public string AuthorizedCustThird2 { get; set; }

        [BookmarkMapping("CustWitnessFirst1")]
        public string CustWitnessFirst1 { get; set; }

        [BookmarkMapping("CustWitnessSecond1")]
        public string CustWitnessSecond1 { get; set; }

        [BookmarkMapping("PositionBuyer1")]
        public string PositionBuyer1 { get; set; }

        [BookmarkMapping("PositionCust1")]
        public string PositionCust1 { get; set; }

        [BookmarkMapping("PositionCust2")]
        public string PositionCust2 { get; set; }
        [BookmarkMapping("PositionCust3")]
        public string PositionCust3 { get; set; }
        [BookmarkMapping("PositionLIT1")]
        public string PositionLIT1 { get; set; }
        [BookmarkMapping("PositionLIT2")]
        public string PositionLIT2 { get; set; }
        [BookmarkMapping("PositionLIT3")]
        public string PositionLIT3 { get; set; }
        [BookmarkMapping("CustAddress1")]
        public string CustAddress1 { get; set; }
        [BookmarkMapping("CustAddress2")]
        public string CustAddress2 { get; set; }

        [BookmarkMapping("ManualNo1")]
        public string ManualNo1 { get; set; }
        public string CustomerAltName { get; set; }
        public string CustomerName { get; set; }
        public Guid? WitnessCompany1GUID { get; set; }
        public Guid? WitnessCompany2GUID { get; set; }

    }
}