using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class AuthorizedPersonTransItemView : ViewCompanyBaseEntity
	{
		public string AuthorizedPersonTransGUID { get; set; }
		public string AuthorizedPersonTypeGUID { get; set; }
		public bool InActive { get; set; }
		public string NCBCheckedBy { get; set; }
		public string NCBCheckedDate { get; set; }
		public int Ordering { get; set; }
		public string RefAuthorizedPersonTransGUID { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public string RelatedPersonTableGUID { get; set; }
		public string RefId { get; set; }
		public string RelatedPersonTable_Name { get; set; }
		public int RelatedPersonTable_IdentificationType { get; set; }
		public string RelatedPersonTable_PassportId { get; set; }
		public string RelatedPersonTable_WorkPermitId { get; set; }
		public string RelatedPersonTable_DateOfBirth { get; set; }
		public string RelatedPersonTable_Phone { get; set; }
		public string RelatedPersonTable_Extension { get; set; }
		public string RelatedPersonTable_Fax { get; set; }
		public string RelatedPersonTable_Mobile { get; set; }
		public string RelatedPersonTable_LineId { get; set; }
		public string RelatedPersonTable_Email { get; set; }
		public string RelatedPersonTable_TaxId { get; set; }
		public string RelatedPersonTable_RelatedPersonId { get; set; }
		public string RelatedPersonTable_RaceId { get; set; }
		public string RelatedPersonTable_NationalityId { get; set; }
		public int RelatedPersonTable_Age { get; set; }
		public string RelatedPersonTable_BackgroundSummary { get; set; }
		public bool Replace { get; set; }
		public string ReplacedAuthorizedPersonTransGUID { get; set; }
	}
}
