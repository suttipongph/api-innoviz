﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class UpdateCreditApplicationRequestK2ParmView
    {
        public string CreditAppRequestTableGUID { get; set; }
        public int ProcesInstanceId { get; set; }
        public string StatusId { get; set; }
    }
}
