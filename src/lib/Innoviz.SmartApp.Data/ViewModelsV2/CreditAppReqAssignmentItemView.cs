using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CreditAppReqAssignmentItemView : ViewCompanyBaseEntity
	{
		public string CreditAppReqAssignmentGUID { get; set; }
		public decimal AssignmentAgreementAmount { get; set; }
		public string AssignmentAgreementTableGUID { get; set; }
		public string AssignmentMethodGUID { get; set; }
		public decimal BuyerAgreementAmount { get; set; }
		public string BuyerAgreementTableGUID { get; set; }
		public string BuyerTableGUID { get; set; }
		public string CreditAppRequestTableGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public string Description { get; set; }
		public bool IsNew { get; set; }
		public string ReferenceAgreementId { get; set; }
		public decimal RemainingAmount { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CreditAppRequestTable_Values { get; set; }
	}
}
