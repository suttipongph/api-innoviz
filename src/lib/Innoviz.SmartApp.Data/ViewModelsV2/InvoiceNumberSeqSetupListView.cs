using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class InvoiceNumberSeqSetupListView : ViewCompanyBaseEntity
	{
		public string InvoiceNumberSeqSetupGUID { get; set; }
		public string InvoiceTypeGUID { get; set; }
		public string InvoiceType_Values { get; set; }
		public string InvoiceType_InvoiceTypeId { get; set; }
		public string BranchGUID { get; set; }
		public string Branch_Values { get; set; }
		public string Branch_BranchId { get; set; }
		public string NumberSeqTableGUID { get; set; }
		public string NumberSeqTable_Values { get; set; }
		public string NumberSeqTable_NumberSeqCode { get; set; }
		public int ProductType { get; set; }
	}
}
