using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class RetentionConditionTransItemView : ViewCompanyBaseEntity
	{
		public string RetentionConditionTransGUID { get; set; }
		public int ProductType { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public decimal RetentionAmount { get; set; }
		public int RetentionCalculateBase { get; set; }
		public int RetentionDeductionMethod { get; set; }
		public decimal RetentionPct { get; set; }
		public string RefId { get; set; }
	}
}
