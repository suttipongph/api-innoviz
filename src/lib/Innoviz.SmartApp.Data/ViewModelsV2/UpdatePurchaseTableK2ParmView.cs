﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class UpdatePurchaseTableK2ParmView
    {
        public string PurchaseTableGUID { get; set; }
        public string StatusId { get; set; }
        public string ProcessInstanceId { get; set; }
        public string DocumentReasonGUID { get; set; }
    }
}

