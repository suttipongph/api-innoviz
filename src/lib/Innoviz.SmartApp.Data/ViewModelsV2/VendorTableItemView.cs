using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class VendorTableItemView : ViewCompanyBaseEntity
	{
		public string VendorTableGUID { get; set; }
		public string AltName { get; set; }
		public string CurrencyGUID { get; set; }
		public string ExternalCode { get; set; }
		public string Name { get; set; }
		public int RecordType { get; set; }
		public bool SentToOtherSystem { get; set; }
		public string TaxId { get; set; }
		public string VendGroupGUID { get; set; }
		public string VendorId { get; set; }
	}
}
