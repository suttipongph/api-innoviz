using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class EmployeeTableListView : ViewCompanyBaseEntity
	{
		public string EmployeeTableGUID { get; set; }
		public string EmployeeId { get; set; }
		public string Name { get; set; }
		public string EmplTeam_TeamId { get; set; }
		public string CollectionGroupGUID { get; set; }
		public string CollectionGroup_Values { get; set; }
		public string CollectionGroup_CollectionGroupId { get; set; }
		public bool InActive { get; set; }
		public bool AssistMD { get; set; }
		public string SysuserTable_UserName { get; set; }
		public string DepartmentGUID { get; set; }
		public string Department_Values { get; set; }
		public string Department_DepartmentId { get; set; }
		public string BusinessUnit_Values { get; set; }
		public string BusinessUnit_BusinessUnitId { get; set; }
		public string BusinessUnitGUID { get; set; }
	}
}
