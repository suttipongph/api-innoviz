using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CompanySignatureListView : ViewCompanyBaseEntity
	{
		public string CompanySignatureGUID { get; set; }
		public string EmployeeTableGUID { get; set; }
		public string DefaultBranchGUID { get; set; }
		public string BranchGUID { get; set; }
		public int Ordering { get; set; }
		public int RefType { get; set; }
		//public string CompanyGUID { get; set; }
		public string Branch_Values { get; set; }
		public string Branch_BranchId { get; set; }
		public string EmployeeTable_EmployeeId { get; set; }
		public string EmployeeTable_Name { get; set; }
	}
}
