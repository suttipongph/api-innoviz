using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class PurchaseLineListView : ViewCompanyBaseEntity
	{
		public string PurchaseLineGUID { get; set; }
		public int LineNum { get; set; }
		public string BuyerTableGUID { get; set; }
		public string BuyerInvoiceTableGUID { get; set; }
		public string DueDate { get; set; }
		public string BillingDate { get; set; }
		public string CollectionDate { get; set; }
		public decimal BuyerInvoiceAmount { get; set; }
		public decimal PurchaseAmount { get; set; }
		public string MethodOfPaymentGUID { get; set; }
		public string BuyerTable_Values { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string MethodOfPayment_MethodOfPaymentId { get; set; }
		public string BuyerInvoiceTable_Values { get; set; }
		public string BuyerInvoiceTable_BuyerInvoiceId { get; set; }
		public string PurchaseTableGUID { get; set; }
	}
}
