using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ReceiptLineListView : ViewBranchCompanyBaseEntity
	{
		public string ReceiptLineGUID { get; set; }
		public int LineNum { get; set; }
		public string InvoiceTableGUID { get; set; }
		public decimal InvoiceAmount { get; set; }
		public decimal SettleBaseAmount { get; set; }
		public decimal SettleTaxAmount { get; set; }
		public decimal SettleAmount { get; set; }
		public decimal WHTAmount { get; set; }
		public string InvoiceTable_Values { get; set; }
		public string ReceiptTableGUID { get; set; }

		public string InvoiceTable_InvoiceId { get; set; }
	}
}
