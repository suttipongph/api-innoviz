using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ContactPersonTransListView : ViewCompanyBaseEntity
	{
		public string ContactPersonTransGUID { get; set; }
		public bool InActive { get; set; }
		public string RelatedPersonTable_Name { get; set; }
		public string RelatedPersonTable_TaxId { get; set; }
		public string RelatedPersonTable_PassportId { get; set; }
		public string RelatedPersonTable_Phone { get; set; }
		public string RelatedPersonTable_Email { get; set; }
		public string RelatedPersonTable_Extension { get; set; }
		public string RelatedPersonTable_Fax { get; set; }
		public string RelatedPersonTable_Mobile { get; set; }
		public string RelatedPersonTable_Position { get; set; }
		public string RelatedPersonTable_RefId { get; set; }
		public string RelatedPersonTable_WorkPermitId { get; set; }
		public string RefGUID { get; set; }

	}
}
