using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class DocumentReturnMethodItemView : ViewCompanyBaseEntity
	{
		public string DocumentReturnMethodGUID { get; set; }
		public string Description { get; set; }
		public string DocumentReturnMethodId { get; set; }
	}
}
