using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CreditAppRequestTableListView : ViewCompanyBaseEntity
	{
		public string CreditAppRequestTableGUID { get; set; }
		public string CreditAppRequestId { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public int ProductType { get; set; }
		public int CreditAppRequestType { get; set; }
		public string CreditLimitTypeGUID { get; set; }
		public string RequestDate { get; set; }
		public decimal CreditLimitRequest { get; set; }
		public decimal ApprovedCreditLimitRequest { get; set; }
		public string CreditLimitType_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string CreditLimitType_CreditLimitTypeId { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string RefCreditAppTableGUID { get; set; }
		#region For ReviewCreditAppRequest
		public string CreditAppLineGUID { get; set; }
		public string BuyerTable_Values { get; set; }
		public string CreditAppRequestLine_BuyerId { get; set; }
		public string CreditAppRequestLine_BuyerTableGUID { get; set; }
		public string RefCreditAppTable_CreditAppId { get; set; }
		public string RefCreditAppTable_Values { get; set; }
		public decimal BuyerCreditLimit { get; set; }
		public decimal CreditLimitLineRequest { get; set; }
		#endregion For ReviewCreditAppRequest
	}
}
