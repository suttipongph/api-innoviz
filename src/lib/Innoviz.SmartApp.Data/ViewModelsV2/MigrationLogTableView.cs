﻿using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class MigrationLogTableView : ViewBaseEntity
    {
        public int Id { get; set; }
        public string MigrationName { get; set; }
        public string LogStartDateTime { get; set; }
        public string LogEndDateTime { get; set; }
        public string LogMessage { get; set; }
    
    }
}
