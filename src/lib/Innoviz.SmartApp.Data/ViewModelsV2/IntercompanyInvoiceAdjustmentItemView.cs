using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class IntercompanyInvoiceAdjustmentItemView : ViewCompanyBaseEntity
	{
		public string IntercompanyInvoiceAdjustmentGUID { get; set; }
		public decimal Adjustment { get; set; }
		public string DocumentReasonGUID { get; set; }
		public string IntercompanyInvoiceTableGUID { get; set; }
		public decimal OriginalAmount { get; set; }
		public decimal OrigInvoiceAmount { get; set; }
		
		public string IntercompanyInvoice_Values { get; set; }
		public string DocumentReason_Values { get; set; }
		public string RefGUID { get; set; }
		public decimal Balance { get; set; }
		public NotificationResponse Notification { get; set; } = new NotificationResponse();
	

	}
}
