﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class ActionHistoryListView : ViewCompanyBaseEntity
    {
        public string ActionHistoryGUID { get; set; }
        public string ActivityName { get; set; }
        public string ActionName { get; set; }
        public string Comment { get; set; }
        public string RefGUID { get; set; }
    }
}
