using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class JointVentureTransListView : ViewCompanyBaseEntity
	{
		public string JointVentureTransGUID { get; set; }
		public int Ordering { get; set; }
		public string AuthorizedPersonTypeGUID { get; set; }
		public string Name { get; set; }
		public string OperatedBy { get; set; }
		public string AuthorizedPersonType_Values { get; set; }
		public string AuthorizedPersonType_AuthorizedPersonTypeId { get; set; }
		public string RefGUID { get; set; }
	public string	DocumentStatus_StatusCode { get; set; }
	}
}
