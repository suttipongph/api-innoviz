using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BuyerAgreementTransListView : ViewCompanyBaseEntity
	{
		public string BuyerAgreementTransGUID { get; set; }
		public string BuyerAgreementTableGUID { get; set; }
		public string BuyerAgreementLineGUID { get; set; }
		public string BuyerAgreementTable_Values { get; set; }
		public string BuyerAgreementLine_Values { get; set; }
		public string BuyerAgreementTable_BuyerAgreementId { get; set; }
		public int BuyerAgreementLine_Period { get; set; }
		public string BuyerAgreementLine_DueDate { get; set; }
		public decimal BuyerAgreementLine_Amount { get; set; }
		public string RefGUID { get; set; }
	}
}
