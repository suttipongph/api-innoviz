using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class AgreementTableInfoTextItemView : ViewCompanyBaseEntity
	{
		public string AgreementTableInfoTextGUID { get; set; }
		public string AgreementTableInfoGUID { get; set; }
		public string Text1 { get; set; }
		public string Text2 { get; set; }
		public string Text3 { get; set; }
		public string Text4 { get; set; }
	}
}
