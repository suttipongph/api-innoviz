﻿using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class RecalWithdrawalInterestParamView
    {
        public string WithdrawalTableGUID { get; set; }
        public string CreditTermGUID { get; set; }
        public string DueDate { get; set; }
        public decimal TotalInterestPct { get; set; }
        public decimal WithdrawalAmount { get; set; }
        public string WithdrawalDate { get; set; }
        public string WithdrawalId { get; set; }
    }
    public class RecalWithdrawalInterestResultView : ResultBaseEntity
    {

    }
}
namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class RecalWithdrawalInterestViewMap
    {
        public WithdrawalTable WithdrawalTable { get; set; }
        public List<WithdrawalLine> WithdrawalLines { get; set; }
        public List<WithdrawalLine> OrigWithdrawalLines { get; set; }
    }
}