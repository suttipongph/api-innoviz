using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class VendorPaymentTransItemView : ViewCompanyBaseEntity
    {
        public string VendorPaymentTransGUID { get; set; }
        public decimal AmountBeforeTax { get; set; }
        public string CreditAppTableGUID { get; set; }
        public string Dimension1 { get; set; }
        public string Dimension2 { get; set; }
        public string Dimension3 { get; set; }
        public string Dimension4 { get; set; }
        public string Dimension5 { get; set; }
        public string DocumentStatusGUID { get; set; }
        public string OffsetAccount { get; set; }
        public string PaymentDate { get; set; }
        public string RefGUID { get; set; }
        public int RefType { get; set; }
        public decimal TaxAmount { get; set; }
        public string TaxTableGUID { get; set; }
        public decimal TotalAmount { get; set; }
        public string VendorTableGUID { get; set; }
        public string VendorTable_VendorId { get; set; }
        public string CreditAppTable_Values { get; set; }
        public string VendorTable_Values { get; set; }
        public string DocumentStatus_Values { get; set; }
        public string Dimension1_Values { get; set; }
        public string Dimension2_Values { get; set; }
        public string Dimension3_Values { get; set; }
        public string Dimension4_Values { get; set; }
        public string Dimension5_Values { get; set; }
        public string RefId { get; set; }
        public string VendorTaxInvoiceId { get; set; }
    }
}
