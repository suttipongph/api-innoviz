using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class ChequeTableListView : ViewCompanyBaseEntity
    {
        public string ChequeTableGUID { get; set; }
        public string CustomerTableGUID { get; set; }
        public string DocumentStatusGUID { get; set; }
        public string BuyerTableGUID { get; set; }
        public string ChequeDate { get; set; }
        public string ChequeNo { get; set; }
        public string ChequeBankAccNo { get; set; }
        public bool PDC { get; set; }
        public decimal Amount { get; set; }
        public string IssuedName { get; set; }
        public string RecipientName { get; set; }
        public string CustomerTable_Values { get; set; }
        public string BuyerTable_Values { get; set; }
        public string DocumentStatus_Values { get; set; }
        public string CustomerTable_CustomerId { get; set; }
        public string DocumentStatus_StatusCode { get; set; }
        public string RefGUID { get; set; }
        public int RefType { get; set; }
        public string DocumentStatus_StatusId { get; set; }
        public string PurchaseLine_Value {get;set;}
        public string WithdrawalLine_Value { get; set; }
    }
}
