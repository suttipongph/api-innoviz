using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BusinessCollateralAgmTableListView : ViewCompanyBaseEntity
	{
		public string BusinessCollateralAgmTableGUID { get; set; }
		public string InternalBusinessCollateralAgmId { get; set; }
		public string BusinessCollateralAgmId { get; set; }
		public string Description { get; set; }
		public int AgreementDocType { get; set; }
		public string CustomerTableGUID { get; set; }
		public string SigningDate { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string CreditAppRequestTableGUID { get; set; }
		public string CustomerTable_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string CreditAppRequestTable_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string CreditAppRequestTable_CreditAppRequestId { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string DocumentStatus_Description { get; set; }
		public int ProductType { get; set; }
		public string RefBusinessCollateralAgmTableGUID { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string CreditAppTable_CreditAppId { get; set; }
	}
}
