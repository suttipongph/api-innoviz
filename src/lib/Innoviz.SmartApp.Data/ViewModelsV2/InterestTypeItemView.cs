using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class InterestTypeItemView : ViewCompanyBaseEntity
	{
		public string InterestTypeGUID { get; set; }
		public string Description { get; set; }
		public string InterestTypeId { get; set; }
	}
}
