﻿using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class PostWithdrawalView
    {
        public string WithdrawalTableGUID { get; set; }
        public string WithdrawalTable_Values { get; set; }
        public string DocumentStatus_Values { get; set; }
        public string CustomerTable_Values { get; set; }
        public string BuyerTable_Values { get; set; }
        public bool TermExtension { get; set; }
        public int NumberOfExtension { get; set; }
        public string WithdrawalDate { get; set; }
        public string DueDate { get; set; }
        public string CreditTerm_Values { get; set; }
        public decimal TotalInterestPct { get; set; }
        public decimal WithdrawalAmount { get; set; }
    }
    public class PostWithdrawalResultView : ResultBaseEntity
    {
    }
}

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class PostWithdrawalViewMap
    {
        public WithdrawalTable WithdrawalTable { get; set; }
        public List<WithdrawalLine> WithdrawalLine { get; set; }
        public ReceiptTempTable ReceiptTempTable { get; set; }
        public List<InvoiceTable> InvoiceTable { get; set; }
        public List<InvoiceLine> InvoiceLine { get; set; }
        public List<CustTrans> CustTrans { get; set; }
        public List<CustTrans> CustTrans_Update { get; set; }
        public List<CreditAppTrans> CreditAppTrans { get; set; }
        public List<ProcessTrans> ProcessTrans { get; set; }
        public List<RetentionTrans> RetentionTrans { get; set; }
        public List<VendorPaymentTrans> VendorPaymentTrans { get; set; }
        public List<InvoiceSettlementDetail> InvoiceSettlementDetail { get; set; }
        public List<InterestRealizedTrans> InterestRealizedTrans { get; set; }
        public List<ReceiptTempPaymDetail> ReceiptTempPaymDetail { get; set; }
        public List<ReceiptTable> ReceiptTable { get; set; }
        public List<ReceiptLine> ReceiptLine { get; set; }
        public List<TaxInvoiceTable> TaxInvoiceTable { get; set; }
        public List<TaxInvoiceLine> TaxInvoiceLine { get; set; }
        public List<PaymentHistory> PaymentHistory { get; set; }
        public List<IntercompanyInvoiceTable> IntercompanyInvoiceTable { get; set; }
    }
}
