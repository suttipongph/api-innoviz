using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class LineOfBusinessListView : ViewCompanyBaseEntity
	{
		public string LineOfBusinessGUID { get; set; }
		public string Description { get; set; }
		public string LineOfBusinessId { get; set; }
	}
}
