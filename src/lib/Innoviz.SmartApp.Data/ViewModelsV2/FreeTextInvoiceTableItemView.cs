using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class FreeTextInvoiceTableItemView : ViewCompanyBaseEntity
	{
		public string FreeTextInvoiceTableGUID { get; set; }
		public string CNReasonGUID { get; set; }
		public string CreditAppTableGUID { get; set; }
		public string CurrencyGUID { get; set; }
		public string CustomerName { get; set; }
		public string CustomerTableGUID { get; set; }
		public string Dimension1GUID { get; set; }
		public string Dimension2GUID { get; set; }
		public string Dimension3GUID { get; set; }
		public string Dimension4GUID { get; set; }
		public string Dimension5GUID { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string DueDate { get; set; }
		public decimal ExchangeRate { get; set; }
		public string FreeTextInvoiceId { get; set; }
		public string InvoiceAddress1 { get; set; }
		public string InvoiceAddress2 { get; set; }
		public string InvoiceAddressGUID { get; set; }
		public decimal InvoiceAmount { get; set; }
		public decimal InvoiceAmountBeforeTax { get; set; }
		public string InvoiceRevenueTypeGUID { get; set; }
		public string InvoiceTableGUID { get; set; }
		public string InvoiceText { get; set; }
		public string InvoiceTypeGUID { get; set; }
		public string IssuedDate { get; set; }
		public string MailingInvoiceAddress1 { get; set; }
		public string MailingInvoiceAddress2 { get; set; }
		public string MailingInvoiceAddressGUID { get; set; }
		public decimal OrigInvoiceAmount { get; set; }
		public string OrigInvoiceId { get; set; }
		public decimal OrigTaxInvoiceAmount { get; set; }
		public string OrigTaxInvoiceId { get; set; }
		public int ProductType { get; set; }
		public string Remark { get; set; }
		public decimal TaxAmount { get; set; }
		public string TaxBranchId { get; set; }
		public string TaxBranchName { get; set; }
		public string TaxTableGUID { get; set; }
		public decimal WHTAmount { get; set; }
		public string WithholdingTaxTableGUID { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string DocumentStatus_Description { get; set; }
		public string TaxValueError { get; set; }
		public string WithholdingTaxValueError { get; set; }
		public string InvoiceTable_Values { get; set; }
	}
}
