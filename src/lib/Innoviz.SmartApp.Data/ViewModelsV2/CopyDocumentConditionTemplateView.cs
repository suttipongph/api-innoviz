﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
   public class CopyDocumentConditionTemplateParamView
    {
        public string DocumentConditionTemplateTableGUID { get; set; }
        public string RefGUID { get; set; }
        public int RefType { get; set; }
        public int DocConVerifyType { get; set; }
        public string CallerLabel { get; set; }
        public string DocConVerifyTypeLabel { get; set; }

    }
    public class CopyDocumentConditionTemplateResultView : ResultBaseEntity
    {
        public string DocumentConditionTamplateTableGUID { get; set; }
    }
}
