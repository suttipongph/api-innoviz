﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class UpdateCustomerTableBlacklistStatusParamView
    {
        public string CustomerTableGUID { get; set; }
        public string BlacklistStatusGUID { get; set; }
        public string DocumentReasonGUID { get; set; }
        public string ReasonRemark { get; set; }
    }
    public class UpdateCustomerTableBlacklistStatusResultView : ResultBaseEntity
    {
        public string CustomerTableGUID { get; set; }
        public string OriginalBlacklistStatus { get; set; }
        public string OriginalBlacklistStatusGUID { get; set; }
        public string BlacklistStatusGUID { get; set; }
        public string DocumentReasonGUID { get; set; }
        public string CustomerTableId { get; set; }
        public string ReasonRemark { get; set; }
        public string ResultLabel { get; set; }
    }
}
