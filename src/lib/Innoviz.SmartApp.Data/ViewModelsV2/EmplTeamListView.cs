using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class EmplTeamListView : ViewCompanyBaseEntity
	{
		public string EmplTeamGUID { get; set; }
		public string TeamId { get; set; }
		public string Name { get; set; }
	}
}
