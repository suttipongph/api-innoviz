using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class VendorPaymentTransListView : ViewCompanyBaseEntity
	{
		public string VendorPaymentTransGUID { get; set; }
		public string CreditAppTableGUID { get; set; }
		public string VendorTableGUID { get; set; }
		public string PaymentDate { get; set; }
		public string OffsetAccount { get; set; }
		public decimal TotalAmount { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string VendorTable_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string RefGUID { get; set; }
		public string CreditAppTable_CreditAppId { get; set; }
		public string VendorTable_VendorId { get; set; }
		public string DocumentStatus_StatusId { get; set; }
	}
}
