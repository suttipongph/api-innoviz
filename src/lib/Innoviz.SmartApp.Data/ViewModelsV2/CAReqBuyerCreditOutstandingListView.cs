using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CAReqBuyerCreditOutstandingListView : ViewCompanyBaseEntity
	{
		public string CAReqBuyerCreditOutstandingGUID { get; set; }
		public int LineNum { get; set; }
		public string BuyerTableGUID { get; set; }
		public string Status { get; set; }
		public decimal ApprovedCreditLimitLine { get; set; }
		public decimal ARBalance { get; set; }
		public decimal MaxPurchasePct { get; set; }
		public string AssignmentMethodGUID { get; set; }
		public string BillingResponsibleByGUID { get; set; }
		public string MethodOfPaymentGUID { get; set; }
		public string BuyerTable_Values { get; set; }
		public string AssignmentMethod_Values { get; set; }
		public string BillingResponsibleBy_Values { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string AssignmentMethod_AssignmentMethodId { get; set; }
		public string BillingResponsibleBy_BillingResponsibleById { get; set; }
		public string MethodOfPayment_MethodOfPaymentId { get; set; }
		public string CreditAppRequestTableGUID { get; set; }
		public string CreditAppTable_CreditAppId { get; set; }

	}
}
