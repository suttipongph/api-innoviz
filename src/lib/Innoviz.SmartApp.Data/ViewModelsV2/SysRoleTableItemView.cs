﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class SysRoleTableItemView : ViewCompanyBaseEntity
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string NormalizedName { get; set; }
        public string DisplayName { get; set; }

        public int SiteLoginType { get; set; }

        public IEnumerable<SysFeatureGroupRoleView> SysFeatureGroupRoleViewList { get; set; }
    }
}
