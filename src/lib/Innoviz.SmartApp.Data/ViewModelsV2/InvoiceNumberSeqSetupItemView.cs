using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class InvoiceNumberSeqSetupItemView : ViewCompanyBaseEntity
	{
		public string InvoiceNumberSeqSetupGUID { get; set; }
		public string InvoiceTypeGUID { get; set; }
		public int ProductType { get; set; }
		public string NumberSeqTableGUID { get; set; }
		public string BranchGUID { get; set; }
	}
}
