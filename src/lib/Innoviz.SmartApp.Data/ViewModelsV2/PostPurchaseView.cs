﻿using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class PostPurchaseParamView
    {
        public string PurchaseTableGUID { get; set; }
        public string PurchaseId { get; set; }
        public string PurchaseDate { get; set; }
        public string DocumentStatus { get; set; }
        public string CustomerId { get; set; }
        public bool Rollbill { get; set; }
        public bool AdditionalPurchase { get; set; }
    }
    public class PostPurchaseResultView: ResultBaseEntity
    {

    }
}
namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class PostPurchaseResultViewMap
    {
        public ReceiptTempTable ReceiptTempTable { get; set; }
        public PurchaseTable PurchaseTable { get; set; }
        public CreditAppTable CreditAppTable { get; set; }
        public List<PurchaseLine> PurchaseLine { get; set; }
        public List<InvoiceTable> InvoiceTable { get; set; }
        public List<InvoiceLine> InvoiceLine { get; set; }
        public List<InvoiceSettlementDetail> InvoiceSettlementDetail { get; set; }
        public List<InterestRealizedTrans> InterestRealizedTransCreate { get; set; }
        public List<InterestRealizedTrans> InterestRealizedTransUpdate { get; set; }
        public List<VendorPaymentTrans> VendorPaymentTrans { get; set; }
        public List<ProcessTrans> ProcessTrans { get; set; }
        public List<ReceiptTempPaymDetail> ReceiptTempPaymDetail { get; set; }
        public List<CustTrans> CustTransCreate { get; set; }
        public List<CustTrans> CustTransUpdate { get; set; }
        public List<CreditAppTrans> CreditAppTrans { get; set; }
        public List<TaxInvoiceTable> TaxInvoiceTable { get; set; }
        public List<TaxInvoiceLine> TaxInvoiceLine { get; set; }
        public List<RetentionTrans> RetentionTrans { get; set; }
        public List<ReceiptTable> ReceiptTable { get; set; }
        public List<ReceiptLine> ReceiptLine { get; set; }
        public List<PaymentHistory> PaymentHistory { get; set; }
        public List<IntercompanyInvoiceTable> IntercompanyInvoiceTable { get; set; }
    }
    public class PostPurchaseParamViewMap
    {
        public PurchaseTable PurchaseTable { get; set; }
        public PurchaseLine PurchaseLineAdditionalPurchase { get; set; }
        public List<PurchaseLine> RefPurchaseLine { get; set; }
        public decimal? RefPurchaseLineSettleAmount { get; set; }
        public CreditAppTable CreditAppTable { get; set; }
        public decimal RetentionBalance { get; set; }
        public decimal PurchaseRetentionAmount { get; set; }
        public decimal SumSuspenseSettleAmount { get; set; }
        public List<InvoiceSettlementDetail> PurchaseInvoiceSettlementDetail { get; set; }
        public decimal SumPurchaseLineSettleAmount { get; set; }
        public decimal SumServiceFeeSettleAmount { get; set; }
        public List<PurchaseLine> RollbillPurchaseLine { get; set; }
        public CompanyParameter CompanyParameter { get; set; }
        public List<PurchaseLine> PurchaseLine { get; set; }
        public Company Company { get; set; }
        public List<PaymentDetail> PurchasePaymentDetail { get; set; }
        public decimal SumPaymentAmount { get; set; }
        public List<CustTrans> CustTrans { get; set; }
        public List<ServiceFeeTrans> ServiceFeeTrans { get; set; }
        public List<InvoiceType> InvoiceTypes { get; set; }
        public List<MethodOfPayment> MethodOfPayment { get; set; }
        public List<VerificationTrans> PurchaseLineApprovedVerificationTrans { get; set; }
        public NumberSeqTable ReceiptTempNumberSeqTable { get; set; }
        public CustomerTable PurchaseCustomerTable { get; set; }
        public decimal PurchaseDateExchangeRate { get; set; }
        public List<DocumentStatus> DocumentStatus { get; set; }
        public bool NormalCaseSettleAmountCondition { get; set; }
    }
}