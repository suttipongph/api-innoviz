﻿using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ListPurchaseLineOutstandingListView : ViewCompanyBaseEntity
	{
		public string ListPurchaseLineOutstandingGUID { get; set; }
		public string BillingDate { get; set; }
		public string BuyerTable_Values { get; set; }
		public string CollectionDate { get; set; }
		public string CustomerTable_Values { get; set; }
		public string DueDate { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public string PurchaseTable_Values { get; set; }
		public decimal InvoiceAmount { get; set; }
		public decimal Outstanding { get; set; }
		public decimal PurchaseAmount { get; set; }
		public string CustomerTableGUID { get; set; }
		public string MethodOfPaymentGUID { get; set; }
		public string PurchaseTableGUID { get; set; }
		public string BuyerTableGUID { get; set; }
		public string CustomerId { get; set; }
		public string MethodOfPaymentId { get; set; }
		public string PurchaseId { get; set; }
		public string BuyerId { get; set; }
		public string InterestDate { get; set; }
		public bool ClosedForAdditionalPurchase { get; set; }
	}
}

