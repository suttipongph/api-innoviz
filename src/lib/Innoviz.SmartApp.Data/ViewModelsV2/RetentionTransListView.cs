using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class RetentionTransListView : ViewCompanyBaseEntity
	{
		public string RetentionTransGUID { get; set; }
		public string CreditAppTableGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public int ProductType { get; set; }
		public string TransDate { get; set; }
		public decimal Amount { get; set; }
		public string BuyerTableGUID { get; set; }
		public string BuyerAgreementTableGUID { get; set; }
		public int RefType { get; set; }
		public string DocumentId { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string BuyerAgreementTable_Values { get; set; }
		public string RefGUID { get; set; }
		public string CreditAppTable_CreditAppId { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string BuyerAgreementTable_ReferenceAgreementID { get; set; }
		#region Outstanding
		public decimal MaximumRetention { get; set; }
		public decimal AccumRetentionAmount { get; set; }
		public decimal RemainingAmount { get; set; }
		public string CreditAppId { get; set; }
		#endregion outstanding
	}
}
