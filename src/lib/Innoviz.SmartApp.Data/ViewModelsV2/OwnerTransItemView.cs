using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class OwnerTransItemView : ViewCompanyBaseEntity
	{
		public string OwnerTransGUID { get; set; }
		public bool InActive { get; set; }
		public int Ordering { get; set; }
		public decimal PropotionOfShareholderPct { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public string RelatedPersonTableGUID { get; set; }
		public string RelatedPersonTable_LineId { get; set; }
		public string RelatedPersonTable_Mobile { get; set; }
		public string RelatedPersonTable_Name { get; set; }
		public string RelatedPersonTable_PassportId { get; set; }
		public string RelatedPersonTable_Phone { get; set; }
		public string RefId { get; set; }
		public string RelatedPersonTable_TaxId { get; set; }
		public string RelatedPersonTable_WorkPermitId { get; set; }
		public string RelatedPersonTable_BackgroundSummary { get; set; }
		public string RelatedPersonTable_DateOfBirth { get; set; }
		public string RelatedPersonTable_Email { get; set; }
		public string RelatedPersonTable_Extension { get; set; }
		public string RelatedPersonTable_Fax { get; set; }
		public int RelatedPersonTable_IdentificationType { get; set; }
		public int Age { get; set; }
	}
}
