using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class RptReportWithdrawalTermExtensionReportView : ViewReportBaseEntity
	{
		public string ReportWithdrawalTermExtensionGUID { get; set; }
		public string FromDueDate { get; set; }
		public string ToDueDate { get; set; }
		public string FromWithdrawalDate { get; set; }
		public string ToWithdrawalDate { get; set; }
		public string[] CustomerTableGUID { get; set; }
		public string[] BuyerTableGUID { get; set; }
		public string[] DocumentStatusGUID { get; set; }
		public bool ShowOutstandingOnly { get; set; }
	}
}
