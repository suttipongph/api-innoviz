using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class InquiryWithdrawalLineOutstandItemView : ViewCompanyBaseEntity
	{
		public string WithdrawalLineGUID { get; set; }
		public string WithdrawalTable_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string BuyerAgreementTrans_Values { get; set; }
		public decimal WithdrawalAmount { get; set; }
		public decimal Outstanding { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public string DueDate { get; set; }
		public string ChequeTable_ChequeDate { get; set; }
		public string BillingDate { get; set; }
		public string CollectionDate { get; set; }
		public string CreditAppTableBankAccountControl_BankAccountName { get; set; }
		public string CreditAppTablePDCBank_BankAccountName { get; set; }
		public int WithdrawalLineType { get; set; }
		public string CreditAppLine_Values { get; set; }
	}
}
