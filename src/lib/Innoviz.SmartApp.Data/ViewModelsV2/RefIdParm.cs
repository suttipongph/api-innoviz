﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class RefIdParm
    {
        public string RefGUID { get; set; }
        public int RefType { get; set; }
    }
}
