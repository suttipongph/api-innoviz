using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class InvoiceRevenueTypeItemView : ViewCompanyBaseEntity
	{
		public string InvoiceRevenueTypeGUID { get; set; }
		public int AssetFeeType { get; set; }
		public string Description { get; set; }
		public decimal FeeAmount { get; set; }
		public string FeeInvoiceText { get; set; }
		public string FeeLedgerAccount { get; set; }
		public decimal FeeTaxAmount { get; set; }
		public string FeeTaxGUID { get; set; }
		public string FeeWHTGUID { get; set; }
		public string IntercompanyTableGUID { get; set; }
		public int ProductType { get; set; }
		public string RevenueTypeId { get; set; }
		public int ServiceFeeCategory { get; set; }
		public string ServiceFeeRevenueTypeGUID { get; set; }
	}
}
