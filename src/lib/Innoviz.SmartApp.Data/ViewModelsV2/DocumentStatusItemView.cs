using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class DocumentStatusItemView : ViewBaseEntity
	{
		public string DocumentStatusGUID { get; set; }
		public string Description { get; set; }
		public string DocumentProcessGUID { get; set; }
		public string StatusCode { get; set; }
		public string StatusId { get; set; }
		public string ProcessId { get; set; }
		public string DocumentProcess_Values { get; set; }


	}
}
