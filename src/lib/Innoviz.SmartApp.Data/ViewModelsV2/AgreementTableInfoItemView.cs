using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class AgreementTableInfoItemView : ViewCompanyBaseEntity
	{
		public string AgreementTableInfoGUID { get; set; }
		public string AuthorizedPersonTransBuyer1GUID { get; set; }
		public string AuthorizedPersonTransBuyer2GUID { get; set; }
		public string AuthorizedPersonTransBuyer3GUID { get; set; }
		public string AuthorizedPersonTransCompany1GUID { get; set; }
		public string AuthorizedPersonTransCompany2GUID { get; set; }
		public string AuthorizedPersonTransCompany3GUID { get; set; }
		public string AuthorizedPersonTransCustomer1GUID { get; set; }
		public string AuthorizedPersonTransCustomer2GUID { get; set; }
		public string AuthorizedPersonTransCustomer3GUID { get; set; }
		public string AuthorizedPersonTypeBuyerGUID { get; set; }
		public string AuthorizedPersonTypeCompanyGUID { get; set; }
		public string AuthorizedPersonTypeCustomerGUID { get; set; }
		public string BuyerAddress1 { get; set; }
		public string BuyerAddress2 { get; set; }
		public string CompanyAddress1 { get; set; }
		public string CompanyAddress2 { get; set; }
		public string CompanyAltName { get; set; }
		public string CompanyBankGUID { get; set; }
		public string CompanyFax { get; set; }
		public string CompanyName { get; set; }
		public string CompanyPhone { get; set; }
		public string CustomerAddress1 { get; set; }
		public string CustomerAddress2 { get; set; }
		public string GuarantorName { get; set; }
		public string GuaranteeText { get; set; }
		public string RefGUID { get; set; }
		public string RefId { get; set; }
		public int RefType { get; set; }
		public string AgreementTableInfoText_Text1 { get; set; }
		public string AgreementTableInfoText_Text2 { get; set; }
		public string AgreementTableInfoText_Text3 { get; set; }
		public string AgreementTableInfoText_Text4 { get; set; }
		public string WitnessCompany1GUID { get; set; }
		public string WitnessCompany2GUID { get; set; }
		public string WitnessCustomer1 { get; set; }
		public string WitnessCustomer2 { get; set; }
		public int ParentTable_ProductType { get; set; }
		public string ParentTable_CustomerTableGUID { get; set; }
		public string ParentTable_CreditAppTableGUID { get; set; }
		public string ParentTable_BuyerTableGUID { get; set; }
		public string Company_DefaultBranchGUID { get; set; }
		public string UnboundBuyerAddress { get; set; }
		public string UnboundCompanyAddress { get; set; }
		public string UnboundCustomerAddress { get; set; }
		#region AgreementTableInfoText
		public string Text1 { get; set; }
		public string Text2 { get; set; }
		public string Text3 { get; set; }
		public string Text4 { get; set; }
		#endregion AgreementTableInfoText

	}
}
