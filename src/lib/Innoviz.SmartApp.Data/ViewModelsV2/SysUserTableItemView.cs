﻿using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.ViewModels;
using NJsonSchema.Annotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    [JsonSchemaFlatten]
    public class SysUserTableItemView : ViewBaseEntity
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string NormalizedEmail { get; set; }
        public string NormalizedUserName { get; set; }
        public string PhoneNumber { get; set; }
        public string Name { get; set; }
        public string DateFormatId { get; set; }
        public string TimeFormatId { get; set; }
        public string NumberFormatId { get; set; }
        public bool InActive { get; set; }
        public string LanguageId { get; set; }

        public IEnumerable<SysUserCompanyMappingView> SysUserCompanyMappingList { get; set; }
        public IEnumerable<SysUserRolesView> SysUserRolesViewList { get; set; }

        public IEnumerable<EmployeeTableItemView> EmployeeMappingList { get; set; }

    }
}
