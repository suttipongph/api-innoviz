using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ProductSettledTransItemView : ViewCompanyBaseEntity
	{
		public string ProductSettledTransGUID { get; set; }
		public string DocumentId { get; set; }
		public decimal InterestCalcAmount { get; set; }
		public string InterestDate { get; set; }
		public int InterestDay { get; set; }
		public string InvoiceSettlementDetailGUID { get; set; }
		public string LastReceivedDate { get; set; }
		public string OriginalDocumentId { get; set; }
		public decimal OriginalInterestCalcAmount { get; set; }
		public string OriginalRefGUID  { get; set; }
		public decimal PDCInterestOutstanding { get; set; }
		public decimal PostedInterestAmount { get; set; }
		public int ProductType { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public decimal ReserveToBeRefund { get; set; }
		public string SettledDate { get; set; }
		public decimal SettledInvoiceAmount { get; set; }
		public decimal SettledPurchaseAmount { get; set; }
		public decimal SettledReserveAmount { get; set; }
		public decimal TotalRefundAdditionalIntAmount { get; set; }
		public string InvoiceSettlementDetail_Values { get; set; }
		public string RefId { get; set; }
	}
}
