using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class AssignmentAgreementLineListView : ViewCompanyBaseEntity
	{
		public string AssignmentAgreementLineGUID { get; set; }
		public string AssignmentAgreementTableGUID { get; set; }
		public string BuyerAgreementTableGUID { get; set; }
		public string BuyerAgreementTable_Values { get; set; }
		public string ReferenceAgreementID { get; set; }
		public int LineNum { get; set; }
		public string BuyerAgreementTable_BuyerAgreementId { get; set; }
	}
}
