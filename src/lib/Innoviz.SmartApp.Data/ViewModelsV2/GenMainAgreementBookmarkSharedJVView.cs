using System;
using Innoviz.SmartApp.Core.Attributes;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class GenMainAgreementBookmarkSharedJVView
    {      

        [BookmarkMapping("CompanyName1")]
        public string CompanyName1 { get; set; }

        [BookmarkMapping("CompanyName2")]
        public string CompanyName2 { get; set; }

        [BookmarkMapping("CompanyName3")]
        public string CompanyName3 { get; set; }

        [BookmarkMapping("CompanyName4")]
        public string CompanyName4 { get; set; }

        [BookmarkMapping("CompanyName5")]
        public string CompanyName5 { get; set; }

        [BookmarkMapping("CompanyName6")]
        public string CompanyName6 { get; set; }

        [BookmarkMapping("CompanyName7")]
        public string CompanyName7 { get; set; }

        [BookmarkMapping("ContAmountFirst1")]
        public decimal ContAmountFirst1 { get; set; }

        [BookmarkMapping("ContAmountSecond1")]
        public string ContAmountSecond1 { get; set; }

        public DateTime AgreementDate { get; set; }

        [BookmarkMapping("AgreementDate1")]
        public string AgreementDate1 { get; set; }

        [BookmarkMapping("AgreementDate2")]
        public string AgreementDate2 { get; set; }

        [BookmarkMapping("AgreementDate3")]
        public string AgreementDate3 { get; set; }

        [BookmarkMapping("AgreementDate4")]
        public string AgreementDate4 { get; set; }

        [BookmarkMapping("AgreementDate5")]
        public string AgreementDate5 { get; set; }

        [BookmarkMapping("AgreementNo1")]
        public string AgreementNo1 { get; set; }

        [BookmarkMapping("AgreementNo2")]
        public string AgreementNo2 { get; set; }

        [BookmarkMapping("AgreementNo3")]
        public string AgreementNo3 { get; set; }

        [BookmarkMapping("AgreementNo4")]
        public string AgreementNo4 { get; set; }

        [BookmarkMapping("AgreementNo5")]
        public string AgreementNo5 { get; set; }

        [BookmarkMapping("InterestRate1")]
        public decimal? InterestRate1 { get; set; }

        [BookmarkMapping("AliasNameCust1")]
        public string AliasNameCust1 { get; set; }

        [BookmarkMapping("AliasNameCust2")]
        public string AliasNameCust2 { get; set; }

        [BookmarkMapping("AliasNameCust3")]
        public string AliasNameCust3 { get; set; }

        [BookmarkMapping("AliasNameCust4")]
        public string AliasNameCust4 { get; set; }

        [BookmarkMapping("AliasNameCust5")]
        public string AliasNameCust5 { get; set; }

        [BookmarkMapping("AliasNameCust6")]
        public string AliasNameCust6 { get; set; }

        [BookmarkMapping("AliasNameCust7")]
        public string AliasNameCust7 { get; set; }

        [BookmarkMapping("AliasNameCust8")]
        public string AliasNameCust8 { get; set; }

        [BookmarkMapping("CustName1")]
        public string CustName1 { get; set; }

        [BookmarkMapping("CustName2")]
        public string CustName2 { get; set; }

        [BookmarkMapping("CustName3")]
        public string CustName3 { get; set; }

        [BookmarkMapping("CustName4")]
        public string CustName4 { get; set; }

        [BookmarkMapping("CustName5")]
        public string CustName5 { get; set; }

        [BookmarkMapping("CustName6")]
        public string CustName6 { get; set; }

        [BookmarkMapping("CustName7")]
        public string CustName7 { get; set; }

        [BookmarkMapping("CustName8")]
        public string CustName8 { get; set; }

        [BookmarkMapping("CustTaxID1")]
        public string CustTaxID1 { get; set; }

        [BookmarkMapping("CompanyWitnessFirst1")]
        public string CompanyWitnessFirst1 { get; set; }

        [BookmarkMapping("CompanyWitnessSecond1")]
        public string CompanyWitnessSecond1 { get; set; }

        [BookmarkMapping("Text1")]
        public string Text1 { get; set; }

        [BookmarkMapping("Text2")]
        public string Text2 { get; set; }

        [BookmarkMapping("Text3")]
        public string Text3 { get; set; }

        [BookmarkMapping("Text4")]
        public string Text4 { get; set; }

        [BookmarkMapping("AuthorityPersonFirst1")]
        public string AuthorityPersonFirst1 { get; set; }

        [BookmarkMapping("AuthorityPersonFirst2")]
        public string AuthorityPersonFirst2 { get; set; }

        [BookmarkMapping("AuthorityPersonSecond1")]
        public string AuthorityPersonSecond1 { get; set; }

        [BookmarkMapping("AuthorityPersonSecond2")]
        public string AuthorityPersonSecond2 { get; set; }

        [BookmarkMapping("AuthorityPersonThird1")]
        public string AuthorityPersonThird1 { get; set; }

        [BookmarkMapping("AuthorityPersonThird2")]
        public string AuthorityPersonThird2 { get; set; }

        [BookmarkMapping("AuthorizedBuyerFirst1")]
        public string AuthorizedBuyerFirst1 { get; set; }

        [BookmarkMapping("AuthorizedBuyerFirst2")]
        public string AuthorizedBuyerFirst2 { get; set; }

        [BookmarkMapping("AuthorizedBuyerSecond1")]
        public string AuthorizedBuyerSecond1 { get; set; }

        [BookmarkMapping("AuthorizedBuyerSecond2")]
        public string AuthorizedBuyerSecond2 { get; set; }

        [BookmarkMapping("AuthorizedBuyerThird1")]
        public string AuthorizedBuyerThird1 { get; set; }

        [BookmarkMapping("AuthorizedBuyerThird2")]
        public string AuthorizedBuyerThird2 { get; set; }

        [BookmarkMapping("AuthorizedCustFirst1")]
        public string AuthorizedCustFirst1 { get; set; }

        [BookmarkMapping("AuthorizedCustFirst2")]
        public string AuthorizedCustFirst2 { get; set; }

        [BookmarkMapping("AuthorizedCustSecond1")]
        public string AuthorizedCustSecond1 { get; set; }

        [BookmarkMapping("AuthorizedCustSecond2")]
        public string AuthorizedCustSecond2 { get; set; }

        [BookmarkMapping("AuthorizedCustThird1")]
        public string AuthorizedCustThird1 { get; set; }

        [BookmarkMapping("AuthorizedCustThird2")]
        public string AuthorizedCustThird2 { get; set; }

        [BookmarkMapping("CustWitnessFirst1")]
        public string CustWitnessFirst1 { get; set; }

        [BookmarkMapping("CustWitnessSecond1")]
        public string CustWitnessSecond1 { get; set; }

        [BookmarkMapping("PositionBuyer1")]
        public string PositionBuyer1 { get; set; }

        [BookmarkMapping("PositionCust1")]
        public string PositionCust1 { get; set; }

        [BookmarkMapping("PositionCust2")]
        public string PositionCust2 { get; set; }
        [BookmarkMapping("PositionCust3")]
        public string PositionCust3 { get; set; }
        [BookmarkMapping("PositionLIT1")]
        public string PositionLIT1 { get; set; }
        [BookmarkMapping("PositionLIT2")]
        public string PositionLIT2 { get; set; }
        [BookmarkMapping("PositionLIT3")]
        public string PositionLIT3 { get; set; }
        [BookmarkMapping("CustAddress1")]
        public string CustAddress1 { get; set; }
        [BookmarkMapping("CustAddress2")]
        public string CustAddress2 { get; set; }
        [BookmarkMapping("CompanyAddress1")]
        public string CompanyAddress1 { get; set; }
        [BookmarkMapping("CompanyAddress2")]
        public string CompanyAddress2 { get; set; }
        [BookmarkMapping("TelNumber1")]
        public string TelNumber1 { get; set; }
        [BookmarkMapping("FaxNumber1")]
        public string FaxNumber1 { get; set; }
        [BookmarkMapping("FaxNumber2")]
        public string FaxNumber2 { get; set; }

        [BookmarkMapping("CompanyBankBranch1")]
        public string CompanyBankBranch1 { get; set; }

        [BookmarkMapping("CompanyBankBranch2")]
        public string CompanyBankBranch2 { get; set; }

        [BookmarkMapping("CompanyBankName1")]
        public string CompanyBankName1 { get; set; }

        [BookmarkMapping("CompanyBankName2")]
        public string CompanyBankName2 { get; set; }

        [BookmarkMapping("CompanyBankNo1")]
        public string CompanyBankNo1 { get; set; }

        [BookmarkMapping("CompanyBankNo2")]
        public string CompanyBankNo2 { get; set; }

        [BookmarkMapping("CompanyBankType1")]
        public string CompanyBankType1 { get; set; }

        [BookmarkMapping("CompanyBankType2")]
        public string CompanyBankType2 { get; set; }

        [BookmarkMapping("ManualNo1")]
        public string ManualNo1 { get; set; }

        [BookmarkMapping("JointAddressFirst1")]
        public string JointAddressFirst1 { get; set; }
        [BookmarkMapping("JointNameFirst1")]
        public string JointNameFirst1 { get; set; }
        [BookmarkMapping("JointAddressSecond1")]
        public string JointAddressSecond1 { get; set; }
        [BookmarkMapping("JointNameSecond1")]
        public string JointNameSecond1 { get; set; }
        [BookmarkMapping("JointAddressThird1")]
        public string JointAddressThird1 { get; set; }
        [BookmarkMapping("JointNameThird1")]
        public string JointNameThird1 { get; set; }
        [BookmarkMapping("JointAddressFourth1")]
        public string JointAddressFourth1 { get; set; }
        [BookmarkMapping("JointNameFourth1")]
        public string JointNameFourth1 { get; set; }
        [BookmarkMapping("PositionJointFirst1")]
        public string PositionJointFirst1 { get; set; }
        [BookmarkMapping("PositionJointSecond1")]
        public string PositionJointSecond1 { get; set; }
        [BookmarkMapping("PositionJointThird1")]
        public string PositionJointThird1 { get; set; }
        [BookmarkMapping("PositionJointFourth1")]
        public string PositionJointFourth1 { get; set; }
        [BookmarkMapping("JointAddressFirst2")]
        public string JointAddressFirst2 { get; set; }
        [BookmarkMapping("JointNameFirst2")]
        public string JointNameFirst2 { get; set; }
        [BookmarkMapping("JointAddressSecond2")]
        public string JointAddressSecond2 { get; set; }
        [BookmarkMapping("JointNameSecond2")]
        public string JointNameSecond2 { get; set; }
        [BookmarkMapping("JointAddressThird2")]
        public string JointAddressThird2 { get; set; }
        [BookmarkMapping("JointNameThird2")]
        public string JointNameThird2 { get; set; }
        [BookmarkMapping("JointAddressFourth2")]
        public string JointAddressFourth2 { get; set; }
        [BookmarkMapping("JointNameFourth2")]
        public string JointNameFourth2 { get; set; }
        [BookmarkMapping("PositionJointFirst2")]
        public string PositionJointFirst2 { get; set; }
        [BookmarkMapping("PositionJointSecond2")]
        public string PositionJointSecond2 { get; set; }
        [BookmarkMapping("PositionJointThird2")]
        public string PositionJointThird2 { get; set; }
        [BookmarkMapping("PositionJointFourth2")]
        public string PositionJointFourth2 { get; set; }
        [BookmarkMapping("JointAddressFirst3")]
        public string JointAddressFirst3 { get; set; }
        [BookmarkMapping("JointNameFirst3")]
        public string JointNameFirst3 { get; set; }
        [BookmarkMapping("JointAddressSecond3")]
        public string JointAddressSecond3 { get; set; }
        [BookmarkMapping("JointNameSecond3")]
        public string JointNameSecond3 { get; set; }
        [BookmarkMapping("JointAddressThird3")]
        public string JointAddressThird3 { get; set; }
        [BookmarkMapping("JointNameThird3")]
        public string JointNameThird3 { get; set; }
        [BookmarkMapping("JointAddressFourth3")]
        public string JointAddressFourth3 { get; set; }
        [BookmarkMapping("JointNameFourth3")]
        public string JointNameFourth3 { get; set; }
        [BookmarkMapping("PositionJointFirst3")]
        public string PositionJointFirst3 { get; set; }
        [BookmarkMapping("PositionJointSecond3")]
        public string PositionJointSecond3 { get; set; }
        [BookmarkMapping("PositionJointThird3")]
        public string PositionJointThird3 { get; set; }
        [BookmarkMapping("PositionJointFourth3")]
        public string PositionJointFourth3 { get; set; }
        [BookmarkMapping("JointAddressFirst4")]
        public string JointAddressFirst4 { get; set; }
        [BookmarkMapping("JointNameFirst4")]
        public string JointNameFirst4 { get; set; }
        [BookmarkMapping("JointAddressSecond4")]
        public string JointAddressSecond4 { get; set; }
        [BookmarkMapping("JointNameSecond4")]
        public string JointNameSecond4 { get; set; }
        [BookmarkMapping("JointAddressThird4")]
        public string JointAddressThird4 { get; set; }
        [BookmarkMapping("JointNameThird4")]
        public string JointNameThird4 { get; set; }
        [BookmarkMapping("JointAddressFourth4")]
        public string JointAddressFourth4 { get; set; }
        [BookmarkMapping("JointNameFourth4")]
        public string JointNameFourth4 { get; set; }
        [BookmarkMapping("PositionJointFirst4")]
        public string PositionJointFirst4 { get; set; }
        [BookmarkMapping("PositionJointSecond4")]
        public string PositionJointSecond4 { get; set; }
        [BookmarkMapping("PositionJointThird4")]
        public string PositionJointThird4 { get; set; }
        [BookmarkMapping("PositionJointFourth4")]
        public string PositionJointFourth4 { get; set; }
        [BookmarkMapping("TextJVDetail1")]
        public string TextJVDetail1 { get; set; }
        [BookmarkMapping("OperatedbyFirst1")]
        public string OperatedbyFirst1 { get; set; }
        [BookmarkMapping("OperatedbySecond1")]
        public string OperatedbySecond1 { get; set; }
        [BookmarkMapping("OperatedbyThird1")]
        public string OperatedbyThird1 { get; set; }
        [BookmarkMapping("OperatedbyFourth1")]
        public string OperatedbyFourth1 { get; set; }
    }
}