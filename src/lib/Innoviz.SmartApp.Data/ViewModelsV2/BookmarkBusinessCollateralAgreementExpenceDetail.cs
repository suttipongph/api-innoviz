﻿using Innoviz.SmartApp.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class BCADTbusinessCollateralAgmTable
    {
        public string BCADTbusinessCollateralAgmTable_AgreementDate { get; set; }
        public string BCADTbusinessCollateralAgmTable_BusinessCollateralAgmId { get; set; }
        public string BCADTbusinessCollateralAgmTable_CustomerName { get; set; }
        public string BCADTbusinessCollateralAgmTable_CreditAppTableGUID { get; set; }
        public string BCADTbusinessCollateralAgmTable_CompanyGUID { get; set; }
        public string BCADTConsortiumTrans_ConsortiumTableGUID { get; set; }
        public string BCADTConsortiumTrans_CreditAppRequestTableGUID { get; set; }
    }
    public class BCADTAgreementTableInfo
    {
        public string BCADTAgreementTableInfo_AuthorizedPersonTransCustomer1GUID { get; set; }
        public string BCADTAgreementTableInfo_AuthorizedPersonTransCustomer2GUID { get; set; }
        public string BCADTAgreementTableInfo_AuthorizedPersonTransCustomer3GUID { get; set; }

    }
    public class BCADTConsortiumTrans
    {
     
        public string BCADTConsortiumTrans_OperatedBy { get; set; }
        public string BCADTConsortiumTrans_CustomerName { get; set; }
    }


    public class BCADTCreditAppTable
    {
        public decimal BCADTCreditAppTable_ApprovedCreditLimit { get; set; }
    }
    public class BCADTCreditAppRequestLine
    {
        public decimal BCADTCreditAppRequestLine_SumApprovedCreditLimitLineRequest { get; set; }
    }
    public class BCADTServiceFeeTrans
    {
        public int BCADTServiceFeeTrans_Ordering { get; set; }
        public string BCADTServiceFeeTrans_Description { get; set; }
        public decimal BCADTServiceFeeTrans_AmountBeforeTax { get; set; }
        public decimal BCADTServiceFeeTrans_TaxAmount { get; set; }
        public decimal BCADTServiceFeeTrans_AmountIncludeTax { get; set; }
        public decimal BCADTServiceFeeTrans_WHTAmount { get; set; }
        public decimal BCADTServiceFeeTrans_LineServiceFeeNet { get; set; }

    }
 
    public class BookmarkBusinessCollateralAgreementExpenceDetail
    {
        [BookmarkMapping("ContractDate1", "ContractDate2", "ContractDate3", "ContractDate4", "ContractDate5")]
        public string BCADTbusinessCollateralAgmTable_AgreementDate { get; set; }
        [BookmarkMapping("ContractNo1", "ContractNo2")]
        public string BCADTbusinessCollateralAgmTable_BusinessCollateralAgmId { get; set; }
        [BookmarkMapping("CustName1", "CustName2", "CustName3")]
        public string BCADTbusinessCollateralAgmTable_CustomerName { get; set; }
        public string BCADTbusinessCollateralAgmTable_CreditAppTableGUID { get; set; }

        [BookmarkMapping("TotalServiceFee1", "TotalServiceFee2")]
        public decimal BCADTbusinessCollateralAgmTable_SumLineServiceFeeNet { get; set; }

        public string BCADTAgreementTableInfo_AuthorizedPersonTransCustomer1GUID { get; set; }
        public string BCADTAgreementTableInfo_AuthorizedPersonTransCustomer2GUID { get; set; }
        public string BCADTAgreementTableInfo_AuthorizedPersonTransCustomer3GUID { get; set; }
        [BookmarkMapping("AuthorizedCustFirst1", "AuthorizedCustFirst2")]
        public string BCADTCustomerAuthorizedPerson1_Name { get; set; }
        [BookmarkMapping("AuthorizedCustSecond1", "AuthorizedCustSecond2")]
        public string BCADTCustomerAuthorizedPerson2_Name { get; set; }
        [BookmarkMapping("AuthorizedCustThird1", "AuthorizedCustThird2")]
        public string BCADTCustomerAuthorizedPerson3_Name { get; set; }


        [BookmarkMapping("ConsortiumName1")]
        public string BCADTConsortium1_Description { get; set; }
        [BookmarkMapping("AuthorizedConsCustFirst1", "AuthorizedConsCustFirst2")]
        public string BCADTConsortiumTrans1_OperatedBy { get; set; }
        [BookmarkMapping("ConsCustNameFirst1")]
        public string BCADTConsortiumTrans1_CustomerName { get; set; }
        [BookmarkMapping("AuthorizedConsCustSecond1", "AuthorizedConsCustSecond2")]
        public string BCADTConsortiumTrans2_OperatedBy { get; set; }
        [BookmarkMapping("ConsCustNameSecond1")]
        public string BCADTConsortiumTrans2_CustomerName { get; set; }
        [BookmarkMapping("AuthorizedConsCustThird1", "AuthorizedConsCustThird2")]
        public string BCADTConsortiumTrans3_OperatedBy { get; set; }
        public string BCADTConsortiumTrans3_CustomerName { get; set; }
        [BookmarkMapping("ExpAmountApproved1","ExpAmountApproved2")]
        public decimal BCADTCompany_ApprovedCreditLimit { get; set; }
        [BookmarkMapping("ExpAmountLoan1")]
        public decimal BCADTCompany_SumApprovedCreditLimitLineRequest { get; set; }
        [BookmarkMapping("DateOfCreditFirst1", "DateOfCreditSecond1")]
        public string BCADTCompanyParameter_PFExtendCreditTermGUID { get; set; }
        [BookmarkMapping("Seq1st")]
        public decimal BCADTServiceFeeTrans1_Ordering { get; set; }
        [BookmarkMapping("LineServiceFeeDescription1st")]
        public string BCADTServiceFeeTrans1_Description { get; set; }
        [BookmarkMapping("LineServiceFeeAmtBefTax1st")]
        public decimal BCADTServiceFeeTrans1_AmountBeforeTax { get; set; }
        [BookmarkMapping("LineServiceFeeVAT1st")]
        public decimal BCADTServiceFeeTrans1_TaxAmount { get; set; }
        [BookmarkMapping("LineServiceFeeAmtIncTax1st")]
        public decimal BCADTServiceFeeTrans1_AmountIncludeTax { get; set; }
        [BookmarkMapping("LIneServiceFeeWHT1st")]
        public decimal BCADTServiceFeeTrans1_WHTAmount { get; set; }
        [BookmarkMapping("LineServiceFeeNet1st")]
        public decimal BCADTServiceFeeTrans1_LineServiceFeeNet { get; set; }

        [BookmarkMapping("Seq2nd")]
        public decimal BCADTServiceFeeTrans2_Ordering { get; set; }
        [BookmarkMapping("LineServiceFeeDescription2nd")]
        public string BCADTServiceFeeTrans2_Description { get; set; }
        [BookmarkMapping("LineServiceFeeAmtBefTax2nd")]
        public decimal BCADTServiceFeeTrans2_AmountBeforeTax { get; set; }
        [BookmarkMapping("LineServiceFeeVAT2nd")]
        public decimal BCADTServiceFeeTrans2_TaxAmount { get; set; }
        [BookmarkMapping("LineServiceFeeAmtIncTax2nd")]
        public decimal BCADTServiceFeeTrans2_AmountIncludeTax { get; set; }
        [BookmarkMapping("LIneServiceFeeWHT2nd")]
        public decimal BCADTServiceFeeTrans2_WHTAmount { get; set; }
        [BookmarkMapping("LineServiceFeeNet2nd")]
        public decimal BCADTServiceFeeTrans2_LineServiceFeeNet { get; set; }

        [BookmarkMapping("Seq3rd")]
        public decimal BCADTServiceFeeTrans3_Ordering { get; set; }
        [BookmarkMapping("LineServiceFeeDescription3rd")]
        public string BCADTServiceFeeTrans3_Description { get; set; }
        [BookmarkMapping("LineServiceFeeAmtBefTax3rd")]
        public decimal BCADTServiceFeeTrans3_AmountBeforeTax { get; set; }
        [BookmarkMapping("LineServiceFeeVAT3rd")]
        public decimal BCADTServiceFeeTrans3_TaxAmount { get; set; }
        [BookmarkMapping("LineServiceFeeAmtIncTax3rd")]
        public decimal BCADTServiceFeeTrans3_AmountIncludeTax { get; set; }
        [BookmarkMapping("LIneServiceFeeWHT3rd")]
        public decimal BCADTServiceFeeTrans3_WHTAmount { get; set; }
        [BookmarkMapping("LineServiceFeeNet3rd")]
        public decimal BCADTServiceFeeTrans3_LineServiceFeeNet { get; set; }

        [BookmarkMapping("Seq4th")]
        public decimal BCADTServiceFeeTrans4_Ordering { get; set; }
        [BookmarkMapping("LineServiceFeeDescription4th")]
        public string BCADTServiceFeeTrans4_Description { get; set; }
        [BookmarkMapping("LineServiceFeeAmtBefTax4th")]
        public decimal BCADTServiceFeeTrans4_AmountBeforeTax { get; set; }
        [BookmarkMapping("LineServiceFeeVAT4th")]
        public decimal BCADTServiceFeeTrans4_TaxAmount { get; set; }
        [BookmarkMapping("LineServiceFeeAmtIncTax4th")]
        public decimal BCADTServiceFeeTrans4_AmountIncludeTax { get; set; }
        [BookmarkMapping("LIneServiceFeeWHT4th")]
        public decimal BCADTServiceFeeTrans4_WHTAmount { get; set; }
        [BookmarkMapping("LineServiceFeeNet4th")]
        public decimal BCADTServiceFeeTrans4_LineServiceFeeNet { get; set; }

        [BookmarkMapping("Seq5th")]
        public decimal BCADTServiceFeeTrans5_Ordering { get; set; }
        [BookmarkMapping("LineServiceFeeDescription5th")]
        public string BCADTServiceFeeTrans5_Description { get; set; }
        [BookmarkMapping("LineServiceFeeAmtBefTax5th")]
        public decimal BCADTServiceFeeTrans5_AmountBeforeTax { get; set; }
        [BookmarkMapping("LineServiceFeeVAT5th")]
        public decimal BCADTServiceFeeTrans5_TaxAmount { get; set; }
        [BookmarkMapping("LineServiceFeeAmtIncTax5th")]
        public decimal BCADTServiceFeeTrans5_AmountIncludeTax { get; set; }
        [BookmarkMapping("LIneServiceFeeWHT5th")]
        public decimal BCADTServiceFeeTrans5_WHTAmount { get; set; }
        [BookmarkMapping("LineServiceFeeNet5th")]
        public decimal BCADTServiceFeeTrans5_LineServiceFeeNet { get; set; }

        [BookmarkMapping("Seq6th")]
        public decimal BCADTServiceFeeTrans6_Ordering { get; set; }
        [BookmarkMapping("LineServiceFeeDescription6th")]
        public string BCADTServiceFeeTrans6_Description { get; set; }
        [BookmarkMapping("LineServiceFeeAmtBefTax6th")]
        public decimal BCADTServiceFeeTrans6_AmountBeforeTax { get; set; }
        [BookmarkMapping("LineServiceFeeVAT6th")]
        public decimal BCADTServiceFeeTrans6_TaxAmount { get; set; }
        [BookmarkMapping("LineServiceFeeAmtIncTax6th")]
        public decimal BCADTServiceFeeTrans6_AmountIncludeTax { get; set; }
        [BookmarkMapping("LIneServiceFeeWHT6th")]
        public decimal BCADTServiceFeeTrans6_WHTAmount { get; set; }
        [BookmarkMapping("LineServiceFeeNet6th")]
        public decimal BCADTServiceFeeTrans6_LineServiceFeeNet { get; set; }

        [BookmarkMapping("Seq7th")]
        public decimal BCADTServiceFeeTrans7_Ordering { get; set; }
        [BookmarkMapping("LineServiceFeeDescription7th")]
        public string BCADTServiceFeeTrans7_Description { get; set; }
        [BookmarkMapping("LineServiceFeeAmtBefTax7th")]
        public decimal BCADTServiceFeeTrans7_AmountBeforeTax { get; set; }
        [BookmarkMapping("LineServiceFeeVAT7th")]
        public decimal BCADTServiceFeeTrans7_TaxAmount { get; set; }
        [BookmarkMapping("LineServiceFeeAmtIncTax7th")]
        public decimal BCADTServiceFeeTrans7_AmountIncludeTax { get; set; }
        [BookmarkMapping("LIneServiceFeeWHT7th")]
        public decimal BCADTServiceFeeTrans7_WHTAmount { get; set; }
        [BookmarkMapping("LineServiceFeeNet7th")]
        public decimal BCADTServiceFeeTrans7_LineServiceFeeNet { get; set; }

        [BookmarkMapping("Seq8th")]
        public decimal BCADTServiceFeeTrans8_Ordering { get; set; }
        [BookmarkMapping("LineServiceFeeDescription8th")]
        public string BCADTServiceFeeTrans8_Description { get; set; }
        [BookmarkMapping("LineServiceFeeAmtBefTax8th")]
        public decimal BCADTServiceFeeTrans8_AmountBeforeTax { get; set; }
        [BookmarkMapping("LineServiceFeeVAT8th")]
        public decimal BCADTServiceFeeTrans8_TaxAmount { get; set; }
        [BookmarkMapping("LineServiceFeeAmtIncTax8th")]
        public decimal BCADTServiceFeeTrans8_AmountIncludeTax { get; set; }
        [BookmarkMapping("LIneServiceFeeWHT8th")]
        public decimal BCADTServiceFeeTrans8_WHTAmount { get; set; }
        [BookmarkMapping("LineServiceFeeNet8th")]
        public decimal BCADTServiceFeeTrans8_LineServiceFeeNet { get; set; }

        [BookmarkMapping("Seq9th")]
        public decimal BCADTServiceFeeTrans9_Ordering { get; set; }
        [BookmarkMapping("LineServiceFeeDescription9th")]
        public string BCADTServiceFeeTrans9_Description { get; set; }
        [BookmarkMapping("LineServiceFeeAmtBefTax9th")]
        public decimal BCADTServiceFeeTrans9_AmountBeforeTax { get; set; }
        [BookmarkMapping("LineServiceFeeVAT9th")]
        public decimal BCADTServiceFeeTrans9_TaxAmount { get; set; }
        [BookmarkMapping("LineServiceFeeAmtIncTax9th")]
        public decimal BCADTServiceFeeTrans9_AmountIncludeTax { get; set; }
        [BookmarkMapping("LIneServiceFeeWHT9th")]
        public decimal BCADTServiceFeeTrans9_WHTAmount { get; set; }
        [BookmarkMapping("LineServiceFeeNet9th")]
        public decimal BCADTServiceFeeTrans9_LineServiceFeeNet { get; set; }

        [BookmarkMapping("Seq10th")]
        public decimal BCADTServiceFeeTrans10_Ordering { get; set; }
        [BookmarkMapping("LineServiceFeeDescription10th")]
        public string BCADTServiceFeeTrans10_Description { get; set; }
        [BookmarkMapping("LineServiceFeeAmtBefTax10th")]
        public decimal BCADTServiceFeeTrans10_AmountBeforeTax { get; set; }
        [BookmarkMapping("LineServiceFeeVAT10th")]
        public decimal BCADTServiceFeeTrans10_TaxAmount { get; set; }
        [BookmarkMapping("LineServiceFeeAmtIncTax10th")]
        public decimal BCADTServiceFeeTrans10_AmountIncludeTax { get; set; }
        [BookmarkMapping("LIneServiceFeeWHT10th")]
        public decimal BCADTServiceFeeTrans10_WHTAmount { get; set; }
        [BookmarkMapping("LineServiceFeeNet10th")]
        public decimal BCADTServiceFeeTrans10_LineServiceFeeNet { get; set; }

        public decimal TotalServiceFee { get; set; }
        [BookmarkMapping("BankChargeCheck1", "BankChargeCheck2")]
        public decimal UpcountryChequeFee { get; set; }
        [BookmarkMapping("ExpTransTotalCheck1")]
        public decimal ExpTransTotalCheck { get; set; }
        [BookmarkMapping("ContAmountFirst1", "ContAmountVatFirst1")]
        public decimal FixValue { get; set; }
        [BookmarkMapping("AgreementDate")]
        public string AgreementDate { get; set; }
        [BookmarkMapping("TotalLineServiceFeeNet")]
        public decimal TotalLineServiceFeeNet { get; set; }
    }
}
