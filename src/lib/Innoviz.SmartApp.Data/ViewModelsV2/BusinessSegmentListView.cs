using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BusinessSegmentListView : ViewCompanyBaseEntity
	{
		public string BusinessSegmentGUID { get; set; }
		public string BusinessSegmentId { get; set; }
		public string Description { get; set; }
		public int BusinessSegmentType { get; set; }
	}
}
