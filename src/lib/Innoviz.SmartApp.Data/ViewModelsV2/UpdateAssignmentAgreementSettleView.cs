using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class UpdateAssignmentAgreementSettleView : ViewCompanyBaseEntity
	{
		public string AssignmentAgreementSettleGUID { get; set; }
		public string AssignmentAgreementId { get; set; }
		public string BuyerAgreementId { get; set; }
		public string DocumentReasonGUID { get; set; }
		public string InternalAssignmentAgreementId { get; set; }
		public decimal NewSettledAmount { get; set; }
		public string NewSettledDate { get; set; }
		public decimal SettledAmount { get; set; }
		public string SettledDate { get; set; }
	}
}
