using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CompanyBankItemView : ViewCompanyBaseEntity
	{
		public string CompanyBankGUID { get; set; }
		public string AccountNumber { get; set; }
		public string BankAccountName { get; set; }
		public string BankBranch { get; set; }
		public string BankGroupGUID { get; set; }
		public string BankTypeGUID { get; set; }
		public bool InActive { get; set; }
		public bool Primary { get; set; }
		public string BankGroup_BankGroupId { get; set; }
	}
}
