using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class DocumentProcessItemView : ViewBaseEntity
	{
		public string DocumentProcessGUID { get; set; }
		public string Description { get; set; }
		public string ProcessId { get; set; }
	}
}
