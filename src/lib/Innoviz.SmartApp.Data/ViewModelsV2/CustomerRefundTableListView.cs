using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CustomerRefundTableListView : ViewCompanyBaseEntity
	{
		public string CustomerRefundTableGUID { get; set; }
		public string CustomerRefundId { get; set; }
		public string Description { get; set; }
		public string TransDate { get; set; }
		public string DocumentStatusGUID { get; set; }
		public decimal PaymentAmount { get; set; }
		public decimal SuspenseAmount { get; set; }
		public string CurrencyGUID { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string Currency_Values { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string Currency_CurrencyId { get; set; }
		public int SuspenseInvoiceType { get; set; }
		public decimal NetAmount { get; set; }
	}
}