﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class GenerateVendorInvoiceStagingView
    {
        public string GenerateVendorInvoiceStagingGUID { get; set; }
        public List<int> ListProcessTransType { get; set; }
        public List<int> StagingBatchStatus { get; set; }
    }
}
