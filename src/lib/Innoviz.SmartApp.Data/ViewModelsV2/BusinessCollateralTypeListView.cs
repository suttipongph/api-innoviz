using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BusinessCollateralTypeListView : ViewCompanyBaseEntity
	{
		public string BusinessCollateralTypeGUID { get; set; }
		public string BusinessCollateralTypeId { get; set; }
		public string Description { get; set; }
		public int AgreementOrdering { get; set; }
	}
}
