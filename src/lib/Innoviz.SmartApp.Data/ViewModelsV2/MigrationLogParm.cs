﻿using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class MigrationLogParm
    {
        public string MigrationFromDate { get; set; }
        public string MigrationToDate { get; set; }
        public string MigrationTable { get; set; }
    }
}
