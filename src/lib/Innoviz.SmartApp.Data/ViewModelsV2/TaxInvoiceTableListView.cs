using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class TaxInvoiceTableListView : ViewBranchCompanyBaseEntity
	{
		public string TaxInvoiceTableGUID { get; set; }
		public string TaxInvoiceId { get; set; }
		public string IssuedDate { get; set; }
		public string DueDate { get; set; }
		public string CustomerTableGUID { get; set; }
		public string CustomerName { get; set; }
		public decimal InvoiceAmountBeforeTax { get; set; }
		public decimal TaxAmount { get; set; }
		public decimal InvoiceAmount { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string CustomerTable_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string InvoiceTableGUID { get; set; }
		public string InvoiceTypeGUID { get; set; }
		public int TaxInvoiceRefType { get; set; }
		public string CurrencyGUID { get; set; }
		public string MethodOfPaymentGUID { get; set; }
		public string CNReasonGUID { get; set; }
		public string Dimension1GUID { get; set; }
		public string Dimension2GUID { get; set; }
		public string Dimension3GUID { get; set; }
		public string Dimension4GUID { get; set; }
		public string Dimension5GUID { get; set; }
		public string RefTaxInvoiceGUID { get; set; }
		public string InvoiceTable_Values { get; set; }
		public string InvoiceType_Values { get; set; }
		public string Currency_Values { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public string CNReason_Values { get; set; }
		public string Dimension1_Values { get; set; }
		public string Dimension2_Values { get; set; }
		public string Dimension3_Values { get; set; }
		public string Dimension4_Values { get; set; }
		public string Dimension5_Values { get; set; }
		public string RefTaxInvoice_Values { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string CustomerTable_CustomerId { get; set; }
	}
}
