using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class NumberSeqParameterItemView : ViewCompanyBaseEntity
	{
		public string NumberSeqParameterGUID { get; set; }
		public string NumberSeqTableGUID { get; set; }
		public string ReferenceId { get; set; }
	}
}
