using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class RptReportServiceFeeFactRealizationReportView : ViewReportBaseEntity
	{
		public string ReportServiceFeeFactRealizationGUID { get; set; }
		public string FromCollectionDate { get; set; }
		public string ToCollectionDate { get; set; }
		public string[] CustomerTableGUID { get; set; }
		public string[] BuyerTableGUID { get; set; }
	}
}
