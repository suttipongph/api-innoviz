using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class InvoiceSettlementDetailListView : ViewCompanyBaseEntity
	{
		public string InvoiceSettlementDetailGUID { get; set; }
		public int ProductType { get; set; }
		public bool WHTSlipReceivedByBuyer { get; set; }
		public decimal SettleInvoiceAmount { get; set; }
		public string DocumentId { get; set; }
		public string InvoiceTableGUID { get; set; }
		public string InvoiceDueDate { get; set; }
		public string InvoiceTypeGUID { get; set; }
		public decimal InvoiceAmount { get; set; }
		public decimal BalanceAmount { get; set; }
		public decimal SettleAmount { get; set; }
		public decimal WHTAmount { get; set; }
		public string InvoiceTable_Values { get; set; }
		public string InvoiceType_Values { get; set; }
		public bool WHTSlipReceivedByCustomer { get; set; }
		public string InvoiceTable_InvoiceId { get; set; }
		public string InvoiceType_InvoiceTypeId { get; set; }
		public string InvoiceTable_InvoiceTypeGUID { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public int SuspenseInvoiceType { get; set; }
	}
}
