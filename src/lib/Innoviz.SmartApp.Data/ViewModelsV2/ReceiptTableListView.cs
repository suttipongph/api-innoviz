using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ReceiptTableListView : ViewBranchCompanyBaseEntity
	{
		public string ReceiptTableGUID { get; set; }
		public string ReceiptId { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string ReceiptDate { get; set; }
		public string TransDate { get; set; }
		public string CustomerTableGUID { get; set; }
		public string CurrencyGUID { get; set; }
		public decimal SettleBaseAmount { get; set; }
		public decimal SettleTaxAmount { get; set; }
		public decimal SettleAmount { get; set; }
		public string CustomerTable_Values { get; set; }
		public string ReceiptTempTable_Values { get; set; }
		public string Currency_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string RefGUID { get; set; }
		public string InvoiceSettlementDetailGUID { get; set; }
		public string InvoiceSettlementDetail_Values { get; set; }
	}
}
