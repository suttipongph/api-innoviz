using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class VerificationTransListView : ViewCompanyBaseEntity
	{
		public string VerificationTransGUID { get; set; }
		public string VerificationTableGUID { get; set; }
		public string VerificationTable_VerificationDate { get; set; }
		public string BuyerId { get; set; }
		public string VerificationTable_Values { get; set; }
		public string VerificationTable_VerificationId { get; set; }
		public string RefGUID { get; set; }
	}
}
