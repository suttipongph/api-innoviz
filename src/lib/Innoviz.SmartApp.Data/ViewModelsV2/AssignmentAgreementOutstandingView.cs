﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class AssignmentAgreementOutstandingView : ViewCompanyBaseEntity
    {
        public string InternalAssignmentAgreementId { get; set; }
        public string CustomerTableGUID { get; set; }
        public string AssignmentAgreementTableGUID { get; set; }
        public string BuyerTableGUID { get; set; }
        public decimal AssignmentAgreementAmount { get; set; }
        public decimal SettledAmount { get; set; }
        public decimal RemainingAmount { get; set; }
        public string CustomerName { get; set; }
        public string BuyerName { get; set; }
        public string CustomerTable_CustomerId { get; set; }
        public string BuyerTable_BuyerId { get; set; }
        public string CustomerTable_Values { get; set; }
        public string BuyerTable_Values { get; set; }
        public int RefType { get; set; }
        public string AssignmentAgreementId { get; set; }
        public string DocumentStatusGUID { get; set; }
        public string DocumentStatus_Values { get; set; }
        public string DocumentStatus_StatusId { get; set; }
        public string AssignmentMethodGUID { get; set; }
        public string AssignmentMethod_Values { get; set; }
        public string AssignmentMethod_InternalAssignmentAgreementId { get; set; }
        public int AgreementDocType { get; set; }
        public string AgreementDate { get; set; }
        public string Description { get; set; }
        public string ReferenceAgreementId { get; set; }
    }
}
