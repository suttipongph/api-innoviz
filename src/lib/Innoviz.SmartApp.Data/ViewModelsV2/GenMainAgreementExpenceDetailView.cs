﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Attributes;
using Innoviz.SmartApp.Data.Models;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class GenMainAgreementExpenceDetailView
    {
        [BookmarkMapping("AuthorizedConsCustFirst1")]
        public string AuthorizedConsCustFirst1 { get; set; }

        [BookmarkMapping("AuthorizedConsCustFirst2")]
        public string AuthorizedConsCustFirst2 { get; set; }

        [BookmarkMapping("AuthorizedConsCustSecond1")]
        public string AuthorizedConsCustSecond1 { get; set; }

        [BookmarkMapping("AuthorizedConsCustSecond2")]
        public string AuthorizedConsCustSecond2 { get; set; }

        [BookmarkMapping("AuthorizedConsCustThird1")]
        public string AuthorizedConsCustThird1 { get; set; }

        [BookmarkMapping("AuthorizedConsCustThird2")]
        public string AuthorizedConsCustThird2 { get; set; }

        [BookmarkMapping("AuthorizedCustFirst1")]
        public string AuthorizedCustFirst1 { get; set; }

        [BookmarkMapping("AuthorizedCustFirst2")]
        public string AuthorizedCustFirst2 { get; set; }

        [BookmarkMapping("AuthorizedCustSecond1")]
        public string AuthorizedCustSecond1 { get; set; }

        [BookmarkMapping("AuthorizedCustSecond2")]
        public string AuthorizedCustSecond2 { get; set; }

        [BookmarkMapping("AuthorizedCustThird1")]
        public string AuthorizedCustThird1 { get; set; }

        [BookmarkMapping("AuthorizedCustThird2")]
        public string AuthorizedCustThird2 { get; set; }

        [BookmarkMapping("CompanyName1")]
        public string CompanyName1 { get; set; }

        [BookmarkMapping("CompanyName2")]
        public string CompanyName2 { get; set; }

        [BookmarkMapping("CompanyName3")]
        public string CompanyName3 { get; set; }
        [BookmarkMapping("ConsCustNameFirst1")]
        public string ConsCustNameFirst1 { get; set; }
        [BookmarkMapping("ConsCustNameSecond1")]
        public string ConsCustNameSecond1 { get; set; }
        [BookmarkMapping("ConsortiumName1")]
        public string ConsortiumName1 { get; set; }
        [BookmarkMapping("ContAmountFirst1")]
        public int ContAmountFirst1 { get; set; }
        [BookmarkMapping("ContAmountVatFirst1")]
        public int ContAmountVatFirst1 { get; set; }
        [BookmarkMapping("ContractDate1")]
        public string ContractDate1 { get; set; }
        [BookmarkMapping("ContractDate2")]
        public string ContractDate2 { get; set; }
        [BookmarkMapping("ContractDate3")]
        public string ContractDate3 { get; set; }
        [BookmarkMapping("ContractDate4")]
        public string ContractDate4 { get; set; }
        [BookmarkMapping("ContractDate5")]
        public string ContractDate5 { get; set; }
        [BookmarkMapping("ContractNo1")]
        public string ContractNo1 { get; set; }
        [BookmarkMapping("ContractNo2")]
        public string ContractNo2 { get; set; }
        [BookmarkMapping("CustName1")]
        public string CustName1 { get; set; }
        [BookmarkMapping("CustName2")]
        public string CustName2 { get; set; }
        [BookmarkMapping("CustName3")]
        public string CustName3 { get; set; }
        [BookmarkMapping("DateOfCreditFirst1")]
        public string DateOfCreditFirst1 { get; set; }
        [BookmarkMapping("DateOfCreditSecond1")]
        public string DateOfCreditSecond1 { get; set; }
        [BookmarkMapping("ExpAmountApproved1")]
        public decimal ExpAmountApproved1 { get; set; }
        [BookmarkMapping("ExpAmountApproved2")]
        public decimal ExpAmountApproved2 { get; set; }
        [BookmarkMapping("ExpAmountLoan1")]
        public decimal ExpAmountLoan1 { get; set; }
        [BookmarkMapping("Seq1st")]
        public int Seq1st { get; set; }
        [BookmarkMapping("LineServiceFeeDescription1st")]
        public string LineServiceFeeDescription1st { get; set; }
        [BookmarkMapping("LineServiceFeeAmtBefTax1st")]
        public decimal LineServiceFeeAmtBefTax1st { get; set; }
        [BookmarkMapping("LineServiceFeeVAT1st")]
        public decimal LineServiceFeeVAT1st { get; set; }
        [BookmarkMapping("LineServiceFeeAmtIncTax1st")]
        public decimal LineServiceFeeAmtIncTax1st { get; set; }
        [BookmarkMapping("LIneServiceFeeWHT1st")]
        public decimal LIneServiceFeeWHT1st { get; set; }
        [BookmarkMapping("LineServiceFeeNet1st")]
        public decimal LineServiceFeeNet1st { get; set; }
        [BookmarkMapping("Seq2nd")]
        public int Seq2nd { get; set; }
        [BookmarkMapping("LineServiceFeeDescription2nd")]
        public string LineServiceFeeDescription2nd { get; set; }
        [BookmarkMapping("LineServiceFeeAmtBefTax2nd")]
        public decimal LineServiceFeeAmtBefTax2nd { get; set; }
        [BookmarkMapping("LineServiceFeeVAT2nd")]
        public decimal LineServiceFeeVAT2nd { get; set; }
        [BookmarkMapping("LineServiceFeeAmtIncTax2nd")]
        public decimal LineServiceFeeAmtIncTax2nd { get; set; }
        [BookmarkMapping("LIneServiceFeeWHT2nd")]
        public decimal LIneServiceFeeWHT2nd { get; set; }
        [BookmarkMapping("LineServiceFeeNet2nd")]
        public decimal LineServiceFeeNet2nd { get; set; }
        [BookmarkMapping("Seq3rd")]
        public int Seq3rd { get; set; }
        [BookmarkMapping("LineServiceFeeDescription3rd")]
        public string LineServiceFeeDescription3rd { get; set; }

        [BookmarkMapping("LineServiceFeeAmtBefTax3rd")]
        public decimal LineServiceFeeAmtBefTax3rd { get; set; }
        [BookmarkMapping("LineServiceFeeVAT3rd")]
        public decimal LineServiceFeeVAT3rd { get; set; }
        [BookmarkMapping("LineServiceFeeAmtIncTax3rd")]
        public decimal LineServiceFeeAmtIncTax3rd { get; set; }
        [BookmarkMapping("LIneServiceFeeWHT3rd")]
        public decimal LIneServiceFeeWHT3rd { get; set; }
        [BookmarkMapping("LineServiceFeeNet3rd")]
        public decimal LineServiceFeeNet3rd { get; set; }
        [BookmarkMapping("Seq4th")]
        public int Seq4th { get; set; }
        [BookmarkMapping("LineServiceFeeDescription4th")]
        public string LineServiceFeeDescription4th { get; set; }
        [BookmarkMapping("LineServiceFeeAmtBefTax4th")]
        public decimal LineServiceFeeAmtBefTax4th { get; set; }
        [BookmarkMapping("LineServiceFeeVAT4th")]
        public decimal LineServiceFeeVAT4th { get; set; }
        [BookmarkMapping("LineServiceFeeAmtIncTax4th")]
        public decimal LineServiceFeeAmtIncTax4th { get; set; }
        [BookmarkMapping("LIneServiceFeeWHT4th")]
        public decimal LIneServiceFeeWHT4th { get; set; }
        [BookmarkMapping("LineServiceFeeNet4th")]
        public decimal LineServiceFeeNet4th { get; set; }
        [BookmarkMapping("Seq5th")]
        public int Seq5th { get; set; }
        [BookmarkMapping("LineServiceFeeDescription5th")]
        public string LineServiceFeeDescription5th { get; set; }
        [BookmarkMapping("LineServiceFeeAmtBefTax5th")]
        public decimal LineServiceFeeAmtBefTax5th { get; set; }
        [BookmarkMapping("LineServiceFeeVAT5th")]
        public decimal LineServiceFeeVAT5th { get; set; }
        [BookmarkMapping("LineServiceFeeAmtIncTax5th")]
        public decimal LineServiceFeeAmtIncTax5th { get; set; }
        [BookmarkMapping("LIneServiceFeeWHT5th")]
        public decimal LIneServiceFeeWHT5th { get; set; }
        [BookmarkMapping("LineServiceFeeNet5th")]
        public decimal LineServiceFeeNet5th { get; set; }
        [BookmarkMapping("Seq6th")]
        public int Seq6th { get; set; }
        [BookmarkMapping("LineServiceFeeDescription6th")]
        public string LineServiceFeeDescription6th { get; set; }
        [BookmarkMapping("LineServiceFeeAmtBefTax6th")]
        public decimal LineServiceFeeAmtBefTax6th { get; set; }
        [BookmarkMapping("LineServiceFeeVAT6th")]
        public decimal LineServiceFeeVAT6th { get; set; }
        [BookmarkMapping("LineServiceFeeAmtIncTax6th")]
        public decimal LineServiceFeeAmtIncTax6th { get; set; }

        [BookmarkMapping("LIneServiceFeeWHT6th")]
        public decimal LIneServiceFeeWHT6th { get; set; }

        [BookmarkMapping("LineServiceFeeNet6th")]
        public decimal LineServiceFeeNet6th { get; set; }
        [BookmarkMapping("Seq7th")]
        public int Seq7th { get; set; }
        [BookmarkMapping("LineServiceFeeDescription7th")]
        public string LineServiceFeeDescription7th { get; set; }
        [BookmarkMapping("LineServiceFeeAmtBefTax7th")]
        public decimal LineServiceFeeAmtBefTax7th { get; set; }
        [BookmarkMapping("LineServiceFeeVAT7th")]
        public decimal LineServiceFeeVAT7th { get; set; }
        [BookmarkMapping("LineServiceFeeAmtIncTax7th")]
        public decimal LineServiceFeeAmtIncTax7th { get; set; }
        [BookmarkMapping("LIneServiceFeeWHT7th")]
        public decimal LIneServiceFeeWHT7th { get; set; }
        [BookmarkMapping("LineServiceFeeNet7th")]
        public decimal LineServiceFeeNet7th { get; set; }
        [BookmarkMapping("Seq8th")]
        public int Seq8th { get; set; }
        [BookmarkMapping("LineServiceFeeDescription8th")]
        public string LineServiceFeeDescription8th { get; set; }
        [BookmarkMapping("LineServiceFeeAmtBefTax8th")]
        public decimal LineServiceFeeAmtBefTax8th { get; set; }
        [BookmarkMapping("LineServiceFeeVAT8th")]
        public decimal LineServiceFeeVAT8th { get; set; }
        [BookmarkMapping("LineServiceFeeAmtIncTax8th")]
        public decimal LineServiceFeeAmtIncTax8th { get; set; }
        [BookmarkMapping("LIneServiceFeeWHT8th")]
        public decimal LIneServiceFeeWHT8th { get; set; }
        [BookmarkMapping("LineServiceFeeNet8th")]
        public decimal LineServiceFeeNet8th { get; set; }
        [BookmarkMapping("Seq9th")]
        public int Seq9th { get; set; }

        [BookmarkMapping("LineServiceFeeDescription9th")]
        public string LineServiceFeeDescription9th { get; set; }


        [BookmarkMapping("LineServiceFeeAmtBefTax9th")]
        public decimal LineServiceFeeAmtBefTax9th { get; set; }
        [BookmarkMapping("LineServiceFeeVAT9th")]
        public decimal LineServiceFeeVAT9th { get; set; }
        [BookmarkMapping("LineServiceFeeAmtIncTax9th")]
        public decimal LineServiceFeeAmtIncTax9th { get; set; }
        [BookmarkMapping("LIneServiceFeeWHT9th")]
        public decimal LIneServiceFeeWHT9th { get; set; }
        [BookmarkMapping("LineServiceFeeNet9th")]
        public decimal LineServiceFeeNet9th { get; set; }
        [BookmarkMapping("Seq10th")]
        public int Seq10th { get; set; }
        [BookmarkMapping("LineServiceFeeDescription10th")]
        public string LineServiceFeeDescription10th { get; set; }
        [BookmarkMapping("LineServiceFeeAmtBefTax10th")]
        public decimal LineServiceFeeAmtBefTax10th { get; set; }
        [BookmarkMapping("LineServiceFeeVAT10th")]
        public decimal LineServiceFeeVAT10th { get; set; }
        [BookmarkMapping("LineServiceFeeAmtIncTax10th")]
        public decimal LineServiceFeeAmtIncTax10th { get; set; }
        [BookmarkMapping("LIneServiceFeeWHT10th")]
        public decimal LIneServiceFeeWHT10th { get; set; }
        [BookmarkMapping("LineServiceFeeNet10th")]
        public decimal LineServiceFeeNet10th { get; set; }
        [BookmarkMapping("TotalServiceFee1")]
        public decimal TotalServiceFee1 { get; set; }
        [BookmarkMapping("TotalServiceFee2")]
        public decimal TotalServiceFee2 { get; set; }
        [BookmarkMapping("BankChargeCheck1")]
        public decimal BankChargeCheck1 { get; set; }
        [BookmarkMapping("BankChargeCheck2")]
        public decimal BankChargeCheck2 { get; set; }
        [BookmarkMapping("ExpTransTotalCheck1")]
        public decimal ExpTransTotalCheck1 { get; set; }
        public List<ConsortiumTrans> ConsortiumTrans { get; set; }
        public List<ServiceFeeTrans> ServiceFeeTrans { get; set; }
        public List<ServiceFeeTrans> ServiceFeeTransSM { get; set; }
        public DateTime AgreementDate { get; set; }
        [BookmarkMapping("SMSeq1st")]
        public int SMSeq1st { get; set; }
        [BookmarkMapping("SMLineServiceFeeDescription1st")]
        public string SMLineServiceFeeDescription1st { get; set; }
        [BookmarkMapping("SMLineServiceFeeAmtBefTax1st")]
        public decimal SMLineServiceFeeAmtBefTax1st { get; set; }
        [BookmarkMapping("SMLineServiceFeeVAT1st")]
        public decimal SMLineServiceFeeVAT1st { get; set; }
        [BookmarkMapping("SMLineServiceFeeAmtIncTax1st")]
        public decimal SMLineServiceFeeAmtIncTax1st { get; set; }

        [BookmarkMapping("SMLIneServiceFeeWHT1st")]
        public decimal SMLIneServiceFeeWHT1st { get; set; }

        [BookmarkMapping("SMLineServiceFeeNet1st1")]
        public decimal SMLineServiceFeeNet1st1 { get; set; }

        [BookmarkMapping("SMLineServiceFeeNet1st2")]
        public decimal SMLineServiceFeeNet1st2 { get; set; }

        [BookmarkMapping("SMLineServiceFeeNet1st3")]
        public decimal SMLineServiceFeeNet1st3 { get; set; }

        [BookmarkMapping("SMBankChargeCheck1")]
        public decimal SMBankChargeCheck1 { get; set; }

        [BookmarkMapping("SMExpTransTotalCheck1")]
        public decimal SMExpTransTotalCheck1 { get; set; }

        [BookmarkMapping("SMSeq2nd")]
        public int SMSeq2nd { get; set; }

        [BookmarkMapping("SMLineServiceFeeDescription2nd")]
        public string SMLineServiceFeeDescription2nd { get; set; }

        [BookmarkMapping("SMLineServiceFeeAmtBefTax2nd")]
        public decimal SMLineServiceFeeAmtBefTax2nd { get; set; }

        [BookmarkMapping("SMLineServiceFeeVAT2nd")]
        public decimal SMLineServiceFeeVAT2nd { get; set; }

        [BookmarkMapping("SMLineServiceFeeAmtIncTax2nd")]
        public decimal SMLineServiceFeeAmtIncTax2nd { get; set; }

        [BookmarkMapping("SMLIneServiceFeeWHT2nd")]
        public decimal SMLIneServiceFeeWHT2nd { get; set; }

        [BookmarkMapping("SMLineServiceFeeNet2nd1")]
        public decimal SMLineServiceFeeNet2nd1 { get; set; }

        [BookmarkMapping("SMLineServiceFeeNet2nd2")]
        public decimal SMLineServiceFeeNet2nd2 { get; set; }

        [BookmarkMapping("SMLineServiceFeeNet2nd3")]
        public decimal SMLineServiceFeeNet2nd3 { get; set; }

        [BookmarkMapping("SMBankChargeCheck2")]
        public decimal SMBankChargeCheck2 { get; set; }

        [BookmarkMapping("SMExpTransTotalCheck2")]
        public decimal SMExpTransTotalCheck2 { get; set; }

        [BookmarkMapping("SMSeq3rd")]
        public int SMSeq3rd { get; set; }

        [BookmarkMapping("SMLineServiceFeeDescription3rd")]
        public string SMLineServiceFeeDescription3rd { get; set; }

        [BookmarkMapping("SMLineServiceFeeAmtBefTax3rd")]
        public decimal SMLineServiceFeeAmtBefTax3rd { get; set; }

        [BookmarkMapping("SMLineServiceFeeVAT3rd")]
        public decimal SMLineServiceFeeVAT3rd { get; set; }

        [BookmarkMapping("SMLineServiceFeeAmtIncTax3rd")]
        public decimal SMLineServiceFeeAmtIncTax3rd { get; set; }

        [BookmarkMapping("SMLIneServiceFeeWHT3rd")]
        public decimal SMLIneServiceFeeWHT3rd { get; set; }

        [BookmarkMapping("SMLineServiceFeeNet3rd1")]
        public decimal SMLineServiceFeeNet3rd1 { get; set; }

        [BookmarkMapping("SMLineServiceFeeNet3rd2")]
        public decimal SMLineServiceFeeNet3rd2 { get; set; }

        [BookmarkMapping("SMLineServiceFeeNet3rd3")]
        public decimal SMLineServiceFeeNet3rd3 { get; set; }

        [BookmarkMapping("SMBankChargeCheck3")]
        public decimal SMBankChargeCheck3 { get; set; }

        [BookmarkMapping("SMExpTransTotalCheck3")]
        public decimal SMExpTransTotalCheck3 { get; set; }

        [BookmarkMapping("MaketingName1")]
        public string MaketingName1 { get; set; }
        [BookmarkMapping("ExpAmountLoan2")]
        public decimal ExpAmountLoan2 { get; set; }
        [BookmarkMapping("NameProject1")]
        public string NameProject1 { get; set; }
        [BookmarkMapping("NameProject2")]
        public string NameProject2 { get; set; }

    }
}
