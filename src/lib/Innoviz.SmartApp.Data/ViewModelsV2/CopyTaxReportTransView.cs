﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class CopyTaxReportTransView : ResultBaseEntity
    {
        public int RefType { get; set; }
        public string RefGUID { get; set; }
        public int Month { get; set; }
        public string ResultLabel { get; set; }
        public string CustomerTableGUID { get; set; }
    }
}
