using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BillingResponsibleByListView : ViewCompanyBaseEntity
	{
		public string BillingResponsibleByGUID { get; set; }
		public string BillingResponsibleById { get; set; }
		public string Description { get; set; }
		public int BillingBy { get; set; }
	}
}
