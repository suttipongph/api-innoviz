using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CAReqCreditOutStandingItemView : ViewCompanyBaseEntity
	{
		public string CAReqCreditOutStandingGUID { get; set; }
		public decimal AccumRetentionAmount { get; set; }
		public decimal ApprovedCreditLimit { get; set; }
		public decimal ARBalance { get; set; }
		public string AsOfDate { get; set; }
		public string CAReqCreditOutStanding { get; set; }
		public string CreditAppRequestTableGUID { get; set; }
		public string CreditAppTableGUID { get; set; }
		public decimal CreditLimitBalance { get; set; }
		public string CreditLimitTypeGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public int ProductType { get; set; }
		public decimal ReserveToBeRefund { get; set; }
	}
}
