using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ContactTransItemView : ViewCompanyBaseEntity
	{
		public string ContactTransGUID { get; set; }
		public int ContactType { get; set; }
		public string ContactValue { get; set; }
		public string Description { get; set; }
		public string Extension { get; set; }
		public bool PrimaryContact { get; set; }
		public string RefGUID { get; set; }
		public string RefId { get; set; }
		public int RefType { get; set; }
	}
}
