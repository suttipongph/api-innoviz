using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class TaxValueListView : ViewDateEffectiveBaseEntity
	{
		public string TaxValueGUID { get; set; }
		public string TaxTableGUID { get; set; }
		public decimal Value { get; set; }
	}
}
