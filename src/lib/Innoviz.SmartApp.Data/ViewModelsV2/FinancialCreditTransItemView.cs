using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class FinancialCreditTransItemView : ViewCompanyBaseEntity
	{
		public string FinancialCreditTransGUID { get; set; }
		public decimal Amount { get; set; }
		public string BankGroupGUID { get; set; }
		public string CreditTypeGUID { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public string RefId { get; set; }
	}
}
