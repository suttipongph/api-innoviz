using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class TaxTableListView : ViewCompanyBaseEntity
	{
		public string TaxTableGUID { get; set; }
		public string Description { get; set; }
		public string TaxCode { get; set; }
	}
}
