using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BusinessCollateralSubTypeListView : ViewCompanyBaseEntity
	{
		public string BusinessCollateralSubTypeGUID { get; set; }
		public string BusinessCollateralTypeGUID { get; set; }
		public int AgreementOrdering { get; set; }
		public string AgreementRefText { get; set; }
		public string BusinessCollateralSubTypeId { get; set; }
		public string Description { get; set; }
		public string BusinessCollateralTypeId { get; set; }
		public string BusinessCollateralType_Values { get; set; }
		public bool DebtorClaims { get; set; }
	}
}
