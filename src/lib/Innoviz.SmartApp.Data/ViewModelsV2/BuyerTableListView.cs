using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BuyerTableListView : ViewCompanyBaseEntity
	{
		public string BuyerTableGUID { get; set; }
		public string BuyerId { get; set; }
		public string BuyerName { get; set; }
		public string PassportId { get; set; }
		public string TaxId { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string DocumentStatusGUID { get; set; }

	}
}
