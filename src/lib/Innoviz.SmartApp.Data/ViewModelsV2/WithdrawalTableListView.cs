using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class WithdrawalTableListView : ViewCompanyBaseEntity
	{
		public string WithdrawalTableGUID { get; set; }
		public string WithdrawalId { get; set; }
		public string Description { get; set; }
		public string CustomerTableGUID { get; set; }
		public string WithdrawalDate { get; set; }
		public string DueDate { get; set; }
		public string CreditTermGUID { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CreditTerm_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string CreditTerm_CreditTermId { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string CreditAppTableGUID { get; set; }
		public string OriginWithdrawalTableGUID { get; set; }
		public bool TermExtension { get; set; }
		public int NumberOfExtension { get; set; }
	}
}
