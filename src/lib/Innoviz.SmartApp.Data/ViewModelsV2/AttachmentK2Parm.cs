﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class AttachmentK2Parm
    {
        public string FileName { get; set; }
        public string RefGUID { get; set; }
        public int RefType { get; set; }
    }
}
