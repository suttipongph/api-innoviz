﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class PrintWithdrawalTermExtension
    {
        public string WithdrawalId { get; set; }
        public string DocumentStatus { get; set; }
        public string CustomerId { get; set; }
        public string BuyerId { get; set; }
        public bool TermExtension { get; set; }
        public int NumberOfExtension { get; set; }
        public string WithdrawalDate { get; set; }
        public string DueDate { get; set; }
        public string CreditTermId { get; set; }
        public decimal TotalInterestPct { get; set; }
        public decimal WithdrawalAmount { get; set; }
        public string WithdrawalTableGUID { get; set; }
    }
    public class PrintWithdrawalTermExtensionReporttView : ViewReportBaseEntity
    {
        public string WithdrawalId { get; set; }
        public string DocumentStatus { get; set; }
        public string CustomerId { get; set; }
        public string BuyerId { get; set; }
        public bool TermExtension { get; set; }
        public int NumberOfExtension { get; set; }
        public string WithdrawalDate { get; set; }
        public string DueDate { get; set; }
        public string CreditTermId { get; set; }
        public decimal TotalInterestPct { get; set; }
        public decimal WithdrawalAmount { get; set; }
        public string WithdrawalTableGUID { get; set; }
    }
}
