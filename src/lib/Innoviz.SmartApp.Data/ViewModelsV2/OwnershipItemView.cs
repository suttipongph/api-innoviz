using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class OwnershipItemView : ViewCompanyBaseEntity
	{
		public string OwnershipGUID { get; set; }
		public string Description { get; set; }
		public string OwnershipId { get; set; }
	}
}
