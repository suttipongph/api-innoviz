using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ServiceFeeCondTemplateLineItemView : ViewCompanyBaseEntity
	{
		public string ServiceFeeCondTemplateLineGUID { get; set; }
		public decimal AmountBeforeTax { get; set; }
		public string Description { get; set; }
		public string InvoiceRevenueTypeGUID { get; set; }
		public int Ordering { get; set; }
		public string ServiceFeeCondTemplateTableGUID { get; set; }
		public string serviceFeeCondTemplateTable_Value { get; set; }
		public int ProductType { get; set; }
	}
}
