﻿using System;
using System.Collections.Generic;
using System.Text;
using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class RptPrintVerificationReportParm : ViewReportBaseEntity
    {
        public string VerificationTableGUID { get; set; }
    }
}
