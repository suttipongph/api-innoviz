﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Attributes;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class GenBookmarkBuyerCreditApplicationView
    {
        [BookmarkMapping("CANumber")]
        public string CANumber { get; set; }
        [BookmarkMapping("RefCANumber")]
        public string RefCANumber { get; set; }
        [BookmarkMapping("CAStatus")]
        public string CAStatus { get; set; }
        [BookmarkMapping("CACreateDate")]
        public DateTime? CACreateDate { get; set; }
        [BookmarkMapping("CustomerName1")]
        public string CustomerName1 { get; set; }
        [BookmarkMapping("CustomerCode")]
        public string CustomerCode { get; set; }
        [BookmarkMapping("SalesResp")]
        public string SalesResp { get; set; }
        [BookmarkMapping("KYCHeader")]
        public string KYCHeader { get; set; }
        [BookmarkMapping("CreditScoring")]
        public string CreditScoring { get; set; }
        [BookmarkMapping("CADesp")]
        public string CADesp { get; set; }
        [BookmarkMapping("Purpose")]
        public string Purpose { get; set; }
        [BookmarkMapping("CARemark")]
        public string CARemark { get; set; }
        [BookmarkMapping("BuyerQtyPrivate")]
        public int BuyerQtyPrivate { get; set; }
        [BookmarkMapping("BuyerName1st_1")]
        public string BuyerName1st_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest1st_1")]
        public decimal BuyerCreditLimitRequest1st_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer1st_1")]
        public decimal BuyerCreditLimitAllCustomer1st_1 { get; set; }
        [BookmarkMapping("PurchasePercent1st_1")]
        public decimal PurchasePercent1st_1 { get; set; }
        [BookmarkMapping("AssignmentCondition1st_1")]
        public string AssignmentCondition1st_1 { get; set; }
        [BookmarkMapping("BillingResponsible1st_1")]
        public string BillingResponsible1st_1 { get; set; }
        [BookmarkMapping("PaymentMethod1st_1")]
        public string PaymentMethod1st_1 { get; set; }
        [BookmarkMapping("BuyerName2nd_1")]
        public string BuyerName2nd_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest2nd_1")]
        public decimal BuyerCreditLimitRequest2nd_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer2nd_1")]
        public decimal BuyerCreditLimitAllCustomer2nd_1 { get; set; }
        [BookmarkMapping("PurchasePercent2nd_1")]
        public decimal PurchasePercent2nd_1 { get; set; }
        [BookmarkMapping("AssignmentCondition2nd_1")]
        public string AssignmentCondition2nd_1 { get; set; }
        [BookmarkMapping("BillingResponsible2nd_1")]
        public string BillingResponsible2nd_1 { get; set; }
        [BookmarkMapping("PaymentMethod2nd_1")]
        public string PaymentMethod2nd_1 { get; set; }
        [BookmarkMapping("BuyerName3rd_1")]
        public string BuyerName3rd_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest3rd_1")]
        public decimal BuyerCreditLimitRequest3rd_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer3rd_1")]
        public decimal BuyerCreditLimitAllCustomer3rd_1 { get; set; }
        [BookmarkMapping("PurchasePercent3rd_1")]
        public decimal PurchasePercent3rd_1 { get; set; }
        [BookmarkMapping("AssignmentCondition3rd_1")]
        public string AssignmentCondition3rd_1 { get; set; }
        [BookmarkMapping("BillingResponsible3rd_1")]
        public string BillingResponsible3rd_1 { get; set; }
        [BookmarkMapping("PaymentMethod3rd_1")]
        public string PaymentMethod3rd_1 { get; set; }
        [BookmarkMapping("BuyerName4th_1")]
        public string BuyerName4th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest4th_1")]
        public decimal BuyerCreditLimitRequest4th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer4th_1")]
        public decimal BuyerCreditLimitAllCustomer4th_1 { get; set; }
        [BookmarkMapping("PurchasePercent4th_1")]
        public decimal PurchasePercent4th_1 { get; set; }
        [BookmarkMapping("AssignmentCondition4th_1")]
        public string AssignmentCondition4th_1 { get; set; }
        [BookmarkMapping("BillingResponsible4th_1")]
        public string BillingResponsible4th_1 { get; set; }
        [BookmarkMapping("PaymentMethod4th_1")]
        public string PaymentMethod4th_1 { get; set; }
        [BookmarkMapping("BuyerName5th_1")]
        public string BuyerName5th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest5th_1")]
        public decimal BuyerCreditLimitRequest5th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer5th_1")]
        public decimal BuyerCreditLimitAllCustomer5th_1 { get; set; }
        [BookmarkMapping("PurchasePercent5th_1")]
        public decimal PurchasePercent5th_1 { get; set; }
        [BookmarkMapping("AssignmentCondition5th_1")]
        public string AssignmentCondition5th_1 { get; set; }
        [BookmarkMapping("BillingResponsible5th_1")]
        public string BillingResponsible5th_1 { get; set; }
        [BookmarkMapping("PaymentMethod5th_1")]
        public string PaymentMethod5th_1 { get; set; }
        [BookmarkMapping("BuyerName6th_1")]
        public string BuyerName6th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest6th_1")]
        public decimal BuyerCreditLimitRequest6th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer6th_1")]
        public decimal BuyerCreditLimitAllCustomer6th_1 { get; set; }
        [BookmarkMapping("PurchasePercent6th_1")]
        public decimal PurchasePercent6th_1 { get; set; }
        [BookmarkMapping("AssignmentCondition6th_1")]
        public string AssignmentCondition6th_1 { get; set; }
        [BookmarkMapping("BillingResponsible6th_1")]
        public string BillingResponsible6th_1 { get; set; }
        [BookmarkMapping("PaymentMethod6th_1")]
        public string PaymentMethod6th_1 { get; set; }
        [BookmarkMapping("BuyerName7th_1")]
        public string BuyerName7th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest7th_1")]
        public decimal BuyerCreditLimitRequest7th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer7th_1")]
        public decimal BuyerCreditLimitAllCustomer7th_1 { get; set; }
        [BookmarkMapping("PurchasePercent7th_1")]
        public decimal PurchasePercent7th_1 { get; set; }
        [BookmarkMapping("AssignmentCondition7th_1")]
        public string AssignmentCondition7th_1 { get; set; }
        [BookmarkMapping("BillingResponsible7th_1")]
        public string BillingResponsible7th_1 { get; set; }
        [BookmarkMapping("PaymentMethod7th_1")]
        public string PaymentMethod7th_1 { get; set; }
        [BookmarkMapping("BuyerName8th_1")]
        public string BuyerName8th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest8th_1")]
        public decimal BuyerCreditLimitRequest8th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer8th_1")]
        public decimal BuyerCreditLimitAllCustomer8th_1 { get; set; }
        [BookmarkMapping("PurchasePercent8th_1")]
        public decimal PurchasePercent8th_1 { get; set; }
        [BookmarkMapping("AssignmentCondition8th_1")]
        public string AssignmentCondition8th_1 { get; set; }
        [BookmarkMapping("BillingResponsible8th_1")]
        public string BillingResponsible8th_1 { get; set; }
        [BookmarkMapping("PaymentMethod8th_1")]
        public string PaymentMethod8th_1 { get; set; }
        [BookmarkMapping("BuyerName9th_1")]
        public string BuyerName9th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest9th_1")]
        public decimal BuyerCreditLimitRequest9th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer9th_1")]
        public decimal BuyerCreditLimitAllCustomer9th_1 { get; set; }
        [BookmarkMapping("PurchasePercent9th_1")]
        public decimal PurchasePercent9th_1 { get; set; }
        [BookmarkMapping("AssignmentCondition9th_1")]
        public string AssignmentCondition9th_1 { get; set; }
        [BookmarkMapping("BillingResponsible9th_1")]
        public string BillingResponsible9th_1 { get; set; }
        [BookmarkMapping("PaymentMethod9th_1")]
        public string PaymentMethod9th_1 { get; set; }
        [BookmarkMapping("BuyerName10th_1")]
        public string BuyerName10th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest10th_1")]
        public decimal BuyerCreditLimitRequest10th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer10th_1")]
        public decimal BuyerCreditLimitAllCustomer10th_1 { get; set; }
        [BookmarkMapping("PurchasePercent10th_1")]
        public decimal PurchasePercent10th_1 { get; set; }
        [BookmarkMapping("AssignmentCondition10th_1")]
        public string AssignmentCondition10th_1 { get; set; }
        [BookmarkMapping("BillingResponsible10th_1")]
        public string BillingResponsible10th_1 { get; set; }
        [BookmarkMapping("PaymentMethod10th_1")]
        public string PaymentMethod10th_1 { get; set; }
        [BookmarkMapping("BuyerQtyGovernment")]
        public int BuyerQtyGovernment { get; set; }
        [BookmarkMapping("BuyerName11th_1")]
        public string BuyerName11th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest11th_1")]
        public decimal BuyerCreditLimitRequest11th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer11th_1")]
        public decimal BuyerCreditLimitAllCustomer11th_1 { get; set; }
        [BookmarkMapping("PurchasePercent11th_1")]
        public decimal PurchasePercent11th_1 { get; set; }
        [BookmarkMapping("AssignmentCondition11th_1")]
        public string AssignmentCondition11th_1 { get; set; }
        [BookmarkMapping("BillingResponsible11th_1")]
        public string BillingResponsible11th_1 { get; set; }
        [BookmarkMapping("PaymentMethod11th_1")]
        public string PaymentMethod11th_1 { get; set; }
        [BookmarkMapping("BuyerName12th_1")]
        public string BuyerName12th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest12th_1")]
        public decimal BuyerCreditLimitRequest12th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer12th_1")]
        public decimal BuyerCreditLimitAllCustomer12th_1 { get; set; }
        [BookmarkMapping("PurchasePercent12th_1")]
        public decimal PurchasePercent12th_1 { get; set; }
        [BookmarkMapping("AssignmentCondition12th_1")]
        public string AssignmentCondition12th_1 { get; set; }
        [BookmarkMapping("BillingResponsible12th_1")]
        public string BillingResponsible12th_1 { get; set; }
        [BookmarkMapping("PaymentMethod12th_1")]
        public string PaymentMethod12th_1 { get; set; }
        //[BookmarkMapping("BuyerName13th_1")]
        //public string BuyerName13th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest13th_1")]
        public decimal BuyerCreditLimitRequest13th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer13th_1")]
        public decimal BuyerCreditLimitAllCustomer13th_1 { get; set; }
        [BookmarkMapping("PurchasePercent13th_1")]
        public decimal PurchasePercent13th_1 { get; set; }
        [BookmarkMapping("AssignmentCondition13th_1")]
        public string AssignmentCondition13th_1 { get; set; }
        [BookmarkMapping("BillingResponsible13th_1")]
        public string BillingResponsible13th_1 { get; set; }
        [BookmarkMapping("PaymentMethod13th_1")]
        public string PaymentMethod13th_1 { get; set; }
        [BookmarkMapping("BuyerName14th_1")]
        public string BuyerName14th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest14th_1")]
        public decimal BuyerCreditLimitRequest14th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer14th_1")]
        public decimal BuyerCreditLimitAllCustomer14th_1 { get; set; }
        [BookmarkMapping("PurchasePercent14th_1")]
        public decimal PurchasePercent14th_1 { get; set; }
        [BookmarkMapping("AssignmentCondition14th_1")]
        public string AssignmentCondition14th_1 { get; set; }
        [BookmarkMapping("BillingResponsible14th_1")]
        public string BillingResponsible14th_1 { get; set; }
        [BookmarkMapping("PaymentMethod14th_1")]
        public string PaymentMethod14th_1 { get; set; }
        [BookmarkMapping("BuyerName15th_1")]
        public string BuyerName15th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest15th_1")]
        public decimal BuyerCreditLimitRequest15th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer15th_1")]
        public decimal BuyerCreditLimitAllCustomer15th_1 { get; set; }
        [BookmarkMapping("PurchasePercent15th_1")]
        public decimal PurchasePercent15th_1 { get; set; }
        [BookmarkMapping("AssignmentCondition15th_1")]
        public string AssignmentCondition15th_1 { get; set; }
        [BookmarkMapping("BillingResponsible15th_1")]
        public string BillingResponsible15th_1 { get; set; }
        [BookmarkMapping("PaymentMethod15th_1")]
        public string PaymentMethod15th_1 { get; set; }
        [BookmarkMapping("BuyerName16th_1")]
        public string BuyerName16th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest16th_1")]
        public decimal BuyerCreditLimitRequest16th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer16th_1")]
        public decimal BuyerCreditLimitAllCustomer16th_1 { get; set; }
        [BookmarkMapping("PurchasePercent16th_1")]
        public decimal PurchasePercent16th_1 { get; set; }
        [BookmarkMapping("AssignmentCondition16th_1")]
        public string AssignmentCondition16th_1 { get; set; }
        [BookmarkMapping("BillingResponsible16th_1")]
        public string BillingResponsible16th_1 { get; set; }
        [BookmarkMapping("PaymentMethod16th_1")]
        public string PaymentMethod16th_1 { get; set; }
        [BookmarkMapping("BuyerName17th_1")]
        public string BuyerName17th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest17th_1")]
        public decimal BuyerCreditLimitRequest17th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer17th_1")]
        public decimal BuyerCreditLimitAllCustomer17th_1 { get; set; }
        [BookmarkMapping("PurchasePercent17th_1")]
        public decimal PurchasePercent17th_1 { get; set; }
        [BookmarkMapping("AssignmentCondition17th_1")]
        public string AssignmentCondition17th_1 { get; set; }
        [BookmarkMapping("BillingResponsible17th_1")]
        public string BillingResponsible17th_1 { get; set; }
        [BookmarkMapping("PaymentMethod17th_1")]
        public string PaymentMethod17th_1 { get; set; }
        [BookmarkMapping("BuyerName18th_1")]
        public string BuyerName18th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest18th_1")]
        public decimal BuyerCreditLimitRequest18th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer18th_1")]
        public decimal BuyerCreditLimitAllCustomer18th_1 { get; set; }
        [BookmarkMapping("PurchasePercent18th_1")]
        public decimal PurchasePercent18th_1 { get; set; }
        [BookmarkMapping("AssignmentCondition18th_1")]
        public string AssignmentCondition18th_1 { get; set; }
        [BookmarkMapping("BillingResponsible18th_1")]
        public string BillingResponsible18th_1 { get; set; }
        [BookmarkMapping("PaymentMethod18th_1")]
        public string PaymentMethod18th_1 { get; set; }
        [BookmarkMapping("BuyerName19th_1")]
        public string BuyerName19th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest19th_1")]
        public decimal BuyerCreditLimitRequest19th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer19th_1")]
        public decimal BuyerCreditLimitAllCustomer19th_1 { get; set; }
        [BookmarkMapping("PurchasePercent19th_1")]
        public decimal PurchasePercent19th_1 { get; set; }
        [BookmarkMapping("AssignmentCondition19th_1")]
        public string AssignmentCondition19th_1 { get; set; }
        [BookmarkMapping("BillingResponsible19th_1")]
        public string BillingResponsible19th_1 { get; set; }
        [BookmarkMapping("PaymentMethod19th_1")]
        public string PaymentMethod19th_1 { get; set; }
        [BookmarkMapping("BuyerName20th_1")]
        public string BuyerName20th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest20th_1")]
        public decimal BuyerCreditLimitRequest20th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer20th_1")]
        public decimal BuyerCreditLimitAllCustomer20th_1 { get; set; }
        [BookmarkMapping("PurchasePercent20th_1")]
        public decimal PurchasePercent20th_1 { get; set; }
        [BookmarkMapping("AssignmentCondition20th_1")]
        public string AssignmentCondition20th_1 { get; set; }
        [BookmarkMapping("BillingResponsible20th_1")]
        public string BillingResponsible20th_1 { get; set; }
        [BookmarkMapping("PaymentMethod20th_1")]
        public string PaymentMethod20th_1 { get; set; }
        [BookmarkMapping("BusinessSegment1st_2")]
        public string BusinessSegment1st_2 { get; set; }
        [BookmarkMapping("BuyerNameCARequestLine1st_1")]
        public string BuyerNameCARequestLine1st_1 { get; set; }
        [BookmarkMapping("BuyerTaxID1st_1")]
        public string BuyerTaxID1st_1 { get; set; }
        [BookmarkMapping("BuyerAddress1st_1")]
        public string BuyerAddress1st_1 { get; set; }
        [BookmarkMapping("BuyerCompanyRegisteredDate1st_1")]
        public DateTime? BuyerCompanyRegisteredDate1st_1 { get; set; }
        [BookmarkMapping("BuyerLineOfBusiness1st_1")]
        public string BuyerLineOfBusiness1st_1 { get; set; }
        [BookmarkMapping("BuyerBlacklistStatus1st_1")]
        public string BuyerBlacklistStatus1st_1 { get; set; }
        [BookmarkMapping("BuyerCreditScoring1st_1")]
        public string BuyerCreditScoring1st_1 { get; set; }
        [BookmarkMapping("BuyerFinYearFirst1_1st_1")]
        public int BuyerFinYearFirst1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalFirst1_1st_1")]
        public decimal BuyerFinRegisteredCapitalFirst1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalFirst1_1st_1")]
        public decimal BuyerFinPaidCapitalFirst1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetFirst1_1st_1")]
        public decimal BuyerFinTotalAssetFirst1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilityFirst1_1st_1")]
        public decimal BuyerFinTotalLiabilityFirst1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquityFirst1_1st_1")]
        public decimal BuyerFinTotalEquityFirst1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinYearFirst2_1st_1")]
        public int BuyerFinYearFirst2_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueFirst1_1st_1")]
        public decimal BuyerFinTotalRevenueFirst1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSFirst1_1st_1")]
        public decimal BuyerFinTotalCOGSFirst1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitFirst1_1st_1")]
        public decimal BuyerFinTotalGrossProfitFirst1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpFirst1_1st_1")]
        public decimal BuyerFinTotalOperExpFirst1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitFirst1_1st_1")]
        public decimal BuyerFinTotalNetProfitFirst1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinYearSecond1_1st_1")]
        public int BuyerFinYearSecond1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalSecond1_1st_1")]
        public decimal BuyerFinRegisteredCapitalSecond1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalSecond1_1st_1")]
        public decimal BuyerFinPaidCapitalSecond1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetSecond1_1st_1")]
        public decimal BuyerFinTotalAssetSecond1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilitySecond1_1st_1")]
        public decimal BuyerFinTotalLiabilitySecond1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquitySecond1_1st_1")]
        public decimal BuyerFinTotalEquitySecond1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinYearSecond2_1st_1")]
        public int BuyerFinYearSecond2_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueSecond1_1st_1")]
        public decimal BuyerFinTotalRevenueSecond1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSSecond1_1st_1")]
        public decimal BuyerFinTotalCOGSSecond1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitSecond1_1st_1")]
        public decimal BuyerFinTotalGrossProfitSecond1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpSecond1_1st_1")]
        public decimal BuyerFinTotalOperExpSecond1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitSecond1_1st_1")]
        public decimal BuyerFinTotalNetProfitSecond1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinYearThird1_1st_1")]
        public int BuyerFinYearThird1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalThird1_1st_1")]
        public decimal BuyerFinRegisteredCapitalThird1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalThird1_1st_1")]
        public decimal BuyerFinPaidCapitalThird1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetThird1_1st_1")]
        public decimal BuyerFinTotalAssetThird1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilityThird1_1st_1")]
        public decimal BuyerFinTotalLiabilityThird1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquityThird1_1st_1")]
        public decimal BuyerFinTotalEquityThird1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinYearThird2_1st_1")]
        public int BuyerFinYearThird2_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueThird1_1st_1")]
        public decimal BuyerFinTotalRevenueThird1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSThird1_1st_1")]
        public decimal BuyerFinTotalCOGSThird1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitThird1_1st_1")]
        public decimal BuyerFinTotalGrossProfitThird1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpThird1_1st_1")]
        public decimal BuyerFinTotalOperExpThird1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitThird1_1st_1")]
        public decimal BuyerFinTotalNetProfitThird1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinNetProfitPercent1_1st_1")]
        public decimal BuyerFinNetProfitPercent1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinlDE1_1st_1")]
        public decimal BuyerFinlDE1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinQuickRatio1_1st_1")]
        public decimal BuyerFinQuickRatio1_1st_1 { get; set; }
        [BookmarkMapping("BuyerFinIntCoverageRatio1_1st_1")]
        public decimal BuyerFinIntCoverageRatio1_1st_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest1st_2")]
        public decimal BuyerCreditLimitRequest1st_2 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer1st_2")]
        public decimal BuyerCreditLimitAllCustomer1st_2 { get; set; }
        [BookmarkMapping("PurchasePercent1st_2")]
        public decimal PurchasePercent1st_2 { get; set; }
        [BookmarkMapping("AssignmentCondition1st_2")]
        public string AssignmentCondition1st_2 { get; set; }
        [BookmarkMapping("FactoringPurchaseFee1st_1")]
        public decimal FactoringPurchaseFee1st_1 { get; set; }
        [BookmarkMapping("FactoringPurchaseFeeCalBase1st_1")]
        public string FactoringPurchaseFeeCalBase1st_1 { get; set; }
        [BookmarkMapping("BillingResponsible1st_2")]
        public string BillingResponsible1st_2 { get; set; }
        [BookmarkMapping("AcceptanceDocument1st_1")]
        public string AcceptanceDocument1st_1 { get; set; }
        [BookmarkMapping("BillingDescriptiong1st_1")]
        public string BillingDescriptiong1st_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPerson1st_1")]
        public string BuyerBillingContactPerson1st_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPersonPosition1st_1")]
        public string BuyerBillingContactPersonPosition1st_1 { get; set; }
        [BookmarkMapping("BuyerBillingAddress1st_1")]
        public string BuyerBillingAddress1st_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPersonTel1st_1")]
        public string BuyerBillingContactPersonTel1st_1 { get; set; }
        [BookmarkMapping("BillingDocumentFirst1_1st_1")]
        public string BillingDocumentFirst1_1st_1 { get; set; }
        [BookmarkMapping("BillingDocumentSecond1_1st_1")]
        public string BillingDocumentSecond1_1st_1 { get; set; }
        [BookmarkMapping("BillingDocumentThird1_1st_1")]
        public string BillingDocumentThird1_1st_1 { get; set; }
        [BookmarkMapping("BillingDocumentFourth_1st_1")]
        public string BillingDocumentFourth_1st_1 { get; set; }
        [BookmarkMapping("BillingDocumentFifth_1st_1")]
        public string BillingDocumentFifth_1st_1 { get; set; }
        [BookmarkMapping("BillingDocumentSixth1_1st_1")]
        public string BillingDocumentSixth1_1st_1 { get; set; }
        [BookmarkMapping("BillingDocumentSeventh_1st_1")]
        public string BillingDocumentSeventh_1st_1 { get; set; }
        [BookmarkMapping("BillingDocumentEigth1_1st_1")]
        public string BillingDocumentEigth1_1st_1 { get; set; }
        [BookmarkMapping("BillingDocumentNineth1_1st_1")]
        public string BillingDocumentNineth1_1st_1 { get; set; }
        [BookmarkMapping("BillingDocumentTenth_1st_1")]
        public string BillingDocumentTenth_1st_1 { get; set; }
        [BookmarkMapping("ReceiptDescriptiong1st_1")]
        public string ReceiptDescriptiong1st_1 { get; set; }
        [BookmarkMapping("PaymentMethod1st_2")]
        public string PaymentMethod1st_2 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPerson1st_1")]
        public string BuyerReceiptContactPerson1st_1 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPersonPosition1st_1")]
        public string BuyerReceiptContactPersonPosition1st_1 { get; set; }
        [BookmarkMapping("BuyerReceiptAddress1st_1")]
        public string BuyerReceiptAddress1st_1 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPersonTel1st_1")]
        public string BuyerReceiptContactPersonTel1st_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFirst1_1st_1")]
        public string ReceiptDocumentFirst1_1st_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSecond1_1st_1")]
        public string ReceiptDocumentSecond1_1st_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentThird1_1st_1")]
        public string ReceiptDocumentThird1_1st_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFourth_1st_1")]
        public string ReceiptDocumentFourth_1st_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFifth_1st_1")]
        public string ReceiptDocumentFifth_1st_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSixth1_1st_1")]
        public string ReceiptDocumentSixth1_1st_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSeventh_1st_1")]
        public string ReceiptDocumentSeventh_1st_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentEigth1_1st_1")]
        public string ReceiptDocumentEigth1_1st_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentNineth1_1st_1")]
        public string ReceiptDocumentNineth1_1st_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentTenth_1st_1")]
        public string ReceiptDocumentTenth_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc01_1st_1")]
        public string ServiceFeeDesc01_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt01_1st_1")]
        public decimal ServiceFeeAmt01_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark01_1st_1")]
        public string ServiceFeeRemark01_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc02_1st_1")]
        public string ServiceFeeDesc02_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt02_1st_1")]
        public decimal ServiceFeeAmt02_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark02_1st_1")]
        public string ServiceFeeRemark02_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc03_1st_1")]
        public string ServiceFeeDesc03_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt03_1st_1")]
        public decimal ServiceFeeAmt03_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark03_1st_1")]
        public string ServiceFeeRemark03_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc04_1st_1")]
        public string ServiceFeeDesc04_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt04_1st_1")]
        public decimal ServiceFeeAmt04_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark04_1st_1")]
        public string ServiceFeeRemark04_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc05_1st_1")]
        public string ServiceFeeDesc05_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt05_1st_1")]
        public decimal ServiceFeeAmt05_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark05_1st_1")]
        public string ServiceFeeRemark05_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc06_1st_1")]
        public string ServiceFeeDesc06_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt06_1st_1")]
        public decimal ServiceFeeAmt06_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark06_1st_1")]
        public string ServiceFeeRemark06_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc07_1st_1")]
        public string ServiceFeeDesc07_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt07_1st_1")]
        public decimal ServiceFeeAmt07_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark07_1st_1")]
        public string ServiceFeeRemark07_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc08_1st_1")]
        public string ServiceFeeDesc08_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt08_1st_1")]
        public decimal ServiceFeeAmt08_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark08_1st_1")]
        public string ServiceFeeRemark08_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc09_1st_1")]
        public string ServiceFeeDesc09_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt09_1st_1")]
        public decimal ServiceFeeAmt09_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark09_1st_1")]
        public string ServiceFeeRemark09_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc10_1st_1")]
        public string ServiceFeeDesc10_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt10_1st_1")]
        public decimal ServiceFeeAmt10_1st_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark10_1st_1")]
        public string ServiceFeeRemark10_1st_1 { get; set; }
        [BookmarkMapping("SumServiceFeeAmt_1st_1")]
        public decimal SumServiceFeeAmt_1st_1 { get; set; }
        [BookmarkMapping("MarketingComment1st")]
        public string MarketingComment1st { get; set; }
        [BookmarkMapping("CreditComment1st")]
        public string CreditComment1st { get; set; }
        [BookmarkMapping("ApproverComment1st")]
        public string ApproverComment1st { get; set; }
        [BookmarkMapping("BusinessSegment2nd_2")]
        public string BusinessSegment2nd_2 { get; set; }
        [BookmarkMapping("BuyerNameCARequestLine2nd_1")]
        public string BuyerNameCARequestLine2nd_1 { get; set; }
        [BookmarkMapping("BuyerTaxID2nd_1")]
        public string BuyerTaxID2nd_1 { get; set; }
        [BookmarkMapping("BuyerAddress2nd_1")]
        public string BuyerAddress2nd_1 { get; set; }
        [BookmarkMapping("BuyerCompanyRegisteredDate2nd_1")]
        public DateTime? BuyerCompanyRegisteredDate2nd_1 { get; set; }
        [BookmarkMapping("BuyerLineOfBusiness2nd_1")]
        public string BuyerLineOfBusiness2nd_1 { get; set; }
        [BookmarkMapping("BuyerBlacklistStatus2nd_1")]
        public string BuyerBlacklistStatus2nd_1 { get; set; }
        [BookmarkMapping("BuyerCreditScoring2nd_1")]
        public string BuyerCreditScoring2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinYearFirst1_2nd_1")]
        public int BuyerFinYearFirst1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalFirst1_2nd_1")]
        public decimal BuyerFinRegisteredCapitalFirst1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalFirst1_2nd_1")]
        public decimal BuyerFinPaidCapitalFirst1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetFirst1_2nd_1")]
        public decimal BuyerFinTotalAssetFirst1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilityFirst1_2nd_1")]
        public decimal BuyerFinTotalLiabilityFirst1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquityFirst1_2nd_1")]
        public decimal BuyerFinTotalEquityFirst1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinYearFirst2_2nd_1")]
        public int BuyerFinYearFirst2_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueFirst1_2nd_1")]
        public decimal BuyerFinTotalRevenueFirst1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSFirst1_2nd_1")]
        public decimal BuyerFinTotalCOGSFirst1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitFirst1_2nd_1")]
        public decimal BuyerFinTotalGrossProfitFirst1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpFirst1_2nd_1")]
        public decimal BuyerFinTotalOperExpFirst1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitFirst1_2nd_1")]
        public decimal BuyerFinTotalNetProfitFirst1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinYearSecond1_2nd_1")]
        public int BuyerFinYearSecond1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalSecond1_2nd_1")]
        public decimal BuyerFinRegisteredCapitalSecond1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalSecond1_2nd_1")]
        public decimal BuyerFinPaidCapitalSecond1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetSecond1_2nd_1")]
        public decimal BuyerFinTotalAssetSecond1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilitySecond1_2nd_1")]
        public decimal BuyerFinTotalLiabilitySecond1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquitySecond1_2nd_1")]
        public decimal BuyerFinTotalEquitySecond1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinYearSecond2_2nd_1")]
        public int BuyerFinYearSecond2_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueSecond1_2nd_1")]
        public decimal BuyerFinTotalRevenueSecond1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSSecond1_2nd_1")]
        public decimal BuyerFinTotalCOGSSecond1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitSecond1_2nd_1")]
        public decimal BuyerFinTotalGrossProfitSecond1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpSecond1_2nd_1")]
        public decimal BuyerFinTotalOperExpSecond1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitSecond1_2nd_1")]
        public decimal BuyerFinTotalNetProfitSecond1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinYearThird1_2nd_1")]
        public int BuyerFinYearThird1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalThird1_2nd_1")]
        public decimal BuyerFinRegisteredCapitalThird1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalThird1_2nd_1")]
        public decimal BuyerFinPaidCapitalThird1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetThird1_2nd_1")]
        public decimal BuyerFinTotalAssetThird1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilityThird1_2nd_1")]
        public decimal BuyerFinTotalLiabilityThird1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquityThird1_2nd_1")]
        public decimal BuyerFinTotalEquityThird1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinYearThird2_2nd_1")]
        public int BuyerFinYearThird2_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueThird1_2nd_1")]
        public decimal BuyerFinTotalRevenueThird1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSThird1_2nd_1")]
        public decimal BuyerFinTotalCOGSThird1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitThird1_2nd_1")]
        public decimal BuyerFinTotalGrossProfitThird1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpThird1_2nd_1")]
        public decimal BuyerFinTotalOperExpThird1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitThird1_2nd_1")]
        public decimal BuyerFinTotalNetProfitThird1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinNetProfitPercent1_2nd_1")]
        public decimal BuyerFinNetProfitPercent1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinlDE1_2nd_1")]
        public decimal BuyerFinlDE1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinQuickRatio1_2nd_1")]
        public decimal BuyerFinQuickRatio1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerFinIntCoverageRatio1_2nd_1")]
        public decimal BuyerFinIntCoverageRatio1_2nd_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest2nd_2")]
        public decimal BuyerCreditLimitRequest2nd_2 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer2nd_2")]
        public decimal BuyerCreditLimitAllCustomer2nd_2 { get; set; }
        [BookmarkMapping("PurchasePercent2nd_2")]
        public decimal PurchasePercent2nd_2 { get; set; }
        [BookmarkMapping("AssignmentCondition2nd_2")]
        public string AssignmentCondition2nd_2 { get; set; }
        [BookmarkMapping("FactoringPurchaseFee2nd_1")]
        public decimal FactoringPurchaseFee2nd_1 { get; set; }
        [BookmarkMapping("FactoringPurchaseFeeCalBase2nd_1")]
        public string FactoringPurchaseFeeCalBase2nd_1 { get; set; }
        [BookmarkMapping("BillingResponsible2nd_2")]
        public string BillingResponsible2nd_2 { get; set; }
        [BookmarkMapping("AcceptanceDocument2nd_1")]
        public string AcceptanceDocument2nd_1 { get; set; }
        [BookmarkMapping("BillingDescriptiong2nd_1")]
        public string BillingDescriptiong2nd_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPerson2nd_1")]
        public string BuyerBillingContactPerson2nd_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPersonPosition2nd_1")]
        public string BuyerBillingContactPersonPosition2nd_1 { get; set; }
        [BookmarkMapping("BuyerBillingAddress2nd_1")]
        public string BuyerBillingAddress2nd_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPersonTel2nd_1")]
        public string BuyerBillingContactPersonTel2nd_1 { get; set; }
        [BookmarkMapping("BillingDocumentFirst1_2nd_1")]
        public string BillingDocumentFirst1_2nd_1 { get; set; }
        [BookmarkMapping("BillingDocumentSecond1_2nd_1")]
        public string BillingDocumentSecond1_2nd_1 { get; set; }
        [BookmarkMapping("BillingDocumentThird1_2nd_1")]
        public string BillingDocumentThird1_2nd_1 { get; set; }
        [BookmarkMapping("BillingDocumentFourth_2nd_1")]
        public string BillingDocumentFourth_2nd_1 { get; set; }
        [BookmarkMapping("BillingDocumentFifth_2nd_1")]
        public string BillingDocumentFifth_2nd_1 { get; set; }
        [BookmarkMapping("BillingDocumentSixth1_2nd_1")]
        public string BillingDocumentSixth1_2nd_1 { get; set; }
        [BookmarkMapping("BillingDocumentSeventh_2nd_1")]
        public string BillingDocumentSeventh_2nd_1 { get; set; }
        [BookmarkMapping("BillingDocumentEigth1_2nd_1")]
        public string BillingDocumentEigth1_2nd_1 { get; set; }
        [BookmarkMapping("BillingDocumentNineth1_2nd_1")]
        public string BillingDocumentNineth1_2nd_1 { get; set; }
        [BookmarkMapping("BillingDocumentTenth_2nd_1")]
        public string BillingDocumentTenth_2nd_1 { get; set; }
        [BookmarkMapping("ReceiptDescriptiong2nd_1")]
        public string ReceiptDescriptiong2nd_1 { get; set; }
        [BookmarkMapping("PaymentMethod2nd_2")]
        public string PaymentMethod2nd_2 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPerson2nd_1")]
        public string BuyerReceiptContactPerson2nd_1 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPersonPosition2nd_1")]
        public string BuyerReceiptContactPersonPosition2nd_1 { get; set; }
        [BookmarkMapping("BuyerReceiptAddress2nd_1")]
        public string BuyerReceiptAddress2nd_1 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPersonTel2nd_1")]
        public string BuyerReceiptContactPersonTel2nd_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFirst1_2nd_1")]
        public string ReceiptDocumentFirst1_2nd_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSecond1_2nd_1")]
        public string ReceiptDocumentSecond1_2nd_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentThird1_2nd_1")]
        public string ReceiptDocumentThird1_2nd_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFourth_2nd_1")]
        public string ReceiptDocumentFourth_2nd_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFifth_2nd_1")]
        public string ReceiptDocumentFifth_2nd_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSixth1_2nd_1")]
        public string ReceiptDocumentSixth1_2nd_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSeventh_2nd_1")]
        public string ReceiptDocumentSeventh_2nd_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentEigth1_2nd_1")]
        public string ReceiptDocumentEigth1_2nd_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentNineth1_2nd_1")]
        public string ReceiptDocumentNineth1_2nd_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentTenth_2nd_1")]
        public string ReceiptDocumentTenth_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc01_2nd_1")]
        public string ServiceFeeDesc01_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt01_2nd_1")]
        public decimal ServiceFeeAmt01_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark01_2nd_1")]
        public string ServiceFeeRemark01_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc02_2nd_1")]
        public string ServiceFeeDesc02_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt02_2nd_1")]
        public decimal ServiceFeeAmt02_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark02_2nd_1")]
        public string ServiceFeeRemark02_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc03_2nd_1")]
        public string ServiceFeeDesc03_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt03_2nd_1")]
        public decimal ServiceFeeAmt03_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark03_2nd_1")]
        public string ServiceFeeRemark03_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc04_2nd_1")]
        public string ServiceFeeDesc04_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt04_2nd_1")]
        public decimal ServiceFeeAmt04_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark04_2nd_1")]
        public string ServiceFeeRemark04_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc05_2nd_1")]
        public string ServiceFeeDesc05_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt05_2nd_1")]
        public decimal ServiceFeeAmt05_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark05_2nd_1")]
        public string ServiceFeeRemark05_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc06_2nd_1")]
        public string ServiceFeeDesc06_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt06_2nd_1")]
        public decimal ServiceFeeAmt06_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark06_2nd_1")]
        public string ServiceFeeRemark06_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc07_2nd_1")]
        public string ServiceFeeDesc07_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt07_2nd_1")]
        public decimal ServiceFeeAmt07_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark07_2nd_1")]
        public string ServiceFeeRemark07_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc08_2nd_1")]
        public string ServiceFeeDesc08_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt08_2nd_1")]
        public decimal ServiceFeeAmt08_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark08_2nd_1")]
        public string ServiceFeeRemark08_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc09_2nd_1")]
        public string ServiceFeeDesc09_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt09_2nd_1")]
        public decimal ServiceFeeAmt09_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark09_2nd_1")]
        public string ServiceFeeRemark09_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc10_2nd_1")]
        public string ServiceFeeDesc10_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt10_2nd_1")]
        public decimal ServiceFeeAmt10_2nd_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark10_2nd_1")]
        public string ServiceFeeRemark10_2nd_1 { get; set; }
        [BookmarkMapping("SumServiceFeeAmt_2nd_1")]
        public decimal SumServiceFeeAmt_2nd_1 { get; set; }
        [BookmarkMapping("MarketingComment2nd")]
        public string MarketingComment2nd { get; set; }
        [BookmarkMapping("CreditComment2nd")]
        public string CreditComment2nd { get; set; }
        [BookmarkMapping("ApproverComment2nd")]
        public string ApproverComment2nd { get; set; }
        [BookmarkMapping("BusinessSegment3rd_2")]
        public string BusinessSegment3rd_2 { get; set; }
        [BookmarkMapping("BuyerNameCARequestLine3rd_1")]
        public string BuyerNameCARequestLine3rd_1 { get; set; }
        [BookmarkMapping("BuyerTaxID3rd_1")]
        public string BuyerTaxID3rd_1 { get; set; }
        [BookmarkMapping("BuyerAddress3rd_1")]
        public string BuyerAddress3rd_1 { get; set; }
        [BookmarkMapping("BuyerCompanyRegisteredDate3rd_1")]
        public DateTime? BuyerCompanyRegisteredDate3rd_1 { get; set; }
        [BookmarkMapping("BuyerLineOfBusiness3rd_1")]
        public string BuyerLineOfBusiness3rd_1 { get; set; }
        [BookmarkMapping("BuyerBlacklistStatus3rd_1")]
        public string BuyerBlacklistStatus3rd_1 { get; set; }
        [BookmarkMapping("BuyerCreditScoring3rd_1")]
        public string BuyerCreditScoring3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinYearFirst1_3rd_1")]
        public int BuyerFinYearFirst1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalFirst1_3rd_1")]
        public decimal BuyerFinRegisteredCapitalFirst1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalFirst1_3rd_1")]
        public decimal BuyerFinPaidCapitalFirst1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetFirst1_3rd_1")]
        public decimal BuyerFinTotalAssetFirst1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilityFirst1_3rd_1")]
        public decimal BuyerFinTotalLiabilityFirst1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquityFirst1_3rd_1")]
        public decimal BuyerFinTotalEquityFirst1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinYearFirst2_3rd_1")]
        public int BuyerFinYearFirst2_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueFirst1_3rd_1")]
        public decimal BuyerFinTotalRevenueFirst1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSFirst1_3rd_1")]
        public decimal BuyerFinTotalCOGSFirst1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitFirst1_3rd_1")]
        public decimal BuyerFinTotalGrossProfitFirst1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpFirst1_3rd_1")]
        public decimal BuyerFinTotalOperExpFirst1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitFirst1_3rd_1")]
        public decimal BuyerFinTotalNetProfitFirst1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinYearSecond1_3rd_1")]
        public int BuyerFinYearSecond1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalSecond1_3rd_1")]
        public decimal BuyerFinRegisteredCapitalSecond1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalSecond1_3rd_1")]
        public decimal BuyerFinPaidCapitalSecond1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetSecond1_3rd_1")]
        public decimal BuyerFinTotalAssetSecond1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilitySecond1_3rd_1")]
        public decimal BuyerFinTotalLiabilitySecond1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquitySecond1_3rd_1")]
        public decimal BuyerFinTotalEquitySecond1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinYearSecond2_3rd_1")]
        public int BuyerFinYearSecond2_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueSecond1_3rd_1")]
        public decimal BuyerFinTotalRevenueSecond1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSSecond1_3rd_1")]
        public decimal BuyerFinTotalCOGSSecond1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitSecond1_3rd_1")]
        public decimal BuyerFinTotalGrossProfitSecond1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpSecond1_3rd_1")]
        public decimal BuyerFinTotalOperExpSecond1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitSecond1_3rd_1")]
        public decimal BuyerFinTotalNetProfitSecond1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinYearThird1_3rd_1")]
        public int BuyerFinYearThird1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalThird1_3rd_1")]
        public decimal BuyerFinRegisteredCapitalThird1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalThird1_3rd_1")]
        public decimal BuyerFinPaidCapitalThird1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetThird1_3rd_1")]
        public decimal BuyerFinTotalAssetThird1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilityThird1_3rd_1")]
        public decimal BuyerFinTotalLiabilityThird1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquityThird1_3rd_1")]
        public decimal BuyerFinTotalEquityThird1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinYearThird2_3rd_1")]
        public int BuyerFinYearThird2_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueThird1_3rd_1")]
        public decimal BuyerFinTotalRevenueThird1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSThird1_3rd_1")]
        public decimal BuyerFinTotalCOGSThird1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitThird1_3rd_1")]
        public decimal BuyerFinTotalGrossProfitThird1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpThird1_3rd_1")]
        public decimal BuyerFinTotalOperExpThird1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitThird1_3rd_1")]
        public decimal BuyerFinTotalNetProfitThird1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinNetProfitPercent1_3rd_1")]
        public decimal BuyerFinNetProfitPercent1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinlDE1_3rd_1")]
        public decimal BuyerFinlDE1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinQuickRatio1_3rd_1")]
        public decimal BuyerFinQuickRatio1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerFinIntCoverageRatio1_3rd_1")]
        public decimal BuyerFinIntCoverageRatio1_3rd_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest3rd_2")]
        public decimal BuyerCreditLimitRequest3rd_2 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer3rd_2")]
        public decimal BuyerCreditLimitAllCustomer3rd_2 { get; set; }
        [BookmarkMapping("PurchasePercent3rd_2")]
        public decimal PurchasePercent3rd_2 { get; set; }
        [BookmarkMapping("AssignmentCondition3rd_2")]
        public string AssignmentCondition3rd_2 { get; set; }
        [BookmarkMapping("FactoringPurchaseFee3rd_1")]
        public decimal FactoringPurchaseFee3rd_1 { get; set; }
        [BookmarkMapping("FactoringPurchaseFeeCalBase3rd_1")]
        public string FactoringPurchaseFeeCalBase3rd_1 { get; set; }
        [BookmarkMapping("BillingResponsible3rd_2")]
        public string BillingResponsible3rd_2 { get; set; }
        [BookmarkMapping("AcceptanceDocument3rd_1")]
        public string AcceptanceDocument3rd_1 { get; set; }
        [BookmarkMapping("BillingDescriptiong3rd_1")]
        public string BillingDescriptiong3rd_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPerson3rd_1")]
        public string BuyerBillingContactPerson3rd_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPersonPosition3rd_1")]
        public string BuyerBillingContactPersonPosition3rd_1 { get; set; }
        [BookmarkMapping("BuyerBillingAddress3rd_1")]
        public string BuyerBillingAddress3rd_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPersonTel3rd_1")]
        public string BuyerBillingContactPersonTel3rd_1 { get; set; }
        [BookmarkMapping("BillingDocumentFirst1_3rd_1")]
        public string BillingDocumentFirst1_3rd_1 { get; set; }
        [BookmarkMapping("BillingDocumentSecond1_3rd_1")]
        public string BillingDocumentSecond1_3rd_1 { get; set; }
        [BookmarkMapping("BillingDocumentThird1_3rd_1")]
        public string BillingDocumentThird1_3rd_1 { get; set; }
        [BookmarkMapping("BillingDocumentFourth_3rd_1")]
        public string BillingDocumentFourth_3rd_1 { get; set; }
        [BookmarkMapping("BillingDocumentFifth_3rd_1")]
        public string BillingDocumentFifth_3rd_1 { get; set; }
        [BookmarkMapping("BillingDocumentSixth1_3rd_1")]
        public string BillingDocumentSixth1_3rd_1 { get; set; }
        [BookmarkMapping("BillingDocumentSeventh_3rd_1")]
        public string BillingDocumentSeventh_3rd_1 { get; set; }
        [BookmarkMapping("BillingDocumentEigth1_3rd_1")]
        public string BillingDocumentEigth1_3rd_1 { get; set; }
        [BookmarkMapping("BillingDocumentNineth1_3rd_1")]
        public string BillingDocumentNineth1_3rd_1 { get; set; }
        [BookmarkMapping("BillingDocumentTenth_3rd_1")]
        public string BillingDocumentTenth_3rd_1 { get; set; }
        [BookmarkMapping("ReceiptDescriptiong3rd_1")]
        public string ReceiptDescriptiong3rd_1 { get; set; }
        [BookmarkMapping("PaymentMethod3rd_2")]
        public string PaymentMethod3rd_2 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPerson3rd_1")]
        public string BuyerReceiptContactPerson3rd_1 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPersonPosition3rd_1")]
        public string BuyerReceiptContactPersonPosition3rd_1 { get; set; }
        [BookmarkMapping("BuyerReceiptAddress3rd_1")]
        public string BuyerReceiptAddress3rd_1 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPersonTel3rd_1")]
        public string BuyerReceiptContactPersonTel3rd_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFirst1_3rd_1")]
        public string ReceiptDocumentFirst1_3rd_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSecond1_3rd_1")]
        public string ReceiptDocumentSecond1_3rd_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentThird1_3rd_1")]
        public string ReceiptDocumentThird1_3rd_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFourth_3rd_1")]
        public string ReceiptDocumentFourth_3rd_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFifth_3rd_1")]
        public string ReceiptDocumentFifth_3rd_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSixth1_3rd_1")]
        public string ReceiptDocumentSixth1_3rd_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSeventh_3rd_1")]
        public string ReceiptDocumentSeventh_3rd_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentEigth1_3rd_1")]
        public string ReceiptDocumentEigth1_3rd_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentNineth1_3rd_1")]
        public string ReceiptDocumentNineth1_3rd_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentTenth_3rd_1")]
        public string ReceiptDocumentTenth_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc01_3rd_1")]
        public string ServiceFeeDesc01_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt01_3rd_1")]
        public decimal ServiceFeeAmt01_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark01_3rd_1")]
        public string ServiceFeeRemark01_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc02_3rd_1")]
        public string ServiceFeeDesc02_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt02_3rd_1")]
        public decimal ServiceFeeAmt02_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark02_3rd_1")]
        public string ServiceFeeRemark02_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc03_3rd_1")]
        public string ServiceFeeDesc03_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt03_3rd_1")]
        public decimal ServiceFeeAmt03_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark03_3rd_1")]
        public string ServiceFeeRemark03_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc04_3rd_1")]
        public string ServiceFeeDesc04_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt04_3rd_1")]
        public decimal ServiceFeeAmt04_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark04_3rd_1")]
        public string ServiceFeeRemark04_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc05_3rd_1")]
        public string ServiceFeeDesc05_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt05_3rd_1")]
        public decimal ServiceFeeAmt05_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark05_3rd_1")]
        public string ServiceFeeRemark05_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc06_3rd_1")]
        public string ServiceFeeDesc06_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt06_3rd_1")]
        public decimal ServiceFeeAmt06_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark06_3rd_1")]
        public string ServiceFeeRemark06_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc07_3rd_1")]
        public string ServiceFeeDesc07_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt07_3rd_1")]
        public decimal ServiceFeeAmt07_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark07_3rd_1")]
        public string ServiceFeeRemark07_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc08_3rd_1")]
        public string ServiceFeeDesc08_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt08_3rd_1")]
        public decimal ServiceFeeAmt08_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark08_3rd_1")]
        public string ServiceFeeRemark08_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc09_3rd_1")]
        public string ServiceFeeDesc09_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt09_3rd_1")]
        public decimal ServiceFeeAmt09_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark09_3rd_1")]
        public string ServiceFeeRemark09_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc10_3rd_1")]
        public string ServiceFeeDesc10_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt10_3rd_1")]
        public decimal ServiceFeeAmt10_3rd_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark10_3rd_1")]
        public string ServiceFeeRemark10_3rd_1 { get; set; }
        [BookmarkMapping("SumServiceFeeAmt_3rd_1")]
        public decimal SumServiceFeeAmt_3rd_1 { get; set; }
        [BookmarkMapping("MarketingComment3rd")]
        public string MarketingComment3rd { get; set; }
        [BookmarkMapping("CreditComment3rd")]
        public string CreditComment3rd { get; set; }
        [BookmarkMapping("ApproverComment3rd")]
        public string ApproverComment3rd { get; set; }
        [BookmarkMapping("BusinessSegment4th_2")]
        public string BusinessSegment4th_2 { get; set; }
        [BookmarkMapping("BuyerNameCARequestLine4th_1")]
        public string BuyerNameCARequestLine4th_1 { get; set; }
        [BookmarkMapping("BuyerTaxID4th_1")]
        public string BuyerTaxID4th_1 { get; set; }
        [BookmarkMapping("BuyerAddress4th_1")]
        public string BuyerAddress4th_1 { get; set; }
        [BookmarkMapping("BuyerCompanyRegisteredDate4th_1")]
        public DateTime? BuyerCompanyRegisteredDate4th_1 { get; set; }
        [BookmarkMapping("BuyerLineOfBusiness4th_1")]
        public string BuyerLineOfBusiness4th_1 { get; set; }
        [BookmarkMapping("BuyerBlacklistStatus4th_1")]
        public string BuyerBlacklistStatus4th_1 { get; set; }
        [BookmarkMapping("BuyerCreditScoring4th_1")]
        public string BuyerCreditScoring4th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearFirst1_4th_1")]
        public int BuyerFinYearFirst1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalFirst1_4th_1")]
        public decimal BuyerFinRegisteredCapitalFirst1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalFirst1_4th_1")]
        public decimal BuyerFinPaidCapitalFirst1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetFirst1_4th_1")]
        public decimal BuyerFinTotalAssetFirst1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilityFirst1_4th_1")]
        public decimal BuyerFinTotalLiabilityFirst1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquityFirst1_4th_1")]
        public decimal BuyerFinTotalEquityFirst1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearFirst2_4th_1")]
        public int BuyerFinYearFirst2_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueFirst1_4th_1")]
        public decimal BuyerFinTotalRevenueFirst1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSFirst1_4th_1")]
        public decimal BuyerFinTotalCOGSFirst1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitFirst1_4th_1")]
        public decimal BuyerFinTotalGrossProfitFirst1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpFirst1_4th_1")]
        public decimal BuyerFinTotalOperExpFirst1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitFirst1_4th_1")]
        public decimal BuyerFinTotalNetProfitFirst1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearSecond1_4th_1")]
        public int BuyerFinYearSecond1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalSecond1_4th_1")]
        public decimal BuyerFinRegisteredCapitalSecond1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalSecond1_4th_1")]
        public decimal BuyerFinPaidCapitalSecond1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetSecond1_4th_1")]
        public decimal BuyerFinTotalAssetSecond1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilitySecond1_4th_1")]
        public decimal BuyerFinTotalLiabilitySecond1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquitySecond1_4th_1")]
        public decimal BuyerFinTotalEquitySecond1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearSecond2_4th_1")]
        public int BuyerFinYearSecond2_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueSecond1_4th_1")]
        public decimal BuyerFinTotalRevenueSecond1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSSecond1_4th_1")]
        public decimal BuyerFinTotalCOGSSecond1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitSecond1_4th_1")]
        public decimal BuyerFinTotalGrossProfitSecond1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpSecond1_4th_1")]
        public decimal BuyerFinTotalOperExpSecond1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitSecond1_4th_1")]
        public decimal BuyerFinTotalNetProfitSecond1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearThird1_4th_1")]
        public int BuyerFinYearThird1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalThird1_4th_1")]
        public decimal BuyerFinRegisteredCapitalThird1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalThird1_4th_1")]
        public decimal BuyerFinPaidCapitalThird1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetThird1_4th_1")]
        public decimal BuyerFinTotalAssetThird1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilityThird1_4th_1")]
        public decimal BuyerFinTotalLiabilityThird1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquityThird1_4th_1")]
        public decimal BuyerFinTotalEquityThird1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearThird2_4th_1")]
        public int BuyerFinYearThird2_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueThird1_4th_1")]
        public decimal BuyerFinTotalRevenueThird1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSThird1_4th_1")]
        public decimal BuyerFinTotalCOGSThird1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitThird1_4th_1")]
        public decimal BuyerFinTotalGrossProfitThird1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpThird1_4th_1")]
        public decimal BuyerFinTotalOperExpThird1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitThird1_4th_1")]
        public decimal BuyerFinTotalNetProfitThird1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinNetProfitPercent1_4th_1")]
        public decimal BuyerFinNetProfitPercent1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinlDE1_4th_1")]
        public decimal BuyerFinlDE1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinQuickRatio1_4th_1")]
        public decimal BuyerFinQuickRatio1_4th_1 { get; set; }
        [BookmarkMapping("BuyerFinIntCoverageRatio1_4th_1")]
        public decimal BuyerFinIntCoverageRatio1_4th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest4th_2")]
        public decimal BuyerCreditLimitRequest4th_2 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer4th_2")]
        public decimal BuyerCreditLimitAllCustomer4th_2 { get; set; }
        [BookmarkMapping("PurchasePercent4th_2")]
        public decimal PurchasePercent4th_2 { get; set; }
        [BookmarkMapping("AssignmentCondition4th_2")]
        public string AssignmentCondition4th_2 { get; set; }
        [BookmarkMapping("FactoringPurchaseFee4th_1")]
        public decimal FactoringPurchaseFee4th_1 { get; set; }
        [BookmarkMapping("FactoringPurchaseFeeCalBase4th_1")]
        public string FactoringPurchaseFeeCalBase4th_1 { get; set; }
        [BookmarkMapping("BillingResponsible4th_2")]
        public string BillingResponsible4th_2 { get; set; }
        [BookmarkMapping("AcceptanceDocument4th_1")]
        public string AcceptanceDocument4th_1 { get; set; }
        [BookmarkMapping("BillingDescriptiong4th_1")]
        public string BillingDescriptiong4th_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPerson4th_1")]
        public string BuyerBillingContactPerson4th_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPersonPosition4th_1")]
        public string BuyerBillingContactPersonPosition4th_1 { get; set; }
        [BookmarkMapping("BuyerBillingAddress4th_1")]
        public string BuyerBillingAddress4th_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPersonTel4th_1")]
        public string BuyerBillingContactPersonTel4th_1 { get; set; }
        [BookmarkMapping("BillingDocumentFirst1_4th_1")]
        public string BillingDocumentFirst1_4th_1 { get; set; }
        [BookmarkMapping("BillingDocumentSecond1_4th_1")]
        public string BillingDocumentSecond1_4th_1 { get; set; }
        [BookmarkMapping("BillingDocumentThird1_4th_1")]
        public string BillingDocumentThird1_4th_1 { get; set; }
        [BookmarkMapping("BillingDocumentFourth_4th_1")]
        public string BillingDocumentFourth_4th_1 { get; set; }
        [BookmarkMapping("BillingDocumentFifth_4th_1")]
        public string BillingDocumentFifth_4th_1 { get; set; }
        [BookmarkMapping("BillingDocumentSixth1_4th_1")]
        public string BillingDocumentSixth1_4th_1 { get; set; }
        [BookmarkMapping("BillingDocumentSeventh_4th_1")]
        public string BillingDocumentSeventh_4th_1 { get; set; }
        [BookmarkMapping("BillingDocumentEigth1_4th_1")]
        public string BillingDocumentEigth1_4th_1 { get; set; }
        [BookmarkMapping("BillingDocumentNineth1_4th_1")]
        public string BillingDocumentNineth1_4th_1 { get; set; }
        [BookmarkMapping("BillingDocumentTenth_4th_1")]
        public string BillingDocumentTenth_4th_1 { get; set; }
        [BookmarkMapping("ReceiptDescriptiong4th_1")]
        public string ReceiptDescriptiong4th_1 { get; set; }
        [BookmarkMapping("PaymentMethod4th_2")]
        public string PaymentMethod4th_2 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPerson4th_1")]
        public string BuyerReceiptContactPerson4th_1 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPersonPosition4th_1")]
        public string BuyerReceiptContactPersonPosition4th_1 { get; set; }
        [BookmarkMapping("BuyerReceiptAddress4th_1")]
        public string BuyerReceiptAddress4th_1 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPersonTel4th_1")]
        public string BuyerReceiptContactPersonTel4th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFirst1_4th_1")]
        public string ReceiptDocumentFirst1_4th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSecond1_4th_1")]
        public string ReceiptDocumentSecond1_4th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentThird1_4th_1")]
        public string ReceiptDocumentThird1_4th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFourth_4th_1")]
        public string ReceiptDocumentFourth_4th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFifth_4th_1")]
        public string ReceiptDocumentFifth_4th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSixth1_4th_1")]
        public string ReceiptDocumentSixth1_4th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSeventh_4th_1")]
        public string ReceiptDocumentSeventh_4th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentEigth1_4th_1")]
        public string ReceiptDocumentEigth1_4th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentNineth1_4th_1")]
        public string ReceiptDocumentNineth1_4th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentTenth_4th_1")]
        public string ReceiptDocumentTenth_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc01_4th_1")]
        public string ServiceFeeDesc01_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt01_4th_1")]
        public decimal ServiceFeeAmt01_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark01_4th_1")]
        public string ServiceFeeRemark01_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc02_4th_1")]
        public string ServiceFeeDesc02_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt02_4th_1")]
        public decimal ServiceFeeAmt02_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark02_4th_1")]
        public string ServiceFeeRemark02_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc03_4th_1")]
        public string ServiceFeeDesc03_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt03_4th_1")]
        public decimal ServiceFeeAmt03_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark03_4th_1")]
        public string ServiceFeeRemark03_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc04_4th_1")]
        public string ServiceFeeDesc04_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt04_4th_1")]
        public decimal ServiceFeeAmt04_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark04_4th_1")]
        public string ServiceFeeRemark04_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc05_4th_1")]
        public string ServiceFeeDesc05_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt05_4th_1")]
        public decimal ServiceFeeAmt05_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark05_4th_1")]
        public string ServiceFeeRemark05_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc06_4th_1")]
        public string ServiceFeeDesc06_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt06_4th_1")]
        public decimal ServiceFeeAmt06_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark06_4th_1")]
        public string ServiceFeeRemark06_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc07_4th_1")]
        public string ServiceFeeDesc07_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt07_4th_1")]
        public decimal ServiceFeeAmt07_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark07_4th_1")]
        public string ServiceFeeRemark07_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc08_4th_1")]
        public string ServiceFeeDesc08_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt08_4th_1")]
        public decimal ServiceFeeAmt08_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark08_4th_1")]
        public string ServiceFeeRemark08_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc09_4th_1")]
        public string ServiceFeeDesc09_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt09_4th_1")]
        public decimal ServiceFeeAmt09_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark09_4th_1")]
        public string ServiceFeeRemark09_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc10_4th_1")]
        public string ServiceFeeDesc10_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt10_4th_1")]
        public decimal ServiceFeeAmt10_4th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark10_4th_1")]
        public string ServiceFeeRemark10_4th_1 { get; set; }
        [BookmarkMapping("SumServiceFeeAmt_4th_1")]
        public decimal SumServiceFeeAmt_4th_1 { get; set; }
        [BookmarkMapping("MarketingComment4th")]
        public string MarketingComment4th { get; set; }
        [BookmarkMapping("CreditComment4th")]
        public string CreditComment4th { get; set; }
        [BookmarkMapping("ApproverComment4th")]
        public string ApproverComment4th { get; set; }
        [BookmarkMapping("BusinessSegment5th_2")]
        public string BusinessSegment5th_2 { get; set; }
        [BookmarkMapping("BuyerNameCARequestLine5th_1")]
        public string BuyerNameCARequestLine5th_1 { get; set; }
        [BookmarkMapping("BuyerTaxID5th_1")]
        public string BuyerTaxID5th_1 { get; set; }
        [BookmarkMapping("BuyerAddress5th_1")]
        public string BuyerAddress5th_1 { get; set; }
        [BookmarkMapping("BuyerCompanyRegisteredDate5th_1")]
        public DateTime? BuyerCompanyRegisteredDate5th_1 { get; set; }
        [BookmarkMapping("BuyerLineOfBusiness5th_1")]
        public string BuyerLineOfBusiness5th_1 { get; set; }
        [BookmarkMapping("BuyerBlacklistStatus5th_1")]
        public string BuyerBlacklistStatus5th_1 { get; set; }
        [BookmarkMapping("BuyerCreditScoring5th_1")]
        public string BuyerCreditScoring5th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearFirst1_5th_1")]
        public int BuyerFinYearFirst1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalFirst1_5th_1")]
        public decimal BuyerFinRegisteredCapitalFirst1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalFirst1_5th_1")]
        public decimal BuyerFinPaidCapitalFirst1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetFirst1_5th_1")]
        public decimal BuyerFinTotalAssetFirst1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilityFirst1_5th_1")]
        public decimal BuyerFinTotalLiabilityFirst1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquityFirst1_5th_1")]
        public decimal BuyerFinTotalEquityFirst1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearFirst2_5th_1")]
        public int BuyerFinYearFirst2_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueFirst1_5th_1")]
        public decimal BuyerFinTotalRevenueFirst1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSFirst1_5th_1")]
        public decimal BuyerFinTotalCOGSFirst1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitFirst1_5th_1")]
        public decimal BuyerFinTotalGrossProfitFirst1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpFirst1_5th_1")]
        public decimal BuyerFinTotalOperExpFirst1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitFirst1_5th_1")]
        public decimal BuyerFinTotalNetProfitFirst1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearSecond1_5th_1")]
        public int BuyerFinYearSecond1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalSecond1_5th_1")]
        public decimal BuyerFinRegisteredCapitalSecond1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalSecond1_5th_1")]
        public decimal BuyerFinPaidCapitalSecond1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetSecond1_5th_1")]
        public decimal BuyerFinTotalAssetSecond1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilitySecond1_5th_1")]
        public decimal BuyerFinTotalLiabilitySecond1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquitySecond1_5th_1")]
        public decimal BuyerFinTotalEquitySecond1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearSecond2_5th_1")]
        public int BuyerFinYearSecond2_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueSecond1_5th_1")]
        public decimal BuyerFinTotalRevenueSecond1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSSecond1_5th_1")]
        public decimal BuyerFinTotalCOGSSecond1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitSecond1_5th_1")]
        public decimal BuyerFinTotalGrossProfitSecond1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpSecond1_5th_1")]
        public decimal BuyerFinTotalOperExpSecond1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitSecond1_5th_1")]
        public decimal BuyerFinTotalNetProfitSecond1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearThird1_5th_1")]
        public int BuyerFinYearThird1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalThird1_5th_1")]
        public decimal BuyerFinRegisteredCapitalThird1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalThird1_5th_1")]
        public decimal BuyerFinPaidCapitalThird1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetThird1_5th_1")]
        public decimal BuyerFinTotalAssetThird1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilityThird1_5th_1")]
        public decimal BuyerFinTotalLiabilityThird1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquityThird1_5th_1")]
        public decimal BuyerFinTotalEquityThird1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearThird2_5th_1")]
        public int BuyerFinYearThird2_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueThird1_5th_1")]
        public decimal BuyerFinTotalRevenueThird1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSThird1_5th_1")]
        public decimal BuyerFinTotalCOGSThird1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitThird1_5th_1")]
        public decimal BuyerFinTotalGrossProfitThird1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpThird1_5th_1")]
        public decimal BuyerFinTotalOperExpThird1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitThird1_5th_1")]
        public decimal BuyerFinTotalNetProfitThird1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinNetProfitPercent1_5th_1")]
        public decimal BuyerFinNetProfitPercent1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinlDE1_5th_1")]
        public decimal BuyerFinlDE1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinQuickRatio1_5th_1")]
        public decimal BuyerFinQuickRatio1_5th_1 { get; set; }
        [BookmarkMapping("BuyerFinIntCoverageRatio1_5th_1")]
        public decimal BuyerFinIntCoverageRatio1_5th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest5th_2")]
        public decimal BuyerCreditLimitRequest5th_2 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer5th_2")]
        public decimal BuyerCreditLimitAllCustomer5th_2 { get; set; }
        [BookmarkMapping("PurchasePercent5th_2")]
        public decimal PurchasePercent5th_2 { get; set; }
        [BookmarkMapping("AssignmentCondition5th_2")]
        public string AssignmentCondition5th_2 { get; set; }
        [BookmarkMapping("FactoringPurchaseFee5th_1")]
        public decimal FactoringPurchaseFee5th_1 { get; set; }
        [BookmarkMapping("FactoringPurchaseFeeCalBase5th_1")]
        public string FactoringPurchaseFeeCalBase5th_1 { get; set; }
        [BookmarkMapping("BillingResponsible5th_2")]
        public string BillingResponsible5th_2 { get; set; }
        [BookmarkMapping("AcceptanceDocument5th_1")]
        public string AcceptanceDocument5th_1 { get; set; }
        [BookmarkMapping("BillingDescriptiong5th_1")]
        public string BillingDescriptiong5th_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPerson5th_1")]
        public string BuyerBillingContactPerson5th_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPersonPosition5th_1")]
        public string BuyerBillingContactPersonPosition5th_1 { get; set; }
        [BookmarkMapping("BuyerBillingAddress5th_1")]
        public string BuyerBillingAddress5th_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPersonTel5th_1")]
        public string BuyerBillingContactPersonTel5th_1 { get; set; }
        [BookmarkMapping("BillingDocumentFirst1_5th_1")]
        public string BillingDocumentFirst1_5th_1 { get; set; }
        [BookmarkMapping("BillingDocumentSecond1_5th_1")]
        public string BillingDocumentSecond1_5th_1 { get; set; }
        [BookmarkMapping("BillingDocumentThird1_5th_1")]
        public string BillingDocumentThird1_5th_1 { get; set; }
        [BookmarkMapping("BillingDocumentFourth_5th_1")]
        public string BillingDocumentFourth_5th_1 { get; set; }
        [BookmarkMapping("BillingDocumentFifth_5th_1")]
        public string BillingDocumentFifth_5th_1 { get; set; }
        [BookmarkMapping("BillingDocumentSixth1_5th_1")]
        public string BillingDocumentSixth1_5th_1 { get; set; }
        [BookmarkMapping("BillingDocumentSeventh_5th_1")]
        public string BillingDocumentSeventh_5th_1 { get; set; }
        [BookmarkMapping("BillingDocumentEigth1_5th_1")]
        public string BillingDocumentEigth1_5th_1 { get; set; }
        [BookmarkMapping("BillingDocumentNineth1_5th_1")]
        public string BillingDocumentNineth1_5th_1 { get; set; }
        [BookmarkMapping("BillingDocumentTenth_5th_1")]
        public string BillingDocumentTenth_5th_1 { get; set; }
        [BookmarkMapping("ReceiptDescriptiong5th_1")]
        public string ReceiptDescriptiong5th_1 { get; set; }
        [BookmarkMapping("PaymentMethod5th_2")]
        public string PaymentMethod5th_2 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPerson5th_1")]
        public string BuyerReceiptContactPerson5th_1 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPersonPosition5th_1")]
        public string BuyerReceiptContactPersonPosition5th_1 { get; set; }
        [BookmarkMapping("BuyerReceiptAddress5th_1")]
        public string BuyerReceiptAddress5th_1 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPersonTel5th_1")]
        public string BuyerReceiptContactPersonTel5th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFirst1_5th_1")]
        public string ReceiptDocumentFirst1_5th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSecond1_5th_1")]
        public string ReceiptDocumentSecond1_5th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentThird1_5th_1")]
        public string ReceiptDocumentThird1_5th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFourth_5th_1")]
        public string ReceiptDocumentFourth_5th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFifth_5th_1")]
        public string ReceiptDocumentFifth_5th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSixth1_5th_1")]
        public string ReceiptDocumentSixth1_5th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSeventh_5th_1")]
        public string ReceiptDocumentSeventh_5th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentEigth1_5th_1")]
        public string ReceiptDocumentEigth1_5th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentNineth1_5th_1")]
        public string ReceiptDocumentNineth1_5th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentTenth_5th_1")]
        public string ReceiptDocumentTenth_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc01_5th_1")]
        public string ServiceFeeDesc01_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt01_5th_1")]
        public decimal ServiceFeeAmt01_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark01_5th_1")]
        public string ServiceFeeRemark01_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc02_5th_1")]
        public string ServiceFeeDesc02_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt02_5th_1")]
        public decimal ServiceFeeAmt02_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark02_5th_1")]
        public string ServiceFeeRemark02_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc03_5th_1")]
        public string ServiceFeeDesc03_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt03_5th_1")]
        public decimal ServiceFeeAmt03_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark03_5th_1")]
        public string ServiceFeeRemark03_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc04_5th_1")]
        public string ServiceFeeDesc04_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt04_5th_1")]
        public decimal ServiceFeeAmt04_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark04_5th_1")]
        public string ServiceFeeRemark04_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc05_5th_1")]
        public string ServiceFeeDesc05_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt05_5th_1")]
        public decimal ServiceFeeAmt05_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark05_5th_1")]
        public string ServiceFeeRemark05_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc06_5th_1")]
        public string ServiceFeeDesc06_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt06_5th_1")]
        public decimal ServiceFeeAmt06_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark06_5th_1")]
        public string ServiceFeeRemark06_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc07_5th_1")]
        public string ServiceFeeDesc07_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt07_5th_1")]
        public decimal ServiceFeeAmt07_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark07_5th_1")]
        public string ServiceFeeRemark07_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc08_5th_1")]
        public string ServiceFeeDesc08_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt08_5th_1")]
        public decimal ServiceFeeAmt08_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark08_5th_1")]
        public string ServiceFeeRemark08_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc09_5th_1")]
        public string ServiceFeeDesc09_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt09_5th_1")]
        public decimal ServiceFeeAmt09_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark09_5th_1")]
        public string ServiceFeeRemark09_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc10_5th_1")]
        public string ServiceFeeDesc10_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt10_5th_1")]
        public decimal ServiceFeeAmt10_5th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark10_5th_1")]
        public string ServiceFeeRemark10_5th_1 { get; set; }
        [BookmarkMapping("SumServiceFeeAmt_5th_1")]
        public decimal SumServiceFeeAmt_5th_1 { get; set; }
        [BookmarkMapping("MarketingComment5th")]
        public string MarketingComment5th { get; set; }
        [BookmarkMapping("CreditComment5th")]
        public string CreditComment5th { get; set; }
        [BookmarkMapping("ApproverComment5th")]
        public string ApproverComment5th { get; set; }
        [BookmarkMapping("BusinessSegment6th_2")]
        public string BusinessSegment6th_2 { get; set; }
        [BookmarkMapping("BuyerNameCARequestLine6th_1")]
        public string BuyerNameCARequestLine6th_1 { get; set; }
        [BookmarkMapping("BuyerTaxID6th_1")]
        public string BuyerTaxID6th_1 { get; set; }
        [BookmarkMapping("BuyerAddress6th_1")]
        public string BuyerAddress6th_1 { get; set; }
        [BookmarkMapping("BuyerCompanyRegisteredDate6th_1")]
        public DateTime? BuyerCompanyRegisteredDate6th_1 { get; set; }
        [BookmarkMapping("BuyerLineOfBusiness6th_1")]
        public string BuyerLineOfBusiness6th_1 { get; set; }
        [BookmarkMapping("BuyerBlacklistStatus6th_1")]
        public string BuyerBlacklistStatus6th_1 { get; set; }
        [BookmarkMapping("BuyerCreditScoring6th_1")]
        public string BuyerCreditScoring6th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearFirst1_6th_1")]
        public int BuyerFinYearFirst1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalFirst1_6th_1")]
        public decimal BuyerFinRegisteredCapitalFirst1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalFirst1_6th_1")]
        public decimal BuyerFinPaidCapitalFirst1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetFirst1_6th_1")]
        public decimal BuyerFinTotalAssetFirst1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilityFirst1_6th_1")]
        public decimal BuyerFinTotalLiabilityFirst1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquityFirst1_6th_1")]
        public decimal BuyerFinTotalEquityFirst1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearFirst2_6th_1")]
        public int BuyerFinYearFirst2_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueFirst1_6th_1")]
        public decimal BuyerFinTotalRevenueFirst1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSFirst1_6th_1")]
        public decimal BuyerFinTotalCOGSFirst1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitFirst1_6th_1")]
        public decimal BuyerFinTotalGrossProfitFirst1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpFirst1_6th_1")]
        public decimal BuyerFinTotalOperExpFirst1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitFirst1_6th_1")]
        public decimal BuyerFinTotalNetProfitFirst1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearSecond1_6th_1")]
        public int BuyerFinYearSecond1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalSecond1_6th_1")]
        public decimal BuyerFinRegisteredCapitalSecond1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalSecond1_6th_1")]
        public decimal BuyerFinPaidCapitalSecond1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetSecond1_6th_1")]
        public decimal BuyerFinTotalAssetSecond1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilitySecond1_6th_1")]
        public decimal BuyerFinTotalLiabilitySecond1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquitySecond1_6th_1")]
        public decimal BuyerFinTotalEquitySecond1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearSecond2_6th_1")]
        public int BuyerFinYearSecond2_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueSecond1_6th_1")]
        public decimal BuyerFinTotalRevenueSecond1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSSecond1_6th_1")]
        public decimal BuyerFinTotalCOGSSecond1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitSecond1_6th_1")]
        public decimal BuyerFinTotalGrossProfitSecond1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpSecond1_6th_1")]
        public decimal BuyerFinTotalOperExpSecond1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitSecond1_6th_1")]
        public decimal BuyerFinTotalNetProfitSecond1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearThird1_6th_1")]
        public int BuyerFinYearThird1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalThird1_6th_1")]
        public decimal BuyerFinRegisteredCapitalThird1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalThird1_6th_1")]
        public decimal BuyerFinPaidCapitalThird1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetThird1_6th_1")]
        public decimal BuyerFinTotalAssetThird1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilityThird1_6th_1")]
        public decimal BuyerFinTotalLiabilityThird1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquityThird1_6th_1")]
        public decimal BuyerFinTotalEquityThird1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearThird2_6th_1")]
        public int BuyerFinYearThird2_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueThird1_6th_1")]
        public decimal BuyerFinTotalRevenueThird1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSThird1_6th_1")]
        public decimal BuyerFinTotalCOGSThird1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitThird1_6th_1")]
        public decimal BuyerFinTotalGrossProfitThird1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpThird1_6th_1")]
        public decimal BuyerFinTotalOperExpThird1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitThird1_6th_1")]
        public decimal BuyerFinTotalNetProfitThird1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinNetProfitPercent1_6th_1")]
        public decimal BuyerFinNetProfitPercent1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinlDE1_6th_1")]
        public decimal BuyerFinlDE1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinQuickRatio1_6th_1")]
        public decimal BuyerFinQuickRatio1_6th_1 { get; set; }
        [BookmarkMapping("BuyerFinIntCoverageRatio1_6th_1")]
        public decimal BuyerFinIntCoverageRatio1_6th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest6th_2")]
        public decimal BuyerCreditLimitRequest6th_2 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer6th_2")]
        public decimal BuyerCreditLimitAllCustomer6th_2 { get; set; }
        [BookmarkMapping("PurchasePercent6th_2")]
        public decimal PurchasePercent6th_2 { get; set; }
        [BookmarkMapping("AssignmentCondition6th_2")]
        public string AssignmentCondition6th_2 { get; set; }
        [BookmarkMapping("FactoringPurchaseFee6th_1")]
        public decimal FactoringPurchaseFee6th_1 { get; set; }
        [BookmarkMapping("FactoringPurchaseFeeCalBase6th_1")]
        public string FactoringPurchaseFeeCalBase6th_1 { get; set; }
        [BookmarkMapping("BillingResponsible6th_2")]
        public string BillingResponsible6th_2 { get; set; }
        [BookmarkMapping("AcceptanceDocument6th_1")]
        public string AcceptanceDocument6th_1 { get; set; }
        [BookmarkMapping("BillingDescriptiong6th_1")]
        public string BillingDescriptiong6th_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPerson6th_1")]
        public string BuyerBillingContactPerson6th_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPersonPosition6th_1")]
        public string BuyerBillingContactPersonPosition6th_1 { get; set; }
        [BookmarkMapping("BuyerBillingAddress6th_1")]
        public string BuyerBillingAddress6th_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPersonTel6th_1")]
        public string BuyerBillingContactPersonTel6th_1 { get; set; }
        [BookmarkMapping("BillingDocumentFirst1_6th_1")]
        public string BillingDocumentFirst1_6th_1 { get; set; }
        [BookmarkMapping("BillingDocumentSecond1_6th_1")]
        public string BillingDocumentSecond1_6th_1 { get; set; }
        [BookmarkMapping("BillingDocumentThird1_6th_1")]
        public string BillingDocumentThird1_6th_1 { get; set; }
        [BookmarkMapping("BillingDocumentFourth_6th_1")]
        public string BillingDocumentFourth_6th_1 { get; set; }
        [BookmarkMapping("BillingDocumentFifth_6th_1")]
        public string BillingDocumentFifth_6th_1 { get; set; }
        [BookmarkMapping("BillingDocumentSixth1_6th_1")]
        public string BillingDocumentSixth1_6th_1 { get; set; }
        [BookmarkMapping("BillingDocumentSeventh_6th_1")]
        public string BillingDocumentSeventh_6th_1 { get; set; }
        [BookmarkMapping("BillingDocumentEigth1_6th_1")]
        public string BillingDocumentEigth1_6th_1 { get; set; }
        [BookmarkMapping("BillingDocumentNineth1_6th_1")]
        public string BillingDocumentNineth1_6th_1 { get; set; }
        [BookmarkMapping("BillingDocumentTenth_6th_1")]
        public string BillingDocumentTenth_6th_1 { get; set; }
        [BookmarkMapping("ReceiptDescriptiong6th_1")]
        public string ReceiptDescriptiong6th_1 { get; set; }
        [BookmarkMapping("PaymentMethod6th_2")]
        public string PaymentMethod6th_2 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPerson6th_1")]
        public string BuyerReceiptContactPerson6th_1 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPersonPosition6th_1")]
        public string BuyerReceiptContactPersonPosition6th_1 { get; set; }
        [BookmarkMapping("BuyerReceiptAddress6th_1")]
        public string BuyerReceiptAddress6th_1 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPersonTel6th_1")]
        public string BuyerReceiptContactPersonTel6th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFirst1_6th_1")]
        public string ReceiptDocumentFirst1_6th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSecond1_6th_1")]
        public string ReceiptDocumentSecond1_6th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentThird1_6th_1")]
        public string ReceiptDocumentThird1_6th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFourth_6th_1")]
        public string ReceiptDocumentFourth_6th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFifth_6th_1")]
        public string ReceiptDocumentFifth_6th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSixth1_6th_1")]
        public string ReceiptDocumentSixth1_6th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSeventh_6th_1")]
        public string ReceiptDocumentSeventh_6th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentEigth1_6th_1")]
        public string ReceiptDocumentEigth1_6th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentNineth1_6th_1")]
        public string ReceiptDocumentNineth1_6th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentTenth_6th_1")]
        public string ReceiptDocumentTenth_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc01_6th_1")]
        public string ServiceFeeDesc01_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt01_6th_1")]
        public decimal ServiceFeeAmt01_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark01_6th_1")]
        public string ServiceFeeRemark01_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc02_6th_1")]
        public string ServiceFeeDesc02_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt02_6th_1")]
        public decimal ServiceFeeAmt02_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark02_6th_1")]
        public string ServiceFeeRemark02_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc03_6th_1")]
        public string ServiceFeeDesc03_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt03_6th_1")]
        public decimal ServiceFeeAmt03_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark03_6th_1")]
        public string ServiceFeeRemark03_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc04_6th_1")]
        public string ServiceFeeDesc04_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt04_6th_1")]
        public decimal ServiceFeeAmt04_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark04_6th_1")]
        public string ServiceFeeRemark04_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc05_6th_1")]
        public string ServiceFeeDesc05_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt05_6th_1")]
        public decimal ServiceFeeAmt05_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark05_6th_1")]
        public string ServiceFeeRemark05_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc06_6th_1")]
        public string ServiceFeeDesc06_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt06_6th_1")]
        public decimal ServiceFeeAmt06_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark06_6th_1")]
        public string ServiceFeeRemark06_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc07_6th_1")]
        public string ServiceFeeDesc07_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt07_6th_1")]
        public decimal ServiceFeeAmt07_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark07_6th_1")]
        public string ServiceFeeRemark07_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc08_6th_1")]
        public string ServiceFeeDesc08_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt08_6th_1")]
        public decimal ServiceFeeAmt08_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark08_6th_1")]
        public string ServiceFeeRemark08_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc09_6th_1")]
        public string ServiceFeeDesc09_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt09_6th_1")]
        public decimal ServiceFeeAmt09_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark09_6th_1")]
        public string ServiceFeeRemark09_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc10_6th_1")]
        public string ServiceFeeDesc10_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt10_6th_1")]
        public decimal ServiceFeeAmt10_6th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark10_6th_1")]
        public string ServiceFeeRemark10_6th_1 { get; set; }
        [BookmarkMapping("SumServiceFeeAmt_6th_1")]
        public decimal SumServiceFeeAmt_6th_1 { get; set; }
        [BookmarkMapping("MarketingComment6th")]
        public string MarketingComment6th { get; set; }
        [BookmarkMapping("CreditComment6th")]
        public string CreditComment6th { get; set; }
        [BookmarkMapping("ApproverComment6th")]
        public string ApproverComment6th { get; set; }
        [BookmarkMapping("BusinessSegment7th_2")]
        public string BusinessSegment7th_2 { get; set; }
        [BookmarkMapping("BuyerNameCARequestLine7th_1")]
        public string BuyerNameCARequestLine7th_1 { get; set; }
        [BookmarkMapping("BuyerTaxID7th_1")]
        public string BuyerTaxID7th_1 { get; set; }
        [BookmarkMapping("BuyerAddress7th_1")]
        public string BuyerAddress7th_1 { get; set; }
        [BookmarkMapping("BuyerCompanyRegisteredDate7th_1")]
        public DateTime? BuyerCompanyRegisteredDate7th_1 { get; set; }
        [BookmarkMapping("BuyerLineOfBusiness7th_1")]
        public string BuyerLineOfBusiness7th_1 { get; set; }
        [BookmarkMapping("BuyerBlacklistStatus7th_1")]
        public string BuyerBlacklistStatus7th_1 { get; set; }
        [BookmarkMapping("BuyerCreditScoring7th_1")]
        public string BuyerCreditScoring7th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearFirst1_7th_1")]
        public int BuyerFinYearFirst1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalFirst1_7th_1")]
        public decimal BuyerFinRegisteredCapitalFirst1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalFirst1_7th_1")]
        public decimal BuyerFinPaidCapitalFirst1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetFirst1_7th_1")]
        public decimal BuyerFinTotalAssetFirst1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilityFirst1_7th_1")]
        public decimal BuyerFinTotalLiabilityFirst1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquityFirst1_7th_1")]
        public decimal BuyerFinTotalEquityFirst1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearFirst2_7th_1")]
        public int BuyerFinYearFirst2_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueFirst1_7th_1")]
        public decimal BuyerFinTotalRevenueFirst1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSFirst1_7th_1")]
        public decimal BuyerFinTotalCOGSFirst1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitFirst1_7th_1")]
        public decimal BuyerFinTotalGrossProfitFirst1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpFirst1_7th_1")]
        public decimal BuyerFinTotalOperExpFirst1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitFirst1_7th_1")]
        public decimal BuyerFinTotalNetProfitFirst1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearSecond1_7th_1")]
        public int BuyerFinYearSecond1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalSecond1_7th_1")]
        public decimal BuyerFinRegisteredCapitalSecond1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalSecond1_7th_1")]
        public decimal BuyerFinPaidCapitalSecond1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetSecond1_7th_1")]
        public decimal BuyerFinTotalAssetSecond1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilitySecond1_7th_1")]
        public decimal BuyerFinTotalLiabilitySecond1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquitySecond1_7th_1")]
        public decimal BuyerFinTotalEquitySecond1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearSecond2_7th_1")]
        public int BuyerFinYearSecond2_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueSecond1_7th_1")]
        public decimal BuyerFinTotalRevenueSecond1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSSecond1_7th_1")]
        public decimal BuyerFinTotalCOGSSecond1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitSecond1_7th_1")]
        public decimal BuyerFinTotalGrossProfitSecond1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpSecond1_7th_1")]
        public decimal BuyerFinTotalOperExpSecond1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitSecond1_7th_1")]
        public decimal BuyerFinTotalNetProfitSecond1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearThird1_7th_1")]
        public int BuyerFinYearThird1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalThird1_7th_1")]
        public decimal BuyerFinRegisteredCapitalThird1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalThird1_7th_1")]
        public decimal BuyerFinPaidCapitalThird1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetThird1_7th_1")]
        public decimal BuyerFinTotalAssetThird1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilityThird1_7th_1")]
        public decimal BuyerFinTotalLiabilityThird1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquityThird1_7th_1")]
        public decimal BuyerFinTotalEquityThird1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearThird2_7th_1")]
        public int BuyerFinYearThird2_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueThird1_7th_1")]
        public decimal BuyerFinTotalRevenueThird1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSThird1_7th_1")]
        public decimal BuyerFinTotalCOGSThird1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitThird1_7th_1")]
        public decimal BuyerFinTotalGrossProfitThird1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpThird1_7th_1")]
        public decimal BuyerFinTotalOperExpThird1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitThird1_7th_1")]
        public decimal BuyerFinTotalNetProfitThird1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinNetProfitPercent1_7th_1")]
        public decimal BuyerFinNetProfitPercent1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinlDE1_7th_1")]
        public decimal BuyerFinlDE1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinQuickRatio1_7th_1")]
        public decimal BuyerFinQuickRatio1_7th_1 { get; set; }
        [BookmarkMapping("BuyerFinIntCoverageRatio1_7th_1")]
        public decimal BuyerFinIntCoverageRatio1_7th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest7th_2")]
        public decimal BuyerCreditLimitRequest7th_2 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer7th_2")]
        public decimal BuyerCreditLimitAllCustomer7th_2 { get; set; }
        [BookmarkMapping("PurchasePercent7th_2")]
        public decimal PurchasePercent7th_2 { get; set; }
        [BookmarkMapping("AssignmentCondition7th_2")]
        public string AssignmentCondition7th_2 { get; set; }
        [BookmarkMapping("FactoringPurchaseFee7th_1")]
        public decimal FactoringPurchaseFee7th_1 { get; set; }
        [BookmarkMapping("FactoringPurchaseFeeCalBase7th_1")]
        public string FactoringPurchaseFeeCalBase7th_1 { get; set; }
        [BookmarkMapping("BillingResponsible7th_2")]
        public string BillingResponsible7th_2 { get; set; }
        [BookmarkMapping("AcceptanceDocument7th_1")]
        public string AcceptanceDocument7th_1 { get; set; }
        [BookmarkMapping("BillingDescriptiong7th_1")]
        public string BillingDescriptiong7th_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPerson7th_1")]
        public string BuyerBillingContactPerson7th_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPersonPosition7th_1")]
        public string BuyerBillingContactPersonPosition7th_1 { get; set; }
        [BookmarkMapping("BuyerBillingAddress7th_1")]
        public string BuyerBillingAddress7th_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPersonTel7th_1")]
        public string BuyerBillingContactPersonTel7th_1 { get; set; }
        [BookmarkMapping("BillingDocumentFirst1_7th_1")]
        public string BillingDocumentFirst1_7th_1 { get; set; }
        [BookmarkMapping("BillingDocumentSecond1_7th_1")]
        public string BillingDocumentSecond1_7th_1 { get; set; }
        [BookmarkMapping("BillingDocumentThird1_7th_1")]
        public string BillingDocumentThird1_7th_1 { get; set; }
        [BookmarkMapping("BillingDocumentFourth_7th_1")]
        public string BillingDocumentFourth_7th_1 { get; set; }
        [BookmarkMapping("BillingDocumentFifth_7th_1")]
        public string BillingDocumentFifth_7th_1 { get; set; }
        [BookmarkMapping("BillingDocumentSixth1_7th_1")]
        public string BillingDocumentSixth1_7th_1 { get; set; }
        [BookmarkMapping("BillingDocumentSeventh_7th_1")]
        public string BillingDocumentSeventh_7th_1 { get; set; }
        [BookmarkMapping("BillingDocumentEigth1_7th_1")]
        public string BillingDocumentEigth1_7th_1 { get; set; }
        [BookmarkMapping("BillingDocumentNineth1_7th_1")]
        public string BillingDocumentNineth1_7th_1 { get; set; }
        [BookmarkMapping("BillingDocumentTenth_7th_1")]
        public string BillingDocumentTenth_7th_1 { get; set; }
        [BookmarkMapping("ReceiptDescriptiong7th_1")]
        public string ReceiptDescriptiong7th_1 { get; set; }
        [BookmarkMapping("PaymentMethod7th_2")]
        public string PaymentMethod7th_2 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPerson7th_1")]
        public string BuyerReceiptContactPerson7th_1 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPersonPosition7th_1")]
        public string BuyerReceiptContactPersonPosition7th_1 { get; set; }
        [BookmarkMapping("BuyerReceiptAddress7th_1")]
        public string BuyerReceiptAddress7th_1 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPersonTel7th_1")]
        public string BuyerReceiptContactPersonTel7th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFirst1_7th_1")]
        public string ReceiptDocumentFirst1_7th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSecond1_7th_1")]
        public string ReceiptDocumentSecond1_7th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentThird1_7th_1")]
        public string ReceiptDocumentThird1_7th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFourth_7th_1")]
        public string ReceiptDocumentFourth_7th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFifth_7th_1")]
        public string ReceiptDocumentFifth_7th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSixth1_7th_1")]
        public string ReceiptDocumentSixth1_7th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSeventh_7th_1")]
        public string ReceiptDocumentSeventh_7th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentEigth1_7th_1")]
        public string ReceiptDocumentEigth1_7th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentNineth1_7th_1")]
        public string ReceiptDocumentNineth1_7th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentTenth_7th_1")]
        public string ReceiptDocumentTenth_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc01_7th_1")]
        public string ServiceFeeDesc01_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt01_7th_1")]
        public decimal ServiceFeeAmt01_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark01_7th_1")]
        public string ServiceFeeRemark01_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc02_7th_1")]
        public string ServiceFeeDesc02_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt02_7th_1")]
        public decimal ServiceFeeAmt02_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark02_7th_1")]
        public string ServiceFeeRemark02_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc03_7th_1")]
        public string ServiceFeeDesc03_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt03_7th_1")]
        public decimal ServiceFeeAmt03_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark03_7th_1")]
        public string ServiceFeeRemark03_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc04_7th_1")]
        public string ServiceFeeDesc04_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt04_7th_1")]
        public decimal ServiceFeeAmt04_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark04_7th_1")]
        public string ServiceFeeRemark04_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc05_7th_1")]
        public string ServiceFeeDesc05_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt05_7th_1")]
        public decimal ServiceFeeAmt05_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark05_7th_1")]
        public string ServiceFeeRemark05_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc06_7th_1")]
        public string ServiceFeeDesc06_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt06_7th_1")]
        public decimal ServiceFeeAmt06_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark06_7th_1")]
        public string ServiceFeeRemark06_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc07_7th_1")]
        public string ServiceFeeDesc07_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt07_7th_1")]
        public decimal ServiceFeeAmt07_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark07_7th_1")]
        public string ServiceFeeRemark07_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc08_7th_1")]
        public string ServiceFeeDesc08_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt08_7th_1")]
        public decimal ServiceFeeAmt08_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark08_7th_1")]
        public string ServiceFeeRemark08_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc09_7th_1")]
        public string ServiceFeeDesc09_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt09_7th_1")]
        public decimal ServiceFeeAmt09_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark09_7th_1")]
        public string ServiceFeeRemark09_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc10_7th_1")]
        public string ServiceFeeDesc10_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt10_7th_1")]
        public decimal ServiceFeeAmt10_7th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark10_7th_1")]
        public string ServiceFeeRemark10_7th_1 { get; set; }
        [BookmarkMapping("SumServiceFeeAmt_7th_1")]
        public decimal SumServiceFeeAmt_7th_1 { get; set; }
        [BookmarkMapping("MarketingComment7th")]
        public string MarketingComment7th { get; set; }
        [BookmarkMapping("CreditComment7th")]
        public string CreditComment7th { get; set; }
        [BookmarkMapping("ApproverComment7th")]
        public string ApproverComment7th { get; set; }
        [BookmarkMapping("BusinessSegment8th_2")]
        public string BusinessSegment8th_2 { get; set; }
        [BookmarkMapping("BuyerNameCARequestLine8th_1")]
        public string BuyerNameCARequestLine8th_1 { get; set; }
        [BookmarkMapping("BuyerTaxID8th_1")]
        public string BuyerTaxID8th_1 { get; set; }
        [BookmarkMapping("BuyerAddress8th_1")]
        public string BuyerAddress8th_1 { get; set; }
        [BookmarkMapping("BuyerCompanyRegisteredDate8th_1")]
        public DateTime? BuyerCompanyRegisteredDate8th_1 { get; set; }
        [BookmarkMapping("BuyerLineOfBusiness8th_1")]
        public string BuyerLineOfBusiness8th_1 { get; set; }
        [BookmarkMapping("BuyerBlacklistStatus8th_1")]
        public string BuyerBlacklistStatus8th_1 { get; set; }
        [BookmarkMapping("BuyerCreditScoring8th_1")]
        public string BuyerCreditScoring8th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearFirst1_8th_1")]
        public int BuyerFinYearFirst1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalFirst1_8th_1")]
        public decimal BuyerFinRegisteredCapitalFirst1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalFirst1_8th_1")]
        public decimal BuyerFinPaidCapitalFirst1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetFirst1_8th_1")]
        public decimal BuyerFinTotalAssetFirst1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilityFirst1_8th_1")]
        public decimal BuyerFinTotalLiabilityFirst1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquityFirst1_8th_1")]
        public decimal BuyerFinTotalEquityFirst1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearFirst2_8th_1")]
        public int BuyerFinYearFirst2_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueFirst1_8th_1")]
        public decimal BuyerFinTotalRevenueFirst1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSFirst1_8th_1")]
        public decimal BuyerFinTotalCOGSFirst1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitFirst1_8th_1")]
        public decimal BuyerFinTotalGrossProfitFirst1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpFirst1_8th_1")]
        public decimal BuyerFinTotalOperExpFirst1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitFirst1_8th_1")]
        public decimal BuyerFinTotalNetProfitFirst1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearSecond1_8th_1")]
        public int BuyerFinYearSecond1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalSecond1_8th_1")]
        public decimal BuyerFinRegisteredCapitalSecond1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalSecond1_8th_1")]
        public decimal BuyerFinPaidCapitalSecond1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetSecond1_8th_1")]
        public decimal BuyerFinTotalAssetSecond1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilitySecond1_8th_1")]
        public decimal BuyerFinTotalLiabilitySecond1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquitySecond1_8th_1")]
        public decimal BuyerFinTotalEquitySecond1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearSecond2_8th_1")]
        public int BuyerFinYearSecond2_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueSecond1_8th_1")]
        public decimal BuyerFinTotalRevenueSecond1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSSecond1_8th_1")]
        public decimal BuyerFinTotalCOGSSecond1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitSecond1_8th_1")]
        public decimal BuyerFinTotalGrossProfitSecond1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpSecond1_8th_1")]
        public decimal BuyerFinTotalOperExpSecond1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitSecond1_8th_1")]
        public decimal BuyerFinTotalNetProfitSecond1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearThird1_8th_1")]
        public int BuyerFinYearThird1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalThird1_8th_1")]
        public decimal BuyerFinRegisteredCapitalThird1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalThird1_8th_1")]
        public decimal BuyerFinPaidCapitalThird1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetThird1_8th_1")]
        public decimal BuyerFinTotalAssetThird1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilityThird1_8th_1")]
        public decimal BuyerFinTotalLiabilityThird1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquityThird1_8th_1")]
        public decimal BuyerFinTotalEquityThird1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearThird2_8th_1")]
        public int BuyerFinYearThird2_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueThird1_8th_1")]
        public decimal BuyerFinTotalRevenueThird1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSThird1_8th_1")]
        public decimal BuyerFinTotalCOGSThird1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitThird1_8th_1")]
        public decimal BuyerFinTotalGrossProfitThird1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpThird1_8th_1")]
        public decimal BuyerFinTotalOperExpThird1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitThird1_8th_1")]
        public decimal BuyerFinTotalNetProfitThird1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinNetProfitPercent1_8th_1")]
        public decimal BuyerFinNetProfitPercent1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinlDE1_8th_1")]
        public decimal BuyerFinlDE1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinQuickRatio1_8th_1")]
        public decimal BuyerFinQuickRatio1_8th_1 { get; set; }
        [BookmarkMapping("BuyerFinIntCoverageRatio1_8th_1")]
        public decimal BuyerFinIntCoverageRatio1_8th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest8th_2")]
        public decimal BuyerCreditLimitRequest8th_2 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer8th_2")]
        public decimal BuyerCreditLimitAllCustomer8th_2 { get; set; }
        [BookmarkMapping("PurchasePercent8th_2")]
        public decimal PurchasePercent8th_2 { get; set; }
        [BookmarkMapping("AssignmentCondition8th_2")]
        public string AssignmentCondition8th_2 { get; set; }
        [BookmarkMapping("FactoringPurchaseFee8th_1")]
        public decimal FactoringPurchaseFee8th_1 { get; set; }
        [BookmarkMapping("FactoringPurchaseFeeCalBase8th_1")]
        public string FactoringPurchaseFeeCalBase8th_1 { get; set; }
        [BookmarkMapping("BillingResponsible8th_2")]
        public string BillingResponsible8th_2 { get; set; }
        [BookmarkMapping("AcceptanceDocument8th_1")]
        public string AcceptanceDocument8th_1 { get; set; }
        [BookmarkMapping("BillingDescriptiong8th_1")]
        public string BillingDescriptiong8th_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPerson8th_1")]
        public string BuyerBillingContactPerson8th_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPersonPosition8th_1")]
        public string BuyerBillingContactPersonPosition8th_1 { get; set; }
        [BookmarkMapping("BuyerBillingAddress8th_1")]
        public string BuyerBillingAddress8th_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPersonTel8th_1")]
        public string BuyerBillingContactPersonTel8th_1 { get; set; }
        [BookmarkMapping("BillingDocumentFirst1_8th_1")]
        public string BillingDocumentFirst1_8th_1 { get; set; }
        [BookmarkMapping("BillingDocumentSecond1_8th_1")]
        public string BillingDocumentSecond1_8th_1 { get; set; }
        [BookmarkMapping("BillingDocumentThird1_8th_1")]
        public string BillingDocumentThird1_8th_1 { get; set; }
        [BookmarkMapping("BillingDocumentFourth_8th_1")]
        public string BillingDocumentFourth_8th_1 { get; set; }
        [BookmarkMapping("BillingDocumentFifth_8th_1")]
        public string BillingDocumentFifth_8th_1 { get; set; }
        [BookmarkMapping("BillingDocumentSixth1_8th_1")]
        public string BillingDocumentSixth1_8th_1 { get; set; }
        [BookmarkMapping("BillingDocumentSeventh_8th_1")]
        public string BillingDocumentSeventh_8th_1 { get; set; }
        [BookmarkMapping("BillingDocumentEigth1_8th_1")]
        public string BillingDocumentEigth1_8th_1 { get; set; }
        [BookmarkMapping("BillingDocumentNineth1_8th_1")]
        public string BillingDocumentNineth1_8th_1 { get; set; }
        [BookmarkMapping("BillingDocumentTenth_8th_1")]
        public string BillingDocumentTenth_8th_1 { get; set; }
        [BookmarkMapping("ReceiptDescriptiong8th_1")]
        public string ReceiptDescriptiong8th_1 { get; set; }
        [BookmarkMapping("PaymentMethod8th_2")]
        public string PaymentMethod8th_2 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPerson8th_1")]
        public string BuyerReceiptContactPerson8th_1 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPersonPosition8th_1")]
        public string BuyerReceiptContactPersonPosition8th_1 { get; set; }
        [BookmarkMapping("BuyerReceiptAddress8th_1")]
        public string BuyerReceiptAddress8th_1 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPersonTel8th_1")]
        public string BuyerReceiptContactPersonTel8th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFirst1_8th_1")]
        public string ReceiptDocumentFirst1_8th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSecond1_8th_1")]
        public string ReceiptDocumentSecond1_8th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentThird1_8th_1")]
        public string ReceiptDocumentThird1_8th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFourth_8th_1")]
        public string ReceiptDocumentFourth_8th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFifth_8th_1")]
        public string ReceiptDocumentFifth_8th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSixth1_8th_1")]
        public string ReceiptDocumentSixth1_8th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSeventh_8th_1")]
        public string ReceiptDocumentSeventh_8th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentEigth1_8th_1")]
        public string ReceiptDocumentEigth1_8th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentNineth1_8th_1")]
        public string ReceiptDocumentNineth1_8th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentTenth_8th_1")]
        public string ReceiptDocumentTenth_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc01_8th_1")]
        public string ServiceFeeDesc01_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt01_8th_1")]
        public decimal ServiceFeeAmt01_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark01_8th_1")]
        public string ServiceFeeRemark01_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc02_8th_1")]
        public string ServiceFeeDesc02_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt02_8th_1")]
        public decimal ServiceFeeAmt02_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark02_8th_1")]
        public string ServiceFeeRemark02_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc03_8th_1")]
        public string ServiceFeeDesc03_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt03_8th_1")]
        public decimal ServiceFeeAmt03_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark03_8th_1")]
        public string ServiceFeeRemark03_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc04_8th_1")]
        public string ServiceFeeDesc04_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt04_8th_1")]
        public decimal ServiceFeeAmt04_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark04_8th_1")]
        public string ServiceFeeRemark04_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc05_8th_1")]
        public string ServiceFeeDesc05_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt05_8th_1")]
        public decimal ServiceFeeAmt05_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark05_8th_1")]
        public string ServiceFeeRemark05_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc06_8th_1")]
        public string ServiceFeeDesc06_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt06_8th_1")]
        public decimal ServiceFeeAmt06_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark06_8th_1")]
        public string ServiceFeeRemark06_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc07_8th_1")]
        public string ServiceFeeDesc07_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt07_8th_1")]
        public decimal ServiceFeeAmt07_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark07_8th_1")]
        public string ServiceFeeRemark07_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc08_8th_1")]
        public string ServiceFeeDesc08_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt08_8th_1")]
        public decimal ServiceFeeAmt08_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark08_8th_1")]
        public string ServiceFeeRemark08_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc09_8th_1")]
        public string ServiceFeeDesc09_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt09_8th_1")]
        public decimal ServiceFeeAmt09_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark09_8th_1")]
        public string ServiceFeeRemark09_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc10_8th_1")]
        public string ServiceFeeDesc10_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt10_8th_1")]
        public decimal ServiceFeeAmt10_8th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark10_8th_1")]
        public string ServiceFeeRemark10_8th_1 { get; set; }
        [BookmarkMapping("SumServiceFeeAmt_8th_1")]
        public decimal SumServiceFeeAmt_8th_1 { get; set; }
        [BookmarkMapping("MarketingComment8th")]
        public string MarketingComment8th { get; set; }
        [BookmarkMapping("CreditComment8th")]
        public string CreditComment8th { get; set; }
        [BookmarkMapping("ApproverComment8th")]
        public string ApproverComment8th { get; set; }
        [BookmarkMapping("BusinessSegment9th_2")]
        public string BusinessSegment9th_2 { get; set; }
        [BookmarkMapping("BuyerNameCARequestLine9th_1")]
        public string BuyerNameCARequestLine9th_1 { get; set; }
        [BookmarkMapping("BuyerTaxID9th_1")]
        public string BuyerTaxID9th_1 { get; set; }
        [BookmarkMapping("BuyerAddress9th_1")]
        public string BuyerAddress9th_1 { get; set; }
        [BookmarkMapping("BuyerCompanyRegisteredDate9th_1")]
        public DateTime? BuyerCompanyRegisteredDate9th_1 { get; set; }
        [BookmarkMapping("BuyerLineOfBusiness9th_1")]
        public string BuyerLineOfBusiness9th_1 { get; set; }
        [BookmarkMapping("BuyerBlacklistStatus9th_1")]
        public string BuyerBlacklistStatus9th_1 { get; set; }
        [BookmarkMapping("BuyerCreditScoring9th_1")]
        public string BuyerCreditScoring9th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearFirst1_9th_1")]
        public int BuyerFinYearFirst1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalFirst1_9th_1")]
        public decimal BuyerFinRegisteredCapitalFirst1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalFirst1_9th_1")]
        public decimal BuyerFinPaidCapitalFirst1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetFirst1_9th_1")]
        public decimal BuyerFinTotalAssetFirst1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilityFirst1_9th_1")]
        public decimal BuyerFinTotalLiabilityFirst1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquityFirst1_9th_1")]
        public decimal BuyerFinTotalEquityFirst1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearFirst2_9th_1")]
        public int BuyerFinYearFirst2_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueFirst1_9th_1")]
        public decimal BuyerFinTotalRevenueFirst1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSFirst1_9th_1")]
        public decimal BuyerFinTotalCOGSFirst1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitFirst1_9th_1")]
        public decimal BuyerFinTotalGrossProfitFirst1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpFirst1_9th_1")]
        public decimal BuyerFinTotalOperExpFirst1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitFirst1_9th_1")]
        public decimal BuyerFinTotalNetProfitFirst1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearSecond1_9th_1")]
        public int BuyerFinYearSecond1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalSecond1_9th_1")]
        public decimal BuyerFinRegisteredCapitalSecond1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalSecond1_9th_1")]
        public decimal BuyerFinPaidCapitalSecond1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetSecond1_9th_1")]
        public decimal BuyerFinTotalAssetSecond1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilitySecond1_9th_1")]
        public decimal BuyerFinTotalLiabilitySecond1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquitySecond1_9th_1")]
        public decimal BuyerFinTotalEquitySecond1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearSecond2_9th_1")]
        public int BuyerFinYearSecond2_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueSecond1_9th_1")]
        public decimal BuyerFinTotalRevenueSecond1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSSecond1_9th_1")]
        public decimal BuyerFinTotalCOGSSecond1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitSecond1_9th_1")]
        public decimal BuyerFinTotalGrossProfitSecond1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpSecond1_9th_1")]
        public decimal BuyerFinTotalOperExpSecond1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitSecond1_9th_1")]
        public decimal BuyerFinTotalNetProfitSecond1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearThird1_9th_1")]
        public int BuyerFinYearThird1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalThird1_9th_1")]
        public decimal BuyerFinRegisteredCapitalThird1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalThird1_9th_1")]
        public decimal BuyerFinPaidCapitalThird1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetThird1_9th_1")]
        public decimal BuyerFinTotalAssetThird1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilityThird1_9th_1")]
        public decimal BuyerFinTotalLiabilityThird1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquityThird1_9th_1")]
        public decimal BuyerFinTotalEquityThird1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearThird2_9th_1")]
        public int BuyerFinYearThird2_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueThird1_9th_1")]
        public decimal BuyerFinTotalRevenueThird1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSThird1_9th_1")]
        public decimal BuyerFinTotalCOGSThird1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitThird1_9th_1")]
        public decimal BuyerFinTotalGrossProfitThird1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpThird1_9th_1")]
        public decimal BuyerFinTotalOperExpThird1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitThird1_9th_1")]
        public decimal BuyerFinTotalNetProfitThird1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinNetProfitPercent1_9th_1")]
        public decimal BuyerFinNetProfitPercent1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinlDE1_9th_1")]
        public decimal BuyerFinlDE1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinQuickRatio1_9th_1")]
        public decimal BuyerFinQuickRatio1_9th_1 { get; set; }
        [BookmarkMapping("BuyerFinIntCoverageRatio1_9th_1")]
        public decimal BuyerFinIntCoverageRatio1_9th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest9th_2")]
        public decimal BuyerCreditLimitRequest9th_2 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer9th_2")]
        public decimal BuyerCreditLimitAllCustomer9th_2 { get; set; }
        [BookmarkMapping("PurchasePercent9th_2")]
        public decimal PurchasePercent9th_2 { get; set; }
        [BookmarkMapping("AssignmentCondition9th_2")]
        public string AssignmentCondition9th_2 { get; set; }
        [BookmarkMapping("FactoringPurchaseFee9th_1")]
        public decimal FactoringPurchaseFee9th_1 { get; set; }
        [BookmarkMapping("FactoringPurchaseFeeCalBase9th_1")]
        public string FactoringPurchaseFeeCalBase9th_1 { get; set; }
        [BookmarkMapping("BillingResponsible9th_2")]
        public string BillingResponsible9th_2 { get; set; }
        [BookmarkMapping("AcceptanceDocument9th_1")]
        public string AcceptanceDocument9th_1 { get; set; }
        [BookmarkMapping("BillingDescriptiong9th_1")]
        public string BillingDescriptiong9th_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPerson9th_1")]
        public string BuyerBillingContactPerson9th_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPersonPosition9th_1")]
        public string BuyerBillingContactPersonPosition9th_1 { get; set; }
        [BookmarkMapping("BuyerBillingAddress9th_1")]
        public string BuyerBillingAddress9th_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPersonTel9th_1")]
        public string BuyerBillingContactPersonTel9th_1 { get; set; }
        [BookmarkMapping("BillingDocumentFirst1_9th_1")]
        public string BillingDocumentFirst1_9th_1 { get; set; }
        [BookmarkMapping("BillingDocumentSecond1_9th_1")]
        public string BillingDocumentSecond1_9th_1 { get; set; }
        [BookmarkMapping("BillingDocumentThird1_9th_1")]
        public string BillingDocumentThird1_9th_1 { get; set; }
        [BookmarkMapping("BillingDocumentFourth_9th_1")]
        public string BillingDocumentFourth_9th_1 { get; set; }
        [BookmarkMapping("BillingDocumentFifth_9th_1")]
        public string BillingDocumentFifth_9th_1 { get; set; }
        [BookmarkMapping("BillingDocumentSixth1_9th_1")]
        public string BillingDocumentSixth1_9th_1 { get; set; }
        [BookmarkMapping("BillingDocumentSeventh_9th_1")]
        public string BillingDocumentSeventh_9th_1 { get; set; }
        [BookmarkMapping("BillingDocumentEigth1_9th_1")]
        public string BillingDocumentEigth1_9th_1 { get; set; }
        [BookmarkMapping("BillingDocumentNineth1_9th_1")]
        public string BillingDocumentNineth1_9th_1 { get; set; }
        [BookmarkMapping("BillingDocumentTenth_9th_1")]
        public string BillingDocumentTenth_9th_1 { get; set; }
        [BookmarkMapping("ReceiptDescriptiong9th_1")]
        public string ReceiptDescriptiong9th_1 { get; set; }
        [BookmarkMapping("PaymentMethod9th_2")]
        public string PaymentMethod9th_2 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPerson9th_1")]
        public string BuyerReceiptContactPerson9th_1 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPersonPosition9th_1")]
        public string BuyerReceiptContactPersonPosition9th_1 { get; set; }
        [BookmarkMapping("BuyerReceiptAddress9th_1")]
        public string BuyerReceiptAddress9th_1 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPersonTel9th_1")]
        public string BuyerReceiptContactPersonTel9th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFirst1_9th_1")]
        public string ReceiptDocumentFirst1_9th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSecond1_9th_1")]
        public string ReceiptDocumentSecond1_9th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentThird1_9th_1")]
        public string ReceiptDocumentThird1_9th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFourth_9th_1")]
        public string ReceiptDocumentFourth_9th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFifth_9th_1")]
        public string ReceiptDocumentFifth_9th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSixth1_9th_1")]
        public string ReceiptDocumentSixth1_9th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSeventh_9th_1")]
        public string ReceiptDocumentSeventh_9th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentEigth1_9th_1")]
        public string ReceiptDocumentEigth1_9th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentNineth1_9th_1")]
        public string ReceiptDocumentNineth1_9th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentTenth_9th_1")]
        public string ReceiptDocumentTenth_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc01_9th_1")]
        public string ServiceFeeDesc01_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt01_9th_1")]
        public decimal ServiceFeeAmt01_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark01_9th_1")]
        public string ServiceFeeRemark01_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc02_9th_1")]
        public string ServiceFeeDesc02_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt02_9th_1")]
        public decimal ServiceFeeAmt02_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark02_9th_1")]
        public string ServiceFeeRemark02_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc03_9th_1")]
        public string ServiceFeeDesc03_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt03_9th_1")]
        public decimal ServiceFeeAmt03_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark03_9th_1")]
        public string ServiceFeeRemark03_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc04_9th_1")]
        public string ServiceFeeDesc04_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt04_9th_1")]
        public decimal ServiceFeeAmt04_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark04_9th_1")]
        public string ServiceFeeRemark04_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc05_9th_1")]
        public string ServiceFeeDesc05_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt05_9th_1")]
        public decimal ServiceFeeAmt05_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark05_9th_1")]
        public string ServiceFeeRemark05_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc06_9th_1")]
        public string ServiceFeeDesc06_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt06_9th_1")]
        public decimal ServiceFeeAmt06_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark06_9th_1")]
        public string ServiceFeeRemark06_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc07_9th_1")]
        public string ServiceFeeDesc07_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt07_9th_1")]
        public decimal ServiceFeeAmt07_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark07_9th_1")]
        public string ServiceFeeRemark07_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc08_9th_1")]
        public string ServiceFeeDesc08_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt08_9th_1")]
        public decimal ServiceFeeAmt08_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark08_9th_1")]
        public string ServiceFeeRemark08_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc09_9th_1")]
        public string ServiceFeeDesc09_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt09_9th_1")]
        public decimal ServiceFeeAmt09_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark09_9th_1")]
        public string ServiceFeeRemark09_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc10_9th_1")]
        public string ServiceFeeDesc10_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt10_9th_1")]
        public decimal ServiceFeeAmt10_9th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark10_9th_1")]
        public string ServiceFeeRemark10_9th_1 { get; set; }
        [BookmarkMapping("MarketingComment9th")]
        public string MarketingComment9th { get; set; }
        [BookmarkMapping("CreditComment9th")]
        public string CreditComment9th { get; set; }
        [BookmarkMapping("ApproverComment9th")]
        public string ApproverComment9th { get; set; }
        [BookmarkMapping("SumServiceFeeAmt_9th_1")]
        public decimal SumServiceFeeAmt_9th_1 { get; set; }
        [BookmarkMapping("BusinessSegment10th_2")]
        public string BusinessSegment10th_2 { get; set; }
        [BookmarkMapping("BuyerNameCARequestLine10th_1")]
        public string BuyerNameCARequestLine10th_1 { get; set; }
        [BookmarkMapping("BuyerTaxID10th_1")]
        public string BuyerTaxID10th_1 { get; set; }
        [BookmarkMapping("BuyerAddress10th_1")]
        public string BuyerAddress10th_1 { get; set; }
        [BookmarkMapping("BuyerCompanyRegisteredDate10th_1")]
        public DateTime? BuyerCompanyRegisteredDate10th_1 { get; set; }
        [BookmarkMapping("BuyerLineOfBusiness10th_1")]
        public string BuyerLineOfBusiness10th_1 { get; set; }
        [BookmarkMapping("BuyerBlacklistStatus10th_1")]
        public string BuyerBlacklistStatus10th_1 { get; set; }
        [BookmarkMapping("BuyerCreditScoring10th_1")]
        public string BuyerCreditScoring10th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearFirst1_10th_1")]
        public int BuyerFinYearFirst1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalFirst1_10th_1")]
        public decimal BuyerFinRegisteredCapitalFirst1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalFirst1_10th_1")]
        public decimal BuyerFinPaidCapitalFirst1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetFirst1_10th_1")]
        public decimal BuyerFinTotalAssetFirst1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilityFirst1_10th_1")]
        public decimal BuyerFinTotalLiabilityFirst1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquityFirst1_10th_1")]
        public decimal BuyerFinTotalEquityFirst1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearFirst2_10th_1")]
        public int BuyerFinYearFirst2_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueFirst1_10th_1")]
        public decimal BuyerFinTotalRevenueFirst1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSFirst1_10th_1")]
        public decimal BuyerFinTotalCOGSFirst1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitFirst1_10th_1")]
        public decimal BuyerFinTotalGrossProfitFirst1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpFirst1_10th_1")]
        public decimal BuyerFinTotalOperExpFirst1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitFirst1_10th_1")]
        public decimal BuyerFinTotalNetProfitFirst1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearSecond1_10th_1")]
        public int BuyerFinYearSecond1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalSecond1_10th_1")]
        public decimal BuyerFinRegisteredCapitalSecond1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalSecond1_10th_1")]
        public decimal BuyerFinPaidCapitalSecond1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetSecond1_10th_1")]
        public decimal BuyerFinTotalAssetSecond1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilitySecond1_10th_1")]
        public decimal BuyerFinTotalLiabilitySecond1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquitySecond1_10th_1")]
        public decimal BuyerFinTotalEquitySecond1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearSecond2_10th_1")]
        public int BuyerFinYearSecond2_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueSecond1_10th_1")]
        public decimal BuyerFinTotalRevenueSecond1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSSecond1_10th_1")]
        public decimal BuyerFinTotalCOGSSecond1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitSecond1_10th_1")]
        public decimal BuyerFinTotalGrossProfitSecond1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpSecond1_10th_1")]
        public decimal BuyerFinTotalOperExpSecond1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitSecond1_10th_1")]
        public decimal BuyerFinTotalNetProfitSecond1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearThird1_10th_1")]
        public int BuyerFinYearThird1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinRegisteredCapitalThird1_10th_1")]
        public decimal BuyerFinRegisteredCapitalThird1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinPaidCapitalThird1_10th_1")]
        public decimal BuyerFinPaidCapitalThird1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalAssetThird1_10th_1")]
        public decimal BuyerFinTotalAssetThird1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalLiabilityThird1_10th_1")]
        public decimal BuyerFinTotalLiabilityThird1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalEquityThird1_10th_1")]
        public decimal BuyerFinTotalEquityThird1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinYearThird2_10th_1")]
        public int BuyerFinYearThird2_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalRevenueThird1_10th_1")]
        public decimal BuyerFinTotalRevenueThird1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalCOGSThird1_10th_1")]
        public decimal BuyerFinTotalCOGSThird1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalGrossProfitThird1_10th_1")]
        public decimal BuyerFinTotalGrossProfitThird1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalOperExpThird1_10th_1")]
        public decimal BuyerFinTotalOperExpThird1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinTotalNetProfitThird1_10th_1")]
        public decimal BuyerFinTotalNetProfitThird1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinNetProfitPercent1_10th_1")]
        public decimal BuyerFinNetProfitPercent1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinlDE1_10th_1")]
        public decimal BuyerFinlDE1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinQuickRatio1_10th_1")]
        public decimal BuyerFinQuickRatio1_10th_1 { get; set; }
        [BookmarkMapping("BuyerFinIntCoverageRatio1_10th_1")]
        public decimal BuyerFinIntCoverageRatio1_10th_1 { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest10th_2")]
        public decimal BuyerCreditLimitRequest10th_2 { get; set; }
        [BookmarkMapping("BuyerCreditLimitAllCustomer10th_2")]
        public decimal BuyerCreditLimitAllCustomer10th_2 { get; set; }
        [BookmarkMapping("PurchasePercent10th_2")]
        public decimal PurchasePercent10th_2 { get; set; }
        [BookmarkMapping("AssignmentCondition10th_2")]
        public string AssignmentCondition10th_2 { get; set; }
        [BookmarkMapping("FactoringPurchaseFee10th_1")]
        public decimal FactoringPurchaseFee10th_1 { get; set; }
        [BookmarkMapping("FactoringPurchaseFeeCalBase10th_1")]
        public string FactoringPurchaseFeeCalBase10th_1 { get; set; }
        [BookmarkMapping("BillingResponsible10th_2")]
        public string BillingResponsible10th_2 { get; set; }
        [BookmarkMapping("AcceptanceDocument10th_1")]
        public string AcceptanceDocument10th_1 { get; set; }
        [BookmarkMapping("BillingDescriptiong10th_1")]
        public string BillingDescriptiong10th_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPerson10th_1")]
        public string BuyerBillingContactPerson10th_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPersonPosition10th_1")]
        public string BuyerBillingContactPersonPosition10th_1 { get; set; }
        [BookmarkMapping("BuyerBillingAddress10th_1")]
        public string BuyerBillingAddress10th_1 { get; set; }
        [BookmarkMapping("BuyerBillingContactPersonTel10th_1")]
        public string BuyerBillingContactPersonTel10th_1 { get; set; }
        [BookmarkMapping("BillingDocumentFirst1_10th_1")]
        public string BillingDocumentFirst1_10th_1 { get; set; }
        [BookmarkMapping("BillingDocumentSecond1_10th_1")]
        public string BillingDocumentSecond1_10th_1 { get; set; }
        [BookmarkMapping("BillingDocumentThird1_10th_1")]
        public string BillingDocumentThird1_10th_1 { get; set; }
        [BookmarkMapping("BillingDocumentFourth_10th_1")]
        public string BillingDocumentFourth_10th_1 { get; set; }
        [BookmarkMapping("BillingDocumentFifth_10th_1")]
        public string BillingDocumentFifth_10th_1 { get; set; }
        [BookmarkMapping("BillingDocumentSixth1_10th_1")]
        public string BillingDocumentSixth1_10th_1 { get; set; }
        [BookmarkMapping("BillingDocumentSeventh_10th_1")]
        public string BillingDocumentSeventh_10th_1 { get; set; }
        [BookmarkMapping("BillingDocumentEigth1_10th_1")]
        public string BillingDocumentEigth1_10th_1 { get; set; }
        [BookmarkMapping("BillingDocumentNineth1_10th_1")]
        public string BillingDocumentNineth1_10th_1 { get; set; }
        [BookmarkMapping("BillingDocumentTenth_10th_1")]
        public string BillingDocumentTenth_10th_1 { get; set; }
        [BookmarkMapping("ReceiptDescriptiong10th_1")]
        public string ReceiptDescriptiong10th_1 { get; set; }
        [BookmarkMapping("PaymentMethod10th_2")]
        public string PaymentMethod10th_2 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPerson10th_1")]
        public string BuyerReceiptContactPerson10th_1 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPersonPosition10th_1")]
        public string BuyerReceiptContactPersonPosition10th_1 { get; set; }
        [BookmarkMapping("BuyerReceiptAddress10th_1")]
        public string BuyerReceiptAddress10th_1 { get; set; }
        [BookmarkMapping("BuyerReceiptContactPersonTel10th_1")]
        public string BuyerReceiptContactPersonTel10th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFirst1_10th_1")]
        public string ReceiptDocumentFirst1_10th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSecond1_10th_1")]
        public string ReceiptDocumentSecond1_10th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentThird1_10th_1")]
        public string ReceiptDocumentThird1_10th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFourth_10th_1")]
        public string ReceiptDocumentFourth_10th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentFifth_10th_1")]
        public string ReceiptDocumentFifth_10th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSixth1_10th_1")]
        public string ReceiptDocumentSixth1_10th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentSeventh_10th_1")]
        public string ReceiptDocumentSeventh_10th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentEigth1_10th_1")]
        public string ReceiptDocumentEigth1_10th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentNineth1_10th_1")]
        public string ReceiptDocumentNineth1_10th_1 { get; set; }
        [BookmarkMapping("ReceiptDocumentTenth_10th_1")]
        public string ReceiptDocumentTenth_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc01_10th_1")]
        public string ServiceFeeDesc01_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt01_10th_1")]
        public decimal ServiceFeeAmt01_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark01_10th_1")]
        public string ServiceFeeRemark01_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc02_10th_1")]
        public string ServiceFeeDesc02_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt02_10th_1")]
        public decimal ServiceFeeAmt02_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark02_10th_1")]
        public string ServiceFeeRemark02_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc03_10th_1")]
        public string ServiceFeeDesc03_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt03_10th_1")]
        public decimal ServiceFeeAmt03_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark03_10th_1")]
        public string ServiceFeeRemark03_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc04_10th_1")]
        public string ServiceFeeDesc04_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt04_10th_1")]
        public decimal ServiceFeeAmt04_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark04_10th_1")]
        public string ServiceFeeRemark04_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc05_10th_1")]
        public string ServiceFeeDesc05_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt05_10th_1")]
        public decimal ServiceFeeAmt05_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark05_10th_1")]
        public string ServiceFeeRemark05_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc06_10th_1")]
        public string ServiceFeeDesc06_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt06_10th_1")]
        public decimal ServiceFeeAmt06_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark06_10th_1")]
        public string ServiceFeeRemark06_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc07_10th_1")]
        public string ServiceFeeDesc07_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt07_10th_1")]
        public decimal ServiceFeeAmt07_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark07_10th_1")]
        public string ServiceFeeRemark07_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc08_10th_1")]
        public string ServiceFeeDesc08_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt08_10th_1")]
        public decimal ServiceFeeAmt08_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark08_10th_1")]
        public string ServiceFeeRemark08_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc09_10th_1")]
        public string ServiceFeeDesc09_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt09_10th_1")]
        public decimal ServiceFeeAmt09_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark09_10th_1")]
        public string ServiceFeeRemark09_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeDesc10_10th_1")]
        public string ServiceFeeDesc10_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeAmt10_10th_1")]
        public decimal ServiceFeeAmt10_10th_1 { get; set; }
        [BookmarkMapping("ServiceFeeRemark10_10th_1")]
        public string ServiceFeeRemark10_10th_1 { get; set; }
        [BookmarkMapping("SumServiceFeeAmt_10th_1")]
        public decimal SumServiceFeeAmt_10th_1 { get; set; }
        [BookmarkMapping("MarketingComment10th")]
        public string MarketingComment10th { get; set; }
        [BookmarkMapping("CreditComment10th")]
        public string CreditComment10th { get; set; }
        [BookmarkMapping("ApproverComment10th")]
        public string ApproverComment10th { get; set; }
        [BookmarkMapping("AssignmentNewFirst1")]
        public string AssignmentNewFirst1 { get; set; }
        [BookmarkMapping("AssignmentBuyerFirst1")]
        public string AssignmentBuyerFirst1 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdFirst1")]
        public string AssignmentRefAgreementIdFirst1 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountFirst1")]
        public decimal AssignmentBuyerAgreementAmountFirst1 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountFirst1")]
        public decimal AssignmentAgreementAmountFirst1 { get; set; }
        [BookmarkMapping("AssignmentAgreementDateFirst1")]
        public string AssignmentAgreementDateFirst1 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountFirst1")]
        public decimal AssignmentRemainingAmountFirst1 { get; set; }
        [BookmarkMapping("AssignmentNewSecond1")]
        public string AssignmentNewSecond1 { get; set; }
        [BookmarkMapping("AssignmentBuyerSecond1")]
        public string AssignmentBuyerSecond1 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdSecond1")]
        public string AssignmentRefAgreementIdSecond1 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountSecond1")]
        public decimal AssignmentBuyerAgreementAmountSecond1 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountSecond1")]
        public decimal AssignmentAgreementAmountSecond1 { get; set; }
        [BookmarkMapping("AssignmentAgreementDateSecond1")]
        public string AssignmentAgreementDateSecond1 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountSecond1")]
        public decimal AssignmentRemainingAmountSecond1 { get; set; }
        [BookmarkMapping("AssignmentNewThird1")]
        public string AssignmentNewThird1 { get; set; }
        [BookmarkMapping("AssignmentBuyerThird1")]
        public string AssignmentBuyerThird1 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdThird1")]
        public string AssignmentRefAgreementIdThird1 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountThird1")]
        public decimal AssignmentBuyerAgreementAmountThird1 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountThird1")]
        public decimal AssignmentAgreementAmountThird1 { get; set; }
        [BookmarkMapping("AssignmentAgreementDateThird1")]
        public string AssignmentAgreementDateThird1 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountThird1")]
        public decimal AssignmentRemainingAmountThird1 { get; set; }
        [BookmarkMapping("AssignmentNewFourth1")]
        public string AssignmentNewFourth1 { get; set; }
        [BookmarkMapping("AssignmentBuyerFourth1")]
        public string AssignmentBuyerFourth1 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdFourth1")]
        public string AssignmentRefAgreementIdFourth1 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountFourth1")]
        public decimal AssignmentBuyerAgreementAmountFourth1 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountFourth1")]
        public decimal AssignmentAgreementAmountFourth1 { get; set; }
        [BookmarkMapping("AssignmentAgreementDateFourth1")]
        public string AssignmentAgreementDateFourth1 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountFourth1")]
        public decimal AssignmentRemainingAmountFourth1 { get; set; }
        [BookmarkMapping("AssignmentNewFifth1")]
        public string AssignmentNewFifth1 { get; set; }
        [BookmarkMapping("AssignmentBuyerFifth1")]
        public string AssignmentBuyerFifth1 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdFifth1")]
        public string AssignmentRefAgreementIdFifth1 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountFifth1")]
        public decimal AssignmentBuyerAgreementAmountFifth1 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountFifth1")]
        public decimal AssignmentAgreementAmountFifth1 { get; set; }
        [BookmarkMapping("AssignmentAgreementDateFifth1")]
        public string AssignmentAgreementDateFifth1 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountFifth1")]
        public decimal AssignmentRemainingAmountFifth1 { get; set; }
        [BookmarkMapping("AssignmentNewSixth1")]
        public string AssignmentNewSixth1 { get; set; }
        [BookmarkMapping("AssignmentBuyerSixth1")]
        public string AssignmentBuyerSixth1 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdSixth1")]
        public string AssignmentRefAgreementIdSixth1 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountSixth1")]
        public decimal AssignmentBuyerAgreementAmountSixth1 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountSixth1")]
        public decimal AssignmentAgreementAmountSixth1 { get; set; }
        [BookmarkMapping("AssignmentAgreementDateSixth1")]
        public string AssignmentAgreementDateSixth1 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountSixth1")]
        public decimal AssignmentRemainingAmountSixth1 { get; set; }
        [BookmarkMapping("AssignmentNewSeventh1")]
        public string AssignmentNewSeventh1 { get; set; }
        [BookmarkMapping("AssignmentBuyerSeventh1")]
        public string AssignmentBuyerSeventh1 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdSeventh1")]
        public string AssignmentRefAgreementIdSeventh1 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountSeventh1")]
        public decimal AssignmentBuyerAgreementAmountSeventh1 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountSeventh1")]
        public decimal AssignmentAgreementAmountSeventh1 { get; set; }
        [BookmarkMapping("AssignmentAgreementDateSeventh1")]
        public string AssignmentAgreementDateSeventh1 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountSeventh1")]
        public decimal AssignmentRemainingAmountSeventh1 { get; set; }
        [BookmarkMapping("AssignmentNewEighth1")]
        public string AssignmentNewEighth1 { get; set; }
        [BookmarkMapping("AssignmentBuyerEighth1")]
        public string AssignmentBuyerEighth1 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdEighth1")]
        public string AssignmentRefAgreementIdEighth1 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountEighth1")]
        public decimal AssignmentBuyerAgreementAmountEighth1 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountEighth1")]
        public decimal AssignmentAgreementAmountEighth1 { get; set; }
        [BookmarkMapping("AssignmentAgreementDateEighth1")]
        public string AssignmentAgreementDateEighth1 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountEighth1")]
        public decimal AssignmentRemainingAmountEighth1 { get; set; }
        [BookmarkMapping("AssignmentNewNineth1")]
        public string AssignmentNewNineth1 { get; set; }
        [BookmarkMapping("AssignmentBuyerNineth1")]
        public string AssignmentBuyerNineth1 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdNineth1")]
        public string AssignmentRefAgreementIdNineth1 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountNineth1")]
        public decimal AssignmentBuyerAgreementAmountNineth1 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountNineth1")]
        public decimal AssignmentAgreementAmountNineth1 { get; set; }
        [BookmarkMapping("AssignmentAgreementDateNineth1")]
        public string AssignmentAgreementDateNineth1 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountNineth1")]
        public decimal AssignmentRemainingAmountNineth1 { get; set; }
        [BookmarkMapping("AssignmentNewTenth1")]
        public string AssignmentNewTenth1 { get; set; }
        [BookmarkMapping("AssignmentBuyerTenth1")]
        public string AssignmentBuyerTenth1 { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdTenth1")]
        public string AssignmentRefAgreementIdTenth1 { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountTenth1")]
        public decimal AssignmentBuyerAgreementAmountTenth1 { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountTenth1")]
        public decimal AssignmentAgreementAmountTenth1 { get; set; }
        [BookmarkMapping("AssignmentAgreementDateTenth1")]
        public string AssignmentAgreementDateTenth1 { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountTenth1")]
        public decimal AssignmentRemainingAmountTenth1 { get; set; }
        [BookmarkMapping("SumAssignmentBuyerAgreementAmount")]
        public decimal SumAssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("SumAssignmentAgreementAmount")]
        public decimal SumAssignmentAgreementAmount { get; set; }
        [BookmarkMapping("SumAssignmentRemainingAmount")]
        public decimal SumAssignmentRemainingAmount { get; set; }
        [BookmarkMapping("BusinessCollateralNewFirst1")]
        public string BusinessCollateralNewFirst1 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeFirst1")]
        public string BusinessCollateralTypeFirst1 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeFirst1")]
        public string BusinessCollateralSubTypeFirst1 { get; set; }
        [BookmarkMapping("BusinessCollateralDescFirst1")]
        public string BusinessCollateralDescFirst1 { get; set; }
        [BookmarkMapping("BusinessCollateralValueFirst1")]
        public decimal BusinessCollateralValueFirst1 { get; set; }
        [BookmarkMapping("BusinessCollateralNewSecond1")]
        public string BusinessCollateralNewSecond1 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeSecond1")]
        public string BusinessCollateralTypeSecond1 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeSecond1")]
        public string BusinessCollateralSubTypeSecond1 { get; set; }
        [BookmarkMapping("BusinessCollateralDescSecond1")]
        public string BusinessCollateralDescSecond1 { get; set; }
        [BookmarkMapping("BusinessCollateralValueSecond1")]
        public decimal BusinessCollateralValueSecond1 { get; set; }
        [BookmarkMapping("BusinessCollateralNewThird1")]
        public string BusinessCollateralNewThird1 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeThird1")]
        public string BusinessCollateralTypeThird1 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeThird1")]
        public string BusinessCollateralSubTypeThird1 { get; set; }
        [BookmarkMapping("BusinessCollateralDescThird1")]
        public string BusinessCollateralDescThird1 { get; set; }
        [BookmarkMapping("BusinessCollateralValueThird1")]
        public decimal BusinessCollateralValueThird1 { get; set; }
        [BookmarkMapping("BusinessCollateralNewFourth1")]
        public string BusinessCollateralNewFourth1 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeFourth1")]
        public string BusinessCollateralTypeFourth1 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeFourth1")]
        public string BusinessCollateralSubTypeFourth1 { get; set; }
        [BookmarkMapping("BusinessCollateralDescFourth1")]
        public string BusinessCollateralDescFourth1 { get; set; }
        [BookmarkMapping("BusinessCollateralValueFourth1")]
        public decimal BusinessCollateralValueFourth1 { get; set; }
        [BookmarkMapping("BusinessCollateralNewFifth1")]
        public string BusinessCollateralNewFifth1 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeFifth1")]
        public string BusinessCollateralTypeFifth1 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeFifth1")]
        public string BusinessCollateralSubTypeFifth1 { get; set; }
        [BookmarkMapping("BusinessCollateralDescFifth1")]
        public string BusinessCollateralDescFifth1 { get; set; }
        [BookmarkMapping("BusinessCollateralValueFifth1")]
        public decimal BusinessCollateralValueFifth1 { get; set; }
        [BookmarkMapping("BusinessCollateralNewSixth1")]
        public string BusinessCollateralNewSixth1 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeSixth1")]
        public string BusinessCollateralTypeSixth1 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeSixth1")]
        public string BusinessCollateralSubTypeSixth1 { get; set; }
        [BookmarkMapping("BusinessCollateralDescSixth1")]
        public string BusinessCollateralDescSixth1 { get; set; }
        [BookmarkMapping("BusinessCollateralValueSixth1")]
        public decimal BusinessCollateralValueSixth1 { get; set; }
        [BookmarkMapping("BusinessCollateralNewSeventh1")]
        public string BusinessCollateralNewSeventh1 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeSeventh1")]
        public string BusinessCollateralTypeSeventh1 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeSeventh1")]
        public string BusinessCollateralSubTypeSeventh1 { get; set; }
        [BookmarkMapping("BusinessCollateralDescSeventh1")]
        public string BusinessCollateralDescSeventh1 { get; set; }
        [BookmarkMapping("BusinessCollateralValueSeventh1")]
        public decimal BusinessCollateralValueSeventh1 { get; set; }
        [BookmarkMapping("BusinessCollateralNewEighth1")]
        public string BusinessCollateralNewEighth1 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeEighth1")]
        public string BusinessCollateralTypeEighth1 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeEighth1")]
        public string BusinessCollateralSubTypeEighth1 { get; set; }
        [BookmarkMapping("BusinessCollateralDescEighth1")]
        public string BusinessCollateralDescEighth1 { get; set; }
        [BookmarkMapping("BusinessCollateralValueEighth1")]
        public decimal BusinessCollateralValueEighth1 { get; set; }
        [BookmarkMapping("BusinessCollateralNewNineth1")]
        public string BusinessCollateralNewNineth1 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeNineth1")]
        public string BusinessCollateralTypeNineth1 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeNineth1")]
        public string BusinessCollateralSubTypeNineth1 { get; set; }
        [BookmarkMapping("BusinessCollateralDescNineth1")]
        public string BusinessCollateralDescNineth1 { get; set; }
        [BookmarkMapping("BusinessCollateralValueNineth1")]
        public decimal BusinessCollateralValueNineth1 { get; set; }
        [BookmarkMapping("BusinessCollateralNewTenth1")]
        public string BusinessCollateralNewTenth1 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeTenth1")]
        public string BusinessCollateralTypeTenth1 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeTenth1")]
        public string BusinessCollateralSubTypeTenth1 { get; set; }
        [BookmarkMapping("BusinessCollateralDescTenth1")]
        public string BusinessCollateralDescTenth1 { get; set; }
        [BookmarkMapping("BusinessCollateralValueTenth1")]
        public decimal BusinessCollateralValueTenth1 { get; set; }
        [BookmarkMapping("BusinessCollateralNewEleventh1")]
        public string BusinessCollateralNewEleventh1 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeEleventh1")]
        public string BusinessCollateralTypeEleventh1 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeEleventh1")]
        public string BusinessCollateralSubTypeEleventh1 { get; set; }
        [BookmarkMapping("BusinessCollateralDescEleventh1")]
        public string BusinessCollateralDescEleventh1 { get; set; }
        [BookmarkMapping("BusinessCollateralValueEleventh1")]
        public decimal BusinessCollateralValueEleventh1 { get; set; }
        [BookmarkMapping("BusinessCollateralNewTwelfth1")]
        public string BusinessCollateralNewTwelfth1 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeTwelfth1")]
        public string BusinessCollateralTypeTwelfth1 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeTwelfth1")]
        public string BusinessCollateralSubTypeTwelfth1 { get; set; }
        [BookmarkMapping("BusinessCollateralDescTwelfth1")]
        public string BusinessCollateralDescTwelfth1 { get; set; }
        [BookmarkMapping("BusinessCollateralValueTwelfth1")]
        public decimal BusinessCollateralValueTwelfth1 { get; set; }
        [BookmarkMapping("BusinessCollateralNewThirteenth1")]
        public string BusinessCollateralNewThirteenth1 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeThirteenth1")]
        public string BusinessCollateralTypeThirteenth1 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeThirteenth1")]
        public string BusinessCollateralSubTypeThirteenth1 { get; set; }
        [BookmarkMapping("BusinessCollateralDescThirteenth1")]
        public string BusinessCollateralDescThirteenth1 { get; set; }
        [BookmarkMapping("BusinessCollateralValueThirteenth1")]
        public decimal BusinessCollateralValueThirteenth1 { get; set; }
        [BookmarkMapping("BusinessCollateralNewFourteenth1")]
        public string BusinessCollateralNewFourteenth1 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeFourteenth1")]
        public string BusinessCollateralTypeFourteenth1 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeFourteenth1")]
        public string BusinessCollateralSubTypeFourteenth1 { get; set; }
        [BookmarkMapping("BusinessCollateralDescFourteenth1")]
        public string BusinessCollateralDescFourteenth1 { get; set; }
        [BookmarkMapping("BusinessCollateralValueFourteenth1")]
        public decimal BusinessCollateralValueFourteenth1 { get; set; }
        [BookmarkMapping("BusinessCollateralNewFifteenth1")]
        public string BusinessCollateralNewFifteenth1 { get; set; }
        [BookmarkMapping("BusinessCollateralTypeFifteenth1")]
        public string BusinessCollateralTypeFifteenth1 { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeFifteenth1")]
        public string BusinessCollateralSubTypeFifteenth1 { get; set; }
        [BookmarkMapping("BusinessCollateralDescFifteenth1")]
        public string BusinessCollateralDescFifteenth1 { get; set; }
        [BookmarkMapping("BusinessCollateralValueFifteenth1")]
        public decimal BusinessCollateralValueFifteenth1 { get; set; }
        [BookmarkMapping("SumBusinessCollateralValue")]
        public decimal SumBusinessCollateralValue { get; set; }
        [BookmarkMapping("MarketingComment")]
        public string MarketingComment { get; set; }
        [BookmarkMapping("CreditComment")]
        public string CreditComment { get; set; }
        [BookmarkMapping("ApproverComment")]
        public string ApproverComment { get; set; }
        [BookmarkMapping("ApproverNameFirst1")]
        public string ApproverNameFirst1 { get; set; }
        [BookmarkMapping("ApproverReasonFirst1")]
        public string ApproverReasonFirst1 { get; set; }
        [BookmarkMapping("ApprovedDateFirst1")]
        public string ApprovedDateFirst1 { get; set; }
        [BookmarkMapping("ApproverNameSecond1")]
        public string ApproverNameSecond1 { get; set; }
        [BookmarkMapping("ApproverReasonSecond1")]
        public string ApproverReasonSecond1 { get; set; }
        [BookmarkMapping("ApprovedDateSecond1")]
        public string ApprovedDateSecond1 { get; set; }
        [BookmarkMapping("ApproverNameThird1")]
        public string ApproverNameThird1 { get; set; }
        [BookmarkMapping("ApproverReasonThird1")]
        public string ApproverReasonThird1 { get; set; }
        [BookmarkMapping("ApprovedDateThird1")]
        public string ApprovedDateThird1 { get; set; }
        [BookmarkMapping("ApproverNameFourth1")]
        public string ApproverNameFourth1 { get; set; }
        [BookmarkMapping("ApproverReasonFourth1")]
        public string ApproverReasonFourth1 { get; set; }
        [BookmarkMapping("ApprovedDateFourth1")]
        public string ApprovedDateFourth1 { get; set; }
        [BookmarkMapping("ApproverNameFifth1")]
        public string ApproverNameFifth1 { get; set; }
        [BookmarkMapping("ApproverReasonFifth1")]
        public string ApproverReasonFifth1 { get; set; }
        [BookmarkMapping("ApprovedDateFifth1")]
        public string ApprovedDateFifth1 { get; set; }
        //R03
        [BookmarkMapping("AllBuyerCoRemainingAmount_R01")]
        public decimal AllBuyerCoRemainingAmount_R01 { get; set; }
        [BookmarkMapping("AROutstandingAllCustomer_1")]
        public decimal AROutstandingAllCustomer_1 { get; set; }
        [BookmarkMapping("AROutstandingAllCustomer_2")]
        public decimal AROutstandingAllCustomer_2 { get; set; }
        [BookmarkMapping("AROutstandingAllCustomer_3")]
        public decimal AROutstandingAllCustomer_3 { get; set; }
        [BookmarkMapping("AROutstandingAllCustomer_4")]
        public decimal AROutstandingAllCustomer_4 { get; set; }
        [BookmarkMapping("AROutstandingAllCustomer_5")]
        public decimal AROutstandingAllCustomer_5 { get; set; }
        [BookmarkMapping("AROutstandingAllCustomer_6")]
        public decimal AROutstandingAllCustomer_6 { get; set; }
        [BookmarkMapping("AROutstandingAllCustomer_7")]
        public decimal AROutstandingAllCustomer_7 { get; set; }
        [BookmarkMapping("AROutstandingAllCustomer_8")]
        public decimal AROutstandingAllCustomer_8 { get; set; }
        [BookmarkMapping("AROutstandingAllCustomer_9")]
        public decimal AROutstandingAllCustomer_9 { get; set; }
        [BookmarkMapping("AROutstandingAllCustomer_10")]
        public decimal AROutstandingAllCustomer_10 { get; set; }
        [BookmarkMapping("PurchaseConditionCust_1")]
        public string PurchaseConditionCust_1 { get; set; }
        [BookmarkMapping("PurchaseConditionCust_2")]
        public string PurchaseConditionCust_2 { get; set; }
        [BookmarkMapping("PurchaseConditionCust_3")]
        public string PurchaseConditionCust_3 { get; set; }
        [BookmarkMapping("PurchaseConditionCust_4")]
        public string PurchaseConditionCust_4 { get; set; }
        [BookmarkMapping("PurchaseConditionCust_5")]
        public string PurchaseConditionCust_5 { get; set; }
        [BookmarkMapping("PurchaseConditionCust_6")]
        public string PurchaseConditionCust_6 { get; set; }
        [BookmarkMapping("PurchaseConditionCust_7")]
        public string PurchaseConditionCust_7 { get; set; }
        [BookmarkMapping("PurchaseConditionCust_8")]
        public string PurchaseConditionCust_8 { get; set; }
        [BookmarkMapping("PurchaseConditionCust_9")]
        public string PurchaseConditionCust_9 { get; set; }
        [BookmarkMapping("PurchaseConditionCust_10")]
        public string PurchaseConditionCust_10 { get; set; }
        public int ProductType { get; set; }
        //R03

        public Guid CreditAppRequestLine1GUID { get; set; }
        public Guid CreditAppRequestLine2GUID { get; set; }
        public Guid CreditAppRequestLine3GUID { get; set; }
        public Guid CreditAppRequestLine4GUID { get; set; }
        public Guid CreditAppRequestLine5GUID { get; set; }
        public Guid CreditAppRequestLine6GUID { get; set; }
        public Guid CreditAppRequestLine7GUID { get; set; }
        public Guid CreditAppRequestLine8GUID { get; set; }
        public Guid CreditAppRequestLine9GUID { get; set; }
        public Guid CreditAppRequestLine10GUID { get; set; }
        public Guid CreditAppRequestTableGUID { get; set; }
    }

    public class QCAReqLine
    {
        public Guid CreditAppRequestLineGUID { get; set; }
        public int LineNum { get; set; }
        public string BuyerName { get; set; }
        public string BusinessSegment { get; set; }
        public string BusinessSegmentId { get; set; }
        public int BusinessSegmentType { get; set; }
        public decimal CreditLimitLineRequest { get; set; }
        public decimal MaxPurchasePct { get; set; }
        public string AssignmentMethodRemark { get; set; }
        public decimal BuyerCreditLimit { get; set; }
        public string BillingResponsibleBy { get; set; }
        public string MethodOfPayment { get; set; }
        public string TaxId { get; set; }
        public string InvoiceAddress { get; set; }
        public DateTime? DateOfEstablish { get; set; }
        public string LineOfBusiness { get; set; }
        public string BlacklistStatus { get; set; }
        public string CreditScoring { get; set; }
        public decimal PurchaseFeePct { get; set; }
        public int PurchaseFeeCalculateBase { get; set; }
        public string BillingDescription { get; set; }
        public string BillingContactPersonName { get; set; }
        public string BillingContactPersonPosition { get; set; }
        public string BillingContactPersonPhone { get; set; }
        public string BuyerBillingAddress { get; set; }
        public string ReceiptDescription { get; set; }
        public string ReceiptContactPersonName { get; set; }
        public string ReceiptContactPersonPosition { get; set; }
        public string ReceiptContactPersonPhone { get; set; }
        public string BuyerReceiptAddress { get; set; }
        public bool AcceptanceDocument { get; set; }
        public string MarketingComment { get; set; }
        public string CreditComment { get; set; }
        public string ApproverComment { get; set; }
        public int RowNumber { get; set; }
        public decimal SumServiceFeeCondition { get; set; }
        //R03
        public decimal AllCustomerBuyerOutstanding { get; set; }
        public Guid BuyerTableGUID { get; set; }
        public string LineCondition { get; set; }
        //R03
        public List<QCreditAppReqLineFin_BuyerCreditApplication> ListFinancial { get; set; }
        public List<QServiceFeeConditionLine_BuyerCreditApplication> ListServiceFeeCondition { get; set; }
        public List<QDocumentCondition_BuyerCreditApplication> ListBillingDocumentCondition { get; set; }
        public List<QDocumentCondition_BuyerCreditApplication> ListReceiptDocumentCondition { get; set; }
    }

    public class QBuyerActionHistory
    {
        public string ApproverName { get; set; }
        public string Comment { get; set; }
        public DateTime CreatedDateTime { get; set; }
    }

    public class QBuyerCreditAppReqAssignment
    {
        public bool IsNew { get; set; }
        public string AssignmentBuyer { get; set; }
        public string AssignmentRefAgreementId { get; set; }
        public decimal AssignmentBuyerAgreementAmount { get; set; }
        public decimal AssignmentAgreementAmount { get; set; }
        public string AssignmentAgreementDate { get; set; }
        public decimal RemainingAmount { get; set; }
    }

    public class QBuyerCreditAppReqBusinessCollateral
    {
        public bool IsNew { get; set; }
        public string BusinessCollateralType { get; set; }
        public string BusinessCollateralSubType { get; set; }
        public string Description { get; set; }
        public decimal BusinessCollateralValue { get; set; }
    }

    public class DistinctYear
    {
        public int Year { get; set; }
    }

    public class QCreditAppReqLineFin_BuyerCreditApplication
    {
        public Guid CreditAppRequestLineGUID { get; set; }
        public int Year { get; set; }
        public decimal RegisteredCapital { get; set; }
        public decimal PaidCapital { get; set; }
        public decimal TotalAsset { get; set; }
        public decimal TotalLiability { get; set; }
        public decimal TotalEquity { get; set; }
        public decimal TotalRevenue { get; set; }
        public decimal TotalCOGS { get; set; }
        public decimal TotalGrossProfit { get; set; }
        public decimal TotalOperExp { get; set; }
        public decimal TotalNetProfit { get; set; }
        public decimal NetProfitPercent { get; set; }
        public decimal lDE { get; set; }
        public decimal QuickRatio { get; set; }
        public decimal IntCoverageRatio { get; set; }
    }

    public class QServiceFeeConditionLine_BuyerCreditApplication
    {
        public Guid CreditAppRequestLineGUID { get; set; }
        public string Description { get; set; }
        public string ServicefeeType { get; set; }
        public decimal AmountBeforeTax { get; set; }
        public int Ordering { get; set; }
        public int RowNumber { get; set; }
    }

    public class QDocumentCondition_BuyerCreditApplication
    {
        public Guid CreditAppRequestLineGUID { get; set; }
        public string Description { get; set; }
        public int RowNumber { get; set; }
    }
}