using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ContactTransListView : ViewCompanyBaseEntity
	{
		public string ContactTransGUID { get; set; }
		public int ContactType { get; set; }
		public bool PrimaryContact { get; set; }
		public string Description { get; set; }
		public string ContactValue { get; set; }
		public string RefGUID { get; set; }

	}
}
