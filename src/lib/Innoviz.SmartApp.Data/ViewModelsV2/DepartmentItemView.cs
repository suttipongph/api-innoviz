using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class DepartmentItemView : ViewCompanyBaseEntity
	{
		public string DepartmentGUID { get; set; }
		public string DepartmentId { get; set; }
		public string Description { get; set; }
	}
}
