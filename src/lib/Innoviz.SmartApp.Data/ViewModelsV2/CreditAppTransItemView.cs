using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CreditAppTransItemView : ViewCompanyBaseEntity
	{
		public string CreditAppTransGUID { get; set; }
		public string BuyerTableGUID { get; set; }
		public string CreditAppLineGUID { get; set; }
		public string CreditAppTableGUID { get; set; }
		public decimal CreditDeductAmount { get; set; }
		public string CustomerTableGUID { get; set; }
		public string DocumentId { get; set; }
		public decimal InvoiceAmount { get; set; }
		public int ProductType { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public string TransDate { get; set; }
		public string ProductType_Values { get; set; }
		public string RefType_Values { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string CreditAppLine_Values { get; set; }
	}
}
