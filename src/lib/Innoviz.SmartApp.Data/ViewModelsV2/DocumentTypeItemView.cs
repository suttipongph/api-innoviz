using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class DocumentTypeItemView : ViewCompanyBaseEntity
	{
		public string DocumentTypeGUID { get; set; }
		public string Description { get; set; }
		public string DocumentTypeId { get; set; }
	}
}
