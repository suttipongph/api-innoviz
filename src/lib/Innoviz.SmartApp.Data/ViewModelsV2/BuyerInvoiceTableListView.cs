using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BuyerInvoiceTableListView : ViewCompanyBaseEntity
	{
		public string BuyerInvoiceTableGUID { get; set; }
		public string BuyerInvoiceId { get; set; }
		public string InvoiceDate { get; set; }
		public string DueDate { get; set; }
		public decimal Amount { get; set; }
		public string CreditAppLineGUID { get; set; }
		public string BuyerAgreementTableGUID { get; set; }
		public string BuyerAgreementLineGUID { get; set; }
		public string BuyerAgreementTable_Values { get; set; }
		public string BuyerAgreementLine_Values { get; set; }
		public string BuyerAgreementTable_BuyerAgreementId { get; set; }
		public string BuyerAgreementLine_Period { get; set; }
		public bool AccessModeCanDelete { get; set; }
	}
}
