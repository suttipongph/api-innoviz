﻿using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class GenMainAgmAddendumView : ViewCompanyBaseEntity
	{
		public string MainAgreementTableGUID {get; set;}
		public string AddendumInternalMainAgreementId {get; set;}
		public string AgreementDate {get; set;}
		public int AgreementDocType {get; set;}
		public string BuyerGUID { get; set; }
		public string BuyerId {get; set;}
		public string BuyerName {get; set;}
		public string CreditAppRequestDescription {get; set;}
		public string CreditAppRequestTableGUID {get; set;}
		public int CreditAppRequestType {get; set;}
		public string CreditLimitTypeGUID {get; set;}
		public string CustomerGUID { get; set; }
		public string CustomerId {get; set;}
		public string CustomerName {get; set;}
		public string Description {get; set;}
		public string InternalMainAgreementId {get; set;}
		public string MainAgreementDescription {get; set;}
		public string MainAgreementId {get; set;}
		public string RequestDate {get; set;}
		public string MainAgreementTable_Values { get; set; }
		public string CreditAppRequestTable_Values { get; set; }
		public string CreditLimitType_Values { get; set; }
		public string Customer_Values { get; set; }
		public string Buyer_Values { get; set; }
		public bool CreditLimitType_Revolving { get; set; }
	}

	public class AddendumMainAgreementView : ResultBaseEntity
    {
		public string MainAgreementTableGUID { get; set; }
		public string AddendumInternalMainAgreementId { get; set; }
		public string AgreementDate { get; set; }
		public int AgreementDocType { get; set; }
		public string BuyerGUID { get; set; }
		public string BuyerId { get; set; }
		public string BuyerName { get; set; }
		public string CreditAppRequestDescription { get; set; }
		public string CreditAppRequestTableGUID { get; set; }
		public int CreditAppRequestType { get; set; }
		public string CreditLimitTypeGUID { get; set; }
		public string CustomerGUID { get; set; }
		public string CustomerId { get; set; }
		public string CustomerName { get; set; }
		public string Description { get; set; }
		public string InternalMainAgreementId { get; set; }
		public string MainAgreementDescription { get; set; }
		public string MainAgreementId { get; set; }
		public string RequestDate { get; set; }
		public string MainAgreementTable_Values { get; set; }
		public string CreditAppRequestTable_Values { get; set; }
		public string CreditLimitType_Values { get; set; }
		public string Customer_Values { get; set; }
		public string Buyer_Values { get; set; }
		public bool CreditLimitType_Revolving { get; set; }
	}
}
