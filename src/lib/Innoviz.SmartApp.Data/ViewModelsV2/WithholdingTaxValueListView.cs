using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class WithholdingTaxValueListView : ViewDateEffectiveBaseEntity
	{
		public string WithholdingTaxValueGUID { get; set; }
		public string WithholdingTaxTableGUID { get; set; }
		public decimal Value { get; set; }
	}
}
