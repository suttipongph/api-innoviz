using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class AuthorizedPersonTransListView : ViewCompanyBaseEntity
	{
		public string AuthorizedPersonTransGUID { get; set; }
		public int Ordering { get; set; }
		public string AuthorizedPersonTypeGUID { get; set; }
		public bool InActive { get; set; }
		public string AuthorizedPersonType_Values { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public string RelatedPersonTableGUID { get; set; }
		public string RefAuthorizedPersonTransGUID { get; set; }
		public string RelatedPersonTable_Name { get; set; }
		public string RelatedPersonTable_TaxId { get; set; }
		public string RelatedPersonTable_PassportId { get; set; }
		public string AuthorizedPersonType_AuthorizedPersonTypeId { get; set; }
	}
}
