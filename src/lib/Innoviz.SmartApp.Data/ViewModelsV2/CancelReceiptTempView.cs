﻿using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class CancelReceiptTempView
    {
        public string ReceiptTempTableGUID { get; set; }
        public string DocumentReasonGUID { get; set; }
        public string ReasonRemark { get; set; }
        public string ReceiptDate { get; set; }
        public string TransDate { get; set; }
        public string ReceiptTempTable_Values { get; set; }
        public string CustomerTable_Values { get; set; }
        public string BuyerTable_Values { get; set; }
        public string SuspenseInvoiceType_Values { get; set; }
        public string CreditAppTable_Values { get; set; }

        public decimal ReceiptAmount { get; set; }
        public decimal SettleAmount { get; set; }
        public decimal SettleFeeAmount { get; set; }
        public decimal SuspenseAmount { get; set; }

        public int ProductType { get; set; }
        public int ReceivedFrom { get; set; }
    }
    public class CancelReceiptTempResultView : ResultBaseEntity
    {
    }
}

