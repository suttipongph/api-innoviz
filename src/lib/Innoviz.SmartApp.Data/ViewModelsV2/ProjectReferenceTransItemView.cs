using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ProjectReferenceTransItemView : ViewCompanyBaseEntity
	{
		public string ProjectReferenceTransGUID { get; set; }
		public string ProjectCompanyName { get; set; }
		public int ProjectCompletion { get; set; }
		public string ProjectName { get; set; }
		public string ProjectStatus { get; set; }
		public decimal ProjectValue { get; set; }
		public string RefGUID { get; set; }
		public string RefId { get; set; }
		public int RefType { get; set; }
		public string Remark { get; set; }
	}
}
