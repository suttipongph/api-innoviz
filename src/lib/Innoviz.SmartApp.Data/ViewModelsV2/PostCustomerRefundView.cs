﻿using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class PostCustomerRefundView
    {
        public string CustomerRefundTableGUID { get; set; }
        public string CustomerTable_Values { get; set; }
        public string CustomerRefundTable_Values { get; set; }
        public string DocumentStatus_Values { get; set; }
        public decimal NetAmount { get; set; }
        public decimal PaymentAmount { get; set; }
        public decimal RetentionAmount { get; set; }
        public decimal ServiceFeeAmount { get; set; }
        public decimal SettleAmount { get; set; }
        public decimal SuspenseAmount { get; set; }
        public string TransDate { get; set; }
    }
    public class PostCustomerRefundResultView : ResultBaseEntity
    {
        public CustomerRefundTable CustomerRefundTable { get; set; }
        public List<InvoiceTable> InvoiceTable { get; set; }
        public List<InvoiceLine> InvoiceLine { get; set; }
        public List<CustTrans> CustTrans { get; set; }
        public List<CustTrans> CustTrans_Update { get; set; }
        public List<ProcessTrans> ProcessTrans { get; set; }
        public List<InvoiceSettlementDetail> InvoiceSettlementDetail { get; set; }
        public List<VendorPaymentTrans> VendorPaymentTrans { get; set; }
        public List<RetentionTrans> RetentionTrans { get; set; }
        public List<InvoiceSettlementDetail> InvoiceSettlementDetail_Updated { get; set; }
        public List<ReceiptTable> ReceiptTable { get; set; }
        public List<ReceiptLine> ReceiptLine { get; set; }
        public List<TaxInvoiceTable> TaxInvoiceTable { get; set; }
        public List<TaxInvoiceLine> TaxInvoiceLine { get; set; }
        public List<PaymentHistory> PaymentHistory { get; set; }
        public List<IntercompanyInvoiceTable> IntercompanyInvoiceTable { get; set; }

        public List<CreditAppTrans> CreditAppTrans { get; set; }
    }
}

