using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ProcessTransItemView : ViewCompanyBaseEntity
	{
		public string ProcessTransGUID { get; set; }
		public decimal Amount { get; set; }
		public decimal AmountMST { get; set; }
		public string ARLedgerAccount { get; set; }
		public string CreditAppTableGUID { get; set; }
		public string CurrencyGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public string DocumentId { get; set; }
		public string DocumentReasonGUID { get; set; }
		public decimal ExchangeRate { get; set; }
		public string OrigTaxTableGUID { get; set; }
		public string PaymentHistoryGUID { get; set; }
		public string PaymentDetailGUID { get; set; }
		public int ProcessTransType { get; set; }
		public int ProductType { get; set; }
		public string RefGUID { get; set; }
		public string RefId { get; set; }
		public string RefProcessTransGUID { get; set; }
		public string RefProcessTrans_Values { get; set; }
		public string RefTaxInvoiceGUID { get; set; }
		public string RefTaxInvoice_Values { get; set; }
		public int RefType { get; set; }
		public bool Revert { get; set; }
		public string StagingBatchId { get; set; }
		public int StagingBatchStatus { get; set; }
		public decimal TaxAmount { get; set; }
		public decimal TaxAmountMST { get; set; }
		public string TaxTableGUID { get; set; }
		public string TransDate { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string CurrencyTable_Values { get; set; }
		public string ReasonTable_Values { get; set; }
		public string RefTexTable_Values { get; set; }
		public string TexCode_Values { get; set; }
		public string OriginalTax_Values { get; set; }
		public string History_Values { get; set; }
		public string InvoiceTableGUID { get; set; }
		public string InvoiceTable_Values { get; set; }
		public string ProcessTrans_Values { get; set; }
		public int SourceRefType { get; set; }
		public string SourceRefId { get; set; }
		public string PaymentDetail_Values { get; set; }
	}
}
