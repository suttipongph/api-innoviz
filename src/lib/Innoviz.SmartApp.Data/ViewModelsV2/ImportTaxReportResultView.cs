using System.Collections.Generic;
using System.Collections.ObjectModel;
using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ImportTaxReportResultView : ResultBaseEntity
	{
		public string TaxReportTransGUID { get; set; }
		public decimal Amount { get; set; }
		public string Description { get; set; }
		public int Month { get; set; }
		public string RefGUID { get; set; }
		public string RefId { get; set; }
		public int RefType { get; set; }
		public int Year { get; set; }
 		public FileInformation FileInfo { get; set; }
		 
	}
}
