using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class WithholdingTaxGroupItemView : ViewCompanyBaseEntity
	{
		public string WithholdingTaxGroupGUID { get; set; }
		public string Description { get; set; }
		public string WHTGroupId { get; set; }
	}
}
