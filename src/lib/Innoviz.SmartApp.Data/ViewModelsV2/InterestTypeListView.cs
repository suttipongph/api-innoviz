using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class InterestTypeListView : ViewCompanyBaseEntity
	{
		public string InterestTypeGUID { get; set; }
		public string InterestTypeId { get; set; }
		public string Description { get; set; }
	}
}
