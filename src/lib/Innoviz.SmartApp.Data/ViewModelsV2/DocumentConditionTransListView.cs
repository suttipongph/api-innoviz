using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class DocumentConditionTransListView : ViewCompanyBaseEntity
	{
		public string DocumentConditionTransGUID { get; set; }
		public string DocumentTypeGUID { get; set; }
		public bool Mandatory { get; set; }
		public string DocumentType_Values { get; set; }
		public string DocumentType_DocumentTypeId { get; set; }
		public string RefGUID { get; set; }
		public int DocConVerifyType { get; set; }
		public string RefDocumentConditionTransGUID { get; set; }

	}
}
