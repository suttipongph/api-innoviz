using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class PaymentDetailItemView : ViewCompanyBaseEntity
	{
		public string PaymentDetailGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public string InvoiceTypeGUID { get; set; }
		public int PaidToType { get; set; }
		public decimal PaymentAmount { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public bool SuspenseTransfer { get; set; }
		public string VendorTableGUID { get; set; }
		public string RefId { get; set; }
	}
}
