﻿using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class CancelBuyerReceiptTableView
    {
        public string BuyerReceiptTableGUID { get; set; }
        public string BuyerTable_Values { get; set; }
        public string BuyerReceiptDate { get; set; }
        public string BuyerReceiptTable_Values { get; set; }
        public string CustomerTable_Values { get; set; }
        public bool Cancel { get; set; }
        public decimal BuyerReceiptAmount { get; set; }
    }
    public class CancelBuyerReceiptTableResultView : ResultBaseEntity
    {
    }
}

