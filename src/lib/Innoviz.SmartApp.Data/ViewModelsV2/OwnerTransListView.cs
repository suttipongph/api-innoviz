using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class OwnerTransListView : ViewCompanyBaseEntity
	{
		public string OwnerTransGUID { get; set; }
		public int Ordering { get; set; }
		public bool InActive { get; set; }
		public string RefGUID { get; set; }
		public string RelatedPersonTable_Name { get; set; }
		public string RelatedPersonTable_TaxId { get; set; }
		public string RelatedPersonTable_PassportId { get; set; }
	}
}
