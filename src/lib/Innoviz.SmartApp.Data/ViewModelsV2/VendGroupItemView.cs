using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class VendGroupItemView : ViewCompanyBaseEntity
	{
		public string VendGroupGUID { get; set; }
		public string Description { get; set; }
		public string VendGroupId { get; set; }
	}
}
