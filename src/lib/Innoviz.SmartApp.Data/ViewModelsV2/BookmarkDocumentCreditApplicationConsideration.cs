﻿using System;
using System.Collections.Generic;
using System.Text;
using Innoviz.SmartApp.Core.Attributes;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class BookmarkDocumentCreditApplicationConsideration
    {
        [BookmarkMapping("CANumber")]
        public string QCreditAppRequestTable_CreditAppRequestId { get; set; }
        [BookmarkMapping("RefCANumber")]
        public string QCreditAppRequestTable_OriginalCreditAppTableId { get; set; }
        [BookmarkMapping("CAStatus")]
        public string QCreditAppRequestTable_Status { get; set; }
        [BookmarkMapping("CACreateDate")]
        public string QCreditAppRequestTable_RequestDate { get; set; }
        [BookmarkMapping("CustomerName1","CustomerName2")]
        public string QCreditAppRequestTable_CustomerName { get; set; }
        [BookmarkMapping("CustomerCode")]
        public string QCreditAppRequestTable_CustomerId { get; set; }
        [BookmarkMapping("SalesResp")]
        public string QCreditAppRequestTable_ResponsibleBy { get; set; }
        [BookmarkMapping("KYCHeader")]
        public string QCreditAppRequestTable_KYC { get; set; }
        [BookmarkMapping("CreditScoring")]
        public string QCreditAppRequestTable_CreditScoring { get; set; }
        [BookmarkMapping("CADesp")]
        public string QCreditAppRequestTable_Description { get; set; }
        [BookmarkMapping("Purpose")]
        public string QCreditAppRequestTable_Purpose { get; set; }
        [BookmarkMapping("CARemark")]
        public string QCreditAppRequestTable_Remark { get; set; }
        [BookmarkMapping("CustCompanyRegistrationID")]
        public string Variable_CustCompanyRegistrationID { get; set; }
        [BookmarkMapping("CustomerAddress")]
        public string QCreditAppRequestTable_RegisteredAddress { get; set; }
        [BookmarkMapping("CustComEstablishedDate")]
        public string Variable_CustComEstablishedDate { get; set; }
        [BookmarkMapping("CustBusinessType")]
        public string QCreditAppRequestTable_BusinessType { get; set; }
        [BookmarkMapping("Channel")]
        public string QCreditAppRequestTable_IntroducedBy { get; set; }
        [BookmarkMapping("CustBankName")]
        public string QCreditAppRequestTable_Instituetion { get; set; }
        [BookmarkMapping("CustBankBranch")]
        public string QCreditAppRequestTable_BankBranch { get; set; }
        [BookmarkMapping("CustBankAccountType")]
        public string QCreditAppRequestTable_BankType { get; set; }
        [BookmarkMapping("CustBankAccountNumber")]
        public string QCreditAppRequestTable_BankAccountName { get; set; }
        [BookmarkMapping("CustAuthorizedPersonNameFirst1")]
        public string QAuthorizedPersonTrans1_AuthorizedPersonName { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeFirst1")]
        public string Variable_AuthorizedPersonTrans1Age { get; set; }
        [BookmarkMapping("CustAuthorizedPersonNameSecond1")]
        public string QAuthorizedPersonTrans2_AuthorizedPersonName { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeSecond1")]
        public string Variable_AuthorizedPersonTrans2Age { get; set; }
        [BookmarkMapping("CustAuthorizedPersonNameThird1")]
        public string QAuthorizedPersonTrans3_AuthorizedPersonName { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeThird1")]
        public string AuthorizedPersonTrans3Age { get; set; }
        [BookmarkMapping("CustAuthorizedPersonNameFourth1")]
        public string QAuthorizedPersonTrans4_AuthorizedPersonName { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeFourth1")]
        public string AuthorizedPersonTrans4Age { get; set; }
        [BookmarkMapping("CustAuthorizedPersonNameFifth1")]
        public string QAuthorizedPersonTrans5_AuthorizedPersonName { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeFifth1")]
        public string AuthorizedPersonTrans5Age { get; set; }
        [BookmarkMapping("CustAuthorizedPersonNameSixth1")]
        public string QAuthorizedPersonTrans6_AuthorizedPersonName { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeSixth1")]
        public string AuthorizedPersonTrans6Age { get; set; }
        [BookmarkMapping("CustAuthorizedPersonNameSeventh1")]
        public string QAuthorizedPersonTrans7_AuthorizedPersonName { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeSeventh1")]
        public string AuthorizedPersonTrans7Age { get; set; }
        [BookmarkMapping("CustAuthorizedPersonNameEighth1")]
        public string QAuthorizedPersonTrans8_AuthorizedPersonName { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeEighth1")]
        public string AuthorizedPersonTrans8Age { get; set; }
        [BookmarkMapping("CustShareHolderPersonNameFirst1")]
        public string QOwnerTrans1_ShareHolderPersonName { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentFirst1")]
        public string QOwnerTrans1_PropotionOfShareholderPct { get; set; }
        [BookmarkMapping("CustShareHolderPersonNameSecond1")]
        public string QOwnerTrans2_ShareHolderPersonName { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentSecond1")]
        public string QOwnerTrans2_PropotionOfShareholderPct { get; set; }
        [BookmarkMapping("CustShareHolderPersonNameThird1")]
        public string QOwnerTrans3_ShareHolderPersonName { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentThird1")]
        public string QOwnerTrans3_PropotionOfShareholderPct { get; set; }
        [BookmarkMapping("CustShareHolderPersonNameFourth1")]
        public string QOwnerTrans4_ShareHolderPersonName { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentFourth1")]
        public string QOwnerTrans4_PropotionOfShareholderPct { get; set; }
        [BookmarkMapping("CustShareHolderPersonNameFifth1")]
        public string QOwnerTrans5_ShareHolderPersonName { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentFifth1")]
        public string  QOwnerTrans5_PropotionOfShareholderPct { get; set; }
        [BookmarkMapping("CustShareHolderPersonNameSixth1")]
        public string QOwnerTrans6_ShareHolderPersonName  { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentSixth1")]
        public string QOwnerTrans6_PropotionOfShareholderPct { get; set; }
        [BookmarkMapping("CustShareHolderPersonNameSeventh1")]
        public string QOwnerTrans7_ShareHolderPersonName  { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentSeventh")]
        public string QOwnerTrans7_PropotionOfShareholderPct { get; set; }
        [BookmarkMapping("CustShareHolderPersonNameEighth1")]
        public string QOwnerTrans8_ShareHolderPersonName { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentEighth1")]
        public string QOwnerTrans8_PropotionOfShareholderPct { get; set; }
        [BookmarkMapping("ProductType")]
        public string QCreditAppRequestTable_ProductType { get; set; }
        [BookmarkMapping("ProductSubType")]
        public string QCreditAppRequestTable_ProductSubType { get; set; }
        [BookmarkMapping("CreditLimitType")]
        public string QCreditAppRequestTable_CreditLimitType { get; set; }
        [BookmarkMapping("CreditLimitLife")]
        public string QCreditAppRequestTable_CreditLimitExpiration { get; set; }
        [BookmarkMapping("CreditPeriodStartDate")]
        public string QCreditAppRequestTable_StartDate { get; set; }
        [BookmarkMapping("CreditPeriodEndDate")]
        public string QCreditAppRequestTable_ExpiryDate { get; set; }
        [BookmarkMapping("CreditLimitReqAmt")]
        public decimal QCreditAppRequestTable_CreditLimitRequest { get; set; }
        [BookmarkMapping("CreditAllBuyer")]
        public decimal QCreditAppRequestTable_CustomerCreditLimit { get; set; }
        [BookmarkMapping("CAInterestType")]
        public string QCreditAppRequestTable_InterestType { get; set; }
        [BookmarkMapping("InterestAdj")]
        public decimal QCreditAppRequestTable_InterestAdjustment { get; set; }
        [BookmarkMapping("TotalInterest")]
        public decimal NewInterestRate { get; set; }
        [BookmarkMapping("MaxPurch")]
        public decimal QCreditAppRequestTable_MaxPurchasePct { get; set; }
        [BookmarkMapping("MaxRetenPerc")]
        public decimal QCreditAppRequestTable_MaxRetentionPct { get; set; }
        [BookmarkMapping("MaxRetenAmt")]
        public decimal QCreditAppRequestTable_MaxRetentionAmount { get; set; }
        [BookmarkMapping("CustPDCBankName")]

        public string QCreditAppRequestTable_CustPDCBankName { get; set; }
        [BookmarkMapping("CustPDCBankBranch")]

        public string QCreditAppRequestTable_CustPDCBankBranch { get; set; }
        [BookmarkMapping("CustPDCBankAccountType")]
        public string QCreditAppRequestTable_CustPDCBankType { get; set; }
        [BookmarkMapping("CustPDCBankAccountNumber")]
        public string QCreditAppRequestTable_CustPDCBankAccountName { get; set; }
        [BookmarkMapping("CreditLimitFee")]
        public decimal QCreditAppRequestTable_CreditRequestFeePct { get; set; }
        [BookmarkMapping("CreditLimitFeeAmt")]
        public decimal QCreditAppRequestTable_CreditRequestFeeAmount { get; set; }
        [BookmarkMapping("FactoringPurchaseFee")]
        public decimal QCreditAppRequestTable_PurchaseFeePct { get; set; }
        [BookmarkMapping("FactoringPurchaseFeeCalBase")]
        public string QCreditAppRequestTable_PurchaseFeeCalculateBase { get; set; }
        [BookmarkMapping("CustFinYearFirst1")]
        public int QCreditAppReqLineFin1_Year { get; set; }
        [BookmarkMapping("CustFinRegisteredCapitalFirst1")]
        public decimal QCreditAppReqLineFin1_RegisteredCapital { get; set; }
        [BookmarkMapping("CustFinPaidCapitalFirst1")]
        public decimal QCreditAppReqLineFin1_PaidCapital { get; set; }
        [BookmarkMapping("CustFinTotalAssetFirst1")]
        public decimal QCreditAppReqLineFin1_TotalAsset { get; set; }
        [BookmarkMapping("CustFinTotalLiabilityFirst1")]
        public decimal QCreditAppReqLineFin1_TotalLiability { get; set; }
        [BookmarkMapping("CustFinTotalEquityFirst1")]
        public decimal QCreditAppReqLineFin1_TotalEquity { get; set; }
        [BookmarkMapping("CustFinYearFirst2")]
        public int AppReqLineFin1_Year { get; set; }
        [BookmarkMapping("CustFinTotalRevenueFirst1")]
        public decimal QCreditAppReqLineFin1_TotalRevenue { get; set; }
        [BookmarkMapping("CustFinTotalCOGSFirst1")]
        public decimal QCreditAppReqLineFin1_TotalCOGS { get; set; }
        [BookmarkMapping("CustFinTotalGrossProfitFirst1")]
        public decimal QCreditAppReqLineFin1_TotalGrossProfit { get; set; }
        [BookmarkMapping("CustFinTotalOperExpFirst1")]
        public decimal QCreditAppReqLineFin1_TotalOperExpFirst { get; set; }
        [BookmarkMapping("CustFinTotalNetProfitFirst1")]
        public decimal QCreditAppReqLineFin1_TotalNetProfitFirst { get; set; }
        [BookmarkMapping("CustFinYearSecond1")]
        public int QCreditAppReqLineFin2_Year { get; set; }
        [BookmarkMapping("CustFinRegisteredCapitalSecond1")]
        public decimal QCreditAppReqLineFin2_RegisteredCapital { get; set; }
        [BookmarkMapping("CustFinPaidCapitalSecond1")]
        public decimal QCreditAppReqLineFin2_PaidCapital { get; set; }
        [BookmarkMapping("CustFinTotalAssetSecond1")]
        public decimal QCreditAppReqLineFin2_TotalAsset { get; set; }
        [BookmarkMapping("CustFinTotalLiabilitySecond1")]
        public decimal QCreditAppReqLineFin2_TotalLiability { get; set; }
        [BookmarkMapping("CustFinTotalEquitySecond1")]
        public decimal QCreditAppReqLineFin2_TotalEquity { get; set; }
        [BookmarkMapping("CustFinYearSecond2")]
        public int AppReqLineFin2_Year { get; set; }
        [BookmarkMapping("CustFinTotalRevenueSecond1")]
        public decimal QCreditAppReqLineFin2_TotalRevenue { get; set; }
        [BookmarkMapping("CustFinTotalCOGSSecond1")]
        public decimal QCreditAppReqLineFin2_TotalCOGS { get; set; }
        [BookmarkMapping("CustFinTotalGrossProfitSecond1")]
        public decimal QCreditAppReqLineFin2_TotalGrossProfit { get; set; }
        [BookmarkMapping("CustFinTotalOperExpSecond1")]
        public decimal QCreditAppReqLineFin2_TotalOperExpFirst { get; set; }
        [BookmarkMapping("CustFinTotalNetProfitSecond1")]
        public decimal QCreditAppReqLineFin2_TotalNetProfitFirst { get; set; }
        [BookmarkMapping("CustFinYearThird1")]
        public int QCreditAppReqLineFin3_Year { get; set; }
        [BookmarkMapping("CustFinRegisteredCapitalThird1")]
        public decimal QCreditAppReqLineFin3_RegisteredCapital { get; set; }
        [BookmarkMapping("CustFinPaidCapitalThird1")]
        public decimal QCreditAppReqLineFin3_PaidCapital { get; set; }
        [BookmarkMapping("CustFinTotalAssetThird1")]
        public decimal QCreditAppReqLineFin3_TotalAsset { get; set; }
        [BookmarkMapping("CustFinTotalLiabilityThird1")]
        public decimal QCreditAppReqLineFin3_TotalLiability { get; set; }
        [BookmarkMapping("CustFinTotalEquityThird1")]
        public decimal QCreditAppReqLineFin3_TotalEquity { get; set; }
        [BookmarkMapping("CustFinYearThird2")]
        public int AppReqLineFin3_Year { get; set; }
        [BookmarkMapping("CustFinTotalRevenueThird1")]
        public decimal QCreditAppReqLineFin3_TotalRevenue { get; set; }
        [BookmarkMapping("CustFinTotalCOGSThird1")]
        public decimal QCreditAppReqLineFin3_TotalCOGS { get; set; }
        [BookmarkMapping("CustFinTotalGrossProfitThird1")]
        public decimal QCreditAppReqLineFin3_TotalGrossProfit { get; set; }
        [BookmarkMapping("CustFinTotalOperExpThird1")]
        public decimal QCreditAppReqLineFin3_TotalOperExpFirst { get; set; }
        [BookmarkMapping("CustFinTotalNetProfitThird1")]
        public decimal QCreditAppReqLineFin3_TotalNetProfitFirst { get; set; }
        [BookmarkMapping("CustFinYearFourth1")]
        public int QCreditAppReqLineFin4_Year { get; set; }
        [BookmarkMapping("CustFinRegisteredCapitalFourth1")]
        public decimal QCreditAppReqLineFin4_RegisteredCapital { get; set; }
        [BookmarkMapping("CustFinPaidCapitalFourth1")]
        public decimal QCreditAppReqLineFin4_PaidCapital { get; set; }
        [BookmarkMapping("CustFinTotalAssetFourth1")]
        public decimal QCreditAppReqLineFin4_TotalAsset { get; set; }
        [BookmarkMapping("CustFinTotalLiabilityFourth1")]
        public decimal QCreditAppReqLineFin4_TotalLiability { get; set; }
        [BookmarkMapping("CustFinTotalEquityFourth1")]
        public decimal QCreditAppReqLineFin4_TotalEquity { get; set; }
        [BookmarkMapping("CustFinYearFourth2")]
        public int AppReqLineFin4_Year { get; set; }
        [BookmarkMapping("CustFinTotalRevenueFourth1")]
        public decimal QCreditAppReqLineFin4_TotalRevenue { get; set; }
        [BookmarkMapping("CustFinTotalCOGSFourth1")]
        public decimal QCreditAppReqLineFin4_TotalCOGS { get; set; }
        [BookmarkMapping("CustFinTotalGrossProfitFourth1")]
        public decimal QCreditAppReqLineFin4_TotalGrossProfit { get; set; }
        [BookmarkMapping("CustFinTotalOperExpFourth1")]
        public decimal QCreditAppReqLineFin4_TotalOperExpFirst { get; set; }
        [BookmarkMapping("CustFinTotalNetProfitFourth1")]
        public decimal QCreditAppReqLineFin4_TotalNetProfitFirst { get; set; }
        [BookmarkMapping("CustFinYearFifth1")]
        public int QCreditAppReqLineFin5_Year { get; set; }
        [BookmarkMapping("CustFinRegisteredCapitalFifth1")]
        public decimal QCreditAppReqLineFin5_RegisteredCapital { get; set; }
        [BookmarkMapping("CustFinPaidCapitalFifth1")]
        public decimal QCreditAppReqLineFin5_PaidCapital { get; set; }
        [BookmarkMapping("CustFinTotalAssetFifth1")]
        public decimal QCreditAppReqLineFin5_TotalAsset { get; set; }
        [BookmarkMapping("CustFinTotalLiabilityFifth1")]
        public decimal QCreditAppReqLineFin5_TotalLiability { get; set; }
        [BookmarkMapping("CustFinTotalEquityFifth1")]
        public decimal QCreditAppReqLineFin5_TotalEquity { get; set; }
        [BookmarkMapping("CustFinYearFifth2")]
        public int AppReqLineFin5_Year { get; set; }
        [BookmarkMapping("CustFinTotalRevenueFifth1")]
        public decimal QCreditAppReqLineFin5_TotalRevenue { get; set; }
        [BookmarkMapping("CustFinTotalCOGSFifth1")]
        public decimal QCreditAppReqLineFin5_TotalCOGS { get; set; }
        [BookmarkMapping("CustFinTotalGrossProfitFifth1")]
        public decimal QCreditAppReqLineFin5_TotalGrossProfit { get; set; }
        [BookmarkMapping("CustFinTotalOperExpFifth1")]
        public decimal QCreditAppReqLineFin5_TotalOperExpFirst { get; set; }
        [BookmarkMapping("CustFinTotalNetProfitFifth1")]
        public decimal QCreditAppReqLineFin5_TotalNetProfitFirst { get; set; }
        [BookmarkMapping("CustFinNetProfitPercent1")]
        public decimal QCreditAppReqLineFin1_NetProfitPercent { get; set; }
        [BookmarkMapping("CustFinlDE1")]
        public decimal QCreditAppReqLineFin1_lDE { get; set; }
        [BookmarkMapping("CustFinQuickRatio1")]
        public decimal QCreditAppReqLineFin1_QuickRatio { get; set; }
        [BookmarkMapping("CustFinIntCoverageRatio1")]
        public decimal QCreditAppReqLineFin1_IntCoverageRatio { get; set; }
        [BookmarkMapping("CustFinRevenuePerMonth1")]
        public decimal QCreditAppRequestTable_SalesAvgPerMonth { get; set; }
        [BookmarkMapping("OutputVATStart")]
        public string QTaxReportStart_TaxMonth { get; set; }
        [BookmarkMapping("OutputVATEnd")]
        public string QTaxReportEnd_TaxMonth { get; set; }
        [BookmarkMapping("Month1")]
        public string QTaxReportTrans1_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth1")]
        public decimal QTaxReportTrans1_Amount { get; set; }
        [BookmarkMapping("Month2")]
        public string QTaxReportTrans2_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth2")]
        public decimal QTaxReportTrans2_Amount { get; set; }
        [BookmarkMapping("Month3")]
        public string QTaxReportTrans3_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth3")]
        public decimal QTaxReportTrans3_Amount { get; set; }
        [BookmarkMapping("Month4")]
        public string QTaxReportTrans4_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth4")]
        public decimal QTaxReportTrans4_Amount { get; set; }
        [BookmarkMapping("Month5")]
        public string QTaxReportTrans5_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth5")]
        public decimal QTaxReportTrans5_Amount { get; set; }
        [BookmarkMapping("Month6")]
        public string QTaxReportTrans6_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth6")]
        public decimal QTaxReportTrans6_Amount { get; set; }
        [BookmarkMapping("Month7")]
        public string QTaxReportTrans7_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth7")]
        public decimal QTaxReportTrans7_Amount { get; set; }
        [BookmarkMapping("Month8")]
        public string QTaxReportTrans8_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth8")]
        public decimal QTaxReportTrans8_Amount { get; set; }
        [BookmarkMapping("Month9")]
        public string QTaxReportTrans9_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth9")]
        public decimal QTaxReportTrans9_Amount { get; set; }
        [BookmarkMapping("Month10")]
        public string QTaxReportTrans10_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth10")]
        public decimal QTaxReportTrans10_Amount { get; set; }
        [BookmarkMapping("Month11")]
        public string QTaxReportTrans11_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth11")]
        public decimal QTaxReportTrans11_Amount { get; set; }
        [BookmarkMapping("Month12")]
        public string QTaxReportTrans12_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth12")]
        public decimal QTaxReportTrans12_Amount { get; set; }
        [BookmarkMapping("TotalOutputVAT")]
        public decimal QTaxReportTransSum_SumTaxReport { get; set; }
        [BookmarkMapping("AverageOutputVATPerMonth")]
        public decimal Variable_AverageOutputVATPerMonth { get; set; }
        [BookmarkMapping("ApprovedCreditAsofDate")]
        public string QCreditAppRequestTable_AsOfDate
        { get; set; }
        [BookmarkMapping("CrditLimitAmtFirst1")]
        public decimal QCAReqCreditOutStanding3_ApprovedCreditLimit
        { get; set; }
        [BookmarkMapping("AROutstandingAmtFirst")]
        public decimal QCAReqCreditOutStanding3_ARBalance
        { get; set; }
        [BookmarkMapping("CreditLimitRemainingFirst1")]
        public decimal QCAReqCreditOutStanding3_CreditLimitBalance
        { get; set; }
        [BookmarkMapping("ReserveOutstandingFirst1")]
        public decimal QCAReqCreditOutStanding3_ReserveToBeRefund
        { get; set; }
        [BookmarkMapping("RetentionOutstandingFirst1")]
        public decimal QCAReqCreditOutStanding3_AccumRetentionAmount
        { get; set; }
        [BookmarkMapping("CrditLimitAmtSecond1")]
        public decimal QCAReqCreditOutStanding2_ApprovedCreditLimit
        { get; set; }
        [BookmarkMapping("AROutstandingAmtSecond")]
        public decimal QCAReqCreditOutStanding2_ARBalance
        { get; set; }
        [BookmarkMapping("CreditLimitRemainingSecond1")]
        public decimal QCAReqCreditOutStanding2_CreditLimitBalance
        { get; set; }
        [BookmarkMapping("ReserveOutstandingSecond1")]
        public decimal QCAReqCreditOutStanding2_ReserveToBeRefund
        { get; set; }
        [BookmarkMapping("RetentionOutstandingSecond1")]
        public decimal QCAReqCreditOutStanding2_AccumRetentionAmount
        { get; set; }
        [BookmarkMapping("CrditLimitAmtThird1")]
        public decimal QCAReqCreditOutStanding4_ApprovedCreditLimit
        { get; set; }
        [BookmarkMapping("AROutstandingAmtThird")]
        public decimal QCAReqCreditOutStanding4_ARBalance
        { get; set; }
        [BookmarkMapping("CreditLimitRemainingThird1")]
        public decimal QCAReqCreditOutStanding4_CreditLimitBalance
        { get; set; }
        [BookmarkMapping("ReserveOutstandingThird1")]
        public decimal QCAReqCreditOutStanding4_ReserveToBeRefund
        { get; set; }
        [BookmarkMapping("RetentionOutstandingThird1")]
        public decimal QCAReqCreditOutStanding4_AccumRetentionAmount
        { get; set; }
        [BookmarkMapping("CrditLimitAmtFourth1")]
        public decimal QCAReqCreditOutStanding5_ApprovedCreditLimit
        { get; set; }
        [BookmarkMapping("AROutstandingAmtFourth")]
        public decimal QCAReqCreditOutStanding5_ARBalance
        { get; set; }
        [BookmarkMapping("CreditLimitRemainingFourth1")]
        public decimal QCAReqCreditOutStanding5_CreditLimitBalance
        { get; set; }
        [BookmarkMapping("ReserveOutstandingFourth1")]
        public decimal QCAReqCreditOutStanding5_ReserveToBeRefund
        { get; set; }
        [BookmarkMapping("RetentionOutstandingFourth1")]
        public decimal QCAReqCreditOutStanding5_AccumRetentionAmount { get; set; }
        [BookmarkMapping("CrditLimitAmtFifth1")]
        public decimal QCAReqCreditOutStanding6_ApprovedCreditLimit { get; set; }
        [BookmarkMapping("AROutstandingAmtFifth")]
        public decimal QCAReqCreditOutStanding6_ARBalance { get; set; }
        [BookmarkMapping("CreditLimitRemainingFifth1")]
        public decimal QCAReqCreditOutStanding6_CreditLimitBalance { get; set; }
        [BookmarkMapping("ReserveOutstandingFifth1")]
        public decimal QCAReqCreditOutStanding6_ReserveToBeRefund { get; set; }
        [BookmarkMapping("RetentionOutstandingFifth1")]
        public decimal QCAReqCreditOutStanding6_AccumRetentionAmount { get; set; }
        [BookmarkMapping("CrditLimitAmtSixth1")]
        public decimal QCAReqCreditOutStanding7_ApprovedCreditLimit { get; set; }
        [BookmarkMapping("AROutstandingAmtSixth")]
        public decimal QCAReqCreditOutStanding7_ARBalance { get; set; }
        [BookmarkMapping("CreditLimitRemainingSixth1")]
        public decimal QCAReqCreditOutStanding7_CreditLimitBalance { get; set; }
        [BookmarkMapping("ReserveOutstandingSixth1")]
        public decimal QCAReqCreditOutStanding7_ReserveToBeRefund { get; set; }
        [BookmarkMapping("RetentionOutstandingSixth1")]
        public decimal QCAReqCreditOutStanding7_AccumRetentionAmount { get; set; }
        [BookmarkMapping("CrditLimitAmtSeventh1")]
        public decimal QCAReqCreditOutStanding8_ApprovedCreditLimit { get; set; }
        [BookmarkMapping("AROutstandingAmtSeventh")]
        public decimal QCAReqCreditOutStanding8_ARBalance { get; set; }
        [BookmarkMapping("CreditLimitRemainingSeventh1")]
        public decimal QCAReqCreditOutStanding8_CreditLimitBalance { get; set; }
        [BookmarkMapping("ReserveOutstandingSeventh1")]
        public decimal QCAReqCreditOutStanding8_ReserveToBeRefund { get; set; }
        [BookmarkMapping("RetentionOutstandingSeventh1")]
        public decimal QCAReqCreditOutStanding8_AccumRetentionAmount { get; set; }
        [BookmarkMapping("CrditLimitAmtEighth1")]
        public decimal QCAReqCreditOutStanding9_ApprovedCreditLimit { get; set; }
        [BookmarkMapping("AROutstandingAmtEighth")]
        public decimal QCAReqCreditOutStanding9_ARBalance { get; set; }
        [BookmarkMapping("CreditLimitRemainingEighth1")]
        public decimal QCAReqCreditOutStanding9_CreditLimitBalance { get; set; }
        [BookmarkMapping("ReserveOutstandingEighth1")]
        public decimal QCAReqCreditOutStanding9_ReserveToBeRefund { get; set; }
        [BookmarkMapping("RetentionOutstandingEighth1")]
        public decimal QCAReqCreditOutStanding9_AccumRetentionAmount { get; set; }
        [BookmarkMapping("CrditLimitAmtNineth1")]
        public decimal QCAReqCreditOutStanding10_ApprovedCreditLimit { get; set; }
        [BookmarkMapping("AROutstandingAmtNineth")]
        public decimal QCAReqCreditOutStanding10_ARBalance { get; set; }
        [BookmarkMapping("CreditLimitRemainingNineth1")]
        public decimal QCAReqCreditOutStanding10_CreditLimitBalance { get; set; }
        [BookmarkMapping("ReserveOutstandingNineth1")]
        public decimal QCAReqCreditOutStanding10_ReserveToBeRefund { get; set; }
        [BookmarkMapping("RetentionOutstandingNineth1")]
        public decimal QCAReqCreditOutStanding10_AccumRetentionAmount { get; set; }
        [BookmarkMapping("CrditLimitAmtTenth1")]
        public decimal QCAReqCreditOutStanding11_ApprovedCreditLimit { get; set; }
        [BookmarkMapping("AROutstandingAmtTenth")]
        public decimal QCAReqCreditOutStanding11_ARBalance { get; set; }
        [BookmarkMapping("CreditLimitRemainingTenth1")]
        public decimal QCAReqCreditOutStanding11_CreditLimitBalance { get; set; }
        [BookmarkMapping("ReserveOutstandingTenth1")]
        public decimal QCAReqCreditOutStanding11_ReserveToBeRefund { get; set; }
        [BookmarkMapping("RetentionOutstandingTenth1")]
        public decimal QCAReqCreditOutStanding11_AccumRetentionAmount { get; set; }
        [BookmarkMapping("TotalCreditLimit")]
        public decimal QCAReqCreditOutStandingSum_SumApprovedCreditLimit { get; set; }
        [BookmarkMapping("TotalAROutstanding")]
        public decimal QCAReqCreditOutStandingSum_SumARBalance { get; set; }
        [BookmarkMapping("TotalCreditLimitRemaining")]
        public decimal QCAReqCreditOutStandingSum_SumCreditLimitBalance { get; set; }
        [BookmarkMapping("TotalReserveOutstanding")]
        public decimal QCAReqCreditOutStandingSum_SumReserveToBeRefund { get; set; }
        [BookmarkMapping("TotalRetentionOutstanding")]
        public decimal QCAReqCreditOutStandingSum_SumAccumRetentionAmount { get; set; }
        [BookmarkMapping("CreditFinCheckDate")]
        public string QCreditAppRequestTable_FinancialCreditCheckedDate { get; set; }
        [BookmarkMapping("FinInstitutionFirst1")]
        public string QFinancialCreditTrans1_FinancialInstitutionDesc { get; set; }
        [BookmarkMapping("LoanTypeFirst1")]
        public string QFinancialCreditTrans1_LoanType { get; set; }
        [BookmarkMapping("LoanLimitFirst1")]
        public decimal QFinancialCreditTrans1_Amount { get; set; }
        [BookmarkMapping("FinInstitutionSecond1")]
        public string QFinancialCreditTrans2_FinancialInstitutionDesc { get; set; }
        [BookmarkMapping("LoanTypeSecond1")]
        public string QFinancialCreditTrans2_LoanType { get; set; }
        [BookmarkMapping("LoanLimitSecond1")]
        public decimal QFinancialCreditTrans2_Amount { get; set; }
        [BookmarkMapping("FinInstitutionThird1")]
        public string QFinancialCreditTrans3_FinancialInstitutionDesc { get; set; }
        [BookmarkMapping("LoanTypeThird1")]
        public string QFinancialCreditTrans3_LoanType { get; set; }
        [BookmarkMapping("LoanLimitThird1")]
        public decimal QFinancialCreditTrans3_Amount { get; set; }
        [BookmarkMapping("FinInstitutionFourth1")]
        public string QFinancialCreditTrans4_FinancialInstitutionDesc { get; set; }
        [BookmarkMapping("LoanTypeFourth1")]
        public string QFinancialCreditTrans4_LoanType { get; set; }
        [BookmarkMapping("LoanLimitFourth1")]
        public decimal QFinancialCreditTrans4_Amount { get; set; }
        [BookmarkMapping("FinInstitutionFifth1")]
        public string QFinancialCreditTrans5_FinancialInstitutionDesc { get; set; }
        [BookmarkMapping("LoanTypeFifth1")]
        public string QFinancialCreditTrans5_LoanType { get; set; }
        [BookmarkMapping("LoanLimitFifth1")]
        public decimal QFinancialCreditTrans5_Amount { get; set; }
        [BookmarkMapping("FinInstitutionSixth1")]
        public string QFinancialCreditTrans6_FinancialInstitutionDesc { get; set; }
        [BookmarkMapping("LoanTypeSixth1")]
        public string QFinancialCreditTrans6_LoanType { get; set; }
        [BookmarkMapping("LoanLimitSixth1")]
        public decimal QFinancialCreditTrans6_Amount { get; set; }
        [BookmarkMapping("FinInstitutionSeventh1")]
        public string QFinancialCreditTrans7_FinancialInstitutionDesc { get; set; }
        [BookmarkMapping("LoanTypeSeventh1")]
        public string QFinancialCreditTrans7_LoanType { get; set; }
        [BookmarkMapping("LoanLimitSeventh1")]
        public decimal QFinancialCreditTrans7_Amount { get; set; }
        [BookmarkMapping("FinInstitutionEighth1")]
        public string QFinancialCreditTrans8_FinancialInstitutionDesc { get; set; }
        [BookmarkMapping("LoanTypeEighth1")]
        public string QFinancialCreditTrans8_LoanType { get; set; }
        [BookmarkMapping("LoanLimitEighth1")]
        public decimal QFinancialCreditTrans8_Amount { get; set; }
        [BookmarkMapping("TotalLoanLimit")]
        public decimal QFinancialCreditTransSum_TotalLoanLimit { get; set; }
        [BookmarkMapping("BuyerQtyPrivate")]
        public decimal QCAReqBuyerCreditOutstandingPrivateCOUNT_CountCAbuyerCreditOutstandingPrivate { get; set; }

        [BookmarkMapping("BuyerName1st_1")]
        public string QCAReqBuyerCreditOutstandingPrivate1_BuyerCreditOutstandingBuyerName { get; set; }
        [BookmarkMapping("BuyerCreditLimitStatus1st_1")]
        public string QCAReqBuyerCreditOutstandingPrivate1_Status { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest1st_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate1_ApprovedCreditLimitLine { get; set; }
        [BookmarkMapping("BuyerARBalance1st_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate1_ARBalance { get; set; }
        [BookmarkMapping("PurchasePercent1st_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate1_MaxPurchasePct { get; set; }
        [BookmarkMapping("AssignmentCondition1st_1")]
        public string QCAReqBuyerCreditOutstandingPrivate1_AssignmentMethod { get; set; }
        [BookmarkMapping("BillingResponsible1st_1")]
        public string QCAReqBuyerCreditOutstandingPrivate1_BillingResponsibleBy { get; set; }
        [BookmarkMapping("PaymentMethod1st_1")]
        public string QCAReqBuyerCreditOutstandingPrivate1_MethodOfPayment { get; set; }
        [BookmarkMapping("BuyerName2nd_1")]
        public string QCAReqBuyerCreditOutstandingPrivate2_BuyerCreditOutstandingBuyerName { get; set; }
        [BookmarkMapping("BuyerCreditLimitStatus2nd_1")]
        public string QCAReqBuyerCreditOutstandingPrivate2_Status { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest2nd_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate2_ApprovedCreditLimitLine { get; set; }
        [BookmarkMapping("BuyerARBalance2nd_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate2_ARBalance { get; set; }
        [BookmarkMapping("PurchasePercent2nd_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate2_MaxPurchasePct { get; set; }
        [BookmarkMapping("AssignmentCondition2nd_1")]
        public string QCAReqBuyerCreditOutstandingPrivate2_AssignmentMethod { get; set; }
        [BookmarkMapping("BillingResponsible2nd_1")]
        public string QCAReqBuyerCreditOutstandingPrivate2_BillingResponsibleBy { get; set; }
        [BookmarkMapping("PaymentMethod2nd_1")]
        public string QCAReqBuyerCreditOutstandingPrivate2_MethodOfPayment { get; set; }
        [BookmarkMapping("BuyerName3rd_1")]
        public string QCAReqBuyerCreditOutstandingPrivate3_BuyerCreditOutstandingBuyerName { get; set; }
        [BookmarkMapping("BuyerCreditLimitStatus3rd_1")]
        public string QCAReqBuyerCreditOutstandingPrivate3_Status { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest3rd_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate3_ApprovedCreditLimitLine { get; set; }
        [BookmarkMapping("BuyerARBalance3rd_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate3_ARBalance { get; set; }
        [BookmarkMapping("PurchasePercent3rd_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate3_MaxPurchasePct { get; set; }
        [BookmarkMapping("AssignmentCondition3rd_1")]
        public string QCAReqBuyerCreditOutstandingPrivate3_AssignmentMethod { get; set; }
        [BookmarkMapping("BillingResponsible3rd_1")]
        public string QCAReqBuyerCreditOutstandingPrivate3_BillingResponsibleBy { get; set; }
        [BookmarkMapping("PaymentMethod3rd_1")]
        public string QCAReqBuyerCreditOutstandingPrivate3_MethodOfPayment { get; set; }
        [BookmarkMapping("BuyerName4th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate4_BuyerCreditOutstandingBuyerName { get; set; }
        [BookmarkMapping("BuyerCreditLimitStatus4th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate4_Status { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest4th_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate4_ApprovedCreditLimitLine { get; set; }
        [BookmarkMapping("BuyerARBalance4th_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate4_ARBalance { get; set; }
        [BookmarkMapping("PurchasePercent4th_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate4_MaxPurchasePct { get; set; }
        [BookmarkMapping("AssignmentCondition4th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate4_AssignmentMethod { get; set; }
        [BookmarkMapping("BillingResponsible4th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate4_BillingResponsibleBy { get; set; }
        [BookmarkMapping("PaymentMethod4th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate4_MethodOfPayment { get; set; }
        [BookmarkMapping("BuyerName5th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate5_BuyerCreditOutstandingBuyerName { get; set; }
        [BookmarkMapping("BuyerCreditLimitStatus5th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate5_Status { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest5th_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate5_ApprovedCreditLimitLine { get; set; }
        [BookmarkMapping("BuyerARBalance5th_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate5_ARBalance { get; set; }
        [BookmarkMapping("PurchasePercent5th_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate5_MaxPurchasePct { get; set; }
        [BookmarkMapping("AssignmentCondition5th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate5_AssignmentMethod { get; set; }
        [BookmarkMapping("BillingResponsible5th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate5_BillingResponsibleBy { get; set; }
        [BookmarkMapping("PaymentMethod5th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate5_MethodOfPayment { get; set; }
        [BookmarkMapping("BuyerName6th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate6_BuyerCreditOutstandingBuyerName { get; set; }
        [BookmarkMapping("BuyerCreditLimitStatus6th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate6_Status { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest6th_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate6_ApprovedCreditLimitLine { get; set; }
        [BookmarkMapping("BuyerARBalance6th_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate6_ARBalance { get; set; }
        [BookmarkMapping("PurchasePercent6th_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate6_MaxPurchasePct { get; set; }
        [BookmarkMapping("AssignmentCondition6th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate6_AssignmentMethod { get; set; }
        [BookmarkMapping("BillingResponsible6th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate6_BillingResponsibleBy { get; set; }
        [BookmarkMapping("PaymentMethod6th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate6_MethodOfPayment { get; set; }
        [BookmarkMapping("BuyerName7th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate7_BuyerCreditOutstandingBuyerName { get; set; }
        [BookmarkMapping("BuyerCreditLimitStatus7th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate7_Status { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest7th_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate7_ApprovedCreditLimitLine { get; set; }
        [BookmarkMapping("BuyerARBalance7th_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate7_ARBalance { get; set; }
        [BookmarkMapping("PurchasePercent7th_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate7_MaxPurchasePct { get; set; }
        [BookmarkMapping("AssignmentCondition7th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate7_AssignmentMethod { get; set; }
        [BookmarkMapping("BillingResponsible7th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate7_BillingResponsibleBy { get; set; }
        [BookmarkMapping("PaymentMethod7th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate7_MethodOfPayment { get; set; }
        [BookmarkMapping("BuyerName8th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate8_BuyerCreditOutstandingBuyerName { get; set; }
        [BookmarkMapping("BuyerCreditLimitStatus8th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate8_Status { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest8th_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate8_ApprovedCreditLimitLine { get; set; }
        [BookmarkMapping("BuyerARBalance8th_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate8_ARBalance { get; set; }

        [BookmarkMapping("PurchasePercent8th_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate8_MaxPurchasePct { get; set; }
        [BookmarkMapping("AssignmentCondition8th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate8_AssignmentMethod { get; set; }
        [BookmarkMapping("BillingResponsible8th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate8_BillingResponsibleBy { get; set; }
        [BookmarkMapping("PaymentMethod8th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate8_MethodOfPayment { get; set; }
        [BookmarkMapping("BuyerName9th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate9_BuyerCreditOutstandingBuyerName { get; set; }
        [BookmarkMapping("BuyerCreditLimitStatus9th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate9_Status { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest9th_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate9_ApprovedCreditLimitLine { get; set; }
        [BookmarkMapping("BuyerARBalance9th_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate9_ARBalance { get; set; }
        [BookmarkMapping("PurchasePercent9th_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate9_MaxPurchasePct { get; set; }

        [BookmarkMapping("AssignmentCondition9th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate9_AssignmentMethod { get; set; }
        [BookmarkMapping("BillingResponsible9th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate9_BillingResponsibleBy { get; set; }
        [BookmarkMapping("PaymentMethod9th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate9_MethodOfPayment { get; set; }
        [BookmarkMapping("BuyerName10th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate10_BuyerCreditOutstandingBuyerName { get; set; }
        [BookmarkMapping("BuyerCreditLimitStatus10th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate10_Status { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest10th_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate10_ApprovedCreditLimitLine { get; set; }
        [BookmarkMapping("BuyerARBalance10th_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate10_ARBalance { get; set; }
        [BookmarkMapping("PurchasePercent10th_1")]
        public decimal QCAReqBuyerCreditOutstandingPrivate10_MaxPurchasePct { get; set; }
        [BookmarkMapping("AssignmentCondition10th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate10_AssignmentMethod { get; set; }
        [BookmarkMapping("BillingResponsible10th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate10_BillingResponsibleBy { get; set; }
        [BookmarkMapping("PaymentMethod10th_1")]
        public string QCAReqBuyerCreditOutstandingPrivate10_MethodOfPayment { get; set; }

        [BookmarkMapping("BuyerQtyGovernment")]
        public decimal QCAReqBuyerCreditOutstandingGovCOUNT_CountCAbuyerCreditOutstandingGovernment { get; set; }
        [BookmarkMapping("BuyerName11th_1")]
        public string QCAReqBuyerCreditOutstandingGov1_BuyerCreditOutstandingBuyerName { get; set; }
        [BookmarkMapping("BuyerCreditLimitStatus11th_1")]
        public string QCAReqBuyerCreditOutstandingGov1_Status { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest11th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov1_ApprovedCreditLimitLine { get; set; }
        [BookmarkMapping("BuyerARBalance11th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov1_ARBalance { get; set; }
        [BookmarkMapping("PurchasePercent11th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov1_MaxPurchasePct { get; set; }
        [BookmarkMapping("AssignmentCondition11th_1")]
        public string QCAReqBuyerCreditOutstandingGov1_AssignmentMethod { get; set; }
        [BookmarkMapping("BillingResponsible11th_1")]
        public string QCAReqBuyerCreditOutstandingGov1_BillingResponsibleBy { get; set; }
        [BookmarkMapping("PaymentMethod11th_1")]
        public string QCAReqBuyerCreditOutstandingGov1_MethodOfPayment { get; set; }
        [BookmarkMapping("BuyerName12th_1")]
        public string QCAReqBuyerCreditOutstandingGov2_BuyerCreditOutstandingBuyerName { get; set; }
        [BookmarkMapping("BuyerCreditLimitStatus12th_1")]
        public string QCAReqBuyerCreditOutstandingGov2_Status { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest12th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov2_ApprovedCreditLimitLine { get; set; }
        [BookmarkMapping("BuyerARBalance12th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov2_ARBalance { get; set; }
        [BookmarkMapping("PurchasePercent12th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov2_MaxPurchasePct { get; set; }
        [BookmarkMapping("AssignmentCondition12th_1")]
        public string QCAReqBuyerCreditOutstandingGov2_AssignmentMethod { get; set; }
        [BookmarkMapping("BillingResponsible12th_1")]
        public string QCAReqBuyerCreditOutstandingGov2_BillingResponsibleBy { get; set; }
        [BookmarkMapping("PaymentMethod12th_1")]
        public string QCAReqBuyerCreditOutstandingGov2_MethodOfPayment { get; set; }
        [BookmarkMapping("BuyerName13th_1")]
        public string QCAReqBuyerCreditOutstandingGov3_BuyerCreditOutstandingBuyerName { get; set; }
        [BookmarkMapping("BuyerCreditLimitStatus13th_1")]
        public string QCAReqBuyerCreditOutstandingGov3_Status { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest13th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov3_ApprovedCreditLimitLine { get; set; }
        [BookmarkMapping("BuyerARBalance13th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov3_ARBalance { get; set; }
        [BookmarkMapping("PurchasePercent13th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov3_MaxPurchasePct { get; set; }

        [BookmarkMapping("AssignmentCondition13th_1")]
        public string QCAReqBuyerCreditOutstandingGov3_AssignmentMethod { get; set; }
        [BookmarkMapping("BillingResponsible13th_1")]
        public string QCAReqBuyerCreditOutstandingGov3_BillingResponsibleBy { get; set; }
        [BookmarkMapping("PaymentMethod13th_1")]
        public string QCAReqBuyerCreditOutstandingGov3_MethodOfPayment { get; set; }
        [BookmarkMapping("BuyerName14th_1")]
        public string QCAReqBuyerCreditOutstandingGov4_BuyerCreditOutstandingBuyerName { get; set; }
        [BookmarkMapping("BuyerCreditLimitStatus14th_1")]
        public string QCAReqBuyerCreditOutstandingGov4_Status { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest14th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov4_ApprovedCreditLimitLine { get; set; }
        [BookmarkMapping("BuyerARBalance14th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov4_ARBalance { get; set; }
        [BookmarkMapping("PurchasePercent14th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov4_MaxPurchasePct { get; set; }
        [BookmarkMapping("AssignmentCondition14th_1")]
        public string QCAReqBuyerCreditOutstandingGov4_AssignmentMethod { get; set; }
        [BookmarkMapping("BillingResponsible14th_1")]
        public string QCAReqBuyerCreditOutstandingGov4_BillingResponsibleBy { get; set; }
        [BookmarkMapping("PaymentMethod14th_1")]
        public string QCAReqBuyerCreditOutstandingGov4_MethodOfPayment { get; set; }
        [BookmarkMapping("BuyerName15th_1")]
        public string QCAReqBuyerCreditOutstandingGov5_BuyerCreditOutstandingBuyerName { get; set; }
        [BookmarkMapping("BuyerCreditLimitStatus15th_1")]
        public string QCAReqBuyerCreditOutstandingGov5_Status { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest15th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov5_ApprovedCreditLimitLine { get; set; }
        [BookmarkMapping("BuyerARBalance15th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov5_ARBalance { get; set; }
        [BookmarkMapping("PurchasePercent15th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov5_MaxPurchasePct { get; set; }
        [BookmarkMapping("AssignmentCondition15th_1")]
        public string QCAReqBuyerCreditOutstandingGov5_AssignmentMethod { get; set; }
        [BookmarkMapping("BillingResponsible15th_1")]
        public string QCAReqBuyerCreditOutstandingGov5_BillingResponsibleBy { get; set; }
        [BookmarkMapping("PaymentMethod15th_1")]
        public string QCAReqBuyerCreditOutstandingGov5_MethodOfPayment { get; set; }
        [BookmarkMapping("BuyerName16th_1")]
        public string QCAReqBuyerCreditOutstandingGov6_BuyerCreditOutstandingBuyerName { get; set; }
        [BookmarkMapping("BuyerCreditLimitStatus16th_1")]
        public string QCAReqBuyerCreditOutstandingGov6_Status { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest16th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov6_ApprovedCreditLimitLine { get; set; }
        [BookmarkMapping("BuyerARBalance16th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov6_ARBalance { get; set; }
        [BookmarkMapping("PurchasePercent16th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov6_MaxPurchasePct { get; set; }
        [BookmarkMapping("AssignmentCondition16th_1")]
        public string QCAReqBuyerCreditOutstandingGov6_AssignmentMethod { get; set; }
        [BookmarkMapping("BillingResponsible16th_1")]
        public string QCAReqBuyerCreditOutstandingGov6_BillingResponsibleBy { get; set; }
        [BookmarkMapping("PaymentMethod16th_1")]
        public string QCAReqBuyerCreditOutstandingGov6_MethodOfPayment { get; set; }
        [BookmarkMapping("BuyerName17th_1")]
        public string QCAReqBuyerCreditOutstandingGov7_BuyerCreditOutstandingBuyerName { get; set; }
        [BookmarkMapping("BuyerCreditLimitStatus17th_1")]
        public string QCAReqBuyerCreditOutstandingGov7_Status { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest17th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov7_ApprovedCreditLimitLine { get; set; }
        [BookmarkMapping("BuyerARBalance17th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov7_ARBalance { get; set; }
        [BookmarkMapping("PurchasePercent17th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov7_MaxPurchasePct { get; set; }

        [BookmarkMapping("AssignmentCondition17th_1")]
        public string QCAReqBuyerCreditOutstandingGov7_AssignmentMethod { get; set; }
        [BookmarkMapping("BillingResponsible17th_1")]
        public string QCAReqBuyerCreditOutstandingGov7_BillingResponsibleBy { get; set; }
        [BookmarkMapping("PaymentMethod17th_1")]
        public string QCAReqBuyerCreditOutstandingGov7_MethodOfPayment { get; set; }
        [BookmarkMapping("BuyerName18th_1")]
        public string QCAReqBuyerCreditOutstandingGov8_BuyerCreditOutstandingBuyerName { get; set; }
        [BookmarkMapping("BuyerCreditLimitStatus18th_1")]
        public string QCAReqBuyerCreditOutstandingGov8_Status { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest18th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov8_ApprovedCreditLimitLine { get; set; }
        [BookmarkMapping("BuyerARBalance18th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov8_ARBalance { get; set; }
        [BookmarkMapping("PurchasePercent18th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov8_MaxPurchasePct { get; set; }
        [BookmarkMapping("AssignmentCondition18th_1")]
        public string QCAReqBuyerCreditOutstandingGov8_AssignmentMethod { get; set; }
        [BookmarkMapping("BillingResponsible18th_1")]
        public string QCAReqBuyerCreditOutstandingGov8_BillingResponsibleBy { get; set; }
        [BookmarkMapping("PaymentMethod18th_1")]
        public string QCAReqBuyerCreditOutstandingGov8_MethodOfPayment { get; set; }
        [BookmarkMapping("BuyerName19th_1")]
        public string QCAReqBuyerCreditOutstandingGov9_BuyerCreditOutstandingBuyerName { get; set; }
        [BookmarkMapping("BuyerCreditLimitStatus19th_1")]
        public string QCAReqBuyerCreditOutstandingGov9_Status { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest19th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov9_ApprovedCreditLimitLine { get; set; }
        [BookmarkMapping("BuyerARBalance19th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov9_ARBalance { get; set; }
        [BookmarkMapping("PurchasePercent19th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov9_MaxPurchasePct { get; set; }
        [BookmarkMapping("AssignmentCondition19th_1")]
        public string QCAReqBuyerCreditOutstandingGov9_AssignmentMethod { get; set; }
        [BookmarkMapping("BillingResponsible19th_1")]
        public string QCAReqBuyerCreditOutstandingGov9_BillingResponsibleBy { get; set; }
        [BookmarkMapping("PaymentMethod19th_1")]
        public string QCAReqBuyerCreditOutstandingGov9_MethodOfPayment { get; set; }
        [BookmarkMapping("BuyerName20th_1")]
        public string QCAReqBuyerCreditOutstandingGov10_BuyerCreditOutstandingBuyerName { get; set; }
        [BookmarkMapping("BuyerCreditLimitStatus20th_1")]
        public string QCAReqBuyerCreditOutstandingGov10_Status { get; set; }
        [BookmarkMapping("BuyerCreditLimitRequest20th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov10_ApprovedCreditLimitLine { get; set; }
        [BookmarkMapping("BuyerARBalance20th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov10_ARBalance { get; set; }
        [BookmarkMapping("PurchasePercent20th_1")]
        public decimal QCAReqBuyerCreditOutstandingGov10_MaxPurchasePct { get; set; }
        [BookmarkMapping("AssignmentCondition20th_1")]
        public string QCAReqBuyerCreditOutstandingGov10_AssignmentMethod { get; set; }
        [BookmarkMapping("BillingResponsible20th_1")]
        public string QCAReqBuyerCreditOutstandingGov10_BillingResponsibleBy { get; set; }
        [BookmarkMapping("PaymentMethod20th_1")]
        public string QCAReqBuyerCreditOutstandingGov10_MethodOfPayment { get; set; }
        [BookmarkMapping("AssignmentBuyer1st_1")]
        public string QCreditAppReqAssignmentOutstanding1_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId1st_1")]
        public string QCreditAppReqAssignmentOutstanding1_AssignmentRefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount1st_1")]
        public decimal QCreditAppReqAssignmentOutstanding1_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount1st_1")]
        public decimal QCreditAppReqAssignmentOutstanding1_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate1st_1")]
        public string QCreditAppReqAssignmentOutstanding1_AssignmentAgreementDate { get; set; }

        [BookmarkMapping("AssignmentRemainingAmount1st_1")]
        public decimal QCreditAppReqAssignmentOutstanding1_RemainingAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDescription1st_1")]
        public string QCreditAppReqAssignmentOutstanding1_Description { get; set; }
        [BookmarkMapping("AssignmentBuyer2nd_1")]
        public string QCreditAppReqAssignmentOutstanding2_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId2nd_1")]
        public string QCreditAppReqAssignmentOutstanding2_AssignmentRefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount2nd_1")]
        public decimal QCreditAppReqAssignmentOutstanding2_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount2nd_1")]
        public decimal QCreditAppReqAssignmentOutstanding2_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate2nd_1")]
        public string QCreditAppReqAssignmentOutstanding2_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount2nd_1")]
        public decimal QCreditAppReqAssignmentOutstanding2_RemainingAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDescription2nd_1")]
        public string QCreditAppReqAssignmentOutstanding2_Description { get; set; }
        [BookmarkMapping("AssignmentBuyer3rd_1")]
        public string QCreditAppReqAssignmentOutstanding3_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId3rd_1")]
        public string QCreditAppReqAssignmentOutstanding3_AssignmentRefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount3rd_1")]
        public decimal QCreditAppReqAssignmentOutstanding3_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount3rd_1")]
        public decimal QCreditAppReqAssignmentOutstanding3_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate3rd_1")]
        public string QCreditAppReqAssignmentOutstanding3_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount3rd_1")]
        public decimal QCreditAppReqAssignmentOutstanding3_RemainingAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDescription3rd_1")]
        public string QCreditAppReqAssignmentOutstanding3_Description { get; set; }
        [BookmarkMapping("AssignmentBuyer4th_1")]
        public string QCreditAppReqAssignmentOutstanding4_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId4th_1")]
        public string QCreditAppReqAssignmentOutstanding4_AssignmentRefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount4th_1")]
        public decimal QCreditAppReqAssignmentOutstanding4_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount4th_1")]
        public decimal QCreditAppReqAssignmentOutstanding4_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate4th_1")]
        public string QCreditAppReqAssignmentOutstanding4_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount4th_1")]
        public decimal QCreditAppReqAssignmentOutstanding4_RemainingAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDescription4th_1")]
        public string QCreditAppReqAssignmentOutstanding4_Description { get; set; }
        [BookmarkMapping("AssignmentBuyer5th_1")]
        public string QCreditAppReqAssignmentOutstanding5_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId5th_1")]
        public string QCreditAppReqAssignmentOutstanding5_AssignmentRefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount5th_1")]
        public decimal QCreditAppReqAssignmentOutstanding5_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount5th_1")]
        public decimal QCreditAppReqAssignmentOutstanding5_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate5th_1")]
        public string QCreditAppReqAssignmentOutstanding5_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount5th_1")]
        public decimal QCreditAppReqAssignmentOutstanding5_RemainingAmount { get; set; }

        [BookmarkMapping("AssignmentAgreementDescription5th_1")]
        public string QCreditAppReqAssignmentOutstanding5_Description { get; set; }
        [BookmarkMapping("AssignmentBuyer6th_1")]
        public string QCreditAppReqAssignmentOutstanding6_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId6th_1")]
        public string QCreditAppReqAssignmentOutstanding6_AssignmentRefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount6th_1")]
        public decimal QCreditAppReqAssignmentOutstanding6_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount6th_1")]
        public decimal QCreditAppReqAssignmentOutstanding6_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate6th_1")]
        public string QCreditAppReqAssignmentOutstanding6_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount6th_1")]
        public decimal QCreditAppReqAssignmentOutstanding6_RemainingAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDescription6th_1")]
        public string QCreditAppReqAssignmentOutstanding6_Description { get; set; }
        [BookmarkMapping("AssignmentBuyer7th_1")]
        public string QCreditAppReqAssignmentOutstanding7_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId7th_1")]
        public string QCreditAppReqAssignmentOutstanding7_AssignmentRefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount7th_1")]
        public decimal QCreditAppReqAssignmentOutstanding7_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount7th_1")]
        public decimal QCreditAppReqAssignmentOutstanding7_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate7th_1")]
        public string QCreditAppReqAssignmentOutstanding7_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount7th_1")]
        public decimal QCreditAppReqAssignmentOutstanding7_RemainingAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDescription7th_1")]
        public string QCreditAppReqAssignmentOutstanding7_Description { get; set; }

        [BookmarkMapping("AssignmentBuyer8th_1")]
        public string QCreditAppReqAssignmentOutstanding8_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId8th_1")]
        public string QCreditAppReqAssignmentOutstanding8_AssignmentRefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount8th_1")]
        public decimal QCreditAppReqAssignmentOutstanding8_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount8th_1")]
        public decimal QCreditAppReqAssignmentOutstanding8_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate8th_1")]
        public string QCreditAppReqAssignmentOutstanding8_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount8th_1")]
        public decimal QCreditAppReqAssignmentOutstanding8_RemainingAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDescription8th_1")]
        public string QCreditAppReqAssignmentOutstanding8_Description { get; set; }
        [BookmarkMapping("AssignmentBuyer9th_1")]
        public string QCreditAppReqAssignmentOutstanding9_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId9th_1")]
        public string QCreditAppReqAssignmentOutstanding9_AssignmentRefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount9th_1")]
        public decimal QCreditAppReqAssignmentOutstanding9_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount9th_1")]
        public decimal QCreditAppReqAssignmentOutstanding9_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate9th_1")]
        public string QCreditAppReqAssignmentOutstanding9_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount9th_1")]
        public decimal QCreditAppReqAssignmentOutstanding9_RemainingAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDescription9th_1")]
        public string QCreditAppReqAssignmentOutstanding9_Description { get; set; }
        [BookmarkMapping("AssignmentBuyer10th_1")]
        public string QCreditAppReqAssignmentOutstanding10_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId10th_1")]
        public string QCreditAppReqAssignmentOutstanding10_AssignmentRefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount10th_1")]
        public decimal QCreditAppReqAssignmentOutstanding10_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount10th_1")]
        public decimal QCreditAppReqAssignmentOutstanding10_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate10th_1")]
        public string QCreditAppReqAssignmentOutstanding10_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount10th_1")]
        public decimal QCreditAppReqAssignmentOutstanding10_RemainingAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDescription10th_1")]
        public string QCreditAppReqAssignmentOutstanding10_Description { get; set; }
        [BookmarkMapping("AssignmentBuyer11th_1")]
        public string QCreditAppReqAssignmentOutstanding11_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId11th_1")]
        public string QCreditAppReqAssignmentOutstanding11_AssignmentRefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount11th_1")]
        public decimal QCreditAppReqAssignmentOutstanding11_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount11th_1")]
        public decimal QCreditAppReqAssignmentOutstanding11_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate11th_1")]
        public string QCreditAppReqAssignmentOutstanding11_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount11th_1")]
        public decimal QCreditAppReqAssignmentOutstanding11_RemainingAmount { get; set; }

        [BookmarkMapping("AssignmentAgreementDescription11th_1")]
        public string QCreditAppReqAssignmentOutstanding11_Description { get; set; }
        [BookmarkMapping("AssignmentBuyer12th_1")]
        public string QCreditAppReqAssignmentOutstanding12_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId12th_1")]
        public string QCreditAppReqAssignmentOutstanding12_AssignmentRefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount12th_1")]
        public decimal QCreditAppReqAssignmentOutstanding12_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount12th_1")]
        public decimal QCreditAppReqAssignmentOutstanding12_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate12th_1")]
        public string QCreditAppReqAssignmentOutstanding12_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount12th_1")]
        public decimal QCreditAppReqAssignmentOutstanding12_RemainingAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDescription12th_1")]
        public string QCreditAppReqAssignmentOutstanding12_Description { get; set; }
        [BookmarkMapping("AssignmentBuyer13th_1")]
        public string QCreditAppReqAssignmentOutstanding13_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId13th_1")]
        public string QCreditAppReqAssignmentOutstanding13_AssignmentRefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount13th_1")]
        public decimal QCreditAppReqAssignmentOutstanding13_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount13th_1")]
        public decimal QCreditAppReqAssignmentOutstanding13_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate13th_1")]
        public string QCreditAppReqAssignmentOutstanding13_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount13th_1")]
        public decimal QCreditAppReqAssignmentOutstanding13_RemainingAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDescription13th_1")]
        public string QCreditAppReqAssignmentOutstanding13_Description { get; set; }
        [BookmarkMapping("AssignmentBuyer14th_1")]
        public string QCreditAppReqAssignmentOutstanding14_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId14th_1")]
        public string QCreditAppReqAssignmentOutstanding14_AssignmentRefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount14th_1")]

        public decimal QCreditAppReqAssignmentOutstanding14_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount14th_1")]
        public decimal QCreditAppReqAssignmentOutstanding14_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate14th_1")]
        public string QCreditAppReqAssignmentOutstanding14_AssignmentAgreementDate { get; set; }

        [BookmarkMapping("AssignmentRemainingAmount14th_1")]
        public decimal QCreditAppReqAssignmentOutstanding14_RemainingAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDescription14th_1")]
        public string QCreditAppReqAssignmentOutstanding14_Description { get; set; }
        [BookmarkMapping("AssignmentBuyer15th_1")]
        public string QCreditAppReqAssignmentOutstanding15_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId15th_1")]
        public string QCreditAppReqAssignmentOutstanding15_AssignmentRefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount15th_1")]
        public decimal QCreditAppReqAssignmentOutstanding15_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount15th_1")]
        public decimal QCreditAppReqAssignmentOutstanding15_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate15th_1")]
        public string QCreditAppReqAssignmentOutstanding15_AssignmentAgreementDate { get; set; }

        [BookmarkMapping("AssignmentRemainingAmount15th_1")]
        public decimal QCreditAppReqAssignmentOutstanding15_RemainingAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDescription15th_1")]
        public string QCreditAppReqAssignmentOutstanding15_Description { get; set; }
        [BookmarkMapping("AssignmentBuyer16th_1")]
        public string QCreditAppReqAssignmentOutstanding16_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId16th_1")]
        public string QCreditAppReqAssignmentOutstanding16_AssignmentRefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount16th_1")]
        public decimal QCreditAppReqAssignmentOutstanding16_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount16th_1")]
        public decimal QCreditAppReqAssignmentOutstanding16_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate16th_1")]
        public string QCreditAppReqAssignmentOutstanding16_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount16th_1")]
        public decimal QCreditAppReqAssignmentOutstanding16_RemainingAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDescription16th_1")]
        public string QCreditAppReqAssignmentOutstanding16_Description { get; set; }
        [BookmarkMapping("AssignmentBuyer17th_1")]
        public string QCreditAppReqAssignmentOutstanding17_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId17th_1")]
        public string QCreditAppReqAssignmentOutstanding17_AssignmentRefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount17th_1")]
        public decimal QCreditAppReqAssignmentOutstanding17_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount17th_1")]
        public decimal QCreditAppReqAssignmentOutstanding17_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate17th_1")]
        public string QCreditAppReqAssignmentOutstanding17_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount17th_1")]
        public decimal QCreditAppReqAssignmentOutstanding17_RemainingAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDescription17th_1")]
        public string QCreditAppReqAssignmentOutstanding17_Description { get; set; }
        [BookmarkMapping("AssignmentBuyer18th_1")]
        public string QCreditAppReqAssignmentOutstanding18_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId18th_1")]
        public string QCreditAppReqAssignmentOutstanding18_AssignmentRefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount18th_1")]
        public decimal QCreditAppReqAssignmentOutstanding18_AssignmentBuyerAgreementAmount { get; set; }

        [BookmarkMapping("AssignmentAgreementAmount18th_1")]
        public decimal QCreditAppReqAssignmentOutstanding18_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate18th_1")]
        public string QCreditAppReqAssignmentOutstanding18_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount18th_1")]
        public decimal QCreditAppReqAssignmentOutstanding18_RemainingAmount { get; set; }

        [BookmarkMapping("AssignmentAgreementDescription18th_1")]
        public string QCreditAppReqAssignmentOutstanding18_Description { get; set; }
        [BookmarkMapping("AssignmentBuyer19th_1")]
        public string QCreditAppReqAssignmentOutstanding19_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId19th_1")]
        public string QCreditAppReqAssignmentOutstanding19_AssignmentRefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount19th_1")]
        public decimal QCreditAppReqAssignmentOutstanding19_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount19th_1")]
        public decimal QCreditAppReqAssignmentOutstanding19_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate19th_1")]
        public string QCreditAppReqAssignmentOutstanding19_AssignmentAgreementDate { get; set; }

        [BookmarkMapping("AssignmentRemainingAmount19th_1")]
        public decimal QCreditAppReqAssignmentOutstanding19_RemainingAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDescription19th_1")]
        public string QCreditAppReqAssignmentOutstanding19_Description { get; set; }
        [BookmarkMapping("AssignmentBuyer20th_1")]
        public string QCreditAppReqAssignmentOutstanding20_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId20th_1")]
        public string QCreditAppReqAssignmentOutstanding20_AssignmentRefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount20th_1")]
        public decimal QCreditAppReqAssignmentOutstanding20_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount20th_1")]
        public decimal QCreditAppReqAssignmentOutstanding20_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate20th_1")]
        public string QCreditAppReqAssignmentOutstanding20_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount20th_1")]
        public decimal QCreditAppReqAssignmentOutstanding20_RemainingAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDescription20th_1")]
        public string QCreditAppReqAssignmentOutstanding20_Description { get; set; }

        [BookmarkMapping("AssignmentBuyer21th_1")]
        public string QCreditAppReqAssignmentOutstanding21_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId21th_1")]
        public string QCreditAppReqAssignmentOutstanding21_AssignmentRefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount21th_1")]
        public decimal QCreditAppReqAssignmentOutstanding21_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount21th_1")]
        public decimal QCreditAppReqAssignmentOutstanding21_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate21th_1")]
        public string QCreditAppReqAssignmentOutstanding21_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount21th_1")]
        public decimal QCreditAppReqAssignmentOutstanding21_RemainingAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDescription21th_1")]
        public string QCreditAppReqAssignmentOutstanding21_Description { get; set; }
        [BookmarkMapping("AssignmentBuyer22th_1")]
        public string QCreditAppReqAssignmentOutstanding22_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId22th_1")]
        public string QCreditAppReqAssignmentOutstanding22_AssignmentRefAgreementId { get; set; }

        [BookmarkMapping("AssignmentBuyerAgreementAmount22th_1")]
        public decimal QCreditAppReqAssignmentOutstanding22_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount22th_1")]
        public decimal QCreditAppReqAssignmentOutstanding22_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate22th_1")]
        public string QCreditAppReqAssignmentOutstanding22_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount22th_1")]
        public decimal QCreditAppReqAssignmentOutstanding22_RemainingAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDescription22th_1")]
        public string QCreditAppReqAssignmentOutstanding22_Description { get; set; }

        [BookmarkMapping("AssignmentBuyer23th_1")]
        public string QCreditAppReqAssignmentOutstanding23_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId23th_1")]
        public string QCreditAppReqAssignmentOutstanding23_AssignmentRefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount23th_1")]
        public decimal QCreditAppReqAssignmentOutstanding23_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount23th_1")]
        public decimal QCreditAppReqAssignmentOutstanding23_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate23th_1")]
        public string QCreditAppReqAssignmentOutstanding23_AssignmentAgreementDate { get; set; }

        [BookmarkMapping("AssignmentRemainingAmount23th_1")]
        public decimal QCreditAppReqAssignmentOutstanding23_RemainingAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDescription23th_1")]
        public string QCreditAppReqAssignmentOutstanding23_Description { get; set; }
        [BookmarkMapping("AssignmentBuyer24th_1")]
        public string QCreditAppReqAssignmentOutstanding24_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId24th_1")]
        public string QCreditAppReqAssignmentOutstanding24_AssignmentRefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount24th_1")]
        public decimal QCreditAppReqAssignmentOutstanding24_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount24th_1")]
        public decimal QCreditAppReqAssignmentOutstanding24_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate24th_1")]
        public string QCreditAppReqAssignmentOutstanding24_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount24th_1")]
        public decimal QCreditAppReqAssignmentOutstanding24_RemainingAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDescription24th_1")]
        public string QCreditAppReqAssignmentOutstanding24_Description { get; set; }

        [BookmarkMapping("AssignmentBuyer25th_1")]
        public string QCreditAppReqAssignmentOutstanding25_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId25th_1")]
        public string QCreditAppReqAssignmentOutstanding25_AssignmentRefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount25th_1")]
        public decimal QCreditAppReqAssignmentOutstanding25_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount25th_1")]
        public decimal QCreditAppReqAssignmentOutstanding25_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate25th_1")]
        public string QCreditAppReqAssignmentOutstanding25_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount25th_1")]
        public decimal QCreditAppReqAssignmentOutstanding25_RemainingAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDescription25th_1")]
        public string QCreditAppReqAssignmentOutstanding25_Description { get; set; }
        [BookmarkMapping("AssignmentBuyer26th_1")]
        public string QCreditAppReqAssignmentOutstanding26_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId26th_1")]
        public string QCreditAppReqAssignmentOutstanding26_AssignmentRefAgreementId { get; set; }

        [BookmarkMapping("AssignmentBuyerAgreementAmount26th_1")]
        public decimal QCreditAppReqAssignmentOutstanding26_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount26th_1")]
        public decimal QCreditAppReqAssignmentOutstanding26_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate26th_1")]
        public string QCreditAppReqAssignmentOutstanding26_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount26th_1")]
        public decimal QCreditAppReqAssignmentOutstanding26_RemainingAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDescription26th_1")]
        public string QCreditAppReqAssignmentOutstanding26_Description { get; set; }
        [BookmarkMapping("AssignmentBuyer27th_1")]
        public string QCreditAppReqAssignmentOutstanding27_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId27th_1")]
        public string QCreditAppReqAssignmentOutstanding27_AssignmentRefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount27th_1")]
        public decimal QCreditAppReqAssignmentOutstanding27_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount27th_1")]
        public decimal QCreditAppReqAssignmentOutstanding27_AssignmentAgreementAmount { get; set; }

        [BookmarkMapping("AssignmentAgreementDate27th_1")]
        public string QCreditAppReqAssignmentOutstanding27_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount27th_1")]
        public decimal QCreditAppReqAssignmentOutstanding27_RemainingAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDescription27th_1")]
        public string QCreditAppReqAssignmentOutstanding27_Description { get; set; }
        [BookmarkMapping("AssignmentBuyer28th_1")]
        public string QCreditAppReqAssignmentOutstanding28_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId28th_1")]
        public string QCreditAppReqAssignmentOutstanding28_AssignmentRefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount28th_1")]
        public decimal QCreditAppReqAssignmentOutstanding28_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount28th_1")]
        public decimal QCreditAppReqAssignmentOutstanding28_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate28th_1")]
        public string QCreditAppReqAssignmentOutstanding28_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount28th_1")]
        public decimal QCreditAppReqAssignmentOutstanding28_RemainingAmount { get; set; }

        [BookmarkMapping("AssignmentAgreementDescription28th_1")]
        public string QCreditAppReqAssignmentOutstanding28_Description { get; set; }
        [BookmarkMapping("AssignmentBuyer29th_1")]
        public string QCreditAppReqAssignmentOutstanding29_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementId29th_1")]
        public string QCreditAppReqAssignmentOutstanding29_AssignmentRefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount29th_1")]
        public decimal QCreditAppReqAssignmentOutstanding29_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount29th_1")]
        public decimal QCreditAppReqAssignmentOutstanding29_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate29th_1")]
        public string QCreditAppReqAssignmentOutstanding29_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount29th_1")]
        public decimal QCreditAppReqAssignmentOutstanding29_RemainingAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDescription29th_1")]
        public string QCreditAppReqAssignmentOutstanding29_Description { get; set; }
        [BookmarkMapping("AssignmentBuyer30th_1")]
        public string QCreditAppReqAssignmentOutstanding30_AssignmentBuyer { get; set; }

        [BookmarkMapping("AssignmentRefAgreementId30th_1")]
        public string QCreditAppReqAssignmentOutstanding30_AssignmentRefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmount30th_1")]
        public decimal QCreditAppReqAssignmentOutstanding30_AssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmount30th_1")]
        public decimal QCreditAppReqAssignmentOutstanding30_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDate30th_1")]
        public string QCreditAppReqAssignmentOutstanding30_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmount30th_1")]
        public decimal QCreditAppReqAssignmentOutstanding30_RemainingAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDescription30th_1")]
        public string QCreditAppReqAssignmentOutstanding30_Description { get; set; }
        [BookmarkMapping("SumAssignmentBuyerAgreementAmount")]
        public decimal QSumCreditAppReqAssignment_SumAssignmentBuyerAgreementAmount { get; set; }
        [BookmarkMapping("SumAssignmentAgreementAmount")]
        public decimal QSumCreditAppReqAssignment_SumAssignmentAgreementAmount { get; set; }
        [BookmarkMapping("SumAssignmentRemainingAmount")]
        public decimal QSumCreditAppReqAssignment_SumAssignmentRemainingAmount { get; set; }

        [BookmarkMapping("BusinessCollateralTypeFirst1")]
        public string QCustBusinessCollateral1_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeFirst1")]
        public string QCustBusinessCollateral1_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescFirst1")]
        public string QCustBusinessCollateral1_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueFirst1")]
        public decimal QCustBusinessCollateral1_BusinessCollateralValue { get; set; }
        [BookmarkMapping("BusinessCollateralTypeSecond1")]
        public string QCustBusinessCollateral2_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeSecond1")]
        public string QCustBusinessCollateral2_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescSecond1")]
        public string QCustBusinessCollateral2_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueSecond1")]
        public decimal QCustBusinessCollateral2_BusinessCollateralValue { get; set; }
        [BookmarkMapping("BusinessCollateralTypeThird1")]
        public string QCustBusinessCollateral3_BusinessCollateralType { get; set; }

        [BookmarkMapping("BusinessCollateralSubTypeThird1")]
        public string QCustBusinessCollateral3_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescThird1")]
        public string QCustBusinessCollateral3_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueThird1")]
        public decimal QCustBusinessCollateral3_BusinessCollateralValue { get; set; }
        [BookmarkMapping("BusinessCollateralTypeFourth1")]
        public string QCustBusinessCollateral4_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeFourth1")]
        public string QCustBusinessCollateral4_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescFourth1")]
        public string QCustBusinessCollateral4_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueFourth1")]
        public decimal QCustBusinessCollateral4_BusinessCollateralValue { get; set; }
        [BookmarkMapping("BusinessCollateralTypeFifth1")]
        public string QCustBusinessCollateral5_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeFifth1")]
        public string QCustBusinessCollateral5_BusinessCollateralSubType { get; set; }

        [BookmarkMapping("BusinessCollateralDescFifth1")]
        public string QCustBusinessCollateral5_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueFifth1")]
        public decimal QCustBusinessCollateral5_BusinessCollateralValue { get; set; }
        [BookmarkMapping("BusinessCollateralTypeSixth1")]
        public string QCustBusinessCollateral6_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeSixth1")]
        public string QCustBusinessCollateral6_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescSixth1")]
        public string QCustBusinessCollateral6_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueSixth1")]
        public decimal QCustBusinessCollateral6_BusinessCollateralValue { get; set; }
        [BookmarkMapping("BusinessCollateralTypeSeventh1")]
        public string QCustBusinessCollateral7_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeSeventh1")]
        public string QCustBusinessCollateral7_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescSeventh1")]
        public string QCustBusinessCollateral7_Description { get; set; }

        [BookmarkMapping("BusinessCollateralValueSeventh1")]
        public decimal QCustBusinessCollateral7_BusinessCollateralValue { get; set; }
        [BookmarkMapping("BusinessCollateralTypeEighth1")]
        public string QCustBusinessCollateral8_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeEighth1")]
        public string QCustBusinessCollateral8_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescEighth1")]
        public string QCustBusinessCollateral8_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueEighth1")]
        public decimal QCustBusinessCollateral8_BusinessCollateralValue { get; set; }
        [BookmarkMapping("BusinessCollateralTypeNineth1")]
        public string QCustBusinessCollateral9_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeNineth1")]
        public string QCustBusinessCollateral9_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescNineth1")]
        public string QCustBusinessCollateral9_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueNineth1")]
        public decimal QCustBusinessCollateral9_BusinessCollateralValue { get; set; }

        [BookmarkMapping("BusinessCollateralTypeTenth1")]
        public string QCustBusinessCollateral10_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeTenth1")]
        public string QCustBusinessCollateral10_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescTenth1")]
        public string QCustBusinessCollateral10_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueTenth1")]
        public decimal QCustBusinessCollateral10_BusinessCollateralValue { get; set; }
        [BookmarkMapping("BusinessCollateralTypeEleventh1")]
        public string QCustBusinessCollateral11_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeEleventh1")]
        public string QCustBusinessCollateral11_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescEleventh1")]
        public string QCustBusinessCollateral11_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueEleventh1")]
        public decimal QCustBusinessCollateral11_BusinessCollateralValue { get; set; }
        [BookmarkMapping("BusinessCollateralTypeTwelfth1")]
        public string QCustBusinessCollateral12_BusinessCollateralType { get; set; }

        [BookmarkMapping("BusinessCollateralSubTypeTwelfth1")]
        public string QCustBusinessCollateral12_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescTwelfth1")]
        public string QCustBusinessCollateral12_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueTwelfth1")]
        public decimal QCustBusinessCollateral12_BusinessCollateralValue { get; set; }
        [BookmarkMapping("BusinessCollateralTypeThirteenth1")]
        public string QCustBusinessCollateral13_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeThirteenth1")]
        public string QCustBusinessCollateral13_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescThirteenth1")]
        public string QCustBusinessCollateral13_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueThirteenth1")]
        public decimal QCustBusinessCollateral13_BusinessCollateralValue { get; set; }
        [BookmarkMapping("BusinessCollateralTypeFourteenth1")]
        public string QCustBusinessCollateral14_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeFourteenth1")]

        public string QCustBusinessCollateral14_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescFourteenth1")]
        public string QCustBusinessCollateral14_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueFourteenth1")]
        public decimal QCustBusinessCollateral14_BusinessCollateralValue { get; set; }
        [BookmarkMapping("BusinessCollateralTypeFifteenth1")]
        public string QCustBusinessCollateral15_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeFifteenth1")]
        public string QCustBusinessCollateral15_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescFifteenth1")]
        public string QCustBusinessCollateral15_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueFifteenth1")]
        public decimal QCustBusinessCollateral15_BusinessCollateralValue { get; set; }
        [BookmarkMapping("BusinessCollateralTypeSixteenth1")]
        public string QCustBusinessCollateral16_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeSixteenth1")]
        public string QCustBusinessCollateral16_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescSixteenth1")]
        public string QCustBusinessCollateral16_Description { get; set; }

        [BookmarkMapping("BusinessCollateralValueSixteenth1")]
        public decimal QCustBusinessCollateral16_BusinessCollateralValue { get; set; }
        [BookmarkMapping("BusinessCollateralTypeSeventeenth1")]
        public string QCustBusinessCollateral17_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeSeventeenth1")]
        public string QCustBusinessCollateral17_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescSeventeenth1")]
        public string QCustBusinessCollateral17_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueSeventeenth1")]
        public decimal QCustBusinessCollateral17_BusinessCollateralValue { get; set; }
        [BookmarkMapping("BusinessCollateralTypeEighteenth1")]
        public string QCustBusinessCollateral18_BusinessCollateralType { get; set; }

        [BookmarkMapping("BusinessCollateralSubTypeEighteenth1")]
        public string QCustBusinessCollateral18_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescEighteenth1")]
        public string QCustBusinessCollateral18_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueEighteenth1")]
        public decimal QCustBusinessCollateral18_BusinessCollateralValue { get; set; }

        [BookmarkMapping("BusinessCollateralTypeNineteenth1")]
        public string QCustBusinessCollateral19_BusinessCollateralType { get; set; }

        [BookmarkMapping("BusinessCollateralSubTypeNineteenth1")]
        public string QCustBusinessCollateral19_BusinessCollateralSubType { get; set; }

        [BookmarkMapping("BusinessCollateralDescNineteenth1")]
        public string QCustBusinessCollateral19_Description { get; set; }

        [BookmarkMapping("BusinessCollateralValueNineteenth1")]
        public decimal QCustBusinessCollateral19_BusinessCollateralValue { get; set; }

        [BookmarkMapping("BusinessCollateralTypeTwentyth1")]
        public string QCustBusinessCollateral20_BusinessCollateralType { get; set; }

        [BookmarkMapping("BusinessCollateralSubTypeTwentyth1")]
        public string QCustBusinessCollateral20_BusinessCollateralSubType { get; set; }

        [BookmarkMapping("BusinessCollateralDescTwentyth1")]
        public string QCustBusinessCollateral20_Description { get; set; }

        [BookmarkMapping("BusinessCollateralValueTwentyth1")]
        public decimal QCustBusinessCollateral20_BusinessCollateralValue { get; set; }

        [BookmarkMapping("BusinessCollateralTypeTwentyFirstSt1")]
        public string QCustBusinessCollateral21_BusinessCollateralType { get; set; }

        [BookmarkMapping("BusinessCollateralSubTypeTwentyFirstSt1")]
        public string QCustBusinessCollateral21_BusinessCollateralSubType { get; set; }

        [BookmarkMapping("BusinessCollateralDescTwentyFirstSt1")]
        public string QCustBusinessCollateral21_Description { get; set; }

        [BookmarkMapping("BusinessCollateralValueTwentyFirstSt1")]
        public decimal QCustBusinessCollateral21_BusinessCollateralValue { get; set; }

        [BookmarkMapping("BusinessCollateralTypeTwentySecondNd1")]
        public string QCustBusinessCollateral22_BusinessCollateralType { get; set; }

        [BookmarkMapping("BusinessCollateralSubTypeTwentySecondNd1")]
        public string QCustBusinessCollateral22_BusinessCollateralSubType { get; set; }

        [BookmarkMapping("BusinessCollateralDescTwentySecondNd1")]
        public string QCustBusinessCollateral22_Description { get; set; }

        [BookmarkMapping("BusinessCollateralValueTwentySecondNd1")]
        public decimal QCustBusinessCollateral22_BusinessCollateralValue { get; set; }

        [BookmarkMapping("BusinessCollateralTypeTwentyThirdRd1")]
        public string QCustBusinessCollateral23_BusinessCollateralType { get; set; }

        [BookmarkMapping("BusinessCollateralSubTypeTwentyThirdRd1")]
        public string QCustBusinessCollateral23_BusinessCollateralSubType { get; set; }

        [BookmarkMapping("BusinessCollateralDescTwentyThirdRd1")]
        public string QCustBusinessCollateral23_Description { get; set; }

        [BookmarkMapping("BusinessCollateralValueTwentyThirdRd1")]
        public decimal QCustBusinessCollateral23_BusinessCollateralValue { get; set; }

        [BookmarkMapping("BusinessCollateralTypeTwentyFourTh1")]
        public string QCustBusinessCollateral24_BusinessCollateralType { get; set; }

        [BookmarkMapping("BusinessCollateralSubTypeTwentyFourTh1")]
        public string QCustBusinessCollateral24_BusinessCollateralSubType { get; set; }

        [BookmarkMapping("BusinessCollateralDescTwentyFourTh1")]
        public string QCustBusinessCollateral24_Description { get; set; }

        [BookmarkMapping("BusinessCollateralValueTwentyFourTh1")]
        public decimal QCustBusinessCollateral24_BusinessCollateralValue { get; set; }

        [BookmarkMapping("BusinessCollateralTypeTwentyFifTh1")]
        public string QCustBusinessCollateral25_BusinessCollateralType { get; set; }

        [BookmarkMapping("BusinessCollateralSubTypeTwentyFifTh1")]
        public string QCustBusinessCollateral25_BusinessCollateralSubType { get; set; }

        [BookmarkMapping("BusinessCollateralDescTwentyFifTh1")]
        public string QCustBusinessCollateral25_Description { get; set; }

        [BookmarkMapping("BusinessCollateralValueTwentyFifTh1")]
        public decimal QCustBusinessCollateral25_BusinessCollateralValue { get; set; }

        [BookmarkMapping("BusinessCollateralTypeTwentySixTh1")]
        public string QCustBusinessCollateral26_BusinessCollateralType { get; set; }

        [BookmarkMapping("BusinessCollateralSubTypeTwentySixTh1")]
        public string QCustBusinessCollateral26_BusinessCollateralSubType { get; set; }

        [BookmarkMapping("BusinessCollateralDescTwentySixTh1")]
        public string QCustBusinessCollateral26_Description { get; set; }

        [BookmarkMapping("BusinessCollateralValueTwentySixTh1")]
        public decimal QCustBusinessCollateral26_BusinessCollateralValue { get; set; }

        [BookmarkMapping("BusinessCollateralTypeTwentySevenTh1")]
        public string QCustBusinessCollateral27_BusinessCollateralType { get; set; }

        [BookmarkMapping("BusinessCollateralSubTypeTwentySevenTh1")]
        public string QCustBusinessCollateral27_BusinessCollateralSubType { get; set; }

        [BookmarkMapping("BusinessCollateralDescTwentySevenTh1")]
        public string QCustBusinessCollateral27_Description { get; set; }

        [BookmarkMapping("BusinessCollateralValueTwentySevenTh1")]
        public decimal QCustBusinessCollateral27_BusinessCollateralValue { get; set; }

        [BookmarkMapping("BusinessCollateralTypeTwentyEighTh1")]
        public string QCustBusinessCollateral28_BusinessCollateralType { get; set; }

        [BookmarkMapping("BusinessCollateralSubTypeTwentyEighTh1")]
        public string QCustBusinessCollateral28_BusinessCollateralSubType { get; set; }

        [BookmarkMapping("BusinessCollateralDescTwentyEighTh1")]
        public string QCustBusinessCollateral28_Description { get; set; }

        [BookmarkMapping("BusinessCollateralValueTwentyEighTh1")]
        public decimal QCustBusinessCollateral28_BusinessCollateralValue { get; set; }

        [BookmarkMapping("BusinessCollateralTypeTwentyNineTh1")]
        public string QCustBusinessCollateral29_BusinessCollateralType { get; set; }

        [BookmarkMapping("BusinessCollateralSubTypeTwentyNineTh1")]
        public string QCustBusinessCollateral29_BusinessCollateralSubType { get; set; }

        [BookmarkMapping("BusinessCollateralDescTwentyNineTh1")]
        public string QCustBusinessCollateral29_Description { get; set; }

        [BookmarkMapping("BusinessCollateralValueTwentyNineTh1")]
        public decimal QCustBusinessCollateral29_BusinessCollateralValue { get; set; }

        [BookmarkMapping("BusinessCollateralTypeThirtyTh1")]
        public string QCustBusinessCollateral30_BusinessCollateralType { get; set; }

        [BookmarkMapping("BusinessCollateralSubTypeThirtyTh1")]
        public string QCustBusinessCollateral30_BusinessCollateralSubType { get; set; }

        [BookmarkMapping("BusinessCollateralDescThirtyTh1")]
        public string QCustBusinessCollateral30_Description { get; set; }

        [BookmarkMapping("BusinessCollateralValueThirtyTh1")]
        public decimal QCustBusinessCollateral30_BusinessCollateralValue { get; set; }

        [BookmarkMapping("SumBusinessCollateralValue")]
        public decimal QSumCustBusinessCollateral_SumBusinessCollateralValue { get; set; }

        [BookmarkMapping("CreditBueroCheckDate")]
        public string AppRequestTable_NCBCheckedDate { get; set; }

        [BookmarkMapping("FinInstitution1st_1")]
        public string QCAReqNCBTrans1_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType1st_1")]
        public string QCAReqNCBTrans1_CreditType { get; set; }

        [BookmarkMapping("LoanLimit1st_1")]
        public decimal QCAReqNCBTrans1_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding1st_1")]
        public decimal QCAReqNCBTrans1_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment1st_1")]
        public decimal QCAReqNCBTrans1_MonthlyRepayment { get; set; }

        [BookmarkMapping("LoanDueDate1st_1")]
        public string QCAReqNCBTrans1_EndDate { get; set; }

        [BookmarkMapping("LoanStatus1st_1")]
        public string QCAReqNCBTrans1_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution2nd_1")]
        public string QCAReqNCBTrans2_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType2nd_1")]
        public string QCAReqNCBTrans2_CreditType { get; set; }

        [BookmarkMapping("LoanLimit2nd_1")]
        public decimal QCAReqNCBTrans2_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding2nd_1")]
        public decimal QCAReqNCBTrans2_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment2nd_1")]
        public decimal QCAReqNCBTrans2_MonthlyRepayment { get; set; }

        [BookmarkMapping("LoanDueDate2nd_1")]
        public string QCAReqNCBTrans2_EndDate { get; set; }

        [BookmarkMapping("LoanStatus2nd_1")]
        public string QCAReqNCBTrans2_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution3rd_1")]
        public string QCAReqNCBTrans3_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType3rd_1")]
        public string QCAReqNCBTrans3_CreditType { get; set; }

        [BookmarkMapping("LoanLimit3rd_1")]
        public decimal QCAReqNCBTrans3_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding3rd_1")]
        public decimal QCAReqNCBTrans3_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment3rd_1")]
        public decimal QCAReqNCBTrans3_MonthlyRepayment { get; set; }

        [BookmarkMapping("LoanDueDate3rd_1")]
        public string QCAReqNCBTrans3_EndDate { get; set; }

        [BookmarkMapping("LoanStatus3rd_1")]
        public string QCAReqNCBTrans3_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution4th_1")]
        public string QCAReqNCBTrans4_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType4th_1")]
        public string QCAReqNCBTrans4_CreditType { get; set; }

        [BookmarkMapping("LoanLimit4th_1")]
        public decimal QCAReqNCBTrans4_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding4th_1")]
        public decimal QCAReqNCBTrans4_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment4th_1")]
        public decimal QCAReqNCBTrans4_MonthlyRepayment { get; set; }

        [BookmarkMapping("LoanDueDate4th_1")]
        public string QCAReqNCBTrans4_EndDate { get; set; }

        [BookmarkMapping("LoanStatus4th_1")]
        public string QCAReqNCBTrans4_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution5th_1")]
        public string QCAReqNCBTrans5_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType5th_1")]
        public string QCAReqNCBTrans5_CreditType { get; set; }

        [BookmarkMapping("LoanLimit5th_1")]
        public decimal QCAReqNCBTrans5_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding5th_1")]
        public decimal QCAReqNCBTrans5_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment5th_1")]
        public decimal QCAReqNCBTrans5_MonthlyRepayment { get; set; }

        [BookmarkMapping("LoanDueDate5th_1")]
        public string QCAReqNCBTrans5_EndDate { get; set; }

        [BookmarkMapping("LoanStatus5th_1")]
        public string QCAReqNCBTrans5_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution6th_1")]
        public string QCAReqNCBTrans6_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType6th_1")]
        public string QCAReqNCBTrans6_CreditType { get; set; }

        [BookmarkMapping("LoanLimit6th_1")]
        public decimal QCAReqNCBTrans6_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding6th_1")]
        public decimal QCAReqNCBTrans6_OutstandingAR { get; set; }
        [BookmarkMapping("LoanMonthlyRepayment6th_1")]
        public decimal QCAReqNCBTrans6_MonthlyRepayment { get; set; }
        

        [BookmarkMapping("LoanDueDate6th_1")]
        public string QCAReqNCBTrans6_EndDate { get; set; }

        [BookmarkMapping("LoanStatus6th_1")]
        public string QCAReqNCBTrans6_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution7th_1")]
        public string QCAReqNCBTrans7_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType7th_1")]
        public string QCAReqNCBTrans7_CreditType { get; set; }

        [BookmarkMapping("LoanLimit7th_1")]
        public decimal QCAReqNCBTrans7_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding7th_1")]
        public decimal QCAReqNCBTrans7_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment7th_1")]
        public decimal QCAReqNCBTrans7_MonthlyRepayment { get; set; }

        [BookmarkMapping("LoanDueDate7th_1")]
        public string QCAReqNCBTrans7_EndDate { get; set; }

        [BookmarkMapping("LoanStatus7th_1")]
        public string QCAReqNCBTrans7_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution8th_1")]
        public string QCAReqNCBTrans8_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType8th_1")]
        public string QCAReqNCBTrans8_CreditType { get; set; }

        [BookmarkMapping("LoanLimit8th_1")]
        public decimal QCAReqNCBTrans8_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding8th_1")]
        public decimal QCAReqNCBTrans8_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment8th_1")]
        public decimal QCAReqNCBTrans8_MonthlyRepayment { get; set; }

        [BookmarkMapping("LoanDueDate8th_1")]
        public string QCAReqNCBTrans8_EndDate { get; set; }

        [BookmarkMapping("LoanStatus8th_1")]
        public string QCAReqNCBTrans8_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution9th_1")]
        public string QCAReqNCBTrans9_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType9th_1")]
        public string QCAReqNCBTrans9_CreditType { get; set; }

        [BookmarkMapping("LoanLimit9th_1")]
        public decimal QCAReqNCBTrans9_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding9th_1")]
        public decimal QCAReqNCBTrans9_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment9th_1")]
        public decimal QCAReqNCBTrans9_MonthlyRepayment { get; set; }

        [BookmarkMapping("LoanDueDate9th_1")]
        public string QCAReqNCBTrans9_EndDate { get; set; }

        [BookmarkMapping("LoanStatus9th_1")]
        public string QCAReqNCBTrans9_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution10th_1")]
        public string QCAReqNCBTrans10_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType10th_1")]
        public string QCAReqNCBTrans10_CreditType { get; set; }

        [BookmarkMapping("LoanLimit10th_1")]
        public decimal QCAReqNCBTrans10_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding10th_1")]
        public decimal QCAReqNCBTrans10_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment10th_1")]
        public decimal QCAReqNCBTrans10_MonthlyRepayment { get; set; }

        [BookmarkMapping("LoanDueDate10th_1")]
        public string QCAReqNCBTrans10_EndDate { get; set; }

        [BookmarkMapping("LoanStatus10th_1")]
        public string QCAReqNCBTrans10_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution11th_1")]
        public string QCAReqNCBTrans11_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType11th_1")]
        public string QCAReqNCBTrans11_CreditType { get; set; }

        [BookmarkMapping("LoanLimit11th_1")]
        public decimal QCAReqNCBTrans11_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding11th_1")]
        public decimal QCAReqNCBTrans11_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment11th_1")]
        public decimal QCAReqNCBTrans11_MonthlyRepayment { get; set; }

        [BookmarkMapping("LoanDueDate11th_1")]
        public string QCAReqNCBTrans11_EndDate { get; set; }

        [BookmarkMapping("LoanStatus11th_1")]
        public string QCAReqNCBTrans11_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution12th_1")]
        public string QCAReqNCBTrans12_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType12th_1")]
        public string QCAReqNCBTrans12_CreditType { get; set; }

        [BookmarkMapping("LoanLimit12th_1")]
        public decimal QCAReqNCBTrans12_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding12th_1")]
        public decimal QCAReqNCBTrans12_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment12th_1")]
        public decimal QCAReqNCBTrans12_MonthlyRepayment { get; set; }

        [BookmarkMapping("LoanDueDate12th_1")]
        public string QCAReqNCBTrans12_EndDate { get; set; }

        [BookmarkMapping("LoanStatus12th_1")]
        public string QCAReqNCBTrans12_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution13th_1")]
        public string QCAReqNCBTrans13_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType13th_1")]
        public string QCAReqNCBTrans13_CreditType { get; set; }

        [BookmarkMapping("LoanLimit13th_1")]
        public decimal QCAReqNCBTrans13_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding13th_1")]
        public decimal QCAReqNCBTrans13_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment13th_1")]
        public decimal QCAReqNCBTrans13_MonthlyRepayment { get; set; }

        [BookmarkMapping("LoanDueDate13th_1")]
        public string QCAReqNCBTrans13_EndDate { get; set; }

        [BookmarkMapping("LoanStatus13th_1")]
        public string QCAReqNCBTrans13_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution14th_1")]
        public string QCAReqNCBTrans14_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType14th_1")]
        public string QCAReqNCBTrans14_CreditType { get; set; }

        [BookmarkMapping("LoanLimit14th_1")]
        public decimal QCAReqNCBTrans14_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding14th_1")]
        public decimal QCAReqNCBTrans14_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment14th_1")]
        public decimal QCAReqNCBTrans14_MonthlyRepayment { get; set; }

        [BookmarkMapping("LoanDueDate14th_1")]
        public string QCAReqNCBTrans14_EndDate { get; set; }

        [BookmarkMapping("LoanStatus14th_1")]
        public string QCAReqNCBTrans14_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution15th_1")]
        public string QCAReqNCBTrans15_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType15th_1")]
        public string QCAReqNCBTrans15_CreditType { get; set; }

        [BookmarkMapping("LoanLimit15th_1")]
        public decimal QCAReqNCBTrans15_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding15th_1")]
        public decimal QCAReqNCBTrans15_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment15th_1")]
        public decimal QCAReqNCBTrans15_MonthlyRepayment { get; set; }

        [BookmarkMapping("LoanDueDate15th_1")]
        public string QCAReqNCBTrans15_EndDate { get; set; }

        [BookmarkMapping("LoanStatus15th_1")]
        public string QCAReqNCBTrans15_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution16th_1")]
        public string QCAReqNCBTrans16_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType16th_1")]
        public string QCAReqNCBTrans16_CreditType { get; set; }

        [BookmarkMapping("LoanLimit16th_1")]
        public decimal QCAReqNCBTrans16_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding16th_1")]
        public decimal QCAReqNCBTrans16_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment16th_1")]
        public decimal QCAReqNCBTrans16_MonthlyRepayment { get; set; }
        [BookmarkMapping("LoanDueDate16th_1")]
        public string QCAReqNCBTrans16_EndDate { get; set; }

        [BookmarkMapping("LoanStatus16th_1")]
        public string QCAReqNCBTrans16_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution17th_1")]
        public string QCAReqNCBTrans17_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType17th_1")]
        public string QCAReqNCBTrans17_CreditType { get; set; }

        [BookmarkMapping("LoanLimit17th_1")]
        public decimal QCAReqNCBTrans17_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding17th_1")]
        public decimal QCAReqNCBTrans17_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment17th_1")]
        public decimal QCAReqNCBTrans17_MonthlyRepayment { get; set; }

        [BookmarkMapping("LoanDueDate17th_1")]
        public string QCAReqNCBTrans17_EndDate { get; set; }

        [BookmarkMapping("LoanStatus17th_1")]
        public string QCAReqNCBTrans17_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution18th_1")]
        public string QCAReqNCBTrans18_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType18th_1")]
        public string QCAReqNCBTrans18_CreditType { get; set; }

        [BookmarkMapping("LoanLimit18th_1")]
        public decimal QCAReqNCBTrans18_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding18th_1")]
        public decimal QCAReqNCBTrans18_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment18th_1")]
        public decimal QCAReqNCBTrans18_MonthlyRepayment { get; set; }

        [BookmarkMapping("LoanDueDate18th_1")]
        public string QCAReqNCBTrans18_EndDate { get; set; }

        [BookmarkMapping("LoanStatus18th_1")]
        public string QCAReqNCBTrans18_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution19th_1")]
        public string QCAReqNCBTrans19_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType19th_1")]
        public string QCAReqNCBTrans19_CreditType { get; set; }

        [BookmarkMapping("LoanLimit19th_1")]
        public decimal QCAReqNCBTrans19_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding19th_1")]
        public decimal QCAReqNCBTrans19_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment19th_1")]
        public decimal QCAReqNCBTrans19_MonthlyRepayment { get; set; }

        [BookmarkMapping("LoanDueDate19th_1")]
        public string QCAReqNCBTrans19_EndDate { get; set; }

        [BookmarkMapping("LoanStatus19th_1")]
        public string QCAReqNCBTrans19_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution20th_1")]
        public string QCAReqNCBTrans20_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType20th_1")]
        public string QCAReqNCBTrans20_CreditType { get; set; }

        [BookmarkMapping("LoanLimit20th_1")]
        public decimal QCAReqNCBTrans20_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding20th_1")]
        public decimal QCAReqNCBTrans20_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment20th_1")]
        public decimal QCAReqNCBTrans20_MonthlyRepayment { get; set; }

        [BookmarkMapping("LoanDueDate20th_1")]
        public string QCAReqNCBTrans20_EndDate { get; set; }

        [BookmarkMapping("LoanStatus20th_1")]
        public string QCAReqNCBTrans20_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution21th_1")]
        public string QCAReqNCBTrans21_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType21th_1")]
        public string QCAReqNCBTrans21_CreditType { get; set; }

        [BookmarkMapping("LoanLimit21th_1")]
        public decimal QCAReqNCBTrans21_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding21th_1")]
        public decimal QCAReqNCBTrans21_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment21th_1")]
        public decimal QCAReqNCBTrans21_MonthlyRepayment { get; set; }

        [BookmarkMapping("LoanDueDate21th_1")]
        public string QCAReqNCBTrans21_EndDate { get; set; }

        [BookmarkMapping("LoanStatus21th_1")]
        public string QCAReqNCBTrans21_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution22th_1")]
        public string QCAReqNCBTrans22_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType22th_1")]
        public string QCAReqNCBTrans22_CreditType { get; set; }

        [BookmarkMapping("LoanLimit22th_1")]
        public decimal QCAReqNCBTrans22_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding22th_1")]
        public decimal QCAReqNCBTrans22_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment22th_1")]
        public decimal QCAReqNCBTrans22_MonthlyRepayment { get; set; }

        [BookmarkMapping("LoanDueDate22th_1")]
        public string QCAReqNCBTrans22_EndDate { get; set; }

        [BookmarkMapping("LoanStatus22th_1")]
        public string QCAReqNCBTrans22_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution23th_1")]
        public string QCAReqNCBTrans23_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType23th_1")]
        public string QCAReqNCBTrans23_CreditType { get; set; }

        [BookmarkMapping("LoanLimit23th_1")]
        public decimal QCAReqNCBTrans23_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding23th_1")]
        public decimal QCAReqNCBTrans23_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment23th_1")]
        public decimal QCAReqNCBTrans23_MonthlyRepayment { get; set; }

        [BookmarkMapping("LoanDueDate23th_1")]
        public string QCAReqNCBTrans23_EndDate { get; set; }

        [BookmarkMapping("LoanStatus23th_1")]
        public string QCAReqNCBTrans23_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution24th_1")]
        public string QCAReqNCBTrans24_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType24th_1")]
        public string QCAReqNCBTrans24_CreditType { get; set; }

        [BookmarkMapping("LoanLimit24th_1")]
        public decimal QCAReqNCBTrans24_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding24th_1")]
        public decimal QCAReqNCBTrans24_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment24th_1")]
        public decimal QCAReqNCBTrans24_MonthlyRepayment { get; set; }

        [BookmarkMapping("LoanDueDate24th_1")]
        public string QCAReqNCBTrans24_EndDate { get; set; }

        [BookmarkMapping("LoanStatus24th_1")]
        public string QCAReqNCBTrans24_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution25th_1")]
        public string QCAReqNCBTrans25_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType25th_1")]
        public string QCAReqNCBTrans25_CreditType { get; set; }

        [BookmarkMapping("LoanLimit25th_1")]
        public decimal QCAReqNCBTrans25_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding25th_1")]
        public decimal QCAReqNCBTrans25_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment25th_1")]
        public decimal QCAReqNCBTrans25_MonthlyRepayment { get; set; }

        [BookmarkMapping("LoanDueDate25th_1")]
        public string QCAReqNCBTrans25_EndDate { get; set; }

        [BookmarkMapping("LoanStatus25th_1")]
        public string QCAReqNCBTrans25_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution26th_1")]
        public string QCAReqNCBTrans26_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType26th_1")]
        public string QCAReqNCBTrans26_CreditType { get; set; }

        [BookmarkMapping("LoanLimit26th_1")]
        public decimal QCAReqNCBTrans26_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding26th_1")]
        public decimal QCAReqNCBTrans26_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment26th_1")]
        public decimal QCAReqNCBTrans26_MonthlyRepayment { get; set; }

        [BookmarkMapping("LoanDueDate26th_1")]
        public string QCAReqNCBTrans26_EndDate { get; set; }

        [BookmarkMapping("LoanStatus26th_1")]
        public string QCAReqNCBTrans26_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution27th_1")]
        public string QCAReqNCBTrans27_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType27th_1")]
        public string QCAReqNCBTrans27_CreditType { get; set; }

        [BookmarkMapping("LoanLimit27th_1")]
        public decimal QCAReqNCBTrans27_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding27th_1")]
        public decimal QCAReqNCBTrans27_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment27th_1")]
        public decimal QCAReqNCBTrans27_MonthlyRepayment { get; set; }

        [BookmarkMapping("LoanDueDate27th_1")]
        public string QCAReqNCBTrans27_EndDate { get; set; }

        [BookmarkMapping("LoanStatus27th_1")]
        public string QCAReqNCBTrans27_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution28th_1")]
        public string QCAReqNCBTrans28_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType28th_1")]
        public string QCAReqNCBTrans28_CreditType { get; set; }

        [BookmarkMapping("LoanLimit28th_1")]
        public decimal QCAReqNCBTrans28_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding28th_1")]
        public decimal QCAReqNCBTrans28_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment28th_1")]
        public decimal QCAReqNCBTrans28_MonthlyRepayment { get; set; }

        [BookmarkMapping("LoanDueDate28th_1")]
        public string QCAReqNCBTrans28_EndDate { get; set; }

        [BookmarkMapping("LoanStatus28th_1")]
        public string QCAReqNCBTrans28_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution29th_1")]
        public string QCAReqNCBTrans29_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType29th_1")]
        public string QCAReqNCBTrans29_CreditType { get; set; }

        [BookmarkMapping("LoanLimit29th_1")]
        public decimal QCAReqNCBTrans29_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding29th_1")]
        public decimal QCAReqNCBTrans29_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment29th_1")]
        public decimal QCAReqNCBTrans29_MonthlyRepayment { get; set; }

        [BookmarkMapping("LoanDueDate29th_1")]
        public string QCAReqNCBTrans29_EndDate { get; set; }

        [BookmarkMapping("LoanStatus29th_1")]
        public string QCAReqNCBTrans29_NCBAccountStatus { get; set; }

        [BookmarkMapping("FinInstitution30th_1")]
        public string QCAReqNCBTrans30_FinancialInstitution { get; set; }

        [BookmarkMapping("LoanType30th_1")]
        public string QCAReqNCBTrans30_CreditType { get; set; }

        [BookmarkMapping("LoanLimit30th_1")]
        public decimal QCAReqNCBTrans30_CreditLimit { get; set; }

        [BookmarkMapping("LoanOutstanding30th_1")]
        public decimal QCAReqNCBTrans30_OutstandingAR { get; set; }

        [BookmarkMapping("LoanMonthlyRepayment30th_1")]
        public decimal QCAReqNCBTrans30_MonthlyRepayment { get; set; }

        [BookmarkMapping("LoanDueDate30th_1")]
        public string QCAReqNCBTrans30_EndDate { get; set; }

        [BookmarkMapping("LoanStatus30th_1")]
        public string QCAReqNCBTrans30_NCBAccountStatus { get; set; }

        [BookmarkMapping("SumLoanLimit")]
        public decimal QCAReqNCBTransSum_SumCreditLimit { get; set; }

        [BookmarkMapping("SumLoanOutstanding")]
        public decimal QCAReqNCBTransSum_SumOutstandingAR { get; set; }

        [BookmarkMapping("SumLoanMonthlyRepayment")]
        public decimal QCAReqNCBTransSum_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedPersonName1st_2")]
        public string QAuthorizedPersonTransForNCB1_AuthorizedPersonName { get; set; }

        [BookmarkMapping("CustAuthorizedCreditBueroCheckDate1st_1")]
        public string QAuthorizedPersonTransForNCB1_NCBCheckedDate { get; set; }

        [BookmarkMapping("CustAuthorizedFinInstitutionFirst_1st_1")]
        public string QAuthorizedNCB11_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeFirst_1st_1")]
        public string QAuthorizedNCB11_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitFirst_1st_1")]
        public decimal QAuthorizedNCB11_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizedLoanOutstandingFirst_1st_1")]
        public decimal QAuthorizedNCB11_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepayFirst_1st_1")]
        public decimal QAuthorizedNCB11_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateFirst_1st_1")]
        public string QAuthorizedNCB11_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusFirst_1st_1")]
        public string QAuthorizedNCB11_NCBAccountStatus { get; set; }

        [BookmarkMapping("CustAuthorizedFinInstitutionSecond_1st_1")]
        public string QAuthorizedNCB12_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeSecond_1st_1")]
        public string QAuthorizedNCB12_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitSecond_1st_1")]
        public decimal QAuthorizedNCB12_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizLoanOutStandSecond_1st_1")]
        public decimal QAuthorizedNCB12_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepaySecond_1st_1")]
        public decimal QAuthorizedNCB12_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateSecond_1st_1")]
        public string QAuthorizedNCB12_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusSecond_1st_1")]
        public string QAuthorizedNCB12_NCBAccountStatus { get; set; }

        [BookmarkMapping("CustAuthorizedFinInstitutionThird_1st_1")]
        public string QAuthorizedNCB13_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeThird_1st_1")]
        public string QAuthorizedNCB13_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitThird_1st_1")]
        public decimal QAuthorizedNCB13_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizedLoanOutstandingThird_1st_1")]
        public decimal QAuthorizedNCB13_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepayThird_1st_1")]
        public decimal QAuthorizedNCB13_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateThird_1st_1")]
        public string QAuthorizedNCB13_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusThird_1st_1")]
        public string QAuthorizedNCB13_NCBAccountStatus { get; set; }

        [BookmarkMapping("CustAuthorizedFinInstitutionFourth_1st_1")]
        public string QAuthorizedNCB14_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeFourth_1st_1")]
        public string QAuthorizedNCB14_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitFourth_1st_1")]
        public decimal QAuthorizedNCB14_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizLoanOutStandFourth_1st_1")]
        public decimal QAuthorizedNCB14_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepayFourth_1st_1")]
        public decimal QAuthorizedNCB14_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateFourth_1st_1")]
        public string QAuthorizedNCB14_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusFourth_1st_1")]
        public string QAuthorizedNCB14_NCBAccountStatus { get; set; }

        [BookmarkMapping("CustAuthorizedFinInstitutionFifth_1st_1")]
        public string QAuthorizedNCB15_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeFifth_1st_1")]
        public string QAuthorizedNCB15_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitFifth_1st_1")]
        public decimal QAuthorizedNCB15_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizedLoanOutstandingFifth_1st_1")]
        public decimal QAuthorizedNCB15_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepayFifth_1st_1")]
        public decimal QAuthorizedNCB15_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateFifth_1st_1")]
        public string QAuthorizedNCB15_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusFifth_1st_1")]
        public string QAuthorizedNCB15_NCBAccountStatus { get; set; }

        [BookmarkMapping("CustAuthorizedFinInstitutionSixth_1st_1")]
        public string QAuthorizedNCB16_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeSixth_1st_1")]
        public string QAuthorizedNCB16_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitSixth_1st_1")]
        public decimal QAuthorizedNCB16_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizedLoanOutstandingSixth_1st_1")]
        public decimal QAuthorizedNCB16_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepaySixth_1st_1")]
        public decimal QAuthorizedNCB16_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateSixth_1st_1")]
        public string QAuthorizedNCB16_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusSixth_1st_1")]
        public string QAuthorizedNCB16_NCBAccountStatus { get; set; }

        [BookmarkMapping("CustAuthorizFinInstitutionSeventh_1st_1")]
        public string QAuthorizedNCB17_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeSeventh_1st_1")]
        public string QAuthorizedNCB17_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitSeventh_1st_1")]
        public decimal QAuthorizedNCB17_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizLoanOutStandSeventh_1st_1")]
        public decimal QAuthorizedNCB17_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepaySeventh_1st_1")]
        public decimal QAuthorizedNCB17_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateSeventh_1st_1")]
        public string QAuthorizedNCB17_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusSeventh_1st_1")]
        public string QAuthorizedNCB17_NCBAccountStatus { get; set; }

        [BookmarkMapping("CustAuthorizedFinInstitutionEighth_1st_1")]
        public string QAuthorizedNCB18_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeEighth_1st_1")]
        public string QAuthorizedNCB18_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitEighth_1st_1")]
        public decimal QAuthorizedNCB18_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizLoanOutStandEighth_1st_1")]
        public decimal QAuthorizedNCB18_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepayEighth_1st_1")]
        public decimal QAuthorizedNCB18_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateEighth_1st_1")]
        public string QAuthorizedNCB18_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusEighth_1st_1")]
        public string QAuthorizedNCB18_NCBAccountStatus { get; set; }

        [BookmarkMapping("CustAuthorizedFinInstitutionNineth_1st_1")]
        public string QAuthorizedNCB19_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeNineth_1st_1")]
        public string QAuthorizedNCB19_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitNineth_1st_1")]
        public decimal QAuthorizedNCB19_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizLoanOutStandNineth_1st_1")]
        public decimal QAuthorizedNCB19_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepayNineth_1st_1")]
        public decimal QAuthorizedNCB19_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateNineth_1st_1")]
        public string QAuthorizedNCB19_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusNineth_1st_1")]
        public string QAuthorizedNCB19_NCBAccountStatus { get; set; }

        [BookmarkMapping("CustAuthorizedFinInstitutionTenth_1st_1")]
        public string QAuthorizedNCB110_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeTenth_1st_1")]
        public string QAuthorizedNCB110_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitTenth_1st_1")]
        public decimal QAuthorizedNCB110_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizedLoanOutstandingTenth_1st_1")]
        public decimal QAuthorizedNCB110_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepayTenth_1st_1")]
        public decimal QAuthorizedNCB110_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateTenth_1st_1")]
        public string QAuthorizedNCB110_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusTenth_1st_1")]
        public string QAuthorizedNCB110_NCBAccountStatus { get; set; }

        [BookmarkMapping("SumLoanLimit1st_1")]
        public decimal QSumAuthorizedNCB1_SumCreditLimit { get; set; }

        [BookmarkMapping("SumLoanOutstanding1st_1")]
        public decimal QSumAuthorizedNCB1_SumOutstandingAR { get; set; }

        [BookmarkMapping("SumLoanMonthlyRepayment1st_1")]
        public decimal QSumAuthorizedNCB1_SumMonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedPersonName2nd_2")]
        public string QAuthorizedPersonTransForNCB2_AuthorizedPersonName { get; set; }

        [BookmarkMapping("CustAuthorizedCreditBueroCheckDate2nd_1")]
        public string QAuthorizedPersonTransForNCB2_NCBCheckedDate { get; set; }

        [BookmarkMapping("CustAuthorizedFinInstitutionFirst_2nd_1")]
        public string QAuthorizedNCB21_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeFirst_2nd_1")]
        public string QAuthorizedNCB21_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitFirst_2nd_1")]
        public decimal QAuthorizedNCB21_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizedLoanOutstandingFirst_2nd_1")]
        public decimal QAuthorizedNCB21_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepayFirst_2nd_1")]
        public decimal QAuthorizedNCB21_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateFirst_2nd_1")]
        public string QAuthorizedNCB21_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusFirst_2nd_1")]
        public string QAuthorizedNCB21_NCBAccountStatus { get; set; }

        [BookmarkMapping("CustAuthorizedFinInstitutionSecond_2nd_1")]
        public string QAuthorizedNCB22_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeSecond_2nd_1")]
        public string QAuthorizedNCB22_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitSecond_2nd_1")]
        public decimal QAuthorizedNCB22_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizLoanOutStandSecond_2nd_1")]
        public decimal QAuthorizedNCB22_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepaySecond_2nd_1")]
        public decimal QAuthorizedNCB22_MonthlyRepayment { get; set; }
        [BookmarkMapping("CustAuthorizedLoanDueDateSecond_2nd_1")]
        public string QAuthorizedNCB22_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusSecond_2nd_1")]
        public string QAuthorizedNCB22_NCBAccountStatus { get; set; }

        [BookmarkMapping("CustAuthorizedFinInstitutionThird_2nd_1")]
        public string QAuthorizedNCB23_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeThird_2nd_1")]
        public string QAuthorizedNCB23_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitThird_2nd_1")]
        public decimal QAuthorizedNCB23_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizedLoanOutstandingThird_2nd_1")]
        public decimal QAuthorizedNCB23_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepayThird_2nd_1")]
        public decimal QAuthorizedNCB23_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateThird_2nd_1")]
        public string QAuthorizedNCB23_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusThird_2nd_1")]
        public string QAuthorizedNCB23_NCBAccountStatus { get; set; }

        [BookmarkMapping("CustAuthorizedFinInstitutionFourth_2nd_1")]
        public string QAuthorizedNCB24_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeFourth_2nd_1")]
        public string QAuthorizedNCB24_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitFourth_2nd_1")]
        public decimal QAuthorizedNCB24_CreditLimit { get; set; }
        [BookmarkMapping("CustAuthorizLoanOutStandFourth_2nd_1")]
        public decimal QAuthorizedNCB24_OutstandingAR { get; set; }
        [BookmarkMapping("CustAuthorizLoanMonthRepayFourth_2nd_1")]
        public decimal QAuthorizedNCB24_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateFourth_2nd_1")]
        public string QAuthorizedNCB24_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusFourth_2nd_1")]
        public string QAuthorizedNCB24_NCBAccountStatus { get; set; }

        [BookmarkMapping("CustAuthorizedFinInstitutionFifth_2nd_1")]
        public string QAuthorizedNCB25_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeFifth_2nd_1")]
        public string QAuthorizedNCB25_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitFifth_2nd_1")]
        public decimal QAuthorizedNCB25_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizedLoanOutstandingFifth_2nd_1")]
        public decimal QAuthorizedNCB25_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepayFifth_2nd_1")]
        public decimal QAuthorizedNCB25_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateFifth_2nd_1")]
        public string QAuthorizedNCB25_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusFifth_2nd_1")]
        public string QAuthorizedNCB25_NCBAccountStatus { get; set; }

        [BookmarkMapping("CustAuthorizedFinInstitutionSixth_2nd_1")]
        public string QAuthorizedNCB26_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeSixth_2nd_1")]
        public string QAuthorizedNCB26_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitSixth_2nd_1")]
        public decimal QAuthorizedNCB26_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizedLoanOutstandingSixth_2nd_1")]
        public decimal QAuthorizedNCB26_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepaySixth_2nd_1")]
        public decimal QAuthorizedNCB26_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateSixth_2nd_1")]
        public string QAuthorizedNCB26_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusSixth_2nd_1")]
        public string QAuthorizedNCB26_NCBAccountStatus { get; set; }

        [BookmarkMapping("CustAuthorizFinInstitutionSeventh_2nd_1")]
        public string QAuthorizedNCB27_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeSeventh_2nd_1")]
        public string QAuthorizedNCB27_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitSeventh_2nd_1")]
        public decimal QAuthorizedNCB27_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizLoanOutStandSeventh_2nd_1")]
        public decimal QAuthorizedNCB27_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepaySeventh_2nd_1")]
        public decimal QAuthorizedNCB27_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateSeventh_2nd_1")]
        public string QAuthorizedNCB27_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusSeventh_2nd_1")]
        public string QAuthorizedNCB27_NCBAccountStatus { get; set; }

        [BookmarkMapping("CustAuthorizedFinInstitutionEighth_2nd_1")]
        public string QAuthorizedNCB28_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeEighth_2nd_1")]
        public string QAuthorizedNCB28_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitEighth_2nd_1")]
        public decimal QAuthorizedNCB28_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizLoanOutStandEighth_2nd_1")]
        public decimal QAuthorizedNCB28_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepayEighth_2nd_1")]
        public decimal QAuthorizedNCB28_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateEighth_2nd_1")]
        public string QAuthorizedNCB28_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusEighth_2nd_1")]
        public string QAuthorizedNCB28_NCBAccountStatus { get; set; }

        [BookmarkMapping("CustAuthorizedFinInstitutionNineth_2nd_1")]
        public string QAuthorizedNCB29_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeNineth_2nd_1")]
        public string QAuthorizedNCB29_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitNineth_2nd_1")]
        public decimal QAuthorizedNCB29_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizLoanOutStandNineth_2nd_1")]
        public decimal QAuthorizedNCB29_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepayNineth_2nd_1")]
        public decimal QAuthorizedNCB29_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateNineth_2nd_1")]
        public string QAuthorizedNCB29_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusNineth_2nd_1")]
        public string QAuthorizedNCB29_NCBAccountStatus { get; set; }

        [BookmarkMapping("CustAuthorizedFinInstitutionTenth_2nd_1")]
        public string QAuthorizedNCB210_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeTenth_2nd_1")]
        public string QAuthorizedNCB210_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitTenth_2nd_1")]
        public decimal QAuthorizedNCB210_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizedLoanOutstandingTenth_2nd_1")]
        public decimal QAuthorizedNCB210_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepayTenth_2nd_1")]
        public decimal QAuthorizedNCB210_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateTenth_2nd_1")]
        public string QAuthorizedNCB210_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusTenth_2nd_1")]
        public string QAuthorizedNCB210_NCBAccountStatus { get; set; }

        [BookmarkMapping("SumLoanLimit2nd_1")]
        public decimal QSumAuthorizedNCB2_SumCreditLimit { get; set; }

        [BookmarkMapping("SumLoanOutstanding2nd_1")]
        public decimal QSumAuthorizedNCB2_SumOutstandingAR { get; set; }

        [BookmarkMapping("SumLoanMonthlyRepayment2nd_1")]
        public decimal QSumAuthorizedNCB2_SumMonthlyRepayment { get; set; }
        
        [BookmarkMapping("CustAuthorizedPersonName3rd_2")]
        public string QAuthorizedPersonTransForNCB3_AuthorizedPersonName { get; set; }

        [BookmarkMapping("CustAuthorizedCreditBueroCheckDate3rd_1")]
        public string QAuthorizedPersonTransForNCB3_NCBCheckedDate { get; set; }

        [BookmarkMapping("CustAuthorizedFinInstitutionFirst_3rd_1")]
        public string QAuthorizedNCB31_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeFirst_3rd_1")]
        public string QAuthorizedNCB31_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitFirst_3rd_1")]
        public decimal QAuthorizedNCB31_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizedLoanOutstandingFirst_3rd_1")]
        public decimal QAuthorizedNCB31_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepayFirst_3rd_1")]
        public decimal QAuthorizedNCB31_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateFirst_3rd_1")]
        public string QAuthorizedNCB31_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusFirst_3rd_1")]
        public string QAuthorizedNCB31_NCBAccountStatus { get; set; }

        [BookmarkMapping("CustAuthorizedFinInstitutionSecond_3rd_1")]
        public string QAuthorizedNCB32_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeSecond_3rd_1")]
        public string QAuthorizedNCB32_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitSecond_3rd_1")]
        public decimal QAuthorizedNCB32_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizLoanOutStandSecond_3rd_1")]
        public decimal QAuthorizedNCB32_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepaySecond_3rd_1")]
        public decimal QAuthorizedNCB32_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateSecond_3rd_1")]
        public string QAuthorizedNCB32_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusSecond_3rd_1")]
        public string QAuthorizedNCB32_NCBAccountStatus { get; set; }

        [BookmarkMapping("CustAuthorizedFinInstitutionThird_3rd_1")]
        public string QAuthorizedNCB33_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeThird_3rd_1")]
        public string QAuthorizedNCB33_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitThird_3rd_1")]
        public decimal QAuthorizedNCB33_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizedLoanOutstandingThird_3rd_1")]
        public decimal QAuthorizedNCB33_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepayThird_3rd_1")]
        public decimal QAuthorizedNCB33_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateThird_3rd_1")]
        public string QAuthorizedNCB33_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusThird_3rd_1")]
        public string QAuthorizedNCB33_NCBAccountStatus { get; set; }

        [BookmarkMapping("CustAuthorizedFinInstitutionFourth_3rd_1")]
        public string QAuthorizedNCB34_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeFourth_3rd_1")]
        public string QAuthorizedNCB34_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitFourth_3rd_1")]
        public decimal QAuthorizedNCB34_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizLoanOutStandFourth_3rd_1")]
        public decimal QAuthorizedNCB34_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepayFourth_3rd_1")]
        public decimal QAuthorizedNCB34_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateFourth_3rd_1")]
        public string QAuthorizedNCB34_EndDate { get; set; }
        [BookmarkMapping("CustAuthorizedLoanStatusFourth_3rd_1")]
        public string QAuthorizedNCB34_NCBAccountStatus { get; set; }

        [BookmarkMapping("CustAuthorizedFinInstitutionFifth_3rd_1")]
        public string QAuthorizedNCB35_FinancialInstitution { get; set; }
        [BookmarkMapping("CustAuthorizedLoanTypeFifth_3rd_1")]
        public string QAuthorizedNCB35_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitFifth_3rd_1")]
        public decimal QAuthorizedNCB35_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizedLoanOutstandingFifth_3rd_1")]
        public decimal QAuthorizedNCB35_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepayFifth_3rd_1")]
        public decimal QAuthorizedNCB35_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateFifth_3rd_1")]
        public string QAuthorizedNCB35_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusFifth_3rd_1")]
        public string QAuthorizedNCB35_NCBAccountStatus { get; set; }

        [BookmarkMapping("CustAuthorizedFinInstitutionSixth_3rd_1")]
        public string QAuthorizedNCB36_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeSixth_3rd_1")]
        public string QAuthorizedNCB36_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitSixth_3rd_1")]
        public decimal QAuthorizedNCB36_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizedLoanOutstandingSixth_3rd_1")]
        public decimal QAuthorizedNCB36_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepaySixth_3rd_1")]
        public decimal QAuthorizedNCB36_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateSixth_3rd_1")]
        public string QAuthorizedNCB36_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusSixth_3rd_1")]
        public string QAuthorizedNCB36_NCBAccountStatus { get; set; }

        [BookmarkMapping("CustAuthorizFinInstitutionSeventh_3rd_1")]
        public string QAuthorizedNCB37_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeSeventh_3rd_1")]
        public string QAuthorizedNCB37_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitSeventh_3rd_1")]
        public decimal QAuthorizedNCB37_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizLoanOutStandSeventh_3rd_1")]
        public decimal QAuthorizedNCB37_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepaySeventh_3rd_1")]
        public decimal QAuthorizedNCB37_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateSeventh_3rd_1")]
        public string QAuthorizedNCB37_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusSeventh_3rd_1")]
        public string QAuthorizedNCB37_NCBAccountStatus { get; set; }

        [BookmarkMapping("CustAuthorizedFinInstitutionEighth_3rd_1")]
        public string QAuthorizedNCB38_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeEighth_3rd_1")]
        public string QAuthorizedNCB38_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitEighth_3rd_1")]
        public decimal QAuthorizedNCB38_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizLoanOutStandEighth_3rd_1")]
        public decimal QAuthorizedNCB38_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepayEighth_3rd_1")]
        public decimal QAuthorizedNCB38_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateEighth_3rd_1")]
        public string QAuthorizedNCB38_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusEighth_3rd_1")]
        public string QAuthorizedNCB38_NCBAccountStatus { get; set; }

        [BookmarkMapping("CustAuthorizedFinInstitutionNineth_3rd_1")]
        public string QAuthorizedNCB39_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeNineth_3rd_1")]
        public string QAuthorizedNCB39_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitNineth_3rd_1")]
        public decimal QAuthorizedNCB39_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizLoanOutStandNineth_3rd_1")]
        public decimal QAuthorizedNCB39_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepayNineth_3rd_1")]
        public decimal QAuthorizedNCB39_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateNineth_3rd_1")]
        public string QAuthorizedNCB39_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusNineth_3rd_1")]
        public string QAuthorizedNCB39_NCBAccountStatus { get; set; }

        [BookmarkMapping("CustAuthorizedFinInstitutionTenth_3rd_1")]
        public string QAuthorizedNCB310_FinancialInstitution { get; set; }

        [BookmarkMapping("CustAuthorizedLoanTypeTenth_3rd_1")]
        public string QAuthorizedNCB310_CreditType { get; set; }

        [BookmarkMapping("CustAuthorizedLoanLimitTenth_3rd_1")]
        public decimal QAuthorizedNCB310_CreditLimit { get; set; }

        [BookmarkMapping("CustAuthorizedLoanOutstandingTenth_3rd_1")]
        public decimal QAuthorizedNCB310_OutstandingAR { get; set; }

        [BookmarkMapping("CustAuthorizLoanMonthRepayTenth_3rd_1")]
        public decimal QAuthorizedNCB310_MonthlyRepayment { get; set; }

        [BookmarkMapping("CustAuthorizedLoanDueDateTenth_3rd_1")]
        public string QAuthorizedNCB310_EndDate { get; set; }

        [BookmarkMapping("CustAuthorizedLoanStatusTenth_3rd_1")]
        public string QAuthorizedNCB310_NCBAccountStatus { get; set; }

        [BookmarkMapping("SumLoanLimit3rd_1")]
        public decimal QSumAuthorizedNCB3_SumCreditLimit { get; set; }

        [BookmarkMapping("SumLoanOutstanding3rd_1")]
        public decimal QSumAuthorizedNCB3_SumOutstandingAR { get; set; }

        [BookmarkMapping("SumLoanMonthlyRepayment3rd_1")]
        public decimal QSumAuthorizedNCB3_SumMonthlyRepayment { get; set; }

        [BookmarkMapping("ProjectCompanyName1st_1")]
        public string QProjectReferenceTrans1_ProjectCompanyName { get; set; }

        [BookmarkMapping("ProjectName1st_1")]
        public string QProjectReferenceTrans1_ProjectName { get; set; }


        [BookmarkMapping("ProjectDetails1st_1")]
        public string QProjectReferenceTrans1_Remark { get; set; }

        [BookmarkMapping("ProjectStatus1st_1")]
        public string QProjectReferenceTrans1_ProjectStatus { get; set; }

        [BookmarkMapping("ProjectValue1st_1")]
        public decimal QProjectReferenceTrans1_ProjectValue { get; set; }

        [BookmarkMapping("ProjectCompletionYear1st_1")]
        public string QProjectReferenceTrans1_ProjectCompletion { get; set; }

        [BookmarkMapping("ProjectCompanyName2nd_1")]
        public string QProjectReferenceTrans2_ProjectCompanyName { get; set; }

        [BookmarkMapping("ProjectName2nd_1")]
        public string QProjectReferenceTrans2_ProjectName { get; set; }

        [BookmarkMapping("ProjectDetails2nd_1")]
        public string QProjectReferenceTrans2_Remark { get; set; }

        [BookmarkMapping("ProjectStatus2nd_1")]
        public string QProjectReferenceTrans2_ProjectStatus { get; set; }

        [BookmarkMapping("ProjectValue2nd_1")]
        public decimal QProjectReferenceTrans2_ProjectValue { get; set; }

        [BookmarkMapping("ProjectCompletionYear2nd_1")]
        public string QProjectReferenceTrans2_ProjectCompletion { get; set; }

        [BookmarkMapping("ProjectCompanyName3rd_1")]
        public string QProjectReferenceTrans3_ProjectCompanyName { get; set; }

        [BookmarkMapping("ProjectName3rd_1")]
        public string QProjectReferenceTrans3_ProjectName { get; set; }

        [BookmarkMapping("ProjectDetails3rd_1")]
        public string QProjectReferenceTrans3_Remark { get; set; }

        [BookmarkMapping("ProjectStatus3rd_1")]
        public string QProjectReferenceTrans3_ProjectStatus { get; set; }

        [BookmarkMapping("ProjectValue3rd_1")]
        public decimal QProjectReferenceTrans3_ProjectValue { get; set; }

        [BookmarkMapping("ProjectCompletionYear3rd_1")]
        public string QProjectReferenceTrans3_ProjectCompletion { get; set; }

        [BookmarkMapping("ProjectCompanyName4th_1")]
        public string QProjectReferenceTrans4_ProjectCompanyName { get; set; }

        [BookmarkMapping("ProjectName4th_1")]
        public string QProjectReferenceTrans4_ProjectName { get; set; }

        [BookmarkMapping("ProjectDetails4th_1")]
        public string QProjectReferenceTrans4_Remark { get; set; }

        [BookmarkMapping("ProjectStatus4th_1")]
        public string QProjectReferenceTrans4_ProjectStatus { get; set; }

        [BookmarkMapping("ProjectValue4th_1")]
        public decimal QProjectReferenceTrans4_ProjectValue { get; set; }

        [BookmarkMapping("ProjectCompletionYear4th_1")]
        public string QProjectReferenceTrans4_ProjectCompletion { get; set; }

        [BookmarkMapping("ProjectCompanyName5th_1")]
        public string QProjectReferenceTrans5_ProjectCompanyName { get; set; }

        [BookmarkMapping("ProjectName5th_1")]
        public string QProjectReferenceTrans5_ProjectName { get; set; }

        [BookmarkMapping("ProjectDetails5th_1")]
        public string QProjectReferenceTrans5_Remark { get; set; }

        [BookmarkMapping("ProjectStatus5th_1")]
        public string QProjectReferenceTrans5_ProjectStatus { get; set; }

        [BookmarkMapping("ProjectValue5th_1")]
        public decimal QProjectReferenceTrans5_ProjectValue { get; set; }

        [BookmarkMapping("ProjectCompletionYear5th_1")]
        public string QProjectReferenceTrans5_ProjectCompletion { get; set; }

        [BookmarkMapping("ProjectCompanyName6th_1")]
        public string QProjectReferenceTrans6_ProjectCompanyName { get; set; }

        [BookmarkMapping("ProjectName6th_1")]
        public string QProjectReferenceTrans6_ProjectName { get; set; }

        [BookmarkMapping("ProjectDetails6th_1")]
        public string QProjectReferenceTrans6_Remark { get; set; }

        [BookmarkMapping("ProjectStatus6th_1")]
        public string QProjectReferenceTrans6_ProjectStatus { get; set; }

        [BookmarkMapping("ProjectValue6th_1")]
        public decimal QProjectReferenceTrans6_ProjectValue { get; set; }

        [BookmarkMapping("ProjectCompletionYear6th_1")]
        public string QProjectReferenceTrans6_ProjectCompletion { get; set; }

        [BookmarkMapping("ProjectCompanyName7th_1")]
        public string QProjectReferenceTrans7_ProjectCompanyName { get; set; }

        [BookmarkMapping("ProjectName7th_1")]
        public string QProjectReferenceTrans7_ProjectName { get; set; }

        [BookmarkMapping("ProjectDetails7th_1")]
        public string QProjectReferenceTrans7_Remark { get; set; }

        [BookmarkMapping("ProjectStatus7th_1")]
        public string QProjectReferenceTrans7_ProjectStatus { get; set; }

        [BookmarkMapping("ProjectValue7th_1")]
        public decimal QProjectReferenceTrans7_ProjectValue { get; set; }

        [BookmarkMapping("ProjectCompletionYear7th_1")]
        public string QProjectReferenceTrans7_ProjectCompletion { get; set; }

        [BookmarkMapping("ProjectCompanyName8th_1")]
        public string QProjectReferenceTrans8_ProjectCompanyName { get; set; }

        [BookmarkMapping("ProjectName8th_1")]
        public string QProjectReferenceTrans8_ProjectName { get; set; }

        [BookmarkMapping("ProjectDetails8th_1")]
        public string QProjectReferenceTrans8_Remark { get; set; }

        [BookmarkMapping("ProjectStatus8th_1")]
        public string QProjectReferenceTrans8_ProjectStatus { get; set; }

        [BookmarkMapping("ProjectValue8th_1")]
        public decimal QProjectReferenceTrans8_ProjectValue { get; set; }

        [BookmarkMapping("ProjectCompletionYear8th_1")]
        public string QProjectReferenceTrans8_ProjectCompletion { get; set; }

        [BookmarkMapping("ProjectCompanyName9th_1")]
        public string QProjectReferenceTrans9_ProjectCompanyName { get; set; }

        [BookmarkMapping("ProjectName9th_1")]
        public string QProjectReferenceTrans9_ProjectName { get; set; }

        [BookmarkMapping("ProjectDetails9th_1")]
        public string QProjectReferenceTrans9_Remark { get; set; }

        [BookmarkMapping("ProjectStatus9th_1")]
        public string QProjectReferenceTrans9_ProjectStatus { get; set; }

        [BookmarkMapping("ProjectValue9th_1")]
        public decimal QProjectReferenceTrans9_ProjectValue { get; set; }

        [BookmarkMapping("ProjectCompletionYear9th_1")]
        public string QProjectReferenceTrans9_ProjectCompletion { get; set; }

        [BookmarkMapping("ProjectCompanyName10th_1")]
        public string QProjectReferenceTrans10_ProjectCompanyName { get; set; }

        [BookmarkMapping("ProjectName10th_1")]
        public string QProjectReferenceTrans10_ProjectName { get; set; }

        [BookmarkMapping("ProjectDetails10th_1")]
        public string QProjectReferenceTrans10_Remark { get; set; }

        [BookmarkMapping("ProjectStatus10th_1")]
        public string QProjectReferenceTrans10_ProjectStatus { get; set; }

        [BookmarkMapping("ProjectValue10th_1")]
        public decimal QProjectReferenceTrans10_ProjectValue { get; set; }
        [BookmarkMapping("ProjectCompletionYear10th_1")]
        public string QProjectReferenceTrans10_ProjectCompletion { get; set; }
        [BookmarkMapping("AllBuyerCoRemainingAmount_R01")]
        public decimal QCAReqBuyerCreditOutstandingPrivateSum_SumARBalance { get; set; }
        [BookmarkMapping("AllBuyerGoverRemainingAmount_R01")]
        public decimal QCAReqBuyerCreditOutstandingGovSum_SumARBalance { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_1")]
        public string QCustBusinessCollateral1_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_2")]
        public string QCustBusinessCollateral2_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_3")]
        public string QCustBusinessCollateral3_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_4")]
        public string QCustBusinessCollateral4_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_5")]
        public string QCustBusinessCollateral5_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_6")]
        public string QCustBusinessCollateral6_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_7")]
        public string QCustBusinessCollateral7_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_8")]
        public string QCustBusinessCollateral8_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_9")]
        public string QCustBusinessCollateral9_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_10")]
        public string QCustBusinessCollateral10_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_11")]
        public string QCustBusinessCollateral11_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_12")]
        public string QCustBusinessCollateral12_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_13")]
        public string QCustBusinessCollateral13_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_14")]
        public string QCustBusinessCollateral14_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_15")]
        public string QCustBusinessCollateral15_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_16")]
        public string QCustBusinessCollateral16_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_17")]
        public string QCustBusinessCollateral17_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_18")]
        public string QCustBusinessCollateral18_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_19")]
        public string QCustBusinessCollateral19_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_20")]
        public string QCustBusinessCollateral20_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_21")]
        public string QCustBusinessCollateral21_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_22")]
        public string QCustBusinessCollateral22_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_23")]
        public string QCustBusinessCollateral23_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_24")]
        public string QCustBusinessCollateral24_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_25")]
        public string QCustBusinessCollateral25_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_26")]
        public string QCustBusinessCollateral26_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_27")]
        public string QCustBusinessCollateral27_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_28")]
        public string QCustBusinessCollateral28_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_29")]
        public string QCustBusinessCollateral29_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("BusinessCollateralStatus_30")]
        public string QCustBusinessCollateral30_BusinessCollateralStatus { get; set; }
        [BookmarkMapping("CrditLimitAmtStart1")]
        public decimal QCAReqCreditOutStanding1_ApprovedCreditLimit { get; set; }
        [BookmarkMapping("AROutstandingAmtStart")]
        public decimal QCAReqCreditOutStanding1_ARBalance { get; set; }
        [BookmarkMapping("CreditLimitRemainingStart1")]
        public decimal QCAReqCreditOutStanding1_CreditLimitBalance { get; set; }
        [BookmarkMapping("ReserveOutstandingStart1")]
        public decimal QCAReqCreditOutStanding1_ReserveToBeRefund { get; set; }
        [BookmarkMapping("RetentionOutstandingStart1")]
        public decimal QCAReqCreditOutStanding1_AccumRetentionAmount { get; set; }
        [BookmarkMapping("FactoringPurchaseCondition")]
        public string Variable_FactoringPurchaseCondition { get; set; }
        [BookmarkMapping("AssignmentCondition")]
        public string Variable_AssignmentCondition { get; set; }
    }
    public class QCAReqBuyerCreditOutstandingGovSum
    {
        public decimal SumARBalance { get; set; }
        public string BuyerTableGUID { get; set; }
        public string BusinessSegmentGUID { get; set; }
        public string BusinessSegmentType { get; set; }

    }
    public class QCAReqBuyerCreditOutstandingPrivateSum
    {
        public decimal SumARBalance { get; set; }
        public string BuyerTableGUID { get; set; }
        public string BusinessSegmentGUID { get; set; }
        public string BusinessSegmentType { get; set; }
    }
    public class QQCreditAppRequestTable
    {
        public string CreditAppRequestId { get; set; }
        public string RefCreditAppTableGUID { get; set; }
        public string OriginalCreditAppTableId { get; set; }
        public string OriginalPurchaseWithdrawalCondition { get; set; }
        public string OriginalAssignmentMethodGUID { get; set; }
        public string OriginalAssignmentMethodTable { get; set; }
        public string DocumentStatusGUID { get; set; }
        public string Status { get; set; }
        public string RequestDate { get; set; }
        public string KYCSetupGUID { get; set; }
        public string KYC { get; set; }
        public string CreditScoringGUID { get; set; }
        public string CreditScoring { get; set; }
        public string Description { get; set; }
        public string DocumentReasonGUID { get; set; }
        public string Purpose { get; set; }
        public string Remark { get; set; }
        public string RegisteredAddressGUID { get; set; }
        public string RegisteredAddress { get; set; }
        public string BusinessTypeGUID { get; set; }
        public string BusinessType { get; set; }
        public string IntroducedByGUID { get; set; }
        public string IntroducedBy { get; set; }
        public int ProductType { get; set; }
        public string ProductSubTypeGUID { get; set; }
        public string ProductSubType { get; set; }
        public string CreditLimitTypeGUID { get; set; }
        public string CreditLimitType { get; set; }
        public int CreditLimitExpiration { get; set; }
        public string StartDate { get; set; }
        public string ExpiryDate { get; set; }
        public decimal CreditLimitRequest { get; set; }
        public decimal CustomerCreditLimit { get; set; }
        public string InterestTypeGUID { get; set; }
        public string InterestType { get; set; }
        public decimal InterestAdjustment { get; set; }
        public decimal CreditRequestFeePct { get; set; }
        public decimal CreditRequestFeeAmount { get; set; }
        public decimal PurchaseFeePct { get; set; }
        public int PurchaseFeeCalculateBase { get; set; }
        public string NCBCheckedDate { get; set; }
        public string InterestValue { get; set; }
        public decimal SalesAvgPerMonth { get; set; }
        public string FinancialCreditCheckedDate { get; set; }
        public decimal MaxPurchasePct { get; set; }
        public decimal MaxRetentionPct { get; set; }
        public decimal MaxRetentionAmount { get; set; }
        public int CreditAppRequestType { get; set; }
        public string PurchaseWithdrawalCondition { get; set; }
        public string AssignmentMethodGUID { get; set; }
        public string AssignmentMethodTable { get; set; }
    }
    public class QQCustomer
    {
        public string CustomerName { get; set; }
        public string CustomerId { get; set; }
        public string ResponsibleByGUID { get; set; }
        public string ResponsibleBy { get; set; }
        public int IdentificationType { get; set; }
        public string TaxID { get; set; }
        public string PassportID { get; set; }
        public int RecordType { get; set; }
        public string DateOfEstablish { get; set; }
        public string DateOfBirth { get; set; }
        public string IntroducedByGUID { get; set; }
        public string BusinessTypeGUID { get; set; }
    }
    public class QQBankAccountControl
    {
        public Guid BankGroupGUID { get; set; }
        public string Instituetion { get; set; }
        public string BankBranch { get; set; }
        public Guid BankTypeGUID { get; set; }
        public string AccountType { get; set; }
        public string BankAccountName { get; set; }
        public string AccountNumber { get; set; }
        public string BankType { get; set; }
        public string ProductType { get; set; }
        public string CreditLimitType { get; set; }
        public string CreditLimitExpiration { get; set; }
        public string StartDate { get; set; }
        public string ExpiryDate { get; set; }
        public string CustomerCreditLimit { get; set; }
        public string InterestType { get; set; }
        public string InterestAdjustment { get; set; }
        public string MaxPurchasePct { get; set; }
        public string MaxRetentionPct { get; set; }
        public string MaxRetentionAmount { get; set; }
        public string CustPDCBankName { get; set; }
        public string CustPDCBankBranch { get; set; }
        public string CustPDCBankType { get; set; }
        public string CustPDCBankAccountName { get; set; }
        public string PurchaseFeePct { get; set; }
        public string PurchaseFeeCalculateBase { get; set; }
    }
    public class QQPDCBank
    {
        public Guid BankGroupGUID { get; set; }
        public string CustPDCBankName { get; set; }
        public string CustPDCBankBranch { get; set; }
        public Guid BankTypeGUID { get; set; }
        public string CustPDCBankType { get; set; }
        public string CustPDCBankAccountName { get; set; }
    }
    public class QQAuthorizedPersonTrans
    {
        public string AuthorizedPersonTransGUID { get; set; }
        public string AuthorizedPersonName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string NCBCheckedDate { get; set; }
        public int Row { get; set; }
        public int Ordering { get; set; }
    }
    public class QAuthorizedPersonTransForNCB
    {
        public string AuthorizedPersonTransGUID { get; set; }
        public string AuthorizedPersonName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string NCBCheckedDate { get; set; }
        public int Row { get; set; }
    }
    public class QQAuthorizedNCB1
    {
        public Guid NCBTransGUID { get; set; }
        public string BankGroupGUID { get; set; }
        public string FinancialInstitution { get; set; }
        public string CreditTypeGUID { get; set; }
        public string CreditType { get; set; }
        public string CreditTypeId { get; set; }
        public decimal CreditLimit { get; set; }
        public decimal OutstandingAR { get; set; }
        public decimal MonthlyRepayment { get; set; }
        public string EndDate { get; set; }
        public string NCBAccountStatusGUID { get; set; }
        public string NCBAccountStatusId { get; set; }
        public string NCBAccountStatus { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public int Row { get; set; }
    }
    public class QQSumAuthorizedNCB1
    {
        public decimal SumCreditLimit { get; set; }
        public decimal SumOutstandingAR { get; set; }
        public decimal SumMonthlyRepayment { get; set; }
    }
    public class QQAuthorizedNCB2
    {
        public Guid NCBTransGUID { get; set; }
        public string BankGroupGUID { get; set; }
        public string FinancialInstitution { get; set; }
        public string CreditTypeGUID { get; set; }
        public string CreditType { get; set; }
        public string CreditTypeId { get; set; }
        public decimal CreditLimit { get; set; }
        public decimal OutstandingAR { get; set; }
        public decimal MonthlyRepayment { get; set; }
        public string EndDate { get; set; }
        public string NCBAccountStatusGUID { get; set; }
        public string NCBAccountStatusId { get; set; }
        public string NCBAccountStatus { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public int Row { get; set; }
    }
    public class QQSumAuthorizedNCB2
    {
        public decimal SumCreditLimit { get; set; }
        public decimal SumOutstandingAR { get; set; }
        public decimal SumMonthlyRepayment { get; set; }
    }
    public class QQAuthorizedNCB3
    {
        public Guid NCBTransGUID { get; set; }
        public string BankGroupGUID { get; set; }
        public string FinancialInstitution { get; set; }
        public string CreditTypeGUID { get; set; }
        public string CreditType { get; set; }
        public string CreditTypeId { get; set; }
        public decimal CreditLimit { get; set; }
        public decimal OutstandingAR { get; set; }
        public decimal MonthlyRepayment { get; set; }
        public string EndDate { get; set; }
        public string NCBAccountStatusGUID { get; set; }
        public string NCBAccountStatusId { get; set; }
        public string NCBAccountStatus { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public int Row { get; set; }

    }
    public class QQSumAuthorizedNCB3
    {
        public decimal SumCreditLimit { get; set; }
        public decimal SumOutstandingAR { get; set; }
        public decimal SumMonthlyRepayment { get; set; }
    }
    public class QQOwnerTrans
    {
        public Guid RelatedPersonTableGUID { get; set; }
        public string ShareHolderPersonName { get; set; }
        public decimal PropotionOfShareholderPct { get; set; }
        public int Row { get; set; }
    }
    public class QQCreditAppReqLineFin
    {
        public int Year { get; set; }
        public int Row { get; set; }
        public decimal RegisteredCapital { get; set; }
        public decimal PaidCapital { get; set; }
        public decimal TotalAsset { get; set; }
        public decimal TotalLiability { get; set; }
        public decimal TotalEquity { get; set; }
        public decimal TotalRevenue { get; set; }
        public decimal TotalCOGS { get; set; }
        public decimal TotalGrossProfit { get; set; }
        public decimal TotalOperExpFirst { get; set; }
        public decimal TotalNetProfitFirst { get; set; }
        public decimal NetProfitPercent { get; set; }
        public decimal lDE { get; set; }
        public decimal QuickRatio { get; set; }
        public decimal IntCoverageRatio { get; set; }
    }
    public class QQTaxReportTrans
    {
        public string TaxMonth { get; set; }
        public decimal Amount { get; set; }
        public int Row { get; set; }
    }
    public class QQTaxReportStart
    {
        public string TaxMonth { get; set; }
    }
    public class QQTaxReportEnd
    {
        public string TaxMonth { get; set; }
    }
    public class QQTaxReportTransSum
    {
        public string SumTaxReport { get; set; }
        public decimal TaxReportCOUNT { get; set; }
    }
    public class QQCAReqCreditOutStanding
    {
        public string AsOfDate { get; set; }
        public string ProductType { get; set; }
        public string BookmarkOrdering { get; set; }
        public string CreditLimitTypeId { get; set; }
        public string CreditLimitTypeDesc { get; set; }
        public string ApprovedCreditLimit { get; set; }
        public string ARBalance { get; set; }
        public string CreditLimitBalance { get; set; }
        public string ReserveToBeRefund { get; set; }
        public string AccumRetentionAmount { get; set; }
        public int Row { get; set; }

    }
    public class QQCAReqCreditOutStandingSum
    {
        public decimal SumApprovedCreditLimit { get; set; }
        public decimal SumARBalance { get; set; }
        public decimal SumCreditLimitBalance { get; set; }
        public decimal SumReserveToBeRefund { get; set; }
        public decimal SumAccumRetentionAmount { get; set; }
    }
    public class QQCAReqNCBTrans
    {
        public Guid NCBTransGUID { get; set; }
        public Guid BankGroupGUID { get; set; }
        public string FinancialInstitution { get; set; }
        public Guid CreditTypeGUID { get; set; }
        public string CreditTypeId { get; set; }
        public string CreditType { get; set; }
        public Guid NCBAccountStatusGUID { get; set; }
        public string NCBAccountStatusId { get; set; }
        public string NCBAccountStatus { get; set; }
        public decimal CreditLimit { get; set; }
        public decimal OutstandingAR { get; set; }
        public decimal MonthlyRepayment { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public int Row { get; set; }
    }
    public class QQCAReqNCBTransSum
    {
        public decimal LoanLimit { get; set; }
        public decimal TotalLoanLimit { get; set; }
    }
    public class QCAReqNCBTransSum
    {
        public decimal SumCreditLimit { get; set; }
        public decimal SumOutstandingAR { get; set; }
        public decimal SumMonthlyRepayment { get; set; }
    }
    public class QFinancialCreditTrans
    {
        public Guid BankGroupGUID { get; set; }
        public string FinancialInstitutionDesc { get; set; }
        public Guid CreditTypeGUID { get; set; }
        public string LoanType { get; set; }
        public decimal Amount { get; set; }
        public int Row { get; set; }
    }
    public class QFinancialCreditTransSum
    {
        public decimal LoanLimit { get; set; }
        public decimal TotalLoanLimit { get; set; }
    }
    public class QQCAReqBuyerCreditOutstandingPrivate
    {
        public string BuyerTableGUID { get; set; }
        public string BuyerCreditOutstandingBuyerName { get; set; }
        public string BusinessSegmentGUID { get; set; }
        public int BusinessSegmentType { get; set; }
        public string Status { get; set; }
        public decimal ApprovedCreditLimitLine { get; set; }
        public decimal ARBalance { get; set; }
        public decimal MaxPurchasePct { get; set; }
        public string AssignmentMethodGUID { get; set; }
        public string AssignmentMethod { get; set; }
        public string BillingResponsibleByGUID { get; set; }
        public string BillingResponsibleBy { get; set; }
        public string MethodOfPaymentGUID { get; set; }
        public string MethodOfPayment { get; set; }
        public int Row { get; set; }
    }
    public class QCAReqBuyerCreditOutstandingPrivateCOUNT
    {
        public int CountCAbuyerCreditOutstandingPrivate { get; set; }
        public Guid BusinessSegmentGUID { get; set; }
        public string BusinessSegmentType { get; set; }
    }
    public class QQCAReqBuyerCreditOutstandingGov
    {
        public string BuyerTableGUID { get; set; }
        public string BuyerCreditOutstandingBuyerName { get; set; }
        public string BusinessSegmentGUID { get; set; }
        public int BusinessSegmentType { get; set; }
        public string Status { get; set; }
        public decimal ApprovedCreditLimitLine { get; set; }
        public decimal ARBalance { get; set; }
        public decimal MaxPurchasePct { get; set; }
        public string AssignmentMethodGUID { get; set; }
        public string AssignmentMethod { get; set; }
        public string BillingResponsibleByGUID { get; set; }
        public string BillingResponsibleBy { get; set; }
        public string MethodOfPaymentGUID { get; set; }
        public string MethodOfPayment { get; set; }
        public string BuyerQtyGovernment { get; set; }
        public int LineNum { get; set; }
        public int Row { get; set; }
    }
    public class QCAReqBuyerCreditOutstandingGovCOUNT
    {
        public int CountCAbuyerCreditOutstandingPrivate { get; set; }
        public Guid BusinessSegmentGUID { get; set; }
        public string BusinessSegmentType { get; set; }
    }
    public class QQCreditAppReqAssignment
    {
        public string BuyerTableGUID { get; set; }
        public string AssignmentBuyer { get; set; }
        public string BuyerAgreementTableGUID { get; set; }
        public string AssignmentRefAgreementId { get; set; }
        public decimal AssignmentBuyerAgreementAmount { get; set; }
        public decimal AssignmentAgreementAmount { get; set; }
        public string AssignmentAgreementTableGUID { get; set; }
        public string InternalAssignmentAgreementId { get; set; }
        public string AssignmentAgreementDate { get; set; }
        public decimal RemainingAmount { get; set; }
        public string Description { get; set; }
        public int Row { get; set; }
    }
    public class QQSumCreditAppReqAssignment
    {
        public decimal SumAssignmentBuyerAgreementAmount { get; set; }
        public decimal SumAssignmentAgreementAmount { get; set; }
        public decimal SumRemainingAmount { get; set; }
    }
    public class QQCustBusinessCollateral
    {
        public string BusinessCollateralTypeGUID { get; set; }
        public string BusinessCollateralType { get; set; }
        public string BusinessCollateralStatus { get; set; }
        public string BusinessCollateralSubTypeGUID { get; set; }
        public string BusinessCollateralSubType { get; set; }
        public string Description { get; set; }
        public decimal BusinessCollateralValue { get; set; }
        public bool Cancelled { get; set; }
        public int Row { get; set; }
    }
    public class QQSumCustBusinessCollateral
    {
        public decimal SumBusinessCollateralValue { get; set; }
        public bool Cancelled { get; set; }
    }
    public class QQNCBTrans
    {
        public string BankGroupGUID { get; set; }
        public string FinancialInstitution { get; set; }
        public string CreditTypeGUID { get; set; }
        public string CreditType { get; set; }
        public string CreditLimit { get; set; }
        public decimal OutstandingAR { get; set; }
        public decimal MonthlyRepayment { get; set; }
        public string EndDate { get; set; }
        public string NCBAccountStatusGUID { get; set; }
        public string NCBAccountStatus { get; set; }
    }
    public class QQSumNCBTrans
    {
        public decimal SumCreditLimit { get; set; }
        public decimal SumOutstandingAR { get; set; }
        public decimal MonthlyRepayment { get; set; }
    }
    public class QQProjectReferenceTrans
    {
        public string ProjectCompanyName { get; set; }
        public string ProjectName { get; set; }
        public string Remark { get; set; }
        public string ProjectStatus { get; set; }
        public decimal ProjectValue { get; set; }
        public int ProjectCompletion { get; set; }
        public int Row { get; set; }
        public int ApprovedCreditLimitRequest { get; set; }
    }
    public class QCreditAppReqAssignmentOutstanding
    {
        public Guid BuyerTableGUID { get; set; }
        public string AssignmentBuyer { get; set; }
        public Guid BuyerAgreementTableGUID { get; set; }
        public Guid AssignmentAgreementTableGUID { get; set; }
        public string AssignmentRefAgreementId { get; set; }
        public decimal AssignmentBuyerAgreementAmount { get; set; }
        public decimal AssignmentAgreementAmount { get; set; }
        public decimal RemainingAmount { get; set; }
        public string InternalAssignmentAgreementId { get; set; }
        public string AssignmentAgreementDate { get; set; }
        public string Description { get; set; }
        public int Row { get; set; }
    }
    public class QSumCreditAppReqAssignmentOutstanding
    {
        public decimal SumAssignmentBuyerAgreementAmount { get; set; }
        public decimal SumAssignmentAgreementAmount { get; set; }
        public decimal SumRemainingAmount { get; set; }
        public Guid BuyerAgreementTableGUID { get; set; }
        public decimal AssignmentBuyerAgreementAmount { get; set; }
    }
    public class QCAReqCreditOutStandingAsOfDate
    {
        public DateTime? AsOfDate { get; set; }
    }
    public class BookmarkDocumentCreditApplicationConsideration_Variable
    {
        public string CustCompanyRegistrationID { get; set; }
        public string CustComEstablishedDate { get; set; }
        public string AuthorizedPersonTrans1Age { get; set; }
        public string AuthorizedPersonTrans2Age { get; set; }
        public string AuthorizedPersonTrans3Age { get; set; }
        public string AuthorizedPersonTrans4Age { get; set; }
        public string AuthorizedPersonTrans5Age { get; set; }
        public string AuthorizedPersonTrans6Age { get; set; }
        public string AuthorizedPersonTrans7Age { get; set; }
        public string AuthorizedPersonTrans8Age { get; set; }
        public decimal InterestValue { get; set; }
        public decimal NewInterestRate { get; set; }
        public decimal AverageOutputVATPerMonth { get; set; }
        public decimal NewCreditLimitAmt { get; set; }
        public string CrditLimitAmtFirst1 { get; set; }
        public string AROutstandingAmtFirst { get; set; }
        public string CreditLimitRemainingFirst1 { get; set; }
        public string ReserveOutstandingFirst1 { get; set; }
        public string RetentionOutstandingFirst1 { get; set; }
        public string CrditLimitAmtSecond1 { get; set; }
        public string AROutstandingAmtSecond { get; set; }
        public string CreditLimitRemainingSecond1 { get; set; }
        public string ReserveOutstandingSecond1 { get; set; }
        public string RetentionOutstandingSecond1 { get; set; }
        public string CrditLimitAmtThird1 { get; set; }
        public string AROutstandingAmtThird { get; set; }
        public string CreditLimitRemainingThird1 { get; set; }
        public string ReserveOutstandingThird1 { get; set; }
        public string RetentionOutstandingThird1 { get; set; }
        public string CrditLimitAmtFourth1 { get; set; }
        public string AROutstandingAmtFourth { get; set; }
        public string CreditLimitRemainingFourth1 { get; set; }
        public string ReserveOutstandingFourth1 { get; set; }
        public string RetentionOutstandingFourth1 { get; set; }
        public string CrditLimitAmtFifth1 { get; set; }
        public string AROutstandingAmtFifth { get; set; }
        public string CreditLimitRemainingFifth1 { get; set; }
        public string ReserveOutstandingFifth1 { get; set; }
        public string RetentionOutstandingFifth1 { get; set; }
        public string CrditLimitAmtSixth1 { get; set; }
        public string AROutstandingAmtSixth { get; set; }
        public string CreditLimitRemainingSixth1 { get; set; }
        public string ReserveOutstandingSixth1 { get; set; }
        public string RetentionOutstandingSixth1 { get; set; }
        public string CrditLimitAmtSeventh1 { get; set; }
        public string AROutstandingAmtSeventh { get; set; }
        public string CreditLimitRemainingSeventh1 { get; set; }
        public string ReserveOutstandingSeventh1 { get; set; }
        public string RetentionOutstandingSeventh1 { get; set; }
        public string CrditLimitAmtEighth1 { get; set; }
        public string AROutstandingAmtEighth { get; set; }
        public string CreditLimitRemainingEighth1 { get; set; }
        public string ReserveOutstandingEighth1 { get; set; }
        public string RetentionOutstandingEighth1 { get; set; }
        public string CrditLimitAmtNineth1 { get; set; }
        public string AROutstandingAmtNineth { get; set; }
        public string CreditLimitRemainingNineth1 { get; set; }
        public string ReserveOutstandingNineth1 { get; set; }
        public string RetentionOutstandingNineth1 { get; set; }
        public string CrditLimitAmtTenth1 { get; set; }
        public string AROutstandingAmtTenth { get; set; }
        public string CreditLimitRemainingTenth1 { get; set; }
        public string ReserveOutstandingTenth1 { get; set; }
        public string RetentionOutstandingTenth1 { get; set; }
        public string CrditLimitAmtSix1 { get; set; }
        public string FactoringPurchaseCondition { get; set; }
        public string AssignmentCondition { get; set; }
    }
}
