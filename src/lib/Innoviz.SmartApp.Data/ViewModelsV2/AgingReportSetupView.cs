using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class AgingReportSetupView : ViewCompanyBaseEntity
	{
		public int From1 { get; set; }
		public int To1 { get; set; }
		public int From2 { get; set; }
		public int To2 { get; set; }
		public int From3 { get; set; }
		public int To3 { get; set; }
		public int From4 { get; set; }
		public int To4 { get; set; }
		public int From5 { get; set; }
		public int To5 { get; set; }
		public int From6 { get; set; }
		public int To6 { get; set; }
		public string Description1 { get; set; }
		public string Description2 { get; set; }
		public string Description3 { get; set; }
		public string Description4 { get; set; }
		public string Description5 { get; set; }
		public string Description6 { get; set; }
	}
}
