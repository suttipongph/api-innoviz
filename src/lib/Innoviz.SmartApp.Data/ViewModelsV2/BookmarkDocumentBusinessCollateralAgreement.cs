﻿using Innoviz.SmartApp.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class QCompanyBusinessCollateralAgreement
    {
        public string QCompanyBusinessCollateralAgreement_Company_Name { get; set; }
        public string QCompanyBusinessCollateralAgreement_Company_Address { get; set; }
    }
    public class QBusinessCollateralAgmTable
    {
        public string QBusinessCollateralAgmTable_BusinessCollateralAgmId { get; set; }
        public DateTime QBusinessCollateralAgmTable_AgreementDate { get; set; }
        public string QBusinessCollateralAgmTable_CustomerAddress { get; set; }
        public string QBusinessCollateralAgmTable_CompanyWitness1 { get; set; }
        public string QBusinessCollateralAgmTable_CompanyWitness2 { get; set; }
        public string QBusinessCollateralAgmTable_CompanyAuthorizedPerson1 { get; set; }
        public string QBusinessCollateralAgmTable_CompanyAuthorizedPerson2 { get; set; }
        public string QBusinessCollateralAgmTable_CompanyAuthorizedPerson3 { get; set; }
        public string QBusinessCollateralAgmTable_AuthorizedPersonTransCustomer1GUID { get; set; }
        public string QBusinessCollateralAgmTable_AuthorizedPersonTransCustomer2GUID { get; set; }
        public string QBusinessCollateralAgmTable_AuthorizedPersonTransCustomer3GUID { get; set; }
        public decimal QBusinessCollateralAgmTable_SumBusinessCollateralValue { get; set; }
        public string QBusinessCollateralAgmTable_DBDRegistrationId { get; set; }
        public string QBusinessCollateralAgmTable_DBDRegistrationDescription { get; set; }
        public decimal QBusinessCollateralAgmTable_DBDRegistrationAmount { get; set; }
        public string QBusinessCollateralAgmTable_AttachmentText { get; set; }
        public string QBusinessCollateralAgmTable_CompanyGUID { get; set; }
        public Guid QBusinessCollateralAgmTable_CustomerTableGUID { get; set; }
}
    public class VariableQBusinessCollateralAgmTable
    {
        public string Variable_BCLeaseholdsRightDateSixth1 { get; set; }
        public string Variable_BCLeaseholdsRightDateSeven1 { get; set; }
        public string Variable_BCLeaseholdsRightDateEighth1 { get; set; }
        public string Variable_BCLeaseholdsRightDateNineth1 { get; set; }
        public string Variable_BCLeaseholdsRightDateTenth1 { get; set; }
        public string Variable_BCBuyerTaxIDFirst1 { get; set; }
        public string Variable_BCBuyerTaxIDSecond1 { get; set; }
        public string Variable_BCBuyerTaxIDThird1 { get; set; }
        public string Variable_BCBuyerTaxIDFourth1 { get; set; }
        public string Variable_BCBuyerTaxIDFifth1 { get; set; }
        public string Variable_BCBuyerTaxIDSixth1 { get; set; }
        public string Variable_BCBuyerTaxIDSeventh1 { get; set; }
        public string Variable_BCBuyerTaxIDEighth1 { get; set; }
        public string Variable_BCBuyerTaxIDNineth1 { get; set; }
        public string Variable_BCBuyerTaxIDTenth1 { get; set; }
        public string Variable_BCTaxIdARFirst1 { get; set; }
        public string Variable_BCTaxIdARSecond1 { get; set; }
        public string Variable_BCTaxIdARThird1 { get; set; }
        public string Variable_BCTaxIdARFourth1 { get; set; }
        public string Variable_BCTaxIdARFifth1 { get; set; }
        public string Variable_BCTaxIdARSixth1 { get; set; }
        public string Variable_BCTaxIdARSeventh1 { get; set; }
        public string Variable_BCTaxIdAREighth1 { get; set; }
        public string Variable_BCTaxIdARNineth1 { get; set; }
        public string Variable_BCTaxIdARTenth1 { get; set; }

    }
    public class QCustomerTableBusinessCollateralAgreement
    {
        public string QCustomerTableBusinessCollateralAgreement_Name { get; set; }
        public string QCustomerTableBusinessCollateralAgreement_TaxID { get; set; }
    }
    public class QBusinessCollateralAgmLineT1S1
    {
        public string QBusinessCollateralAgmLineT1S1_BankTypeGUID { get; set; }
        public string QBusinessCollateralAgmLineT1S1_AccountNumber { get; set; }
        public string QBusinessCollateralAgmLineT1S1_BankGroup { get; set; }
        public string QBusinessCollateralAgmLineT1S1_BankType { get; set; }
        public string QBusinessCollateralAgmLineT1S1_SubTypeId { get; set; }
        public decimal QBusinessCollateralAgmLineT1S1_SumBusinessCollateralValue { get; set; }
        public int QBusinessCollateralAgmLineT1S1_ConutLine { get; set; }
        public int QBusinessCollateralAgmLineT1S1_PreferentialCreditorNumber { get; set; }
        public string QBusinessCollateralAgmLineT1S1_Product { get; set; }
        public string QBusinessCollateralAgmLineT1S1_AttachmentText { get; set; }
        public int QBusinessCollateralAgmLineT1S1_Row { get; set; }
    }
    public class QBusinessCollateralAgmLineT1S2
    {
        public string QBusinessCollateralAgmLineT1S2_RefAgreementId { get; set; }
        public DateTime? QBusinessCollateralAgmLineT1S2_RefAgreementDate { get; set; }
        public string QBusinessCollateralAgmLineT1S2_Lessee { get; set; }
        public string QBusinessCollateralAgmLineT1S2_Lessor { get; set; }
        public string QBusinessCollateralAgmLineT1S2_SubTypeId { get; set; }
        public decimal QBusinessCollateralAgmLineT1S2_SumBusinessCollateralValue { get; set; }
        public int QBusinessCollateralAgmLineT1S2_ConutLine { get; set; }
        public int QBusinessCollateralAgmLineT1S2_PreferentialCreditorNumber { get; set; }
        public Guid QBusinessCollateralAgmLineT1S2_BusinessCollateralSubTypeGUID { get; set; }
        public string QBusinessCollateralAgmLineT1S2_Product { get; set; }
        public string QBusinessCollateralAgmLineT1S2_AttachmentText { get; set; }
        public int QBusinessCollateralAgmLineT1S2_Row { get; set; }
    }
    public class QBusinessCollateralAgmLineT1S3
    {
        public string QBusinessCollateralAgmLineT1S3_Description { get; set; }
        public string QBusinessCollateralAgmLineT1S3_RefAgreementId { get; set; }
        public DateTime? QBusinessCollateralAgmLineT1S3_RefAgreementDate { get; set; }
        public string QBusinessCollateralAgmLineT1S3_Lessee { get; set; }
        public string QBusinessCollateralAgmLineT1S3_Lessor { get; set; }
        public string QBusinessCollateralAgmLineT1S3_SubTypeId { get; set; }
        public decimal QBusinessCollateralAgmLineT1S3_SumBusinessCollateralValue { get; set; }
        public string QBusinessCollateralAgmLineT1S3_AttachmentText { get; set; }

        public int QBusinessCollateralAgmLineT1S3_ConutLine { get; set; }
        public int QBusinessCollateralAgmLineT1S3_PreferentialCreditorNumber { get; set; }

        public string QBusinessCollateralAgmLineT1S3_Product { get; set; }
        public int QBusinessCollateralAgmLineT1S3_Row { get; set; }
    }
    public class QBusinessCollateralAgmLineT2S1
    {
        public string QBusinessCollateralAgmLineT2S1_SubTypeId { get; set; }
        public decimal QBusinessCollateralAgmLineT2S1_SumBusinessCollateralValue { get; set; }
        public string QBusinessCollateralAgmLineT2S1_AttachmentText { get; set; }
        public string QBusinessCollateralAgmLineT2S1_BuyerName { get; set; }
        public int QBusinessCollateralAgmLineT2S1_IdentificationType { get; set; }
        public string QBusinessCollateralAgmLineT2S1_TaxId { get; set; }
        public string QBusinessCollateralAgmLineT2S1_PassportId { get; set; }
        public int QBusinessCollateralAgmLineT2S1_ConutLine { get; set; }
        public int QBusinessCollateralAgmLineT2S1_PreferentialCreditorNumber { get; set; }

        public string QBusinessCollateralAgmLineT2S1_Product { get; set; }
        public int QBusinessCollateralAgmLineT2S1_Row { get; set; }
    }
    public class QBusinessCollateralAgmLineT2S2
    {
        public string QBusinessCollateralAgmLineT2S2_SubTypeId { get; set; }
        public decimal QBusinessCollateralAgmLineT2S2_SumBusinessCollateralValue { get; set; }
        public string QBusinessCollateralAgmLineT2S2_BuyerName { get; set; }
        public int QBusinessCollateralAgmLineT2S2_IdentificationType { get; set; }
        public string QBusinessCollateralAgmLineT2S2_TaxId { get; set; }
        public string QBusinessCollateralAgmLineT2S2_PassportId { get; set; }
        public int QBusinessCollateralAgmLineT2S2_ConutLine { get; set; }
        public int QBusinessCollateralAgmLineT2S2_PreferentialCreditorNumber { get; set; }

        public string QBusinessCollateralAgmLineT2S2_AttachmentText { get; set; }
        public string QBusinessCollateralAgmLineT2S2_Product { get; set; }
        public int QBusinessCollateralAgmLineT2S2_Row { get; set; }
    }
    public class QBusinessCollateralAgmLineT3S1
    {
        public string QBusinessCollateralAgmLineT3S1_RegistrationPlateNumber { get; set; }
        public string QBusinessCollateralAgmLineT3S1_MachineNumber { get; set; }
        public string QBusinessCollateralAgmLineT3S1_MachineRegisteredStatus { get; set; }
        public string QBusinessCollateralAgmLineT3S1_RegisteredPlace { get; set; }
        public decimal QBusinessCollateralAgmLineT3S1_BusinessCollateralValue { get; set; }
        public string QBusinessCollateralAgmLineT3S1_SubTypeId { get; set; }
        public decimal QBusinessCollateralAgmLineT3S1_SumBusinessCollateralValue { get; set; }
        public int QBusinessCollateralAgmLineT3S1_ConutLine { get; set; }
        public int QBusinessCollateralAgmLineT3S1_PreferentialCreditorNumber { get; set; }
        public string QBusinessCollateralAgmLineT3S1_Product { get; set; }

        public string QBusinessCollateralAgmLineT3S1_AttachmentText { get; set; }
        public string QBusinessCollateralAgmLineT3S1_Unit { get; set; }
        public int QBusinessCollateralAgmLineT3S1_Row { get; set; }
    }
    public class QBusinessCollateralAgmLineT3S2
    {
        public decimal QBusinessCollateralAgmLineT3S2_Quantity { get; set; }
        public decimal QBusinessCollateralAgmLineT3S2_BusinessCollateralValue { get; set; }
        public string QBusinessCollateralAgmLineT3S2_SubTypeId { get; set; }
        public decimal QBusinessCollateralAgmLineT3S2_SumBusinessCollateralValue { get; set; }
        public string QBusinessCollateralAgmLineT3S2_Product { get; set; }
        public int QBusinessCollateralAgmLineT3S2_ConutLine { get; set; }
        public int QBusinessCollateralAgmLineT3S2_PreferentialCreditorNumber { get; set; }

        public string QBusinessCollateralAgmLineT3S2_Unit { get; set; }
        public string QBusinessCollateralAgmLineT3S2_AttachmentText { get; set; }

        public int QBusinessCollateralAgmLineT3S2_Row { get; set; }
    }
    public class QBusinessCollateralAgmLineT3S3
    {
        public decimal QBusinessCollateralAgmLineT3S3_Quantity { get; set; }
        public decimal QBusinessCollateralAgmLineT3S3_BusinessCollateralValue { get; set; }
        public string QBusinessCollateralAgmLineT3S3_SubTypeId { get; set; }
        public decimal QBusinessCollateralAgmLineT3S3_SumBusinessCollateralValue { get; set; }
        public int QBusinessCollateralAgmLineT3S3_ConutLine { get; set; }
        public int QBusinessCollateralAgmLineT3S3_PreferentialCreditorNumber { get; set; }
        public int QBusinessCollateralAgmLineT3S3_Row { get; set; }
        public string QBusinessCollateralAgmLineT3S3_Product { get; set; }
        public string QBusinessCollateralAgmLineT3S3_Unit { get; set; }
        public string QBusinessCollateralAgmLineT3S3_AttachmentText { get; set; }

    }
    public class QBusinessCollateralAgmLineT3S4
    {
        public string QBusinessCollateralAgmLineT3S4_MachineNumber { get; set; }
        public string QBusinessCollateralAgmLineT3S4_ChassisNumber { get; set; }
        public string QBusinessCollateralAgmLineT3S4_RegistrationPlateNumber { get; set; }
        public decimal QBusinessCollateralAgmLineT3S4_Quantity { get; set; }
        public decimal QBusinessCollateralAgmLineT3S4_BusinessCollateralValue { get; set; }
        public string QBusinessCollateralAgmLineT3S4_SubTypeId { get; set; }
        public decimal QBusinessCollateralAgmLineT3S4_SumBusinessCollateralValue { get; set; }
        public string QBusinessCollateralAgmLineT3S4_Product { get; set; }

        public int QBusinessCollateralAgmLineT3S4_ConutLine { get; set; }
        public int QBusinessCollateralAgmLineT3S4_PreferentialCreditorNumber { get; set; }

        public string QBusinessCollateralAgmLineT3S4_AttachmentText { get; set; }
        public string QBusinessCollateralAgmLineT3S4_Unit { get; set; }
        public int QBusinessCollateralAgmLineT3S4_Row { get; set; }
    }
    public class QBusinessCollateralAgmLineT3S5
    {
        public decimal QBusinessCollateralAgmLineT3S5_Quantity { get; set; }
        public decimal QBusinessCollateralAgmLineT3S5_BusinessCollateralValue { get; set; }
        public string QBusinessCollateralAgmLineT3S5_SubTypeId { get; set; }
        public decimal QBusinessCollateralAgmLineT3S5_SumBusinessCollateralValue { get; set; }
        public string QBusinessCollateralAgmLineT3S5_Product { get; set; }
        public string QBusinessCollateralAgmLineT3S5_AttachmentText { get; set; }
        public int QBusinessCollateralAgmLineT3S5_Unit { get; set; }
        public Guid QBusinessCollateralAgmLineT3S5_BusinessCollateralSubTypeGUID { get; set; }
        public int QBusinessCollateralAgmLineT3S5_ConutLine { get; set; }
        public int QBusinessCollateralAgmLineT3S5_PreferentialCreditorNumber { get; set; }

        public int QBusinessCollateralAgmLineT3S5_Row { get; set; }
    }
    public class QBusinessCollateralAgmLineT4S1
    {
        public string QBusinessCollateralAgmLineT4S1_ProjectName { get; set; }
        public string QBusinessCollateralAgmLineT4S1_TitleDeedNumber { get; set; }
        public string QBusinessCollateralAgmLineT4S1_TitleDeedSubDistrict { get; set; }
        public string QBusinessCollateralAgmLineT4S1_TitleDeedDistrict { get; set; }
        public string QBusinessCollateralAgmLineT4S1_TitleDeedProvince { get; set; }
        public string QBusinessCollateralAgmLineT4S1_SubTypeId { get; set; }
        public decimal QBusinessCollateralAgmLineT4S1_SumBusinessCollateralValue { get; set; }
        public int QBusinessCollateralAgmLineT4S1_ConutLine { get; set; }
        public string QBusinessCollateralAgmLineT4S1_AttachmentText { get; set; }
        public Guid QBusinessCollateralAgmLineT4S1_BusinessCollateralSubTypeGUID { get; set; }
        public string QBusinessCollateralAgmLineT4S1_Product { get; set; }
        public string QBusinessCollateralAgmLineT4S1_Unit { get; set; }

        public int QBusinessCollateralAgmLineT4S1_PreferentialCreditorNumber { get; set; }
        public int QBusinessCollateralAgmLineT4S1_Row { get; set; }
    }
    public class QBusinessCollateralAgmLineT4S2
    {
        public string QBusinessCollateralAgmLineT4S2_SubTypeId { get; set; }
        public decimal QBusinessCollateralAgmLineT4S2_SumBusinessCollateralValue { get; set; }
       
        public int QBusinessCollateralAgmLineT4S2_ConutLine { get; set; }
        public int QBusinessCollateralAgmLineT4S2_PreferentialCreditorNumber { get; set; }

        public string QBusinessCollateralAgmLineT4S2_AttachmentText { get; set; }
        public string QBusinessCollateralAgmLineT4S2_Product { get; set; }
        public string QBusinessCollateralAgmLineT4S2_Unit { get; set; }
        public int QBusinessCollateralAgmLineT4S2_Row { get; set; }
    }
    public class QBusinessCollateralAgmLineT4S3
    {
        public string QBusinessCollateralAgmLineT4S3_SubTypeId { get; set; }
        public decimal QBusinessCollateralAgmLineT4S3_SumBusinessCollateralValue { get; set; }
        public string QBusinessCollateralAgmLineT4S3_AttachmentText { get; set; }
        public string QBusinessCollateralAgmLineT4S3_Product { get; set; }
        public int QBusinessCollateralAgmLineT4S3_ConutLine { get; set; }
        public int QBusinessCollateralAgmLineT4S3_PreferentialCreditorNumber { get; set; }
        public int QBusinessCollateralAgmLineT4S3_Row { get; set; }
    }
    public class QBusinessCollateralAgmLineT5S1
    {
        public string QBusinessCollateralAgmLineT5S1_SubTypeId { get; set; }
        public decimal QBusinessCollateralAgmLineT5S1_SumBusinessCollateralValue { get; set; }
        public string QBusinessCollateralAgmLineT5S1_AttachmentText { get; set; }
        public string QBusinessCollateralAgmLineT5S1_Unit { get; set; }
        public int QBusinessCollateralAgmLineT5S1_ConutLine { get; set; }
        public int QBusinessCollateralAgmLineT5S1_PreferentialCreditorNumber { get; set; }
        public decimal QBusinessCollateralAgmLineT5S1_BusinessCollateralValue { get; set; }
        public string QBusinessCollateralAgmLineT5S1_Product { get; set; }
        public string QBusinessCollateralAgmLineT5S1_BusinessCollateralAgmLineDesc { get; set; }
        public int QBusinessCollateralAgmLineT5S1_Row { get; set; }
    }
    public class QBusinessCollateralAgmLineT5S2
    {
        public string QBusinessCollateralAgmLineT5S2_SubTypeId { get; set; }
        public decimal QBusinessCollateralAgmLineT5S2_SumBusinessCollateralValue { get; set; }
        public string QBusinessCollateralAgmLineT5S2_AttachmentText { get; set; }
        public string QBusinessCollateralAgmLineT5S2_Product { get; set; }
        public int QBusinessCollateralAgmLineT5S2_ConutLine { get; set; }
        public int QBusinessCollateralAgmLineT5S2_PreferentialCreditorNumber { get; set; }
        public int QBusinessCollateralAgmLineT5S2_Row { get; set; }
    }
    public class QBusinessCollateralAgmLineT5S3
    {
        public string QBusinessCollateralAgmLineT5S3_SubTypeId { get; set; }
        public decimal QBusinessCollateralAgmLineT5S3_SumBusinessCollateralValue { get; set; }
        public string QBusinessCollateralAgmLineT5S3_AttachmentText { get; set; }
        public string QBusinessCollateralAgmLineT5S3_Product { get; set; }
        public int QBusinessCollateralAgmLineT5S3_ConutLine { get; set; }
        public int QBusinessCollateralAgmLineT5S3_PreferentialCreditorNumber { get; set; }
        public int QBusinessCollateralAgmLineT5S3_Row { get; set; }
    }

    public class QBusinessCollateralAgmLineT5S4
    {
        public string QBusinessCollateralAgmLineT5S4_AttachmentText { get; set; }
        public string QBusinessCollateralAgmLineT5S4_Product { get; set; }
        public int QBusinessCollateralAgmLineT5S4_ConutLine { get; set; }
        public int QBusinessCollateralAgmLineT5S4_PreferentialCreditorNumber { get; set; }
        public int QBusinessCollateralAgmLineT5S4_Row { get; set; }
    }
    public class BookmarkBusinessCollateralAgmTable
    {
        [BookmarkMapping("AuthorizedPersonCustFirst1", "AuthorizedCustFirst1", "AuthorizedCustFirst2", "AuthorizedCustFirst3", "AuthorizedCustFirst4", "AuthorizedCustFirst5", "AuthorizedCustFirst6", "AuthorizedCustFirst7", "AuthorizedCustFirst8", "AuthorizedCustFirst9", "AuthorizedCustFirst10", "AuthorizedCustFirst11", "AuthorizedCustFirst12", "AuthorizedCustFirst13", "AuthorizedCustFirst14", "AuthorizedCustFirst15")]
        public string QCustomerAuthorizedPerson1_Name { get; set; }
        [BookmarkMapping("AuthorizedPersonCustSecond1", "AuthorizedCustSecond1")]
        public string QCustomerAuthorizedPerson2_Name { get; set; }
        [BookmarkMapping("AuthorizedPersonCustThird1", "AuthorizedCustThird1")]
        public string QCustomerAuthorizedPerson3_Name { get; set; }
        [BookmarkMapping("CompanyName1", "CompanyName2", "CompanyName3")]
        public string QCompanyBusinessCollateralAgreement_Company_Name { get; set; }
        [BookmarkMapping("CompanyAddress2", "CompanyAddress1")]
        public string QCompanyBusinessCollateralAgreement_Company_Address { get; set; }
        [BookmarkMapping("ContractCollateralNo1", "ContractCollateralNo2", "ContractCollateralNo3", "ContractCollateralNo4", "ContractCollateralNo5", "ContractCollateralNo6", "ContractCollateralNo7", "ContractCollateralNo8", "ContractCollateralNo9", "ContractCollateralNo10", "ContractCollateralNo11", "ContractCollateralNo12", "ContractCollateralNo13", "ContractCollateralNo14")]
        public string QBusinessCollateralAgmTable_BusinessCollateralAgmId { get; set; }
        public DateTime QBusinessCollateralAgmTable_AgreementDate { get; set; }
        [BookmarkMapping("CustAddress1")]
        public string QBusinessCollateralAgmTable_CustomerAddress { get; set; }
        [BookmarkMapping("WitnessFirst1", "WitnessFirst2", "WitnessFirst3", "WitnessFirst4", "WitnessFirst5", "WitnessFirst6", "WitnessFirst7", "WitnessFirst8", "WitnessFirst9", "WitnessFirst10", "WitnessFirst11", "WitnessFirst12", "WitnessFirst13", "WitnessFirst14", "WitnessFirst15")]
        public string QBusinessCollateralAgmTable_CompanyWitness1 { get; set; }
        [BookmarkMapping("WitnessSecond1", "WitnessSecond2", "WitnessSecond3", "WitnessSecond4", "WitnessSecond5", "WitnessSecond6", "WitnessSecond7", "WitnessSecond8", "WitnessSecond9", "WitnessSecond10", "WitnessSecond11", "WitnessSecond12", "WitnessSecond13", "WitnessSecond14", "WitnessSecond15")]
        public string QBusinessCollateralAgmTable_CompanyWitness2 { get; set; }
        [BookmarkMapping("AuthorityPersonFirst1", "AuthorityPersonFirst2", "AuthorityPersonFirst3", "AuthorityPersonFirst4", "AuthorityPersonFirst5", "AuthorityPersonFirst6", "AuthorityPersonFirst7", "AuthorityPersonFirst8", "AuthorityPersonFirst9", "AuthorityPersonFirst10", "AuthorityPersonFirst11", "AuthorityPersonFirst12", "AuthorityPersonFirst13", "AuthorityPersonFirst14", "AuthorityPersonFirst15")]
        public string QBusinessCollateralAgmTable_CompanyAuthorizedPerson1 { get; set; }
        [BookmarkMapping("AuthorityPersonSecond1", "AuthorityPersonSecond2", "AuthorityPersonSecond3", "AuthorityPersonSecond4", "AuthorityPersonSecond5", "AuthorityPersonSecond6", "AuthorityPersonSecond7", "AuthorityPersonSecond8", "AuthorityPersonSecond9", "AuthorityPersonSecond10", "AuthorityPersonSecond11", "AuthorityPersonSecond12", "AuthorityPersonSecond13", "AuthorityPersonSecond14", "AuthorityPersonSecond15")]
        public string QBusinessCollateralAgmTable_CompanyAuthorizedPerson2 { get; set; }
        [BookmarkMapping("AuthorityPersonThird1")]
        public string QBusinessCollateralAgmTable_CompanyAuthorizedPerson3 { get; set; }
        public string QBusinessCollateralAgmTable_AuthorizedPersonTransCustomer1GUID { get; set; }
        public string QBusinessCollateralAgmTable_AuthorizedPersonTransCustomer2GUID { get; set; }
        public string QBusinessCollateralAgmTable_AuthorizedPersonTransCustomer3GUID { get; set; }
        [BookmarkMapping("BCAllSubtypeAmountFirst1")]
        public decimal QBusinessCollateralAgmTable_SumBusinessCollateralValue { get; set; }
        //[BookmarkMapping("BCIntellAssetsSubtypeFirst1", "BCIntellAssetsSubtypeSecond1", "BCIntellAssetsSubtypeThird1", "BCIntellAssetsSubtypeFour1")]
        public string QBusinessCollateralAgmTable_DBDRegistrationId { get; set; }
        //[BookmarkMapping("BCIntellAssetsDesFirst1", "BCIntellAssetsDesSecond1", "BCIntellAssetsDesThird1", "BCIntellAssetsDesFour1")]
        public string QBusinessCollateralAgmTable_DBDRegistrationDescription { get; set; }
        //[BookmarkMapping("BCIntellAssetsAmountFirst1", "BCIntellAssetsAmountSecond1", "BCIntellAssetsAmountThird1", "BCIntellAssetsAmountFour1")]
        public decimal QBusinessCollateralAgmTable_DBDRegistrationAmount { get; set; }
        [BookmarkMapping("CustName1", "CustName2", "CustName3", "CustName4", "CustName5", "CustName6", "CustName7", "CustName8", "CustName9", "CustName10", "CustName11", "CustName12", "CustName13", "CustName14")]
        public string QCustomerTableBusinessCollateralAgreement_Name { get; set; }
        [BookmarkMapping("CustTaxID1")]
        public string QCustomerTableBusinessCollateralAgreement_TaxID { get; set; }

        #region QBusinessCollateralAgmLineT1S1
        public string QBusinessCollateralAgmLineT1S1_BankTypeGUID { get; set; }
        [BookmarkMapping("BCBankAccNumFirst1")]
        public string QBusinessCollateralAgmLineT1S1_AccountNumber { get; set; }
        [BookmarkMapping("BCBankNameFirst1")]
        public string QBusinessCollateralAgmLineT1S1_BankGroup { get; set; }
        [BookmarkMapping("BCBankTypeFirst1")]
        public string QBusinessCollateralAgmLineT1S1_BankType { get; set; }
        [BookmarkMapping("BCNameSubtypeBank1")]
        public string QBusinessCollateralAgmLineT1S1_SubTypeId { get; set; }
        [BookmarkMapping("BCSumSubtypeBank1")]
        public decimal QBusinessCollateralAgmLineT1S1_SumBusinessCollateralValue { get; set; }
        [BookmarkMapping("SubtypeTextAttachmentFirst1")]
        public string QBusinessCollateralAgmLineT1S1_AttachmentText { get; set; }
        [BookmarkMapping("SubtypeQtyFirst1")]
        public int QBusinessCollateralAgmLineT1S1_ConutLine { get; set; }
        [BookmarkMapping("SubtypePreferentialFirst1")]
        public int QBusinessCollateralAgmLineT1S1_PreferentialCreditorNumber { get; set; }
        public int QBusinessCollateralAgmLineT1S1_Row { get; set; }


        [BookmarkMapping("BCBankAccNumSecond1")]
        public string QBusinessCollateralAgmLineT1S1_AccountNumber_Row_2 { get; set; }
        [BookmarkMapping("BCBankNameSecond1")]
        public string QBusinessCollateralAgmLineT1S1_BankGroup_Row_2 { get; set; }
        [BookmarkMapping("BCBankTypeSecond1")]
        public string QBusinessCollateralAgmLineT1S1_BankType_Row_2 { get; set; }



        [BookmarkMapping("BCBankAccNumThird1")]
        public string QBusinessCollateralAgmLineT1S1_AccountNumber_Row_3 { get; set; }
        [BookmarkMapping("BCBankNameThird1")]
        public string QBusinessCollateralAgmLineT1S1_BankGroup_Row_3 { get; set; }
        [BookmarkMapping("BCBankTypeThird1")]
        public string QBusinessCollateralAgmLineT1S1_BankType_Row_3 { get; set; }



        [BookmarkMapping("BCBankAccNumFourth1")]
        public string QBusinessCollateralAgmLineT1S1_AccountNumber_Row_4 { get; set; }
        [BookmarkMapping("BCBankNameFourth1")]
        public string QBusinessCollateralAgmLineT1S1_BankGroup_Row_4 { get; set; }
        [BookmarkMapping("BCBankTypeFourth1")]
        public string QBusinessCollateralAgmLineT1S1_BankType_Row_4 { get; set; }



        [BookmarkMapping("BCBankAccNumFifth1")]
        public string QBusinessCollateralAgmLineT1S1_AccountNumber_Row_5 { get; set; }
        [BookmarkMapping("BCBankNameFifth1")]
        public string QBusinessCollateralAgmLineT1S1_BankGroup_Row_5 { get; set; }
        [BookmarkMapping("BCBankTypeFifth1")]
        public string QBusinessCollateralAgmLineT1S1_BankType_Row_5 { get; set; }


        #endregion QBusinessCollateralAgmLineT1S1

        #region QBusinessCollateralAgmLineT1S2

        [BookmarkMapping("BCLeaseholdsRightIDFirst1")]
        public string QBusinessCollateralAgmLineT1S2_RefAgreementId { get; set; }
        public DateTime? QBusinessCollateralAgmLineT1S2_RefAgreementDate { get; set; }
        [BookmarkMapping("BCLeaseholdsRightLesseeFirst1")]
        public string QBusinessCollateralAgmLineT1S2_Lessee { get; set; }
        [BookmarkMapping("BCLeaseholdsRightLessorFirst1")]
        public string QBusinessCollateralAgmLineT1S2_Lessor { get; set; }
        [BookmarkMapping("BCNameSubtypeLeasehold1")]
        public string QBusinessCollateralAgmLineT1S2_SubTypeId { get; set; }
        [BookmarkMapping("BCSumSubtypeLeasehold1")]
        public decimal QBusinessCollateralAgmLineT1S2_SumBusinessCollateralValue { get; set; }
        [BookmarkMapping("SubtypeTextAttachmentSecond1")]
        public string QBusinessCollateralAgmLineT1S2_AttachmentText { get; set; }
        [BookmarkMapping("SubtypeQtySecond1")]
        public int QBusinessCollateralAgmLineT1S2_ConutLine { get; set; }
        [BookmarkMapping("SubtypePreferentialSecond1")]
        public int QBusinessCollateralAgmLineT1S2_PreferentialCreditorNumber { get; set; }
        public int QBusinessCollateralAgmLineT1S2_Row { get; set; }
        public int QBusinessCollateralAgmLineT1S2_Product { get; set; }

        [BookmarkMapping("BCLeaseholdsRightIDSecond1")]
        public string QBusinessCollateralAgmLineT1S2_RefAgreementId_Row_2 { get; set; }
        public DateTime? QBusinessCollateralAgmLineT1S2_RefAgreementDate_Row_2 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightLesseeSecond1")]
        public string QBusinessCollateralAgmLineT1S2_Lessee_Row_2 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightLessorSecond1")]
        public string QBusinessCollateralAgmLineT1S2_Lessor_Row_2 { get; set; }

        [BookmarkMapping("BCLeaseholdsRightIDThird1")]
        public string QBusinessCollateralAgmLineT1S2_RefAgreementId_Row_3 { get; set; }
        public DateTime? QBusinessCollateralAgmLineT1S2_RefAgreementDate_Row_3 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightLesseeThird1")]
        public string QBusinessCollateralAgmLineT1S2_Lessee_Row_3 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightLessorThird1")]
        public string QBusinessCollateralAgmLineT1S2_Lessor_Row_3 { get; set; }

        [BookmarkMapping("BCLeaseholdsRightIDFourth1")]
        public string QBusinessCollateralAgmLineT1S2_RefAgreementId_Row_4 { get; set; }
        public DateTime? QBusinessCollateralAgmLineT1S2_RefAgreementDate_Row_4 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightLesseeFourth1")]
        public string QBusinessCollateralAgmLineT1S2_Lessee_Row_4 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightLessorFourth1")]
        public string QBusinessCollateralAgmLineT1S2_Lessor_Row_4 { get; set; }

        [BookmarkMapping("BCLeaseholdsRightIDFifth1")]
        public string QBusinessCollateralAgmLineT1S2_RefAgreementId_Row_5 { get; set; }
        public DateTime? QBusinessCollateralAgmLineT1S2_RefAgreementDate_Row_5 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightLesseeFifth1")]
        public string QBusinessCollateralAgmLineT1S2_Lessee_Row_5 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightLessorFifth1")]
        public string QBusinessCollateralAgmLineT1S2_Lessor_Row_5 { get; set; }


        public DateTime? QBusinessCollateralAgmLineT1S2_RefAgreementDate_Row_6 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightIDSixth1")]
        public string QBusinessCollateralAgmLineT1S2_RefAgreementId_Row_6 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightLesseeSixth1")]
        public string QBusinessCollateralAgmLineT1S2_Lessee_Row_6 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightLessorSixth1")]
        public string QBusinessCollateralAgmLineT1S2_Lessor_Row_6 { get; set; }


        public DateTime? QBusinessCollateralAgmLineT1S2_RefAgreementDate_Row_7 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightIDSeven1")]
        public string QBusinessCollateralAgmLineT1S2_RefAgreementId_Row_7 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightLesseeSeven1")]
        public string QBusinessCollateralAgmLineT1S2_Lessee_Row_7 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightLessorSeven1")]
        public string QBusinessCollateralAgmLineT1S2_Lessor_Row_7 { get; set; }


        public DateTime? QBusinessCollateralAgmLineT1S2_RefAgreementDate_Row_8 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightIDEighth1")]
        public string QBusinessCollateralAgmLineT1S2_RefAgreementId_Row_8 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightLesseeEighth1")]
        public string QBusinessCollateralAgmLineT1S2_Lessee_Row_8 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightLessorEighth1")]
        public string QBusinessCollateralAgmLineT1S2_Lessor_Row_8 { get; set; }


        public DateTime? QBusinessCollateralAgmLineT1S2_RefAgreementDate_Row_9 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightIDNineth1")]
        public string QBusinessCollateralAgmLineT1S2_RefAgreementId_Row_9 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightLesseeNineth1")]
        public string QBusinessCollateralAgmLineT1S2_Lessee_Row_9 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightLessorNineth1")]
        public string QBusinessCollateralAgmLineT1S2_Lessor_Row_9 { get; set; }


        public DateTime? QBusinessCollateralAgmLineT1S2_RefAgreementDate_Row_10 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightIDTenth1")]
        public string QBusinessCollateralAgmLineT1S2_RefAgreementId_Row_10 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightLesseeTenth1")]
        public string QBusinessCollateralAgmLineT1S2_Lessee_Row_10 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightLessorTenth1")]
        public string QBusinessCollateralAgmLineT1S2_Lessor_Row_10 { get; set; }
        #endregion QBusinessCollateralAgmLineT1S2

        #region QBusinessCollateralAgmLineT1S3

        [BookmarkMapping("BCRightofClaimDesFirst1")]
        public string QBusinessCollateralAgmLineT1S3_Description { get; set; }
        [BookmarkMapping("BCRightofClaimIDFirst1")]
        public string QBusinessCollateralAgmLineT1S3_RefAgreementId { get; set; }
        public DateTime? QBusinessCollateralAgmLineT1S3_RefAgreementDate { get; set; }
        [BookmarkMapping("BCRightofClaimLesseeFirst1")]
        public string QBusinessCollateralAgmLineT1S3_Lessee { get; set; }
        [BookmarkMapping("BCRightofClaimLessorFirst1")]
        public string QBusinessCollateralAgmLineT1S3_Lessor { get; set; }
        [BookmarkMapping("BCNameSubtypeRightClaim1")]
        public string QBusinessCollateralAgmLineT1S3_SubTypeId { get; set; }

        [BookmarkMapping("BCSumSubtypeRightClaim1")]
        public decimal QBusinessCollateralAgmLineT1S3_SumBusinessCollateralValue { get; set; }
        [BookmarkMapping("SubtypeTextAttachmentThird1")]
        public string QBusinessCollateralAgmLineT1S3_AttachmentText { get; set; }
        [BookmarkMapping("SubtypeQtyThird1")]
        public int QBusinessCollateralAgmLineT1S3_ConutLine { get; set; }
        [BookmarkMapping("SubtypePreferentialThird1")]
        public int QBusinessCollateralAgmLineT1S3_PreferentialCreditorNumber { get; set; }
        [BookmarkMapping("TextRefAgeement1")]
        public string QBusinessCollateralAgmLineT1S3_Product { get; set; }
        public int QBusinessCollateralAgmLineT1S3_Row { get; set; }

        [BookmarkMapping("BCRightofClaimDesSecond1")]
        public string QBusinessCollateralAgmLineT1S3_Description_Row_2 { get; set; }
        [BookmarkMapping("BCRightofClaimIDSecond1")]
        public string QBusinessCollateralAgmLineT1S3_RefAgreementId_Row_2 { get; set; }
        public DateTime? QBusinessCollateralAgmLineT1S3_RefAgreementDate_Row_2 { get; set; }
        [BookmarkMapping("BCRightofClaimLesseeSecond1")]
        public string QBusinessCollateralAgmLineT1S3_Lessee_Row_2 { get; set; }
        [BookmarkMapping("BCRightofClaimLessorSecond1")]
        public string QBusinessCollateralAgmLineT1S3_Lessor_Row_2 { get; set; }
        [BookmarkMapping("TextRefAgeement2")]
        public string QBusinessCollateralAgmLineT1S3_Product_Row_2 { get; set; }

        [BookmarkMapping("BCRightofClaimDesThird1")]
        public string QBusinessCollateralAgmLineT1S3_Description_Row_3 { get; set; }
        [BookmarkMapping("BCRightofClaimIDThird1")]
        public string QBusinessCollateralAgmLineT1S3_RefAgreementId_Row_3 { get; set; }
        public DateTime? QBusinessCollateralAgmLineT1S3_RefAgreementDate_Row_3 { get; set; }
        [BookmarkMapping("BCRightofClaimLesseeThird1")]
        public string QBusinessCollateralAgmLineT1S3_Lessee_Row_3 { get; set; }
        [BookmarkMapping("BCRightofClaimLessorThird1")]
        public string QBusinessCollateralAgmLineT1S3_Lessor_Row_3 { get; set; }
        [BookmarkMapping("TextRefAgeement3")]
        public string QBusinessCollateralAgmLineT1S3_Product_Row_3 { get; set; }

        [BookmarkMapping("BCRightofClaimDesFourth1")]
        public string QBusinessCollateralAgmLineT1S3_Description_Row_4 { get; set; }
        [BookmarkMapping("BCRightofClaimIDFourth1")]
        public string QBusinessCollateralAgmLineT1S3_RefAgreementId_Row_4 { get; set; }
        public DateTime? QBusinessCollateralAgmLineT1S3_RefAgreementDate_Row_4 { get; set; }
        [BookmarkMapping("BCRightofClaimLesseeFourth1")]
        public string QBusinessCollateralAgmLineT1S3_Lessee_Row_4 { get; set; }
        [BookmarkMapping("BCRightofClaimLessorFourth1")]
        public string QBusinessCollateralAgmLineT1S3_Lessor_Row_4 { get; set; }
        [BookmarkMapping("TextRefAgeement4")]
        public string QBusinessCollateralAgmLineT1S3_Product_Row_4 { get; set; }

        [BookmarkMapping("BCRightofClaimDesFifth1")]
        public string QBusinessCollateralAgmLineT1S3_Description_Row_5 { get; set; }
        [BookmarkMapping("BCRightofClaimIDFifth1")]
        public string QBusinessCollateralAgmLineT1S3_RefAgreementId_Row_5 { get; set; }
        public DateTime? QBusinessCollateralAgmLineT1S3_RefAgreementDate_Row_5 { get; set; }
        [BookmarkMapping("BCRightofClaimLesseeFifth1")]
        public string QBusinessCollateralAgmLineT1S3_Lessee_Row_5 { get; set; }
        [BookmarkMapping("BCRightofClaimLessorFifth1")]
        public string QBusinessCollateralAgmLineT1S3_Lessor_Row_5 { get; set; }
        [BookmarkMapping("TextRefAgeement5")]
        public string QBusinessCollateralAgmLineT1S3_Product_Row_5 { get; set; }

        [BookmarkMapping("BCRightofClaimDesSixth1")]
        public string QBusinessCollateralAgmLineT1S3_Description_Row_6 { get; set; }
        [BookmarkMapping("BCRightofClaimIDSixth1")]
        public string QBusinessCollateralAgmLineT1S3_RefAgreementId_Row_6 { get; set; }
        public DateTime? QBusinessCollateralAgmLineT1S3_RefAgreementDate_Row_6 { get; set; }
        [BookmarkMapping("BCRightofClaimLesseeSixth1")]
        public string QBusinessCollateralAgmLineT1S3_Lessee_Row_6 { get; set; }
        [BookmarkMapping("BCRightofClaimLessorSixth1")]
        public string QBusinessCollateralAgmLineT1S3_Lessor_Row_6 { get; set; }
        [BookmarkMapping("TextRefAgeement6")]
        public string QBusinessCollateralAgmLineT1S3_Product_Row_6 { get; set; }

        [BookmarkMapping("BCRightofClaimDesSeventh1")]
        public string QBusinessCollateralAgmLineT1S3_Description_Row_7 { get; set; }
        [BookmarkMapping("BCRightofClaimIDSeventh1")]
        public string QBusinessCollateralAgmLineT1S3_RefAgreementId_Row_7 { get; set; }
        public DateTime? QBusinessCollateralAgmLineT1S3_RefAgreementDate_Row_7 { get; set; }
        [BookmarkMapping("BCRightofClaimLesseeSeventh1")]
        public string QBusinessCollateralAgmLineT1S3_Lessee_Row_7 { get; set; }
        [BookmarkMapping("BCRightofClaimLessorSeventh1")]
        public string QBusinessCollateralAgmLineT1S3_Lessor_Row_7 { get; set; }
        [BookmarkMapping("TextRefAgeement7")]
        public string QBusinessCollateralAgmLineT1S3_Product_Row_7 { get; set; }

        [BookmarkMapping("BCRightofClaimDesEighth1")]
        public string QBusinessCollateralAgmLineT1S3_Description_Row_8 { get; set; }
        [BookmarkMapping("BCRightofClaimIDEighth1")]
        public string QBusinessCollateralAgmLineT1S3_RefAgreementId_Row_8 { get; set; }
        public DateTime? QBusinessCollateralAgmLineT1S3_RefAgreementDate_Row_8 { get; set; }
        [BookmarkMapping("BCRightofClaimLesseeEighth1")]
        public string QBusinessCollateralAgmLineT1S3_Lessee_Row_8 { get; set; }
        [BookmarkMapping("BCRightofClaimLessorEighth1")]
        public string QBusinessCollateralAgmLineT1S3_Lessor_Row_8 { get; set; }
        [BookmarkMapping("TextRefAgeement8")]
        public string QBusinessCollateralAgmLineT1S3_Product_Row_8 { get; set; }

        [BookmarkMapping("BCRightofClaimDesNineth1")]
        public string QBusinessCollateralAgmLineT1S3_Description_Row_9 { get; set; }
        [BookmarkMapping("BCRightofClaimIDNineth1")]
        public string QBusinessCollateralAgmLineT1S3_RefAgreementId_Row_9 { get; set; }
        public DateTime? QBusinessCollateralAgmLineT1S3_RefAgreementDate_Row_9 { get; set; }
        [BookmarkMapping("BCRightofClaimLesseeNineth1")]
        public string QBusinessCollateralAgmLineT1S3_Lessee_Row_9 { get; set; }
        [BookmarkMapping("BCRightofClaimLessorNineth1")]
        public string QBusinessCollateralAgmLineT1S3_Lessor_Row_9 { get; set; }
        [BookmarkMapping("TextRefAgeement9")]
        public string QBusinessCollateralAgmLineT1S3_Product_Row_9 { get; set; }

        [BookmarkMapping("BCRightofClaimDesTenth1")]
        public string QBusinessCollateralAgmLineT1S3_Description_Row_10 { get; set; }
        [BookmarkMapping("BCRightofClaimIDTenth1")]
        public string QBusinessCollateralAgmLineT1S3_RefAgreementId_Row_10 { get; set; }
        public DateTime? QBusinessCollateralAgmLineT1S3_RefAgreementDate_Row_10 { get; set; }
        [BookmarkMapping("BCRightofClaimLesseeTenth1")]
        public string QBusinessCollateralAgmLineT1S3_Lessee_Row_10 { get; set; }
        [BookmarkMapping("BCRightofClaimLessorTenth1")]
        public string QBusinessCollateralAgmLineT1S3_Lessor_Row_10 { get; set; }
        [BookmarkMapping("TextRefAgeement10")]
        public string QBusinessCollateralAgmLineT1S3_Product_Row_10 { get; set; }
        #endregion QBusinessCollateralAgmLineT1S3

        #region QBusinessCollateralAgmLineT2S1

        [BookmarkMapping("BCNameSubtypeARFirst1")]
        public string QBusinessCollateralAgmLineT2S1_SubTypeId { get; set; }
        [BookmarkMapping("BCSumSubtypeARFirst1")]
        public decimal QBusinessCollateralAgmLineT2S1_SumBusinessCollateralValue { get; set; }
        [BookmarkMapping("SubtypeTextAttachmentFourth1")]
        public string QBusinessCollateralAgmLineT2S1_AttachmentText { get; set; }
        [BookmarkMapping("SubtypeQtyFourth1")]
        public int QBusinessCollateralAgmLineT2S1_ConutLine { get; set; }
        [BookmarkMapping("SubtypePreferentialFourth1")]
        public int QBusinessCollateralAgmLineT2S1_PreferentialCreditorNumber { get; set; }
        public int QBusinessCollateralAgmLineT2S1_Row { get; set; }

        #region Row 
        [BookmarkMapping("ATBuyerNameFirst1")]
        public string QBusinessCollateralAgmLineT2S1_BuyerName_Row_1 { get; set; }
        public int QBusinessCollateralAgmLineT2S1_IdentificationType_Row_1 { get; set; }
        public string QBusinessCollateralAgmLineT2S1_TaxId_Row_1 { get; set; }
        public string QBusinessCollateralAgmLineT2S1_PassportId_Row_1 { get; set; }

        [BookmarkMapping("ATBuyerNameSecond1")]
        public string QBusinessCollateralAgmLineT2S1_BuyerName_Row_2 { get; set; }
        public int QBusinessCollateralAgmLineT2S1_IdentificationType_Row_2 { get; set; }
        public string QBusinessCollateralAgmLineT2S1_TaxId_Row_2 { get; set; }
        public string QBusinessCollateralAgmLineT2S1_PassportId_Row_2 { get; set; }

        [BookmarkMapping("ATBuyerNameThird1")]
        public string QBusinessCollateralAgmLineT2S1_BuyerName_Row_3 { get; set; }
        public int QBusinessCollateralAgmLineT2S1_IdentificationType_Row_3 { get; set; }
        public string QBusinessCollateralAgmLineT2S1_TaxId_Row_3 { get; set; }
        public string QBusinessCollateralAgmLineT2S1_PassportId_Row_3 { get; set; }

        [BookmarkMapping("ATBuyerNameFourth1")]
        public string QBusinessCollateralAgmLineT2S1_BuyerName_Row_4 { get; set; }
        public int QBusinessCollateralAgmLineT2S1_IdentificationType_Row_4 { get; set; }
        public string QBusinessCollateralAgmLineT2S1_TaxId_Row_4 { get; set; }
        public string QBusinessCollateralAgmLineT2S1_PassportId_Row_4 { get; set; }

        [BookmarkMapping("ATBuyerNameFifth1")]
        public string QBusinessCollateralAgmLineT2S1_BuyerName_Row_5 { get; set; }
        public int QBusinessCollateralAgmLineT2S1_IdentificationType_Row_5 { get; set; }
        public string QBusinessCollateralAgmLineT2S1_TaxId_Row_5 { get; set; }
        public string QBusinessCollateralAgmLineT2S1_PassportId_Row_5 { get; set; }

        [BookmarkMapping("ATBuyerNameSixth1")]
        public string QBusinessCollateralAgmLineT2S1_BuyerName_Row_6 { get; set; }
        public int QBusinessCollateralAgmLineT2S1_IdentificationType_Row_6 { get; set; }
        public string QBusinessCollateralAgmLineT2S1_TaxId_Row_6 { get; set; }
        public string QBusinessCollateralAgmLineT2S1_PassportId_Row_6 { get; set; }

        [BookmarkMapping("ATBuyerNameSeventh1")]
        public string QBusinessCollateralAgmLineT2S1_BuyerName_Row_7 { get; set; }
        public int QBusinessCollateralAgmLineT2S1_IdentificationType_Row_7 { get; set; }
        public string QBusinessCollateralAgmLineT2S1_TaxId_Row_7 { get; set; }
        public string QBusinessCollateralAgmLineT2S1_PassportId_Row_7 { get; set; }

        [BookmarkMapping("ATBuyerNameEighth1")]
        public string QBusinessCollateralAgmLineT2S1_BuyerName_Row_8 { get; set; }
        public int QBusinessCollateralAgmLineT2S1_IdentificationType_Row_8 { get; set; }
        public string QBusinessCollateralAgmLineT2S1_TaxId_Row_8 { get; set; }
        public string QBusinessCollateralAgmLineT2S1_PassportId_Row_8 { get; set; }

        [BookmarkMapping("ATBuyerNameNineth1")]
        public string QBusinessCollateralAgmLineT2S1_BuyerName_Row_9 { get; set; }
        public int QBusinessCollateralAgmLineT2S1_IdentificationType_Row_9 { get; set; }
        public string QBusinessCollateralAgmLineT2S1_TaxId_Row_9 { get; set; }
        public string QBusinessCollateralAgmLineT2S1_PassportId_Row_9 { get; set; }

        [BookmarkMapping("ATBuyerNameTenth1")]
        public string QBusinessCollateralAgmLineT2S1_BuyerName_Row_10 { get; set; }
        public int QBusinessCollateralAgmLineT2S1_IdentificationType_Row_10 { get; set; }
        public string QBusinessCollateralAgmLineT2S1_TaxId_Row_10 { get; set; }
        public string QBusinessCollateralAgmLineT2S1_PassportId_Row_10 { get; set; }
        #endregion Row 

        #endregion QBusinessCollateralAgmLineT2S1

        #region QBusinessCollateralAgmLineT2S2
        [BookmarkMapping("BCNameSubtypeARSecond1")]
        public string QBusinessCollateralAgmLineT2S2_SubTypeId { get; set; }
        [BookmarkMapping("BCSumSubtypeARSecond1")]
        public decimal QBusinessCollateralAgmLineT2S2_SumBusinessCollateralValue { get; set; }
        [BookmarkMapping("SubtypeTextAttachmentFifth1")]
        public string QBusinessCollateralAgmLineT2S2_AttachmentText { get; set; }
        [BookmarkMapping("SubtypeQtyFifth1")]
        public int QBusinessCollateralAgmLineT2S2_ConutLine { get; set; }
        [BookmarkMapping("SubtypePreferentialFifth1")]
        public int QBusinessCollateralAgmLineT2S2_PreferentialCreditorNumber { get; set; }
        public int QBusinessCollateralAgmLineT2S2_Row { get; set; }

        #region variable

        [BookmarkMapping("BCLeaseholdsRightDateFirst1")]
        public string Variable_BCLeaseholdsRightDateFirst1 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightDateSecond1")]
        public string Variable_BCLeaseholdsRightDateSecond1 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightDateThird1")]
        public string Variable_BCLeaseholdsRightDateThird1 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightDateFourth1")]
        public string Variable_BCLeaseholdsRightDateFourth1 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightDateFifth1")]
        public string Variable_BCLeaseholdsRightDateFifth1 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightDateSixth1")]
        public string Variable_BCLeaseholdsRightDateSixth1 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightDateSeven1")]
        public string Variable_BCLeaseholdsRightDateSeven1 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightDateEighth1")]
        public string Variable_BCLeaseholdsRightDateEighth1 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightDateNineth1")]
        public string Variable_BCLeaseholdsRightDateNineth1 { get; set; }
        [BookmarkMapping("BCLeaseholdsRightDateTenth1")]
        public string Variable_BCLeaseholdsRightDateTenth1 { get; set; }



        [BookmarkMapping("BCRightofClaimDateFirst1")]
        public string Variable_BCRightofClaimDateFirst1 { get; set; }
        [BookmarkMapping("BCRightofClaimDateSecond1")]
        public string Variable_BCRightofClaimDateSecond1 { get; set; }
        [BookmarkMapping("BCRightofClaimDateThird1")]
        public string Variable_BCRightofClaimDateThird1 { get; set; }

        [BookmarkMapping("BCRightofClaimDateFourth1")]
        public string Variable_BCRightofClaimDateFourth1 { get; set; }
        [BookmarkMapping("BCRightofClaimDateFifth1")]
        public string Variable_BCRightofClaimDateFifth1 { get; set; }
        [BookmarkMapping("BCRightofClaimDateSixth1")]
        public string Variable_BCRightofClaimDateSixth1 { get; set; }
        [BookmarkMapping("BCRightofClaimDateSeventh1")]
        public string Variable_BCRightofClaimDateSeventh1 { get; set; }
        [BookmarkMapping("BCRightofClaimDateEighth1")]
        public string Variable_BCRightofClaimDateEighth1 { get; set; }
        [BookmarkMapping("BCRightofClaimDateNineth1")]
        public string Variable_BCRightofClaimDateNineth1 { get; set; }
        [BookmarkMapping("BCRightofClaimDateTenth1")]
        public string Variable_BCRightofClaimDateTenth1 { get; set; }

        [BookmarkMapping("CollateralDate1", "CollateralDate2", "CollateralDate3", "CollateralDate4", "CollateralDate5", "CollateralDate6", "CollateralDate7", "CollateralDate8", "CollateralDate9", "CollateralDate10", "CollateralDate11", "CollateralDate12", "CollateralDate13", "CollateralDate14", "CollateralDate15", "CollateralDate16", "CollateralDate17", "CollateralDate18", "CollateralDate19", "CollateralDate20", "CollateralDate21", "CollateralDate22", "CollateralDate23", "CollateralDate24", "CollateralDate25", "CollateralDate26")]
        public string Variable_CollateralDate { get; set; }
        

        [BookmarkMapping("BCBuyerTaxIDFirst1")]
        public string Variable_BCBuyerTaxIDFirst1 { get; set; }
        [BookmarkMapping("BCBuyerTaxIDSecond1")]
        public string Variable_BCBuyerTaxIDSecond1 { get; set; }
        [BookmarkMapping("BCBuyerTaxIDThird1")]
        public string Variable_BCBuyerTaxIDThird1 { get; set; }
        [BookmarkMapping("BCBuyerTaxIDFourth1")]
        public string Variable_BCBuyerTaxIDFourth1 { get; set; }
        [BookmarkMapping("BCBuyerTaxIDFifth1")]
        public string Variable_BCBuyerTaxIDFifth1 { get; set; }
        [BookmarkMapping("BCBuyerTaxIDSixth1")]
        public string Variable_BCBuyerTaxIDSixth1 { get; set; }
        [BookmarkMapping("BCBuyerTaxIDSeventh1")]
        public string Variable_BCBuyerTaxIDSeventh1 { get; set; }
        [BookmarkMapping("BCBuyerTaxIDEighth1")]
        public string Variable_BCBuyerTaxIDEighth1 { get; set; }
        [BookmarkMapping("BCBuyerTaxIDNineth1")]
        public string Variable_BCBuyerTaxIDNineth1 { get; set; }
        [BookmarkMapping("BCBuyerTaxIDTenth1")]
        public string Variable_BCBuyerTaxIDTenth1 { get; set; }

        [BookmarkMapping("BCTaxIdARFirst1")]
        public string Variable_BCTaxIdARFirst1 { get; set; }
        [BookmarkMapping("BCTaxIdARSecond1")]
        public string Variable_BCTaxIdARSecond1 { get; set; }
        [BookmarkMapping("BCTaxIdARThird1")]
        public string Variable_BCTaxIdARThird1 { get; set; }
        [BookmarkMapping("BCTaxIdARFourth1")]
        public string Variable_BCTaxIdARFourth1 { get; set; }
        [BookmarkMapping("BCTaxIdARFifth1")]
        public string Variable_BCTaxIdARFifth1 { get; set; }
        [BookmarkMapping("BCTaxIdARSixth1")]
        public string Variable_BCTaxIdARSixth1 { get; set; }
        [BookmarkMapping("BCTaxIdARSeventh1")]
        public string Variable_BCTaxIdARSeventh1 { get; set; }
        [BookmarkMapping("BCTaxIdAREighth1")]
        public string Variable_BCTaxIdAREighth1 { get; set; }
        [BookmarkMapping("BCTaxIdARNineth1")]
        public string Variable_BCTaxIdARNineth1 { get; set; }
        [BookmarkMapping("BCTaxIdARTenth1")]
        public string Variable_BCTaxIdARTenth1 { get; set; }



        [BookmarkMapping("BCMachineAmountTextFirst1")]
        public string Variable_BCMachineAmountTextFirst1 { get; set; }
        [BookmarkMapping("BCMachineAmountTextSecond1")]
        public string Variable_BCMachineAmountTextSecond1 { get; set; }
        [BookmarkMapping("BCMachineAmountTextThird1")]
        public string Variable_BCMachineAmountTextThird1 { get; set; }
        [BookmarkMapping("BCMachineAmountTextFourth1")]
        public string Variable_BCMachineAmountTextFourth1 { get; set; }
        [BookmarkMapping("BCMachineAmountTextFifth1")]
        public string Variable_BCMachineAmountTextFifth1 { get; set; }

        [BookmarkMapping("BCInvenAmountTextFirst1")]
        public string Variable_BCInvenAmountTextFirst1 { get; set; }
        [BookmarkMapping("BCInvenAmountTextSecond1")]
        public string Variable_BCInvenAmountTextSecond1 { get; set; }
        [BookmarkMapping("BCInvenAmountTextThird1")]
        public string Variable_BCInvenAmountTextThird1 { get; set; }
        [BookmarkMapping("BCInvenAmountTextFourth1")]
        public string Variable_BCInvenAmountTextFourth1 { get; set; }
        [BookmarkMapping("BCInvenAmountTextFifth1")]
        public string Variable_BCInvenAmountTextFifth1 { get; set; }

        [BookmarkMapping("BCMaterialAmountTextFirst1")]
        public string Variable_BCMaterialAmountTextFirst1 { get; set; }
        [BookmarkMapping("BCMaterialAmountTextSecond1")]
        public string Variable_BCMaterialAmountTextSecond1 { get; set; }
        [BookmarkMapping("BCMaterialAmountTextThird1")]
        public string Variable_BCMaterialAmountTextThird1 { get; set; }
        [BookmarkMapping("BCMaterialAmountTextFourth1")]
        public string Variable_BCMaterialAmountTextFourth1 { get; set; }
        [BookmarkMapping("BCMaterialAmountTextFifth1")]
        public string Variable_BCMaterialAmountTextFifth1 { get; set; }

        [BookmarkMapping("BCCarAmountTextFirst1")]
        public string Variable_BCCarAmountTextFirst1 { get; set; }
        [BookmarkMapping("BCCarAmountTextSecond1")]
        public string Variable_BCCarAmountTextSecond1 { get; set; }
        [BookmarkMapping("BCCarAmountTextThird1")]
        public string Variable_BCCarAmountTextThird1 { get; set; }
        [BookmarkMapping("BCCarAmountTextFourth1")]
        public string Variable_BCCarAmountTextFourth1 { get; set; }
        [BookmarkMapping("BCCarAmountTextFifth1")]
        public string Variable_BCCarAmountTextFifth1 { get; set; }

        [BookmarkMapping("BCOtherAmountTextFirst1")]
        public string Variable_BCOtherAmountTextFirst1 { get; set; }
        [BookmarkMapping("BCOtherAmountTextSecond1")]
        public string Variable_BCOtherAmountTextSecond1 { get; set; }
        [BookmarkMapping("BCOtherAmountTextThird1")]
        public string Variable_BCOtherAmountTextThird1 { get; set; }
        [BookmarkMapping("BCOtherAmountTextFourth1")]
        public string Variable_BCOtherAmountTextFourth1 { get; set; }
        [BookmarkMapping("BCOtherAmountTextFifth1")]
        public string Variable_BCOtherAmountTextFifth1 { get; set; }

        [BookmarkMapping("BCIntellAssetsAmountTextFirst1")]
        public string Variable_BCIntellAssetsAmountTextFirst1 { get; set; }
        [BookmarkMapping("BCIntellAssetsAmountTextSecond1")]
        public string Variable_BCIntellAssetsAmountTextSecond1 { get; set; }
        [BookmarkMapping("BCIntellAssetsAmountTextThird1")]
        public string Variable_BCIntellAssetsAmountTextThird1 { get; set; }
        [BookmarkMapping("BCIntellAssetsAmountTextFour1")]
        public string Variable_BCIntellAssetsAmountTextFour1 { get; set; }



        [BookmarkMapping("BCCheckNotRegisFirst1")]
        public string Variable_NotRegisFirst { get; set; }
        [BookmarkMapping("BCCheckAlreadyRegisFirst1")]
        public string Variable_AlreadyRegisFirst { get; set; }


        [BookmarkMapping("BCCheckNotRegisSecond1")]
        public string Variable_NotRegisSecond { get; set; }
        [BookmarkMapping("BCCheckAlreadyRegisSecond1")]
        public string Variable_AlreadyRegisSecond { get; set; }


        [BookmarkMapping("BCCheckNotRegisThird1")]
        public string Variable_NotRegisThird { get; set; }
        [BookmarkMapping("BCCheckAlreadyRegisThird1")]
        public string Variable_AlreadyRegisThird { get; set; }


        [BookmarkMapping("BCCheckNotRegisFourth1")]
        public string Variable_NotRegisFourth { get; set; }
        [BookmarkMapping("BCCheckAlreadyRegisFourth1")]
        public string Variable_AlreadyRegisFourth { get; set; }


        [BookmarkMapping("BCCheckNotRegisFifth1")]
        public string Variable_NotRegisFifth { get; set; }
        [BookmarkMapping("BCCheckAlreadyRegisFifth1")]
        public string Variable_AlreadyRegisFifth { get; set; }

        #endregion variable

        #region Row
        [BookmarkMapping("BCARNameFirst1", "BCNameARFirst1")]
        public string QBusinessCollateralAgmLineT2S2_BuyerName_Row_1 { get; set; }
        public int QBusinessCollateralAgmLineT2S2_IdentificationType_Row_1 { get; set; }
        public string QBusinessCollateralAgmLineT2S2_TaxId_Row_1 { get; set; }
        public string QBusinessCollateralAgmLineT2S2_PassportId_Row_1 { get; set; }

        [BookmarkMapping("BCNameARSecond1")]
        public string QBusinessCollateralAgmLineT2S2_BuyerName_Row_2 { get; set; }
        public int QBusinessCollateralAgmLineT2S2_IdentificationType_Row_2 { get; set; }
        public string QBusinessCollateralAgmLineT2S2_TaxId_Row_2 { get; set; }
        public string QBusinessCollateralAgmLineT2S2_PassportId_Row_2 { get; set; }


        [BookmarkMapping("BCNameARThird1")]
        public string QBusinessCollateralAgmLineT2S2_BuyerName_Row_3 { get; set; }
        public int QBusinessCollateralAgmLineT2S2_IdentificationType_Row_3 { get; set; }
        public string QBusinessCollateralAgmLineT2S2_TaxId_Row_3 { get; set; }
        public string QBusinessCollateralAgmLineT2S2_PassportId_Row_3 { get; set; }


        [BookmarkMapping("BCNameARFourth1")]
        public string QBusinessCollateralAgmLineT2S2_BuyerName_Row_4 { get; set; }
        public int QBusinessCollateralAgmLineT2S2_IdentificationType_Row_4 { get; set; }
        public string QBusinessCollateralAgmLineT2S2_TaxId_Row_4 { get; set; }
        public string QBusinessCollateralAgmLineT2S2_PassportId_Row_4 { get; set; }

        [BookmarkMapping("BCNameARFifth1")]
        public string QBusinessCollateralAgmLineT2S2_BuyerName_Row_5 { get; set; }
        public int QBusinessCollateralAgmLineT2S2_IdentificationType_Row_5 { get; set; }
        public string QBusinessCollateralAgmLineT2S2_TaxId_Row_5 { get; set; }
        public string QBusinessCollateralAgmLineT2S2_PassportId_Row_5 { get; set; }

        [BookmarkMapping("BCNameARSixth1")]
        public string QBusinessCollateralAgmLineT2S2_BuyerName_Row_6 { get; set; }
        public int QBusinessCollateralAgmLineT2S2_IdentificationType_Row_6 { get; set; }
        public string QBusinessCollateralAgmLineT2S2_TaxId_Row_6 { get; set; }
        public string QBusinessCollateralAgmLineT2S2_PassportId_Row_6 { get; set; }

        [BookmarkMapping("BCNameARSeventh1")]
        public string QBusinessCollateralAgmLineT2S2_BuyerName_Row_7 { get; set; }
        public int QBusinessCollateralAgmLineT2S2_IdentificationType_Row_7 { get; set; }
        public string QBusinessCollateralAgmLineT2S2_TaxId_Row_7 { get; set; }
        public string QBusinessCollateralAgmLineT2S2_PassportId_Row_7 { get; set; }

        [BookmarkMapping("BCNameAREighth1")]
        public string QBusinessCollateralAgmLineT2S2_BuyerName_Row_8 { get; set; }
        public int QBusinessCollateralAgmLineT2S2_IdentificationType_Row_8 { get; set; }
        public string QBusinessCollateralAgmLineT2S2_TaxId_Row_8 { get; set; }
        public string QBusinessCollateralAgmLineT2S2_PassportId_Row_8 { get; set; }

        [BookmarkMapping("BCNameARNineth1")]
        public string QBusinessCollateralAgmLineT2S2_BuyerName_Row_9 { get; set; }
        public int QBusinessCollateralAgmLineT2S2_IdentificationType_Row_9 { get; set; }
        public string QBusinessCollateralAgmLineT2S2_TaxId_Row_9 { get; set; }
        public string QBusinessCollateralAgmLineT2S2_PassportId_Row_9 { get; set; }

        [BookmarkMapping("BCNameARTenth1")]
        public string QBusinessCollateralAgmLineT2S2_BuyerName_Row_10 { get; set; }
        public int QBusinessCollateralAgmLineT2S2_IdentificationType_Row_10 { get; set; }
        public string QBusinessCollateralAgmLineT2S2_TaxId_Row_10 { get; set; }
        public string QBusinessCollateralAgmLineT2S2_PassportId_Row_10 { get; set; }

        #endregion Row


        #endregion QBusinessCollateralAgmLineT2S2

        #region QBusinessCollateralAgmLineT3S1

        [BookmarkMapping("BCMachineDesFirst1")]
        public string QBusinessCollateralAgmLineT3S1_Product { get; set; }
        [BookmarkMapping("BCMachinePlateNumFirst1")]
        public string QBusinessCollateralAgmLineT3S1_RegistrationPlateNumber { get; set; }
        [BookmarkMapping("BCMachineNumberFirst1")]
        public string QBusinessCollateralAgmLineT3S1_MachineNumber { get; set; }
  
        public string QBusinessCollateralAgmLineT3S1_MachineRegisteredStatus { get; set; }
        [BookmarkMapping("BCMachineRegisPlaceFirst1")]
        public string QBusinessCollateralAgmLineT3S1_RegisteredPlace { get; set; }
        [BookmarkMapping("BCMachineAmountFirst1")]
        public decimal QBusinessCollateralAgmLineT3S1_BusinessCollateralValue { get; set; }
        [BookmarkMapping("BCNameSubtypeMachine1")]
        public string QBusinessCollateralAgmLineT3S1_SubTypeId { get; set; }
        [BookmarkMapping("BCSumSubtypeMachine1")]
        public decimal QBusinessCollateralAgmLineT3S1_SumBusinessCollateralValue { get; set; }
        [BookmarkMapping("SubtypeTextAttachmentSixth1")]
        public string QBusinessCollateralAgmLineT3S1_AttachmentText { get; set; }
        [BookmarkMapping("SubtypeQtySixth1")]
        public int QBusinessCollateralAgmLineT3S1_ConutLine { get; set; }
        [BookmarkMapping("SubtypePreferentialSixth1")]
        public int QBusinessCollateralAgmLineT3S1_PreferentialCreditorNumber { get; set; }
        public int QBusinessCollateralAgmLineT3S1_Row { get; set; }
        [BookmarkMapping("UnitMachine1")]
        public string QBusinessCollateralAgmLineT3S1_Unit { get; set; }

        [BookmarkMapping("BCMachineDesSecond1")]
        public string QBusinessCollateralAgmLineT3S1_Product_Row_2 { get; set; }
        [BookmarkMapping("BCMachinePlateNumSecond1")]
        public string QBusinessCollateralAgmLineT3S1_RegistrationPlateNumber_Row_2 { get; set; }
        [BookmarkMapping("BCMachineNumberSecond1")]
        public string QBusinessCollateralAgmLineT3S1_MachineNumber_Row_2 { get; set; }
        
        public string QBusinessCollateralAgmLineT3S1_MachineRegisteredStatus_Row_2 { get; set; }
        [BookmarkMapping("BCMachineRegisPlaceSecond1")]
        public string QBusinessCollateralAgmLineT3S1_RegisteredPlace_Row_2 { get; set; }
        [BookmarkMapping("BCMachineAmountSecond1")]
        public decimal QBusinessCollateralAgmLineT3S1_BusinessCollateralValue_Row_2 { get; set; }
        [BookmarkMapping("UnitMachine2")]
        public string QBusinessCollateralAgmLineT3S1_Unit_Row_2 { get; set; }

        [BookmarkMapping("BCMachineDesThird1")]
        public string QBusinessCollateralAgmLineT3S1_Product_Row_3 { get; set; }
        [BookmarkMapping("BCMachinePlateNumThird1")]
        public string QBusinessCollateralAgmLineT3S1_RegistrationPlateNumber_Row_3 { get; set; }
        [BookmarkMapping("BCMachineNumberThird1")]
        public string QBusinessCollateralAgmLineT3S1_MachineNumber_Row_3 { get; set; }
        
        public string QBusinessCollateralAgmLineT3S1_MachineRegisteredStatus_Row_3 { get; set; }
        [BookmarkMapping("BCMachineRegisPlaceThird1")]
        public string QBusinessCollateralAgmLineT3S1_RegisteredPlace_Row_3 { get; set; }
        [BookmarkMapping("BCMachineAmountThird1")]
        public decimal QBusinessCollateralAgmLineT3S1_BusinessCollateralValue_Row_3 { get; set; }
        [BookmarkMapping("UnitMachine3")]
        public string QBusinessCollateralAgmLineT3S1_Unit_Row_3 { get; set; }

        [BookmarkMapping("BCMachineDesFourth1")]
        public string QBusinessCollateralAgmLineT3S1_Product_Row_4 { get; set; }
        [BookmarkMapping("BCMachinePlateNumFourth1")]
        public string QBusinessCollateralAgmLineT3S1_RegistrationPlateNumber_Row_4 { get; set; }
        [BookmarkMapping("BCMachineNumberFourth1")]
        public string QBusinessCollateralAgmLineT3S1_MachineNumber_Row_4 { get; set; }
        
        public string QBusinessCollateralAgmLineT3S1_MachineRegisteredStatus_Row_4 { get; set; }
        [BookmarkMapping("BCMachineRegisPlaceFourth1")]
        public string QBusinessCollateralAgmLineT3S1_RegisteredPlace_Row_4 { get; set; }
        [BookmarkMapping("BCMachineAmountFourth1")]
        public decimal QBusinessCollateralAgmLineT3S1_BusinessCollateralValue_Row_4 { get; set; }
        [BookmarkMapping("UnitMachine4")]
        public string QBusinessCollateralAgmLineT3S1_Unit_Row_4 { get; set; }

        [BookmarkMapping("BCMachineDesFifth1")]
        public string QBusinessCollateralAgmLineT3S1_Product_Row_5 { get; set; }
        [BookmarkMapping("BCMachinePlateNumFifth1")]
        public string QBusinessCollateralAgmLineT3S1_RegistrationPlateNumber_Row_5 { get; set; }
        [BookmarkMapping("BCMachineNumberFifth1")]
        public string QBusinessCollateralAgmLineT3S1_MachineNumber_Row_5 { get; set; }
        
        public string QBusinessCollateralAgmLineT3S1_MachineRegisteredStatus_Row_5 { get; set; }
        [BookmarkMapping("BCMachineRegisPlaceFifth1")]
        public string QBusinessCollateralAgmLineT3S1_RegisteredPlace_Row_5 { get; set; }
        [BookmarkMapping("BCMachineAmountFifth1")]
        public decimal QBusinessCollateralAgmLineT3S1_BusinessCollateralValue_Row_5 { get; set; }
        [BookmarkMapping("UnitMachine5")]
        public string QBusinessCollateralAgmLineT3S1_Unit_Row_5 { get; set; }

        public string QBusinessCollateralAgmLineT3S1_BCCheckAlreadyRegis { get; set; }
        public string QBusinessCollateralAgmLineT3S1_BCCheckNotRegis { get; set; }


        #endregion QBusinessCollateralAgmLineT3S1

        #region QBusinessCollateralAgmLineT3S2

        [BookmarkMapping("BCInvenDesFirst1")]
        public string QBusinessCollateralAgmLineT3S2_Product { get; set; }
        [BookmarkMapping("BCInvenQtyFirst1")]
        public decimal QBusinessCollateralAgmLineT3S2_Quantity { get; set; }
        [BookmarkMapping("BCInvenAmountFirst1")]
        public decimal QBusinessCollateralAgmLineT3S2_BusinessCollateralValue { get; set; }
        [BookmarkMapping("BCNameSubtypeInven1")]
        public string QBusinessCollateralAgmLineT3S2_SubTypeId { get; set; }
        [BookmarkMapping("BCSumSubtypeInven1")]
        public decimal QBusinessCollateralAgmLineT3S2_SumBusinessCollateralValue { get; set; }
        [BookmarkMapping("SubtypeTextAttachmentSeventh1")]
        public string QBusinessCollateralAgmLineT3S2_AttachmentText { get; set; }
        [BookmarkMapping("SubtypeQtySeventh1")]
        public int QBusinessCollateralAgmLineT3S2_ConutLine { get; set; }
        [BookmarkMapping("SubtypePreferentialSeventh1")]
        public int QBusinessCollateralAgmLineT3S2_PreferentialCreditorNumber { get; set; }
        public int QBusinessCollateralAgmLineT3S2_Row { get; set; }
        [BookmarkMapping("UnitInven1")]
        public string QBusinessCollateralAgmLineT3S2_Unit { get; set; }

        [BookmarkMapping("BCInvenDesSecond1")]
        public string QBusinessCollateralAgmLineT3S2_Product_Row_2 { get; set; }
        [BookmarkMapping("BCInvenQtySecond1")]
        public decimal QBusinessCollateralAgmLineT3S2_Quantity_Row_2 { get; set; }
        [BookmarkMapping("BCInvenAmountSecond1")]
        public decimal QBusinessCollateralAgmLineT3S2_BusinessCollateralValue_Row_2 { get; set; }
        [BookmarkMapping("UnitInven2")]
        public string QBusinessCollateralAgmLineT3S2_Unit_Row_2 { get; set; }

        [BookmarkMapping("BCInvenDesThird1")]
        public string QBusinessCollateralAgmLineT3S2_Product_Row_3 { get; set; }
        [BookmarkMapping("BCInvenQtyThird1")]
        public decimal QBusinessCollateralAgmLineT3S2_Quantity_Row_3 { get; set; }
        [BookmarkMapping("BCInvenAmountThird1")]
        public decimal QBusinessCollateralAgmLineT3S2_BusinessCollateralValue_Row_3 { get; set; }
        [BookmarkMapping("UnitInven3")]
        public string QBusinessCollateralAgmLineT3S2_Unit_Row_3 { get; set; }

        [BookmarkMapping("BCInvenDesFourth1")]
        public string QBusinessCollateralAgmLineT3S2_Product_Row_4 { get; set; }
        [BookmarkMapping("BCInvenQtyFourth1")]
        public decimal QBusinessCollateralAgmLineT3S2_Quantity_Row_4 { get; set; }
        [BookmarkMapping("BCInvenAmountFourth1")]
        public decimal QBusinessCollateralAgmLineT3S2_BusinessCollateralValue_Row_4 { get; set; }
        [BookmarkMapping("UnitInven4")]
        public string QBusinessCollateralAgmLineT3S2_Unit_Row_4 { get; set; }

        [BookmarkMapping("BCInvenDesFifth1")]
        public string QBusinessCollateralAgmLineT3S2_Product_Row_5 { get; set; }
        [BookmarkMapping("BCInvenQtyFifth1")]
        public decimal QBusinessCollateralAgmLineT3S2_Quantity_Row_5 { get; set; }
        [BookmarkMapping("BCInvenAmountFifth1")]
        public decimal QBusinessCollateralAgmLineT3S2_BusinessCollateralValue_Row_5 { get; set; }
        [BookmarkMapping("UnitInven5")]
        public string QBusinessCollateralAgmLineT3S2_Unit_Row_5 { get; set; }

        #endregion QBusinessCollateralAgmLineT3S2

        #region QBusinessCollateralAgmLineT3S3

        [BookmarkMapping("BCMaterialDesFirst1")]
        public string QBusinessCollateralAgmLineT3S3_Product { get; set; }
        [BookmarkMapping("BCMaterialQtyFirst1")]
        public decimal QBusinessCollateralAgmLineT3S3_Quantity { get; set; }
        [BookmarkMapping("BCMaterialAmountFirst1")]
        public decimal QBusinessCollateralAgmLineT3S3_BusinessCollateralValue { get; set; }
        [BookmarkMapping("BCNameSubtypeMaterial1")]
        public string QBusinessCollateralAgmLineT3S3_SubTypeId { get; set; }
        [BookmarkMapping("BCSumSubtypeMaterial1")]
        public decimal QBusinessCollateralAgmLineT3S3_SumBusinessCollateralValue { get; set; }
        [BookmarkMapping("SubtypeTextAttachmentEighth1")]
        public string QBusinessCollateralAgmLineT3S3_AttachmentText { get; set; }
        [BookmarkMapping("SubtypeQtyEighth1")]
        public int QBusinessCollateralAgmLineT3S3_ConutLine { get; set; }
        [BookmarkMapping("SubtypePreferentialEighth1")]
        public int QBusinessCollateralAgmLineT3S3_PreferentialCreditorNumber { get; set; }
        public int QBusinessCollateralAgmLineT3S3_Row { get; set; }
        [BookmarkMapping("UnitMaterial1")]
        public string QBusinessCollateralAgmLineT3S3_Unit { get; set; }

        [BookmarkMapping("BCMaterialDesSecond1")]
        public string QBusinessCollateralAgmLineT3S3_Product_Row_2 { get; set; }
        [BookmarkMapping("BCMaterialQtySecond1")]
        public decimal QBusinessCollateralAgmLineT3S3_Quantity_Row_2 { get; set; }
        [BookmarkMapping("BCMaterialAmountSecond1")]
        public decimal QBusinessCollateralAgmLineT3S3_BusinessCollateralValue_Row_2 { get; set; }
        [BookmarkMapping("UnitMaterial2")]
        public string QBusinessCollateralAgmLineT3S3_Unit_Row_2 { get; set; }

        [BookmarkMapping("BCMaterialDesThird1")]
        public string QBusinessCollateralAgmLineT3S3_Product_Row_3 { get; set; }
        [BookmarkMapping("BCMaterialQtyThird1")]
        public decimal QBusinessCollateralAgmLineT3S3_Quantity_Row_3 { get; set; }
        [BookmarkMapping("BCMaterialAmountThird1")]
        public decimal QBusinessCollateralAgmLineT3S3_BusinessCollateralValue_Row_3 { get; set; }
        [BookmarkMapping("UnitMaterial3")]
        public string QBusinessCollateralAgmLineT3S3_Unit_Row_3 { get; set; }

        [BookmarkMapping("BCMaterialDesFourth1")]
        public string QBusinessCollateralAgmLineT3S3_Product_Row_4 { get; set; }
        [BookmarkMapping("BCMaterialQtyFourth1")]
        public decimal QBusinessCollateralAgmLineT3S3_Quantity_Row_4 { get; set; }
        [BookmarkMapping("BCMaterialAmountFourth1")]
        public decimal QBusinessCollateralAgmLineT3S3_BusinessCollateralValue_Row_4 { get; set; }
        [BookmarkMapping("UnitMaterial4")]
        public string QBusinessCollateralAgmLineT3S3_Unit_Row_4 { get; set; }

        [BookmarkMapping("BCMaterialDesFifth1")]
        public string QBusinessCollateralAgmLineT3S3_Product_Row_5 { get; set; }
        [BookmarkMapping("BCMaterialQtyFifth1")]
        public decimal QBusinessCollateralAgmLineT3S3_Quantity_Row_5 { get; set; }
        [BookmarkMapping("BCMaterialAmountFifth1")]
        public decimal QBusinessCollateralAgmLineT3S3_BusinessCollateralValue_Row_5 { get; set; }
        [BookmarkMapping("UnitMaterial5")]
        public string QBusinessCollateralAgmLineT3S3_Unit_Row_5 { get; set; }
        #endregion QBusinessCollateralAgmLineT3S3

        #region QBusinessCollateralAgmLineT3S4

        [BookmarkMapping("BCCarDesFirst1")]
        public string QBusinessCollateralAgmLineT3S4_Product { get; set; }
        [BookmarkMapping("BCCarNumberFirst1")]
        public string QBusinessCollateralAgmLineT3S4_MachineNumber { get; set; }
        [BookmarkMapping("BCCarChassisNumFirst1")]
        public string QBusinessCollateralAgmLineT3S4_ChassisNumber { get; set; }
        [BookmarkMapping("BCCarPlateNumFirst1")]
        public string QBusinessCollateralAgmLineT3S4_RegistrationPlateNumber { get; set; }
        [BookmarkMapping("BCCarQtyFirst1")]
        public decimal QBusinessCollateralAgmLineT3S4_Quantity { get; set; }
        [BookmarkMapping("BCCarAmountFirst1")]
        public decimal QBusinessCollateralAgmLineT3S4_BusinessCollateralValue { get; set; }
        [BookmarkMapping("BCNameSubtypeCar1")]
        public string QBusinessCollateralAgmLineT3S4_SubTypeId { get; set; }
        [BookmarkMapping("BCSumSubtypeCar1")]
        public decimal QBusinessCollateralAgmLineT3S4_SumBusinessCollateralValue { get; set; }
        [BookmarkMapping("SubtypeTextAttachmentNineth1")]
        public string QBusinessCollateralAgmLineT3S4_AttachmentText { get; set; }
        [BookmarkMapping("SubtypeQtyNineth1")]
        public int QBusinessCollateralAgmLineT3S4_ConutLine { get; set; }
        [BookmarkMapping("SubtypePreferentialNineth1")]
        public int QBusinessCollateralAgmLineT3S4_PreferentialCreditorNumber { get; set; }
        public int QBusinessCollateralAgmLineT3S4_Row { get; set; }
        [BookmarkMapping("UnitCar1")]
        public string QBusinessCollateralAgmLineT3S4_Unit { get; set; }


        [BookmarkMapping("BCCarDesSecond1")]
        public string QBusinessCollateralAgmLineT3S4_Product_Row_2 { get; set; }
        [BookmarkMapping("BCCarNumberSecond1")]
        public string QBusinessCollateralAgmLineT3S4_MachineNumber_Row_2 { get; set; }
        [BookmarkMapping("BCCarChassisNumSecond1")]
        public string QBusinessCollateralAgmLineT3S4_ChassisNumber_Row_2 { get; set; }
        [BookmarkMapping("BCCarPlateNumSecond1")]
        public string QBusinessCollateralAgmLineT3S4_RegistrationPlateNumber_Row_2 { get; set; }
        [BookmarkMapping("BCCarQtySecond1")]
        public decimal QBusinessCollateralAgmLineT3S4_Quantity_Row_2 { get; set; }
        [BookmarkMapping("BCCarAmountSecond1")]
        public decimal QBusinessCollateralAgmLineT3S4_BusinessCollateralValue_Row_2 { get; set; }
        [BookmarkMapping("UnitCar2")]
        public string QBusinessCollateralAgmLineT3S4_Unit_Row_2 { get; set; }

        [BookmarkMapping("BCCarDesThird1")]
        public string QBusinessCollateralAgmLineT3S4_Product_Row_3 { get; set; }
        [BookmarkMapping("BCCarNumberThird1")]
        public string QBusinessCollateralAgmLineT3S4_MachineNumber_Row_3 { get; set; }
        [BookmarkMapping("BCCarChassisNumThird1")]
        public string QBusinessCollateralAgmLineT3S4_ChassisNumber_Row_3 { get; set; }
        [BookmarkMapping("BCCarPlateNumThird1")]
        public string QBusinessCollateralAgmLineT3S4_RegistrationPlateNumber_Row_3 { get; set; }
        [BookmarkMapping("BCCarQtyThird1")]
        public decimal QBusinessCollateralAgmLineT3S4_Quantity_Row_3 { get; set; }
        [BookmarkMapping("BCCarAmountThird1")]
        public decimal QBusinessCollateralAgmLineT3S4_BusinessCollateralValue_Row_3 { get; set; }
        [BookmarkMapping("UnitCar3")]
        public string QBusinessCollateralAgmLineT3S4_Unit_Row_3 { get; set; }

        [BookmarkMapping("BCCarDesFourth1")]
        public string QBusinessCollateralAgmLineT3S4_Product_Row_4 { get; set; }
        [BookmarkMapping("BCCarNumberFourth1")]
        public string QBusinessCollateralAgmLineT3S4_MachineNumber_Row_4 { get; set; }
        [BookmarkMapping("BCCarChassisNumFourth1")]
        public string QBusinessCollateralAgmLineT3S4_ChassisNumber_Row_4 { get; set; }
        [BookmarkMapping("BCCarPlateNumFourth1")]
        public string QBusinessCollateralAgmLineT3S4_RegistrationPlateNumber_Row_4 { get; set; }
        [BookmarkMapping("BCCarQtyFourth1")]
        public decimal QBusinessCollateralAgmLineT3S4_Quantity_Row_4 { get; set; }
        [BookmarkMapping("BCCarAmountFourth1")]
        public decimal QBusinessCollateralAgmLineT3S4_BusinessCollateralValue_Row_4 { get; set; }
        [BookmarkMapping("UnitCar4")]
        public string QBusinessCollateralAgmLineT3S4_Unit_Row_4 { get; set; }

        [BookmarkMapping("BCCarDesFifth1")]
        public string QBusinessCollateralAgmLineT3S4_Product_Row_5 { get; set; }
        [BookmarkMapping("BCCarNumberFifth1")]
        public string QBusinessCollateralAgmLineT3S4_MachineNumber_Row_5 { get; set; }
        [BookmarkMapping("BCCarChassisNumFifth1")]
        public string QBusinessCollateralAgmLineT3S4_ChassisNumber_Row_5 { get; set; }
        [BookmarkMapping("BCCarPlateNumFifth1")]
        public string QBusinessCollateralAgmLineT3S4_RegistrationPlateNumber_Row_5 { get; set; }
        [BookmarkMapping("BCCarQtyFifth1")]
        public decimal QBusinessCollateralAgmLineT3S4_Quantity_Row_5 { get; set; }
        [BookmarkMapping("BCCarAmountFifth1")]
        public decimal QBusinessCollateralAgmLineT3S4_BusinessCollateralValue_Row_5 { get; set; }
        [BookmarkMapping("UnitCar5")]
        public string QBusinessCollateralAgmLineT3S4_Unit_Row_5 { get; set; }

        #endregion QBusinessCollateralAgmLineT3S4

        #region QBusinessCollateralAgmLineT3S5

        [BookmarkMapping("BCOtherDesFirst1")]
        public string QBusinessCollateralAgmLineT3S5_Product { get; set; }
        [BookmarkMapping("BCOtherQtyFirst1")]
        public decimal QBusinessCollateralAgmLineT3S5_Quantity { get; set; }
        [BookmarkMapping("BCOtherAmountFirst1")]
        public decimal QBusinessCollateralAgmLineT3S5_BusinessCollateralValue { get; set; }
        [BookmarkMapping("BCNameSubtypeOther1")]
        public string QBusinessCollateralAgmLineT3S5_SubTypeId { get; set; }
        [BookmarkMapping("BCSumSubtypeOther1")]
        public decimal QBusinessCollateralAgmLineT3S5_SumBusinessCollateralValue { get; set; }
        [BookmarkMapping("SubtypeTextAttachmentTenth1")]
        public string QBusinessCollateralAgmLineT3S5_AttachmentText { get; set; }
        [BookmarkMapping("SubtypeQtyTenth1")]
        public int QBusinessCollateralAgmLineT3S5_ConutLine { get; set; }
        [BookmarkMapping("SubtypePreferentialTenth1")]
        public int QBusinessCollateralAgmLineT3S5_PreferentialCreditorNumber { get; set; }
        public int QBusinessCollateralAgmLineT3S5_Row { get; set; }


        [BookmarkMapping("BCOtherDesSecond1")]
        public string QBusinessCollateralAgmLineT3S5_Product_Row_2 { get; set; }
        [BookmarkMapping( "BCOtherQtySecond1")]
        public decimal QBusinessCollateralAgmLineT3S5_Quantity_Row_2 { get; set; }
        [BookmarkMapping("BCOtherAmountSecond1")]
        public decimal QBusinessCollateralAgmLineT3S5_BusinessCollateralValue_Row_2 { get; set; }


        [BookmarkMapping("BCOtherDesThird1")]
        public string QBusinessCollateralAgmLineT3S5_Product_Row_3 { get; set; }
        [BookmarkMapping("BCOtherQtyThird1")]
        public decimal QBusinessCollateralAgmLineT3S5_Quantity_Row_3 { get; set; }
        [BookmarkMapping("BCOtherAmountThird1" )]
        public decimal QBusinessCollateralAgmLineT3S5_BusinessCollateralValue_Row_3 { get; set; }


        [BookmarkMapping("BCOtherDesFourth1")]
        public string QBusinessCollateralAgmLineT3S5_Product_Row_4 { get; set; }
        [BookmarkMapping("BCOtherQtyFourth1")]
        public decimal QBusinessCollateralAgmLineT3S5_Quantity_Row_4 { get; set; }
        [BookmarkMapping("BCOtherAmountFourth1")]
        public decimal QBusinessCollateralAgmLineT3S5_BusinessCollateralValue_Row_4 { get; set; }


        [BookmarkMapping("BCOtherDesFifth1")]
        public string QBusinessCollateralAgmLineT3S5_Product_Row_5 { get; set; }
        [BookmarkMapping("BCOtherQtyFifth1")]
        public decimal QBusinessCollateralAgmLineT3S5_Quantity_Row_5 { get; set; }
        [BookmarkMapping("BCOtherAmountFifth1")]
        public decimal QBusinessCollateralAgmLineT3S5_BusinessCollateralValue_Row_5 { get; set; }


        #endregion QBusinessCollateralAgmLineT3S5

        #region QBusinessCollateralAgmLineT4S1

        [BookmarkMapping("BCPropertyProjectNameFirst1")]
        public string QBusinessCollateralAgmLineT4S1_ProjectName { get; set; }
        [BookmarkMapping("BCPropertyTitleDeedNumFirst1")]
        public string QBusinessCollateralAgmLineT4S1_TitleDeedNumber { get; set; }
        [BookmarkMapping("BCPropertySubdistrictFirst1")]
        public string QBusinessCollateralAgmLineT4S1_TitleDeedSubDistrict { get; set; }
        [BookmarkMapping("BCPropertyDistrictFirst1")]
        public string QBusinessCollateralAgmLineT4S1_TitleDeedDistrict { get; set; }
        [BookmarkMapping("BCPropertyProvinceFirst1")]
        public string QBusinessCollateralAgmLineT4S1_TitleDeedProvince { get; set; }
        [BookmarkMapping("BCNameSubtypeProperty1")]
        public string QBusinessCollateralAgmLineT4S1_SubTypeId { get; set; }
        [BookmarkMapping("BCSumSubtypeProperty1")]
        public decimal QBusinessCollateralAgmLineT4S1_SumBusinessCollateralValue { get; set; }
        [BookmarkMapping("SubtypeTextAttachmentEleventh1")]
        public string QBusinessCollateralAgmLineT4S1_AttachmentText { get; set; }
        [BookmarkMapping("SubtypeQtyEleventh1")]
        public int QBusinessCollateralAgmLineT4S1_ConutLine { get; set; }
        [BookmarkMapping("SubtypePreferentialEleventh1")]
        public int QBusinessCollateralAgmLineT4S1_PreferentialCreditorNumber { get; set; }
        public int QBusinessCollateralAgmLineT4S1_Row { get; set; }
        
        [BookmarkMapping("UnitProperty1")]
        public string QBusinessCollateralAgmLineT4S1_Unit { get; set; }

        [BookmarkMapping("BCPropertyProjectNameSecond1")]
        public string QBusinessCollateralAgmLineT4S1_ProjectName_Row_2 { get; set; }
        [BookmarkMapping("BCPropertyTitleDeedNumSecond1")]
        public string QBusinessCollateralAgmLineT4S1_TitleDeedNumber_Row_2 { get; set; }
        [BookmarkMapping("BCPropertySubdistrictSecond1")]
        public string QBusinessCollateralAgmLineT4S1_TitleDeedSubDistrict_Row_2 { get; set; }
        [BookmarkMapping("BCPropertyDistrictSecond1")]
        public string QBusinessCollateralAgmLineT4S1_TitleDeedDistrict_Row_2 { get; set; }
        [BookmarkMapping("BCPropertyProvinceSecond1")]
        public string QBusinessCollateralAgmLineT4S1_TitleDeedProvince_Row_2 { get; set; }
        [BookmarkMapping("UnitProperty2")]
        public string QBusinessCollateralAgmLineT4S1_Unit_Row_2 { get; set; }


        [BookmarkMapping("BCPropertyProjectNameThird1")]
        public string QBusinessCollateralAgmLineT4S1_ProjectName_Row_3 { get; set; }
        [BookmarkMapping("BCPropertyTitleDeedNumThird1")]
        public string QBusinessCollateralAgmLineT4S1_TitleDeedNumber_Row_3 { get; set; }
        [BookmarkMapping("BCPropertySubdistrictThird1")]
        public string QBusinessCollateralAgmLineT4S1_TitleDeedSubDistrict_Row_3 { get; set; }
        [BookmarkMapping("BCPropertyDistrictThird1")]
        public string QBusinessCollateralAgmLineT4S1_TitleDeedDistrict_Row_3 { get; set; }
        [BookmarkMapping("BCPropertyProvinceThird1")]
        public string QBusinessCollateralAgmLineT4S1_TitleDeedProvince_Row_3 { get; set; }
        [BookmarkMapping("UnitProperty3")]
        public string QBusinessCollateralAgmLineT4S1_Unit_Row_3 { get; set; }



        [BookmarkMapping("BCPropertyProjectNameFourth1")]
        public string QBusinessCollateralAgmLineT4S1_ProjectName_Row_4 { get; set; }
        [BookmarkMapping("BCPropertyTitleDeedNumFourth1")]
        public string QBusinessCollateralAgmLineT4S1_TitleDeedNumber_Row_4 { get; set; }
        [BookmarkMapping("BCPropertySubdistrictFourth1")]
        public string QBusinessCollateralAgmLineT4S1_TitleDeedSubDistrict_Row_4 { get; set; }
        [BookmarkMapping("BCPropertyDistrictFourth1")]
        public string QBusinessCollateralAgmLineT4S1_TitleDeedDistrict_Row_4 { get; set; }
        [BookmarkMapping("BCPropertyProvinceFourth1")]
        public string QBusinessCollateralAgmLineT4S1_TitleDeedProvince_Row_4 { get; set; }
        [BookmarkMapping("UnitProperty4")]
        public string QBusinessCollateralAgmLineT4S1_Unit_Row_4 { get; set; }



        [BookmarkMapping("BCPropertyProjectNameFifth1")]
        public string QBusinessCollateralAgmLineT4S1_ProjectName_Row_5 { get; set; }
        [BookmarkMapping("BCPropertyTitleDeedNumFifth1")]
        public string QBusinessCollateralAgmLineT4S1_TitleDeedNumber_Row_5 { get; set; }
        [BookmarkMapping("BCPropertySubdistrictFifth1")]
        public string QBusinessCollateralAgmLineT4S1_TitleDeedSubDistrict_Row_5 { get; set; }
        [BookmarkMapping("BCPropertyDistrictFifth1")]
        public string QBusinessCollateralAgmLineT4S1_TitleDeedDistrict_Row_5 { get; set; }
        [BookmarkMapping("BCPropertyProvinceFifth1")]
        public string QBusinessCollateralAgmLineT4S1_TitleDeedProvince_Row_5 { get; set; }
        [BookmarkMapping("UnitProperty5")]
        public string QBusinessCollateralAgmLineT4S1_Unit_Row_5 { get; set; }

        #endregion QBusinessCollateralAgmLineT4S1

        #region QBusinessCollateralAgmLineT4S2

        [BookmarkMapping("BCNameSubtypeLand1")]
        public string QBusinessCollateralAgmLineT4S2_SubTypeId { get; set; }
        [BookmarkMapping("BCSumSubtypeLand1")] 
        public decimal QBusinessCollateralAgmLineT4S2_SumBusinessCollateralValue { get; set; }
        [BookmarkMapping("SubtypeTextAttachmentTwelfth1")]
        public string QBusinessCollateralAgmLineT4S2_AttachmentText { get; set; }
        [BookmarkMapping("SubtypeQtyTwelfth1")]
        public int QBusinessCollateralAgmLineT4S2_ConutLine { get; set; }
        [BookmarkMapping("SubtypePreferentialTwelfth1")]
        public int QBusinessCollateralAgmLineT4S2_PreferentialCreditorNumber { get; set; }
        public int QBusinessCollateralAgmLineT4S2_Row { get; set; }
        [BookmarkMapping("UnitLand1")]
        public string QBusinessCollateralAgmLineT4S2_Unit { get; set; }
        [BookmarkMapping("UnitLand2")]
        public string QBusinessCollateralAgmLineT4S2_Unit_2 { get; set; }
        [BookmarkMapping("UnitLand3")]
        public string QBusinessCollateralAgmLineT4S2_Unit_3 { get; set; }
        [BookmarkMapping("UnitLand4")]
        public string QBusinessCollateralAgmLineT4S2_Unit_4 { get; set; }
        [BookmarkMapping("UnitLand5")]
        public string QBusinessCollateralAgmLineT4S2_Unit_5 { get; set; }

       // [BookmarkMapping("")]
        public string QBusinessCollateralAgmLineT4S3_SubTypeId { get; set; }
        //[BookmarkMapping("BCSumSubtypeLand1")]
        public decimal QBusinessCollateralAgmLineT4S3_SumBusinessCollateralValue { get; set; }
        [BookmarkMapping("SubtypeTextAttachmentThirteenth1")]
        public string QBusinessCollateralAgmLineT4S3_AttachmentText { get; set; }
        [BookmarkMapping("SubtypeQtyThirteenth1")]
        public int QBusinessCollateralAgmLineT4S3_ConutLine { get; set; }
        [BookmarkMapping("SubtypePreferentialThirteenth1")]
        public int QBusinessCollateralAgmLineT4S3_PreferentialCreditorNumber { get; set; }
        public int QBusinessCollateralAgmLineT4S3_Row { get; set; }

        #endregion QBusinessCollateralAgmLineT4S2



        #region QBusinessCollateralAgmLineT5S2
        [BookmarkMapping("BCNameSubtypeTrademark1")]
        public string QBusinessCollateralAgmLineT5S2_SubTypeId { get; set; }
        [BookmarkMapping("BCSumSubtypeTrademark1")]
        public decimal QBusinessCollateralAgmLineT5S2_SumBusinessCollateralValue { get; set; }
        [BookmarkMapping("SubtypeTextAttachmentFifteenth1")]
        public string QBusinessCollateralAgmLineT5S2_AttachmentText { get; set; }
        [BookmarkMapping("SubtypeQtyFifteenth1")]
        public int QBusinessCollateralAgmLineT5S2_ConutLine { get; set; }
        [BookmarkMapping("SubtypePreferentialFifteenth1")]
        public int QBusinessCollateralAgmLineT5S2_PreferentialCreditorNumber { get; set; }
        public int QBusinessCollateralAgmLineT5S2_Row { get; set; }
        #endregion QBusinessCollateralAgmLineT5S2

        #region QBusinessCollateralAgmLineT5S3
        [BookmarkMapping("BCNameSubtypeIntellOther1")]
        public string QBusinessCollateralAgmLineT5S3_SubTypeId { get; set; }
        [BookmarkMapping("BCSumSubtypeIntellOther1")]
        public decimal QBusinessCollateralAgmLineT5S3_SumBusinessCollateralValue { get; set; }
        [BookmarkMapping("SubtypeTextAttachmentSixteenth1")]
        public string QBusinessCollateralAgmLineT5S3_AttachmentText { get; set; }
        [BookmarkMapping("SubtypeQtySixteenth1")]
        public int QBusinessCollateralAgmLineT5S3_ConutLine { get; set; }
        [BookmarkMapping("SubtypePreferentialSixteenth1")]
        public int QBusinessCollateralAgmLineT5S3_PreferentialCreditorNumber { get; set; }
        public int QBusinessCollateralAgmLineT5S3_Row { get; set; }
        #endregion QBusinessCollateralAgmLineT5S3

        public string QBCLType1_Mark1 { get; set; }
        public string QBCLType2_Mark1 { get; set; }
        public string QBCLType3_Mark1 { get; set; }
        [BookmarkMapping("MaxInterestRate1")]
        public decimal MaxInterestPct { get; set; }
        [BookmarkMapping("BCLType1_Mark1")]
        public string BCLType1 { get; set; }
        [BookmarkMapping("BCLType2_Mark1")]
        public string BCLType2 { get; set; }
        [BookmarkMapping("BCLType3_Mark1")]
        public string BCLType3 { get; set; }
        [BookmarkMapping("BCLType4_Mark1")]
        public string BCLType4 { get; set; }
        [BookmarkMapping("BCLType5_Mark1")]
        public string BCLType5 { get; set; }
      
        public string DBDRegistrationAmountText { get; set; }
        [BookmarkMapping("BCAllSubtypeAmountTextFirst1")]
        public string SumBusinessCollateralValueText { get; set; }
        [BookmarkMapping("BCSumSubtypeTextBank1")]
        public string SumBusinessCollateralValueTextT1S1 { get; set; }
        [BookmarkMapping("BCSumSubtypeTextLeasehold1")]
        public string SumBusinessCollateralValueTextT1S2 { get; set; }
        [BookmarkMapping("BCSumSubtypeTextRightClaim1")]
        public string SumBusinessCollateralValueTextT1S3 { get; set; }
        [BookmarkMapping("BCSumSubtypeTextARFirst1")]
        public string SumBusinessCollateralValueTextT2S1 { get; set; }
        [BookmarkMapping("BCSumSubtypeTextARSecond1")]
        public string SumBusinessCollateralValueTextT2S2 { get; set; }
        [BookmarkMapping("BCSumSubtypeTextMachine1")]
        public string SumBusinessCollateralValueTextT3S1 { get; set; }
        [BookmarkMapping("BCSumSubtypeTextInven1")]
        public string SumBusinessCollateralValueTextT3S2 { get; set; }
        [BookmarkMapping("BCSumSubtypeTextMaterial1")]
        public string SumBusinessCollateralValueTextT3S3 { get; set; }
        [BookmarkMapping("BCSumSubtypeTextCar1")]
        public string SumBusinessCollateralValueTextT3S4 { get; set; }
        [BookmarkMapping("BCSumSubtypeTextOther1")]
        public string SumBusinessCollateralValueTextT3S5 { get; set; }
        [BookmarkMapping("BCSumSubtypeTextProperty1")]
        public string SumBusinessCollateralValueTextT4S1 { get; set; }
        [BookmarkMapping("BCSumSubtypeTextLand1")]
        public string SumBusinessCollateralValueTextT4S2 { get; set; }
        //[BookmarkMapping("BCSumSubtypeTextLand1")]
        public string SumBusinessCollateralValueTextT4S3 { get; set; }
        [BookmarkMapping("BCSumSubtypeTextPatent1")]
        public string SumBusinessCollateralValueTextT5S1 { get; set; }
        [BookmarkMapping("BCSumSubtypeTextTrademark1")]
        public string SumBusinessCollateralValueTextT5S2 { get; set; }
        [BookmarkMapping("BCSumSubtypeTextIntellOther1")]
        public string SumBusinessCollateralValueTextT5S3 { get; set; }


        #region T5S1

        [BookmarkMapping("BCNameSubtypePatent1")]
        public string QBusinessCollateralAgmLineT5S1_SubTypeId { get; set; }
        [BookmarkMapping("BCSumSubtypePatent1")]
        public decimal QBusinessCollateralAgmLineT5S1_SumBusinessCollateralValue { get; set; }
        [BookmarkMapping("SubtypeTextAttachmentFourteenth1")]
        public string QBusinessCollateralAgmLineT5S1_AttachmentText { get; set; }
        [BookmarkMapping("SubtypeQtyFourteenth1")]
        public int QBusinessCollateralAgmLineT5S1_ConutLine { get; set; }
        [BookmarkMapping("SubtypePreferentialFourteenth1")]
        public int QBusinessCollateralAgmLineT5S1_PreferentialCreditorNumber { get; set; }
        [BookmarkMapping("BCIntellAssetsAmountFirst1")]
        public decimal QBusinessCollateralAgmLineT5S1_BusinessCollateralValue { get; set; }
        [BookmarkMapping("BCIntellAssetsDesFirst1")]
        public string QBusinessCollateralAgmLineT5S1_Product { get; set; }
        [BookmarkMapping("BCIntellAssetsSubtypeFirst1")]
        public string QBusinessCollateralAgmLineT5S1_BusinessCollateralAgmLineDesc { get; set; }
        
        [BookmarkMapping("UnitIntell1")]
        public string QBusinessCollateralAgmLineT5S1_Unit { get; set; }
        public string QBusinessCollateralAgmLineT5S1_SubTypeId_2 { get; set; }
        public decimal QBusinessCollateralAgmLineT5S1_SumBusinessCollateralValue_2 { get; set; }
        public string QBusinessCollateralAgmLineT5S1_AttachmentText_2 { get; set; }
        public int QBusinessCollateralAgmLineT5S1_ConutLine_2 { get; set; }
        public int QBusinessCollateralAgmLineT5S1_PreferentialCreditorNumber_2 { get; set; }
        [BookmarkMapping("BCIntellAssetsAmountSecond1")]
        public decimal QBusinessCollateralAgmLineT5S1_BusinessCollateralValue_2 { get; set; }
        [BookmarkMapping("BCIntellAssetsDesSecond1")]
        public string QBusinessCollateralAgmLineT5S1_Product_2 { get; set; }
        [BookmarkMapping("BCIntellAssetsSubtypeSecond1")]
        public string QBusinessCollateralAgmLineT5S1_BusinessCollateralAgmLineDesc_2 { get; set; }
        [BookmarkMapping("UnitIntell2")]
        public string QBusinessCollateralAgmLineT5S1_Unit_2 { get; set; }

        public string QBusinessCollateralAgmLineT5S1_SubTypeId_3 { get; set; }
        public decimal QBusinessCollateralAgmLineT5S1_SumBusinessCollateralValue_3 { get; set; }
        public string QBusinessCollateralAgmLineT5S1_AttachmentText_3 { get; set; }
        public int QBusinessCollateralAgmLineT5S1_ConutLine_3 { get; set; }
        public int QBusinessCollateralAgmLineT5S1_PreferentialCreditorNumber_3 { get; set; }
        [BookmarkMapping("BCIntellAssetsAmountThird1")]
        public decimal QBusinessCollateralAgmLineT5S1_BusinessCollateralValue_3 { get; set; }
        [BookmarkMapping("BCIntellAssetsDesThird1")]
        public string QBusinessCollateralAgmLineT5S1_Product_3 { get; set; }
        [BookmarkMapping("BCIntellAssetsSubtypeThird1")]
        public string QBusinessCollateralAgmLineT5S1_BusinessCollateralAgmLineDesc_3 { get; set; }
        [BookmarkMapping("UnitIntell3")]
        public string QBusinessCollateralAgmLineT5S1_Unit_3 { get; set; }
        public string QBusinessCollateralAgmLineT5S1_SubTypeId_4 { get; set; }
        public decimal QBusinessCollateralAgmLineT5S1_SumBusinessCollateralValue_4 { get; set; }
        public string QBusinessCollateralAgmLineT5S1_AttachmentText_4 { get; set; }
        public int QBusinessCollateralAgmLineT5S1_ConutLine_4 { get; set; }
        public int QBusinessCollateralAgmLineT5S1_PreferentialCreditorNumber_4 { get; set; }
        [BookmarkMapping("BCIntellAssetsAmountFour1")]
        public decimal QBusinessCollateralAgmLineT5S1_BusinessCollateralValue_4 { get; set; }
        [BookmarkMapping("BCIntellAssetsDesFour1")]
        public string QBusinessCollateralAgmLineT5S1_Product_4 { get; set; }
        [BookmarkMapping("BCIntellAssetsSubtypeFour1")]
        public string QBusinessCollateralAgmLineT5S1_BusinessCollateralAgmLineDesc_4 { get; set; }
        [BookmarkMapping("UnitIntell4")]
        public string QBusinessCollateralAgmLineT5S1_Unit_4 { get; set; }


        [BookmarkMapping("UnitIntell5")]
        public string QBusinessCollateralAgmLineT5S1_Unit_5 { get; set; }

        [BookmarkMapping("UnitIntell6")]
        public string QBusinessCollateralAgmLineT5S1_Unit_6 { get; set; }

        [BookmarkMapping("UnitIntell7")]
        public string QBusinessCollateralAgmLineT5S1_Unit_7 { get; set; }
        [BookmarkMapping("UnitIntell8")]
        public string QBusinessCollateralAgmLineT5S1_Unit_8 { get; set; }
        [BookmarkMapping("UnitIntell9")]
        public string QBusinessCollateralAgmLineT5S1_Unit_9 { get; set; }
        [BookmarkMapping("UnitIntell10")]
        public string QBusinessCollateralAgmLineT5S1_Unit_10 { get; set; }
        [BookmarkMapping("UnitIntell11")]
        public string QBusinessCollateralAgmLineT5S1_Unit_11 { get; set; }

        [BookmarkMapping("UnitIntell12")]
        public string QBusinessCollateralAgmLineT5S1_Unit_12 { get; set; }
        [BookmarkMapping("UnitIntell13")]
        public string QBusinessCollateralAgmLineT5S1_Unit_13 { get; set; }
        [BookmarkMapping("UnitIntell14")]
        public string QBusinessCollateralAgmLineT5S1_Unit_14 { get; set; }
        [BookmarkMapping("UnitIntell15")]
        public string QBusinessCollateralAgmLineT5S1_Unit_15 { get; set; }
        [BookmarkMapping("UnitIntell16")]
        public string QBusinessCollateralAgmLineT5S1_Unit_16 { get; set; }
        [BookmarkMapping("UnitIntell17")]
        public string QBusinessCollateralAgmLineT5S1_Unit_17 { get; set; }
        [BookmarkMapping("UnitIntell18")]
        public string QBusinessCollateralAgmLineT5S1_Unit_18 { get; set; }
        [BookmarkMapping("UnitIntell19")]
        public string QBusinessCollateralAgmLineT5S1_Unit_19 { get; set; }
        [BookmarkMapping("UnitIntell20")]
        public string QBusinessCollateralAgmLineT5S1_Unit_20 { get; set; }
        #endregion T5s1
        [BookmarkMapping("SubtypeTextAttachmentSeventeenth1")]
        public string QBusinessCollateralAgmLineT5S4_AttachmentText { get; set; }
        public string QBusinessCollateralAgmLineT5S4_Product { get; set; }
        [BookmarkMapping("SubtypeQtySeventeenth1")]
        public int QBusinessCollateralAgmLineT5S4_ConutLine { get; set; }
        [BookmarkMapping("SubtypePreferentialSeventeenth1")]
        public int QBusinessCollateralAgmLineT5S4_PreferentialCreditorNumber { get; set; }

    }
}
