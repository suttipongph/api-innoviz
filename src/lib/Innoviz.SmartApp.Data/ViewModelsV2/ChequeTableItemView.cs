using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ChequeTableItemView : ViewCompanyBaseEntity
	{
		public string ChequeTableGUID { get; set; }
		public decimal Amount { get; set; }
		public string BuyerTableGUID { get; set; }
		public string ChequeBankAccNo { get; set; }
		public string ChequeBankGroupGUID { get; set; }
		public string ChequeBranch { get; set; }
		public string ChequeDate { get; set; }
		public string ChequeNo { get; set; }
		public string CompletedDate { get; set; }
		public string CustomerTableGUID { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string ExpectedDepositDate { get; set; }
		public string IssuedName { get; set; }
		public bool PDC { get; set; }
		public int ReceivedFrom { get; set; }
		public string RecipientName { get; set; }
		public string RefGUID { get; set; }
		public string RefPDCGUID { get; set; }
		public int RefType { get; set; }
		public string Remark { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string RefID { get; set; }
		public string RefPDC_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string document_StatusId { get; set; }
		public string PurchaseTable_Value { get; set; }
		public string CustomerTable_Value {get;set;}
		public string BuyerTable_Value { get; set; }
		public string ChequeBankGroup_Value { get; set; }

	}
}
