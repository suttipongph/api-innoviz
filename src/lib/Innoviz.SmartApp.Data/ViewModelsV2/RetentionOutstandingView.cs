﻿using Innoviz.SmartApp.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public partial class RetentionOutstandingView : CompanyBaseEntity
    {
        public Guid CustomerTableGUID { get; set; }
        public int ProductType { get; set; }
        public decimal MaximumRetention { get; set; }
        public decimal AccumRetentionAmount { get; set; }
        public decimal RemainingAmount { get; set; }
        public Guid CreditAppTableGUID { get; set; }
        public string CreditAppId { get; set; }

    }
}
