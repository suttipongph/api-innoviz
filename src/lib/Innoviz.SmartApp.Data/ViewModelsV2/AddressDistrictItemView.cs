using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class AddressDistrictItemView : ViewCompanyBaseEntity
	{
		public string AddressDistrictGUID { get; set; }
		public string AddressProvinceGUID { get; set; }
		public string DistrictId { get; set; }
		public string Name { get; set; }
	}
}
