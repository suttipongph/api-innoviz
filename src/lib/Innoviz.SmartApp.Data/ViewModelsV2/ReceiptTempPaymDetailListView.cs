using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ReceiptTempPaymDetailListView : ViewCompanyBaseEntity
	{
		public string ReceiptTempPaymDetailGUID { get; set; }
		public string MethodOfPaymentGUID { get; set; }
		public string ReceiptTempTableGUID { get; set; }
		public decimal ReceiptAmount { get; set; }
		public string ChequeTableGUID { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public string ChequeTable_Values { get; set; }
		public string MethodOfPayment_MethodOfPaymentId { get; set; }
		public string ChequeTable_ChequeNo { get; set; }
		public string InvoiceSettlementDetail_RefReceiptTempPaymDetailGUID { get; set; }
	}
}
