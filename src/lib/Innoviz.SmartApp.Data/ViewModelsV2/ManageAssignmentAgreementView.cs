﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class ManageAssignmentAgreementItemView
    {
        public string AgreementTableGUID { get; set; }
        public string NumberSeqTableGUID { get; set; }
        public string DocumentStatusGUID { get; set; }
        public bool IsInternalAssignmentAgreementIdManual { get; set; }
        public bool IsAssignmentAgreementIdManual { get; set; }
    }
}
