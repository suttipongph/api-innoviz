﻿using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class ViewReplaceInvoiceLineInvoiceTextParameter
    {
        public List<InvoiceLine> InvoiceLine { get; set; }
    }
}
