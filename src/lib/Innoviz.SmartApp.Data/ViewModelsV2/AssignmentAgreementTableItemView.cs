using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class AssignmentAgreementTableItemView : ViewCompanyBaseEntity
	{
		public string AssignmentAgreementTableGUID { get; set; }
		public string AcceptanceName { get; set; }
		public string AgreementDate { get; set; }
		public int AgreementDocType { get; set; }
		public decimal AssignmentAgreementAmount { get; set; }
		public string AssignmentAgreementId { get; set; }
		public string AssignmentMethodGUID { get; set; }
		public string BuyerName { get; set; }
		public string BuyerTableGUID { get; set; }
		public string ConsortiumTableGUID { get; set; }
		public string CustBankGUID { get; set; }
		public string CustomerAltName { get; set; }
		public string CustomerName { get; set; }
		public string CustomerTableGUID { get; set; }
		public string Description { get; set; }
		public string DocumentReasonGUID { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string InternalAssignmentAgreementId { get; set; }
		public string RefAssignmentAgreementTableGUID { get; set; }
		public string ReferenceAgreementId { get; set; }
		public string Remark { get; set; }
		public string SigningDate { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public bool IsInternalAssignmentAgreementIdManual { get; set; }
		public bool IsAssignmentAgreementIdManual { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string buyerTable_Values { get; set; }
		public string AssignmentProductDescription { get; set; }
		public string ToWhomConcern { get; set; }
		public string CancelAuthorityPersonID { get; set; }
		public string CancelAuthorityPersonName { get; set; }
		public string CancelAuthorityPersonAddress { get; set; }
		public string CustRegisteredLocation { get; set; }
		public string RefCreditAppRequestTableGUID { get; set; }
		public string CreditAppReqAssignmentGUID { get; set; }
		public string DocumentReason_Values { get; set; }
		public string AssignmentMethod_AssignmentMethodId { get; set; }
		public string DocumentProcess_ProcessId { get; set; }
		public string AssignmentAgreementTable_InternalAssignmentAgreementId { get; set; }
		public string AssignmentAgreementTable_AssignmentAgreementId { get; set; }
	}
}
