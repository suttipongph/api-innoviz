using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class GuarantorAgreementTableListView : ViewCompanyBaseEntity
	{
		public string GuarantorAgreementTableGUID { get; set; }
		public string InternalGuarantorAgreementId { get; set; }
		public string GuarantorAgreementId { get; set; }
		public int AgreementDocType { get; set; }
		public string Description { get; set; }
		public string AgreementDate { get; set; }
		public string ExpiryDate { get; set; }
		public string DocumentStatusGUID { get; set; }
		public bool Affiliate { get; set; }
		public string ParentCompanyGUID { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string MainAgreementTable_MainAgreementId { get; set; }
		public int creditApp_ProductType { get; set; }
		public string company_CompanyId { get; set; }
		public string MainAgreementTable_MainAgreementGUID { get; set; }
		public string RefGuarantorAgreementTableGUID { get; set; }
	}
}
