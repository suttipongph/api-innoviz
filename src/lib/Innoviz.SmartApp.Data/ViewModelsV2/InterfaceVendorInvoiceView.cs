﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class InterfaceVendorInvoiceView
    {
        public List<int> InterfaceStatus { get; set; }
        public List<int> ProcessTransType { get; set; }
        public List<Guid> ListRefGUID { get; set; }
    }
}
