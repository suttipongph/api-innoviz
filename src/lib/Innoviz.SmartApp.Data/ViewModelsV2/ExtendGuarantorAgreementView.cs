﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class ExtendGuarantorAgreementView : ResultBaseEntity
    {

        public string GuarantorAgreementTableGUID { get; set; }
        public bool Affiliate { get; set; }
        public string AgreementDate { get; set; }
        public int AgreementDocType { get; set; }
        public int AgreementExtension { get; set; }
        public string Description { get; set; }
        public string DocumentReasonGUID { get; set; }
        public string ExpiryDate { get; set; }
        public string ExtendInternalGuarantorAgreementId { get; set; }
        public string GuarantorAgreementId { get; set; }
        public string InternalGuarantorAgreementId { get; set; }
        public string InternalMainAgreementId { get; set; }
        public string NewAgreementDate { get; set; }
        public string NewDescription { get; set; }
        public string ReasonRemark { get; set; }
        public string ExtendGuarantorAgreement_Values { get; set; }
        public string DocumentReason_Values { get; set; }
        public string DocumentStatusGUID { get; set; }
        public int  ProductType { get; set; }
        public string CompanyGUID { get; set; }
        
    }
}
