using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class LedgerFiscalYearListView : ViewCompanyBaseEntity
	{
		public string LedgerFiscalYearGUID { get; set; }
		public string Description { get; set; }
		public string EndDate { get; set; }
		public string FiscalYearId { get; set; }
		public int LengthOfPeriod { get; set; }
		public int PeriodUnit { get; set; }
		public string StartDate { get; set; }
	}
}
