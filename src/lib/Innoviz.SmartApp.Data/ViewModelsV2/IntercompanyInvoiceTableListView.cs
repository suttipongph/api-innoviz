using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class IntercompanyInvoiceTableListView : ViewCompanyBaseEntity
	{
		public string IntercompanyInvoiceTableGUID { get; set; }
		public string IntercompanyInvoiceId { get; set; }
		public string CustomerTableGUID { get; set; }
		public int ProductType { get; set; }
		public string CreditAppTableGUID { get; set; }
		public string DocumentId { get; set; }
		public string IssuedDate { get; set; }
		public string DueDate { get; set; }
		public string InvoiceRevenueTypeGUID { get; set; }
		public decimal InvoiceAmount { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string InvoiceRevenueType_Values { get; set; }
		public decimal Balance { get; set; }
		public decimal SettleAmount { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string CreditAppTable_CreditAppId { get; set; }
		public string InvoiceRevenueType_InvoiceRevenueTypeId { get; set; }
	}
}
