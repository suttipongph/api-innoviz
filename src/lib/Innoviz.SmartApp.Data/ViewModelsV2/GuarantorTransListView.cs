using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class GuarantorTransListView : ViewCompanyBaseEntity
	{
		public string GuarantorTransGUID { get; set; }
		public int Ordering { get; set; }
		public string GuarantorTypeGUID { get; set; }
		public bool Affiliate { get; set; }
		public bool InActive { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public string RefGuarantorTransGUID { get; set; }
		public string GuarantorType_Values { get; set; }
		public string RelatedPersonTable_Name { get; set; }
		public string RelatedPersonTable_PassportId { get; set; }
		public string RelatedPersonTable_TaxId { get; set; }
		public string GuarantorType_GuarantorTypeId { get; set; }
		public decimal MaximumGuaranteeAmount { get; set; }
	}
}
