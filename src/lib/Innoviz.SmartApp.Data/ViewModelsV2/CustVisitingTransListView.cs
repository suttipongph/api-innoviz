using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CustVisitingTransListView : ViewCompanyBaseEntity
	{
		public string CustVisitingTransGUID { get; set; }
		public string CustVisitingDate { get; set; }
		public string ResponsibleByGUID { get; set; }
		public string Description { get; set; }
		public string EmployeeTable_Values { get; set; }
		public string EmployeeTable_EmployeeId { get; set; }
		public string RefGUID { get; set; }
	}
}
