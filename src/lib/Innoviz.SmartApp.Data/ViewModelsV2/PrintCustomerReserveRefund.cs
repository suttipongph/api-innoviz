﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
  public  class PrintCustomerReserveRefund
    {
        public string printCustomerRefundReserveGUID { get; set; }
        public string customerId { get; set; }
        public string customerRefundId { get; set; }
        public string documentStatus { get; set; }
        public decimal netAmount { get; set; }
        public decimal paymentAmount { get; set; }
        public decimal retentionAmount { get; set; }
        public decimal serviceFeeAmount { get; set; }
        public decimal settleAmount { get; set; }
        public decimal suspenseAmount { get; set; }
        public string transDate { get; set; }
        public string CustomerRefundTableGUID { get; set; }
    }
    public class PrintCustomerReserveRefundReporttView : ViewReportBaseEntity
    {
        public string printCustomerRefundReserveGUID { get; set; }
        public string customerId { get; set; }
        public string customerRefundId { get; set; }
        public string documentStatus { get; set; }
        public string netAmount { get; set; }
        public string paymentAmount { get; set; }
        public string retentionAmount { get; set; }
        public string serviceFeeAmount { get; set; }
        public string settleAmount { get; set; }
        public string suspenseAmount { get; set; }
        public string transDate { get; set; }
        public string CustomerRefundTableGUID { get; set; }
    }
}
