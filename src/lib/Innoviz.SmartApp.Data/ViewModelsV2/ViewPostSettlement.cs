﻿using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class ViewPostSettlementParameter
    {
        public RefType RefType { get; set; }
        public List<InvoiceSettlementDetail> InvoiceSettlementDetails { get; set; }
        public List<InvoiceTable> InvoiceTables { get; set; }
        public List<InvoiceLine> InvoiceLines { get; set; }
        public List<CustTrans> CustTrans { get; set; }
        public ReceiptTempTable ReceiptTempTable { get; set; } = null;
        public CustomerRefundTable CustomerRefundTable { get; set; } = null;
        public RefType SourceRefType { get; set; }
        public string SourceRefId { get; set; }
    }

    public class ViewPostSettlementResult
    {
        public List<CustTrans> UpdateCustTrans { get; set; }
        public List<CreditAppTrans> CreateCreditAppTrans { get; set; }
        public List<ReceiptTable> CreateReceiptTable { get; set; }
        public List<ReceiptLine> CreateReceiptLine { get; set; }
        public List<TaxInvoiceTable> CreateTaxInvoiceTable { get; set; }
        public List<TaxInvoiceLine> CreateTaxInvoiceLine { get; set; }
        public List<PaymentHistory> CreatePaymentHistory { get; set; }
        public List<ProcessTrans> CreateProcessTrans { get; set; }
    }

}
