using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class GuarantorAgreementLineAffiliateListView : ViewCompanyBaseEntity
	{
		public string GuarantorAgreementLineAffiliateGUID { get; set; }
		public int Ordering { get; set; }
		public string CustomerTableGUID { get; set; }
		public string Customer_Values { get; set; }
		public string GuarantorAgreementLineGUID { get; set; }
	}
}
