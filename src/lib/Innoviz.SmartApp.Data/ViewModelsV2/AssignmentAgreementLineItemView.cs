using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class AssignmentAgreementLineItemView : ViewCompanyBaseEntity
	{
		public string AssignmentAgreementLineGUID { get; set; }
		public string AssignmentAgreementTableGUID { get; set; }
		public string BuyerAgreementTableGUID { get; set; }
		public int LineNum { get; set; }
		public string ReferenceAgreementID { get; set; }
		public string StartDate { get; set; }
		public string EndDate { get; set; }
		public decimal BuyerAgreementAmount { get; set; }
		public string AssignmentAgreementTable_StatusId { get; set; }
		public string AssignmentAgreementTable_InternalAssignmentAgreementId { get; set; }
		public string AssignmentAgreementTable_AssignmentAgreementId { get; set; }
		public string AssignmentAgreementTable_BuyerTableGUID { get; set; }
		public string AssignmentAgreementTable_CustomerTableGUID { get; set; }
		public string AssignmentAgreementTable_Description { get; set; }
		public string AssignmentAgreementTable_Values { get; set; }
	}
}
