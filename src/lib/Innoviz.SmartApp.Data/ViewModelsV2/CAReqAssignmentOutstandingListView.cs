using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CAReqAssignmentOutstandingListView : ViewCompanyBaseEntity
	{
		public string CAReqAssignmentOutstandingGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public string AssignmentAgreementTableGUID { get; set; }
		public string BuyerTableGUID { get; set; }
		public decimal AssignmentAgreementAmount { get; set; }
		public decimal SettleAmount { get; set; }
		public decimal RemainingAmount { get; set; }
		public string CreditAppRequestTableGUID { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string AssignmentAgreement_Values { get; set; }
		public string AssignmentAgreement_InternalAssignmentAgreementId { get; set; }
		public string BuyerTable_Values { get; set; }
		public string BuyerTable_BuyerId { get; set; }
	}
}
