﻿using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class UpdateChequeReferenceResultView : ResultBaseEntity
    {
        public List<ChequeTable> ChequeTable { get; set; } = new List<ChequeTable>();
    }
}
