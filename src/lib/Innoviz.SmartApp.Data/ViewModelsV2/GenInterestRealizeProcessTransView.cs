﻿using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class GenInterestRealizeProcessTransView : ResultBaseEntity
    {
        public string AsofDate { get; set; }
        public int[] ProductType { get; set; }
        public string TransDate { get; set; }
    }
}

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class GenInterestRealizeProcessTransViewMap
    {
        public List<ProcessTrans> ProcessTrans { get; set; } = new List<ProcessTrans>();
        public List<InterestRealizedTrans> InterestRealizedTrans { get; set; } = new List<InterestRealizedTrans>();
    }
}
