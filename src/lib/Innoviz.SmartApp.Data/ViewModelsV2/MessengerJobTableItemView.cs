using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class MessengerJobTableItemView : ViewCompanyBaseEntity
	{
		public string MessengerJobTableGUID { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string BuyerTableGUID { get; set; }
		public string ContactPersonExtension { get; set; }
		public string ContactPersonMobile { get; set; }
		public string ContactPersonName { get; set; }
		public string ContactPersonPhone { get; set; }
		public string ContactPersonPosition { get; set; }
		public int ContactTo { get; set; }
		public string CustomerTableGUID { get; set; }
		public string DocumentStatusGUID { get; set; }
		public decimal FeeAmount { get; set; }
		public string JobDate { get; set; }
		public string JobDetail { get; set; }
		public string JobId { get; set; }
		public string JobStartTime { get; set; }
		public string JobEndTime { get; set; }
		public string JobTypeGUID { get; set; }
		public bool Map { get; set; }
		public string MessengerTableGUID { get; set; }
		public int Priority { get; set; }
		public int ProductType { get; set; }
		public string RefCreditAppLineGUID { get; set; }
		public string RefGUID { get; set; }
		public string RefMessengerJobTableGUID { get; set; }
		public int RefType { get; set; }
		public string RequestorGUID { get; set; }
		public int Result { get; set; }
		public string ResultRemark { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string RefID { get; set; }
		public string WithdrawalLine_Values { get; set; }

		public string MessengerTable_Value { get; set; }

		public string MessengerJob_Value { get; set; }
		public string JobType_Value { get; set; }
		public string DocumentStatus_Value { get; set; }
		public string ContactName { get; set; }
		public string CustomerTable_Value { get; set; }

		public string BuyerTable_Value { get; set; }
		public string ContactPersonDepartment { get; set; }
		public string AssignmentAgreementTableGUID { get; set; }
		public decimal ServiceFeeAmount { get; set; }
		public string RefCreditAppLine_Values { get; set; }
		public decimal ExpenseAmount { get; set; }
	}
}
