﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class SysRoleTableListView : ViewCompanyBaseEntity
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }

        public int SiteLoginType { get; set; }
        public string Company_Name { get; set; }
    }
}
