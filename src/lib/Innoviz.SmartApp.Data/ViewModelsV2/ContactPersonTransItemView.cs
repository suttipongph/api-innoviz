using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ContactPersonTransItemView : ViewCompanyBaseEntity
	{
		public string ContactPersonTransGUID { get; set; }
		public bool InActive { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public string RelatedPersonTableGUID { get; set; }
		public string RefId { get; set; }
		public string RelatedPersonTable_WorkPermitId { get; set; }
		public string ContactPersonTrans_Values { get; set; }
		public string RelatedPersonTable_Name { get; set; }
		public string RelatedPersonTable_Phone { get; set; }
		public string RelatedPersonTable_Extension { get; set; }
		public string RelatedPersonTable_Mobile { get; set; }
		public string RelatedPersonTable_Position { get; set; }
		public string RelatedPersonTable_RelatedPersonId { get; set; }
	}
}
