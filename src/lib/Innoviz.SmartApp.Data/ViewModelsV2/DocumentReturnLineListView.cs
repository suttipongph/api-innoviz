using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class DocumentReturnLineListView : ViewCompanyBaseEntity
	{
		public string DocumentReturnLineGUID { get; set; }
		public string DocumentReturnTableGUID { get; set; }
		
		public int LineNum { get; set; }
		public string BuyerTableGUID { get; set; }
		public string ContactPersonName { get; set; }
		public string DocumentTypeGUID { get; set; }
		public string DocumentNo { get; set; }
		public string BuyerTable_Values { get; set; }
		public string DocumentType_Values { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string DocumentType_DocumentTypeId { get; set; }
	}
}
