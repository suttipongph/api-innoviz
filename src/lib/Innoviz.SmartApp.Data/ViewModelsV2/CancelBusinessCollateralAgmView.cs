﻿using Innoviz.SmartApp.Core.ViewModels;
namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class CancelBusinessCollateralAgmView : ViewCompanyBaseEntity
    {
        public string CancelBusinessCollateralAgmGUID { get; set; }
        public string AgreementDate { get; set; }
        public int AgreementDocType { get; set; }
        public string BusinessCollateralAgmDescription { get; set; }
        public string BusinessCollateralAgmId { get; set; }
        public string CancelDate { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string Description { get; set; }
        public string DocumentReasonGUID { get; set; }
        public string InternalBusinessCollateralAgmId { get; set; }
        public string NoticeOfCancellationInternalBusineeCollateralId { get; set; }
        public string ReasonRemark { get; set; }
        public string CustomerTable_Values { get; set; }
    }

    public class CancelBusinessCollateralAgmResultView : ResultBaseEntity
    {
        public string CancelBusinessCollateralAgmGUID { get; set; }
        public string AgreementDate { get; set; }
        public int AgreementDocType { get; set; }
        public string BusinessCollateralAgmDescription { get; set; }
        public string BusinessCollateralAgmId { get; set; }
        public string CancelDate { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string Description { get; set; }
        public string DocumentReasonGUID { get; set; }
        public string InternalBusinessCollateralAgmId { get; set; }
        public string NoticeOfCancellationInternalBusineeCollateralId { get; set; }
        public string ReasonRemark { get; set; }
        public string CustomerTable_Values { get; set; }

    }

}
