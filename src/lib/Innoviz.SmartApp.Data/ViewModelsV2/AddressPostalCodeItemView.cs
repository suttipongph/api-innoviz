using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class AddressPostalCodeItemView : ViewCompanyBaseEntity
	{
		public string AddressPostalCodeGUID { get; set; }
		public string AddressSubDistrictGUID { get; set; }
		public string PostalCode { get; set; }
		public string AddressSubDistrict_Value { get; set; }
	}
}
