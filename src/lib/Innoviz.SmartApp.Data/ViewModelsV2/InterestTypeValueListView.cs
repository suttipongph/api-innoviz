using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class InterestTypeValueListView : ViewDateEffectiveBaseEntity
	{
		public string InterestTypeValueGUID { get; set; }
		public decimal Value { get; set; }
		public string InterestTypeGUID { get; set; }

	}
}
