using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class IntercompanyInvoiceAdjustmentListView : ViewCompanyBaseEntity
	{
		public string IntercompanyInvoiceAdjustmentGUID { get; set; }
		public decimal OriginalAmount { get; set; }
		public decimal Adjustment { get; set; }
		public string DocumentReasonGUID { get; set; }
		public string DocumentReason_Values { get; set; }
		public string IntercompanyInvoiceTableGUID { get; set; }
		public string DocumentReason_DocumentReasonId { get; set; }
	}
}
