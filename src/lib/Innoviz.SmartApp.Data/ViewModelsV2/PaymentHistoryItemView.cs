using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class PaymentHistoryItemView : ViewBranchCompanyBaseEntity
	{
		public string PaymentHistoryGUID { get; set; }
		public string CreditAppTableGUID { get; set; }
		public bool Cancel { get; set; }
		public string CurrencyGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public string CustTransGUID { get; set; }
		public string DueDate { get; set; }
		public decimal ExchangeRate { get; set; }
		public string InvoiceSettlementDetailGUID { get; set; }
		public string InvoiceTableGUID { get; set; }
		public string OrigTaxTableGUID { get; set; }
		public decimal PaymentAmount { get; set; }
		public decimal PaymentAmountMST { get; set; }
		public decimal PaymentBaseAmount { get; set; }
		public decimal PaymentBaseAmountMST { get; set; }
		public string PaymentDate { get; set; }
		public decimal PaymentTaxAmount { get; set; }
		public decimal PaymentTaxAmountMST { get; set; }
		public decimal PaymentTaxBaseAmount { get; set; }
		public decimal PaymentTaxBaseAmountMST { get; set; }
		public int ProductType { get; set; }
		public string ReceiptTableGUID { get; set; }
		public int ReceivedFrom { get; set; }
		public string ReceivedDate { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public decimal WHTAmount { get; set; }
		public decimal WHTAmountMST { get; set; }
		public string WithholdingTaxTableGUID { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string Currency_Values { get; set; }

		public string CustomerTable_Values { get; set; }

		public string InvoiceTable_Values { get; set; }

		public string WithholdingTaxTable_Values { get; set; }
		public string ReceiptTable_Values { get; set; }
		public bool WHTSlipReceivedByCustomer { get; set; }
		public string InvoiceSettlementDetail_Values { get; set; }
		public string OrigTaxTable_Values { get; set; }
		public string RefID { get; set; }
	}
}
