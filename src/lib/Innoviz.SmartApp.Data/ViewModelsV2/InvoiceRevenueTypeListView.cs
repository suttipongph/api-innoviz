using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class InvoiceRevenueTypeListView : ViewCompanyBaseEntity
	{
		public string InvoiceRevenueTypeGUID { get; set; }
		public string RevenueTypeId { get; set; }
		public string Description { get; set; }
		public decimal FeeAmount { get; set; }
		public string FeeTaxGUID { get; set; }
		public decimal FeeTaxAmount { get; set; }
		public string FeeWHTGUID { get; set; }
		public string ServiceFeeRevenueTypeGUID { get; set; }
		public int AssetFeeType { get; set; }
		public string WithholdingTaxTable_Values { get; set; }
		public string TaxTable_Values { get; set; }
		public string InvoiceRevenueType_Values { get; set; }
		public string WithholdingTaxTable_WHTCode { get; set; }
		public string TaxTable_TaxCode { get; set; }
		public string InvoiceRevenueType_revenueTypeId { get; set; }
		public int ServiceFeeCategory { get; set; }
	}
}
