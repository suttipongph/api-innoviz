﻿using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class PrintBookDocTransParm
	{
		public string DocumentTemplateTableGUID { get; set; }
		public string RefGUID { get; set; }
		public string BookmarkDocumentTransGUID { get; set; }
	}
}
