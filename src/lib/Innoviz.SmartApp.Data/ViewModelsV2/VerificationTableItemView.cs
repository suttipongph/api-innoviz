using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class VerificationTableItemView : ViewCompanyBaseEntity
    {
        public string VerificationTableGUID { get; set; }
        public string BuyerTableGUID { get; set; }
        public string CreditAppTableGUID { get; set; }
        public string CreditAppTable_Values { get; set; }
        public string CustomerTableGUID { get; set; }
        public string CustomerTable_Values { get; set; }
        public string Description { get; set; }
        public string DocumentStatusGUID { get; set; }
        public string Remark { get; set; }
        public string VerificationDate { get; set; }
        public string VerificationId { get; set; }
        public string DocumentStatus_Values { get; set; }
        public string DocumentStatus_StatusId { get; set; }
        public int TotalVericationLine { get; set; }
        public string originalDocumentStatusGUID { get; set; }
        public string BuyerTable_Values { get; set; }
        public bool HaveLine { get; set; }
        public string CopyVerificationGUID { get; set; }
        public NotificationResponse Notification { get; set; } = new NotificationResponse();
    }
}
