﻿using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class PostFreeTextInvoiceView
    {
        public string FreeTextInvoiceTableGUID { get; set; }
        public string Currency_Values { get; set; }
        public string CustomerTable_Values { get; set; }
        public string CustomerName { get; set; }
        public string DueDate { get; set; }
        public string FreeTextInvoiceId { get; set; }
        public decimal InvoiceAmount { get; set; }
        public decimal InvoiceAmountBeforeTax { get; set; }
        public string InvoiceRevenueType_Values { get; set; }
        public string InvoiceType_Values { get; set; }
        public string IssuedDate { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal WHTAmount { get; set; }
    }
    public class PostFreeTextInvoiceResultView : ResultBaseEntity
    {
    }
}

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class PostFreeTextInvoiceViewMap
    {
        public FreeTextInvoiceTable FreeTextInvoiceTable { get; set; }
        public List<InvoiceTable> InvoiceTable { get; set; }
        public List<InvoiceLine> InvoiceLine { get; set; }
        public List<CustTrans> CustTrans { get; set; }
        public List<CreditAppTrans> CreditAppTrans { get; set; }
        public List<TaxInvoiceTable> TaxInvoiceTable { get; set; }
        public List<TaxInvoiceLine> TaxInvoiceLine { get; set; }
        public List<ProcessTrans> ProcessTrans { get; set; }
        public List<RetentionTrans> RetentionTrans { get; set; }
    }
}
