using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class MessengerTableListView : ViewCompanyBaseEntity
	{
		public string MessengerTableGUID { get; set; }
		public string Name { get; set; }
		public string TaxId { get; set; }
		public string VendorTableGUID { get; set; }
		public string Phone { get; set; }
		public string PlateNumber { get; set; }
		public string DriverLicenseId { get; set; }
		public string VendorTable_Values { get; set; }
		public string VendorTable_VendorId { get; set; }
	}
}
