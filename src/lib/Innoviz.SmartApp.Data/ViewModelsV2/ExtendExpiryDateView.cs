﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class ExtendExpiryDateParamView
    {
        public string CreditAppTableGUID { get; set; }
        public string CreditAppId { get; set; }
        public string CustomerTable_Values { get; set; }
        public string OriginalExpiryDate { get; set; }
        public string ExpiryDate { get; set; }
        public int CreditLimitExpiration { get; set; }
    }
    public class ExtendExpiryDateResultView : ResultBaseEntity
    {
        public string CreditAppTableGUID { get; set; }
        public string ExpiryDate { get; set; }
        public string DocumentReasonGUID { get; set; }
        public string ReasonRemark { get; set; }
        public string ResultLabel { get; set; }
        public int CreditLimitExpiration { get; set; }
        public string OriginalExpiryDate { get; set; }
    }
}
