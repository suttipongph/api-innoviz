﻿using Innoviz.SmartApp.Core.ViewModels;

public class PrintBuyerReceipt
    {
        public string BuyerReceiptTableGUID { get; set; }
        public string BuyerReceiptId { get; set; }
        public string BuyerReceiptDate { get; set; }
        public string BuyerId { get; set; }
        public string CustomerId { get; set; }
        public decimal BuyerReceiptAmount { get; set; }
        public bool Cancel { get; set; }
    }
    public class PrintBuyerReceiptReportView : ViewReportBaseEntity
    {
        public string BuyerReceiptTableGUID { get; set; }
        public string BuyerReceiptID { get; set; }
        public string BuyerReceiptDate { get; set; }
        public string BuyerId { get; set; }
        public string CustomerId { get; set; }
        public decimal BuyerReceiptAmount { get; set; }
        public bool Cancel { get; set; }
    }

