using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class VendBankItemView : ViewCompanyBaseEntity
	{
		public string VendBankGUID { get; set; }
		public string BankAccount { get; set; }
		public string BankAccountName { get; set; }
		public string BankBranch { get; set; }
		public string BankGroupGUID { get; set; }
		public string VendorTableGUID { get; set; }
		public bool Primary { get; set; }

	}
}
