﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class UpdateCustomerTableStatusParamView
    {
        public string CustomerTableGUID { get; set; }
        public string DocumentStatusGUID { get; set; }
        public string DocumentReasonGUID { get; set; }
        public string ReasonRemark { get; set; }
    }
    public class UpdateCustomerTableStatusResultView : ResultBaseEntity
    {
        public string CustomerTableGUID { get; set; }
        public string OriginalDocumentStatus { get; set; }
        public string OriginalDocumentStatusGUID { get; set; }
        public string DocumentStatusGUID { get; set; }
        public string DocumentReasonGUID { get; set; }
        public string CustomerTableId { get; set; }
        public string ReasonRemark { get; set; }
        public string ResultLabel { get; set; }
    }
}
