using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class TerritoryItemView : ViewCompanyBaseEntity
	{
		public string TerritoryGUID { get; set; }
		public string Description { get; set; }
		public string TerritoryId { get; set; }
	}
}
