﻿using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class GenInvoiceFromFreeTextInvoiceResult
    {
        public List<InvoiceTable> InvoiceTable { get; set; } = new List<InvoiceTable>();
        public List<InvoiceLine> InvoiceLine { get; set; } = new List<InvoiceLine>();
    }
}
