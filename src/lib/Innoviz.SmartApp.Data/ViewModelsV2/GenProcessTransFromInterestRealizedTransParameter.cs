﻿using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class GenProcessTransFromInterestRealizedTransParameter
    {
        public List<InterestRealizedTrans> interestRealizedTrans { get; set; }
        public string transDate { get; set; }
    }
}
