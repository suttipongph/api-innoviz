﻿using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class GenProcessTransFromInterestRealizedTransResultView
    {
        public List<ProcessTrans> ProcessTrans { get; set; } = new List<ProcessTrans>();
        public List<InterestRealizedTrans> InterestRealizedTrans { get; set; } = new List<InterestRealizedTrans>();
    }
}
