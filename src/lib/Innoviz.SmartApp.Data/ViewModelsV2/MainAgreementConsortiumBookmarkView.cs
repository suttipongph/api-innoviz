﻿using Innoviz.SmartApp.Core.Attributes;
using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
   public class MainAgreementConsortiumBookmarkView : ViewCompanyBaseEntity
    {
        [BookmarkMapping("ConsName1")]
        public string ConsName1 { get; set; }
        [BookmarkMapping("ConsName2")]
        public string ConsName2 { get; set; }
        [BookmarkMapping("ConsName3")]
        public string ConsName3 { get; set; }
        [BookmarkMapping("ConsName4")]
        public string ConsName4 { get; set; }
        [BookmarkMapping("ConsName5")]
        public string ConsName5 { get; set; }
        [BookmarkMapping("ConsName6")]
        public string ConsName6 { get; set; }
        [BookmarkMapping("ConsName7")]
        public string ConsName7 { get; set; }


        [BookmarkMapping("ConsNameCustFirst1")]
        public string ConsNameCustFirst1 { get; set; }
        [BookmarkMapping("ConsNameCustFirst2")]
        public string ConsNameCustFirst2 { get; set; }
        [BookmarkMapping("ConsNameCustFirst3")]
        public string ConsNameCustFirst3 { get; set; }
        [BookmarkMapping("ConsNameCustFirst4")]
        public string ConsNameCustFirst4 { get; set; }

        [BookmarkMapping("ConsNameCustSecond1")]
        public string ConsNameCustSecond1 { get; set; }
        [BookmarkMapping("ConsNameCustSecond2")]
        public string ConsNameCustSecond2 { get; set; }
        [BookmarkMapping("ConsNameCustSecond3")]
        public string ConsNameCustSecond3 { get; set; }
        [BookmarkMapping("ConsNameCustSecond4")]
        public string ConsNameCustSecond4 { get; set; }


        [BookmarkMapping("ConsNameCustThird1")]
        public string ConsNameCustThird1 { get; set; }
        [BookmarkMapping("ConsNameCustThird2")]
        public string ConsNameCustThird2 { get; set; }
        [BookmarkMapping("ConsNameCustThird3")]
        public string ConsNameCustThird3{ get; set; }
        [BookmarkMapping("ConsNameCustThird4")]
        public string ConsNameCustThird4 { get; set; }


        [BookmarkMapping("ConsNameCustForth1")]
        public string ConsNameCustForth1 { get; set; }
        [BookmarkMapping("ConsNameCustForth2")]
        public string ConsNameCustForth2 { get; set; }
        [BookmarkMapping("ConsNameCustForth3")]
        public string ConsNameCustForth3 { get; set; }
        [BookmarkMapping("ConsNameCustForth4")]
        public string ConsNameCustForth4 { get; set; }


        [BookmarkMapping("ConsAddressCustFirst1")]
        public string ConsAddressCustFirst1 { get; set; }
        [BookmarkMapping("ConsAddressCustFirst2")]
        public string ConsAddressCustFirst2 { get; set; }
        [BookmarkMapping("ConsAddressCustFirst3")]
        public string ConsAddressCustFirst3 { get; set; }
        [BookmarkMapping("ConsAddressCustFirst4")]
        public string ConsAddressCustFirst4 { get; set; }


        [BookmarkMapping("ConsAddressCustSecond1")]
        public string ConsAddressCustSecond1 { get; set; }
        [BookmarkMapping("ConsAddressCustSecond2")]
        public string ConsAddressCustSecond2 { get; set; }
        [BookmarkMapping("ConsAddressCustSecond3")]
        public string ConsAddressCustSecond3 { get; set; }
        [BookmarkMapping("ConsAddressCustSecond4")]
        public string ConsAddressCustSecond4 { get; set; }


        [BookmarkMapping("ConsAddressCustThird1")]
        public string ConsAddressCustThird1 { get; set; }
        [BookmarkMapping("ConsAddressCustThird2")]
        public string ConsAddressCustThird2 { get; set; }
        [BookmarkMapping("ConsAddressCustThird3")]
        public string ConsAddressCustThird3 { get; set; }
        [BookmarkMapping("ConsAddressCustThird4")]
        public string ConsAddressCustThird4 { get; set; }


        [BookmarkMapping("ConsAddressCustForth1")]
        public string ConsAddressCustForth1 { get; set; }
        [BookmarkMapping("ConsAddressCustForth2")]
        public string ConsAddressCustForth2 { get; set; }
        [BookmarkMapping("ConsAddressCustForth3")]
        public string ConsAddressCustForth3 { get; set; }
        [BookmarkMapping("ConsAddressCustForth4")]
        public string ConsAddressCustForth4 { get; set; }


        [BookmarkMapping("ConOperatedbyFirst1")]
        public string ConOperatedbyFirst1 { get; set; }
        [BookmarkMapping("ConOperatedbyFirst2")]
        public string ConOperatedbyFirst2 { get; set; }
        [BookmarkMapping("ConOperatedbyFirst3")]
        public string ConOperatedbyFirst3 { get; set; }
        [BookmarkMapping("ConOperatedbyFirst4")]
        public string ConOperatedbyFirst4 { get; set; }


        [BookmarkMapping("ConOperatedbySecond1")]
        public string ConOperatedbySecond1 { get; set; }
        [BookmarkMapping("ConOperatedbySecond2")]
        public string ConOperatedbySecond2 { get; set; }
        [BookmarkMapping("ConOperatedbySecond3")]
        public string ConOperatedbySecond3 { get; set; }
        [BookmarkMapping("ConOperatedbySecond4")]
        public string ConOperatedbySecond4 { get; set; }


        [BookmarkMapping("ConOperatedbyThird1")]
        public string ConOperatedbyThird1 { get; set; }
        [BookmarkMapping("ConOperatedbyThird2")]
        public string ConOperatedbyThird2 { get; set; }
        [BookmarkMapping("ConOperatedbyThird3")]
        public string ConOperatedbyThird3 { get; set; }
        [BookmarkMapping("ConOperatedbyThird4")]
        public string ConOperatedbyThird4 { get; set; }


        [BookmarkMapping("ConOperatedbyForth1")]
        public string ConOperatedbyForth1 { get; set; }
        [BookmarkMapping("ConOperatedbyForth2")]
        public string ConOperatedbyForth2 { get; set; }
        [BookmarkMapping("ConOperatedbyForth3")]
        public string ConOperatedbyForth3 { get; set; }
        [BookmarkMapping("ConOperatedbyForth4")]
        public string ConOperatedbyForth4 { get; set; }


        [BookmarkMapping("PositionConsCustFirst1")]
        public string PositionConsCustFirst1 { get; set; }
        [BookmarkMapping("PositionConsCustFirst2")]
        public string PositionConsCustFirst2 { get; set; }
        [BookmarkMapping("PositionConsCustFirst3")]
        public string PositionConsCustFirst3 { get; set; }
        [BookmarkMapping("PositionConsCustFirst4")]
        public string PositionConsCustFirst4 { get; set; }


        [BookmarkMapping("PositionConsCustSecond1")]
        public string PositionConsCustSecond1 { get; set; }
        [BookmarkMapping("PositionConsCustSecond2")]
        public string PositionConsCustSecond2 { get; set; }
        [BookmarkMapping("PositionConsCustSecond3")]
        public string PositionConsCustSecond3 { get; set; }
        [BookmarkMapping("PositionConsCustSecond4")]
        public string PositionConsCustSecond4 { get; set; }


        [BookmarkMapping("PositionConsCustThird1")]
        public string PositionConsCustThird1 { get; set; }
        [BookmarkMapping("PositionConsCustThird2")]
        public string PositionConsCustThird2 { get; set; }
        [BookmarkMapping("PositionConsCustThird3")]
        public string PositionConsCustThird3 { get; set; }
        [BookmarkMapping("PositionConsCustThird4")]
        public string PositionConsCustThird4 { get; set; }


        [BookmarkMapping("PositionConsCustForth1")]
        public string PositionConsCustForth1 { get; set; }
        [BookmarkMapping("PositionConsCustForth2")]
        public string PositionConsCustForth2 { get; set; }
        [BookmarkMapping("PositionConsCustForth3")]
        public string PositionConsCustForth3 { get; set; }
        [BookmarkMapping("PositionConsCustForth4")]
        public string PositionConsCustForth4 { get; set; }
    }
}
