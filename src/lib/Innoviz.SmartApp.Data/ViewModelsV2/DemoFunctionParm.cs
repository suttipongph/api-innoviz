﻿using Innoviz.SmartApp.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class DemoFunctionParm
    {
        public string Param1 { get; set; }
        public string Param2 { get; set; }
        public WorkflowInstance Workflow { get; set; }
    }
    public class ReplaceBookmarkDemoView
    {
        [BookmarkMapping("DemoId1", "DemoId2", "DemoId_Customer")]
        public string DemoId { get; set; }
        [BookmarkMapping("Date1", "Customer_Date")]
        public string DemoDate { get; set; }
    }
}
