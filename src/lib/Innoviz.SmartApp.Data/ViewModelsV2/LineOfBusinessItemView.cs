using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class LineOfBusinessItemView : ViewCompanyBaseEntity
	{
		public string LineOfBusinessGUID { get; set; }
		public string Description { get; set; }
		public string LineOfBusinessId { get; set; }
	}
}
