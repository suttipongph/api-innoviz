using System;
using Innoviz.SmartApp.Core.Attributes;
using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class GenMainAgreementConsortiumBookmarkView
	{
        [BookmarkMapping("AliasNameCompany1")]
        public string  AliasNameCompany1 { get; set; }
        
        [BookmarkMapping("AliasNameCompany2")]
        public string  AliasNameCompany2 { get; set; }

        [BookmarkMapping("AliasNameCompany3")]
        public string  AliasNameCompany3 { get; set; }

        [BookmarkMapping("AliasNameCompany4")]
        public string  AliasNameCompany4 { get; set; }

        [BookmarkMapping("AliasNameCompany5")]
        public string  AliasNameCompany5 { get; set; }

        [BookmarkMapping("AliasNameCompany6")]
        public string  AliasNameCompany6 { get; set; }

        [BookmarkMapping("AliasNameCompany7")]
        public string  AliasNameCompany7 { get; set; }

        [BookmarkMapping("AliasNameCompany8")]
        public string  AliasNameCompany8 { get; set; }

        [BookmarkMapping("CompanyName1")]
        public string  CompanyName1 { get; set; }

        [BookmarkMapping("CompanyName2")]
        public string  CompanyName2 { get; set; }

        [BookmarkMapping("CompanyName3")]
        public string  CompanyName3 { get; set; }

        [BookmarkMapping("CompanyName4")]
        public string  CompanyName4 { get; set; }

        [BookmarkMapping("CompanyName5")]
        public string  CompanyName5 { get; set; }

        [BookmarkMapping("CompanyName6")]
        public string  CompanyName6 { get; set; }

        [BookmarkMapping("CompanyName7")]
        public string  CompanyName7 { get; set; }

        [BookmarkMapping("ContAmountFirst1")]
        public decimal  ContAmountFirst1 { get; set; }

        [BookmarkMapping("ContAmountSecond1")]
        public string  ContAmountSecond1 { get; set; }

        public DateTime AgreementDate { get; set; }

        [BookmarkMapping("AgreementDate1")]
        public string  AgreementDate1 { get; set; }

        [BookmarkMapping("AgreementDate2")]
        public string  AgreementDate2 { get; set; }

        [BookmarkMapping("AgreementDate3")]
        public string  AgreementDate3 { get; set; }

        [BookmarkMapping("AgreementDate4")]
        public string  AgreementDate4 { get; set; }

        [BookmarkMapping("AgreementDate5")]
        public string  AgreementDate5 { get; set; }

        [BookmarkMapping("AgreementNo1")]
        public string  AgreementNo1 { get; set; }

        [BookmarkMapping("AgreementNo2")]
        public string  AgreementNo2 { get; set; }

        [BookmarkMapping("AgreementNo3")]
        public string  AgreementNo3 { get; set; }

        [BookmarkMapping("AgreementNo4")]
        public string  AgreementNo4 { get; set; }

        [BookmarkMapping("AgreementNo5")]
        public string  AgreementNo5 { get; set; }

        [BookmarkMapping("InterestRate1")]
        public decimal?  InterestRate1 { get; set; }

        [BookmarkMapping("AliasNameCust1")]
        public string  AliasNameCust1 { get; set; }

        [BookmarkMapping("AliasNameCust2")]
        public string  AliasNameCust2 { get; set; }

        [BookmarkMapping("AliasNameCust3")]
        public string  AliasNameCust3 { get; set; }

        [BookmarkMapping("AliasNameCust4")]
        public string  AliasNameCust4 { get; set; }

        [BookmarkMapping("AliasNameCust5")]
        public string  AliasNameCust5 { get; set; }

        [BookmarkMapping("AliasNameCust6")]
        public string  AliasNameCust6 { get; set; }

        [BookmarkMapping("AliasNameCust7")]
        public string  AliasNameCust7 { get; set; }

        [BookmarkMapping("AliasNameCust8")]
        public string  AliasNameCust8 { get; set; }

        [BookmarkMapping("CustName1")]
        public string  CustName1 { get; set; }

        [BookmarkMapping("CustName2")]
        public string  CustName2 { get; set; }

        [BookmarkMapping("CustName3")]
        public string  CustName3 { get; set; }

        [BookmarkMapping("CustName4")]
        public string  CustName4 { get; set; }

        [BookmarkMapping("CustName5")]
        public string  CustName5 { get; set; }

        [BookmarkMapping("CustName6")]
        public string  CustName6 { get; set; }

        [BookmarkMapping("CustName7")]
        public string  CustName7 { get; set; }

        [BookmarkMapping("CustName8")]
        public string  CustName8 { get; set; }

        [BookmarkMapping("CustTaxID1")]
        public string  CustTaxID1 { get; set; }

        [BookmarkMapping("CompanyWitnessFirst1")]
        public string  CompanyWitnessFirst1 { get; set; }

        [BookmarkMapping("CompanyWitnessSecond1")]
        public string  CompanyWitnessSecond1 { get; set; }

        [BookmarkMapping("Text1")]
        public string  Text1 { get; set; }

        [BookmarkMapping("Text2")]
        public string  Text2 { get; set; }

        [BookmarkMapping("Text3")]
        public string  Text3 { get; set; }

        [BookmarkMapping("Text4")]
        public string  Text4 { get; set; }

        [BookmarkMapping("AuthorityPersonFirst1")]
        public string  AuthorityPersonFirst1 { get; set; }

        [BookmarkMapping("AuthorityPersonFirst2")]
        public string  AuthorityPersonFirst2 { get; set; }

        [BookmarkMapping("AuthorityPersonSecond1")]
        public string  AuthorityPersonSecond1 { get; set; }

        [BookmarkMapping("AuthorityPersonSecond2")]
        public string  AuthorityPersonSecond2 { get; set; }

        [BookmarkMapping("AuthorityPersonThird1")]
        public string  AuthorityPersonThird1 { get; set; }

        [BookmarkMapping("AuthorityPersonThird2")]
        public string  AuthorityPersonThird2 { get; set; }

        [BookmarkMapping("AuthorizedBuyerFirst1")]
        public string  AuthorizedBuyerFirst1 { get; set; }

        [BookmarkMapping("AuthorizedBuyerFirst2")]
        public string  AuthorizedBuyerFirst2 { get; set; }

        [BookmarkMapping("AuthorizedBuyerSecond1")]
        public string  AuthorizedBuyerSecond1 { get; set; }

        [BookmarkMapping("AuthorizedBuyerSecond2")]
        public string  AuthorizedBuyerSecond2 { get; set; }

        [BookmarkMapping("AuthorizedBuyerThird1")]
        public string  AuthorizedBuyerThird1 { get; set; }

        [BookmarkMapping("AuthorizedBuyerThird2")]
        public string  AuthorizedBuyerThird2 { get; set; }

        [BookmarkMapping("AuthorizedCustFirst1")]
        public string  AuthorizedCustFirst1 { get; set; }

        [BookmarkMapping("AuthorizedCustFirst2")]
        public string  AuthorizedCustFirst2 { get; set; }

        [BookmarkMapping("AuthorizedCustSecond1")]
        public string  AuthorizedCustSecond1 { get; set; }

        [BookmarkMapping("AuthorizedCustSecond2")]
        public string  AuthorizedCustSecond2 { get; set; }

        [BookmarkMapping("AuthorizedCustThird1")]
        public string  AuthorizedCustThird1 { get; set; }

        [BookmarkMapping("AuthorizedCustThird2")]
        public string  AuthorizedCustThird2 { get; set; }

        [BookmarkMapping("CustWitnessFirst1")]
        public string  CustWitnessFirst1 { get; set; }

        [BookmarkMapping("CustWitnessSecond1")]
        public string  CustWitnessSecond1 { get; set; }

        [BookmarkMapping("PositionBuyer1")]
        public string  PositionBuyer1 { get; set; }

        [BookmarkMapping("PositionCust1")]
        public string  PositionCust1 { get; set; }

        [BookmarkMapping("PositionCust2")]
        public string  PositionCust2 { get; set; }
        [BookmarkMapping("PositionCust3")]
        public string  PositionCust3 { get; set; }
        [BookmarkMapping("PositionLIT1")]
        public string  PositionLIT1 { get; set; }
        [BookmarkMapping("PositionLIT2")]
        public string  PositionLIT2 { get; set; }
        [BookmarkMapping("PositionLIT3")]
        public string  PositionLIT3 { get; set; }
        [BookmarkMapping("CustAddress1")]
        public string  CustAddress1 { get; set; }
        [BookmarkMapping("CustAddress2")]
        public string  CustAddress2 { get; set; }
        [BookmarkMapping("CompanyAddress1")]
        public string  CompanyAddress1 { get; set; }
        [BookmarkMapping("CompanyAddress2")]
        public string  CompanyAddress2 { get; set; }
        [BookmarkMapping("TelNumber1")]
        public string  TelNumber1 { get; set; }
        [BookmarkMapping("FaxNumber1")]
        public string  FaxNumber1 { get; set; }
        [BookmarkMapping("FaxNumber2")]
        public string  FaxNumber2 { get; set; }
        
        [BookmarkMapping("CompanyBankBranch1")]
        public string  CompanyBankBranch1 { get; set; }
        
        [BookmarkMapping("CompanyBankBranch2")]
        public string  CompanyBankBranch2 { get; set; }
        
        [BookmarkMapping("CompanyBankName1")]
        public string  CompanyBankName1 { get; set; }
        
        [BookmarkMapping("CompanyBankName2")]
        public string  CompanyBankName2 { get; set; }
        
        [BookmarkMapping("CompanyBankNo1")]
        public string  CompanyBankNo1 { get; set; }
        
        [BookmarkMapping("CompanyBankNo2")]
        public string  CompanyBankNo2 { get; set; }
        
        [BookmarkMapping("CompanyBankType1")]
        public string  CompanyBankType1 { get; set; }
        
        [BookmarkMapping("CompanyBankType2")]
        public string  CompanyBankType2 { get; set; }
        
        [BookmarkMapping("ManualNo1")]
        public string  ManualNo1 { get; set; }

        [BookmarkMapping("ConsName1")]
        public string ConsName1 { get; set; }
        [BookmarkMapping("ConsName2")]
        public string ConsName2 { get; set; }
        [BookmarkMapping("ConsName3")]
        public string ConsName3 { get; set; }
        [BookmarkMapping("ConsName4")]
        public string ConsName4 { get; set; }
        [BookmarkMapping("ConsName5")]
        public string ConsName5 { get; set; }
        [BookmarkMapping("ConsName6")]
        public string ConsName6 { get; set; }
        [BookmarkMapping("ConsName7")]
        public string ConsName7 { get; set; }


        [BookmarkMapping("ConsNameCustFirst1")]
        public string ConsNameCustFirst1 { get; set; }
        [BookmarkMapping("ConsNameCustFirst2")]
        public string ConsNameCustFirst2 { get; set; }
        [BookmarkMapping("ConsNameCustFirst3")]
        public string ConsNameCustFirst3 { get; set; }
        [BookmarkMapping("ConsNameCustFirst4")]
        public string ConsNameCustFirst4 { get; set; }

        [BookmarkMapping("ConsNameCustSecond1")]
        public string ConsNameCustSecond1 { get; set; }
        [BookmarkMapping("ConsNameCustSecond2")]
        public string ConsNameCustSecond2 { get; set; }
        [BookmarkMapping("ConsNameCustSecond3")]
        public string ConsNameCustSecond3 { get; set; }
        [BookmarkMapping("ConsNameCustSecond4")]
        public string ConsNameCustSecond4 { get; set; }


        [BookmarkMapping("ConsNameCustThird1")]
        public string ConsNameCustThird1 { get; set; }
        [BookmarkMapping("ConsNameCustThird2")]
        public string ConsNameCustThird2 { get; set; }
        [BookmarkMapping("ConsNameCustThird3")]
        public string ConsNameCustThird3{ get; set; }
        [BookmarkMapping("ConsNameCustThird4")]
        public string ConsNameCustThird4 { get; set; }


        [BookmarkMapping("ConsNameCustForth1")]
        public string ConsNameCustForth1 { get; set; }
        [BookmarkMapping("ConsNameCustForth2")]
        public string ConsNameCustForth2 { get; set; }
        [BookmarkMapping("ConsNameCustForth3")]
        public string ConsNameCustForth3 { get; set; }
        [BookmarkMapping("ConsNameCustForth4")]
        public string ConsNameCustForth4 { get; set; }


        [BookmarkMapping("ConsAddressCustFirst1")]
        public string ConsAddressCustFirst1 { get; set; }
        [BookmarkMapping("ConsAddressCustFirst2")]
        public string ConsAddressCustFirst2 { get; set; }
        [BookmarkMapping("ConsAddressCustFirst3")]
        public string ConsAddressCustFirst3 { get; set; }
        [BookmarkMapping("ConsAddressCustFirst4")]
        public string ConsAddressCustFirst4 { get; set; }


        [BookmarkMapping("ConsAddressCustSecond1")]
        public string ConsAddressCustSecond1 { get; set; }
        [BookmarkMapping("ConsAddressCustSecond2")]
        public string ConsAddressCustSecond2 { get; set; }
        [BookmarkMapping("ConsAddressCustSecond3")]
        public string ConsAddressCustSecond3 { get; set; }
        [BookmarkMapping("ConsAddressCustSecond4")]
        public string ConsAddressCustSecond4 { get; set; }


        [BookmarkMapping("ConsAddressCustThird1")]
        public string ConsAddressCustThird1 { get; set; }
        [BookmarkMapping("ConsAddressCustThird2")]
        public string ConsAddressCustThird2 { get; set; }
        [BookmarkMapping("ConsAddressCustThird3")]
        public string ConsAddressCustThird3 { get; set; }
        [BookmarkMapping("ConsAddressCustThird4")]
        public string ConsAddressCustThird4 { get; set; }


        [BookmarkMapping("ConsAddressCustForth1")]
        public string ConsAddressCustForth1 { get; set; }
        [BookmarkMapping("ConsAddressCustForth2")]
        public string ConsAddressCustForth2 { get; set; }
        [BookmarkMapping("ConsAddressCustForth3")]
        public string ConsAddressCustForth3 { get; set; }
        [BookmarkMapping("ConsAddressCustForth4")]
        public string ConsAddressCustForth4 { get; set; }


        [BookmarkMapping("ConOperatedbyFirst1")]
        public string ConOperatedbyFirst1 { get; set; }
        [BookmarkMapping("ConOperatedbyFirst2")]
        public string ConOperatedbyFirst2 { get; set; }
        [BookmarkMapping("ConOperatedbyFirst3")]
        public string ConOperatedbyFirst3 { get; set; }
        [BookmarkMapping("ConOperatedbyFirst4")]
        public string ConOperatedbyFirst4 { get; set; }


        [BookmarkMapping("ConOperatedbySecond1")]
        public string ConOperatedbySecond1 { get; set; }
        [BookmarkMapping("ConOperatedbySecond2")]
        public string ConOperatedbySecond2 { get; set; }
        [BookmarkMapping("ConOperatedbySecond3")]
        public string ConOperatedbySecond3 { get; set; }
        [BookmarkMapping("ConOperatedbySecond4")]
        public string ConOperatedbySecond4 { get; set; }


        [BookmarkMapping("ConOperatedbyThird1")]
        public string ConOperatedbyThird1 { get; set; }
        [BookmarkMapping("ConOperatedbyThird2")]
        public string ConOperatedbyThird2 { get; set; }
        [BookmarkMapping("ConOperatedbyThird3")]
        public string ConOperatedbyThird3 { get; set; }
        [BookmarkMapping("ConOperatedbyThird4")]
        public string ConOperatedbyThird4 { get; set; }


        [BookmarkMapping("ConOperatedbyForth1")]
        public string ConOperatedbyForth1 { get; set; }
        [BookmarkMapping("ConOperatedbyForth2")]
        public string ConOperatedbyForth2 { get; set; }
        [BookmarkMapping("ConOperatedbyForth3")]
        public string ConOperatedbyForth3 { get; set; }
        [BookmarkMapping("ConOperatedbyForth4")]
        public string ConOperatedbyForth4 { get; set; }


        [BookmarkMapping("PositionConsCustFirst1")]
        public string PositionConsCustFirst1 { get; set; }
        [BookmarkMapping("PositionConsCustFirst2")]
        public string PositionConsCustFirst2 { get; set; }
        [BookmarkMapping("PositionConsCustFirst3")]
        public string PositionConsCustFirst3 { get; set; }
        [BookmarkMapping("PositionConsCustFirst4")]
        public string PositionConsCustFirst4 { get; set; }


        [BookmarkMapping("PositionConsCustSecond1")]
        public string PositionConsCustSecond1 { get; set; }
        [BookmarkMapping("PositionConsCustSecond2")]
        public string PositionConsCustSecond2 { get; set; }
        [BookmarkMapping("PositionConsCustSecond3")]
        public string PositionConsCustSecond3 { get; set; }
        [BookmarkMapping("PositionConsCustSecond4")]
        public string PositionConsCustSecond4 { get; set; }


        [BookmarkMapping("PositionConsCustThird1")]
        public string PositionConsCustThird1 { get; set; }
        [BookmarkMapping("PositionConsCustThird2")]
        public string PositionConsCustThird2 { get; set; }
        [BookmarkMapping("PositionConsCustThird3")]
        public string PositionConsCustThird3 { get; set; }
        [BookmarkMapping("PositionConsCustThird4")]
        public string PositionConsCustThird4 { get; set; }


        [BookmarkMapping("PositionConsCustForth1")]
        public string PositionConsCustForth1 { get; set; }
        [BookmarkMapping("PositionConsCustForth2")]
        public string PositionConsCustForth2 { get; set; }
        [BookmarkMapping("PositionConsCustForth3")]
        public string PositionConsCustForth3 { get; set; }
        [BookmarkMapping("PositionConsCustForth4")]
        public string PositionConsCustForth4 { get; set; }
    }
}