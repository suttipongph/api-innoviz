using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BuyerTableItemView : ViewCompanyBaseEntity
	{
		public string BuyerTableGUID { get; set; }
		public string AltName { get; set; }
		public string BlacklistStatusGUID { get; set; }
		public string BusinessSegmentGUID { get; set; }
		public string BusinessSizeGUID { get; set; }
		public string BusinessTypeGUID { get; set; }
		public string BuyerId { get; set; }
		public string BuyerName { get; set; }
		public string CreditScoringGUID { get; set; }
		public string CurrencyGUID { get; set; }
		public string DateOfEstablish { get; set; }
		public string DateOfExpiry { get; set; }
		public string DateOfIssue { get; set; }
		public string DocumentStatusGUID { get; set; }
		public int FixedAssets { get; set; }
		public int IdentificationType { get; set; }
		public string IssuedBy { get; set; }
		public string KYCSetupGUID { get; set; }
		public int Labor { get; set; }
		public string LineOfBusinessGUID { get; set; }
		public decimal PaidUpCapital { get; set; }
		public string PassportId { get; set; }
		public string ReferenceId { get; set; }
		public decimal RegisteredCapital { get; set; }
		public string TaxId { get; set; }
		public string WorkPermitId { get; set; }
		public string BlacklistStatus_Values { get; set; }
		public string BusinessSegment_Values { get; set; }
		public string BusinessType_Values { get; set; }
		public string LineOfBusiness_Values { get; set; }
		public string CreditAppTable_CreditAppTableGUID { get; set; }
		public string BuyerAgreementTable_Description { get; set; }
		public string BuyerAgreementTable_ReferenceAgreementID { get; set; }
		public string CreditAppLine_ExpiryDate { get; set; }
		public string CreditAppLine_CreditAppLineGUID { get; set; }
		public decimal PurchaseLine_PurchaseFeeCalculateBase { get; set; }
		public string MethodOfPayment_MethodOfPaymentGUID { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public string CreditAppLine_Values { get; set; }
		public decimal CreditAppLine_MaxPurchasePct { get; set; }
		public decimal CreditAppLine_PurchaseFeePct { get; set; }
	}
}
