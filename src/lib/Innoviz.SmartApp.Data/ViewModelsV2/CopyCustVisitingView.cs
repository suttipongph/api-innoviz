﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
   public class CopyCustVisitingView : ResultBaseEntity
    {
        public string CustVisitingTransGUID { get; set; }
        public string CustomerTableGUID { get; set; }
        public string RefGUID { get; set; }
        public int RefType { get; set; }
        public string ResultLabel { get; set; }
    }
}
