using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ProjectReferenceTransListView : ViewCompanyBaseEntity
	{
		public string ProjectReferenceTransGUID { get; set; }
		public string ProjectCompanyName { get; set; }
		public string ProjectName { get; set; }
		public decimal ProjectValue { get; set; }
		public int ProjectCompletion { get; set; }
		public string ProjectStatus { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
	}
}
