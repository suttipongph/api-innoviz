using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class IntercompanyListView : ViewCompanyBaseEntity
	{
		public string IntercompanyGUID { get; set; }
		public string IntercompanyId { get; set; }
		public string Description { get; set; }
	}
}
