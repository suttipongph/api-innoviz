using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class MethodOfPaymentItemView : ViewCompanyBaseEntity
	{
		public string MethodOfPaymentGUID { get; set; }
		public string CompanyBankGUID { get; set; }
		public string Description { get; set; }
		public string MethodOfPaymentId { get; set; }
		public string PaymentRemark { get; set; }
		public int PaymentType { get; set; }
		public int AccountType { get; set; }
		public string AccountNum { get; set; }
	}
}
