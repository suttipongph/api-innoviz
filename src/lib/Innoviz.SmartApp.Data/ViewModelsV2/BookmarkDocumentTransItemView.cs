using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BookmarkDocumentTransItemView : ViewCompanyBaseEntity
	{
		public string BookmarkDocumentTransGUID { get; set; }
		public string BookmarkDocumentGUID { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string DocumentTemplateTableGUID { get; set; }
		public string ReferenceExternalDate { get; set; }
		public string ReferenceExternalId { get; set; }
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public string Remark { get; set; }
		public string RefId { get; set; }
		public int BookmarkDocument_BookmarkDocumentRefType { get; set; }
		public int BookmarkDocument_DocumentTemplateType { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string BookmarkDocument_Values { get; set; }
		public string DocumentTemplateTable_Values { get; set; }
		public string BookmarkDocument_BookmarkDocumentId { get; set; }
		public string DocumentTemplateTable_TemplateId { get; set; }
		public string DocumentStatus_Description { get; set; }
		public string DocumentStatus_StatusId { get; set; }
	}
}
