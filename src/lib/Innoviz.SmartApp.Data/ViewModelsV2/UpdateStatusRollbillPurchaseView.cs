﻿using Innoviz.SmartApp.Core.ViewModels;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class UpdateStatusRollbillPurchaseView
    {
        public string PurchaseTableGUID { get; set; }
        public string PurchaseId { get; set; }
        public ApprovalDecision ApprovedResult { get; set; }
        public string UpdateStatusRollbillPurchaseGUID { get; set; }

    }
    public class UpdateStatusRollbillPurchaseResultView : ResultBaseEntity
    {
    }
}
