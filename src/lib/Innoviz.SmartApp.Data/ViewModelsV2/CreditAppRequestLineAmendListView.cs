using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CreditAppRequestLineAmendListView : ViewCompanyBaseEntity
	{
		public string CreditAppRequestLineAmendGUID { get; set; }
		public string CreditAppRequestTable_CreditAppRequestId { get; set; }
		public string CreditAppRequestTable_Description { get; set; }
		public int CreditAppRequestTable_CreditAppRequestType { get; set; }
		public string CreditAppRequestTable_RequestDate { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string BuyerTable_Values { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string CreditAppRequestTable_DocumentStatusGUID { get; set; }
		public string CreditAppRequestTable_CustomerTableGUID { get; set; }
		public string CreditAppRequestTable_BuyerTableGUID { get; set; }
		public string CreditAppRequestLine_RefCreditAppLineGUID { get; set; }

	}
}
