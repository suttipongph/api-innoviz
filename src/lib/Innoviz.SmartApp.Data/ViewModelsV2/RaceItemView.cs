using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class RaceItemView : ViewCompanyBaseEntity
	{
		public string RaceGUID { get; set; }
		public string Description { get; set; }
		public string RaceId { get; set; }
	}
}
