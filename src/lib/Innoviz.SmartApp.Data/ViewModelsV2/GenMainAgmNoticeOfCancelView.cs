﻿using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class GenMainAgmNoticeOfCancelView : ViewCompanyBaseEntity
    {
        public string MainAgreementTableGUID { get; set; }
        public string AgreementDate { get; set; }
        public int AgreementDocType { get; set; }
        public string BuyerName { get; set; }
        public string BuyerTableGUID { get; set; }
        public string CancelDate { get; set; }
        public string CustomerTableGUID { get; set; }
        public string CustomerName { get; set; }
        public string Description { get; set; }
        public string DocumentReasonGUID { get; set; }
        public string InternalMainAgreementId { get; set; }
        public string MainAgreementDescription { get; set; }
        public string MainAgreementId { get; set; }
        public string NoticeOfCancellationInternalMainAgreementId { get; set; }
        public string ReasonRemark { get; set; }
        public string BuyerTable_Values { get; set; }
        public string CustomerTable_Values { get; set; }
        public string DocumentReason_Values { get; set; }
        public bool CreditLimitType_Revolving { get; set; }
    }
    public class GenMainAgmNoticeOfCancelResultView : ResultBaseEntity
    {
        public string MainAgreementTableGUID { get; set; }
        public string AgreementDate { get; set; }
        public int AgreementDocType { get; set; }
        public string BuyerName { get; set; }
        public string BuyerTableGUID { get; set; }
        public string CancelDate { get; set; }
        public string CustomerTableGUID { get; set; }
        public string CustomerName { get; set; }
        public string Description { get; set; }
        public string DocumentReasonGUID { get; set; }
        public string InternalMainAgreementId { get; set; }
        public string MainAgreementDescription { get; set; }
        public string MainAgreementId { get; set; }
        public string NoticeOfCancellationInternalMainAgreementId { get; set; }
        public string ReasonRemark { get; set; }
        public string BuyerTable_Values { get; set; }
        public string CustomerTable_Values { get; set; }
        public string DocumentReason_Values { get; set; }
        public bool CreditLimitType_Revolving { get; set; }
    }
}
