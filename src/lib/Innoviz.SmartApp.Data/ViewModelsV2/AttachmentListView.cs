﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class AttachmentListView : ViewBaseEntity
    {
        public string AttachmentGUID { get; set; }
        public int RefType { get; set; }
        public string RefGUID { get; set; }
        public string RefId { get; set; }
        public string FileName { get; set; }
        public string FileDescription { get; set; }
    }
}
