using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class MemoTransItemView : ViewCompanyBaseEntity
	{
		public string MemoTransGUID { get; set; }
		public string Memo { get; set; }
		public string RefGUID { get; set; }
		public string RefId { get; set; }
		public int RefType { get; set; }
		public string Topic { get; set; }
	}
}
