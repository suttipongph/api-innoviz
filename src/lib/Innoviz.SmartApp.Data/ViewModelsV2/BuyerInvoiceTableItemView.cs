using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BuyerInvoiceTableItemView : ViewCompanyBaseEntity
	{
		public string BuyerInvoiceTableGUID { get; set; }
		public decimal Amount { get; set; }
		public string BuyerAgreementLineGUID { get; set; }
		public string BuyerAgreementTableGUID { get; set; }
		public string BuyerInvoiceId { get; set; }
		public string CreditAppLineGUID { get; set; }
		public string DueDate { get; set; }
		public string InvoiceDate { get; set; }
		public string Remark { get; set; }
		public string BuyerId { get; set; }
		public string CreditAppId { get; set; }
		public int LineNum { get; set; }
		public string PurchaseLineGUID { get; set; }
		public string PurchaseTableGUID { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string CreditAppLine_CreditAppTableGUID { get; set; }
		public string CreditAppLine_BuyerTableGUID { get; set; }
		public string CreditAppTable_CustomerTableGUID { get; set; }
	}
}
