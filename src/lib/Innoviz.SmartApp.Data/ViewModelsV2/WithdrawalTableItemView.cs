using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class WithdrawalTableItemView : ViewCompanyBaseEntity
	{
		public string WithdrawalTableGUID { get; set; }
		public string AssignmentAgreementTableGUID { get; set; }
		public decimal AssignmentAmount { get; set; }
		public string BuyerAgreementTableGUID { get; set; }
		public string BuyerTableGUID { get; set; }
		public string CreditAppLineGUID { get; set; }
		public string CreditAppTableGUID { get; set; }
		public string CreditComment { get; set; }
		public bool CreditMarkComment { get; set; }
		public string CreditTermGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public string Description { get; set; }
		public string Dimension1GUID { get; set; }
		public string Dimension2GUID { get; set; }
		public string Dimension3GUID { get; set; }
		public string Dimension4GUID { get; set; }
		public string Dimension5GUID { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string DueDate { get; set; }
		public string ExtendInterestDate { get; set; }
		public string ExtendWithdrawalTableGUID { get; set; }
		public int InterestCutDay { get; set; }
		public string MarketingComment { get; set; }
		public bool MarketingMarkComment { get; set; }
		public string MethodOfPaymentGUID { get; set; }
		public int NumberOfExtension { get; set; }
		public string OperationComment { get; set; }
		public bool OperationMarkComment { get; set; }
		public string OriginalWithdrawalTableGUID { get; set; }
		public int ProductType { get; set; }
		public string ReceiptTempTableGUID { get; set; }
		public string Remark { get; set; }
		public decimal RetentionAmount { get; set; }
		public int RetentionCalculateBase { get; set; }
		public decimal RetentionPct { get; set; }
		public decimal SettleTermExtensionFeeAmount { get; set; }
		public bool TermExtension { get; set; }
		public decimal TermExtensionFeeAmount { get; set; }
		public string TermExtensionInvoiceRevenueTypeGUID { get; set; }
		public decimal TotalInterestPct { get; set; }
		public decimal WithdrawalAmount { get; set; }
		public string WithdrawalDate { get; set; }
		public string WithdrawalId { get; set; }
		public int CreditLimitType_CreditLimitConditionType { get; set; }
		public bool CreditLimitType_ValidateBuyerAgreement { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public string ExtendWithdrawalTable_Values { get; set; }
		public string OriginalWithdrawalTable_Values { get; set; }
		public decimal NetPaid { get; set; }
		// MainAgreementTable
		public string DocumentStatus_StatusId { get; set; }
		// ExtendTerm
		public string CustomerTable_CustomerId { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string CreditTerm_CreditTermId { get; set; }
		public string DocumentReasonGUID { get; set; }
		public string OperReportSignatureGUID { get; set; }
		public string CreditTerm_Values { get; set; }
		public string InvoiceRevenueType_IntercompanyTableGUID { get; set; }
	}
}
