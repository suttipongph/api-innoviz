using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class RaceListView : ViewCompanyBaseEntity
	{
		public string RaceGUID { get; set; }
		public string RaceId { get; set; }
		public string Description { get; set; }
	}
}
