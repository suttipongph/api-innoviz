using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class DocumentConditionTemplateLineListView : ViewCompanyBaseEntity
	{
		public string DocumentConditionTemplateLineGUID { get; set; }
		public string DocumentConditionTemplateTableGUID { get; set; }
		public string DocumentTypeGUID { get; set; }
		public bool Mandatory { get; set; }
		public string DocumentTypeId { get; set; }
		public string DocumentType_Values { get; set; }
	}
}
