using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BuyerCreditLimitByProductItemView : ViewCompanyBaseEntity
	{
		public string BuyerCreditLimitByProductGUID { get; set; }
		public string BuyerTableGUID { get; set; }
		public decimal CreditLimit { get; set; }
		public int ProductType { get; set; }
		public decimal CreditLimitBalance { get; set; }

	}
}
