using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class MethodOfPaymentListView : ViewCompanyBaseEntity
	{
		public string MethodOfPaymentGUID { get; set; }
		public string MethodOfPaymentId { get; set; }
		public string Description { get; set; }
		public string CompanyBankGUID { get; set; }
		public int PaymentType { get; set; }
		public string PaymentRemark { get; set; }
		public string CompanyBank_Values { get; set; }
		public string CompanyBank_AccountNumber { get; set; }
		public int AccountType { get; set; }
		public string AccountNum { get; set; }
	}
}
