using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ProdUnitListView : ViewCompanyBaseEntity
	{
		public string ProdUnitGUID { get; set; }
		public string Description { get; set; }
		public string UnitId { get; set; }
	}
}
