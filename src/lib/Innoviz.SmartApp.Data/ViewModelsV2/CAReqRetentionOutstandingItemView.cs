using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class CAReqRetentionOutstandingItemView : ViewCompanyBaseEntity
	{
		public string CAReqRetentionOutstandingGUID { get; set; }
		public decimal AccumRetentionAmount { get; set; }
		public string CreditAppRequestTableGUID { get; set; }
		public string CreditAppTableGUID { get; set; }
		public string CustomerTableGUID { get; set; }
		public decimal MaximumRetention { get; set; }
		public int ProductType { get; set; }
		public decimal RemainingAmount { get; set; }
	}
}
