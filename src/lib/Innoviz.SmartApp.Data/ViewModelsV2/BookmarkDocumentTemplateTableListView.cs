using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BookmarkDocumentTemplateTableListView : ViewCompanyBaseEntity
	{
		public string BookmarkDocumentTemplateTableGUID { get; set; }
		public string BookmarkDocumentTemplateId { get; set; }
		public string Description { get; set; }
		public int BookmarkDocumentRefType { get; set; }
	}
}
