using System.Collections.Generic;
using System.Collections.ObjectModel;
using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ImportTaxReportView : ViewCompanyBaseEntity
	{
		public string TaxReportTransGUID { get; set; }
		public string Amount { get; set; }
		public string Description { get; set; }
		public string Month { get; set; }
		public string RefGUID { get; set; }
		public string RefId { get; set; }
		public int RefType { get; set; }
		public string Name { get; set; }
		public string Year { get; set; }
 		public FileInformation FileInfo { get; set; }
		 
		public static IReadOnlyDictionary<string, string> GetMapper()
		{
			// ("ExcelColumnName", "ModelColumnName")
			IDictionary<string, string> dict = new Dictionary<string, string>();
			dict.Add("Year", "Year");
			dict.Add("Month", "Month");
			dict.Add("Description", "Description");
			dict.Add("Amount", "Amount");
			return new ReadOnlyDictionary<string, string>(dict);
		}
	}
}
