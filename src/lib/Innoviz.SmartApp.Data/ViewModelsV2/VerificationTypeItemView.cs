using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class VerificationTypeItemView : ViewCompanyBaseEntity
	{
		public string VerificationTypeGUID { get; set; }
		public string Description { get; set; }
		public string VerificationTypeId { get; set; }
		public int VerifyType { get; set; }
	}
}
