﻿using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class InquiryRollbillPurchaseLineItemView : ViewCompanyBaseEntity
	{
		public string PurchaseLineGUID { get; set; }
		public int LineNum { get; set; }
		public string DueDate { get; set; }
		public int NumberOfRollbill { get; set; }
		public string PurchaseTable_PurchaseId { get; set; }
		public decimal OutstandingBuyerInvoiceAmount { get; set; }
		public string PurchaseTable_PurchaseDate { get; set; }
		public string InterestDate { get; set; }
		public decimal LinePurchaseAmount { get; set; }
		public string DocumentStatus_Description { get; set; }
	}
}

