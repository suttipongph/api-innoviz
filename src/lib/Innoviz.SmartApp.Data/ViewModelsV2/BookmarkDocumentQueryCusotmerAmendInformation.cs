﻿using Innoviz.SmartApp.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class BookmarkDocumentQueryCusotmerAmendInformation
    {
        [BookmarkMapping("CANumber")]
        public string QCreditAppRequestTableCusAmnedInfo_CreditAppRequestId { get; set; }
        public Guid? QCreditAppRequestTableCusAmnedInfo_RefCreditAppTableGUID { get; set; }
        [BookmarkMapping("RefCANumber")]
        public string QCreditAppRequestTableCusAmnedInfo_OriginalCreditAppTableId { get; set; }
        [BookmarkMapping("CACreateDate")]
        public DateTime? QCreditAppRequestTableCusAmnedInfo_RequestDate { get; set; }
        public Guid? QCreditAppRequestTableCusAmnedInfo_CustomerTableGUID { get; set; }
        [BookmarkMapping("CADesp")]
        public string QCreditAppRequestTableCusAmnedInfo_Description { get; set; }
        [BookmarkMapping("CARemark")]
        public string QCreditAppRequestTableCusAmnedInfo_Remark { get; set; }
        public Guid? QCreditAppRequestTableCusAmnedInfo_DocumentStatusGUID { get; set; }
        [BookmarkMapping("CAStatus")]
        public string QCreditAppRequestTableCusAmnedInfo_Status { get; set; }
        public Guid? QCreditAppRequestTableCusAmnedInfo_KYCSetupGUID { get; set; }
         [BookmarkMapping("KYCHeader")]
        public string QCreditAppRequestTableCusAmnedInfo_KYC { get; set; }
        public Guid? QCreditAppRequestTableCusAmnedInfo_CreditScoringGUID { get; set; }
        [BookmarkMapping("CreditScoring")]
        public string QCreditAppRequestTableCusAmnedInfo_CreditScoring { get; set; }
        public Guid? QCreditAppRequestTableCusAmnedInfo_DocumentReasonGUID { get; set; }
        public Guid? QCreditAppRequestTableCusAmnedInfo_RegisteredAddressGUID { get; set; }
        public Guid? QCreditAppRequestTableCusAmnedInfo_BankAccountControlGUID { get; set; }
        [BookmarkMapping("CustomerAddress")]
        public string QCreditAppRequestTableCusAmnedInfo_RegisteredAddress { get; set; }
        [BookmarkMapping("MarketingComment")]
        public string QCreditAppRequestTableCusAmnedInfo_MarketingComment { get; set; }
        [BookmarkMapping("CreditComment")]
        public string QCreditAppRequestTableCusAmnedInfo_CreditComment { get; set; }
        [BookmarkMapping("ApproverComment")]
        public string QCreditAppRequestTableCusAmnedInfo_ApproverComment { get; set; }
        public Guid? QCreditAppRequestTableCusAmnedInfo_InterestTypeGUID { get; set; }
        [BookmarkMapping("NewInterestRateType1")]
        public string QCreditAppRequestTableCusAmnedInfo_InterestType { get; set; }
        public decimal QCreditAppRequestTableCusAmnedInfo_InterestValue { get; set; }
        public decimal QCreditAppRequestTableCusAmnedInfo_TotalInterestPct { get; set; }
        [BookmarkMapping("NewMaximumRetentionRate1")]
        public decimal QCreditAppRequestTableCusAmnedInfo_MaxRetentionPct { get; set; }
        [BookmarkMapping("NewMaximumRetentionAmt1")]
        public decimal QCreditAppRequestTableCusAmnedInfo_MaxRetentionAmount { get; set; }
        [BookmarkMapping("NewCreditLimitRequestFeeRate1")]
        public decimal QCreditAppRequestTableCusAmnedInfo_CreditLimitRequest { get; set; }
        [BookmarkMapping("NewPurchaseFeeRate1")]
        public decimal QCreditAppRequestTableCusAmnedInfo_PurchaseFeePct { get; set; }
        [BookmarkMapping("NewPurchaseFeeCalBase1")]
        public string QCreditAppRequestTableCusAmnedInfo_PurchaseFeeCalculateBase { get; set; }
        public string QCreditAppRequestTableCusAmnedInfo_InterestId { get; set; }
        public string QOriginalCreditAppTableCusAmendInfo_OriginalCreditAppTableId { get; set; }
        [BookmarkMapping("CustomerCode")]
        public string QCustomerCusAmendInfo_CustomerId { get; set; }
        [BookmarkMapping("CustomerName1","CustomerName2")]
        public string QCustomerCusAmendInfo_CustomerName { get; set; }
        public int QCustomerCusAmendInfo_IdentificationType { get; set; }
        public string QCustomerCusAmendInfo_TaxID { get; set; }
        public string QCustomerCusAmendInfo_PassportID { get; set; }
        public int QCustomerCusAmendInfo_RecordType { get; set; }
        public DateTime? QCustomerCusAmendInfo_DateOfEstablish { get; set; }
        public DateTime? QCustomerCusAmendInfo_DateOfBirth { get; set; }
        public Guid? QCustomerCusAmendInfo_IntroducedByGUID { get; set; }
        public Guid? QCustomerCusAmendInfo_BusinessTypeGUID { get; set; }
        public Guid? QCustomerCusAmendInfo_ResponsibleByGUID { get; set; }
        [BookmarkMapping("SalesResp")]
        public string QCustomerCusAmendInfo_ResponsibleBy { get; set; }
        [BookmarkMapping("CustBusinessType")]
        public string QCustomerCusAmendInfo_BusinessType { get; set; }
        [BookmarkMapping("Channel")]
        public string QCustomerCusAmendInfo_IntroducedBy { get; set; }
        [BookmarkMapping("CustBankName")]
        public string QBankAccountControlCusAmendInfo_CustBankName { get; set; }
        [BookmarkMapping("CustBankBranch")]
        public string QBankAccountControlCusAmendInfo_CustBankBranch { get; set; }
        [BookmarkMapping("CustBankAccountType")]
        public string QBankAccountControlCusAmendInfo_CustBankType { get; set; }
        [BookmarkMapping("CustBankAccountNumber")]
        public string QBankAccountControlCusAmendInfo_CustBankAccountName { get; set; }
        [BookmarkMapping("OriginalMaximumRetentionRate1")]
        public decimal QCreditAppRequestTableAmendCusAmendInfo_OriginalMaxRetentionPct { get; set; }
        [BookmarkMapping("OriginalMaximumRetentionAmt1")]
        public decimal QCreditAppRequestTableAmendCusAmendInfo_OriginalMaxRetentionAmount { get; set; }
        [BookmarkMapping("OldCreditLimitRequestFeeRate1")]
        public decimal QCreditAppRequestTableAmendCusAmendInfo_OriginalCreditLimitRequestFeeAmount { get; set; }
        [BookmarkMapping("OldPurchaseFeeRate1")]
        public decimal QCreditAppRequestTableAmendCusAmendInfo_OriginalPurchaseFeePct { get; set; }
        [BookmarkMapping("OldPurchaseFeeCalBase1")]
        public string QCreditAppRequestTableAmendCusAmendInfo_OriginalPurchaseFeeCalculateBase { get; set; }
        public Guid? QCreditAppTableCusAmendInfo_CreditAppTableGUID { get; set; }
        [BookmarkMapping("MaximumRetentionRate1")]
        public decimal QCreditAppTableCusAmendInfo_MaxRetentionPct { get; set; }
        [BookmarkMapping("MaximumRetentionAmt1")]
        public decimal QCreditAppTableCusAmendInfo_MaxRetentionAmount { get; set; }


        /// /////////////////////////
        [BookmarkMapping("RetentionDeductionMethod1st_1")]
        public string QRetentionConditionTransCusAmendInfo_RetentionDeductionMethod_1 { get; set; }
        [BookmarkMapping("RetentionCalculateBase1st_1")]
        public string QRetentionConditionTransCusAmendInfo_RetentionCalculateBase_1 { get; set; }
        [BookmarkMapping("RetentionRate1st_1")]
        public decimal QRetentionConditionTransCusAmendInfo_RetentionPct_1 { get; set; }
        [BookmarkMapping("RetentionAmt1st_1")]
        public decimal QRetentionConditionTransCusAmendInfo_RetentionAmount_1 { get; set; }

        [BookmarkMapping("RetentionDeductionMethod2nd_1")]
        public string QRetentionConditionTransCusAmendInfo_RetentionDeductionMethod_2 { get; set; }
        [BookmarkMapping("RetentionCalculateBase2nd_1")]
        public string QRetentionConditionTransCusAmendInfo_RetentionCalculateBase_2 { get; set; }
        [BookmarkMapping("RetentionRate2nd_1")]
        public decimal QRetentionConditionTransCusAmendInfo_RetentionPct_2 { get; set; }
        [BookmarkMapping("RetentionAmt2nd_1")]
        public decimal QRetentionConditionTransCusAmendInfo_RetentionAmount_2 { get; set; }

        [BookmarkMapping("RetentionDeductionMethod3rd_1")]
        public string QRetentionConditionTransCusAmendInfo_RetentionDeductionMethod_3 { get; set; }
        [BookmarkMapping("RetentionCalculateBase3rd_1")]
        public string QRetentionConditionTransCusAmendInfo_RetentionCalculateBase_3 { get; set; }
        [BookmarkMapping("RetentionRate3rd_1")]
        public decimal QRetentionConditionTransCusAmendInfo_RetentionPct_3 { get; set; }
        [BookmarkMapping("RetentionAmt3rd_1")]
        public decimal QRetentionConditionTransCusAmendInfo_RetentionAmount_3 { get; set; }


        /// /////////////////////////

        [BookmarkMapping("AuthorizedCustNew1st_1")]
        public string QAuthorizedCustCusAmendInfo_AuthorizedCustNew_1 { get; set; }
      
        public Guid? QAuthorizedCustCusAmendInfo_AuthorizedPersonTypeGUID_1 { get; set; }
        [BookmarkMapping("AuthorizedPositionCustNew1st_1")]

        public string QAuthorizedCustCusAmendInfo_AuthorizedPositionCustNew_1 { get; set; }

        [BookmarkMapping("AuthorizedCustNew2nd_1")]
        public string QAuthorizedCustCusAmendInfo_AuthorizedCustNew_2 { get; set; }
        public Guid? QAuthorizedCustCusAmendInfo_AuthorizedPersonTypeGUID_2 { get; set; }
        [BookmarkMapping("AuthorizedPositionCustNew2nd_1")]
        public string QAuthorizedCustCusAmendInfo_AuthorizedPositionCustNew_2 { get; set; }

        [BookmarkMapping("AuthorizedCustNew3rd_1")]
        public string QAuthorizedCustCusAmendInfo_AuthorizedCustNew_3 { get; set; }
        public Guid? QAuthorizedCustCusAmendInfo_AuthorizedPersonTypeGUID_3 { get; set; }
        [BookmarkMapping("AuthorizedPositionCustNew3rd_1")]
        public string QAuthorizedCustCusAmendInfo_AuthorizedPositionCustNew_3 { get; set; }

        [BookmarkMapping("AuthorizedCustNew4th_1")]
        public string QAuthorizedCustCusAmendInfo_AuthorizedCustNew_4 { get; set; }
        public Guid? QAuthorizedCustCusAmendInfo_AuthorizedPersonTypeGUID_4 { get; set; }
        [BookmarkMapping("AuthorizedPositionCustNew4th_1")]
        public string QAuthorizedCustCusAmendInfo_AuthorizedPositionCustNew_4 { get; set; }

        [BookmarkMapping("AuthorizedCustNew5th_1")]
        public string QAuthorizedCustCusAmendInfo_AuthorizedCustNew_5 { get; set; }
        public Guid? QAuthorizedCustCusAmendInfo_AuthorizedPersonTypeGUID_5 { get; set; }
        [BookmarkMapping("AuthorizedPositionCustNew5th_1")]
        public string QAuthorizedCustCusAmendInfo_AuthorizedPositionCustNew_5 { get; set; }

        [BookmarkMapping("AuthorizedCustNew6th_1")]
        public string QAuthorizedCustCusAmendInfo_AuthorizedCustNew_6 { get; set; }
        public Guid? QAuthorizedCustCusAmendInfo_AuthorizedPersonTypeGUID_6 { get; set; }
        [BookmarkMapping("AuthorizedPositionCustNew6th_1")]
        public string QAuthorizedCustCusAmendInfo_AuthorizedPositionCustNew_6 { get; set; }

        // R02
        [BookmarkMapping("AuthorizedCustOld1st_1")]
        public string QAuthorizedCustOldCusAmendInfo_AuthorizedCustOld_1 { get; set; }

        public Guid? QAuthorizedCustOldCusAmendInfo_AuthorizedPersonTypeGUID_1 { get; set; }
        [BookmarkMapping("AuthorizedPositionCustOld1st_1")]

        public string QAuthorizedCustOldCusAmendInfo_AuthorizedPositionCustOld_1 { get; set; }

        [BookmarkMapping("AuthorizedCustOld2nd_1")]
        public string QAuthorizedCustOldCusAmendInfo_AuthorizedCustOld_2 { get; set; }
        public Guid? QAuthorizedCustOldCusAmendInfo_AuthorizedPersonTypeGUID_2 { get; set; }
        [BookmarkMapping("AuthorizedPositionCustOld2nd_1")]
        public string QAuthorizedCustOldCusAmendInfo_AuthorizedPositionCustOld_2 { get; set; }

        [BookmarkMapping("AuthorizedCustOld3rd_1")]
        public string QAuthorizedCustOldCusAmendInfo_AuthorizedCustOld_3 { get; set; }
        public Guid? QAuthorizedCustOldCusAmendInfo_AuthorizedPersonTypeGUID_3 { get; set; }
        [BookmarkMapping("AuthorizedPositionCustOld3rd_1")]
        public string QAuthorizedCustOldCusAmendInfo_AuthorizedPositionCustOld_3 { get; set; }

        [BookmarkMapping("AuthorizedCustOld4th_1")]
        public string QAuthorizedCustOldCusAmendInfo_AuthorizedCustOld_4 { get; set; }
        public Guid? QAuthorizedCustOldCusAmendInfo_AuthorizedPersonTypeGUID_4 { get; set; }
        [BookmarkMapping("AuthorizedPositionCustOld4th_1")]
        public string QAuthorizedCustOldCusAmendInfo_AuthorizedPositionCustOld_4 { get; set; }

        [BookmarkMapping("AuthorizedCustOld5th_1")]
        public string QAuthorizedCustOldCusAmendInfo_AuthorizedCustOld_5 { get; set; }
        public Guid? QAuthorizedCustOldCusAmendInfo_AuthorizedPersonTypeGUID_5 { get; set; }
        [BookmarkMapping("AuthorizedPositionCustOld5th_1")]
        public string QAuthorizedCustOldCusAmendInfo_AuthorizedPositionCustOld_5 { get; set; }

        [BookmarkMapping("AuthorizedCustOld6th_1")]
        public string QAuthorizedCustOldCusAmendInfo_AuthorizedCustOld_6 { get; set; }
        public Guid? QAuthorizedCustOldCusAmendInfo_AuthorizedPersonTypeGUID_6 { get; set; }
        [BookmarkMapping("AuthorizedPositionCustOld6th_1")]
        public string QAuthorizedCustOldCusAmendInfo_AuthorizedPositionCustOld_6 { get; set; }

        /// ///////////////////////////////////////////
        [BookmarkMapping("GuarantorNew1st_1")]
        public string QGuarantorNewCusAmendInfo_GuarantorName_1 { get; set; }
        [BookmarkMapping("GuarantorNewPosition1st_1")]
        public string QGuarantorNewCusAmendInfo_GuarantorPositionNew_1 { get; set; }
        [BookmarkMapping("GuarantorNew2nd_1")]
        public string QGuarantorNewCusAmendInfo_GuarantorName_2 { get; set; }
        [BookmarkMapping("GuarantorNewPosition2nd_1")]
        public string QGuarantorNewCusAmendInfo_GuarantorPositionNew_2 { get; set; }
        [BookmarkMapping("GuarantorNew3rd_1")]
        public string QGuarantorNewCusAmendInfo_GuarantorName_3 { get; set; }
        [BookmarkMapping("GuarantorNewPosition3rd_1")]
        public string QGuarantorNewCusAmendInfo_GuarantorPositionNew_3 { get; set; }
        [BookmarkMapping("GuarantorNew4th_1")]
        public string QGuarantorNewCusAmendInfo_GuarantorName_4 { get; set; }
        [BookmarkMapping("GuarantorNewPosition4th_1")]
        public string QGuarantorNewCusAmendInfo_GuarantorPositionNew_4 { get; set; }
        [BookmarkMapping("GuarantorNew5th_1")]
        public string QGuarantorNewCusAmendInfo_GuarantorName_5 { get; set; }
        [BookmarkMapping("GuarantorNewPosition5th_1")]
        public string QGuarantorNewCusAmendInfo_GuarantorPositionNew_5 { get; set; }
        [BookmarkMapping("GuarantorNew6th_1")]
        public string QGuarantorNewCusAmendInfo_GuarantorName_6 { get; set; }
        [BookmarkMapping("GuarantorNewPosition6th_1")]
        public string QGuarantorNewCusAmendInfo_GuarantorPositionNew_6 { get; set; }

        /// ///////////////////////////////////////////
        [BookmarkMapping("GuarantorOld1st_1")]
        public string QGuarantorOldCusAmendInfo_GuarantorName_1 { get; set; }
        [BookmarkMapping("GuarantorOldPosition1st_1")]
        public string QGuarantorOldCusAmendInfo_GuarantorPositionNew_1 { get; set; }
        [BookmarkMapping("GuarantorOld2nd_1")]
        public string QGuarantorOldCusAmendInfo_GuarantorName_2 { get; set; }
        [BookmarkMapping("GuarantorOldPosition2nd_1")]
        public string QGuarantorOldCusAmendInfo_GuarantorPositionNew_2 { get; set; }
        [BookmarkMapping("GuarantorOld3rd_1")]
        public string QGuarantorOldCusAmendInfo_GuarantorName_3 { get; set; }
        [BookmarkMapping("GuarantorOldPosition3rd_1")]
        public string QGuarantorOldCusAmendInfo_GuarantorPositionNew_3 { get; set; }
        [BookmarkMapping("GuarantorOld4th_1")]
        public string QGuarantorOldCusAmendInfo_GuarantorName_4 { get; set; }
        [BookmarkMapping("GuarantorOldPosition4th_1")]
        public string QGuarantorOldCusAmendInfo_GuarantorPositionNew_4 { get; set; }
        [BookmarkMapping("GuarantorOld5th_1")]
        public string QGuarantorOldCusAmendInfo_GuarantorName_5 { get; set; }
        [BookmarkMapping("GuarantorOldPosition5th_1")]
        public string QGuarantorOldCusAmendInfo_GuarantorPositionNew_5 { get; set; }
        [BookmarkMapping("GuarantorOld6th_1")]
        public string QGuarantorOldCusAmendInfo_GuarantorName_6 { get; set; }
        [BookmarkMapping("GuarantorOldPosition6th_1")]
        public string QGuarantorOldCusAmendInfo_GuarantorPositionNew_6 { get; set; }

        /// //////////////////////

        [BookmarkMapping("ApproverReasonFirst1")]
        public string QActionHistoryCusAmnedInfo_Comment_1 { get; set; }
        [BookmarkMapping("ApprovedDateFirst1")]
        public string QActionHistoryCusAmnedInfo_CreatedDateTime_1 { get; set; }
        public string QActionHistoryCusAmnedInfo_ActionName_1 { get; set; }
        [BookmarkMapping("ApproverNameFirst1")]
        public string QActionHistoryCusAmnedInfo_ApproverName_1 { get; set; }

        [BookmarkMapping("ApproverReasonSecond1")]
        public string QActionHistoryCusAmnedInfo_Comment_2 { get; set; }
        [BookmarkMapping("ApprovedDateSecond1")]
        public string QActionHistoryCusAmnedInfo_CreatedDateTime_2 { get; set; }
        public string QActionHistoryCusAmnedInfo_ActionName_2 { get; set; }
        [BookmarkMapping("ApproverNameSecond1")]
        public string QActionHistoryCusAmnedInfo_ApproverName_2 { get; set; }

        [BookmarkMapping("ApproverReasonThird1")]
        public string QActionHistoryCusAmnedInfo_Comment_3 { get; set; }
        [BookmarkMapping("ApprovedDateThird1")]
        public string QActionHistoryCusAmnedInfo_CreatedDateTime_3 { get; set; }
        public string QActionHistoryCusAmnedInfo_ActionName_3 { get; set; }
        [BookmarkMapping("ApproverNameThird1")]
        public string QActionHistoryCusAmnedInfo_ApproverName_3 { get; set; }
        [BookmarkMapping("ApproverReasonFourth1")]
        public string QActionHistoryCusAmnedInfo_Comment_4 { get; set; }
        [BookmarkMapping("ApprovedDateFourth1")]
        public string QActionHistoryCusAmnedInfo_CreatedDateTime_4 { get; set; }
        public string QActionHistoryCusAmnedInfo_ActionName_4 { get; set; }
        [BookmarkMapping("ApproverNameFourth1")]
        public string QActionHistoryCusAmnedInfo_ApproverName_4 { get; set; }

        [BookmarkMapping("ApproverReasonFifth1")]
        public string QActionHistoryCusAmnedInfo_Comment_5 { get; set; }
        [BookmarkMapping("ApprovedDateFifth1")]
        public string QActionHistoryCusAmnedInfo_CreatedDateTime_5 { get; set; }
        public string QActionHistoryCusAmnedInfo_ActionName_5 { get; set; }
        [BookmarkMapping("ApproverNameFifth1")]
        public string QActionHistoryCusAmnedInfo_ApproverName_5 { get; set; }
        //////////////
        [BookmarkMapping("CustCompanyRegistrationID")]
        public string BookmarkDocumentQueryCusotmerAmendInformation_CustCompanyRegistrationID { get; set; }
        [BookmarkMapping("CustComEstablishedDate")]
        public DateTime? BookmarkDocumentQueryCusotmerAmendInformation_CustComEstablishedDate { get; set; }
        [BookmarkMapping("NewInterestRate1")]
        public decimal BookmarkDocumentQueryCusotmerAmendInformation_NewInterestRate { get; set; }

        public string QCreditAppRequestTableAmendCusAmendInfo_InterestAdjiusment { get; set; }
        [BookmarkMapping("OldInterestRate1")]
        public decimal QCreditAppRequestTableAmendCusAmendInfo_OriginalTotalLnterestPct { get; set; }
        [BookmarkMapping("OldInterestRateType1")]
        public string QCreditAppRequestTableAmendCusAmendInfo_OriginalIntersetType { get; set; }

        // R02
        [BookmarkMapping("NewInterestAdjustmentRate1")]
        public decimal QCreditAppRequestTableCusAmendInfo_InterestAdjustment { get; set; }
        [BookmarkMapping("NewMaximumPurchase1")]
        public decimal QCreditAppRequestTableCusAmendInfo_MaximumPurchasePct { get; set; }


        [BookmarkMapping("OldInterestAdjustmentRate1")]
        public decimal QCreditAppRequestTableAmendCusAmendInfo_OriginalInterestAdjustmentPct { get; set; }
        [BookmarkMapping("OldMaximumPurchase1")]
        public decimal QCreditAppRequestTableAmendCusAmendInfo_OriginalMaxPurchasePct { get; set; }
    }
    public class QCreditAppRequestTableCusAmnedInfo
    {
        public string QCreditAppRequestTableCusAmnedInfo_CreditAppRequestId { get; set; }
        public Guid? QCreditAppRequestTableCusAmnedInfo_RefCreditAppTableGUID { get; set; }
        public string QCreditAppRequestTableCusAmnedInfo_OriginalCreditAppTableId { get; set; }
        public DateTime? QCreditAppRequestTableCusAmnedInfo_RequestDate { get; set; }
        public Guid? QCreditAppRequestTableCusAmnedInfo_CustomerTableGUID { get; set; }
        public string QCreditAppRequestTableCusAmnedInfo_Description { get; set; }
        public string QCreditAppRequestTableCusAmnedInfo_Remark { get; set; }
        public Guid? QCreditAppRequestTableCusAmnedInfo_DocumentStatusGUID { get; set; }
        public string QCreditAppRequestTableCusAmnedInfo_Status { get; set; }
        public Guid? QCreditAppRequestTableCusAmnedInfo_KYCSetupGUID { get; set; }
        public string QCreditAppRequestTableCusAmnedInfo_KYC { get; set; }
        public Guid? QCreditAppRequestTableCusAmnedInfo_CreditScoringGUID { get; set; }
        public string QCreditAppRequestTableCusAmnedInfo_CreditScoring { get; set; }
        public Guid? QCreditAppRequestTableCusAmnedInfo_DocumentReasonGUID { get; set; }
        public Guid? QCreditAppRequestTableCusAmnedInfo_RegisteredAddressGUID { get; set; }
        public Guid? QCreditAppRequestTableCusAmnedInfo_BankAccountControlGUID { get; set; }
        public string QCreditAppRequestTableCusAmnedInfo_RegisteredAddress { get; set; }
        public string QCreditAppRequestTableCusAmnedInfo_MarketingComment { get; set; }
        public string QCreditAppRequestTableCusAmnedInfo_CreditComment { get; set; }
        public string QCreditAppRequestTableCusAmnedInfo_ApproverComment { get; set; }
        public Guid? QCreditAppRequestTableCusAmnedInfo_InterestTypeGUID { get; set; }
        public string QCreditAppRequestTableCusAmnedInfo_InterestType { get; set; }
        public decimal QCreditAppRequestTableCusAmnedInfo_InterestValue { get; set; }
        public decimal QCreditAppRequestTableCusAmnedInfo_TotalInterestPct { get; set; }
        public decimal QCreditAppRequestTableCusAmnedInfo_MaxRetentionPct { get; set; }
        public decimal QCreditAppRequestTableCusAmnedInfo_MaxRetentionAmount { get; set; }
        public decimal QCreditAppRequestTableCusAmnedInfo_CreditLimitRequest { get; set; }
        public decimal QCreditAppRequestTableCusAmnedInfo_PurchaseFeePct { get; set; }
        public int QCreditAppRequestTableCusAmnedInfo_PurchaseFeeCalculateBase { get; set; }
        public string QCreditAppRequestTableCusAmnedInfo_InterestId { get; set; }
        public decimal QCreditAppRequestTableAmendCusAmendInfo_InterestAdjustment { get; set; }
        public decimal QCreditAppRequestTableAmendCusAmendInfo_CreditRequestFeeAmount { get; set; }
        public decimal QCreditAppRequestTableAmendCusAmendInfo_MaximumPurchasePct { get; set; }

    }
    public class QOriginalCreditAppTableCusAmendInfo
    {
       public string QOriginalCreditAppTableCusAmendInfo_OriginalCreditAppTableId { get; set; }
       public decimal QOriginalCreditAppTableCusAmendInfo_MaxRetentionPct { get; set; }
       public decimal QOriginalCreditAppTableCusAmendInfo_MaxRetentionAmount { get; set; }
       public Guid QOriginalCreditAppTableCusAmendInfo_CreditAppTableGUID { get; set; }
    }
    public class QCustomerCusAmendInfo
    {
        public string QCustomerCusAmendInfo_CustomerId { get; set; }
        public string QCustomerCusAmendInfo_CustomerName { get; set; }
        public int QCustomerCusAmendInfo_IdentificationType { get; set; }
        public string QCustomerCusAmendInfo_TaxID { get; set; }
        public string QCustomerCusAmendInfo_PassportID { get; set; }
        public int QCustomerCusAmendInfo_RecordType { get; set; }
        public DateTime? QCustomerCusAmendInfo_DateOfEstablish { get; set; }
        public DateTime? QCustomerCusAmendInfo_DateOfBirth { get; set; }
        public Guid? QCustomerCusAmendInfo_IntroducedByGUID { get; set; }
        public Guid? QCustomerCusAmendInfo_BusinessTypeGUID { get; set; }
        public Guid? QCustomerCusAmendInfo_ResponsibleByGUID { get; set; }
        public string QCustomerCusAmendInfo_ResponsibleBy { get; set; }
        public string QCustomerCusAmendInfo_BusinessType { get; set; }
        public string QCustomerCusAmendInfo_IntroducedBy { get; set; }
    }
    public class QBankAccountControlCusAmendInfo
    {
        public string QBankAccountControlCusAmendInfo_CustBankName { get; set; }
        public string QBankAccountControlCusAmendInfo_CustBankBranch { get; set; }
        public string QBankAccountControlCusAmendInfo_CustBankType { get; set; }
        public string QBankAccountControlCusAmendInfo_CustBankAccountName { get; set; }

    }
    public class QCreditAppRequestTableAmendCusAmendInfo
    {
        public decimal QCreditAppRequestTableAmendCusAmendInfo_OriginalMaxRetentionPct { get; set;}
        public decimal QCreditAppRequestTableAmendCusAmendInfo_OriginalMaxRetentionAmount { get; set; }
        public decimal QCreditAppRequestTableAmendCusAmendInfo_OriginalCreditLimitRequestFeePct { get; set; }
        public decimal QCreditAppRequestTableAmendCusAmendInfo_OriginalPurchaseFeePct { get; set; }
        public int QCreditAppRequestTableAmendCusAmendInfo_OriginalPurchaseFeeCalculateBase { get; set; }
        public decimal QCreditAppRequestTableAmendCusAmendInfo_OriginalTotalLnterestPct { get; set; }
        public string QCreditAppRequestTableAmendCusAmendInfo_OriginalIntersetType { get; set; }

        // R02
        public decimal QCreditAppRequestTableAmendCusAmendInfo_OriginalInterestAdjustmentPct { get; set; }
        public decimal QCreditAppRequestTableAmendCusAmendInfo_OriginalCreditLimitRequestFeeAmount { get; set; }
        public decimal QCreditAppRequestTableAmendCusAmendInfo_OriginalMaxPurchasePct { get; set; }
    }
    //public class QCreditAppTableCusAmendInfo
    //{
    //    public Guid? QCreditAppTableCusAmendInfo_CreditAppTableGUID { get; set; }
    //    public decimal QCreditAppTableCusAmendInfo_MaxRetentionPct { get; set; }
    //    public decimal QCreditAppTableCusAmendInfo_MaxRetentionAmount { get; set; }
    //}
    public class QRetentionConditionTransCusAmendInfo
    {
        public int QRetentionConditionTransCusAmendInfo_RetentionDeductionMethod { get; set; }
        public int QRetentionConditionTransCusAmendInfo_RetentionCalculateBase { get; set; }
        public decimal QRetentionConditionTransCusAmendInfo_RetentionPct { get; set; }
        public decimal QRetentionConditionTransCusAmendInfo_RetentionAmount { get; set; }
        public int QRetentionConditionTransCusAmendInfo_Row { get; set; }
    }
    public class QAuthorizedCustCusAmendInfo
    {
        public string QAuthorizedCustCusAmendInfo_AuthorizedCustNew { get; set; }   
        public Guid? QAuthorizedCustCusAmendInfo_AuthorizedPersonTypeGUID { get; set; }   
        public string QAuthorizedCustCusAmendInfo_AuthorizedPositionCustNew { get; set; }
        public int QAuthorizedCustCusAmendInfo_Row { get; set; }
    }
    public class QAuthorizedCustOldCusAmendInfo
    {
        public string QAuthorizedCustOldCusAmendInfo_AuthorizedCustOld { get; set; }
        public Guid? QAuthorizedCustOldCusAmendInfo_AuthorizedPersonTypeGUID { get; set; }
        public string QAuthorizedCustOldCusAmendInfo_AuthorizedPositionCustNew { get; set; }
        public int QAuthorizedCustOldCusAmendInfo_Row { get; set; }
    }
    public class QGuarantorNewCusAmendInfo
    {
        public string QGuarantorNewCusAmendInfo_GuarantorName { get; set; }
        public string QGuarantorNewCusAmendInfo_GuarantorPositionNew { get; set; }
        public int QGuarantorNewCusAmendInfo_Row { get; set; }
    }
    public class QGuarantorOldCusAmendInfo
    {
        public string QGuarantorOldCusAmendInfo_GuarantorName { get; set; }
        public string QGuarantorOldCusAmendInfo_GuarantorPositionNew { get; set; }
        public int QGuarantorOldCusAmendInfo_Row { get; set; }
    }
    public class QActionHistoryCusAmnedInfo
    {
        public string QActionHistoryCusAmnedInfo_Comment { get; set; }
        public DateTime QActionHistoryCusAmnedInfo_CreatedDateTime { get; set; }
        public string QActionHistoryCusAmnedInfo_ActionName { get; set; }
        public string QActionHistoryCusAmnedInfo_ApproverName { get; set; }
        public int QActionHistoryCusAmnedInfo_Row { get; set; }
       
    }
}
