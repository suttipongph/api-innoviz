using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BlacklistStatusItemView : ViewCompanyBaseEntity
	{
		public string BlacklistStatusGUID { get; set; }
		public string BlacklistStatusId { get; set; }
		public string Description { get; set; }
	}
}
