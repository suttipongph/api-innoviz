﻿using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class ReportAccruedInterestView : ViewReportBaseEntity
	{
		public string AsOfDate { get; set; }
		public string[] CustomerTableGUID { get; set; }
		public string[] BuyerTableGUID { get; set; }
		public string[] InvoiceTypeGUID { get; set; }
		public int[] ProductType { get; set; }
		public int[] RefType { get; set; }
		public int SuspenseInvoiceType { get; set; }
	}
}
