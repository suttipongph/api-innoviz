using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class BusinessTypeListView : ViewCompanyBaseEntity
	{
		public string BusinessTypeGUID { get; set; }
		public string BusinessTypeId { get; set; }
		public string Description { get; set; }
	}
}
