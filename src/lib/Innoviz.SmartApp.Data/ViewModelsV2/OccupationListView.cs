using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class OccupationListView : ViewCompanyBaseEntity
	{
		public string OccupationGUID { get; set; }
		public string Description { get; set; }
		public string OccupationId { get; set; }
	}
}
