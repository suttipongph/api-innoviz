using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class GuarantorAgreementLineListView : ViewCompanyBaseEntity
	{
		public string GuarantorAgreementLineGUID { get; set; }
		public int Ordering { get; set; }
		public string GuarantorTransGUID { get; set; }
		public string OperatedBy { get; set; }
		public string TaxId { get; set; }
		public string PassportId { get; set; }
		public string Name { get; set; }
		public string GuarantorTrans_Values { get; set; }
		public string GuarantorAgreementTableGUID { get; set; }
		public bool IsAffiliateCreated { get; set; }
	}
}
