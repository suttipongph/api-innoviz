using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class AddressTransListView : ViewCompanyBaseEntity
	{
		public string AddressTransGUID { get; set; }
		public string Name { get; set; }
		public bool Primary { get; set; }
		public bool CurrentAddress { get; set; }
		public string Address1 { get; set; }
		public string RefGUID { get; set; }

	}
}
