﻿using System;
using System.Collections.Generic;
using System.Text;
using Innoviz.SmartApp.Core.Attributes;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
    public class BookmarkDocumentCreditApplication
    {
        [BookmarkMapping("CANumber")]
        public string QCreditAppRequestTable_CreditAppRequestId { get; set; }
        [BookmarkMapping("RefCANumber")]
        public string QCreditAppRequestTable_OriginalCreditAppTableId { get; set; }
        [BookmarkMapping("CAStatus")]
        public string QCreditAppRequestTable_Status { get; set; }
        [BookmarkMapping("CACreateDate")]
        public string QCreditAppRequestTable_RequestDate { get; set; }
        [BookmarkMapping("CustomerName1", "CustomerName2")]
        public string QCreditAppRequestTable_CustomerName { get; set; }
        [BookmarkMapping("CustomerCode")]
        public string QCreditAppRequestTable_CustomerId { get; set; }
        [BookmarkMapping("SalesResp")]
        public string QCreditAppRequestTable_ResponsibleBy { get; set; }
        [BookmarkMapping("KYCHeader")]
        public string QCreditAppRequestTable_KYC { get; set; }
        [BookmarkMapping("CreditScoring")]
        public string QCreditAppRequestTable_CreditScoring { get; set; }
        [BookmarkMapping("CADesp")]
        public string QCreditAppRequestTable_Description { get; set; }
        [BookmarkMapping("Purpose")]
        public string QCreditAppRequestTable_Purpose { get; set; }
        [BookmarkMapping("CARemark")]
        public string QCreditAppRequestTable_CARemark { get; set; }
        [BookmarkMapping("ProductType")]
        public string QCreditAppRequestTable_ProductType { get; set; }
        [BookmarkMapping("ProductSubType")]
        public string QCreditAppRequestTable_ProductSubType { get; set; }
        [BookmarkMapping("CreditLimitType")]
        public string QCreditAppRequestTable_CreditLimitTypeId { get; set; }
        [BookmarkMapping("CreditLimitLife")]
        public string QCreditAppRequestTable_CreditLimitExpiration { get; set; }
        [BookmarkMapping("CreditPeriodStartDate")]
        public string QCreditAppRequestTable_StartDate { get; set; }
        [BookmarkMapping("CreditPeriodEndDate")]
        public string QCreditAppRequestTable_ExpiryDate { get; set; }
        [BookmarkMapping("CreditLimitReqAmt")]
        public decimal QCreditAppRequestTable_CreditLimitRequest { get; set; }
        [BookmarkMapping("CreditAllBuyer")]
        public decimal QCreditAppRequestTable_CustomerCreditLimit { get; set; }
        [BookmarkMapping("CAInterestType")]
        public string QCreditAppRequestTable_InterestType { get; set; }
        [BookmarkMapping("InterestAdj")]
        public decimal QCreditAppRequestTable_InterestAdjustment { get; set; }
        [BookmarkMapping("TotalInterest")]
        public decimal Variable_NewInterestRate { get; set; }
        [BookmarkMapping("MaxPurch")]
        public decimal QCreditAppRequestTable_MaxPurchasePct { get; set; }
        [BookmarkMapping("MaxRetenPerc")]
        public decimal QCreditAppRequestTable_MaxRetentionPct { get; set; }
        [BookmarkMapping("MaxRetenAmt")]
        public decimal QCreditAppRequestTable_MaxRetentionAmount { get; set; }
        [BookmarkMapping("CustPDCBankName")]
        public string QCreditAppRequestTable_CustPDCBankName { get; set; }
        [BookmarkMapping("CustPDCBankBranch")]
        public string QCreditAppRequestTable_CustPDCBankBranch { get; set; }
        [BookmarkMapping("CustPDCBankAccountType")]
        public string QCreditAppRequestTable_CustPDCBankType { get; set; }
        [BookmarkMapping("CustPDCBankAccountNumber")]
        public string QCreditAppRequestTable_CustPDCBankAccountName { get; set; }
        [BookmarkMapping("CreditLimitFee")]
        public decimal QCreditAppRequestTable_CreditRequestFeePct { get; set; }
        [BookmarkMapping("CreditLimitFeeAmt")]
        public decimal QCreditAppRequestTable_CreditRequestFeeAmount { get; set; }
        [BookmarkMapping("FactoringPurchaseFee")]
        public decimal QCreditAppRequestTable_PurchaseFeePct { get; set; }
        [BookmarkMapping("FactoringPurchaseFeeCalBase")]
        public string QCreditAppRequestTable_PurchaseFeeCalculateBase { get; set; }
        [BookmarkMapping("CACondition")]
        public string QCreditAppRequestTable_CACondition { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("RetenDeductMethod01")]
        public string QRetentionConditionTrans1_RetentionDeductionMethod { get; set; }
        [BookmarkMapping("RetenCalBase01")]
        public string QRetentionConditionTrans1_RetentionCalculateBase { get; set; }
        [BookmarkMapping("RetenPerc01")]
        public decimal QRetentionConditionTrans1_RetentionPct { get; set; }
        [BookmarkMapping("RetenAmt01")]
        public decimal tentionConditionTrans1_RetentionAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("RetenDeductMethod02")]
        public string QRetentionConditionTrans2_RetentionDeductionMethod { get; set; }
        [BookmarkMapping("RetenCalBase02")]
        public string QRetentionConditionTrans2_RetentionCalculateBase { get; set; }
        [BookmarkMapping("RetenPerc02")]
        public decimal QRetentionConditionTrans2_RetentionPct { get; set; }
        [BookmarkMapping("RetenAmt02")]
        public decimal tentionConditionTrans2_RetentionAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("RetenDeductMethod03")]
        public string QRetentionConditionTrans3_RetentionDeductionMethod { get; set; }
        [BookmarkMapping("RetenCalBase03")]
        public string QRetentionConditionTrans3_RetentionCalculateBase { get; set; }
        [BookmarkMapping("RetenPerc03")]
        public decimal QRetentionConditionTrans3_RetentionPct { get; set; }
        [BookmarkMapping("RetenAmt03")]
        public decimal tentionConditionTrans3_RetentionAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ServiceFeeDesc01")]
        public string QServiceFeeConditionTrans1_ServicefeeTypeId { get; set; }
        [BookmarkMapping("ServiceFeeAmt01")]
        public decimal QServiceFeeConditionTrans1_AmountBeforeTax { get; set; }
        [BookmarkMapping("ServiceFeeRemark01")]
        public string QServiceFeeConditionTrans1_Description { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ServiceFeeDesc02")]
        public string QServiceFeeConditionTrans2_ServicefeeTypeId { get; set; }
        [BookmarkMapping("ServiceFeeAmt02")]
        public decimal QServiceFeeConditionTrans2_AmountBeforeTax { get; set; }
        [BookmarkMapping("ServiceFeeRemark02")]
        public string QServiceFeeConditionTrans2_Description { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ServiceFeeDesc03")]
        public string QServiceFeeConditionTrans3_ServicefeeTypeId { get; set; }
        [BookmarkMapping("ServiceFeeAmt03")]
        public decimal QServiceFeeConditionTrans3_AmountBeforeTax { get; set; }
        [BookmarkMapping("ServiceFeeRemark03")]
        public string QServiceFeeConditionTrans3_Description { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ServiceFeeDesc04")]
        public string QServiceFeeConditionTrans4_ServicefeeTypeId { get; set; }
        [BookmarkMapping("ServiceFeeAmt04")]
        public decimal QServiceFeeConditionTrans4_AmountBeforeTax { get; set; }
        [BookmarkMapping("ServiceFeeRemark04")]
        public string QServiceFeeConditionTrans4_Description { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ServiceFeeDesc05")]
        public string QServiceFeeConditionTrans5_ServicefeeTypeId { get; set; }
        [BookmarkMapping("ServiceFeeAmt05")]
        public decimal QServiceFeeConditionTrans5_AmountBeforeTax { get; set; }
        [BookmarkMapping("ServiceFeeRemark05")]
        public string QServiceFeeConditionTrans5_Description { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ServiceFeeDesc06")]
        public string QServiceFeeConditionTrans6_ServicefeeTypeId { get; set; }
        [BookmarkMapping("ServiceFeeAmt06")]
        public decimal QServiceFeeConditionTrans6_AmountBeforeTax { get; set; }
        [BookmarkMapping("ServiceFeeRemark06")]
        public string QServiceFeeConditionTrans6_Description { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ServiceFeeDesc07")]
        public string QServiceFeeConditionTrans7_ServicefeeTypeId { get; set; }
        [BookmarkMapping("ServiceFeeAmt07")]
        public decimal QServiceFeeConditionTrans7_AmountBeforeTax { get; set; }
        [BookmarkMapping("ServiceFeeRemark07")]
        public string QServiceFeeConditionTrans7_Description { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ServiceFeeDesc08")]
        public string QServiceFeeConditionTrans8_ServicefeeTypeId { get; set; }
        [BookmarkMapping("ServiceFeeAmt08")]
        public decimal QServiceFeeConditionTrans8_AmountBeforeTax { get; set; }
        [BookmarkMapping("ServiceFeeRemark08")]
        public string QServiceFeeConditionTrans8_Description { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ServiceFeeDesc09")]
        public string QServiceFeeConditionTrans9_ServicefeeTypeId { get; set; }
        [BookmarkMapping("ServiceFeeAmt09")]
        public decimal QServiceFeeConditionTrans9_AmountBeforeTax { get; set; }
        [BookmarkMapping("ServiceFeeRemark09")]
        public string QServiceFeeConditionTrans9_Description { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ServiceFeeDesc10")]
        public string QServiceFeeConditionTrans10_ServicefeeTypeId { get; set; }
        [BookmarkMapping("ServiceFeeAmt10")]
        public decimal QServiceFeeConditionTrans10_AmountBeforeTax { get; set; }
        [BookmarkMapping("ServiceFeeRemark10")]
        public string QServiceFeeConditionTrans10_Description { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("SumServiceFeeAmt")]
        public decimal QSumServiceFeeConditionTrans_ServiceFeeAmt { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("GuarantorNameFirst1")]
        public string QGuarantorTrans1_RelatedPersonName { get; set; }
        [BookmarkMapping("GuarantorPositionFirst1")]
        public string QGuarantorTrans1_GuarantorType { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("GuarantorNameSecond1")]
        public string QGuarantorTrans2_RelatedPersonName { get; set; }
        [BookmarkMapping("GuarantorPositionSecond1")]
        public string QGuarantorTrans2_GuarantorType { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("GuarantorNameThird1")]
        public string QGuarantorTrans3_RelatedPersonName { get; set; }
        [BookmarkMapping("GuarantorPositionThird1")]
        public string QGuarantorTrans3_GuarantorType { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("GuarantorNameFourth1")]
        public string QGuarantorTrans4_RelatedPersonName { get; set; }
        [BookmarkMapping("GuarantorPositionFourth1")]
        public string QGuarantorTrans4_GuarantorType { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("GuarantorNameFifth1")]
        public string QGuarantorTrans5_RelatedPersonName { get; set; }
        [BookmarkMapping("GuarantorPositionFifth1")]
        public string QGuarantorTrans5_GuarantorType { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("GuarantorNameSixth1")]
        public string QGuarantorTrans6_RelatedPersonName { get; set; }
        [BookmarkMapping("GuarantorPositionSixth1")]
        public string QGuarantorTrans6_GuarantorType { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("GuarantorNameSeventh1")]
        public string QGuarantorTrans7_RelatedPersonName { get; set; }
        [BookmarkMapping("GuarantorPositionSeventh1")]
        public string QGuarantorTrans7_GuarantorType { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("GuarantorNameEighth1")]
        public string QGuarantorTrans8_RelatedPersonName { get; set; }
        [BookmarkMapping("GuarantorPositionEighth1")]
        public string QGuarantorTrans8_GuarantorType { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("AssignmentNewFirst1")]
        public string Variable_IsNew1 { get; set; }
        [BookmarkMapping("AssignmentBuyerFirst1")]
        public string QCreditAppReqAssignment1_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdFirst1")]
        public string QCreditAppReqAssignment1_RefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountFirst1")]
        public decimal QCreditAppReqAssignment1_BuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountFirst1")]
        public decimal QCreditAppReqAssignment1_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDateFirst1")]
        public string QCreditAppReqAssignment1_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountFirst1")]
        public decimal QCreditAppReqAssignment1_RemainingAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("AssignmentNewSecond1")]
        public string Variable_IsNew2 { get; set; }
        [BookmarkMapping("AssignmentBuyerSecond1")]
        public string QCreditAppReqAssignment2_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdSecond1")]
        public string QCreditAppReqAssignment2_RefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountSecond1")]
        public decimal QCreditAppReqAssignment2_BuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountSecond1")]
        public decimal QCreditAppReqAssignment2_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDateSecond1")]
        public string QCreditAppReqAssignment2_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountSecond1")]
        public decimal QCreditAppReqAssignment2_RemainingAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("AssignmentNewThird1")]
        public string Variable_IsNew3 { get; set; }
        [BookmarkMapping("AssignmentBuyerThird1")]
        public string QCreditAppReqAssignment3_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdThird1")]
        public string QCreditAppReqAssignment3_RefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountThird1")]
        public decimal QCreditAppReqAssignment3_BuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountThird1")]
        public decimal QCreditAppReqAssignment3_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDateThird1")]
        public string QCreditAppReqAssignment3_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountThird1")]
        public decimal QCreditAppReqAssignment3_RemainingAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("AssignmentNewFourth1")]
        public string Variable_IsNew4 { get; set; }
        [BookmarkMapping("AssignmentBuyerFourth1")]
        public string QCreditAppReqAssignment4_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdFourth1")]
        public string QCreditAppReqAssignment4_RefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountFourth1")]
        public decimal QCreditAppReqAssignment4_BuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountFourth1")]
        public decimal QCreditAppReqAssignment4_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDateFourth1")]
        public string QCreditAppReqAssignment4_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountFourth1")]
        public decimal QCreditAppReqAssignment4_RemainingAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("AssignmentNewFifth1")]
        public string Variable_IsNew5 { get; set; }
        [BookmarkMapping("AssignmentBuyerFifth1")]
        public string QCreditAppReqAssignment5_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdFifth1")]
        public string QCreditAppReqAssignment5_RefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountFifth1")]
        public decimal QCreditAppReqAssignment5_BuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountFifth1")]
        public decimal QCreditAppReqAssignment5_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDateFifth1")]
        public string QCreditAppReqAssignment5_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountFifth1")]
        public decimal QCreditAppReqAssignment5_RemainingAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("AssignmentNewSixth1")]
        public string Variable_IsNew6 { get; set; }
        [BookmarkMapping("AssignmentBuyerSixth1")]
        public string QCreditAppReqAssignment6_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdSixth1")]
        public string QCreditAppReqAssignment6_RefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountSixth1")]
        public decimal QCreditAppReqAssignment6_BuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountSixth1")]
        public decimal QCreditAppReqAssignment6_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDateSixth1")]
        public string QCreditAppReqAssignment6_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountSixth1")]
        public decimal QCreditAppReqAssignment6_RemainingAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("AssignmentNewSeventh1")]
        public string Variable_IsNew7 { get; set; }
        [BookmarkMapping("AssignmentBuyerSeventh1")]
        public string QCreditAppReqAssignment7_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdSeventh1")]
        public string QCreditAppReqAssignment7_RefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountSeventh1")]
        public decimal QCreditAppReqAssignment7_BuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountSeventh1")]
        public decimal QCreditAppReqAssignment7_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDateSeventh1")]
        public string QCreditAppReqAssignment7_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountSeventh1")]
        public decimal QCreditAppReqAssignment7_RemainingAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("AssignmentNewEighth1")]
        public string Variable_IsNew8 { get; set; }
        [BookmarkMapping("AssignmentBuyerEighth1")]
        public string QCreditAppReqAssignment8_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdEighth1")]
        public string QCreditAppReqAssignment8_RefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountEighth1")]
        public decimal QCreditAppReqAssignment8_BuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountEighth1")]
        public decimal QCreditAppReqAssignment8_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDateEighth1")]
        public string QCreditAppReqAssignment8_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountEighth1")]
        public decimal QCreditAppReqAssignment8_RemainingAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("AssignmentNewNineth1")]
        public string Variable_IsNew9 { get; set; }
        [BookmarkMapping("AssignmentBuyerNineth1")]
        public string QCreditAppReqAssignment9_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdNineth1")]
        public string QCreditAppReqAssignment9_RefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountNineth1")]
        public decimal QCreditAppReqAssignment9_BuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountNineth1")]
        public decimal QCreditAppReqAssignment9_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDateNineth1")]
        public string QCreditAppReqAssignment9_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountNineth1")]
        public decimal QCreditAppReqAssignment9_RemainingAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("AssignmentNewTenth1")]
        public string Variable_IsNew10 { get; set; }
        [BookmarkMapping("AssignmentBuyerTenth1")]
        public string QCreditAppReqAssignment10_AssignmentBuyer { get; set; }
        [BookmarkMapping("AssignmentRefAgreementIdTenth1")]
        public string QCreditAppReqAssignment10_RefAgreementId { get; set; }
        [BookmarkMapping("AssignmentBuyerAgreementAmountTenth1")]
        public decimal QCreditAppReqAssignment10_BuyerAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementAmountTenth1")]
        public decimal QCreditAppReqAssignment10_AssignmentAgreementAmount { get; set; }
        [BookmarkMapping("AssignmentAgreementDateTenth1")]
        public string QCreditAppReqAssignment10_AssignmentAgreementDate { get; set; }
        [BookmarkMapping("AssignmentRemainingAmountTenth1")]
        public decimal QCreditAppReqAssignment10_RemainingAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("SumAssignmentBuyerAgreementAmount")]
        public decimal QSumCreditAppReqAssignment_SumBuyerAgreementAmount { get; set; }
        [BookmarkMapping("SumAssignmentAgreementAmount")]
        public decimal QSumCreditAppReqAssignment_SumAssignmentAgreementAmount { get; set; }
        [BookmarkMapping("SumAssignmentRemainingAmount")]
        public decimal QSumCreditAppReqAssignment_SumRemainingAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewFirst1")]
        public string QCreditAppReqBusinessCollateral1_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeFirst1")]
        public string QCreditAppReqBusinessCollateral1_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeFirst1")]
        public string QCreditAppReqBusinessCollateral1_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescFirst1")]
        public string QCreditAppReqBusinessCollateral1_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueFirst1")]
        public decimal QCreditAppReqBusinessCollateral1_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewSecond1")]
        public string QCreditAppReqBusinessCollateral2_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeSecond1")]
        public string QCreditAppReqBusinessCollateral2_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeSecond1")]
        public string QCreditAppReqBusinessCollateral2_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescSecond1")]
        public string QCreditAppReqBusinessCollateral2_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueSecond1")]
        public decimal QCreditAppReqBusinessCollateral2_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewThird1")]
        public string QCreditAppReqBusinessCollateral3_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeThird1")]
        public string QCreditAppReqBusinessCollateral3_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeThird1")]
        public string QCreditAppReqBusinessCollateral3_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescThird1")]
        public string QCreditAppReqBusinessCollateral3_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueThird1")]
        public decimal QCreditAppReqBusinessCollateral3_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewFourth1")]
        public string QCreditAppReqBusinessCollateral4_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeFourth1")]
        public string QCreditAppReqBusinessCollateral4_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeFourth1")]
        public string QCreditAppReqBusinessCollateral4_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescFourth1")]
        public string QCreditAppReqBusinessCollateral4_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueFourth1")]
        public decimal QCreditAppReqBusinessCollateral4_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewFifth1")]
        public string QCreditAppReqBusinessCollateral5_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeFifth1")]
        public string QCreditAppReqBusinessCollateral5_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeFifth1")]
        public string QCreditAppReqBusinessCollateral5_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescFifth1")]
        public string QCreditAppReqBusinessCollateral5_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueFifth1")]
        public decimal QCreditAppReqBusinessCollateral5_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewSixth1")]
        public string QCreditAppReqBusinessCollateral6_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeSixth1")]
        public string QCreditAppReqBusinessCollateral6_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeSixth1")]
        public string QCreditAppReqBusinessCollateral6_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescSixth1")]
        public string QCreditAppReqBusinessCollateral6_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueSixth1")]
        public decimal QCreditAppReqBusinessCollateral6_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewSeventh1")]
        public string QCreditAppReqBusinessCollateral7_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeSeventh1")]
        public string QCreditAppReqBusinessCollateral7_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeSeventh1")]
        public string QCreditAppReqBusinessCollateral7_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescSeventh1")]
        public string QCreditAppReqBusinessCollateral7_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueSeventh1")]
        public decimal QCreditAppReqBusinessCollateral7_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewEighth1")]
        public string QCreditAppReqBusinessCollateral8_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeEighth1")]
        public string QCreditAppReqBusinessCollateral8_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeEighth1")]
        public string QCreditAppReqBusinessCollateral8_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescEighth1")]
        public string QCreditAppReqBusinessCollateral8_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueEighth1")]
        public decimal QCreditAppReqBusinessCollateral8_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewNineth1")]
        public string QCreditAppReqBusinessCollateral9_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeNineth1")]
        public string QCreditAppReqBusinessCollateral9_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeNineth1")]
        public string QCreditAppReqBusinessCollateral9_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescNineth1")]
        public string QCreditAppReqBusinessCollateral9_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueNineth1")]
        public decimal QCreditAppReqBusinessCollateral9_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewTenth1")]
        public string QCreditAppReqBusinessCollateral10_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeTenth1")]
        public string QCreditAppReqBusinessCollateral10_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeTenth1")]
        public string QCreditAppReqBusinessCollateral10_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescTenth1")]
        public string QCreditAppReqBusinessCollateral10_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueTenth1")]
        public decimal QCreditAppReqBusinessCollateral10_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewEleventh1")]
        public string QCreditAppReqBusinessCollateral11_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeEleventh1")]
        public string QCreditAppReqBusinessCollateral11_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeEleventh1")]
        public string QCreditAppReqBusinessCollateral11_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescEleventh1")]
        public string QCreditAppReqBusinessCollateral11_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueEleventh1")]
        public decimal QCreditAppReqBusinessCollateral11_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewTwelfth1")]
        public string QCreditAppReqBusinessCollateral12_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeTwelfth1")]
        public string QCreditAppReqBusinessCollateral12_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeTwelfth1")]
        public string QCreditAppReqBusinessCollateral12_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescTwelfth1")]
        public string QCreditAppReqBusinessCollateral12_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueTwelfth1")]
        public decimal QCreditAppReqBusinessCollateral12_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewThirteenth1")]
        public string QCreditAppReqBusinessCollateral13_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeThirteenth1")]
        public string QCreditAppReqBusinessCollateral13_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeThirteenth1")]
        public string QCreditAppReqBusinessCollateral13_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescThirteenth1")]
        public string QCreditAppReqBusinessCollateral13_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueThirteenth1")]
        public decimal QCreditAppReqBusinessCollateral13_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewFourteenth1")]
        public string QCreditAppReqBusinessCollateral14_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeFourteenth1")]
        public string QCreditAppReqBusinessCollateral14_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeFourteenth1")]
        public string QCreditAppReqBusinessCollateral14_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescFourteenth1")]
        public string QCreditAppReqBusinessCollateral14_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueFourteenth1")]
        public decimal QCreditAppReqBusinessCollateral14_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("BusinessCollateralNewFifteenth1")]
        public string QCreditAppReqBusinessCollateral15_IsNew { get; set; }
        [BookmarkMapping("BusinessCollateralTypeFifteenth1")]
        public string QCreditAppReqBusinessCollateral15_BusinessCollateralType { get; set; }
        [BookmarkMapping("BusinessCollateralSubTypeFifteenth1")]
        public string QCreditAppReqBusinessCollateral15_BusinessCollateralSubType { get; set; }
        [BookmarkMapping("BusinessCollateralDescFifteenth1")]
        public string QCreditAppReqBusinessCollateral15_Description { get; set; }
        [BookmarkMapping("BusinessCollateralValueFifteenth1")]
        public decimal QCreditAppReqBusinessCollateral15_BusinessCollateralValue { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("SumBusinessCollateralValue")]
        public decimal QSumCreditAppReqBusinessCollateral_SumBusinessCollateralValue { get; set; }
        [BookmarkMapping("CustCompanyRegistrationID")]
        public string Variable_CustCompanyRegistrationID { get; set; }
        [BookmarkMapping("CustomerAddress")]
        public string QCreditAppRequestTable_RegisteredAddress { get; set; }
        [BookmarkMapping("CustComEstablishedDate")]
        public string Variable_CustComEstablishedDate { get; set; }
        [BookmarkMapping("CustBusinessType")]
        public string QCreditAppRequestTable_BusinessType { get; set; }
        [BookmarkMapping("Channel")]
        public string QCreditAppRequestTable_IntroducedBy { get; set; }
        [BookmarkMapping("CustBankName")]
        public string QCreditAppRequestTable_Instituetion { get; set; }
        [BookmarkMapping("CustBankBranch")]
        public string QCreditAppRequestTable_BankBranch { get; set; }
        [BookmarkMapping("CustBankAccountType")]
        public string QCreditAppRequestTable_BankType { get; set; }
        [BookmarkMapping("CustBankAccountNumber")]
        public string QCreditAppRequestTable_BankAccountName { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustAuthorizedPersonNameFirst1")]
        public string QAuthorizedPersonTrans1_AuthorizedPersonName { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeFirst1")]
        public string Variable_AuthorizedPersonTrans1Age { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustAuthorizedPersonNameSecond1")]
        public string QAuthorizedPersonTrans2_AuthorizedPersonName { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeSecond1")]
        public string Variable_AuthorizedPersonTrans2Age { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustAuthorizedPersonNameThird1")]
        public string QAuthorizedPersonTrans3_AuthorizedPersonName { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeThird1")]
        public string Variable_AuthorizedPersonTrans3Age { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustAuthorizedPersonNameFourth1")]
        public string QAuthorizedPersonTrans4_AuthorizedPersonName { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeFourth1")]
        public string Variable_AuthorizedPersonTrans4Age { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustAuthorizedPersonNameFifth1")]
        public string QAuthorizedPersonTrans5_AuthorizedPersonName { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeFifth1")]
        public string Variable_AuthorizedPersonTrans5Age { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustAuthorizedPersonNameSixth1")]
        public string QAuthorizedPersonTrans6_AuthorizedPersonName { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeSixth1")]
        public string Variable_AuthorizedPersonTrans6Age { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustAuthorizedPersonNameSeventh1")]
        public string QAuthorizedPersonTrans7_AuthorizedPersonName { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeSeventh1")]
        public string Variable_AuthorizedPersonTrans7Age { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustAuthorizedPersonNameEighth1")]
        public string QAuthorizedPersonTrans8_AuthorizedPersonName { get; set; }
        [BookmarkMapping("CustAuthorizedPersonAgeEighth1")]
        public string Variable_AuthorizedPersonTrans8Age { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustShareHolderPersonNameFirst1")]
        public string QOwnerTrans1_ShareHolderPersonName { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentFirst1")]
        public decimal QOwnerTrans1_PropotionOfShareholderPct { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustShareHolderPersonNameSecond1")]
        public string QOwnerTrans2_ShareHolderPersonName { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentSecond1")]
        public decimal QOwnerTrans2_PropotionOfShareholderPct { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustShareHolderPersonNameThird1")]
        public string QOwnerTrans3_ShareHolderPersonName { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentThird1")]
        public decimal QOwnerTrans3_PropotionOfShareholderPct { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustShareHolderPersonNameFourth1")]
        public string QOwnerTrans4_ShareHolderPersonName { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentFourth1")]
        public decimal QOwnerTrans4_PropotionOfShareholderPct { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustShareHolderPersonNameFifth1")]
        public string QOwnerTrans5_ShareHolderPersonName { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentFifth1")]
        public decimal QOwnerTrans5_PropotionOfShareholderPct { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustShareHolderPersonNameSixth1")]
        public string QOwnerTrans6_ShareHolderPersonName { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentSixth1")]
        public decimal QOwnerTrans6_PropotionOfShareholderPct { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustShareHolderPersonNameSeventh1")]
        public string QOwnerTrans7_ShareHolderPersonName { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentSeventh1")]
        public decimal QOwnerTrans7_PropotionOfShareholderPct { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustShareHolderPersonNameEighth1")]
        public string QOwnerTrans8_ShareHolderPersonName { get; set; }
        [BookmarkMapping("CustShareHolderPersonSharePercentEighth1")]
        public decimal QOwnerTrans8_PropotionOfShareholderPct { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustFinYearFirst1", "CustFinYearFirst2")]
        public int QCreditAppReqLineFin1_Year { get; set; }
        [BookmarkMapping("CustFinRegisteredCapitalFirst1")]
        public decimal QCreditAppReqLineFin1_RegisteredCapital { get; set; }
        [BookmarkMapping("CustFinPaidCapitalFirst1")]
        public decimal QCreditAppReqLineFin1_PaidCapital { get; set; }
        [BookmarkMapping("CustFinTotalAssetFirst1")]
        public decimal QCreditAppReqLineFin1_TotalAsset { get; set; }
        [BookmarkMapping("CustFinTotalLiabilityFirst1")]
        public decimal QCreditAppReqLineFin1_TotalLiability { get; set; }
        [BookmarkMapping("CustFinTotalEquityFirst1")]
        public decimal QCreditAppReqLineFin1_TotalEquity { get; set; }
        [BookmarkMapping("CustFinTotalRevenueFirst1")]
        public decimal QCreditAppReqLineFin1_TotalRevenue { get; set; }
        [BookmarkMapping("CustFinTotalCOGSFirst1")]
        public decimal QCreditAppReqLineFin1_TotalCOGS { get; set; }
        [BookmarkMapping("CustFinTotalGrossProfitFirst1")]
        public decimal QCreditAppReqLineFin1_TotalGrossProfit { get; set; }
        [BookmarkMapping("CustFinIntCoverageRatio1")]
        public decimal QCreditAppReqLineFin1_IntCoverageRatio { get; set; }
        [BookmarkMapping("CustFinTotalOperExpFirst1")]
        public decimal QCreditAppReqLineFin1_TotalOperExpFirst { get; set; }
        [BookmarkMapping("CustFinTotalNetProfitFirst1")]
        public decimal QCreditAppReqLineFin1_TotalNetProfitFirst { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustFinYearSecond1", "CustFinYearSecond2")]
        public int QCreditAppReqLineFin2_Year { get; set; }
        [BookmarkMapping("CustFinRegisteredCapitalSecond1")]
        public decimal QCreditAppReqLineFin2_RegisteredCapital { get; set; }
        [BookmarkMapping("CustFinPaidCapitalSecond1")]
        public decimal QCreditAppReqLineFin2_PaidCapital { get; set; }
        [BookmarkMapping("CustFinTotalAssetSecond1")]
        public decimal QCreditAppReqLineFin2_TotalAsset { get; set; }
        [BookmarkMapping("CustFinTotalLiabilitySecond1")]
        public decimal QCreditAppReqLineFin2_TotalLiability { get; set; }
        [BookmarkMapping("CustFinTotalEquitySecond1")]
        public decimal QCreditAppReqLineFin2_TotalEquity { get; set; }
        [BookmarkMapping("CustFinTotalRevenueSecond1")]
        public decimal QCreditAppReqLineFin2_TotalRevenue { get; set; }
        [BookmarkMapping("CustFinTotalCOGSSecond1")]
        public decimal QCreditAppReqLineFin2_TotalCOGS { get; set; }
        [BookmarkMapping("CustFinTotalGrossProfitSecond1")]
        public decimal QCreditAppReqLineFin2_TotalGrossProfit { get; set; }
        [BookmarkMapping("CustFinTotalOperExpSecond1")]
        public decimal QCreditAppReqLineFin2_TotalOperExpSecond { get; set; }
        [BookmarkMapping("CustFinTotalNetProfitSecond1")]
        public decimal QCreditAppReqLineFin2_TotalNetProfitSecond { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustFinYearThird1", "CustFinYearThird2")]
        public int QCreditAppReqLineFin3_Year { get; set; }
        [BookmarkMapping("CustFinRegisteredCapitalThird1")]
        public decimal QCreditAppReqLineFin3_RegisteredCapital { get; set; }
        [BookmarkMapping("CustFinPaidCapitalThird1")]
        public decimal QCreditAppReqLineFin3_PaidCapital { get; set; }
        [BookmarkMapping("CustFinTotalAssetThird1")]
        public decimal QCreditAppReqLineFin3_TotalAsset { get; set; }
        [BookmarkMapping("CustFinTotalLiabilityThird1")]
        public decimal QCreditAppReqLineFin3_TotalLiability { get; set; }
        [BookmarkMapping("CustFinTotalEquityThird1")]
        public decimal QCreditAppReqLineFin3_TotalEquity { get; set; }
        [BookmarkMapping("CustFinTotalRevenueThird1")]
        public decimal QCreditAppReqLineFin3_TotalRevenue { get; set; }
        [BookmarkMapping("CustFinTotalCOGSThird1")]
        public decimal QCreditAppReqLineFin3_TotalCOGS { get; set; }
        [BookmarkMapping("CustFinTotalGrossProfitThird1")]
        public decimal QCreditAppReqLineFin3_TotalGrossProfit { get; set; }
        [BookmarkMapping("CustFinTotalOperExpThird1")]
        public decimal QCreditAppReqLineFin3_TotalOperExpThird { get; set; }
        [BookmarkMapping("CustFinTotalNetProfitThird1")]
        public decimal QCreditAppReqLineFin3_TotalNetProfitThird { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustFinYearFourth1", "CustFinYearFourth2")]
        public int QCreditAppReqLineFin4_Year { get; set; }
        [BookmarkMapping("CustFinRegisteredCapitalFourth1")]
        public decimal QCreditAppReqLineFin4_RegisteredCapital { get; set; }
        [BookmarkMapping("CustFinPaidCapitalFourth1")]
        public decimal QCreditAppReqLineFin4_PaidCapital { get; set; }
        [BookmarkMapping("CustFinTotalAssetFourth1")]
        public decimal QCreditAppReqLineFin4_TotalAsset { get; set; }
        [BookmarkMapping("CustFinTotalLiabilityFourth1")]
        public decimal QCreditAppReqLineFin4_TotalLiability { get; set; }
        [BookmarkMapping("CustFinTotalEquityFourth1")]
        public decimal QCreditAppReqLineFin4_TotalEquity { get; set; }
        [BookmarkMapping("CustFinTotalRevenueFourth1")]
        public decimal QCreditAppReqLineFin4_TotalRevenue { get; set; }
        [BookmarkMapping("CustFinTotalCOGSFourth1")]
        public decimal QCreditAppReqLineFin4_TotalCOGS { get; set; }
        [BookmarkMapping("CustFinTotalGrossProfitFourth1")]
        public decimal QCreditAppReqLineFin4_TotalGrossProfit { get; set; }
        [BookmarkMapping("CustFinTotalOperExpFourth1")]
        public decimal QCreditAppReqLineFin4_TotalOperExpFourth { get; set; }
        [BookmarkMapping("CustFinTotalNetProfitFourth1")]
        public decimal QCreditAppReqLineFin4_TotalNetProfitFourth { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustFinYearFifth1", "CustFinYearFifth2")]
        public int QCreditAppReqLineFin5_Year { get; set; }
        [BookmarkMapping("CustFinRegisteredCapitalFifth1")]
        public decimal QCreditAppReqLineFin5_RegisteredCapital { get; set; }
        [BookmarkMapping("CustFinPaidCapitalFifth1")]
        public decimal QCreditAppReqLineFin5_PaidCapital { get; set; }
        [BookmarkMapping("CustFinTotalAssetFifth1")]
        public decimal QCreditAppReqLineFin5_TotalAsset { get; set; }
        [BookmarkMapping("CustFinTotalLiabilityFifth1")]
        public decimal QCreditAppReqLineFin5_TotalLiability { get; set; }
        [BookmarkMapping("CustFinTotalEquityFifth1")]
        public decimal QCreditAppReqLineFin5_TotalEquity { get; set; }
        [BookmarkMapping("CustFinTotalRevenueFifth1")]
        public decimal QCreditAppReqLineFin5_TotalRevenue { get; set; }
        [BookmarkMapping("CustFinTotalCOGSFifth1")]
        public decimal QCreditAppReqLineFin5_TotalCOGS { get; set; }
        [BookmarkMapping("CustFinTotalGrossProfitFifth1")]
        public decimal QCreditAppReqLineFin5_TotalGrossProfit { get; set; }
        [BookmarkMapping("CustFinTotalOperExpFifth1")]
        public decimal QCreditAppReqLineFin5_TotalOperExpFifth { get; set; }
        [BookmarkMapping("CustFinTotalNetProfitFifth1")]
        public decimal QCreditAppReqLineFin5_TotalNetProfitFifth { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CustFinNetProfitPercent1")]
        public decimal QCreditAppReqLineFin1_NetProfitPercent { get; set; }
        [BookmarkMapping("CustFinlDE1")]
        public decimal QCreditAppReqLineFin1_lDE { get; set; }
        [BookmarkMapping("CustFinQuickRatio1")]
        public decimal QCreditAppReqLineFin1_QuickRatio { get; set; }
        [BookmarkMapping("CustFinRevenuePerMonth1")]
        public decimal QCreditAppRequestTable_SalesAvgPerMonth { get; set; }
        [BookmarkMapping("OutputVATStart")]
        public string QTaxReportStart_TaxMonth { get; set; }
        [BookmarkMapping("OutputVATEnd")]
        public string QTaxReportEnd_TaxMonth { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("Month1")]
        public string QTaxReportTrans1_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth1")]
        public decimal QTaxReportTrans1_Amount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("Month2")]
        public string QTaxReportTrans2_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth2")]
        public decimal QTaxReportTrans2_Amount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("Month3")]
        public string QTaxReportTrans3_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth3")]
        public decimal QTaxReportTrans3_Amount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("Month4")]
        public string QTaxReportTrans4_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth4")]
        public decimal QTaxReportTrans4_Amount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("Month5")]
        public string QTaxReportTrans5_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth5")]
        public decimal QTaxReportTrans5_Amount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("Month6")]
        public string QTaxReportTrans6_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth6")]
        public decimal QTaxReportTrans6_Amount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("Month7")]
        public string QTaxReportTrans7_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth7")]
        public decimal QTaxReportTrans7_Amount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("Month8")]
        public string QTaxReportTrans8_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth8")]
        public decimal QTaxReportTrans8_Amount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("Month9")]
        public string QTaxReportTrans9_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth9")]
        public decimal QTaxReportTrans9_Amount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("Month10")]
        public string QTaxReportTrans10_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth10")]
        public decimal QTaxReportTrans10_Amount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("Month11")]
        public string QTaxReportTrans11_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth11")]
        public decimal QTaxReportTrans11_Amount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("Month12")]
        public string QTaxReportTrans12_TaxMonth { get; set; }
        [BookmarkMapping("VATAmountMonth12")]
        public decimal QTaxReportTrans12_Amount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("TotalOutputVAT")]
        public decimal QTaxReportTransSum_SumTaxReport { get; set; }
        [BookmarkMapping("AverageOutputVATPerMonth")]
        public decimal Variable_AverageOutputVATPerMonth { get; set; }
        [BookmarkMapping("ApprovedCreditAsofDate")]
        public string QCreditAppRequestTable_AsOfDate { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CrditLimitAmtFirst1")]
        public decimal Variable_CrditLimitAmtFirst1 { get; set; }
        [BookmarkMapping("AROutstandingAmtFirst")]
        public decimal Variable_AROutstandingAmtFirst { get; set; }
        [BookmarkMapping("CreditLimitRemainingFirst1")]
        public decimal Variable_CreditLimitRemainingFirst1 { get; set; }
        [BookmarkMapping("ReserveOutstandingFirst1")]
        public decimal Variable_ReserveOutstandingFirst1 { get; set; }
        [BookmarkMapping("RetentionOutstandingFirst1")]
        public decimal Variable_RetentionOutstandingFirst1 { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CrditLimitAmtSecond1")]
        public decimal Variable_CrditLimitAmtSecond1 { get; set; }
        [BookmarkMapping("AROutstandingAmtSecond")]
        public decimal Variable_AROutstandingAmtSecond { get; set; }
        [BookmarkMapping("CreditLimitRemainingSecond1")]
        public decimal Variable_CreditLimitRemainingSecond1 { get; set; }
        [BookmarkMapping("ReserveOutstandingSecond1")]
        public decimal Variable_ReserveOutstandingSecond1 { get; set; }
        [BookmarkMapping("RetentionOutstandingSecond1")]
        public decimal Variable_RetentionOutstandingSecond1 { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CrditLimitAmtThird1")]
        public decimal Variable_CrditLimitAmtThird1 { get; set; }
        [BookmarkMapping("AROutstandingAmtThird")]
        public decimal Variable_AROutstandingAmtThird { get; set; }
        [BookmarkMapping("CreditLimitRemainingThird1")]
        public decimal Variable_CreditLimitRemainingThird1 { get; set; }
        [BookmarkMapping("ReserveOutstandingThird1")]
        public decimal Variable_ReserveOutstandingThird1 { get; set; }
        [BookmarkMapping("RetentionOutstandingThird1")]
        public decimal Variable_RetentionOutstandingThird1 { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CrditLimitAmtFourth1")]
        public decimal Variable_CrditLimitAmtFourth1 { get; set; }
        [BookmarkMapping("AROutstandingAmtFourth")]
        public decimal Variable_AROutstandingAmtFourth { get; set; }
        [BookmarkMapping("CreditLimitRemainingFourth1")]
        public decimal Variable_CreditLimitRemainingFourth1 { get; set; }
        [BookmarkMapping("ReserveOutstandingFourth1")]
        public decimal Variable_ReserveOutstandingFourth1 { get; set; }
        [BookmarkMapping("RetentionOutstandingFourth1")]
        public decimal Variable_RetentionOutstandingFourth1 { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CrditLimitAmtFifth1")]
        public decimal Variable_CrditLimitAmtFifth1 { get; set; }
        [BookmarkMapping("AROutstandingAmtFifth")]
        public decimal Variable_AROutstandingAmtFifth { get; set; }
        [BookmarkMapping("CreditLimitRemainingFifth1")]
        public decimal Variable_CreditLimitRemainingFifth1 { get; set; }
        [BookmarkMapping("ReserveOutstandingFifth1")]
        public decimal Variable_ReserveOutstandingFifth1 { get; set; }
        [BookmarkMapping("RetentionOutstandingFifth1")]
        public decimal Variable_RetentionOutstandingFifth1 { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CrditLimitAmtSixth1")]
        public decimal Variable_CrditLimitAmtSixth1 { get; set; }
        [BookmarkMapping("AROutstandingAmtSixth")]
        public decimal Variable_AROutstandingAmtSixth { get; set; }
        [BookmarkMapping("CreditLimitRemainingSixth1")]
        public decimal Variable_CreditLimitRemainingSixth1 { get; set; }
        [BookmarkMapping("ReserveOutstandingSixth1")]
        public decimal Variable_ReserveOutstandingSixth1 { get; set; }
        [BookmarkMapping("RetentionOutstandingSixth1")]
        public decimal Variable_RetentionOutstandingSixth1 { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CrditLimitAmtSeventh1")]
        public decimal Variable_CrditLimitAmtSeventh1 { get; set; }
        [BookmarkMapping("AROutstandingAmtSeventh")]
        public decimal Variable_AROutstandingAmtSeventh { get; set; }
        [BookmarkMapping("CreditLimitRemainingSeventh1")]
        public decimal Variable_CreditLimitRemainingSeventh1 { get; set; }
        [BookmarkMapping("ReserveOutstandingSeventh1")]
        public decimal Variable_ReserveOutstandingSeventh1 { get; set; }
        [BookmarkMapping("RetentionOutstandingSeventh1")]
        public decimal Variable_RetentionOutstandingSeventh1 { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CrditLimitAmtEighth1")]
        public decimal Variable_CrditLimitAmtEighth1 { get; set; }
        [BookmarkMapping("AROutstandingAmtEighth")]
        public decimal Variable_AROutstandingAmtEighth { get; set; }
        [BookmarkMapping("CreditLimitRemainingEighth1")]
        public decimal Variable_CreditLimitRemainingEighth1 { get; set; }
        [BookmarkMapping("ReserveOutstandingEighth1")]
        public decimal Variable_ReserveOutstandingEighth1 { get; set; }
        [BookmarkMapping("RetentionOutstandingEighth1")]
        public decimal Variable_RetentionOutstandingEighth1 { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CrditLimitAmtNineth1")]
        public decimal Variable_CrditLimitAmtNineth1 { get; set; }
        [BookmarkMapping("AROutstandingAmtNineth")]
        public decimal Variable_AROutstandingAmtNineth { get; set; }
        [BookmarkMapping("CreditLimitRemainingNineth1")]
        public decimal Variable_CreditLimitRemainingNineth1 { get; set; }
        [BookmarkMapping("ReserveOutstandingNineth1")]
        public decimal Variable_ReserveOutstandingNineth1 { get; set; }
        [BookmarkMapping("RetentionOutstandingNineth1")]
        public decimal Variable_RetentionOutstandingNineth1 { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CrditLimitAmtTenth1")]
        public decimal Variable_CrditLimitAmtTenth1 { get; set; }
        [BookmarkMapping("AROutstandingAmtTenth")]
        public decimal Variable_AROutstandingAmtTenth { get; set; }
        [BookmarkMapping("CreditLimitRemainingTenth1")]
        public decimal Variable_CreditLimitRemainingTenth1 { get; set; }
        [BookmarkMapping("ReserveOutstandingTenth1")]
        public decimal Variable_ReserveOutstandingTenth1 { get; set; }
        [BookmarkMapping("RetentionOutstandingTenth1")]
        public decimal Variable_RetentionOutstandingTenth1 { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("TotalCreditLimit")]
        public decimal QCAReqCreditOutStandingSum_SumApprovedCreditLimit { get; set; }
        [BookmarkMapping("TotalAROutstanding")]
        public decimal QCAReqCreditOutStandingSum_SumARBalance { get; set; }
        [BookmarkMapping("TotalCreditLimitRemaining")]
        public decimal QCAReqCreditOutStandingSum_SumCreditLimitBalance { get; set; }
        [BookmarkMapping("TotalReserveOutstanding")]
        public decimal QCAReqCreditOutStandingSum_SumReserveToBeRefund { get; set; }
        [BookmarkMapping("TotalRetentionOutstanding")]
        public decimal QCAReqCreditOutStandingSum_SumAccumRetentionAmount { get; set; }
        [BookmarkMapping("CreditBueroCheckDate")]
        public string QCreditAppRequestTable_NCBCheckedDate { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("FinInstitutionFirst1")]
        public string QNCBTrans1_Institution { get; set; }
        [BookmarkMapping("LoanTypeFirst1")]
        public string QNCBTrans1_LoanType { get; set; }
        [BookmarkMapping("LoanLimitFirst1")]
        public decimal QNCBTrans1_LoanLimit { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("FinInstitutionSecond1")]
        public string QNCBTrans2_Institution { get; set; }
        [BookmarkMapping("LoanTypeSecond1")]
        public string QNCBTrans2_LoanType { get; set; }
        [BookmarkMapping("LoanLimitSecond1")]
        public decimal QNCBTrans2_LoanLimit { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("FinInstitutionThird1")]
        public string QNCBTrans3_Institution { get; set; }
        [BookmarkMapping("LoanTypeThird1")]
        public string QNCBTrans3_LoanType { get; set; }
        [BookmarkMapping("LoanLimitThird1")]
        public decimal QNCBTrans3_LoanLimit { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("FinInstitutionFourth1")]
        public string QNCBTrans4_Institution { get; set; }
        [BookmarkMapping("LoanTypeFourth1")]
        public string QNCBTrans4_LoanType { get; set; }
        [BookmarkMapping("LoanLimitFourth1")]
        public decimal QNCBTrans4_LoanLimit { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("FinInstitutionFifth1")]
        public string QNCBTrans5_Institution { get; set; }
        [BookmarkMapping("LoanTypeFifth1")]
        public string QNCBTrans5_LoanType { get; set; }
        [BookmarkMapping("LoanLimitFifth1")]
        public decimal QNCBTrans5_LoanLimit { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("FinInstitutionSixth1")]
        public string QNCBTrans6_Institution { get; set; }
        [BookmarkMapping("LoanTypeSixth1")]
        public string QNCBTrans6_LoanType { get; set; }
        [BookmarkMapping("LoanLimitSixth1")]
        public decimal QNCBTrans6_LoanLimit { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("FinInstitutionSeventh1")]
        public string QNCBTrans7_Institution { get; set; }
        [BookmarkMapping("LoanTypeSeventh1")]
        public string QNCBTrans7_LoanType { get; set; }
        [BookmarkMapping("LoanLimitSeventh1")]
        public decimal QNCBTrans7_LoanLimit { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("FinInstitutionEighth1")]
        public string QNCBTrans8_Institution { get; set; }
        [BookmarkMapping("LoanTypeEighth1")]
        public string QNCBTrans8_LoanType { get; set; }
        [BookmarkMapping("LoanLimitEighth1")]
        public decimal QNCBTrans8_LoanLimit { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("TotalLoanLimit")]
        public decimal QNCBTransSum_TotalLoanLimit { get; set; }
        [BookmarkMapping("MarketingComment")]
        public string QCreditAppRequestTable_MarketingComment { get; set; }
        [BookmarkMapping("ApproverComment")]
        public string QCreditAppRequestTable_ApproverComment { get; set; }
        [BookmarkMapping("CreditComment")]
        public string QCreditAppRequestTable_CreditComment { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ApproverNameFirst1")]
        public string QActionHistory1_ApproverName { get; set; }
        [BookmarkMapping("ApproverReasonFirst1")]
        public string QActionHistory1_Comment { get; set; }
        [BookmarkMapping("ApprovedDateFirst1")]
        public string QActionHistory1_CreatedDateTime { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ApproverNameSecond1")]
        public string QActionHistory2_ApproverName { get; set; }
        [BookmarkMapping("ApproverReasonSecond1")]
        public string QActionHistory2_Comment { get; set; }
        [BookmarkMapping("ApprovedDateSecond1")]
        public string QActionHistory2_CreatedDateTime { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ApproverNameThird1")]
        public string QActionHistory3_ApproverName { get; set; }
        [BookmarkMapping("ApproverReasonThird1")]
        public string QActionHistory3_Comment { get; set; }
        [BookmarkMapping("ApprovedDateThird1")]
        public string QActionHistory3_CreatedDateTime { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ApproverNameFourth1")]
        public string QActionHistory4_ApproverName { get; set; }
        [BookmarkMapping("ApproverReasonFourth1")]
        public string QActionHistory4_Comment { get; set; }
        [BookmarkMapping("ApprovedDateFourth1")]
        public string QActionHistory4_CreatedDateTime { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("ApproverNameFifth1")]
        public string QActionHistory5_ApproverName { get; set; }
        [BookmarkMapping("ApproverReasonFifth1")]
        public string QActionHistory5_Comment { get; set; }
        [BookmarkMapping("ApprovedDateFifth1")]
        public string QActionHistory5_CreatedDateTime { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("CrditLimitAmtStart1")]
        public decimal QCAReqCreditOutStanding1_ApprovedCreditLimit { get; set; }
        [BookmarkMapping("AROutstandingAmtStart")]
        public decimal QCAReqCreditOutStanding1_ARBalance { get; set; }
        [BookmarkMapping("CreditLimitRemainingStart1")]
        public decimal QCAReqCreditOutStanding1_CreditLimitBalance { get; set; }
        [BookmarkMapping("ReserveOutstandingStart1")]
        public decimal QCAReqCreditOutStanding1_ReserveToBeRefund { get; set; }
        [BookmarkMapping("RetentionOutstandingStart1")]
        public decimal QCAReqCreditOutStanding1_AccumRetentionAmount { get; set; }
        //-----------------------------------------------------------------------------------
        [BookmarkMapping("FactoringPurchaseCondition")]
        public string QCreditAppRequestTable_PurchaseWithdrawalCondition { get; set; }
        [BookmarkMapping("AssignmentCondition")]
        public string QCreditAppRequestTable_AssignmentMethod { get; set; }

    }
    public class QCreditAppRequestTable
    {
        public string CreditAppRequestId { get; set; }
        public string RefCreditAppTableGUID { get; set; }
        public string OriginalCreditAppTableId { get; set; }
        public string OriginalPurchaseWithdrawalCondition { get; set; }
        public string OriginalAssignmentMethodGUID { get; set; }
        public string OriginalAssignmentMethodTable { get; set; }
        public string RequestDate { get; set; }
        public string CustomerTableGUID { get; set; }
        public string Description { get; set; }
        public string Remark { get; set; }
        public int ProductType { get; set; }
        public int CreditLimitExpiration { get; set; }
        public string StartDate { get; set; }
        public string ExpiryDate { get; set; }
        public decimal CreditLimitRequest { get; set; }
        public decimal InterestAdjustment { get; set; }
        public string InterestValue { get; set; }
        public decimal MaxPurchasePct { get; set; }
        public decimal MaxRetentionPct { get; set; }
        public decimal MaxRetentionAmount { get; set; }
        public string CACondition { get; set; }
        public string PDCBankGUID { get; set; }
        public decimal CreditRequestFeePct { get; set; }
        public decimal CreditRequestFeeAmount { get; set; }
        public decimal PurchaseFeePct { get; set; }
        public int PurchaseFeeCalculateBase { get; set; }
        public string BankAccountControlGUID { get; set; }
        public decimal SalesAvgPerMonth { get; set; }
        public decimal CustomerCreditLimit { get; set; }
        public string NCBCheckedDate { get; set; }
        public string MarketingComment { get; set; }
        public string CreditComment { get; set; }
        public string ApproverComment { get; set; }
        public string DocumentStatusGUID { get; set; }
        public string Status { get; set; }
        public string KYC { get; set; }
        public string CreditScoring { get; set; }
        public string Purpose { get; set; }
        public string ProductSubType { get; set; }
        public string CreditLimitTypeId { get; set; }
        public string CreditLimitType { get; set; }
        public string InterestTypeGUID { get; set; }
        public string InterestType { get; set; }
        public string RegisteredAddressGUID { get; set; }
        public string RegisteredAddress { get; set; }
        public string IntroducedByGUID { get; set; }
        public string IntroducedBy { get; set; }
        public string BusinessTypeGUID { get; set; }
        public string BusinessType { get; set; }
        public string PurchaseWithdrawalCondition { get; set; }
        public string AssignmentMethodGUID { get; set; }
        public string AssignmentMethodTable { get; set; }
    }
    public class QCustomer
    {
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public decimal IdentificationType { get; set; }
        public string TaxID { get; set; }
        public string PassportID { get; set; }
        public decimal RecordType { get; set; }
        public string DateOfEstablish { get; set; }
        public string DateOfBirth { get; set; }
        public string IntroducedByGUID { get; set; }
        public string BusinessTypeGUID { get; set; }
        public string ResponsibleByGUID { get; set; }
        public string ResponsibleBy { get; set; }
    }
    public class QPDCBank
    {
        public string BankGroupGUID { get; set; }
        public string CustPDCBankName { get; set; }
        public string CustPDCBankBranch { get; set; }
        public string CustPDCBankType { get; set; }
        public string CustPDCBankAccountName { get; set; }
    }
    public class QRetentionConditionTrans
    {
        public RetentionDeductionMethod RetentionDeductionMethod { get; set; }
        public RetentionCalculateBase RetentionCalculateBase { get; set; }
        public decimal RetentionPct { get; set; }
        public decimal RetentionAmount { get; set; }
        public decimal Row { get; set; }
    }
    public class QBankAccountControl
    {
        public string BankGroupGUID { get; set; }
        public string Instituetion { get; set; }
        public string BankBranch { get; set; }
        public string BankTypeGUID { get; set; }
        public string AccountType { get; set; }
        public string BankAccountName { get; set; }
        public string AccountNumber { get; set; }
    }
    public class QCreditAppReqBusinessCollateral
    {
        public bool IsNew { get; set; }
        public string BusinessCollateralTypeGUID { get; set; }
        public string BusinessCollateralTypeId { get; set; }
        public string BusinessCollateralSubTypeId { get; set; }
        public string BusinessCollateralSubTypeGUID { get; set; }
        public string Description { get; set; }
        public decimal BusinessCollateralValue { get; set; }
        public decimal SumBusinessCollateralValue { get; set; }
        public decimal Row { get; set; }
        public DateTime CreatedDateTime { get; set; }
    }
    public class QSumCreditAppReqBusinessCollateral
    {
        public decimal SumBusinessCollateralValue { get; set; }
    }
    public class QServiceFeeConditionTrans
    {
        public string Description { get; set; }
        public decimal AmountBeforeTax { get; set; }
        public string InvoiceRevenueTypeGUID { get; set; }
        public string ServicefeeTypeId { get; set; }
        public string ServicefeeTypeDesc { get; set; }
        public decimal Row { get; set; }
    }
    public class QSumServiceFeeConditionTrans
    {
        public decimal ServiceFeeAmt { get; set; }
    }
    public class QGuarantorTrans
    {
        public string RelatedPersonTableGUID { get; set; }
        public string RelatedPersonName { get; set; }
        public string GuarantorTypeGUID { get; set; }
        public string GuarantorType { get; set; }
        public decimal Row { get; set; }
    }
    public class QAuthorizedPersonTrans
    {
        public string AuthorizedPersonName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public decimal Row { get; set; }
    }
    public class QOwnerTrans
    {
        public string RelatedPersonTableGUID { get; set; }
        public string ShareHolderPersonName { get; set; }
        public decimal PropotionOfShareholderPct { get; set; }
        public decimal Row { get; set; }
    }
    public class QCreditAppReqLineFin
    {
        public int Year { get; set; }
        public decimal Row { get; set; }
        public decimal RegisteredCapital { get; set; }
        public decimal PaidCapital { get; set; }
        public decimal TotalAsset { get; set; }
        public decimal TotalLiability { get; set; }
        public decimal TotalEquity { get; set; }
        public decimal TotalRevenue { get; set; }
        public decimal TotalCOGS { get; set; }
        public decimal TotalGrossProfit { get; set; }
        public decimal TotalOperExpFirst { get; set; }
        public decimal TotalNetProfitFirst { get; set; }
        public decimal NetProfitPercent { get; set; }
        public decimal lDE { get; set; }
        public decimal QuickRatio { get; set; }
        public decimal IntCoverageRatio { get; set; }
    }
    public class QTaxReportTrans
    {
        public string TaxMonth { get; set; }
        public decimal Amount { get; set; }
        public decimal Row { get; set; }
    }
    public class QTaxReportStart
    {
        public string TaxMonth { get; set; }
    }
    public class QTaxReportEnd
    {
        public string TaxMonth { get; set; }
        public string TaxReportCOUNT { get; set; }
    }
    public class QTaxReportTransSum
    {
        public decimal SumTaxReport { get; set; }
        public int TaxReportCOUNT { get; set; }
    }
    public class QCAReqCreditOutStanding
    {
        public string AsOfDate { get; set; }
        public string ProductType { get; set; }
        public int BookmarkOrdering { get; set; }
        public string CreditLimitTypeId { get; set; }
        public string CreditLimitTypeDesc { get; set; }
        public decimal ApprovedCreditLimit { get; set; }
        public decimal ARBalance { get; set; }
        public decimal CreditLimitBalance { get; set; }
        public decimal ReserveToBeRefund { get; set; }
        public decimal AccumRetentionAmount { get; set; }
        public Guid? CreditLimitTypeGUID { get; set; }
        public int Row { get; set; }
    }
    public class QCAReqCreditOutStandingSum
    {
        public decimal SumApprovedCreditLimit { get; set; }
        public decimal SumARBalance { get; set; }
        public decimal SumCreditLimitBalance { get; set; }
        public decimal SumReserveToBeRefund { get; set; }
        public decimal SumAccumRetentionAmount { get; set; }
        public decimal TotalLoanLimit { get; set; }
    }
    public class QNCBTrans
    {
        public string BankGroupGUID { get; set; }
        public string Institution { get; set; }
        public string CreditTypeId { get; set; }
        public string CreditTypeGUID { get; set; }
        public string LoanType { get; set; }
        public decimal LoanLimit { get; set; }
        public string NCBAccountStatusGUID { get; set; }
        public string NCBAccountStatusId { get; set; }
        public int Row { get; set; }
    }
    public class QActionHistory
    {
        public string Comment { get; set; }
        public string CreatedDateTime { get; set; }
        public string ActionName { get; set; }
        public string CreatedBy { get; set; }
        public string ApproverName { get; set; }
        public decimal Row { get; set; }
    }
    public class QCreditAppReqAssignment
    {
        public bool IsNew { get; set; }
        public string AssignmentBuyer { get; set; }
        public string BuyerAgreementTableGUID { get; set; }
        public string RefAgreementId { get; set; }
        public decimal BuyerAgreementAmount { get; set; }
        public decimal AssignmentAgreementAmount { get; set; }
        public string AssignmentAgreementTableGUID { get; set; }
        public string AssignmentAgreementDate { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public decimal RemainingAmount { get; set; }
        public decimal Row { get; set; }
    }
    public class QSumCreditAppReqAssignment
    {
        public decimal SumBuyerAgreementAmount { get; set; }
        public decimal SumAssignmentAgreementAmount { get; set; }
        public decimal SumRemainingAmount { get; set; }
    }
    public class BookmarkDocumentCreditApplication_Variable
    {
        public string AuthorizedPersonTrans1Age { get; set; }
        public string AuthorizedPersonTrans2Age { get; set; }
        public string AuthorizedPersonTrans3Age { get; set; }
        public string AuthorizedPersonTrans4Age { get; set; }
        public string AuthorizedPersonTrans5Age { get; set; }
        public string AuthorizedPersonTrans6Age { get; set; }
        public string AuthorizedPersonTrans7Age { get; set; }
        public string AuthorizedPersonTrans8Age { get; set; }
        public string CustCompanyRegistrationID { get; set; }
        public string CustComEstablishedDate { get; set; }
        public decimal AverageOutputVATPerMonth { get; set; }
        public decimal InterestValue { get; set; }
        public string RevolvingByCustomer { get; set; }
        public decimal CrditLimitAmtFirst1 { get; set; }
        public decimal AROutstandingAmtFirst { get; set; }
        public decimal CreditLimitRemainingFirst1 { get; set; }
        public decimal ReserveOutstandingFirst1 { get; set; }
        public decimal RetentionOutstandingFirst1 { get; set; }
        public string CrditLimitAmtSecond1 { get; set; }
        public string AROutstandingAmtSecond { get; set; }
        public string CreditLimitRemainingSecond1 { get; set; }
        public string ReserveOutstandingSecond1 { get; set; }
        public string RetentionOutstandingSecond1 { get; set; }
        public string CrditLimitAmtThird1 { get; set; }
        public string AROutstandingAmtThird { get; set; }
        public string CreditLimitRemainingThird1 { get; set; }
        public string ReserveOutstandingThird1 { get; set; }
        public string RetentionOutstandingThird1 { get; set; }
        public string CrditLimitAmtFourth1 { get; set; }
        public string AROutstandingAmtFourth { get; set; }
        public string CreditLimitRemainingFourth1 { get; set; }
        public string ReserveOutstandingFourth1 { get; set; }
        public string RetentionOutstandingFourth1 { get; set; }
        public string CrditLimitAmtFifth1 { get; set; }
        public string AROutstandingAmtFifth { get; set; }
        public string CreditLimitRemainingFifth1 { get; set; }
        public string ReserveOutstandingFifth1 { get; set; }
        public string RetentionOutstandingFifth1 { get; set; }
        public string CrditLimitAmtSixth1 { get; set; }
        public string AROutstandingAmtSixth { get; set; }
        public string CreditLimitRemainingSixth1 { get; set; }
        public string ReserveOutstandingSixth1 { get; set; }
        public string RetentionOutstandingSixth1 { get; set; }
        public string CrditLimitAmtSeventh1 { get; set; }
        public string AROutstandingAmtSeventh { get; set; }
        public string CreditLimitRemainingSeventh1 { get; set; }
        public string ReserveOutstandingSeventh1 { get; set; }
        public string RetentionOutstandingSeventh1 { get; set; }
        public string CrditLimitAmtEighth1 { get; set; }
        public string AROutstandingAmtEighth { get; set; }
        public string CreditLimitRemainingEighth1 { get; set; }
        public string ReserveOutstandingEighth1 { get; set; }
        public string RetentionOutstandingEighth1 { get; set; }
        public string CrditLimitAmtNineth1 { get; set; }
        public string AROutstandingAmtNineth { get; set; }
        public string CreditLimitRemainingNineth1 { get; set; }
        public string ReserveOutstandingNineth1 { get; set; }
        public string RetentionOutstandingNineth1 { get; set; }
        public string CrditLimitAmtTenth1 { get; set; }
        public string AROutstandingAmtTenth { get; set; }
        public string CreditLimitRemainingTenth1 { get; set; }
        public string ReserveOutstandingTenth1 { get; set; }
        public string RetentionOutstandingTenth1 { get; set; }
        public decimal NewInterestRate { get; set; }
        public string BusinessCollateralIsNew1 { get; set; }
        public string BusinessCollateralIsNew2 { get; set; }
        public string BusinessCollateralIsNew3 { get; set; }
        public string BusinessCollateralIsNew4 { get; set; }
        public string BusinessCollateralIsNew5 { get; set; }
        public string BusinessCollateralIsNew6 { get; set; }
        public string BusinessCollateralIsNew7 { get; set; }
        public string BusinessCollateralIsNew8 { get; set; }
        public string BusinessCollateralIsNew9 { get; set; }
        public string BusinessCollateralIsNew10 { get; set; }
        public string BusinessCollateralIsNew11 { get; set; }
        public string BusinessCollateralIsNew12 { get; set; }
        public string BusinessCollateralIsNew13 { get; set; }
        public string BusinessCollateralIsNew14 { get; set; }
        public string BusinessCollateralIsNew15 { get; set; }
    }
 }
