using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class DocumentConditionTemplateTableListView : ViewCompanyBaseEntity
	{
		public string DocumentConditionTemplateTableGUID { get; set; }
		public string DocumentConditionTemplateTableId { get; set; }
		public string Description { get; set; }
	}
}
