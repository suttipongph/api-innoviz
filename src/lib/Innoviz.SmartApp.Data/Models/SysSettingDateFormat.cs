﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
    public partial class SysSettingDateFormat : BaseEntity {
        public SysSettingDateFormat()
        {
            SysUserTable = new HashSet<SysUserTable>();
        }
        
        [Key]
        [StringLength(20)]
        public string DateFormatId { get; set; }
        [StringLength(100)]
        public string DateMapping { get; set; }

        [InverseProperty("DateFormat")]
        public ICollection<SysUserTable> SysUserTable { get; set; }
    }
}
