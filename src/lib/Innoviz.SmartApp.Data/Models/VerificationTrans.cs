using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class VerificationTrans : CompanyBaseEntity
	{
		public VerificationTrans()
		{
		}
 
		[Key]
		public Guid VerificationTransGUID { get; set; }
		[StringLength(20)]
		public string DocumentId { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		public Guid VerificationTableGUID { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("VerificationTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("VerificationTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("VerificationTableGUID")]
		[InverseProperty("VerificationTransVerificationTable")]
		public VerificationTable VerificationTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
