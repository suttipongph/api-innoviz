using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class BuyerInvoiceTable : CompanyBaseEntity
	{
		public BuyerInvoiceTable()
		{
			InvoiceTableBuyerInvoiceTable = new HashSet<InvoiceTable>();
		}
 
		[Key]
		public Guid BuyerInvoiceTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Amount { get; set; }
		public Guid? BuyerAgreementLineGUID { get; set; }
		public Guid? BuyerAgreementTableGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string BuyerInvoiceId { get; set; }
		public Guid CreditAppLineGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime DueDate { get; set; }
		[Column(TypeName = "date")]
		public DateTime InvoiceDate { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("BuyerInvoiceTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("BuyerInvoiceTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BuyerAgreementLineGUID")]
		[InverseProperty("BuyerInvoiceTableBuyerAgreementLine")]
		public BuyerAgreementLine BuyerAgreementLine { get; set; }
		[ForeignKey("BuyerAgreementTableGUID")]
		[InverseProperty("BuyerInvoiceTableBuyerAgreementTable")]
		public BuyerAgreementTable BuyerAgreementTable { get; set; }
		[ForeignKey("CreditAppLineGUID")]
		[InverseProperty("BuyerInvoiceTableCreditAppLine")]
		public CreditAppLine CreditAppLine { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("BuyerInvoiceTable")]
		public ICollection<InvoiceTable> InvoiceTableBuyerInvoiceTable { get; set; }
		#endregion Collection
	}
}
