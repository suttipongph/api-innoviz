using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_PurchaseTable
	{
		[Key]
		[Column(Order = 1)]
		[StringLength(50)]
		public string PurchaseTableGUID { get; set; }
		public bool AdditionalPurchase { get; set; }
		[Required]
		[StringLength(50)]
		public string CreditAppTableGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string CustomerTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public bool DiffChequeIssuedName { get; set; }
		[StringLength(50)]
		public string Dimension1GUID { get; set; }
		[StringLength(50)]
		public string Dimension2GUID { get; set; }
		[StringLength(50)]
		public string Dimension3GUID { get; set; }
		[StringLength(50)]
		public string Dimension4GUID { get; set; }
		[StringLength(50)]
		public string Dimension5GUID { get; set; }
		[StringLength(50)]
		public string DocumentStatusGUID { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal OverCreditBuyer { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal OverCreditCA { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal OverCreditCALine { get; set; }
		public int ProcessInstanceId { get; set; }
		public int ProductType { get; set; }
		public string PurchaseDate { get; set; }
		[Key]
		[Column(Order = 2)]
		[StringLength(20)]
		public string PurchaseId { get; set; }
		[StringLength(50)]
		public string ReceiptTempTableGUID { get; set; }
		public int RetentionCalculateBase { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal RetentionFixedAmount { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal RetentionPct { get; set; }
		public bool Rollbill { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SumPurchaseAmount { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal TotalInterestPct { get; set; }
		[StringLength(50)]
		public string DocumentReasonGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string OperReportSignatureGUID { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }
		[Key]
		[Column(Order = 3)]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }

	}
}
