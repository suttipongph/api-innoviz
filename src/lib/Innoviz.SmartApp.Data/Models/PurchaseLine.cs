using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class PurchaseLine : CompanyBaseEntity
	{
		public PurchaseLine()
		{
			PurchaseLineOriginalPurchaseLine = new HashSet<PurchaseLine>();
			PurchaseLineRefPurchaseLine = new HashSet<PurchaseLine>();
			PurchaseLineRollbillPurchaseLine = new HashSet<PurchaseLine>();
		}
 
		[Key]
		public Guid PurchaseLineGUID { get; set; }
		public Guid AssignmentAgreementTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal AssignmentAmount { get; set; }
		[Column(TypeName = "date")]
		public DateTime? BillingDate { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal BillingFeeAmount { get; set; }
		public Guid? BuyerAgreementTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal BuyerInvoiceAmount { get; set; }
		public Guid BuyerInvoiceTableGUID { get; set; }
		public Guid? BuyerPDCTableGUID { get; set; }
		public Guid BuyerTableGUID { get; set; }
		public bool ClosedForAdditionalPurchase { get; set; }
		[Column(TypeName = "date")]
		public DateTime CollectionDate { get; set; }
		public Guid CreditAppLineGUID  { get; set; }
		public Guid? CustomerPDCTableGUID { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime DueDate { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InterestAmount { get; set; }
		[Column(TypeName = "date")]
		public DateTime InterestDate { get; set; }
		public int InterestDay { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InterestRefundAmount { get; set; }
		public int LineNum { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal LinePurchaseAmount { get; set; }
		[Column(TypeName = "decimal(10,7)")]
		public decimal LinePurchasePct { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal NetPurchaseAmount { get; set; }
		public int NumberOfRollbill { get; set; }
		[Column(TypeName = "date")]
		public DateTime? OriginalInterestDate { get; set; }
		public Guid? OriginalPurchaseLineGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OutstandingBuyerInvoiceAmount  { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal PurchaseAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal PurchaseFeeAmount { get; set; }
		public int PurchaseFeeCalculateBase { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal PurchaseFeePct { get; set; }
		public Guid? PurchaseLineInvoiceTableGUID { get; set; }
		[Column(TypeName = "decimal(10,7)")]
		public decimal PurchasePct { get; set; }
		public Guid PurchaseTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ReceiptFeeAmount { get; set; }
		public Guid? RefPurchaseLineGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ReserveAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal RetentionAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal RollbillInterestAmount { get; set; }
		public int RollbillInterestDay { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal RollbillInterestPct { get; set; }
		public Guid? RollbillPurchaseLineGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleBillingFeeAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleInterestAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettlePurchaseFeeAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleReceiptFeeAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleRollBillInterestAmount { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("PurchaseLineCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("PurchaseLineBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("AssignmentAgreementTableGUID")]
		[InverseProperty("PurchaseLineAssignmentAgreementTable")]
		public AssignmentAgreementTable AssignmentAgreementTable { get; set; }
		[ForeignKey("BuyerAgreementTableGUID")]
		[InverseProperty("PurchaseLineBuyerAgreementTable")]
		public BuyerAgreementTable BuyerAgreementTable { get; set; }
		[ForeignKey("BuyerPDCTableGUID")]
		[InverseProperty("PurchaseLineBuyerPDCTable")]
		public ChequeTable BuyerPDCTable { get; set; }
		[ForeignKey("BuyerTableGUID")]
		[InverseProperty("PurchaseLineBuyerTable")]
		public BuyerTable BuyerTable { get; set; }
		[ForeignKey("CreditAppLineGUID")]
		[InverseProperty("PurchaseLineCreditAppLine")]
		public CreditAppLine CreditAppLine  { get; set; }
		[ForeignKey("CustomerPDCTableGUID")]
		[InverseProperty("PurchaseLineCustomerPDCTable")]
		public ChequeTable CustomerPDCTable { get; set; }
		[ForeignKey("Dimension1GUID")]
		[InverseProperty("PurchaseLineDimension1")]
		public LedgerDimension LedgerDimension1 { get; set; }
		[ForeignKey("Dimension2GUID")]
		[InverseProperty("PurchaseLineDimension2")]
		public LedgerDimension LedgerDimension2 { get; set; }
		[ForeignKey("Dimension3GUID")]
		[InverseProperty("PurchaseLineDimension3")]
		public LedgerDimension LedgerDimension3 { get; set; }
		[ForeignKey("Dimension4GUID")]
		[InverseProperty("PurchaseLineDimension4")]
		public LedgerDimension LedgerDimension4 { get; set; }
		[ForeignKey("Dimension5GUID")]
		[InverseProperty("PurchaseLineDimension5")]
		public LedgerDimension LedgerDimension5 { get; set; }
		[ForeignKey("MethodOfPaymentGUID")]
		[InverseProperty("PurchaseLineMethodOfPayment")]
		public MethodOfPayment MethodOfPayment { get; set; }
		[ForeignKey("OriginalPurchaseLineGUID")]
		[InverseProperty("PurchaseLineOriginalPurchaseLine")]
		public PurchaseLine OriginalPurchaseLine { get; set; }
		[ForeignKey("PurchaseLineInvoiceTableGUID")]
		[InverseProperty("PurchaseLineInvoiceTable")]
		public InvoiceTable InvoiceTable { get; set; }
		[ForeignKey("PurchaseTableGUID")]
		[InverseProperty("PurchaseLinePurchaseTable")]
		public PurchaseTable PurchaseTable { get; set; }
		[ForeignKey("RefPurchaseLineGUID")]
		[InverseProperty("PurchaseLineRefPurchaseLine")]
		public PurchaseLine RefPurchaseLine { get; set; }
		[ForeignKey("RollbillPurchaseLineGUID")]
		[InverseProperty("PurchaseLineRollbillPurchaseLine")]
		public PurchaseLine RollbillPurchaseLine { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("OriginalPurchaseLine")]
		public ICollection<PurchaseLine> PurchaseLineOriginalPurchaseLine { get; set; }
		[InverseProperty("RefPurchaseLine")]
		public ICollection<PurchaseLine> PurchaseLineRefPurchaseLine { get; set; }
		[InverseProperty("RollbillPurchaseLine")]
		public ICollection<PurchaseLine> PurchaseLineRollbillPurchaseLine { get; set; }
		#endregion Collection
	}
}
