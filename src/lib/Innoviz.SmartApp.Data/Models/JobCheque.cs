using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class JobCheque : CompanyBaseEntity
	{
		public JobCheque()
		{
		}
 
		[Key]
		public Guid JobChequeGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Amount { get; set; }
		[StringLength(20)]
		public string ChequeBankAccNo { get; set; }
		public Guid? ChequeBankGroupGUID { get; set; }
		[StringLength(100)]
		public string ChequeBranch { get; set; }
		[Column(TypeName = "date")]
		public DateTime ChequeDate { get; set; }
		[Required]
		[StringLength(20)]
		public string ChequeNo { get; set; }
		public int ChequeSource { get; set; }
		public Guid? ChequeTableGUID { get; set; }
		public Guid? CollectionFollowUpGUID { get; set; }
		public Guid MessengerJobTableGUID { get; set; }
		[StringLength(100)]
		public string RecipientName { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("JobChequeCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("JobChequeBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("ChequeBankGroupGUID")]
		[InverseProperty("JobChequeChequeBankGroup")]
		public BankGroup ChequeBankGroup { get; set; }
		[ForeignKey("ChequeTableGUID")]
		[InverseProperty("JobChequeChequeTable")]
		public ChequeTable ChequeTable { get; set; }
		[ForeignKey("CollectionFollowUpGUID")]
		[InverseProperty("JobChequeCollectionFollowUp")]
		public CollectionFollowUp CollectionFollowUp { get; set; }
		[ForeignKey("MessengerJobTableGUID")]
		[InverseProperty("JobChequeMessengerJobTable")]
		public MessengerJobTable MessengerJobTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
