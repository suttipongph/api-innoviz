using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class DocumentConditionTemplateTable : CompanyBaseEntity
	{
		public DocumentConditionTemplateTable()
		{
			DocumentConditionTemplateLineDocumentConditionTemplateTable = new HashSet<DocumentConditionTemplateLine>();
		}
 
		[Key]
		public Guid DocumentConditionTemplateTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string DocumentConditionTemplateTableId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("DocumentConditionTemplateTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("DocumentConditionTemplateTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("DocumentConditionTemplateTable")]
		public ICollection<DocumentConditionTemplateLine> DocumentConditionTemplateLineDocumentConditionTemplateTable { get; set; }
		#endregion Collection
	}
}
