using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class AuthorizedPersonType : CompanyBaseEntity
	{
		public AuthorizedPersonType()
		{
			AgreementTableInfoAuthorizedPersonTypeBuyer = new HashSet<AgreementTableInfo>();
			AgreementTableInfoAuthorizedPersonTypeCompany = new HashSet<AgreementTableInfo>();
			AgreementTableInfoAuthorizedPersonTypeCustomer = new HashSet<AgreementTableInfo>();
			AuthorizedPersonTransAuthorizedPersonType = new HashSet<AuthorizedPersonTrans>();
			ConsortiumLineAuthorizedPersonType = new HashSet<ConsortiumLine>();
			ConsortiumTransAuthorizedPersonType = new HashSet<ConsortiumTrans>();
			JointVentureTransAuthorizedPersonType = new HashSet<JointVentureTrans>();
		}
 
		[Key]
		public Guid AuthorizedPersonTypeGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string AuthorizedPersonTypeId { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("AuthorizedPersonTypeCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("AuthorizedPersonTypeBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("AuthorizedPersonType")]
		public ICollection<AuthorizedPersonTrans> AuthorizedPersonTransAuthorizedPersonType { get; set; }
		[InverseProperty("AuthorizedPersonType")]
		public ICollection<ConsortiumLine> ConsortiumLineAuthorizedPersonType { get; set; }
		[InverseProperty("AuthorizedPersonType")]
		public ICollection<ConsortiumTrans> ConsortiumTransAuthorizedPersonType { get; set; }
		[InverseProperty("AuthorizedPersonType")]
		public ICollection<JointVentureTrans> JointVentureTransAuthorizedPersonType { get; set; }
		[InverseProperty("AuthorizedPersonTypeBuyer")]
		public ICollection<AgreementTableInfo> AgreementTableInfoAuthorizedPersonTypeBuyer { get; set; }
		[InverseProperty("AuthorizedPersonTypeCompany")]
		public ICollection<AgreementTableInfo> AgreementTableInfoAuthorizedPersonTypeCompany { get; set; }
		[InverseProperty("AuthorizedPersonTypeCustomer")]
		public ICollection<AgreementTableInfo> AgreementTableInfoAuthorizedPersonTypeCustomer { get; set; }
		#endregion Collection
	}
}
