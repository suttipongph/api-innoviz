using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class ReceiptTable : BranchCompanyBaseEntity
	{
		public ReceiptTable()
		{
			PaymentHistoryReceiptTable = new HashSet<PaymentHistory>();
			ReceiptLineReceiptTable = new HashSet<ReceiptLine>();
		}
 
		[Key]
		public Guid ReceiptTableGUID { get; set; }
		public Guid? ChequeBankGroupGUID { get; set; }
		[StringLength(100)]
		public string ChequeBranch { get; set; }
		[Column(TypeName = "date")]
		public DateTime? ChequeDate { get; set; }
		[StringLength(20)]
		public string ChequeNo { get; set; }
		public Guid? CurrencyGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ExchangeRate { get; set; }
		public Guid? InvoiceSettlementDetailGUID { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OverUnderAmount { get; set; }
		[StringLength(250)]
		public string ReceiptAddress1 { get; set; }
		[StringLength(250)]
		public string ReceiptAddress2 { get; set; }
		[Column(TypeName = "date")]
		public DateTime ReceiptDate { get; set; }
		[Required]
		[StringLength(20)]
		public string ReceiptId { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleAmountMST { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleBaseAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleBaseAmountMST { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleTaxAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleTaxAmountMST { get; set; }
		[Column(TypeName = "date")]
		public DateTime TransDate { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("ReceiptTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("BranchGUID")]
		[InverseProperty("ReceiptTableBranch")]
		public Branch Branch { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("ReceiptTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("ChequeBankGroupGUID")]
		[InverseProperty("ReceiptTableChequeBankGroup")]
		public BankGroup ChequeBankGroup { get; set; }
		[ForeignKey("CurrencyGUID")]
		[InverseProperty("ReceiptTableCurrency")]
		public Currency Currency { get; set; }
		[ForeignKey("CustomerTableGUID")]
		[InverseProperty("ReceiptTableCustomerTable")]
		public CustomerTable CustomerTable { get; set; }
		[ForeignKey("DocumentStatusGUID")]
		[InverseProperty("ReceiptTableDocumentStatus")]
		public DocumentStatus DocumentStatus { get; set; }
		[ForeignKey("InvoiceSettlementDetailGUID")]
		[InverseProperty("ReceiptTableInvoiceSettlementDetail")]
		public InvoiceSettlementDetail InvoiceSettlementDetail { get; set; }
		[ForeignKey("MethodOfPaymentGUID")]
		[InverseProperty("ReceiptTableMethodOfPayment")]
		public MethodOfPayment MethodOfPayment { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("ReceiptTable")]
		public ICollection<PaymentHistory> PaymentHistoryReceiptTable { get; set; }
		[InverseProperty("ReceiptTable")]
		public ICollection<ReceiptLine> ReceiptLineReceiptTable { get; set; }
		#endregion Collection
	}
}
