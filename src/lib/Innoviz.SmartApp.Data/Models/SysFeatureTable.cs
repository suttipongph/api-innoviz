﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
    public partial class SysFeatureTable : BaseEntity {
        public SysFeatureTable()
        {
            SysFeatureGroupMapping = new HashSet<SysFeatureGroupMapping>();
        }
        
        [Required]
        [StringLength(100)]
        public string FeatureId { get; set; }
        [StringLength(100)]
        public string ParentFeatureId { get; set; }
        [Key]
        public Guid SysFeatureTableGUID { get; set; }
        public string Path { get; set; }

        
        [InverseProperty("SysFeatureTableGU")]
        public ICollection<SysFeatureGroupMapping> SysFeatureGroupMapping { get; set; }
    }
}
