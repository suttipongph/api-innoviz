﻿using Innoviz.SmartApp.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Innoviz.SmartApp.Data.Models
{
    public class SysControllerEntityMapping: BaseEntity
    {
        [Key]
        public Guid SysControllerEntityMappingGUID { get; set; }
        public Guid SysControllerTableGUID { get; set; }
        public string ModelName { get; set; }
        public int FeatureType { get; set; } // primary, related-info

        [ForeignKey("SysControllerTableGUID")]
        [InverseProperty("SysControllerEntityMapping")]
        public SysControllerTable SysControllerTableGU { get; set; }
    }
}
