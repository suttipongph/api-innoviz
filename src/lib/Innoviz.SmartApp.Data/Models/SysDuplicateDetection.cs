﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
    public partial class SysDuplicateDetection : BaseEntity
    {
        [StringLength(100)]
        public string ModelName { get; set; }
        public int RuleNum { get; set; }
        [StringLength(100)]
        public string PropertyName { get; set; }
        [Required]
        [StringLength(100)]
        public string LabelName { get; set; }
        public bool System { get; set; }
        public bool InActive { get; set; }
    }
}