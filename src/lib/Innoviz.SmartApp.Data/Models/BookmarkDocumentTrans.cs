using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class BookmarkDocumentTrans : CompanyBaseEntity
	{
		public BookmarkDocumentTrans()
		{
		}
 
		[Key]
		public Guid BookmarkDocumentTransGUID { get; set; }
		public Guid BookmarkDocumentGUID { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		public Guid DocumentTemplateTableGUID { get; set; }
		[StringLength(250)]
		public string ReferenceExternalDate  { get; set; }
		[StringLength(20)]
		public string ReferenceExternalId { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("BookmarkDocumentTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("BookmarkDocumentTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BookmarkDocumentGUID")]
		[InverseProperty("BookmarkDocumentTransBookmarkDocument")]
		public BookmarkDocument BookmarkDocument { get; set; }
		[ForeignKey("DocumentStatusGUID")]
		[InverseProperty("BookmarkDocumentTransDocumentStatus")]
		public DocumentStatus DocumentStatus { get; set; }
		[ForeignKey("DocumentTemplateTableGUID")]
		[InverseProperty("BookmarkDocumentTransDocumentTemplateTable")]
		public DocumentTemplateTable DocumentTemplateTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
