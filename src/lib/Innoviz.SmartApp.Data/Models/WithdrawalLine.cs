using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class WithdrawalLine : CompanyBaseEntity
	{
		public WithdrawalLine()
		{
		}
 
		[Key]
		public Guid WithdrawalLineGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime? BillingDate { get; set; }
		public Guid? BuyerPDCTableGUID { get; set; }
		public bool ClosedForTermExtension { get; set; }
		[Column(TypeName = "date")]
		public DateTime CollectionDate { get; set; }
		public Guid? CustomerPDCTableGUID { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime DueDate { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InterestAmount { get; set; }
		[Column(TypeName = "date")]
		public DateTime InterestDate { get; set; }
		public int InterestDay { get; set; }
		public int LineNum { get; set; }
		[Column(TypeName = "date")]
		public DateTime StartDate { get; set; }
		public Guid? WithdrawalLineInvoiceTableGUID { get; set; }
		public int WithdrawalLineType { get; set; }
		public Guid WithdrawalTableGUID { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("WithdrawalLineCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("WithdrawalLineBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BuyerPDCTableGUID")]
		[InverseProperty("WithdrawalLineBuyerPDCTable")]
		public ChequeTable BuyerPDCTable { get; set; }
		[ForeignKey("CustomerPDCTableGUID")]
		[InverseProperty("WithdrawalLineCustomerPDCTable")]
		public ChequeTable CustomerPDCTable { get; set; }
		[ForeignKey("Dimension1GUID")]
		[InverseProperty("WithdrawalLineDimension1")]
		public LedgerDimension LedgerDimension1 { get; set; }
		[ForeignKey("Dimension2GUID")]
		[InverseProperty("WithdrawalLineDimension2")]
		public LedgerDimension LedgerDimension2 { get; set; }
		[ForeignKey("Dimension3GUID")]
		[InverseProperty("WithdrawalLineDimension3")]
		public LedgerDimension LedgerDimension3 { get; set; }
		[ForeignKey("Dimension4GUID")]
		[InverseProperty("WithdrawalLineDimension4")]
		public LedgerDimension LedgerDimension4 { get; set; }
		[ForeignKey("Dimension5GUID")]
		[InverseProperty("WithdrawalLineDimension5")]
		public LedgerDimension LedgerDimension5 { get; set; }
		[ForeignKey("WithdrawalLineInvoiceTableGUID")]
		[InverseProperty("WithdrawalLineWithdrawalLineInvoiceTable")]
		public InvoiceTable WithdrawalLineInvoiceTable { get; set; }
		[ForeignKey("WithdrawalTableGUID")]
		[InverseProperty("WithdrawalLineWithdrawalTable")]
		public WithdrawalTable WithdrawalTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
