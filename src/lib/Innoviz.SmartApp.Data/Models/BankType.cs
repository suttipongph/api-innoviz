using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class BankType : CompanyBaseEntity
	{
		public BankType()
		{
			BusinessCollateralAgmLineBankType = new HashSet<BusinessCollateralAgmLine>();
			CompanyBankBankType = new HashSet<CompanyBank>();
			CreditAppReqBusinessCollateralBankType = new HashSet<CreditAppReqBusinessCollateral>();
			CustBankBankType = new HashSet<CustBank>();
			CustBusinessCollateralBankType = new HashSet<CustBusinessCollateral>();
		}
 
		[Key]
		public Guid BankTypeGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string BankTypeId { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("BankTypeCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("BankTypeBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("BankType")]
		public ICollection<BusinessCollateralAgmLine> BusinessCollateralAgmLineBankType { get; set; }
		[InverseProperty("BankType")]
		public ICollection<CompanyBank> CompanyBankBankType { get; set; }
		[InverseProperty("BankType")]
		public ICollection<CreditAppReqBusinessCollateral> CreditAppReqBusinessCollateralBankType { get; set; }
		[InverseProperty("BankType")]
		public ICollection<CustBank> CustBankBankType { get; set; }
		[InverseProperty("BankType")]
		public ICollection<CustBusinessCollateral> CustBusinessCollateralBankType { get; set; }
		#endregion Collection
	}
}
