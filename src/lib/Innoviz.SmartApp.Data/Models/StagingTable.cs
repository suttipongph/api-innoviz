using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class StagingTable : CompanyBaseEntity
	{
		[Key]
		public Guid StagingTableGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string AccountNum { get; set; }
		public int AccountType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal AmountMST { get; set; }
		[Required]
		[StringLength(20)]
		public string CompanyId { get; set; }
		[StringLength(5)]
		public string CompanyTaxBranchId { get; set; }
		[StringLength(20)]
		public string DimensionCode1 { get; set; }
		[StringLength(20)]
		public string DimensionCode2 { get; set; }
		[StringLength(20)]
		public string DimensionCode3 { get; set; }
		[StringLength(20)]
		public string DimensionCode4 { get; set; }
		[StringLength(20)]
		public string DimensionCode5 { get; set; }
		[StringLength(20)]
		public string DocumentId { get; set; }
		[StringLength(20)]
		public string InterfaceStagingBatchId { get; set; }
		public int InterfaceStatus { get; set; }
		public Guid? ProcessTransGUID { get; set; }
		public int ProcessTransType { get; set; }
		public int ProductType { get; set; }
		public Guid? RefGUID { get; set; }
		public int RefType { get; set; }
		[Required]
		[StringLength(20)]
		public string StagingBatchId { get; set; }
		[StringLength(20)]
		public string TaxAccountId { get; set; }
		[StringLength(100)]
		public string TaxAccountName { get; set; }
		[StringLength(500)]
		public string TaxAddress { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TaxBaseAmount { get; set; }
		[StringLength(5)]
		public string TaxBranchId { get; set; }
		[StringLength(20)]
		public string TaxCode { get; set; }
		[StringLength(13)]
		public string TaxId { get; set; }

		[StringLength(20)]
		public string TaxInvoiceId { get; set; }
		[Column(TypeName = "date")]
		public DateTime TransDate { get; set; }
		[StringLength(250)]
		public string TransText { get; set; }
		[StringLength(20)]
		public string VendorTaxInvoiceId { get; set; }
		[StringLength(20)]
		public string WHTCode { get; set; }
		[StringLength(100)]
		public string SourceRefId { get; set; }
		public int SourceRefType { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("StagingTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("StagingTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("ProcessTransGUID")]
		[InverseProperty("StagingTableProcessTrans")]
		public ProcessTrans ProcessTrans { get; set; }
		#endregion ForeignKey
	}
}
