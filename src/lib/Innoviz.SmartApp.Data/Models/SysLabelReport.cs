﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models 
{
    public class SysLabelReport
    {
        [Key]
        public Guid SysLabelReportGUID { get; set; }
        [StringLength(20)]
        public string LabelType { get; set; }
        [StringLength(100)]
        public string LabelId { get; set; }
        [StringLength(20)]
        public string LanguageId { get; set; }
        [StringLength(250)]
        public string Label { get; set; }
    }
}