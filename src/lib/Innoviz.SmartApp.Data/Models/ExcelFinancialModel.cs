﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.Data.Models
{
    public class ExcelFinancialModel
    {

    }

    public class IRRModel
    {
        public double[] ValueArray { get; set; }
        public double Guess { get; set; } = 0.1;
        public double Result { get; set; }
    }
    public class PMTModel
    {
        public double Rate { get; set; }
        public double Nper { get; set; }
        public double Pv { get; set; }
        public double Fv { get; set; }
        public DueDate Due { get; set; } = DueDate.EndOfPeriod;
        public double Result { get; set; }
    }
    public class RATEModel
    {
        public double Nper { get; set; }
        public double Pmt { get; set; }
        public double Pv { get; set; }
        public double Fv { get; set; } = 0;
        public DueDate Due { get; set; } = DueDate.EndOfPeriod;
        public double Guess { get; set; } = 0.1;
        public double Result { get; set; }
    }

    public enum DueDate
    {
        EndOfPeriod = 0,
        BegOfPeriod = 1
    }
}
