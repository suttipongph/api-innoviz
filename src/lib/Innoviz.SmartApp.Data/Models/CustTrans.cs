using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CustTrans : BranchCompanyBaseEntity
	{
		public CustTrans()
		{
		}
 
		[Key]
		public Guid CustTransGUID { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public Guid? CreditAppLineGUID { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public Guid CurrencyGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public int CustTransStatus { get; set; }
		[StringLength(100)]
		public string Description { get; set; }
		[Column(TypeName = "date")]
		public DateTime? DueDate { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ExchangeRate { get; set; }
		public Guid InvoiceTableGUID { get; set; }
		public Guid InvoiceTypeGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime? LastSettleDate { get; set; }
		public int ProductType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleAmountMST { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TransAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TransAmountMST { get; set; }
		[Column(TypeName = "date")]
		public DateTime TransDate { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CustTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("BranchGUID")]
		[InverseProperty("CustTransBranch")]
		public Branch Branch { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CustTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BuyerTableGUID")]
		[InverseProperty("CustTransBuyerTable")]
		public BuyerTable BuyerTable { get; set; }
		[ForeignKey("CreditAppLineGUID")]
		[InverseProperty("CustTransCreditAppLine")]
		public CreditAppLine CreditAppLine { get; set; }
		[ForeignKey("CreditAppTableGUID")]
		[InverseProperty("CustTransCreditAppTable")]
		public CreditAppTable CreditAppTable { get; set; }
		[ForeignKey("CurrencyGUID")]
		[InverseProperty("CustTransCurrency")]
		public Currency Currency { get; set; }
		[ForeignKey("CustomerTableGUID")]
		[InverseProperty("CustTransCustomerTable")]
		public CustomerTable CustomerTable { get; set; }
		[ForeignKey("InvoiceTableGUID")]
		[InverseProperty("CustTransInvoiceTable")]
		public InvoiceTable InvoiceTable { get; set; }
		[ForeignKey("InvoiceTypeGUID")]
		[InverseProperty("CustTransInvoiceType")]
		public InvoiceType InvoiceType { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
