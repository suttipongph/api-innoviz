using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class BuyerReceiptTable : CompanyBaseEntity
	{
		public BuyerReceiptTable()
		{
			ReceiptTempTableBuyerReceiptTable = new HashSet<ReceiptTempTable>();
		}
 
		[Key]
		public Guid BuyerReceiptTableGUID { get; set; }
		public Guid? AssignmentAgreementTableGUID { get; set; }
		public Guid? BuyerAgreementTableGUID { get; set; }
		[StringLength(500)]
		public string BuyerReceiptAddress { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal BuyerReceiptAmount { get; set; }
		[Column(TypeName = "date")]
		public DateTime BuyerReceiptDate { get; set; }
		[Required]
		[StringLength(20)]
		public string BuyerReceiptId { get; set; }
		public Guid BuyerTableGUID { get; set; }
		public bool Cancel { get; set; }
		[Column(TypeName = "date")]
		public DateTime? ChequeDate { get; set; }
		[StringLength(20)]
		public string ChequeNo { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
		public Guid? RefGUID { get; set; }
		public int RefType { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		public bool ShowRemarkOnly { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("BuyerReceiptTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("BuyerReceiptTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("AssignmentAgreementTableGUID")]
		[InverseProperty("BuyerReceiptTableAssignmentAgreementTable")]
		public AssignmentAgreementTable AssignmentAgreementTable { get; set; }
		[ForeignKey("BuyerAgreementTableGUID")]
		[InverseProperty("BuyerReceiptTableBuyerAgreementTable")]
		public BuyerAgreementTable BuyerAgreementTable { get; set; }
		[ForeignKey("BuyerTableGUID")]
		[InverseProperty("BuyerReceiptTableBuyerTable")]
		public BuyerTable BuyerTable { get; set; }
		[ForeignKey("CustomerTableGUID")]
		[InverseProperty("BuyerReceiptTableCustomerTable")]
		public CustomerTable CustomerTable { get; set; }
		[ForeignKey("MethodOfPaymentGUID")]
		[InverseProperty("BuyerReceiptTableMethodOfPayment")]
		public MethodOfPayment MethodOfPayment { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("BuyerReceiptTable")]
		public ICollection<ReceiptTempTable> ReceiptTempTableBuyerReceiptTable { get; set; }
		#endregion Collection
	}
}
