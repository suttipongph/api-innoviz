using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class StagingTransText : CompanyBaseEntity 
	{
		public StagingTransText()
		{
		}
		[Key]
		public Guid StagingTransTextGUID { get; set; }
		public int ProcessTransType { get; set; }
		[Required]
		[StringLength(250)]
		public string TransText { get; set; }
        #region ForeignKey
        [ForeignKey("CompanyGUID")]
        [InverseProperty("StagingTransTextCompany")]
        public Company Company { get; set; }
        [ForeignKey("OwnerBusinessUnitGUID")]
        [InverseProperty("StagingTransTextBusinessUnit")]
        public BusinessUnit BusinessUnit { get; set; }
        #endregion ForeignKey

        #region Collection
        #endregion Collection
    }
}
