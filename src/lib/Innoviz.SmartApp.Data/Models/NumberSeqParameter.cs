using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class NumberSeqParameter : CompanyBaseEntity
	{
		public NumberSeqParameter()
		{
		}
 
		[Key]
		public Guid NumberSeqParameterGUID { get; set; }
		public Guid? NumberSeqTableGUID { get; set; }
		[Required]
		[StringLength(40)]
		public string ReferenceId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("NumberSeqParameterCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("NumberSeqParameterBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("NumberSeqTableGUID")]
		[InverseProperty("NumberSeqParameterNumberSeqTable")]
		public NumberSeqTable NumberSeqTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
