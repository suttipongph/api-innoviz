using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_PurchaseLine
	{
		[Key]
		[Column(Order = 1)]
		[StringLength(50)]
		public string PurchaseLineGUID { get; set; }
		[StringLength(50)]
		public string AssignmentAgreementTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal AssignmentAmount { get; set; }
		public string BillingDate { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal BillingFeeAmount { get; set; }
		[StringLength(50)]
		public string BuyerAgreementTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal BuyerInvoiceAmount { get; set; }
		[StringLength(50)]
		public string BuyerInvoiceTableGUID { get; set; }
		[StringLength(50)]
		public string BuyerPDCTableGUID { get; set; }
		[StringLength(50)]
		public string BuyerTableGUID { get; set; }
		public bool ClosedForAdditionalPurchase { get; set; }
		public string CollectionDate { get; set; }
		[StringLength(50)]
		public string CreditAppLineGUID  { get; set; }
		[StringLength(50)]
		public string CustomerPDCTableGUID { get; set; }
		[StringLength(50)]
		public string Dimension1GUID { get; set; }
		[StringLength(50)]
		public string Dimension2GUID { get; set; }
		[StringLength(50)]
		public string Dimension3GUID { get; set; }
		[StringLength(50)]
		public string Dimension4GUID { get; set; }
		[StringLength(50)]
		public string Dimension5GUID { get; set; }
		public string DueDate { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InterestAmount { get; set; }
		public string InterestDate { get; set; }
		public int InterestDay { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InterestRefundAmount { get; set; }
		[Key]
		[Column(Order = 3)]
		public int LineNum { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal LinePurchaseAmount { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal LinePurchasePct { get; set; }
		[StringLength(50)]
		public string MethodOfPaymentGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal NetPurchaseAmount { get; set; }
		public int NumberOfRollbill { get; set; }
		public string OriginalInterestDate { get; set; }
		[StringLength(50)]
		public string OriginalPurchaseLineGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OutstandingBuyerInvoiceAmount  { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal PurchaseAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal PurchaseFeeAmount { get; set; }
		public int PurchaseFeeCalculateBase { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal PurchaseFeePct { get; set; }
		[StringLength(50)]
		public string PurchaseLineInvoiceTableGUID { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal PurchasePct { get; set; }
		[Key]
		[Column(Order = 2)]
		[StringLength(50)]
		public string PurchaseTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ReceiptFeeAmount { get; set; }
		[StringLength(50)]
		public string RefPurchaseLineGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ReserveAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal RetentionAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal RollbillInterestAmount { get; set; }
		public int RollbillInterestDay { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal RollbillInterestPct { get; set; }
		[StringLength(50)]
		public string RollbillPurchaseLineGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleBillingFeeAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleInterestAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettlePurchaseFeeAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleReceiptFeeAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleRollBillInterestAmount { get; set; }
		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }
		[Key]
		[Column(Order = 4)]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }
	}
}
