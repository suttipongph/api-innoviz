using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_CustomerTable
	{
		[Key]
		[Column(Order = 1)]
		[StringLength(50)]
		public string CustomerTableGUID { get; set; }
		[StringLength(100)]
		public string AltName { get; set; }
		[StringLength(50)]
		public string BlacklistStatusGUID { get; set; }
		[StringLength(100)]
		public string BOTRating { get; set; }
		[StringLength(50)]
		public string BusinessSegmentGUID { get; set; }
		[StringLength(50)]
		public string BusinessSizeGUID { get; set; }
		[StringLength(50)]
		public string BusinessTypeGUID { get; set; }
		[StringLength(100)]
		public string CompanyName { get; set; }
		[StringLength(100)]
		public string CompanyWebsite { get; set; }
		[StringLength(50)]
		public string CreditScoringGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string CurrencyGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string CustGroupGUID { get; set; }
		[Required]
		[Key]
		[Column(Order = 2)]
		[StringLength(20)]
		public string CustomerId { get; set; }
		[StringLength(50)]
		public string DateOfBirth { get; set; }
		[StringLength(50)]
		public string DateOfEstablish { get; set; }
		[StringLength(50)]
		public string DateOfExpiry { get; set; }
		[StringLength(50)]
		public string DateOfIssue { get; set; }
		[StringLength(50)]
		public string Dimension1GUID { get; set; }
		[StringLength(50)]
		public string Dimension2GUID { get; set; }
		[StringLength(50)]
		public string Dimension3GUID { get; set; }
		[StringLength(50)]
		public string Dimension4GUID { get; set; }
		[StringLength(50)]
		public string Dimension5GUID { get; set; }
		[StringLength(50)]
		public string DocumentStatusGUID { get; set; }
		public int DueDay { get; set; }
		[StringLength(50)]
		public string ExposureGroupGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal FixedAsset { get; set; }
		[StringLength(50)]
		public string GenderGUID { get; set; }
		[StringLength(50)]
		public string GradeClassificationGUID { get; set; }
		public int IdentificationType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Income { get; set; }
		[StringLength(50)]
		public string IntroducedByGUID { get; set; }
		[StringLength(100)]
		public string IntroducedByRemark { get; set; }
		public int InvoiceIssuingDay { get; set; }
		[StringLength(100)]
		public string IssuedBy { get; set; }
		[StringLength(50)]
		public string KYCSetupGUID { get; set; }
		public int Labor { get; set; }
		[StringLength(50)]
		public string LineOfBusinessGUID { get; set; }
		[StringLength(50)]
		public string MaritalStatusGUID { get; set; }
		[StringLength(50)]
		public string MethodOfPaymentGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Name { get; set; }
		[StringLength(50)]
		public string NationalityGUID { get; set; }
		[StringLength(50)]
		public string NCBAccountStatusGUID { get; set; }
		[StringLength(50)]
		public string OccupationGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OtherIncome { get; set; }
		[StringLength(100)]
		public string OtherSourceIncome { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal PaidUpCapital { get; set; }
		[StringLength(50)]
		public string ParentCompanyGUID { get; set; }
		[StringLength(20)]
		public string PassportID { get; set; }
		[StringLength(100)]
		public string Position { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal PrivateARPct { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal PublicARPct { get; set; }
		public string RaceGUID { get; set; }
		public int RecordType { get; set; }
		[StringLength(20)]
		public string ReferenceId { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal RegisteredCapital { get; set; }
		[StringLength(50)]
		public string RegistrationTypeGUID { get; set; }
		[StringLength(50)]
		public string ResponsibleByGUID { get; set; }
		[StringLength(100)]
		public string SigningCondition { get; set; }
		[StringLength(100)]
		public string SpouseName { get; set; }
		[StringLength(13)]
		public string TaxID { get; set; }
		[StringLength(50)]
		public string TerritoryGUID { get; set; }
		[StringLength(50)]
		public string VendorTableGUID { get; set; }
		[StringLength(50)]
		public string WithholdingTaxGroupGUID { get; set; }
		public int WorkExperienceMonth { get; set; }
		public int WorkExperienceYear { get; set; }
		[StringLength(20)]
		public string WorkPermitID { get; set; }

		[Key]
		[Column(Order = 3)]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		[StringLength(50)]
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		[StringLength(50)]
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }
		public int MigrateStatus { get; set; }
        public int MigrateSource { get; set; }
    }
}
