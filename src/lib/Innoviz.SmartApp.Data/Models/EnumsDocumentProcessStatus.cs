﻿namespace Innoviz.SmartApp.Data.Models
{
    public class EnumsDocumentProcessStatus
    {
        public enum AgentJobJournalStatus
        {
            Draft = 33100,
            Assign = 33110,
            Posted = 33120,
            Cancelled = 33130
        }

        public enum AgreementAssetFeesStatus
        {
            Open = 31100,
            Closed = 31110
        }

        public enum AgreementClassificationJournalStatus
        {
            Draft = 36100,
            Posted = 36110,
            Cancelled = 36120
        }

        public enum AgreementEarlyPayoffStatus
        {
            Draft = 34100,
            Posted = 34110,
            Closed = 34120,
            Cancelled = 34130
        }

        public enum AgreementStatus
        {
            Draft = 30100,
            Confirmed = 30110,
            Executed = 30120,
            Closed = 30130,
            Terminated = 30140,
            WriteOff = 30150,
            Cancelled = 30180,
        }

        public enum ApplicationLineStatus
        {
            Draft = 20100,
            Confirmed = 20110,
            Cancelled = 20120
        }

        public enum ApplicationStatus
        {
            Draft = 10100,
            Submitted = 10110,
            Approved = 10120,
            Confirmed = 10130,
            Rejected = 10140,
            Cancelled = 10150
        }

        public enum CollateralMovementJournalStatus
        {
            Draft = 81100,
            Intransit = 81110,
            Posted = 81120
        }

        public enum CollateralStatus
        {
            Open = 80100,
            Closed = 80110,
            Cancelled = 80120
        }

        public enum CollectionActivitiesStatus
        {
            Open = 75100,
            Assigned = 75110,
            Closed = 75120
        }

        public enum DocumentProcessStatus
        {
            Application = 10,
            ApplicationLine = 20,
            Agreement = 30,
            AgreementAssetFees = 31,
            LetterOfRenewalJournal = 32,
            AgentJobJournal = 33,
            AgreementEarlyPayoff = 34,
            LetterOfExpirationJournal = 35,
            AgreementClassificationJournal = 36,
            ProvisionJournal = 37,
            DutyStampJournal = 38,
            PurchaseAgreement = 40,
            PurchaseAgreementConfirmation = 41,
            TemporaryReceipt = 50,
            Receipt = 51,
            Invoice = 60,
            TaxInvoice = 61,
            Waive = 70,
            CollectionActivities = 75,
            LetterOfOverdueJournal = 76,
            CollectionFeeTable = 77,
            Collateral = 80,
            CollateralMovementJournal = 81,
            PaymentStructure = 90,
            Customer = 100,
            Buyer = 101,
            Consortium = 102,
            CreditAppRequestTable = 110,
            AssignmentAgreement = 120,
            Cheque = 130,
            Job = 140,
            Verification = 150,
            Purchase = 160,
            MainAgreement = 170,
            GuarantorAgreement = 180,
            BusinessCollateralAgreement = 190,
            AgreementDocument = 200,
            VendorPayment = 210,
            Withdrawal = 220,
            CustomerRefund = 230,
            BookmarkDocument = 240,
            DocumentReturn = 250,

        }

        public enum InvoiceStatus
        {
            Draft = 60100,
            Posted = 60110,
            Cancelled = 60120
        }

        public enum LetterOfExpirationJournalStatus
        {
            Draft = 35100,
            Posted = 35110,
            Cancelled = 35120
        }

        public enum LetterOfOverdueJournalStatus
        {
            Draft = 76100,
            Posted = 76110,
            Cancelled = 76120
        }

        public enum LetterOfRenewalJournalStatus
        {
            Draft = 32100,
            Posted = 32110,
            Cancelled = 32120
        }

        public enum PaymentStructureStatus
        {
            Draft = 90100,
            Active = 90110,
            Inactive = 90120
        }

        public enum ProvisionJournalStatus
        {
            Draft = 37100,
            Posted = 37110,
            Cancelled = 37120
        }

        public enum PurchaseAgreementConfirmationStatus
        {
            Confirmed = 41100,
            Cancelled = 41110
        }

        public enum PurchaseAgreementStatus
        {
            Draft = 40100,
            Confirmed = 40110,
            Cancelled = 40120
        }

        public enum ReceiptStatus
        {
            Posted = 51100,
            Cancelled = 51110
        }

        public enum TaxInvoiceStatus
        {
            Posted = 61100,
            Cancelled = 61110
        }

        public enum TemporaryReceiptStatus
        {
            Draft = 50100,
            Posted = 50110,
            Cancelled = 50120
        }

        public enum WaiveStatus
        {
            Draft = 70100,
            Submitted = 70110,
            Approved = 70120,
            Closed = 70130,
            Cancelled = 70140
        }

        public enum CollectionFeeStatus
        {
            Open = 77100,
            Closed = 77110,
            Cancelled = 77120
        }

        public enum MovementType
        {
            Issue = 0,
            Receive = 1,
            Transfer = 2,
            Close = 3
        }

        public enum ImportReceiptHeaderStatus
        {
            Draft = 52100,
            Pass = 52110,
            Error = 52120,
            Posted = 52130,
            Cancelled = 52140
        }
        public enum ImportReceiptLineStatus
        {
            Draft = 53100,
            Pass = 53110,
            Error = 53120
        }
        public enum RepossessionAsset
        {
            WaitingForRedemption = 39100,
            WaitingForSale = 39110,
            Sold = 39120
        }


        public enum DutyStampJournalStatus
        {
            Draft = 38100,
            Posted = 38110,
            Cancelled = 38120
        }
        public enum CustomerStatus
        {
            Lead = 100100,
            Prospect = 100110,
            Customer = 100120
        }
        public enum CreditAppRequestStatus
        {
            Draft = 110100,
            WaitingForMarketingStaff = 110110,
            WaitingForMarketingHead = 110120,
            WaitingForCreditStaff = 110130,
            WaitingForCreditHead = 110140,
            WaitingForAssistMD = 110150,
            WaitingForBoard2Of3 = 110160,
            WaitingForBoard3Of4 = 110170,
            WaitingForRetry = 110180,
            Approved = 110190,
            Rejected = 110200,
            Cancelled = 110210,
        }
        public enum AssignmentAgreementStatus
        {
            Draft = 120100,
            Posted = 120110,
            Sent = 120120,
            Signed = 120130,
            Closed = 120140,
            Cancelled = 120150
        }
        public enum MainAgreementStatus
        {
            Draft = 170100,
            Posted = 170110,
            Sent = 170120,
            Signed = 170130,
            Closed = 170140,
            Cancelled = 170150
        }
        public enum GuarantorAgreementStatus
        {
            Draft = 180100,
            Posted = 180110,
            Sent = 180120,
            Signed = 180130,
            Closed = 180140,
            Cancelled = 180150
        }
        public enum PurchaseStatus
        {
            Draft = 160100,
            WaitingForOperationStaff = 160110,
            WaitingForOperationFunctionalHead = 160120,
            WaitingForAssistMD = 160130,
            WaitingForMD = 160140,
            WaitingForRetry = 160145,
            Approved = 160150,
            Rejected = 160160,
            Posted = 160170,
            Closed = 160180,
            Cancelled = 160190,
        }
        public enum BookMarkDocumentStatus
        {
            Draft = 240100,
            InProgress = 240110,
            Completed = 240120,
            Rejected = 240130
        }
        public enum ChequeDocumentStatus
        {
            Created = 130100,
            Deposit = 130110,
            Completed = 130120,
            Bounced = 130130,
            Replaced = 130140,
            Returned = 130150,
            Cancelled = 130160
        }
        public enum BusinessCollateralAgreementStatus
        {
            Draft = 190100,
            Posted = 190110,
            Sent = 190120,
            Signed = 190130,
            Closed = 190140,
            Cancelled = 190150
        }
        public enum VerificationDocumentStatus
        {
            Draft = 150100,
            Approved = 150110,
            Rejected = 150120,
            Cancelled = 150130
        }


        public enum ChequeStatus
        {
            Created = 130100,
            Deposit = 130110,
            Completed = 130120,
            Bounced = 130130,
            Replaced = 130140,
            Returned = 130150,
            Cancelled = 130160
        }
        public enum WithdrawalStatus
        {
            Draft = 220100,
            Posted = 220110,
            Cancelled = 220120
        }
        public enum MessengerJobStatus
        {
            Draft = 140100,
            Assigned = 140110,
            Posted = 140120,
            Cancelled = 140130

        }
        public enum VendorPaymentStatus
        {
            Confirmed = 210100,
            Cancelled = 210110
        }
        public enum CustomerRefundStatus
        {
            Draft = 230100,
            Posted = 230110,
            Cancelled = 230120
        }
        public enum DocumentReturnStatus
        {
            Draft = 250100,
            Posted = 250110,
            Closed = 250120
        }
        public enum IntercompanyInvoiceStatus
        {
            Draft = 260100,
            Posted = 260110,
            Cancelled = 260120
        }
        public enum IntercompanyInvoiceSettlementStatus
        {
            Draft = 270100,
            Posted = 270110,
            Cancelled = 270120
        }
        public enum FreeTextInvoiceStatus
        {
            Draft = 280100,
            Posted = 280110,
            Cancelled = 280120
        }
    }
}