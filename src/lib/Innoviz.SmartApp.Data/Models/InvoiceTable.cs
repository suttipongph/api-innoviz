using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class InvoiceTable : BranchCompanyBaseEntity
	{
		public InvoiceTable()
		{
			AssignmentAgreementSettleInvoiceTable = new HashSet<AssignmentAgreementSettle>();
			CustTransInvoiceTable = new HashSet<CustTrans>();
			FreeTextInvoiceTableInvoiceTable = new HashSet<FreeTextInvoiceTable>();
			InvoiceLineInvoiceTable = new HashSet<InvoiceLine>();
			InvoiceSettlementDetailInvoiceTable = new HashSet<InvoiceSettlementDetail>();
			InvoiceTableRefInvoice = new HashSet<InvoiceTable>();
			PaymentHistoryInvoiceTable = new HashSet<PaymentHistory>();
			PurchaseLineInvoiceTable  = new HashSet<PurchaseLine>();
			ReceiptLineInvoiceTable = new HashSet<ReceiptLine>();
			TaxInvoiceTableInvoiceTable = new HashSet<TaxInvoiceTable>();
			WithdrawalLineWithdrawalLineInvoiceTable = new HashSet<WithdrawalLine>();
		}
 
		[Key]
		public Guid InvoiceTableGUID { get; set; }
		public int AccountingPeriod { get; set; }
		public Guid? BuyerAgreementTableGUID { get; set; }
		public Guid? BuyerInvoiceTableGUID { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public Guid? CNReasonGUID { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public Guid CurrencyGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string CustomerName { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		[StringLength(20)]
		public string DocumentId { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime DueDate { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ExchangeRate { get; set; }
		[StringLength(250)]
		public string InvoiceAddress1 { get; set; }
		[StringLength(250)]
		public string InvoiceAddress2 { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InvoiceAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InvoiceAmountBeforeTax { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InvoiceAmountBeforeTaxMST { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InvoiceAmountMST { get; set; }
		[StringLength(20)]
		public string InvoiceId { get; set; }
		public Guid InvoiceTypeGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime IssuedDate { get; set; }
		[StringLength(250)]
		public string MailingInvoiceAddress1 { get; set; }
		[StringLength(250)]
		public string MailingInvoiceAddress2 { get; set; }
		public int MarketingPeriod { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OrigInvoiceAmount { get; set; }
		[StringLength(20)]
		public string OrigInvoiceId { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OrigTaxInvoiceAmount { get; set; }
		[StringLength(20)]
		public string OrigTaxInvoiceId { get; set; }
		public bool ProductInvoice { get; set; }
		public int ProductType { get; set; }
		public Guid? ReceiptTempTableGUID { get; set; }
		public Guid RefGUID { get; set; }
		public Guid? RefInvoiceGUID { get; set; }
		public Guid? RefTaxInvoiceGUID { get; set; }
		public int RefType { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		public int SuspenseInvoiceType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TaxAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TaxAmountMST { get; set; }
		[StringLength(5)]
		public string TaxBranchId { get; set; }
		[StringLength(100)]
		public string TaxBranchName { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal WHTAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal WHTBaseAmount { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("InvoiceTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("BranchGUID")]
		[InverseProperty("InvoiceTableBranch")]
		public Branch Branch { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("InvoiceTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BuyerAgreementTableGUID")]
		[InverseProperty("InvoiceTableBuyerAgreementTable")]
		public BuyerAgreementTable BuyerAgreementTable { get; set; }
		[ForeignKey("BuyerInvoiceTableGUID")]
		[InverseProperty("InvoiceTableBuyerInvoiceTable")]
		public BuyerInvoiceTable BuyerInvoiceTable { get; set; }
		[ForeignKey("BuyerTableGUID")]
		[InverseProperty("InvoiceTableBuyerTable")]
		public BuyerTable BuyerTable { get; set; }
		[ForeignKey("CNReasonGUID")]
		[InverseProperty("InvoiceTableCNReason")]
		public DocumentReason CNReason { get; set; }
		[ForeignKey("CreditAppTableGUID")]
		[InverseProperty("InvoiceTableCreditAppTable")]
		public CreditAppTable CreditAppTable { get; set; }
		[ForeignKey("CurrencyGUID")]
		[InverseProperty("InvoiceTableCurrency")]
		public Currency Currency { get; set; }
		[ForeignKey("Dimension1GUID")]
		[InverseProperty("InvoiceTableDimension1")]
		public LedgerDimension LedgerDimension1 { get; set; }
		[ForeignKey("Dimension2GUID")]
		[InverseProperty("InvoiceTableDimension2")]
		public LedgerDimension LedgerDimension2 { get; set; }
		[ForeignKey("Dimension3GUID")]
		[InverseProperty("InvoiceTableDimension3")]
		public LedgerDimension LedgerDimension3 { get; set; }
		[ForeignKey("Dimension4GUID")]
		[InverseProperty("InvoiceTableDimension4")]
		public LedgerDimension LedgerDimension4 { get; set; }
		[ForeignKey("Dimension5GUID")]
		[InverseProperty("InvoiceTableDimension5")]
		public LedgerDimension LedgerDimension5 { get; set; }
		[ForeignKey("DocumentStatusGUID")]
		[InverseProperty("InvoiceTableDocumentStatus")]
		public DocumentStatus DocumentStatus { get; set; }
		[ForeignKey("InvoiceTypeGUID")]
		[InverseProperty("InvoiceTableInvoiceType")]
		public InvoiceType InvoiceType { get; set; }
		[ForeignKey("MethodOfPaymentGUID")]
		[InverseProperty("InvoiceTableMethodOfPayment")]
		public MethodOfPayment MethodOfPayment { get; set; }
		[ForeignKey("ReceiptTempTableGUID")]
		[InverseProperty("InvoiceTableReceiptTempTable")]
		public ReceiptTempTable ReceiptTempTable { get; set; }
		[ForeignKey("RefInvoiceGUID")]
		[InverseProperty("InvoiceTableRefInvoice")]
		public InvoiceTable RefInvoice { get; set; }
		[ForeignKey("RefTaxInvoiceGUID")]
		[InverseProperty("InvoiceTableRefTaxInvoice")]
		public TaxInvoiceTable RefTaxInvoice { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("InvoiceTable")]
		public ICollection<AssignmentAgreementSettle> AssignmentAgreementSettleInvoiceTable { get; set; }
		[InverseProperty("InvoiceTable")]
		public ICollection<FreeTextInvoiceTable> FreeTextInvoiceTableInvoiceTable { get; set; }
		[InverseProperty("InvoiceTable")]
		public ICollection<CustTrans> CustTransInvoiceTable { get; set; }
		[InverseProperty("InvoiceTable")]
		public ICollection<InvoiceLine> InvoiceLineInvoiceTable { get; set; }
		[InverseProperty("InvoiceTable")]
		public ICollection<InvoiceSettlementDetail> InvoiceSettlementDetailInvoiceTable { get; set; }
		[InverseProperty("InvoiceTable")]
		public ICollection<PaymentHistory> PaymentHistoryInvoiceTable { get; set; }
		[InverseProperty("InvoiceTable")]
		public ICollection<ReceiptLine> ReceiptLineInvoiceTable { get; set; }
		[InverseProperty("InvoiceTable")]
		public ICollection<TaxInvoiceTable> TaxInvoiceTableInvoiceTable { get; set; }
		[InverseProperty("InvoiceTable")]
		public ICollection<PurchaseLine> PurchaseLineInvoiceTable { get; set; }
		[InverseProperty("RefInvoice")]
		public ICollection<InvoiceTable> InvoiceTableRefInvoice { get; set; }
		[InverseProperty("WithdrawalLineInvoiceTable")]
		public ICollection<WithdrawalLine> WithdrawalLineWithdrawalLineInvoiceTable { get; set; }
		#endregion Collection
	}
}
