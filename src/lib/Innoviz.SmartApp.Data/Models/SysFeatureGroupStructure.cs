﻿using Innoviz.SmartApp.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Innoviz.SmartApp.Data.Models
{
    public class SysFeatureGroupStructure: BaseEntity
    {
        public int NodeId { get; set; }
        public int NodeParentId { get; set; }
        public int SiteLoginType { get; set; }
        public Guid? SysFeatureGroupGUID { get; set; }
        public string NodeLabel { get; set; }
        public int FeatureType { get; set; }

        [ForeignKey("SysFeatureGroupGUID")]
        [InverseProperty("SysFeatureGroupStructure")]
        public SysFeatureGroup SysFeatureGroupGU { get; set; }
        public ICollection<SysFeatureGroupStructure> NodeChildren { get; set; }
        public SysFeatureGroupStructure NodeParent { get; set; }
    }
}
