using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_WithdrawalTable
	{
		[Key]
		[Column(Order = 1)]
		[StringLength(50)]
		public string WithdrawalTableGUID { get; set; }
		[StringLength(50)]
		public string AssignmentAgreementTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal AssignmentAmount { get; set; }
		[StringLength(50)]
		public string BuyerAgreementTableGUID { get; set; }
		[StringLength(50)]
		public string BuyerTableGUID { get; set; }
		[StringLength(50)]
		public string CreditAppLineGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string CreditAppTableGUID { get; set; }
		[StringLength(250)]
		public string CreditComment { get; set; }
		public bool CreditMarkComment { get; set; }
		[Required]
		[StringLength(50)]
		public string CreditTermGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string CustomerTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[StringLength(50)]
		public string Dimension1GUID { get; set; }
		[StringLength(50)]
		public string Dimension2GUID { get; set; }
		[StringLength(50)]
		public string Dimension3GUID { get; set; }
		[StringLength(50)]
		public string Dimension4GUID { get; set; }
		[StringLength(50)]
		public string Dimension5GUID { get; set; }
		[Required]
		[StringLength(50)]
		public string DocumentStatusGUID { get; set; }
		public string DueDate { get; set; }
		public string ExtendInterestDate { get; set; }
		[StringLength(50)]
		public string ExtendWithdrawalTableGUID { get; set; }
		public int InterestCutDay { get; set; }
		[StringLength(50)]
		public string MethodOfPaymentGUID { get; set; }
		public int NumberOfExtension { get; set; }
		[StringLength(250)]
		public string OperationComment { get; set; }
		public bool OperationMarkComment { get; set; }
		[StringLength(50)]
		public string OriginalWithdrawalTableGUID { get; set; }
		public int ProductType { get; set; }
		[StringLength(50)]
		public string ReceiptTempTableGUID { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal RetentionAmount { get; set; }
		public int RetentionCalculateBase { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal RetentionPct { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleTermExtensionFeeAmount { get; set; }
		public bool TermExtension { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TermExtensionFeeAmount { get; set; }
		[StringLength(50)]
		public string TermExtensionInvoiceRevenueTypeGUID { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal TotalInterestPct { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal WithdrawalAmount { get; set; }
		public string WithdrawalDate { get; set; }
		[Required]
		[StringLength(50)]
		public string OperReportSignatureGUID { get; set; }

		[Key]
		[Column(Order = 2)]
		[StringLength(20)]
		public string WithdrawalId { get; set; }
		[StringLength(50)]
		public string DocumentReasonGUID { get; set; }
		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }
		[Key]
		[Column(Order = 3)]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }

	}
}
