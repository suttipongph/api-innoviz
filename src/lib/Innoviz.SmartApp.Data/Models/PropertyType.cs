using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class PropertyType : CompanyBaseEntity
	{
		public PropertyType()
		{
			AddressTransPropertyType = new HashSet<AddressTrans>();
		}
 
		[Key]
		public Guid PropertyTypeGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string PropertyTypeId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("PropertyTypeCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("PropertyTypeBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("PropertyType")]
		public ICollection<AddressTrans> AddressTransPropertyType { get; set; }
		#endregion Collection
	}
}
