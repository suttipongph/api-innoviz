﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.Models
{
    public class GenerateAccountingStagingView
    {
        public string GenerateAccountingStagingGUID { get; set; }
        public List<int> ListProcessTransType { get; set; }
        public List<int> StagingBatchStatus { get; set; }
    }
}
