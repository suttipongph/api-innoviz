using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_AddressTrans
	{

		[Key]
		public string AddressTransGUID { get; set; }
		[Required]
		[StringLength(250)]
		public string Address1 { get; set; }
		[StringLength(250)]
		public string Address2 { get; set; }
		[Required]
		[StringLength(50)]
		public string AddressCountryGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string AddressDistrictGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string AddressPostalCodeGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string AddressProvinceGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string AddressSubDistrictGUID { get; set; }
		[StringLength(500)]
		public string AltAddress { get; set; }
		public bool CurrentAddress { get; set; }
		public bool IsTax { get; set; }
		[Required]
		[StringLength(100)]
		public string Name { get; set; }
		[StringLength(50)]
		public string OwnershipGUID { get; set; }
		public bool Primary { get; set; }
		[StringLength(50)]
		public string PropertyTypeGUID { get; set; }
		[StringLength(50)]
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		[StringLength(5)]
		public string TaxBranchId { get; set; }
		[StringLength(100)]
		public string TaxBranchName { get; set; }

		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }

		[Required]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }
	}
}


