using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class PaymentFrequency : CompanyBaseEntity
	{
		public PaymentFrequency()
		{
			ApplicationTablePaymentFrequency = new HashSet<ApplicationTable>();
		}
 
		[Key]
		public Guid PaymentFrequencyGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string PaymentFrequencyId { get; set; }
		public int PeriodGap { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("PaymentFrequencyCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("PaymentFrequencyBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("PaymentFrequency")]
		public ICollection<ApplicationTable> ApplicationTablePaymentFrequency { get; set; }
		#endregion Collection
	}
}
