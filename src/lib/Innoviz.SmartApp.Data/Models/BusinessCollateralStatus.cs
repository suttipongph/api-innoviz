using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class BusinessCollateralStatus : CompanyBaseEntity
	{
		public BusinessCollateralStatus()
		{
			CustBusinessCollateralBusinessCollateralStatus = new HashSet<CustBusinessCollateral>();
			BusinessCollateralAgmLineBusinessCollateralStatus = new HashSet<BusinessCollateralAgmLine>();
		}
 
		[Key]
		public Guid BusinessCollateralStatusGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string BusinessCollateralStatusId { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("BusinessCollateralStatusCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("BusinessCollateralStatusBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("BusinessCollateralStatus")]
		public ICollection<CustBusinessCollateral> CustBusinessCollateralBusinessCollateralStatus { get; set; }
		[InverseProperty("BusinessCollateralStatus")]
		public ICollection<BusinessCollateralAgmLine> BusinessCollateralAgmLineBusinessCollateralStatus { get; set; }
		#endregion Collection
	}
}
