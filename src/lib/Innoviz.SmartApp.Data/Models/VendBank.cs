using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class VendBank : CompanyBaseEntity
	{
		public VendBank()
		{
		}
 
		[Key]
		public Guid VendBankGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string BankAccount { get; set; }
		[Required]
		[StringLength(100)]
		public string BankAccountName { get; set; }
		[StringLength(100)]
		public string BankBranch { get; set; }
		public Guid BankGroupGUID { get; set; }
		public Guid VendorTableGUID { get; set; }
		public bool Primary { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("VendBankCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("VendBankBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BankGroupGUID")]
		[InverseProperty("VendBankBankGroup")]
		public BankGroup BankGroup { get; set; }
		[ForeignKey("VendorTableGUID")]
		[InverseProperty("VendBankVendorTable")]
		public VendorTable VendorTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
