using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CreditTerm : CompanyBaseEntity
	{
		public CreditTerm()
		{
			CreditAppLineCreditTerm = new HashSet<CreditAppLine>();
			CreditAppRequestLineBuyerCreditTerm = new HashSet<CreditAppRequestLine>();
			CreditAppRequestTableCreditTerm = new HashSet<CreditAppRequestTable>();
			CreditAppTableCreditTerm = new HashSet<CreditAppTable>();
			WithdrawalTableCreditTerm = new HashSet<WithdrawalTable>();
			CompanyParameterPFExtendCreditTerm = new HashSet<CompanyParameter>();
		}
 
		[Key]
		public Guid CreditTermGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string CreditTermId { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public int NumberOfDays { get; set; }

		[InverseProperty("PFExtendCreditTerm")]
		public ICollection<CompanyParameter> CompanyParameterPFExtendCreditTerm { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CreditTermCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CreditTermBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("BuyerCreditTerm")]
		public ICollection<CreditAppRequestLine> CreditAppRequestLineBuyerCreditTerm { get; set; }
		[InverseProperty("CreditTerm")]
		public ICollection<CreditAppLine> CreditAppLineCreditTerm { get; set; }
		[InverseProperty("CreditTerm")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTableCreditTerm { get; set; }
		[InverseProperty("CreditTerm")]
		public ICollection<CreditAppTable> CreditAppTableCreditTerm { get; set; }
		[InverseProperty("CreditTerm")]
		public ICollection<WithdrawalTable> WithdrawalTableCreditTerm { get; set; }
		#endregion Collection
	}
}
