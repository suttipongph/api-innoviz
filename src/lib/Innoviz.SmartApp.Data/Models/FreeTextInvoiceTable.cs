using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class FreeTextInvoiceTable : CompanyBaseEntity {
		[Key]
		public Guid FreeTextInvoiceTableGUID { get; set; }
		public Guid? CNReasonGUID { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public Guid CurrencyGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string CustomerName { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime DueDate { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ExchangeRate { get; set; }
		[StringLength(20)]
		public string FreeTextInvoiceId { get; set; }
		[StringLength(250)]
		public string InvoiceAddress1 { get; set; }
		[StringLength(250)]
		public string InvoiceAddress2 { get; set; }
		public Guid InvoiceAddressGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InvoiceAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InvoiceAmountBeforeTax { get; set; }
		public Guid InvoiceRevenueTypeGUID { get; set; }
		public Guid? InvoiceTableGUID { get; set; }
		[StringLength(250)]
		public string InvoiceText { get; set; }
		public Guid InvoiceTypeGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime IssuedDate { get; set; }
		[StringLength(250)]
		public string MailingInvoiceAddress1 { get; set; }
		[StringLength(250)]
		public string MailingInvoiceAddress2 { get; set; }
		public Guid MailingInvoiceAddressGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OrigInvoiceAmount { get; set; }
		[StringLength(20)]
		public string OrigInvoiceId { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OrigTaxInvoiceAmount { get; set; }
		[StringLength(20)]
		public string OrigTaxInvoiceId { get; set; }
		public int ProductType { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TaxAmount { get; set; }
		[StringLength(5)]
		public string TaxBranchId { get; set; }
		[StringLength(100)]
		public string TaxBranchName { get; set; }
		public Guid? TaxTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal WHTAmount { get; set; }
		public Guid? WithholdingTaxTableGUID { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("FreeTextInvoiceTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("FreeTextInvoiceTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("CNReasonGUID")]
		[InverseProperty("FreeTextInvoiceTableCNReason")]
		public DocumentReason CNReason { get; set; }
		[ForeignKey("CreditAppTableGUID")]
		[InverseProperty("FreeTextInvoiceTableCreditAppTable")]
		public CreditAppTable CreditAppTable { get; set; }
		[ForeignKey("CurrencyGUID")]
		[InverseProperty("FreeTextInvoiceTableCurrency")]
		public Currency Currency { get; set; }
		[ForeignKey("CustomerTableGUID")]
		[InverseProperty("FreeTextInvoiceTableCustomerTable")]
		public CustomerTable CustomerTable { get; set; }
		[ForeignKey("Dimension1GUID")]
		[InverseProperty("FreeTextInvoiceTableDimension1")]
		public LedgerDimension LedgerDimension1 { get; set; }
		[ForeignKey("Dimension2GUID")]
		[InverseProperty("FreeTextInvoiceTableDimension2")]
		public LedgerDimension LedgerDimension2 { get; set; }
		[ForeignKey("Dimension3GUID")]
		[InverseProperty("FreeTextInvoiceTableDimension3")]
		public LedgerDimension LedgerDimension3 { get; set; }
		[ForeignKey("Dimension4GUID")]
		[InverseProperty("FreeTextInvoiceTableDimension4")]
		public LedgerDimension LedgerDimension4 { get; set; }
		[ForeignKey("Dimension5GUID")]
		[InverseProperty("FreeTextInvoiceTableDimension5")]
		public LedgerDimension LedgerDimension5 { get; set; }
		[ForeignKey("DocumentStatusGUID")]
		[InverseProperty("FreeTextInvoiceTableDocumentStatus")]
		public DocumentStatus DocumentStatus { get; set; }
		[ForeignKey("InvoiceAddressGUID")]
		[InverseProperty("FreeTextInvoiceTableInvoiceAddress")]
		public AddressTrans InvoiceAddress { get; set; }
		[ForeignKey("InvoiceRevenueTypeGUID")]
		[InverseProperty("FreeTextInvoiceTableInvoiceRevenueType")]
		public InvoiceRevenueType InvoiceRevenueType { get; set; }
		[ForeignKey("InvoiceTableGUID")]
		[InverseProperty("FreeTextInvoiceTableInvoiceTable")]
		public InvoiceTable InvoiceTable { get; set; }
		[ForeignKey("InvoiceTypeGUID")]
		[InverseProperty("FreeTextInvoiceTableInvoiceType")]
		public InvoiceType InvoiceType { get; set; }
		[ForeignKey("MailingInvoiceAddressGUID")]
		[InverseProperty("FreeTextInvoiceTableMailingInvoiceAddress")]
		public AddressTrans MailingInvoiceAddress { get; set; }
		[ForeignKey("TaxTableGUID")]
		[InverseProperty("FreeTextInvoiceTableTaxTable")]
		public TaxTable TaxTable { get; set; }
		[ForeignKey("WithholdingTaxTableGUID")]
		[InverseProperty("FreeTextInvoiceTableWithholdingTaxTable")]
		public WithholdingTaxTable WithholdingTaxTable { get; set; }
		#endregion ForeignKey
	}
}
