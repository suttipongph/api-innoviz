using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_MainAgreementTable {
		[Key]
		[Column(Order = 1)]
		[StringLength(50)]
		public string MainAgreementTableGUID { get; set; }
		public string AgreementDate { get; set; }
		public int AgreementDocType { get; set; }
		public int Agreementextension { get; set; }
		public int AgreementYear { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ApprovedCreditLimit { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ApprovedCreditLimitLine { get; set; }
		[StringLength(100)]
		public string BuyerAgreementDescription { get; set; }
		[StringLength(20)]
		public string BuyerAgreementReferenceId { get; set; }
		[StringLength(100)]
		public string BuyerName { get; set; }
		public string BuyerTableGUID { get; set; }
		public string ConsortiumTableGUID { get; set; }
		public string CreditAppRequestTableGUID { get; set; }
		public string CreditAppTableGUID { get; set; }
		public string CreditLimitTypeGUID { get; set; }
		[StringLength(100)]
		public string CustomerAltName { get; set; }
		[Required]
		[StringLength(100)]
		public string CustomerName { get; set; }
		public string CustomerTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public string DocumentReasonGUID { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string ExpiryDate { get; set; }
		[Key]
		[Column(Order = 2)]
		[StringLength(20)]
		public string InternalMainAgreementId { get; set; }
		[StringLength(20)]
		public string MainAgreementId { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal MaxInterestPct { get; set; }
		public int ProductType { get; set; }
		public string RefMainAgreementTableGUID { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		public string SigningDate { get; set; }
		public string StartDate { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal TotalInterestPct { get; set; }
		public string WithdrawalDueDate { get; set; }
		public string WithdrawalTableGUID { get; set; }
		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }
		[Key]
		[Column(Order = 3)]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		public string OwnerBusinessUnitGUID { get; set; }
	}
}
