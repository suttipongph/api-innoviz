using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class WithholdingTaxValue : DateEffectiveBaseEntity
	{
		public WithholdingTaxValue()
		{
		}
 
		[Key]
		public Guid WithholdingTaxValueGUID { get; set; }
		[Column(TypeName = "decimal")]
		public decimal Value { get; set; }
		public Guid WithholdingTaxTableGUID { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("WithholdingTaxValueCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("WithholdingTaxValueBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("WithholdingTaxTableGUID")]
		[InverseProperty("WithholdingTaxValueWithholdingTaxTable")]
		public WithholdingTaxTable WithholdingTaxTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
