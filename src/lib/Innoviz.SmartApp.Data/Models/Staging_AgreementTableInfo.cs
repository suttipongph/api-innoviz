using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_AgreementTableInfo
	{

		[Key]
		[StringLength(50)]
		public string AgreementTableInfoGUID { get; set; }
		[StringLength(50)]
		public string AuthorizedPersonTransBuyer1GUID { get; set; }
		[StringLength(50)]
		public string AuthorizedPersonTransBuyer2GUID { get; set; }
		[StringLength(50)]
		public string AuthorizedPersonTransBuyer3GUID { get; set; }
		[StringLength(50)]
		public string AuthorizedPersonTransCompany1GUID { get; set; }
		[StringLength(50)]
		public string AuthorizedPersonTransCompany2GUID { get; set; }
		[StringLength(50)]
		public string AuthorizedPersonTransCompany3GUID { get; set; }
		[StringLength(50)]
		public string AuthorizedPersonTransCustomer1GUID { get; set; }
		[StringLength(50)]
		public string AuthorizedPersonTransCustomer2GUID { get; set; }
		[StringLength(50)]
		public string AuthorizedPersonTransCustomer3GUID { get; set; }
		[StringLength(50)]
		public string AuthorizedPersonTypeBuyerGUID { get; set; }
		[StringLength(50)]
		public string AuthorizedPersonTypeCompanyGUID { get; set; }
		[StringLength(50)]
		public string AuthorizedPersonTypeCustomerGUID { get; set; }
		[StringLength(250)]
		public string BuyerAddress1 { get; set; }
		[StringLength(250)]
		public string BuyerAddress2 { get; set; }
		[StringLength(250)]
		public string CompanyAddress1 { get; set; }
		[StringLength(250)]
		public string CompanyAddress2 { get; set; }
		[StringLength(100)]
		public string CompanyAltName { get; set; }
		public string CompanyBankGUID { get; set; }
		[StringLength(100)]
		public string CompanyFax { get; set; }
		[StringLength(100)]
		public string CompanyName { get; set; }
		[StringLength(100)]
		public string CompanyPhone { get; set; }
		[StringLength(250)]
		public string CustomerAddress1 { get; set; }
		[StringLength(250)]
		public string CustomerAddress2 { get; set; }
		[StringLength(500)]
		public string GuaranteeText { get; set; }
		[StringLength(500)]
		public string GuarantorName { get; set; }
		[StringLength(50)]
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		[StringLength(50)]
		public string WitnessCompany1GUID { get; set; }
		[StringLength(50)]
		public string WitnessCompany2GUID { get; set; }
		[StringLength(100)]
		public string WitnessCustomer1 { get; set; }
		[StringLength(100)]
		public string WitnessCustomer2 { get; set; }
		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }

		[Required]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }

		#region AgreementTableInfoText
		[StringLength(1000)]
		public string Text1 { get; set; }
		[StringLength(1000)]
		public string Text2 { get; set; }
		[StringLength(1000)]
		public string Text3 { get; set; }
		[StringLength(1000)]
		public string Text4 { get; set; }
		#endregion AgreementTableInfoText

	}
}
