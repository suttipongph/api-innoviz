using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class AssignmentAgreementLine : CompanyBaseEntity
	{
		public AssignmentAgreementLine()
		{
		}
 
		[Key]
		public Guid AssignmentAgreementLineGUID { get; set; }
		public Guid AssignmentAgreementTableGUID { get; set; }
		public Guid BuyerAgreementTableGUID { get; set; }
		public int LineNum { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("AssignmentAgreementLineCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("AssignmentAgreementLineBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("AssignmentAgreementTableGUID")]
		[InverseProperty("AssignmentAgreementLineAssignmentAgreementTable")]
		public AssignmentAgreementTable AssignmentAgreementTable { get; set; }
		[ForeignKey("BuyerAgreementTableGUID")]
		[InverseProperty("AssignmentAgreementLineBuyerAgreementTable")]
		public BuyerAgreementTable BuyerAgreementTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
