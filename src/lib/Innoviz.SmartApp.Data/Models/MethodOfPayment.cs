using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class MethodOfPayment : CompanyBaseEntity
	{
		public MethodOfPayment()
		{
			BuyerReceiptTableMethodOfPayment = new HashSet<BuyerReceiptTable>();
			CollectionFollowUpMethodOfPayment = new HashSet<CollectionFollowUp>();
			CreditAppLineMethodOfPayment = new HashSet<CreditAppLine>();
			CreditAppRequestLineMethodOfPayment = new HashSet<CreditAppRequestLine>();
			CustomerTableMethodOfPayment = new HashSet<CustomerTable>();
			IntercompanyInvoiceSettlementMethodOfPayment = new HashSet<IntercompanyInvoiceSettlement>();
			InvoiceTableMethodOfPayment = new HashSet<InvoiceTable>();
			PurchaseLineMethodOfPayment = new HashSet<PurchaseLine>();
			ReceiptTableMethodOfPayment = new HashSet<ReceiptTable>();
			ReceiptTempPaymDetailMethodOfPayment = new HashSet<ReceiptTempPaymDetail>();
			TaxInvoiceTableMethodOfPayment = new HashSet<TaxInvoiceTable>();
			WithdrawalTableMethodOfPayment = new HashSet<WithdrawalTable>();
			CompanyParameterDiscountMethodOfPayment = new HashSet<CompanyParameter>();
			CompanyParameterEarlyToleranceMethodOfPayment = new HashSet<CompanyParameter>();
			CompanyParameterWaiveMethodOfPayment = new HashSet<CompanyParameter>();
			CompanyParameterSettlementMethodOfPayment = new HashSet<CompanyParameter>();
			CompanyParameterSuspenseMethodOfPayment = new HashSet<CompanyParameter>();
		}
 
		[Key]
		public Guid MethodOfPaymentGUID { get; set; }
		[StringLength(20)]
		public string AccountNum { get; set; }
		[Required]
	
		public int AccountType { get; set; }
		public Guid? CompanyBankGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string MethodOfPaymentId { get; set; }
		[StringLength(250)]
		public string PaymentRemark { get; set; }
		public int PaymentType { get; set; }

		[InverseProperty("DiscountMethodOfPayment")]
		public ICollection<CompanyParameter> CompanyParameterDiscountMethodOfPayment { get; set; }
		[InverseProperty("EarlyToleranceMethodOfPayment")]
		public ICollection<CompanyParameter> CompanyParameterEarlyToleranceMethodOfPayment { get; set; }
		[InverseProperty("WaiveMethodOfPayment")]
		public ICollection<CompanyParameter> CompanyParameterWaiveMethodOfPayment { get; set; }
		[InverseProperty("SettlementMethodOfPayment")]
		public ICollection<CompanyParameter> CompanyParameterSettlementMethodOfPayment { get; set; }
		[InverseProperty("SuspenseMethodOfPayment")]
		public ICollection<CompanyParameter> CompanyParameterSuspenseMethodOfPayment { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("MethodOfPaymentCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("MethodOfPaymentBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("CompanyBankGUID")]
		[InverseProperty("MethodOfPaymentCompanyBank")]
		public CompanyBank CompanyBank { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("MethodOfPayment")]
		public ICollection<BuyerReceiptTable> BuyerReceiptTableMethodOfPayment { get; set; }
		[InverseProperty("MethodOfPayment")]
		public ICollection<CollectionFollowUp> CollectionFollowUpMethodOfPayment { get; set; }
		[InverseProperty("MethodOfPayment")]
		public ICollection<CreditAppLine> CreditAppLineMethodOfPayment { get; set; }
		[InverseProperty("MethodOfPayment")]
		public ICollection<CreditAppRequestLine> CreditAppRequestLineMethodOfPayment { get; set; }
		[InverseProperty("MethodOfPayment")]
		public ICollection<CustomerTable> CustomerTableMethodOfPayment { get; set; }
		[InverseProperty("MethodOfPayment")]
		public ICollection<IntercompanyInvoiceSettlement> IntercompanyInvoiceSettlementMethodOfPayment { get; set; }
		[InverseProperty("MethodOfPayment")]
		public ICollection<InvoiceTable> InvoiceTableMethodOfPayment { get; set; }
		[InverseProperty("MethodOfPayment")]
		public ICollection<PurchaseLine> PurchaseLineMethodOfPayment { get; set; }
		[InverseProperty("MethodOfPayment")]
		public ICollection<ReceiptTable> ReceiptTableMethodOfPayment { get; set; }
		[InverseProperty("MethodOfPayment")]
		public ICollection<ReceiptTempPaymDetail> ReceiptTempPaymDetailMethodOfPayment { get; set; }
		[InverseProperty("MethodOfPayment")]
		public ICollection<TaxInvoiceTable> TaxInvoiceTableMethodOfPayment { get; set; }
		[InverseProperty("MethodOfPayment")]
		public ICollection<WithdrawalTable> WithdrawalTableMethodOfPayment { get; set; }
		#endregion Collection
		
	
	}
}
