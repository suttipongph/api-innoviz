using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Territory : CompanyBaseEntity
	{
		public Territory()
		{
			CustomerTableTerritory = new HashSet<CustomerTable>();
		}
 
		[Key]
		public Guid TerritoryGUID { get; set; }
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string TerritoryId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("TerritoryCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("TerritoryBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("Territory")]
		public ICollection<CustomerTable> CustomerTableTerritory { get; set; }
		#endregion Collection
	}
}
