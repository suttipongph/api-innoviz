using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_DocumentConditionTrans
	{
		[Key]
		[StringLength(50)]
		public string DocumentConditionTransGUID { get; set; }
		public int DocConVerifyType { get; set; }
		[Required]
		[StringLength(50)]
		public string DocumentTypeGUID { get; set; }
		public bool Inactive { get; set; }
		public bool Mandatory { get; set; }
		[StringLength(50)]
		public string RefDocumentConditionTransGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string RefGUID { get; set; }
		public int RefType { get; set; }

		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }

		[Required]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }
	}
}


