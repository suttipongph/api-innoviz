using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CreditAppLine : CompanyBaseEntity
	{
		public CreditAppLine()
		{
			BuyerInvoiceTableCreditAppLine = new HashSet<BuyerInvoiceTable>();
			CAReqBuyerCreditOutstandingCreditAppLine = new HashSet<CAReqBuyerCreditOutstanding>();
			CollectionFollowUpCreditAppLine = new HashSet<CollectionFollowUp>();
			CreditAppTransCreditAppLine = new HashSet<CreditAppTrans>();
			CustTransCreditAppLine = new HashSet<CustTrans>();
			MessengerJobTableRefCreditAppLine = new HashSet<MessengerJobTable>();
			PurchaseLineCreditAppLine = new HashSet<PurchaseLine>();
			WithdrawalTableCreditAppLine = new HashSet<WithdrawalTable>();
		}
 
		[Key]
		public Guid CreditAppLineGUID { get; set; }
		public bool AcceptanceDocument { get; set; }
		[StringLength(250)]
		public string AcceptanceDocumentDescription { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ApprovedCreditLimitLine { get; set; }
		public Guid? AssignmentAgreementTableGUID { get; set; }
		public Guid? AssignmentMethodGUID { get; set; }
		[StringLength(250)]
		public string AssignmentMethodRemark { get; set; }
		public Guid? BillingAddressGUID { get; set; }
		public Guid? BillingContactPersonGUID { get; set; }
		public int BillingDay { get; set; }
		[StringLength(100)]
		public string BillingDescription { get; set; }
		[StringLength(250)]
		public string BillingRemark { get; set; }
		public Guid? BillingResponsibleByGUID { get; set; }
		public Guid BuyerTableGUID { get; set; }
		public Guid CreditAppTableGUID { get; set; }
		[StringLength(100)]
		public string CreditTermDescription { get; set; }
		public Guid? CreditTermGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime ExpiryDate { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InsuranceCreditLimit { get; set; }
		public Guid? InvoiceAddressGUID { get; set; }
		[StringLength(250)]
		public string LineCondition { get; set; }
		public int LineNum { get; set; }
		public Guid? MailingReceiptAddressGUID { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal MaxPurchasePct { get; set; }
		public int MethodOfBilling { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
		[StringLength(250)]
		public string PaymentCondition { get; set; }
		public int PurchaseFeeCalculateBase { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal PurchaseFeePct { get; set; }
		public Guid? ReceiptAddressGUID { get; set; }
		public Guid? ReceiptContactPersonGUID { get; set; }
		public int ReceiptDay { get; set; }
		[StringLength(100)]
		public string ReceiptDescription { get; set; }
		[StringLength(250)]
		public string ReceiptRemark { get; set; }
		public Guid? RefCreditAppRequestLineGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime? ReviewDate { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CreditAppLineCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CreditAppLineBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("AssignmentAgreementTableGUID")]
		[InverseProperty("CreditAppLineAssignmentAgreementTable")]
		public AssignmentAgreementTable AssignmentAgreementTable { get; set; }
		[ForeignKey("AssignmentMethodGUID")]
		[InverseProperty("CreditAppLineAssignmentMethod")]
		public AssignmentMethod AssignmentMethod { get; set; }
		[ForeignKey("BillingAddressGUID")]
		[InverseProperty("CreditAppLineBillingAddress")]
		public AddressTrans BillingAddress { get; set; }
		[ForeignKey("BillingContactPersonGUID")]
		[InverseProperty("CreditAppLineBillingContactPerson")]
		public ContactPersonTrans BillingContactPerson { get; set; }
		[ForeignKey("BillingResponsibleByGUID")]
		[InverseProperty("CreditAppLineBillingResponsibleBy")]
		public BillingResponsibleBy BillingResponsibleBy { get; set; }
		[ForeignKey("BuyerTableGUID")]
		[InverseProperty("CreditAppLineBuyerTable")]
		public BuyerTable BuyerTable { get; set; }
		[ForeignKey("CreditAppTableGUID")]
		[InverseProperty("CreditAppLineCreditAppTable")]
		public CreditAppTable CreditAppTable { get; set; }
		[ForeignKey("CreditTermGUID")]
		[InverseProperty("CreditAppLineCreditTerm")]
		public CreditTerm CreditTerm { get; set; }
		[ForeignKey("InvoiceAddressGUID")]
		[InverseProperty("CreditAppLineInvoiceAddress")]
		public AddressTrans InvoiceAddress { get; set; }
		[ForeignKey("MailingReceiptAddressGUID")]
		[InverseProperty("CreditAppLineMailingReceiptAddress")]
		public AddressTrans MailingReceiptAddress { get; set; }
		[ForeignKey("MethodOfPaymentGUID")]
		[InverseProperty("CreditAppLineMethodOfPayment")]
		public MethodOfPayment MethodOfPayment { get; set; }
		[ForeignKey("ReceiptAddressGUID")]
		[InverseProperty("CreditAppLineReceiptAddress")]
		public AddressTrans ReceiptAddress { get; set; }
		[ForeignKey("ReceiptContactPersonGUID")]
		[InverseProperty("CreditAppLineReceiptContactPerson")]
		public ContactPersonTrans ReceiptContactPerson { get; set; }
		[ForeignKey("RefCreditAppRequestLineGUID")]
		[InverseProperty("CreditAppLineRefCreditAppRequestLine")]
		public CreditAppRequestLine RefCreditAppRequestLine { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("CreditAppLine")]
		public ICollection<CAReqBuyerCreditOutstanding> CAReqBuyerCreditOutstandingCreditAppLine { get; set; }
		[InverseProperty("CreditAppLine")]
		public ICollection<PurchaseLine> PurchaseLineCreditAppLine { get; set; }
		[InverseProperty("CreditAppLine")]
		public ICollection<BuyerInvoiceTable> BuyerInvoiceTableCreditAppLine { get; set; }
		[InverseProperty("CreditAppLine")]
		public ICollection<CollectionFollowUp> CollectionFollowUpCreditAppLine { get; set; }
		[InverseProperty("CreditAppLine")]
		public ICollection<CreditAppTrans> CreditAppTransCreditAppLine { get; set; }
		[InverseProperty("CreditAppLine")]
		public ICollection<CustTrans> CustTransCreditAppLine { get; set; }
		[InverseProperty("CreditAppLine")]
		public ICollection<WithdrawalTable> WithdrawalTableCreditAppLine { get; set; }
		[InverseProperty("RefCreditAppLine")]
		public ICollection<MessengerJobTable> MessengerJobTableRefCreditAppLine { get; set; }
		#endregion Collection
	}
}
