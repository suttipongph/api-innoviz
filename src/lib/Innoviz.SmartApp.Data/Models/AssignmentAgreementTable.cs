using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class AssignmentAgreementTable : CompanyBaseEntity
	{
		public AssignmentAgreementTable()
		{
			AssignmentAgreementLineAssignmentAgreementTable = new HashSet<AssignmentAgreementLine>();
			AssignmentAgreementSettleAssignmentAgreementTable = new HashSet<AssignmentAgreementSettle>();
			AssignmentAgreementTableRefAssignmentAgreementTable = new HashSet<AssignmentAgreementTable>();
			BuyerReceiptTableAssignmentAgreementTable = new HashSet<BuyerReceiptTable>();
			CAReqAssignmentOutstandingAssignmentAgreementTable = new HashSet<CAReqAssignmentOutstanding>();
			CreditAppLineAssignmentAgreementTable = new HashSet<CreditAppLine>();
			CreditAppReqAssignmentAssignmentAgreementTable = new HashSet<CreditAppReqAssignment>();
			CreditAppRequestLineAssignmentAgreementTable = new HashSet<CreditAppRequestLine>();
			InvoiceSettlementDetailAssignmentAgreementTable = new HashSet<InvoiceSettlementDetail>();
			MessengerJobTableAssignmentAgreementTable = new HashSet<MessengerJobTable>();
			PurchaseLineAssignmentAgreementTable = new HashSet<PurchaseLine>();
			WithdrawalTableAssignmentAgreementTable = new HashSet<WithdrawalTable>();
		}
 
		[Key]
		public Guid AssignmentAgreementTableGUID { get; set; }
		[StringLength(100)]
		public string AcceptanceName { get; set; }
		[Column(TypeName = "date")]
		public DateTime AgreementDate { get; set; }
		public int AgreementDocType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal AssignmentAgreementAmount { get; set; }
		[StringLength(20)]
		public string AssignmentAgreementId { get; set; }
		public Guid AssignmentMethodGUID { get; set; }
		[StringLength(250)]
		public string AssignmentProductDescription { get; set; }
		[StringLength(100)]
		public string BuyerName { get; set; }
		public Guid BuyerTableGUID { get; set; }
		[StringLength(250)]
		public string CancelAuthorityPersonAddress { get; set; }
		[StringLength(250)]
		public string CancelAuthorityPersonID { get; set; }
		[StringLength(250)]
		public string CancelAuthorityPersonName { get; set; }
		public Guid? ConsortiumTableGUID { get; set; }
		public Guid? CustBankGUID { get; set; }
		[StringLength(100)]
		public string CustomerAltName { get; set; }
		[Required]
		[StringLength(100)]
		public string CustomerName { get; set; }
		public Guid CustomerTableGUID { get; set; }
		[StringLength(250)]
		public string CustRegisteredLocation { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public Guid? DocumentReasonGUID { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string InternalAssignmentAgreementId { get; set; }
		public Guid? RefAssignmentAgreementTableGUID { get; set; }
		[StringLength(20)]
		public string ReferenceAgreementId { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		[Column(TypeName = "date")]
		public DateTime? SigningDate { get; set; }
		[StringLength(250)]
		public string ToWhomConcern { get; set; }
		public Guid? RefCreditAppRequestTableGUID { get; set; }
		public Guid? CreditAppReqAssignmentGUID { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("AssignmentAgreementTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("AssignmentAgreementTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("AssignmentMethodGUID")]
		[InverseProperty("AssignmentAgreementTableAssignmentMethod")]
		public AssignmentMethod AssignmentMethod { get; set; }
		[ForeignKey("BuyerTableGUID")]
		[InverseProperty("AssignmentAgreementTableBuyerTable")]
		public BuyerTable BuyerTable { get; set; }
		[ForeignKey("ConsortiumTableGUID")]
		[InverseProperty("AssignmentAgreementTableConsortiumTable")]
		public ConsortiumTable ConsortiumTable { get; set; }
		[ForeignKey("CustBankGUID")]
		[InverseProperty("AssignmentAgreementTableCustBank")]
		public CustBank CustBank { get; set; }
		[ForeignKey("CustomerTableGUID")]
		[InverseProperty("AssignmentAgreementTableCustomerTable")]
		public CustomerTable CustomerTable { get; set; }
		[ForeignKey("DocumentReasonGUID")]
		[InverseProperty("AssignmentAgreementTableDocumentReason")]
		public DocumentReason DocumentReason { get; set; }
		[ForeignKey("DocumentStatusGUID")]
		[InverseProperty("AssignmentAgreementTableDocumentStatus")]
		public DocumentStatus DocumentStatus { get; set; }
		[ForeignKey("RefAssignmentAgreementTableGUID")]
		[InverseProperty("AssignmentAgreementTableRefAssignmentAgreementTable")]
		public AssignmentAgreementTable RefAssignmentAgreementTable { get; set; }
		[ForeignKey("RefCreditAppRequestTableGUID")]
		[InverseProperty("AssignmentAgreementTableRefCreditAppRequestTable")]
		public CreditAppRequestTable RefCreditAppRequestTable { get; set; }
		[ForeignKey("CreditAppReqAssignmentGUID")]
		[InverseProperty("AssignmentAgreementTableCreditAppReqAssignment")]
		public CreditAppReqAssignment CreditAppReqAssignment { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("AssignmentAgreementTable")]
		public ICollection<AssignmentAgreementLine> AssignmentAgreementLineAssignmentAgreementTable { get; set; }
		[InverseProperty("AssignmentAgreementTable")]
		public ICollection<AssignmentAgreementSettle> AssignmentAgreementSettleAssignmentAgreementTable { get; set; }
		[InverseProperty("AssignmentAgreementTable")]
		public ICollection<BuyerReceiptTable> BuyerReceiptTableAssignmentAgreementTable { get; set; }
		[InverseProperty("AssignmentAgreementTable")]
		public ICollection<CAReqAssignmentOutstanding> CAReqAssignmentOutstandingAssignmentAgreementTable { get; set; }
		[InverseProperty("AssignmentAgreementTable")]
		public ICollection<CreditAppLine> CreditAppLineAssignmentAgreementTable { get; set; }
		[InverseProperty("AssignmentAgreementTable")]
		public ICollection<CreditAppReqAssignment> CreditAppReqAssignmentAssignmentAgreementTable { get; set; }
		[InverseProperty("AssignmentAgreementTable")]
		public ICollection<CreditAppRequestLine> CreditAppRequestLineAssignmentAgreementTable { get; set; }
		[InverseProperty("AssignmentAgreementTable")]
		public ICollection<InvoiceSettlementDetail> InvoiceSettlementDetailAssignmentAgreementTable { get; set; }
		[InverseProperty("AssignmentAgreementTable")]
		public ICollection<MessengerJobTable> MessengerJobTableAssignmentAgreementTable { get; set; }
		[InverseProperty("AssignmentAgreementTable")]
		public ICollection<PurchaseLine> PurchaseLineAssignmentAgreementTable { get; set; }
		[InverseProperty("AssignmentAgreementTable")]
		public ICollection<WithdrawalTable> WithdrawalTableAssignmentAgreementTable { get; set; }
		[InverseProperty("RefAssignmentAgreementTable")]
		public ICollection<AssignmentAgreementTable> AssignmentAgreementTableRefAssignmentAgreementTable { get; set; }
		#endregion Collection
		
	}
}
