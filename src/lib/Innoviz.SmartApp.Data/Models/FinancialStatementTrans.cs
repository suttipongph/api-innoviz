using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class FinancialStatementTrans : CompanyBaseEntity
	{
		public FinancialStatementTrans()
		{
		}
 
		[Key]
		public Guid FinancialStatementTransGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Amount { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public int Ordering { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		public int Year { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("FinancialStatementTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("FinancialStatementTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
