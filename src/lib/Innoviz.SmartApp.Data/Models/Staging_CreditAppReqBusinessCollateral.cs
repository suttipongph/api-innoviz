﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_CreditAppReqBusinessCollateral
	{
		[Key]
		[StringLength(50)]
		public string CreditAppReqBusinessCollateralGUID { get; set; }
		[StringLength(20)]
		public string AccountNumber { get; set; }
		[StringLength(250)]
		public string AttachmentRemark { get; set; }
		[StringLength(50)]
		public string BankGroupGUID { get; set; }
		[StringLength(50)]
		public string BankTypeGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string BusinessCollateralSubTypeGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string BusinessCollateralTypeGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal BusinessCollateralValue { get; set; }
		[StringLength(100)]
		public string BuyerName { get; set; }
		[StringLength(50)]
		public string BuyerTableGUID { get; set; }
		[StringLength(13)]
		public string BuyerTaxIdentificationId { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CapitalValuation { get; set; }
		[StringLength(100)]
		public string ChassisNumber { get; set; }
		[Required]
		[StringLength(50)]
		public string CreditAppRequestTableGUID { get; set; }
		[StringLength(50)]
		public string CustBusinessCollateralGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string CustomerTableGUID { get; set; }
		[StringLength(50)]
		public string DateOfValuation { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal DBDRegistrationAmount { get; set; }
		[StringLength(50)]
		public string DBDRegistrationDate { get; set; }
		[StringLength(100)]
		public string DBDRegistrationDescription { get; set; }
		[StringLength(20)]
		public string DBDRegistrationId { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal GuaranteeAmount { get; set; }
		public bool IsNew { get; set; }
		[StringLength(100)]
		public string Lessee { get; set; }
		[StringLength(100)]
		public string Lessor { get; set; }
		[StringLength(100)]
		public string MachineNumber { get; set; }
		[StringLength(100)]
		public string MachineRegisteredStatus { get; set; }
		[StringLength(100)]
		public string Ownership { get; set; }
		public int PreferentialCreditorNumber { get; set; }
		[StringLength(100)]
		public string ProjectName { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Quantity { get; set; }
		[StringLength(50)]
		public string RefAgreementDate { get; set; }
		[StringLength(20)]
		public string RefAgreementId { get; set; }
		[StringLength(100)]
		public string RegisteredPlace { get; set; }
		[StringLength(100)]
		public string RegistrationPlateNumber { get; set; }
		[StringLength(100)]
		public string TitleDeedDistrict { get; set; }
		[StringLength(100)]
		public string TitleDeedNumber { get; set; }
		[StringLength(100)]
		public string TitleDeedProvince { get; set; }
		[StringLength(100)]
		public string TitleDeedSubDistrict { get; set; }
		[StringLength(20)]
		public string Unit { get; set; }
		[StringLength(100)]
		public string ValuationCommittee { get; set; }
		[Required]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		[StringLength(50)]
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		[StringLength(50)]
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		public string OwnerBusinessUnitGUID { get; set; }
		public int MigrateStatus { get; set; }
		public int MigrateSource { get; set; }
	}
}
