using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class ReceiptTempPaymDetail : CompanyBaseEntity
	{
		public ReceiptTempPaymDetail()
		{
			InvoiceSettlementDetailRefReceiptTempPaymDetail = new HashSet<InvoiceSettlementDetail>();
		}
 
		[Key]
		public Guid ReceiptTempPaymDetailGUID { get; set; }
		public Guid? ChequeBankGroupGUID { get; set; }
		[StringLength(100)]
		public string ChequeBranch { get; set; }
		public Guid? ChequeTableGUID { get; set; }
		public Guid MethodOfPaymentGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ReceiptAmount { get; set; }
		public Guid ReceiptTempTableGUID { get; set; }
		[StringLength(20)]
		public string TransferReference { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("ReceiptTempPaymDetailCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("ReceiptTempPaymDetailBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("ChequeBankGroupGUID")]
		[InverseProperty("ReceiptTempPaymDetailChequeBankGroup")]
		public BankGroup ChequeBankGroup { get; set; }
		[ForeignKey("ChequeTableGUID")]
		[InverseProperty("ReceiptTempPaymDetailChequeTable")]
		public ChequeTable ChequeTable { get; set; }
		[ForeignKey("MethodOfPaymentGUID")]
		[InverseProperty("ReceiptTempPaymDetailMethodOfPayment")]
		public MethodOfPayment MethodOfPayment { get; set; }
		[ForeignKey("ReceiptTempTableGUID")]
		[InverseProperty("ReceiptTempPaymDetailReceiptTempTable")]
		public ReceiptTempTable ReceiptTempTable { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("RefReceiptTempPaymDetail")]
		public ICollection<InvoiceSettlementDetail> InvoiceSettlementDetailRefReceiptTempPaymDetail { get; set; }
		#endregion Collection
	}
}
