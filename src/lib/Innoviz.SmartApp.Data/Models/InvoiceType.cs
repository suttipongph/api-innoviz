using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class InvoiceType : CompanyBaseEntity
	{
		public InvoiceType()
		{
			CustTransInvoiceType = new HashSet<CustTrans>();
			FreeTextInvoiceTableInvoiceType = new HashSet<FreeTextInvoiceTable>();
			IntercompanyInvoiceTableInvoiceType = new HashSet<IntercompanyInvoiceTable>();
			InvoiceNumberSeqSetupInvoiceType = new HashSet<InvoiceNumberSeqSetup>();
			InvoiceSettlementDetailInvoiceType = new HashSet<InvoiceSettlementDetail>();
			InvoiceTableInvoiceType = new HashSet<InvoiceTable>();
			PaymentDetailInvoiceType = new HashSet<PaymentDetail>();
			ReceiptTempTableSuspenseInvoiceType = new HashSet<ReceiptTempTable>();
			TaxInvoiceTableInvoiceType = new HashSet<TaxInvoiceTable>();
			CompanyParameterCollectionInvType = new HashSet<CompanyParameter>();
			CompanyParameterCompulsoryInvType = new HashSet<CompanyParameter>();
			CompanyParameterDepositReturnInvType = new HashSet<CompanyParameter>();
			CompanyParameterEarlyPayOffToleranceInvType = new HashSet<CompanyParameter>();
			CompanyParameterInitInvAdvInvType = new HashSet<CompanyParameter>();
			CompanyParameterInitInvDepInvType = new HashSet<CompanyParameter>();
			CompanyParameterInitInvDownInvType = new HashSet<CompanyParameter>();
			CompanyParameterInstallAdvInvType = new HashSet<CompanyParameter>();
			CompanyParameterInstallInvType = new HashSet<CompanyParameter>();
			CompanyParameterInsuranceInvType = new HashSet<CompanyParameter>();
			CompanyParameterPenaltyInvType = new HashSet<CompanyParameter>();
			CompanyParameterVehicleTaxInvType = new HashSet<CompanyParameter>();
			CompanyParameterServiceFeeInvType = new HashSet<CompanyParameter>();
			CompanyParameterFTInterestInvType = new HashSet<CompanyParameter>();
			CompanyParameterFTInterestRefundInvType = new HashSet<CompanyParameter>();
			CompanyParameterFTFeeInvType = new HashSet<CompanyParameter>();
			CompanyParameterFTInvType = new HashSet<CompanyParameter>();
			CompanyParameterFTReserveToBeRefundedInvType = new HashSet<CompanyParameter>();
			CompanyParameterFTRetentionInvType = new HashSet<CompanyParameter>();
			CompanyParameterFTUnearnedInterestInvType = new HashSet<CompanyParameter>();
			CompanyParameterPFInvType = new HashSet<CompanyParameter>();
			CompanyParameterPFUnearnedInterestInvType = new HashSet<CompanyParameter>();
			CompanyParameterPFInterestInvType = new HashSet<CompanyParameter>();
			CompanyParameterPFInterestRefundInvType = new HashSet<CompanyParameter>();
			CompanyParameterPFRetentionInvType = new HashSet<CompanyParameter>();
			CompanyParameterBondRetentionInvType = new HashSet<CompanyParameter>();
			CompanyParameterLCRetentionInvType = new HashSet<CompanyParameter>();
			CompanyParameterFTRollbillUnearnedInterestInvType = new HashSet<CompanyParameter>();
		}

		[Key]
		public Guid InvoiceTypeGUID { get; set; }
		public bool DirectReceipt { get; set; }
		public bool ValidateDirectReceiveByCA { get; set; }
		[StringLength(20)]
		public string ARLedgerAccount { get; set; }
		public bool AutoGen { get; set; }
		public bool ProductInvoice { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public int ProductType { get; set; }
		[Required]
		[StringLength(20)]
		public string InvoiceTypeId { get; set; }
		public int SuspenseInvoiceType { get; set; }
		public Guid? AutoGenInvoiceRevenueTypeGUID { get; set; }

		[InverseProperty("CollectionInvType")]
		public ICollection<CompanyParameter> CompanyParameterCollectionInvType { get; set; }
		[InverseProperty("CompulsoryInvType")]
		public ICollection<CompanyParameter> CompanyParameterCompulsoryInvType { get; set; }
		[InverseProperty("DepositReturnInvType")]
		public ICollection<CompanyParameter> CompanyParameterDepositReturnInvType { get; set; }
		[InverseProperty("EarlyPayOffToleranceInvType")]
		public ICollection<CompanyParameter> CompanyParameterEarlyPayOffToleranceInvType { get; set; }
		[InverseProperty("InitInvAdvInvType")]
		public ICollection<CompanyParameter> CompanyParameterInitInvAdvInvType { get; set; }
		[InverseProperty("InitInvDepInvType")]
		public ICollection<CompanyParameter> CompanyParameterInitInvDepInvType { get; set; }
		[InverseProperty("InitInvDownInvType")]
		public ICollection<CompanyParameter> CompanyParameterInitInvDownInvType { get; set; }
		[InverseProperty("InstallAdvInvType")]
		public ICollection<CompanyParameter> CompanyParameterInstallAdvInvType { get; set; }
		[InverseProperty("InstallInvType")]
		public ICollection<CompanyParameter> CompanyParameterInstallInvType { get; set; }
		[InverseProperty("InsuranceInvType")]
		public ICollection<CompanyParameter> CompanyParameterInsuranceInvType { get; set; }
		[InverseProperty("PenaltyInvType")]
		public ICollection<CompanyParameter> CompanyParameterPenaltyInvType { get; set; }
		[InverseProperty("VehicleTaxInvType")]
		public ICollection<CompanyParameter> CompanyParameterVehicleTaxInvType { get; set; }
		[InverseProperty("ServiceFeeInvType")]
		public ICollection<CompanyParameter> CompanyParameterServiceFeeInvType { get; set; }
		[InverseProperty("FTInterestInvType")]
		public ICollection<CompanyParameter> CompanyParameterFTInterestInvType { get; set; }
		[InverseProperty("FTInterestRefundInvType")]
		public ICollection<CompanyParameter> CompanyParameterFTInterestRefundInvType { get; set; }
		[InverseProperty("FTFeeInvType")]
		public ICollection<CompanyParameter> CompanyParameterFTFeeInvType { get; set; }
		[InverseProperty("FTInvType")]
		public ICollection<CompanyParameter> CompanyParameterFTInvType { get; set; }
		[InverseProperty("FTReserveToBeRefundedInvType")]
		public ICollection<CompanyParameter> CompanyParameterFTReserveToBeRefundedInvType { get; set; }
		[InverseProperty("FTRetentionInvType")]
		public ICollection<CompanyParameter> CompanyParameterFTRetentionInvType { get; set; }
		[InverseProperty("FTUnearnedInterestInvType")]
		public ICollection<CompanyParameter> CompanyParameterFTUnearnedInterestInvType { get; set; }
		[InverseProperty("PFInvType")]
		public ICollection<CompanyParameter> CompanyParameterPFInvType { get; set; }
		[InverseProperty("PFUnearnedInterestInvType")]
		public ICollection<CompanyParameter> CompanyParameterPFUnearnedInterestInvType { get; set; }
		[InverseProperty("PFInterestInvType")]
		public ICollection<CompanyParameter> CompanyParameterPFInterestInvType { get; set; }
		[InverseProperty("PFInterestRefundInvType")]
		public ICollection<CompanyParameter> CompanyParameterPFInterestRefundInvType { get; set; }
		[InverseProperty("PFRetentionInvType")]
		public ICollection<CompanyParameter> CompanyParameterPFRetentionInvType { get; set; }
		[InverseProperty("BondRetentionInvType")]
		public ICollection<CompanyParameter> CompanyParameterBondRetentionInvType { get; set; }
		[InverseProperty("LCRetentionInvType")]
		public ICollection<CompanyParameter> CompanyParameterLCRetentionInvType { get; set; }
		[InverseProperty("FTRollbillUnearnedInterestInvType")]
		public ICollection<CompanyParameter> CompanyParameterFTRollbillUnearnedInterestInvType { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("InvoiceTypeCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("InvoiceTypeBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("InvoiceType")]
		public ICollection<CustTrans> CustTransInvoiceType { get; set; }
		[InverseProperty("InvoiceType")]
		public ICollection<FreeTextInvoiceTable> FreeTextInvoiceTableInvoiceType { get; set; }
		[InverseProperty("InvoiceType")]
		public ICollection<IntercompanyInvoiceTable> IntercompanyInvoiceTableInvoiceType { get; set; }
		[InverseProperty("InvoiceType")]
		public ICollection<InvoiceNumberSeqSetup> InvoiceNumberSeqSetupInvoiceType { get; set; }
		[InverseProperty("InvoiceType")]
		public ICollection<InvoiceSettlementDetail> InvoiceSettlementDetailInvoiceType { get; set; }
		[InverseProperty("InvoiceType")]
		public ICollection<InvoiceTable> InvoiceTableInvoiceType { get; set; }
		[InverseProperty("InvoiceType")]
		public ICollection<PaymentDetail> PaymentDetailInvoiceType { get; set; }
		[InverseProperty("InvoiceType")]
		public ICollection<TaxInvoiceTable> TaxInvoiceTableInvoiceType { get; set; }
		[InverseProperty("SuspenseInvoiceType")]
		public ICollection<ReceiptTempTable> ReceiptTempTableSuspenseInvoiceType { get; set; }
		#endregion Collection
	}
}
