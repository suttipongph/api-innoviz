using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class DocumentType : CompanyBaseEntity
	{
		public DocumentType()
		{
			DocumentConditionTemplateLineDocumentType = new HashSet<DocumentConditionTemplateLine>();
			DocumentConditionTransDocumentType = new HashSet<DocumentConditionTrans>();
			DocumentReturnLineDocumentType = new HashSet<DocumentReturnLine>();
		}
 
		[Key]
		public Guid DocumentTypeGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string DocumentTypeId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("DocumentTypeCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("DocumentTypeBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("DocumentType")]
		public ICollection<DocumentConditionTemplateLine> DocumentConditionTemplateLineDocumentType { get; set; }
		[InverseProperty("DocumentType")]
		public ICollection<DocumentConditionTrans> DocumentConditionTransDocumentType { get; set; }
		[InverseProperty("DocumentType")]
		public ICollection<DocumentReturnLine> DocumentReturnLineDocumentType { get; set; }
		#endregion Collection
	}
}
