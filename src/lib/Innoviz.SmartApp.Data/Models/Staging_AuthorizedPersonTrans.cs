using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_AuthorizedPersonTrans
	{

		[Key]
		[StringLength(50)]
		public string AuthorizedPersonTransGUID { get; set; }
		[StringLength(50)]
		public string AuthorizedPersonTypeGUID { get; set; }
		public bool InActive { get; set; }
		[StringLength(100)]
		public string NCBCheckedBy { get; set; }
		public string NCBCheckedDate { get; set; }
		public int Ordering { get; set; }
		[StringLength(50)]
		public string RefAuthorizedPersonTransGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		[Required]
		[StringLength(50)]
		public string RelatedPersonTableGUID { get; set; }
		public bool Replace { get; set; }
		[StringLength(50)]
		public string ReplacedAuthorizedPersonTransGUID { get; set; }

		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }

		[Required]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }
	}
}



