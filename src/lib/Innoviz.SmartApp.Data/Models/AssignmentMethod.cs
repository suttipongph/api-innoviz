using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class AssignmentMethod : CompanyBaseEntity
	{
		public AssignmentMethod()
		{
			AssignmentAgreementTableAssignmentMethod = new HashSet<AssignmentAgreementTable>();
			CAReqBuyerCreditOutstandingAssignmentMethod = new HashSet<CAReqBuyerCreditOutstanding>();
			CreditAppLineAssignmentMethod = new HashSet<CreditAppLine>();
			CreditAppReqAssignmentAssignmentMethod = new HashSet<CreditAppReqAssignment>();
			CreditAppRequestLineAssignmentMethod = new HashSet<CreditAppRequestLine>();
			CreditAppRequestTableAssignmentMethod = new HashSet<CreditAppRequestTable>();
			CreditAppTableAssignmentMethod = new HashSet<CreditAppTable>();
		}

		[Key]
		public Guid AssignmentMethodGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string AssignmentMethodId { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public bool ValidateAssignmentBalance { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("AssignmentMethodCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("AssignmentMethodBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("AssignmentMethod")]
		public ICollection<AssignmentAgreementTable> AssignmentAgreementTableAssignmentMethod { get; set; }
		[InverseProperty("AssignmentMethod")]
		public ICollection<CAReqBuyerCreditOutstanding> CAReqBuyerCreditOutstandingAssignmentMethod { get; set; }
		[InverseProperty("AssignmentMethod")]
		public ICollection<CreditAppLine> CreditAppLineAssignmentMethod { get; set; }
		[InverseProperty("AssignmentMethod")]
		public ICollection<CreditAppReqAssignment> CreditAppReqAssignmentAssignmentMethod { get; set; }
		[InverseProperty("AssignmentMethod")]
		public ICollection<CreditAppRequestLine> CreditAppRequestLineAssignmentMethod { get; set; }
		[InverseProperty("AssignmentMethod")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTableAssignmentMethod { get; set; }
		[InverseProperty("AssignmentMethod")]
		public ICollection<CreditAppTable> CreditAppTableAssignmentMethod { get; set; }
		#endregion Collection
	}
}
