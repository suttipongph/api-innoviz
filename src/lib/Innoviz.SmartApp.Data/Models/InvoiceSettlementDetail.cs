using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class InvoiceSettlementDetail : CompanyBaseEntity
	{
		public InvoiceSettlementDetail()
		{
			ProductSettledTransInvoiceSettlementDetail = new HashSet<ProductSettledTrans>();
			ReceiptTableInvoiceSettlementDetail = new HashSet<ReceiptTable>();
		}
 
		[Key]
		public Guid InvoiceSettlementDetailGUID { get; set; }
		public Guid? AssignmentAgreementTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal BalanceAmount { get; set; }
		public Guid? BuyerAgreementTableGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string DocumentId { get; set; }
		public int InterestCalculationMethod { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InvoiceAmount { get; set; }
		[Column(TypeName = "date")]
		public DateTime? InvoiceDueDate { get; set; }
		public Guid? InvoiceTableGUID { get; set; }
		public Guid? InvoiceTypeGUID { get; set; }
		public int ProductType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal PurchaseAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal PurchaseAmountBalance { get; set; }
		public Guid? RefGUID { get; set; }
		public Guid? RefReceiptTempPaymDetailGUID { get; set; }
		public int RefType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal RetentionAmountAccum { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleAssignmentAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleInvoiceAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettlePurchaseAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleReserveAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleTaxAmount { get; set; }
		public int SuspenseInvoiceType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal WHTAmount { get; set; }
		public bool WHTSlipReceivedByBuyer { get; set; }
		public bool WHTSlipReceivedByCustomer { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("InvoiceSettlementDetailCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("InvoiceSettlementDetailBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("AssignmentAgreementTableGUID")]
		[InverseProperty("InvoiceSettlementDetailAssignmentAgreementTable")]
		public AssignmentAgreementTable AssignmentAgreementTable { get; set; }
		[ForeignKey("BuyerAgreementTableGUID")]
		[InverseProperty("InvoiceSettlementDetailBuyerAgreementTable")]
		public BuyerAgreementTable BuyerAgreementTable { get; set; }
		[ForeignKey("InvoiceTableGUID")]
		[InverseProperty("InvoiceSettlementDetailInvoiceTable")]
		public InvoiceTable InvoiceTable { get; set; }
		[ForeignKey("InvoiceTypeGUID")]
		[InverseProperty("InvoiceSettlementDetailInvoiceType")]
		public InvoiceType InvoiceType { get; set; }
		[ForeignKey("RefReceiptTempPaymDetailGUID")]
		[InverseProperty("InvoiceSettlementDetailRefReceiptTempPaymDetail")]
		public ReceiptTempPaymDetail RefReceiptTempPaymDetail { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("InvoiceSettlementDetail")]
		public ICollection<ProductSettledTrans> ProductSettledTransInvoiceSettlementDetail { get; set; }
		[InverseProperty("InvoiceSettlementDetail")]
		public ICollection<ReceiptTable> ReceiptTableInvoiceSettlementDetail { get; set; }
		#endregion Collection
	}
}
