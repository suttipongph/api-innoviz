﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_CreditAppRequestLine
	{
		[Key]
		[Column(Order = 1)]
		[StringLength(50)]
		public string CreditAppRequestLineGUID { get; set; }
		public bool AcceptanceDocument { get; set; }
		[StringLength(100)]
		public string AcceptanceDocumentDescription { get; set; }
		public int ApprovalDecision { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ApprovedCreditLimitLineRequest { get; set; }
		[StringLength(500)]
		public string ApproverComment { get; set; }
		[StringLength(50)]
		public string AssignmentAgreementTableGUID { get; set; }
		[StringLength(50)]
		public string AssignmentMethodGUID { get; set; }
		[StringLength(250)]
		public string AssignmentMethodRemark { get; set; }
		[StringLength(50)]
		public string BillingAddressGUID { get; set; }
		[StringLength(50)]
		public string BillingContactPersonGUID { get; set; }
		public int BillingDay { get; set; }
		[StringLength(100)]
		public string BillingDescription { get; set; }
		[StringLength(250)]
		public string BillingRemark { get; set; }
		[StringLength(50)]
		public string BillingResponsibleByGUID { get; set; }
		[StringLength(50)]
		public string BlacklistStatusGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal BuyerCreditLimit { get; set; }
		[StringLength(50)]
		public string BuyerCreditTermGUID { get; set; }
		[StringLength(100)]
		public string BuyerProduct { get; set; }
		[Required]
		[StringLength(50)]
		public string BuyerTableGUID { get; set; }
		[Key]
		[Column(Order = 2)]
		[StringLength(50)]
		public string CreditAppRequestTableGUID { get; set; }
		[StringLength(500)]
		public string CreditComment { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CreditLimitLineRequest { get; set; }
		[StringLength(50)]
		public string CreditScoringGUID { get; set; }
		[StringLength(100)]
		public string CreditTermDescription { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CustomerContactBuyerPeriod { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CustomerRequest { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InsuranceCreditLimit { get; set; }
		[StringLength(50)]
		public string InvoiceAddressGUID { get; set; }
		[StringLength(50)]
		public string KYCSetupGUID { get; set; }
		[Key]
		[Column(Order = 3)]
		public int LineNum { get; set; }
		[StringLength(50)]
		public string MailingReceiptAddressGUID { get; set; }
		[StringLength(500)]
		public string MarketingComment { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal MaxPurchasePct { get; set; }
		public int MethodOfBilling { get; set; }
		[StringLength(50)]
		public string MethodOfPaymentGUID { get; set; }
		[StringLength(250)]
		public string PaymentCondition { get; set; }
		public int PurchaseFeeCalculateBase { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal PurchaseFeePct { get; set; }
		[StringLength(50)]
		public string ReceiptAddressGUID { get; set; }
		[StringLength(50)]
		public string ReceiptContactPersonGUID { get; set; }
		public int ReceiptDay { get; set; }
		[StringLength(100)]
		public string ReceiptDescription { get; set; }
		[StringLength(250)]
		public string ReceiptRemark { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal RemainingCreditLoanRequest { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal AllCustomerBuyerOutstanding { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CustomerBuyerOutstanding { get; set; }
		[StringLength(50)]
		public string RefCreditAppLineGUID { get; set; }
		[StringLength(250)]
		public string LineCondition { get; set; }
		[Key]
		[Column(Order = 4)]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		[StringLength(50)]
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		[StringLength(50)]
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		public string OwnerBusinessUnitGUID { get; set; }
		public int MigrateStatus { get; set; }
		public int MigrateSource { get; set; }
	}
}
