using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class MaritalStatus : CompanyBaseEntity
	{
		public MaritalStatus()
		{
			CustomerTableMaritalStatus = new HashSet<CustomerTable>();
			RelatedPersonTableMaritalStatus = new HashSet<RelatedPersonTable>();
		}
 
		[Key]
		public Guid MaritalStatusGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string MaritalStatusId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("MaritalStatusCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("MaritalStatusBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("MaritalStatus")]
		public ICollection<CustomerTable> CustomerTableMaritalStatus { get; set; }
		[InverseProperty("MaritalStatus")]
		public ICollection<RelatedPersonTable> RelatedPersonTableMaritalStatus { get; set; }
		#endregion Collection
	}
}
