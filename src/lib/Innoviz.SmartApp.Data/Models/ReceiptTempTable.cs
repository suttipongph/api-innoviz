using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class ReceiptTempTable : CompanyBaseEntity
	{
		public ReceiptTempTable()
		{
			InvoiceTableReceiptTempTable = new HashSet<InvoiceTable>();
			PurchaseTableReceiptTempTable = new HashSet<PurchaseTable>();
			ReceiptTempPaymDetailReceiptTempTable = new HashSet<ReceiptTempPaymDetail>();
			ReceiptTempTableMainReceiptTemp = new HashSet<ReceiptTempTable>();
			ReceiptTempTableRefReceiptTemp = new HashSet<ReceiptTempTable>();
			WithdrawalTableReceiptTempTable = new HashSet<WithdrawalTable>();
		}
 
		[Key]
		public Guid ReceiptTempTableGUID { get; set; }
		public Guid? BuyerReceiptTableGUID { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public Guid? CurrencyGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public Guid? DocumentReasonGUID { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		[Column(TypeName = "decimal(18,5)")]
		public decimal ExchangeRate { get; set; }
		public Guid? MainReceiptTempGUID { get; set; }
		public int ProductType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ReceiptAmount { get; set; }
		[Column(TypeName = "date")]
		public DateTime ReceiptDate { get; set; }
		[Required]
		[StringLength(20)]
		public string ReceiptTempId { get; set; }
		public int ReceiptTempRefType { get; set; }
		public int ReceivedFrom { get; set; }
		public Guid? RefReceiptTempGUID { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleFeeAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SuspenseAmount { get; set; }
		public Guid? SuspenseInvoiceTypeGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime TransDate { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("ReceiptTempTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("ReceiptTempTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BuyerReceiptTableGUID")]
		[InverseProperty("ReceiptTempTableBuyerReceiptTable")]
		public BuyerReceiptTable BuyerReceiptTable { get; set; }
		[ForeignKey("BuyerTableGUID")]
		[InverseProperty("ReceiptTempTableBuyerTable")]
		public BuyerTable BuyerTable { get; set; }
		[ForeignKey("CreditAppTableGUID")]
		[InverseProperty("ReceiptTempTableCreditAppTable")]
		public CreditAppTable CreditAppTable { get; set; }
		[ForeignKey("CurrencyGUID")]
		[InverseProperty("ReceiptTempTableCurrency")]
		public Currency Currency { get; set; }
		[ForeignKey("Dimension1GUID")]
		[InverseProperty("ReceiptTempTableDimension1")]
		public LedgerDimension LedgerDimension1 { get; set; }
		[ForeignKey("Dimension2GUID")]
		[InverseProperty("ReceiptTempTableDimension2")]
		public LedgerDimension LedgerDimension2 { get; set; }
		[ForeignKey("Dimension3GUID")]
		[InverseProperty("ReceiptTempTableDimension3")]
		public LedgerDimension LedgerDimension3 { get; set; }
		[ForeignKey("Dimension4GUID")]
		[InverseProperty("ReceiptTempTableDimension4")]
		public LedgerDimension LedgerDimension4 { get; set; }
		[ForeignKey("Dimension5GUID")]
		[InverseProperty("ReceiptTempTableDimension5")]
		public LedgerDimension LedgerDimension5 { get; set; }
		[ForeignKey("DocumentReasonGUID")]
		[InverseProperty("ReceiptTempTableDocumentReason")]
		public DocumentReason DocumentReason { get; set; }
		[ForeignKey("DocumentStatusGUID")]
		[InverseProperty("ReceiptTempTableDocumentStatus")]
		public DocumentStatus DocumentStatus { get; set; }
		[ForeignKey("MainReceiptTempGUID")]
		[InverseProperty("ReceiptTempTableMainReceiptTemp")]
		public ReceiptTempTable MainReceiptTemp { get; set; }
		[ForeignKey("RefReceiptTempGUID")]
		[InverseProperty("ReceiptTempTableRefReceiptTemp")]
		public ReceiptTempTable RefReceiptTemp { get; set; }
		[ForeignKey("SuspenseInvoiceTypeGUID")]
		[InverseProperty("ReceiptTempTableSuspenseInvoiceType")]
		public InvoiceType SuspenseInvoiceType { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("MainReceiptTemp")]
		public ICollection<ReceiptTempTable> ReceiptTempTableMainReceiptTemp { get; set; }
		[InverseProperty("ReceiptTempTable")]
		public ICollection<InvoiceTable> InvoiceTableReceiptTempTable { get; set; }
		[InverseProperty("ReceiptTempTable")]
		public ICollection<PurchaseTable> PurchaseTableReceiptTempTable { get; set; }
		[InverseProperty("ReceiptTempTable")]
		public ICollection<ReceiptTempPaymDetail> ReceiptTempPaymDetailReceiptTempTable { get; set; }
		[InverseProperty("ReceiptTempTable")]
		public ICollection<WithdrawalTable> WithdrawalTableReceiptTempTable { get; set; }
		[InverseProperty("RefReceiptTemp")]
		public ICollection<ReceiptTempTable> ReceiptTempTableRefReceiptTemp { get; set; }
		#endregion Collection
	}
}
