using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class EmplTeam : CompanyBaseEntity
	{
		public EmplTeam()
		{
			EmployeeTableEmplTeam = new HashSet<EmployeeTable>();
		}
 
		[Key]
		public Guid EmplTeamGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Name { get; set; }
		[Required]
		[StringLength(20)]
		public string TeamId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("EmplTeamCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("EmplTeamBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("EmplTeam")]
		public ICollection<EmployeeTable> EmployeeTableEmplTeam { get; set; }
		#endregion Collection
	}
}
