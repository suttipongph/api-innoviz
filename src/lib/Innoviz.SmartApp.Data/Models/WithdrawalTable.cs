using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class WithdrawalTable : CompanyBaseEntity
	{
		public WithdrawalTable()
		{
			MainAgreementTableWithdrawalTable = new HashSet<MainAgreementTable>();
			ProjectProgressTableWithdrawalTable = new HashSet<ProjectProgressTable>();
			WithdrawalLineWithdrawalTable = new HashSet<WithdrawalLine>();
			WithdrawalTableExtendWithdrawalTable = new HashSet<WithdrawalTable>();
			WithdrawalTableOriginalWithdrawalTable = new HashSet<WithdrawalTable>();
		}
 
		[Key]
		public Guid WithdrawalTableGUID { get; set; }
		public Guid? AssignmentAgreementTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal AssignmentAmount { get; set; }
		public Guid? BuyerAgreementTableGUID { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public Guid? CreditAppLineGUID { get; set; }
		public Guid CreditAppTableGUID { get; set; }
		[StringLength(250)]
		public string CreditComment { get; set; }
		public bool CreditMarkComment { get; set; }
		public Guid CreditTermGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime DueDate { get; set; }
		[Column(TypeName = "date")]
		public DateTime? ExtendInterestDate { get; set; }
		public Guid? ExtendWithdrawalTableGUID { get; set; }
		public int InterestCutDay { get; set; }
		[StringLength(250)]
		public string MarketingComment { get; set; }
		public bool MarketingMarkComment { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
		public int NumberOfExtension { get; set; }
		[StringLength(250)]
		public string OperationComment { get; set; }
		public bool OperationMarkComment { get; set; }
		public Guid? OriginalWithdrawalTableGUID { get; set; }
		public int ProductType { get; set; }
		public Guid? ReceiptTempTableGUID { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal RetentionAmount { get; set; }
		public int RetentionCalculateBase { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal RetentionPct { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleTermExtensionFeeAmount { get; set; }
		public bool TermExtension { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TermExtensionFeeAmount { get; set; }
		public Guid? TermExtensionInvoiceRevenueTypeGUID { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal TotalInterestPct { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal WithdrawalAmount { get; set; }
		[Column(TypeName = "date")]
		public DateTime WithdrawalDate { get; set; }
		[Required]
		[StringLength(20)]
		public string WithdrawalId { get; set; }
		public Guid? DocumentReasonGUID { get; set; }
		public Guid OperReportSignatureGUID { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("WithdrawalTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("WithdrawalTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("AssignmentAgreementTableGUID")]
		[InverseProperty("WithdrawalTableAssignmentAgreementTable")]
		public AssignmentAgreementTable AssignmentAgreementTable { get; set; }
		[ForeignKey("BuyerAgreementTableGUID")]
		[InverseProperty("WithdrawalTableBuyerAgreementTable")]
		public BuyerAgreementTable BuyerAgreementTable { get; set; }
		[ForeignKey("BuyerTableGUID")]
		[InverseProperty("WithdrawalTableBuyerTable")]
		public BuyerTable BuyerTable { get; set; }
		[ForeignKey("CreditAppLineGUID")]
		[InverseProperty("WithdrawalTableCreditAppLine")]
		public CreditAppLine CreditAppLine { get; set; }
		[ForeignKey("CreditAppTableGUID")]
		[InverseProperty("WithdrawalTableCreditAppTable")]
		public CreditAppTable CreditAppTable { get; set; }
		[ForeignKey("CreditTermGUID")]
		[InverseProperty("WithdrawalTableCreditTerm")]
		public CreditTerm CreditTerm { get; set; }
		[ForeignKey("CustomerTableGUID")]
		[InverseProperty("WithdrawalTableCustomerTable")]
		public CustomerTable CustomerTable { get; set; }
		[ForeignKey("Dimension1GUID")]
		[InverseProperty("WithdrawalTableDimension1")]
		public LedgerDimension LedgerDimension1 { get; set; }
		[ForeignKey("Dimension2GUID")]
		[InverseProperty("WithdrawalTableDimension2")]
		public LedgerDimension LedgerDimension2 { get; set; }
		[ForeignKey("Dimension3GUID")]
		[InverseProperty("WithdrawalTableDimension3")]
		public LedgerDimension LedgerDimension3 { get; set; }
		[ForeignKey("Dimension4GUID")]
		[InverseProperty("WithdrawalTableDimension4")]
		public LedgerDimension LedgerDimension4 { get; set; }
		[ForeignKey("Dimension5GUID")]
		[InverseProperty("WithdrawalTableDimension5")]
		public LedgerDimension LedgerDimension5 { get; set; }
		[ForeignKey("DocumentReasonGUID")]
		[InverseProperty("WithdrawalTableDocumentReason")]
		public DocumentReason DocumentReason { get; set; }
		[ForeignKey("DocumentStatusGUID")]
		[InverseProperty("WithdrawalTableDocumentStatus")]
		public DocumentStatus DocumentStatus { get; set; }
		[ForeignKey("ExtendWithdrawalTableGUID")]
		[InverseProperty("WithdrawalTableExtendWithdrawalTable")]
		public WithdrawalTable ExtendWithdrawalTable { get; set; }
		[ForeignKey("MethodOfPaymentGUID")]
		[InverseProperty("WithdrawalTableMethodOfPayment")]
		public MethodOfPayment MethodOfPayment { get; set; }
		[ForeignKey("OriginalWithdrawalTableGUID")]
		[InverseProperty("WithdrawalTableOriginalWithdrawalTable")]
		public WithdrawalTable OriginalWithdrawalTable { get; set; }
		[ForeignKey("ReceiptTempTableGUID")]
		[InverseProperty("WithdrawalTableReceiptTempTable")]
		public ReceiptTempTable ReceiptTempTable { get; set; }
		[ForeignKey("TermExtensionInvoiceRevenueTypeGUID")]
		[InverseProperty("WithdrawalTableTermExtensionInvoiceRevenueType")]
		public InvoiceRevenueType TermExtensionInvoiceRevenueType { get; set; }
		[ForeignKey("OperReportSignatureGUID")]
		[InverseProperty("WithdrawalTableEmployeeTable")]
		public EmployeeTable EmployeeTable { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("ExtendWithdrawalTable")]
		public ICollection<WithdrawalTable> WithdrawalTableExtendWithdrawalTable { get; set; }
		[InverseProperty("OriginalWithdrawalTable")]
		public ICollection<WithdrawalTable> WithdrawalTableOriginalWithdrawalTable { get; set; }
		[InverseProperty("WithdrawalTable")]
		public ICollection<MainAgreementTable> MainAgreementTableWithdrawalTable { get; set; }
		[InverseProperty("WithdrawalTable")]
		public ICollection<ProjectProgressTable> ProjectProgressTableWithdrawalTable { get; set; }
		[InverseProperty("WithdrawalTable")]
		public ICollection<WithdrawalLine> WithdrawalLineWithdrawalTable { get; set; }
		#endregion Collection
	}
}
