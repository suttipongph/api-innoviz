﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Innoviz.SmartApp.Data.Models
{
    public class SysDataInitializationHistory
    {
        public int DataInitializationType { get; set; }
        [Required]
        [StringLength(20)]
        public string Version { get; set; }
    }
}
