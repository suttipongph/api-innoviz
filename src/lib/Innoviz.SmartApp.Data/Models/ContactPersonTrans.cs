using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class ContactPersonTrans : CompanyBaseEntity
	{
		public ContactPersonTrans()
		{
			CreditAppLineBillingContactPerson = new HashSet<CreditAppLine>();
			CreditAppLineReceiptContactPerson = new HashSet<CreditAppLine>();
			CreditAppRequestLineBillingContactPerson = new HashSet<CreditAppRequestLine>();
			CreditAppRequestLineReceiptContactPerson = new HashSet<CreditAppRequestLine>();
			CreditAppRequestTableBillingContactPerson = new HashSet<CreditAppRequestTable>();
			CreditAppRequestTableReceiptContactPerson = new HashSet<CreditAppRequestTable>();
			CreditAppTableBillingContactPerson = new HashSet<CreditAppTable>();
			CreditAppTableReceiptContactPerson = new HashSet<CreditAppTable>();
		}
 
		[Key]
		public Guid ContactPersonTransGUID { get; set; }
		public bool InActive { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		public Guid RelatedPersonTableGUID { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("ContactPersonTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("ContactPersonTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("RelatedPersonTableGUID")]
		[InverseProperty("ContactPersonTransRelatedPersonTable")]
		public RelatedPersonTable RelatedPersonTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("BillingContactPerson")]
		public ICollection<CreditAppLine> CreditAppLineBillingContactPerson { get; set; }
		[InverseProperty("BillingContactPerson")]
		public ICollection<CreditAppRequestLine> CreditAppRequestLineBillingContactPerson { get; set; }
		[InverseProperty("BillingContactPerson")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTableBillingContactPerson { get; set; }
		[InverseProperty("BillingContactPerson")]
		public ICollection<CreditAppTable> CreditAppTableBillingContactPerson { get; set; }
		[InverseProperty("ReceiptContactPerson")]
		public ICollection<CreditAppLine> CreditAppLineReceiptContactPerson { get; set; }
		[InverseProperty("ReceiptContactPerson")]
		public ICollection<CreditAppRequestLine> CreditAppRequestLineReceiptContactPerson { get; set; }
		[InverseProperty("ReceiptContactPerson")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTableReceiptContactPerson { get; set; }
		[InverseProperty("ReceiptContactPerson")]
		public ICollection<CreditAppTable> CreditAppTableReceiptContactPerson { get; set; }
		#endregion Collection
	}
}
