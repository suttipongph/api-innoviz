using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class VendGroup : CompanyBaseEntity
	{
		public VendGroup()
		{
			VendorTableVendGroup = new HashSet<VendorTable>();
		}
 
		[Key]
		public Guid VendGroupGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string VendGroupId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("VendGroupCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("VendGroupBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("VendGroup")]
		public ICollection<VendorTable> VendorTableVendGroup { get; set; }
		#endregion Collection
	}
}
