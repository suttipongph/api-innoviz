using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class ExchangeRate : DateEffectiveBaseEntity
	{
		public ExchangeRate()
		{
		}
 
		[Key]
		public Guid ExchangeRateGUID { get; set; }
		public Guid CurrencyGUID { get; set; }
		public Guid? HomeCurrencyGUID { get; set; }
		[Column(TypeName = "decimal")]
		public decimal Rate { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("ExchangeRateCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("ExchangeRateBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("CurrencyGUID")]
		[InverseProperty("ExchangeRateCurrency")]
		public Currency Currency { get; set; }
		[ForeignKey("HomeCurrencyGUID")]
		[InverseProperty("ExchangeRateHomeCurrency")]
		public Currency HomeCurrency { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
