using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class CAReqBuyerCreditOutstanding : CompanyBaseEntity {
		[Key]
		public Guid CAReqBuyerCreditOutstandingGUID { get; set; }
		[Column(TypeName = "Decimal(18,2)")]
		public decimal ApprovedCreditLimitLine { get; set; }
		[Column(TypeName = "Decimal(18,2)")]
		public decimal ARBalance { get; set; }
		public Guid? AssignmentMethodGUID { get; set; }
		public Guid? BillingResponsibleByGUID { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public Guid? CreditAppLineGUID { get; set; }
		public Guid? CreditAppRequestTableGUID { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public int LineNum { get; set; }
		[Column(TypeName = "Decimal(5,2)")]
		public decimal MaxPurchasePct { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
		public int ProductType { get; set; }
		[StringLength(100)]
		public string Status { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CAReqBuyerCreditOutstandingCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CAReqBuyerCreditOutstandingBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("AssignmentMethodGUID")]
		[InverseProperty("CAReqBuyerCreditOutstandingAssignmentMethod")]
		public AssignmentMethod AssignmentMethod { get; set; }
		[ForeignKey("BillingResponsibleByGUID")]
		[InverseProperty("CAReqBuyerCreditOutstandingBillingResponsibleBy")]
		public BillingResponsibleBy BillingResponsibleBy { get; set; }
		[ForeignKey("BuyerTableGUID")]
		[InverseProperty("CAReqBuyerCreditOutstandingBuyerTable")]
		public BuyerTable BuyerTable { get; set; }
		[ForeignKey("CreditAppLineGUID")]
		[InverseProperty("CAReqBuyerCreditOutstandingCreditAppLine")]
		public CreditAppLine CreditAppLine { get; set; }
		[ForeignKey("CreditAppRequestTableGUID")]
		[InverseProperty("CAReqBuyerCreditOutstandingCreditAppRequestTable")]
		public CreditAppRequestTable CreditAppRequestTable { get; set; }
		[ForeignKey("CreditAppTableGUID")]
		[InverseProperty("CAReqBuyerCreditOutstandingCreditAppTable")]
		public CreditAppTable CreditAppTable { get; set; }
		#endregion ForeignKey
	}
}
