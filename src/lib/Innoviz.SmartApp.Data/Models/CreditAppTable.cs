using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CreditAppTable : CompanyBaseEntity
	{
		public CreditAppTable()
		{
			BusinessCollateralAgmTableCreditAppTable = new HashSet<BusinessCollateralAgmTable>();
			CAReqBuyerCreditOutstandingCreditAppTable = new HashSet<CAReqBuyerCreditOutstanding>();
			CAReqCreditOutStandingCreditAppTable = new HashSet<CAReqCreditOutStanding>();
			CAReqRetentionOutstandingCreditAppTable = new HashSet<CAReqRetentionOutstanding>();
			CreditAppLineCreditAppTable = new HashSet<CreditAppLine>();
			CreditAppRequestTableRefCreditAppTable = new HashSet<CreditAppRequestTable>();
			CreditAppTransCreditAppTable = new HashSet<CreditAppTrans>();
			CustomerRefundTableCreditAppTable = new HashSet<CustomerRefundTable>();
			CustTransCreditAppTable = new HashSet<CustTrans>();
			FreeTextInvoiceTableCreditAppTable = new HashSet<FreeTextInvoiceTable>();
			IntercompanyInvoiceTableCreditAppTable = new HashSet<IntercompanyInvoiceTable>();
			InvoiceTableCreditAppTable = new HashSet<InvoiceTable>();
			MainAgreementTableCreditAppTable = new HashSet<MainAgreementTable>();
			PaymentHistoryCreditAppTable = new HashSet<PaymentHistory>();
			ProcessTransCreditAppTable = new HashSet<ProcessTrans>();
			PurchaseTableCreditAppTable = new HashSet<PurchaseTable>();
			ReceiptTempTableCreditAppTable = new HashSet<ReceiptTempTable>();
			RetentionTransCreditAppTable = new HashSet<RetentionTrans>();
			VendorPaymentTransCreditAppTable = new HashSet<VendorPaymentTrans>();
			VerificationTableCreditAppTable = new HashSet<VerificationTable>();
			WithdrawalTableCreditAppTable = new HashSet<WithdrawalTable>();
		}
 
		[Key]
		public Guid CreditAppTableGUID { get; set; }
		public Guid? ApplicationTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ApprovedCreditLimit { get; set; }
		[Column(TypeName = "date")]
		public DateTime ApprovedDate { get; set; }
		public Guid? BankAccountControlGUID { get; set; }
		public Guid? BillingAddressGUID { get; set; }
		public Guid? BillingContactPersonGUID { get; set; }
		[StringLength(250)]
		public string CACondition { get; set; }
		public Guid? ConsortiumTableGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string CreditAppId { get; set; }
		public int CreditLimitExpiration { get; set; }
		[StringLength(250)]
		public string CreditLimitRemark { get; set; }
		public Guid CreditLimitTypeGUID { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal CreditRequestFeePct { get; set; }
		public Guid? CreditTermGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public Guid? DocumentReasonGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime? ExpectedAgreementSigningDate { get; set; }
		[Column(TypeName = "date")]
		public DateTime ExpiryDate { get; set; }
		public Guid? ExtensionServiceFeeCondTemplateGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime? InactiveDate { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal InterestAdjustment { get; set; }
		public Guid InterestTypeGUID { get; set; }
		public Guid? InvoiceAddressGUID { get; set; }
		public Guid? MailingReceipAddressGUID { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal MaxPurchasePct { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal MaxRetentionAmount { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal MaxRetentionPct { get; set; }
		public Guid? PDCBankGUID { get; set; }
		public Guid ProductSubTypeGUID { get; set; }
		public int ProductType { get; set; }
		public int PurchaseFeeCalculateBase { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal PurchaseFeePct { get; set; }
		public Guid? ReceiptAddressGUID { get; set; }
		public Guid? ReceiptContactPersonGUID { get; set; }
		public Guid RefCreditAppRequestTableGUID { get; set; }
		public Guid? RegisteredAddressGUID { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SalesAvgPerMonth { get; set; }
		[Column(TypeName = "date")]
		public DateTime StartDate { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal TotalInterestPct { get; set; }
		public Guid? AssignmentMethodGUID { get; set; }
		[StringLength(100)]
		public string PurchaseWithdrawalCondition { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CreditAppTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CreditAppTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("ApplicationTableGUID")]
		[InverseProperty("CreditAppTableApplicationTable")]
		public ApplicationTable ApplicationTable { get; set; }
		[ForeignKey("AssignmentMethodGUID")]
		[InverseProperty("CreditAppTableAssignmentMethod")]
		public AssignmentMethod AssignmentMethod { get; set; }
		[ForeignKey("BankAccountControlGUID")]
		[InverseProperty("CreditAppTableBankAccountControl")]
		public CustBank BankAccountControl { get; set; }
		[ForeignKey("BillingAddressGUID")]
		[InverseProperty("CreditAppTableBillingAddress")]
		public AddressTrans BillingAddress { get; set; }
		[ForeignKey("BillingContactPersonGUID")]
		[InverseProperty("CreditAppTableBillingContactPerson")]
		public ContactPersonTrans BillingContactPerson { get; set; }
		[ForeignKey("ConsortiumTableGUID")]
		[InverseProperty("CreditAppTableConsortiumTable")]
		public ConsortiumTable ConsortiumTable { get; set; }
		[ForeignKey("CreditLimitTypeGUID")]
		[InverseProperty("CreditAppTableCreditLimitType")]
		public CreditLimitType CreditLimitType { get; set; }
		[ForeignKey("CreditTermGUID")]
		[InverseProperty("CreditAppTableCreditTerm")]
		public CreditTerm CreditTerm { get; set; }
		[ForeignKey("Dimension1GUID")]
		[InverseProperty("CreditAppTableDimension1")]
		public LedgerDimension LedgerDimension1 { get; set; }
		[ForeignKey("Dimension2GUID")]
		[InverseProperty("CreditAppTableDimension2")]
		public LedgerDimension LedgerDimension2 { get; set; }
		[ForeignKey("Dimension3GUID")]
		[InverseProperty("CreditAppTableDimension3")]
		public LedgerDimension LedgerDimension3 { get; set; }
		[ForeignKey("Dimension4GUID")]
		[InverseProperty("CreditAppTableDimension4")]
		public LedgerDimension LedgerDimension4 { get; set; }
		[ForeignKey("Dimension5GUID")]
		[InverseProperty("CreditAppTableDimension5")]
		public LedgerDimension LedgerDimension5 { get; set; }
		[ForeignKey("DocumentReasonGUID")]
		[InverseProperty("CreditAppTableDocumentReason")]
		public DocumentReason DocumentReason { get; set; }
		[ForeignKey("ExtensionServiceFeeCondTemplateGUID")]
		[InverseProperty("CreditAppTableExtensionServiceFeeCondTemplate")]
		public ServiceFeeCondTemplateTable ExtensionServiceFeeCondTemplate { get; set; }
		[ForeignKey("InterestTypeGUID")]
		[InverseProperty("CreditAppTableInterestType")]
		public InterestType InterestType { get; set; }
		[ForeignKey("InvoiceAddressGUID")]
		[InverseProperty("CreditAppTableInvoiceAddress")]
		public AddressTrans InvoiceAddress { get; set; }
		[ForeignKey("MailingReceipAddressGUID")]
		[InverseProperty("CreditAppTableMailingReceipAddress")]
		public AddressTrans MailingReceipAddress { get; set; }
		[ForeignKey("PDCBankGUID")]
		[InverseProperty("CreditAppTablePDCBank")]
		public CustBank PDCBank { get; set; }
		[ForeignKey("ProductSubTypeGUID")]
		[InverseProperty("CreditAppTableProductSubType")]
		public ProductSubType ProductSubType { get; set; }
		[ForeignKey("ReceiptAddressGUID")]
		[InverseProperty("CreditAppTableReceiptAddress")]
		public AddressTrans ReceiptAddress { get; set; }
		[ForeignKey("ReceiptContactPersonGUID")]
		[InverseProperty("CreditAppTableReceiptContactPerson")]
		public ContactPersonTrans ReceiptContactPerson { get; set; }
		[ForeignKey("RefCreditAppRequestTableGUID")]
		[InverseProperty("CreditAppTableRefCreditAppRequestTable")]
		public CreditAppRequestTable RefCreditAppRequestTable { get; set; }
		[ForeignKey("RegisteredAddressGUID")]
		[InverseProperty("CreditAppTableRegisteredAddress")]
		public AddressTrans RegisteredAddress { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("CreditAppTable")]
		public ICollection<BusinessCollateralAgmTable> BusinessCollateralAgmTableCreditAppTable { get; set; }
		[InverseProperty("CreditAppTable")]
		public ICollection<CAReqBuyerCreditOutstanding> CAReqBuyerCreditOutstandingCreditAppTable { get; set; }
		[InverseProperty("CreditAppTable")]
		public ICollection<CAReqCreditOutStanding> CAReqCreditOutStandingCreditAppTable { get; set; }
		[InverseProperty("CreditAppTable")]
		public ICollection<CAReqRetentionOutstanding> CAReqRetentionOutstandingCreditAppTable { get; set; }
		[InverseProperty("CreditAppTable")]
		public ICollection<CreditAppLine> CreditAppLineCreditAppTable { get; set; }
		[InverseProperty("CreditAppTable")]
		public ICollection<CustomerRefundTable> CustomerRefundTableCreditAppTable { get; set; }
		[InverseProperty("CreditAppTable")]
		public ICollection<CreditAppTrans> CreditAppTransCreditAppTable { get; set; }
		[InverseProperty("CreditAppTable")]
		public ICollection<CustTrans> CustTransCreditAppTable { get; set; }
		[InverseProperty("CreditAppTable")]
		public ICollection<FreeTextInvoiceTable> FreeTextInvoiceTableCreditAppTable { get; set; }
		[InverseProperty("CreditAppTable")]
		public ICollection<IntercompanyInvoiceTable> IntercompanyInvoiceTableCreditAppTable { get; set; }
		[InverseProperty("CreditAppTable")]
		public ICollection<InvoiceTable> InvoiceTableCreditAppTable { get; set; }
		[InverseProperty("CreditAppTable")]
		public ICollection<MainAgreementTable> MainAgreementTableCreditAppTable { get; set; }
		[InverseProperty("CreditAppTable")]
		public ICollection<PaymentHistory> PaymentHistoryCreditAppTable { get; set; }
		[InverseProperty("CreditAppTable")]
		public ICollection<ProcessTrans> ProcessTransCreditAppTable { get; set; }
		[InverseProperty("CreditAppTable")]
		public ICollection<PurchaseTable> PurchaseTableCreditAppTable { get; set; }
		[InverseProperty("CreditAppTable")]
		public ICollection<ReceiptTempTable> ReceiptTempTableCreditAppTable { get; set; }
		[InverseProperty("CreditAppTable")]
		public ICollection<RetentionTrans> RetentionTransCreditAppTable { get; set; }
		[InverseProperty("CreditAppTable")]
		public ICollection<VendorPaymentTrans> VendorPaymentTransCreditAppTable { get; set; }
		[InverseProperty("CreditAppTable")]
		public ICollection<VerificationTable> VerificationTableCreditAppTable { get; set; }
		[InverseProperty("CreditAppTable")]
		public ICollection<WithdrawalTable> WithdrawalTableCreditAppTable { get; set; }
		[InverseProperty("RefCreditAppTable")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTableRefCreditAppTable { get; set; }
		#endregion Collection
	}
}
