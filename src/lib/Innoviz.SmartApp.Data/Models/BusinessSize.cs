using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class BusinessSize : CompanyBaseEntity
	{
		public BusinessSize()
		{
			BuyerTableBusinessSize = new HashSet<BuyerTable>();
			CustomerTableBusinessSize = new HashSet<CustomerTable>();
		}
 
		[Key]
		public Guid BusinessSizeGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string BusinessSizeId { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("BusinessSizeCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("BusinessSizeBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("BusinessSize")]
		public ICollection<BuyerTable> BuyerTableBusinessSize { get; set; }
		[InverseProperty("BusinessSize")]
		public ICollection<CustomerTable> CustomerTableBusinessSize { get; set; }
		#endregion Collection
	}
}
