﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
    public class MigrationTable : BaseEntity
    {
        [Key]
        public Guid MigrationTableGUID { get; set; }

        [Required]
        [StringLength(100)]
        public string GroupName { get; set; }

        [Required]
        public int GroupOrder { get; set; }

        [Required]
        [StringLength(100)]
        public string MigrateName { get; set; }

        [Required]
        public int MigrateOrder { get; set; }

        [StringLength(20)]
        public string MigrateResult { get; set; }

        [Required]
        [StringLength(100)]
        public string PackageName { get; set; }
    }
}
