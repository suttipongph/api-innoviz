using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class BusinessCollateralAgmLine : CompanyBaseEntity
	{
		public BusinessCollateralAgmLine()
		{
			CustBusinessCollateralBusinessCollateralAgmLine = new HashSet<CustBusinessCollateral>();
		}
 
		[Key]
		public Guid BusinessCollateralAgmLineGUID { get; set; }
		[StringLength(20)]
		public string AccountNumber { get; set; }

		public Guid? BankGroupGUID { get; set; }
		public Guid? BankTypeGUID { get; set; }
		public Guid BusinessCollateralAgmTableGUID { get; set; }
		public Guid? BusinessCollateralStatusGUID { get; set; }
		public Guid BusinessCollateralSubTypeGUID { get; set; }
		public Guid BusinessCollateralTypeGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal BusinessCollateralValue { get; set; }
		[StringLength(100)]
		public string BuyerName { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		[StringLength(13)]
		public string BuyerTaxIdentificationId { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CapitalValuation { get; set; }
		[StringLength(100)]
		public string ChassisNumber { get; set; }
		public Guid CreditAppReqBusinessCollateralGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime? DateOfValuation { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal GuaranteeAmount { get; set; }
		[StringLength(100)]
		public string Lessee { get; set; }
		[StringLength(100)]
		public string Lessor { get; set; }
		public int LineNum { get; set; }
		[StringLength(100)]
		public string MachineNumber { get; set; }
		[StringLength(100)]
		public string MachineRegisteredStatus { get; set; }
		[StringLength(100)]
		public string Ownership { get; set; }
		public int PreferentialCreditorNumber { get; set; }
		[StringLength(100)]
		public string ProjectName { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Quantity { get; set; }
		[Column(TypeName = "date")]
		public DateTime? RefAgreementDate { get; set; }
		[StringLength(20)]
		public string RefAgreementId { get; set; }
		[StringLength(100)]
		public string RegisteredPlace { get; set; }
		[StringLength(100)]
		public string RegistrationPlateNumber { get; set; }
		[StringLength(100)]
		public string TitleDeedDistrict { get; set; }
		[StringLength(100)]
		public string TitleDeedNumber { get; set; }
		[StringLength(100)]
		public string TitleDeedProvince { get; set; }
		[StringLength(100)]
		public string TitleDeedSubDistrict { get; set; }
		[StringLength(20)]
		public string Unit { get; set; }
		[StringLength(100)]
		public string ValuationCommittee { get; set; }
		[StringLength(1000)]
		public string Product { get; set; }
		public Guid? OriginalBusinessCollateralAgreementLineGUID { get; set; }
		public Guid? OriginalBusinessCollateralAgreementTableGUID { get; set; }
		public bool Cancelled { get; set; }
		[StringLength(100)]
		public string AttachmentText { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("BusinessCollateralAgmLineCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("BusinessCollateralAgmLineBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BankGroupGUID")]
		[InverseProperty("BusinessCollateralAgmLineBankGroup")]
		public BankGroup BankGroup { get; set; }
		[ForeignKey("BankTypeGUID")]
		[InverseProperty("BusinessCollateralAgmLineBankType")]
		public BankType BankType { get; set; }
		[ForeignKey("BusinessCollateralAgmTableGUID")]
		[InverseProperty("BusinessCollateralAgmLineBusinessCollateralAgmTable")]
		public BusinessCollateralAgmTable BusinessCollateralAgmTable { get; set; }
		[ForeignKey("BusinessCollateralSubTypeGUID")]
		[InverseProperty("BusinessCollateralAgmLineBusinessCollateralSubType")]
		public BusinessCollateralSubType BusinessCollateralSubType { get; set; }
		[ForeignKey("BusinessCollateralTypeGUID")]
		[InverseProperty("BusinessCollateralAgmLineBusinessCollateralType")]
		public BusinessCollateralType BusinessCollateralType { get; set; }
		[ForeignKey("BuyerTableGUID")]
		[InverseProperty("BusinessCollateralAgmLineBuyerTable")]
		public BuyerTable BuyerTable { get; set; }
		[ForeignKey("CreditAppReqBusinessCollateralGUID")]
		[InverseProperty("BusinessCollateralAgmLineCreditAppReqBusinessCollateral")]
		public CreditAppReqBusinessCollateral CreditAppReqBusinessCollateral { get; set; }
		[ForeignKey("BusinessCollateralStatusGUID")]
		[InverseProperty("BusinessCollateralAgmLineBusinessCollateralStatus")]
		public BusinessCollateralStatus BusinessCollateralStatus { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("BusinessCollateralAgmLine")]
		public ICollection<CustBusinessCollateral> CustBusinessCollateralBusinessCollateralAgmLine { get; set; }
		#endregion Collection
	}
}
