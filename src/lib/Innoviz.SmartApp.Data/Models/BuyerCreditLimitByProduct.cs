using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class BuyerCreditLimitByProduct : CompanyBaseEntity
	{
		public BuyerCreditLimitByProduct()
		{
		}
 
		[Key]
		public Guid BuyerCreditLimitByProductGUID { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CreditLimit { get; set; }
		public int ProductType { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("BuyerCreditLimitByProductCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("BuyerCreditLimitByProductBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BuyerTableGUID")]
		[InverseProperty("BuyerCreditLimitByProductBuyerTable")]
		public BuyerTable BuyerTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
