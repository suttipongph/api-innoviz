using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class InterestTypeValue : DateEffectiveBaseEntity
	{
		public InterestTypeValue()
		{
		}
 
		[Key]
		public Guid InterestTypeValueGUID { get; set; }
		public Guid InterestTypeGUID { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal Value { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("InterestTypeValueCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("InterestTypeValueBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("InterestTypeGUID")]
		[InverseProperty("InterestTypeValueInterestType")]
		public InterestType InterestType { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
