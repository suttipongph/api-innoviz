﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Innoviz.SmartApp.Data.Models
{
    public class SysLabelReport_CUSTOM
    {
        [Key]
        public Guid SysLabelReport_CUSTOMGUID { get; set; }
        [StringLength(20)]
        public string LabelType { get; set; }
        [StringLength(100)]
        public string LabelId { get; set; }
        [StringLength(20)]
        public string LanguageId { get; set; }
        [StringLength(250)]
        public string Label { get; set; }
    }
}