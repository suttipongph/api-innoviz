using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class BillingResponsibleBy : CompanyBaseEntity
	{
		public BillingResponsibleBy()
		{
			CAReqBuyerCreditOutstandingBillingResponsibleBy = new HashSet<CAReqBuyerCreditOutstanding>();
			CreditAppLineBillingResponsibleBy = new HashSet<CreditAppLine>();
			CreditAppRequestLineBillingResponsibleBy = new HashSet<CreditAppRequestLine>();
		}
 
		[Key]
		public Guid BillingResponsibleByGUID { get; set; }
		public int BillingBy { get; set; }
		[Required]
		[StringLength(20)]
		public string BillingResponsibleById { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("BillingResponsibleByCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("BillingResponsibleByBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("BillingResponsibleBy")]
		public ICollection<CAReqBuyerCreditOutstanding> CAReqBuyerCreditOutstandingBillingResponsibleBy { get; set; }
		[InverseProperty("BillingResponsibleBy")]
		public ICollection<CreditAppLine> CreditAppLineBillingResponsibleBy { get; set; }
		[InverseProperty("BillingResponsibleBy")]
		public ICollection<CreditAppRequestLine> CreditAppRequestLineBillingResponsibleBy { get; set; }
		#endregion Collection
	}
}
