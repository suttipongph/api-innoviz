﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_FinancialStatementTrans
	{
		[Key]
		[Column(Order = 1)]
		[StringLength(50)]
		public string FinancialStatementTransGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Amount { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Key]
		[Column(Order = 4)]
		public int Ordering { get; set; }
		[Key]
		[Column(Order = 2)]
		[StringLength(50)]
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		[Key]
		[Column(Order = 3)]
		public int Year { get; set; }
		[Key]
		[Column(Order = 5)]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		[StringLength(50)]
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		[StringLength(50)]
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		public string OwnerBusinessUnitGUID { get; set; }
		public int MigrateStatus { get; set; }
		public int MigrateSource { get; set; }
	}
}
