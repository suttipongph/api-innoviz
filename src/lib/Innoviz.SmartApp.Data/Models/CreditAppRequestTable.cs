using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CreditAppRequestTable : CompanyBaseEntity
	{
		public CreditAppRequestTable()
		{
			BusinessCollateralAgmTableCreditAppRequestTable = new HashSet<BusinessCollateralAgmTable>();
			CAReqAssignmentOutstandingCreditAppRequestTable = new HashSet<CAReqAssignmentOutstanding>();
			CAReqBuyerCreditOutstandingCreditAppRequestTable = new HashSet<CAReqBuyerCreditOutstanding>();
			CAReqCreditOutStandingCreditAppRequestTable = new HashSet<CAReqCreditOutStanding>();
			CAReqRetentionOutstandingCreditAppRequestTable = new HashSet<CAReqRetentionOutstanding>();
			CreditAppReqAssignmentCreditAppRequestTable = new HashSet<CreditAppReqAssignment>();
			CreditAppReqBusinessCollateralCreditAppRequestTable = new HashSet<CreditAppReqBusinessCollateral>();
			CreditAppRequestLineCreditAppRequestTable = new HashSet<CreditAppRequestLine>();
			CreditAppTableRefCreditAppRequestTable = new HashSet<CreditAppTable>();
			CustBusinessCollateralCreditAppRequestTable = new HashSet<CustBusinessCollateral>();
			MainAgreementTableCreditAppRequestTable = new HashSet<MainAgreementTable>();
			AssignmentAgreementTableRefCreditAppRequestTable = new HashSet<AssignmentAgreementTable>();
		}
 
		[Key]
		public Guid CreditAppRequestTableGUID { get; set; }
		public Guid? ApplicationTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ApprovedCreditLimitRequest { get; set; }
		[Column(TypeName = "date")]
		public DateTime? ApprovedDate { get; set; }
		[StringLength(500)]
		public string ApproverComment { get; set; }
		public Guid? BankAccountControlGUID { get; set; }
		public Guid? BillingAddressGUID { get; set; }
		public Guid? BillingContactPersonGUID { get; set; }
		public Guid? BlacklistStatusGUID { get; set; }
		[StringLength(250)]
		public string CACondition { get; set; }
		public Guid? ConsortiumTableGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string CreditAppRequestId { get; set; }
		public int CreditAppRequestType { get; set; }
		[StringLength(500)]
		public string CreditComment { get; set; }
		public int CreditLimitExpiration { get; set; }
		[StringLength(250)]
		public string CreditLimitRemark { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CreditLimitRequest { get; set; }
		public Guid CreditLimitTypeGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CreditRequestFeeAmount { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal CreditRequestFeePct { get; set; }
		public Guid? CreditScoringGUID { get; set; }
		public Guid? CreditTermGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CustomerAllBuyerOutstanding { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CustomerCreditLimit { get; set; }
		public Guid CustomerTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public Guid? DocumentReasonGUID { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime? ExpiryDate { get; set; }
		public Guid? ExtensionServiceFeeCondTemplateGUID { get; set; }
		[StringLength(100)]
		public string FinancialCheckedBy { get; set; }
		[Column(TypeName = "date")]
		public DateTime? FinancialCreditCheckedDate { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal InterestAdjustment { get; set; }
		public Guid InterestTypeGUID { get; set; }
		public Guid? InvoiceAddressGUID { get; set; }
		public Guid? KYCGUID { get; set; }
		public Guid? MailingReceiptAddressGUID { get; set; }
		[StringLength(500)]
		public string MarketingComment { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal MaxPurchasePct { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal MaxRetentionAmount { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal MaxRetentionPct { get; set; }
		[StringLength(100)]
		public string NCBCheckedBy { get; set; }
		[Column(TypeName = "date")]
		public DateTime? NCBCheckedDate { get; set; }
		public Guid? PDCBankGUID { get; set; }
		public int ProcessInstanceId { get; set; }
		public Guid ProductSubTypeGUID { get; set; }
		public int ProductType { get; set; }
		public int PurchaseFeeCalculateBase { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal PurchaseFeePct { get; set; }
		public Guid? ReceiptAddressGUID { get; set; }
		public Guid? ReceiptContactPersonGUID { get; set; }
		public Guid? RefCreditAppTableGUID { get; set; }
		public Guid RegisteredAddressGUID { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		[Column(TypeName = "date")]
		public DateTime RequestDate { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SalesAvgPerMonth { get; set; }
		[StringLength(100)]
		public string SigningCondition { get; set; }
		[Column(TypeName = "date")]
		public DateTime? StartDate { get; set; }
		public Guid? AssignmentMethodGUID { get; set; }
		[StringLength(100)]
		public string PurchaseWithdrawalCondition { get; set; }

 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CreditAppRequestTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CreditAppRequestTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("ApplicationTableGUID")]
		[InverseProperty("CreditAppRequestTableApplicationTable")]
		public ApplicationTable ApplicationTable { get; set; }
		[ForeignKey("AssignmentMethodGUID")]
		[InverseProperty("CreditAppRequestTableAssignmentMethod")]
		public AssignmentMethod AssignmentMethod { get; set; }
		[ForeignKey("BankAccountControlGUID")]
		[InverseProperty("CreditAppRequestTableBankAccountControl")]
		public CustBank BankAccountControl { get; set; }
		[ForeignKey("BillingAddressGUID")]
		[InverseProperty("CreditAppRequestTableBillingAddress")]
		public AddressTrans BillingAddress { get; set; }
		[ForeignKey("BillingContactPersonGUID")]
		[InverseProperty("CreditAppRequestTableBillingContactPerson")]
		public ContactPersonTrans BillingContactPerson { get; set; }
		[ForeignKey("BlacklistStatusGUID")]
		[InverseProperty("CreditAppRequestTableBlacklistStatus")]
		public BlacklistStatus BlacklistStatus { get; set; }
		[ForeignKey("ConsortiumTableGUID")]
		[InverseProperty("CreditAppRequestTableConsortiumTable")]
		public ConsortiumTable ConsortiumTable { get; set; }
		[ForeignKey("CreditLimitTypeGUID")]
		[InverseProperty("CreditAppRequestTableCreditLimitType")]
		public CreditLimitType CreditLimitType { get; set; }
		[ForeignKey("CreditScoringGUID")]
		[InverseProperty("CreditAppRequestTableCreditScoring")]
		public CreditScoring CreditScoring { get; set; }
		[ForeignKey("CreditTermGUID")]
		[InverseProperty("CreditAppRequestTableCreditTerm")]
		public CreditTerm CreditTerm { get; set; }
		[ForeignKey("Dimension1GUID")]
		[InverseProperty("CreditAppRequestTableDimension1")]
		public LedgerDimension LedgerDimension1 { get; set; }
		[ForeignKey("Dimension2GUID")]
		[InverseProperty("CreditAppRequestTableDimension2")]
		public LedgerDimension LedgerDimension2 { get; set; }
		[ForeignKey("Dimension3GUID")]
		[InverseProperty("CreditAppRequestTableDimension3")]
		public LedgerDimension LedgerDimension3 { get; set; }
		[ForeignKey("Dimension4GUID")]
		[InverseProperty("CreditAppRequestTableDimension4")]
		public LedgerDimension LedgerDimension4 { get; set; }
		[ForeignKey("Dimension5GUID")]
		[InverseProperty("CreditAppRequestTableDimension5")]
		public LedgerDimension LedgerDimension5 { get; set; }
		[ForeignKey("DocumentReasonGUID")]
		[InverseProperty("CreditAppRequestTableDocumentReason")]
		public DocumentReason DocumentReason { get; set; }
		[ForeignKey("DocumentStatusGUID")]
		[InverseProperty("CreditAppRequestTableDocumentStatus")]
		public DocumentStatus DocumentStatus { get; set; }
		[ForeignKey("ExtensionServiceFeeCondTemplateGUID")]
		[InverseProperty("CreditAppRequestTableExtensionServiceFeeCondTemplate")]
		public ServiceFeeCondTemplateTable ExtensionServiceFeeCondTemplate { get; set; }
		[ForeignKey("InterestTypeGUID")]
		[InverseProperty("CreditAppRequestTableInterestType")]
		public InterestType InterestType { get; set; }
		[ForeignKey("InvoiceAddressGUID")]
		[InverseProperty("CreditAppRequestTableInvoiceAddress")]
		public AddressTrans InvoiceAddress { get; set; }
		[ForeignKey("KYCGUID")]
		[InverseProperty("CreditAppRequestTableKYCSetup")]
		public KYCSetup KYCSetup { get; set; }
		[ForeignKey("MailingReceiptAddressGUID")]
		[InverseProperty("CreditAppRequestTableMailingReceiptAddress")]
		public AddressTrans MailingReceiptAddress { get; set; }
		[ForeignKey("PDCBankGUID")]
		[InverseProperty("CreditAppRequestTablePDCBank")]
		public CustBank PDCBank { get; set; }
		[ForeignKey("ProductSubTypeGUID")]
		[InverseProperty("CreditAppRequestTableProductSubType")]
		public ProductSubType ProductSubType { get; set; }
		[ForeignKey("ReceiptAddressGUID")]
		[InverseProperty("CreditAppRequestTableReceiptAddress")]
		public AddressTrans ReceiptAddress { get; set; }
		[ForeignKey("ReceiptContactPersonGUID")]
		[InverseProperty("CreditAppRequestTableReceiptContactPerson")]
		public ContactPersonTrans ReceiptContactPerson { get; set; }
		[ForeignKey("RefCreditAppTableGUID")]
		[InverseProperty("CreditAppRequestTableRefCreditAppTable")]
		public CreditAppTable RefCreditAppTable { get; set; }
		[ForeignKey("RegisteredAddressGUID")]
		[InverseProperty("CreditAppRequestTableRegisteredAddress")]
		public AddressTrans RegisteredAddress { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("CreditAppRequestTable")]
		public ICollection<BusinessCollateralAgmTable> BusinessCollateralAgmTableCreditAppRequestTable { get; set; }
		[InverseProperty("CreditAppRequestTable")]
		public ICollection<CAReqAssignmentOutstanding> CAReqAssignmentOutstandingCreditAppRequestTable { get; set; }
		[InverseProperty("CreditAppRequestTable")]
		public ICollection<CAReqBuyerCreditOutstanding> CAReqBuyerCreditOutstandingCreditAppRequestTable { get; set; }
		[InverseProperty("CreditAppRequestTable")]
		public ICollection<CAReqCreditOutStanding> CAReqCreditOutStandingCreditAppRequestTable { get; set; }
		[InverseProperty("CreditAppRequestTable")]
		public ICollection<CAReqRetentionOutstanding> CAReqRetentionOutstandingCreditAppRequestTable { get; set; }
		[InverseProperty("CreditAppRequestTable")]
		public ICollection<CreditAppReqAssignment> CreditAppReqAssignmentCreditAppRequestTable { get; set; }
		[InverseProperty("CreditAppRequestTable")]
		public ICollection<CreditAppReqBusinessCollateral> CreditAppReqBusinessCollateralCreditAppRequestTable { get; set; }
		[InverseProperty("CreditAppRequestTable")]
		public ICollection<CreditAppRequestLine> CreditAppRequestLineCreditAppRequestTable { get; set; }
		[InverseProperty("CreditAppRequestTable")]
		public ICollection<CustBusinessCollateral> CustBusinessCollateralCreditAppRequestTable { get; set; }
		[InverseProperty("CreditAppRequestTable")]
		public ICollection<MainAgreementTable> MainAgreementTableCreditAppRequestTable { get; set; }
		[InverseProperty("RefCreditAppRequestTable")]
		public ICollection<CreditAppTable> CreditAppTableRefCreditAppRequestTable { get; set; }
		[InverseProperty("RefCreditAppRequestTable")]
		public ICollection<AssignmentAgreementTable> AssignmentAgreementTableRefCreditAppRequestTable { get; set; }
		#endregion Collection
	}
}
