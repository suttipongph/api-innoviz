using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class GuarantorAgreementLine : CompanyBaseEntity
	{
		public GuarantorAgreementLine()
		{
			GuarantorAgreementLineAffiliateGuarantorAgreementLine = new HashSet<GuarantorAgreementLineAffiliate>();
		}
 
		[Key]
		public Guid GuarantorAgreementLineGUID { get; set; }
		public int Age { get; set; }
		[Column(TypeName = "date")]
		public DateTime? DateOfBirth { get; set; }
		[Column(TypeName = "date")]
		public DateTime? DateOfIssue { get; set; }
		[StringLength(250)]
		public string GuaranteeLand { get; set; }
		public Guid GuarantorAgreementTableGUID { get; set; }
		public Guid GuarantorTransGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Name { get; set; }
		public Guid? NationalityGUID { get; set; }
		[StringLength(100)]
		public string OperatedBy { get; set; }
		public int Ordering { get; set; }
		[StringLength(20)]
		public string PassportId { get; set; }
		[StringLength(250)]
		public string PrimaryAddress1 { get; set; }
		[StringLength(250)]
		public string PrimaryAddress2 { get; set; }
		public Guid? RaceGUID { get; set; }
		[StringLength(13)]
		public string TaxId { get; set; }
		[StringLength(20)]
		public string WorkPermitId { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal MaximumGuaranteeAmount { get; set; }
		public Guid? CustomerTableGUID { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ApprovedCreditLimit { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("GuarantorAgreementLineCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("GuarantorAgreementLineBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("GuarantorAgreementTableGUID")]
		[InverseProperty("GuarantorAgreementLineGuarantorAgreementTable")]
		public GuarantorAgreementTable GuarantorAgreementTable { get; set; }
		[ForeignKey("GuarantorTransGUID")]
		[InverseProperty("GuarantorAgreementLineGuarantorTrans")]
		public GuarantorTrans GuarantorTrans { get; set; }
		[ForeignKey("NationalityGUID")]
		[InverseProperty("GuarantorAgreementLineNationality")]
		public Nationality Nationality { get; set; }
		[ForeignKey("RaceGUID")]
		[InverseProperty("GuarantorAgreementLineRace")]
		public Race Race { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("GuarantorAgreementLine")]
		public ICollection<GuarantorAgreementLineAffiliate> GuarantorAgreementLineAffiliateGuarantorAgreementLine { get; set; }
		#endregion Collection
		
	}
}
