using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class ApplicationTable : CompanyBaseEntity
	{
		public ApplicationTable()
		{
			CreditAppRequestTableApplicationTable = new HashSet<CreditAppRequestTable>();
			CreditAppTableApplicationTable = new HashSet<CreditAppTable>();
		}
 
		[Key]
		public Guid ApplicationTableGUID { get; set; }
		public Guid AgreementTypeGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime? ApplicationDate { get; set; }
		[Required]
		[StringLength(20)]
		public string ApplicationId { get; set; }
		public Guid? BillingAddressGUID { get; set; }
		public int CalculationType { get; set; }
		public Guid? CreditResultGUID { get; set; }
		[StringLength(100)]
		public string CreditResultNotes { get; set; }
		public Guid? CurrencyGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid? DeliveryAddressGUID { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public Guid? DocumentReasonGUID { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime? ExpectedExecuteDate { get; set; }
		[Column(TypeName = "date")]
		public DateTime? ExpirationDate { get; set; }
		[StringLength(250)]
		public string FlexInfo1 { get; set; }
		[StringLength(250)]
		public string FlexInfo2 { get; set; }
		public Guid? FlexSetupGUID1 { get; set; }
		public Guid? FlexSetupGUID2 { get; set; }
		[Column(TypeName = "date")]
		public DateTime? FollowupDate { get; set; }
		public Guid? IntroducedByGUID { get; set; }
		public Guid? InvoiceAddressGUID { get; set; }
		public Guid? LanguageGUID { get; set; }
		public Guid? LeaseSubTypeGUID { get; set; }
		public Guid LeaseTypeGUID { get; set; }
		public Guid? MailingInvoiceAddressGUID { get; set; }
		public Guid? MailingReceiptAddressGUID { get; set; }
		public Guid? NCBAccountStatusGUID { get; set; }
		[StringLength(100)]
		public string NCBNotes { get; set; }
		public Guid? OriginalAgreementGUID { get; set; }
		public Guid PaymentFrequencyGUID { get; set; }
		public int ProcessInstanceId { get; set; }
		public Guid? PropertyAddressGUID { get; set; }
		public Guid? ProspectTableGUID { get; set; }
		[StringLength(250)]
		public string ReasonRemark { get; set; }
		public Guid? ReceiptAddressGUID { get; set; }
		public int RefAgreementExtension { get; set; }
		public Guid? RegisterAddressGUID { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		public Guid? ResponsibleBy { get; set; }
		public bool SalesLeaseBack { get; set; }
		public Guid? WorkingAddressGUID { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("ApplicationTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("ApplicationTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("AgreementTypeGUID")]
		[InverseProperty("ApplicationTableAgreementType")]
		public AgreementType AgreementType { get; set; }
		[ForeignKey("BillingAddressGUID")]
		[InverseProperty("ApplicationTableBillingAddress")]
		public AddressTrans BillingAddress { get; set; }
		[ForeignKey("CurrencyGUID")]
		[InverseProperty("ApplicationTableCurrency")]
		public Currency Currency { get; set; }
		[ForeignKey("CustomerTableGUID")]
		[InverseProperty("ApplicationTableCustomerTable")]
		public CustomerTable CustomerTable { get; set; }
		[ForeignKey("DeliveryAddressGUID")]
		[InverseProperty("ApplicationTableDeliveryAddress")]
		public AddressTrans DeliveryAddress { get; set; }
		[ForeignKey("Dimension1GUID")]
		[InverseProperty("ApplicationTableDimension1")]
		public LedgerDimension LedgerDimension1 { get; set; }
		[ForeignKey("Dimension2GUID")]
		[InverseProperty("ApplicationTableDimension2")]
		public LedgerDimension LedgerDimension2 { get; set; }
		[ForeignKey("Dimension3GUID")]
		[InverseProperty("ApplicationTableDimension3")]
		public LedgerDimension LedgerDimension3 { get; set; }
		[ForeignKey("Dimension4GUID")]
		[InverseProperty("ApplicationTableDimension4")]
		public LedgerDimension LedgerDimension4 { get; set; }
		[ForeignKey("Dimension5GUID")]
		[InverseProperty("ApplicationTableDimension5")]
		public LedgerDimension LedgerDimension5 { get; set; }
		[ForeignKey("DocumentReasonGUID")]
		[InverseProperty("ApplicationTableDocumentReason")]
		public DocumentReason DocumentReason { get; set; }
		[ForeignKey("DocumentStatusGUID")]
		[InverseProperty("ApplicationTableDocumentStatus")]
		public DocumentStatus DocumentStatus { get; set; }
		[ForeignKey("IntroducedByGUID")]
		[InverseProperty("ApplicationTableIntroducedBy")]
		public IntroducedBy IntroducedBy { get; set; }
		[ForeignKey("InvoiceAddressGUID")]
		[InverseProperty("ApplicationTableInvoiceAddress")]
		public AddressTrans InvoiceAddress { get; set; }
		[ForeignKey("LanguageGUID")]
		[InverseProperty("ApplicationTableLanguage")]
		public Language Language { get; set; }
		[ForeignKey("LeaseTypeGUID")]
		[InverseProperty("ApplicationTableLeaseType")]
		public LeaseType LeaseType { get; set; }
		[ForeignKey("MailingInvoiceAddressGUID")]
		[InverseProperty("ApplicationTableMailingInvoiceAddress")]
		public AddressTrans MailingInvoiceAddress { get; set; }
		[ForeignKey("MailingReceiptAddressGUID")]
		[InverseProperty("ApplicationTableMailingReceiptAddress")]
		public AddressTrans MailingReceiptAddress { get; set; }
		[ForeignKey("NCBAccountStatusGUID")]
		[InverseProperty("ApplicationTableNCBAccountStatus")]
		public NCBAccountStatus NCBAccountStatus { get; set; }
		[ForeignKey("PaymentFrequencyGUID")]
		[InverseProperty("ApplicationTablePaymentFrequency")]
		public PaymentFrequency PaymentFrequency { get; set; }
		[ForeignKey("PropertyAddressGUID")]
		[InverseProperty("ApplicationTablePropertyAddress")]
		public AddressTrans PropertyAddress { get; set; }
		[ForeignKey("ReceiptAddressGUID")]
		[InverseProperty("ApplicationTableReceiptAddress")]
		public AddressTrans ReceiptAddress { get; set; }
		[ForeignKey("RegisterAddressGUID")]
		[InverseProperty("ApplicationTableRegisterAddress")]
		public AddressTrans RegisterAddress { get; set; }
		[ForeignKey("WorkingAddressGUID")]
		[InverseProperty("ApplicationTableWorkingAddress")]
		public AddressTrans WorkingAddress { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("ApplicationTable")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTableApplicationTable { get; set; }
		[InverseProperty("ApplicationTable")]
		public ICollection<CreditAppTable> CreditAppTableApplicationTable { get; set; }
		#endregion Collection
	}
}
