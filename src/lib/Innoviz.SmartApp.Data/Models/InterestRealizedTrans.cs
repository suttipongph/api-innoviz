using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class InterestRealizedTrans : CompanyBaseEntity
	{
		public InterestRealizedTrans()
		{
		}
 
		[Key]
		public Guid InterestRealizedTransGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal AccInterestAmount { get; set; }
		[Column(TypeName = "date")]
		public DateTime AccountingDate { get; set; }
		public bool Accrued { get; set; }
		[StringLength(20)]
		public string DocumentId { get; set; }
		[Column(TypeName = "date")]
		public DateTime EndDate { get; set; }
		public int InterestDay { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InterestPerDay { get; set; }
		public int LineNum { get; set; }
		public int ProductType { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		[Column(TypeName = "date")]
		public DateTime StartDate { get; set; }
		public bool Cancelled { get; set; }
		public Guid? RefProcessTransGUID { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("InterestRealizedTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("InterestRealizedTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("Dimension1GUID")]
		[InverseProperty("InterestRealizedTransDimension1")]
		public LedgerDimension LedgerDimension1 { get; set; }
		[ForeignKey("Dimension2GUID")]
		[InverseProperty("InterestRealizedTransDimension2")]
		public LedgerDimension LedgerDimension2 { get; set; }
		[ForeignKey("Dimension3GUID")]
		[InverseProperty("InterestRealizedTransDimension3")]
		public LedgerDimension LedgerDimension3 { get; set; }
		[ForeignKey("Dimension4GUID")]
		[InverseProperty("InterestRealizedTransDimension4")]
		public LedgerDimension LedgerDimension4 { get; set; }
		[ForeignKey("Dimension5GUID")]
		[InverseProperty("InterestRealizedTransDimension5")]
		public LedgerDimension LedgerDimension5 { get; set; }
		#endregion ForeignKey

		#region Collection
		#endregion Collection
	}
}
