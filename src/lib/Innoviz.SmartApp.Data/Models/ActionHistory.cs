﻿using Innoviz.SmartApp.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Innoviz.SmartApp.Data.Models
{
    public class ActionHistory : CompanyBaseEntity
    {
        [Key]
        public Guid ActionHistoryGUID { get; set; }
        [StringLength(20)]
        public string SerialNo { get; set; }
        [Required]
        [StringLength(100)]
        public string WorkflowName { get; set; }
        [Required]
        [StringLength(100)]
        public string ActivityName { get; set; }
        [Required]
        [StringLength(100)]
        public string ActionName { get; set; }
        [StringLength(250)]
        public string Comment { get; set; }
        public Guid RefGUID { get; set; }

        #region ForeignKey
        [ForeignKey("CompanyGUID")]
        [InverseProperty("ActionHistoryCompany")]
        public Company Company { get; set; }
        [ForeignKey("OwnerBusinessUnitGUID")]
        [InverseProperty("ActionHistoryBusinessUnit")]
        public BusinessUnit BusinessUnit { get; set; }
        #endregion ForeignKey
    }
}
