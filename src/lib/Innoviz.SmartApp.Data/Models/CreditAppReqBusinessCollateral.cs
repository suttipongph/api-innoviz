using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CreditAppReqBusinessCollateral : CompanyBaseEntity
	{
		public CreditAppReqBusinessCollateral()
		{
			BusinessCollateralAgmLineCreditAppReqBusinessCollateral = new HashSet<BusinessCollateralAgmLine>();
		}
 
		[Key]
		public Guid CreditAppReqBusinessCollateralGUID { get; set; }
		[StringLength(20)]
		public string AccountNumber { get; set; }
		[StringLength(1000)]
		public string AttachmentRemark { get; set; }
		public Guid? BankGroupGUID { get; set; }
		public Guid? BankTypeGUID { get; set; }
		public Guid BusinessCollateralSubTypeGUID { get; set; }
		public Guid BusinessCollateralTypeGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal BusinessCollateralValue { get; set; }
		[StringLength(100)]
		public string BuyerName { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		[StringLength(13)]
		public string BuyerTaxIdentificationId { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CapitalValuation { get; set; }
		[StringLength(100)]
		public string ChassisNumber { get; set; }
		public Guid CreditAppRequestTableGUID { get; set; }
		public Guid? CustBusinessCollateralGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime? DateOfValuation { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal DBDRegistrationAmount { get; set; }
		[Column(TypeName = "date")]
		public DateTime? DBDRegistrationDate { get; set; }
		[StringLength(100)]
		public string DBDRegistrationDescription { get; set; }
		[StringLength(20)]
		public string DBDRegistrationId { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal GuaranteeAmount { get; set; }
		public bool IsNew { get; set; }
		[StringLength(100)]
		public string Lessee { get; set; }
		[StringLength(100)]
		public string Lessor { get; set; }
		[StringLength(100)]
		public string MachineNumber { get; set; }
		[StringLength(100)]
		public string MachineRegisteredStatus { get; set; }
		[StringLength(100)]
		public string Ownership { get; set; }
		public int PreferentialCreditorNumber { get; set; }
		[StringLength(100)]
		public string ProjectName { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Quantity { get; set; }
		[Column(TypeName = "date")]
		public DateTime? RefAgreementDate { get; set; }
		[StringLength(20)]
		public string RefAgreementId { get; set; }
		[StringLength(100)]
		public string RegisteredPlace { get; set; }
		[StringLength(100)]
		public string RegistrationPlateNumber { get; set; }
		[StringLength(100)]
		public string TitleDeedDistrict { get; set; }
		[StringLength(100)]
		public string TitleDeedNumber { get; set; }
		[StringLength(100)]
		public string TitleDeedProvince { get; set; }
		[StringLength(100)]
		public string TitleDeedSubDistrict { get; set; }
		[StringLength(20)]
		public string Unit { get; set; }
		[StringLength(100)]
		public string ValuationCommittee { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CreditAppReqBusinessCollateralCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CreditAppReqBusinessCollateralBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BankGroupGUID")]
		[InverseProperty("CreditAppReqBusinessCollateralBankGroup")]
		public BankGroup BankGroup { get; set; }
		[ForeignKey("BankTypeGUID")]
		[InverseProperty("CreditAppReqBusinessCollateralBankType")]
		public BankType BankType { get; set; }
		[ForeignKey("BusinessCollateralSubTypeGUID")]
		[InverseProperty("CreditAppReqBusinessCollateralBusinessCollateralSubType")]
		public BusinessCollateralSubType BusinessCollateralSubType { get; set; }
		[ForeignKey("BusinessCollateralTypeGUID")]
		[InverseProperty("CreditAppReqBusinessCollateralBusinessCollateralType")]
		public BusinessCollateralType BusinessCollateralType { get; set; }
		[ForeignKey("BuyerTableGUID")]
		[InverseProperty("CreditAppReqBusinessCollateralBuyerTable")]
		public BuyerTable BuyerTable { get; set; }
		[ForeignKey("CreditAppRequestTableGUID")]
		[InverseProperty("CreditAppReqBusinessCollateralCreditAppRequestTable")]
		public CreditAppRequestTable CreditAppRequestTable { get; set; }
		[ForeignKey("CustBusinessCollateralGUID")]
		[InverseProperty("CreditAppReqBusinessCollateralCustBusinessCollateral")]
		public CustBusinessCollateral CustBusinessCollateral { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("CreditAppReqBusinessCollateral")]
		public ICollection<BusinessCollateralAgmLine> BusinessCollateralAgmLineCreditAppReqBusinessCollateral { get; set; }
		#endregion Collection
	}
}
