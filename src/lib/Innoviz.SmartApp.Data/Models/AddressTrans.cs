using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class AddressTrans : CompanyBaseEntity
	{
		public AddressTrans()
		{
			ApplicationTableBillingAddress = new HashSet<ApplicationTable>();
			ApplicationTableDeliveryAddress = new HashSet<ApplicationTable>();
			ApplicationTableInvoiceAddress = new HashSet<ApplicationTable>();
			ApplicationTableMailingInvoiceAddress = new HashSet<ApplicationTable>();
			ApplicationTableMailingReceiptAddress = new HashSet<ApplicationTable>();
			ApplicationTablePropertyAddress = new HashSet<ApplicationTable>();
			ApplicationTableReceiptAddress = new HashSet<ApplicationTable>();
			ApplicationTableRegisterAddress = new HashSet<ApplicationTable>();
			ApplicationTableWorkingAddress = new HashSet<ApplicationTable>();
			CreditAppLineBillingAddress = new HashSet<CreditAppLine>();
			CreditAppLineInvoiceAddress = new HashSet<CreditAppLine>();
			CreditAppLineMailingReceiptAddress = new HashSet<CreditAppLine>();
			CreditAppLineReceiptAddress = new HashSet<CreditAppLine>();
			CreditAppRequestLineBillingAddress = new HashSet<CreditAppRequestLine>();
			CreditAppRequestLineInvoiceAddress = new HashSet<CreditAppRequestLine>();
			CreditAppRequestLineMailingReceiptAddress = new HashSet<CreditAppRequestLine>();
			CreditAppRequestLineReceiptAddress = new HashSet<CreditAppRequestLine>();
			CreditAppRequestTableBillingAddress = new HashSet<CreditAppRequestTable>();
			CreditAppRequestTableInvoiceAddress = new HashSet<CreditAppRequestTable>();
			CreditAppRequestTableMailingReceiptAddress = new HashSet<CreditAppRequestTable>();
			CreditAppRequestTableReceiptAddress = new HashSet<CreditAppRequestTable>();
			CreditAppRequestTableRegisteredAddress = new HashSet<CreditAppRequestTable>();
			CreditAppTableBillingAddress = new HashSet<CreditAppTable>();
			CreditAppTableInvoiceAddress = new HashSet<CreditAppTable>();
			CreditAppTableMailingReceipAddress = new HashSet<CreditAppTable>();
			CreditAppTableReceiptAddress = new HashSet<CreditAppTable>();
			CreditAppTableRegisteredAddress = new HashSet<CreditAppTable>();
			FreeTextInvoiceTableInvoiceAddress = new HashSet<FreeTextInvoiceTable>();
			FreeTextInvoiceTableMailingInvoiceAddress = new HashSet<FreeTextInvoiceTable>();
		}

		[Key]
		public Guid AddressTransGUID { get; set; }
		[Required]
		[StringLength(250)]
		public string Address1 { get; set; }
		[StringLength(250)]
		public string Address2 { get; set; }
		public Guid AddressCountryGUID { get; set; }
		public Guid AddressDistrictGUID { get; set; }
		public Guid AddressPostalCodeGUID { get; set; }
		public Guid AddressProvinceGUID { get; set; }
		public Guid AddressSubDistrictGUID { get; set; }
		[StringLength(500)]
		public string AltAddress { get; set; }
		public bool CurrentAddress { get; set; }
		public bool IsTax { get; set; }
		[Required]
		[StringLength(100)]
		public string Name { get; set; }
		public Guid? OwnershipGUID { get; set; }
		public bool Primary { get; set; }
		public Guid? PropertyTypeGUID { get; set; }
		public Guid? RefGUID { get; set; }
		public int RefType { get; set; }
		[StringLength(5)]
		public string TaxBranchId { get; set; }
		[StringLength(100)]
		public string TaxBranchName { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("AddressTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("AddressTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("AddressCountryGUID")]
		[InverseProperty("AddressTransAddressCountry")]
		public AddressCountry AddressCountry { get; set; }
		[ForeignKey("AddressDistrictGUID")]
		[InverseProperty("AddressTransAddressDistrict")]
		public AddressDistrict AddressDistrict { get; set; }
		[ForeignKey("AddressPostalCodeGUID")]
		[InverseProperty("AddressTransAddressPostalCode")]
		public AddressPostalCode AddressPostalCode { get; set; }
		[ForeignKey("AddressProvinceGUID")]
		[InverseProperty("AddressTransAddressProvince")]
		public AddressProvince AddressProvince { get; set; }
		[ForeignKey("AddressSubDistrictGUID")]
		[InverseProperty("AddressTransAddressSubDistrict")]
		public AddressSubDistrict AddressSubDistrict { get; set; }
		[ForeignKey("OwnershipGUID")]
		[InverseProperty("AddressTransOwnership")]
		public Ownership Ownership { get; set; }
		[ForeignKey("PropertyTypeGUID")]
		[InverseProperty("AddressTransPropertyType")]
		public PropertyType PropertyType { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("BillingAddress")]
		public ICollection<ApplicationTable> ApplicationTableBillingAddress { get; set; }
		[InverseProperty("DeliveryAddress")]
		public ICollection<ApplicationTable> ApplicationTableDeliveryAddress { get; set; }
		[InverseProperty("InvoiceAddress")]
		public ICollection<ApplicationTable> ApplicationTableInvoiceAddress { get; set; }
		[InverseProperty("MailingInvoiceAddress")]
		public ICollection<ApplicationTable> ApplicationTableMailingInvoiceAddress { get; set; }
		[InverseProperty("MailingReceiptAddress")]
		public ICollection<ApplicationTable> ApplicationTableMailingReceiptAddress { get; set; }
		[InverseProperty("PropertyAddress")]
		public ICollection<ApplicationTable> ApplicationTablePropertyAddress { get; set; }
		[InverseProperty("ReceiptAddress")]
		public ICollection<ApplicationTable> ApplicationTableReceiptAddress { get; set; }
		[InverseProperty("RegisterAddress")]
		public ICollection<ApplicationTable> ApplicationTableRegisterAddress { get; set; }
		[InverseProperty("WorkingAddress")]
		public ICollection<ApplicationTable> ApplicationTableWorkingAddress { get; set; }
		[InverseProperty("BillingAddress")]
		public ICollection<CreditAppLine> CreditAppLineBillingAddress { get; set; }
		[InverseProperty("BillingAddress")]
		public ICollection<CreditAppRequestLine> CreditAppRequestLineBillingAddress { get; set; }
		[InverseProperty("BillingAddress")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTableBillingAddress { get; set; }
		[InverseProperty("BillingAddress")]
		public ICollection<CreditAppTable> CreditAppTableBillingAddress { get; set; }
		[InverseProperty("InvoiceAddress")]
		public ICollection<CreditAppLine> CreditAppLineInvoiceAddress { get; set; }
		[InverseProperty("InvoiceAddress")]
		public ICollection<CreditAppRequestLine> CreditAppRequestLineInvoiceAddress { get; set; }
		[InverseProperty("InvoiceAddress")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTableInvoiceAddress { get; set; }
		[InverseProperty("InvoiceAddress")]
		public ICollection<CreditAppTable> CreditAppTableInvoiceAddress { get; set; }
		[InverseProperty("MailingReceipAddress")]
		public ICollection<CreditAppTable> CreditAppTableMailingReceipAddress { get; set; }
		[InverseProperty("MailingReceiptAddress")]
		public ICollection<CreditAppLine> CreditAppLineMailingReceiptAddress { get; set; }
		[InverseProperty("MailingReceiptAddress")]
		public ICollection<CreditAppRequestLine> CreditAppRequestLineMailingReceiptAddress { get; set; }
		[InverseProperty("MailingReceiptAddress")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTableMailingReceiptAddress { get; set; }
		[InverseProperty("ReceiptAddress")]
		public ICollection<CreditAppLine> CreditAppLineReceiptAddress { get; set; }
		[InverseProperty("ReceiptAddress")]
		public ICollection<CreditAppRequestLine> CreditAppRequestLineReceiptAddress { get; set; }
		[InverseProperty("ReceiptAddress")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTableReceiptAddress { get; set; }
		[InverseProperty("ReceiptAddress")]
		public ICollection<CreditAppTable> CreditAppTableReceiptAddress { get; set; }
		[InverseProperty("RegisteredAddress")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTableRegisteredAddress { get; set; }
		[InverseProperty("RegisteredAddress")]
		public ICollection<CreditAppTable> CreditAppTableRegisteredAddress { get; set; }
		[InverseProperty("InvoiceAddress")]
		public ICollection<FreeTextInvoiceTable> FreeTextInvoiceTableInvoiceAddress { get; set; }
		[InverseProperty("MailingInvoiceAddress")]
		public ICollection<FreeTextInvoiceTable> FreeTextInvoiceTableMailingInvoiceAddress { get; set; }
		#endregion Collection
	}
}
