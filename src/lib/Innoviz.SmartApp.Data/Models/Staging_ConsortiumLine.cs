
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_ConsortiumLine
	{
		[Key]
		[Column(Order = 1)]
		[StringLength(50)]
		public string ConsortiumLineGUID { get; set; }
		[StringLength(500)]
		public string Address { get; set; }
		[StringLength(50)]
		public string AuthorizedPersonTypeGUID { get; set; }
		[Key]
		[Column(Order = 4)]
		[StringLength(50)]
		public string ConsortiumTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string CustomerName { get; set; }
		public bool IsMain { get; set; }
		[StringLength(100)]
		public string OperatedBy { get; set; }
		[Key]
		[Column(Order = 2)]
		public int Ordering { get; set; }
		[StringLength(100)]
		public string Position { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal ProportionOfShareholderPct { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }

		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }

		[Key]
		[Column(Order = 3)]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }

	}
}


