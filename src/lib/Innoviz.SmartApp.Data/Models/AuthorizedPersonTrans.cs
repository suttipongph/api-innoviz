using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class AuthorizedPersonTrans : CompanyBaseEntity
	{
		public AuthorizedPersonTrans()
		{
			AgreementTableInfoAuthorizedPersonTransBuyer1 = new HashSet<AgreementTableInfo>();
			AgreementTableInfoAuthorizedPersonTransBuyer2 = new HashSet<AgreementTableInfo>();
			AgreementTableInfoAuthorizedPersonTransBuyer3 = new HashSet<AgreementTableInfo>();
			AgreementTableInfoAuthorizedPersonTransCustomer1 = new HashSet<AgreementTableInfo>();
			AgreementTableInfoAuthorizedPersonTransCustomer2 = new HashSet<AgreementTableInfo>();
			AgreementTableInfoAuthorizedPersonTransCustomer3 = new HashSet<AgreementTableInfo>();
		}
 
		[Key]
		public Guid AuthorizedPersonTransGUID { get; set; }
		public Guid? AuthorizedPersonTypeGUID { get; set; }
		public bool InActive { get; set; }
		[StringLength(100)]
		public string NCBCheckedBy { get; set; }
		[Column(TypeName = "date")]
		public DateTime? NCBCheckedDate { get; set; }
		public int Ordering { get; set; }
		public Guid? RefAuthorizedPersonTransGUID { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		public Guid RelatedPersonTableGUID { get; set; }
		public bool Replace { get; set; }
		public Guid? ReplacedAuthorizedPersonTransGUID  { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("AuthorizedPersonTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("AuthorizedPersonTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("AuthorizedPersonTypeGUID")]
		[InverseProperty("AuthorizedPersonTransAuthorizedPersonType")]
		public AuthorizedPersonType AuthorizedPersonType { get; set; }
		[ForeignKey("RelatedPersonTableGUID")]
		[InverseProperty("AuthorizedPersonTransRelatedPersonTable")]
		public RelatedPersonTable RelatedPersonTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("AuthorizedPersonTransBuyer1")]
		public ICollection<AgreementTableInfo> AgreementTableInfoAuthorizedPersonTransBuyer1 { get; set; }
		[InverseProperty("AuthorizedPersonTransBuyer2")]
		public ICollection<AgreementTableInfo> AgreementTableInfoAuthorizedPersonTransBuyer2 { get; set; }
		[InverseProperty("AuthorizedPersonTransBuyer3")]
		public ICollection<AgreementTableInfo> AgreementTableInfoAuthorizedPersonTransBuyer3 { get; set; }
		[InverseProperty("AuthorizedPersonTransCustomer1")]
		public ICollection<AgreementTableInfo> AgreementTableInfoAuthorizedPersonTransCustomer1 { get; set; }
		[InverseProperty("AuthorizedPersonTransCustomer2")]
		public ICollection<AgreementTableInfo> AgreementTableInfoAuthorizedPersonTransCustomer2 { get; set; }
		[InverseProperty("AuthorizedPersonTransCustomer3")]
		public ICollection<AgreementTableInfo> AgreementTableInfoAuthorizedPersonTransCustomer3 { get; set; }
		#endregion Collection
	}
}
