using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class InvoiceLine : BranchCompanyBaseEntity
	{
		public InvoiceLine()
		{
		}
 
		[Key]
		public Guid InvoiceLineGUID { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public Guid InvoiceRevenueTypeGUID { get; set; }
		public Guid InvoiceTableGUID { get; set; }
		[StringLength(250)]
		public string InvoiceText { get; set; }
		public int LineNum { get; set; }
		public Guid? ProdUnitGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Qty { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TaxAmount { get; set; }
		public Guid? TaxTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TotalAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TotalAmountBeforeTax { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal UnitPrice { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal WHTAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal WHTBaseAmount { get; set; }
		public Guid? WithholdingTaxTableGUID { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("InvoiceLineCompany")]
		public Company Company { get; set; }
		[ForeignKey("BranchGUID")]
		[InverseProperty("InvoiceLineBranch")]
		public Branch Branch { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("InvoiceLineBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("Dimension1GUID")]
		[InverseProperty("InvoiceLineDimension1")]
		public LedgerDimension LedgerDimension1 { get; set; }
		[ForeignKey("Dimension2GUID")]
		[InverseProperty("InvoiceLineDimension2")]
		public LedgerDimension LedgerDimension2 { get; set; }
		[ForeignKey("Dimension3GUID")]
		[InverseProperty("InvoiceLineDimension3")]
		public LedgerDimension LedgerDimension3 { get; set; }
		[ForeignKey("Dimension4GUID")]
		[InverseProperty("InvoiceLineDimension4")]
		public LedgerDimension LedgerDimension4 { get; set; }
		[ForeignKey("Dimension5GUID")]
		[InverseProperty("InvoiceLineDimension5")]
		public LedgerDimension LedgerDimension5 { get; set; }
		[ForeignKey("InvoiceRevenueTypeGUID")]
		[InverseProperty("InvoiceLineInvoiceRevenueType")]
		public InvoiceRevenueType InvoiceRevenueType { get; set; }
		[ForeignKey("InvoiceTableGUID")]
		[InverseProperty("InvoiceLineInvoiceTable")]
		public InvoiceTable InvoiceTable { get; set; }
		[ForeignKey("ProdUnitGUID")]
		[InverseProperty("InvoiceLineProdUnit")]
		public ProdUnit ProdUnit { get; set; }
		[ForeignKey("TaxTableGUID")]
		[InverseProperty("InvoiceLineTaxTable")]
		public TaxTable TaxTable { get; set; }
		[ForeignKey("WithholdingTaxTableGUID")]
		[InverseProperty("InvoiceLineWithholdingTaxTable")]
		public WithholdingTaxTable WithholdingTaxTable { get; set; }
		#endregion ForeignKey

		#region Collection
		#endregion Collection
	}
}
