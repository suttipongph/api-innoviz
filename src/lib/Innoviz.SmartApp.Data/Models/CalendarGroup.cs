using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CalendarGroup : CompanyBaseEntity
	{
		public CalendarGroup()
		{
			CalendarNonWorkingDateCalendarGroup = new HashSet<CalendarNonWorkingDate>();
			CompanyParameterCompanyCalendarGroup = new HashSet<CompanyParameter>();
			CompanyParameterBOTCalendarGroup = new HashSet<CompanyParameter>();
		}
 
		[Key]
		public Guid CalendarGroupGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string CalendarGroupId { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public int WorkingDay { get; set; }

		[InverseProperty("CompanyCalendarGroup")]
		public ICollection<CompanyParameter> CompanyParameterCompanyCalendarGroup { get; set; }
		[InverseProperty("BOTCalendarGroup")]
		public ICollection<CompanyParameter> CompanyParameterBOTCalendarGroup { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CalendarGroupCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CalendarGroupBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("CalendarGroup")]
		public ICollection<CalendarNonWorkingDate> CalendarNonWorkingDateCalendarGroup { get; set; }
		#endregion Collection
	}
}
