using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class VerificationType : CompanyBaseEntity
	{
		public VerificationType()
		{
			VerificationLineVerificationType = new HashSet<VerificationLine>();
		}
 
		[Key]
		public Guid VerificationTypeGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string VerificationTypeId { get; set; }
		public int VerifyType { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("VerificationTypeCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("VerificationTypeBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("VerificationType")]
		public ICollection<VerificationLine> VerificationLineVerificationType { get; set; }
		#endregion Collection
	}
}
