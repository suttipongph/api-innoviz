using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_GuarantorTrans
	{

		[Key]
		[StringLength(50)]
		public string GuarantorTransGUID { get; set; }
		public bool Affiliate { get; set; }
		[Required]
		[StringLength(50)]
		public string GuarantorTypeGUID { get; set; }
		public bool InActive { get; set; }
		public int Ordering { get; set; }
		[StringLength(50)]
		public string RefGuarantorTransGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		[Required]
		[StringLength(50)]
		public string RelatedPersonTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal MaximumGuaranteeAmount { get; set; }

		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }
		[Required]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }
	}
}
