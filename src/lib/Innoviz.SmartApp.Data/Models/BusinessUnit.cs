using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class BusinessUnit : CompanyBaseEntity
	{
		public BusinessUnit()
		{
			ActionHistoryBusinessUnit = new HashSet<ActionHistory>();
			AddressCountryBusinessUnit = new HashSet<AddressCountry>();
			AddressDistrictBusinessUnit = new HashSet<AddressDistrict>();
			AddressPostalCodeBusinessUnit = new HashSet<AddressPostalCode>();
			AddressProvinceBusinessUnit = new HashSet<AddressProvince>();
			AddressSubDistrictBusinessUnit = new HashSet<AddressSubDistrict>();
			AddressTransBusinessUnit = new HashSet<AddressTrans>();
			AgingReportSetupBusinessUnit = new HashSet<AgingReportSetup>();
			AgreementTableInfoBusinessUnit = new HashSet<AgreementTableInfo>();
			AgreementTableInfoTextBusinessUnit = new HashSet<AgreementTableInfoText>();
			AgreementTypeBusinessUnit = new HashSet<AgreementType>();
			ApplicationTableBusinessUnit = new HashSet<ApplicationTable>();
			AssignmentAgreementLineBusinessUnit = new HashSet<AssignmentAgreementLine>();
			AssignmentAgreementSettleBusinessUnit = new HashSet<AssignmentAgreementSettle>();
			AssignmentAgreementTableBusinessUnit = new HashSet<AssignmentAgreementTable>();
			AssignmentMethodBusinessUnit = new HashSet<AssignmentMethod>();
			AuthorizedPersonTransBusinessUnit = new HashSet<AuthorizedPersonTrans>();
			AuthorizedPersonTypeBusinessUnit = new HashSet<AuthorizedPersonType>();
			BankGroupBusinessUnit = new HashSet<BankGroup>();
			BankTypeBusinessUnit = new HashSet<BankType>();
			BillingResponsibleByBusinessUnit = new HashSet<BillingResponsibleBy>();
			BlacklistStatusBusinessUnit = new HashSet<BlacklistStatus>();
			BookmarkDocumentBusinessUnit = new HashSet<BookmarkDocument>();
			BookmarkDocumentTemplateLineBusinessUnit = new HashSet<BookmarkDocumentTemplateLine>();
			BookmarkDocumentTemplateTableBusinessUnit = new HashSet<BookmarkDocumentTemplateTable>();
			BookmarkDocumentTransBusinessUnit = new HashSet<BookmarkDocumentTrans>();
			BranchBusinessUnit = new HashSet<Branch>();
			BusinessCollateralAgmLineBusinessUnit = new HashSet<BusinessCollateralAgmLine>();
			BusinessCollateralAgmTableBusinessUnit = new HashSet<BusinessCollateralAgmTable>();
			BusinessCollateralStatusBusinessUnit = new HashSet<BusinessCollateralStatus>();
			BusinessCollateralSubTypeBusinessUnit = new HashSet<BusinessCollateralSubType>();
			BusinessCollateralTypeBusinessUnit = new HashSet<BusinessCollateralType>();
			BusinessSegmentBusinessUnit = new HashSet<BusinessSegment>();
			BusinessSizeBusinessUnit = new HashSet<BusinessSize>();
			BusinessTypeBusinessUnit = new HashSet<BusinessType>();
			BuyerAgreementLineBusinessUnit = new HashSet<BuyerAgreementLine>();
			BuyerAgreementTableBusinessUnit = new HashSet<BuyerAgreementTable>();
			BuyerAgreementTransBusinessUnit = new HashSet<BuyerAgreementTrans>();
			BuyerCreditLimitByProductBusinessUnit = new HashSet<BuyerCreditLimitByProduct>();
			BuyerInvoiceTableBusinessUnit = new HashSet<BuyerInvoiceTable>();
			BuyerReceiptTableBusinessUnit = new HashSet<BuyerReceiptTable>();
			BuyerTableBusinessUnit = new HashSet<BuyerTable>();
			CalendarGroupBusinessUnit = new HashSet<CalendarGroup>();
			CalendarNonWorkingDateBusinessUnit = new HashSet<CalendarNonWorkingDate>();
			CAReqAssignmentOutstandingBusinessUnit = new HashSet<CAReqAssignmentOutstanding>();
			CAReqBuyerCreditOutstandingBusinessUnit = new HashSet<CAReqBuyerCreditOutstanding>();
			CAReqCreditOutStandingBusinessUnit = new HashSet<CAReqCreditOutStanding>();
			CAReqRetentionOutstandingBusinessUnit = new HashSet<CAReqRetentionOutstanding>();
			ChequeTableBusinessUnit = new HashSet<ChequeTable>();
			CollectionFollowUpBusinessUnit = new HashSet<CollectionFollowUp>();
			CollectionGroupBusinessUnit = new HashSet<CollectionGroup>();
			CompanyBankBusinessUnit = new HashSet<CompanyBank>();
			CompanyParameterBusinessUnit = new HashSet<CompanyParameter>();
			CompanySignatureBusinessUnit = new HashSet<CompanySignature>();
			ConsortiumLineBusinessUnit = new HashSet<ConsortiumLine>();
			ConsortiumTableBusinessUnit = new HashSet<ConsortiumTable>();
			ConsortiumTransBusinessUnit = new HashSet<ConsortiumTrans>();
			ContactPersonTransBusinessUnit = new HashSet<ContactPersonTrans>();
			ContactTransBusinessUnit = new HashSet<ContactTrans>();
			CreditAppLineBusinessUnit = new HashSet<CreditAppLine>();
			CreditAppReqAssignmentBusinessUnit = new HashSet<CreditAppReqAssignment>();
			CreditAppReqBusinessCollateralBusinessUnit = new HashSet<CreditAppReqBusinessCollateral>();
			CreditAppRequestLineBusinessUnit = new HashSet<CreditAppRequestLine>();
			CreditAppRequestLineAmendBusinessUnit = new HashSet<CreditAppRequestLineAmend>();
			CreditAppRequestTableBusinessUnit = new HashSet<CreditAppRequestTable>();
			CreditAppRequestTableAmendBusinessUnit = new HashSet<CreditAppRequestTableAmend>();
			CreditAppTableBusinessUnit = new HashSet<CreditAppTable>();
			CreditAppTransBusinessUnit = new HashSet<CreditAppTrans>();
			CreditLimitTypeBusinessUnit = new HashSet<CreditLimitType>();
			CreditScoringBusinessUnit = new HashSet<CreditScoring>();
			CreditTermBusinessUnit = new HashSet<CreditTerm>();
			CreditTypeBusinessUnit = new HashSet<CreditType>();
			CurrencyBusinessUnit = new HashSet<Currency>();
			CustBankBusinessUnit = new HashSet<CustBank>();
			CustBusinessCollateralBusinessUnit = new HashSet<CustBusinessCollateral>();
			CustGroupBusinessUnit = new HashSet<CustGroup>();
			CustomerCreditLimitByProductBusinessUnit = new HashSet<CustomerCreditLimitByProduct>();
			CustomerRefundTableBusinessUnit = new HashSet<CustomerRefundTable>();
			CustomerTableBusinessUnit = new HashSet<CustomerTable>();
			CustTransBusinessUnit = new HashSet<CustTrans>();
			CustVisitingTransBusinessUnit = new HashSet<CustVisitingTrans>();
			DepartmentBusinessUnit = new HashSet<Department>();
			DocumentConditionTemplateLineBusinessUnit = new HashSet<DocumentConditionTemplateLine>();
			DocumentConditionTemplateTableBusinessUnit = new HashSet<DocumentConditionTemplateTable>();
			DocumentConditionTransBusinessUnit = new HashSet<DocumentConditionTrans>();
			DocumentReasonBusinessUnit = new HashSet<DocumentReason>();
			DocumentReturnMethodBusinessUnit = new HashSet<DocumentReturnMethod>();
			DocumentReturnLineBusinessUnit = new HashSet<DocumentReturnLine>();
			DocumentReturnTableBusinessUnit = new HashSet<DocumentReturnTable>();
			DocumentTemplateTableBusinessUnit = new HashSet<DocumentTemplateTable>();
			DocumentTypeBusinessUnit = new HashSet<DocumentType>();
			EmployeeTable = new HashSet<EmployeeTable>();
			EmployeeTableBusinessUnit = new HashSet<EmployeeTable>();
			EmplTeamBusinessUnit = new HashSet<EmplTeam>();
			ExchangeRateBusinessUnit = new HashSet<ExchangeRate>();
			ExposureGroupBusinessUnit = new HashSet<ExposureGroup>();
			ExposureGroupByProductBusinessUnit = new HashSet<ExposureGroupByProduct>();
			FinancialCreditTransBusinessUnit = new HashSet<FinancialCreditTrans>();
			FinancialStatementTransBusinessUnit = new HashSet<FinancialStatementTrans>();
			FreeTextInvoiceTableBusinessUnit = new HashSet<FreeTextInvoiceTable>();
			GenderBusinessUnit = new HashSet<Gender>();
			GradeClassificationBusinessUnit = new HashSet<GradeClassification>();
			GuarantorAgreementLineBusinessUnit = new HashSet<GuarantorAgreementLine>();
			GuarantorAgreementLineAffiliateBusinessUnit = new HashSet<GuarantorAgreementLineAffiliate>();
			GuarantorAgreementTableBusinessUnit = new HashSet<GuarantorAgreementTable>();
			GuarantorTransBusinessUnit = new HashSet<GuarantorTrans>();
			GuarantorTypeBusinessUnit = new HashSet<GuarantorType>();
			IntercompanyBusinessUnit = new HashSet<Intercompany>();
			IntercompanyInvoiceAdjustmentBusinessUnit = new HashSet<IntercompanyInvoiceAdjustment>();
			IntercompanyInvoiceSettlementBusinessUnit = new HashSet<IntercompanyInvoiceSettlement>();
			IntercompanyInvoiceTableBusinessUnit = new HashSet<IntercompanyInvoiceTable>();
			InterestRealizedTransBusinessUnit = new HashSet<InterestRealizedTrans>();
			InterestTypeBusinessUnit = new HashSet<InterestType>();
			InterestTypeValueBusinessUnit = new HashSet<InterestTypeValue>();
			IntroducedByBusinessUnit = new HashSet<IntroducedBy>();
			InvoiceLineBusinessUnit = new HashSet<InvoiceLine>();
			InvoiceNumberSeqSetupBusinessUnit = new HashSet<InvoiceNumberSeqSetup>();
			InvoiceRevenueTypeBusinessUnit = new HashSet<InvoiceRevenueType>();
			InvoiceSettlementDetailBusinessUnit = new HashSet<InvoiceSettlementDetail>();
			InvoiceTableBusinessUnit = new HashSet<InvoiceTable>();
			InvoiceTypeBusinessUnit = new HashSet<InvoiceType>();
			JobChequeBusinessUnit = new HashSet<JobCheque>();
			JobTypeBusinessUnit = new HashSet<JobType>();
			JointVentureTransBusinessUnit = new HashSet<JointVentureTrans>();
			KYCSetupBusinessUnit = new HashSet<KYCSetup>();
			LanguageBusinessUnit = new HashSet<Language>();
			LeaseTypeBusinessUnit = new HashSet<LeaseType>();
			LedgerDimensionBusinessUnit = new HashSet<LedgerDimension>();
			LedgerFiscalPeriodBusinessUnit = new HashSet<LedgerFiscalPeriod>();
			LedgerFiscalYearBusinessUnit = new HashSet<LedgerFiscalYear>();
			LineOfBusinessBusinessUnit = new HashSet<LineOfBusiness>();
			MainAgreementTableBusinessUnit = new HashSet<MainAgreementTable>();
			MaritalStatusBusinessUnit = new HashSet<MaritalStatus>();
			MemoTransBusinessUnit = new HashSet<MemoTrans>();
			MessengerJobTableBusinessUnit = new HashSet<MessengerJobTable>();
			MessengerTableBusinessUnit = new HashSet<MessengerTable>();
			MethodOfPaymentBusinessUnit = new HashSet<MethodOfPayment>();
			NationalityBusinessUnit = new HashSet<Nationality>();
			NCBAccountStatusBusinessUnit = new HashSet<NCBAccountStatus>();
			NCBTransBusinessUnit = new HashSet<NCBTrans>();
			NumberSeqParameterBusinessUnit = new HashSet<NumberSeqParameter>();
			NumberSeqSegmentBusinessUnit = new HashSet<NumberSeqSegment>();
			NumberSeqSetupByProductTypeBusinessUnit = new HashSet<NumberSeqSetupByProductType>();
			NumberSeqTableBusinessUnit = new HashSet<NumberSeqTable>();
			OccupationBusinessUnit = new HashSet<Occupation>();
			OwnershipBusinessUnit = new HashSet<Ownership>();
			OwnerTransBusinessUnit = new HashSet<OwnerTrans>();
			ParentCompanyBusinessUnit = new HashSet<ParentCompany>();
			PaymentDetailBusinessUnit = new HashSet<PaymentDetail>();
			PaymentFrequencyBusinessUnit = new HashSet<PaymentFrequency>();
			PaymentHistoryBusinessUnit = new HashSet<PaymentHistory>();
			ProcessTransBusinessUnit = new HashSet<ProcessTrans>();
			ProductSettledTransBusinessUnit = new HashSet<ProductSettledTrans>();
			ProductSubTypeBusinessUnit = new HashSet<ProductSubType>();
			ProdUnitBusinessUnit = new HashSet<ProdUnit>();
			ProjectProgressTableBusinessUnit = new HashSet<ProjectProgressTable>();
			ProjectReferenceTransBusinessUnit = new HashSet<ProjectReferenceTrans>();
			PropertyTypeBusinessUnit = new HashSet<PropertyType>();
			PurchaseLineBusinessUnit = new HashSet<PurchaseLine>();
			PurchaseTableBusinessUnit = new HashSet<PurchaseTable>();
			RaceBusinessUnit = new HashSet<Race>();
			ReceiptLineBusinessUnit = new HashSet<ReceiptLine>();
			ReceiptTableBusinessUnit = new HashSet<ReceiptTable>();
			ReceiptTempPaymDetailBusinessUnit = new HashSet<ReceiptTempPaymDetail>();
			ReceiptTempTableBusinessUnit = new HashSet<ReceiptTempTable>();
			RegistrationTypeBusinessUnit = new HashSet<RegistrationType>();
			RelatedPersonTableBusinessUnit = new HashSet<RelatedPersonTable>();
			RetentionConditionSetupBusinessUnit = new HashSet<RetentionConditionSetup>();
			RetentionConditionTransBusinessUnit = new HashSet<RetentionConditionTrans>();
			RetentionTransBusinessUnit = new HashSet<RetentionTrans>();
			ServiceFeeConditionTransBusinessUnit = new HashSet<ServiceFeeConditionTrans>();
			ServiceFeeCondTemplateLineBusinessUnit = new HashSet<ServiceFeeCondTemplateLine>();
			ServiceFeeCondTemplateTableBusinessUnit = new HashSet<ServiceFeeCondTemplateTable>();
			ServiceFeeTransBusinessUnit = new HashSet<ServiceFeeTrans>();
			SysRoleTableBusinessUnit = new HashSet<SysRoleTable>();
			TaxInvoiceLineBusinessUnit = new HashSet<TaxInvoiceLine>();
			TaxInvoiceTableBusinessUnit = new HashSet<TaxInvoiceTable>();
			TaxReportTransBusinessUnit = new HashSet<TaxReportTrans>();
			TaxTableBusinessUnit = new HashSet<TaxTable>();
			TaxValueBusinessUnit = new HashSet<TaxValue>();
			TerritoryBusinessUnit = new HashSet<Territory>();
			VendBankBusinessUnit = new HashSet<VendBank>();
			VendGroupBusinessUnit = new HashSet<VendGroup>();
			VendorPaymentTransBusinessUnit = new HashSet<VendorPaymentTrans>();
			VendorTableBusinessUnit = new HashSet<VendorTable>();
			VerificationLineBusinessUnit = new HashSet<VerificationLine>();
			VerificationTableBusinessUnit = new HashSet<VerificationTable>();
			VerificationTransBusinessUnit = new HashSet<VerificationTrans>();
			VerificationTypeBusinessUnit = new HashSet<VerificationType>();
			WithdrawalLineBusinessUnit = new HashSet<WithdrawalLine>();
			WithdrawalTableBusinessUnit = new HashSet<WithdrawalTable>();
			WithholdingTaxGroupBusinessUnit = new HashSet<WithholdingTaxGroup>();
			WithholdingTaxTableBusinessUnit = new HashSet<WithholdingTaxTable>();
			WithholdingTaxValueBusinessUnit = new HashSet<WithholdingTaxValue>();
			StagingTableBusinessUnit = new HashSet<StagingTable>();
			StagingTableVendorInfoBusinessUnit = new HashSet<StagingTableVendorInfo>();
			StagingTransTextBusinessUnit = new HashSet<StagingTransText>();
			StagingTableIntercoInvSettleBusinessUnit = new HashSet<StagingTableIntercoInvSettle>();
		}
 
		[Key]
		public Guid BusinessUnitGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string BusinessUnitId { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public Guid? ParentBusinessUnitGUID { get; set; }
		public BusinessUnit ParentBusinessUnit { get; set; }
		public ICollection<BusinessUnit> ChildrenBusinessUnits { get; set; } = new List<BusinessUnit>();

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("BusinessUnitCompany")]
		public Company Company { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("BusinessUnit")]
		public ICollection<ActionHistory> ActionHistoryBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<AddressCountry> AddressCountryBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<AddressDistrict> AddressDistrictBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<AddressPostalCode> AddressPostalCodeBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<AddressProvince> AddressProvinceBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<AddressSubDistrict> AddressSubDistrictBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<AddressTrans> AddressTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<AgingReportSetup> AgingReportSetupBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<AgreementTableInfo> AgreementTableInfoBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<AgreementTableInfoText> AgreementTableInfoTextBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<AgreementType> AgreementTypeBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<ApplicationTable> ApplicationTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<AssignmentAgreementLine> AssignmentAgreementLineBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<AssignmentAgreementSettle> AssignmentAgreementSettleBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<AssignmentAgreementTable> AssignmentAgreementTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<AssignmentMethod> AssignmentMethodBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<AuthorizedPersonTrans> AuthorizedPersonTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<AuthorizedPersonType> AuthorizedPersonTypeBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<BankGroup> BankGroupBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<BankType> BankTypeBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<BillingResponsibleBy> BillingResponsibleByBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<BlacklistStatus> BlacklistStatusBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<BookmarkDocument> BookmarkDocumentBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<BookmarkDocumentTemplateLine> BookmarkDocumentTemplateLineBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<BookmarkDocumentTemplateTable> BookmarkDocumentTemplateTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<BookmarkDocumentTrans> BookmarkDocumentTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<Branch> BranchBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<BusinessCollateralAgmLine> BusinessCollateralAgmLineBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<BusinessCollateralAgmTable> BusinessCollateralAgmTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<BusinessCollateralStatus> BusinessCollateralStatusBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<BusinessCollateralSubType> BusinessCollateralSubTypeBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<BusinessCollateralType> BusinessCollateralTypeBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<BusinessSegment> BusinessSegmentBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<BusinessSize> BusinessSizeBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<BusinessType> BusinessTypeBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<BuyerAgreementLine> BuyerAgreementLineBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<BuyerAgreementTable> BuyerAgreementTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<BuyerAgreementTrans> BuyerAgreementTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<BuyerCreditLimitByProduct> BuyerCreditLimitByProductBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<BuyerInvoiceTable> BuyerInvoiceTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<BuyerReceiptTable> BuyerReceiptTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<BuyerTable> BuyerTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CalendarGroup> CalendarGroupBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CalendarNonWorkingDate> CalendarNonWorkingDateBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CAReqAssignmentOutstanding> CAReqAssignmentOutstandingBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CAReqBuyerCreditOutstanding> CAReqBuyerCreditOutstandingBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CAReqCreditOutStanding> CAReqCreditOutStandingBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CAReqRetentionOutstanding> CAReqRetentionOutstandingBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<ChequeTable> ChequeTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CollectionFollowUp> CollectionFollowUpBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CollectionGroup> CollectionGroupBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CompanyBank> CompanyBankBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CompanyParameter> CompanyParameterBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CompanySignature> CompanySignatureBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<ConsortiumLine> ConsortiumLineBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<ConsortiumTable> ConsortiumTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<ConsortiumTrans> ConsortiumTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<ContactPersonTrans> ContactPersonTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<ContactTrans> ContactTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CreditAppLine> CreditAppLineBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CreditAppReqAssignment> CreditAppReqAssignmentBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CreditAppReqBusinessCollateral> CreditAppReqBusinessCollateralBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CreditAppRequestLine> CreditAppRequestLineBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CreditAppRequestLineAmend> CreditAppRequestLineAmendBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CreditAppRequestTableAmend> CreditAppRequestTableAmendBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CreditAppTable> CreditAppTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CreditAppTrans> CreditAppTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CreditLimitType> CreditLimitTypeBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CreditScoring> CreditScoringBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CreditTerm> CreditTermBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CreditType> CreditTypeBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<Currency> CurrencyBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CustBank> CustBankBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CustBusinessCollateral> CustBusinessCollateralBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CustGroup> CustGroupBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CustomerCreditLimitByProduct> CustomerCreditLimitByProductBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CustomerRefundTable> CustomerRefundTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CustomerTable> CustomerTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CustTrans> CustTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<CustVisitingTrans> CustVisitingTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<Department> DepartmentBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<DocumentConditionTemplateLine> DocumentConditionTemplateLineBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<DocumentConditionTemplateTable> DocumentConditionTemplateTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<DocumentConditionTrans> DocumentConditionTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<DocumentReason> DocumentReasonBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<DocumentReturnMethod> DocumentReturnMethodBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<DocumentReturnLine> DocumentReturnLineBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<DocumentReturnTable> DocumentReturnTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<DocumentTemplateTable> DocumentTemplateTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<DocumentType> DocumentTypeBusinessUnit { get; set; }
		[InverseProperty("BusinessUnitGU")]
		public ICollection<EmployeeTable> EmployeeTable { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<EmployeeTable> EmployeeTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<EmplTeam> EmplTeamBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<ExchangeRate> ExchangeRateBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<ExposureGroup> ExposureGroupBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<ExposureGroupByProduct> ExposureGroupByProductBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<FinancialCreditTrans> FinancialCreditTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<FinancialStatementTrans> FinancialStatementTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<FreeTextInvoiceTable> FreeTextInvoiceTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<Gender> GenderBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<GradeClassification> GradeClassificationBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<GuarantorAgreementLine> GuarantorAgreementLineBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<GuarantorAgreementLineAffiliate> GuarantorAgreementLineAffiliateBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<GuarantorAgreementTable> GuarantorAgreementTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<GuarantorTrans> GuarantorTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<GuarantorType> GuarantorTypeBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<Intercompany> IntercompanyBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<IntercompanyInvoiceAdjustment> IntercompanyInvoiceAdjustmentBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<IntercompanyInvoiceSettlement> IntercompanyInvoiceSettlementBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<IntercompanyInvoiceTable> IntercompanyInvoiceTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<InterestRealizedTrans> InterestRealizedTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<InterestType> InterestTypeBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<InterestTypeValue> InterestTypeValueBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<IntroducedBy> IntroducedByBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<InvoiceLine> InvoiceLineBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<InvoiceNumberSeqSetup> InvoiceNumberSeqSetupBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<InvoiceRevenueType> InvoiceRevenueTypeBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<InvoiceSettlementDetail> InvoiceSettlementDetailBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<InvoiceTable> InvoiceTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<InvoiceType> InvoiceTypeBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<JobCheque> JobChequeBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<JobType> JobTypeBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<JointVentureTrans> JointVentureTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<KYCSetup> KYCSetupBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<Language> LanguageBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<LeaseType> LeaseTypeBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<LedgerDimension> LedgerDimensionBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<LedgerFiscalPeriod> LedgerFiscalPeriodBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<LedgerFiscalYear> LedgerFiscalYearBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<LineOfBusiness> LineOfBusinessBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<MainAgreementTable> MainAgreementTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<MaritalStatus> MaritalStatusBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<MemoTrans> MemoTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<MessengerJobTable> MessengerJobTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<MessengerTable> MessengerTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<MethodOfPayment> MethodOfPaymentBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<Nationality> NationalityBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<NCBAccountStatus> NCBAccountStatusBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<NCBTrans> NCBTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<NumberSeqParameter> NumberSeqParameterBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<NumberSeqSegment> NumberSeqSegmentBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<NumberSeqSetupByProductType> NumberSeqSetupByProductTypeBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<NumberSeqTable> NumberSeqTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<Occupation> OccupationBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<Ownership> OwnershipBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<OwnerTrans> OwnerTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<ParentCompany> ParentCompanyBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<PaymentDetail> PaymentDetailBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<PaymentFrequency> PaymentFrequencyBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<PaymentHistory> PaymentHistoryBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<ProcessTrans> ProcessTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<ProductSettledTrans> ProductSettledTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<ProductSubType> ProductSubTypeBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<ProdUnit> ProdUnitBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<ProjectProgressTable> ProjectProgressTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<ProjectReferenceTrans> ProjectReferenceTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<PropertyType> PropertyTypeBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<PurchaseLine> PurchaseLineBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<PurchaseTable> PurchaseTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<Race> RaceBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<ReceiptLine> ReceiptLineBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<ReceiptTable> ReceiptTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<ReceiptTempPaymDetail> ReceiptTempPaymDetailBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<ReceiptTempTable> ReceiptTempTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<RegistrationType> RegistrationTypeBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<RelatedPersonTable> RelatedPersonTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<RetentionConditionSetup> RetentionConditionSetupBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<RetentionConditionTrans> RetentionConditionTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<RetentionTrans> RetentionTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<ServiceFeeConditionTrans> ServiceFeeConditionTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<ServiceFeeCondTemplateLine> ServiceFeeCondTemplateLineBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<ServiceFeeCondTemplateTable> ServiceFeeCondTemplateTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<ServiceFeeTrans> ServiceFeeTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<SysRoleTable> SysRoleTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<TaxInvoiceLine> TaxInvoiceLineBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<TaxInvoiceTable> TaxInvoiceTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<TaxReportTrans> TaxReportTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<TaxTable> TaxTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<TaxValue> TaxValueBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<Territory> TerritoryBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<VendBank> VendBankBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<VendGroup> VendGroupBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<VendorPaymentTrans> VendorPaymentTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<VendorTable> VendorTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<VerificationLine> VerificationLineBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<VerificationTable> VerificationTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<VerificationTrans> VerificationTransBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<VerificationType> VerificationTypeBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<WithdrawalLine> WithdrawalLineBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<WithdrawalTable> WithdrawalTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<WithholdingTaxGroup> WithholdingTaxGroupBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<WithholdingTaxTable> WithholdingTaxTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<WithholdingTaxValue> WithholdingTaxValueBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<StagingTableVendorInfo> StagingTableVendorInfoBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<StagingTable> StagingTableBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<StagingTransText> StagingTransTextBusinessUnit { get; set; }
		[InverseProperty("BusinessUnit")]
		public ICollection<StagingTableIntercoInvSettle> StagingTableIntercoInvSettleBusinessUnit { get; set; }
		#endregion Collection
	}
}
