using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class ServiceFeeCondTemplateTable : CompanyBaseEntity
	{
		public ServiceFeeCondTemplateTable()
		{
			CreditAppRequestTableExtensionServiceFeeCondTemplate = new HashSet<CreditAppRequestTable>();
			CreditAppTableExtensionServiceFeeCondTemplate = new HashSet<CreditAppTable>();
			ServiceFeeCondTemplateLineServiceFeeCondTemplateTable = new HashSet<ServiceFeeCondTemplateLine>();
		}
 
		[Key]
		public Guid ServiceFeeCondTemplateTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public int ProductType { get; set; }
		[Required]
		[StringLength(20)]
		public string ServiceFeeCondTemplateId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("ServiceFeeCondTemplateTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("ServiceFeeCondTemplateTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("ExtensionServiceFeeCondTemplate")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTableExtensionServiceFeeCondTemplate { get; set; }
		[InverseProperty("ExtensionServiceFeeCondTemplate")]
		public ICollection<CreditAppTable> CreditAppTableExtensionServiceFeeCondTemplate { get; set; }
		[InverseProperty("ServiceFeeCondTemplateTable")]
		public ICollection<ServiceFeeCondTemplateLine> ServiceFeeCondTemplateLineServiceFeeCondTemplateTable { get; set; }
		#endregion Collection
	}
}
