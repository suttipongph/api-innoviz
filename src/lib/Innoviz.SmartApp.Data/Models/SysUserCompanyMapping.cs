﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
    public partial class SysUserCompanyMapping : BaseEntity {
        
        public Guid BranchGUID { get; set; }
        public Guid CompanyGUID { get; set; }
        public Guid UserGUID { get; set; }

        [ForeignKey("BranchGUID")]
        [InverseProperty("SysUserCompanyMapping")]
        public Branch BranchGU { get; set; }
        [ForeignKey("CompanyGUID")]
        [InverseProperty("SysUserCompanyMapping")]
        public Company CompanyGU { get; set; }
        [ForeignKey("UserGUID")]
        [InverseProperty("SysUserCompanyMapping")]
        public SysUserTable UserGU { get; set; }
    }
}
