using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_InvoiceSettlementDetail
	{

		[Key]
		[StringLength(50)]
		public string InvoiceSettlementDetailGUID { get; set; }
		[StringLength(50)]
		public string AssignmentAgreementTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal BalanceAmount { get; set; }
		[StringLength(50)]
		public string BuyerAgreementTableGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string DocumentId { get; set; }
		public int InterestCalculationMethod { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InvoiceAmount { get; set; }
		public string InvoiceDueDate { get; set; }
		[StringLength(50)]
		public string InvoiceTableGUID { get; set; }
		[StringLength(50)]
		public string InvoiceTypeGUID { get; set; }
		public int ProductType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal PurchaseAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal PurchaseAmountBalance { get; set; }
		[Required]
		[StringLength(50)]
		public string RefGUID { get; set; }
		[StringLength(50)]
		public string RefReceiptTempPaymDetailGUID { get; set; }
		public int RefType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal RetentionAmountAccum { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleAssignmentAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleInvoiceAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettlePurchaseAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleReserveAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleTaxAmount { get; set; }
		public int SuspenseInvoiceType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal WHTAmount { get; set; }
		public bool WHTSlipReceivedByBuyer { get; set; }
		public bool WHTSlipReceivedByCustomer { get; set; }
		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }
		[Required]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }
	}
}
