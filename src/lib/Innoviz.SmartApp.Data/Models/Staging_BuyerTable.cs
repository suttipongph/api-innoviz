﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_BuyerTable
	{
		[Key]
		[Column(Order = 1)]
		public string BuyerTableGUID { get; set; }
		[StringLength(100)]
		public string AltName { get; set; }
		[StringLength(50)]
		public string BlacklistStatusGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string BusinessSegmentGUID { get; set; }
		[StringLength(50)]
		public string BusinessSizeGUID { get; set; }
		[StringLength(50)]
		public string BusinessTypeGUID { get; set; }
		[Key]
		[Column(Order = 2)]
		[StringLength(20)]
		public string BuyerId { get; set; }
		[Required]
		[StringLength(100)]
		public string BuyerName { get; set; }
		[StringLength(50)]
		public string CreditScoringGUID { get; set; }
		[StringLength(50)]
		public string CurrencyGUID { get; set; }
		public string DateOfEstablish { get; set; }
		public string DateOfExpiry { get; set; }
		public string DateOfIssue { get; set; }
		[StringLength(50)]
		public string DocumentStatusGUID { get; set; }
		public int FixedAssets { get; set; }
		public int IdentificationType { get; set; }
		[StringLength(100)]
		public string IssuedBy { get; set; }
		[StringLength(50)]
		public string KYCSetupGUID { get; set; }
		public int Labor { get; set; }
		[StringLength(50)]
		public string LineOfBusinessGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal PaidUpCapital { get; set; }
		[StringLength(20)]
		public string PassportId { get; set; }
		[StringLength(20)]
		public string ReferenceId { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal RegisteredCapital { get; set; }
		[StringLength(13)]
		public string TaxId { get; set; }
		[StringLength(20)]
		public string WorkPermitId { get; set; }


		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }

		[Key]
		[Column(Order = 3)]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }
	}
}

