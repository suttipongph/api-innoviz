using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class DocumentReturnTable : CompanyBaseEntity {
		public DocumentReturnTable()
		{
			DocumentReturnLineDocumentReturnTable = new HashSet<DocumentReturnLine>();
		}
		[Key]
		public Guid DocumentReturnTableGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime? ActualReturnDate { get; set; }
		[Required]
		[StringLength(500)]
		public string Address { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string ContactPersonName { get; set; }
		public int ContactTo { get; set; }
		public Guid? CustomerTableGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string DocumentReturnId { get; set; }
		public Guid DocumentReturnMethodGUID { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime? ExpectedReturnDate { get; set; }
		[StringLength(20)]
		public string PostAcknowledgementNo { get; set; }
		[StringLength(20)]
		public string PostNumber { get; set; }
		[StringLength(100)]
		public string ReceivedBy { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		public Guid RequestorGUID { get; set; }
		public Guid? ReturnByGUID { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("DocumentReturnTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("DocumentReturnTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BuyerTableGUID")]
		[InverseProperty("DocumentReturnTableBuyerTable")]
		public BuyerTable BuyerTable { get; set; }
		[ForeignKey("CustomerTableGUID")]
		[InverseProperty("DocumentReturnTableCustomerTable")]
		public CustomerTable CustomerTable { get; set; }
		[ForeignKey("DocumentReturnMethodGUID")]
		[InverseProperty("DocumentReturnTableDocumentReturnMethod")]
		public DocumentReturnMethod DocumentReturnMethod { get; set; }
		[ForeignKey("DocumentStatusGUID")]
		[InverseProperty("DocumentReturnTableDocumentStatus")]
		public DocumentStatus DocumentStatus { get; set; }
		[ForeignKey("RequestorGUID")]
		[InverseProperty("DocumentReturnTableRequestor")]
		public EmployeeTable Requestor { get; set; }
		[ForeignKey("ReturnByGUID")]
		[InverseProperty("DocumentReturnTableReturnBy")]
		public EmployeeTable ReturnBy { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("DocumentReturnTable")]
		public ICollection<DocumentReturnLine> DocumentReturnLineDocumentReturnTable { get; set; }
		#endregion Collection
	}
}
