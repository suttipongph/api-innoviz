using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class InvoiceRevenueType : CompanyBaseEntity
	{
		public InvoiceRevenueType()
		{
			FreeTextInvoiceTableInvoiceRevenueType = new HashSet<FreeTextInvoiceTable>();
			IntercompanyInvoiceTableInvoiceRevenueType = new HashSet<IntercompanyInvoiceTable>();
			InvoiceLineInvoiceRevenueType = new HashSet<InvoiceLine>();
			InvoiceRevenueTypeServiceFeeRevenueType = new HashSet<InvoiceRevenueType>();
			ServiceFeeConditionTransInvoiceRevenueType = new HashSet<ServiceFeeConditionTrans>();
			ServiceFeeCondTemplateLineInvoiceRevenueType = new HashSet<ServiceFeeCondTemplateLine>();
			ServiceFeeTransInvoiceRevenueType = new HashSet<ServiceFeeTrans>();
			TaxInvoiceLineInvoiceRevenueType = new HashSet<TaxInvoiceLine>();
			WithdrawalTableTermExtensionInvoiceRevenueType = new HashSet<WithdrawalTable>();
			CompanyParameterCreditReqInvRevenueType = new HashSet<CompanyParameter>();
			CompanyParameterFTBillingFeeInvRevenueType = new HashSet<CompanyParameter>();
			CompanyParameterFTReceiptFeeInvRevenueType = new HashSet<CompanyParameter>();
			CompanyParameterFTReserveInvRevenueType = new HashSet<CompanyParameter>();
			CompanyParameterPFTermExtensionInvRevenueType = new HashSet<CompanyParameter>();
		}
 
		[Key]
		public Guid InvoiceRevenueTypeGUID { get; set; }
		public int AssetFeeType { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal FeeAmount { get; set; }
		[Required]
		[StringLength(250)]
		public string FeeInvoiceText { get; set; }
		[StringLength(20)]
		public string FeeLedgerAccount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal FeeTaxAmount { get; set; }
		public Guid? FeeTaxGUID { get; set; }
		public Guid? FeeWHTGUID { get; set; }
		public Guid? IntercompanyTableGUID { get; set; }
		public int ProductType { get; set; }
		[Required]
		[StringLength(20)]
		public string RevenueTypeId { get; set; }
		public int ServiceFeeCategory { get; set; }
		public Guid? ServiceFeeRevenueTypeGUID { get; set; }

		[InverseProperty("CreditReqInvRevenueType")]
		public ICollection<CompanyParameter> CompanyParameterCreditReqInvRevenueType { get; set; }
		[InverseProperty("FTBillingFeeInvRevenueType")]
		public ICollection<CompanyParameter> CompanyParameterFTBillingFeeInvRevenueType { get; set; }
		[InverseProperty("FTReceiptFeeInvRevenueType")]
		public ICollection<CompanyParameter> CompanyParameterFTReceiptFeeInvRevenueType { get; set; }
		[InverseProperty("FTReserveInvRevenueType")]
		public ICollection<CompanyParameter> CompanyParameterFTReserveInvRevenueType { get; set; }
		[InverseProperty("PFTermExtensionInvRevenueType")]
		public ICollection<CompanyParameter> CompanyParameterPFTermExtensionInvRevenueType { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("InvoiceRevenueTypeCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("InvoiceRevenueTypeBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("FeeTaxGUID")]
		[InverseProperty("InvoiceRevenueTypeFeeTax")]
		public TaxTable FeeTax { get; set; }
		[ForeignKey("FeeWHTGUID")]
		[InverseProperty("InvoiceRevenueTypeFeeWHT")]
		public WithholdingTaxTable FeeWHT { get; set; }
		[ForeignKey("IntercompanyTableGUID")]
		[InverseProperty("InvoiceRevenueTypeIntercompany")]
		public Intercompany Intercompany { get; set; }
		[ForeignKey("ServiceFeeRevenueTypeGUID")]
		[InverseProperty("InvoiceRevenueTypeServiceFeeRevenueType")]
		public InvoiceRevenueType ServiceFeeRevenueType { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("InvoiceRevenueType")]
		public ICollection<FreeTextInvoiceTable> FreeTextInvoiceTableInvoiceRevenueType { get; set; }
		[InverseProperty("InvoiceRevenueType")]
		public ICollection<IntercompanyInvoiceTable> IntercompanyInvoiceTableInvoiceRevenueType { get; set; }
		[InverseProperty("InvoiceRevenueType")]
		public ICollection<InvoiceLine> InvoiceLineInvoiceRevenueType { get; set; }
		[InverseProperty("InvoiceRevenueType")]
		public ICollection<ServiceFeeConditionTrans> ServiceFeeConditionTransInvoiceRevenueType { get; set; }
		[InverseProperty("InvoiceRevenueType")]
		public ICollection<ServiceFeeCondTemplateLine> ServiceFeeCondTemplateLineInvoiceRevenueType { get; set; }
		[InverseProperty("InvoiceRevenueType")]
		public ICollection<ServiceFeeTrans> ServiceFeeTransInvoiceRevenueType { get; set; }
		[InverseProperty("InvoiceRevenueType")]
		public ICollection<TaxInvoiceLine> TaxInvoiceLineInvoiceRevenueType { get; set; }
		[InverseProperty("ServiceFeeRevenueType")]
		public ICollection<InvoiceRevenueType> InvoiceRevenueTypeServiceFeeRevenueType { get; set; }
		[InverseProperty("TermExtensionInvoiceRevenueType")]
		public ICollection<WithdrawalTable> WithdrawalTableTermExtensionInvoiceRevenueType { get; set; }
		#endregion Collection
	}
}
