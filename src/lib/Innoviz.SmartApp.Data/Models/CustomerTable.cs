using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CustomerTable : CompanyBaseEntity
	{
		public CustomerTable()
		{
			AssignmentAgreementTableCustomerTable = new HashSet<AssignmentAgreementTable>();
			ApplicationTableCustomerTable = new HashSet<ApplicationTable>();
			BuyerAgreementTableCustomerTable = new HashSet<BuyerAgreementTable>();
			BuyerReceiptTableCustomerTable = new HashSet<BuyerReceiptTable>();
			CAReqAssignmentOutstandingCustomerTable = new HashSet<CAReqAssignmentOutstanding>();
			CAReqCreditOutStandingCustomerTable = new HashSet<CAReqCreditOutStanding>();
			ChequeTableCustomerTable = new HashSet<ChequeTable>();
			CollectionFollowUpCustomerTable = new HashSet<CollectionFollowUp>();
			CreditAppReqAssignmentCustomerTable = new HashSet<CreditAppReqAssignment>();
			CreditAppTransCustomerTable = new HashSet<CreditAppTrans>();
			CustBankCustomerTable = new HashSet<CustBank>();
			CustBusinessCollateralCustomerTable = new HashSet<CustBusinessCollateral>();
			CustomerCreditLimitByProductCustomerTable = new HashSet<CustomerCreditLimitByProduct>();
			CustTransCustomerTable = new HashSet<CustTrans>();
			DocumentReturnTableCustomerTable = new HashSet<DocumentReturnTable>();
			FreeTextInvoiceTableCustomerTable = new HashSet<FreeTextInvoiceTable>();
			MessengerJobTableCustomerTable = new HashSet<MessengerJobTable>();
			PaymentDetailCustomerTable = new HashSet<PaymentDetail>();
			PaymentHistoryCustomerTable = new HashSet<PaymentHistory>();
			PurchaseTableCustomerTable = new HashSet<PurchaseTable>();
			ReceiptTableCustomerTable = new HashSet<ReceiptTable>();
			RetentionTransCustomerTable = new HashSet<RetentionTrans>();
			TaxInvoiceTableCustomerTable = new HashSet<TaxInvoiceTable>();
			VerificationTableCustomerTable = new HashSet<VerificationTable>();
			WithdrawalTableCustomerTable = new HashSet<WithdrawalTable>();
		}
 
		[Key]
		public Guid CustomerTableGUID { get; set; }
		[StringLength(100)]
		public string AltName { get; set; }
		public Guid? BlacklistStatusGUID { get; set; }
		[StringLength(100)]
		public string BOTRating { get; set; }
		public Guid? BusinessSegmentGUID { get; set; }
		public Guid? BusinessSizeGUID { get; set; }
		public Guid? BusinessTypeGUID { get; set; }
		[StringLength(100)]
		public string CompanyName { get; set; }
		[StringLength(100)]
		public string CompanyWebsite { get; set; }
		public Guid? CreditScoringGUID { get; set; }
		public Guid CurrencyGUID { get; set; }
		public Guid CustGroupGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string CustomerId { get; set; }
		[Column(TypeName = "date")]
		public DateTime? DateOfBirth { get; set; }
		[Column(TypeName = "date")]
		public DateTime? DateOfEstablish { get; set; }
		[Column(TypeName = "date")]
		public DateTime? DateOfExpiry { get; set; }
		[Column(TypeName = "date")]
		public DateTime? DateOfIssue { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		public int DueDay { get; set; }
		public Guid? ExposureGroupGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal FixedAsset { get; set; }
		public Guid? GenderGUID { get; set; }
		public Guid? GradeClassificationGUID { get; set; }
		public int IdentificationType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Income { get; set; }
		public Guid? IntroducedByGUID { get; set; }
		[StringLength(100)]
		public string IntroducedByRemark { get; set; }
		public int InvoiceIssuingDay { get; set; }
		[StringLength(100)]
		public string IssuedBy { get; set; }
		public Guid? KYCSetupGUID { get; set; }
		public int Labor { get; set; }
		public Guid? LineOfBusinessGUID { get; set; }
		public Guid? MaritalStatusGUID { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Name { get; set; }
		public Guid? NationalityGUID { get; set; }
		public Guid? NCBAccountStatusGUID { get; set; }
		public Guid? OccupationGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OtherIncome { get; set; }
		[StringLength(100)]
		public string OtherSourceIncome { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal PaidUpCapital { get; set; }
		public Guid? ParentCompanyGUID { get; set; }
		[StringLength(20)]
		public string PassportID { get; set; }
		[StringLength(100)]
		public string Position { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal PrivateARPct { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal PublicARPct { get; set; }
		public Guid? RaceGUID { get; set; }
		public int RecordType { get; set; }
		[StringLength(20)]
		public string ReferenceId { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal RegisteredCapital { get; set; }
		public Guid? RegistrationTypeGUID { get; set; }
		public Guid ResponsibleByGUID { get; set; }
		[StringLength(100)]
		public string SigningCondition { get; set; }
		[StringLength(100)]
		public string SpouseName { get; set; }
		[StringLength(13)]
		public string TaxID { get; set; }
		public Guid? TerritoryGUID { get; set; }
		public Guid? VendorTableGUID { get; set; }
		public Guid? WithholdingTaxGroupGUID { get; set; }
		public int WorkExperienceMonth { get; set; }
		public int WorkExperienceYear { get; set; }
		[StringLength(20)]
		public string WorkPermitID { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CustomerTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CustomerTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BlacklistStatusGUID")]
		[InverseProperty("CustomerTableBlacklistStatus")]
		public BlacklistStatus BlacklistStatus { get; set; }
		[ForeignKey("BusinessSegmentGUID")]
		[InverseProperty("CustomerTableBusinessSegment")]
		public BusinessSegment BusinessSegment { get; set; }
		[ForeignKey("BusinessSizeGUID")]
		[InverseProperty("CustomerTableBusinessSize")]
		public BusinessSize BusinessSize { get; set; }
		[ForeignKey("BusinessTypeGUID")]
		[InverseProperty("CustomerTableBusinessType")]
		public BusinessType BusinessType { get; set; }
		[ForeignKey("CreditScoringGUID")]
		[InverseProperty("CustomerTableCreditScoring")]
		public CreditScoring CreditScoring { get; set; }
		[ForeignKey("CurrencyGUID")]
		[InverseProperty("CustomerTableCurrency")]
		public Currency Currency { get; set; }
		[ForeignKey("CustGroupGUID")]
		[InverseProperty("CustomerTableCustGroup")]
		public CustGroup CustGroup { get; set; }
		[ForeignKey("Dimension1GUID")]
		[InverseProperty("CustomerTableDimension1")]
		public LedgerDimension LedgerDimension1 { get; set; }
		[ForeignKey("Dimension2GUID")]
		[InverseProperty("CustomerTableDimension2")]
		public LedgerDimension LedgerDimension2 { get; set; }
		[ForeignKey("Dimension3GUID")]
		[InverseProperty("CustomerTableDimension3")]
		public LedgerDimension LedgerDimension3 { get; set; }
		[ForeignKey("Dimension4GUID")]
		[InverseProperty("CustomerTableDimension4")]
		public LedgerDimension LedgerDimension4 { get; set; }
		[ForeignKey("Dimension5GUID")]
		[InverseProperty("CustomerTableDimension5")]
		public LedgerDimension LedgerDimension5 { get; set; }
		[ForeignKey("DocumentStatusGUID")]
		[InverseProperty("CustomerTableDocumentStatus")]
		public DocumentStatus DocumentStatus { get; set; }
		[ForeignKey("ExposureGroupGUID")]
		[InverseProperty("CustomerTableExposureGroup")]
		public ExposureGroup ExposureGroup { get; set; }
		[ForeignKey("GenderGUID")]
		[InverseProperty("CustomerTableGender")]
		public Gender Gender { get; set; }
		[ForeignKey("GradeClassificationGUID")]
		[InverseProperty("CustomerTableGradeClassification")]
		public GradeClassification GradeClassification { get; set; }
		[ForeignKey("IntroducedByGUID")]
		[InverseProperty("CustomerTableIntroducedBy")]
		public IntroducedBy IntroducedBy { get; set; }
		[ForeignKey("KYCSetupGUID")]
		[InverseProperty("CustomerTableKYCSetup")]
		public KYCSetup KYCSetup { get; set; }
		[ForeignKey("LineOfBusinessGUID")]
		[InverseProperty("CustomerTableLineOfBusiness")]
		public LineOfBusiness LineOfBusiness { get; set; }
		[ForeignKey("MaritalStatusGUID")]
		[InverseProperty("CustomerTableMaritalStatus")]
		public MaritalStatus MaritalStatus { get; set; }
		[ForeignKey("MethodOfPaymentGUID")]
		[InverseProperty("CustomerTableMethodOfPayment")]
		public MethodOfPayment MethodOfPayment { get; set; }
		[ForeignKey("NationalityGUID")]
		[InverseProperty("CustomerTableNationality")]
		public Nationality Nationality { get; set; }
		[ForeignKey("NCBAccountStatusGUID")]
		[InverseProperty("CustomerTableNCBAccountStatus")]
		public NCBAccountStatus NCBAccountStatus { get; set; }
		[ForeignKey("OccupationGUID")]
		[InverseProperty("CustomerTableOccupation")]
		public Occupation Occupation { get; set; }
		[ForeignKey("ParentCompanyGUID")]
		[InverseProperty("CustomerTableParentCompany")]
		public ParentCompany ParentCompany { get; set; }
		[ForeignKey("RaceGUID")]
		[InverseProperty("CustomerTableRace")]
		public Race Race { get; set; }
		[ForeignKey("RegistrationTypeGUID")]
		[InverseProperty("CustomerTableRegistrationType")]
		public RegistrationType RegistrationType { get; set; }
		[ForeignKey("ResponsibleByGUID")]
		[InverseProperty("CustomerTableResponsibleBy")]
		public EmployeeTable ResponsibleBy { get; set; }
		[ForeignKey("TerritoryGUID")]
		[InverseProperty("CustomerTableTerritory")]
		public Territory Territory { get; set; }
		[ForeignKey("VendorTableGUID")]
		[InverseProperty("CustomerTableVendorTable")]
		public VendorTable VendorTable { get; set; }
		[ForeignKey("WithholdingTaxGroupGUID")]
		[InverseProperty("CustomerTableWithholdingTaxGroup")]
		public WithholdingTaxGroup WithholdingTaxGroup { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("CustomerTable")]
		public ICollection<ApplicationTable> ApplicationTableCustomerTable { get; set; }
		[InverseProperty("CustomerTable")]
		public ICollection<AssignmentAgreementTable> AssignmentAgreementTableCustomerTable { get; set; }
		[InverseProperty("CustomerTable")]
		public ICollection<BuyerAgreementTable> BuyerAgreementTableCustomerTable { get; set; }
		[InverseProperty("CustomerTable")]
		public ICollection<BuyerReceiptTable> BuyerReceiptTableCustomerTable { get; set; }
		[InverseProperty("CustomerTable")]
		public ICollection<CAReqAssignmentOutstanding> CAReqAssignmentOutstandingCustomerTable { get; set; }
		[InverseProperty("CustomerTable")]
		public ICollection<CAReqCreditOutStanding> CAReqCreditOutStandingCustomerTable { get; set; }
		[InverseProperty("CustomerTable")]
		public ICollection<ChequeTable> ChequeTableCustomerTable { get; set; }
		[InverseProperty("CustomerTable")]
		public ICollection<CollectionFollowUp> CollectionFollowUpCustomerTable { get; set; }
		[InverseProperty("CustomerTable")]
		public ICollection<CreditAppReqAssignment> CreditAppReqAssignmentCustomerTable { get; set; }
		[InverseProperty("CustomerTable")]
		public ICollection<CreditAppTrans> CreditAppTransCustomerTable { get; set; }
		[InverseProperty("CustomerTable")]
		public ICollection<CustBank> CustBankCustomerTable { get; set; }
		[InverseProperty("CustomerTable")]
		public ICollection<CustBusinessCollateral> CustBusinessCollateralCustomerTable { get; set; }
		[InverseProperty("CustomerTable")]
		public ICollection<CustomerCreditLimitByProduct> CustomerCreditLimitByProductCustomerTable { get; set; }
		[InverseProperty("CustomerTable")]
		public ICollection<CustTrans> CustTransCustomerTable { get; set; }
		[InverseProperty("CustomerTable")]
		public ICollection<DocumentReturnTable> DocumentReturnTableCustomerTable { get; set; }
		[InverseProperty("CustomerTable")]
		public ICollection<FreeTextInvoiceTable> FreeTextInvoiceTableCustomerTable { get; set; }
		[InverseProperty("CustomerTable")]
		public ICollection<MessengerJobTable> MessengerJobTableCustomerTable { get; set; }
		[InverseProperty("CustomerTable")]
		public ICollection<PaymentDetail> PaymentDetailCustomerTable { get; set; }
		[InverseProperty("CustomerTable")]
		public ICollection<PaymentHistory> PaymentHistoryCustomerTable { get; set; }
		[InverseProperty("CustomerTable")]
		public ICollection<PurchaseTable> PurchaseTableCustomerTable { get; set; }
		[InverseProperty("CustomerTable")]
		public ICollection<ReceiptTable> ReceiptTableCustomerTable { get; set; }
		[InverseProperty("CustomerTable")]
		public ICollection<RetentionTrans> RetentionTransCustomerTable { get; set; }
		[InverseProperty("CustomerTable")]
		public ICollection<TaxInvoiceTable> TaxInvoiceTableCustomerTable { get; set; }
		[InverseProperty("CustomerTable")]
		public ICollection<VerificationTable> VerificationTableCustomerTable { get; set; }
		[InverseProperty("CustomerTable")]
		public ICollection<WithdrawalTable> WithdrawalTableCustomerTable { get; set; }
		#endregion Collection
	}
}
