using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class AssignmentAgreementSettle : CompanyBaseEntity
	{
		public AssignmentAgreementSettle()
		{
			AssignmentAgreementSettleRefAssignmentAgreementSettle = new HashSet<AssignmentAgreementSettle>();
		}
 
		[Key]
		public Guid AssignmentAgreementSettleGUID { get; set; }
		public Guid AssignmentAgreementTableGUID { get; set; }
		public Guid? BuyerAgreementTableGUID { get; set; }
		[StringLength(20)]
		public string DocumentId { get; set; }
		public Guid? DocumentReasonGUID { get; set; }
		public Guid? InvoiceTableGUID { get; set; }
		public Guid? RefAssignmentAgreementSettleGUID { get; set; }
		public Guid? RefGUID { get; set; }
		public int RefType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettledAmount { get; set; }
		[Column(TypeName = "date")]
		public DateTime SettledDate { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("AssignmentAgreementSettleCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("AssignmentAgreementSettleBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("AssignmentAgreementTableGUID")]
		[InverseProperty("AssignmentAgreementSettleAssignmentAgreementTable")]
		public AssignmentAgreementTable AssignmentAgreementTable { get; set; }
		[ForeignKey("BuyerAgreementTableGUID")]
		[InverseProperty("AssignmentAgreementSettleBuyerAgreementTable")]
		public BuyerAgreementTable BuyerAgreementTable { get; set; }
		[ForeignKey("DocumentReasonGUID")]
		[InverseProperty("AssignmentAgreementSettleDocumentReason")]
		public DocumentReason DocumentReason { get; set; }
		[ForeignKey("RefAssignmentAgreementSettleGUID")]
		[InverseProperty("AssignmentAgreementSettleRefAssignmentAgreementSettle")]
		public AssignmentAgreementSettle RefAssignmentAgreementSettle { get; set; }
		[ForeignKey("InvoiceTableGUID")]
		[InverseProperty("AssignmentAgreementSettleInvoiceTable")]
		public InvoiceTable InvoiceTable { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("RefAssignmentAgreementSettle")]
		public ICollection<AssignmentAgreementSettle> AssignmentAgreementSettleRefAssignmentAgreementSettle { get; set; }
		#endregion Collection
	}
}
