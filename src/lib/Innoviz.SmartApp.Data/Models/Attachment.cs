using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Attachment : BaseEntity
	{
		public Attachment()
		{
		}
 
		[Key]
		public Guid AttachmentGUID { get; set; }
		public string Base64Data { get; set; }
		[StringLength(100)]
		public string ContentType { get; set; }
		public string FileDescription { get; set; }
		[StringLength(200)]
		public string FileName { get; set; }
		public string FilePath { get; set; }
		public Guid? RefGUID { get; set; }
		public int RefType { get; set; }
 
		#region ForeignKey
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
