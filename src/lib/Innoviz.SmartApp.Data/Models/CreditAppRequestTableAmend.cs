using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CreditAppRequestTableAmend : CompanyBaseEntity
	{
		public CreditAppRequestTableAmend()
		{
		}
 
		[Key]
		public Guid CreditAppRequestTableAmendGUID { get; set; }
		public bool AmendAuthorizedPerson { get; set; }
		public bool AmendGuarantor { get; set; }
		public bool AmendRate { get; set; }
		public Guid CreditAppRequestTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OriginalCreditLimit { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal OriginalInterestAdjustmentPct { get; set; }
		public Guid? OriginalInterestTypeGUID { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal OriginalMaxPurchasePct { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OriginalMaxRetentionAmount { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal OriginalMaxRetentionPct { get; set; }
		public int OriginalPurchaseFeeCalculateBase { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal OriginalPurchaseFeePct { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal OriginalTotalInterestPct { get; set; }
		public Guid? CNReasonGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OrigInvoiceAmount { get; set; }
		[StringLength(100)]
		public string OrigInvoiceId { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OrigTaxInvoiceAmount { get; set; }
		[StringLength(100)]
		public string OrigTaxInvoiceId { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OriginalCreditLimitRequestFeeAmount { get; set; }


		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CreditAppRequestTableAmendCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CreditAppRequestTableAmendBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("CNReasonGUID")]
		[InverseProperty("CreditAppRequestTableAmendDocumentReason")]
		public DocumentReason DocumentReason { get; set; }
		#endregion ForeignKey

		#region Collection
		#endregion Collection
	}
}
