using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class BlacklistStatus : CompanyBaseEntity
	{
		public BlacklistStatus()
		{
			BuyerTableBlacklistStatus = new HashSet<BuyerTable>();
			CreditAppRequestLineBlacklistStatus = new HashSet<CreditAppRequestLine>();
			CreditAppRequestTableBlacklistStatus = new HashSet<CreditAppRequestTable>();
			CustomerTableBlacklistStatus = new HashSet<CustomerTable>();
		}
 
		[Key]
		public Guid BlacklistStatusGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string BlacklistStatusId { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("BlacklistStatusCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("BlacklistStatusBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("BlacklistStatus")]
		public ICollection<BuyerTable> BuyerTableBlacklistStatus { get; set; }
		[InverseProperty("BlacklistStatus")]
		public ICollection<CreditAppRequestLine> CreditAppRequestLineBlacklistStatus { get; set; }
		[InverseProperty("BlacklistStatus")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTableBlacklistStatus { get; set; }
		[InverseProperty("BlacklistStatus")]
		public ICollection<CustomerTable> CustomerTableBlacklistStatus { get; set; }
		#endregion Collection
	}
}
