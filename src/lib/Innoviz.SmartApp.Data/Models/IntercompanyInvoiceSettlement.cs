using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class IntercompanyInvoiceSettlement : CompanyBaseEntity {
		public IntercompanyInvoiceSettlement()
        {
			StagingTableIntercoInvSettleIntercompanyInvoiceSettlement = new HashSet<StagingTableIntercoInvSettle>();
		}
		[Key]
		public Guid IntercompanyInvoiceSettlementGUID { get; set; }
		public Guid? CNReasonGUID { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		[StringLength(20)]
		public string IntercompanyInvoiceSettlementId { get; set; }
		public Guid IntercompanyInvoiceTableGUID { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OrigInvoiceAmount { get; set; }
		[StringLength(20)]
		public string OrigInvoiceId { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OrigTaxInvoiceAmount { get; set; }
		[StringLength(20)]
		public string OrigTaxInvoiceId { get; set; }
		public Guid? RefIntercompanyInvoiceSettlementGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleInvoiceAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleWHTAmount { get; set; }
		[Column(TypeName = "date")]
		public DateTime TransDate { get; set; }
		public bool WHTSlipReceivedByCustomer { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("IntercompanyInvoiceSettlementCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("IntercompanyInvoiceSettlementBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("CNReasonGUID")]
		[InverseProperty("IntercompanyInvoiceSettlementCNReason")]
		public DocumentReason CNReason { get; set; }
		[ForeignKey("DocumentStatusGUID")]
		[InverseProperty("IntercompanyInvoiceSettlementDocumentStatus")]
		public DocumentStatus DocumentStatus { get; set; }
		[ForeignKey("IntercompanyInvoiceTableGUID")]
		[InverseProperty("IntercompanyInvoiceSettlementIntercompanyInvoiceTable")]
		public IntercompanyInvoiceTable IntercompanyInvoiceTable { get; set; }
		[ForeignKey("MethodOfPaymentGUID")]
		[InverseProperty("IntercompanyInvoiceSettlementMethodOfPayment")]
		public MethodOfPayment MethodOfPayment { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("IntercompanyInvoiceSettlement")]
		public ICollection<StagingTableIntercoInvSettle> StagingTableIntercoInvSettleIntercompanyInvoiceSettlement { get; set; }
		#endregion Collection
	}
}
