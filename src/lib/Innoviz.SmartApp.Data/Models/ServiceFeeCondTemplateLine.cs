using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class ServiceFeeCondTemplateLine : CompanyBaseEntity
	{
		public ServiceFeeCondTemplateLine()
		{
		}
 
		[Key]
		public Guid ServiceFeeCondTemplateLineGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal AmountBeforeTax { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public Guid InvoiceRevenueTypeGUID { get; set; }
		public int Ordering { get; set; }
		public Guid? ServiceFeeCondTemplateTableGUID { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("ServiceFeeCondTemplateLineCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("ServiceFeeCondTemplateLineBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("InvoiceRevenueTypeGUID")]
		[InverseProperty("ServiceFeeCondTemplateLineInvoiceRevenueType")]
		public InvoiceRevenueType InvoiceRevenueType { get; set; }
		[ForeignKey("ServiceFeeCondTemplateTableGUID")]
		[InverseProperty("ServiceFeeCondTemplateLineServiceFeeCondTemplateTable")]
		public ServiceFeeCondTemplateTable ServiceFeeCondTemplateTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
