using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class CustomerRefundTable : CompanyBaseEntity
	{
		[Key]
		public Guid CustomerRefundTableGUID { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public Guid CurrencyGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string CustomerRefundId { get; set; }
		public Guid CustomerTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public Guid? DocumentReasonGUID { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		[Column(TypeName = "decimal(18,5)")]
		public decimal ExchangeRate { get; set; }
		public Guid OperReportSignatureGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal PaymentAmount { get; set; }
		public int ProductType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal RetentionAmount { get; set; }
		public int RetentionCalculateBase { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal RetentionPct { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ServiceFeeAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SuspenseAmount { get; set; }
		public int SuspenseInvoiceType { get; set; }
		[Column(TypeName = "date")]
		public DateTime TransDate { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CustomerRefundTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CustomerRefundTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("CreditAppTableGUID")]
		[InverseProperty("CustomerRefundTableCreditAppTable")]
		public CreditAppTable CreditAppTable { get; set; }
		[ForeignKey("CurrencyGUID")]
		[InverseProperty("CustomerRefundTableCurrency")]
		public Currency Currency { get; set; }
		[ForeignKey("Dimension1GUID")]
		[InverseProperty("CustomerRefundTableDimension1")]
		public LedgerDimension LedgerDimension1 { get; set; }
		[ForeignKey("Dimension2GUID")]
		[InverseProperty("CustomerRefundTableDimension2")]
		public LedgerDimension LedgerDimension2 { get; set; }
		[ForeignKey("Dimension3GUID")]
		[InverseProperty("CustomerRefundTableDimension3")]
		public LedgerDimension LedgerDimension3 { get; set; }
		[ForeignKey("Dimension4GUID")]
		[InverseProperty("CustomerRefundTableDimension4")]
		public LedgerDimension LedgerDimension4 { get; set; }
		[ForeignKey("Dimension5GUID")]
		[InverseProperty("CustomerRefundTableDimension5")]
		public LedgerDimension LedgerDimension5 { get; set; }
		[ForeignKey("DocumentReasonGUID")]
		[InverseProperty("CustomerRefundTableDocumentReason")]
		public DocumentReason DocumentReason { get; set; }
		[ForeignKey("DocumentStatusGUID")]
		[InverseProperty("CustomerRefundTableDocumentStatus")]
		public DocumentStatus DocumentStatus { get; set; }
		[ForeignKey("OperReportSignatureGUID")]
		[InverseProperty("CustomerRefundTableEmployeeTable")]
		public EmployeeTable EmployeeTable { get; set; }
		#endregion ForeignKey

		#region Collection
		#endregion Collection
	}
}
