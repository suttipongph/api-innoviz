using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CompanySignature : BranchCompanyBaseEntity
	{
		public CompanySignature()
		{
		}
 
		[Key]
		public Guid CompanySignatureGUID { get; set; }
		public Guid EmployeeTableGUID { get; set; }
		public int Ordering { get; set; }
		public int RefType { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CompanySignatureCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CompanySignatureBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BranchGUID")]
		[InverseProperty("CompanySignatureBranch")]
		public Branch Branch { get; set; }
		[ForeignKey("EmployeeTableGUID")]
		[InverseProperty("CompanySignatureEmployeeTable")]
		public EmployeeTable EmployeeTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
