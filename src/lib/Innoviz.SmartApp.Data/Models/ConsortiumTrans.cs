using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class ConsortiumTrans : CompanyBaseEntity
	{
		public ConsortiumTrans()
		{
		}
 
		[Key]
		public Guid ConsortiumTransGUID { get; set; }
		[StringLength(500)]
		public string Address { get; set; }
		public Guid? AuthorizedPersonTypeGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string CustomerName { get; set; }
		[StringLength(100)]
		public string OperatedBy { get; set; }
		public int Ordering { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		public Guid? ConsortiumLineGUID { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("ConsortiumTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("ConsortiumTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("AuthorizedPersonTypeGUID")]
		[InverseProperty("ConsortiumTransAuthorizedPersonType")]
		public AuthorizedPersonType AuthorizedPersonType { get; set; }
		[ForeignKey("ConsortiumLineGUID")]
		[InverseProperty("ConsortiumTransConsortiumLine")]
		public ConsortiumLine ConsortiumLine { get; set; }
		#endregion ForeignKey

		#region Collection
		#endregion Collection

	}
}
