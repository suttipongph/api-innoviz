using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Intercompany : CompanyBaseEntity
	{
		public Intercompany()
		{
			IntercompanyInvoiceTableIntercompany = new HashSet<IntercompanyInvoiceTable>();
			InvoiceRevenueTypeIntercompany = new HashSet<InvoiceRevenueType>();
		}
 
		[Key]
		public Guid IntercompanyGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string IntercompanyId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("IntercompanyCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("IntercompanyBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("Intercompany")]
		public ICollection<IntercompanyInvoiceTable> IntercompanyInvoiceTableIntercompany { get; set; }
		[InverseProperty("Intercompany")]
		public ICollection<InvoiceRevenueType> InvoiceRevenueTypeIntercompany { get; set; }
		#endregion Collection
	}
}
