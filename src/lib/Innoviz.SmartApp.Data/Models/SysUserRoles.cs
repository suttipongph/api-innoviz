﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Innoviz.SmartApp.Core.Models;

namespace Innoviz.SmartApp.Data.Models
{
   
    public partial class SysUserRoles
    {
        
        public Guid UserId { get; set; }
        
        public Guid RoleId { get; set; }
        
        
    }
}
