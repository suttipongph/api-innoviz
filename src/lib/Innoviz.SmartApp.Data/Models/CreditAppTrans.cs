using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CreditAppTrans : CompanyBaseEntity
	{
		public CreditAppTrans()
		{
		}
 
		[Key]
		public Guid CreditAppTransGUID { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public Guid? CreditAppLineGUID { get; set; }
		public Guid CreditAppTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CreditDeductAmount { get; set; }
		public Guid CustomerTableGUID { get; set; }
		[StringLength(20)]
		public string DocumentId { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InvoiceAmount { get; set; }
		public int ProductType { get; set; }
		public Guid? RefGUID { get; set; }
		public int RefType { get; set; }
		[Column(TypeName = "date")]
		public DateTime TransDate { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CreditAppTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CreditAppTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BuyerTableGUID")]
		[InverseProperty("CreditAppTransBuyerTable")]
		public BuyerTable BuyerTable { get; set; }
		[ForeignKey("CreditAppLineGUID")]
		[InverseProperty("CreditAppTransCreditAppLine")]
		public CreditAppLine CreditAppLine { get; set; }
		[ForeignKey("CreditAppTableGUID")]
		[InverseProperty("CreditAppTransCreditAppTable")]
		public CreditAppTable CreditAppTable { get; set; }
		[ForeignKey("CustomerTableGUID")]
		[InverseProperty("CreditAppTransCustomerTable")]
		public CustomerTable CustomerTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
