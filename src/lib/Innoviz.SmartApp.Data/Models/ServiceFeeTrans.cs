using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class ServiceFeeTrans : CompanyBaseEntity
	{
		public ServiceFeeTrans()
		{
		}
 
		[Key]
		public Guid ServiceFeeTransGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal AmountBeforeTax { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal AmountIncludeTax { get; set; }
		public Guid? CNReasonGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal FeeAmount { get; set; }
		public bool IncludeTax { get; set; }
		public Guid InvoiceRevenueTypeGUID { get; set; }
		public int Ordering { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OrigInvoiceAmount { get; set; }
		[StringLength(100)]
		public string OrigInvoiceId { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OrigTaxInvoiceAmount { get; set; }
		[StringLength(100)]
		public string OrigTaxInvoiceId { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleInvoiceAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleWHTAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TaxAmount { get; set; }
		public Guid? TaxTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal WHTAmount { get; set; }
		public Guid? WithholdingTaxTableGUID { get; set; }
		public bool WHTSlipReceivedByCustomer { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("ServiceFeeTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("ServiceFeeTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("Dimension1GUID")]
		[InverseProperty("ServiceFeeTransDimension1")]
		public LedgerDimension LedgerDimension1 { get; set; }
		[ForeignKey("Dimension2GUID")]
		[InverseProperty("ServiceFeeTransDimension2")]
		public LedgerDimension LedgerDimension2 { get; set; }
		[ForeignKey("Dimension3GUID")]
		[InverseProperty("ServiceFeeTransDimension3")]
		public LedgerDimension LedgerDimension3 { get; set; }
		[ForeignKey("Dimension4GUID")]
		[InverseProperty("ServiceFeeTransDimension4")]
		public LedgerDimension LedgerDimension4 { get; set; }
		[ForeignKey("Dimension5GUID")]
		[InverseProperty("ServiceFeeTransDimension5")]
		public LedgerDimension LedgerDimension5 { get; set; }
		[ForeignKey("InvoiceRevenueTypeGUID")]
		[InverseProperty("ServiceFeeTransInvoiceRevenueType")]
		public InvoiceRevenueType InvoiceRevenueType { get; set; }
		[ForeignKey("TaxTableGUID")]
		[InverseProperty("ServiceFeeTransTaxTable")]
		public TaxTable TaxTable { get; set; }
		[ForeignKey("WithholdingTaxTableGUID")]
		[InverseProperty("ServiceFeeTransWithholdingTaxTable")]
		public WithholdingTaxTable WithholdingTaxTable { get; set; }
		[ForeignKey("CNReasonGUID")]
		[InverseProperty("ServiceFeeTransDocumentReason")]
		public DocumentReason DocumentReason { get; set; }
		#endregion ForeignKey

		#region Collection
		#endregion Collection
	}
}
