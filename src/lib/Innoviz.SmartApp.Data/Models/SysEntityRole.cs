﻿using Innoviz.SmartApp.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Innoviz.SmartApp.Data.Models
{
    public class SysEntityRole : BaseEntity
    {
        [Key]
        public Guid SysEntityRoleGUID { get; set; }
        public Guid RoleGUID { get; set; }
        public string ModelName { get; set; }
        public int AccessLevel { get; set; }
    }
}
