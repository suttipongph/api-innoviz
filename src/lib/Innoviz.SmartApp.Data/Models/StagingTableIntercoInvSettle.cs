using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class StagingTableIntercoInvSettle : CompanyBaseEntity {
		[Key]
		public Guid StagingTableIntercoInvSettleGUID { get; set; }
		[Required]
		[StringLength(500)]
		public string Address1 { get; set; }
		[StringLength(100)]
		public string AltName { get; set; }
		[StringLength(20)]
		public string CNReasonId { get; set; }
		[Required]
		[StringLength(20)]
		public string CompanyId { get; set; }
		[Required]
		[StringLength(20)]
		public string CountryId { get; set; }
		[Required]
		[StringLength(20)]
		public string CurrencyId { get; set; }
		[StringLength(100)]
		public string CustFaxValue { get; set; }
		[Required]
		[StringLength(20)]
		public string CustGroupId { get; set; }
		[Required]
		[StringLength(20)]
		public string CustomerId { get; set; }
		[StringLength(100)]
		public string CustPhoneValue { get; set; }
		[StringLength(20)]
		public string DimensionCode1 { get; set; }
		[StringLength(20)]
		public string DimensionCode2 { get; set; }
		[StringLength(20)]
		public string DimensionCode3 { get; set; }
		[StringLength(20)]
		public string DimensionCode4 { get; set; }
		[StringLength(20)]
		public string DimensionCode5 { get; set; }
		[Required]
		[StringLength(20)]
		public string DistrictId { get; set; }
		[Column(TypeName = "date")]
		public DateTime DueDate { get; set; }
		[Required]
		[StringLength(100)]
		public string FeeLedgerAccount { get; set; }
		public Guid IntercompanyInvoiceSettlementGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string IntercompanyInvoiceSettlementId { get; set; }
		[StringLength(20)]
		public string InterfaceStagingBatchId { get; set; }
		public int InterfaceStatus { get; set; }
		[Column(TypeName = "date")]
		public DateTime InvoiceDate { get; set; }
		[Required]
		[StringLength(250)]
		public string InvoiceText { get; set; }
		[StringLength(20)]
		public string MethodOfPaymentId { get; set; }
		[Required]
		[StringLength(100)]
		public string Name { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OrigInvoiceAmount { get; set; }
		[StringLength(20)]
		public string OrigInvoiceId { get; set; }
		[Required]
		[StringLength(20)]
		public string PostalCode { get; set; }
		[Required]
		[StringLength(20)]
		public string ProvinceId { get; set; }
		public int RecordType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleInvoiceAmount { get; set; }
		[Required]
		[StringLength(20)]
		public string SubDistrictId { get; set; }
		[Required]
		[StringLength(5)]
		public string TaxBranchId { get; set; }
		[StringLength(20)]
		public string TaxCode { get; set; }
		[Required]
		[StringLength(13)]
		public string TaxId { get; set; }
		[StringLength(5)]
		public string CompanyTaxBranchId { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("StagingTableIntercoInvSettleCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("StagingTableIntercoInvSettleBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("IntercompanyInvoiceSettlementGUID")]
		[InverseProperty("StagingTableIntercoInvSettleIntercompanyInvoiceSettlement")]
		public IntercompanyInvoiceSettlement IntercompanyInvoiceSettlement { get; set; }
		#endregion ForeignKey
	}
}
