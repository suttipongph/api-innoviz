﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
    public partial class BatchInstanceLog 
    {
        
        public int Id { get; set; }
        public Guid? InstanceHistoryId { get; set; }
        public int ResultStatus { get; set; }
        public string Message { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime TimeStamp { get; set; }
        public string ItemValues { get; set; }
        public string StackTrace { get; set; }
        [StringLength(100)]
        public string Reference { get; set; }

        [ForeignKey("InstanceHistoryId")]
        [InverseProperty("BatchInstanceLog")]
        public BatchInstanceHistory InstanceHistory { get; set; }
    }
}
