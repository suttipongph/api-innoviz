using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class FinancialCreditTrans : CompanyBaseEntity
	{
		public FinancialCreditTrans()
		{
		}
 
		[Key]
		public Guid FinancialCreditTransGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Amount { get; set; }
		public Guid BankGroupGUID { get; set; }
		public Guid CreditTypeGUID { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("FinancialCreditTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("FinancialCreditTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BankGroupGUID")]
		[InverseProperty("FinancialCreditTransBankGroup")]
		public BankGroup BankGroup { get; set; }
		[ForeignKey("CreditTypeGUID")]
		[InverseProperty("FinancialCreditTransCreditType")]
		public CreditType CreditType { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
