using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_BusinessCollateralAgmTable
	{

		[Key]
		[Column(Order = 1)]
		[StringLength(50)]
		public string BusinessCollateralAgmTableGUID { get; set; }
		public string AgreementDate { get; set; }
		public int AgreementDocType { get; set; }
		public int AgreementExtension { get; set; }
		[StringLength(20)]
		public string BusinessCollateralAgmId { get; set; }
		[StringLength(50)]
		public string ConsortiumTableGUID { get; set; }
		[StringLength(50)]
		public string CreditAppRequestTableGUID { get; set; }
		[StringLength(50)]
		public string CreditAppTableGUID { get; set; }
		[StringLength(50)]
		public string CreditLimitTypeGUID { get; set; }
		[StringLength(100)]
		public string CustomerAltName { get; set; }
		[Required]
		[StringLength(100)]
		public string CustomerName { get; set; }
		[StringLength(50)]
		public string CustomerTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[StringLength(50)]
		public string DocumentReasonGUID { get; set; }
		[StringLength(50)]
		public string DocumentStatusGUID { get; set; }
		[Key]
		[Column(Order = 2)]
		[StringLength(20)]
		public string InternalBusinessCollateralAgmId { get; set; }
		public int ProductType { get; set; }
		[StringLength(50)]
		public string RefBusinessCollateralAgmTableGUID { get; set; }
		public string SigningDate { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal DBDRegistrationAmount { get; set; }
		public string DBDRegistrationDate { get; set; }
		[StringLength(100)]
		public string DBDRegistrationDescription { get; set; }
		[StringLength(20)]
		public string DBDRegistrationId { get; set; }
		[StringLength(1000)]
		public string AttachmentRemark { get; set; }
		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }
		[Key]
		[Column(Order = 3)]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }

	}
}
