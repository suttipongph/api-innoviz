using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Language : CompanyBaseEntity
	{
		public Language()
		{
			ApplicationTableLanguage = new HashSet<ApplicationTable>();
		}
 
		[Key]
		public Guid LanguageGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string LanguageId { get; set; }
		[Required]
		[StringLength(100)]
		public string Name { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("LanguageCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("LanguageBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("Language")]
		public ICollection<ApplicationTable> ApplicationTableLanguage { get; set; }
		#endregion Collection
	}
}
