using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class MemoTrans : CompanyBaseEntity
	{
		public MemoTrans()
		{
		}
 
		[Key]
		public Guid MemoTransGUID { get; set; }
		[Required]
		[StringLength(1000)]
		public string Memo { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		[Required]
		[StringLength(100)]
		public string Topic { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("MemoTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("MemoTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
