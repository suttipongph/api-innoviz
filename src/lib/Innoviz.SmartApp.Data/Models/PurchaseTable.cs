using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class PurchaseTable : CompanyBaseEntity
	{
		public PurchaseTable()
		{
			PurchaseLinePurchaseTable = new HashSet<PurchaseLine>();
		}
 
		[Key]
		public Guid PurchaseTableGUID { get; set; }
		public bool AdditionalPurchase { get; set; }
		public Guid CreditAppTableGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public bool DiffChequeIssuedName { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal OverCreditBuyer { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal OverCreditCA { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal OverCreditCALine { get; set; }
		public int ProcessInstanceId { get; set; }
		public int ProductType { get; set; }
		[Column(TypeName = "date")]
		public DateTime PurchaseDate { get; set; }
		[Required]
		[StringLength(20)]
		public string PurchaseId { get; set; }
		public Guid? ReceiptTempTableGUID { get; set; }
		public int RetentionCalculateBase { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal RetentionFixedAmount { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal RetentionPct { get; set; }
		public bool Rollbill { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SumPurchaseAmount { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal TotalInterestPct { get; set; }
		public Guid? DocumentReasonGUID { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		public Guid OperReportSignatureGUID { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("PurchaseTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("PurchaseTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("CreditAppTableGUID")]
		[InverseProperty("PurchaseTableCreditAppTable")]
		public CreditAppTable CreditAppTable { get; set; }
		[ForeignKey("CustomerTableGUID")]
		[InverseProperty("PurchaseTableCustomerTable")]
		public CustomerTable CustomerTable { get; set; }
		[ForeignKey("Dimension1GUID")]
		[InverseProperty("PurchaseTableDimension1")]
		public LedgerDimension LedgerDimension1 { get; set; }
		[ForeignKey("Dimension2GUID")]
		[InverseProperty("PurchaseTableDimension2")]
		public LedgerDimension LedgerDimension2 { get; set; }
		[ForeignKey("Dimension3GUID")]
		[InverseProperty("PurchaseTableDimension3")]
		public LedgerDimension LedgerDimension3 { get; set; }
		[ForeignKey("Dimension4GUID")]
		[InverseProperty("PurchaseTableDimension4")]
		public LedgerDimension LedgerDimension4 { get; set; }
		[ForeignKey("Dimension5GUID")]
		[InverseProperty("PurchaseTableDimension5")]
		public LedgerDimension LedgerDimension5 { get; set; }
		[ForeignKey("DocumentReasonGUID")]
		[InverseProperty("PurchaseTableDocumentReason")]
		public DocumentReason DocumentReason { get; set; }
		[ForeignKey("DocumentStatusGUID")]
		[InverseProperty("PurchaseTableDocumentStatus")]
		public DocumentStatus DocumentStatus { get; set; }
		[ForeignKey("ReceiptTempTableGUID")]
		[InverseProperty("PurchaseTableReceiptTempTable")]
		public ReceiptTempTable ReceiptTempTable { get; set; }

		[ForeignKey("OperReportSignatureGUID")]
		[InverseProperty("PurchaseTableEmployeeTable")]
		public EmployeeTable EmployeeTable { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("PurchaseTable")]
		public ICollection<PurchaseLine> PurchaseLinePurchaseTable { get; set; }
		#endregion Collection
		
	}
}
