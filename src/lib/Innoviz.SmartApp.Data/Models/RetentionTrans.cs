using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class RetentionTrans : CompanyBaseEntity
	{
		public RetentionTrans()
		{
		}
 
		[Key]
		public Guid RetentionTransGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Amount { get; set; }
		public Guid? BuyerAgreementTableGUID { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public Guid CreditAppTableGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string DocumentId { get; set; }
		public int ProductType { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		[Column(TypeName = "date")]
		public DateTime TransDate { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("RetentionTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("RetentionTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BuyerAgreementTableGUID")]
		[InverseProperty("RetentionTransBuyerAgreementTable")]
		public BuyerAgreementTable BuyerAgreementTable { get; set; }
		[ForeignKey("BuyerTableGUID")]
		[InverseProperty("RetentionTransBuyerTable")]
		public BuyerTable BuyerTable { get; set; }
		[ForeignKey("CreditAppTableGUID")]
		[InverseProperty("RetentionTransCreditAppTable")]
		public CreditAppTable CreditAppTable { get; set; }
		[ForeignKey("CustomerTableGUID")]
		[InverseProperty("RetentionTransCustomerTable")]
		public CustomerTable CustomerTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
