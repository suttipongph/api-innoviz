﻿using Innoviz.SmartApp.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Innoviz.SmartApp.Data.Models
{
    public class SysFeatureGroup : BaseEntity
    {
        public SysFeatureGroup()
        {
            SysFeatureGroupController = new HashSet<SysFeatureGroupController>();
            SysFeatureGroupMapping = new HashSet<SysFeatureGroupMapping>();
            SysFeatureGroupRole = new HashSet<SysFeatureGroupRole>();
            SysFeatureGroupStructure = new HashSet<SysFeatureGroupStructure>();
        }
        [Key]
        public Guid SysFeatureGroupGUID { get; set; }
        public string GroupId { get; set; }
        public int FeatureType { get; set; }
        public string PathMenu { get; set; }

        [InverseProperty("SysFeatureGroupGU")]
        public ICollection<SysFeatureGroupController> SysFeatureGroupController { get; set; }
        [InverseProperty("SysFeatureGroupGU")]
        public ICollection<SysFeatureGroupMapping> SysFeatureGroupMapping { get; set; }
        [InverseProperty("SysFeatureGroupGU")]
        public ICollection<SysFeatureGroupRole> SysFeatureGroupRole { get; set; }
        [InverseProperty("SysFeatureGroupGU")]
        public ICollection<SysFeatureGroupStructure> SysFeatureGroupStructure { get; set; }
    }
}
