using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Branch : CompanyBaseEntity
	{
		public Branch()
		{
			CompanySignatureBranch = new HashSet<CompanySignature>();
			CustTransBranch = new HashSet<CustTrans>();
			InvoiceLineBranch = new HashSet<InvoiceLine>();
			InvoiceNumberSeqSetupBranch = new HashSet<InvoiceNumberSeqSetup>();
			InvoiceTableBranch = new HashSet<InvoiceTable>();
			PaymentHistoryBranch = new HashSet<PaymentHistory>();
			ReceiptLineBranch = new HashSet<ReceiptLine>();
			ReceiptTableBranch = new HashSet<ReceiptTable>();
			TaxInvoiceLineBranch = new HashSet<TaxInvoiceLine>();
			TaxInvoiceTableBranch = new HashSet<TaxInvoiceTable>();
			SysUserCompanyMapping = new HashSet<SysUserCompanyMapping>();
			SysUserTable = new HashSet<SysUserTable>();
		}
 
		[Key]
		public Guid BranchGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string BranchId { get; set; }
		[Required]
		[StringLength(100)]
		public string Name { get; set; }
		public Guid? TaxBranchGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string CompanyId { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("BranchCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("BranchBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("BranchGU")]
		public ICollection<SysUserCompanyMapping> SysUserCompanyMapping { get; set; }
		[InverseProperty("DefaultBranchGU")]
		public ICollection<SysUserTable> SysUserTable { get; set; }
		[InverseProperty("Branch")]
		public ICollection<CompanySignature> CompanySignatureBranch { get; set; }
		[InverseProperty("Branch")]
		public ICollection<CustTrans> CustTransBranch { get; set; }
		[InverseProperty("Branch")]
		public ICollection<InvoiceLine> InvoiceLineBranch { get; set; }
		[InverseProperty("Branch")]
		public ICollection<InvoiceNumberSeqSetup> InvoiceNumberSeqSetupBranch { get; set; }
		[InverseProperty("Branch")]
		public ICollection<InvoiceTable> InvoiceTableBranch { get; set; }
		[InverseProperty("Branch")]
		public ICollection<PaymentHistory> PaymentHistoryBranch { get; set; }
		[InverseProperty("Branch")]
		public ICollection<ReceiptLine> ReceiptLineBranch { get; set; }
		[InverseProperty("Branch")]
		public ICollection<ReceiptTable> ReceiptTableBranch { get; set; }
		[InverseProperty("Branch")]
		public ICollection<TaxInvoiceLine> TaxInvoiceLineBranch { get; set; }
		[InverseProperty("Branch")]
		public ICollection<TaxInvoiceTable> TaxInvoiceTableBranch { get; set; }
		#endregion Collection
	}
}
