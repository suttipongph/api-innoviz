using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class AddressSubDistrict : CompanyBaseEntity
	{
		public AddressSubDistrict()
		{
			AddressPostalCodeAddressSubDistrict = new HashSet<AddressPostalCode>();
			AddressTransAddressSubDistrict = new HashSet<AddressTrans>();
		}
 
		[Key]
		public Guid AddressSubDistrictGUID { get; set; }
		public Guid AddressDistrictGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Name { get; set; }
		[Required]
		[StringLength(20)]
		public string SubDistrictId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("AddressSubDistrictCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("AddressSubDistrictBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("AddressDistrictGUID")]
		[InverseProperty("AddressSubDistrictAddressDistrict")]
		public AddressDistrict AddressDistrict { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("AddressSubDistrict")]
		public ICollection<AddressTrans> AddressTransAddressSubDistrict { get; set; }
		[InverseProperty("AddressSubDistrict")]
		public ICollection<AddressPostalCode> AddressPostalCodeAddressSubDistrict { get; set; }
		#endregion Collection
	}
}
