using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class AddressProvince : CompanyBaseEntity
	{
		public AddressProvince()
		{
			AddressDistrictAddressProvince = new HashSet<AddressDistrict>();
			AddressTransAddressProvince = new HashSet<AddressTrans>();
		}
 
		[Key]
		public Guid AddressProvinceGUID { get; set; }
		public Guid AddressCountryGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Name { get; set; }
		[Required]
		[StringLength(20)]
		public string ProvinceId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("AddressProvinceCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("AddressProvinceBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("AddressCountryGUID")]
		[InverseProperty("AddressProvinceAddressCountry")]
		public AddressCountry AddressCountry { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("AddressProvince")]
		public ICollection<AddressDistrict> AddressDistrictAddressProvince { get; set; }
		[InverseProperty("AddressProvince")]
		public ICollection<AddressTrans> AddressTransAddressProvince { get; set; }
		#endregion Collection
	}
}
