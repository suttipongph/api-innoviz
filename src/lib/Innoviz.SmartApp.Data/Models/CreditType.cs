using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CreditType : CompanyBaseEntity
	{
		public CreditType()
		{
			FinancialCreditTransCreditType = new HashSet<FinancialCreditTrans>();
			NCBTransCreditType = new HashSet<NCBTrans>();
		}
 
		[Key]
		public Guid CreditTypeGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string CreditTypeId { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CreditTypeCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CreditTypeBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("CreditType")]
		public ICollection<FinancialCreditTrans> FinancialCreditTransCreditType { get; set; }
		[InverseProperty("CreditType")]
		public ICollection<NCBTrans> NCBTransCreditType { get; set; }
		#endregion Collection
	}
}
