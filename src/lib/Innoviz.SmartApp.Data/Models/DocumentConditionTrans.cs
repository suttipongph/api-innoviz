using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class DocumentConditionTrans : CompanyBaseEntity
	{
		public DocumentConditionTrans()
		{
		}
 
		[Key]
		public Guid DocumentConditionTransGUID { get; set; }
		public int DocConVerifyType { get; set; }
		public Guid DocumentTypeGUID { get; set; }
		public bool Inactive { get; set; }
		public bool Mandatory { get; set; }
		public Guid? RefDocumentConditionTransGUID { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("DocumentConditionTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("DocumentConditionTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("DocumentTypeGUID")]
		[InverseProperty("DocumentConditionTransDocumentType")]
		public DocumentType DocumentType { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
