using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class RetentionConditionTrans : CompanyBaseEntity
	{
		public RetentionConditionTrans()
		{
		}
 
		[Key]
		public Guid RetentionConditionTransGUID { get; set; }
		public int ProductType { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal RetentionAmount { get; set; }
		public int RetentionCalculateBase { get; set; }
		public int RetentionDeductionMethod { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal RetentionPct { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("RetentionConditionTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("RetentionConditionTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
