﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
    public partial class SysControllerTable : BaseEntity {
        public SysControllerTable()
        {
            SysControllerRole = new HashSet<SysControllerRole>();
            SysFeatureGroupController = new HashSet<SysFeatureGroupController>();
            SysControllerEntityMapping = new HashSet<SysControllerEntityMapping>();
        }

        [Key]
        public Guid SysControllerTableGUID { get; set; }
        [Required]
        [StringLength(200)]
        public string ControllerName { get; set; }
        
        [Required]
        [StringLength(500)]
        public string RouteAttribute { get; set; }

        [InverseProperty("SysControllerTableGU")]
        public ICollection<SysControllerRole> SysControllerRole { get; set; }
        [InverseProperty("SysControllerTableGU")]
        public ICollection<SysFeatureGroupController> SysFeatureGroupController { get; set; }
        [InverseProperty("SysControllerTableGU")]
        public ICollection<SysControllerEntityMapping> SysControllerEntityMapping { get; set; }
    }
}
