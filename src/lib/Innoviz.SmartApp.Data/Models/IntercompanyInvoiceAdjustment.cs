using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class IntercompanyInvoiceAdjustment : CompanyBaseEntity {
		[Key]
		public Guid IntercompanyInvoiceAdjustmentGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Adjustment { get; set; }
		public Guid DocumentReasonGUID { get; set; }
		public Guid IntercompanyInvoiceTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OriginalAmount { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("IntercompanyInvoiceAdjustmentCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("IntercompanyInvoiceAdjustmentBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("DocumentReasonGUID")]
		[InverseProperty("IntercompanyInvoiceAdjustmentDocumentReason")]
		public DocumentReason DocumentReason { get; set; }
		[ForeignKey("IntercompanyInvoiceTableGUID")]
		[InverseProperty("IntercompanyInvoiceAdjustmentIntercompanyInvoiceTable")]
		public IntercompanyInvoiceTable IntercompanyInvoiceTable { get; set; }
		#endregion ForeignKey
	}
}
