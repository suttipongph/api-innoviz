using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class MessengerTable : CompanyBaseEntity
	{
		public MessengerTable()
		{
			MessengerJobTableMessengerTable = new HashSet<MessengerJobTable>();
		}
 
		[Key]
		public Guid MessengerTableGUID { get; set; }
		[StringLength(250)]
		public string Address { get; set; }
		[StringLength(100)]
		public string DriverLicenseId { get; set; }
		[Required]
		[StringLength(100)]
		public string Name { get; set; }
		[StringLength(100)]
		public string Phone { get; set; }
		[StringLength(100)]
		public string PlateNumber { get; set; }
		[StringLength(13)]
		public string TaxId { get; set; }
		public Guid? VendorTableGUID { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("MessengerTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("MessengerTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("VendorTableGUID")]
		[InverseProperty("MessengerTableVendorTable")]
		public VendorTable VendorTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("MessengerTable")]
		public ICollection<MessengerJobTable> MessengerJobTableMessengerTable { get; set; }
		#endregion Collection
	}
}
