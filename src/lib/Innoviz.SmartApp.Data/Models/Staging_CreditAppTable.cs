﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_CreditAppTable
	{
		[Key]
		[Column(Order = 1)]
		[StringLength(50)]
		public string CreditAppTableGUID { get; set; }
		[StringLength(50)]
		public string ApplicationTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ApprovedCreditLimit { get; set; }
		[Required]
		[StringLength(50)]
		public string ApprovedDate { get; set; }
		[StringLength(50)]
		public string BankAccountControlGUID { get; set; }
		[StringLength(50)]
		public string BillingAddressGUID { get; set; }
		[StringLength(50)]
		public string BillingContactPersonGUID { get; set; }
		[StringLength(250)]
		public string CACondition { get; set; }
		[StringLength(50)]
		public string ConsortiumTableGUID { get; set; }
		[Key]
		[Column(Order = 2)]
		[StringLength(20)]
		public string CreditAppId { get; set; }
		public int CreditLimitExpiration { get; set; }
		[StringLength(100)]
		public string CreditLimitRemark { get; set; }
		[Required]
		[StringLength(50)]
		public string CreditLimitTypeGUID { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal CreditRequestFeePct { get; set; }
		[StringLength(50)]
		public string CreditTermGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string CustomerTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[StringLength(50)]
		public string Dimension1GUID { get; set; }
		[StringLength(50)]
		public string Dimension2GUID { get; set; }
		[StringLength(50)]
		public string Dimension3GUID { get; set; }
		[StringLength(50)]
		public string Dimension4GUID { get; set; }
		[StringLength(50)]
		public string Dimension5GUID { get; set; }
		[StringLength(50)]
		public string DocumentReasonGUID { get; set; }
		[StringLength(50)]
		public string ExpectedAgreementSigningDate { get; set; }
		[Required]
		[StringLength(50)]
		public string ExpiryDate { get; set; }
		[StringLength(50)]
		public string ExtensionServiceFeeCondTemplateGUID { get; set; }
		[StringLength(50)]
		public string InactiveDate { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal InterestAdjustment { get; set; }
		[Required]
		[StringLength(50)]
		public string InterestTypeGUID { get; set; }
		[StringLength(50)]
		public string InvoiceAddressGUID { get; set; }
		[StringLength(50)]
		public string MailingReceipAddressGUID { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal MaxPurchasePct { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal MaxRetentionAmount { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal MaxRetentionPct { get; set; }
		[StringLength(50)]
		public string PDCBankGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string ProductSubTypeGUID { get; set; }
		public int ProductType { get; set; }
		public int PurchaseFeeCalculateBase { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal PurchaseFeePct { get; set; }
		[StringLength(50)]
		public string ReceiptAddressGUID { get; set; }
		[StringLength(50)]
		public string ReceiptContactPersonGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string RefCreditAppRequestTableGUID { get; set; }
		[StringLength(50)]
		public string RegisteredAddressGUID { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SalesAvgPerMonth { get; set; }
		[Required]
		[StringLength(50)]
		public string StartDate { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal TotalInterestPct { get; set; }
		[Key]
		[Column(Order = 3)]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		[StringLength(50)]
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		[StringLength(50)]
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		public string OwnerBusinessUnitGUID { get; set; }
		public int MigrateStatus { get; set; }
		public int MigrateSource { get; set; }
	}
}
