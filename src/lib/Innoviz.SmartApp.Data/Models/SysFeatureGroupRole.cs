﻿using Innoviz.SmartApp.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Innoviz.SmartApp.Data.Models
{
    public class SysFeatureGroupRole : BaseEntity
    {

        [Key]
        public Guid SysFeatureGroupRoleGUID { get; set; }
        public Guid SysFeatureGroupGUID { get; set; }
        public Guid RoleGUID { get; set; }
        #region LIT
        public int Read { get; set; }
        public int Update { get; set; }
        public int Create { get; set; }
        public int Delete { get; set; }
        public int Action { get; set; }
        #endregion LIT

        
        [ForeignKey("SysFeatureGroupGUID")]
        [InverseProperty("SysFeatureGroupRole")]
        public SysFeatureGroup SysFeatureGroupGU { get; set; }
    }
}
