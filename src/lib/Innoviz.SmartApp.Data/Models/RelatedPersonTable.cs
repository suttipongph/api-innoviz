using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class RelatedPersonTable : CompanyBaseEntity
	{
		public RelatedPersonTable()
		{
			AuthorizedPersonTransRelatedPersonTable = new HashSet<AuthorizedPersonTrans>();
			ContactPersonTransRelatedPersonTable = new HashSet<ContactPersonTrans>();
			GuarantorTransRelatedPersonTable = new HashSet<GuarantorTrans>();
			OwnerTransRelatedPersonTable = new HashSet<OwnerTrans>();
		}
 
		[Key]
		public Guid RelatedPersonTableGUID { get; set; }
		[StringLength(250)]
		public string BackgroundSummary { get; set; }
		[StringLength(100)]
		public string CompanyName { get; set; }
		[Column(TypeName = "date")]
		public DateTime? DateOfBirth { get; set; }
		[Column(TypeName = "date")]
		public DateTime? DateOfEstablishment { get; set; }
		[Column(TypeName = "date")]
		public DateTime? DateOfExpiry { get; set; }
		[Column(TypeName = "date")]
		public DateTime? DateOfIssue { get; set; }
		[StringLength(100)]
		public string Email { get; set; }
		[StringLength(100)]
		public string Extension { get; set; }
		[StringLength(100)]
		public string Fax { get; set; }
		public Guid? GenderGUID { get; set; }
		public int IdentificationType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Income { get; set; }
		[StringLength(100)]
		public string IssuedBy { get; set; }
		[StringLength(100)]
		public string LineId { get; set; }
		public Guid? MaritalStatusGUID { get; set; }
		[StringLength(100)]
		public string Mobile { get; set; }
		[Required]
		[StringLength(100)]
		public string Name { get; set; }
		public Guid? NationalityGUID { get; set; }
		public Guid? OccupationGUID { get; set; }
		[StringLength(100)]
		public string OperatedBy { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OtherIncome { get; set; }
		[StringLength(100)]
		public string OtherSourceIncome { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal PaidUpCapital { get; set; }
		[StringLength(20)]
		public string PassportId { get; set; }
		[StringLength(100)]
		public string Phone { get; set; }
		[StringLength(100)]
		public string Position { get; set; }
		public Guid? RaceGUID { get; set; }
		public int RecordType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal RegisteredCapital { get; set; }
		public Guid? RegistrationTypeGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string RelatedPersonId { get; set; }
		[StringLength(100)]
		public string SpouseName { get; set; }
		[StringLength(13)]
		public string TaxId { get; set; }
		public int WorkExperienceMonth { get; set; }
		public int WorkExperienceYear { get; set; }
		[StringLength(20)]
		public string WorkPermitId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("RelatedPersonTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("RelatedPersonTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("GenderGUID")]
		[InverseProperty("RelatedPersonTableGender")]
		public Gender Gender { get; set; }
		[ForeignKey("MaritalStatusGUID")]
		[InverseProperty("RelatedPersonTableMaritalStatus")]
		public MaritalStatus MaritalStatus { get; set; }
		[ForeignKey("NationalityGUID")]
		[InverseProperty("RelatedPersonTableNationality")]
		public Nationality Nationality { get; set; }
		[ForeignKey("OccupationGUID")]
		[InverseProperty("RelatedPersonTableOccupation")]
		public Occupation Occupation { get; set; }
		[ForeignKey("RaceGUID")]
		[InverseProperty("RelatedPersonTableRace")]
		public Race Race { get; set; }
		[ForeignKey("RegistrationTypeGUID")]
		[InverseProperty("RelatedPersonTableRegistrationType")]
		public RegistrationType RegistrationType { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("RelatedPersonTable")]
		public ICollection<AuthorizedPersonTrans> AuthorizedPersonTransRelatedPersonTable { get; set; }
		[InverseProperty("RelatedPersonTable")]
		public ICollection<ContactPersonTrans> ContactPersonTransRelatedPersonTable { get; set; }
		[InverseProperty("RelatedPersonTable")]
		public ICollection<GuarantorTrans> GuarantorTransRelatedPersonTable { get; set; }
		[InverseProperty("RelatedPersonTable")]
		public ICollection<OwnerTrans> OwnerTransRelatedPersonTable { get; set; }
		#endregion Collection
	}
}
