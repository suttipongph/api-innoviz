using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class JointVentureTrans : CompanyBaseEntity
	{
		public JointVentureTrans()
		{
		}
 
		[Key]
		public Guid JointVentureTransGUID { get; set; }
		[StringLength(500)]
		public string Address { get; set; }
		public Guid? AuthorizedPersonTypeGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Name { get; set; }
		[StringLength(100)]
		public string OperatedBy { get; set; }
		public int Ordering { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("JointVentureTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("JointVentureTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("AuthorizedPersonTypeGUID")]
		[InverseProperty("JointVentureTransAuthorizedPersonType")]
		public AuthorizedPersonType AuthorizedPersonType { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
