using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class ProductSettledTrans : CompanyBaseEntity
	{
		public ProductSettledTrans()
		{
		}
 
		[Key]
		public Guid ProductSettledTransGUID { get; set; }
		[StringLength(20)]
		public string DocumentId { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InterestCalcAmount { get; set; }
		[Column(TypeName = "date")]
		public DateTime InterestDate { get; set; }
		public int InterestDay { get; set; }
		public Guid InvoiceSettlementDetailGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime? LastReceivedDate { get; set; }
		[StringLength(20)]
		public string OriginalDocumentId { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OriginalInterestCalcAmount { get; set; }
		public Guid? OriginalRefGUID  { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal PDCInterestOutstanding { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal PostedInterestAmount { get; set; }
		public int ProductType { get; set; }
		public Guid? RefGUID { get; set; }
		public int RefType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ReserveToBeRefund { get; set; }
		[Column(TypeName = "date")]
		public DateTime SettledDate { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettledInvoiceAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettledPurchaseAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettledReserveAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TotalRefundAdditionalIntAmount { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("ProductSettledTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("ProductSettledTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("InvoiceSettlementDetailGUID")]
		[InverseProperty("ProductSettledTransInvoiceSettlementDetail")]
		public InvoiceSettlementDetail InvoiceSettlementDetail { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
