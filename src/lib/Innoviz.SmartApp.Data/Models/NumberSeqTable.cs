using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class NumberSeqTable : CompanyBaseEntity
	{
		public NumberSeqTable()
		{
			CustGroupNumberSeqTable = new HashSet<CustGroup>();
			InvoiceNumberSeqSetupNumberSeqTable = new HashSet<InvoiceNumberSeqSetup>();
			NumberSeqParameterNumberSeqTable = new HashSet<NumberSeqParameter>();
			NumberSeqSegmentNumberSeqTable = new HashSet<NumberSeqSegment>();
			NumberSeqSetupByProductTypeCreditAppNumberSeq = new HashSet<NumberSeqSetupByProductType>();
			NumberSeqSetupByProductTypeCreditAppRequestNumberSeq = new HashSet<NumberSeqSetupByProductType>();
			NumberSeqSetupByProductTypeGuarantorAgreementNumberSeq = new HashSet<NumberSeqSetupByProductType>();
			NumberSeqSetupByProductTypeInternalGuarantorAgreementNumberSeq = new HashSet<NumberSeqSetupByProductType>();
			NumberSeqSetupByProductTypeInternalMainAgreementNumberSeq = new HashSet<NumberSeqSetupByProductType>();
			NumberSeqSetupByProductTypeMainAgreementNumberSeq = new HashSet<NumberSeqSetupByProductType>();
		}
 
		[Key]
		public Guid NumberSeqTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public int Largest { get; set; }
		public bool Manual { get; set; }
		public int Next { get; set; }
		[Required]
		[StringLength(20)]
		public string NumberSeqCode { get; set; }
		public int Smallest { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("NumberSeqTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("NumberSeqTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("CreditAppNumberSeq")]
		public ICollection<NumberSeqSetupByProductType> NumberSeqSetupByProductTypeCreditAppNumberSeq { get; set; }
		[InverseProperty("CreditAppRequestNumberSeq")]
		public ICollection<NumberSeqSetupByProductType> NumberSeqSetupByProductTypeCreditAppRequestNumberSeq { get; set; }
		[InverseProperty("GuarantorAgreementNumberSeq")]
		public ICollection<NumberSeqSetupByProductType> NumberSeqSetupByProductTypeGuarantorAgreementNumberSeq { get; set; }
		[InverseProperty("InternalGuarantorAgreementNumberSeq")]
		public ICollection<NumberSeqSetupByProductType> NumberSeqSetupByProductTypeInternalGuarantorAgreementNumberSeq { get; set; }
		[InverseProperty("InternalMainAgreementNumberSeq")]
		public ICollection<NumberSeqSetupByProductType> NumberSeqSetupByProductTypeInternalMainAgreementNumberSeq { get; set; }
		[InverseProperty("MainAgreementNumberSeq")]
		public ICollection<NumberSeqSetupByProductType> NumberSeqSetupByProductTypeMainAgreementNumberSeq { get; set; }
		[InverseProperty("NumberSeqTable")]
		public ICollection<CustGroup> CustGroupNumberSeqTable { get; set; }
		[InverseProperty("NumberSeqTable")]
		public ICollection<InvoiceNumberSeqSetup> InvoiceNumberSeqSetupNumberSeqTable { get; set; }
		[InverseProperty("NumberSeqTable")]
		public ICollection<NumberSeqParameter> NumberSeqParameterNumberSeqTable { get; set; }
		[InverseProperty("NumberSeqTable")]
		public ICollection<NumberSeqSegment> NumberSeqSegmentNumberSeqTable { get; set; }
		#endregion Collection
	}
}
