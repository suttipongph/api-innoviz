using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class NumberSeqSetupByProductType : CompanyBaseEntity
	{
		public NumberSeqSetupByProductType()
		{
		}
 
		[Key]
		public Guid NumberSeqSetupByProductTypeGUID { get; set; }
		public Guid? CreditAppNumberSeqGUID { get; set; }
		public Guid? CreditAppRequestNumberSeqGUID { get; set; }
		public Guid? GuarantorAgreementNumberSeqGUID { get; set; }
		public Guid? InternalGuarantorAgreementNumberSeqGUID { get; set; }
		public Guid? InternalMainAgreementNumberSeqGUID { get; set; }
		public Guid? MainAgreementNumberSeqGUID { get; set; }
		public Guid? TaxInvoiceNumberSeqGUID { get; set; }
		public Guid? TaxCreditNoteNumberSeqGUID { get; set; }
		public Guid? ReceiptNumberSeqGUID { get; set; }
		public int ProductType { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("NumberSeqSetupByProductTypeCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("NumberSeqSetupByProductTypeBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("CreditAppNumberSeqGUID")]
		[InverseProperty("NumberSeqSetupByProductTypeCreditAppNumberSeq")]
		public NumberSeqTable CreditAppNumberSeq { get; set; }
		[ForeignKey("CreditAppRequestNumberSeqGUID")]
		[InverseProperty("NumberSeqSetupByProductTypeCreditAppRequestNumberSeq")]
		public NumberSeqTable CreditAppRequestNumberSeq { get; set; }
		[ForeignKey("GuarantorAgreementNumberSeqGUID")]
		[InverseProperty("NumberSeqSetupByProductTypeGuarantorAgreementNumberSeq")]
		public NumberSeqTable GuarantorAgreementNumberSeq { get; set; }
		[ForeignKey("InternalGuarantorAgreementNumberSeqGUID")]
		[InverseProperty("NumberSeqSetupByProductTypeInternalGuarantorAgreementNumberSeq")]
		public NumberSeqTable InternalGuarantorAgreementNumberSeq { get; set; }
		[ForeignKey("InternalMainAgreementNumberSeqGUID")]
		[InverseProperty("NumberSeqSetupByProductTypeInternalMainAgreementNumberSeq")]
		public NumberSeqTable InternalMainAgreementNumberSeq { get; set; }
		[ForeignKey("MainAgreementNumberSeqGUID")]
		[InverseProperty("NumberSeqSetupByProductTypeMainAgreementNumberSeq")]
		public NumberSeqTable MainAgreementNumberSeq { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
