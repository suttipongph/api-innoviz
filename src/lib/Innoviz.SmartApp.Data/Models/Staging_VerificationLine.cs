﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_VerificationLine
	{
		[Key]
		[StringLength(50)]
		public string VerificationLineGUID { get; set; }
		public bool Pass { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		[StringLength(50)]
		public string VendorTableGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string VerificationTableGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string VerificationTypeGUID { get; set; }
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		[StringLength(50)]
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		[StringLength(50)]
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		public string OwnerBusinessUnitGUID { get; set; }
		public int MigrateStatus { get; set; }
		public int MigrateSource { get; set; }
	}
}
