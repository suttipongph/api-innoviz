using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class AddressPostalCode : CompanyBaseEntity
	{
		public AddressPostalCode()
		{
			AddressTransAddressPostalCode = new HashSet<AddressTrans>();
		}
		[Key]
		public Guid AddressPostalCodeGUID { get; set; }
		public Guid AddressSubDistrictGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string PostalCode { get; set; }
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("AddressPostalCodeCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("AddressPostalCodeBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("AddressSubDistrictGUID")]
		[InverseProperty("AddressPostalCodeAddressSubDistrict")]
		public AddressSubDistrict AddressSubDistrict { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("AddressPostalCode")]
		public ICollection<AddressTrans> AddressTransAddressPostalCode { get; set; }
		#endregion Collection
		
	}
}
