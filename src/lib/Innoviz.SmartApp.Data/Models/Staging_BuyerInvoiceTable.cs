﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_BuyerInvoiceTable
	{
		[Key]
		[StringLength(50)]
		public string BuyerInvoiceTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Amount { get; set; }
		[StringLength(50)]
		public string BuyerAgreementLineGUID { get; set; }
		[StringLength(50)]
		public string BuyerAgreementTableGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string BuyerInvoiceId { get; set; }
		[Required]
		[StringLength(50)]
		public string CreditAppLineGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string DueDate { get; set; }
		[Required]
		[StringLength(50)]
		public string InvoiceDate { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		[StringLength(50)]
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		[StringLength(50)]
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		public string OwnerBusinessUnitGUID { get; set; }
		public int MigrateStatus { get; set; }
		public int MigrateSource { get; set; }
	}
}
