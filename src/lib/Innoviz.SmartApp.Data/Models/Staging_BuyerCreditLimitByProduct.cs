﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_BuyerCreditLimitByProduct
	{
		[Key]
		[StringLength(50)]
		public string BuyerCreditLimitByProductGUID { get; set; }
		[StringLength(50)]
		public string BuyerTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CreditLimit { get; set; }
		public int ProductType { get; set; }

		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }

		[Required]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }

	}
}

