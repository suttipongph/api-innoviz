using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CompanyParameter : CompanyBaseEntity
	{
		public CompanyParameter()
		{
		}

        public override Guid CompanyGUID { get; set; }
        public bool CalcSheetIncTax { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal VehicleTaxServiceFee { get; set; }
        public bool CalcSheetIncAdditionalCost { get; set; }
        public int CollectionFeeMethod { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal CollectionFeeMinBalCal { get; set; }
        [StringLength(10)]
        public string NumberSeqVariable { get; set; }
        public int ReceiptGenBy { get; set; }
        public int InvoiceIssuingDay { get; set; }
        public int InvoiceIssuing { get; set; }
        public int PenaltyBase { get; set; }
        [StringLength(20)]
        public string AutoSettleToleranceLedgerAcc { get; set; }
        public int PaymStructureRefNum { get; set; }
        [StringLength(20)]
        public string EarlyToleranceOverLedgerAcc { get; set; }
        [StringLength(20)]
        public string EarlyToleranceUnderLedgerAcc { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal EarlyToleranceOverAmount { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal EarlyToleranceUnderAmount { get; set; }
        public int NPLMethod { get; set; }
        public bool BOTRealization { get; set; }
        public int ProvisionMethod { get; set; }
        public int ProvisionRateBy { get; set; }
        [StringLength(20)]
        public string RealizedGainExchRateLedgerAcc { get; set; }
        [StringLength(20)]
        public string RealizedLossExchRateLedgerAcc { get; set; }
        [Key]
        public Guid CompanyParameterGUID { get; set; }
        public Guid? HomeCurrencyGUID { get; set; }
        public Guid? PenaltyInvTypeGUID { get; set; }
        public Guid? VehicleTaxInvTypeGUID { get; set; }
        public Guid? InitInvAdvInvTypeGUID { get; set; }
        public Guid? InitInvDownInvTypeGUID { get; set; }
        public Guid? InstallInvTypeGUID { get; set; }
        public Guid? InsuranceInvTypeGUID { get; set; }
        public Guid? CompulsoryInvTypeGUID { get; set; }
        public Guid? CollectionInvTypeGUID { get; set; }
        public Guid? InstallAdvInvTypeGUID { get; set; }
        public Guid? InitInvDepInvTypeGUID { get; set; }
        public Guid? DepositReturnInvTypeGUID { get; set; }
        public Guid? DiscountMethodOfPaymentGUID { get; set; }
        public Guid? WaiveMethodOfPaymentGUID { get; set; }
        public Guid? CompulsoryTaxGUID { get; set; }
        public Guid? InsuranceTaxGUID { get; set; }
        public Guid? MaintenanceTaxGUID { get; set; }
        public Guid? VehicleTaxServiceFeeTaxGUID { get; set; }
        public int CollectionOverdueDay { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal MaximumPennyDifference { get; set; }
        [StringLength(20)]
        public string PennyDifferenceLedgerAcc { get; set; }
        public int OverdueType { get; set; }
        public int PenaltyCalculationMethod { get; set; }
        public bool SettleByPeriod { get; set; }
        public int SettlementGraceDay { get; set; }
        public bool AgreementType { get; set; }
        public bool LeaseSubType { get; set; }
        public bool LeaseType { get; set; }
        public bool Product { get; set; }
        public bool ProductGroup { get; set; }
        public bool RecordType { get; set; }
        public Guid? EarlyPayOffToleranceInvTypeGUID { get; set; }
        public Guid? EarlyToleranceMethodOfPaymentGUID { get; set; }
        //Add new 
        public Guid? CompanyCalendarGroupGUID { get; set; }
        public Guid? BOTCalendarGroupGUID { get; set; }
        public Guid? CreditReqInvRevenueTypeGUID { get; set; }
        public Guid? ServiceFeeInvTypeGUID { get; set; }
        public Guid? SettlementMethodOfPaymentGUID { get; set; }
        public Guid? SuspenseMethodOfPaymentGUID { get; set; }
        public Guid? FTBillingFeeInvRevenueTypeGUID { get; set; }
        public Guid? FTInterestInvTypeGUID { get; set; }
        public Guid? FTInterestRefundInvTypeGUID { get; set; }
        public Guid? FTFeeInvTypeGUID { get; set; }
        public Guid? FTInvTypeGUID { get; set; }
        public Guid? FTReceiptFeeInvRevenueTypeGUID { get; set; }
        public Guid? FTReserveInvRevenueTypeGUID { get; set; }
        public Guid? FTReserveToBeRefundedInvTypeGUID { get; set; }
        public Guid? FTRetentionInvTypeGUID { get; set; }
        public Guid? FTUnearnedInterestInvTypeGUID { get; set; }
        public Guid? PFInvTypeGUID { get; set; }
        public Guid? PFUnearnedInterestInvTypeGUID { get; set; }
        public Guid? PFInterestInvTypeGUID { get; set; }
        public Guid? PFInterestRefundInvTypeGUID { get; set; }
        public Guid? PFRetentionInvTypeGUID { get; set; }
        public Guid? PFExtendCreditTermGUID { get; set; }
        public Guid? PFTermExtensionInvRevenueTypeGUID { get; set; }
        public Guid? BondRetentionInvTypeGUID { get; set; }
        public Guid? LCRetentionInvTypeGUID { get; set; }
        public int BuyerReviewedDay { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal ChequeToleranceAmount { get; set; }
        public int DaysPerYear { get; set; }
        public int MaxCopyFinancialStatementNumOfYear { get; set; }
        public int MaxCopyTaxReportNumOfMonth { get; set; }
        [Column(TypeName = "decimal(5, 2)")]
        public decimal MaxInterestPct { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal MinUpcountryChequeFeeAmount { get; set; }
        [Column(TypeName = "decimal(5, 2)")]
        public decimal UpcountryChequeFeePct { get; set; }
        public int FTMaxRollbillInterestDay { get; set; }
        public int FTMinInterestDay { get; set; }
        public int FTRollbillInterestDay { get; set; }
        public int PFInterestCutDay { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal PFTermExtensionDaysPerFeeAmount { get; set; }
        public int PFMaxPostponeCollectionDay { get; set; }
        public Guid? FTRollbillUnearnedInterestInvTypeGUID { get; set; }
        public Guid? OperReportSignatureGUID { get; set; }
        public int FTMinSettleInterestDayMaxPct { get; set; }
        public int NormalAccruedIntDay { get; set; }
        public int MaxAccruedIntDay { get; set; }

        [ForeignKey("CompanyCalendarGroupGUID")]
        [InverseProperty("CompanyParameterCompanyCalendarGroup")]
        public CalendarGroup CompanyCalendarGroup { get; set; }
        [ForeignKey("BOTCalendarGroupGUID")]
        [InverseProperty("CompanyParameterBOTCalendarGroup")]
        public CalendarGroup BOTCalendarGroup { get; set; }
        [ForeignKey("CreditReqInvRevenueTypeGUID")]
        [InverseProperty("CompanyParameterCreditReqInvRevenueType")]
        public InvoiceRevenueType CreditReqInvRevenueType { get; set; }
        [ForeignKey("ServiceFeeInvTypeGUID")]
        [InverseProperty("CompanyParameterServiceFeeInvType")]
        public InvoiceType ServiceFeeInvType { get; set; }
        [ForeignKey("SettlementMethodOfPaymentGUID")]
        [InverseProperty("CompanyParameterSettlementMethodOfPayment")]
        public MethodOfPayment SettlementMethodOfPayment { get; set; }
        [ForeignKey("SuspenseMethodOfPaymentGUID")]
        [InverseProperty("CompanyParameterSuspenseMethodOfPayment")]
        public MethodOfPayment SuspenseMethodOfPayment { get; set; }
        [ForeignKey("FTBillingFeeInvRevenueTypeGUID")]
        [InverseProperty("CompanyParameterFTBillingFeeInvRevenueType")]
        public InvoiceRevenueType FTBillingFeeInvRevenueType { get; set; }
        [ForeignKey("FTInterestInvTypeGUID")]
        [InverseProperty("CompanyParameterFTInterestInvType")]
        public InvoiceType FTInterestInvType { get; set; }
        [ForeignKey("FTInterestRefundInvTypeGUID")]
        [InverseProperty("CompanyParameterFTInterestRefundInvType")]
        public InvoiceType FTInterestRefundInvType { get; set; }
        [ForeignKey("FTFeeInvTypeGUID")]
        [InverseProperty("CompanyParameterFTFeeInvType")]
        public InvoiceType FTFeeInvType { get; set; }
        [ForeignKey("FTInvTypeGUID")]
        [InverseProperty("CompanyParameterFTInvType")]
        public InvoiceType FTInvType { get; set; }
        [ForeignKey("FTReceiptFeeInvRevenueTypeGUID")]
        [InverseProperty("CompanyParameterFTReceiptFeeInvRevenueType")]
        public InvoiceRevenueType FTReceiptFeeInvRevenueType { get; set; }
        [ForeignKey("FTReserveInvRevenueTypeGUID")]
        [InverseProperty("CompanyParameterFTReserveInvRevenueType")]
        public InvoiceRevenueType FTReserveInvRevenueType { get; set; }
        [ForeignKey("FTReserveToBeRefundedInvTypeGUID")]
        [InverseProperty("CompanyParameterFTReserveToBeRefundedInvType")]
        public InvoiceType FTReserveToBeRefundedInvType { get; set; }
        [ForeignKey("FTRetentionInvTypeGUID")]
        [InverseProperty("CompanyParameterFTRetentionInvType")]
        public InvoiceType FTRetentionInvType { get; set; }
        [ForeignKey("FTUnearnedInterestInvTypeGUID")]
        [InverseProperty("CompanyParameterFTUnearnedInterestInvType")]
        public InvoiceType FTUnearnedInterestInvType { get; set; }
        [ForeignKey("PFInvTypeGUID")]
        [InverseProperty("CompanyParameterPFInvType")]
        public InvoiceType PFInvType { get; set; }
        [ForeignKey("PFUnearnedInterestInvTypeGUID")]
        [InverseProperty("CompanyParameterPFUnearnedInterestInvType")]
        public InvoiceType PFUnearnedInterestInvType { get; set; }
        [ForeignKey("PFInterestInvTypeGUID")]
        [InverseProperty("CompanyParameterPFInterestInvType")]
        public InvoiceType PFInterestInvType { get; set; }
        [ForeignKey("PFInterestRefundInvTypeGUID")]
        [InverseProperty("CompanyParameterPFInterestRefundInvType")]
        public InvoiceType PFInterestRefundInvType { get; set; }
        [ForeignKey("PFRetentionInvTypeGUID")]
        [InverseProperty("CompanyParameterPFRetentionInvType")]
        public InvoiceType PFRetentionInvType { get; set; }
        [ForeignKey("PFExtendCreditTermGUID")]
        [InverseProperty("CompanyParameterPFExtendCreditTerm")]
        public CreditTerm PFExtendCreditTerm { get; set; }
        [ForeignKey("PFTermExtensionInvRevenueTypeGUID")]
        [InverseProperty("CompanyParameterPFTermExtensionInvRevenueType")]
        public InvoiceRevenueType PFTermExtensionInvRevenueType { get; set; }
        [ForeignKey("BondRetentionInvTypeGUID")]
        [InverseProperty("CompanyParameterBondRetentionInvType")]
        public InvoiceType BondRetentionInvType { get; set; }
        [ForeignKey("LCRetentionInvTypeGUID")]
        [InverseProperty("CompanyParameterLCRetentionInvType")]
        public InvoiceType LCRetentionInvType { get; set; }
        [ForeignKey("FTRollbillUnearnedInterestInvTypeGUID")]
        [InverseProperty("CompanyParameterFTRollbillUnearnedInterestInvType")]
        public InvoiceType FTRollbillUnearnedInterestInvType { get; set; }
        [ForeignKey("OperReportSignatureGUID")]
        [InverseProperty("CompanyParameterOperReportSignature")]
        public EmployeeTable OperReportSignature { get; set; }

        #region ForeignKey
        [ForeignKey("CompanyGUID")]
		[InverseProperty("CompanyParameterCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CompanyParameterBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("CollectionInvTypeGUID")]
		[InverseProperty("CompanyParameterCollectionInvType")]
		public InvoiceType CollectionInvType { get; set; }
		[ForeignKey("CompulsoryInvTypeGUID")]
		[InverseProperty("CompanyParameterCompulsoryInvType")]
		public InvoiceType CompulsoryInvType { get; set; }
		[ForeignKey("CompulsoryTaxGUID")]
		[InverseProperty("CompanyParameterCompulsoryTax")]
		public TaxTable CompulsoryTax { get; set; }
		[ForeignKey("DepositReturnInvTypeGUID")]
		[InverseProperty("CompanyParameterDepositReturnInvType")]
		public InvoiceType DepositReturnInvType { get; set; }
		[ForeignKey("DiscountMethodOfPaymentGUID")]
		[InverseProperty("CompanyParameterDiscountMethodOfPayment")]
		public MethodOfPayment DiscountMethodOfPayment { get; set; }
		[ForeignKey("EarlyPayOffToleranceInvTypeGUID")]
		[InverseProperty("CompanyParameterEarlyPayOffToleranceInvType")]
		public InvoiceType EarlyPayOffToleranceInvType { get; set; }
		[ForeignKey("EarlyToleranceMethodOfPaymentGUID")]
		[InverseProperty("CompanyParameterEarlyToleranceMethodOfPayment")]
		public MethodOfPayment EarlyToleranceMethodOfPayment { get; set; }
		[ForeignKey("HomeCurrencyGUID")]
		[InverseProperty("CompanyParameterHomeCurrency")]
		public Currency HomeCurrency { get; set; }
		[ForeignKey("InitInvAdvInvTypeGUID")]
		[InverseProperty("CompanyParameterInitInvAdvInvType")]
		public InvoiceType InitInvAdvInvType { get; set; }
		[ForeignKey("InitInvDepInvTypeGUID")]
		[InverseProperty("CompanyParameterInitInvDepInvType")]
		public InvoiceType InitInvDepInvType { get; set; }
		[ForeignKey("InitInvDownInvTypeGUID")]
		[InverseProperty("CompanyParameterInitInvDownInvType")]
		public InvoiceType InitInvDownInvType { get; set; }
		[ForeignKey("InstallAdvInvTypeGUID")]
		[InverseProperty("CompanyParameterInstallAdvInvType")]
		public InvoiceType InstallAdvInvType { get; set; }
		[ForeignKey("InstallInvTypeGUID")]
		[InverseProperty("CompanyParameterInstallInvType")]
		public InvoiceType InstallInvType { get; set; }
		[ForeignKey("InsuranceInvTypeGUID")]
		[InverseProperty("CompanyParameterInsuranceInvType")]
		public InvoiceType InsuranceInvType { get; set; }
		[ForeignKey("InsuranceTaxGUID")]
		[InverseProperty("CompanyParameterInsuranceTax")]
		public TaxTable InsuranceTax { get; set; }
		[ForeignKey("MaintenanceTaxGUID")]
		[InverseProperty("CompanyParameterMaintenanceTax")]
		public TaxTable MaintenanceTax { get; set; }
		[ForeignKey("PenaltyInvTypeGUID")]
		[InverseProperty("CompanyParameterPenaltyInvType")]
		public InvoiceType PenaltyInvType { get; set; }
		[ForeignKey("VehicleTaxInvTypeGUID")]
		[InverseProperty("CompanyParameterVehicleTaxInvType")]
		public InvoiceType VehicleTaxInvType { get; set; }
		[ForeignKey("VehicleTaxServiceFeeTaxGUID")]
		[InverseProperty("CompanyParameterVehicleTaxServiceFeeTax")]
		public TaxTable VehicleTaxServiceFeeTax { get; set; }
		[ForeignKey("WaiveMethodOfPaymentGUID")]
		[InverseProperty("CompanyParameterWaiveMethodOfPayment")]
		public MethodOfPayment WaiveMethodOfPayment { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
