using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class EmployeeTable : CompanyBaseEntity
	{
		public EmployeeTable()
		{
			AgreementTableInfoAuthorizedPersonTransCompany1 = new HashSet<AgreementTableInfo>();
			AgreementTableInfoAuthorizedPersonTransCompany2 = new HashSet<AgreementTableInfo>();
			AgreementTableInfoAuthorizedPersonTransCompany3 = new HashSet<AgreementTableInfo>();
			AgreementTableInfoWitnessCompany1 = new HashSet<AgreementTableInfo>();
			AgreementTableInfoWitnessCompany2 = new HashSet<AgreementTableInfo>();
			CompanyParameterOperReportSignature = new HashSet<CompanyParameter>();
			CompanySignatureEmployeeTable = new HashSet<CompanySignature>();
			CustomerTableResponsibleBy = new HashSet<CustomerTable>();
			CustVisitingTransResponsibleBy = new HashSet<CustVisitingTrans>();
			DocumentReturnTableRequestor = new HashSet<DocumentReturnTable>();
			DocumentReturnTableReturnBy = new HashSet<DocumentReturnTable>();
			GuarantorAgreementTableWitnessCompany1 = new HashSet<GuarantorAgreementTable>();
			GuarantorAgreementTableWitnessCompany2 = new HashSet<GuarantorAgreementTable>();
			MessengerJobTableRequestor = new HashSet<MessengerJobTable>();
			WithdrawalTableEmployeeTable = new HashSet<WithdrawalTable>();
			CustomerRefundTableEmployeeTable = new HashSet<CustomerRefundTable>();
			PurchaseTableEmployeeTable = new HashSet<PurchaseTable>();
		}

		[Required]
		[StringLength(20)]
		public string EmployeeId { get; set; }
		[Required]
		[StringLength(100)]
		public string Name { get; set; }
		[Key]
		public Guid EmployeeTableGUID { get; set; }
		public override Guid CompanyGUID { get; set; }
		public Guid? EmplTeamGUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? DepartmentGUID { get; set; }
		public bool AssistMD { get; set; }
		public bool InActive { get; set; }
		public string Signature { get; set; }
		public Guid? UserId { get; set; }

		#region LIT
		public Guid? BusinessUnitGUID { get; set; }
		public Guid? ReportToEmployeeTableGUID { get; set; }
		public string Position { get; set; }
		#endregion LIT

		[ForeignKey("UserId")]
		[InverseProperty("EmployeeTable")]
		public SysUserTable User { get; set; }

		#region LIT
		[ForeignKey("BusinessUnitGUID")]
		[InverseProperty("EmployeeTable")]
		public BusinessUnit BusinessUnitGU { get; set; }
		#endregion LIT

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("EmployeeTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("EmployeeTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("Dimension1GUID")]
		[InverseProperty("EmployeeTableDimension1")]
		public LedgerDimension LedgerDimension1 { get; set; }
		[ForeignKey("Dimension2GUID")]
		[InverseProperty("EmployeeTableDimension2")]
		public LedgerDimension LedgerDimension2 { get; set; }
		[ForeignKey("Dimension3GUID")]
		[InverseProperty("EmployeeTableDimension3")]
		public LedgerDimension LedgerDimension3 { get; set; }
		[ForeignKey("Dimension4GUID")]
		[InverseProperty("EmployeeTableDimension4")]
		public LedgerDimension LedgerDimension4 { get; set; }
		[ForeignKey("Dimension5GUID")]
		[InverseProperty("EmployeeTableDimension5")]
		public LedgerDimension LedgerDimension5 { get; set; }
		[ForeignKey("EmplTeamGUID")]
		[InverseProperty("EmployeeTableEmplTeam")]
		public EmplTeam EmplTeam { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("AuthorizedPersonTransCompany1")]
		public ICollection<AgreementTableInfo> AgreementTableInfoAuthorizedPersonTransCompany1 { get; set; }
		[InverseProperty("AuthorizedPersonTransCompany2")]
		public ICollection<AgreementTableInfo> AgreementTableInfoAuthorizedPersonTransCompany2 { get; set; }
		[InverseProperty("AuthorizedPersonTransCompany3")]
		public ICollection<AgreementTableInfo> AgreementTableInfoAuthorizedPersonTransCompany3 { get; set; }
		[InverseProperty("OperReportSignature")]
		public ICollection<CompanyParameter> CompanyParameterOperReportSignature { get; set; }
		[InverseProperty("EmployeeTable")]
		public ICollection<CompanySignature> CompanySignatureEmployeeTable { get; set; }
		[InverseProperty("Requestor")]
		public ICollection<MessengerJobTable> MessengerJobTableRequestor { get; set; }
		[InverseProperty("ResponsibleBy")]
		public ICollection<CustomerTable> CustomerTableResponsibleBy { get; set; }
		[InverseProperty("ResponsibleBy")]
		public ICollection<CustVisitingTrans> CustVisitingTransResponsibleBy { get; set; }
		[InverseProperty("Requestor")]
		public ICollection<DocumentReturnTable> DocumentReturnTableRequestor { get; set; }
		[InverseProperty("ReturnBy")]
		public ICollection<DocumentReturnTable> DocumentReturnTableReturnBy { get; set; }
		[InverseProperty("WitnessCompany1")]
		public ICollection<AgreementTableInfo> AgreementTableInfoWitnessCompany1 { get; set; }
		[InverseProperty("WitnessCompany1")]
		public ICollection<GuarantorAgreementTable> GuarantorAgreementTableWitnessCompany1 { get; set; }
		[InverseProperty("WitnessCompany2")]
		public ICollection<AgreementTableInfo> AgreementTableInfoWitnessCompany2 { get; set; }
		[InverseProperty("WitnessCompany2")]
		public ICollection<GuarantorAgreementTable> GuarantorAgreementTableWitnessCompany2 { get; set; }
		[InverseProperty("EmployeeTable")]
		public ICollection<WithdrawalTable> WithdrawalTableEmployeeTable { get; set; }
		[InverseProperty("EmployeeTable")]
		public ICollection<CustomerRefundTable> CustomerRefundTableEmployeeTable { get; set; }
		[InverseProperty("EmployeeTable")]
		public ICollection<PurchaseTable> PurchaseTableEmployeeTable { get; set; }
		#endregion Collection
	}
}
