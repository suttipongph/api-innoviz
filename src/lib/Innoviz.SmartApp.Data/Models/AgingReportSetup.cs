using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class AgingReportSetup : CompanyBaseEntity {
		[Key]
		public Guid AgingReportSetupGUID { get; set; }
		[StringLength(100)]
		public string Description { get; set; }
		public int LineNum { get; set; }
		public int MaximumDays { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("AgingReportSetupCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("AgingReportSetupBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
	}
}
