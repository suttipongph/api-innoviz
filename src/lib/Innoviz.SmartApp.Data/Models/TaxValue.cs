using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class TaxValue : DateEffectiveBaseEntity
	{
		public TaxValue()
		{
		}
 
		[Key]
		public Guid TaxValueGUID { get; set; }
		public Guid TaxTableGUID { get; set; }
		[Column(TypeName = "decimal")]
		public decimal Value { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("TaxValueCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("TaxValueBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("TaxTableGUID")]
		[InverseProperty("TaxValueTaxTable")]
		public TaxTable TaxTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
