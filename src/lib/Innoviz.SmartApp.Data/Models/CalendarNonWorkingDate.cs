using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CalendarNonWorkingDate : CompanyBaseEntity
	{
		public CalendarNonWorkingDate()
		{
		}
 
		[Key]
		public Guid CalendarNonWorkingDateGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime CalendarDate { get; set; }
		public Guid CalendarGroupGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public int HolidayType { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CalendarNonWorkingDateCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CalendarNonWorkingDateBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("CalendarGroupGUID")]
		[InverseProperty("CalendarNonWorkingDateCalendarGroup")]
		public CalendarGroup CalendarGroup { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
