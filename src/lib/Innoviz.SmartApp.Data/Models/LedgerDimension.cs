using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class LedgerDimension : CompanyBaseEntity
	{
		public LedgerDimension()
		{
			ApplicationTableDimension1 = new HashSet<ApplicationTable>();
			ApplicationTableDimension2 = new HashSet<ApplicationTable>();
			ApplicationTableDimension3 = new HashSet<ApplicationTable>();
			ApplicationTableDimension4 = new HashSet<ApplicationTable>();
			ApplicationTableDimension5 = new HashSet<ApplicationTable>();
			CreditAppRequestTableDimension1 = new HashSet<CreditAppRequestTable>();
			CreditAppRequestTableDimension2 = new HashSet<CreditAppRequestTable>();
			CreditAppRequestTableDimension3 = new HashSet<CreditAppRequestTable>();
			CreditAppRequestTableDimension4 = new HashSet<CreditAppRequestTable>();
			CreditAppRequestTableDimension5 = new HashSet<CreditAppRequestTable>();
			CreditAppTableDimension1 = new HashSet<CreditAppTable>();
			CreditAppTableDimension2 = new HashSet<CreditAppTable>();
			CreditAppTableDimension3 = new HashSet<CreditAppTable>();
			CreditAppTableDimension4 = new HashSet<CreditAppTable>();
			CreditAppTableDimension5 = new HashSet<CreditAppTable>();
			CustomerRefundTableDimension1 = new HashSet<CustomerRefundTable>();
			CustomerRefundTableDimension2 = new HashSet<CustomerRefundTable>();
			CustomerRefundTableDimension3 = new HashSet<CustomerRefundTable>();
			CustomerRefundTableDimension4 = new HashSet<CustomerRefundTable>();
			CustomerRefundTableDimension5 = new HashSet<CustomerRefundTable>();
			CustomerTableDimension1 = new HashSet<CustomerTable>();
			CustomerTableDimension2 = new HashSet<CustomerTable>();
			CustomerTableDimension3 = new HashSet<CustomerTable>();
			CustomerTableDimension4 = new HashSet<CustomerTable>();
			CustomerTableDimension5 = new HashSet<CustomerTable>();
			EmployeeTableDimension1 = new HashSet<EmployeeTable>();
			EmployeeTableDimension2 = new HashSet<EmployeeTable>();
			EmployeeTableDimension3 = new HashSet<EmployeeTable>();
			EmployeeTableDimension4 = new HashSet<EmployeeTable>();
			EmployeeTableDimension5 = new HashSet<EmployeeTable>();
			FreeTextInvoiceTableDimension1 = new HashSet<FreeTextInvoiceTable>();
			FreeTextInvoiceTableDimension2 = new HashSet<FreeTextInvoiceTable>();
			FreeTextInvoiceTableDimension3 = new HashSet<FreeTextInvoiceTable>();
			FreeTextInvoiceTableDimension4 = new HashSet<FreeTextInvoiceTable>();
			FreeTextInvoiceTableDimension5 = new HashSet<FreeTextInvoiceTable>();
			IntercompanyInvoiceTableDimension1 = new HashSet<IntercompanyInvoiceTable>();
			IntercompanyInvoiceTableDimension2 = new HashSet<IntercompanyInvoiceTable>();
			IntercompanyInvoiceTableDimension3 = new HashSet<IntercompanyInvoiceTable>();
			IntercompanyInvoiceTableDimension4 = new HashSet<IntercompanyInvoiceTable>();
			IntercompanyInvoiceTableDimension5 = new HashSet<IntercompanyInvoiceTable>();
			InvoiceLineDimension1 = new HashSet<InvoiceLine>();
			InvoiceLineDimension2 = new HashSet<InvoiceLine>();
			InvoiceLineDimension3 = new HashSet<InvoiceLine>();
			InvoiceLineDimension4 = new HashSet<InvoiceLine>();
			InvoiceLineDimension5 = new HashSet<InvoiceLine>();
			InvoiceTableDimension1 = new HashSet<InvoiceTable>();
			InvoiceTableDimension2 = new HashSet<InvoiceTable>();
			InvoiceTableDimension3 = new HashSet<InvoiceTable>();
			InvoiceTableDimension4 = new HashSet<InvoiceTable>();
			InvoiceTableDimension5 = new HashSet<InvoiceTable>();
			InterestRealizedTransDimension1 = new HashSet<InterestRealizedTrans>();
			InterestRealizedTransDimension2 = new HashSet<InterestRealizedTrans>();
			InterestRealizedTransDimension3 = new HashSet<InterestRealizedTrans>();
			InterestRealizedTransDimension4 = new HashSet<InterestRealizedTrans>();
			InterestRealizedTransDimension5 = new HashSet<InterestRealizedTrans>();
			PurchaseLineDimension1 = new HashSet<PurchaseLine>();
			PurchaseLineDimension2 = new HashSet<PurchaseLine>();
			PurchaseLineDimension3 = new HashSet<PurchaseLine>();
			PurchaseLineDimension4 = new HashSet<PurchaseLine>();
			PurchaseLineDimension5 = new HashSet<PurchaseLine>();
			PurchaseTableDimension1 = new HashSet<PurchaseTable>();
			PurchaseTableDimension2 = new HashSet<PurchaseTable>();
			PurchaseTableDimension3 = new HashSet<PurchaseTable>();
			PurchaseTableDimension4 = new HashSet<PurchaseTable>();
			PurchaseTableDimension5 = new HashSet<PurchaseTable>();
			ReceiptTempTableDimension1 = new HashSet<ReceiptTempTable>();
			ReceiptTempTableDimension2 = new HashSet<ReceiptTempTable>();
			ReceiptTempTableDimension3 = new HashSet<ReceiptTempTable>();
			ReceiptTempTableDimension4 = new HashSet<ReceiptTempTable>();
			ReceiptTempTableDimension5 = new HashSet<ReceiptTempTable>();
			ServiceFeeTransDimension1 = new HashSet<ServiceFeeTrans>();
			ServiceFeeTransDimension2 = new HashSet<ServiceFeeTrans>();
			ServiceFeeTransDimension3 = new HashSet<ServiceFeeTrans>();
			ServiceFeeTransDimension4 = new HashSet<ServiceFeeTrans>();
			ServiceFeeTransDimension5 = new HashSet<ServiceFeeTrans>();
			TaxInvoiceLineDimension1 = new HashSet<TaxInvoiceLine>();
			TaxInvoiceLineDimension2 = new HashSet<TaxInvoiceLine>();
			TaxInvoiceLineDimension3 = new HashSet<TaxInvoiceLine>();
			TaxInvoiceLineDimension4 = new HashSet<TaxInvoiceLine>();
			TaxInvoiceLineDimension5 = new HashSet<TaxInvoiceLine>();
			TaxInvoiceTableDimension1 = new HashSet<TaxInvoiceTable>();
			TaxInvoiceTableDimension2 = new HashSet<TaxInvoiceTable>();
			TaxInvoiceTableDimension3 = new HashSet<TaxInvoiceTable>();
			TaxInvoiceTableDimension4 = new HashSet<TaxInvoiceTable>();
			TaxInvoiceTableDimension5 = new HashSet<TaxInvoiceTable>();
			VendorPaymentTransDimension1 = new HashSet<VendorPaymentTrans>();
			VendorPaymentTransDimension2 = new HashSet<VendorPaymentTrans>();
			VendorPaymentTransDimension3 = new HashSet<VendorPaymentTrans>();
			VendorPaymentTransDimension4 = new HashSet<VendorPaymentTrans>();
			VendorPaymentTransDimension5 = new HashSet<VendorPaymentTrans>();
			WithdrawalLineDimension1 = new HashSet<WithdrawalLine>();
			WithdrawalLineDimension2 = new HashSet<WithdrawalLine>();
			WithdrawalLineDimension3 = new HashSet<WithdrawalLine>();
			WithdrawalLineDimension4 = new HashSet<WithdrawalLine>();
			WithdrawalLineDimension5 = new HashSet<WithdrawalLine>();
			WithdrawalTableDimension1 = new HashSet<WithdrawalTable>();
			WithdrawalTableDimension2 = new HashSet<WithdrawalTable>();
			WithdrawalTableDimension3 = new HashSet<WithdrawalTable>();
			WithdrawalTableDimension4 = new HashSet<WithdrawalTable>();
			WithdrawalTableDimension5 = new HashSet<WithdrawalTable>();
		}
 
		[Key]
		public Guid LedgerDimensionGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public int Dimension { get; set; }
		[Required]
		[StringLength(20)]
		public string DimensionCode { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("LedgerDimensionCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("LedgerDimensionBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("LedgerDimension1")]
		public ICollection<ApplicationTable> ApplicationTableDimension1 { get; set; }
		[InverseProperty("LedgerDimension1")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTableDimension1 { get; set; }
		[InverseProperty("LedgerDimension1")]
		public ICollection<CreditAppTable> CreditAppTableDimension1 { get; set; }
		[InverseProperty("LedgerDimension1")]
		public ICollection<CustomerRefundTable> CustomerRefundTableDimension1 { get; set; }
		[InverseProperty("LedgerDimension1")]
		public ICollection<CustomerTable> CustomerTableDimension1 { get; set; }
		[InverseProperty("LedgerDimension1")]
		public ICollection<EmployeeTable> EmployeeTableDimension1 { get; set; }
		[InverseProperty("LedgerDimension1")]
		public ICollection<FreeTextInvoiceTable> FreeTextInvoiceTableDimension1 { get; set; }
		[InverseProperty("LedgerDimension1")]
		public ICollection<IntercompanyInvoiceTable> IntercompanyInvoiceTableDimension1 { get; set; }
		[InverseProperty("LedgerDimension1")]
		public ICollection<InvoiceLine> InvoiceLineDimension1 { get; set; }
		[InverseProperty("LedgerDimension1")]
		public ICollection<InvoiceTable> InvoiceTableDimension1 { get; set; }
		[InverseProperty("LedgerDimension1")]
		public ICollection<InterestRealizedTrans> InterestRealizedTransDimension1 { get; set; }
		[InverseProperty("LedgerDimension1")]
		public ICollection<PurchaseLine> PurchaseLineDimension1 { get; set; }
		[InverseProperty("LedgerDimension1")]
		public ICollection<PurchaseTable> PurchaseTableDimension1 { get; set; }
		[InverseProperty("LedgerDimension1")]
		public ICollection<ReceiptTempTable> ReceiptTempTableDimension1 { get; set; }
		[InverseProperty("LedgerDimension1")]
		public ICollection<ServiceFeeTrans> ServiceFeeTransDimension1 { get; set; }
		[InverseProperty("LedgerDimension1")]
		public ICollection<TaxInvoiceLine> TaxInvoiceLineDimension1 { get; set; }
		[InverseProperty("LedgerDimension1")]
		public ICollection<TaxInvoiceTable> TaxInvoiceTableDimension1 { get; set; }
		[InverseProperty("LedgerDimension1")]
		public ICollection<VendorPaymentTrans> VendorPaymentTransDimension1 { get; set; }
		[InverseProperty("LedgerDimension1")]
		public ICollection<WithdrawalLine> WithdrawalLineDimension1 { get; set; }
		[InverseProperty("LedgerDimension1")]
		public ICollection<WithdrawalTable> WithdrawalTableDimension1 { get; set; }
		[InverseProperty("LedgerDimension2")]
		public ICollection<ApplicationTable> ApplicationTableDimension2 { get; set; }
		[InverseProperty("LedgerDimension2")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTableDimension2 { get; set; }
		[InverseProperty("LedgerDimension2")]
		public ICollection<CreditAppTable> CreditAppTableDimension2 { get; set; }
		[InverseProperty("LedgerDimension2")]
		public ICollection<CustomerRefundTable> CustomerRefundTableDimension2 { get; set; }
		[InverseProperty("LedgerDimension2")]
		public ICollection<CustomerTable> CustomerTableDimension2 { get; set; }
		[InverseProperty("LedgerDimension2")]
		public ICollection<EmployeeTable> EmployeeTableDimension2 { get; set; }
		[InverseProperty("LedgerDimension2")]
		public ICollection<FreeTextInvoiceTable> FreeTextInvoiceTableDimension2 { get; set; }
		[InverseProperty("LedgerDimension2")]
		public ICollection<IntercompanyInvoiceTable> IntercompanyInvoiceTableDimension2 { get; set; }
		[InverseProperty("LedgerDimension2")]
		public ICollection<InvoiceLine> InvoiceLineDimension2 { get; set; }
		[InverseProperty("LedgerDimension2")]
		public ICollection<InvoiceTable> InvoiceTableDimension2 { get; set; }
		[InverseProperty("LedgerDimension2")]
		public ICollection<InterestRealizedTrans> InterestRealizedTransDimension2 { get; set; }
		[InverseProperty("LedgerDimension2")]
		public ICollection<PurchaseLine> PurchaseLineDimension2 { get; set; }
		[InverseProperty("LedgerDimension2")]
		public ICollection<PurchaseTable> PurchaseTableDimension2 { get; set; }
		[InverseProperty("LedgerDimension2")]
		public ICollection<ReceiptTempTable> ReceiptTempTableDimension2 { get; set; }
		[InverseProperty("LedgerDimension2")]
		public ICollection<ServiceFeeTrans> ServiceFeeTransDimension2 { get; set; }
		[InverseProperty("LedgerDimension2")]
		public ICollection<TaxInvoiceLine> TaxInvoiceLineDimension2 { get; set; }
		[InverseProperty("LedgerDimension2")]
		public ICollection<TaxInvoiceTable> TaxInvoiceTableDimension2 { get; set; }
		[InverseProperty("LedgerDimension2")]
		public ICollection<VendorPaymentTrans> VendorPaymentTransDimension2 { get; set; }
		[InverseProperty("LedgerDimension2")]
		public ICollection<WithdrawalLine> WithdrawalLineDimension2 { get; set; }
		[InverseProperty("LedgerDimension2")]
		public ICollection<WithdrawalTable> WithdrawalTableDimension2 { get; set; }
		[InverseProperty("LedgerDimension3")]
		public ICollection<ApplicationTable> ApplicationTableDimension3 { get; set; }
		[InverseProperty("LedgerDimension3")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTableDimension3 { get; set; }
		[InverseProperty("LedgerDimension3")]
		public ICollection<CreditAppTable> CreditAppTableDimension3 { get; set; }
		[InverseProperty("LedgerDimension3")]
		public ICollection<CustomerRefundTable> CustomerRefundTableDimension3 { get; set; }
		[InverseProperty("LedgerDimension3")]
		public ICollection<CustomerTable> CustomerTableDimension3 { get; set; }
		[InverseProperty("LedgerDimension3")]
		public ICollection<EmployeeTable> EmployeeTableDimension3 { get; set; }
		[InverseProperty("LedgerDimension3")]
		public ICollection<FreeTextInvoiceTable> FreeTextInvoiceTableDimension3 { get; set; }
		[InverseProperty("LedgerDimension3")]
		public ICollection<IntercompanyInvoiceTable> IntercompanyInvoiceTableDimension3 { get; set; }
		[InverseProperty("LedgerDimension3")]
		public ICollection<InvoiceLine> InvoiceLineDimension3 { get; set; }
		[InverseProperty("LedgerDimension3")]
		public ICollection<InvoiceTable> InvoiceTableDimension3 { get; set; }
		[InverseProperty("LedgerDimension3")]
		public ICollection<InterestRealizedTrans> InterestRealizedTransDimension3 { get; set; }
		[InverseProperty("LedgerDimension3")]
		public ICollection<PurchaseLine> PurchaseLineDimension3 { get; set; }
		[InverseProperty("LedgerDimension3")]
		public ICollection<PurchaseTable> PurchaseTableDimension3 { get; set; }
		[InverseProperty("LedgerDimension3")]
		public ICollection<ReceiptTempTable> ReceiptTempTableDimension3 { get; set; }
		[InverseProperty("LedgerDimension3")]
		public ICollection<ServiceFeeTrans> ServiceFeeTransDimension3 { get; set; }
		[InverseProperty("LedgerDimension3")]
		public ICollection<TaxInvoiceLine> TaxInvoiceLineDimension3 { get; set; }
		[InverseProperty("LedgerDimension3")]
		public ICollection<TaxInvoiceTable> TaxInvoiceTableDimension3 { get; set; }
		[InverseProperty("LedgerDimension3")]
		public ICollection<VendorPaymentTrans> VendorPaymentTransDimension3 { get; set; }
		[InverseProperty("LedgerDimension3")]
		public ICollection<WithdrawalLine> WithdrawalLineDimension3 { get; set; }
		[InverseProperty("LedgerDimension3")]
		public ICollection<WithdrawalTable> WithdrawalTableDimension3 { get; set; }
		[InverseProperty("LedgerDimension4")]
		public ICollection<ApplicationTable> ApplicationTableDimension4 { get; set; }
		[InverseProperty("LedgerDimension4")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTableDimension4 { get; set; }
		[InverseProperty("LedgerDimension4")]
		public ICollection<CreditAppTable> CreditAppTableDimension4 { get; set; }
		[InverseProperty("LedgerDimension4")]
		public ICollection<CustomerRefundTable> CustomerRefundTableDimension4 { get; set; }
		[InverseProperty("LedgerDimension4")]
		public ICollection<CustomerTable> CustomerTableDimension4 { get; set; }
		[InverseProperty("LedgerDimension4")]
		public ICollection<EmployeeTable> EmployeeTableDimension4 { get; set; }
		[InverseProperty("LedgerDimension4")]
		public ICollection<FreeTextInvoiceTable> FreeTextInvoiceTableDimension4 { get; set; }
		[InverseProperty("LedgerDimension4")]
		public ICollection<IntercompanyInvoiceTable> IntercompanyInvoiceTableDimension4 { get; set; }
		[InverseProperty("LedgerDimension4")]
		public ICollection<InvoiceLine> InvoiceLineDimension4 { get; set; }
		[InverseProperty("LedgerDimension4")]
		public ICollection<InvoiceTable> InvoiceTableDimension4 { get; set; }
		[InverseProperty("LedgerDimension4")]
		public ICollection<InterestRealizedTrans> InterestRealizedTransDimension4 { get; set; }
		[InverseProperty("LedgerDimension4")]
		public ICollection<PurchaseLine> PurchaseLineDimension4 { get; set; }
		[InverseProperty("LedgerDimension4")]
		public ICollection<PurchaseTable> PurchaseTableDimension4 { get; set; }
		[InverseProperty("LedgerDimension4")]
		public ICollection<ReceiptTempTable> ReceiptTempTableDimension4 { get; set; }
		[InverseProperty("LedgerDimension4")]
		public ICollection<ServiceFeeTrans> ServiceFeeTransDimension4 { get; set; }
		[InverseProperty("LedgerDimension4")]
		public ICollection<TaxInvoiceLine> TaxInvoiceLineDimension4 { get; set; }
		[InverseProperty("LedgerDimension4")]
		public ICollection<TaxInvoiceTable> TaxInvoiceTableDimension4 { get; set; }
		[InverseProperty("LedgerDimension4")]
		public ICollection<VendorPaymentTrans> VendorPaymentTransDimension4 { get; set; }
		[InverseProperty("LedgerDimension4")]
		public ICollection<WithdrawalLine> WithdrawalLineDimension4 { get; set; }
		[InverseProperty("LedgerDimension4")]
		public ICollection<WithdrawalTable> WithdrawalTableDimension4 { get; set; }
		[InverseProperty("LedgerDimension5")]
		public ICollection<ApplicationTable> ApplicationTableDimension5 { get; set; }
		[InverseProperty("LedgerDimension5")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTableDimension5 { get; set; }
		[InverseProperty("LedgerDimension5")]
		public ICollection<CreditAppTable> CreditAppTableDimension5 { get; set; }
		[InverseProperty("LedgerDimension5")]
		public ICollection<CustomerRefundTable> CustomerRefundTableDimension5 { get; set; }
		[InverseProperty("LedgerDimension5")]
		public ICollection<CustomerTable> CustomerTableDimension5 { get; set; }
		[InverseProperty("LedgerDimension5")]
		public ICollection<EmployeeTable> EmployeeTableDimension5 { get; set; }
		[InverseProperty("LedgerDimension5")]
		public ICollection<FreeTextInvoiceTable> FreeTextInvoiceTableDimension5 { get; set; }
		[InverseProperty("LedgerDimension5")]
		public ICollection<IntercompanyInvoiceTable> IntercompanyInvoiceTableDimension5 { get; set; }
		[InverseProperty("LedgerDimension5")]
		public ICollection<InvoiceLine> InvoiceLineDimension5 { get; set; }
		[InverseProperty("LedgerDimension5")]
		public ICollection<InvoiceTable> InvoiceTableDimension5 { get; set; }
		[InverseProperty("LedgerDimension5")]
		public ICollection<InterestRealizedTrans> InterestRealizedTransDimension5 { get; set; }
		[InverseProperty("LedgerDimension5")]
		public ICollection<PurchaseLine> PurchaseLineDimension5 { get; set; }
		[InverseProperty("LedgerDimension5")]
		public ICollection<PurchaseTable> PurchaseTableDimension5 { get; set; }
		[InverseProperty("LedgerDimension5")]
		public ICollection<ReceiptTempTable> ReceiptTempTableDimension5 { get; set; }
		[InverseProperty("LedgerDimension5")]
		public ICollection<ServiceFeeTrans> ServiceFeeTransDimension5 { get; set; }
		[InverseProperty("LedgerDimension5")]
		public ICollection<TaxInvoiceLine> TaxInvoiceLineDimension5 { get; set; }
		[InverseProperty("LedgerDimension5")]
		public ICollection<TaxInvoiceTable> TaxInvoiceTableDimension5 { get; set; }
		[InverseProperty("LedgerDimension5")]
		public ICollection<VendorPaymentTrans> VendorPaymentTransDimension5 { get; set; }
		[InverseProperty("LedgerDimension5")]
		public ICollection<WithdrawalLine> WithdrawalLineDimension5 { get; set; }
		[InverseProperty("LedgerDimension5")]
		public ICollection<WithdrawalTable> WithdrawalTableDimension5 { get; set; }
		#endregion Collection
	}
}
