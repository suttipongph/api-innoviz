using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class JobType : CompanyBaseEntity
	{
		public JobType()
		{
			MessengerJobTableJobType = new HashSet<MessengerJobTable>();
		}
 
		[Key]
		public Guid JobTypeGUID { get; set; }
		public bool Assignment { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string JobTypeId { get; set; }
		public int ShowDocConVerifyType { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("JobTypeCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("JobTypeBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("JobType")]
		public ICollection<MessengerJobTable> MessengerJobTableJobType { get; set; }
		#endregion Collection
	}
}
