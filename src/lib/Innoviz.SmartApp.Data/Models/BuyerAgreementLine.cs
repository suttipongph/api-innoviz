using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class BuyerAgreementLine : CompanyBaseEntity
	{
		public BuyerAgreementLine()
		{
			BuyerAgreementTransBuyerAgreementLine = new HashSet<BuyerAgreementTrans>();
			BuyerInvoiceTableBuyerAgreementLine = new HashSet<BuyerInvoiceTable>();
		}
 
		[Key]
		public Guid BuyerAgreementLineGUID { get; set; }
		public bool AlreadyInvoiced { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Amount { get; set; }
		public Guid BuyerAgreementTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Column(TypeName = "date")]
		public DateTime? DueDate { get; set; }
		public int Period { get; set; }
		[StringLength(1000)]
		public string Remark { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("BuyerAgreementLineCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("BuyerAgreementLineBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BuyerAgreementTableGUID")]
		[InverseProperty("BuyerAgreementLineBuyerAgreementTable")]
		public BuyerAgreementTable BuyerAgreementTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("BuyerAgreementLine")]
		public ICollection<BuyerAgreementTrans> BuyerAgreementTransBuyerAgreementLine { get; set; }
		[InverseProperty("BuyerAgreementLine")]
		public ICollection<BuyerInvoiceTable> BuyerInvoiceTableBuyerAgreementLine { get; set; }
		#endregion Collection
	}
}
