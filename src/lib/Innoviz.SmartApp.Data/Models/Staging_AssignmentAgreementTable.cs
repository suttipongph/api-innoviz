using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_AssignmentAgreementTable
	{
		[Key]
		[Column(Order = 1)]
		[StringLength(50)]
		public string AssignmentAgreementTableGUID { get; set; }
		[StringLength(100)]
		public string AcceptanceName { get; set; }
		public string AgreementDate { get; set; }
		public int AgreementDocType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal AssignmentAgreementAmount { get; set; }
		[StringLength(20)]
		public string AssignmentAgreementId { get; set; }
		public string AssignmentMethodGUID { get; set; }
		[StringLength(250)]
		public string AssignmentProductDescription { get; set; }
		[StringLength(100)]
		public string BuyerName { get; set; }
		[StringLength(50)]
		public string BuyerTableGUID { get; set; }
		[StringLength(250)]
		public string CancelAuthorityPersonAddress { get; set; }
		[StringLength(250)]
		public string CancelAuthorityPersonID { get; set; }
		[StringLength(250)]
		public string CancelAuthorityPersonName { get; set; }
		[StringLength(50)]
		public string ConsortiumTableGUID { get; set; }
		[StringLength(50)]
		public string CustBankGUID { get; set; }
		[StringLength(100)]
		public string CustomerAltName { get; set; }
		[Required]
		[StringLength(100)]
		public string CustomerName { get; set; }
		[StringLength(50)]
		public string CustomerTableGUID { get; set; }
		[StringLength(250)]
		public string CustRegisteredLocation { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public string DocumentReasonGUID { get; set; }
		[StringLength(50)]
		public string DocumentStatusGUID { get; set; }
		[Key]
		[Column(Order = 2)]
		[StringLength(20)]
		public string InternalAssignmentAgreementId { get; set; }
		[StringLength(50)]
		public string RefAssignmentAgreementTableGUID { get; set; }
		[StringLength(20)]
		public string ReferenceAgreementId { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		public string SigningDate { get; set; }
		[StringLength(250)]
		public string ToWhomConcern { get; set; }
		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }
		[Key]
		[Column(Order = 3)]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		public string OwnerBusinessUnitGUID { get; set; }

	}
}
