using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class NCBAccountStatus : CompanyBaseEntity
	{
		public NCBAccountStatus()
		{
			ApplicationTableNCBAccountStatus = new HashSet<ApplicationTable>();
			CustomerTableNCBAccountStatus = new HashSet<CustomerTable>();
			NCBTransNCBAccountStatus = new HashSet<NCBTrans>();
		}
 
		[Key]
		public Guid NCBAccountStatusGUID { get; set; }
		[StringLength(20)]
		public string Code { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string NCBAccStatusId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("NCBAccountStatusCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("NCBAccountStatusBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("NCBAccountStatus")]
		public ICollection<ApplicationTable> ApplicationTableNCBAccountStatus { get; set; }
		[InverseProperty("NCBAccountStatus")]
		public ICollection<CustomerTable> CustomerTableNCBAccountStatus { get; set; }
		[InverseProperty("NCBAccountStatus")]
		public ICollection<NCBTrans> NCBTransNCBAccountStatus { get; set; }
		#endregion Collection
	}
}
