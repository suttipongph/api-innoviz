﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_InvoiceLine
	{
		[Key]
		[Column(Order = 1)]
		[StringLength(50)]
		public string InvoiceLineGUID { get; set; }
		[StringLength(50)]
		public string Dimension1GUID { get; set; }
		[StringLength(50)]
		public string Dimension2GUID { get; set; }
		[StringLength(50)]
		public string Dimension3GUID { get; set; }
		[StringLength(50)]
		public string Dimension4GUID { get; set; }
		[StringLength(50)]
		public string Dimension5GUID { get; set; }
		[Required]
		[StringLength(50)]
		public string InvoiceRevenueTypeGUID { get; set; }
		[Key]
		[Column(Order = 2)]
		[StringLength(50)]
		public string InvoiceTableGUID { get; set; }
		[StringLength(250)]
		public string InvoiceText { get; set; }
		[Key]
		[Column(Order = 3)]
		public int LineNum { get; set; }
		[StringLength(50)]
		public string ProdUnitGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Qty { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TaxAmount { get; set; }
		[StringLength(50)]
		public string TaxTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TotalAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TotalAmountBeforeTax { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal UnitPrice { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal WHTAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal WHTBaseAmount { get; set; }
		[StringLength(50)]
		public string WithholdingTaxTableGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string BranchGUID { get; set; }
		[Key]
		[Column(Order = 4)]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		[StringLength(50)]
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		[StringLength(50)]
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		public string OwnerBusinessUnitGUID { get; set; }
		public int MigrateStatus { get; set; }
		public int MigrateSource { get; set; }
	}
}
