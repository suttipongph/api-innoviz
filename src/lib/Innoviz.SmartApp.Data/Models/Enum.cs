﻿using Innoviz.SmartApp.Core.Constants;

namespace Innoviz.SmartApp.Data.Models
{
    public class Enum
    {
        public enum AccountType
        {
            Ledger = 0,
            Bank = 1,
            Vendor = 2
        }

        public enum AgreementRevisionType
        {
            Execute = 0,
            Amend = 1,
            Transfer = 2,
            Close = 3,
            Terminate = 4,
            WriteOff = 5
        }

        public enum AgreementTransType
        {
            DownPaym = 0,
            AdvancePaym = 1,
            Deposit = 2,
            DepositReturn = 3,
            Execution = 4,
            Installment = 5,
            Invoice = 6,
            Penalty = 7,
            Collection = 8,
            TaxRealization = 9,
            IncomeRealization = 10,
            IncomeRealizationNPL = 11,
            IncomeRealizationTerminate = 12,
            IncomeRealizationWriteOff = 13,
            Payment = 14,
            Closing = 15,
            Termination = 16,
            WriteOff = 17,
            Selling = 18,
            Disposal = 19,
            PurchaseAgreement = 20
        }

        public enum AssetFeeType
        {
            None = 0,
            VehicleTax = 1,
            Compulsory = 2,
            Insurance = 3,
            Other = 4
        }

        public enum CalculationType
        {
            General = 0,
            Import = 1
        }

        public enum CollateralMovementType
        {
            Issue = 0,
            Receive = 1,
            Transfer = 2,
            Close = 3
        }

        public enum CollectionFeeMethod
        {
            Day = 0,
            Period = 1
        }

        public enum ContactType
        {
            [Lss(Value = 0, Code = "ENUM.CONTACT_TYPE_PHONE")]
            Phone,
            [Lss(Value = 1, Code = "ENUM.CONTACT_TYPE_EMAIL")]
            Email,
            [Lss(Value = 2, Code = "ENUM.CONTACT_TYPE_URL")]
            URL,
            [Lss(Value = 3, Code = "ENUM.CONTACT_TYPE_FAX")]
            Fax,
        }

        public enum CustTransStatus
        {
            Open = 0,
            Closed = 1
        }

        public enum Dimension
        {
            Dimension1 = 0,
            Dimension2 = 1,
            Dimension3 = 2,
            Dimension4 = 3,
            Dimension5 = 4
        }

        public enum DiscountBase
        {
            None = 0,
            Interest = 1,
            Payment = 2
        }

        public enum FVFormula
        {
            RV = 0,
            RVDeposit = 1,
            Repurchase = 2,
            Balloon = 3
        }

        public enum InvoiceIssuing
        {
            Company = 0,
            Customer = 1,
            Agreement = 2
        }

        public enum IncomeRealizeGenMethod
        {
            SpecificAgreement = 0,
            SpecificLeaseType = 1,
            All = 2
        }

        public enum LedgerFiscalPeriodStatus
        {
            Open = 0,
            Closed = 1
        }

        public enum LedgerFiscalPeriodUnit
        {
            Month = 0,
            Year = 1
        }

        public enum NPLMethod
        {
            Day = 0,
            Period = 1
        }

        public enum NumberSeqSegmentType
        {
            Constant = 0,
            Number = 1,
            Variable1 = 2,
            Variable2 = 3,
            Variable3 = 4,
            CompanyVariable = 5
        }

        public enum PaymentType
        {
            Ledger = 0,
            Transfer = 1,
            Cheque = 2,
            SuspenseAccount = 3
        }

        public enum PaymOverruleType
        {
            InvoiceType = 0,
            PaymStructure = 1
        }

        public enum PaymScheduleLineType
        {
            Down = 0,
            Installment = 1,
            RV = 2
        }

        public enum PaymStructureRefNum
        {
            None = 0,
            RefNum1 = 1,
            RefNum2 = 2
        }

        public enum PenaltyBase
        {
            Payment = 0,
            PaymentIncTax = 1
        }

        public enum ProvisionMethod
        {
            Day = 0,
            Period = 1
        }

        public enum ProvisionRateBy
        {
            Customer = 0,
            Agreement = 1
        }

        public enum PVFormula
        {
            Cost = 0,
            CostDeposit = 1
        }

        public enum ReceiptGenBy
        {
            Agreement = 0,
            AgreementInvoiceType = 1,
            AgreementInvoice = 2
        }

        public enum ReceiptTempRefType
        {
            Normal = 0,
            Imported = 1,
            Waive = 2,
            Discount = 3,
            Product = 4,
            Refund = 5
        }

        public enum RecordType
        {
            Person = 0,
            Organization = 1
        }

        public enum RefType
        {
            [Lss(Value = 0, Code = "ENUM.COMPANY")]
            Company = 0,
            [Lss(Value = 1, Code = "ENUM.PROSPECT")]
            Prospect = 1,
            [Lss(Value = 2, Code = "ENUM.CUSTOMER")]
            Customer = 2,
            [Lss(Value = 3, Code = "ENUM.GUARANTOR")]
            Guarantor = 3,
            [Lss(Value = 4, Code = "ENUM.VENDOR")]
            Vendor = 4,
            [Lss(Value = 5, Code = "ENUM.APPLICATION")]
            Application = 5,
            [Lss(Value = 7, Code = "ENUM.AGREEMENT")]
            Agreement = 7,
            [Lss(Value = 8, Code = "ENUM.INVOICE")]
            Invoice = 8,
            [Lss(Value = 9, Code = "ENUM.PAYMENT")]
            Payment = 9,
            [Lss(Value = 10, Code = "ENUM.PURCHASE_AGREEMENT_CONFIRMATION")]
            PurchaseAgreementConfirm = 10,
            [Lss(Value = 11, Code = "ENUM.AGREEMENT_CLASSIFICATION_JOURNAL")]
            AgreementClassificationJournal = 11,
            [Lss(Value = 12, Code = "ENUM.RECEIPT_TEMP")]
            ReceiptTemp = 12,
            [Lss(Value = 13, Code = "ENUM.WAIVE")]
            Waive = 13,
            [Lss(Value = 14, Code = "ENUM.APPLICATION_ASSET")]
            ApplicationAsset = 14,
            [Lss(Value = 15, Code = "ENUM.AGREEMENT_ASSET")]
            AgreementAsset = 15,
            [Lss(Value = 16, Code = "ENUM.AGREEMENT_ASSET_FEES")]
            AgreementAssetFees = 16,
            [Lss(Value = 17, Code = "ENUM.COLLATERAL")]
            Collateral = 17,
            [Lss(Value = 18, Code = "ENUM.PRODUCT")]
            Product = 18,
            [Lss(Value = 19, Code = "ENUM.ADDRESS")]
            Address = 19,
            [Lss(Value = 20, Code = "ENUM.CUSTOMER_BANK")]
            CustBank = 20,
            [Lss(Value = 21, Code = "ENUM.CONTACT")]
            Contact = 21,
            [Lss(Value = 22, Code = "ENUM.AUTHORIZED_PERSON_TRANSACTIONS")]
            AuthorizedPersonTrans = 22,
            [Lss(Value = 23, Code = "ENUM.GUARANTOR_TRANSACTIONS")]
            GuarantorTrans = 23,
            [Lss(Value = 24, Code = "ENUM.WITNESS_TRANSACTIONS")]
            WitnessTrans = 24,
            [Lss(Value = 25, Code = "ENUM.CUSTOMER_TRANSACTIONS")]
            CustTrans = 25,
            [Lss(Value = 26, Code = "ENUM.COLLATERAL_MOVEMENT_JOURNAL")]
            CollateralMovementJournal = 26,
            [Lss(Value = 28, Code = "ENUM.PROVISION_JOURNAL")]
            ProvisionJournal = 28,
            [Lss(Value = 29, Code = "ENUM.AGREEMENT_TRANSACTIONS_INCOME_REALIZATION")]
            AgreementTransIncomeRealization = 29,
            [Lss(Value = 30, Code = "ENUM.AGREEMENT_EARLY_PAYOFF")]
            AgreementEarlyPayoff = 30,
            [Lss(Value = 31, Code = "ENUM.BRANCH")]
            Branch = 31,
            [Lss(Value = 32, Code = "ENUM.DOCUMENT_CHECKLIST")]
            DocumentCheckList = 32,
            [Lss(Value = 33, Code = "ENUM.COLLECTION_ACTIVITY_TRANSACTIONS")]
            CollectionActivityTrans = 33,
            [Lss(Value = 34, Code = "ENUM.DUTY_STAMP_JOURNAL")]
            DutyStampJournal = 34,
            [Lss(Value = 35, Code = "ENUM.ALL_AGREEMENT_EARLY_PAY_OFF")]
            AllAgreementEarlyPayOff = 35,
            [Lss(Value = 36, Code = "ENUM.PURCHASE_AGREEMENT")]
            PurchAgreementTable = 36,
            [Lss(Value = 37, Code = "ENUM.VENDOR_BANK")]
            VendBank = 37,
            [Lss(Value = 38, Code = "ENUM.BUYER")]
            Buyer = 38,
            [Lss(Value = 39, Code = "ENUM.CREDIT_APPLICATION_REQUEST_TABLE")]
            CreditAppRequestTable = 39,
            [Lss(Value = 40, Code = "ENUM.CREDIT_APPLICATION_TABLE")]
            CreditAppTable = 40,
            [Lss(Value = 41, Code = "ENUM.CREDIT_APPLICATION_REQUEST_LINE")]
            CreditAppRequestLine = 41,
            [Lss(Value = 42, Code = "ENUM.CREDIT_APPLICATION_LINE")]
            CreditAppLine = 42,
            [Lss(Value = 43, Code = "ENUM.ASSIGNMENT_AGREEMENT")]
            AssignmentAgreement = 43,
            [Lss(Value = 44, Code = "ENUM.CHEQUE")]
            Cheque = 44,
            [Lss(Value = 45, Code = "ENUM.PURCHASE_TABLE")]
            PurchaseTable = 45,
            [Lss(Value = 46, Code = "ENUM.PURCHASE_LINE")]
            PurchaseLine = 46,
            [Lss(Value = 47, Code = "ENUM.MAIN_AGREEMENT")]
            MainAgreement = 47,
            [Lss(Value = 48, Code = "ENUM.GUARANTOR_AGREEMENT")]
            GuarantorAgreement = 48,
            [Lss(Value = 49, Code = "ENUM.BUSINESS_COLLATERAL_AGREEMENT")]
            BusinessCollateralAgreement = 49,
            [Lss(Value = 50, Code = "ENUM.BUYER_AGREEMENT")]
            BuyerAgreement = 50,
            [Lss(Value = 51, Code = "ENUM.PAYMENT_DETAIL")]
            PaymentDetail = 51,
            [Lss(Value = 53, Code = "ENUM.COLLECTION_FOLLOW_UP")]
            CollectionFollowUp = 53,
            [Lss(Value = 54, Code = "ENUM.RELATED_PERSON")]
            RelatedPerson = 54,
            [Lss(Value = 55, Code = "ENUM.INVOICE_SETTLEMENT_DETAIL")]
            InvoiceSettlementDetail = 55,
            [Lss(Value = 56, Code = "ENUM.WITHDRAWAL_TABLE")]
            WithdrawalTable = 56,
            [Lss(Value = 57, Code = "ENUM.WITHDRAWAL_LINE")]
            WithdrawalLine = 57,
            [Lss(Value = 58, Code = "ENUM.CUSTOMER_REFUND")]
            CustomerRefund = 58,
            [Lss(Value = 59, Code = "ENUM.BLACKLIST")]
            Blacklist = 59,
            [Lss(Value = 60, Code = "ENUM.VERIFICATION")]
            Verification = 60,
            [Lss(Value = 61, Code = "ENUM.EMPLOYEE")]
            Employee = 61,
            [Lss(Value = 62, Code = "ENUM.SERVICE_FEE_TRANS")]
            ServiceFeeTrans = 62,
            [Lss(Value = 63, Code = "ENUM.MESSENGER_JOB")]
            MessengerJob = 63,
            [Lss(Value = 64, Code = "ENUM.VENDOR_PAYMENT_TRANS")]
            VendorPaymentTrans = 64,
            [Lss(Value = 65, Code = "ENUM.PROJECT_PROGRESS")]
            ProjectProgress = 65,
            [Lss(Value = 66, Code = "ENUM.DOCUMENT_RETURN")]
            DocumentReturn = 66,
            [Lss(Value = 67, Code = "ENUM.INTEREST_REALIZED_TRANS")]
            InterestRealizedTrans = 67,
            [Lss(Value = 68, Code = "ENUM.FREE_TEXT_INVOICE")]
            FreeTextInvoice = 68,
            [Lss(Value = 69, Code = "ENUM.INTERCOMPANY_INVOICE_ADJUSTMENT")]
            IntercomapnyInvoiceAdjustment = 69,
            [Lss(Value = 70, Code = "ENUM.ASSIGNMENT_AGREEMENT_SETTLE")]
            AssignmentAgreementSettle = 70,
        }
        public enum AssetFeeGenMethod
        {
            None = 0,
            PerAgreement = 1,
            PerAgreementAsset = 2,
        }
        public enum ResponsibleBy
        {
            NotSpecified = 0,
            Customer = 1,
            Owner = 2,
            Include = 3,
            Vendor = 4
        }

        public enum SysAccessLevel
        {
            Individual = 0,
            Branch = 1,
            Company = 2,
            Organization = 3
        }

        public enum SysAccessRight
        {
            NoAccess = 0,
            Read = 1,
            Update = 2,
            Create = 3,
            Full = 4
        }

        public enum SysFeatureType
        {
            Menu = 0,
            Lookup = 1,
            RelatedInfo = 2,
            Function = 3
        }

        public enum SysSessionType
        {
            LogIn = 0,
            LogOut = 1
        }

        public enum TaxInvoiceRefType
        {
            None = 0,
            Invoice = 1,
            Receipt = 2
        }

        public enum WHTBase
        {
            None = 0,
            Payment = 1,
            Interest = 2
        }

        public enum InstallmentInvoiceGenBy
        {
            Agreement = 0,
            Customer = 1,
        }

        public enum InterestRateType
        {
            EffectiveRate = 0,
            FlatRate = 1,
        }

        public enum RoundingType
        {
            None = 0,
            RoundUp = 1,
            RoundDown = 2,
        }
        public enum LocalStatus
        {
            ADD = 0,
            DELETE = 1,
        }

        public enum GeneratePenaltyGenMethod
        {
            Agreement = 0,
            Customer = 1
        }

        public enum StagingBatchStatus
        {
            None = 0,
            Success = 1,
            Fail = 2
        }

        public enum LetterOfOverdueGenMethod
        {
            SpecificAgreement = 0,
            SpecificLeaseType = 1,
            All = 2
        }

        public enum AgentActionResult
        {
            Open = 0,
            Completed = 1,
            Failed = 2
        }

        public enum PenaltyCalculationMethod
        {
            OutstandingBalanceOfInstallmentInvoice = 0,
            AllInstallmentInvoice = 1
        }

        public enum IRRCalculationType
        {
            Financial = 0,
            Accounting = 1
        }

        public enum PerAgreementOrAsset
        {
            None = 0,
            PerAgreement = 1,
            PerAgreementAsset = 2
        }
        public enum RepossessedAsset
        {
            SpecificAgreement = 0,
            SpecificLeaseType = 1,
            All = 2
        }


        public enum StopTaxRealizationAgreement
        {
            SpecificAgreement = 1,
            SpecificLeaseType = 0,
            SpecificCustomer = 2,
            All = 3
        }

        public enum TypeOfDate
        {
            DueDate = 0,
            InvoiceDate = 1,
            TaxInvoiceDate = 2,
        }

        public enum InterfaceStatus
        {
            None = 0,
            Success = 1,
            Fail = 2
        }

        public enum PaymentStatus
        {
            NotSpecified = 0,
            OpenInvoice = 1,
            ClosedInvoice = 2,
        }

        public enum DocumentFormat
        {
            Invoice = 0,
            CreditNote = 1,
        }

        public enum SettleInvoiceCriteria
        {
            None = 0,
            SettlementByMonth = 1,
            SettlementByDay = 2
        }

        public enum RoundingMethod
        {
            None = 0,
            ExcludeTax_Rounding = 1,
            IncludeTax_Rounding = 2
        }

        public enum FlexType
        {
            FlexSetup1 = 0,
            FlexSetup2 = 1,
            FlexSetup3 = 2,
            FlexSetup4 = 3
        }

        public enum AssetFlexType
        {
            AssetFlexSetup1 = 0,
            AssetFlexSetup2 = 1,
            AssetFlexSetup3 = 2
        }
        public enum FilterBy
        {
            LastDueDate = 0,
            LastEndDate = 1
        }

        public enum LetterOfRenewalGenMethod
        {
            SpecificAgreement = 0,
            SpecificCustomer = 1,
            All = 2
        }

        public enum LetterOfExpirationGenMethod
        {
            SpecificAgreement = 0,
            SpecificLeaseType = 1,
            All = 2
        }

        public enum OverdueType
        {
            Day = 0,
            Period = 1
        }

        public enum TempReceiptGroupBy
        {
            ReceivedBy = 0,
            Branch = 1
        }

        public enum GenRenewAgreementAssetFeeMethod
        {
            AllBranch = 0,
            SpecificBranch = 1
        }
        public enum AssetCondition
        {
            New = 0,
            Used = 1,
            Other = 2
        }

        public enum RevertRepossessType
        {
            Redemption = 0,
            Cancel = 1
        }
        public enum AgreementRepossessionStatus
        {
            PartialRepossession = 0,
            WaitingForRedemption = 1,
            Partiallysold = 2,
            WaitingForSale = 3,
            Sold = 4
        }
        public enum CompanySignatureRefType
        {
            AuthorizedPerson = 0,
            Witness = 1
        }
        public enum DocumentTemplateType
        {
            None = 0,
            MainAgreementShared = 1,
            MainAgreementConsortium = 2,
            MainAgreementJv = 3,
            MainAgreementPf = 4,
            MainAgreementExpenseNotice = 5,
            AddendumMainAgreement = 6,
            AssignmentAgreement = 7,
            CancelAssignmentAgreement = 8,
            BusinessCollateralAgreement = 9,
            ExtendNotesBusCollateralAgm = 10,
            BusCollateralAgmExpenseNotice = 11,
            LetterReqCancelBusCollateralAgm = 12,
            GuarantorAgreement_ThaiPerson = 13,
            CrossGuarantorAgreement = 14,
            CustomerAmendCredit_information = 15,
            CustomerAmendCredit_limit = 16,
            CustomerCreditApplication_shared = 17,
            CustomerCreditApplication_pf = 18,
            CreditApplicationConsideration = 19,
            BuyerAmendCreditInformation = 20,
            BuyerAmendCreditLimit = 21,
            BuyerCreditApplication_Private = 22,
            BuyerReviewCreditLimit = 23,
            CreditLimitClosing = 24,
            GuarantorAgreement_Foreigner = 25,
            GuarantorAgreement_Organization = 26,
            BuyerCreditApplication_Government = 27
        }
        public enum IdentificationType
        {
            TaxId = 0,
            PassportId = 1
        }
        public enum CreditAppRequestType
        {
            [Lss(Value = 0, Code = "ENUM.MAIN_CREDIT_LIMIT")]
            MainCreditLimit,
            [Lss(Value = 1, Code = "ENUM.BUYER_MATCHING")]
            BuyerMatching,
            [Lss(Value = 2, Code = "ENUM.LOAN_REQUEST")]
            LoanRequest,
            [Lss(Value = 3, Code = "ENUM.ACTIVE_AMEND_CUSTOMER_CREDIT_LIMIT")]
            ActiveAmendCustomerCreditLimit,
            [Lss(Value = 4, Code = "ENUM.AMEND_CUSTOMER_INFO")]
            AmendCustomerInfo,
            [Lss(Value = 5, Code = "ENUM.AMEND_CUSTOMER_BUYER_CREDIT_LIMIT")]
            AmendCustomerBuyerCreditLimit,
            [Lss(Value = 6, Code = "ENUM.AMEND_BUYER_INFO")]
            AmendBuyerInfo,
            [Lss(Value = 7, Code = "ENUM.REVIEW_CUSTOMER_BUYER_CREDIT_LIMIT")]
            ReviewBuyerCreditLimit,
            [Lss(Value = 8, Code = "ENUM.CLOSE_CUSTOMER_CREDIT_LIMIT")]
            CloseCustomerCreditLimit
        }
        public enum ProductType
        {
            [Lss(Value = 0, Code = "ENUM.NONE")]
            None,
            [Lss(Value = 1, Code = "ENUM.FACTORING")]
            Factoring,
            [Lss(Value = 2, Code = "ENUM.PROJECT_FINANCE")]
            ProjectFinance,
            [Lss(Value = 3, Code = "ENUM.LEASING")]
            Leasing,
            [Lss(Value = 4, Code = "ENUM.HIRE_PURCHASE")]
            HirePurchase,
            [Lss(Value = 5, Code = "ENUM.BOND")]
            Bond,
            [Lss(Value = 6, Code = "ENUM.LC_DLC")]
            LC_DLC,
        }
        public enum RetentionDeductionMethod
        {
            [Lss(Value = 0, Code = "ENUM.PURCHASE")]
            Purchase,
            [Lss(Value = 1, Code = "ENUM.WITHDRAWAL")]
            Withdrawal,
            [Lss(Value = 2, Code = "ENUM.RESERVE_REFUND")]
            ReserveRefund,
            [Lss(Value = 3, Code = "ENUM.CUSTOMER_SPECIFIC")]
            CustomerSpecific,
        }
        public enum RetentionCalculateBase
        {
            [Lss(Value = 0, Code = "ENUM.NONE")]
            None,
            [Lss(Value = 1, Code = "ENUM.INVOICE_AMOUNT")]
            InvoiceAmount,
            [Lss(Value = 2, Code = "ENUM.PURCHASE_AMOUNT")]
            PurchaseAmount,
            [Lss(Value = 3, Code = "ENUM.FIXED_AMOUNT")]
            FixedAmount,
            [Lss(Value = 4, Code = "ENUM.NET_RESERVE_AMOUNT")]
            NetReserveAmount,
            [Lss(Value = 5, Code = "ENUM.WITHDRAWAL_AMOUNT")]
            WithdrawalAmount,
        }
        public enum AgreementDocType
        {
            [Lss(Value = 0, Code = "ENUM.NEW")]
            New,
            [Lss(Value = 1, Code = "ENUM.EXTEND")]
            Extend,
            [Lss(Value = 2, Code = "ENUM.ADDENDUM")]
            Addendum,
            [Lss(Value = 3, Code = "ENUM.NOTICE_OF_CANCELLATION")]
            NoticeOfCancellation
        }
        public enum BookmarkDocumentRefType
        {
            None = 0,
            MainAgreement = 1,
            GuarantorAgreement = 2,
            AssignmentAgreement = 3,
            BusinessCollateralAgreement = 4,
            CreditAppRequest = 5
        }
        public enum SuspenseInvoiceType
        {
            [Lss(Value = 0,Code = "ENUM.NONE")]
            None = 0,
            [Lss(Value = 1, Code = "ENUM.SUSPENSE_ACCOUNT")]
            SuspenseAccount = 1,
            [Lss(Value = 2, Code = "ENUM.TO_BE_REFUNDED")]
            ToBeRefunded = 2,
            [Lss(Value = 3, Code = "ENUM.RETENTION")]
            Retention = 3
        }
        public enum CreditLimitConditionType
        {
            [Lss(Value = 0, Code = "ENUM.BY_CUSTOMER")]
            ByCustomer,
            [Lss(Value = 1, Code = "ENUM.BY_CREDIT_APPLICATION")]
            ByCreditApplication
        }
        public enum CreditLimitExpiration

        {
            [Lss(Value = 0, Code = "ENUM.BY_TRANSACTION")]
            ByTransaction,
            [Lss(Value = 1, Code = "ENUM.BY_CREDIT_APPLICATION")]
            ByCreditApplication,
            [Lss(Value = 2, Code = "ENUM.BY_BUYER_AGREEMENT")]
            ByBuyerAgreement,
            [Lss(Value = 3, Code = "ENUM.BY_SPECIFIC")]
            BySpecific,
            [Lss(Value = 4, Code = "ENUM.NO_EXPIRATION")]
            NoExpiration
        }
        public enum WorkingDay
        {
            MonToFri = 0,
            MonToSat = 1,
            MonToSun = 2
        }
        public enum CalcInterestMethod
        {
            None = 0,
            NextCompanyCalendar = 1,
            NextBOTCalendar = 2,
            NextMinCalendar = 3,
            NextMaxCalendar = 4
        }
        public enum CalcInterestDayMethod
        {
            None = 0,
            NextDay = 1
        }
        public enum ManageAgreementAction
        {
            Post = 0,
            Send = 1,
            Sign = 2,
            Close = 3,
            Cancel = 4
        }
        public enum DocumentStatusCode
        {
            Draft = 100,
            Posted = 110,
            Sent = 120,
            Signed = 130
        }
        public enum ApprovalDecision
        {
            [Lss(Value = 0, Code = "ENUM.NONE")]
            None = 0,
            [Lss(Value = 1, Code = "ENUM.APPROVED")]
            Approved = 1,
            [Lss(Value = 2, Code = "ENUM.REJECTED")]
            Rejected = 2
        }
        public enum ReceivedFrom
        {
            Customer = 0,
            Buyer = 1
        }
        public enum PurchaseFeeCalculateBase
        {
            [Lss(Value = 0, Code = "ENUM.INVOICE_AMOUNT")]
            InvoiceAmount,
            [Lss(Value = 1, Code = "ENUM.PURCHASE_AMOUNT")]
            PurchaseAmount,
        }
        public enum DocConVerifyType
        {
            Billing = 0,
            Receipt = 1
        }
        public enum ProcessTransType
        {
            [Lss(Value = 0, Code = "ENUM.DOWN_PAYMENT")]
            DownPaym,
            [Lss(Value = 1, Code = "ENUM.ADVANCE_PAYMENT")]
            AdvancePaym,
            [Lss(Value = 2, Code = "ENUM.DEPOSIT")]
            Deposit,
            [Lss(Value = 3, Code = "ENUM.DEPOSIT_RETURNING")]
            DepositReturn,
            [Lss(Value = 4, Code = "ENUM.EXECUTION")]
            Execution,
            [Lss(Value = 5, Code = "ENUM.INSTALLMENT")]
            Installment,
            [Lss(Value = 6, Code = "ENUM.INVOICE")]
            Invoice,
            [Lss(Value = 7, Code = "ENUM.PENALTY")]
            Penalty,
            [Lss(Value = 8, Code = "ENUM.COLLECTION_FEE")]
            Collection,
            [Lss(Value = 9, Code = "ENUM.TAX_REALIZATION")]
            TaxRealization,
            [Lss(Value = 10, Code = "ENUM.INCOME_REALIZATION")]
            IncomeRealization,
            [Lss(Value = 11, Code = "ENUM.INCOME_REALIZATION_NPL")]
            IncomeRealizationNPL,
            [Lss(Value = 12, Code = "ENUM.INCOME_REALIZATION_TERMINATE")]
            IncomeRealizationTerminate,
            [Lss(Value = 13, Code = "ENUM.INCOME_REALIZATION_WRITEOFF")]
            IncomeRealizationWriteOff,
            [Lss(Value = 14, Code = "ENUM.PAYMENT")]
            Payment,
            [Lss(Value = 15, Code = "ENUM.CLOSING")]
            Closing,
            [Lss(Value = 16, Code = "ENUM.TERMINATION")]
            Termination,
            [Lss(Value = 17, Code = "ENUM.WRITEOFF")]
            WriteOff,
            [Lss(Value = 18, Code = "ENUM.SELLING")]
            Selling,
            [Lss(Value = 19, Code = "ENUM.DISPOSAL")]
            Disposal,
            [Lss(Value = 20, Code = "ENUM.PURCHASE_AGREEMENT")]
            PurchaseAgreement,
            [Lss(Value = 21, Code = "ENUM.VENDOR_PAYMENT")]
            VendorPayment,
            [Lss(Value = 22, Code = "ENUM.PAYMENT_DETAIL")]
            PaymentDetail,
        }
        public enum CollectionFollowUpResult
        {
            None = 0,
            Confirmed = 1,
            Postpone = 2,
            Extend = 3
        }
        public enum InterestCalculationMethod
        {
            Minimum = 0,
            Actual = 1
        }
        public enum WithdrawalLineType
        {
            Principal = 0,
            Interest = 1
        }
        public enum Result
        {
            None = 0,
            Completed = 1,
            Failed = 2
        }

        public enum ServiceFeeCategory
        {
            None = 0,
            Agreement = 1,
            Billing = 2,
            Receipt = 3,
            Retention = 4
        }
        public enum PaidToType
        {
            Customer = 0,
            Vendor = 1,
            Suspense = 2
        }
        public enum ContactTo
        {
            Customer = 0,
            Buyer = 1,
            Specific = 2
        }

        public enum MonthOfYear
        {
            M01 = 1,
            M02 = 2,
            M03 = 3,
            M04 = 4,
            M05 = 5,
            M06 = 6,
            M07 = 7,
            M08 = 8,
            M09 = 9,
            M10 = 10,
            M11 = 11,
            M12 = 12
        }

        public enum BusinessSegmentType
        {
            Person = 0,
            Private = 1,
            Goverment = 2
        }
        public enum MonthNames
        {
            [Lss(Value = 1, Code = "January")]
            January ,
            [Lss(Value = 2, Code = "February")]
            February,
            [Lss(Value = 3, Code = "March")]
            March,
            [Lss(Value = 4, Code = "April")]
            April,
            [Lss(Value = 5, Code = "May")]
            May,
            [Lss(Value = 6, Code = "June")]
            June,
            [Lss(Value = 7, Code = "July")]
            July,
            [Lss(Value = 8, Code = "August")]
            August,
            [Lss(Value = 9, Code = "September")]
            September,
            [Lss(Value = 10, Code = "October")]
            October,
            [Lss(Value = 11, Code = "November")]
            November,
            [Lss(Value = 12, Code = "December")]
            December
        }
    }
}
