using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class InvoiceNumberSeqSetup : BranchCompanyBaseEntity
	{
		public InvoiceNumberSeqSetup()
		{
		}
 
		[Key]
		public Guid InvoiceNumberSeqSetupGUID { get; set; }
		public Guid InvoiceTypeGUID { get; set; }
		public int ProductType { get; set; }
		public Guid? NumberSeqTableGUID { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("InvoiceNumberSeqSetupCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("InvoiceNumberSeqSetupBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BranchGUID")]
		[InverseProperty("InvoiceNumberSeqSetupBranch")]
		public Branch Branch { get; set; }
		[ForeignKey("InvoiceTypeGUID")]
		[InverseProperty("InvoiceNumberSeqSetupInvoiceType")]
		public InvoiceType InvoiceType { get; set; }
		[ForeignKey("NumberSeqTableGUID")]
		[InverseProperty("InvoiceNumberSeqSetupNumberSeqTable")]
		public NumberSeqTable NumberSeqTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
