using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class TaxTable : CompanyBaseEntity
	{
		public TaxTable()
		{
			CompanyParameterCompulsoryTax = new HashSet<CompanyParameter>();
			CompanyParameterInsuranceTax = new HashSet<CompanyParameter>();
			CompanyParameterMaintenanceTax = new HashSet<CompanyParameter>();
			CompanyParameterVehicleTaxServiceFeeTax = new HashSet<CompanyParameter>();
			FreeTextInvoiceTableTaxTable = new HashSet<FreeTextInvoiceTable>();
			IntercompanyInvoiceTableTaxTable = new HashSet<IntercompanyInvoiceTable>();
			InvoiceLineTaxTable = new HashSet<InvoiceLine>();
			InvoiceRevenueTypeFeeTax = new HashSet<InvoiceRevenueType>();
			LeaseTypeTaxCost = new HashSet<LeaseType>();
			LeaseTypeTaxInstallment = new HashSet<LeaseType>();
			LeaseTypeTaxWriteOff = new HashSet<LeaseType>();
			ProcessTransOrigTaxTable = new HashSet<ProcessTrans>();
			ProcessTransTaxTable = new HashSet<ProcessTrans>();
			ReceiptLineTaxTable = new HashSet<ReceiptLine>();
			ServiceFeeTransTaxTable = new HashSet<ServiceFeeTrans>();
			TaxInvoiceLineTaxTable = new HashSet<TaxInvoiceLine>();
			TaxTablePaymentTaxTable = new HashSet<TaxTable>();
			TaxValueTaxTable = new HashSet<TaxValue>();
			VendorPaymentTransTaxTable = new HashSet<VendorPaymentTrans>();
		}
 
		[Key]
		public Guid TaxTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[StringLength(20)]
		public string InputTaxLedgerAccount { get; set; }
		[StringLength(20)]
		public string OutputTaxLedgerAccount { get; set; }
		public Guid? PaymentTaxTableGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string TaxCode { get; set; }

		[InverseProperty("CompulsoryTax")]
		public ICollection<CompanyParameter> CompanyParameterCompulsoryTax { get; set; }
		[InverseProperty("InsuranceTax")]
		public ICollection<CompanyParameter> CompanyParameterInsuranceTax { get; set; }
		[InverseProperty("MaintenanceTax")]
		public ICollection<CompanyParameter> CompanyParameterMaintenanceTax { get; set; }
		[InverseProperty("VehicleTaxServiceFeeTax")]
		public ICollection<CompanyParameter> CompanyParameterVehicleTaxServiceFeeTax { get; set; }
		[InverseProperty("TaxCost")]
		public ICollection<LeaseType> LeaseTypeTaxCost { get; set; }
		[InverseProperty("TaxInstallment")]
		public ICollection<LeaseType> LeaseTypeTaxInstallment { get; set; }
		[InverseProperty("TaxWriteOff")]
		public ICollection<LeaseType> LeaseTypeTaxWriteOff { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("TaxTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("TaxTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("PaymentTaxTableGUID")]
		[InverseProperty("TaxTablePaymentTaxTable")]
		public TaxTable PaymentTaxTable { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("TaxTable")]
		public ICollection<FreeTextInvoiceTable> FreeTextInvoiceTableTaxTable { get; set; }
		[InverseProperty("TaxTable")]
		public ICollection<IntercompanyInvoiceTable> IntercompanyInvoiceTableTaxTable { get; set; }
		[InverseProperty("TaxTable")]
		public ICollection<InvoiceLine> InvoiceLineTaxTable { get; set; }
		[InverseProperty("FeeTax")]
		public ICollection<InvoiceRevenueType> InvoiceRevenueTypeFeeTax { get; set; }
		[InverseProperty("OrigTaxTable")]
		public ICollection<ProcessTrans> ProcessTransOrigTaxTable { get; set; }		
		[InverseProperty("TaxTable")]
		public ICollection<ProcessTrans> ProcessTransTaxTable { get; set; }
		[InverseProperty("TaxTable")]
		public ICollection<ReceiptLine> ReceiptLineTaxTable { get; set; }
		[InverseProperty("TaxTable")]
		public ICollection<ServiceFeeTrans> ServiceFeeTransTaxTable { get; set; }
		[InverseProperty("TaxTable")]
		public ICollection<TaxInvoiceLine> TaxInvoiceLineTaxTable { get; set; }
		[InverseProperty("PaymentTaxTable")]
		public ICollection<TaxTable> TaxTablePaymentTaxTable { get; set; }
		[InverseProperty("TaxTable")]
		public ICollection<TaxValue> TaxValueTaxTable { get; set; }
		[InverseProperty("TaxTable")]
		public ICollection<VendorPaymentTrans> VendorPaymentTransTaxTable { get; set; }
		#endregion Collection
	}
}
