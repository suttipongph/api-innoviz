using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class KYCSetup : CompanyBaseEntity {
		public KYCSetup()
		{
			BuyerTableKYCSetup = new HashSet<BuyerTable>();
			CustomerTableKYCSetup = new HashSet<CustomerTable>();
			CreditAppRequestLineKYCSetup = new HashSet<CreditAppRequestLine>();
			CreditAppRequestTableKYCSetup = new HashSet<CreditAppRequestTable>();
		}

		[Key]
		public Guid KYCSetupGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string KYCId { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("KYCSetupCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("KYCSetupBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("KYCSetup")]
		public ICollection<BuyerTable> BuyerTableKYCSetup { get; set; }
		[InverseProperty("KYCSetup")]
		public ICollection<CustomerTable> CustomerTableKYCSetup { get; set; }
		[InverseProperty("KYCSetup")]
		public ICollection<CreditAppRequestLine> CreditAppRequestLineKYCSetup { get; set; }
		[InverseProperty("KYCSetup")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTableKYCSetup { get; set; }
		#endregion Collection
	}
}
