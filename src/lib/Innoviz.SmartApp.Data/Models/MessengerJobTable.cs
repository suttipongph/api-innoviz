using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class MessengerJobTable : CompanyBaseEntity
	{
		public MessengerJobTable()
		{
			JobChequeMessengerJobTable = new HashSet<JobCheque>();
			MessengerJobTableRefMessengerJobTable = new HashSet<MessengerJobTable>();
		}
 
		[Key]
		public Guid MessengerJobTableGUID { get; set; }
		[StringLength(250)]
		public string Address1 { get; set; }
		[StringLength(250)]
		public string Address2 { get; set; }
		public Guid? AssignmentAgreementTableGUID { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		[StringLength(20)]
		public string ContactPersonExtension { get; set; }
		[StringLength(100)]
		public string ContactPersonMobile { get; set; }
		[StringLength(100)]
		public string ContactPersonName { get; set; }
		[StringLength(100)]
		public string ContactPersonPhone { get; set; }
		[StringLength(100)]
		public string ContactPersonPosition { get; set; }
		public int ContactTo { get; set; }
		public Guid? CustomerTableGUID { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal FeeAmount { get; set; }
		[Column(TypeName = "date")]
		public DateTime? JobDate { get; set; }
		[Required]
		[StringLength(1000)]
		public string JobDetail { get; set; }
		[Required]
		[StringLength(20)]
		public string JobId { get; set; }
		public DateTime? JobStartTime { get; set; }
		public DateTime? JobEndTime { get; set; }
		public Guid JobTypeGUID { get; set; }
		public bool Map { get; set; }
		public Guid? MessengerTableGUID { get; set; }
		public int Priority { get; set; }
		public int ProductType { get; set; }
		public Guid? RefCreditAppLineGUID { get; set; }
		public Guid? RefGUID { get; set; }
		public Guid? RefMessengerJobTableGUID { get; set; }
		public int RefType { get; set; }
		public Guid RequestorGUID { get; set; }
		public int Result { get; set; }
		[StringLength(250)]
		public string ResultRemark { get; set; }
		public string ContactName { get; set; }
		[StringLength(100)]
		public string ContactPersonDepartment { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ExpenseAmount { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("MessengerJobTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("MessengerJobTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("AssignmentAgreementTableGUID")]
		[InverseProperty("MessengerJobTableAssignmentAgreementTable")]
		public AssignmentAgreementTable AssignmentAgreementTable { get; set; }
		[ForeignKey("BuyerTableGUID")]
		[InverseProperty("MessengerJobTableBuyerTable")]
		public BuyerTable BuyerTable { get; set; }
		[ForeignKey("CustomerTableGUID")]
		[InverseProperty("MessengerJobTableCustomerTable")]
		public CustomerTable CustomerTable { get; set; }
		[ForeignKey("DocumentStatusGUID")]
		[InverseProperty("MessengerJobTableDocumentStatus")]
		public DocumentStatus DocumentStatus { get; set; }
		[ForeignKey("JobTypeGUID")]
		[InverseProperty("MessengerJobTableJobType")]
		public JobType JobType { get; set; }
		[ForeignKey("MessengerTableGUID")]
		[InverseProperty("MessengerJobTableMessengerTable")]
		public MessengerTable MessengerTable { get; set; }
		[ForeignKey("RefCreditAppLineGUID")]
		[InverseProperty("MessengerJobTableRefCreditAppLine")]
		public CreditAppLine RefCreditAppLine { get; set; }
		[ForeignKey("RefMessengerJobTableGUID")]
		[InverseProperty("MessengerJobTableRefMessengerJobTable")]
		public MessengerJobTable RefMessengerJobTable { get; set; }
		[ForeignKey("RequestorGUID")]
		[InverseProperty("MessengerJobTableRequestor")]
		public EmployeeTable Requestor { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("MessengerJobTable")]
		public ICollection<JobCheque> JobChequeMessengerJobTable { get; set; }
		[InverseProperty("RefMessengerJobTable")]
		public ICollection<MessengerJobTable> MessengerJobTableRefMessengerJobTable { get; set; }
		#endregion Collection
	}
}
