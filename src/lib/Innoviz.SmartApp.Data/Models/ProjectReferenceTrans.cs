using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class ProjectReferenceTrans : CompanyBaseEntity
	{
		public ProjectReferenceTrans()
		{
		}
 
		[Key]
		public Guid ProjectReferenceTransGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string ProjectCompanyName { get; set; }
		public int ProjectCompletion { get; set; }
		[Required]
		[StringLength(100)]
		public string ProjectName { get; set; }
		[StringLength(100)]
		public string ProjectStatus { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ProjectValue { get; set; }
		public Guid? RefGUID { get; set; }
		public int RefType { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("ProjectReferenceTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("ProjectReferenceTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
