using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class BankGroup : CompanyBaseEntity
	{
		public BankGroup()
		{
			BusinessCollateralAgmLineBankGroup = new HashSet<BusinessCollateralAgmLine>();
			ChequeTableChequeBankGroup = new HashSet<ChequeTable>();
			CollectionFollowUpChequeBankGroup = new HashSet<CollectionFollowUp>();
			CompanyBankBankGroup = new HashSet<CompanyBank>();
			CreditAppReqBusinessCollateralBankGroup = new HashSet<CreditAppReqBusinessCollateral>();
			CustBankBankGroup = new HashSet<CustBank>();
			CustBusinessCollateralBankGroup = new HashSet<CustBusinessCollateral>();
			FinancialCreditTransBankGroup = new HashSet<FinancialCreditTrans>();
			JobChequeChequeBankGroup = new HashSet<JobCheque>();
			NCBTransBankGroup = new HashSet<NCBTrans>();
			ReceiptTableChequeBankGroup = new HashSet<ReceiptTable>();
			ReceiptTempPaymDetailChequeBankGroup = new HashSet<ReceiptTempPaymDetail>();
			VendBankBankGroup = new HashSet<VendBank>();
		}
 
		[Key]
		public Guid BankGroupGUID { get; set; }
		[StringLength(10)]
		public string Alias { get; set; }
		[Required]
		[StringLength(20)]
		public string BankGroupId { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("BankGroupCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("BankGroupBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("BankGroup")]
		public ICollection<BusinessCollateralAgmLine> BusinessCollateralAgmLineBankGroup { get; set; }
		[InverseProperty("BankGroup")]
		public ICollection<CompanyBank> CompanyBankBankGroup { get; set; }
		[InverseProperty("BankGroup")]
		public ICollection<CreditAppReqBusinessCollateral> CreditAppReqBusinessCollateralBankGroup { get; set; }
		[InverseProperty("BankGroup")]
		public ICollection<CustBank> CustBankBankGroup { get; set; }
		[InverseProperty("BankGroup")]
		public ICollection<CustBusinessCollateral> CustBusinessCollateralBankGroup { get; set; }
		[InverseProperty("BankGroup")]
		public ICollection<FinancialCreditTrans> FinancialCreditTransBankGroup { get; set; }
		[InverseProperty("BankGroup")]
		public ICollection<NCBTrans> NCBTransBankGroup { get; set; }
		[InverseProperty("BankGroup")]
		public ICollection<VendBank> VendBankBankGroup { get; set; }
		[InverseProperty("ChequeBankGroup")]
		public ICollection<ChequeTable> ChequeTableChequeBankGroup { get; set; }
		[InverseProperty("ChequeBankGroup")]
		public ICollection<CollectionFollowUp> CollectionFollowUpChequeBankGroup { get; set; }
		[InverseProperty("ChequeBankGroup")]
		public ICollection<JobCheque> JobChequeChequeBankGroup { get; set; }
		[InverseProperty("ChequeBankGroup")]
		public ICollection<ReceiptTable> ReceiptTableChequeBankGroup { get; set; }
		[InverseProperty("ChequeBankGroup")]
		public ICollection<ReceiptTempPaymDetail> ReceiptTempPaymDetailChequeBankGroup { get; set; }
		#endregion Collection
	}
}
