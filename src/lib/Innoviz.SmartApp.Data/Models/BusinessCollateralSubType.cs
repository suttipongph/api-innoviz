using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class BusinessCollateralSubType : CompanyBaseEntity
	{
		public BusinessCollateralSubType()
		{
			BusinessCollateralAgmLineBusinessCollateralSubType = new HashSet<BusinessCollateralAgmLine>();
			CreditAppReqBusinessCollateralBusinessCollateralSubType = new HashSet<CreditAppReqBusinessCollateral>();
			CustBusinessCollateralBusinessCollateralSubType = new HashSet<CustBusinessCollateral>();
		}
 
		[Key]
		public Guid BusinessCollateralSubTypeGUID { get; set; }
		public int AgreementOrdering { get; set; }
		[StringLength(100)]
		public string AgreementRefText { get; set; }
		[Required]
		[StringLength(20)]
		public string BusinessCollateralSubTypeId { get; set; }
		public Guid BusinessCollateralTypeGUID { get; set; }
		public bool DebtorClaims { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("BusinessCollateralSubTypeCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("BusinessCollateralSubTypeBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BusinessCollateralTypeGUID")]
		[InverseProperty("BusinessCollateralSubTypeBusinessCollateralType")]
		public BusinessCollateralType BusinessCollateralType { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("BusinessCollateralSubType")]
		public ICollection<BusinessCollateralAgmLine> BusinessCollateralAgmLineBusinessCollateralSubType { get; set; }
		[InverseProperty("BusinessCollateralSubType")]
		public ICollection<CreditAppReqBusinessCollateral> CreditAppReqBusinessCollateralBusinessCollateralSubType { get; set; }
		[InverseProperty("BusinessCollateralSubType")]
		public ICollection<CustBusinessCollateral> CustBusinessCollateralBusinessCollateralSubType { get; set; }
		#endregion Collection
	}
}
