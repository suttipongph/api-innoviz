using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class LedgerFiscalPeriod : CompanyBaseEntity
	{
		public LedgerFiscalPeriod()
		{
		}
 
		[Key]
		public Guid LedgerFiscalPeriodGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime EndDate { get; set; }
		public Guid LedgerFiscalYearGUID { get; set; }
		public int PeriodStatus { get; set; }
		[Column(TypeName = "date")]
		public DateTime StartDate { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("LedgerFiscalPeriodCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("LedgerFiscalPeriodBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("LedgerFiscalYearGUID")]
		[InverseProperty("LedgerFiscalPeriodLedgerFiscalYear")]
		public LedgerFiscalYear LedgerFiscalYear { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
