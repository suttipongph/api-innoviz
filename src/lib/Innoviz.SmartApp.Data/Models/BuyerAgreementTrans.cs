using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class BuyerAgreementTrans : CompanyBaseEntity
	{
		public BuyerAgreementTrans()
		{
		}
 
		[Key]
		public Guid BuyerAgreementTransGUID { get; set; }
		public Guid BuyerAgreementLineGUID { get; set; }
		public Guid BuyerAgreementTableGUID { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("BuyerAgreementTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("BuyerAgreementTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BuyerAgreementLineGUID")]
		[InverseProperty("BuyerAgreementTransBuyerAgreementLine")]
		public BuyerAgreementLine BuyerAgreementLine { get; set; }
		[ForeignKey("BuyerAgreementTableGUID")]
		[InverseProperty("BuyerAgreementTransBuyerAgreementTable")]
		public BuyerAgreementTable BuyerAgreementTable { get; set; }
		#endregion ForeignKey
	}
}
