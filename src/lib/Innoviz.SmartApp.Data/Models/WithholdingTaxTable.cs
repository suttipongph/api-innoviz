using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class WithholdingTaxTable : CompanyBaseEntity
	{
		public WithholdingTaxTable()
		{
			FreeTextInvoiceTableWithholdingTaxTable = new HashSet<FreeTextInvoiceTable>();
			IntercompanyInvoiceTableWithholdingTaxTable = new HashSet<IntercompanyInvoiceTable>();
			InvoiceLineWithholdingTaxTable = new HashSet<InvoiceLine>();
			InvoiceRevenueTypeFeeWHT = new HashSet<InvoiceRevenueType>();
			LeaseTypeWithholdingTaxTable = new HashSet<LeaseType>();
			PaymentHistoryWithholdingTaxTable = new HashSet<PaymentHistory>();
			ReceiptLineWithholdingTaxTable = new HashSet<ReceiptLine>();
			ServiceFeeTransWithholdingTaxTable = new HashSet<ServiceFeeTrans>();
			WithholdingTaxValueWithholdingTaxTable = new HashSet<WithholdingTaxValue>();
		}
 
		[Key]
		public Guid WithholdingTaxTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[StringLength(20)]
		public string LedgerAccount { get; set; }
		[Required]
		[StringLength(20)]
		public string WHTCode { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("WithholdingTaxTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("WithholdingTaxTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("WithholdingTaxTable")]
		public ICollection<FreeTextInvoiceTable> FreeTextInvoiceTableWithholdingTaxTable { get; set; }
		[InverseProperty("WithholdingTaxTable")]
		public ICollection<IntercompanyInvoiceTable> IntercompanyInvoiceTableWithholdingTaxTable { get; set; }
		[InverseProperty("WithholdingTaxTable")]
		public ICollection<InvoiceLine> InvoiceLineWithholdingTaxTable { get; set; }
		[InverseProperty("FeeWHT")]
		public ICollection<InvoiceRevenueType> InvoiceRevenueTypeFeeWHT { get; set; }
		[InverseProperty("WithholdingTaxTable")]
		public ICollection<LeaseType> LeaseTypeWithholdingTaxTable { get; set; }
		[InverseProperty("WithholdingTaxTable")]
		public ICollection<PaymentHistory> PaymentHistoryWithholdingTaxTable { get; set; }
		[InverseProperty("WithholdingTaxTable")]
		public ICollection<ReceiptLine> ReceiptLineWithholdingTaxTable { get; set; }
		[InverseProperty("WithholdingTaxTable")]
		public ICollection<ServiceFeeTrans> ServiceFeeTransWithholdingTaxTable { get; set; }
		[InverseProperty("WithholdingTaxTable")]
		public ICollection<WithholdingTaxValue> WithholdingTaxValueWithholdingTaxTable { get; set; }
		#endregion Collection
	}
}
