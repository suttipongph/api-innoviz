using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class LeaseType : CompanyBaseEntity
	{
		public LeaseType()
		{
			ApplicationTableLeaseType = new HashSet<ApplicationTable>();
		}
 
		[Key]
		public Guid LeaseTypeGUID { get; set; }
		[StringLength(20)]
		public string AdvPaymLedgerAccount { get; set; }
		[StringLength(20)]
		public string ARLedgerAccount { get; set; }
		public bool CalcAccInstallment { get; set; }
		public bool CalcAccInterest { get; set; }
		public bool CalcSheet_EnableAdvPaym { get; set; }
		public bool CalcSheet_EnableBalloonPaym { get; set; }
		public bool CalcSheet_EnableDeposit { get; set; }
		public bool CalcSheet_EnableDown { get; set; }
		public bool CalcSheet_EnableRepurchase { get; set; }
		public bool CalcSheet_EnableResidualValue { get; set; }
		public int CalcSheet_InterestRateType { get; set; }
		public int CalcSheet_RoundingMethod { get; set; }
		public int CalcSheet_RoundingPaymAmount { get; set; }
		public int CalcSheet_RoundingType { get; set; }
		[StringLength(20)]
		public string CostLedgerAccount { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[StringLength(20)]
		public string DownLedgerAccount { get; set; }
		public int EarlyPayoffDiscBase { get; set; }
		[Column(TypeName = "decimal")]
		public decimal EarlyPayoffDiscRate { get; set; }
		public int FVFormula { get; set; }
		public int GenTaxInvoiceGraceDay { get; set; }
		public Guid? IncomeNormalToNPLRateGUID { get; set; }
		public Guid? IncomeNPLToNormalRateGUID { get; set; }
		[StringLength(20)]
		public string IntIncomeLedgerAccount { get; set; }
		[Required]
		[StringLength(20)]
		public string LeaseTypeId { get; set; }
		public int PaymDayOfODLetter { get; set; }
		public Guid? PaymentStructureTableGUID { get; set; }
		public int PenaltyDayPerPeriod { get; set; }
		[Column(TypeName = "decimal")]
		public decimal PenaltyFactor { get; set; }
		public int PenaltyGraceDay { get; set; }
		public Guid? PenaltySetupGUID { get; set; }
		public bool PostAdvancePaym { get; set; }
		public bool PostClosing { get; set; }
		public bool PostCollectionFee { get; set; }
		public bool PostDeposit { get; set; }
		public bool PostDepositReturn { get; set; }
		public bool PostDownPaym { get; set; }
		public bool PostExecution { get; set; }
		public bool PostIncomeRealization { get; set; }
		public bool PostInstallment { get; set; }
		public bool PostInvoice { get; set; }
		public bool PostPayment { get; set; }
		public bool PostPenalty { get; set; }
		public bool PostPurchaseAgreement { get; set; }
		public bool PostTaxRealization { get; set; }
		public bool PostTermination { get; set; }
		public bool PostWriteOff { get; set; }
		public int PVFormula { get; set; }
		public Guid? TaxCostGUID { get; set; }
		public Guid? TaxInstallmentGUID { get; set; }
		public Guid? TaxNormalToNPLRateGUID { get; set; }
		public Guid? TaxWriteOffGUID { get; set; }
		[StringLength(20)]
		public string TerminateLedgerAccount { get; set; }
		public Guid? TerminatePaymentStructureGUID { get; set; }
		[StringLength(20)]
		public string UnearedIntLedgerAccount { get; set; }
		public int WHTBase { get; set; }
		public int WHTMaximumTerm { get; set; }
		public Guid? WithholdingTaxTableGUID { get; set; }
		[StringLength(20)]
		public string WriteOffLedgerAccount { get; set; }
		[StringLength(20)]
		public string WriteOffRecoveryLedgerAccount { get; set; }
		[StringLength(20)]
		public string WriteOffUnearnedLedgerAccount { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("LeaseTypeCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("LeaseTypeBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("TaxCostGUID")]
		[InverseProperty("LeaseTypeTaxCost")]
		public TaxTable TaxCost { get; set; }
		[ForeignKey("TaxInstallmentGUID")]
		[InverseProperty("LeaseTypeTaxInstallment")]
		public TaxTable TaxInstallment { get; set; }
		[ForeignKey("TaxWriteOffGUID")]
		[InverseProperty("LeaseTypeTaxWriteOff")]
		public TaxTable TaxWriteOff { get; set; }
		[ForeignKey("WithholdingTaxTableGUID")]
		[InverseProperty("LeaseTypeWithholdingTaxTable")]
		public WithholdingTaxTable WithholdingTaxTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("LeaseType")]
		public ICollection<ApplicationTable> ApplicationTableLeaseType { get; set; }
		#endregion Collection
	}
}
