using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class NumberSeqSegment : CompanyBaseEntity
	{
		public NumberSeqSegment()
		{
		}
 
		[Key]
		public Guid NumberSeqSegmentGUID { get; set; }
		public Guid NumberSeqTableGUID { get; set; }
		public int Ordering { get; set; }
		public int SegmentType { get; set; }
		[StringLength(10)]
		public string SegmentValue { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("NumberSeqSegmentCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("NumberSeqSegmentBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("NumberSeqTableGUID")]
		[InverseProperty("NumberSeqSegmentNumberSeqTable")]
		public NumberSeqTable NumberSeqTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
