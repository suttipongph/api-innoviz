using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class WithholdingTaxGroup : CompanyBaseEntity
	{
		public WithholdingTaxGroup()
		{
			CustomerTableWithholdingTaxGroup = new HashSet<CustomerTable>();
		}
 
		[Key]
		public Guid WithholdingTaxGroupGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string WHTGroupId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("WithholdingTaxGroupCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("WithholdingTaxGroupBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("WithholdingTaxGroup")]
		public ICollection<CustomerTable> CustomerTableWithholdingTaxGroup { get; set; }
		#endregion Collection
	}
}
