﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_ContactPersonTrans
	{
		[Key]
		[StringLength(50)]
		public string ContactPersonTransGUID { get; set; }
		public bool InActive { get; set; }
		[Required]
		[StringLength(50)]
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		[Required]
		[StringLength(50)]
		public string RelatedPersonTableGUID { get; set; }


		[Required]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		[StringLength(50)]
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		[StringLength(50)]
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		public string OwnerBusinessUnitGUID { get; set; }
		public int MigrateStatus { get; set; }
		public int MigrateSource { get; set; }
	}
}
