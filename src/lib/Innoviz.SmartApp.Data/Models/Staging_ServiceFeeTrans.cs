﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_ServiceFeeTrans
	{
		[Key]
		[Column(Order = 1)]
		[StringLength(50)]
		public string ServiceFeeTransGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal AmountBeforeTax { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal AmountIncludeTax { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[StringLength(50)]
		public string Dimension1GUID { get; set; }
		[StringLength(50)]
		public string Dimension2GUID { get; set; }
		[StringLength(50)]
		public string Dimension3GUID { get; set; }
		[StringLength(50)]
		public string Dimension4GUID { get; set; }
		[StringLength(50)]
		public string Dimension5GUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal FeeAmount { get; set; }
		public bool IncludeTax { get; set; }
		[Required]
		[StringLength(50)]
		public string InvoiceRevenueTypeGUID { get; set; }
		[Key]
		[Column(Order = 3)]
		public int Ordering { get; set; }
		[Key]
		[Column(Order = 2)]
		[StringLength(50)]
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleInvoiceAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleWHTAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TaxAmount { get; set; }
		[StringLength(50)]
		public string TaxTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal WHTAmount { get; set; }
		[StringLength(50)]
		public string WithholdingTaxTableGUID { get; set; }
		public bool WHTSlipReceivedByCustomer { get; set; }
		[Key]
		[Column(Order = 4)]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		[StringLength(50)]
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		[StringLength(50)]
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		public string OwnerBusinessUnitGUID { get; set; }
		public int MigrateStatus { get; set; }
		public int MigrateSource { get; set; }
	}
}
