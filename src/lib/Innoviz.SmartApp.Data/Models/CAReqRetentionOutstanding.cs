using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CAReqRetentionOutstanding : CompanyBaseEntity
	{
		public CAReqRetentionOutstanding()
		{
		}
 
		[Key]
		public Guid CAReqRetentionOutstandingGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal AccumRetentionAmount { get; set; }
		public Guid? CreditAppRequestTableGUID { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public Guid? CustomerTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal MaximumRetention { get; set; }
		public int ProductType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal RemainingAmount { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CAReqRetentionOutstandingCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CAReqRetentionOutstandingBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("CreditAppRequestTableGUID")]
		[InverseProperty("CAReqRetentionOutstandingCreditAppRequestTable")]
		public CreditAppRequestTable CreditAppRequestTable { get; set; }
		[ForeignKey("CreditAppTableGUID")]
		[InverseProperty("CAReqRetentionOutstandingCreditAppTable")]
		public CreditAppTable CreditAppTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
