using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_ProjectProgressTable
	{

		[Key]
		[Column(Order = 1)]
		[StringLength(50)]
		public string ProjectProgressTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Key]
		[Column(Order = 2)]
		[StringLength(20)]
		public string ProjectProgressId { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		public string TransDate { get; set; }
		[Required]
		[StringLength(50)]
		public string WithdrawalTableGUID { get; set; }

		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }

		[Required]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }
	}
}


