using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CustBusinessCollateral : CompanyBaseEntity
	{
		public CustBusinessCollateral()
		{
			CreditAppReqBusinessCollateralCustBusinessCollateral = new HashSet<CreditAppReqBusinessCollateral>();
		}
 
		[Key]
		public Guid CustBusinessCollateralGUID { get; set; }
		[StringLength(20)]
		public string AccountNumber { get; set; }
		public Guid? BankGroupGUID { get; set; }
		public Guid? BankTypeGUID { get; set; }
		public Guid BusinessCollateralAgmLineGUID { get; set; }
		public Guid? BusinessCollateralStatusGUID { get; set; }
		public Guid BusinessCollateralSubTypeGUID { get; set; }
		public Guid BusinessCollateralTypeGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal BusinessCollateralValue { get; set; }
		[StringLength(100)]
		public string BuyerName { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		[StringLength(20)]
		public string BuyerTaxIdentificationId { get; set; }
		public bool Cancelled { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CapitalValuation { get; set; }
		[StringLength(100)]
		public string ChassisNumber { get; set; }
		public Guid CreditAppRequestTableGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string CustBusinessCollateralId { get; set; }
		public Guid CustomerTableGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime? DateOfValuation { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal DBDRegistrationAmount { get; set; }
		[Column(TypeName = "date")]
		public DateTime? DBDRegistrationDate { get; set; }
		[StringLength(100)]
		public string DBDRegistrationDescription { get; set; }
		[StringLength(20)]
		public string DBDRegistrationId { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal GuaranteeAmount { get; set; }
		[StringLength(100)]
		public string Lessee { get; set; }
		[StringLength(100)]
		public string Lessor { get; set; }
		[StringLength(100)]
		public string MachineNumber { get; set; }
		[StringLength(100)]
		public string MachineRegisteredStatus { get; set; }
		[StringLength(100)]
		public string Ownership { get; set; }
		public int PreferentialCreditorNumber { get; set; }
		[StringLength(100)]
		public string ProjectName { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Quantity { get; set; }
		[Column(TypeName = "date")]
		public DateTime? RefAgreementDate { get; set; }
		[StringLength(20)]
		public string RefAgreementId { get; set; }
		[StringLength(100)]
		public string RegisteredPlace { get; set; }
		[StringLength(100)]
		public string RegistrationPlateNumber { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		[StringLength(100)]
		public string TitleDeedDistrict { get; set; }
		[StringLength(100)]
		public string TitleDeedNumber { get; set; }
		[StringLength(100)]
		public string TitleDeedProvince { get; set; }
		[StringLength(100)]
		public string TitleDeedSubDistrict { get; set; }
		[StringLength(20)]
		public string Unit { get; set; }
		[StringLength(100)]
		public string ValuationCommittee { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CustBusinessCollateralCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CustBusinessCollateralBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BankGroupGUID")]
		[InverseProperty("CustBusinessCollateralBankGroup")]
		public BankGroup BankGroup { get; set; }
		[ForeignKey("BankTypeGUID")]
		[InverseProperty("CustBusinessCollateralBankType")]
		public BankType BankType { get; set; }
		[ForeignKey("BusinessCollateralAgmLineGUID")]
		[InverseProperty("CustBusinessCollateralBusinessCollateralAgmLine")]
		public BusinessCollateralAgmLine BusinessCollateralAgmLine { get; set; }
		[ForeignKey("BusinessCollateralStatusGUID")]
		[InverseProperty("CustBusinessCollateralBusinessCollateralStatus")]
		public BusinessCollateralStatus BusinessCollateralStatus { get; set; }
		[ForeignKey("BusinessCollateralSubTypeGUID")]
		[InverseProperty("CustBusinessCollateralBusinessCollateralSubType")]
		public BusinessCollateralSubType BusinessCollateralSubType { get; set; }
		[ForeignKey("BusinessCollateralTypeGUID")]
		[InverseProperty("CustBusinessCollateralBusinessCollateralType")]
		public BusinessCollateralType BusinessCollateralType { get; set; }
		[ForeignKey("BuyerTableGUID")]
		[InverseProperty("CustBusinessCollateralBuyerTable")]
		public BuyerTable BuyerTable { get; set; }
		[ForeignKey("CreditAppRequestTableGUID")]
		[InverseProperty("CustBusinessCollateralCreditAppRequestTable")]
		public CreditAppRequestTable CreditAppRequestTable { get; set; }
		[ForeignKey("CustomerTableGUID")]
		[InverseProperty("CustBusinessCollateralCustomerTable")]
		public CustomerTable CustomerTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("CustBusinessCollateral")]
		public ICollection<CreditAppReqBusinessCollateral> CreditAppReqBusinessCollateralCustBusinessCollateral { get; set; }
		#endregion Collection
	}
}
