using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class DocumentReturnMethod : CompanyBaseEntity {
		public DocumentReturnMethod()
		{
			DocumentReturnTableDocumentReturnMethod = new HashSet<DocumentReturnTable>();
		}
		[Key]
		public Guid DocumentReturnMethodGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string DocumentReturnMethodId { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("DocumentReturnMethodCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("DocumentReturnMethodBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey

		[InverseProperty("DocumentReturnMethod")]
		public ICollection<DocumentReturnTable> DocumentReturnTableDocumentReturnMethod { get; set; }
	}
}
