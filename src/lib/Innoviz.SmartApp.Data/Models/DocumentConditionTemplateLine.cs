using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class DocumentConditionTemplateLine : CompanyBaseEntity
	{
		public DocumentConditionTemplateLine()
		{
		}
 
		[Key]
		public Guid DocumentConditionTemplateLineGUID { get; set; }
		public Guid? DocumentConditionTemplateTableGUID { get; set; }
		public Guid DocumentTypeGUID { get; set; }
		public bool Mandatory { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("DocumentConditionTemplateLineCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("DocumentConditionTemplateLineBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("DocumentConditionTemplateTableGUID")]
		[InverseProperty("DocumentConditionTemplateLineDocumentConditionTemplateTable")]
		public DocumentConditionTemplateTable DocumentConditionTemplateTable { get; set; }
		[ForeignKey("DocumentTypeGUID")]
		[InverseProperty("DocumentConditionTemplateLineDocumentType")]
		public DocumentType DocumentType { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
