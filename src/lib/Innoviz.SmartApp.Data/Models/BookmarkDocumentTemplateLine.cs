using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class BookmarkDocumentTemplateLine : CompanyBaseEntity
	{
		public BookmarkDocumentTemplateLine()
		{
		}
 
		[Key]
		public Guid BookmarkDocumentTemplateLineGUID { get; set; }
		public Guid BookmarkDocumentGUID { get; set; }
		public Guid? BookmarkDocumentTemplateTableGUID { get; set; }
		public Guid DocumentTemplateTableGUID { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("BookmarkDocumentTemplateLineCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("BookmarkDocumentTemplateLineBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BookmarkDocumentGUID")]
		[InverseProperty("BookmarkDocumentTemplateLineBookmarkDocument")]
		public BookmarkDocument BookmarkDocument { get; set; }
		[ForeignKey("BookmarkDocumentTemplateTableGUID")]
		[InverseProperty("BookmarkDocumentTemplateLineBookmarkDocumentTemplateTable")]
		public BookmarkDocumentTemplateTable BookmarkDocumentTemplateTable { get; set; }
		[ForeignKey("DocumentTemplateTableGUID")]
		[InverseProperty("BookmarkDocumentTemplateLineDocumentTemplateTable")]
		public DocumentTemplateTable DocumentTemplateTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
