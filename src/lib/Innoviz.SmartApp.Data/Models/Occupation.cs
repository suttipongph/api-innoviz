using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Occupation : CompanyBaseEntity
	{
		public Occupation()
		{
			CustomerTableOccupation = new HashSet<CustomerTable>();
			RelatedPersonTableOccupation = new HashSet<RelatedPersonTable>();
		}
 
		[Key]
		public Guid OccupationGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string OccupationId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("OccupationCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("OccupationBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("Occupation")]
		public ICollection<CustomerTable> CustomerTableOccupation { get; set; }
		[InverseProperty("Occupation")]
		public ICollection<RelatedPersonTable> RelatedPersonTableOccupation { get; set; }
		#endregion Collection
	}
}
