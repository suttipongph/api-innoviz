﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Innoviz.SmartApp.Core.Models;

namespace Innoviz.SmartApp.Data.Models
{
   
    public partial class SysRoleTable : CompanyBaseEntity
    {

        
        public Guid Id { get; set; }
        [StringLength(256)]
        public string Name { get; set; }
        [StringLength(256)]
        public string NormalizedName { get; set; }

        public string DisplayName { set; get; }

        public override Guid CompanyGUID { get; set; }

        public int SiteLoginType { get; set; }

        #region ForeignKey
        [ForeignKey("CompanyGUID")]
        [InverseProperty("SysRoleTableCompany")]
        public Company Company { get; set; }
        [ForeignKey("OwnerBusinessUnitGUID")]
        [InverseProperty("SysRoleTableBusinessUnit")]
        public BusinessUnit BusinessUnit { get; set; }
        #endregion ForeignKey

    }
}
