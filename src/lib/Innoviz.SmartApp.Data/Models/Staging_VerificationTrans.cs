using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_VerificationTrans
	{
		[Key]
		[StringLength(50)]
		public string VerificationTransGUID { get; set; }
		[StringLength(50)]
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		[Required]
		[StringLength(50)]
		public string VerificationTableGUID { get; set; }
		[StringLength(20)]
		public string DocumentId { get; set; }
		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }
		[Required]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }

	}
}
