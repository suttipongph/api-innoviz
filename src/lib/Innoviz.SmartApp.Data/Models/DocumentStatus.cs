using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class DocumentStatus : BaseEntity
	{
		public DocumentStatus()
		{
			ApplicationTableDocumentStatus = new HashSet<ApplicationTable>();
			AssignmentAgreementTableDocumentStatus = new HashSet<AssignmentAgreementTable>();
			BookmarkDocumentTransDocumentStatus = new HashSet<BookmarkDocumentTrans>();
			BusinessCollateralAgmTableDocumentStatus = new HashSet<BusinessCollateralAgmTable>();
			BuyerTableDocumentStatus = new HashSet<BuyerTable>();
			ChequeTableDocumentStatus = new HashSet<ChequeTable>();
			ConsortiumTableDocumentStatus = new HashSet<ConsortiumTable>();
			CreditAppRequestTableDocumentStatus = new HashSet<CreditAppRequestTable>();
			CustomerRefundTableDocumentStatus = new HashSet<CustomerRefundTable>();
			CustomerTableDocumentStatus = new HashSet<CustomerTable>();
			DocumentReturnTableDocumentStatus = new HashSet<DocumentReturnTable>();
			FreeTextInvoiceTableDocumentStatus = new HashSet<FreeTextInvoiceTable>();
			GuarantorAgreementTableDocumentStatus = new HashSet<GuarantorAgreementTable>();
			IntercompanyInvoiceSettlementDocumentStatus = new HashSet<IntercompanyInvoiceSettlement>();
			IntercompanyInvoiceTableDocumentStatus = new HashSet<IntercompanyInvoiceTable>();
			InvoiceTableDocumentStatus = new HashSet<InvoiceTable>();
			MainAgreementTableDocumentStatus = new HashSet<MainAgreementTable>();
			MessengerJobTableDocumentStatus = new HashSet<MessengerJobTable>();
			PurchaseTableDocumentStatus = new HashSet<PurchaseTable>();
			ReceiptTableDocumentStatus = new HashSet<ReceiptTable>();
			ReceiptTempTableDocumentStatus = new HashSet<ReceiptTempTable>();
			TaxInvoiceTableDocumentStatus = new HashSet<TaxInvoiceTable>();
			VendorPaymentTransDocumentStatus = new HashSet<VendorPaymentTrans>();
			VerificationTableDocumentStatus = new HashSet<VerificationTable>();
			WithdrawalTableDocumentStatus = new HashSet<WithdrawalTable>();
		}
 
		[Key]
		public Guid DocumentStatusGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public Guid DocumentProcessGUID { get; set; }
		[Required]
		[StringLength(10)]
		public string StatusCode { get; set; }
		[Required]
		[StringLength(20)]
		public string StatusId { get; set; }
 
		#region ForeignKey
		[ForeignKey("DocumentProcessGUID")]
		[InverseProperty("DocumentStatusDocumentProcess")]
		public DocumentProcess DocumentProcess { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("DocumentStatus")]
		public ICollection<ApplicationTable> ApplicationTableDocumentStatus { get; set; }
		[InverseProperty("DocumentStatus")]
		public ICollection<AssignmentAgreementTable> AssignmentAgreementTableDocumentStatus { get; set; }
		[InverseProperty("DocumentStatus")]
		public ICollection<BookmarkDocumentTrans> BookmarkDocumentTransDocumentStatus { get; set; }
		[InverseProperty("DocumentStatus")]
		public ICollection<BusinessCollateralAgmTable> BusinessCollateralAgmTableDocumentStatus { get; set; }
		[InverseProperty("DocumentStatus")]
		public ICollection<BuyerTable> BuyerTableDocumentStatus { get; set; }
		[InverseProperty("DocumentStatus")]
		public ICollection<ChequeTable> ChequeTableDocumentStatus { get; set; }
		[InverseProperty("DocumentStatus")]
		public ICollection<ConsortiumTable> ConsortiumTableDocumentStatus { get; set; }
		[InverseProperty("DocumentStatus")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTableDocumentStatus { get; set; }
		[InverseProperty("DocumentStatus")]
		public ICollection<CustomerRefundTable> CustomerRefundTableDocumentStatus { get; set; }
		[InverseProperty("DocumentStatus")]
		public ICollection<CustomerTable> CustomerTableDocumentStatus { get; set; }
		[InverseProperty("DocumentStatus")]
		public ICollection<DocumentReturnTable> DocumentReturnTableDocumentStatus { get; set; }
		[InverseProperty("DocumentStatus")]
		public ICollection<FreeTextInvoiceTable> FreeTextInvoiceTableDocumentStatus { get; set; }
		[InverseProperty("DocumentStatus")]
		public ICollection<GuarantorAgreementTable> GuarantorAgreementTableDocumentStatus { get; set; }
		[InverseProperty("DocumentStatus")]
		public ICollection<IntercompanyInvoiceSettlement> IntercompanyInvoiceSettlementDocumentStatus { get; set; }
		[InverseProperty("DocumentStatus")]
		public ICollection<IntercompanyInvoiceTable> IntercompanyInvoiceTableDocumentStatus { get; set; }
		[InverseProperty("DocumentStatus")]
		public ICollection<InvoiceTable> InvoiceTableDocumentStatus { get; set; }
		[InverseProperty("DocumentStatus")]
		public ICollection<MainAgreementTable> MainAgreementTableDocumentStatus { get; set; }
		[InverseProperty("DocumentStatus")]
		public ICollection<MessengerJobTable> MessengerJobTableDocumentStatus { get; set; }
		[InverseProperty("DocumentStatus")]
		public ICollection<PurchaseTable> PurchaseTableDocumentStatus { get; set; }
		[InverseProperty("DocumentStatus")]
		public ICollection<ReceiptTable> ReceiptTableDocumentStatus { get; set; }
		[InverseProperty("DocumentStatus")]
		public ICollection<ReceiptTempTable> ReceiptTempTableDocumentStatus { get; set; }
		[InverseProperty("DocumentStatus")]
		public ICollection<TaxInvoiceTable> TaxInvoiceTableDocumentStatus { get; set; }
		[InverseProperty("DocumentStatus")]
		public ICollection<VendorPaymentTrans> VendorPaymentTransDocumentStatus { get; set; }
		[InverseProperty("DocumentStatus")]
		public ICollection<VerificationTable> VerificationTableDocumentStatus { get; set; }
		[InverseProperty("DocumentStatus")]
		public ICollection<WithdrawalTable> WithdrawalTableDocumentStatus { get; set; }
		#endregion Collection
	}
}
