using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class PaymentHistory : BranchCompanyBaseEntity
	{
		public PaymentHistory()
		{
			ProcessTransPaymentHistory = new HashSet<ProcessTrans>();
		}
 
		[Key]
		public Guid PaymentHistoryGUID { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public bool Cancel { get; set; }
		public Guid CurrencyGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid CustTransGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime? DueDate { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ExchangeRate { get; set; }
		public Guid? InvoiceSettlementDetailGUID { get; set; }
		public Guid InvoiceTableGUID { get; set; }
		public Guid? OrigTaxTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal PaymentAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal PaymentAmountMST { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal PaymentBaseAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal PaymentBaseAmountMST { get; set; }
		[Column(TypeName = "date")]
		public DateTime PaymentDate { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal PaymentTaxAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal PaymentTaxAmountMST { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal PaymentTaxBaseAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal PaymentTaxBaseAmountMST { get; set; }
		public int ProductType { get; set; }
		public Guid? ReceiptTableGUID { get; set; }
		public int ReceivedFrom { get; set; }
		[Column(TypeName = "date")]
		public DateTime ReceivedDate { get; set; }
		public Guid? RefGUID { get; set; }
		public int RefType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal WHTAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal WHTAmountMST { get; set; }
		public Guid? WithholdingTaxTableGUID { get; set; }
		public bool WHTSlipReceivedByCustomer { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("PaymentHistoryCompany")]
		public Company Company { get; set; }
		[ForeignKey("BranchGUID")]
		[InverseProperty("PaymentHistoryBranch")]
		public Branch Branch { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("PaymentHistoryBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("CreditAppTableGUID")]
		[InverseProperty("PaymentHistoryCreditAppTable")]
		public CreditAppTable CreditAppTable { get; set; }
		[ForeignKey("CurrencyGUID")]
		[InverseProperty("PaymentHistoryCurrency")]
		public Currency Currency { get; set; }
		[ForeignKey("CustomerTableGUID")]
		[InverseProperty("PaymentHistoryCustomerTable")]
		public CustomerTable CustomerTable { get; set; }
		[ForeignKey("InvoiceTableGUID")]
		[InverseProperty("PaymentHistoryInvoiceTable")]
		public InvoiceTable InvoiceTable { get; set; }
		[ForeignKey("ReceiptTableGUID")]
		[InverseProperty("PaymentHistoryReceiptTable")]
		public ReceiptTable ReceiptTable { get; set; }
		[ForeignKey("WithholdingTaxTableGUID")]
		[InverseProperty("PaymentHistoryWithholdingTaxTable")]
		public WithholdingTaxTable WithholdingTaxTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("PaymentHistory")]
		public ICollection<ProcessTrans> ProcessTransPaymentHistory { get; set; }
		#endregion Collection
	}
}
