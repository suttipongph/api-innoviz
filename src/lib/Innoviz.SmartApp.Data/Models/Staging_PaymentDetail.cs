using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_PaymentDetail
	{
		[Key]
		[StringLength(50)]
		public string PaymentDetailGUID { get; set; }
		[StringLength(50)]
		public string CustomerTableGUID { get; set; }
		[StringLength(50)]
		public string InvoiceTypeGUID { get; set; }
		public int PaidToType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal PaymentAmount { get; set; }
		[Required]
		[StringLength(50)]
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		public bool SuspenseTransfer { get; set; }
		[StringLength(50)]
		public string VendorTableGUID { get; set; }
		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }
		[Required]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }
	}
}
