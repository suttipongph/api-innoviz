using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CollectionFollowUp : CompanyBaseEntity
	{
		public CollectionFollowUp()
		{
			JobChequeCollectionFollowUp = new HashSet<JobCheque>();
		}
 
		[Key]
		public Guid CollectionFollowUpGUID { get; set; }
		public Guid BuyerTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ChequeAmount { get; set; }
		[StringLength(20)]
		public string ChequeBankAccNo { get; set; }
		public Guid? ChequeBankGroupGUID { get; set; }
		[StringLength(100)]
		public string ChequeBranch { get; set; }
		public int ChequeCollectionResult { get; set; }
		[Column(TypeName = "date")]
		public DateTime? ChequeDate { get; set; }
		[StringLength(20)]
		public string ChequeNo { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CollectionAmount { get; set; }
		[Column(TypeName = "date")]
		public DateTime CollectionDate { get; set; }
		public int CollectionFollowUpResult { get; set; }
		public Guid? CreditAppLineGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string DocumentId { get; set; }
		public Guid MethodOfPaymentGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime? NewCollectionDate { get; set; }
		public int ReceivedFrom { get; set; }
		[StringLength(100)]
		public string RecipientName { get; set; }
		public Guid? RefGUID { get; set; }
		public int RefType { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CollectionFollowUpCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CollectionFollowUpBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BuyerTableGUID")]
		[InverseProperty("CollectionFollowUpBuyerTable")]
		public BuyerTable BuyerTable { get; set; }
		[ForeignKey("ChequeBankGroupGUID")]
		[InverseProperty("CollectionFollowUpChequeBankGroup")]
		public BankGroup ChequeBankGroup { get; set; }
		[ForeignKey("CreditAppLineGUID")]
		[InverseProperty("CollectionFollowUpCreditAppLine")]
		public CreditAppLine CreditAppLine { get; set; }
		[ForeignKey("CustomerTableGUID")]
		[InverseProperty("CollectionFollowUpCustomerTable")]
		public CustomerTable CustomerTable { get; set; }
		[ForeignKey("MethodOfPaymentGUID")]
		[InverseProperty("CollectionFollowUpMethodOfPayment")]
		public MethodOfPayment MethodOfPayment { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("CollectionFollowUp")]
		public ICollection<JobCheque> JobChequeCollectionFollowUp { get; set; }
		#endregion Collection
	}
}
