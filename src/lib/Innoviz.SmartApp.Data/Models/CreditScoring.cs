using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CreditScoring : CompanyBaseEntity
	{
		public CreditScoring()
		{
			BuyerTableCreditScoring = new HashSet<BuyerTable>();
			CreditAppRequestLineCreditScoring = new HashSet<CreditAppRequestLine>();
			CreditAppRequestTableCreditScoring = new HashSet<CreditAppRequestTable>();
			CustomerTableCreditScoring = new HashSet<CustomerTable>();
		}
 
		[Key]
		public Guid CreditScoringGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string CreditScoringId { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CreditScoringCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CreditScoringBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("CreditScoring")]
		public ICollection<BuyerTable> BuyerTableCreditScoring { get; set; }
		[InverseProperty("CreditScoring")]
		public ICollection<CreditAppRequestLine> CreditAppRequestLineCreditScoring { get; set; }
		[InverseProperty("CreditScoring")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTableCreditScoring { get; set; }
		[InverseProperty("CreditScoring")]
		public ICollection<CustomerTable> CustomerTableCreditScoring { get; set; }
		#endregion Collection
	}
}
