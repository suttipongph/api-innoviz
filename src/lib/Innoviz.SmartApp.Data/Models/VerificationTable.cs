using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class VerificationTable : CompanyBaseEntity
	{
		public VerificationTable()
		{
			VerificationLineVerificationTable = new HashSet<VerificationLine>();
			VerificationTransVerificationTable = new HashSet<VerificationTrans>();
		}
 
		[Key]
		public Guid VerificationTableGUID { get; set; }
		public Guid BuyerTableGUID { get; set; }
		public Guid CreditAppTableGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		[Column(TypeName = "date")]
		public DateTime VerificationDate { get; set; }
		[Required]
		[StringLength(20)]
		public string VerificationId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("VerificationTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("VerificationTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BuyerTableGUID")]
		[InverseProperty("VerificationTableBuyerTable")]
		public BuyerTable BuyerTable { get; set; }
		[ForeignKey("CreditAppTableGUID")]
		[InverseProperty("VerificationTableCreditAppTable")]
		public CreditAppTable CreditAppTable { get; set; }
		[ForeignKey("CustomerTableGUID")]
		[InverseProperty("VerificationTableCustomerTable")]
		public CustomerTable CustomerTable { get; set; }
		[ForeignKey("DocumentStatusGUID")]
		[InverseProperty("VerificationTableDocumentStatus")]
		public DocumentStatus DocumentStatus { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("VerificationTable")]
		public ICollection<VerificationLine> VerificationLineVerificationTable { get; set; }
		[InverseProperty("VerificationTable")]
		public ICollection<VerificationTrans> VerificationTransVerificationTable { get; set; }
		#endregion Collection
	}
}
