﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_CreditAppRequestTable
	{
		[Key]
		[Column(Order = 1)]
		[StringLength(50)]
		public string CreditAppRequestTableGUID { get; set; }
		[StringLength(50)]
		public string ApplicationTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ApprovedCreditLimitRequest { get; set; }
		[StringLength(500)]
		public string ApproverComment { get; set; }
		[StringLength(50)]
		public string BankAccountControlGUID { get; set; }
		[StringLength(50)]
		public string BillingAddressGUID { get; set; }
		[StringLength(50)]
		public string BillingContactPersonGUID { get; set; }
		[StringLength(50)]
		public string BlacklistStatusGUID { get; set; }
		[StringLength(250)]
		public string CACondition { get; set; }
		[StringLength(50)]
		public string ConsortiumTableGUID { get; set; }
		[Key]
		[Column(Order = 2)]
		[StringLength(20)]
		public string CreditAppRequestId { get; set; }
		public int CreditAppRequestType { get; set; }
		[StringLength(500)]
		public string CreditComment { get; set; }
		public int CreditLimitExpiration { get; set; }
		[StringLength(100)]
		public string CreditLimitRemark { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CreditLimitRequest { get; set; }
		[Required]
		[StringLength(50)]
		public string CreditLimitTypeGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CreditRequestFeeAmount { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal CreditRequestFeePct { get; set; }
		[StringLength(50)]
		public string CreditScoringGUID { get; set; }
		[StringLength(50)]
		public string CreditTermGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CustomerCreditLimit { get; set; }
		[Required]
		[StringLength(50)]
		public string CustomerTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[StringLength(50)]
		public string Dimension1GUID { get; set; }
		[StringLength(50)]
		public string Dimension2GUID { get; set; }
		[StringLength(50)]
		public string Dimension3GUID { get; set; }
		[StringLength(50)]
		public string Dimension4GUID { get; set; }
		[StringLength(50)]
		public string Dimension5GUID { get; set; }
		[StringLength(50)]
		public string DocumentReasonGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string DocumentStatusGUID { get; set; }
		[StringLength(50)]
		public string ExpiryDate { get; set; }
		[StringLength(50)]
		public string ExtensionServiceFeeCondTemplateGUID { get; set; }
		[StringLength(100)]
		public string FinancialCheckedBy { get; set; }
		[StringLength(50)]
		public string FinancialCreditCheckedDate { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal InterestAdjustment { get; set; }
		[Required]
		[StringLength(50)]
		public string InterestTypeGUID { get; set; }
		[StringLength(50)]
		public string InvoiceAddressGUID { get; set; }
		[StringLength(50)]
		public string KYCGUID { get; set; }
		[StringLength(50)]
		public string MailingReceiptAddressGUID { get; set; }
		[StringLength(500)]
		public string MarketingComment { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal MaxPurchasePct { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal MaxRetentionAmount { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal MaxRetentionPct { get; set; }
		[StringLength(100)]
		public string NCBCheckedBy { get; set; }
		[StringLength(50)]
		public string NCBCheckedDate { get; set; }
		[StringLength(50)]
		public string PDCBankGUID { get; set; }
		[StringLength(50)]
		public string ProductSubTypeGUID { get; set; }
		public int ProductType { get; set; }
		public int PurchaseFeeCalculateBase { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal PurchaseFeePct { get; set; }
		[StringLength(50)]
		public string ReceiptAddressGUID { get; set; }
		[StringLength(50)]
		public string ReceiptContactPersonGUID { get; set; }
		[StringLength(50)]
		public string RefCreditAppTableGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string RegisteredAddressGUID { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		[Required]
		[StringLength(50)]
		public string RequestDate { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SalesAvgPerMonth { get; set; }
		[StringLength(100)]
		public string SigningCondition { get; set; }
		[StringLength(50)]
		public string StartDate { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CustomerAllBuyerOutstanding { get; set; }
		public int ProcessInstanceId { get; set; }
		[StringLength(50)]
		public string ApprovedDate { get; set; }
		[Key]
		[Column(Order = 3)]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		[StringLength(50)]
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		[StringLength(50)]
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		public string OwnerBusinessUnitGUID { get; set; }
		public int MigrateStatus { get; set; }
		public int MigrateSource { get; set; }
	}
}
