using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CAReqAssignmentOutstanding : CompanyBaseEntity
	{
		public CAReqAssignmentOutstanding()
		{
		}
 
		[Key]
		public Guid CAReqAssignmentOutstandingGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal AssignmentAgreementAmount { get; set; }
		public Guid? AssignmentAgreementTableGUID { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public Guid? CreditAppRequestTableGUID { get; set; }
		public Guid? CustomerTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal RemainingAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleAmount { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CAReqAssignmentOutstandingCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CAReqAssignmentOutstandingBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("AssignmentAgreementTableGUID")]
		[InverseProperty("CAReqAssignmentOutstandingAssignmentAgreementTable")]
		public AssignmentAgreementTable AssignmentAgreementTable { get; set; }
		[ForeignKey("BuyerTableGUID")]
		[InverseProperty("CAReqAssignmentOutstandingBuyerTable")]
		public BuyerTable BuyerTable { get; set; }
		[ForeignKey("CreditAppRequestTableGUID")]
		[InverseProperty("CAReqAssignmentOutstandingCreditAppRequestTable")]
		public CreditAppRequestTable CreditAppRequestTable { get; set; }
		[ForeignKey("CustomerTableGUID")]
		[InverseProperty("CAReqAssignmentOutstandingCustomerTable")]
		public CustomerTable CustomerTable { get; set; }
		#endregion ForeignKey

		#region Collection
		#endregion Collection
	}
}
