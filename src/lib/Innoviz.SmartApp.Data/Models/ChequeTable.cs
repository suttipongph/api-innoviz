using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class ChequeTable : CompanyBaseEntity
	{
		public ChequeTable()
		{
			ChequeTableRefPDC = new HashSet<ChequeTable>();
			JobChequeChequeTable = new HashSet<JobCheque>();
			PurchaseLineBuyerPDCTable = new HashSet<PurchaseLine>();
			PurchaseLineCustomerPDCTable = new HashSet<PurchaseLine>();
			ReceiptTempPaymDetailChequeTable = new HashSet<ReceiptTempPaymDetail>();
			WithdrawalLineBuyerPDCTable = new HashSet<WithdrawalLine>();
			WithdrawalLineCustomerPDCTable = new HashSet<WithdrawalLine>();
		}
 
		[Key]
		public Guid ChequeTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Amount { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		[StringLength(20)]
		public string ChequeBankAccNo { get; set; }
		public Guid ChequeBankGroupGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string ChequeBranch { get; set; }
		[Column(TypeName = "date")]
		public DateTime ChequeDate { get; set; }
		[Required]
		[StringLength(20)]
		public string ChequeNo { get; set; }
		[Column(TypeName = "date")]
		public DateTime? CompletedDate { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime? ExpectedDepositDate { get; set; }
		[StringLength(100)]
		public string IssuedName { get; set; }
		public bool PDC { get; set; }
		public int ReceivedFrom { get; set; }
		[StringLength(100)]
		public string RecipientName { get; set; }
		public Guid? RefGUID { get; set; }
		public Guid? RefPDCGUID { get; set; }
		public int RefType { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("ChequeTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("ChequeTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BuyerTableGUID")]
		[InverseProperty("ChequeTableBuyerTable")]
		public BuyerTable BuyerTable { get; set; }
		[ForeignKey("ChequeBankGroupGUID")]
		[InverseProperty("ChequeTableChequeBankGroup")]
		public BankGroup ChequeBankGroup { get; set; }
		[ForeignKey("CustomerTableGUID")]
		[InverseProperty("ChequeTableCustomerTable")]
		public CustomerTable CustomerTable { get; set; }
		[ForeignKey("DocumentStatusGUID")]
		[InverseProperty("ChequeTableDocumentStatus")]
		public DocumentStatus DocumentStatus { get; set; }
		[ForeignKey("RefPDCGUID")]
		[InverseProperty("ChequeTableRefPDC")]
		public ChequeTable RefPDC { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("BuyerPDCTable")]
		public ICollection<PurchaseLine> PurchaseLineBuyerPDCTable { get; set; }
		[InverseProperty("BuyerPDCTable")]
		public ICollection<WithdrawalLine> WithdrawalLineBuyerPDCTable { get; set; }
		[InverseProperty("ChequeTable")]
		public ICollection<JobCheque> JobChequeChequeTable { get; set; }
		[InverseProperty("ChequeTable")]
		public ICollection<ReceiptTempPaymDetail> ReceiptTempPaymDetailChequeTable { get; set; }
		[InverseProperty("CustomerPDCTable")]
		public ICollection<PurchaseLine> PurchaseLineCustomerPDCTable { get; set; }
		[InverseProperty("CustomerPDCTable")]
		public ICollection<WithdrawalLine> WithdrawalLineCustomerPDCTable { get; set; }
		[InverseProperty("RefPDC")]
		public ICollection<ChequeTable> ChequeTableRefPDC { get; set; }
		#endregion Collection
	}
}
