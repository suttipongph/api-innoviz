using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class AddressCountry : CompanyBaseEntity
	{
		public AddressCountry()
		{
			AddressProvinceAddressCountry = new HashSet<AddressProvince>();
			AddressTransAddressCountry = new HashSet<AddressTrans>();
		}
 
		[Key]
		public Guid AddressCountryGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string CountryId { get; set; }
		[Required]
		[StringLength(100)]
		public string Name { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("AddressCountryCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("AddressCountryBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("AddressCountry")]
		public ICollection<AddressProvince> AddressProvinceAddressCountry { get; set; }
		[InverseProperty("AddressCountry")]
		public ICollection<AddressTrans> AddressTransAddressCountry { get; set; }
		#endregion Collection
	}
}
