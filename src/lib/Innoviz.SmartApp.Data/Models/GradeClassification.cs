using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class GradeClassification : CompanyBaseEntity
	{
		public GradeClassification()
		{
			CustomerTableGradeClassification = new HashSet<CustomerTable>();
		}
 
		[Key]
		public Guid GradeClassificationGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string GradeClassificationId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("GradeClassificationCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("GradeClassificationBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("GradeClassification")]
		public ICollection<CustomerTable> CustomerTableGradeClassification { get; set; }
		#endregion Collection
	}
}
