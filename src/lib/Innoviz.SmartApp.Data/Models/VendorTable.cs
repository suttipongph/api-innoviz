using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class VendorTable : CompanyBaseEntity
	{
		public VendorTable()
		{
			CustomerTableVendorTable = new HashSet<CustomerTable>();
			IntroducedByVendorTable = new HashSet<IntroducedBy>();
			MessengerTableVendorTable = new HashSet<MessengerTable>();
			PaymentDetailVendorTable = new HashSet<PaymentDetail>();
			VendBankVendorTable = new HashSet<VendBank>();
			VendorPaymentTransVendorTable = new HashSet<VendorPaymentTrans>();
			VerificationLineVendorTable = new HashSet<VerificationLine>();
		}
 
		[Key]
		public Guid VendorTableGUID { get; set; }
		[StringLength(100)]
		public string AltName { get; set; }
		public Guid CurrencyGUID { get; set; }
		[StringLength(20)]
		public string ExternalCode { get; set; }
		[Required]
		[StringLength(100)]
		public string Name { get; set; }
		public int RecordType { get; set; }
		public bool SentToOtherSystem { get; set; }
		[StringLength(13)]
		public string TaxId { get; set; }
		public Guid VendGroupGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string VendorId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("VendorTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("VendorTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("CurrencyGUID")]
		[InverseProperty("VendorTableCurrency")]
		public Currency Currency { get; set; }
		[ForeignKey("VendGroupGUID")]
		[InverseProperty("VendorTableVendGroup")]
		public VendGroup VendGroup { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("VendorTable")]
		public ICollection<CustomerTable> CustomerTableVendorTable { get; set; }
		[InverseProperty("VendorTable")]
		public ICollection<IntroducedBy> IntroducedByVendorTable { get; set; }
		[InverseProperty("VendorTable")]
		public ICollection<MessengerTable> MessengerTableVendorTable { get; set; }
		[InverseProperty("VendorTable")]
		public ICollection<PaymentDetail> PaymentDetailVendorTable { get; set; }
		[InverseProperty("VendorTable")]
		public ICollection<VendBank> VendBankVendorTable { get; set; }
		[InverseProperty("VendorTable")]
		public ICollection<VendorPaymentTrans> VendorPaymentTransVendorTable { get; set; }
		[InverseProperty("VendorTable")]
		public ICollection<VerificationLine> VerificationLineVendorTable { get; set; }
		#endregion Collection
	}
}
