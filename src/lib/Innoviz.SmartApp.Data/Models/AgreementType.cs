using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class AgreementType : CompanyBaseEntity
	{
		public AgreementType()
		{
			ApplicationTableAgreementType = new HashSet<ApplicationTable>();
		}
 
		[Key]
		public Guid AgreementTypeGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string AgreementTypeId { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("AgreementTypeCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("AgreementTypeBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("AgreementType")]
		public ICollection<ApplicationTable> ApplicationTableAgreementType { get; set; }
		#endregion Collection
	}
}
