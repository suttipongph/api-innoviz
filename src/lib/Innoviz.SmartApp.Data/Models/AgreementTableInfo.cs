using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class AgreementTableInfo : CompanyBaseEntity
	{
		public AgreementTableInfo()
		{
		}
 
		[Key]
		public Guid AgreementTableInfoGUID { get; set; }
		public Guid? AuthorizedPersonTransBuyer1GUID { get; set; }
		public Guid? AuthorizedPersonTransBuyer2GUID { get; set; }
		public Guid? AuthorizedPersonTransBuyer3GUID { get; set; }
		public Guid? AuthorizedPersonTransCompany1GUID { get; set; }
		public Guid? AuthorizedPersonTransCompany2GUID { get; set; }
		public Guid? AuthorizedPersonTransCompany3GUID { get; set; }
		public Guid? AuthorizedPersonTransCustomer1GUID { get; set; }
		public Guid? AuthorizedPersonTransCustomer2GUID { get; set; }
		public Guid? AuthorizedPersonTransCustomer3GUID { get; set; }
		public Guid? AuthorizedPersonTypeBuyerGUID { get; set; }
		public Guid? AuthorizedPersonTypeCompanyGUID { get; set; }
		public Guid? AuthorizedPersonTypeCustomerGUID { get; set; }
		[StringLength(250)]
		public string BuyerAddress1 { get; set; }
		[StringLength(250)]
		public string BuyerAddress2 { get; set; }
		[StringLength(250)]
		public string CompanyAddress1 { get; set; }
		[StringLength(250)]
		public string CompanyAddress2 { get; set; }
		[StringLength(100)]
		public string CompanyAltName { get; set; }
		public Guid? CompanyBankGUID { get; set; }
		[StringLength(100)]
		public string CompanyFax { get; set; }
		[StringLength(100)]
		public string CompanyName { get; set; }
		[StringLength(100)]
		public string CompanyPhone { get; set; }
		[StringLength(250)]
		public string CustomerAddress1 { get; set; }
		[StringLength(250)]
		public string CustomerAddress2 { get; set; }
		[StringLength(1000)]
		public string GuaranteeText { get; set; }
		[StringLength(500)]
		public string GuarantorName { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		public Guid? WitnessCompany1GUID { get; set; }
		public Guid? WitnessCompany2GUID { get; set; }
		[StringLength(100)]
		public string WitnessCustomer1 { get; set; }
		[StringLength(100)]
		public string WitnessCustomer2 { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("AgreementTableInfoCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("AgreementTableInfoBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("AuthorizedPersonTransBuyer1GUID")]
		[InverseProperty("AgreementTableInfoAuthorizedPersonTransBuyer1")]
		public AuthorizedPersonTrans AuthorizedPersonTransBuyer1 { get; set; }
		[ForeignKey("AuthorizedPersonTransBuyer2GUID")]
		[InverseProperty("AgreementTableInfoAuthorizedPersonTransBuyer2")]
		public AuthorizedPersonTrans AuthorizedPersonTransBuyer2 { get; set; }
		[ForeignKey("AuthorizedPersonTransBuyer3GUID")]
		[InverseProperty("AgreementTableInfoAuthorizedPersonTransBuyer3")]
		public AuthorizedPersonTrans AuthorizedPersonTransBuyer3 { get; set; }
		[ForeignKey("AuthorizedPersonTransCompany1GUID")]
		[InverseProperty("AgreementTableInfoAuthorizedPersonTransCompany1")]
		public EmployeeTable AuthorizedPersonTransCompany1 { get; set; }
		[ForeignKey("AuthorizedPersonTransCompany2GUID")]
		[InverseProperty("AgreementTableInfoAuthorizedPersonTransCompany2")]
		public EmployeeTable AuthorizedPersonTransCompany2 { get; set; }
		[ForeignKey("AuthorizedPersonTransCompany3GUID")]
		[InverseProperty("AgreementTableInfoAuthorizedPersonTransCompany3")]
		public EmployeeTable AuthorizedPersonTransCompany3 { get; set; }
		[ForeignKey("AuthorizedPersonTransCustomer1GUID")]
		[InverseProperty("AgreementTableInfoAuthorizedPersonTransCustomer1")]
		public AuthorizedPersonTrans AuthorizedPersonTransCustomer1 { get; set; }
		[ForeignKey("AuthorizedPersonTransCustomer2GUID")]
		[InverseProperty("AgreementTableInfoAuthorizedPersonTransCustomer2")]
		public AuthorizedPersonTrans AuthorizedPersonTransCustomer2 { get; set; }
		[ForeignKey("AuthorizedPersonTransCustomer3GUID")]
		[InverseProperty("AgreementTableInfoAuthorizedPersonTransCustomer3")]
		public AuthorizedPersonTrans AuthorizedPersonTransCustomer3 { get; set; }
		[ForeignKey("AuthorizedPersonTypeBuyerGUID")]
		[InverseProperty("AgreementTableInfoAuthorizedPersonTypeBuyer")]
		public AuthorizedPersonType AuthorizedPersonTypeBuyer { get; set; }
		[ForeignKey("AuthorizedPersonTypeCompanyGUID")]
		[InverseProperty("AgreementTableInfoAuthorizedPersonTypeCompany")]
		public AuthorizedPersonType AuthorizedPersonTypeCompany { get; set; }
		[ForeignKey("AuthorizedPersonTypeCustomerGUID")]
		[InverseProperty("AgreementTableInfoAuthorizedPersonTypeCustomer")]
		public AuthorizedPersonType AuthorizedPersonTypeCustomer { get; set; }
		[ForeignKey("CompanyBankGUID")]
		[InverseProperty("AgreementTableInfoCompanyBank")]
		public CompanyBank CompanyBank { get; set; }
		[ForeignKey("WitnessCompany1GUID")]
		[InverseProperty("AgreementTableInfoWitnessCompany1")]
		public EmployeeTable WitnessCompany1 { get; set; }
		[ForeignKey("WitnessCompany2GUID")]
		[InverseProperty("AgreementTableInfoWitnessCompany2")]
		public EmployeeTable WitnessCompany2 { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
