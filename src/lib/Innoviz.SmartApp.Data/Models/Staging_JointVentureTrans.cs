﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_JointVentureTrans
	{
		[Key]
		[Column(Order = 1)]
		[StringLength(50)]
		public string JointVentureTransGUID { get; set; }
		[StringLength(500)]
		public string Address { get; set; }
		[StringLength(50)]
		public string AuthorizedPersonTypeGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Name { get; set; }
		[StringLength(100)]
		public string OperatedBy { get; set; }
		[Key]
		[Column(Order = 3)]
		public int Ordering { get; set; }
		[Key]
		[Column(Order = 2)]
		[StringLength(50)]
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		[Key]
		[Column(Order = 4)]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		[StringLength(50)]
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		[StringLength(50)]
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		public string OwnerBusinessUnitGUID { get; set; }
		public int MigrateStatus { get; set; }
		public int MigrateSource { get; set; }
	}
}
