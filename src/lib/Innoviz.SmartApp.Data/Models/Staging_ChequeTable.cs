﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_ChequeTable
	{
		[Key]
		[StringLength(50)]
		public string ChequeTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Amount { get; set; }
		[StringLength(50)]
		public string BuyerTableGUID { get; set; }
		[StringLength(20)]
		public string ChequeBankAccNo { get; set; }
		[Required]
		[StringLength(50)]
		public string ChequeBankGroupGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string ChequeBranch { get; set; }
		[Required]
		public string ChequeDate { get; set; }
		[Required]
		[StringLength(20)]
		public string ChequeNo { get; set; }
		public string CompletedDate { get; set; }
		[Required]
		[StringLength(50)]
		public string CustomerTableGUID { get; set; }
		public string DocumentStatusGUID { get; set; }
		public string ExpectedDepositDate { get; set; }
		[StringLength(100)]
		public string IssuedName { get; set; }
		public bool PDC { get; set; }
		public int ReceivedFrom { get; set; }
		[StringLength(100)]
		public string RecipientName { get; set; }
		[StringLength(50)]
		public string RefGUID { get; set; }
		[StringLength(50)]
		public string RefPDCGUID { get; set; }
		public int RefType { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }


		[Required]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		[StringLength(50)]
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		[StringLength(50)]
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		public string OwnerBusinessUnitGUID { get; set; }
		public int MigrateStatus { get; set; }
		public int MigrateSource { get; set; }
	}
}
