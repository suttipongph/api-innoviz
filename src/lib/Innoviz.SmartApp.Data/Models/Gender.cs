using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Gender : CompanyBaseEntity
	{
		public Gender()
		{
			CustomerTableGender = new HashSet<CustomerTable>();
			RelatedPersonTableGender = new HashSet<RelatedPersonTable>();
		}
 
		[Key]
		public Guid GenderGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string GenderId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("GenderCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("GenderBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("Gender")]
		public ICollection<CustomerTable> CustomerTableGender { get; set; }
		[InverseProperty("Gender")]
		public ICollection<RelatedPersonTable> RelatedPersonTableGender { get; set; }
		#endregion Collection
	}
}
