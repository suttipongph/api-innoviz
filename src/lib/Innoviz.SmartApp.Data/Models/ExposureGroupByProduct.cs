using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class ExposureGroupByProduct : CompanyBaseEntity
	{
		public ExposureGroupByProduct()
		{
		}
 
		[Key]
		public Guid ExposureGroupByProductGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ExposureAmount { get; set; }
		public Guid ExposureGroupGUID { get; set; }
		public int ProductType { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("ExposureGroupByProductCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("ExposureGroupByProductBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
