using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class BusinessCollateralAgmTable : CompanyBaseEntity
	{
		public BusinessCollateralAgmTable()
		{
			BusinessCollateralAgmLineBusinessCollateralAgmTable = new HashSet<BusinessCollateralAgmLine>();
			BusinessCollateralAgmTableRefBusinessCollateralAgmTable = new HashSet<BusinessCollateralAgmTable>();
		}
 
		[Key]
		public Guid BusinessCollateralAgmTableGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime AgreementDate { get; set; }
		public int AgreementDocType { get; set; }
		public int AgreementExtension { get; set; }
		[StringLength(20)]
		public string BusinessCollateralAgmId { get; set; }
		public Guid? ConsortiumTableGUID { get; set; }
		public Guid CreditAppRequestTableGUID { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public Guid? CreditLimitTypeGUID { get; set; }
		[StringLength(100)]
		public string CustomerAltName { get; set; }
		[Required]
		[StringLength(100)]
		public string CustomerName { get; set; }
		public Guid CustomerTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public Guid? DocumentReasonGUID { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string InternalBusinessCollateralAgmId { get; set; }
		public int ProductType { get; set; }
		public Guid? RefBusinessCollateralAgmTableGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime? SigningDate { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal DBDRegistrationAmount { get; set; }
		[Column(TypeName = "date")]
		public DateTime? DBDRegistrationDate { get; set; }
		[StringLength(100)]
		public string DBDRegistrationDescription { get; set; }
		[StringLength(20)]
		public string DBDRegistrationId { get; set; }
		[StringLength(1000)]
		public string AttachmentRemark { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("BusinessCollateralAgmTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("BusinessCollateralAgmTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("ConsortiumTableGUID")]
		[InverseProperty("BusinessCollateralAgmTableConsortiumTable")]
		public ConsortiumTable ConsortiumTable { get; set; }
		[ForeignKey("CreditAppRequestTableGUID")]
		[InverseProperty("BusinessCollateralAgmTableCreditAppRequestTable")]
		public CreditAppRequestTable CreditAppRequestTable { get; set; }
		[ForeignKey("CreditAppTableGUID")]
		[InverseProperty("BusinessCollateralAgmTableCreditAppTable")]
		public CreditAppTable CreditAppTable { get; set; }
		[ForeignKey("CreditLimitTypeGUID")]
		[InverseProperty("BusinessCollateralAgmTableCreditLimitType")]
		public CreditLimitType CreditLimitType { get; set; }
		[ForeignKey("DocumentReasonGUID")]
		[InverseProperty("BusinessCollateralAgmTableDocumentReason")]
		public DocumentReason DocumentReason { get; set; }
		[ForeignKey("DocumentStatusGUID")]
		[InverseProperty("BusinessCollateralAgmTableDocumentStatus")]
		public DocumentStatus DocumentStatus { get; set; }
		[ForeignKey("RefBusinessCollateralAgmTableGUID")]
		[InverseProperty("BusinessCollateralAgmTableRefBusinessCollateralAgmTable")]
		public BusinessCollateralAgmTable RefBusinessCollateralAgmTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("BusinessCollateralAgmTable")]
		public ICollection<BusinessCollateralAgmLine> BusinessCollateralAgmLineBusinessCollateralAgmTable { get; set; }
		[InverseProperty("RefBusinessCollateralAgmTable")]
		public ICollection<BusinessCollateralAgmTable> BusinessCollateralAgmTableRefBusinessCollateralAgmTable { get; set; }
		#endregion Collection
	}
}
