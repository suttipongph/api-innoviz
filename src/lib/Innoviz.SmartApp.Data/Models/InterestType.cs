using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class InterestType : CompanyBaseEntity
	{
		public InterestType()
		{
			CreditAppRequestTableInterestType = new HashSet<CreditAppRequestTable>();
			CreditAppTableInterestType = new HashSet<CreditAppTable>();
			InterestTypeValueInterestType = new HashSet<InterestTypeValue>();
		}
 
		[Key]
		public Guid InterestTypeGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string InterestTypeId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("InterestTypeCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("InterestTypeBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("InterestType")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTableInterestType { get; set; }
		[InverseProperty("InterestType")]
		public ICollection<CreditAppTable> CreditAppTableInterestType { get; set; }
		[InverseProperty("InterestType")]
		public ICollection<InterestTypeValue> InterestTypeValueInterestType { get; set; }
		#endregion Collection
	}
}
