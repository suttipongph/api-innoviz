﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
    public partial class SysNavigation : BaseEntity {
       
        [StringLength(20)]
        public string Id { get; set; }
        public bool Expanded { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(10)]
        public string ParentId { get; set; }
        [StringLength(100)]
        public string Route { get; set; }
        [StringLength(100)]
        public string Icon { get; set; }
    }
}
