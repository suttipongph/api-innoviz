using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CreditAppRequestLine : CompanyBaseEntity
	{
		public CreditAppRequestLine()
		{
			CreditAppLineRefCreditAppRequestLine = new HashSet<CreditAppLine>();
		}
 
		[Key]
		public Guid CreditAppRequestLineGUID { get; set; }
		public bool AcceptanceDocument { get; set; }
		[StringLength(100)]
		public string AcceptanceDocumentDescription { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal AllCustomerBuyerOutstanding { get; set; }
		public int ApprovalDecision { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ApprovedCreditLimitLineRequest { get; set; }
		[StringLength(500)]
		public string ApproverComment { get; set; }
		public Guid? AssignmentAgreementTableGUID { get; set; }
		public Guid? AssignmentMethodGUID { get; set; }
		[StringLength(250)]
		public string AssignmentMethodRemark { get; set; }
		public Guid? BillingAddressGUID { get; set; }
		public Guid? BillingContactPersonGUID { get; set; }
		public int BillingDay { get; set; }
		[StringLength(100)]
		public string BillingDescription { get; set; }
		[StringLength(250)]
		public string BillingRemark { get; set; }
		public Guid? BillingResponsibleByGUID { get; set; }
		public Guid? BlacklistStatusGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal BuyerCreditLimit { get; set; }
		public Guid? BuyerCreditTermGUID { get; set; }
		[StringLength(100)]
		public string BuyerProduct { get; set; }
		public Guid BuyerTableGUID { get; set; }
		public Guid CreditAppRequestTableGUID { get; set; }
		[StringLength(500)]
		public string CreditComment { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CreditLimitLineRequest { get; set; }
		public Guid? CreditScoringGUID { get; set; }
		[StringLength(100)]
		public string CreditTermDescription { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CustomerBuyerOutstanding { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CustomerContactBuyerPeriod { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CustomerRequest { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InsuranceCreditLimit { get; set; }
		public Guid? InvoiceAddressGUID { get; set; }
		public Guid? KYCSetupGUID { get; set; }
		[StringLength(250)]
		public string LineCondition { get; set; }
		public int LineNum { get; set; }
		public Guid? MailingReceiptAddressGUID { get; set; }
		[StringLength(500)]
		public string MarketingComment { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal MaxPurchasePct { get; set; }
		public int MethodOfBilling { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
		[StringLength(250)]
		public string PaymentCondition { get; set; }
		public int PurchaseFeeCalculateBase { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal PurchaseFeePct { get; set; }
		public Guid? ReceiptAddressGUID { get; set; }
		public Guid? ReceiptContactPersonGUID { get; set; }
		public int ReceiptDay { get; set; }
		[StringLength(100)]
		public string ReceiptDescription { get; set; }
		[StringLength(250)]
		public string ReceiptRemark { get; set; }
		public Guid? RefCreditAppLineGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal RemainingCreditLoanRequest { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CreditAppRequestLineCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CreditAppRequestLineBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("AssignmentAgreementTableGUID")]
		[InverseProperty("CreditAppRequestLineAssignmentAgreementTable")]
		public AssignmentAgreementTable AssignmentAgreementTable { get; set; }
		[ForeignKey("AssignmentMethodGUID")]
		[InverseProperty("CreditAppRequestLineAssignmentMethod")]
		public AssignmentMethod AssignmentMethod { get; set; }
		[ForeignKey("BillingAddressGUID")]
		[InverseProperty("CreditAppRequestLineBillingAddress")]
		public AddressTrans BillingAddress { get; set; }
		[ForeignKey("BillingContactPersonGUID")]
		[InverseProperty("CreditAppRequestLineBillingContactPerson")]
		public ContactPersonTrans BillingContactPerson { get; set; }
		[ForeignKey("BillingResponsibleByGUID")]
		[InverseProperty("CreditAppRequestLineBillingResponsibleBy")]
		public BillingResponsibleBy BillingResponsibleBy { get; set; }
		[ForeignKey("BlacklistStatusGUID")]
		[InverseProperty("CreditAppRequestLineBlacklistStatus")]
		public BlacklistStatus BlacklistStatus { get; set; }
		[ForeignKey("BuyerCreditTermGUID")]
		[InverseProperty("CreditAppRequestLineBuyerCreditTerm")]
		public CreditTerm BuyerCreditTerm { get; set; }
		[ForeignKey("BuyerTableGUID")]
		[InverseProperty("CreditAppRequestLineBuyerTable")]
		public BuyerTable BuyerTable { get; set; }
		[ForeignKey("CreditAppRequestTableGUID")]
		[InverseProperty("CreditAppRequestLineCreditAppRequestTable")]
		public CreditAppRequestTable CreditAppRequestTable { get; set; }
		[ForeignKey("CreditScoringGUID")]
		[InverseProperty("CreditAppRequestLineCreditScoring")]
		public CreditScoring CreditScoring { get; set; }
		[ForeignKey("InvoiceAddressGUID")]
		[InverseProperty("CreditAppRequestLineInvoiceAddress")]
		public AddressTrans InvoiceAddress { get; set; }
		[ForeignKey("KYCSetupGUID")]
		[InverseProperty("CreditAppRequestLineKYCSetup")]
		public KYCSetup KYCSetup { get; set; }
		[ForeignKey("MailingReceiptAddressGUID")]
		[InverseProperty("CreditAppRequestLineMailingReceiptAddress")]
		public AddressTrans MailingReceiptAddress { get; set; }
		[ForeignKey("MethodOfPaymentGUID")]
		[InverseProperty("CreditAppRequestLineMethodOfPayment")]
		public MethodOfPayment MethodOfPayment { get; set; }
		[ForeignKey("ReceiptAddressGUID")]
		[InverseProperty("CreditAppRequestLineReceiptAddress")]
		public AddressTrans ReceiptAddress { get; set; }
		[ForeignKey("ReceiptContactPersonGUID")]
		[InverseProperty("CreditAppRequestLineReceiptContactPerson")]
		public ContactPersonTrans ReceiptContactPerson { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("RefCreditAppRequestLine")]
		public ICollection<CreditAppLine> CreditAppLineRefCreditAppRequestLine { get; set; }
		#endregion Collection
	}
}
