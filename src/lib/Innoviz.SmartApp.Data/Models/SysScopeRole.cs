﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
    public partial class SysScopeRole : BaseEntity
    {
        
        public Guid RoleGUID { get; set; }
        [StringLength(350)]
        public string ScopeName { get; set; }
        [StringLength(50)]
        public string ClientId { get; set; }
        
    }
}