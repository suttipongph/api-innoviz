using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class ConsortiumLine : CompanyBaseEntity
	{
		public ConsortiumLine()
		{
			ConsortiumTransConsortiumLine = new HashSet<ConsortiumTrans>();
		}
 
		[Key]
		public Guid ConsortiumLineGUID { get; set; }
		[StringLength(500)]
		public string Address { get; set; }
		public Guid? AuthorizedPersonTypeGUID { get; set; }
		public Guid ConsortiumTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string CustomerName { get; set; }
		public bool IsMain { get; set; }
		[StringLength(100)]
		public string OperatedBy { get; set; }
		public int Ordering { get; set; }
		[StringLength(100)]
		public string Position { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal ProportionOfShareholderPct { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("ConsortiumLineCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("ConsortiumLineBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("AuthorizedPersonTypeGUID")]
		[InverseProperty("ConsortiumLineAuthorizedPersonType")]
		public AuthorizedPersonType AuthorizedPersonType { get; set; }
		[ForeignKey("ConsortiumTableGUID")]
		[InverseProperty("ConsortiumLineConsortiumTable")]
		public ConsortiumTable ConsortiumTable { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("ConsortiumLine")]
		public ICollection<ConsortiumTrans> ConsortiumTransConsortiumLine { get; set; }
		#endregion Collection
	}
}
