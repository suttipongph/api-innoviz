using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class LineOfBusiness : CompanyBaseEntity
	{
		public LineOfBusiness()
		{
			BuyerTableLineOfBusiness = new HashSet<BuyerTable>();
			CustomerTableLineOfBusiness = new HashSet<CustomerTable>();
		}
 
		[Key]
		public Guid LineOfBusinessGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string LineOfBusinessId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("LineOfBusinessCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("LineOfBusinessBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("LineOfBusiness")]
		public ICollection<BuyerTable> BuyerTableLineOfBusiness { get; set; }
		[InverseProperty("LineOfBusiness")]
		public ICollection<CustomerTable> CustomerTableLineOfBusiness { get; set; }
		#endregion Collection
	}
}
