using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Race : CompanyBaseEntity
	{
		public Race()
		{
			CustomerTableRace = new HashSet<CustomerTable>();
			GuarantorAgreementLineRace = new HashSet<GuarantorAgreementLine>();
			RelatedPersonTableRace = new HashSet<RelatedPersonTable>();
		}
 
		[Key]
		public Guid RaceGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string RaceId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("RaceCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("RaceBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("Race")]
		public ICollection<CustomerTable> CustomerTableRace { get; set; }
		[InverseProperty("Race")]
		public ICollection<GuarantorAgreementLine> GuarantorAgreementLineRace { get; set; }
		[InverseProperty("Race")]
		public ICollection<RelatedPersonTable> RelatedPersonTableRace { get; set; }
		#endregion Collection
	}
}
