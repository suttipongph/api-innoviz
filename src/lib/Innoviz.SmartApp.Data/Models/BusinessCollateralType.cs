using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class BusinessCollateralType : CompanyBaseEntity
	{
		public BusinessCollateralType()
		{
			BusinessCollateralAgmLineBusinessCollateralType = new HashSet<BusinessCollateralAgmLine>();
			BusinessCollateralSubTypeBusinessCollateralType = new HashSet<BusinessCollateralSubType>();
			CreditAppReqBusinessCollateralBusinessCollateralType = new HashSet<CreditAppReqBusinessCollateral>();
			CustBusinessCollateralBusinessCollateralType = new HashSet<CustBusinessCollateral>();
		}
 
		[Key]
		public Guid BusinessCollateralTypeGUID { get; set; }
		public int AgreementOrdering { get; set; }
		[Required]
		[StringLength(20)]
		public string BusinessCollateralTypeId { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("BusinessCollateralTypeCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("BusinessCollateralTypeBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("BusinessCollateralType")]
		public ICollection<BusinessCollateralAgmLine> BusinessCollateralAgmLineBusinessCollateralType { get; set; }
		[InverseProperty("BusinessCollateralType")]
		public ICollection<BusinessCollateralSubType> BusinessCollateralSubTypeBusinessCollateralType { get; set; }
		[InverseProperty("BusinessCollateralType")]
		public ICollection<CreditAppReqBusinessCollateral> CreditAppReqBusinessCollateralBusinessCollateralType { get; set; }
		[InverseProperty("BusinessCollateralType")]
		public ICollection<CustBusinessCollateral> CustBusinessCollateralBusinessCollateralType { get; set; }
		#endregion Collection
	}
}
