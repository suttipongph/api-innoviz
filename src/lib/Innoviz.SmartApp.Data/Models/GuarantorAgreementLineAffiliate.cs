using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class GuarantorAgreementLineAffiliate : CompanyBaseEntity
	{
		public GuarantorAgreementLineAffiliate()
		{
		}
 
		[Key]
		public Guid GuarantorAgreementLineAffiliateGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid GuarantorAgreementLineGUID { get; set; }
		public Guid MainAgreementTableGUID { get; set; }
		public int Ordering { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("GuarantorAgreementLineAffiliateCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("GuarantorAgreementLineAffiliateBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("GuarantorAgreementLineGUID")]
		[InverseProperty("GuarantorAgreementLineAffiliateGuarantorAgreementLine")]
		public GuarantorAgreementLine GuarantorAgreementLine { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
