using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class BookmarkDocument : CompanyBaseEntity
	{
		public BookmarkDocument()
		{
			BookmarkDocumentTemplateLineBookmarkDocument = new HashSet<BookmarkDocumentTemplateLine>();
			BookmarkDocumentTransBookmarkDocument = new HashSet<BookmarkDocumentTrans>();
		}
 
		[Key]
		public Guid BookmarkDocumentGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string BookmarkDocumentId { get; set; }
		public int BookmarkDocumentRefType { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public int DocumentTemplateType { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("BookmarkDocumentCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("BookmarkDocumentBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("BookmarkDocument")]
		public ICollection<BookmarkDocumentTemplateLine> BookmarkDocumentTemplateLineBookmarkDocument { get; set; }
		[InverseProperty("BookmarkDocument")]
		public ICollection<BookmarkDocumentTrans> BookmarkDocumentTransBookmarkDocument { get; set; }
		#endregion Collection
	}
}
