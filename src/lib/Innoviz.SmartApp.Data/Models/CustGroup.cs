using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CustGroup : CompanyBaseEntity
	{
		public CustGroup()
		{
			CustomerTableCustGroup = new HashSet<CustomerTable>();
		}
 
		[Key]
		public Guid CustGroupGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string CustGroupId { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public Guid NumberSeqTableGUID { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CustGroupCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CustGroupBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("NumberSeqTableGUID")]
		[InverseProperty("CustGroupNumberSeqTable")]
		public NumberSeqTable NumberSeqTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("CustGroup")]
		public ICollection<CustomerTable> CustomerTableCustGroup { get; set; }
		#endregion Collection
	}
}
