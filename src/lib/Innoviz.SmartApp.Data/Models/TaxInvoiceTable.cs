using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class TaxInvoiceTable : BranchCompanyBaseEntity
	{
		public TaxInvoiceTable()
		{
			InvoiceTableRefTaxInvoice = new HashSet<InvoiceTable>();
			ProcessTransRefTaxInvoice = new HashSet<ProcessTrans>();
			TaxInvoiceLineTaxInvoiceTable = new HashSet<TaxInvoiceLine>();
			TaxInvoiceTableRefTaxInvoice = new HashSet<TaxInvoiceTable>();
		}
 
		[Key]
		public Guid TaxInvoiceTableGUID { get; set; }
		public Guid? CNReasonGUID { get; set; }
		public Guid CurrencyGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string CustomerName { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		[StringLength(20)]
		public string DocumentId { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime DueDate { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ExchangeRate { get; set; }
		[StringLength(250)]
		public string InvoiceAddress1 { get; set; }
		[StringLength(250)]
		public string InvoiceAddress2 { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InvoiceAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InvoiceAmountBeforeTax { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InvoiceAmountBeforeTaxMST { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InvoiceAmountMST { get; set; }
		public Guid InvoiceTableGUID { get; set; }
		public Guid InvoiceTypeGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime IssuedDate { get; set; }
		[StringLength(250)]
		public string MailingInvoiceAddress1 { get; set; }
		[StringLength(250)]
		public string MailingInvoiceAddress2 { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OrigTaxInvoiceAmount { get; set; }
		[StringLength(20)]
		public string OrigTaxInvoiceId { get; set; }
		public Guid? RefTaxInvoiceGUID { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TaxAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TaxAmountMST { get; set; }
		[StringLength(5)]
		public string TaxBranchId { get; set; }
		[StringLength(100)]
		public string TaxBranchName { get; set; }
		[Required]
		[StringLength(20)]
		public string TaxInvoiceId { get; set; }
		public Guid TaxInvoiceRefGUID { get; set; }
		public int TaxInvoiceRefType { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("TaxInvoiceTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("BranchGUID")]
		[InverseProperty("TaxInvoiceTableBranch")]
		public Branch Branch { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("TaxInvoiceTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("CNReasonGUID")]
		[InverseProperty("TaxInvoiceTableCNReason")]
		public DocumentReason CNReason { get; set; }
		[ForeignKey("CurrencyGUID")]
		[InverseProperty("TaxInvoiceTableCurrency")]
		public Currency Currency { get; set; }
		[ForeignKey("CustomerTableGUID")]
		[InverseProperty("TaxInvoiceTableCustomerTable")]
		public CustomerTable CustomerTable { get; set; }
		[ForeignKey("Dimension1GUID")]
		[InverseProperty("TaxInvoiceTableDimension1")]
		public LedgerDimension LedgerDimension1 { get; set; }
		[ForeignKey("Dimension2GUID")]
		[InverseProperty("TaxInvoiceTableDimension2")]
		public LedgerDimension LedgerDimension2 { get; set; }
		[ForeignKey("Dimension3GUID")]
		[InverseProperty("TaxInvoiceTableDimension3")]
		public LedgerDimension LedgerDimension3 { get; set; }
		[ForeignKey("Dimension4GUID")]
		[InverseProperty("TaxInvoiceTableDimension4")]
		public LedgerDimension LedgerDimension4 { get; set; }
		[ForeignKey("Dimension5GUID")]
		[InverseProperty("TaxInvoiceTableDimension5")]
		public LedgerDimension LedgerDimension5 { get; set; }
		[ForeignKey("DocumentStatusGUID")]
		[InverseProperty("TaxInvoiceTableDocumentStatus")]
		public DocumentStatus DocumentStatus { get; set; }
		[ForeignKey("InvoiceTableGUID")]
		[InverseProperty("TaxInvoiceTableInvoiceTable")]
		public InvoiceTable InvoiceTable { get; set; }
		[ForeignKey("InvoiceTypeGUID")]
		[InverseProperty("TaxInvoiceTableInvoiceType")]
		public InvoiceType InvoiceType { get; set; }
		[ForeignKey("MethodOfPaymentGUID")]
		[InverseProperty("TaxInvoiceTableMethodOfPayment")]
		public MethodOfPayment MethodOfPayment { get; set; }
		[ForeignKey("RefTaxInvoiceGUID")]
		[InverseProperty("TaxInvoiceTableRefTaxInvoice")]
		public TaxInvoiceTable RefTaxInvoice { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("RefTaxInvoice")]
		public ICollection<InvoiceTable> InvoiceTableRefTaxInvoice { get; set; }
		[InverseProperty("RefTaxInvoice")]
		public ICollection<ProcessTrans> ProcessTransRefTaxInvoice { get; set; }
		[InverseProperty("RefTaxInvoice")]
		public ICollection<TaxInvoiceTable> TaxInvoiceTableRefTaxInvoice { get; set; }
		[InverseProperty("TaxInvoiceTable")]
		public ICollection<TaxInvoiceLine> TaxInvoiceLineTaxInvoiceTable { get; set; }
		#endregion Collection
	}
}
