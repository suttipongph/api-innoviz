﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
    [Table("sysssislog")]
    public class MigrationLogTable : BaseEntity
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Required]
        [StringLength(128)]
        [Column("computer")]
        public string Computer { get; set; }

        [Required]
        [StringLength(128)]
        [Column("event")]
        public string EventName { get; set; }

        [Required]
        [StringLength(128)]
        [Column("operator")]
        public string OperatorName { get; set; }

        [Required]
        [StringLength(1024)]
        [Column("source")]
        public string Source { get; set; }

        [Required]
        [Column("sourceid")]
        public Guid SourceId { get; set; }

        [Required]
        [Column("executionid")]
        public Guid ExecutionId { get; set; }

        [Column("starttime")]
        public DateTime StartDateTime { get; set; }

        [Column("endtime")]
        public DateTime EndDateTime { get; set; }

        [Required]
        [Column("datacode")]
        public int DataCode { get; set; }

        [Required]
        [Column("databytes")]
        public byte[] DataBytes { get; set; }

        [Required]
        [StringLength(2048)]
        [Column("message")]
        public string Message { get; set; }

    }
}
