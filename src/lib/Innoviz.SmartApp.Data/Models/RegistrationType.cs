using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class RegistrationType : CompanyBaseEntity
	{
		public RegistrationType()
		{
			CustomerTableRegistrationType = new HashSet<CustomerTable>();
			RelatedPersonTableRegistrationType = new HashSet<RelatedPersonTable>();
		}
 
		[Key]
		public Guid RegistrationTypeGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string RegistrationTypeId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("RegistrationTypeCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("RegistrationTypeBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("RegistrationType")]
		public ICollection<CustomerTable> CustomerTableRegistrationType { get; set; }
		[InverseProperty("RegistrationType")]
		public ICollection<RelatedPersonTable> RelatedPersonTableRegistrationType { get; set; }
		#endregion Collection
	}
}
