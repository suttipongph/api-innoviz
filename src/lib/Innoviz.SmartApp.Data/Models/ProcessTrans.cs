using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class ProcessTrans : CompanyBaseEntity
	{
		public ProcessTrans()
		{
			ProcessTransRefProcessTrans = new HashSet<ProcessTrans>();
			StagingTableProcessTrans = new HashSet<StagingTable>();
			StagingTableVendorInfoProcessTrans = new HashSet<StagingTableVendorInfo>();
		}
 
		[Key]
		public Guid ProcessTransGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Amount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal AmountMST { get; set; }
		[StringLength(20)]
		public string ARLedgerAccount { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public Guid CurrencyGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		[StringLength(20)]
		public string DocumentId { get; set; }
		public Guid? DocumentReasonGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ExchangeRate { get; set; }
		public Guid? OrigTaxTableGUID { get; set; }
		public Guid? PaymentDetailGUID { get; set; }
		public Guid? PaymentHistoryGUID { get; set; }
		public int ProcessTransType { get; set; }
		public int ProductType { get; set; }
		public Guid? RefGUID { get; set; }
		public Guid? RefProcessTransGUID { get; set; }
		public Guid? RefTaxInvoiceGUID { get; set; }
		public int RefType { get; set; }
		public bool Revert { get; set; }
		[StringLength(20)]
		public string StagingBatchId { get; set; }
		public int StagingBatchStatus { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TaxAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TaxAmountMST { get; set; }
		public Guid? TaxTableGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime TransDate { get; set; }
		public Guid? InvoiceTableGUID { get; set; }
		public int SourceRefType { get; set; }
		[StringLength(100)]
		public string SourceRefId { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("ProcessTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("ProcessTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("CreditAppTableGUID")]
		[InverseProperty("ProcessTransCreditAppTable")]
		public CreditAppTable CreditAppTable { get; set; }
		[ForeignKey("CurrencyGUID")]
		[InverseProperty("ProcessTransCurrency")]
		public Currency Currency { get; set; }
		[ForeignKey("DocumentReasonGUID")]
		[InverseProperty("ProcessTransDocumentReason")]
		public DocumentReason DocumentReason { get; set; }
		[ForeignKey("OrigTaxTableGUID")]
		[InverseProperty("ProcessTransOrigTaxTable")]
		public TaxTable OrigTaxTable { get; set; }
		[ForeignKey("PaymentDetailGUID")]
		[InverseProperty("ProcessTransPaymentDetail")]
		public PaymentDetail PaymentDetail { get; set; }
		[ForeignKey("PaymentHistoryGUID")]
		[InverseProperty("ProcessTransPaymentHistory")]
		public PaymentHistory PaymentHistory { get; set; }
		[ForeignKey("RefProcessTransGUID")]
		[InverseProperty("ProcessTransRefProcessTrans")]
		public ProcessTrans RefProcessTrans { get; set; }
		[ForeignKey("RefTaxInvoiceGUID")]
		[InverseProperty("ProcessTransRefTaxInvoice")]
		public TaxInvoiceTable RefTaxInvoice { get; set; }
		[ForeignKey("TaxTableGUID")]
		[InverseProperty("ProcessTransTaxTable")]
		public TaxTable TaxTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("RefProcessTrans")]
		public ICollection<ProcessTrans> ProcessTransRefProcessTrans { get; set; }
		[InverseProperty("ProcessTrans")]
		public ICollection<StagingTable> StagingTableProcessTrans { get; set; }
		[InverseProperty("ProcessTrans")]
		public ICollection<StagingTableVendorInfo> StagingTableVendorInfoProcessTrans { get; set; }
		#endregion Collection
	}
}
