using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_BusinessCollateralAgmLine
	{

		[Key]
		[Column(Order = 1)]
		[StringLength(50)]
		public string BusinessCollateralAgmLineGUID { get; set; }
		[StringLength(20)]
		public string AccountNumber { get; set; }
		[StringLength(50)]
		public string BankGroupGUID { get; set; }
		[StringLength(50)]
		public string BankTypeGUID { get; set; }
		[Key]
		[Column(Order = 2)]
		[StringLength(50)]
		public string BusinessCollateralAgmTableGUID { get; set; }
		[StringLength(50)]
		public string BusinessCollateralSubTypeGUID { get; set; }
		[StringLength(50)]
		public string BusinessCollateralTypeGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal BusinessCollateralValue { get; set; }
		[StringLength(100)]
		public string BuyerName { get; set; }
		public string BuyerTableGUID { get; set; }
		[StringLength(13)]
		public string BuyerTaxIdentificationId { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CapitalValuation { get; set; }
		[StringLength(100)]
		public string ChassisNumber { get; set; }
		[StringLength(50)]
		public string CreditAppReqBusinessCollateralGUID { get; set; }
		public string DateOfValuation { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal GuaranteeAmount { get; set; }
		[StringLength(100)]
		public string Lessee { get; set; }
		[StringLength(100)]
		public string Lessor { get; set; }
		[Key]
		[Column(Order = 3)]
		public int LineNum { get; set; }
		[StringLength(100)]
		public string MachineNumber { get; set; }
		[StringLength(100)]
		public string MachineRegisteredStatus { get; set; }
		[StringLength(100)]
		public string Ownership { get; set; }
		public int PreferentialCreditorNumber { get; set; }
		[StringLength(100)]
		public string ProjectName { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Quantity { get; set; }
		public string RefAgreementDate { get; set; }
		[StringLength(20)]
		public string RefAgreementId { get; set; }
		[StringLength(100)]
		public string RegisteredPlace { get; set; }
		[StringLength(100)]
		public string RegistrationPlateNumber { get; set; }
		[StringLength(100)]
		public string TitleDeedDistrict { get; set; }
		[StringLength(100)]
		public string TitleDeedNumber { get; set; }
		[StringLength(100)]
		public string TitleDeedProvince { get; set; }
		[StringLength(100)]
		public string TitleDeedSubDistrict { get; set; }
		[StringLength(20)]
		public string Unit { get; set; }
		[StringLength(100)]
		public string ValuationCommittee { get; set; }
		[StringLength(150)]
		public string Product { get; set; }
		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }
		[Key]
		[Column(Order = 4)]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }

	}
}
