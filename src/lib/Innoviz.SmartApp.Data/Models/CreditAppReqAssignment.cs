using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class CreditAppReqAssignment : CompanyBaseEntity 
	{
		public CreditAppReqAssignment()
		{
			AssignmentAgreementTableCreditAppReqAssignment = new HashSet<AssignmentAgreementTable>();
		}

		[Key]
		public Guid CreditAppReqAssignmentGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal AssignmentAgreementAmount { get; set; }
		public Guid? AssignmentAgreementTableGUID { get; set; }
		public Guid AssignmentMethodGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal BuyerAgreementAmount { get; set; }
		public Guid? BuyerAgreementTableGUID { get; set; }
		public Guid BuyerTableGUID { get; set; }
		public Guid CreditAppRequestTableGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public bool IsNew { get; set; }
		[StringLength(20)]
		public string ReferenceAgreementId { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal RemainingAmount { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CreditAppReqAssignmentCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CreditAppReqAssignmentBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("AssignmentAgreementTableGUID")]
		[InverseProperty("CreditAppReqAssignmentAssignmentAgreementTable")]
		public AssignmentAgreementTable AssignmentAgreementTable { get; set; }
		[ForeignKey("AssignmentMethodGUID")]
		[InverseProperty("CreditAppReqAssignmentAssignmentMethod")]
		public AssignmentMethod AssignmentMethod { get; set; }
		[ForeignKey("BuyerAgreementTableGUID")]
		[InverseProperty("CreditAppReqAssignmentBuyerAgreementTable")]
		public BuyerAgreementTable BuyerAgreementTable { get; set; }
		[ForeignKey("BuyerTableGUID")]
		[InverseProperty("CreditAppReqAssignmentBuyerTable")]
		public BuyerTable BuyerTable { get; set; }
		[ForeignKey("CreditAppRequestTableGUID")]
		[InverseProperty("CreditAppReqAssignmentCreditAppRequestTable")]
		public CreditAppRequestTable CreditAppRequestTable { get; set; }
		[ForeignKey("CustomerTableGUID")]
		[InverseProperty("CreditAppReqAssignmentCustomerTable")]
		public CustomerTable CustomerTable { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("CreditAppReqAssignment")]
		public ICollection<AssignmentAgreementTable> AssignmentAgreementTableCreditAppReqAssignment { get; set; }
		#endregion Collection
	}
}
