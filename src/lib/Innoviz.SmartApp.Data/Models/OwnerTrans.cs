using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class OwnerTrans : CompanyBaseEntity
	{
		public OwnerTrans()
		{
		}
 
		[Key]
		public Guid OwnerTransGUID { get; set; }
		public bool InActive { get; set; }
		public int Ordering { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal PropotionOfShareholderPct { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		public Guid RelatedPersonTableGUID { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("OwnerTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("OwnerTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("RelatedPersonTableGUID")]
		[InverseProperty("OwnerTransRelatedPersonTable")]
		public RelatedPersonTable RelatedPersonTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
