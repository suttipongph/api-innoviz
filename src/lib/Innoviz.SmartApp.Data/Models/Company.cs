﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
    public partial class Company : BaseEntity 
    {
        public Company()
        {
            ActionHistoryCompany = new HashSet<ActionHistory>();
            AddressCountryCompany = new HashSet<AddressCountry>();
            AddressDistrictCompany = new HashSet<AddressDistrict>();
            AddressPostalCodeCompany = new HashSet<AddressPostalCode>();
            AddressProvinceCompany = new HashSet<AddressProvince>();
            AddressSubDistrictCompany = new HashSet<AddressSubDistrict>();
            AddressTransCompany = new HashSet<AddressTrans>();
            AgingReportSetupCompany = new HashSet<AgingReportSetup>();
            AgreementTableInfoCompany = new HashSet<AgreementTableInfo>();
            AgreementTableInfoTextCompany = new HashSet<AgreementTableInfoText>();
            AgreementTypeCompany = new HashSet<AgreementType>();
            ApplicationTableCompany = new HashSet<ApplicationTable>();
            AssignmentAgreementLineCompany = new HashSet<AssignmentAgreementLine>();
            AssignmentAgreementSettleCompany = new HashSet<AssignmentAgreementSettle>();
            AssignmentAgreementTableCompany = new HashSet<AssignmentAgreementTable>();
            AssignmentMethodCompany = new HashSet<AssignmentMethod>();
            AuthorizedPersonTransCompany = new HashSet<AuthorizedPersonTrans>();
            AuthorizedPersonTypeCompany = new HashSet<AuthorizedPersonType>();
            BankGroupCompany = new HashSet<BankGroup>();
            BankTypeCompany = new HashSet<BankType>();
            BillingResponsibleByCompany = new HashSet<BillingResponsibleBy>();
            BlacklistStatusCompany = new HashSet<BlacklistStatus>();
            BookmarkDocumentCompany = new HashSet<BookmarkDocument>();
            BookmarkDocumentTemplateLineCompany = new HashSet<BookmarkDocumentTemplateLine>();
            BookmarkDocumentTemplateTableCompany = new HashSet<BookmarkDocumentTemplateTable>();
            BookmarkDocumentTransCompany = new HashSet<BookmarkDocumentTrans>();
            BranchCompany = new HashSet<Branch>();
            BusinessCollateralAgmLineCompany = new HashSet<BusinessCollateralAgmLine>();
            BusinessCollateralAgmTableCompany = new HashSet<BusinessCollateralAgmTable>();
            BusinessCollateralStatusCompany = new HashSet<BusinessCollateralStatus>();
            BusinessCollateralSubTypeCompany = new HashSet<BusinessCollateralSubType>();
            BusinessCollateralTypeCompany = new HashSet<BusinessCollateralType>();
            BusinessSegmentCompany = new HashSet<BusinessSegment>();
            BusinessSizeCompany = new HashSet<BusinessSize>();
            BusinessTypeCompany = new HashSet<BusinessType>();
            BusinessUnitCompany = new HashSet<BusinessUnit>();
            BuyerAgreementLineCompany = new HashSet<BuyerAgreementLine>();
            BuyerAgreementTableCompany = new HashSet<BuyerAgreementTable>();
            BuyerAgreementTransCompany = new HashSet<BuyerAgreementTrans>();
            BuyerCreditLimitByProductCompany = new HashSet<BuyerCreditLimitByProduct>();
            BuyerInvoiceTableCompany = new HashSet<BuyerInvoiceTable>();
            BuyerReceiptTableCompany = new HashSet<BuyerReceiptTable>();
            BuyerTableCompany = new HashSet<BuyerTable>();
            CalendarGroupCompany = new HashSet<CalendarGroup>();
            CalendarNonWorkingDateCompany = new HashSet<CalendarNonWorkingDate>();
            CAReqAssignmentOutstandingCompany = new HashSet<CAReqAssignmentOutstanding>();
            CAReqBuyerCreditOutstandingCompany = new HashSet<CAReqBuyerCreditOutstanding>();
            CAReqCreditOutStandingCompany = new HashSet<CAReqCreditOutStanding>();
            CAReqRetentionOutstandingCompany = new HashSet<CAReqRetentionOutstanding>();
            ChequeTableCompany = new HashSet<ChequeTable>();
            CollectionFollowUpCompany = new HashSet<CollectionFollowUp>();
            CollectionGroupCompany = new HashSet<CollectionGroup>();
            CompanyBankCompany = new HashSet<CompanyBank>();
            CompanyParameterCompany = new HashSet<CompanyParameter>();
            CompanySignatureCompany = new HashSet<CompanySignature>();
            ConsortiumLineCompany = new HashSet<ConsortiumLine>();
            ConsortiumTableCompany = new HashSet<ConsortiumTable>();
            ConsortiumTransCompany = new HashSet<ConsortiumTrans>();
            ContactPersonTransCompany = new HashSet<ContactPersonTrans>();
            ContactTransCompany = new HashSet<ContactTrans>();
            CreditAppLineCompany = new HashSet<CreditAppLine>();
            CreditAppReqAssignmentCompany = new HashSet<CreditAppReqAssignment>();
            CreditAppReqBusinessCollateralCompany = new HashSet<CreditAppReqBusinessCollateral>();
            CreditAppRequestLineCompany = new HashSet<CreditAppRequestLine>();
            CreditAppRequestLineAmendCompany = new HashSet<CreditAppRequestLineAmend>();
            CreditAppRequestTableCompany = new HashSet<CreditAppRequestTable>();
            CreditAppRequestTableAmendCompany = new HashSet<CreditAppRequestTableAmend>();
            CreditAppTableCompany = new HashSet<CreditAppTable>();
            CreditAppTransCompany = new HashSet<CreditAppTrans>();
            CreditLimitTypeCompany = new HashSet<CreditLimitType>();
            CreditScoringCompany = new HashSet<CreditScoring>();
            CreditTermCompany = new HashSet<CreditTerm>();
            CreditTypeCompany = new HashSet<CreditType>();
            CurrencyCompany = new HashSet<Currency>();
            CustBankCompany = new HashSet<CustBank>();
            CustBusinessCollateralCompany = new HashSet<CustBusinessCollateral>();
            CustGroupCompany = new HashSet<CustGroup>();
            CustomerCreditLimitByProductCompany = new HashSet<CustomerCreditLimitByProduct>();
            CustomerRefundTableCompany = new HashSet<CustomerRefundTable>();
            CustomerTableCompany = new HashSet<CustomerTable>();
            CustTransCompany = new HashSet<CustTrans>();
            CustVisitingTransCompany = new HashSet<CustVisitingTrans>();
            DepartmentCompany = new HashSet<Department>();
            DocumentConditionTemplateLineCompany = new HashSet<DocumentConditionTemplateLine>();
            DocumentConditionTemplateTableCompany = new HashSet<DocumentConditionTemplateTable>();
            DocumentConditionTransCompany = new HashSet<DocumentConditionTrans>();
            DocumentReasonCompany = new HashSet<DocumentReason>();
            DocumentReturnMethodCompany = new HashSet<DocumentReturnMethod>();
            DocumentReturnLineCompany = new HashSet<DocumentReturnLine>();
            DocumentReturnTableCompany = new HashSet<DocumentReturnTable>();
            DocumentTemplateTableCompany = new HashSet<DocumentTemplateTable>();
            DocumentTypeCompany = new HashSet<DocumentType>();
            EmployeeTableCompany = new HashSet<EmployeeTable>();
            EmplTeamCompany = new HashSet<EmplTeam>();
            ExchangeRateCompany = new HashSet<ExchangeRate>();
            ExposureGroupCompany = new HashSet<ExposureGroup>();
            ExposureGroupByProductCompany = new HashSet<ExposureGroupByProduct>();
            FinancialCreditTransCompany = new HashSet<FinancialCreditTrans>();
            FinancialStatementTransCompany = new HashSet<FinancialStatementTrans>();
            FreeTextInvoiceTableCompany = new HashSet<FreeTextInvoiceTable>();
            GenderCompany = new HashSet<Gender>();
            GradeClassificationCompany = new HashSet<GradeClassification>();
            GuarantorAgreementLineCompany = new HashSet<GuarantorAgreementLine>();
            GuarantorAgreementLineAffiliateCompany = new HashSet<GuarantorAgreementLineAffiliate>();
            GuarantorAgreementTableCompany = new HashSet<GuarantorAgreementTable>();
            GuarantorTransCompany = new HashSet<GuarantorTrans>();
            GuarantorTypeCompany = new HashSet<GuarantorType>();
            IntercompanyCompany = new HashSet<Intercompany>();
            IntercompanyInvoiceAdjustmentCompany = new HashSet<IntercompanyInvoiceAdjustment>();
            IntercompanyInvoiceSettlementCompany = new HashSet<IntercompanyInvoiceSettlement>();
            IntercompanyInvoiceTableCompany = new HashSet<IntercompanyInvoiceTable>();
            InterestRealizedTransCompany = new HashSet<InterestRealizedTrans>();
            InterestTypeCompany = new HashSet<InterestType>();
            InterestTypeValueCompany = new HashSet<InterestTypeValue>();
            IntroducedByCompany = new HashSet<IntroducedBy>();
            InvoiceLineCompany = new HashSet<InvoiceLine>();
            InvoiceNumberSeqSetupCompany = new HashSet<InvoiceNumberSeqSetup>();
            InvoiceRevenueTypeCompany = new HashSet<InvoiceRevenueType>();
            InvoiceSettlementDetailCompany = new HashSet<InvoiceSettlementDetail>();
            InvoiceTableCompany = new HashSet<InvoiceTable>();
            InvoiceTypeCompany = new HashSet<InvoiceType>();
            JobChequeCompany = new HashSet<JobCheque>();
            JobTypeCompany = new HashSet<JobType>();
            JointVentureTransCompany = new HashSet<JointVentureTrans>();
            KYCSetupCompany = new HashSet<KYCSetup>();
            LanguageCompany = new HashSet<Language>();
            LeaseTypeCompany = new HashSet<LeaseType>();
            LedgerDimensionCompany = new HashSet<LedgerDimension>();
            LedgerFiscalPeriodCompany = new HashSet<LedgerFiscalPeriod>();
            LedgerFiscalYearCompany = new HashSet<LedgerFiscalYear>();
            LineOfBusinessCompany = new HashSet<LineOfBusiness>();
            MainAgreementTableCompany = new HashSet<MainAgreementTable>();
            MaritalStatusCompany = new HashSet<MaritalStatus>();
            MemoTransCompany = new HashSet<MemoTrans>();
            MessengerJobTableCompany = new HashSet<MessengerJobTable>();
            MessengerTableCompany = new HashSet<MessengerTable>();
            MethodOfPaymentCompany = new HashSet<MethodOfPayment>();
            NationalityCompany = new HashSet<Nationality>();
            NCBAccountStatusCompany = new HashSet<NCBAccountStatus>();
            NCBTransCompany = new HashSet<NCBTrans>();
            NumberSeqParameterCompany = new HashSet<NumberSeqParameter>();
            NumberSeqSegmentCompany = new HashSet<NumberSeqSegment>();
            NumberSeqSetupByProductTypeCompany = new HashSet<NumberSeqSetupByProductType>();
            NumberSeqTableCompany = new HashSet<NumberSeqTable>();
            OccupationCompany = new HashSet<Occupation>();
            OwnershipCompany = new HashSet<Ownership>();
            OwnerTransCompany = new HashSet<OwnerTrans>();
            ParentCompanyCompany = new HashSet<ParentCompany>();
            PaymentDetailCompany = new HashSet<PaymentDetail>();
            PaymentFrequencyCompany = new HashSet<PaymentFrequency>();
            PaymentHistoryCompany = new HashSet<PaymentHistory>();
            ProcessTransCompany = new HashSet<ProcessTrans>();
            ProductSettledTransCompany = new HashSet<ProductSettledTrans>();
            ProductSubTypeCompany = new HashSet<ProductSubType>();
            ProdUnitCompany = new HashSet<ProdUnit>();
            ProjectProgressTableCompany = new HashSet<ProjectProgressTable>();
            ProjectReferenceTransCompany = new HashSet<ProjectReferenceTrans>();
            PropertyTypeCompany = new HashSet<PropertyType>();
            PurchaseLineCompany = new HashSet<PurchaseLine>();
            PurchaseTableCompany = new HashSet<PurchaseTable>();
            RaceCompany = new HashSet<Race>();
            ReceiptLineCompany = new HashSet<ReceiptLine>();
            ReceiptTableCompany = new HashSet<ReceiptTable>();
            ReceiptTempPaymDetailCompany = new HashSet<ReceiptTempPaymDetail>();
            ReceiptTempTableCompany = new HashSet<ReceiptTempTable>();
            RegistrationTypeCompany = new HashSet<RegistrationType>();
            RelatedPersonTableCompany = new HashSet<RelatedPersonTable>();
            RetentionConditionSetupCompany = new HashSet<RetentionConditionSetup>();
            RetentionConditionTransCompany = new HashSet<RetentionConditionTrans>();
            RetentionTransCompany = new HashSet<RetentionTrans>();
            SysRoleTableCompany = new HashSet<SysRoleTable>();
            ServiceFeeConditionTransCompany = new HashSet<ServiceFeeConditionTrans>();
            ServiceFeeCondTemplateLineCompany = new HashSet<ServiceFeeCondTemplateLine>();
            ServiceFeeCondTemplateTableCompany = new HashSet<ServiceFeeCondTemplateTable>();
            ServiceFeeTransCompany = new HashSet<ServiceFeeTrans>();
            TaxInvoiceLineCompany = new HashSet<TaxInvoiceLine>();
            TaxInvoiceTableCompany = new HashSet<TaxInvoiceTable>();
            TaxReportTransCompany = new HashSet<TaxReportTrans>();
            TaxTableCompany = new HashSet<TaxTable>();
            TaxValueCompany = new HashSet<TaxValue>();
            TerritoryCompany = new HashSet<Territory>();
            VendBankCompany = new HashSet<VendBank>();
            VendGroupCompany = new HashSet<VendGroup>();
            VendorPaymentTransCompany = new HashSet<VendorPaymentTrans>();
            VendorTableCompany = new HashSet<VendorTable>();
            VerificationLineCompany = new HashSet<VerificationLine>();
            VerificationTableCompany = new HashSet<VerificationTable>();
            VerificationTransCompany = new HashSet<VerificationTrans>();
            VerificationTypeCompany = new HashSet<VerificationType>();
            WithdrawalLineCompany = new HashSet<WithdrawalLine>();
            WithdrawalTableCompany = new HashSet<WithdrawalTable>();
            WithholdingTaxGroupCompany = new HashSet<WithholdingTaxGroup>();
            WithholdingTaxTableCompany = new HashSet<WithholdingTaxTable>();
            WithholdingTaxValueCompany = new HashSet<WithholdingTaxValue>();
            SysUserCompanyMapping = new HashSet<SysUserCompanyMapping>();
            SysUserTable = new HashSet<SysUserTable>();
            StagingTableCompany = new HashSet<StagingTable>();
            StagingTableVendorInfoCompany = new HashSet<StagingTableVendorInfo>();
            StagingTransTextCompany = new HashSet<StagingTransText>();
            StagingTableIntercoInvSettleCompany = new HashSet<StagingTableIntercoInvSettle>();
        }

        [Required]
        [StringLength(20)]
        public string CompanyId { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        [Key]
        public Guid CompanyGUID { get; set; }
        public string CompanyLogo { get; set; }
        [StringLength(100)]
        public string SecondName { get; set; }
        [StringLength(100)]
        public string AltName { get; set; }
        [StringLength(13)]
        public string TaxId { get; set; }
        public Guid? DefaultBranchGUID { get; set; }

        [InverseProperty("CompanyGU")]
        public ICollection<SysUserCompanyMapping> SysUserCompanyMapping { get; set; }
        [InverseProperty("DefaultCompanyGU")]
        public ICollection<SysUserTable> SysUserTable { get; set; }
        #region Collection
        [InverseProperty("Company")]
        public ICollection<ActionHistory> ActionHistoryCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<AddressCountry> AddressCountryCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<AddressDistrict> AddressDistrictCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<AddressPostalCode> AddressPostalCodeCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<AddressProvince> AddressProvinceCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<AddressSubDistrict> AddressSubDistrictCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<AddressTrans> AddressTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<AgingReportSetup> AgingReportSetupCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<AgreementTableInfo> AgreementTableInfoCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<AgreementTableInfoText> AgreementTableInfoTextCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<AgreementType> AgreementTypeCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<ApplicationTable> ApplicationTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<AssignmentAgreementLine> AssignmentAgreementLineCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<AssignmentAgreementSettle> AssignmentAgreementSettleCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<AssignmentAgreementTable> AssignmentAgreementTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<AssignmentMethod> AssignmentMethodCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<AuthorizedPersonTrans> AuthorizedPersonTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<AuthorizedPersonType> AuthorizedPersonTypeCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<BankGroup> BankGroupCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<BankType> BankTypeCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<BillingResponsibleBy> BillingResponsibleByCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<BlacklistStatus> BlacklistStatusCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<BookmarkDocument> BookmarkDocumentCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<BookmarkDocumentTemplateLine> BookmarkDocumentTemplateLineCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<BookmarkDocumentTemplateTable> BookmarkDocumentTemplateTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<BookmarkDocumentTrans> BookmarkDocumentTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<Branch> BranchCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<BusinessCollateralAgmLine> BusinessCollateralAgmLineCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<BusinessCollateralAgmTable> BusinessCollateralAgmTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<BusinessCollateralStatus> BusinessCollateralStatusCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<BusinessCollateralSubType> BusinessCollateralSubTypeCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<BusinessCollateralType> BusinessCollateralTypeCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<BusinessSegment> BusinessSegmentCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<BusinessSize> BusinessSizeCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<BusinessType> BusinessTypeCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<BusinessUnit> BusinessUnitCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<BuyerAgreementLine> BuyerAgreementLineCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<BuyerAgreementTable> BuyerAgreementTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<BuyerAgreementTrans> BuyerAgreementTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<BuyerCreditLimitByProduct> BuyerCreditLimitByProductCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<BuyerInvoiceTable> BuyerInvoiceTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<BuyerReceiptTable> BuyerReceiptTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<BuyerTable> BuyerTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CalendarGroup> CalendarGroupCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CalendarNonWorkingDate> CalendarNonWorkingDateCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CAReqAssignmentOutstanding> CAReqAssignmentOutstandingCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CAReqBuyerCreditOutstanding> CAReqBuyerCreditOutstandingCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CAReqCreditOutStanding> CAReqCreditOutStandingCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CAReqRetentionOutstanding> CAReqRetentionOutstandingCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<ChequeTable> ChequeTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CollectionFollowUp> CollectionFollowUpCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CollectionGroup> CollectionGroupCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CompanyBank> CompanyBankCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CompanyParameter> CompanyParameterCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CompanySignature> CompanySignatureCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<ConsortiumLine> ConsortiumLineCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<ConsortiumTable> ConsortiumTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<ConsortiumTrans> ConsortiumTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<ContactPersonTrans> ContactPersonTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<ContactTrans> ContactTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CreditAppLine> CreditAppLineCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CreditAppReqAssignment> CreditAppReqAssignmentCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CreditAppReqBusinessCollateral> CreditAppReqBusinessCollateralCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CreditAppRequestLine> CreditAppRequestLineCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CreditAppRequestLineAmend> CreditAppRequestLineAmendCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CreditAppRequestTable> CreditAppRequestTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CreditAppRequestTableAmend> CreditAppRequestTableAmendCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CreditAppTable> CreditAppTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CreditAppTrans> CreditAppTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CreditLimitType> CreditLimitTypeCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CreditScoring> CreditScoringCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CreditTerm> CreditTermCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CreditType> CreditTypeCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<Currency> CurrencyCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CustBank> CustBankCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CustBusinessCollateral> CustBusinessCollateralCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CustGroup> CustGroupCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CustomerCreditLimitByProduct> CustomerCreditLimitByProductCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CustomerRefundTable> CustomerRefundTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CustomerTable> CustomerTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CustTrans> CustTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<CustVisitingTrans> CustVisitingTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<Department> DepartmentCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<DocumentConditionTemplateLine> DocumentConditionTemplateLineCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<DocumentConditionTemplateTable> DocumentConditionTemplateTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<DocumentConditionTrans> DocumentConditionTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<DocumentReason> DocumentReasonCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<DocumentReturnMethod> DocumentReturnMethodCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<DocumentReturnLine> DocumentReturnLineCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<DocumentReturnTable> DocumentReturnTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<DocumentTemplateTable> DocumentTemplateTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<DocumentType> DocumentTypeCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<EmployeeTable> EmployeeTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<EmplTeam> EmplTeamCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<ExchangeRate> ExchangeRateCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<ExposureGroup> ExposureGroupCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<ExposureGroupByProduct> ExposureGroupByProductCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<FinancialCreditTrans> FinancialCreditTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<FinancialStatementTrans> FinancialStatementTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<FreeTextInvoiceTable> FreeTextInvoiceTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<Gender> GenderCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<GradeClassification> GradeClassificationCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<GuarantorAgreementLine> GuarantorAgreementLineCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<GuarantorAgreementLineAffiliate> GuarantorAgreementLineAffiliateCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<GuarantorAgreementTable> GuarantorAgreementTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<GuarantorTrans> GuarantorTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<GuarantorType> GuarantorTypeCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<Intercompany> IntercompanyCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<IntercompanyInvoiceAdjustment> IntercompanyInvoiceAdjustmentCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<IntercompanyInvoiceSettlement> IntercompanyInvoiceSettlementCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<IntercompanyInvoiceTable> IntercompanyInvoiceTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<InterestRealizedTrans> InterestRealizedTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<InterestType> InterestTypeCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<InterestTypeValue> InterestTypeValueCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<IntroducedBy> IntroducedByCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<InvoiceLine> InvoiceLineCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<InvoiceNumberSeqSetup> InvoiceNumberSeqSetupCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<InvoiceRevenueType> InvoiceRevenueTypeCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<InvoiceSettlementDetail> InvoiceSettlementDetailCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<InvoiceTable> InvoiceTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<InvoiceType> InvoiceTypeCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<JobCheque> JobChequeCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<JobType> JobTypeCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<JointVentureTrans> JointVentureTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<KYCSetup> KYCSetupCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<Language> LanguageCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<LeaseType> LeaseTypeCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<LedgerDimension> LedgerDimensionCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<LedgerFiscalPeriod> LedgerFiscalPeriodCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<LedgerFiscalYear> LedgerFiscalYearCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<LineOfBusiness> LineOfBusinessCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<MainAgreementTable> MainAgreementTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<MaritalStatus> MaritalStatusCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<MemoTrans> MemoTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<MessengerJobTable> MessengerJobTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<MessengerTable> MessengerTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<MethodOfPayment> MethodOfPaymentCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<Nationality> NationalityCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<NCBAccountStatus> NCBAccountStatusCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<NCBTrans> NCBTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<NumberSeqParameter> NumberSeqParameterCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<NumberSeqSegment> NumberSeqSegmentCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<NumberSeqSetupByProductType> NumberSeqSetupByProductTypeCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<NumberSeqTable> NumberSeqTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<Occupation> OccupationCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<Ownership> OwnershipCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<OwnerTrans> OwnerTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<ParentCompany> ParentCompanyCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<PaymentDetail> PaymentDetailCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<PaymentFrequency> PaymentFrequencyCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<PaymentHistory> PaymentHistoryCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<ProcessTrans> ProcessTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<ProductSettledTrans> ProductSettledTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<ProductSubType> ProductSubTypeCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<ProdUnit> ProdUnitCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<ProjectProgressTable> ProjectProgressTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<ProjectReferenceTrans> ProjectReferenceTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<PropertyType> PropertyTypeCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<PurchaseLine> PurchaseLineCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<PurchaseTable> PurchaseTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<Race> RaceCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<ReceiptLine> ReceiptLineCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<ReceiptTable> ReceiptTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<ReceiptTempPaymDetail> ReceiptTempPaymDetailCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<ReceiptTempTable> ReceiptTempTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<RegistrationType> RegistrationTypeCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<RelatedPersonTable> RelatedPersonTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<RetentionConditionSetup> RetentionConditionSetupCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<RetentionConditionTrans> RetentionConditionTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<RetentionTrans> RetentionTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<ServiceFeeConditionTrans> ServiceFeeConditionTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<ServiceFeeCondTemplateLine> ServiceFeeCondTemplateLineCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<ServiceFeeCondTemplateTable> ServiceFeeCondTemplateTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<ServiceFeeTrans> ServiceFeeTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<SysRoleTable> SysRoleTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<TaxInvoiceLine> TaxInvoiceLineCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<TaxInvoiceTable> TaxInvoiceTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<TaxReportTrans> TaxReportTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<TaxTable> TaxTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<TaxValue> TaxValueCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<Territory> TerritoryCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<VendBank> VendBankCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<VendGroup> VendGroupCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<VendorPaymentTrans> VendorPaymentTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<VendorTable> VendorTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<VerificationLine> VerificationLineCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<VerificationTable> VerificationTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<VerificationTrans> VerificationTransCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<VerificationType> VerificationTypeCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<WithdrawalLine> WithdrawalLineCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<WithdrawalTable> WithdrawalTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<WithholdingTaxGroup> WithholdingTaxGroupCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<WithholdingTaxTable> WithholdingTaxTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<WithholdingTaxValue> WithholdingTaxValueCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<StagingTable> StagingTableCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<StagingTableVendorInfo> StagingTableVendorInfoCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<StagingTransText> StagingTransTextCompany { get; set; }
        [InverseProperty("Company")]
        public ICollection<StagingTableIntercoInvSettle> StagingTableIntercoInvSettleCompany { get; set; }
        #endregion
    }
}
