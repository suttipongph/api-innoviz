using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class PaymentDetail : CompanyBaseEntity
	{
		public PaymentDetail()
		{
			ProcessTransPaymentDetail = new HashSet<ProcessTrans>();
		}
 
		[Key]
		public Guid PaymentDetailGUID { get; set; }
		public Guid? CustomerTableGUID { get; set; }
		public Guid? InvoiceTypeGUID { get; set; }
		public int PaidToType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal PaymentAmount { get; set; }
		public Guid? RefGUID { get; set; }
		public int RefType { get; set; }
		public bool SuspenseTransfer { get; set; }
		public Guid? VendorTableGUID { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("PaymentDetailCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("PaymentDetailBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("CustomerTableGUID")]
		[InverseProperty("PaymentDetailCustomerTable")]
		public CustomerTable CustomerTable { get; set; }
		[ForeignKey("InvoiceTypeGUID")]
		[InverseProperty("PaymentDetailInvoiceType")]
		public InvoiceType InvoiceType { get; set; }
		[ForeignKey("VendorTableGUID")]
		[InverseProperty("PaymentDetailVendorTable")]
		public VendorTable VendorTable { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("PaymentDetail")]
		public ICollection<ProcessTrans> ProcessTransPaymentDetail { get; set; }
		#endregion Collection
	}
}
