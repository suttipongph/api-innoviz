using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class ServiceFeeConditionTrans : CompanyBaseEntity
	{
		public ServiceFeeConditionTrans()
		{
		}
 
		[Key]
		public Guid ServiceFeeConditionTransGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal AmountBeforeTax { get; set; }
		public Guid? CreditAppRequestTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public bool Inactive { get; set; }
		public Guid InvoiceRevenueTypeGUID { get; set; }
		public int Ordering { get; set; }
		public Guid RefGUID { get; set; }
		public Guid? RefServiceFeeConditionTransGUID { get; set; }
		public int RefType { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("ServiceFeeConditionTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("ServiceFeeConditionTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("InvoiceRevenueTypeGUID")]
		[InverseProperty("ServiceFeeConditionTransInvoiceRevenueType")]
		public InvoiceRevenueType InvoiceRevenueType { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
