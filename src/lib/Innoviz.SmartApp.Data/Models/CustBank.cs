using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CustBank : CompanyBaseEntity
	{
		public CustBank()
		{
			AssignmentAgreementTableCustBank = new HashSet<AssignmentAgreementTable>();
			CreditAppRequestTableBankAccountControl = new HashSet<CreditAppRequestTable>();
			CreditAppRequestTablePDCBank = new HashSet<CreditAppRequestTable>();
			CreditAppTableBankAccountControl = new HashSet<CreditAppTable>();
			CreditAppTablePDCBank = new HashSet<CreditAppTable>();
		}
 
		[Key]
		public Guid CustBankGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string AccountNumber { get; set; }
		public bool BankAccountControl { get; set; }
		[Required]
		[StringLength(100)]
		public string BankAccountName { get; set; }
		[Required]
		[StringLength(100)]
		public string BankBranch { get; set; }
		public Guid BankGroupGUID { get; set; }
		public Guid BankTypeGUID { get; set; }
		public Guid? CustomerTableGUID { get; set; }
		public bool InActive { get; set; }
		public bool PDC { get; set; }
		public bool Primary { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CustBankCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CustBankBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BankGroupGUID")]
		[InverseProperty("CustBankBankGroup")]
		public BankGroup BankGroup { get; set; }
		[ForeignKey("BankTypeGUID")]
		[InverseProperty("CustBankBankType")]
		public BankType BankType { get; set; }
		[ForeignKey("CustomerTableGUID")]
		[InverseProperty("CustBankCustomerTable")]
		public CustomerTable CustomerTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("BankAccountControl")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTableBankAccountControl { get; set; }
		[InverseProperty("BankAccountControl")]
		public ICollection<CreditAppTable> CreditAppTableBankAccountControl { get; set; }
		[InverseProperty("CustBank")]
		public ICollection<AssignmentAgreementTable> AssignmentAgreementTableCustBank { get; set; }
		[InverseProperty("PDCBank")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTablePDCBank { get; set; }
		[InverseProperty("PDCBank")]
		public ICollection<CreditAppTable> CreditAppTablePDCBank { get; set; }
		#endregion Collection
	}
}
