using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class TaxInvoiceLine : BranchCompanyBaseEntity
	{
		public TaxInvoiceLine()
		{
		}
 
		[Key]
		public Guid TaxInvoiceLineGUID { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public Guid InvoiceRevenueTypeGUID { get; set; }
		[StringLength(250)]
		public string InvoiceText { get; set; }
		public int LineNum { get; set; }
		public Guid? ProdUnitGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Qty { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TaxAmount { get; set; }
		public Guid TaxInvoiceTableGUID { get; set; }
		public Guid TaxTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TotalAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TotalAmountBeforeTax { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal UnitPrice { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("TaxInvoiceLineCompany")]
		public Company Company { get; set; }
		[ForeignKey("BranchGUID")]
		[InverseProperty("TaxInvoiceLineBranch")]
		public Branch Branch { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("TaxInvoiceLineBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("Dimension1GUID")]
		[InverseProperty("TaxInvoiceLineDimension1")]
		public LedgerDimension LedgerDimension1 { get; set; }
		[ForeignKey("Dimension2GUID")]
		[InverseProperty("TaxInvoiceLineDimension2")]
		public LedgerDimension LedgerDimension2 { get; set; }
		[ForeignKey("Dimension3GUID")]
		[InverseProperty("TaxInvoiceLineDimension3")]
		public LedgerDimension LedgerDimension3 { get; set; }
		[ForeignKey("Dimension4GUID")]
		[InverseProperty("TaxInvoiceLineDimension4")]
		public LedgerDimension LedgerDimension4 { get; set; }
		[ForeignKey("Dimension5GUID")]
		[InverseProperty("TaxInvoiceLineDimension5")]
		public LedgerDimension LedgerDimension5 { get; set; }
		[ForeignKey("InvoiceRevenueTypeGUID")]
		[InverseProperty("TaxInvoiceLineInvoiceRevenueType")]
		public InvoiceRevenueType InvoiceRevenueType { get; set; }
		[ForeignKey("ProdUnitGUID")]
		[InverseProperty("TaxInvoiceLineProdUnit")]
		public ProdUnit ProdUnit { get; set; }
		[ForeignKey("TaxInvoiceTableGUID")]
		[InverseProperty("TaxInvoiceLineTaxInvoiceTable")]
		public TaxInvoiceTable TaxInvoiceTable { get; set; }
		[ForeignKey("TaxTableGUID")]
		[InverseProperty("TaxInvoiceLineTaxTable")]
		public TaxTable TaxTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
