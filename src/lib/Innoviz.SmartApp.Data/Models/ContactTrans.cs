using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class ContactTrans : CompanyBaseEntity
	{
		public ContactTrans()
		{
		}
 
		[Key]
		public Guid ContactTransGUID { get; set; }
		public int ContactType { get; set; }
		[Required]
		[StringLength(100)]
		public string ContactValue { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[StringLength(20)]
		public string Extension { get; set; }
		public bool PrimaryContact { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("ContactTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("ContactTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
