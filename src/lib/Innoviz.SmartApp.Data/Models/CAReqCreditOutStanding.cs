using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CAReqCreditOutStanding : CompanyBaseEntity
	{
		public CAReqCreditOutStanding()
		{
		}
 
		[Key]
		public Guid CAReqCreditOutStandingGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal AccumRetentionAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ApprovedCreditLimit { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ARBalance { get; set; }
		[Column(TypeName = "date")]
		public DateTime? AsOfDate { get; set; }
		public Guid CreditAppRequestTableGUID { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CreditLimitBalance { get; set; }
		public Guid? CreditLimitTypeGUID { get; set; }
		public Guid? CustomerTableGUID { get; set; }
		public int ProductType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ReserveToBeRefund { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CAReqCreditOutStandingCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CAReqCreditOutStandingBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("CreditAppRequestTableGUID")]
		[InverseProperty("CAReqCreditOutStandingCreditAppRequestTable")]
		public CreditAppRequestTable CreditAppRequestTable { get; set; }
		[ForeignKey("CreditAppTableGUID")]
		[InverseProperty("CAReqCreditOutStandingCreditAppTable")]
		public CreditAppTable CreditAppTable { get; set; }
		[ForeignKey("CreditLimitTypeGUID")]
		[InverseProperty("CAReqCreditOutStandingCreditLimitType")]
		public CreditLimitType CreditLimitType { get; set; }
		[ForeignKey("CustomerTableGUID")]
		[InverseProperty("CAReqCreditOutStandingCustomerTable")]
		public CustomerTable CustomerTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
