using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_EmployeeTable
	{

		[Key]
		[Column(Order = 2)]
		[StringLength(20)]
		public string EmployeeId { get; set; }
		[Required]
		[StringLength(100)]
		public string Name { get; set; }
		[Key]
		[Column(Order = 1)]
		[StringLength(50)]
		public string EmployeeTableGUID { get; set; }
		[StringLength(50)]
		public string EmplTeamGUID { get; set; }
		[StringLength(50)]
		public string Dimension5GUID { get; set; }
		[StringLength(50)]
		public string Dimension4GUID { get; set; }
		[StringLength(50)]
		public string Dimension3GUID { get; set; }
		[StringLength(50)]
		public string Dimension2GUID { get; set; }
		[StringLength(50)]
		public string Dimension1GUID { get; set; }
		[StringLength(50)]
		public string DepartmentGUID { get; set; }
		public bool AssistMD { get; set; }
		public bool InActive { get; set; }
		public string Signature { get; set; }
		[StringLength(50)]
		public string UserId { get; set; }

		#region LIT
		[StringLength(50)]
		public string BusinessUnitGUID { get; set; }
		[StringLength(50)]
		public string ReportToEmployeeTableGUID { get; set; }
		public string Position { get; set; }
		#endregion LIT

		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }
		[Key]
		[Column(Order = 3)]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }

	}
}


