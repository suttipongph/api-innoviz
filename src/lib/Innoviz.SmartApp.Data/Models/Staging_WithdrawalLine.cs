using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_WithdrawalLine
	{
		[Key]
		[Column(Order = 1)]
		[StringLength(50)]
		public string WithdrawalLineGUID { get; set; }
		public string BillingDate { get; set; }
		[StringLength(50)]
		public string BuyerPDCTableGUID { get; set; }
		public bool ClosedForTermExtension { get; set; }
		public string CollectionDate { get; set; }
		[StringLength(50)]
		public string CustomerPDCTableGUID { get; set; }
		[StringLength(50)]
		public string Dimension1GUID { get; set; }
		[StringLength(50)]
		public string Dimension2GUID { get; set; }
		[StringLength(50)]
		public string Dimension3GUID { get; set; }
		[StringLength(50)]
		public string Dimension4GUID { get; set; }
		[StringLength(50)]
		public string Dimension5GUID { get; set; }
		[Required]
		public string DueDate { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InterestAmount { get; set; }
		[Required]
		public string InterestDate { get; set; }
		public int InterestDay { get; set; }
		[Key]
		[Column(Order = 3)]
		public int LineNum { get; set; }
		[Required]
		public string StartDate { get; set; }
		[StringLength(50)]
		public string WithdrawalLineInvoiceTableGUID { get; set; }
		public int WithdrawalLineType { get; set; }
		[Key]
		[Column(Order = 2)]
		[StringLength(50)]
		public string WithdrawalTableGUID { get; set; }

		[Key]
		[Column(Order = 4)]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }
		public int MigrateStatus { get; set; }
		public int MigrateSource { get; set; }

	}
}

