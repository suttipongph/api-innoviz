﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_IntercompanyInvoiceTable
	{
		[Key]
		[Column(Order = 1)]
		[StringLength(50)]
		public string IntercompanyInvoiceId { get; set; }
		[Required]
		[StringLength(50)]
		public string CustomerTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string CustomerName { get; set; }
		[Required]
		[StringLength(50)]
		public string InvoiceTypeGUID { get; set; }
		public int ProductType { get; set; }
		[StringLength(50)]
		public string CreditAppTableGUID { get; set; }
		[Required]
		public string IssuedDate { get; set; }
		[Required]
		public string DueDate { get; set; }
		[Required]
		[StringLength(20)]
		public string DocumentId { get; set; }
		[StringLength(5)]
		public string TaxBranchId { get; set; }
		[StringLength(100)]
		public string TaxBranchName { get; set; }
		[Required]
		[StringLength(50)]
		public string CurrencyGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ExchangeRate { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InvoiceAmount { get; set; }
		[Required]
		[StringLength(50)]
		public string InvoiceAddressTransGUID { get; set; }
		public int RefType { get; set; }
		[Required]
		[StringLength(50)]
		public string RefGUID { get; set; }
		[StringLength(50)]
		public string Dimension1GUID { get; set; }
		[StringLength(50)]
		public string Dimension2GUID { get; set; }
		[StringLength(50)]
		public string Dimension3GUID { get; set; }
		[StringLength(50)]
		public string Dimension4GUID { get; set; }
		[StringLength(50)]
		public string Dimension5GUID { get; set; }
		[StringLength(250)]
		public string InvoiceText { get; set; }
		[Required]
		[StringLength(50)]
		public string InvoiceRevenueTypeGUID { get; set; }
		[StringLength(50)]
		public string TaxTableGUID { get; set; }
		[StringLength(50)]
		public string WithholdingTaxTableGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string IntercompanyGUID { get; set; }
		[StringLength(50)]
		public string DocumentStatusGUID { get; set; }
		[StringLength(100)]
		public string FeeLedgerAccount { get; set; }
		[StringLength(50)]
		public string CNReasonGUID { get; set; }
		[StringLength(20)]
		public string OrigInvoiceId { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OrigInvoiceAmount { get; set; }
		[StringLength(20)]
		public string OrigTaxInvoiceId { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OrigTaxInvoiceAmount { get; set; }
		[Required]
		[StringLength(50)]
		public string IntercompanyInvoiceTableGUID { get; set; }

		[Key]
		[Column(Order = 2)]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }
		public int MigrateStatus { get; set; }
		public int MigrateSource { get; set; }
	}
}
