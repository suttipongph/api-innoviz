using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class IntroducedBy : CompanyBaseEntity
	{
		public IntroducedBy()
		{
			ApplicationTableIntroducedBy = new HashSet<ApplicationTable>();
			CustomerTableIntroducedBy = new HashSet<CustomerTable>();
		}
 
		[Key]
		public Guid IntroducedByGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string IntroducedById { get; set; }
		public Guid? VendorTableGUID { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("IntroducedByCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("IntroducedByBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("VendorTableGUID")]
		[InverseProperty("IntroducedByVendorTable")]
		public VendorTable VendorTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("IntroducedBy")]
		public ICollection<ApplicationTable> ApplicationTableIntroducedBy { get; set; }
		[InverseProperty("IntroducedBy")]
		public ICollection<CustomerTable> CustomerTableIntroducedBy { get; set; }
		#endregion Collection
	}
}
