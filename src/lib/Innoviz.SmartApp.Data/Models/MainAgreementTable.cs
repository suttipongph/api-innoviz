using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class MainAgreementTable : CompanyBaseEntity
	{
		public MainAgreementTable()
		{
			GuarantorAgreementTableMainAgreementTable = new HashSet<GuarantorAgreementTable>();
			MainAgreementTableRefMainAgreementTable = new HashSet<MainAgreementTable>();
		}
 
		[Key]
		public Guid MainAgreementTableGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime AgreementDate { get; set; }
		public int AgreementDocType { get; set; }
		public int Agreementextension { get; set; }
		public int AgreementYear { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ApprovedCreditLimit { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ApprovedCreditLimitLine { get; set; }
		[StringLength(100)]
		public string BuyerAgreementDescription { get; set; }
		[StringLength(20)]
		public string BuyerAgreementReferenceId { get; set; }
		[StringLength(100)]
		public string BuyerName { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public Guid? ConsortiumTableGUID { get; set; }
		public Guid CreditAppRequestTableGUID { get; set; }
		public Guid CreditAppTableGUID { get; set; }
		public Guid CreditLimitTypeGUID { get; set; }
		[StringLength(100)]
		public string CustomerAltName { get; set; }
		[Required]
		[StringLength(100)]
		public string CustomerName { get; set; }
		public Guid CustomerTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public Guid? DocumentReasonGUID { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime? ExpiryDate { get; set; }
		[Required]
		[StringLength(20)]
		public string InternalMainAgreementId { get; set; }
		[StringLength(20)]
		public string MainAgreementId { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal MaxInterestPct { get; set; }
		public int ProductType { get; set; }
		public Guid? RefMainAgreementTableGUID { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		[Column(TypeName = "date")]
		public DateTime? SigningDate { get; set; }
		[Column(TypeName = "date")]
		public DateTime? StartDate { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal TotalInterestPct { get; set; }
		public Guid? WithdrawalTableGUID { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("MainAgreementTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("MainAgreementTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BuyerTableGUID")]
		[InverseProperty("MainAgreementTableBuyerTable")]
		public BuyerTable BuyerTable { get; set; }
		[ForeignKey("ConsortiumTableGUID")]
		[InverseProperty("MainAgreementTableConsortiumTable")]
		public ConsortiumTable ConsortiumTable { get; set; }
		[ForeignKey("CreditAppRequestTableGUID")]
		[InverseProperty("MainAgreementTableCreditAppRequestTable")]
		public CreditAppRequestTable CreditAppRequestTable { get; set; }
		[ForeignKey("CreditAppTableGUID")]
		[InverseProperty("MainAgreementTableCreditAppTable")]
		public CreditAppTable CreditAppTable { get; set; }
		[ForeignKey("CreditLimitTypeGUID")]
		[InverseProperty("MainAgreementTableCreditLimitType")]
		public CreditLimitType CreditLimitType { get; set; }
		[ForeignKey("DocumentReasonGUID")]
		[InverseProperty("MainAgreementTableDocumentReason")]
		public DocumentReason DocumentReason { get; set; }
		[ForeignKey("DocumentStatusGUID")]
		[InverseProperty("MainAgreementTableDocumentStatus")]
		public DocumentStatus DocumentStatus { get; set; }
		[ForeignKey("RefMainAgreementTableGUID")]
		[InverseProperty("MainAgreementTableRefMainAgreementTable")]
		public MainAgreementTable RefMainAgreementTable { get; set; }
		[ForeignKey("WithdrawalTableGUID")]
		[InverseProperty("MainAgreementTableWithdrawalTable")]
		public WithdrawalTable WithdrawalTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("MainAgreementTable")]
		public ICollection<GuarantorAgreementTable> GuarantorAgreementTableMainAgreementTable { get; set; }
		[InverseProperty("RefMainAgreementTable")]
		public ICollection<MainAgreementTable> MainAgreementTableRefMainAgreementTable { get; set; }
		#endregion Collection
	}
}
