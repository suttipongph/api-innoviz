﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_CreditAppTrans
	{
		[Key]
		[StringLength(50)]
		public string CreditAppTransGUID { get; set; }
		[StringLength(50)]
		public string BuyerTableGUID { get; set; }
		[StringLength(50)]
		public string CreditAppLineGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string CreditAppTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CreditDeductAmount { get; set; }
		[Required]
		[StringLength(50)]
		public string CustomerTableGUID { get; set; }
		[StringLength(20)]
		public string DocumentId { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InvoiceAmount { get; set; }
		public int ProductType { get; set; }
		[StringLength(50)]
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		[Required]
		[StringLength(50)]
		public string TransDate { get; set; }
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		[StringLength(50)]
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		[StringLength(50)]
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		public string OwnerBusinessUnitGUID { get; set; }
		public int MigrateStatus { get; set; }
		public int MigrateSource { get; set; }
	}
}
