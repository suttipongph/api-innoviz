using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_InterestRealizedTrans
	{
		[Key]
		[Column(Order = 1)]
		[StringLength(50)]
		public string InterestRealizedTransGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal AccInterestAmount { get; set; }
		[Required]
		[StringLength(50)]
		public string AccountingDate { get; set; }
		public bool Accrued { get; set; }
		[StringLength(20)]
		public string DocumentId { get; set; }
		[Required]
		[StringLength(50)]
		public string EndDate { get; set; }
		public int InterestDay { get; set; }
		[Required]
		[StringLength(50)]
		public string InterestPerDay { get; set; }
		[Key]
		[Column(Order = 3)]
		public int LineNum { get; set; }
		public int ProductType { get; set; }
		[Key]
		[Column(Order = 2)]
		[StringLength(50)]
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		[Required]
		[StringLength(50)]
		public string StartDate { get; set; }
		public bool Cancelled { get; set; }
		[StringLength(50)]
		public string RefProcessTransGUID { get; set; }
		[StringLength(50)]
		public string Dimension1GUID { get; set; }
		[StringLength(50)]
		public string Dimension2GUID { get; set; }
		[StringLength(50)]
		public string Dimension3GUID { get; set; }
		[StringLength(50)]
		public string Dimension4GUID { get; set; }
		[StringLength(50)]
		public string Dimension5GUID { get; set; }


		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }
		[Key]
		[Column(Order = 4)]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		public string OwnerBusinessUnitGUID { get; set; }
	}
}


