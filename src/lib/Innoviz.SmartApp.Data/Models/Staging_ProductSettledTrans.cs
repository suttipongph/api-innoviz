using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_ProductSettledTrans
	{
 
		[Key]
		[StringLength(50)]
		public string ProductSettledTransGUID { get; set; }
		[StringLength(20)]
		public string DocumentId { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InterestCalcAmount { get; set; }
		public string InterestDate { get; set; }
		public int InterestDay { get; set; }
		[StringLength(50)]
		public string InvoiceSettlementDetailGUID { get; set; }
		[StringLength(20)]
		public string OriginalDocumentId { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OriginalInterestCalcAmount { get; set; }
		[StringLength(50)]
		public string OriginalRefGUID  { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal PDCInterestOutstanding { get; set; }
		public int ProductType { get; set; }
		[Required]
		[StringLength(50)]
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ReserveToBeRefund { get; set; }
		public string SettledDate { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettledInvoiceAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettledPurchaseAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettledReserveAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TotalRefundAdditionalIntAmount { get; set; }
		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }
		[Required]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }

	}
}
