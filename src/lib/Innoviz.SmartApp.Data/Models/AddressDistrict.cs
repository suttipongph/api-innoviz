using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class AddressDistrict : CompanyBaseEntity
	{
		public AddressDistrict()
		{
			AddressSubDistrictAddressDistrict = new HashSet<AddressSubDistrict>();
			AddressTransAddressDistrict = new HashSet<AddressTrans>();
		}
 
		[Key]
		public Guid AddressDistrictGUID { get; set; }
		public Guid AddressProvinceGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string DistrictId { get; set; }
		[Required]
		[StringLength(100)]
		public string Name { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("AddressDistrictCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("AddressDistrictBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("AddressProvinceGUID")]
		[InverseProperty("AddressDistrictAddressProvince")]
		public AddressProvince AddressProvince { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("AddressDistrict")]
		public ICollection<AddressSubDistrict> AddressSubDistrictAddressDistrict { get; set; }
		[InverseProperty("AddressDistrict")]
		public ICollection<AddressTrans> AddressTransAddressDistrict { get; set; }
		#endregion Collection
	}
}
