using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CompanyBank : CompanyBaseEntity
	{
		public CompanyBank()
		{
			AgreementTableInfoCompanyBank = new HashSet<AgreementTableInfo>();
			MethodOfPaymentCompanyBank = new HashSet<MethodOfPayment>();
		}
 
		[Key]
		public Guid CompanyBankGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string AccountNumber { get; set; }
		[Required]
		[StringLength(100)]
		public string BankAccountName { get; set; }
		[Required]
		[StringLength(100)]
		public string BankBranch { get; set; }
		public Guid BankGroupGUID { get; set; }
		public Guid BankTypeGUID { get; set; }
		public bool InActive { get; set; }
		public bool Primary { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CompanyBankCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CompanyBankBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BankGroupGUID")]
		[InverseProperty("CompanyBankBankGroup")]
		public BankGroup BankGroup { get; set; }
		[ForeignKey("BankTypeGUID")]
		[InverseProperty("CompanyBankBankType")]
		public BankType BankType { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("CompanyBank")]
		public ICollection<AgreementTableInfo> AgreementTableInfoCompanyBank { get; set; }
		[InverseProperty("CompanyBank")]
		public ICollection<MethodOfPayment> MethodOfPaymentCompanyBank { get; set; }
		#endregion Collection
	}
}
