﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_BuyerAgreementLine
	{
		[Key]
		[StringLength(50)]
		public string BuyerAgreementLineGUID { get; set; }
		public bool AlreadyInvoiced { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Amount { get; set; }
		[Required]
		[StringLength(50)]
		public string BuyerAgreementTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public string DueDate { get; set; }
		public int Period { get; set; }
		[StringLength(1000)]
		public string Remark { get; set; }

		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }

		[Required]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }
	}
}


