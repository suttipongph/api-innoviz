using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class StagingTableVendorInfo : CompanyBaseEntity
	{
		public StagingTableVendorInfo()
		{
		}
		[Key]
		public Guid StagingTableVendorInfoGUID { get; set; }
		[StringLength(250)]
		public string Address { get; set; }
		[StringLength(100)]
		public string AddressName { get; set; }
		[StringLength(100)]
		public string AltName { get; set; }
		[StringLength(20)]
		public string BankAccount { get; set; }
		[StringLength(100)]
		public string BankAccountName { get; set; }
		[StringLength(100)]
		public string BankBranch { get; set; }
		[StringLength(20)]
		public string BankGroupId { get; set; }
		[Required]
		[StringLength(20)]
		public string CompanyId { get; set; }
		[StringLength(20)]
		public string CountryId { get; set; }
		[StringLength(20)]
		public string CurrencyId { get; set; }
		[StringLength(20)]
		public string DistrictId { get; set; }
		[StringLength(100)]
		public string Name { get; set; }
		[StringLength(20)]
		public string PostalCode { get; set; }
		public Guid? ProcessTransGUID { get; set; }
		[StringLength(20)]
		public string ProvinceId { get; set; }
		public int RecordType { get; set; }
		[StringLength(20)]
		public string SubDistrictId { get; set; }
		[StringLength(5)]
		public string TaxBranchId { get; set; }
		[StringLength(20)]
		public string VendGroupId { get; set; }
		[StringLength(20)]
		public string VendorId { get; set; }
		public Guid? VendorPaymentTransGUID { get; set; }
		[StringLength(20)]
		public string VendorTaxId { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("StagingTableVendorInfoCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("StagingTableVendorInfoBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("ProcessTransGUID")]
		[InverseProperty("StagingTableVendorInfoProcessTrans")]
		public ProcessTrans ProcessTrans { get; set; }
		[ForeignKey("VendorPaymentTransGUID")]
		[InverseProperty("StagingTableVendorInfoVendorPaymentTrans")]
		public VendorPaymentTrans VendorPaymentTrans { get; set; }
		#endregion ForeignKey

		#region Collection
		#endregion Collection
	}
}
