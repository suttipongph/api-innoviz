using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CollectionGroup : CompanyBaseEntity
	{
		public CollectionGroup()
		{
		}
 
		[Key]
		public Guid CollectionGroupGUID { get; set; }
		public string AgreementBranchId { get; set; }
		public int AgreementFromOverdueDay { get; set; }
		public string AgreementLeaseTypeId { get; set; }
		public int AgreementToOverdueDay { get; set; }
		[Required]
		[StringLength(20)]
		public string CollectionGroupId { get; set; }
		public string CustGroupId { get; set; }
		public string CustTerritoryId { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CollectionGroupCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CollectionGroupBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
