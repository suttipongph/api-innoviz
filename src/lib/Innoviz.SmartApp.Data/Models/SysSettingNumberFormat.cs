﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
    public partial class SysSettingNumberFormat : BaseEntity {
        public SysSettingNumberFormat()
        {
            SysUserTable = new HashSet<SysUserTable>();
        }
        
        [Key]
        [StringLength(20)]
        public string NumberFormatId { get; set; }
        [StringLength(100)]
        public string NumberMapping { get; set; }

        [InverseProperty("NumberFormat")]
        public ICollection<SysUserTable> SysUserTable { get; set; }
    }
}
