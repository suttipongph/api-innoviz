using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class LedgerFiscalYear : CompanyBaseEntity
	{
		public LedgerFiscalYear()
		{
			LedgerFiscalPeriodLedgerFiscalYear = new HashSet<LedgerFiscalPeriod>();
		}
 
		[Key]
		public Guid LedgerFiscalYearGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Column(TypeName = "date")]
		public DateTime EndDate { get; set; }
		[Required]
		[StringLength(20)]
		public string FiscalYearId { get; set; }
		public int LengthOfPeriod { get; set; }
		public int PeriodUnit { get; set; }
		[Column(TypeName = "date")]
		public DateTime StartDate { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("LedgerFiscalYearCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("LedgerFiscalYearBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("LedgerFiscalYear")]
		public ICollection<LedgerFiscalPeriod> LedgerFiscalPeriodLedgerFiscalYear { get; set; }
		#endregion Collection
	}
}
