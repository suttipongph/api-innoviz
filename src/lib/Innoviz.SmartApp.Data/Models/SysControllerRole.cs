﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
    public partial class SysControllerRole : BaseEntity {

        [Key]
        public Guid SysControllerRoleGUID { get; set; }
        public Guid SysControllerTableGUID { get; set; }
        public Guid RoleGUID { get; set; }

        public int AccessLevel { get; set; }

        [ForeignKey("SysControllerTableGUID")]
        [InverseProperty("SysControllerRole")]
        public SysControllerTable SysControllerTableGU { get; set; }
    }
}
