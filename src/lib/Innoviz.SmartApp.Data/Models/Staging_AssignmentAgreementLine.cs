using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_AssignmentAgreementLine
	{
		[Key]
		[Column(Order = 1)]
		[StringLength(50)]
		public string AssignmentAgreementLineGUID { get; set; }
		[Key]
		[Column(Order = 2)]
		[StringLength(50)]
		public string AssignmentAgreementTableGUID { get; set; }
		[StringLength(50)]
		public string BuyerAgreementTableGUID { get; set; }
		[Key]
		[Column(Order = 3)]
		public int LineNum { get; set; }
		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }

		[Key]
		[Column(Order = 4)]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }

	}
}
