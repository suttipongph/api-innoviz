using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Nationality : CompanyBaseEntity
	{
		public Nationality()
		{
			CustomerTableNationality = new HashSet<CustomerTable>();
			GuarantorAgreementLineNationality = new HashSet<GuarantorAgreementLine>();
			RelatedPersonTableNationality = new HashSet<RelatedPersonTable>();
		}
 
		[Key]
		public Guid NationalityGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string NationalityId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("NationalityCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("NationalityBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("Nationality")]
		public ICollection<CustomerTable> CustomerTableNationality { get; set; }
		[InverseProperty("Nationality")]
		public ICollection<GuarantorAgreementLine> GuarantorAgreementLineNationality { get; set; }
		[InverseProperty("Nationality")]
		public ICollection<RelatedPersonTable> RelatedPersonTableNationality { get; set; }
		#endregion Collection
	}
}
