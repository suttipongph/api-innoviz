﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_VendBank
	{
		[Key]
		[StringLength(50)]
		public string VendBankGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string BankAccount { get; set; }
		[Required]
		[StringLength(100)]
		public string BankAccountName { get; set; }
		[StringLength(100)]
		public string BankBranch { get; set; }
		[Required]
		[StringLength(50)]
		public string BankGroupGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string VendorTableGUID { get; set; }
		public bool Primary { get; set; }

		[Required]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		[StringLength(50)]
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		[StringLength(50)]
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		public string OwnerBusinessUnitGUID { get; set; }
		public int MigrateStatus { get; set; }
		public int MigrateSource { get; set; }
	}
}
