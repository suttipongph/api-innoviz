using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CustomerCreditLimitByProduct : CompanyBaseEntity
	{
		public CustomerCreditLimitByProduct()
		{
		}
 
		[Key]
		public Guid CustomerCreditLimitByProductGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CreditLimit { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public int ProductType { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CustomerCreditLimitByProductCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CustomerCreditLimitByProductBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("CustomerTableGUID")]
		[InverseProperty("CustomerCreditLimitByProductCustomerTable")]
		public CustomerTable CustomerTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
