using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_BuyerAgreementTable
	{

		[Key]
		[Column(Order = 1)]
		[StringLength(50)]
		public string BuyerAgreementTableGUID { get; set; }
		[Key]
		[Column(Order = 2)]
		[StringLength(20)]
		public string BuyerAgreementId { get; set; }
		[Required]
		[StringLength(50)]
		public string BuyerTableGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string CustomerTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		public string EndDate { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal MaximumCreditLimit { get; set; }
		[StringLength(100)]
		public string Penalty { get; set; }
		[Required]
		[StringLength(20)]
		public string ReferenceAgreementID { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		[Required]
		public string StartDate { get; set; }

		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }

		[Key]
		[Column(Order = 3)]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }

	}
}
