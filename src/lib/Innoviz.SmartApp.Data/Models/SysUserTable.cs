﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
    public partial class SysUserTable : BaseEntity {
        public SysUserTable()
        {
            EmployeeTable = new HashSet<EmployeeTable>();
            SysUserCompanyMapping = new HashSet<SysUserCompanyMapping>();
           
        }
        
        public Guid Id { get; set; }
        [StringLength(256)]
        public string UserName { get; set; }
        [StringLength(256)]
        public string Email { get; set; }
        [StringLength(256)]
        public string NormalizedEmail { get; set; }
        [StringLength(256)]
        public string NormalizedUserName { get; set; }
        public string PhoneNumber { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(20)]
        public string DateFormatId { get; set; }
        [StringLength(20)]
        public string TimeFormatId { get; set; }
        [StringLength(20)]
        public string NumberFormatId { get; set; }
        public bool InActive { get; set; }
        [StringLength(5)]
        public string LanguageId { get; set; }
        public bool ShowSystemLog { get; set; }
        public Guid? DefaultCompanyGUID { get; set; }
        public Guid? DefaultBranchGUID { get; set; }
        public string UserImage { get; set; }

        [ForeignKey("DateFormatId")]
        [InverseProperty("SysUserTable")]
        public SysSettingDateFormat DateFormat { get; set; }
        [ForeignKey("DefaultBranchGUID")]
        [InverseProperty("SysUserTable")]
        public Branch DefaultBranchGU { get; set; }
        [ForeignKey("DefaultCompanyGUID")]
        [InverseProperty("SysUserTable")]
        public Company DefaultCompanyGU { get; set; }
        [ForeignKey("NumberFormatId")]
        [InverseProperty("SysUserTable")]
        public SysSettingNumberFormat NumberFormat { get; set; }
        [ForeignKey("TimeFormatId")]
        [InverseProperty("SysUserTable")]
        public SysSettingTimeFormat TimeFormat { get; set; }
        [InverseProperty("User")]
        public ICollection<EmployeeTable> EmployeeTable { get; set; }
        [InverseProperty("UserGU")]
        public ICollection<SysUserCompanyMapping> SysUserCompanyMapping { get; set; }
    }
}
