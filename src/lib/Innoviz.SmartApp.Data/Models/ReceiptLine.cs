using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class ReceiptLine : BranchCompanyBaseEntity
	{
		public ReceiptLine()
		{
		}
 
		[Key]
		public Guid ReceiptLineGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime? DueDate { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InvoiceAmount { get; set; }
		public Guid? InvoiceTableGUID { get; set; }
		public int LineNum { get; set; }
		public Guid ReceiptTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleAmountMST { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleBaseAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleBaseAmountMST { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleTaxAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal SettleTaxAmountMST { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TaxAmount { get; set; }
		public Guid? TaxTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal WHTAmount { get; set; }
		public Guid? WithholdingTaxTableGUID { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("ReceiptLineCompany")]
		public Company Company { get; set; }
		[ForeignKey("BranchGUID")]
		[InverseProperty("ReceiptLineBranch")]
		public Branch Branch { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("ReceiptLineBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("InvoiceTableGUID")]
		[InverseProperty("ReceiptLineInvoiceTable")]
		public InvoiceTable InvoiceTable { get; set; }
		[ForeignKey("ReceiptTableGUID")]
		[InverseProperty("ReceiptLineReceiptTable")]
		public ReceiptTable ReceiptTable { get; set; }
		[ForeignKey("TaxTableGUID")]
		[InverseProperty("ReceiptLineTaxTable")]
		public TaxTable TaxTable { get; set; }
		[ForeignKey("WithholdingTaxTableGUID")]
		[InverseProperty("ReceiptLineWithholdingTaxTable")]
		public WithholdingTaxTable WithholdingTaxTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
