using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Currency : CompanyBaseEntity
	{
		public Currency()
		{
			ApplicationTableCurrency = new HashSet<ApplicationTable>();
			BuyerTableCurrency = new HashSet<BuyerTable>();
			CompanyParameterHomeCurrency = new HashSet<CompanyParameter>();
			CustomerRefundTableCurrency = new HashSet<CustomerRefundTable>();
			CustomerTableCurrency = new HashSet<CustomerTable>();
			CustTransCurrency = new HashSet<CustTrans>();
			ExchangeRateCurrency = new HashSet<ExchangeRate>();
			ExchangeRateHomeCurrency = new HashSet<ExchangeRate>();
			FreeTextInvoiceTableCurrency = new HashSet<FreeTextInvoiceTable>();
			IntercompanyInvoiceTableCurrency = new HashSet<IntercompanyInvoiceTable>();
			InvoiceTableCurrency = new HashSet<InvoiceTable>();
			PaymentHistoryCurrency = new HashSet<PaymentHistory>();
			ProcessTransCurrency = new HashSet<ProcessTrans>();
			ReceiptTableCurrency = new HashSet<ReceiptTable>();
			ReceiptTempTableCurrency = new HashSet<ReceiptTempTable>();
			TaxInvoiceTableCurrency = new HashSet<TaxInvoiceTable>();
			VendorTableCurrency = new HashSet<VendorTable>();
		}
 
		[Key]
		public Guid CurrencyGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string CurrencyId { get; set; }
		[Required]
		[StringLength(100)]
		public string Name { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CurrencyCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CurrencyBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("Currency")]
		public ICollection<ApplicationTable> ApplicationTableCurrency { get; set; }
		[InverseProperty("Currency")]
		public ICollection<BuyerTable> BuyerTableCurrency { get; set; }
		[InverseProperty("HomeCurrency")]
		public ICollection<CompanyParameter> CompanyParameterHomeCurrency { get; set; }
		[InverseProperty("Currency")]
		public ICollection<CustomerRefundTable> CustomerRefundTableCurrency { get; set; }
		[InverseProperty("Currency")]
		public ICollection<CustomerTable> CustomerTableCurrency { get; set; }
		[InverseProperty("Currency")]
		public ICollection<CustTrans> CustTransCurrency { get; set; }
		[InverseProperty("Currency")]
		public ICollection<ExchangeRate> ExchangeRateCurrency { get; set; }
		[InverseProperty("HomeCurrency")]
		public ICollection<ExchangeRate> ExchangeRateHomeCurrency { get; set; }
		[InverseProperty("Currency")]
		public ICollection<FreeTextInvoiceTable> FreeTextInvoiceTableCurrency { get; set; }
		[InverseProperty("Currency")]
		public ICollection<IntercompanyInvoiceTable> IntercompanyInvoiceTableCurrency { get; set; }
		[InverseProperty("Currency")]
		public ICollection<InvoiceTable> InvoiceTableCurrency { get; set; }
		[InverseProperty("Currency")]
		public ICollection<PaymentHistory> PaymentHistoryCurrency { get; set; }
		[InverseProperty("Currency")]
		public ICollection<ProcessTrans> ProcessTransCurrency { get; set; }
		[InverseProperty("Currency")]
		public ICollection<ReceiptTable> ReceiptTableCurrency { get; set; }
		[InverseProperty("Currency")]
		public ICollection<ReceiptTempTable> ReceiptTempTableCurrency { get; set; }
		[InverseProperty("Currency")]
		public ICollection<TaxInvoiceTable> TaxInvoiceTableCurrency { get; set; }
		[InverseProperty("Currency")]
		public ICollection<VendorTable> VendorTableCurrency { get; set; }
		#endregion Collection
	}
}
