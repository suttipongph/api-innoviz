using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_GuarantorAgreementTable
	{

		[Key]
		[Column(Order = 1)]
		[StringLength(50)]
		public string GuarantorAgreementTableGUID { get; set; }
		public bool Affiliate { get; set; }
		public string AgreementDate { get; set; }
		public int AgreementDocType { get; set; }
		public int AgreementExtension { get; set; }
		public int AgreementYear { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[StringLength(50)]
		public string DocumentReasonGUID { get; set; }
		[StringLength(50)]
		public string DocumentStatusGUID { get; set; }
		public string ExpiryDate { get; set; }
		[StringLength(20)]
		public string GuarantorAgreementId { get; set; }
		[Key]
		[Column(Order = 2)]
		[StringLength(20)]
		public string InternalGuarantorAgreementId { get; set; }
		[StringLength(50)]
		public string MainAgreementTableGUID { get; set; }
		[StringLength(50)]
		public string ParentCompanyGUID { get; set; }
		[StringLength(50)]
		public string RefGuarantorAgreementTableGUID { get; set; }
		public string SigningDate { get; set; }
		[StringLength(50)]
		public string WitnessCompany1GUID { get; set; }
		[StringLength(50)]
		public string WitnessCompany2GUID { get; set; }
		[StringLength(100)]
		public string WitnessCustomer1 { get; set; }
		[StringLength(100)]
		public string WitnessCustomer2 { get; set; }
		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }
		[Key]
		[Column(Order = 3)]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }
	}
}
