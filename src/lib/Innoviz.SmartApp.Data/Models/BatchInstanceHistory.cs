﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
    public partial class BatchInstanceHistory : BaseEntity
    {
        public BatchInstanceHistory() {
            BatchInstanceLog = new HashSet<BatchInstanceLog>();
        }

        [Key]
        public Guid InstanceHistoryId { get; set; }
        public Guid? TriggerId { get; set; }
        public Guid JobId { get; set; }
        public DateTime? ScheduledTime { get; set; }
        public DateTime? ActualStartTime { get; set; }
        public int? InstanceState { get; set; }
        public string ParamValues { get; set; }
        public string ControllerUrl { get; set; }
        public DateTime? FinishedDateTime { get; set; }

        [ForeignKey("JobId")]
        [InverseProperty("BatchInstanceHistory")]
        public BatchJob Job { get; set; }
        [ForeignKey("TriggerId")]
        [InverseProperty("BatchInstanceHistory")]
        public BatchTrigger Trigger { get; set; }
        [InverseProperty("InstanceHistory")]
        public ICollection<BatchInstanceLog> BatchInstanceLog { get; set; }
    }
}
