﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_VerificationTable
	{
		[Key]
		[Column(Order = 1)]
		[StringLength(50)]
		public string VerificationTableGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string BuyerTableGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string CreditAppTableGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string CustomerTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(50)]
		public string DocumentStatusGUID { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		[Required]
		[StringLength(50)]
		public string VerificationDate { get; set; }
		[Key]
		[Column(Order = 2)]
		[StringLength(20)]
		public string VerificationId { get; set; }
		[Key]
		[Column(Order = 3)]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		[StringLength(50)]
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		[StringLength(50)]
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		public string OwnerBusinessUnitGUID { get; set; }
		public int MigrateStatus { get; set; }
		public int MigrateSource { get; set; }
	}
}
