using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class GuarantorAgreementTable : CompanyBaseEntity
	{
		public GuarantorAgreementTable()
		{
			GuarantorAgreementLineGuarantorAgreementTable = new HashSet<GuarantorAgreementLine>();
			GuarantorAgreementTableRefGuarantorAgreementTable = new HashSet<GuarantorAgreementTable>();
		}
 
		[Key]
		public Guid GuarantorAgreementTableGUID { get; set; }
		public bool Affiliate { get; set; }
		[Column(TypeName = "date")]
		public DateTime AgreementDate { get; set; }
		public int AgreementDocType { get; set; }
		public int AgreementExtension { get; set; }
		public int AgreementYear { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public Guid? DocumentReasonGUID { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime ExpiryDate { get; set; }
		[StringLength(20)]
		public string GuarantorAgreementId { get; set; }
		[Required]
		[StringLength(20)]
		public string InternalGuarantorAgreementId { get; set; }
		public Guid MainAgreementTableGUID { get; set; }
		public Guid? ParentCompanyGUID { get; set; }
		public Guid? RefGuarantorAgreementTableGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime? SigningDate { get; set; }
		public Guid? WitnessCompany1GUID { get; set; }
		public Guid? WitnessCompany2GUID { get; set; }
		[StringLength(100)]
		public string WitnessCustomer1 { get; set; }
		[StringLength(100)]
		public string WitnessCustomer2 { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("GuarantorAgreementTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("GuarantorAgreementTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("DocumentReasonGUID")]
		[InverseProperty("GuarantorAgreementTableDocumentReason")]
		public DocumentReason DocumentReason { get; set; }
		[ForeignKey("DocumentStatusGUID")]
		[InverseProperty("GuarantorAgreementTableDocumentStatus")]
		public DocumentStatus DocumentStatus { get; set; }
		[ForeignKey("MainAgreementTableGUID")]
		[InverseProperty("GuarantorAgreementTableMainAgreementTable")]
		public MainAgreementTable MainAgreementTable { get; set; }
		[ForeignKey("RefGuarantorAgreementTableGUID")]
		[InverseProperty("GuarantorAgreementTableRefGuarantorAgreementTable")]
		public GuarantorAgreementTable RefGuarantorAgreementTable { get; set; }
		[ForeignKey("WitnessCompany1GUID")]
		[InverseProperty("GuarantorAgreementTableWitnessCompany1")]
		public EmployeeTable WitnessCompany1 { get; set; }
		[ForeignKey("WitnessCompany2GUID")]
		[InverseProperty("GuarantorAgreementTableWitnessCompany2")]
		public EmployeeTable WitnessCompany2 { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("GuarantorAgreementTable")]
		public ICollection<GuarantorAgreementLine> GuarantorAgreementLineGuarantorAgreementTable { get; set; }
		[InverseProperty("RefGuarantorAgreementTable")]
		public ICollection<GuarantorAgreementTable> GuarantorAgreementTableRefGuarantorAgreementTable { get; set; }
		#endregion Collection
	}
}
