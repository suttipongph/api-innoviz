﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_CustBank
	{
		[Key]
		[StringLength(50)]
		public string CustBankGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string AccountNumber { get; set; }
		public bool BankAccountControl { get; set; }
		[Required]
		[StringLength(100)]
		public string BankAccountName { get; set; }
		[Required]
		[StringLength(100)]
		public string BankBranch { get; set; }
		[Required]
		[StringLength(50)]
		public string BankGroupGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string BankTypeGUID { get; set; }
		[StringLength(50)]
		public string CustomerTableGUID { get; set; }
		public bool InActive { get; set; }
		public bool PDC { get; set; }
		public bool Primary { get; set; }

		[Required]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		[StringLength(50)]
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		[StringLength(50)]
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		public string OwnerBusinessUnitGUID { get; set; }
		public int MigrateStatus { get; set; }
		public int MigrateSource { get; set; }
	}
}
