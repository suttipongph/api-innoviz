using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class DocumentProcess : BaseEntity
	{
		public DocumentProcess()
		{
			DocumentStatusDocumentProcess = new HashSet<DocumentStatus>();
		}
 
		[Key]
		public Guid DocumentProcessGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string ProcessId { get; set; }
 
		#region ForeignKey
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("DocumentProcess")]
		public ICollection<DocumentStatus> DocumentStatusDocumentProcess { get; set; }
		#endregion Collection
	}
}
