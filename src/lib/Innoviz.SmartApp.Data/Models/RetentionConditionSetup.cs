using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class RetentionConditionSetup : CompanyBaseEntity
	{
		public RetentionConditionSetup()
		{
		}
 
		[Key]
		public Guid RetentionConditionSetupGUID { get; set; }
		public int ProductType { get; set; }
		public int RetentionCalculateBase { get; set; }
		public int RetentionDeductionMethod { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("RetentionConditionSetupCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("RetentionConditionSetupBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
