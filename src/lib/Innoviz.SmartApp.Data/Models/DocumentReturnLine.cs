using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class DocumentReturnLine : CompanyBaseEntity {
		[Key]
		public Guid DocumentReturnLineGUID { get; set; }
		[StringLength(500)]
		public string Address { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		[StringLength(100)]
		public string ContactPersonName { get; set; }
		[StringLength(20)]
		public string DocumentNo { get; set; }
		public Guid DocumentReturnTableGUID { get; set; }
		public Guid? DocumentTypeGUID { get; set; }
		public int LineNum { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("DocumentReturnLineCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("DocumentReturnLineBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BuyerTableGUID")]
		[InverseProperty("DocumentReturnLineBuyerTable")]
		public BuyerTable BuyerTable { get; set; }
		[ForeignKey("DocumentReturnTableGUID")]
		[InverseProperty("DocumentReturnLineDocumentReturnTable")]
		public DocumentReturnTable DocumentReturnTable { get; set; }
		[ForeignKey("DocumentTypeGUID")]
		[InverseProperty("DocumentReturnLineDocumentType")]
		public DocumentType DocumentType { get; set; }
		#endregion ForeignKey
	}
}
