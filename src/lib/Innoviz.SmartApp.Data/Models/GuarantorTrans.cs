using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class GuarantorTrans : CompanyBaseEntity
	{
		public GuarantorTrans()
		{
			GuarantorAgreementLineGuarantorTrans = new HashSet<GuarantorAgreementLine>();
		}
 
		[Key]
		public Guid GuarantorTransGUID { get; set; }
		public bool Affiliate { get; set; }
		public Guid GuarantorTypeGUID { get; set; }
		public bool InActive { get; set; }
		public int Ordering { get; set; }
		public Guid? RefGuarantorTransGUID { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		public Guid RelatedPersonTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal MaximumGuaranteeAmount { get; set; }


		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("GuarantorTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("GuarantorTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("GuarantorTypeGUID")]
		[InverseProperty("GuarantorTransGuarantorType")]
		public GuarantorType GuarantorType { get; set; }
		[ForeignKey("RelatedPersonTableGUID")]
		[InverseProperty("GuarantorTransRelatedPersonTable")]
		public RelatedPersonTable RelatedPersonTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("GuarantorTrans")]
		public ICollection<GuarantorAgreementLine> GuarantorAgreementLineGuarantorTrans { get; set; }
		#endregion Collection
		
	}
}
