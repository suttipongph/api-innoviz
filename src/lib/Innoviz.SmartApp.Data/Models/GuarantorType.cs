using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class GuarantorType : CompanyBaseEntity
	{
		public GuarantorType()
		{
			GuarantorTransGuarantorType = new HashSet<GuarantorTrans>();
		}
 
		[Key]
		public Guid GuarantorTypeGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string GuarantorTypeId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("GuarantorTypeCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("GuarantorTypeBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("GuarantorType")]
		public ICollection<GuarantorTrans> GuarantorTransGuarantorType { get; set; }
		#endregion Collection
	}
}
