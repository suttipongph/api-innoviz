using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class BuyerAgreementTable : CompanyBaseEntity
	{
		public BuyerAgreementTable()
		{
			AssignmentAgreementLineBuyerAgreementTable = new HashSet<AssignmentAgreementLine>();
			AssignmentAgreementSettleBuyerAgreementTable = new HashSet<AssignmentAgreementSettle>();
			BuyerAgreementLineBuyerAgreementTable = new HashSet<BuyerAgreementLine>();
			BuyerAgreementTransBuyerAgreementTable = new HashSet<BuyerAgreementTrans>();
			BuyerInvoiceTableBuyerAgreementTable = new HashSet<BuyerInvoiceTable>();
			BuyerReceiptTableBuyerAgreementTable = new HashSet<BuyerReceiptTable>();
			CreditAppReqAssignmentBuyerAgreementTable = new HashSet<CreditAppReqAssignment>();
			InvoiceSettlementDetailBuyerAgreementTable = new HashSet<InvoiceSettlementDetail>();
			InvoiceTableBuyerAgreementTable = new HashSet<InvoiceTable>();
			PurchaseLineBuyerAgreementTable = new HashSet<PurchaseLine>();
			RetentionTransBuyerAgreementTable = new HashSet<RetentionTrans>();
			WithdrawalTableBuyerAgreementTable = new HashSet<WithdrawalTable>();
		}

		[Key]
		public Guid BuyerAgreementTableGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string BuyerAgreementId { get; set; }
		public Guid BuyerTableGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Column(TypeName = "date")]
		public DateTime EndDate { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal MaximumCreditLimit { get; set; }
		[StringLength(100)]
		public string Penalty { get; set; }
		[Required]
		[StringLength(20)]
		public string ReferenceAgreementID { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		[Column(TypeName = "date")]
		public DateTime StartDate { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("BuyerAgreementTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("BuyerAgreementTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BuyerTableGUID")]
		[InverseProperty("BuyerAgreementTableBuyerTable")]
		public BuyerTable BuyerTable { get; set; }
		[ForeignKey("CustomerTableGUID")]
		[InverseProperty("BuyerAgreementTableCustomerTable")]
		public CustomerTable CustomerTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("BuyerAgreementTable")]
		public ICollection<AssignmentAgreementLine> AssignmentAgreementLineBuyerAgreementTable { get; set; }
		[InverseProperty("BuyerAgreementTable")]
		public ICollection<AssignmentAgreementSettle> AssignmentAgreementSettleBuyerAgreementTable { get; set; }
		[InverseProperty("BuyerAgreementTable")]
		public ICollection<BuyerAgreementLine> BuyerAgreementLineBuyerAgreementTable { get; set; }
		[InverseProperty("BuyerAgreementTable")]
		public ICollection<BuyerAgreementTrans> BuyerAgreementTransBuyerAgreementTable { get; set; }
		[InverseProperty("BuyerAgreementTable")]
		public ICollection<BuyerInvoiceTable> BuyerInvoiceTableBuyerAgreementTable { get; set; }
		[InverseProperty("BuyerAgreementTable")]
		public ICollection<BuyerReceiptTable> BuyerReceiptTableBuyerAgreementTable { get; set; }
		[InverseProperty("BuyerAgreementTable")]
		public ICollection<CreditAppReqAssignment> CreditAppReqAssignmentBuyerAgreementTable { get; set; }
		[InverseProperty("BuyerAgreementTable")]
		public ICollection<InvoiceSettlementDetail> InvoiceSettlementDetailBuyerAgreementTable { get; set; }
		[InverseProperty("BuyerAgreementTable")]
		public ICollection<InvoiceTable> InvoiceTableBuyerAgreementTable { get; set; }
		[InverseProperty("BuyerAgreementTable")]
		public ICollection<PurchaseLine> PurchaseLineBuyerAgreementTable { get; set; }
		[InverseProperty("BuyerAgreementTable")]
		public ICollection<RetentionTrans> RetentionTransBuyerAgreementTable { get; set; }
		[InverseProperty("BuyerAgreementTable")]
		public ICollection<WithdrawalTable> WithdrawalTableBuyerAgreementTable { get; set; }
		#endregion Collection
	}
}
