﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
    public partial class SysEnumTable : BaseEntity {
        
        [StringLength(30)]
        public string EnumName { get; set; }
        [Required]
        [StringLength(30)]
        public string AttributeName { get; set; }
        public int AttributeValue { get; set; }
        [StringLength(100)]
        public string AttributeDescription { get; set; }
    }
}
