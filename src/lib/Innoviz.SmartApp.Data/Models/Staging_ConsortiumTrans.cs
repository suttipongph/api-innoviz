using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_ConsortiumTrans
	{
		[Key]
		public string ConsortiumTransGUID { get; set; }
		[StringLength(500)]
		public string Address { get; set; }
		[StringLength(50)]
		public string AuthorizedPersonTypeGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string CustomerName { get; set; }
		[StringLength(100)]
		public string OperatedBy { get; set; }
		public int Ordering { get; set; }
		[Required]
		[StringLength(50)]
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		[StringLength(50)]
		public string ConsortiumLineGUID { get; set; }

		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }

		[Required]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }

	}
}

