using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class ConsortiumTable : CompanyBaseEntity
	{
		public ConsortiumTable()
		{
			AssignmentAgreementTableConsortiumTable = new HashSet<AssignmentAgreementTable>();
			BusinessCollateralAgmTableConsortiumTable = new HashSet<BusinessCollateralAgmTable>();
			ConsortiumLineConsortiumTable = new HashSet<ConsortiumLine>();
			CreditAppRequestTableConsortiumTable = new HashSet<CreditAppRequestTable>();
			CreditAppTableConsortiumTable = new HashSet<CreditAppTable>();
			MainAgreementTableConsortiumTable = new HashSet<MainAgreementTable>();
		}
 
		[Key]
		public Guid ConsortiumTableGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string ConsortiumId { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public Guid DocumentStatusGUID { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("ConsortiumTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("ConsortiumTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("DocumentStatusGUID")]
		[InverseProperty("ConsortiumTableDocumentStatus")]
		public DocumentStatus DocumentStatus { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("ConsortiumTable")]
		public ICollection<AssignmentAgreementTable> AssignmentAgreementTableConsortiumTable { get; set; }
		[InverseProperty("ConsortiumTable")]
		public ICollection<BusinessCollateralAgmTable> BusinessCollateralAgmTableConsortiumTable { get; set; }
		[InverseProperty("ConsortiumTable")]
		public ICollection<ConsortiumLine> ConsortiumLineConsortiumTable { get; set; }
		[InverseProperty("ConsortiumTable")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTableConsortiumTable { get; set; }
		[InverseProperty("ConsortiumTable")]
		public ICollection<CreditAppTable> CreditAppTableConsortiumTable { get; set; }
		[InverseProperty("ConsortiumTable")]
		public ICollection<MainAgreementTable> MainAgreementTableConsortiumTable { get; set; }
		#endregion Collection
	}
}
