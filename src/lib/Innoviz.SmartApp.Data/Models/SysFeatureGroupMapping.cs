﻿using Innoviz.SmartApp.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Innoviz.SmartApp.Data.Models
{
    public class SysFeatureGroupMapping : BaseEntity
    {
        public Guid SysFeatureGroupGUID { get; set; }
        public Guid SysFeatureTableGUID { get; set; }

        [ForeignKey("SysFeatureGroupGUID")]
        [InverseProperty("SysFeatureGroupMapping")]
        public SysFeatureGroup SysFeatureGroupGU { get; set; }
        [ForeignKey("SysFeatureTableGUID")]
        [InverseProperty("SysFeatureGroupMapping")]
        public SysFeatureTable SysFeatureTableGU { get; set; }
    }
}
