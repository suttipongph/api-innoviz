﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
    public partial class BatchTrigger : CompanyBaseEntity
    {
        public BatchTrigger() {
            BatchInstanceHistory = new HashSet<BatchInstanceHistory>();
        }

        [Key]
        public Guid TriggerId { get; set; }
        public Guid? JobId { get; set; }
        public string CRONExpression { get; set; }
        public int? RecurringCount { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }
        public int? IntervalType { get; set; }
        public string IntervalCount { get; set; }
        public DateTime? NextScheduledTime { get; set; }
        public DateTime? LastExecuted { get; set; }
        public bool IsFinalized { get; set; }
        public override Guid CompanyGUID { get; set; }

        [ForeignKey("JobId")]
        [InverseProperty("BatchTrigger")]
        public BatchJob Job { get; set; }
        [InverseProperty("Trigger")]
        public ICollection<BatchInstanceHistory> BatchInstanceHistory { get; set; }
    }
}
