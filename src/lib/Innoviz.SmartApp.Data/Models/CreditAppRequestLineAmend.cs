using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CreditAppRequestLineAmend : CompanyBaseEntity
	{
		public CreditAppRequestLineAmend()
		{
		}
 
		[Key]
		public Guid CreditAppRequestLineAmendGUID { get; set; }
		public bool AmendAssignmentMethod { get; set; }
		public bool AmendBillingDocumentCondition { get; set; }
		public bool AmendBillingInformation { get; set; }
		public bool AmendRate { get; set; }
		public bool AmendReceiptDocumentCondition { get; set; }
		public bool AmendReceiptInformation { get; set; }
		public Guid CreditAppRequestLineGUID { get; set; }
		public Guid? OriginalAssignmentMethodGUID { get; set; }
		[StringLength(250)]
		public string OriginalAssignmentMethodRemark { get; set; }
		public Guid? OriginalBillingAddressGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OriginalCreditLimitLineRequest { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal OriginalMaxPurchasePct { get; set; }
		public Guid? OriginalMethodOfPaymentGUID { get; set; }
		public int OriginalPurchaseFeeCalculateBase { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal OriginalPurchaseFeePct { get; set; }
		public Guid? OriginalReceiptAddressGUID { get; set; }
		[StringLength(250)]
		public string OriginalReceiptRemark { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CreditAppRequestLineAmendCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CreditAppRequestLineAmendBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
