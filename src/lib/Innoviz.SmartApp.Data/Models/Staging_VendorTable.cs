using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_VendorTable
	{
		[Key]
		[Column(Order = 1)]
		[StringLength(50)]
		public string VendorTableGUID { get; set; }
		[StringLength(100)]
		public string AltName { get; set; }
		[Required]
		[StringLength(50)]
		public string CurrencyGUID { get; set; }
		[StringLength(20)]
		public string ExternalCode { get; set; }
		[Required]
		[StringLength(100)]
		public string Name { get; set; }
		public int RecordType { get; set; }
		public bool SentToOtherSystem { get; set; }
		[StringLength(13)]
		public string TaxId { get; set; }
		[Required]
		[StringLength(50)]
		public string VendGroupGUID { get; set; }
		[Key]
		[Column(Order = 2)]
		[StringLength(20)]
		public string VendorId { get; set; }

		[Key]
		[Column(Order = 3)]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		[StringLength(50)]
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		[StringLength(50)]
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		public string OwnerBusinessUnitGUID { get; set; }
		public int MigrateStatus { get; set; }
		public int MigrateSource { get; set; }
	}
}

