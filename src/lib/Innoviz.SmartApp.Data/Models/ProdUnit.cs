using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class ProdUnit : CompanyBaseEntity
	{
		public ProdUnit()
		{
			InvoiceLineProdUnit = new HashSet<InvoiceLine>();
			TaxInvoiceLineProdUnit = new HashSet<TaxInvoiceLine>();
		}
 
		[Key]
		public Guid ProdUnitGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string UnitId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("ProdUnitCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("ProdUnitBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("ProdUnit")]
		public ICollection<InvoiceLine> InvoiceLineProdUnit { get; set; }
		[InverseProperty("ProdUnit")]
		public ICollection<TaxInvoiceLine> TaxInvoiceLineProdUnit { get; set; }
		#endregion Collection
	}
}
