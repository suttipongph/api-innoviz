using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_GuarantorAgreementLine
	{
		[Key]
		[Column(Order = 1)]
		[StringLength(50)]
		public string GuarantorAgreementLineGUID { get; set; }
		public int Age { get; set; }
		public string DateOfBirth { get; set; }
		public string DateOfIssue { get; set; }
		[Key]
		[Column(Order = 2)]
		[StringLength(50)]
		public string GuarantorAgreementTableGUID { get; set; }
		[StringLength(50)]
		public string GuarantorTransGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Name { get; set; }
		[StringLength(50)]
		public string NationalityGUID { get; set; }
		[Key]
		[Column(Order = 3)]
		public int Ordering { get; set; }
		[StringLength(20)]
		public string PassportId { get; set; }
		[StringLength(250)]
		public string PrimaryAddress1 { get; set; }
		[StringLength(250)]
		public string PrimaryAddress2 { get; set; }
		[StringLength(50)]
		public string RaceGUID { get; set; }
		[StringLength(13)]
		public string TaxId { get; set; }
		[StringLength(20)]
		public string WorkPermitId { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal MaximumGuaranteeAmount { get; set; }
		[StringLength(50)]
		public string CustomerTableGUID { get; set; }
		[StringLength(50)]
		public string CreditAppTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ApprovedCreditLimit { get; set; }
		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }

		[Key]
		[Column(Order = 4)]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }
	}
}
