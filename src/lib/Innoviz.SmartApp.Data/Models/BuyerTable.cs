using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class BuyerTable : CompanyBaseEntity
	{
		public BuyerTable()
		{
			AssignmentAgreementTableBuyerTable = new HashSet<AssignmentAgreementTable>();
			BusinessCollateralAgmLineBuyerTable = new HashSet<BusinessCollateralAgmLine>();
			BuyerAgreementTableBuyerTable = new HashSet<BuyerAgreementTable>();
			BuyerCreditLimitByProductBuyerTable = new HashSet<BuyerCreditLimitByProduct>();
			BuyerReceiptTableBuyerTable = new HashSet<BuyerReceiptTable>();
			CAReqAssignmentOutstandingBuyerTable = new HashSet<CAReqAssignmentOutstanding>();
			CAReqBuyerCreditOutstandingBuyerTable = new HashSet<CAReqBuyerCreditOutstanding>();
			ChequeTableBuyerTable = new HashSet<ChequeTable>();
			CollectionFollowUpBuyerTable = new HashSet<CollectionFollowUp>();
			CreditAppLineBuyerTable = new HashSet<CreditAppLine>();
			CreditAppReqAssignmentBuyerTable = new HashSet<CreditAppReqAssignment>();
			CreditAppReqBusinessCollateralBuyerTable = new HashSet<CreditAppReqBusinessCollateral>();
			CreditAppRequestLineBuyerTable = new HashSet<CreditAppRequestLine>();
			CreditAppTransBuyerTable = new HashSet<CreditAppTrans>();
			CustBusinessCollateralBuyerTable = new HashSet<CustBusinessCollateral>();
			CustTransBuyerTable = new HashSet<CustTrans>();
			DocumentReturnLineBuyerTable = new HashSet<DocumentReturnLine>();
			DocumentReturnTableBuyerTable = new HashSet<DocumentReturnTable>();
			InvoiceTableBuyerTable = new HashSet<InvoiceTable>();
			MainAgreementTableBuyerTable = new HashSet<MainAgreementTable>();
			MessengerJobTableBuyerTable = new HashSet<MessengerJobTable>();
			PurchaseLineBuyerTable = new HashSet<PurchaseLine>();
			ReceiptTempTableBuyerTable = new HashSet<ReceiptTempTable>();
			RetentionTransBuyerTable = new HashSet<RetentionTrans>();
			VerificationTableBuyerTable = new HashSet<VerificationTable>();
			WithdrawalTableBuyerTable = new HashSet<WithdrawalTable>();
		}
 
		[Key]
		public Guid BuyerTableGUID { get; set; }
		[StringLength(100)]
		public string AltName { get; set; }
		public Guid? BlacklistStatusGUID { get; set; }
		public Guid BusinessSegmentGUID { get; set; }
		public Guid? BusinessSizeGUID { get; set; }
		public Guid? BusinessTypeGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string BuyerId { get; set; }
		[Required]
		[StringLength(100)]
		public string BuyerName { get; set; }
		public Guid? CreditScoringGUID { get; set; }
		public Guid CurrencyGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime? DateOfEstablish { get; set; }
		[Column(TypeName = "date")]
		public DateTime? DateOfExpiry { get; set; }
		[Column(TypeName = "date")]
		public DateTime? DateOfIssue { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		public int FixedAssets { get; set; }
		public int IdentificationType { get; set; }
		[StringLength(100)]
		public string IssuedBy { get; set; }
		public Guid? KYCSetupGUID { get; set; }
		public int Labor { get; set; }
		public Guid? LineOfBusinessGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal PaidUpCapital { get; set; }
		[StringLength(20)]
		public string PassportId { get; set; }
		[StringLength(20)]
		public string ReferenceId { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal RegisteredCapital { get; set; }
		[StringLength(13)]
		public string TaxId { get; set; }
		[StringLength(20)]
		public string WorkPermitId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("BuyerTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("BuyerTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BlacklistStatusGUID")]
		[InverseProperty("BuyerTableBlacklistStatus")]
		public BlacklistStatus BlacklistStatus { get; set; }
		[ForeignKey("BusinessSegmentGUID")]
		[InverseProperty("BuyerTableBusinessSegment")]
		public BusinessSegment BusinessSegment { get; set; }
		[ForeignKey("BusinessSizeGUID")]
		[InverseProperty("BuyerTableBusinessSize")]
		public BusinessSize BusinessSize { get; set; }
		[ForeignKey("BusinessTypeGUID")]
		[InverseProperty("BuyerTableBusinessType")]
		public BusinessType BusinessType { get; set; }
		[ForeignKey("CreditScoringGUID")]
		[InverseProperty("BuyerTableCreditScoring")]
		public CreditScoring CreditScoring { get; set; }
		[ForeignKey("CurrencyGUID")]
		[InverseProperty("BuyerTableCurrency")]
		public Currency Currency { get; set; }
		[ForeignKey("DocumentStatusGUID")]
		[InverseProperty("BuyerTableDocumentStatus")]
		public DocumentStatus DocumentStatus { get; set; }
		[ForeignKey("KYCSetupGUID")]
		[InverseProperty("BuyerTableKYCSetup")]
		public KYCSetup KYCSetup { get; set; }
		[ForeignKey("LineOfBusinessGUID")]
		[InverseProperty("BuyerTableLineOfBusiness")]
		public LineOfBusiness LineOfBusiness { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("BuyerTable")]
		public ICollection<AssignmentAgreementTable> AssignmentAgreementTableBuyerTable { get; set; }
		[InverseProperty("BuyerTable")]
		public ICollection<BusinessCollateralAgmLine> BusinessCollateralAgmLineBuyerTable { get; set; }
		[InverseProperty("BuyerTable")]
		public ICollection<BuyerAgreementTable> BuyerAgreementTableBuyerTable { get; set; }
		[InverseProperty("BuyerTable")]
		public ICollection<BuyerCreditLimitByProduct> BuyerCreditLimitByProductBuyerTable { get; set; }
		[InverseProperty("BuyerTable")]
		public ICollection<BuyerReceiptTable> BuyerReceiptTableBuyerTable { get; set; }
		[InverseProperty("BuyerTable")]
		public ICollection<CAReqAssignmentOutstanding> CAReqAssignmentOutstandingBuyerTable { get; set; }
		[InverseProperty("BuyerTable")]
		public ICollection<CAReqBuyerCreditOutstanding> CAReqBuyerCreditOutstandingBuyerTable { get; set; }
		[InverseProperty("BuyerTable")]
		public ICollection<ChequeTable> ChequeTableBuyerTable { get; set; }
		[InverseProperty("BuyerTable")]
		public ICollection<CollectionFollowUp> CollectionFollowUpBuyerTable { get; set; }
		[InverseProperty("BuyerTable")]
		public ICollection<CreditAppLine> CreditAppLineBuyerTable { get; set; }
		[InverseProperty("BuyerTable")]
		public ICollection<CreditAppReqAssignment> CreditAppReqAssignmentBuyerTable { get; set; }
		[InverseProperty("BuyerTable")]
		public ICollection<CreditAppReqBusinessCollateral> CreditAppReqBusinessCollateralBuyerTable { get; set; }
		[InverseProperty("BuyerTable")]
		public ICollection<CreditAppRequestLine> CreditAppRequestLineBuyerTable { get; set; }
		[InverseProperty("BuyerTable")]
		public ICollection<CreditAppTrans> CreditAppTransBuyerTable { get; set; }
		[InverseProperty("BuyerTable")]
		public ICollection<CustBusinessCollateral> CustBusinessCollateralBuyerTable { get; set; }
		[InverseProperty("BuyerTable")]
		public ICollection<CustTrans> CustTransBuyerTable { get; set; }
		[InverseProperty("BuyerTable")]
		public ICollection<DocumentReturnLine> DocumentReturnLineBuyerTable { get; set; }
		[InverseProperty("BuyerTable")]
		public ICollection<DocumentReturnTable> DocumentReturnTableBuyerTable { get; set; }
		[InverseProperty("BuyerTable")]
		public ICollection<InvoiceTable> InvoiceTableBuyerTable { get; set; }
		[InverseProperty("BuyerTable")]
		public ICollection<MainAgreementTable> MainAgreementTableBuyerTable { get; set; }
		[InverseProperty("BuyerTable")]
		public ICollection<MessengerJobTable> MessengerJobTableBuyerTable { get; set; }
		[InverseProperty("BuyerTable")]
		public ICollection<PurchaseLine> PurchaseLineBuyerTable { get; set; }
		[InverseProperty("BuyerTable")]
		public ICollection<ReceiptTempTable> ReceiptTempTableBuyerTable { get; set; }
		[InverseProperty("BuyerTable")]
		public ICollection<RetentionTrans> RetentionTransBuyerTable { get; set; }
		[InverseProperty("BuyerTable")]
		public ICollection<VerificationTable> VerificationTableBuyerTable { get; set; }
		[InverseProperty("BuyerTable")]
		public ICollection<WithdrawalTable> WithdrawalTableBuyerTable { get; set; }
		#endregion Collection
	}
}
