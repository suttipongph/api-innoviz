using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class ParentCompany : CompanyBaseEntity
	{
		public ParentCompany()
		{
			CustomerTableParentCompany = new HashSet<CustomerTable>();
		}
 
		[Key]
		public Guid ParentCompanyGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string ParentCompanyId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("ParentCompanyCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("ParentCompanyBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("ParentCompany")]
		public ICollection<CustomerTable> CustomerTableParentCompany { get; set; }
		#endregion Collection
	}
}
