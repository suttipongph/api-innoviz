using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_RelatedPersonTable
	{
		[Key]
		[StringLength(50)]
		public string RelatedPersonTableGUID { get; set; }
		[StringLength(250)]
		public string BackgroundSummary { get; set; }
		[StringLength(100)]
		public string CompanyName { get; set; }
		public string DateOfBirth { get; set; }
		public string DateOfEstablishment { get; set; }
		public string DateOfExpiry { get; set; }
		public string DateOfIssue { get; set; }
		[StringLength(100)]
		public string Email { get; set; }
		[StringLength(100)]
		public string Extension { get; set; }
		[StringLength(100)]
		public string Fax { get; set; }
		[StringLength(50)]
		public string GenderGUID { get; set; }
		public int IdentificationType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Income { get; set; }
		[StringLength(100)]
		public string IssuedBy { get; set; }
		[StringLength(100)]
		public string LineId { get; set; }
		[StringLength(50)]
		public string MaritalStatusGUID { get; set; }
		[StringLength(100)]
		public string Mobile { get; set; }
		[Required]
		[StringLength(100)]
		public string Name { get; set; }
		[StringLength(50)]
		public string NationalityGUID { get; set; }
		[StringLength(50)]
		public string OccupationGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OtherIncome { get; set; }
		[StringLength(100)]
		public string OtherSourceIncome { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal PaidUpCapital { get; set; }
		[StringLength(20)]
		public string PassportId { get; set; }
		[StringLength(100)]
		public string Phone { get; set; }
		[StringLength(100)]
		public string Position { get; set; }
		[StringLength(50)]
		public string RaceGUID { get; set; }
		public int RecordType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal RegisteredCapital { get; set; }
		[StringLength(50)]
		public string RegistrationTypeGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string RelatedPersonId { get; set; }
		[StringLength(100)]
		public string SpouseName { get; set; }
		[StringLength(13)]
		public string TaxId { get; set; }
		public int WorkExperienceMonth { get; set; }
		public int WorkExperienceYear { get; set; }
		[StringLength(20)]
		public string WorkPermitId { get; set; }

		[Required]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		[StringLength(50)]
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		[StringLength(50)]
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		public string OwnerBusinessUnitGUID { get; set; }
		public int MigrateStatus { get; set; }
		public int MigrateSource { get; set; }
	}
}

