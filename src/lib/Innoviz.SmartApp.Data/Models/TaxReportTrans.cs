using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class TaxReportTrans : CompanyBaseEntity
	{
		public TaxReportTrans()
		{
		}
 
		[Key]
		public Guid TaxReportTransGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Amount { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public int Month { get; set; }
		public Guid? RefGUID { get; set; }
		[StringLength(20)]
		public string RefId { get; set; }
		public int RefType { get; set; }
		public int Year { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("TaxReportTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("TaxReportTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
