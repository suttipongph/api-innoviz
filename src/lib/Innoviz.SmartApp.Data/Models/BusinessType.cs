using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class BusinessType : CompanyBaseEntity
	{
		public BusinessType()
		{
			BuyerTableBusinessType = new HashSet<BuyerTable>();
			CustomerTableBusinessType = new HashSet<CustomerTable>();
		}
 
		[Key]
		public Guid BusinessTypeGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string BusinessTypeId { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("BusinessTypeCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("BusinessTypeBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("BusinessType")]
		public ICollection<BuyerTable> BuyerTableBusinessType { get; set; }
		[InverseProperty("BusinessType")]
		public ICollection<CustomerTable> CustomerTableBusinessType { get; set; }
		#endregion Collection
	}
}
