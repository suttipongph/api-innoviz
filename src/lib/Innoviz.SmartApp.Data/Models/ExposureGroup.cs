using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class ExposureGroup : CompanyBaseEntity
	{
		public ExposureGroup()
		{
			CustomerTableExposureGroup = new HashSet<CustomerTable>();
		}
 
		[Key]
		public Guid ExposureGroupGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string ExposureGroupId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("ExposureGroupCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("ExposureGroupBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("ExposureGroup")]
		public ICollection<CustomerTable> CustomerTableExposureGroup { get; set; }
		#endregion Collection
	}
}
