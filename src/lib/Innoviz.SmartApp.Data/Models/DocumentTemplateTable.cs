using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class DocumentTemplateTable : CompanyBaseEntity
	{
		public DocumentTemplateTable()
		{
			BookmarkDocumentTemplateLineDocumentTemplateTable = new HashSet<BookmarkDocumentTemplateLine>();
			BookmarkDocumentTransDocumentTemplateTable = new HashSet<BookmarkDocumentTrans>();
		}
 
		[Key]
		public Guid DocumentTemplateTableGUID { get; set; }
		[StringLength(-1)]
		public string Base64Data { get; set; }
		[StringLength(100)]
		public string Description { get; set; }
		public int DocumentTemplateType { get; set; }
		[StringLength(-1)]
		public string FileName { get; set; }
		[StringLength(-1)]
		public string FilePath { get; set; }
		[StringLength(20)]
		public string TemplateId { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("DocumentTemplateTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("DocumentTemplateTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("DocumentTemplateTable")]
		public ICollection<BookmarkDocumentTemplateLine> BookmarkDocumentTemplateLineDocumentTemplateTable { get; set; }
		[InverseProperty("DocumentTemplateTable")]
		public ICollection<BookmarkDocumentTrans> BookmarkDocumentTransDocumentTemplateTable { get; set; }
		#endregion Collection
	}
}
