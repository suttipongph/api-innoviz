﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
    public class SysMessage
    {
        [Key]
        [StringLength(100)]
        public string Code { get; set; }
        public string Message { get; set; }
    }
}