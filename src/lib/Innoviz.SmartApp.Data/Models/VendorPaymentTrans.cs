using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class VendorPaymentTrans : CompanyBaseEntity
	{
		public VendorPaymentTrans()
		{
			StagingTableVendorInfoVendorPaymentTrans = new HashSet<StagingTableVendorInfo>();
		}
 
		[Key]
		public Guid VendorPaymentTransGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal AmountBeforeTax { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public Guid? Dimension1 { get; set; }
		public Guid? Dimension2 { get; set; }
		public Guid? Dimension3 { get; set; }
		public Guid? Dimension4 { get; set; }
		public Guid? Dimension5 { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		[StringLength(20)]
		public string OffsetAccount { get; set; }
		[Column(TypeName = "date")]
		public DateTime? PaymentDate { get; set; }
		public Guid? RefGUID { get; set; }
		public int RefType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TaxAmount { get; set; }
		public Guid? TaxTableGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TotalAmount { get; set; }
		public Guid? VendorTableGUID { get; set; }
		[StringLength(20)]
		public string VendorTaxInvoiceId { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("VendorPaymentTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("VendorPaymentTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("CreditAppTableGUID")]
		[InverseProperty("VendorPaymentTransCreditAppTable")]
		public CreditAppTable CreditAppTable { get; set; }
		[ForeignKey("Dimension1")]
		[InverseProperty("VendorPaymentTransDimension1")]
		public LedgerDimension LedgerDimension1 { get; set; }
		[ForeignKey("Dimension2")]
		[InverseProperty("VendorPaymentTransDimension2")]
		public LedgerDimension LedgerDimension2 { get; set; }
		[ForeignKey("Dimension3")]
		[InverseProperty("VendorPaymentTransDimension3")]
		public LedgerDimension LedgerDimension3 { get; set; }
		[ForeignKey("Dimension4")]
		[InverseProperty("VendorPaymentTransDimension4")]
		public LedgerDimension LedgerDimension4 { get; set; }
		[ForeignKey("Dimension5")]
		[InverseProperty("VendorPaymentTransDimension5")]
		public LedgerDimension LedgerDimension5 { get; set; }
		[ForeignKey("DocumentStatusGUID")]
		[InverseProperty("VendorPaymentTransDocumentStatus")]
		public DocumentStatus DocumentStatus { get; set; }
		[ForeignKey("TaxTableGUID")]
		[InverseProperty("VendorPaymentTransTaxTable")]
		public TaxTable TaxTable { get; set; }
		[ForeignKey("VendorTableGUID")]
		[InverseProperty("VendorPaymentTransVendorTable")]
		public VendorTable VendorTable { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("VendorPaymentTrans")]
		public ICollection<StagingTableVendorInfo> StagingTableVendorInfoVendorPaymentTrans { get; set; }
		#endregion Collection
	}
}
