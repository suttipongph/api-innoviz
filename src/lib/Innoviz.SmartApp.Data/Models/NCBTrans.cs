using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class NCBTrans : CompanyBaseEntity
	{
		public NCBTrans()
		{
		}
 
		[Key]
		public Guid NCBTransGUID { get; set; }
		public Guid BankGroupGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal CreditLimit { get; set; }
		public Guid CreditTypeGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime EndDate { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal MonthlyRepayment { get; set; }
		public Guid NCBAccountStatusGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OutstandingAR { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("NCBTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("NCBTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("BankGroupGUID")]
		[InverseProperty("NCBTransBankGroup")]
		public BankGroup BankGroup { get; set; }
		[ForeignKey("CreditTypeGUID")]
		[InverseProperty("NCBTransCreditType")]
		public CreditType CreditType { get; set; }
		[ForeignKey("NCBAccountStatusGUID")]
		[InverseProperty("NCBTransNCBAccountStatus")]
		public NCBAccountStatus NCBAccountStatus { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
