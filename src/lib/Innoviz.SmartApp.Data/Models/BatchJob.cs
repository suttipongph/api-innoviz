﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
    public partial class BatchJob : CompanyBaseEntity
    {
        public BatchJob() {
            BatchInstanceHistory = new HashSet<BatchInstanceHistory>();
            BatchTrigger = new HashSet<BatchTrigger>();
        }

        [Key]
        public Guid JobId { get; set; }
        [StringLength(25)]
        public string GroupName { get; set; }
        public string ControllerUrl { get; set; }
        public string JobData { get; set; }
        [StringLength(100)]
        public string Description { get; set; }
        public int? JobStatus { get; set; }
        public override Guid CompanyGUID { get; set; }

        [InverseProperty("Job")]
        public ICollection<BatchInstanceHistory> BatchInstanceHistory { get; set; }
        [InverseProperty("Job")]
        public ICollection<BatchTrigger> BatchTrigger { get; set; }
    }
}
