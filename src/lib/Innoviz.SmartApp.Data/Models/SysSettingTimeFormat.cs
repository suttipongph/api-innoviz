﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
    public partial class SysSettingTimeFormat : BaseEntity {
        public SysSettingTimeFormat()
        {
            SysUserTable = new HashSet<SysUserTable>();
        }
        
        [Key]
        [StringLength(20)]
        public string TimeFormatId { get; set; }
        [StringLength(100)]
        public string TimeMapping { get; set; }

        [InverseProperty("TimeFormat")]
        public ICollection<SysUserTable> SysUserTable { get; set; }
    }
}
