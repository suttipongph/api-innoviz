﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
    public partial class SysFeatureGroupController : BaseEntity {

        public int AccessRight { get; set; }
        [Key]
        public Guid SysFeatureGroupControllerGUID { get; set; }
        public Guid SysControllerTableGUID { get; set; }
        public Guid SysFeatureGroupGUID { get; set; }
        public int SiteLoginType { get; set; }

        [ForeignKey("SysControllerTableGUID")]
        [InverseProperty("SysFeatureGroupController")]
        public SysControllerTable SysControllerTableGU { get; set; }
        [ForeignKey("SysFeatureGroupGUID")]
        [InverseProperty("SysFeatureGroupController")]
        public SysFeatureGroup SysFeatureGroupGU { get; set; }
    }
}
