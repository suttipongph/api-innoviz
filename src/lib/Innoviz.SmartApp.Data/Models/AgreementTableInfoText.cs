using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class AgreementTableInfoText : CompanyBaseEntity
	{
		public AgreementTableInfoText()
		{
		}
 
		[Key]
		public Guid AgreementTableInfoTextGUID { get; set; }
		public Guid AgreementTableInfoGUID { get; set; }
		[StringLength(1000)]
		public string Text1 { get; set; }
		[StringLength(1000)]
		public string Text2 { get; set; }
		[StringLength(1000)]
		public string Text3 { get; set; }
		[StringLength(1000)]
		public string Text4 { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("AgreementTableInfoTextCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("AgreementTableInfoTextBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
