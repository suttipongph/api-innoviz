using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_ProjectReferenceTrans
	{

		[Key]
		[StringLength(50)]
		public string ProjectReferenceTransGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string ProjectCompanyName { get; set; }
		public int ProjectCompletion { get; set; }
		[Required]
		[StringLength(100)]
		public string ProjectName { get; set; }
		[StringLength(100)]
		public string ProjectStatus { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ProjectValue { get; set; }
		[StringLength(50)]
		public string RefGUID { get; set; }
		public int RefType { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }

		public string MigrateStatus { get; set; }
		public string MigrateSource { get; set; }

		[Required]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		[StringLength(50)]
		public string OwnerBusinessUnitGUID { get; set; }
	}
}


