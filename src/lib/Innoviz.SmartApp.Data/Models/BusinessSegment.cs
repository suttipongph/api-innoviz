using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class BusinessSegment : CompanyBaseEntity
	{
		public BusinessSegment()
		{
			BuyerTableBusinessSegment = new HashSet<BuyerTable>();
			CustomerTableBusinessSegment = new HashSet<CustomerTable>();
		}
 
		[Key]
		public Guid BusinessSegmentGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string BusinessSegmentId { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		public int BusinessSegmentType { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("BusinessSegmentCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("BusinessSegmentBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("BusinessSegment")]
		public ICollection<BuyerTable> BuyerTableBusinessSegment { get; set; }
		[InverseProperty("BusinessSegment")]
		public ICollection<CustomerTable> CustomerTableBusinessSegment { get; set; }
		#endregion Collection
	}
}
