﻿using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class Staging_InvoiceTable
	{
		[Key]	
		[Column(Order = 1)]
		[StringLength(50)]
		public string InvoiceTableGUID { get; set; }
		public int AccountingPeriod { get; set; }
		[StringLength(50)]
		public string BuyerAgreementTableGUID { get; set; }
		[StringLength(50)]
		public string BuyerInvoiceTableGUID { get; set; }
		[StringLength(50)]
		public string BuyerTableGUID { get; set; }
		[StringLength(50)]
		public string CNReasonGUID { get; set; }
		[StringLength(50)]
		public string CreditAppTableGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string CurrencyGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string CustomerName { get; set; }
		[Required]
		[StringLength(50)]
		public string CustomerTableGUID { get; set; }
		[StringLength(50)]
		public string Dimension1GUID { get; set; }
		[StringLength(50)]
		public string Dimension2GUID { get; set; }
		[StringLength(50)]
		public string Dimension3GUID { get; set; }
		[StringLength(50)]
		public string Dimension4GUID { get; set; }
		[StringLength(50)]
		public string Dimension5GUID { get; set; }
		[StringLength(20)]
		public string DocumentId { get; set; }
		[Required]
		[StringLength(50)]
		public string DocumentStatusGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string DueDate { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ExchangeRate { get; set; }
		[StringLength(250)]
		public string InvoiceAddress1 { get; set; }
		[StringLength(250)]
		public string InvoiceAddress2 { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InvoiceAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InvoiceAmountBeforeTax { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InvoiceAmountBeforeTaxMST { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InvoiceAmountMST { get; set; }
		[Key]
		[Column(Order = 2)]
		[StringLength(20)]
		public string InvoiceId { get; set; }
		[Required]
		[StringLength(50)]
		public string InvoiceTypeGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string IssuedDate { get; set; }
		[StringLength(250)]
		public string MailingInvoiceAddress1 { get; set; }
		[StringLength(250)]
		public string MailingInvoiceAddress2 { get; set; }
		public int MarketingPeriod { get; set; }
		[StringLength(50)]
		public string MethodOfPaymentGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OrigInvoiceAmount { get; set; }
		[StringLength(20)]
		public string OrigInvoiceId { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OrigTaxInvoiceAmount { get; set; }
		[StringLength(20)]
		public string OrigTaxInvoiceId { get; set; }
		public bool ProductInvoice { get; set; }
		public int ProductType { get; set; }
		[StringLength(50)]
		public string ReceiptTempTableGUID { get; set; }
		[Required]
		[StringLength(50)]
		public string RefGUID { get; set; }
		[StringLength(50)]
		public string RefInvoiceGUID { get; set; }
		[StringLength(50)]
		public string RefTaxInvoiceGUID { get; set; }
		public int RefType { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		public int SuspenseInvoiceType { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TaxAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal TaxAmountMST { get; set; }
		[StringLength(5)]
		public string TaxBranchId { get; set; }
		[StringLength(100)]
		public string TaxBranchName { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal WHTAmount { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal WHTBaseAmount { get; set; }
		[Required]
		[StringLength(50)]
		public string BranchGUID { get; set; }
		[Key]
		[Column(Order = 3)]
		[StringLength(50)]
		public string CompanyGUID { get; set; }
		public string CreatedBy { get; set; }
		[StringLength(50)]
		public string CreatedDateTime { get; set; }
		public string ModifiedBy { get; set; }
		[StringLength(50)]
		public string ModifiedDateTime { get; set; }
		public string Owner { get; set; }
		public string OwnerBusinessUnitGUID { get; set; }
		public int MigrateStatus { get; set; }
		public int MigrateSource { get; set; }
	}
}
