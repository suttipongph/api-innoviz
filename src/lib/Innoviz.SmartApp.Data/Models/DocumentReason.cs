using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class DocumentReason : CompanyBaseEntity
	{
		public DocumentReason()
		{
			ApplicationTableDocumentReason = new HashSet<ApplicationTable>();
			AssignmentAgreementSettleDocumentReason = new HashSet<AssignmentAgreementSettle>();
			AssignmentAgreementTableDocumentReason = new HashSet<AssignmentAgreementTable>();
			BusinessCollateralAgmTableDocumentReason = new HashSet<BusinessCollateralAgmTable>();
			CreditAppRequestTableDocumentReason = new HashSet<CreditAppRequestTable>();
			CreditAppRequestTableAmendDocumentReason = new HashSet<CreditAppRequestTableAmend>();
			CreditAppTableDocumentReason = new HashSet<CreditAppTable>();
			CustomerRefundTableDocumentReason = new HashSet<CustomerRefundTable>();
			FreeTextInvoiceTableCNReason = new HashSet<FreeTextInvoiceTable>();
			GuarantorAgreementTableDocumentReason = new HashSet<GuarantorAgreementTable>();
			IntercompanyInvoiceAdjustmentDocumentReason = new HashSet<IntercompanyInvoiceAdjustment>();
			IntercompanyInvoiceTableCNReason = new HashSet<IntercompanyInvoiceTable>();
			IntercompanyInvoiceSettlementCNReason = new HashSet<IntercompanyInvoiceSettlement>();
			InvoiceTableCNReason = new HashSet<InvoiceTable>();
			MainAgreementTableDocumentReason = new HashSet<MainAgreementTable>();
			ProcessTransDocumentReason = new HashSet<ProcessTrans>();
			PurchaseTableDocumentReason = new HashSet<PurchaseTable>();
			ReceiptTempTableDocumentReason = new HashSet<ReceiptTempTable>();
			ServiceFeeTransDocumentReason = new HashSet<ServiceFeeTrans>();
			TaxInvoiceTableCNReason = new HashSet<TaxInvoiceTable>();
			WithdrawalTableDocumentReason = new HashSet<WithdrawalTable>();
		}

		[Key]
		public Guid DocumentReasonGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string ReasonId { get; set; }
		public int RefType { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("DocumentReasonCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("DocumentReasonBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("DocumentReason")]
		public ICollection<IntercompanyInvoiceAdjustment> IntercompanyInvoiceAdjustmentDocumentReason { get; set; }
		[InverseProperty("CNReason")]
		public ICollection<IntercompanyInvoiceTable> IntercompanyInvoiceTableCNReason { get; set; }
		[InverseProperty("CNReason")]
		public ICollection<IntercompanyInvoiceSettlement> IntercompanyInvoiceSettlementCNReason { get; set; }
		[InverseProperty("CNReason")]
		public ICollection<InvoiceTable> InvoiceTableCNReason { get; set; }
		[InverseProperty("CNReason")]
		public ICollection<TaxInvoiceTable> TaxInvoiceTableCNReason { get; set; }
		[InverseProperty("DocumentReason")]
		public ICollection<ApplicationTable> ApplicationTableDocumentReason { get; set; }
		[InverseProperty("DocumentReason")]
		public ICollection<AssignmentAgreementSettle> AssignmentAgreementSettleDocumentReason { get; set; }
		[InverseProperty("DocumentReason")]
		public ICollection<AssignmentAgreementTable> AssignmentAgreementTableDocumentReason { get; set; }
		[InverseProperty("DocumentReason")]
		public ICollection<BusinessCollateralAgmTable> BusinessCollateralAgmTableDocumentReason { get; set; }
		[InverseProperty("DocumentReason")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTableDocumentReason { get; set; }
		[InverseProperty("DocumentReason")]
		public ICollection<CreditAppRequestTableAmend> CreditAppRequestTableAmendDocumentReason { get; set; }
		[InverseProperty("DocumentReason")]
		public ICollection<CreditAppTable> CreditAppTableDocumentReason { get; set; }
		[InverseProperty("DocumentReason")]
		public ICollection<CustomerRefundTable> CustomerRefundTableDocumentReason { get; set; }
		[InverseProperty("CNReason")]
		public ICollection<FreeTextInvoiceTable> FreeTextInvoiceTableCNReason { get; set; }
		[InverseProperty("DocumentReason")]
		public ICollection<GuarantorAgreementTable> GuarantorAgreementTableDocumentReason { get; set; }
		[InverseProperty("DocumentReason")]
		public ICollection<MainAgreementTable> MainAgreementTableDocumentReason { get; set; }
		[InverseProperty("DocumentReason")]
		public ICollection<ProcessTrans> ProcessTransDocumentReason { get; set; }
		[InverseProperty("DocumentReason")]
		public ICollection<PurchaseTable> PurchaseTableDocumentReason { get; set; }
		[InverseProperty("DocumentReason")]
		public ICollection<ReceiptTempTable> ReceiptTempTableDocumentReason { get; set; }
		[InverseProperty("DocumentReason")]
		public ICollection<WithdrawalTable> WithdrawalTableDocumentReason { get; set; }
		[InverseProperty("DocumentReason")]
		public ICollection<ServiceFeeTrans> ServiceFeeTransDocumentReason { get; set; }
		#endregion Collection
	}
}
