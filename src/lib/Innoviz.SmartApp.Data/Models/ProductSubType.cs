using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class ProductSubType : CompanyBaseEntity
	{
		public ProductSubType()
		{
			CreditAppRequestTableProductSubType = new HashSet<CreditAppRequestTable>();
			CreditAppTableProductSubType = new HashSet<CreditAppTable>();
		}
 
		[Key]
		public Guid ProductSubTypeGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public int GuarantorAgreementYear { get; set; }
		[Column(TypeName = "decimal(5,2)")]
		public decimal MaxInterestFeePct  { get; set; }
		[Required]
		[StringLength(20)]
		public string ProductSubTypeId { get; set; }
		public int ProductType { get; set; }
		public int CalcInterestMethod { get; set; }
		public int CalcInterestDayMethod { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("ProductSubTypeCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("ProductSubTypeBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("ProductSubType")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTableProductSubType { get; set; }
		[InverseProperty("ProductSubType")]
		public ICollection<CreditAppTable> CreditAppTableProductSubType { get; set; }
		#endregion Collection
	}
}
