using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CreditLimitType : CompanyBaseEntity
	{
		public CreditLimitType()
		{
			BusinessCollateralAgmTableCreditLimitType = new HashSet<BusinessCollateralAgmTable>();
			CAReqCreditOutStandingCreditLimitType = new HashSet<CAReqCreditOutStanding>();
			CreditAppRequestTableCreditLimitType = new HashSet<CreditAppRequestTable>();
			CreditAppTableCreditLimitType = new HashSet<CreditAppTable>();
			CreditLimitTypeParentCreditLimitType = new HashSet<CreditLimitType>();
			MainAgreementTableCreditLimitType = new HashSet<MainAgreementTable>();
		}
 
		[Key]
		public Guid CreditLimitTypeGUID { get; set; }
		public bool AllowAddLine { get; set; }
		public bool BuyerMatchingAndLoanRequest { get; set; }
		public int CloseDay { get; set; }
		public int CreditLimitConditionType { get; set; }
		public int CreditLimitExpiration { get; set; }
		public int CreditLimitExpiryDay { get; set; }
		[StringLength(250)]
		public string CreditLimitRemark { get; set; }
		[Required]
		[StringLength(20)]
		public string CreditLimitTypeId { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public int InactiveDay { get; set; }
		public Guid? ParentCreditLimitTypeGUID { get; set; }
		public int ProductType { get; set; }
		public bool Revolving { get; set; }
		public bool ValidateBuyerAgreement { get; set; }
		public bool ValidateCreditLimitType { get; set; }
		public int BookmarkOrdering { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CreditLimitTypeCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CreditLimitTypeBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("ParentCreditLimitTypeGUID")]
		[InverseProperty("CreditLimitTypeParentCreditLimitType")]
		public CreditLimitType ParentCreditLimitType { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("CreditLimitType")]
		public ICollection<BusinessCollateralAgmTable> BusinessCollateralAgmTableCreditLimitType { get; set; }
		[InverseProperty("CreditLimitType")]
		public ICollection<CAReqCreditOutStanding> CAReqCreditOutStandingCreditLimitType { get; set; }
		[InverseProperty("CreditLimitType")]
		public ICollection<CreditAppRequestTable> CreditAppRequestTableCreditLimitType { get; set; }
		[InverseProperty("CreditLimitType")]
		public ICollection<CreditAppTable> CreditAppTableCreditLimitType { get; set; }
		[InverseProperty("CreditLimitType")]
		public ICollection<MainAgreementTable> MainAgreementTableCreditLimitType { get; set; }
		[InverseProperty("ParentCreditLimitType")]
		public ICollection<CreditLimitType> CreditLimitTypeParentCreditLimitType { get; set; }
		#endregion Collection
	}
}
