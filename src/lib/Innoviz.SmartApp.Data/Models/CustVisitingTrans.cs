using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class CustVisitingTrans : CompanyBaseEntity
	{
		public CustVisitingTrans()
		{
		}
 
		[Key]
		public Guid CustVisitingTransGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime CustVisitingDate { get; set; }
		[StringLength(250)]
		public string CustVisitingMemo { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public Guid? RefGUID { get; set; }
		public int RefType { get; set; }
		public Guid ResponsibleByGUID { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("CustVisitingTransCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("CustVisitingTransBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("ResponsibleByGUID")]
		[InverseProperty("CustVisitingTransResponsibleBy")]
		public EmployeeTable ResponsibleBy { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
