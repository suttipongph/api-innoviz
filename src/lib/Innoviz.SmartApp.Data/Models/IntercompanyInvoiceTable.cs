using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Data.Models
{
	public partial class IntercompanyInvoiceTable : CompanyBaseEntity {
		public IntercompanyInvoiceTable()
		{
			IntercompanyInvoiceAdjustmentIntercompanyInvoiceTable = new HashSet<IntercompanyInvoiceAdjustment>();
			IntercompanyInvoiceSettlementIntercompanyInvoiceTable = new HashSet<IntercompanyInvoiceSettlement>();
		}

		[Key]
		public Guid IntercompanyInvoiceTableGUID { get; set; }
		public Guid? CNReasonGUID { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public Guid CurrencyGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string CustomerName { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		[Required]
		[StringLength(20)]
		public string DocumentId { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime DueDate { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ExchangeRate { get; set; }
		public Guid IntercompanyGUID { get; set; }
		[Required]
		[StringLength(20)]
		public string IntercompanyInvoiceId { get; set; }
		public Guid InvoiceAddressTransGUID { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal InvoiceAmount { get; set; }
		public Guid InvoiceRevenueTypeGUID { get; set; }
		[StringLength(250)]
		public string InvoiceText { get; set; }
		public Guid InvoiceTypeGUID { get; set; }
		[Column(TypeName = "date")]
		public DateTime IssuedDate { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OrigInvoiceAmount { get; set; }
		[StringLength(20)]
		public string OrigInvoiceId { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal OrigTaxInvoiceAmount { get; set; }
		[StringLength(20)]
		public string OrigTaxInvoiceId { get; set; }
		public int ProductType { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		[StringLength(5)]
		public string TaxBranchId { get; set; }
		[StringLength(100)]
		public string TaxBranchName { get; set; }
		public Guid? TaxTableGUID { get; set; }
		public Guid? WithholdingTaxTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string FeeLedgerAccount { get; set; }

		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("IntercompanyInvoiceTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("IntercompanyInvoiceTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("CNReasonGUID")]
		[InverseProperty("IntercompanyInvoiceTableCNReason")]
		public DocumentReason CNReason { get; set; }
		[ForeignKey("CreditAppTableGUID")]
		[InverseProperty("IntercompanyInvoiceTableCreditAppTable")]
		public CreditAppTable CreditAppTable { get; set; }
		[ForeignKey("CurrencyGUID")]
		[InverseProperty("IntercompanyInvoiceTableCurrency")]
		public Currency Currency { get; set; }
		[ForeignKey("Dimension1GUID")]
		[InverseProperty("IntercompanyInvoiceTableDimension1")]
		public LedgerDimension LedgerDimension1 { get; set; }
		[ForeignKey("Dimension2GUID")]
		[InverseProperty("IntercompanyInvoiceTableDimension2")]
		public LedgerDimension LedgerDimension2 { get; set; }
		[ForeignKey("Dimension3GUID")]
		[InverseProperty("IntercompanyInvoiceTableDimension3")]
		public LedgerDimension LedgerDimension3 { get; set; }
		[ForeignKey("Dimension4GUID")]
		[InverseProperty("IntercompanyInvoiceTableDimension4")]
		public LedgerDimension LedgerDimension4 { get; set; }
		[ForeignKey("Dimension5GUID")]
		[InverseProperty("IntercompanyInvoiceTableDimension5")]
		public LedgerDimension LedgerDimension5 { get; set; }
		[ForeignKey("DocumentStatusGUID")]
		[InverseProperty("IntercompanyInvoiceTableDocumentStatus")]
		public DocumentStatus DocumentStatus { get; set; }
		[ForeignKey("IntercompanyGUID")]
		[InverseProperty("IntercompanyInvoiceTableIntercompany")]
		public Intercompany Intercompany { get; set; }
		[ForeignKey("InvoiceRevenueTypeGUID")]
		[InverseProperty("IntercompanyInvoiceTableInvoiceRevenueType")]
		public InvoiceRevenueType InvoiceRevenueType { get; set; }
		[ForeignKey("InvoiceTypeGUID")]
		[InverseProperty("IntercompanyInvoiceTableInvoiceType")]
		public InvoiceType InvoiceType { get; set; }
		[ForeignKey("TaxTableGUID")]
		[InverseProperty("IntercompanyInvoiceTableTaxTable")]
		public TaxTable TaxTable { get; set; }
		[ForeignKey("WithholdingTaxTableGUID")]
		[InverseProperty("IntercompanyInvoiceTableWithholdingTaxTable")]
		public WithholdingTaxTable WithholdingTaxTable { get; set; }
		#endregion ForeignKey

		#region Collection
		[InverseProperty("IntercompanyInvoiceTable")]
		public ICollection<IntercompanyInvoiceAdjustment> IntercompanyInvoiceAdjustmentIntercompanyInvoiceTable { get; set; }
		[InverseProperty("IntercompanyInvoiceTable")]
		public ICollection<IntercompanyInvoiceSettlement> IntercompanyInvoiceSettlementIntercompanyInvoiceTable { get; set; }
		#endregion Collection
	}
}
