using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class VerificationLine : CompanyBaseEntity
	{
		public VerificationLine()
		{
		}
 
		[Key]
		public Guid VerificationLineGUID { get; set; }
		public bool Pass { get; set; }
		[StringLength(1000)]
		public string Remark { get; set; }
		public Guid? VendorTableGUID { get; set; }
		public Guid VerificationTableGUID { get; set; }
		public Guid VerificationTypeGUID { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("VerificationLineCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("VerificationLineBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("VendorTableGUID")]
		[InverseProperty("VerificationLineVendorTable")]
		public VendorTable VendorTable { get; set; }
		[ForeignKey("VerificationTableGUID")]
		[InverseProperty("VerificationLineVerificationTable")]
		public VerificationTable VerificationTable { get; set; }
		[ForeignKey("VerificationTypeGUID")]
		[InverseProperty("VerificationLineVerificationType")]
		public VerificationType VerificationType { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
