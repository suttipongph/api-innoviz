using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class ProjectProgressTable : CompanyBaseEntity
	{
		public ProjectProgressTable()
		{
		}
 
		[Key]
		public Guid ProjectProgressTableGUID { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		[Required]
		[StringLength(20)]
		public string ProjectProgressId { get; set; }
		[StringLength(250)]
		public string Remark { get; set; }
		[Column(TypeName = "date")]
		public DateTime TransDate { get; set; }
		public Guid WithdrawalTableGUID { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("ProjectProgressTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("ProjectProgressTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		[ForeignKey("WithdrawalTableGUID")]
		[InverseProperty("ProjectProgressTableWithdrawalTable")]
		public WithdrawalTable WithdrawalTable { get; set; }
		#endregion ForeignKey
 
		#region Collection
		#endregion Collection
	}
}
