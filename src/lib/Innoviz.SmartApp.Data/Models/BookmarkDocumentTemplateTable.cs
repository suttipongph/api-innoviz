using System;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Innoviz.SmartApp.Data.Models
{
	public partial class BookmarkDocumentTemplateTable : CompanyBaseEntity
	{
		public BookmarkDocumentTemplateTable()
		{
			BookmarkDocumentTemplateLineBookmarkDocumentTemplateTable = new HashSet<BookmarkDocumentTemplateLine>();
		}
 
		[Key]
		public Guid BookmarkDocumentTemplateTableGUID { get; set; }
		public int BookmarkDocumentRefType { get; set; }
		[Required]
		[StringLength(20)]
		public string BookmarkDocumentTemplateId { get; set; }
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
 
		#region ForeignKey
		[ForeignKey("CompanyGUID")]
		[InverseProperty("BookmarkDocumentTemplateTableCompany")]
		public Company Company { get; set; }
		[ForeignKey("OwnerBusinessUnitGUID")]
		[InverseProperty("BookmarkDocumentTemplateTableBusinessUnit")]
		public BusinessUnit BusinessUnit { get; set; }
		#endregion ForeignKey
 
		#region Collection
		[InverseProperty("BookmarkDocumentTemplateTable")]
		public ICollection<BookmarkDocumentTemplateLine> BookmarkDocumentTemplateLineBookmarkDocumentTemplateTable { get; set; }
		#endregion Collection
	}
}
