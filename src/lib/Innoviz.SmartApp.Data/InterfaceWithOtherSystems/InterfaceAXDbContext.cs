﻿using Innoviz.SmartApp.Data.InterfaceWithOtherSystems.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.InterfaceWithOtherSystems
{
    public class InterfaceAXDbContext : DbContext
    {
        private readonly string stagingGLTableName;
        private readonly string stagingAPTableName;
        private readonly string stagingARTableName;
        public InterfaceAXDbContext(DbContextOptions<InterfaceAXDbContext> options, IConfiguration configuration)
            : base(options)
        {
            stagingGLTableName = configuration["InterfaceWithAX:TableNames:StagingGL"];
            stagingAPTableName = configuration["InterfaceWithAX:TableNames:StagingAP"];
            stagingARTableName = configuration["InterfaceWithAX:TableNames:StagingAR"];
        }
        public virtual DbSet<InterfaceAXStagingGL> InterfaceAXStagingGL { get; set; }
        public virtual DbSet<InterfaceAXStagingAP> InterfaceAXStagingAP { get; set; }
        public virtual DbSet<InterfaceAXStagingAR> InterfaceAXStagingAR { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<InterfaceAXStagingGL>(entity => {
                entity.Property(e => e.RecId).ValueGeneratedNever();
                entity.ToTable(stagingGLTableName);
            });

            modelBuilder.Entity<InterfaceAXStagingAP>(entity =>
            {
                entity.Property(e => e.RecId).ValueGeneratedNever();
                entity.ToTable(stagingAPTableName);
            });

            modelBuilder.Entity<InterfaceAXStagingAR>(entity =>
            {
                entity.Property(e => e.RecId).ValueGeneratedNever();
                entity.ToTable(stagingARTableName);
            });
            base.OnModelCreating(modelBuilder);
        }
    }
}
