﻿using EFCore.BulkExtensions;
using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.InterfaceWithOtherSystems.Models;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.InterfaceWithOtherSystems.Services
{
    public interface IInterfaceStagingService
    {
        bool InterfaceAccountingStaging(InterfaceAccountingStagingView parm);
        bool InterfaceVendorInvoice(InterfaceVendorInvoiceView parm);
        bool InterfaceIntercoInvSettlement(InterfaceVendorInvoiceView parm);
    }
    public class InterfaceStagingService : SmartAppService, IInterfaceStagingService
    {
        private InterfaceAXDbContext axDbContext;
        private UnitOfWork UnitOfWorkAX;
        public InterfaceStagingService(List<DbContext> contexts, ISysTransactionLogService transactionLogService)
            : base(contexts, transactionLogService)
        {
            axDbContext = (InterfaceAXDbContext)contexts
                        .Where(item => item.GetType() == (typeof(InterfaceAXDbContext)))
                        .FirstOrDefault();
            UnitOfWorkAX = new UnitOfWork(axDbContext);
        }
        public InterfaceStagingService(List<DbContext> contexts, ISysTransactionLogService transactionLogService, IBatchLogService batchLogService) 
            : base(contexts, transactionLogService, batchLogService)
        {
            axDbContext = (InterfaceAXDbContext)contexts
                        .Where(item => item.GetType() == (typeof(InterfaceAXDbContext)))
                        .FirstOrDefault();
            UnitOfWorkAX = new UnitOfWork(axDbContext);
        }

        public bool InterfaceAccountingStaging(InterfaceAccountingStagingView parm)
        {
            try
            {
                IStagingTableRepo stagingTableRepo = new StagingTableRepo(db);
                var stagingTableData = stagingTableRepo.GetInterfaceAccountingStagingData(parm.ProcessTransType, parm.InterfaceStatus);
                
                string interfaceStagingBatchId = SmartAppUtil.GenIDByDateTime(DateTime.Now);

                #region select data to interface
                var interfaceData = stagingTableData
                    .Select((stagingTable, i) => new InterfaceAXStagingGL
                    {
                        CompanyId = SmartAppUtil.HandleInterfaceStringValue(stagingTable.CompanyId, 4),
                        DataAreaId = SmartAppUtil.HandleInterfaceStringValue(stagingTable.CompanyId, 4),
                        AccountNum = SmartAppUtil.HandleInterfaceStringValue(stagingTable.AccountNum, 30),
                        AccountType = GetAXAccountTypeValue(stagingTable.AccountType),
                        Amount = stagingTable.AmountMST,
                        CompanyTaxBranchId = SmartAppUtil.HandleInterfaceStringValue(stagingTable.CompanyTaxBranchId, 30),
                        Description = SmartAppUtil.HandleInterfaceStringValue(stagingTable.TransText, 60),
                        DimensionCode1 = SmartAppUtil.HandleInterfaceStringValue(stagingTable.DimensionCode1, 30),
                        DimensionCode2 = SmartAppUtil.HandleInterfaceStringValue(stagingTable.DimensionCode2, 30),
                        DimensionCode3 = SmartAppUtil.HandleInterfaceStringValue(stagingTable.DimensionCode3, 30),
                        DimensionCode4 = SmartAppUtil.HandleInterfaceStringValue(stagingTable.DimensionCode4, 30),
                        DimensionCode5 = SmartAppUtil.HandleInterfaceStringValue(stagingTable.DimensionCode5, 30),
                        InterfaceStagingBatchId = SmartAppUtil.HandleInterfaceStringValue(interfaceStagingBatchId, 255),
                        InvoiceNumber = SmartAppUtil.HandleInterfaceStringValue(stagingTable.TaxInvoiceId, 20),
                        RefAgreementTransId = stagingTable.RefGUID,
                        TaxAccountName = SmartAppUtil.HandleInterfaceStringValue(stagingTable.TaxAccountName, 100),
                        TaxBaseAmount = stagingTable.TaxBaseAmount,
                        TaxBranchId = SmartAppUtil.HandleInterfaceStringValue(stagingTable.TaxBranchId, 15),
                        TaxCode = SmartAppUtil.HandleInterfaceStringValue(stagingTable.TaxCode, 10),
                        TaxId = SmartAppUtil.HandleInterfaceStringValue(stagingTable.TaxId, 25),
                        TaxInvoiceNumber = SmartAppUtil.HandleInterfaceStringValue(stagingTable.TaxInvoiceId, 20),
                        TransDate = stagingTable.TransDate,
                        TransType = stagingTable.ProcessTransType,
                        RecId = SmartAppUtil.GetRecIdStagingTable(i)
                    }).ToList();
                #endregion
                if (interfaceData.Any())
                {
                    try
                    {
                        // insert to AX
                        using (var transaction = UnitOfWorkAX.ContextTransaction())
                        {
                            this.BulkInsert(axDbContext, interfaceData, false, false, new BulkConfig { UseTempDB = true });
                            UnitOfWorkAX.Commit(transaction);
                        }
                    }
                    catch (Exception e)
                    {
                        // fail
                        stagingTableData = UpdateStagingTableInterfaceStatus(stagingTableData, InterfaceStatus.Fail, interfaceStagingBatchId);
                        
                        SmartAppException ex = new SmartAppException("ERROR.ERROR");
                        ex.AddData("ERROR.90143", "LABEL.INTERFACE_ACCOUNTING_STAGING");
                        throw SmartAppUtil.AddStackTrace(e, ex);
                    }
                    // success
                    stagingTableData = UpdateStagingTableInterfaceStatus(stagingTableData, InterfaceStatus.Success, interfaceStagingBatchId);
                    
                    LogBatchSuccess(null, null, "SUCCESS.90020", new string[] { "LABEL.INTERFACE_ACCOUNTING_STAGING" }, null, null);
                }
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool InterfaceVendorInvoice(InterfaceVendorInvoiceView parm)
        {
            try
            {
                IStagingTableRepo stagingTableRepo = new StagingTableRepo(db);
                IStagingTableVendorInfoRepo stagingVendorTableInfoRepo = new StagingTableVendorInfoRepo(db);
                var stagingTableData = stagingTableRepo.GetInterfaceVendorInvoiceStagingData(parm.ProcessTransType, parm.InterfaceStatus, parm.ListRefGUID);
                var stagingTableVendorInfoData =
                    stagingVendorTableInfoRepo.GetStagingTableVendorInfoByProcessTransGUIDNoTracking(
                            stagingTableData.Where(w => w.ProcessTransGUID.HasValue).Select(s => s.ProcessTransGUID.Value));

                string interfaceStagingBatchId = SmartAppUtil.GenIDByDateTime(DateTime.Now);

                #region select data to interface
                var interfaceData =
                    (from stagingTable in stagingTableData
                     join stagingTableVendorInfo in stagingTableVendorInfoData
                     on stagingTable.ProcessTransGUID equals stagingTableVendorInfo.ProcessTransGUID
                     select new InterfaceAXStagingAP
                     {
                         CompanyId = SmartAppUtil.HandleInterfaceStringValue(stagingTable.CompanyId, 4),
                         DataAreaId = SmartAppUtil.HandleInterfaceStringValue(stagingTable.CompanyId, 4),
                         VendRecordType = stagingTableVendorInfo.RecordType,
                         VendAccount = SmartAppUtil.HandleInterfaceStringValue(stagingTableVendorInfo.VendorId, 20),
                         VendName = SmartAppUtil.HandleInterfaceStringValue(stagingTableVendorInfo.Name, 100),
                         VendNameAlias = SmartAppUtil.HandleInterfaceStringValue(stagingTableVendorInfo.AltName, 20),
                         VendCurrency = SmartAppUtil.HandleInterfaceStringValue(stagingTableVendorInfo.CurrencyId, 3),
                         VendGroup = SmartAppUtil.HandleInterfaceStringValue(stagingTableVendorInfo.VendGroupId, 10),
                         VendTaxExemptNum = SmartAppUtil.HandleInterfaceStringValue(stagingTableVendorInfo.VendorTaxId, 20),
                         VendBankAccName = SmartAppUtil.HandleInterfaceStringValue(stagingTableVendorInfo.BankAccountName, 60),
                         VendBankAccNum = SmartAppUtil.HandleInterfaceStringValue(stagingTableVendorInfo.BankAccount, 35),
                         VendBankGroups = SmartAppUtil.HandleInterfaceStringValue(stagingTableVendorInfo.BankGroupId, 10),
                         VendBankRoutingNum = SmartAppUtil.HandleInterfaceStringValue(stagingTableVendorInfo.BankBranch, 12),
                         VendAddressCountry = SmartAppUtil.HandleInterfaceStringValue(stagingTableVendorInfo.CountryId, 10),
                         VendAddressPostalCode = SmartAppUtil.HandleInterfaceStringValue(stagingTableVendorInfo.PostalCode, 10),
                         VendAddressStreet = SmartAppUtil.HandleInterfaceStringValue(stagingTableVendorInfo.Address, 250),
                         VendAddressCity = SmartAppUtil.HandleInterfaceStringValue(stagingTableVendorInfo.DistrictId, 60),
                         VendAddressDistrict = SmartAppUtil.HandleInterfaceStringValue(stagingTableVendorInfo.SubDistrictId, 60),
                         VendAddressState = SmartAppUtil.HandleInterfaceStringValue(stagingTableVendorInfo.ProvinceId, 10),
                         VendTaxInfoBranchNum = SmartAppUtil.HandleInterfaceStringValue(stagingTableVendorInfo.TaxBranchId, 15),
                         AccountNum = SmartAppUtil.HandleInterfaceStringValue(stagingTable.AccountNum, 30),
                         AccountType = GetAXAccountTypeValue(stagingTable.AccountType),
                         Amount = stagingTable.AmountMST,
                         CompanyTaxBranchId = SmartAppUtil.HandleInterfaceStringValue(stagingTable.CompanyTaxBranchId, 30),
                         Description = SmartAppUtil.HandleInterfaceStringValue(stagingTable.TransText, 60),
                         DimensionCode1 = SmartAppUtil.HandleInterfaceStringValue(stagingTable.DimensionCode1, 30),
                         DimensionCode2 = SmartAppUtil.HandleInterfaceStringValue(stagingTable.DimensionCode2, 30),
                         DimensionCode3 = SmartAppUtil.HandleInterfaceStringValue(stagingTable.DimensionCode3, 30),
                         DimensionCode4 = SmartAppUtil.HandleInterfaceStringValue(stagingTable.DimensionCode4, 30),
                         DimensionCode5 = SmartAppUtil.HandleInterfaceStringValue(stagingTable.DimensionCode5, 30),
                         InterfaceStagingBatchId = SmartAppUtil.HandleInterfaceStringValue(interfaceStagingBatchId, 255),
                         InvoiceNumber = SmartAppUtil.HandleInterfaceStringValue(stagingTable.TaxInvoiceId, 20),
                         RefAgreementTransId = stagingTable.RefGUID,
                         TaxAccountName = SmartAppUtil.HandleInterfaceStringValue(stagingTable.TaxAccountName, 100),
                         TaxBaseAmount = stagingTable.TaxBaseAmount,
                         TaxBranchId = SmartAppUtil.HandleInterfaceStringValue(stagingTable.TaxBranchId, 15),
                         TaxCode = SmartAppUtil.HandleInterfaceStringValue(stagingTable.TaxCode, 10),
                         TaxId = SmartAppUtil.HandleInterfaceStringValue(stagingTable.TaxId, 25),
                         TaxInvoiceNumber = SmartAppUtil.HandleInterfaceStringValue(stagingTable.TaxInvoiceId, 20),
                         TransDate = stagingTable.TransDate,
                         TransType = stagingTable.ProcessTransType
                     });
                
                interfaceData = interfaceData.Select((s, i) =>
                {
                    s.RecId = SmartAppUtil.GetRecIdStagingTable(i);
                    return s;
                });
                #endregion

                if (interfaceData.Any())
                {
                    try
                    {
                        // insert to AX
                        using (var transaction = UnitOfWorkAX.ContextTransaction())
                        {
                            this.BulkInsert(axDbContext, interfaceData.ToList(), false, false, new BulkConfig { UseTempDB = true });
                            UnitOfWorkAX.Commit(transaction);
                        }
                    }
                    catch (Exception e)
                    {
                        // fail
                        stagingTableData = UpdateStagingTableInterfaceStatus(stagingTableData, InterfaceStatus.Fail, interfaceStagingBatchId);
                        
                        SmartAppException ex = new SmartAppException("ERROR.ERROR");
                        ex.AddData("ERROR.90143", "LABEL.INTERFACE_VENDOR_INVOICE");
                        throw SmartAppUtil.AddStackTrace(e, ex);
                    }
                    // success
                    stagingTableData = UpdateStagingTableInterfaceStatus(stagingTableData, InterfaceStatus.Success, interfaceStagingBatchId);
                    
                    LogBatchSuccess(null, null, "SUCCESS.90020", new string[] { "LABEL.INTERFACE_VENDOR_INVOICE" }, null, null);
                }
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool InterfaceIntercoInvSettlement(InterfaceVendorInvoiceView parm)
        {
            try
            {
                IStagingTableIntercoInvSettleRepo stagingTableIntercoInvSettleRepo = new StagingTableIntercoInvSettleRepo(db);
                var stagingData = stagingTableIntercoInvSettleRepo.InterfaceIntercoInvSettlementData(parm.InterfaceStatus);
                List<StagingTableIntercoInvSettle> StagingTableIntercoInvSettle = stagingTableIntercoInvSettleRepo.GetByInterfaceStatuses(parm.InterfaceStatus).ToList();
                string interfaceStagingBatchId = SmartAppUtil.GenIDByDateTime(DateTime.Now);
                if (stagingData.Any())
                {
                    using (var transaction = UnitOfWorkAX.ContextTransaction())
                    {
                        try
                        {
                            this.BulkInsert(axDbContext, stagingData, false, false);
                            UnitOfWorkAX.Commit(transaction);
                            UpdateStatusAndInterfaceStagingBatchId(StagingTableIntercoInvSettle, interfaceStagingBatchId, InterfaceStatus.Success);
                            LogBatchSuccess(null, null, "SUCCESS.90020", new string[] { "LABEL.INTERFACE_INTERCOMPANY_INVOICE_SETTLEMENT_STAGING" }, null, null);
                        }
                        catch (Exception e)
                        {
                            transaction.Rollback();
                            UpdateStatusAndInterfaceStagingBatchId(StagingTableIntercoInvSettle, interfaceStagingBatchId, InterfaceStatus.Fail);
                            SmartAppException smartAppException = new SmartAppException("EROR.ERROR");
                            smartAppException.AddData("ERROR.90143", "LABEL.INTERFACE_INTERCOMPANY_INVOICE_SETTLEMENT_STAGING");
                            throw SmartAppUtil.AddStackTrace(e, smartAppException);
                        }
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private List<StagingTable> UpdateStagingTableInterfaceStatus(List<StagingTable> list, InterfaceStatus status, string interfaceStagingBatchId)
        {
            try
            {
                list = list.Select(s =>
                        {
                            s.InterfaceStagingBatchId = interfaceStagingBatchId;
                            s.InterfaceStatus = (int)status;
                            return s;
                        }).ToList();

                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    this.BulkUpdate(list);
                    UnitOfWork.Commit(transaction);
                }
                return list;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void UpdateStatusAndInterfaceStagingBatchId(List<StagingTableIntercoInvSettle> stagingTableIntercoInvSettle, string interfaceStagingBatchId, InterfaceStatus interfaceStatus)
        {
            try
            {
                stagingTableIntercoInvSettle.ForEach(f => { f.InterfaceStatus = (int)interfaceStatus; f.InterfaceStagingBatchId = interfaceStagingBatchId; });
                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    try
                    {
                        BulkUpdate(stagingTableIntercoInvSettle);
                        UnitOfWork.Commit(transaction);
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        throw SmartAppUtil.AddStackTrace(e);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static int GetAXAccountTypeValue(int accountType)
        {
            AccountType input = (AccountType)accountType;
            switch (input)
            {
                case AccountType.Ledger: return 0;
                case AccountType.Bank: return 6;
                case AccountType.Vendor: return 2;

                default: return 0;
            }
        }
    }
}
