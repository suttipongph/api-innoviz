﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Innoviz.SmartApp.Data.InterfaceWithOtherSystems.Models
{
    public class InterfaceAXStagingAR 
    {
        [Column("COMPANYID")]
        public string CompanyId { get; set; }
        [Column("DATAAREAID")]
        public string DataAreaId { get; set; }
        [Column("CUSTRECORDTYPE")]
        public int CustRecordType { get; set; }
        [Column("CUSTACCOUNT")]
        public string CustAccount { get; set; }
        [Column("CUSTNAME")]
        public string CustName { get; set; }
        [Column("CUSTNAMEALIAS")]
        public string CustNameAlias { get; set; }
        [Column("CUSTCURRENCY")]
        public string CustCurrency { get; set; }
        [Column("CUSTGROUP")]
        public string CustGroup { get; set; }
        [Column("CUSTTAXEXEMPTNUM")]
        public string CustTaxExemptNum { get; set; }
        [Column("CUSTPHONENO")]
        public string CustPhoneNo { get; set; }
        [Column("CUSTFAXNO")]
        public string CustFaxNo { get; set; }
        [Column("CUSTADDRESSCOUNTRY")]
        public string CustAddressCountry { get; set; }
        [Column("CUSTADDRESSPOSTALCODE")]
        public string CustAddressPostalCode { get; set; }
        [Column("CUSTADDRESSSTREET")]
        public string CustAddressStreet { get; set; }
        [Column("CUSTADDRESSCITY")]
        public string CustAddressCity { get; set; }
        [Column("CUSTADDRESSDISTRICT")]
        public string CustAddressDistrict { get; set; }
        [Column("CUSTADDRESSSTATE")]
        public string CustAddressState { get; set; }
        [Column("CUSTTAXINFOBRANCHNUM")]
        public string CustTaxInfoBranchNum { get; set; }
        [Column("INVOICEDATE")]
        public DateTime InvoiceDate { get; set; }
        [Column("DUEDATE")]
        public DateTime DueDate { get; set; }
        [Column("REFERENCEINVOICE")]
        public string ReferenceInvoice { get; set; }
        [Column("REFERENCEINVOICEVALUE", TypeName = "decimal(18,2)")]
        public decimal ReferenceInvoiceValue { get; set; }
        [Column("REASONCODE")]
        public string ReasonCode { get; set; }
        [Column("PAYMMODE")]
        public string PaymMode { get; set; }
        [Column("CUSTOMERREQ")]
        public string CustomerReq { get; set; }
        [Column("ACCOUNTNUM")]
        public string AccountNum { get; set; }
        [Column("TAXCODE")]
        public string TaxCode { get; set; }
        [Column("AMOUNT", TypeName = "decimal(18,2)")]
        public decimal Amount { get; set; }
        [Column("DESCRIPTION")]
        public string Description { get; set; }
        [Column("DIMENSIONCODE1")]
        public string DimensionCode1 { get; set; }
        [Column("DIMENSIONCODE2")]
        public string DimensionCode2 { get; set; }
        [Column("DIMENSIONCODE3")]
        public string DimensionCode3 { get; set; }
        [Column("DIMENSIONCODE4")]
        public string DimensionCode4 { get; set; }
        [Column("DIMENSIONCODE5")]
        public string DimensionCode5 { get; set; }
        [Key]
        [Column("RECID")]
        public Int64 RecId { get; set; }
        [Column("COMPANYTAXBRANCHID")]
        public string CompanyTaxBranchId { get; set; }
    }
}
