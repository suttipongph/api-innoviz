﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Innoviz.SmartApp.Data.InterfaceWithOtherSystems.Models
{
    public class InterfaceAXStagingAP
    {
        [Column("COMPANYID")]
        public string CompanyId { get; set; }
        [Column("DATAAREAID")]
        public string DataAreaId { get; set; }
        [Column("VENDRECORDTYPE")]
        public int VendRecordType { get; set; }
        [Column("VENDACCOUNT")]
        public string VendAccount { get; set; }
        [Column("VENDNAME")]
        public string VendName { get; set; }
        [Column("VENDNAMEALIAS")]
        public string VendNameAlias { get; set; }
        [Column("VENDCURRENCY")]
        public string VendCurrency { get; set; }
        [Column("VENDGROUP")]
        public string VendGroup { get; set; }
        [Column("VENDTAXEXEMPTNUM")]
        public string VendTaxExemptNum { get; set; }
        [Column("VENDBANKACCNAME")]
        public string VendBankAccName { get; set; }
        [Column("VENDBANKACCNUM")]
        public string VendBankAccNum { get; set; }
        [Column("VENDBANKGROUPS")]
        public string VendBankGroups { get; set; }
        [Column("VENDBANKROUTINGNUM")]
        public string VendBankRoutingNum { get; set; }
        [Column("VENDADDRESSCOUNTRY")]
        public string VendAddressCountry { get; set; }
        [Column("VENDADDRESSPOSTALCODE")]
        public string VendAddressPostalCode { get; set; }
        [Column("VENDADDRESSSTREET")]
        public string VendAddressStreet { get; set; }
        [Column("VENDADDRESSCITY")]
        public string VendAddressCity { get; set; }
        [Column("VENDADDRESSDISTRICT")]
        public string VendAddressDistrict { get; set; }
        [Column("VENDADDRESSSTATE")]
        public string VendAddressState { get; set; }
        [Column("VENDTAXINFOBRANCHNUM")]
        public string VendTaxInfoBranchNum { get; set; }
        [Column("ACCOUNTNUM")]
        public string AccountNum { get; set; }
        [Column("ACCOUNTTYPE")]
        public int AccountType { get; set; }
        [Column("AMOUNT", TypeName = "decimal(18, 2)")]
        public decimal Amount { get; set; }
        [Column("COMPANYTAXBRANCH")]
        public string CompanyTaxBranchId { get; set; }
        [Column("DESCRIPTION")]
        public string Description { get; set; }
        [Column("DIMENSIONCODE1")]
        public string DimensionCode1 { get; set; }
        [Column("DIMENSIONCODE2")]
        public string DimensionCode2 { get; set; }
        [Column("DIMENSIONCODE3")]
        public string DimensionCode3 { get; set; }
        [Column("DIMENSIONCODE4")]
        public string DimensionCode4 { get; set; }
        [Column("DIMENSIONCODE5")]
        public string DimensionCode5 { get; set; }
        [Column("INTERFACESTAGINGBATCHID")]
        public string InterfaceStagingBatchId { get; set; }
        [Column("INVOICENUMBER")]
        public string InvoiceNumber { get; set; }
        [Column("REFAGREEMENTTRANSID")]
        public Guid? RefAgreementTransId { get; set; }
        [Column("TAXACCOUNTNAME")]
        public string TaxAccountName { get; set; }
        [Column("TAXBASEAMOUNT", TypeName = "decimal(18, 2)")]
        public decimal TaxBaseAmount { get; set; }
        [Column("TAXBRANCHID")]
        public string TaxBranchId { get; set; }
        [Column("TAXCODE")]
        public string TaxCode { get; set; }
        [Column("TAXID")]
        public string TaxId { get; set; }
        [Column("TAXINVOICENUMBER")]
        public string TaxInvoiceNumber { get; set; }
        [Column("TRANSDATE", TypeName = "date")]
        public DateTime TransDate { get; set; }
        [Column("TRANSTYPE")]
        public int TransType { get; set; }
        [Key]
        [Column("RECID")]
        public Int64 RecId { get; set; }
    }
}
