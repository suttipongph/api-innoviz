﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Innoviz.SmartApp.Data.InterfaceWithOtherSystems.Models
{
    public class InterfaceAXStagingGL
    {
        [Column("COMPANYID")]
        public string CompanyId { get; set; }
        [Column("DATAAREAID")]
        public string DataAreaId { get; set; }
        [Column("ACCOUNTNUM")]
        public string AccountNum { get; set; }
        [Column("ACCOUNTTYPE")]
        public int AccountType { get; set; }
        [Column("AMOUNT", TypeName = "decimal(18, 2)")]
        public decimal Amount { get; set; }
        [Column("COMPANYTAXBRANCH")]
        public string CompanyTaxBranchId { get; set; }
        [Column("DESCRIPTION")]
        public string Description { get; set; }
        [Column("DIMENSIONCODE1")]
        public string DimensionCode1 { get; set; }
        [Column("DIMENSIONCODE2")]
        public string DimensionCode2 { get; set; }
        [Column("DIMENSIONCODE3")]
        public string DimensionCode3 { get; set; }
        [Column("DIMENSIONCODE4")]
        public string DimensionCode4 { get; set; }
        [Column("DIMENSIONCODE5")]
        public string DimensionCode5 { get; set; }
        [Column("INTERFACESTAGINGBATCHID")]
        public string InterfaceStagingBatchId { get; set; }
        [Column("INVOICENUMBER")]
        public string InvoiceNumber { get; set; }
        [Column("REFAGREEMENTTRANSID")]
        public Guid? RefAgreementTransId { get; set; }
        [Column("TAXACCOUNTNAME")]
        public string TaxAccountName { get; set; }
        [Column("TAXBASEAMOUNT", TypeName = "decimal(18, 2)")]
        public decimal TaxBaseAmount { get; set; }
        [Column("TAXBRANCHID")]
        public string TaxBranchId { get; set; }
        [Column("TAXCODE")]
        public string TaxCode { get; set; }
        [Column("TAXID")]
        public string TaxId { get; set; }
        [Column("TAXINVOICENUMBER")]
        public string TaxInvoiceNumber { get; set; }
        [Column("TRANSDATE", TypeName = "date")]
        public DateTime TransDate { get; set; }
        [Column("TRANSTYPE")]
        public int TransType { get; set; }
        [Key]
        [Column("RECID")]
        public Int64 RecId { get; set; }
    }
}
