﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Data.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Hosting;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data.Services;

namespace Innoviz.SmartApp.Data
{
    public partial class SmartAppContext : SmartAppDbContext
    {

        public virtual DbSet<Branch> Branch { get; set; }

        public virtual DbSet<Company> Company { get; set; }
        public virtual DbSet<SysControllerRole> SysControllerRole { get; set; }
        public virtual DbSet<SysControllerTable> SysControllerTable { get; set; }
        public virtual DbSet<SysDuplicateDetection> SysDuplicateDetection { get; set; }
        public virtual DbSet<SysEnumTable> SysEnumTable { get; set; }
        public virtual DbSet<SysFeatureTable> SysFeatureTable { get; set; }
        public virtual DbSet<SysLabelReport> SysLabelReport { get; set; }
        public virtual DbSet<SysLabelReport_CUSTOM> SysLabelReport_CUSTOM { get; set; }
        public virtual DbSet<SysMessage> SysMessage { get; set; }
        public virtual DbSet<SysNavigation> SysNavigation { get; set; }
        public virtual DbSet<SysRoleTable> SysRoleTable { get; set; }
        public virtual DbSet<SysScopeRole> SysScopeRole { get; set; }
        public virtual DbSet<SysSettingDateFormat> SysSettingDateFormat { get; set; }
        public virtual DbSet<SysSettingNumberFormat> SysSettingNumberFormat { get; set; }
        public virtual DbSet<SysSettingTimeFormat> SysSettingTimeFormat { get; set; }
        public virtual DbSet<SysUserCompanyMapping> SysUserCompanyMapping { get; set; }
        public virtual DbSet<SysUserRoles> SysUserRoles { get; set; }
        public virtual DbSet<SysUserTable> SysUserTable { get; set; }
        public DbSet<SysFeatureGroup> SysFeatureGroup { get; set; }
        public DbSet<SysFeatureGroupMapping> SysFeatureGroupMapping { get; set; }
        public DbSet<SysFeatureGroupController> SysFeatureGroupController { get; set; }
        public DbSet<SysFeatureGroupRole> SysFeatureGroupRole { get; set; }
        public DbSet<SysFeatureGroupStructure> SysFeatureGroupStructure { get; set; }
        public DbSet<SysControllerEntityMapping> SysControllerEntityMapping { get; set; }
        public DbSet<SysEntityRole> SysEntityRole { get; set; }
        public DbSet<SysDataInitializationHistory> SysDataInitializationHistory { get; set; }

        private SystemParameter systemParameter;
        private DbContextOptions<SmartAppContext> _options;

        public SmartAppContext(DbContextOptions<SmartAppContext> options,
                                IConfiguration configuration)
            : base(options)
        {
            _options = options;
        }

        public SmartAppContext(DbContextOptions<SmartAppContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.HasAnnotation("ProductVersion", "2.2.0-rtm-35687");
            modelBuilder.Entity<Branch>(entity => { entity.HasIndex(e => e.CompanyGUID); entity.Property(e => e.BranchGUID).ValueGeneratedNever(); });
            modelBuilder.Entity<BusinessUnit>(entity =>
            {
                entity.Property(e => e.BusinessUnitGUID).ValueGeneratedNever();

                entity.HasOne(o => o.ParentBusinessUnit)
                        .WithMany(m => m.ChildrenBusinessUnits)
                        .HasForeignKey(f => f.ParentBusinessUnitGUID)
                        .OnDelete(DeleteBehavior.Restrict)
                        .HasConstraintName("FK_ParentBusinessUnitBusinessUnit");

            });

            modelBuilder.Entity<Company>(entity => { entity.HasIndex(e => e.CompanyGUID); entity.Property(e => e.CompanyGUID).ValueGeneratedNever(); });
            modelBuilder.Entity<SysControllerRole>(entity =>
            {
                entity.HasIndex(e => e.SysControllerTableGUID);

                entity.Property(e => e.SysControllerRoleGUID).ValueGeneratedNever();

                entity.HasOne(d => d.SysControllerTableGU)
                    .WithMany(p => p.SysControllerRole)
                    .HasForeignKey(d => d.SysControllerTableGUID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SysControllerRoleSysControllerTable");
            });
            modelBuilder.Entity<SysControllerTable>(entity =>
            {
                entity.HasIndex(e => new { e.ControllerName, e.RouteAttribute })
                    .HasName("IX_SysControllerTable_keys");

                entity.Property(e => e.SysControllerTableGUID).ValueGeneratedNever();
            });
            modelBuilder.Entity<SysDuplicateDetection>(entity =>
            {
                entity.HasKey(e => new { e.ModelName, e.RuleNum, e.PropertyName });
            });
            modelBuilder.Entity<SysEnumTable>(entity =>
            {
                entity.HasKey(e => new { e.EnumName, e.AttributeValue });
            });
            

            modelBuilder.Entity<SysFeatureTable>(entity =>
            {
                entity.Property(e => e.SysFeatureTableGUID).ValueGeneratedNever();
            });
            modelBuilder.Entity<SysLabelReport>(entity =>
            {
                entity.Property(e => e.SysLabelReportGUID).ValueGeneratedNever();
            });
            modelBuilder.Entity<SysLabelReport_CUSTOM>(entity =>
            {
                entity.Property(e => e.SysLabelReport_CUSTOMGUID).ValueGeneratedNever();
            });
            modelBuilder.Entity<SysMessage>(entity =>
            {
                entity.Property(e => e.Code).ValueGeneratedNever();
            });
            modelBuilder.Entity<SysNavigation>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });
            modelBuilder.Entity<SysRoleTable>(entity =>
            {
                entity.HasIndex(e => e.NormalizedName)
                    .HasName("RoleNameIndex")
                    .IsUnique()
                    .HasFilter("([NormalizedName] IS NOT NULL)");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });
            modelBuilder.Entity<SysScopeRole>(entity =>
            {
                entity.HasKey(e => new { e.ClientId, e.RoleGUID, e.ScopeName })
                    .HasName("PK_SysScopeRole");

            });
            modelBuilder.Entity<SysSettingDateFormat>(entity =>
            {
                entity.Property(e => e.DateFormatId).ValueGeneratedNever();
            });
            modelBuilder.Entity<SysSettingNumberFormat>(entity =>
            {
                entity.Property(e => e.NumberFormatId).ValueGeneratedNever();
            });
            modelBuilder.Entity<SysSettingTimeFormat>(entity =>
            {
                entity.Property(e => e.TimeFormatId).ValueGeneratedNever();
            });
            modelBuilder.Entity<SysUserCompanyMapping>(entity =>
            {
                entity.HasKey(e => new { e.UserGUID, e.CompanyGUID, e.BranchGUID });

                entity.HasIndex(e => e.BranchGUID);

                entity.HasIndex(e => e.CompanyGUID);

                entity.HasIndex(e => e.UserGUID);

                entity.HasOne(d => d.BranchGU)
                    .WithMany(p => p.SysUserCompanyMapping)
                    .HasForeignKey(d => d.BranchGUID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SysUserCompanyMappingBranch");

                entity.HasOne(d => d.CompanyGU)
                    .WithMany(p => p.SysUserCompanyMapping)
                    .HasForeignKey(d => d.CompanyGUID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SysUserCompanyMappingCompany");

                entity.HasOne(d => d.UserGU)
                    .WithMany(p => p.SysUserCompanyMapping)
                    .HasForeignKey(d => d.UserGUID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SysUserCompanyMappingSysUserTable");
            });
            modelBuilder.Entity<SysUserRoles>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId });

                entity.HasIndex(e => e.RoleId);
            });
            modelBuilder.Entity<SysUserTable>(entity =>
            {
                entity.HasIndex(e => e.DateFormatId);

                entity.HasIndex(e => e.NumberFormatId);

                entity.HasIndex(e => e.TimeFormatId);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.DateFormat)
                    .WithMany(p => p.SysUserTable)
                    .HasForeignKey(d => d.DateFormatId)
                    .HasConstraintName("FK_SysUserTableSysSettingDateFormat");

                entity.HasOne(d => d.DefaultBranchGU)
                    .WithMany(p => p.SysUserTable)
                    .HasForeignKey(d => d.DefaultBranchGUID)
                    .HasConstraintName("FK_SysUserTableBranch");

                entity.HasOne(d => d.DefaultCompanyGU)
                    .WithMany(p => p.SysUserTable)
                    .HasForeignKey(d => d.DefaultCompanyGUID)
                    .HasConstraintName("FK_SysUserTableCompany");

                entity.HasOne(d => d.NumberFormat)
                    .WithMany(p => p.SysUserTable)
                    .HasForeignKey(d => d.NumberFormatId)
                    .HasConstraintName("FK_SysUserTableSysSettingNumberFormat");

                entity.HasOne(d => d.TimeFormat)
                    .WithMany(p => p.SysUserTable)
                    .HasForeignKey(d => d.TimeFormatId)
                    .HasConstraintName("FK_SysUserTableSysSettingTimeFormat");
            });
            modelBuilder.Entity<SysFeatureGroup>(entity =>
            {
                entity.Property(e => e.SysFeatureGroupGUID).ValueGeneratedNever();
            });
            modelBuilder.Entity<SysFeatureGroupMapping>(entity =>
            {
                entity.HasKey(e => new { e.SysFeatureTableGUID, e.SysFeatureGroupGUID });

                entity.HasIndex(e => e.SysFeatureTableGUID);

                entity.HasIndex(e => e.SysFeatureGroupGUID);

                entity.HasOne(d => d.SysFeatureTableGU)
                    .WithMany(p => p.SysFeatureGroupMapping)
                    .HasForeignKey(d => d.SysFeatureTableGUID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SysFeatureGroupMappingSysFeatureTable");

                entity.HasOne(d => d.SysFeatureGroupGU)
                    .WithMany(p => p.SysFeatureGroupMapping)
                    .HasForeignKey(d => d.SysFeatureGroupGUID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SysFeatureGroupMappingSysFeatureGroup");


            });

            modelBuilder.Entity<SysFeatureGroupController>(entity =>
            {
                entity.HasIndex(e => e.SysControllerTableGUID);

                entity.HasIndex(e => e.SysFeatureGroupGUID);

                entity.Property(e => e.SysFeatureGroupControllerGUID).ValueGeneratedNever();

                entity.HasOne(d => d.SysControllerTableGU)
                    .WithMany(p => p.SysFeatureGroupController)
                    .HasForeignKey(d => d.SysControllerTableGUID)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_SysFeatureGroupControllerSysControllerTable");

                entity.HasOne(d => d.SysFeatureGroupGU)
                    .WithMany(p => p.SysFeatureGroupController)
                    .HasForeignKey(d => d.SysFeatureGroupGUID)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_SysFeatureGroupControllerSysFeatureGroup");
            });
            modelBuilder.Entity<SysFeatureGroupRole>(entity =>
            {
                entity.HasIndex(e => e.SysFeatureGroupGUID);

                entity.Property(e => e.SysFeatureGroupRoleGUID).ValueGeneratedNever();
               
                entity.HasOne(d => d.SysFeatureGroupGU)
                    .WithMany(p => p.SysFeatureGroupRole)
                    .HasForeignKey(d => d.SysFeatureGroupGUID)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_SysFeatureGroupRoleSysFeatureGroup");
            });
            modelBuilder.Entity<SysFeatureGroupStructure>(entity =>
            {
                entity.HasKey(e => new { e.NodeId, e.SiteLoginType });
                entity.Property(e => e.NodeId).ValueGeneratedNever();
                entity.HasOne(d => d.NodeParent)
                    .WithMany(p => p.NodeChildren)
                    .HasForeignKey(d => new { d.NodeParentId, d.SiteLoginType })
                    .OnDelete(DeleteBehavior.Restrict);
            });
            modelBuilder.Entity<SysControllerEntityMapping>(entity =>
            {
                entity.HasIndex(e => e.SysControllerTableGUID);
                entity.HasIndex(e => e.ModelName);
                entity.Property(e => e.SysControllerEntityMappingGUID).ValueGeneratedNever();

                entity.HasOne(d => d.SysControllerTableGU)
                    .WithMany(p => p.SysControllerEntityMapping)
                    .HasForeignKey(d => d.SysControllerTableGUID)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_SysControllerEntityMappingSysControllerTable");
            });
            modelBuilder.Entity<SysEntityRole>(entity =>
            {
                entity.HasIndex(e => e.RoleGUID);
                entity.HasIndex(e => e.ModelName);
                entity.Property(e => e.SysEntityRoleGUID).ValueGeneratedNever();

            });
            modelBuilder.Entity<SysDataInitializationHistory>(entity =>
            {
                entity.HasKey(e => new { e.DataInitializationType, e.Version });

            });

            OnModelCreating_CUSTOM(modelBuilder);
            OnModelCreating_Phase2(modelBuilder);
        }


        public override void SetSystemParameter(SystemParameter input)
        {
            systemParameter = input;
        }
        public override SystemParameter GetSystemParameter()
        {
            return systemParameter;
        }
        public override string GetUserName()
        {
            // check from SystemParameter property
            if (systemParameter != null && systemParameter.UserName != null)
            {
                return systemParameter.UserName;
            }
            else
            {
                throw new NullReferenceException("System Parameter [UserName] cannot be null.");
            }
        }

        public override string GetCompanyFilter()
        {
            return systemParameter?.CompanyGUID;
        }

        public override string GetBranchFilter()
        {
            return systemParameter?.BranchGUID;
        }
        public override string GetBatchInstanceId()
        {
            if (systemParameter != null && systemParameter.InstanceHistoryId != null)
            {
                return systemParameter.InstanceHistoryId;
            }
            else
            {
                return null;
            }

        }

        public override string GetUserId()
        {
            if (systemParameter != null && systemParameter.UserName != null)
            {
                return systemParameter.UserId;
            }
            else
            {
                throw new NullReferenceException("System Parameter [UserId] cannot be null.");
            }
        }
        public override int GetSiteLoginType()
        {
            return (systemParameter != null) ? systemParameter.SiteLogin : -1;
        }

        public override List<string> GetAvailableBranch()
        {
            if (GetCompanyFilter() == null) return null;
            Guid userGUID = GetUserId().StringToGuid();
            Guid companyGUID = GetCompanyFilter().StringToGuid();
            List<string> branches = SysUserCompanyMapping.Where(map => map.UserGUID == userGUID
                                                                    && map.CompanyGUID == companyGUID)
                                                         .AsNoTracking()
                                                         .Select(s => s.BranchGUID).ToList()
                                                         .Select(ss => ss.GuidNullToString()).ToList();
            return branches.Count() == 0 ? null : branches;
        }

        #region AccessLevel
        public override AccessLevelParm GetAccessLevel()
        {
            if (systemParameter != null)
            {
                return systemParameter.AccessLevel;
            }
            else
            {
                return null;
            }
        }
        public override AccessLevelParm GetDeleteAccessLevel()
        {
            if (systemParameter != null)
            {
                return systemParameter.DeleteAccessLevel;
            }
            else
            {
                return null;
            }
        }
        public override SearchCondition GetAccessLevelFilter()
        {
            if (systemParameter != null)
            {
                return SysAccessLevelHelper.GetFilterCondByAccessLevel(GetAccessLevel());
            }
            else
            {
                return SysAccessLevelHelper.GetPredicateNotCondition();
            }
        }

        #endregion AccessLevel
    }
}
