using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class AssignmentAgreementLineHandler
	{
		#region AssignmentAgreementLineListView
		public static List<AssignmentAgreementLineListView> GetAssignmentAgreementLineListViewValidation(this List<AssignmentAgreementLineListView> assignmentAgreementLineListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return assignmentAgreementLineListViews;
			}
			var result = new List<AssignmentAgreementLineListView>();
			try
			{
				foreach (AssignmentAgreementLineListView item in assignmentAgreementLineListViews)
				{
					result.Add(item.GetAssignmentAgreementLineListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AssignmentAgreementLineListView GetAssignmentAgreementLineListViewValidation(this AssignmentAgreementLineListView assignmentAgreementLineListView)
		{
			try
			{
				assignmentAgreementLineListView.RowAuthorize = assignmentAgreementLineListView.GetListRowAuthorize();
				return assignmentAgreementLineListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this AssignmentAgreementLineListView assignmentAgreementLineListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion AssignmentAgreementLineListView
		#region AssignmentAgreementLineItemView
		public static AssignmentAgreementLineItemView GetAssignmentAgreementLineItemViewValidation(this AssignmentAgreementLineItemView assignmentAgreementLineItemView)
		{
			try
			{
				assignmentAgreementLineItemView.RowAuthorize = assignmentAgreementLineItemView.GetItemRowAuthorize();
				return assignmentAgreementLineItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this AssignmentAgreementLineItemView assignmentAgreementLineItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static AssignmentAgreementLine ToAssignmentAgreementLine(this AssignmentAgreementLineItemView assignmentAgreementLineItemView)
		{
			try
			{
				AssignmentAgreementLine assignmentAgreementLine = new AssignmentAgreementLine();
				assignmentAgreementLine.CompanyGUID = assignmentAgreementLineItemView.CompanyGUID.StringToGuid();
				assignmentAgreementLine.CreatedBy = assignmentAgreementLineItemView.CreatedBy;
				assignmentAgreementLine.CreatedDateTime = assignmentAgreementLineItemView.CreatedDateTime.StringToSystemDateTime();
				assignmentAgreementLine.ModifiedBy = assignmentAgreementLineItemView.ModifiedBy;
				assignmentAgreementLine.ModifiedDateTime = assignmentAgreementLineItemView.ModifiedDateTime.StringToSystemDateTime();
				assignmentAgreementLine.Owner = assignmentAgreementLineItemView.Owner;
				assignmentAgreementLine.OwnerBusinessUnitGUID = assignmentAgreementLineItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				assignmentAgreementLine.AssignmentAgreementLineGUID = assignmentAgreementLineItemView.AssignmentAgreementLineGUID.StringToGuid();
				assignmentAgreementLine.AssignmentAgreementTableGUID = assignmentAgreementLineItemView.AssignmentAgreementTableGUID.StringToGuid();
				assignmentAgreementLine.BuyerAgreementTableGUID = assignmentAgreementLineItemView.BuyerAgreementTableGUID.StringToGuid();
				assignmentAgreementLine.LineNum = assignmentAgreementLineItemView.LineNum;
				
				assignmentAgreementLine.RowVersion = assignmentAgreementLineItemView.RowVersion;
				return assignmentAgreementLine;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<AssignmentAgreementLine> ToAssignmentAgreementLine(this IEnumerable<AssignmentAgreementLineItemView> assignmentAgreementLineItemViews)
		{
			try
			{
				List<AssignmentAgreementLine> assignmentAgreementLines = new List<AssignmentAgreementLine>();
				foreach (AssignmentAgreementLineItemView item in assignmentAgreementLineItemViews)
				{
					assignmentAgreementLines.Add(item.ToAssignmentAgreementLine());
				}
				return assignmentAgreementLines;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AssignmentAgreementLineItemView ToAssignmentAgreementLineItemView(this AssignmentAgreementLine assignmentAgreementLine)
		{
			try
			{
				AssignmentAgreementLineItemView assignmentAgreementLineItemView = new AssignmentAgreementLineItemView();
				assignmentAgreementLineItemView.CompanyGUID = assignmentAgreementLine.CompanyGUID.GuidNullToString();
				assignmentAgreementLineItemView.CreatedBy = assignmentAgreementLine.CreatedBy;
				assignmentAgreementLineItemView.CreatedDateTime = assignmentAgreementLine.CreatedDateTime.DateTimeToString();
				assignmentAgreementLineItemView.ModifiedBy = assignmentAgreementLine.ModifiedBy;
				assignmentAgreementLineItemView.ModifiedDateTime = assignmentAgreementLine.ModifiedDateTime.DateTimeToString();
				assignmentAgreementLineItemView.Owner = assignmentAgreementLine.Owner;
				assignmentAgreementLineItemView.OwnerBusinessUnitGUID = assignmentAgreementLine.OwnerBusinessUnitGUID.GuidNullToString();
				assignmentAgreementLineItemView.AssignmentAgreementLineGUID = assignmentAgreementLine.AssignmentAgreementLineGUID.GuidNullToString();
				assignmentAgreementLineItemView.AssignmentAgreementTableGUID = assignmentAgreementLine.AssignmentAgreementTableGUID.GuidNullToString();
				assignmentAgreementLineItemView.BuyerAgreementTableGUID = assignmentAgreementLine.BuyerAgreementTableGUID.GuidNullToString();
				assignmentAgreementLineItemView.LineNum = assignmentAgreementLine.LineNum;
				
				assignmentAgreementLineItemView.RowVersion = assignmentAgreementLine.RowVersion;
				return assignmentAgreementLineItemView.GetAssignmentAgreementLineItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion AssignmentAgreementLineItemView
		#region ToDropDown
		public static SelectItem<AssignmentAgreementLineItemView> ToDropDownItem(this AssignmentAgreementLineItemView assignmentAgreementLineView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<AssignmentAgreementLineItemView> selectItem = new SelectItem<AssignmentAgreementLineItemView>();
				selectItem.Label = null;
				selectItem.Value = assignmentAgreementLineView.AssignmentAgreementLineGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: assignmentAgreementLineView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<AssignmentAgreementLineItemView>> ToDropDownItem(this IEnumerable<AssignmentAgreementLineItemView> assignmentAgreementLineItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<AssignmentAgreementLineItemView>> selectItems = new List<SelectItem<AssignmentAgreementLineItemView>>();
				foreach (AssignmentAgreementLineItemView item in assignmentAgreementLineItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

