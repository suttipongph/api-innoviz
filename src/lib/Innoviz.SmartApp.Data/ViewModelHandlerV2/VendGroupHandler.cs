using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class VendGroupHandler
	{
		#region VendGroupListView
		public static List<VendGroupListView> GetVendGroupListViewValidation(this List<VendGroupListView> vendGroupListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return vendGroupListViews;
			}
			var result = new List<VendGroupListView>();
			try
			{
				foreach (VendGroupListView item in vendGroupListViews)
				{
					result.Add(item.GetVendGroupListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static VendGroupListView GetVendGroupListViewValidation(this VendGroupListView vendGroupListView)
		{
			try
			{
				vendGroupListView.RowAuthorize = vendGroupListView.GetListRowAuthorize();
				return vendGroupListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this VendGroupListView vendGroupListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion VendGroupListView
		#region VendGroupItemView
		public static VendGroupItemView GetVendGroupItemViewValidation(this VendGroupItemView vendGroupItemView)
		{
			try
			{
				vendGroupItemView.RowAuthorize = vendGroupItemView.GetItemRowAuthorize();
				return vendGroupItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this VendGroupItemView vendGroupItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static VendGroup ToVendGroup(this VendGroupItemView vendGroupItemView)
		{
			try
			{
				VendGroup vendGroup = new VendGroup();
				vendGroup.CompanyGUID = vendGroupItemView.CompanyGUID.StringToGuid();
				vendGroup.CreatedBy = vendGroupItemView.CreatedBy;
				vendGroup.CreatedDateTime = vendGroupItemView.CreatedDateTime.StringToSystemDateTime();
				vendGroup.ModifiedBy = vendGroupItemView.ModifiedBy;
				vendGroup.ModifiedDateTime = vendGroupItemView.ModifiedDateTime.StringToSystemDateTime();
				vendGroup.Owner = vendGroupItemView.Owner;
				vendGroup.OwnerBusinessUnitGUID = vendGroupItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				vendGroup.VendGroupGUID = vendGroupItemView.VendGroupGUID.StringToGuid();
				vendGroup.Description = vendGroupItemView.Description;
				vendGroup.VendGroupId = vendGroupItemView.VendGroupId;
				
				vendGroup.RowVersion = vendGroupItemView.RowVersion;
				return vendGroup;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<VendGroup> ToVendGroup(this IEnumerable<VendGroupItemView> vendGroupItemViews)
		{
			try
			{
				List<VendGroup> vendGroups = new List<VendGroup>();
				foreach (VendGroupItemView item in vendGroupItemViews)
				{
					vendGroups.Add(item.ToVendGroup());
				}
				return vendGroups;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static VendGroupItemView ToVendGroupItemView(this VendGroup vendGroup)
		{
			try
			{
				VendGroupItemView vendGroupItemView = new VendGroupItemView();
				vendGroupItemView.CompanyGUID = vendGroup.CompanyGUID.GuidNullToString();
				vendGroupItemView.CreatedBy = vendGroup.CreatedBy;
				vendGroupItemView.CreatedDateTime = vendGroup.CreatedDateTime.DateTimeToString();
				vendGroupItemView.ModifiedBy = vendGroup.ModifiedBy;
				vendGroupItemView.ModifiedDateTime = vendGroup.ModifiedDateTime.DateTimeToString();
				vendGroupItemView.Owner = vendGroup.Owner;
				vendGroupItemView.OwnerBusinessUnitGUID = vendGroup.OwnerBusinessUnitGUID.GuidNullToString();
				vendGroupItemView.VendGroupGUID = vendGroup.VendGroupGUID.GuidNullToString();
				vendGroupItemView.Description = vendGroup.Description;
				vendGroupItemView.VendGroupId = vendGroup.VendGroupId;
				
				vendGroupItemView.RowVersion = vendGroup.RowVersion;
				return vendGroupItemView.GetVendGroupItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion VendGroupItemView
		#region ToDropDown
		public static SelectItem<VendGroupItemView> ToDropDownItem(this VendGroupItemView vendGroupView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<VendGroupItemView> selectItem = new SelectItem<VendGroupItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(vendGroupView.VendGroupId, vendGroupView.Description);
				selectItem.Value = vendGroupView.VendGroupGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: vendGroupView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<VendGroupItemView>> ToDropDownItem(this IEnumerable<VendGroupItemView> vendGroupItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<VendGroupItemView>> selectItems = new List<SelectItem<VendGroupItemView>>();
				foreach (VendGroupItemView item in vendGroupItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

