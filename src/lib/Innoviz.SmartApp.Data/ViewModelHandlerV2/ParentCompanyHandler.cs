using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class ParentCompanyHandler
	{
		#region ParentCompanyListView
		public static List<ParentCompanyListView> GetParentCompanyListViewValidation(this List<ParentCompanyListView> parentCompanyListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return parentCompanyListViews;
			}
			var result = new List<ParentCompanyListView>();
			try
			{
				foreach (ParentCompanyListView item in parentCompanyListViews)
				{
					result.Add(item.GetParentCompanyListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ParentCompanyListView GetParentCompanyListViewValidation(this ParentCompanyListView parentCompanyListView)
		{
			try
			{
				parentCompanyListView.RowAuthorize = parentCompanyListView.GetListRowAuthorize();
				return parentCompanyListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this ParentCompanyListView parentCompanyListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion ParentCompanyListView
		#region ParentCompanyItemView
		public static ParentCompanyItemView GetParentCompanyItemViewValidation(this ParentCompanyItemView parentCompanyItemView)
		{
			try
			{
				parentCompanyItemView.RowAuthorize = parentCompanyItemView.GetItemRowAuthorize();
				return parentCompanyItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this ParentCompanyItemView parentCompanyItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static ParentCompany ToParentCompany(this ParentCompanyItemView parentCompanyItemView)
		{
			try
			{
				ParentCompany parentCompany = new ParentCompany();
				parentCompany.CompanyGUID = parentCompanyItemView.CompanyGUID.StringToGuid();
				parentCompany.CreatedBy = parentCompanyItemView.CreatedBy;
				parentCompany.CreatedDateTime = parentCompanyItemView.CreatedDateTime.StringToSystemDateTime();
				parentCompany.ModifiedBy = parentCompanyItemView.ModifiedBy;
				parentCompany.ModifiedDateTime = parentCompanyItemView.ModifiedDateTime.StringToSystemDateTime();
				parentCompany.Owner = parentCompanyItemView.Owner;
				parentCompany.OwnerBusinessUnitGUID = parentCompanyItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				parentCompany.ParentCompanyGUID = parentCompanyItemView.ParentCompanyGUID.StringToGuid();
				parentCompany.Description = parentCompanyItemView.Description;
				parentCompany.ParentCompanyId = parentCompanyItemView.ParentCompanyId;
				
				parentCompany.RowVersion = parentCompanyItemView.RowVersion;
				return parentCompany;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<ParentCompany> ToParentCompany(this IEnumerable<ParentCompanyItemView> parentCompanyItemViews)
		{
			try
			{
				List<ParentCompany> parentCompanys = new List<ParentCompany>();
				foreach (ParentCompanyItemView item in parentCompanyItemViews)
				{
					parentCompanys.Add(item.ToParentCompany());
				}
				return parentCompanys;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ParentCompanyItemView ToParentCompanyItemView(this ParentCompany parentCompany)
		{
			try
			{
				ParentCompanyItemView parentCompanyItemView = new ParentCompanyItemView();
				parentCompanyItemView.CompanyGUID = parentCompany.CompanyGUID.GuidNullToString();
				parentCompanyItemView.CreatedBy = parentCompany.CreatedBy;
				parentCompanyItemView.CreatedDateTime = parentCompany.CreatedDateTime.DateTimeToString();
				parentCompanyItemView.ModifiedBy = parentCompany.ModifiedBy;
				parentCompanyItemView.ModifiedDateTime = parentCompany.ModifiedDateTime.DateTimeToString();
				parentCompanyItemView.Owner = parentCompany.Owner;
				parentCompanyItemView.OwnerBusinessUnitGUID = parentCompany.OwnerBusinessUnitGUID.GuidNullToString();
				parentCompanyItemView.ParentCompanyGUID = parentCompany.ParentCompanyGUID.GuidNullToString();
				parentCompanyItemView.Description = parentCompany.Description;
				parentCompanyItemView.ParentCompanyId = parentCompany.ParentCompanyId;
				
				parentCompanyItemView.RowVersion = parentCompany.RowVersion;
				return parentCompanyItemView.GetParentCompanyItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ParentCompanyItemView
		#region ToDropDown
		public static SelectItem<ParentCompanyItemView> ToDropDownItem(this ParentCompanyItemView parentCompanyView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<ParentCompanyItemView> selectItem = new SelectItem<ParentCompanyItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(parentCompanyView.ParentCompanyId, parentCompanyView.Description);
				selectItem.Value = parentCompanyView.ParentCompanyGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: parentCompanyView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<ParentCompanyItemView>> ToDropDownItem(this IEnumerable<ParentCompanyItemView> parentCompanyItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<ParentCompanyItemView>> selectItems = new List<SelectItem<ParentCompanyItemView>>();
				foreach (ParentCompanyItemView item in parentCompanyItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

