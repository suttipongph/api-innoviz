using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class ConsortiumTransHandler
	{
		#region ConsortiumTransListView
		public static List<ConsortiumTransListView> GetConsortiumTransListViewValidation(this List<ConsortiumTransListView> consortiumTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return consortiumTransListViews;
			}
			var result = new List<ConsortiumTransListView>();
			try
			{
				foreach (ConsortiumTransListView item in consortiumTransListViews)
				{
					result.Add(item.GetConsortiumTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ConsortiumTransListView GetConsortiumTransListViewValidation(this ConsortiumTransListView consortiumTransListView)
		{
			try
			{
				consortiumTransListView.RowAuthorize = consortiumTransListView.GetListRowAuthorize();
				return consortiumTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this ConsortiumTransListView consortiumTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion ConsortiumTransListView
		#region ConsortiumTransItemView
		public static ConsortiumTransItemView GetConsortiumTransItemViewValidation(this ConsortiumTransItemView consortiumTransItemView)
		{
			try
			{
				consortiumTransItemView.RowAuthorize = consortiumTransItemView.GetItemRowAuthorize();
				return consortiumTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this ConsortiumTransItemView consortiumTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static ConsortiumTrans ToConsortiumTrans(this ConsortiumTransItemView consortiumTransItemView)
		{
			try
			{
				ConsortiumTrans consortiumTrans = new ConsortiumTrans();
				consortiumTrans.CompanyGUID = consortiumTransItemView.CompanyGUID.StringToGuid();
				consortiumTrans.CreatedBy = consortiumTransItemView.CreatedBy;
				consortiumTrans.CreatedDateTime = consortiumTransItemView.CreatedDateTime.StringToSystemDateTime();
				consortiumTrans.ModifiedBy = consortiumTransItemView.ModifiedBy;
				consortiumTrans.ModifiedDateTime = consortiumTransItemView.ModifiedDateTime.StringToSystemDateTime();
				consortiumTrans.Owner = consortiumTransItemView.Owner;
				consortiumTrans.OwnerBusinessUnitGUID = consortiumTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				consortiumTrans.ConsortiumTransGUID = consortiumTransItemView.ConsortiumTransGUID.StringToGuid();
				consortiumTrans.Address = consortiumTransItemView.Address;
				consortiumTrans.AuthorizedPersonTypeGUID = consortiumTransItemView.AuthorizedPersonTypeGUID.StringToGuidNull();
				consortiumTrans.CustomerName = consortiumTransItemView.CustomerName;
				consortiumTrans.OperatedBy = consortiumTransItemView.OperatedBy;
				consortiumTrans.Ordering = consortiumTransItemView.Ordering;
				consortiumTrans.RefGUID = consortiumTransItemView.RefGUID.StringToGuid();
				consortiumTrans.RefType = consortiumTransItemView.RefType;
				consortiumTrans.Remark = consortiumTransItemView.Remark;
				consortiumTrans.ConsortiumLineGUID = consortiumTransItemView.ConsortiumLineGUID.StringToGuidNull();
				
				consortiumTrans.RowVersion = consortiumTransItemView.RowVersion;
				return consortiumTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<ConsortiumTrans> ToConsortiumTrans(this IEnumerable<ConsortiumTransItemView> consortiumTransItemViews)
		{
			try
			{
				List<ConsortiumTrans> consortiumTranss = new List<ConsortiumTrans>();
				foreach (ConsortiumTransItemView item in consortiumTransItemViews)
				{
					consortiumTranss.Add(item.ToConsortiumTrans());
				}
				return consortiumTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ConsortiumTransItemView ToConsortiumTransItemView(this ConsortiumTrans consortiumTrans)
		{
			try
			{
				ConsortiumTransItemView consortiumTransItemView = new ConsortiumTransItemView();
				consortiumTransItemView.CompanyGUID = consortiumTrans.CompanyGUID.GuidNullToString();
				consortiumTransItemView.CreatedBy = consortiumTrans.CreatedBy;
				consortiumTransItemView.CreatedDateTime = consortiumTrans.CreatedDateTime.DateTimeToString();
				consortiumTransItemView.ModifiedBy = consortiumTrans.ModifiedBy;
				consortiumTransItemView.ModifiedDateTime = consortiumTrans.ModifiedDateTime.DateTimeToString();
				consortiumTransItemView.Owner = consortiumTrans.Owner;
				consortiumTransItemView.OwnerBusinessUnitGUID = consortiumTrans.OwnerBusinessUnitGUID.GuidNullToString();
				consortiumTransItemView.ConsortiumTransGUID = consortiumTrans.ConsortiumTransGUID.GuidNullToString();
				consortiumTransItemView.Address = consortiumTrans.Address;
				consortiumTransItemView.AuthorizedPersonTypeGUID = consortiumTrans.AuthorizedPersonTypeGUID.GuidNullToString();
				consortiumTransItemView.CustomerName = consortiumTrans.CustomerName;
				consortiumTransItemView.OperatedBy = consortiumTrans.OperatedBy;
				consortiumTransItemView.Ordering = consortiumTrans.Ordering;
				consortiumTransItemView.RefGUID = consortiumTrans.RefGUID.GuidNullToString();
				consortiumTransItemView.RefType = consortiumTrans.RefType;
				consortiumTransItemView.Remark = consortiumTrans.Remark;
				consortiumTransItemView.ConsortiumLineGUID = consortiumTrans.ConsortiumLineGUID.GuidNullToString();
				
				consortiumTransItemView.RowVersion = consortiumTrans.RowVersion;
				return consortiumTransItemView.GetConsortiumTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ConsortiumTransItemView
		#region ToDropDown
		public static SelectItem<ConsortiumTransItemView> ToDropDownItem(this ConsortiumTransItemView consortiumTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<ConsortiumTransItemView> selectItem = new SelectItem<ConsortiumTransItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(consortiumTransView.CustomerName);
				selectItem.Value = consortiumTransView.ConsortiumTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: consortiumTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<ConsortiumTransItemView>> ToDropDownItem(this IEnumerable<ConsortiumTransItemView> consortiumTransItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<ConsortiumTransItemView>> selectItems = new List<SelectItem<ConsortiumTransItemView>>();
				foreach (ConsortiumTransItemView item in consortiumTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

