using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class BusinessSegmentHandler
	{
		#region BusinessSegmentListView
		public static List<BusinessSegmentListView> GetBusinessSegmentListViewValidation(this List<BusinessSegmentListView> businessSegmentListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return businessSegmentListViews;
			}
			var result = new List<BusinessSegmentListView>();
			try
			{
				foreach (BusinessSegmentListView item in businessSegmentListViews)
				{
					result.Add(item.GetBusinessSegmentListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BusinessSegmentListView GetBusinessSegmentListViewValidation(this BusinessSegmentListView businessSegmentListView)
		{
			try
			{
				businessSegmentListView.RowAuthorize = businessSegmentListView.GetListRowAuthorize();
				return businessSegmentListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this BusinessSegmentListView businessSegmentListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion BusinessSegmentListView
		#region BusinessSegmentItemView
		public static BusinessSegmentItemView GetBusinessSegmentItemViewValidation(this BusinessSegmentItemView businessSegmentItemView)
		{
			try
			{
				businessSegmentItemView.RowAuthorize = businessSegmentItemView.GetItemRowAuthorize();
				return businessSegmentItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this BusinessSegmentItemView businessSegmentItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static BusinessSegment ToBusinessSegment(this BusinessSegmentItemView businessSegmentItemView)
		{
			try
			{
				BusinessSegment businessSegment = new BusinessSegment();
				businessSegment.CompanyGUID = businessSegmentItemView.CompanyGUID.StringToGuid();
				businessSegment.CreatedBy = businessSegmentItemView.CreatedBy;
				businessSegment.CreatedDateTime = businessSegmentItemView.CreatedDateTime.StringToSystemDateTime();
				businessSegment.ModifiedBy = businessSegmentItemView.ModifiedBy;
				businessSegment.ModifiedDateTime = businessSegmentItemView.ModifiedDateTime.StringToSystemDateTime();
				businessSegment.Owner = businessSegmentItemView.Owner;
				businessSegment.OwnerBusinessUnitGUID = businessSegmentItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				businessSegment.BusinessSegmentGUID = businessSegmentItemView.BusinessSegmentGUID.StringToGuid();
				businessSegment.BusinessSegmentId = businessSegmentItemView.BusinessSegmentId;
				businessSegment.BusinessSegmentType = businessSegmentItemView.BusinessSegmentType;
				businessSegment.Description = businessSegmentItemView.Description;
				
				businessSegment.RowVersion = businessSegmentItemView.RowVersion;
				return businessSegment;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<BusinessSegment> ToBusinessSegment(this IEnumerable<BusinessSegmentItemView> businessSegmentItemViews)
		{
			try
			{
				List<BusinessSegment> businessSegments = new List<BusinessSegment>();
				foreach (BusinessSegmentItemView item in businessSegmentItemViews)
				{
					businessSegments.Add(item.ToBusinessSegment());
				}
				return businessSegments;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BusinessSegmentItemView ToBusinessSegmentItemView(this BusinessSegment businessSegment)
		{
			try
			{
				BusinessSegmentItemView businessSegmentItemView = new BusinessSegmentItemView();
				businessSegmentItemView.CompanyGUID = businessSegment.CompanyGUID.GuidNullToString();
				businessSegmentItemView.CreatedBy = businessSegment.CreatedBy;
				businessSegmentItemView.CreatedDateTime = businessSegment.CreatedDateTime.DateTimeToString();
				businessSegmentItemView.ModifiedBy = businessSegment.ModifiedBy;
				businessSegmentItemView.ModifiedDateTime = businessSegment.ModifiedDateTime.DateTimeToString();
				businessSegmentItemView.Owner = businessSegment.Owner;
				businessSegmentItemView.OwnerBusinessUnitGUID = businessSegment.OwnerBusinessUnitGUID.GuidNullToString();
				businessSegmentItemView.BusinessSegmentGUID = businessSegment.BusinessSegmentGUID.GuidNullToString();
				businessSegmentItemView.BusinessSegmentId = businessSegment.BusinessSegmentId;
				businessSegmentItemView.BusinessSegmentType = businessSegment.BusinessSegmentType;
				businessSegmentItemView.Description = businessSegment.Description;
				
				businessSegmentItemView.RowVersion = businessSegment.RowVersion;
				return businessSegmentItemView.GetBusinessSegmentItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion BusinessSegmentItemView
		#region ToDropDown
		public static SelectItem<BusinessSegmentItemView> ToDropDownItem(this BusinessSegmentItemView businessSegmentView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<BusinessSegmentItemView> selectItem = new SelectItem<BusinessSegmentItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(businessSegmentView.BusinessSegmentId, businessSegmentView.Description);
				selectItem.Value = businessSegmentView.BusinessSegmentGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: businessSegmentView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<BusinessSegmentItemView>> ToDropDownItem(this IEnumerable<BusinessSegmentItemView> businessSegmentItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<BusinessSegmentItemView>> selectItems = new List<SelectItem<BusinessSegmentItemView>>();
				foreach (BusinessSegmentItemView item in businessSegmentItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

