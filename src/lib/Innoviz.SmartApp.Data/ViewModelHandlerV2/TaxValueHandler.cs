using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class TaxValueHandler
	{
		#region TaxValueListView
		public static List<TaxValueListView> GetTaxValueListViewValidation(this List<TaxValueListView> taxValueListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return taxValueListViews;
			}
			var result = new List<TaxValueListView>();
			try
			{
				foreach (TaxValueListView item in taxValueListViews)
				{
					result.Add(item.GetTaxValueListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static TaxValueListView GetTaxValueListViewValidation(this TaxValueListView taxValueListView)
		{
			try
			{
				taxValueListView.RowAuthorize = taxValueListView.GetListRowAuthorize();
				return taxValueListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this TaxValueListView taxValueListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion TaxValueListView
		#region TaxValueItemView
		public static TaxValueItemView GetTaxValueItemViewValidation(this TaxValueItemView taxValueItemView)
		{
			try
			{
				taxValueItemView.RowAuthorize = taxValueItemView.GetItemRowAuthorize();
				return taxValueItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this TaxValueItemView taxValueItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static TaxValue ToTaxValue(this TaxValueItemView taxValueItemView)
		{
			try
			{
				TaxValue taxValue = new TaxValue();
				taxValue.CompanyGUID = taxValueItemView.CompanyGUID.StringToGuid();
				taxValue.CreatedBy = taxValueItemView.CreatedBy;
				taxValue.CreatedDateTime = taxValueItemView.CreatedDateTime.StringToSystemDateTime();
				taxValue.ModifiedBy = taxValueItemView.ModifiedBy;
				taxValue.ModifiedDateTime = taxValueItemView.ModifiedDateTime.StringToSystemDateTime();
				taxValue.Owner = taxValueItemView.Owner;
				taxValue.OwnerBusinessUnitGUID = taxValueItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				taxValue.TaxValueGUID = taxValueItemView.TaxValueGUID.StringToGuid();
				taxValue.EffectiveFrom = taxValueItemView.EffectiveFrom.StringToDate();
				taxValue.EffectiveTo = taxValueItemView.EffectiveTo.StringNullToDateNull();
				taxValue.TaxTableGUID = taxValueItemView.TaxTableGUID.StringToGuid();
				taxValue.Value = taxValueItemView.Value;
				
				taxValue.RowVersion = taxValueItemView.RowVersion;
				return taxValue;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<TaxValue> ToTaxValue(this IEnumerable<TaxValueItemView> taxValueItemViews)
		{
			try
			{
				List<TaxValue> taxValues = new List<TaxValue>();
				foreach (TaxValueItemView item in taxValueItemViews)
				{
					taxValues.Add(item.ToTaxValue());
				}
				return taxValues;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static TaxValueItemView ToTaxValueItemView(this TaxValue taxValue)
		{
			try
			{
				TaxValueItemView taxValueItemView = new TaxValueItemView();
				taxValueItemView.CompanyGUID = taxValue.CompanyGUID.GuidNullToString();
				taxValueItemView.CreatedBy = taxValue.CreatedBy;
				taxValueItemView.CreatedDateTime = taxValue.CreatedDateTime.DateTimeToString();
				taxValueItemView.ModifiedBy = taxValue.ModifiedBy;
				taxValueItemView.ModifiedDateTime = taxValue.ModifiedDateTime.DateTimeToString();
				taxValueItemView.Owner = taxValue.Owner;
				taxValueItemView.OwnerBusinessUnitGUID = taxValue.OwnerBusinessUnitGUID.GuidNullToString();
				taxValueItemView.TaxValueGUID = taxValue.TaxValueGUID.GuidNullToString();
				taxValueItemView.EffectiveFrom = taxValue.EffectiveFrom.DateToString();
				taxValueItemView.EffectiveTo = taxValue.EffectiveTo.DateNullToString();
				taxValueItemView.TaxTableGUID = taxValue.TaxTableGUID.GuidNullToString();
				taxValueItemView.Value = taxValue.Value;
				
				taxValueItemView.RowVersion = taxValue.RowVersion;
				return taxValueItemView.GetTaxValueItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion TaxValueItemView
		#region ToDropDown
		public static SelectItem<TaxValueItemView> ToDropDownItem(this TaxValueItemView taxValueView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<TaxValueItemView> selectItem = new SelectItem<TaxValueItemView>();
				selectItem.Label = null;
				selectItem.Value = taxValueView.TaxValueGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: taxValueView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<TaxValueItemView>> ToDropDownItem(this IEnumerable<TaxValueItemView> taxValueItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<TaxValueItemView>> selectItems = new List<SelectItem<TaxValueItemView>>();
				foreach (TaxValueItemView item in taxValueItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

