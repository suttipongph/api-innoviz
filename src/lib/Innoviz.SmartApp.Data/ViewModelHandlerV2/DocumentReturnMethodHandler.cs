using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class DocumentReturnMethodHandler
	{
		#region DocumentReturnMethodListView
		public static List<DocumentReturnMethodListView> GetDocumentReturnMethodListViewValidation(this List<DocumentReturnMethodListView> documentReturnMethodListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return documentReturnMethodListViews;
			}
			var result = new List<DocumentReturnMethodListView>();
			try
			{
				foreach (DocumentReturnMethodListView item in documentReturnMethodListViews)
				{
					result.Add(item.GetDocumentReturnMethodListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static DocumentReturnMethodListView GetDocumentReturnMethodListViewValidation(this DocumentReturnMethodListView documentReturnMethodListView)
		{
			try
			{
				documentReturnMethodListView.RowAuthorize = documentReturnMethodListView.GetListRowAuthorize();
				return documentReturnMethodListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this DocumentReturnMethodListView documentReturnMethodListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion DocumentReturnMethodListView
		#region DocumentReturnMethodItemView
		public static DocumentReturnMethodItemView GetDocumentReturnMethodItemViewValidation(this DocumentReturnMethodItemView documentReturnMethodItemView)
		{
			try
			{
				documentReturnMethodItemView.RowAuthorize = documentReturnMethodItemView.GetItemRowAuthorize();
				return documentReturnMethodItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this DocumentReturnMethodItemView documentReturnMethodItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static DocumentReturnMethod ToDocumentReturnMethod(this DocumentReturnMethodItemView documentReturnMethodItemView)
		{
			try
			{
				DocumentReturnMethod documentReturnMethod = new DocumentReturnMethod();
				documentReturnMethod.CompanyGUID = documentReturnMethodItemView.CompanyGUID.StringToGuid();
				documentReturnMethod.CreatedBy = documentReturnMethodItemView.CreatedBy;
				documentReturnMethod.CreatedDateTime = documentReturnMethodItemView.CreatedDateTime.StringToSystemDateTime();
				documentReturnMethod.ModifiedBy = documentReturnMethodItemView.ModifiedBy;
				documentReturnMethod.ModifiedDateTime = documentReturnMethodItemView.ModifiedDateTime.StringToSystemDateTime();
				documentReturnMethod.Owner = documentReturnMethodItemView.Owner;
				documentReturnMethod.OwnerBusinessUnitGUID = documentReturnMethodItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				documentReturnMethod.DocumentReturnMethodGUID = documentReturnMethodItemView.DocumentReturnMethodGUID.StringToGuid();
				documentReturnMethod.Description = documentReturnMethodItemView.Description;
				documentReturnMethod.DocumentReturnMethodId = documentReturnMethodItemView.DocumentReturnMethodId;
				
				documentReturnMethod.RowVersion = documentReturnMethodItemView.RowVersion;
				return documentReturnMethod;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<DocumentReturnMethod> ToDocumentReturnMethod(this IEnumerable<DocumentReturnMethodItemView> documentReturnMethodItemViews)
		{
			try
			{
				List<DocumentReturnMethod> documentReturnMethods = new List<DocumentReturnMethod>();
				foreach (DocumentReturnMethodItemView item in documentReturnMethodItemViews)
				{
					documentReturnMethods.Add(item.ToDocumentReturnMethod());
				}
				return documentReturnMethods;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static DocumentReturnMethodItemView ToDocumentReturnMethodItemView(this DocumentReturnMethod documentReturnMethod)
		{
			try
			{
				DocumentReturnMethodItemView documentReturnMethodItemView = new DocumentReturnMethodItemView();
				documentReturnMethodItemView.CompanyGUID = documentReturnMethod.CompanyGUID.GuidNullToString();
				documentReturnMethodItemView.CreatedBy = documentReturnMethod.CreatedBy;
				documentReturnMethodItemView.CreatedDateTime = documentReturnMethod.CreatedDateTime.DateTimeToString();
				documentReturnMethodItemView.ModifiedBy = documentReturnMethod.ModifiedBy;
				documentReturnMethodItemView.ModifiedDateTime = documentReturnMethod.ModifiedDateTime.DateTimeToString();
				documentReturnMethodItemView.Owner = documentReturnMethod.Owner;
				documentReturnMethodItemView.OwnerBusinessUnitGUID = documentReturnMethod.OwnerBusinessUnitGUID.GuidNullToString();
				documentReturnMethodItemView.DocumentReturnMethodGUID = documentReturnMethod.DocumentReturnMethodGUID.GuidNullToString();
				documentReturnMethodItemView.Description = documentReturnMethod.Description;
				documentReturnMethodItemView.DocumentReturnMethodId = documentReturnMethod.DocumentReturnMethodId;
				
				documentReturnMethodItemView.RowVersion = documentReturnMethod.RowVersion;
				return documentReturnMethodItemView.GetDocumentReturnMethodItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion DocumentReturnMethodItemView
		#region ToDropDown
		public static SelectItem<DocumentReturnMethodItemView> ToDropDownItem(this DocumentReturnMethodItemView documentReturnMethodView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<DocumentReturnMethodItemView> selectItem = new SelectItem<DocumentReturnMethodItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(documentReturnMethodView.DocumentReturnMethodId, documentReturnMethodView.Description);
				selectItem.Value = documentReturnMethodView.DocumentReturnMethodGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: documentReturnMethodView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<DocumentReturnMethodItemView>> ToDropDownItem(this IEnumerable<DocumentReturnMethodItemView> documentReturnMethodItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<DocumentReturnMethodItemView>> selectItems = new List<SelectItem<DocumentReturnMethodItemView>>();
				foreach (DocumentReturnMethodItemView item in documentReturnMethodItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

