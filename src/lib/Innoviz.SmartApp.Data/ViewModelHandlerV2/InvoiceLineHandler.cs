using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class InvoiceLineHandler
	{
		#region InvoiceLineListView
		public static List<InvoiceLineListView> GetInvoiceLineListViewValidation(this List<InvoiceLineListView> invoiceLineListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return invoiceLineListViews;
			}
			var result = new List<InvoiceLineListView>();
			try
			{
				foreach (InvoiceLineListView item in invoiceLineListViews)
				{
					result.Add(item.GetInvoiceLineListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static InvoiceLineListView GetInvoiceLineListViewValidation(this InvoiceLineListView invoiceLineListView)
		{
			try
			{
				invoiceLineListView.RowAuthorize = invoiceLineListView.GetListRowAuthorize();
				return invoiceLineListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this InvoiceLineListView invoiceLineListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion InvoiceLineListView
		#region InvoiceLineItemView
		public static InvoiceLineItemView GetInvoiceLineItemViewValidation(this InvoiceLineItemView invoiceLineItemView)
		{
			try
			{
				invoiceLineItemView.RowAuthorize = invoiceLineItemView.GetItemRowAuthorize();
				return invoiceLineItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this InvoiceLineItemView invoiceLineItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static InvoiceLine ToInvoiceLine(this InvoiceLineItemView invoiceLineItemView)
		{
			try
			{
				InvoiceLine invoiceLine = new InvoiceLine();
				invoiceLine.CompanyGUID = invoiceLineItemView.CompanyGUID.StringToGuid();
				invoiceLine.BranchGUID = invoiceLineItemView.BranchGUID.StringToGuid();
				invoiceLine.CreatedBy = invoiceLineItemView.CreatedBy;
				invoiceLine.CreatedDateTime = invoiceLineItemView.CreatedDateTime.StringToSystemDateTime();
				invoiceLine.ModifiedBy = invoiceLineItemView.ModifiedBy;
				invoiceLine.ModifiedDateTime = invoiceLineItemView.ModifiedDateTime.StringToSystemDateTime();
				invoiceLine.Owner = invoiceLineItemView.Owner;
				invoiceLine.OwnerBusinessUnitGUID = invoiceLineItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				invoiceLine.InvoiceLineGUID = invoiceLineItemView.InvoiceLineGUID.StringToGuid();
				invoiceLine.Dimension1GUID = invoiceLineItemView.Dimension1GUID.StringToGuidNull();
				invoiceLine.Dimension2GUID = invoiceLineItemView.Dimension2GUID.StringToGuidNull();
				invoiceLine.Dimension3GUID = invoiceLineItemView.Dimension3GUID.StringToGuidNull();
				invoiceLine.Dimension4GUID = invoiceLineItemView.Dimension4GUID.StringToGuidNull();
				invoiceLine.Dimension5GUID = invoiceLineItemView.Dimension5GUID.StringToGuidNull();
				invoiceLine.InvoiceRevenueTypeGUID = invoiceLineItemView.InvoiceRevenueTypeGUID.StringToGuid();
				invoiceLine.InvoiceTableGUID = invoiceLineItemView.InvoiceTableGUID.StringToGuid();
				invoiceLine.InvoiceText = invoiceLineItemView.InvoiceText;
				invoiceLine.LineNum = invoiceLineItemView.LineNum;
				invoiceLine.ProdUnitGUID = invoiceLineItemView.ProdUnitGUID.StringToGuidNull();
				invoiceLine.Qty = invoiceLineItemView.Qty;
				invoiceLine.TaxAmount = invoiceLineItemView.TaxAmount;
				invoiceLine.TaxTableGUID = invoiceLineItemView.TaxTableGUID.StringToGuidNull();
				invoiceLine.TotalAmount = invoiceLineItemView.TotalAmount;
				invoiceLine.TotalAmountBeforeTax = invoiceLineItemView.TotalAmountBeforeTax;
				invoiceLine.UnitPrice = invoiceLineItemView.UnitPrice;
				invoiceLine.WHTAmount = invoiceLineItemView.WHTAmount;
				invoiceLine.WHTBaseAmount = invoiceLineItemView.WHTBaseAmount;
				invoiceLine.WithholdingTaxTableGUID = invoiceLineItemView.WithholdingTaxTableGUID.StringToGuidNull();
				
				invoiceLine.RowVersion = invoiceLineItemView.RowVersion;
				return invoiceLine;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<InvoiceLine> ToInvoiceLine(this IEnumerable<InvoiceLineItemView> invoiceLineItemViews)
		{
			try
			{
				List<InvoiceLine> invoiceLines = new List<InvoiceLine>();
				foreach (InvoiceLineItemView item in invoiceLineItemViews)
				{
					invoiceLines.Add(item.ToInvoiceLine());
				}
				return invoiceLines;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static InvoiceLineItemView ToInvoiceLineItemView(this InvoiceLine invoiceLine)
		{
			try
			{
				InvoiceLineItemView invoiceLineItemView = new InvoiceLineItemView();
				invoiceLineItemView.CompanyGUID = invoiceLine.CompanyGUID.GuidNullToString();
				invoiceLineItemView.BranchGUID = invoiceLine.BranchGUID.GuidNullToString();
				invoiceLineItemView.CreatedBy = invoiceLine.CreatedBy;
				invoiceLineItemView.CreatedDateTime = invoiceLine.CreatedDateTime.DateTimeToString();
				invoiceLineItemView.ModifiedBy = invoiceLine.ModifiedBy;
				invoiceLineItemView.ModifiedDateTime = invoiceLine.ModifiedDateTime.DateTimeToString();
				invoiceLineItemView.Owner = invoiceLine.Owner;
				invoiceLineItemView.OwnerBusinessUnitGUID = invoiceLine.OwnerBusinessUnitGUID.GuidNullToString();
				invoiceLineItemView.InvoiceLineGUID = invoiceLine.InvoiceLineGUID.GuidNullToString();
				invoiceLineItemView.Dimension1GUID = invoiceLine.Dimension1GUID.GuidNullToString();
				invoiceLineItemView.Dimension2GUID = invoiceLine.Dimension2GUID.GuidNullToString();
				invoiceLineItemView.Dimension3GUID = invoiceLine.Dimension3GUID.GuidNullToString();
				invoiceLineItemView.Dimension4GUID = invoiceLine.Dimension4GUID.GuidNullToString();
				invoiceLineItemView.Dimension5GUID = invoiceLine.Dimension5GUID.GuidNullToString();
				invoiceLineItemView.InvoiceRevenueTypeGUID = invoiceLine.InvoiceRevenueTypeGUID.GuidNullToString();
				invoiceLineItemView.InvoiceTableGUID = invoiceLine.InvoiceTableGUID.GuidNullToString();
				invoiceLineItemView.InvoiceText = invoiceLine.InvoiceText;
				invoiceLineItemView.LineNum = invoiceLine.LineNum;
				invoiceLineItemView.ProdUnitGUID = invoiceLine.ProdUnitGUID.GuidNullToString();
				invoiceLineItemView.Qty = invoiceLine.Qty;
				invoiceLineItemView.TaxAmount = invoiceLine.TaxAmount;
				invoiceLineItemView.TaxTableGUID = invoiceLine.TaxTableGUID.GuidNullToString();
				invoiceLineItemView.TotalAmount = invoiceLine.TotalAmount;
				invoiceLineItemView.TotalAmountBeforeTax = invoiceLine.TotalAmountBeforeTax;
				invoiceLineItemView.UnitPrice = invoiceLine.UnitPrice;
				invoiceLineItemView.WHTAmount = invoiceLine.WHTAmount;
				invoiceLineItemView.WHTBaseAmount = invoiceLine.WHTBaseAmount;
				invoiceLineItemView.WithholdingTaxTableGUID = invoiceLine.WithholdingTaxTableGUID.GuidNullToString();
				
				invoiceLineItemView.RowVersion = invoiceLine.RowVersion;
				return invoiceLineItemView.GetInvoiceLineItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion InvoiceLineItemView
		#region ToDropDown
		public static SelectItem<InvoiceLineItemView> ToDropDownItem(this InvoiceLineItemView invoiceLineView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<InvoiceLineItemView> selectItem = new SelectItem<InvoiceLineItemView>();
				selectItem.Label = null;
				selectItem.Value = invoiceLineView.InvoiceLineGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: invoiceLineView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<InvoiceLineItemView>> ToDropDownItem(this IEnumerable<InvoiceLineItemView> invoiceLineItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<InvoiceLineItemView>> selectItems = new List<SelectItem<InvoiceLineItemView>>();
				foreach (InvoiceLineItemView item in invoiceLineItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

