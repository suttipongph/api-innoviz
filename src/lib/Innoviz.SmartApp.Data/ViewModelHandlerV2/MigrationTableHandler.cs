using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
    public static class MigrationHandler
    {
        #region MigrationTableListView
        public static List<MigrationTableListView> GetMigrationTableListViewValidation(this List<MigrationTableListView> migrationListViews, bool skipMethod = false)
        {
            if (skipMethod)
            {
                return migrationListViews;
            }
            var result = new List<MigrationTableListView>();
            try
            {
                foreach (MigrationTableListView item in migrationListViews)
                {
                    result.Add(item.GetMigrationTableListViewValidation());
                }
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static MigrationTableListView GetMigrationTableListViewValidation(this MigrationTableListView migrationListView)
        {
            try
            {
                migrationListView.RowAuthorize = migrationListView.GetListRowAuthorize();
                return migrationListView;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        private static int GetListRowAuthorize(this MigrationTableListView migrationListView)
        {
            return AccessMode.Full.GetAttrValue();
        }
        #endregion MigrationTableListView
        #region MigrationTableItemView
        public static MigrationTableItemView GetMigrationTableItemViewValidation(this MigrationTableItemView migrationTableItemView)
        {
            try
            {
                migrationTableItemView.RowAuthorize = migrationTableItemView.GetItemRowAuthorize();
                return migrationTableItemView;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        private static int GetItemRowAuthorize(this MigrationTableItemView migrationTableItemView)
        {
            return AccessMode.Full.GetAttrValue();
        }
        public static MigrationTable ToMigrationTable(this MigrationTableItemView migrationTableItemView)
        {
            try
            {
                MigrationTable migrationTable = new MigrationTable();
                migrationTable.MigrationTableGUID = migrationTableItemView.MigrationTableGUID.StringToGuid();
                migrationTable.CreatedBy = migrationTableItemView.CreatedBy;
                migrationTable.CreatedDateTime = migrationTableItemView.CreatedDateTime.StringToSystemDateTime();
                migrationTable.ModifiedBy = migrationTableItemView.ModifiedBy;
                migrationTable.ModifiedDateTime = migrationTableItemView.ModifiedDateTime.StringToSystemDateTime();
                migrationTable.GroupName = migrationTableItemView.GroupName;
                migrationTable.GroupOrder = migrationTableItemView.GroupOrder;
                migrationTable.MigrateName = migrationTableItemView.MigrationName;
                migrationTable.MigrateOrder = migrationTableItemView.MigrationOrder;
                migrationTable.MigrateResult = migrationTableItemView.MigrationResult;
                migrationTable.PackageName = migrationTableItemView.PackageName;
                
                migrationTable.RowVersion = migrationTableItemView.RowVersion;
                return migrationTable;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static IEnumerable<MigrationTable> ToMigrationTable(this IEnumerable<MigrationTableItemView> magrationViews)
        {
            try
            {
                List<MigrationTable> migrations = new List<MigrationTable>();
                foreach (MigrationTableItemView item in magrationViews)
                {
                    migrations.Add(item.ToMigrationTable());
                }
                return migrations;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static IEnumerable<MigrationTableItemView> ToMigrationTableItemView(this IEnumerable<MigrationTable> migrationTables)
        {
            try
            {
                List<MigrationTableItemView> migrationTableItemViews = new List<MigrationTableItemView>();
                foreach (MigrationTable item in migrationTables)
                {
                    migrationTableItemViews.Add(item.ToMigrationTableItemView());
                }
                return migrationTableItemViews;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static MigrationTableItemView ToMigrationTableItemView(this MigrationTable migrationTable)
        {
            try
            {
                MigrationTableItemView migrationTableItemView = new MigrationTableItemView();
                migrationTableItemView.MigrationTableGUID = migrationTable.MigrationTableGUID.GuidNullToString();
                migrationTableItemView.CreatedBy = migrationTable.CreatedBy;
                migrationTableItemView.CreatedDateTime = migrationTable.CreatedDateTime.DateTimeToString();
                migrationTableItemView.ModifiedBy = migrationTable.ModifiedBy;
                migrationTableItemView.ModifiedDateTime = migrationTable.ModifiedDateTime.DateTimeToString();
                migrationTableItemView.GroupName = migrationTable.GroupName;
                migrationTableItemView.GroupOrder = migrationTable.GroupOrder;
                migrationTableItemView.MigrationName = migrationTable.MigrateName;
                migrationTableItemView.MigrationOrder = migrationTable.MigrateOrder;
                migrationTableItemView.MigrationResult = migrationTable.MigrateResult;
                migrationTableItemView.PackageName = migrationTable.PackageName;
                
                migrationTableItemView.RowVersion = migrationTable.RowVersion;
                return migrationTableItemView.GetMigrationTableItemViewValidation();
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion MigrationTableItemView
        #region ToDropDown
        public static SelectItem<MigrationTableItemView> ToDropDownItem(this MigrationTableItemView migrationTableItemView, bool excludeRowData = false)
        {
            try
            {
                SelectItem<MigrationTableItemView> selectItem = new SelectItem<MigrationTableItemView>();
                selectItem.Label = migrationTableItemView.MigrationName;
                selectItem.Value = migrationTableItemView.MigrationTableGUID;
                selectItem.RowData = excludeRowData ? null: migrationTableItemView;
                return selectItem;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static IEnumerable<SelectItem<MigrationTableItemView>> ToDropDownItem(this IEnumerable<MigrationTableItemView> migrationTableItemViews, bool excludeRowData = false)
        {
            try
            {
                List<SelectItem<MigrationTableItemView>> selectItems = new List<SelectItem<MigrationTableItemView>>();
                foreach (MigrationTableItemView item in migrationTableItemViews)
                {
                    selectItems.Add(item.ToDropDownItem(excludeRowData));
                }
                return selectItems;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion ToDropDown
    }
}

