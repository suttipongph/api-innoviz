using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class FreeTextInvoiceTableHandler
	{
		#region FreeTextInvoiceTableListView
		public static List<FreeTextInvoiceTableListView> GetFreeTextInvoiceTableListViewValidation(this List<FreeTextInvoiceTableListView> freeTextInvoiceTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return freeTextInvoiceTableListViews;
			}
			var result = new List<FreeTextInvoiceTableListView>();
			try
			{
				foreach (FreeTextInvoiceTableListView item in freeTextInvoiceTableListViews)
				{
					result.Add(item.GetFreeTextInvoiceTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static FreeTextInvoiceTableListView GetFreeTextInvoiceTableListViewValidation(this FreeTextInvoiceTableListView freeTextInvoiceTableListView)
		{
			try
			{
				freeTextInvoiceTableListView.RowAuthorize = freeTextInvoiceTableListView.GetListRowAuthorize();
				return freeTextInvoiceTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this FreeTextInvoiceTableListView freeTextInvoiceTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion FreeTextInvoiceTableListView
		#region FreeTextInvoiceTableItemView
		public static FreeTextInvoiceTableItemView GetFreeTextInvoiceTableItemViewValidation(this FreeTextInvoiceTableItemView freeTextInvoiceTableItemView)
		{
			try
			{
				freeTextInvoiceTableItemView.RowAuthorize = freeTextInvoiceTableItemView.GetItemRowAuthorize();
				return freeTextInvoiceTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this FreeTextInvoiceTableItemView freeTextInvoiceTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static FreeTextInvoiceTable ToFreeTextInvoiceTable(this FreeTextInvoiceTableItemView freeTextInvoiceTableItemView)
		{
			try
			{
				FreeTextInvoiceTable freeTextInvoiceTable = new FreeTextInvoiceTable();
				freeTextInvoiceTable.CompanyGUID = freeTextInvoiceTableItemView.CompanyGUID.StringToGuid();
				freeTextInvoiceTable.CreatedBy = freeTextInvoiceTableItemView.CreatedBy;
				freeTextInvoiceTable.CreatedDateTime = freeTextInvoiceTableItemView.CreatedDateTime.StringToSystemDateTime();
				freeTextInvoiceTable.ModifiedBy = freeTextInvoiceTableItemView.ModifiedBy;
				freeTextInvoiceTable.ModifiedDateTime = freeTextInvoiceTableItemView.ModifiedDateTime.StringToSystemDateTime();
				freeTextInvoiceTable.Owner = freeTextInvoiceTableItemView.Owner;
				freeTextInvoiceTable.OwnerBusinessUnitGUID = freeTextInvoiceTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				freeTextInvoiceTable.FreeTextInvoiceTableGUID = freeTextInvoiceTableItemView.FreeTextInvoiceTableGUID.StringToGuid();
				freeTextInvoiceTable.CNReasonGUID = freeTextInvoiceTableItemView.CNReasonGUID.StringToGuidNull();
				freeTextInvoiceTable.CreditAppTableGUID = freeTextInvoiceTableItemView.CreditAppTableGUID.StringToGuidNull();
				freeTextInvoiceTable.CurrencyGUID = freeTextInvoiceTableItemView.CurrencyGUID.StringToGuid();
				freeTextInvoiceTable.CustomerName = freeTextInvoiceTableItemView.CustomerName;
				freeTextInvoiceTable.CustomerTableGUID = freeTextInvoiceTableItemView.CustomerTableGUID.StringToGuid();
				freeTextInvoiceTable.Dimension1GUID = freeTextInvoiceTableItemView.Dimension1GUID.StringToGuidNull();
				freeTextInvoiceTable.Dimension2GUID = freeTextInvoiceTableItemView.Dimension2GUID.StringToGuidNull();
				freeTextInvoiceTable.Dimension3GUID = freeTextInvoiceTableItemView.Dimension3GUID.StringToGuidNull();
				freeTextInvoiceTable.Dimension4GUID = freeTextInvoiceTableItemView.Dimension4GUID.StringToGuidNull();
				freeTextInvoiceTable.Dimension5GUID = freeTextInvoiceTableItemView.Dimension5GUID.StringToGuidNull();
				freeTextInvoiceTable.DocumentStatusGUID = freeTextInvoiceTableItemView.DocumentStatusGUID.StringToGuidNull();
				freeTextInvoiceTable.DueDate = freeTextInvoiceTableItemView.DueDate.StringToDate();
				freeTextInvoiceTable.ExchangeRate = freeTextInvoiceTableItemView.ExchangeRate;
				freeTextInvoiceTable.FreeTextInvoiceId = freeTextInvoiceTableItemView.FreeTextInvoiceId;
				freeTextInvoiceTable.InvoiceAddress1 = freeTextInvoiceTableItemView.InvoiceAddress1;
				freeTextInvoiceTable.InvoiceAddress2 = freeTextInvoiceTableItemView.InvoiceAddress2;
				freeTextInvoiceTable.InvoiceAddressGUID = freeTextInvoiceTableItemView.InvoiceAddressGUID.StringToGuid();
				freeTextInvoiceTable.InvoiceAmount = freeTextInvoiceTableItemView.InvoiceAmount;
				freeTextInvoiceTable.InvoiceAmountBeforeTax = freeTextInvoiceTableItemView.InvoiceAmountBeforeTax;
				freeTextInvoiceTable.InvoiceRevenueTypeGUID = freeTextInvoiceTableItemView.InvoiceRevenueTypeGUID.StringToGuid();
				freeTextInvoiceTable.InvoiceTableGUID = freeTextInvoiceTableItemView.InvoiceTableGUID.StringToGuidNull();
				freeTextInvoiceTable.InvoiceText = freeTextInvoiceTableItemView.InvoiceText;
				freeTextInvoiceTable.InvoiceTypeGUID = freeTextInvoiceTableItemView.InvoiceTypeGUID.StringToGuid();
				freeTextInvoiceTable.IssuedDate = freeTextInvoiceTableItemView.IssuedDate.StringToDate();
				freeTextInvoiceTable.MailingInvoiceAddress1 = freeTextInvoiceTableItemView.MailingInvoiceAddress1;
				freeTextInvoiceTable.MailingInvoiceAddress2 = freeTextInvoiceTableItemView.MailingInvoiceAddress2;
				freeTextInvoiceTable.MailingInvoiceAddressGUID = freeTextInvoiceTableItemView.MailingInvoiceAddressGUID.StringToGuid();
				freeTextInvoiceTable.OrigInvoiceAmount = freeTextInvoiceTableItemView.OrigInvoiceAmount;
				freeTextInvoiceTable.OrigInvoiceId = freeTextInvoiceTableItemView.OrigInvoiceId;
				freeTextInvoiceTable.OrigTaxInvoiceAmount = freeTextInvoiceTableItemView.OrigTaxInvoiceAmount;
				freeTextInvoiceTable.OrigTaxInvoiceId = freeTextInvoiceTableItemView.OrigTaxInvoiceId;
				freeTextInvoiceTable.ProductType = freeTextInvoiceTableItemView.ProductType;
				freeTextInvoiceTable.Remark = freeTextInvoiceTableItemView.Remark;
				freeTextInvoiceTable.TaxAmount = freeTextInvoiceTableItemView.TaxAmount;
				freeTextInvoiceTable.TaxBranchId = freeTextInvoiceTableItemView.TaxBranchId;
				freeTextInvoiceTable.TaxBranchName = freeTextInvoiceTableItemView.TaxBranchName;
				freeTextInvoiceTable.TaxTableGUID = freeTextInvoiceTableItemView.TaxTableGUID.StringToGuidNull();
				freeTextInvoiceTable.WHTAmount = freeTextInvoiceTableItemView.WHTAmount;
				freeTextInvoiceTable.WithholdingTaxTableGUID = freeTextInvoiceTableItemView.WithholdingTaxTableGUID.StringToGuidNull();
				
				freeTextInvoiceTable.RowVersion = freeTextInvoiceTableItemView.RowVersion;
				return freeTextInvoiceTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<FreeTextInvoiceTable> ToFreeTextInvoiceTable(this IEnumerable<FreeTextInvoiceTableItemView> freeTextInvoiceTableItemViews)
		{
			try
			{
				List<FreeTextInvoiceTable> freeTextInvoiceTables = new List<FreeTextInvoiceTable>();
				foreach (FreeTextInvoiceTableItemView item in freeTextInvoiceTableItemViews)
				{
					freeTextInvoiceTables.Add(item.ToFreeTextInvoiceTable());
				}
				return freeTextInvoiceTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static FreeTextInvoiceTableItemView ToFreeTextInvoiceTableItemView(this FreeTextInvoiceTable freeTextInvoiceTable)
		{
			try
			{
				FreeTextInvoiceTableItemView freeTextInvoiceTableItemView = new FreeTextInvoiceTableItemView();
				freeTextInvoiceTableItemView.CompanyGUID = freeTextInvoiceTable.CompanyGUID.GuidNullToString();
				freeTextInvoiceTableItemView.CreatedBy = freeTextInvoiceTable.CreatedBy;
				freeTextInvoiceTableItemView.CreatedDateTime = freeTextInvoiceTable.CreatedDateTime.DateTimeToString();
				freeTextInvoiceTableItemView.ModifiedBy = freeTextInvoiceTable.ModifiedBy;
				freeTextInvoiceTableItemView.ModifiedDateTime = freeTextInvoiceTable.ModifiedDateTime.DateTimeToString();
				freeTextInvoiceTableItemView.Owner = freeTextInvoiceTable.Owner;
				freeTextInvoiceTableItemView.OwnerBusinessUnitGUID = freeTextInvoiceTable.OwnerBusinessUnitGUID.GuidNullToString();
				freeTextInvoiceTableItemView.FreeTextInvoiceTableGUID = freeTextInvoiceTable.FreeTextInvoiceTableGUID.GuidNullToString();
				freeTextInvoiceTableItemView.CNReasonGUID = freeTextInvoiceTable.CNReasonGUID.GuidNullToString();
				freeTextInvoiceTableItemView.CreditAppTableGUID = freeTextInvoiceTable.CreditAppTableGUID.GuidNullToString();
				freeTextInvoiceTableItemView.CurrencyGUID = freeTextInvoiceTable.CurrencyGUID.GuidNullToString();
				freeTextInvoiceTableItemView.CustomerName = freeTextInvoiceTable.CustomerName;
				freeTextInvoiceTableItemView.CustomerTableGUID = freeTextInvoiceTable.CustomerTableGUID.GuidNullToString();
				freeTextInvoiceTableItemView.Dimension1GUID = freeTextInvoiceTable.Dimension1GUID.GuidNullToString();
				freeTextInvoiceTableItemView.Dimension2GUID = freeTextInvoiceTable.Dimension2GUID.GuidNullToString();
				freeTextInvoiceTableItemView.Dimension3GUID = freeTextInvoiceTable.Dimension3GUID.GuidNullToString();
				freeTextInvoiceTableItemView.Dimension4GUID = freeTextInvoiceTable.Dimension4GUID.GuidNullToString();
				freeTextInvoiceTableItemView.Dimension5GUID = freeTextInvoiceTable.Dimension5GUID.GuidNullToString();
				freeTextInvoiceTableItemView.DocumentStatusGUID = freeTextInvoiceTable.DocumentStatusGUID.GuidNullToString();
				freeTextInvoiceTableItemView.DueDate = freeTextInvoiceTable.DueDate.DateToString();
				freeTextInvoiceTableItemView.ExchangeRate = freeTextInvoiceTable.ExchangeRate;
				freeTextInvoiceTableItemView.FreeTextInvoiceId = freeTextInvoiceTable.FreeTextInvoiceId;
				freeTextInvoiceTableItemView.InvoiceAddress1 = freeTextInvoiceTable.InvoiceAddress1;
				freeTextInvoiceTableItemView.InvoiceAddress2 = freeTextInvoiceTable.InvoiceAddress2;
				freeTextInvoiceTableItemView.InvoiceAddressGUID = freeTextInvoiceTable.InvoiceAddressGUID.GuidNullToString();
				freeTextInvoiceTableItemView.InvoiceAmount = freeTextInvoiceTable.InvoiceAmount;
				freeTextInvoiceTableItemView.InvoiceAmountBeforeTax = freeTextInvoiceTable.InvoiceAmountBeforeTax;
				freeTextInvoiceTableItemView.InvoiceRevenueTypeGUID = freeTextInvoiceTable.InvoiceRevenueTypeGUID.GuidNullToString();
				freeTextInvoiceTableItemView.InvoiceTableGUID = freeTextInvoiceTable.InvoiceTableGUID.GuidNullToString();
				freeTextInvoiceTableItemView.InvoiceText = freeTextInvoiceTable.InvoiceText;
				freeTextInvoiceTableItemView.InvoiceTypeGUID = freeTextInvoiceTable.InvoiceTypeGUID.GuidNullToString();
				freeTextInvoiceTableItemView.IssuedDate = freeTextInvoiceTable.IssuedDate.DateToString();
				freeTextInvoiceTableItemView.MailingInvoiceAddress1 = freeTextInvoiceTable.MailingInvoiceAddress1;
				freeTextInvoiceTableItemView.MailingInvoiceAddress2 = freeTextInvoiceTable.MailingInvoiceAddress2;
				freeTextInvoiceTableItemView.MailingInvoiceAddressGUID = freeTextInvoiceTable.MailingInvoiceAddressGUID.GuidNullToString();
				freeTextInvoiceTableItemView.OrigInvoiceAmount = freeTextInvoiceTable.OrigInvoiceAmount;
				freeTextInvoiceTableItemView.OrigInvoiceId = freeTextInvoiceTable.OrigInvoiceId;
				freeTextInvoiceTableItemView.OrigTaxInvoiceAmount = freeTextInvoiceTable.OrigTaxInvoiceAmount;
				freeTextInvoiceTableItemView.OrigTaxInvoiceId = freeTextInvoiceTable.OrigTaxInvoiceId;
				freeTextInvoiceTableItemView.ProductType = freeTextInvoiceTable.ProductType;
				freeTextInvoiceTableItemView.Remark = freeTextInvoiceTable.Remark;
				freeTextInvoiceTableItemView.TaxAmount = freeTextInvoiceTable.TaxAmount;
				freeTextInvoiceTableItemView.TaxBranchId = freeTextInvoiceTable.TaxBranchId;
				freeTextInvoiceTableItemView.TaxBranchName = freeTextInvoiceTable.TaxBranchName;
				freeTextInvoiceTableItemView.TaxTableGUID = freeTextInvoiceTable.TaxTableGUID.GuidNullToString();
				freeTextInvoiceTableItemView.WHTAmount = freeTextInvoiceTable.WHTAmount;
				freeTextInvoiceTableItemView.WithholdingTaxTableGUID = freeTextInvoiceTable.WithholdingTaxTableGUID.GuidNullToString();
				
				freeTextInvoiceTableItemView.RowVersion = freeTextInvoiceTable.RowVersion;
				return freeTextInvoiceTableItemView.GetFreeTextInvoiceTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion FreeTextInvoiceTableItemView
		#region ToDropDown
		public static SelectItem<FreeTextInvoiceTableItemView> ToDropDownItem(this FreeTextInvoiceTableItemView freeTextInvoiceTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<FreeTextInvoiceTableItemView> selectItem = new SelectItem<FreeTextInvoiceTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(freeTextInvoiceTableView.FreeTextInvoiceId, freeTextInvoiceTableView.IssuedDate.ToString());
				selectItem.Value = freeTextInvoiceTableView.FreeTextInvoiceTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: freeTextInvoiceTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<FreeTextInvoiceTableItemView>> ToDropDownItem(this IEnumerable<FreeTextInvoiceTableItemView> freeTextInvoiceTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<FreeTextInvoiceTableItemView>> selectItems = new List<SelectItem<FreeTextInvoiceTableItemView>>();
				foreach (FreeTextInvoiceTableItemView item in freeTextInvoiceTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

