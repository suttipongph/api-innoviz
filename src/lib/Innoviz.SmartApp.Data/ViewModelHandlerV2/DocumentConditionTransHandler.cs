using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class DocumentConditionTransHandler
	{
		#region DocumentConditionTransListView
		public static List<DocumentConditionTransListView> GetDocumentConditionTransListViewValidation(this List<DocumentConditionTransListView> documentConditionTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return documentConditionTransListViews;
			}
			var result = new List<DocumentConditionTransListView>();
			try
			{
				foreach (DocumentConditionTransListView item in documentConditionTransListViews)
				{
					result.Add(item.GetDocumentConditionTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static DocumentConditionTransListView GetDocumentConditionTransListViewValidation(this DocumentConditionTransListView documentConditionTransListView)
		{
			try
			{
				documentConditionTransListView.RowAuthorize = documentConditionTransListView.GetListRowAuthorize();
				return documentConditionTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this DocumentConditionTransListView documentConditionTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion DocumentConditionTransListView
		#region DocumentConditionTransItemView
		public static DocumentConditionTransItemView GetDocumentConditionTransItemViewValidation(this DocumentConditionTransItemView documentConditionTransItemView)
		{
			try
			{
				documentConditionTransItemView.RowAuthorize = documentConditionTransItemView.GetItemRowAuthorize();
				return documentConditionTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this DocumentConditionTransItemView documentConditionTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static DocumentConditionTrans ToDocumentConditionTrans(this DocumentConditionTransItemView documentConditionTransItemView)
		{
			try
			{
				DocumentConditionTrans documentConditionTrans = new DocumentConditionTrans();
				documentConditionTrans.CompanyGUID = documentConditionTransItemView.CompanyGUID.StringToGuid();
				documentConditionTrans.CreatedBy = documentConditionTransItemView.CreatedBy;
				documentConditionTrans.CreatedDateTime = documentConditionTransItemView.CreatedDateTime.StringToSystemDateTime();
				documentConditionTrans.ModifiedBy = documentConditionTransItemView.ModifiedBy;
				documentConditionTrans.ModifiedDateTime = documentConditionTransItemView.ModifiedDateTime.StringToSystemDateTime();
				documentConditionTrans.Owner = documentConditionTransItemView.Owner;
				documentConditionTrans.OwnerBusinessUnitGUID = documentConditionTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				documentConditionTrans.DocumentConditionTransGUID = documentConditionTransItemView.DocumentConditionTransGUID.StringToGuid();
				documentConditionTrans.DocConVerifyType = documentConditionTransItemView.DocConVerifyType;
				documentConditionTrans.DocumentTypeGUID = documentConditionTransItemView.DocumentTypeGUID.StringToGuid();
				documentConditionTrans.Inactive = documentConditionTransItemView.Inactive;
				documentConditionTrans.Mandatory = documentConditionTransItemView.Mandatory;
				documentConditionTrans.RefDocumentConditionTransGUID = documentConditionTransItemView.RefDocumentConditionTransGUID.StringToGuidNull();
				documentConditionTrans.RefGUID = documentConditionTransItemView.RefGUID.StringToGuid();
				documentConditionTrans.RefType = documentConditionTransItemView.RefType;
				
				documentConditionTrans.RowVersion = documentConditionTransItemView.RowVersion;
				return documentConditionTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<DocumentConditionTrans> ToDocumentConditionTrans(this IEnumerable<DocumentConditionTransItemView> documentConditionTransItemViews)
		{
			try
			{
				List<DocumentConditionTrans> documentConditionTranss = new List<DocumentConditionTrans>();
				foreach (DocumentConditionTransItemView item in documentConditionTransItemViews)
				{
					documentConditionTranss.Add(item.ToDocumentConditionTrans());
				}
				return documentConditionTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static DocumentConditionTransItemView ToDocumentConditionTransItemView(this DocumentConditionTrans documentConditionTrans)
		{
			try
			{
				DocumentConditionTransItemView documentConditionTransItemView = new DocumentConditionTransItemView();
				documentConditionTransItemView.CompanyGUID = documentConditionTrans.CompanyGUID.GuidNullToString();
				documentConditionTransItemView.CreatedBy = documentConditionTrans.CreatedBy;
				documentConditionTransItemView.CreatedDateTime = documentConditionTrans.CreatedDateTime.DateTimeToString();
				documentConditionTransItemView.ModifiedBy = documentConditionTrans.ModifiedBy;
				documentConditionTransItemView.ModifiedDateTime = documentConditionTrans.ModifiedDateTime.DateTimeToString();
				documentConditionTransItemView.Owner = documentConditionTrans.Owner;
				documentConditionTransItemView.OwnerBusinessUnitGUID = documentConditionTrans.OwnerBusinessUnitGUID.GuidNullToString();
				documentConditionTransItemView.DocumentConditionTransGUID = documentConditionTrans.DocumentConditionTransGUID.GuidNullToString();
				documentConditionTransItemView.DocConVerifyType = documentConditionTrans.DocConVerifyType;
				documentConditionTransItemView.DocumentTypeGUID = documentConditionTrans.DocumentTypeGUID.GuidNullToString();
				documentConditionTransItemView.Inactive = documentConditionTrans.Inactive;
				documentConditionTransItemView.Mandatory = documentConditionTrans.Mandatory;
				documentConditionTransItemView.RefDocumentConditionTransGUID = documentConditionTrans.RefDocumentConditionTransGUID.GuidNullToString();
				documentConditionTransItemView.RefGUID = documentConditionTrans.RefGUID.GuidNullToString();
				documentConditionTransItemView.RefType = documentConditionTrans.RefType;
				
				documentConditionTransItemView.RowVersion = documentConditionTrans.RowVersion;
				return documentConditionTransItemView.GetDocumentConditionTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion DocumentConditionTransItemView
		#region ToDropDown
		public static SelectItem<DocumentConditionTransItemView> ToDropDownItem(this DocumentConditionTransItemView documentConditionTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<DocumentConditionTransItemView> selectItem = new SelectItem<DocumentConditionTransItemView>();
				selectItem.Label = null;
				selectItem.Value = documentConditionTransView.DocumentConditionTransGUID;
				selectItem.RowData = excludeRowData ? null: documentConditionTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<DocumentConditionTransItemView>> ToDropDownItem(this IEnumerable<DocumentConditionTransItemView> documentConditionTransItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<DocumentConditionTransItemView>> selectItems = new List<SelectItem<DocumentConditionTransItemView>>();
				foreach (DocumentConditionTransItemView item in documentConditionTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

