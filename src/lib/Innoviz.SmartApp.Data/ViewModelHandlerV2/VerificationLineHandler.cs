using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class VerificationLineHandler
	{
		#region VerificationLineListView
		public static List<VerificationLineListView> GetVerificationLineListViewValidation(this List<VerificationLineListView> verificationLineListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return verificationLineListViews;
			}
			var result = new List<VerificationLineListView>();
			try
			{
				foreach (VerificationLineListView item in verificationLineListViews)
				{
					result.Add(item.GetVerificationLineListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static VerificationLineListView GetVerificationLineListViewValidation(this VerificationLineListView verificationLineListView)
		{
			try
			{
				verificationLineListView.RowAuthorize = verificationLineListView.GetListRowAuthorize();
				return verificationLineListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this VerificationLineListView verificationLineListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion VerificationLineListView
		#region VerificationLineItemView
		public static VerificationLineItemView GetVerificationLineItemViewValidation(this VerificationLineItemView verificationLineItemView)
		{
			try
			{
				verificationLineItemView.RowAuthorize = verificationLineItemView.GetItemRowAuthorize();
				return verificationLineItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this VerificationLineItemView verificationLineItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static VerificationLine ToVerificationLine(this VerificationLineItemView verificationLineItemView)
		{
			try
			{
				VerificationLine verificationLine = new VerificationLine();
				verificationLine.CompanyGUID = verificationLineItemView.CompanyGUID.StringToGuid();
				verificationLine.CreatedBy = verificationLineItemView.CreatedBy;
				verificationLine.CreatedDateTime = verificationLineItemView.CreatedDateTime.StringToSystemDateTime();
				verificationLine.ModifiedBy = verificationLineItemView.ModifiedBy;
				verificationLine.ModifiedDateTime = verificationLineItemView.ModifiedDateTime.StringToSystemDateTime();
				verificationLine.Owner = verificationLineItemView.Owner;
				verificationLine.OwnerBusinessUnitGUID = verificationLineItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				verificationLine.VerificationLineGUID = verificationLineItemView.VerificationLineGUID.StringToGuid();
				verificationLine.Pass = verificationLineItemView.Pass;
				verificationLine.Remark = verificationLineItemView.Remark;
				verificationLine.VendorTableGUID = verificationLineItemView.VendorTableGUID.StringToGuidNull();
				verificationLine.VerificationTableGUID = verificationLineItemView.VerificationTableGUID.StringToGuid();
				verificationLine.VerificationTypeGUID = verificationLineItemView.VerificationTypeGUID.StringToGuid();
				
				verificationLine.RowVersion = verificationLineItemView.RowVersion;
				return verificationLine;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<VerificationLine> ToVerificationLine(this IEnumerable<VerificationLineItemView> verificationLineItemViews)
		{
			try
			{
				List<VerificationLine> verificationLines = new List<VerificationLine>();
				foreach (VerificationLineItemView item in verificationLineItemViews)
				{
					verificationLines.Add(item.ToVerificationLine());
				}
				return verificationLines;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static VerificationLineItemView ToVerificationLineItemView(this VerificationLine verificationLine)
		{
			try
			{
				VerificationLineItemView verificationLineItemView = new VerificationLineItemView();
				verificationLineItemView.CompanyGUID = verificationLine.CompanyGUID.GuidNullToString();
				verificationLineItemView.CreatedBy = verificationLine.CreatedBy;
				verificationLineItemView.CreatedDateTime = verificationLine.CreatedDateTime.DateTimeToString();
				verificationLineItemView.ModifiedBy = verificationLine.ModifiedBy;
				verificationLineItemView.ModifiedDateTime = verificationLine.ModifiedDateTime.DateTimeToString();
				verificationLineItemView.Owner = verificationLine.Owner;
				verificationLineItemView.OwnerBusinessUnitGUID = verificationLine.OwnerBusinessUnitGUID.GuidNullToString();
				verificationLineItemView.VerificationLineGUID = verificationLine.VerificationLineGUID.GuidNullToString();
				verificationLineItemView.Pass = verificationLine.Pass;
				verificationLineItemView.Remark = verificationLine.Remark;
				verificationLineItemView.VendorTableGUID = verificationLine.VendorTableGUID.GuidNullToString();
				verificationLineItemView.VerificationTableGUID = verificationLine.VerificationTableGUID.GuidNullToString();
				verificationLineItemView.VerificationTypeGUID = verificationLine.VerificationTypeGUID.GuidNullToString();
				
				verificationLineItemView.RowVersion = verificationLine.RowVersion;
				return verificationLineItemView.GetVerificationLineItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion VerificationLineItemView
		#region ToDropDown
		public static SelectItem<VerificationLineItemView> ToDropDownItem(this VerificationLineItemView verificationLineView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<VerificationLineItemView> selectItem = new SelectItem<VerificationLineItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(verificationLineView.VerificationTableGUID.ToString(), verificationLineView.VerificationTypeGUID.ToString());
				selectItem.Value = verificationLineView.VerificationLineGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: verificationLineView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<VerificationLineItemView>> ToDropDownItem(this IEnumerable<VerificationLineItemView> verificationLineItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<VerificationLineItemView>> selectItems = new List<SelectItem<VerificationLineItemView>>();
				foreach (VerificationLineItemView item in verificationLineItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

