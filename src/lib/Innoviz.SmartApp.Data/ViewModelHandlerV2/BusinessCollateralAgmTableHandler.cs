using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class BusinessCollateralAgmTableHandler
	{
		#region BusinessCollateralAgmTableListView
		public static List<BusinessCollateralAgmTableListView> GetBusinessCollateralAgmTableListViewValidation(this List<BusinessCollateralAgmTableListView> businessCollateralAgmTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return businessCollateralAgmTableListViews;
			}
			var result = new List<BusinessCollateralAgmTableListView>();
			try
			{
				foreach (BusinessCollateralAgmTableListView item in businessCollateralAgmTableListViews)
				{
					result.Add(item.GetBusinessCollateralAgmTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BusinessCollateralAgmTableListView GetBusinessCollateralAgmTableListViewValidation(this BusinessCollateralAgmTableListView businessCollateralAgmTableListView)
		{
			try
			{
				businessCollateralAgmTableListView.RowAuthorize = businessCollateralAgmTableListView.GetListRowAuthorize();
				return businessCollateralAgmTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this BusinessCollateralAgmTableListView businessCollateralAgmTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion BusinessCollateralAgmTableListView
		#region BusinessCollateralAgmTableItemView
		public static BusinessCollateralAgmTableItemView GetBusinessCollateralAgmTableItemViewValidation(this BusinessCollateralAgmTableItemView businessCollateralAgmTableItemView)
		{
			try
			{
				businessCollateralAgmTableItemView.RowAuthorize = businessCollateralAgmTableItemView.GetItemRowAuthorize();
				return businessCollateralAgmTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this BusinessCollateralAgmTableItemView businessCollateralAgmTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static BusinessCollateralAgmTable ToBusinessCollateralAgmTable(this BusinessCollateralAgmTableItemView businessCollateralAgmTableItemView)
		{
			try
			{
				BusinessCollateralAgmTable businessCollateralAgmTable = new BusinessCollateralAgmTable();
				businessCollateralAgmTable.CompanyGUID = businessCollateralAgmTableItemView.CompanyGUID.StringToGuid();
				businessCollateralAgmTable.CreatedBy = businessCollateralAgmTableItemView.CreatedBy;
				businessCollateralAgmTable.CreatedDateTime = businessCollateralAgmTableItemView.CreatedDateTime.StringToSystemDateTime();
				businessCollateralAgmTable.ModifiedBy = businessCollateralAgmTableItemView.ModifiedBy;
				businessCollateralAgmTable.ModifiedDateTime = businessCollateralAgmTableItemView.ModifiedDateTime.StringToSystemDateTime();
				businessCollateralAgmTable.Owner = businessCollateralAgmTableItemView.Owner;
				businessCollateralAgmTable.OwnerBusinessUnitGUID = businessCollateralAgmTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				businessCollateralAgmTable.BusinessCollateralAgmTableGUID = businessCollateralAgmTableItemView.BusinessCollateralAgmTableGUID.StringToGuid();
				businessCollateralAgmTable.AgreementDate = businessCollateralAgmTableItemView.AgreementDate.StringToDate();
				businessCollateralAgmTable.AgreementDocType = businessCollateralAgmTableItemView.AgreementDocType;
				businessCollateralAgmTable.AgreementExtension = businessCollateralAgmTableItemView.AgreementExtension;
				businessCollateralAgmTable.BusinessCollateralAgmId = businessCollateralAgmTableItemView.BusinessCollateralAgmId;
				businessCollateralAgmTable.CreditAppRequestTableGUID = businessCollateralAgmTableItemView.CreditAppRequestTableGUID.StringToGuid();
				businessCollateralAgmTable.CreditAppTableGUID = businessCollateralAgmTableItemView.CreditAppTableGUID.StringToGuidNull();
				businessCollateralAgmTable.CreditLimitTypeGUID = businessCollateralAgmTableItemView.CreditLimitTypeGUID.StringToGuidNull();
				businessCollateralAgmTable.ConsortiumTableGUID = businessCollateralAgmTableItemView.ConsortiumTableGUID.StringToGuidNull();
				businessCollateralAgmTable.CustomerAltName = businessCollateralAgmTableItemView.CustomerAltName;
				businessCollateralAgmTable.CustomerName = businessCollateralAgmTableItemView.CustomerName;
				businessCollateralAgmTable.CustomerTableGUID = businessCollateralAgmTableItemView.CustomerTableGUID.StringToGuid();
				businessCollateralAgmTable.Description = businessCollateralAgmTableItemView.Description;
				businessCollateralAgmTable.DocumentReasonGUID = businessCollateralAgmTableItemView.DocumentReasonGUID.StringToGuidNull();
				businessCollateralAgmTable.DocumentStatusGUID = businessCollateralAgmTableItemView.DocumentStatusGUID.StringToGuid();
				businessCollateralAgmTable.InternalBusinessCollateralAgmId = businessCollateralAgmTableItemView.InternalBusinessCollateralAgmId;
				businessCollateralAgmTable.ProductType = businessCollateralAgmTableItemView.ProductType;
				businessCollateralAgmTable.RefBusinessCollateralAgmTableGUID = businessCollateralAgmTableItemView.RefBusinessCollateralAgmTableGUID.StringToGuidNull();
				businessCollateralAgmTable.SigningDate = businessCollateralAgmTableItemView.SigningDate.StringNullToDateNull();
				businessCollateralAgmTable.DBDRegistrationAmount = businessCollateralAgmTableItemView.DBDRegistrationAmount;
				businessCollateralAgmTable.DBDRegistrationDate = businessCollateralAgmTableItemView.DBDRegistrationDate.StringNullToDateNull();
				businessCollateralAgmTable.DBDRegistrationDescription = businessCollateralAgmTableItemView.DBDRegistrationDescription;
				businessCollateralAgmTable.DBDRegistrationId = businessCollateralAgmTableItemView.DBDRegistrationId;
				businessCollateralAgmTable.AttachmentRemark = businessCollateralAgmTableItemView.AttachmentRemark;
				businessCollateralAgmTable.Remark = businessCollateralAgmTableItemView.Remark;
				
				businessCollateralAgmTable.RowVersion = businessCollateralAgmTableItemView.RowVersion;
				return businessCollateralAgmTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<BusinessCollateralAgmTable> ToBusinessCollateralAgmTable(this IEnumerable<BusinessCollateralAgmTableItemView> businessCollateralAgmTableItemViews)
		{
			try
			{
				List<BusinessCollateralAgmTable> businessCollateralAgmTables = new List<BusinessCollateralAgmTable>();
				foreach (BusinessCollateralAgmTableItemView item in businessCollateralAgmTableItemViews)
				{
					businessCollateralAgmTables.Add(item.ToBusinessCollateralAgmTable());
				}
				return businessCollateralAgmTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BusinessCollateralAgmTableItemView ToBusinessCollateralAgmTableItemView(this BusinessCollateralAgmTable businessCollateralAgmTable)
		{
			try
			{
				BusinessCollateralAgmTableItemView businessCollateralAgmTableItemView = new BusinessCollateralAgmTableItemView();
				businessCollateralAgmTableItemView.CompanyGUID = businessCollateralAgmTable.CompanyGUID.GuidNullToString();
				businessCollateralAgmTableItemView.CreatedBy = businessCollateralAgmTable.CreatedBy;
				businessCollateralAgmTableItemView.CreatedDateTime = businessCollateralAgmTable.CreatedDateTime.DateTimeToString();
				businessCollateralAgmTableItemView.ModifiedBy = businessCollateralAgmTable.ModifiedBy;
				businessCollateralAgmTableItemView.ModifiedDateTime = businessCollateralAgmTable.ModifiedDateTime.DateTimeToString();
				businessCollateralAgmTableItemView.Owner = businessCollateralAgmTable.Owner;
				businessCollateralAgmTableItemView.OwnerBusinessUnitGUID = businessCollateralAgmTable.OwnerBusinessUnitGUID.GuidNullToString();
				businessCollateralAgmTableItemView.BusinessCollateralAgmTableGUID = businessCollateralAgmTable.BusinessCollateralAgmTableGUID.GuidNullToString();
				businessCollateralAgmTableItemView.AgreementDate = businessCollateralAgmTable.AgreementDate.DateToString();
				businessCollateralAgmTableItemView.AgreementDocType = businessCollateralAgmTable.AgreementDocType;
				businessCollateralAgmTableItemView.AgreementExtension = businessCollateralAgmTable.AgreementExtension;
				businessCollateralAgmTableItemView.BusinessCollateralAgmId = businessCollateralAgmTable.BusinessCollateralAgmId;
				businessCollateralAgmTableItemView.CreditAppRequestTableGUID = businessCollateralAgmTable.CreditAppRequestTableGUID.GuidNullToString();
				businessCollateralAgmTableItemView.CreditAppTableGUID = businessCollateralAgmTable.CreditAppTableGUID.GuidNullToString();
				businessCollateralAgmTableItemView.CreditLimitTypeGUID = businessCollateralAgmTable.CreditLimitTypeGUID.GuidNullToString();
				businessCollateralAgmTableItemView.ConsortiumTableGUID = businessCollateralAgmTable.ConsortiumTableGUID.GuidNullToString();
				businessCollateralAgmTableItemView.CustomerAltName = businessCollateralAgmTable.CustomerAltName;
				businessCollateralAgmTableItemView.CustomerName = businessCollateralAgmTable.CustomerName;
				businessCollateralAgmTableItemView.CustomerTableGUID = businessCollateralAgmTable.CustomerTableGUID.GuidNullToString();
				businessCollateralAgmTableItemView.Description = businessCollateralAgmTable.Description;
				businessCollateralAgmTableItemView.DocumentReasonGUID = businessCollateralAgmTable.DocumentReasonGUID.GuidNullToString();
				businessCollateralAgmTableItemView.DocumentStatusGUID = businessCollateralAgmTable.DocumentStatusGUID.GuidNullToString();
				businessCollateralAgmTableItemView.InternalBusinessCollateralAgmId = businessCollateralAgmTable.InternalBusinessCollateralAgmId;
				businessCollateralAgmTableItemView.ProductType = businessCollateralAgmTable.ProductType;
				businessCollateralAgmTableItemView.RefBusinessCollateralAgmTableGUID = businessCollateralAgmTable.RefBusinessCollateralAgmTableGUID.GuidNullToString();
				businessCollateralAgmTableItemView.SigningDate = businessCollateralAgmTable.SigningDate.DateNullToString();
				businessCollateralAgmTableItemView.AttachmentRemark = businessCollateralAgmTable.AttachmentRemark;
				businessCollateralAgmTableItemView.DBDRegistrationAmount = businessCollateralAgmTable.DBDRegistrationAmount;
				businessCollateralAgmTableItemView.DBDRegistrationDate = businessCollateralAgmTable.DBDRegistrationDate.DateNullToString();
				businessCollateralAgmTableItemView.DBDRegistrationDescription = businessCollateralAgmTable.DBDRegistrationDescription;
				businessCollateralAgmTableItemView.DBDRegistrationId = businessCollateralAgmTable.DBDRegistrationId;
				
				businessCollateralAgmTableItemView.RowVersion = businessCollateralAgmTable.RowVersion;
				return businessCollateralAgmTableItemView.GetBusinessCollateralAgmTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion BusinessCollateralAgmTableItemView
		#region ToDropDown
		public static SelectItem<BusinessCollateralAgmTableItemView> ToDropDownItem(this BusinessCollateralAgmTableItemView businessCollateralAgmTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<BusinessCollateralAgmTableItemView> selectItem = new SelectItem<BusinessCollateralAgmTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(businessCollateralAgmTableView.InternalBusinessCollateralAgmId,businessCollateralAgmTableView.Description);
		     	selectItem.Value = businessCollateralAgmTableView.BusinessCollateralAgmTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: businessCollateralAgmTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<BusinessCollateralAgmTableItemView>> ToDropDownItem(this IEnumerable<BusinessCollateralAgmTableItemView> businessCollateralAgmTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<BusinessCollateralAgmTableItemView>> selectItems = new List<SelectItem<BusinessCollateralAgmTableItemView>>();
				foreach (BusinessCollateralAgmTableItemView item in businessCollateralAgmTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

