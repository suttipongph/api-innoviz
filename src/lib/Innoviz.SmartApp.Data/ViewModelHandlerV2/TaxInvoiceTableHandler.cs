using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class TaxInvoiceTableHandler
	{
		#region TaxInvoiceTableListView
		public static List<TaxInvoiceTableListView> GetTaxInvoiceTableListViewValidation(this List<TaxInvoiceTableListView> taxInvoiceTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return taxInvoiceTableListViews;
			}
			var result = new List<TaxInvoiceTableListView>();
			try
			{
				foreach (TaxInvoiceTableListView item in taxInvoiceTableListViews)
				{
					result.Add(item.GetTaxInvoiceTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static TaxInvoiceTableListView GetTaxInvoiceTableListViewValidation(this TaxInvoiceTableListView taxInvoiceTableListView)
		{
			try
			{
				taxInvoiceTableListView.RowAuthorize = taxInvoiceTableListView.GetListRowAuthorize();
				return taxInvoiceTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this TaxInvoiceTableListView taxInvoiceTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion TaxInvoiceTableListView
		#region TaxInvoiceTableItemView
		public static TaxInvoiceTableItemView GetTaxInvoiceTableItemViewValidation(this TaxInvoiceTableItemView taxInvoiceTableItemView)
		{
			try
			{
				taxInvoiceTableItemView.RowAuthorize = taxInvoiceTableItemView.GetItemRowAuthorize();
				return taxInvoiceTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this TaxInvoiceTableItemView taxInvoiceTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static TaxInvoiceTable ToTaxInvoiceTable(this TaxInvoiceTableItemView taxInvoiceTableItemView)
		{
			try
			{
				TaxInvoiceTable taxInvoiceTable = new TaxInvoiceTable();
				taxInvoiceTable.CompanyGUID = taxInvoiceTableItemView.CompanyGUID.StringToGuid();
				taxInvoiceTable.BranchGUID = taxInvoiceTableItemView.BranchGUID.StringToGuid();
				taxInvoiceTable.CreatedBy = taxInvoiceTableItemView.CreatedBy;
				taxInvoiceTable.CreatedDateTime = taxInvoiceTableItemView.CreatedDateTime.StringToSystemDateTime();
				taxInvoiceTable.ModifiedBy = taxInvoiceTableItemView.ModifiedBy;
				taxInvoiceTable.ModifiedDateTime = taxInvoiceTableItemView.ModifiedDateTime.StringToSystemDateTime();
				taxInvoiceTable.Owner = taxInvoiceTableItemView.Owner;
				taxInvoiceTable.OwnerBusinessUnitGUID = taxInvoiceTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				taxInvoiceTable.TaxInvoiceTableGUID = taxInvoiceTableItemView.TaxInvoiceTableGUID.StringToGuid();
				taxInvoiceTable.CurrencyGUID = taxInvoiceTableItemView.CurrencyGUID.StringToGuid();
				taxInvoiceTable.CustomerName = taxInvoiceTableItemView.CustomerName;
				taxInvoiceTable.CustomerTableGUID = taxInvoiceTableItemView.CustomerTableGUID.StringToGuid();
				taxInvoiceTable.Dimension1GUID = taxInvoiceTableItemView.Dimension1GUID.StringToGuidNull();
				taxInvoiceTable.Dimension2GUID = taxInvoiceTableItemView.Dimension2GUID.StringToGuidNull();
				taxInvoiceTable.Dimension3GUID = taxInvoiceTableItemView.Dimension3GUID.StringToGuidNull();
				taxInvoiceTable.Dimension4GUID = taxInvoiceTableItemView.Dimension4GUID.StringToGuidNull();
				taxInvoiceTable.Dimension5GUID = taxInvoiceTableItemView.Dimension5GUID.StringToGuidNull();
				taxInvoiceTable.DocumentId = taxInvoiceTableItemView.DocumentId;
				taxInvoiceTable.CNReasonGUID = taxInvoiceTableItemView.CNReasonGUID.StringToGuidNull();
				taxInvoiceTable.DocumentStatusGUID = taxInvoiceTableItemView.DocumentStatusGUID.StringToGuid();
				taxInvoiceTable.DueDate = taxInvoiceTableItemView.DueDate.StringToDate();
				taxInvoiceTable.ExchangeRate = taxInvoiceTableItemView.ExchangeRate;
				taxInvoiceTable.InvoiceAddress1 = taxInvoiceTableItemView.InvoiceAddress1;
				taxInvoiceTable.InvoiceAddress2 = taxInvoiceTableItemView.InvoiceAddress2;
				taxInvoiceTable.InvoiceAmount = taxInvoiceTableItemView.InvoiceAmount;
				taxInvoiceTable.InvoiceAmountBeforeTax = taxInvoiceTableItemView.InvoiceAmountBeforeTax;
				taxInvoiceTable.InvoiceAmountBeforeTaxMST = taxInvoiceTableItemView.InvoiceAmountBeforeTaxMST;
				taxInvoiceTable.InvoiceAmountMST = taxInvoiceTableItemView.InvoiceAmountMST;
				taxInvoiceTable.InvoiceTableGUID = taxInvoiceTableItemView.InvoiceTableGUID.StringToGuid();
				taxInvoiceTable.InvoiceTypeGUID = taxInvoiceTableItemView.InvoiceTypeGUID.StringToGuid();
				taxInvoiceTable.IssuedDate = taxInvoiceTableItemView.IssuedDate.StringToDate();
				taxInvoiceTable.MailingInvoiceAddress1 = taxInvoiceTableItemView.MailingInvoiceAddress1;
				taxInvoiceTable.MailingInvoiceAddress2 = taxInvoiceTableItemView.MailingInvoiceAddress2;
				taxInvoiceTable.MethodOfPaymentGUID = taxInvoiceTableItemView.MethodOfPaymentGUID.StringToGuidNull();
				taxInvoiceTable.OrigTaxInvoiceAmount = taxInvoiceTableItemView.OrigTaxInvoiceAmount;
				taxInvoiceTable.OrigTaxInvoiceId = taxInvoiceTableItemView.OrigTaxInvoiceId;
				taxInvoiceTable.TaxInvoiceRefGUID = taxInvoiceTableItemView.TaxInvoiceRefGUID.StringToGuid();
				taxInvoiceTable.RefTaxInvoiceGUID = taxInvoiceTableItemView.RefTaxInvoiceGUID.StringToGuidNull();
				taxInvoiceTable.TaxInvoiceRefType = taxInvoiceTableItemView.TaxInvoiceRefType;
				taxInvoiceTable.Remark = taxInvoiceTableItemView.Remark;
				taxInvoiceTable.TaxAmount = taxInvoiceTableItemView.TaxAmount;
				taxInvoiceTable.TaxAmountMST = taxInvoiceTableItemView.TaxAmountMST;
				taxInvoiceTable.TaxBranchId = taxInvoiceTableItemView.TaxBranchId;
				taxInvoiceTable.TaxBranchName = taxInvoiceTableItemView.TaxBranchName;
				taxInvoiceTable.TaxInvoiceId = taxInvoiceTableItemView.TaxInvoiceId;
				
				taxInvoiceTable.RowVersion = taxInvoiceTableItemView.RowVersion;
				return taxInvoiceTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<TaxInvoiceTable> ToTaxInvoiceTable(this IEnumerable<TaxInvoiceTableItemView> taxInvoiceTableItemViews)
		{
			try
			{
				List<TaxInvoiceTable> taxInvoiceTables = new List<TaxInvoiceTable>();
				foreach (TaxInvoiceTableItemView item in taxInvoiceTableItemViews)
				{
					taxInvoiceTables.Add(item.ToTaxInvoiceTable());
				}
				return taxInvoiceTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static TaxInvoiceTableItemView ToTaxInvoiceTableItemView(this TaxInvoiceTable taxInvoiceTable)
		{
			try
			{
				TaxInvoiceTableItemView taxInvoiceTableItemView = new TaxInvoiceTableItemView();
				taxInvoiceTableItemView.CompanyGUID = taxInvoiceTable.CompanyGUID.GuidNullToString();
				taxInvoiceTableItemView.BranchGUID = taxInvoiceTable.BranchGUID.GuidNullToString();
				taxInvoiceTableItemView.CreatedBy = taxInvoiceTable.CreatedBy;
				taxInvoiceTableItemView.CreatedDateTime = taxInvoiceTable.CreatedDateTime.DateTimeToString();
				taxInvoiceTableItemView.ModifiedBy = taxInvoiceTable.ModifiedBy;
				taxInvoiceTableItemView.ModifiedDateTime = taxInvoiceTable.ModifiedDateTime.DateTimeToString();
				taxInvoiceTableItemView.Owner = taxInvoiceTable.Owner;
				taxInvoiceTableItemView.OwnerBusinessUnitGUID = taxInvoiceTable.OwnerBusinessUnitGUID.GuidNullToString();
				taxInvoiceTableItemView.TaxInvoiceTableGUID = taxInvoiceTable.TaxInvoiceTableGUID.GuidNullToString();
				taxInvoiceTableItemView.CurrencyGUID = taxInvoiceTable.CurrencyGUID.GuidNullToString();
				taxInvoiceTableItemView.CustomerName = taxInvoiceTable.CustomerName;
				taxInvoiceTableItemView.CustomerTableGUID = taxInvoiceTable.CustomerTableGUID.GuidNullToString();
				taxInvoiceTableItemView.Dimension1GUID = taxInvoiceTable.Dimension1GUID.GuidNullToString();
				taxInvoiceTableItemView.Dimension2GUID = taxInvoiceTable.Dimension2GUID.GuidNullToString();
				taxInvoiceTableItemView.Dimension3GUID = taxInvoiceTable.Dimension3GUID.GuidNullToString();
				taxInvoiceTableItemView.Dimension4GUID = taxInvoiceTable.Dimension4GUID.GuidNullToString();
				taxInvoiceTableItemView.Dimension5GUID = taxInvoiceTable.Dimension5GUID.GuidNullToString();
				taxInvoiceTableItemView.DocumentId = taxInvoiceTable.DocumentId;
				taxInvoiceTableItemView.CNReasonGUID = taxInvoiceTable.CNReasonGUID.GuidNullToString();
				taxInvoiceTableItemView.DocumentStatusGUID = taxInvoiceTable.DocumentStatusGUID.GuidNullToString();
				taxInvoiceTableItemView.DueDate = taxInvoiceTable.DueDate.DateToString();
				taxInvoiceTableItemView.ExchangeRate = taxInvoiceTable.ExchangeRate;
				taxInvoiceTableItemView.InvoiceAddress1 = taxInvoiceTable.InvoiceAddress1;
				taxInvoiceTableItemView.InvoiceAddress2 = taxInvoiceTable.InvoiceAddress2;
				taxInvoiceTableItemView.InvoiceAmount = taxInvoiceTable.InvoiceAmount;
				taxInvoiceTableItemView.InvoiceAmountBeforeTax = taxInvoiceTable.InvoiceAmountBeforeTax;
				taxInvoiceTableItemView.InvoiceAmountBeforeTaxMST = taxInvoiceTable.InvoiceAmountBeforeTaxMST;
				taxInvoiceTableItemView.InvoiceAmountMST = taxInvoiceTable.InvoiceAmountMST;
				taxInvoiceTableItemView.InvoiceTableGUID = taxInvoiceTable.InvoiceTableGUID.GuidNullToString();
				taxInvoiceTableItemView.InvoiceTypeGUID = taxInvoiceTable.InvoiceTypeGUID.GuidNullToString();
				taxInvoiceTableItemView.IssuedDate = taxInvoiceTable.IssuedDate.DateToString();
				taxInvoiceTableItemView.MailingInvoiceAddress1 = taxInvoiceTable.MailingInvoiceAddress1;
				taxInvoiceTableItemView.MailingInvoiceAddress2 = taxInvoiceTable.MailingInvoiceAddress2;
				taxInvoiceTableItemView.MethodOfPaymentGUID = taxInvoiceTable.MethodOfPaymentGUID.GuidNullToString();
				taxInvoiceTableItemView.OrigTaxInvoiceAmount = taxInvoiceTable.OrigTaxInvoiceAmount;
				taxInvoiceTableItemView.OrigTaxInvoiceId = taxInvoiceTable.OrigTaxInvoiceId;
				taxInvoiceTableItemView.TaxInvoiceRefGUID = taxInvoiceTable.TaxInvoiceRefGUID.GuidNullToString();
				taxInvoiceTableItemView.RefTaxInvoiceGUID = taxInvoiceTable.RefTaxInvoiceGUID.GuidNullToString();
				taxInvoiceTableItemView.TaxInvoiceRefType = taxInvoiceTable.TaxInvoiceRefType;
				taxInvoiceTableItemView.Remark = taxInvoiceTable.Remark;
				taxInvoiceTableItemView.TaxAmount = taxInvoiceTable.TaxAmount;
				taxInvoiceTableItemView.TaxAmountMST = taxInvoiceTable.TaxAmountMST;
				taxInvoiceTableItemView.TaxBranchId = taxInvoiceTable.TaxBranchId;
				taxInvoiceTableItemView.TaxBranchName = taxInvoiceTable.TaxBranchName;
				taxInvoiceTableItemView.TaxInvoiceId = taxInvoiceTable.TaxInvoiceId;
				
				taxInvoiceTableItemView.RowVersion = taxInvoiceTable.RowVersion;
				return taxInvoiceTableItemView.GetTaxInvoiceTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion TaxInvoiceTableItemView
		#region ToDropDown
		public static SelectItem<TaxInvoiceTableItemView> ToDropDownItem(this TaxInvoiceTableItemView taxInvoiceTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<TaxInvoiceTableItemView> selectItem = new SelectItem<TaxInvoiceTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(taxInvoiceTableView.TaxInvoiceId, taxInvoiceTableView.IssuedDate.ToString());
				selectItem.Value = taxInvoiceTableView.TaxInvoiceTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: taxInvoiceTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<TaxInvoiceTableItemView>> ToDropDownItem(this IEnumerable<TaxInvoiceTableItemView> taxInvoiceTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<TaxInvoiceTableItemView>> selectItems = new List<SelectItem<TaxInvoiceTableItemView>>();
				foreach (TaxInvoiceTableItemView item in taxInvoiceTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

