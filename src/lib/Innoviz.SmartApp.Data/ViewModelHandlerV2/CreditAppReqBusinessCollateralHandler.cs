using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CreditAppReqBusinessCollateralHandler
	{
		#region CreditAppReqBusinessCollateralListView
		public static List<CreditAppReqBusinessCollateralListView> GetCreditAppReqBusinessCollateralListViewValidation(this List<CreditAppReqBusinessCollateralListView> creditAppReqBusinessCollateralListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return creditAppReqBusinessCollateralListViews;
			}
			var result = new List<CreditAppReqBusinessCollateralListView>();
			try
			{
				foreach (CreditAppReqBusinessCollateralListView item in creditAppReqBusinessCollateralListViews)
				{
					result.Add(item.GetCreditAppReqBusinessCollateralListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CreditAppReqBusinessCollateralListView GetCreditAppReqBusinessCollateralListViewValidation(this CreditAppReqBusinessCollateralListView creditAppReqBusinessCollateralListView)
		{
			try
			{
				creditAppReqBusinessCollateralListView.RowAuthorize = creditAppReqBusinessCollateralListView.GetListRowAuthorize();
				return creditAppReqBusinessCollateralListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CreditAppReqBusinessCollateralListView creditAppReqBusinessCollateralListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CreditAppReqBusinessCollateralListView
		#region CreditAppReqBusinessCollateralItemView
		public static CreditAppReqBusinessCollateralItemView GetCreditAppReqBusinessCollateralItemViewValidation(this CreditAppReqBusinessCollateralItemView creditAppReqBusinessCollateralItemView)
		{
			try
			{
				creditAppReqBusinessCollateralItemView.RowAuthorize = creditAppReqBusinessCollateralItemView.GetItemRowAuthorize();
				return creditAppReqBusinessCollateralItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CreditAppReqBusinessCollateralItemView creditAppReqBusinessCollateralItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CreditAppReqBusinessCollateral ToCreditAppReqBusinessCollateral(this CreditAppReqBusinessCollateralItemView creditAppReqBusinessCollateralItemView)
		{
			try
			{
				CreditAppReqBusinessCollateral creditAppReqBusinessCollateral = new CreditAppReqBusinessCollateral();
				creditAppReqBusinessCollateral.CompanyGUID = creditAppReqBusinessCollateralItemView.CompanyGUID.StringToGuid();
				creditAppReqBusinessCollateral.CreatedBy = creditAppReqBusinessCollateralItemView.CreatedBy;
				creditAppReqBusinessCollateral.CreatedDateTime = creditAppReqBusinessCollateralItemView.CreatedDateTime.StringToSystemDateTime();
				creditAppReqBusinessCollateral.ModifiedBy = creditAppReqBusinessCollateralItemView.ModifiedBy;
				creditAppReqBusinessCollateral.ModifiedDateTime = creditAppReqBusinessCollateralItemView.ModifiedDateTime.StringToSystemDateTime();
				creditAppReqBusinessCollateral.Owner = creditAppReqBusinessCollateralItemView.Owner;
				creditAppReqBusinessCollateral.OwnerBusinessUnitGUID = creditAppReqBusinessCollateralItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				creditAppReqBusinessCollateral.CreditAppReqBusinessCollateralGUID = creditAppReqBusinessCollateralItemView.CreditAppReqBusinessCollateralGUID.StringToGuid();
				creditAppReqBusinessCollateral.AccountNumber = creditAppReqBusinessCollateralItemView.AccountNumber;
				creditAppReqBusinessCollateral.AttachmentRemark = creditAppReqBusinessCollateralItemView.AttachmentRemark;
				creditAppReqBusinessCollateral.BankGroupGUID = creditAppReqBusinessCollateralItemView.BankGroupGUID.StringToGuidNull();
				creditAppReqBusinessCollateral.BankTypeGUID = creditAppReqBusinessCollateralItemView.BankTypeGUID.StringToGuidNull();
				creditAppReqBusinessCollateral.BusinessCollateralSubTypeGUID = creditAppReqBusinessCollateralItemView.BusinessCollateralSubTypeGUID.StringToGuid();
				creditAppReqBusinessCollateral.BusinessCollateralTypeGUID = creditAppReqBusinessCollateralItemView.BusinessCollateralTypeGUID.StringToGuid();
				creditAppReqBusinessCollateral.BusinessCollateralValue = creditAppReqBusinessCollateralItemView.BusinessCollateralValue;
				creditAppReqBusinessCollateral.BuyerName = creditAppReqBusinessCollateralItemView.BuyerName;
				creditAppReqBusinessCollateral.BuyerTableGUID = creditAppReqBusinessCollateralItemView.BuyerTableGUID.StringToGuidNull();
				creditAppReqBusinessCollateral.BuyerTaxIdentificationId = creditAppReqBusinessCollateralItemView.BuyerTaxIdentificationId;
				creditAppReqBusinessCollateral.CapitalValuation = creditAppReqBusinessCollateralItemView.CapitalValuation;
				creditAppReqBusinessCollateral.ChassisNumber = creditAppReqBusinessCollateralItemView.ChassisNumber;
				creditAppReqBusinessCollateral.CreditAppRequestTableGUID = creditAppReqBusinessCollateralItemView.CreditAppRequestTableGUID.StringToGuid();
				creditAppReqBusinessCollateral.CustBusinessCollateralGUID = creditAppReqBusinessCollateralItemView.CustBusinessCollateralGUID.StringToGuidNull();
				creditAppReqBusinessCollateral.CustomerTableGUID = creditAppReqBusinessCollateralItemView.CustomerTableGUID.StringToGuid();
				creditAppReqBusinessCollateral.DateOfValuation = creditAppReqBusinessCollateralItemView.DateOfValuation.StringNullToDateNull();
				creditAppReqBusinessCollateral.DBDRegistrationAmount = creditAppReqBusinessCollateralItemView.DBDRegistrationAmount;
				creditAppReqBusinessCollateral.DBDRegistrationDate = creditAppReqBusinessCollateralItemView.DBDRegistrationDate.StringNullToDateNull();
				creditAppReqBusinessCollateral.DBDRegistrationDescription = creditAppReqBusinessCollateralItemView.DBDRegistrationDescription;
				creditAppReqBusinessCollateral.DBDRegistrationId = creditAppReqBusinessCollateralItemView.DBDRegistrationId;
				creditAppReqBusinessCollateral.Description = creditAppReqBusinessCollateralItemView.Description;
				creditAppReqBusinessCollateral.GuaranteeAmount = creditAppReqBusinessCollateralItemView.GuaranteeAmount;
				creditAppReqBusinessCollateral.IsNew = creditAppReqBusinessCollateralItemView.IsNew;
				creditAppReqBusinessCollateral.Lessee = creditAppReqBusinessCollateralItemView.Lessee;
				creditAppReqBusinessCollateral.Lessor = creditAppReqBusinessCollateralItemView.Lessor;
				creditAppReqBusinessCollateral.MachineNumber = creditAppReqBusinessCollateralItemView.MachineNumber;
				creditAppReqBusinessCollateral.MachineRegisteredStatus = creditAppReqBusinessCollateralItemView.MachineRegisteredStatus;
				creditAppReqBusinessCollateral.Ownership = creditAppReqBusinessCollateralItemView.Ownership;
				creditAppReqBusinessCollateral.PreferentialCreditorNumber = creditAppReqBusinessCollateralItemView.PreferentialCreditorNumber;
				creditAppReqBusinessCollateral.ProjectName = creditAppReqBusinessCollateralItemView.ProjectName;
				creditAppReqBusinessCollateral.Quantity = creditAppReqBusinessCollateralItemView.Quantity;
				creditAppReqBusinessCollateral.RefAgreementDate = creditAppReqBusinessCollateralItemView.RefAgreementDate.StringNullToDateNull();
				creditAppReqBusinessCollateral.RefAgreementId = creditAppReqBusinessCollateralItemView.RefAgreementId;
				creditAppReqBusinessCollateral.RegisteredPlace = creditAppReqBusinessCollateralItemView.RegisteredPlace;
				creditAppReqBusinessCollateral.RegistrationPlateNumber = creditAppReqBusinessCollateralItemView.RegistrationPlateNumber;
				creditAppReqBusinessCollateral.TitleDeedDistrict = creditAppReqBusinessCollateralItemView.TitleDeedDistrict;
				creditAppReqBusinessCollateral.TitleDeedNumber = creditAppReqBusinessCollateralItemView.TitleDeedNumber;
				creditAppReqBusinessCollateral.TitleDeedProvince = creditAppReqBusinessCollateralItemView.TitleDeedProvince;
				creditAppReqBusinessCollateral.TitleDeedSubDistrict = creditAppReqBusinessCollateralItemView.TitleDeedSubDistrict;
				creditAppReqBusinessCollateral.Unit = creditAppReqBusinessCollateralItemView.Unit;
				creditAppReqBusinessCollateral.ValuationCommittee = creditAppReqBusinessCollateralItemView.ValuationCommittee;
				
				creditAppReqBusinessCollateral.RowVersion = creditAppReqBusinessCollateralItemView.RowVersion;
				return creditAppReqBusinessCollateral;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CreditAppReqBusinessCollateral> ToCreditAppReqBusinessCollateral(this IEnumerable<CreditAppReqBusinessCollateralItemView> creditAppReqBusinessCollateralItemViews)
		{
			try
			{
				List<CreditAppReqBusinessCollateral> creditAppReqBusinessCollaterals = new List<CreditAppReqBusinessCollateral>();
				foreach (CreditAppReqBusinessCollateralItemView item in creditAppReqBusinessCollateralItemViews)
				{
					creditAppReqBusinessCollaterals.Add(item.ToCreditAppReqBusinessCollateral());
				}
				return creditAppReqBusinessCollaterals;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CreditAppReqBusinessCollateralItemView ToCreditAppReqBusinessCollateralItemView(this CreditAppReqBusinessCollateral creditAppReqBusinessCollateral)
		{
			try
			{
				CreditAppReqBusinessCollateralItemView creditAppReqBusinessCollateralItemView = new CreditAppReqBusinessCollateralItemView();
				creditAppReqBusinessCollateralItemView.CompanyGUID = creditAppReqBusinessCollateral.CompanyGUID.GuidNullToString();
				creditAppReqBusinessCollateralItemView.CreatedBy = creditAppReqBusinessCollateral.CreatedBy;
				creditAppReqBusinessCollateralItemView.CreatedDateTime = creditAppReqBusinessCollateral.CreatedDateTime.DateTimeToString();
				creditAppReqBusinessCollateralItemView.ModifiedBy = creditAppReqBusinessCollateral.ModifiedBy;
				creditAppReqBusinessCollateralItemView.ModifiedDateTime = creditAppReqBusinessCollateral.ModifiedDateTime.DateTimeToString();
				creditAppReqBusinessCollateralItemView.Owner = creditAppReqBusinessCollateral.Owner;
				creditAppReqBusinessCollateralItemView.OwnerBusinessUnitGUID = creditAppReqBusinessCollateral.OwnerBusinessUnitGUID.GuidNullToString();
				creditAppReqBusinessCollateralItemView.CreditAppReqBusinessCollateralGUID = creditAppReqBusinessCollateral.CreditAppReqBusinessCollateralGUID.GuidNullToString();
				creditAppReqBusinessCollateralItemView.AccountNumber = creditAppReqBusinessCollateral.AccountNumber;
				creditAppReqBusinessCollateralItemView.AttachmentRemark = creditAppReqBusinessCollateral.AttachmentRemark;
				creditAppReqBusinessCollateralItemView.BankGroupGUID = creditAppReqBusinessCollateral.BankGroupGUID.GuidNullToString();
				creditAppReqBusinessCollateralItemView.BankTypeGUID = creditAppReqBusinessCollateral.BankTypeGUID.GuidNullToString();
				creditAppReqBusinessCollateralItemView.BusinessCollateralSubTypeGUID = creditAppReqBusinessCollateral.BusinessCollateralSubTypeGUID.GuidNullToString();
				creditAppReqBusinessCollateralItemView.BusinessCollateralTypeGUID = creditAppReqBusinessCollateral.BusinessCollateralTypeGUID.GuidNullToString();
				creditAppReqBusinessCollateralItemView.BusinessCollateralValue = creditAppReqBusinessCollateral.BusinessCollateralValue;
				creditAppReqBusinessCollateralItemView.BuyerName = creditAppReqBusinessCollateral.BuyerName;
				creditAppReqBusinessCollateralItemView.BuyerTableGUID = creditAppReqBusinessCollateral.BuyerTableGUID.GuidNullToString();
				creditAppReqBusinessCollateralItemView.BuyerTaxIdentificationId = creditAppReqBusinessCollateral.BuyerTaxIdentificationId;
				creditAppReqBusinessCollateralItemView.CapitalValuation = creditAppReqBusinessCollateral.CapitalValuation;
				creditAppReqBusinessCollateralItemView.ChassisNumber = creditAppReqBusinessCollateral.ChassisNumber;
				creditAppReqBusinessCollateralItemView.CreditAppRequestTableGUID = creditAppReqBusinessCollateral.CreditAppRequestTableGUID.GuidNullToString();
				creditAppReqBusinessCollateralItemView.CustBusinessCollateralGUID = creditAppReqBusinessCollateral.CustBusinessCollateralGUID.GuidNullToString();
				creditAppReqBusinessCollateralItemView.CustomerTableGUID = creditAppReqBusinessCollateral.CustomerTableGUID.GuidNullToString();
				creditAppReqBusinessCollateralItemView.DateOfValuation = creditAppReqBusinessCollateral.DateOfValuation.DateNullToString();
				creditAppReqBusinessCollateralItemView.DBDRegistrationAmount = creditAppReqBusinessCollateral.DBDRegistrationAmount;
				creditAppReqBusinessCollateralItemView.DBDRegistrationDate = creditAppReqBusinessCollateral.DBDRegistrationDate.DateNullToString();
				creditAppReqBusinessCollateralItemView.DBDRegistrationDescription = creditAppReqBusinessCollateral.DBDRegistrationDescription;
				creditAppReqBusinessCollateralItemView.DBDRegistrationId = creditAppReqBusinessCollateral.DBDRegistrationId;
				creditAppReqBusinessCollateralItemView.Description = creditAppReqBusinessCollateral.Description;
				creditAppReqBusinessCollateralItemView.GuaranteeAmount = creditAppReqBusinessCollateral.GuaranteeAmount;
				creditAppReqBusinessCollateralItemView.IsNew = creditAppReqBusinessCollateral.IsNew;
				creditAppReqBusinessCollateralItemView.Lessee = creditAppReqBusinessCollateral.Lessee;
				creditAppReqBusinessCollateralItemView.Lessor = creditAppReqBusinessCollateral.Lessor;
				creditAppReqBusinessCollateralItemView.MachineNumber = creditAppReqBusinessCollateral.MachineNumber;
				creditAppReqBusinessCollateralItemView.MachineRegisteredStatus = creditAppReqBusinessCollateral.MachineRegisteredStatus;
				creditAppReqBusinessCollateralItemView.Ownership = creditAppReqBusinessCollateral.Ownership;
				creditAppReqBusinessCollateralItemView.PreferentialCreditorNumber = creditAppReqBusinessCollateral.PreferentialCreditorNumber;
				creditAppReqBusinessCollateralItemView.ProjectName = creditAppReqBusinessCollateral.ProjectName;
				creditAppReqBusinessCollateralItemView.Quantity = creditAppReqBusinessCollateral.Quantity;
				creditAppReqBusinessCollateralItemView.RefAgreementDate = creditAppReqBusinessCollateral.RefAgreementDate.DateNullToString();
				creditAppReqBusinessCollateralItemView.RefAgreementId = creditAppReqBusinessCollateral.RefAgreementId;
				creditAppReqBusinessCollateralItemView.RegisteredPlace = creditAppReqBusinessCollateral.RegisteredPlace;
				creditAppReqBusinessCollateralItemView.RegistrationPlateNumber = creditAppReqBusinessCollateral.RegistrationPlateNumber;
				creditAppReqBusinessCollateralItemView.TitleDeedDistrict = creditAppReqBusinessCollateral.TitleDeedDistrict;
				creditAppReqBusinessCollateralItemView.TitleDeedNumber = creditAppReqBusinessCollateral.TitleDeedNumber;
				creditAppReqBusinessCollateralItemView.TitleDeedProvince = creditAppReqBusinessCollateral.TitleDeedProvince;
				creditAppReqBusinessCollateralItemView.TitleDeedSubDistrict = creditAppReqBusinessCollateral.TitleDeedSubDistrict;
				creditAppReqBusinessCollateralItemView.Unit = creditAppReqBusinessCollateral.Unit;
				creditAppReqBusinessCollateralItemView.ValuationCommittee = creditAppReqBusinessCollateral.ValuationCommittee;
				
				creditAppReqBusinessCollateralItemView.RowVersion = creditAppReqBusinessCollateral.RowVersion;
				return creditAppReqBusinessCollateralItemView.GetCreditAppReqBusinessCollateralItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CreditAppReqBusinessCollateralItemView
		#region ToDropDown
		public static SelectItem<CreditAppReqBusinessCollateralItemView> ToDropDownItem(this CreditAppReqBusinessCollateralItemView creditAppReqBusinessCollateralView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CreditAppReqBusinessCollateralItemView> selectItem = new SelectItem<CreditAppReqBusinessCollateralItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(creditAppReqBusinessCollateralView.Description);
				selectItem.Value = creditAppReqBusinessCollateralView.CreditAppReqBusinessCollateralGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: creditAppReqBusinessCollateralView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CreditAppReqBusinessCollateralItemView>> ToDropDownItem(this IEnumerable<CreditAppReqBusinessCollateralItemView> creditAppReqBusinessCollateralItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CreditAppReqBusinessCollateralItemView>> selectItems = new List<SelectItem<CreditAppReqBusinessCollateralItemView>>();
				foreach (CreditAppReqBusinessCollateralItemView item in creditAppReqBusinessCollateralItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

