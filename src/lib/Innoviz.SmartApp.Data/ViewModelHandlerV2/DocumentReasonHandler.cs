using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class DocumentReasonHandler
	{
		#region DocumentReasonListView
		public static List<DocumentReasonListView> GetDocumentReasonListViewValidation(this List<DocumentReasonListView> documentReasonListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return documentReasonListViews;
			}
			var result = new List<DocumentReasonListView>();
			try
			{
				foreach (DocumentReasonListView item in documentReasonListViews)
				{
					result.Add(item.GetDocumentReasonListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static DocumentReasonListView GetDocumentReasonListViewValidation(this DocumentReasonListView documentReasonListView)
		{
			try
			{
				documentReasonListView.RowAuthorize = documentReasonListView.GetListRowAuthorize();
				return documentReasonListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this DocumentReasonListView documentReasonListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion DocumentReasonListView
		#region DocumentReasonItemView
		public static DocumentReasonItemView GetDocumentReasonItemViewValidation(this DocumentReasonItemView documentReasonItemView)
		{
			try
			{
				documentReasonItemView.RowAuthorize = documentReasonItemView.GetItemRowAuthorize();
				return documentReasonItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this DocumentReasonItemView documentReasonItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static DocumentReason ToDocumentReason(this DocumentReasonItemView documentReasonItemView)
		{
			try
			{
				DocumentReason documentReason = new DocumentReason();
				documentReason.CompanyGUID = documentReasonItemView.CompanyGUID.StringToGuid();
				documentReason.CreatedBy = documentReasonItemView.CreatedBy;
				documentReason.CreatedDateTime = documentReasonItemView.CreatedDateTime.StringToSystemDateTime();
				documentReason.ModifiedBy = documentReasonItemView.ModifiedBy;
				documentReason.ModifiedDateTime = documentReasonItemView.ModifiedDateTime.StringToSystemDateTime();
				documentReason.Owner = documentReasonItemView.Owner;
				documentReason.OwnerBusinessUnitGUID = documentReasonItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				documentReason.DocumentReasonGUID = documentReasonItemView.DocumentReasonGUID.StringToGuid();
				documentReason.Description = documentReasonItemView.Description;
				documentReason.ReasonId = documentReasonItemView.ReasonId;
				documentReason.RefType = documentReasonItemView.RefType;
				
				documentReason.RowVersion = documentReasonItemView.RowVersion;
				return documentReason;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<DocumentReason> ToDocumentReason(this IEnumerable<DocumentReasonItemView> documentReasonItemViews)
		{
			try
			{
				List<DocumentReason> documentReasons = new List<DocumentReason>();
				foreach (DocumentReasonItemView item in documentReasonItemViews)
				{
					documentReasons.Add(item.ToDocumentReason());
				}
				return documentReasons;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static DocumentReasonItemView ToDocumentReasonItemView(this DocumentReason documentReason)
		{
			try
			{
				DocumentReasonItemView documentReasonItemView = new DocumentReasonItemView();
				documentReasonItemView.CompanyGUID = documentReason.CompanyGUID.GuidNullToString();
				documentReasonItemView.CreatedBy = documentReason.CreatedBy;
				documentReasonItemView.CreatedDateTime = documentReason.CreatedDateTime.DateTimeToString();
				documentReasonItemView.ModifiedBy = documentReason.ModifiedBy;
				documentReasonItemView.ModifiedDateTime = documentReason.ModifiedDateTime.DateTimeToString();
				documentReasonItemView.Owner = documentReason.Owner;
				documentReasonItemView.OwnerBusinessUnitGUID = documentReason.OwnerBusinessUnitGUID.GuidNullToString();
				documentReasonItemView.DocumentReasonGUID = documentReason.DocumentReasonGUID.GuidNullToString();
				documentReasonItemView.Description = documentReason.Description;
				documentReasonItemView.ReasonId = documentReason.ReasonId;
				documentReasonItemView.RefType = documentReason.RefType;
				
				documentReasonItemView.RowVersion = documentReason.RowVersion;
				return documentReasonItemView.GetDocumentReasonItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion DocumentReasonItemView
		#region ToDropDown
		public static SelectItem<DocumentReasonItemView> ToDropDownItem(this DocumentReasonItemView documentReasonView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<DocumentReasonItemView> selectItem = new SelectItem<DocumentReasonItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(documentReasonView.ReasonId, documentReasonView.Description);
				selectItem.Value = documentReasonView.DocumentReasonGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: documentReasonView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<DocumentReasonItemView>> ToDropDownItem(this IEnumerable<DocumentReasonItemView> documentReasonItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<DocumentReasonItemView>> selectItems = new List<SelectItem<DocumentReasonItemView>>();
				foreach (DocumentReasonItemView item in documentReasonItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

