using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class TaxInvoiceLineHandler
	{
		#region TaxInvoiceLineListView
		public static List<TaxInvoiceLineListView> GetTaxInvoiceLineListViewValidation(this List<TaxInvoiceLineListView> taxInvoiceLineListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return taxInvoiceLineListViews;
			}
			var result = new List<TaxInvoiceLineListView>();
			try
			{
				foreach (TaxInvoiceLineListView item in taxInvoiceLineListViews)
				{
					result.Add(item.GetTaxInvoiceLineListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static TaxInvoiceLineListView GetTaxInvoiceLineListViewValidation(this TaxInvoiceLineListView taxInvoiceLineListView)
		{
			try
			{
				taxInvoiceLineListView.RowAuthorize = taxInvoiceLineListView.GetListRowAuthorize();
				return taxInvoiceLineListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this TaxInvoiceLineListView taxInvoiceLineListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion TaxInvoiceLineListView
		#region TaxInvoiceLineItemView
		public static TaxInvoiceLineItemView GetTaxInvoiceLineItemViewValidation(this TaxInvoiceLineItemView taxInvoiceLineItemView)
		{
			try
			{
				taxInvoiceLineItemView.RowAuthorize = taxInvoiceLineItemView.GetItemRowAuthorize();
				return taxInvoiceLineItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this TaxInvoiceLineItemView taxInvoiceLineItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static TaxInvoiceLine ToTaxInvoiceLine(this TaxInvoiceLineItemView taxInvoiceLineItemView)
		{
			try
			{
				TaxInvoiceLine taxInvoiceLine = new TaxInvoiceLine();
				taxInvoiceLine.CompanyGUID = taxInvoiceLineItemView.CompanyGUID.StringToGuid();
				taxInvoiceLine.BranchGUID = taxInvoiceLineItemView.BranchGUID.StringToGuid();
				taxInvoiceLine.CreatedBy = taxInvoiceLineItemView.CreatedBy;
				taxInvoiceLine.CreatedDateTime = taxInvoiceLineItemView.CreatedDateTime.StringToSystemDateTime();
				taxInvoiceLine.ModifiedBy = taxInvoiceLineItemView.ModifiedBy;
				taxInvoiceLine.ModifiedDateTime = taxInvoiceLineItemView.ModifiedDateTime.StringToSystemDateTime();
				taxInvoiceLine.Owner = taxInvoiceLineItemView.Owner;
				taxInvoiceLine.OwnerBusinessUnitGUID = taxInvoiceLineItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				taxInvoiceLine.TaxInvoiceLineGUID = taxInvoiceLineItemView.TaxInvoiceLineGUID.StringToGuid();
				taxInvoiceLine.Dimension1GUID = taxInvoiceLineItemView.Dimension1GUID.StringToGuidNull();
				taxInvoiceLine.Dimension2GUID = taxInvoiceLineItemView.Dimension2GUID.StringToGuidNull();
				taxInvoiceLine.Dimension3GUID = taxInvoiceLineItemView.Dimension3GUID.StringToGuidNull();
				taxInvoiceLine.Dimension4GUID = taxInvoiceLineItemView.Dimension4GUID.StringToGuidNull();
				taxInvoiceLine.Dimension5GUID = taxInvoiceLineItemView.Dimension5GUID.StringToGuidNull();
				taxInvoiceLine.InvoiceRevenueTypeGUID = taxInvoiceLineItemView.InvoiceRevenueTypeGUID.StringToGuid();
				taxInvoiceLine.InvoiceText = taxInvoiceLineItemView.InvoiceText;
				taxInvoiceLine.LineNum = taxInvoiceLineItemView.LineNum;
				taxInvoiceLine.Qty = taxInvoiceLineItemView.Qty;
				taxInvoiceLine.TaxAmount = taxInvoiceLineItemView.TaxAmount;
				taxInvoiceLine.TaxInvoiceTableGUID = taxInvoiceLineItemView.TaxInvoiceTableGUID.StringToGuid();
				taxInvoiceLine.TaxTableGUID = taxInvoiceLineItemView.TaxTableGUID.StringToGuid();
				taxInvoiceLine.TotalAmount = taxInvoiceLineItemView.TotalAmount;
				taxInvoiceLine.TotalAmountBeforeTax = taxInvoiceLineItemView.TotalAmountBeforeTax;
				taxInvoiceLine.UnitPrice = taxInvoiceLineItemView.UnitPrice;
				
				taxInvoiceLine.RowVersion = taxInvoiceLineItemView.RowVersion;
				return taxInvoiceLine;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<TaxInvoiceLine> ToTaxInvoiceLine(this IEnumerable<TaxInvoiceLineItemView> taxInvoiceLineItemViews)
		{
			try
			{
				List<TaxInvoiceLine> taxInvoiceLines = new List<TaxInvoiceLine>();
				foreach (TaxInvoiceLineItemView item in taxInvoiceLineItemViews)
				{
					taxInvoiceLines.Add(item.ToTaxInvoiceLine());
				}
				return taxInvoiceLines;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static TaxInvoiceLineItemView ToTaxInvoiceLineItemView(this TaxInvoiceLine taxInvoiceLine)
		{
			try
			{
				TaxInvoiceLineItemView taxInvoiceLineItemView = new TaxInvoiceLineItemView();
				taxInvoiceLineItemView.CompanyGUID = taxInvoiceLine.CompanyGUID.GuidNullToString();
				taxInvoiceLineItemView.BranchGUID = taxInvoiceLine.BranchGUID.GuidNullToString();
				taxInvoiceLineItemView.CreatedBy = taxInvoiceLine.CreatedBy;
				taxInvoiceLineItemView.CreatedDateTime = taxInvoiceLine.CreatedDateTime.DateTimeToString();
				taxInvoiceLineItemView.ModifiedBy = taxInvoiceLine.ModifiedBy;
				taxInvoiceLineItemView.ModifiedDateTime = taxInvoiceLine.ModifiedDateTime.DateTimeToString();
				taxInvoiceLineItemView.Owner = taxInvoiceLine.Owner;
				taxInvoiceLineItemView.OwnerBusinessUnitGUID = taxInvoiceLine.OwnerBusinessUnitGUID.GuidNullToString();
				taxInvoiceLineItemView.TaxInvoiceLineGUID = taxInvoiceLine.TaxInvoiceLineGUID.GuidNullToString();
				taxInvoiceLineItemView.Dimension1GUID = taxInvoiceLine.Dimension1GUID.GuidNullToString();
				taxInvoiceLineItemView.Dimension2GUID = taxInvoiceLine.Dimension2GUID.GuidNullToString();
				taxInvoiceLineItemView.Dimension3GUID = taxInvoiceLine.Dimension3GUID.GuidNullToString();
				taxInvoiceLineItemView.Dimension4GUID = taxInvoiceLine.Dimension4GUID.GuidNullToString();
				taxInvoiceLineItemView.Dimension5GUID = taxInvoiceLine.Dimension5GUID.GuidNullToString();
				taxInvoiceLineItemView.InvoiceRevenueTypeGUID = taxInvoiceLine.InvoiceRevenueTypeGUID.GuidNullToString();
				taxInvoiceLineItemView.InvoiceText = taxInvoiceLine.InvoiceText;
				taxInvoiceLineItemView.LineNum = taxInvoiceLine.LineNum;
				taxInvoiceLineItemView.Qty = taxInvoiceLine.Qty;
				taxInvoiceLineItemView.TaxAmount = taxInvoiceLine.TaxAmount;
				taxInvoiceLineItemView.TaxInvoiceTableGUID = taxInvoiceLine.TaxInvoiceTableGUID.GuidNullToString();
				taxInvoiceLineItemView.TaxTableGUID = taxInvoiceLine.TaxTableGUID.GuidNullToString();
				taxInvoiceLineItemView.TotalAmount = taxInvoiceLine.TotalAmount;
				taxInvoiceLineItemView.TotalAmountBeforeTax = taxInvoiceLine.TotalAmountBeforeTax;
				taxInvoiceLineItemView.UnitPrice = taxInvoiceLine.UnitPrice;
				
				taxInvoiceLineItemView.RowVersion = taxInvoiceLine.RowVersion;
				return taxInvoiceLineItemView.GetTaxInvoiceLineItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion TaxInvoiceLineItemView
		#region ToDropDown
		public static SelectItem<TaxInvoiceLineItemView> ToDropDownItem(this TaxInvoiceLineItemView taxInvoiceLineView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<TaxInvoiceLineItemView> selectItem = new SelectItem<TaxInvoiceLineItemView>();
				selectItem.Label = null;
				selectItem.Value = taxInvoiceLineView.TaxInvoiceLineGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: taxInvoiceLineView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<TaxInvoiceLineItemView>> ToDropDownItem(this IEnumerable<TaxInvoiceLineItemView> taxInvoiceLineItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<TaxInvoiceLineItemView>> selectItems = new List<SelectItem<TaxInvoiceLineItemView>>();
				foreach (TaxInvoiceLineItemView item in taxInvoiceLineItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

