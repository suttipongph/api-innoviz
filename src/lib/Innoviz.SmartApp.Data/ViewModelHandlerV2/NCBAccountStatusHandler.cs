using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class NCBAccountStatusHandler
	{
		#region NCBAccountStatusListView
		public static List<NCBAccountStatusListView> GetNCBAccountStatusListViewValidation(this List<NCBAccountStatusListView> ncbAccountStatusListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return ncbAccountStatusListViews;
			}
			var result = new List<NCBAccountStatusListView>();
			try
			{
				foreach (NCBAccountStatusListView item in ncbAccountStatusListViews)
				{
					result.Add(item.GetNCBAccountStatusListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static NCBAccountStatusListView GetNCBAccountStatusListViewValidation(this NCBAccountStatusListView ncbAccountStatusListView)
		{
			try
			{
				ncbAccountStatusListView.RowAuthorize = ncbAccountStatusListView.GetListRowAuthorize();
				return ncbAccountStatusListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this NCBAccountStatusListView ncbAccountStatusListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion NCBAccountStatusListView
		#region NCBAccountStatusItemView
		public static NCBAccountStatusItemView GetNCBAccountStatusItemViewValidation(this NCBAccountStatusItemView ncbAccountStatusItemView)
		{
			try
			{
				ncbAccountStatusItemView.RowAuthorize = ncbAccountStatusItemView.GetItemRowAuthorize();
				return ncbAccountStatusItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this NCBAccountStatusItemView ncbAccountStatusItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static NCBAccountStatus ToNCBAccountStatus(this NCBAccountStatusItemView ncbAccountStatusItemView)
		{
			try
			{
				NCBAccountStatus ncbAccountStatus = new NCBAccountStatus();
				ncbAccountStatus.CompanyGUID = ncbAccountStatusItemView.CompanyGUID.StringToGuid();
				ncbAccountStatus.CreatedBy = ncbAccountStatusItemView.CreatedBy;
				ncbAccountStatus.CreatedDateTime = ncbAccountStatusItemView.CreatedDateTime.StringToSystemDateTime();
				ncbAccountStatus.ModifiedBy = ncbAccountStatusItemView.ModifiedBy;
				ncbAccountStatus.ModifiedDateTime = ncbAccountStatusItemView.ModifiedDateTime.StringToSystemDateTime();
				ncbAccountStatus.Owner = ncbAccountStatusItemView.Owner;
				ncbAccountStatus.OwnerBusinessUnitGUID = ncbAccountStatusItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				ncbAccountStatus.NCBAccountStatusGUID = ncbAccountStatusItemView.NCBAccountStatusGUID.StringToGuid();
				ncbAccountStatus.Code = ncbAccountStatusItemView.Code;
				ncbAccountStatus.Description = ncbAccountStatusItemView.Description;
				ncbAccountStatus.NCBAccStatusId = ncbAccountStatusItemView.NCBAccStatusId;
				
				ncbAccountStatus.RowVersion = ncbAccountStatusItemView.RowVersion;
				return ncbAccountStatus;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<NCBAccountStatus> ToNCBAccountStatus(this IEnumerable<NCBAccountStatusItemView> ncbAccountStatusItemViews)
		{
			try
			{
				List<NCBAccountStatus> ncbAccountStatuss = new List<NCBAccountStatus>();
				foreach (NCBAccountStatusItemView item in ncbAccountStatusItemViews)
				{
					ncbAccountStatuss.Add(item.ToNCBAccountStatus());
				}
				return ncbAccountStatuss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static NCBAccountStatusItemView ToNCBAccountStatusItemView(this NCBAccountStatus ncbAccountStatus)
		{
			try
			{
				NCBAccountStatusItemView ncbAccountStatusItemView = new NCBAccountStatusItemView();
				ncbAccountStatusItemView.CompanyGUID = ncbAccountStatus.CompanyGUID.GuidNullToString();
				ncbAccountStatusItemView.CreatedBy = ncbAccountStatus.CreatedBy;
				ncbAccountStatusItemView.CreatedDateTime = ncbAccountStatus.CreatedDateTime.DateTimeToString();
				ncbAccountStatusItemView.ModifiedBy = ncbAccountStatus.ModifiedBy;
				ncbAccountStatusItemView.ModifiedDateTime = ncbAccountStatus.ModifiedDateTime.DateTimeToString();
				ncbAccountStatusItemView.Owner = ncbAccountStatus.Owner;
				ncbAccountStatusItemView.OwnerBusinessUnitGUID = ncbAccountStatus.OwnerBusinessUnitGUID.GuidNullToString();
				ncbAccountStatusItemView.NCBAccountStatusGUID = ncbAccountStatus.NCBAccountStatusGUID.GuidNullToString();
				ncbAccountStatusItemView.Code = ncbAccountStatus.Code;
				ncbAccountStatusItemView.Description = ncbAccountStatus.Description;
				ncbAccountStatusItemView.NCBAccStatusId = ncbAccountStatus.NCBAccStatusId;
				
				ncbAccountStatusItemView.RowVersion = ncbAccountStatus.RowVersion;
				return ncbAccountStatusItemView.GetNCBAccountStatusItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion NCBAccountStatusItemView
		#region ToDropDown
		public static SelectItem<NCBAccountStatusItemView> ToDropDownItem(this NCBAccountStatusItemView ncbAccountStatusView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<NCBAccountStatusItemView> selectItem = new SelectItem<NCBAccountStatusItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(ncbAccountStatusView.NCBAccStatusId, ncbAccountStatusView.Description);
				selectItem.Value = ncbAccountStatusView.NCBAccountStatusGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: ncbAccountStatusView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<NCBAccountStatusItemView>> ToDropDownItem(this IEnumerable<NCBAccountStatusItemView> ncbAccountStatusItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<NCBAccountStatusItemView>> selectItems = new List<SelectItem<NCBAccountStatusItemView>>();
				foreach (NCBAccountStatusItemView item in ncbAccountStatusItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

