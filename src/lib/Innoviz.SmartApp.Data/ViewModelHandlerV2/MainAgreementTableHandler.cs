using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class MainAgreementTableHandler
	{
		#region MainAgreementTableListView
		public static List<MainAgreementTableListView> GetMainAgreementTableListViewValidation(this List<MainAgreementTableListView> mainAgreementTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return mainAgreementTableListViews;
			}
			var result = new List<MainAgreementTableListView>();
			try
			{
				foreach (MainAgreementTableListView item in mainAgreementTableListViews)
				{
					result.Add(item.GetMainAgreementTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static MainAgreementTableListView GetMainAgreementTableListViewValidation(this MainAgreementTableListView mainAgreementTableListView)
		{
			try
			{
				mainAgreementTableListView.RowAuthorize = mainAgreementTableListView.GetListRowAuthorize();
				return mainAgreementTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this MainAgreementTableListView mainAgreementTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion MainAgreementTableListView
		#region MainAgreementTableItemView
		public static MainAgreementTableItemView GetMainAgreementTableItemViewValidation(this MainAgreementTableItemView mainAgreementTableItemView)
		{
			try
			{
				mainAgreementTableItemView.RowAuthorize = mainAgreementTableItemView.GetItemRowAuthorize();
				return mainAgreementTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this MainAgreementTableItemView mainAgreementTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static MainAgreementTable ToMainAgreementTable(this MainAgreementTableItemView mainAgreementTableItemView)
		{
			try
			{
				MainAgreementTable mainAgreementTable = new MainAgreementTable();
				mainAgreementTable.CompanyGUID = mainAgreementTableItemView.CompanyGUID.StringToGuid();
				mainAgreementTable.CreatedBy = mainAgreementTableItemView.CreatedBy;
				mainAgreementTable.CreatedDateTime = mainAgreementTableItemView.CreatedDateTime.StringToSystemDateTime();
				mainAgreementTable.ModifiedBy = mainAgreementTableItemView.ModifiedBy;
				mainAgreementTable.ModifiedDateTime = mainAgreementTableItemView.ModifiedDateTime.StringToSystemDateTime();
				mainAgreementTable.Owner = mainAgreementTableItemView.Owner;
				mainAgreementTable.OwnerBusinessUnitGUID = mainAgreementTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				mainAgreementTable.MainAgreementTableGUID = mainAgreementTableItemView.MainAgreementTableGUID.StringToGuid();
				mainAgreementTable.AgreementDate = mainAgreementTableItemView.AgreementDate.StringToDate();
				mainAgreementTable.AgreementDocType = mainAgreementTableItemView.AgreementDocType;
				mainAgreementTable.Agreementextension = mainAgreementTableItemView.Agreementextension;
				mainAgreementTable.AgreementYear = mainAgreementTableItemView.AgreementYear;
				mainAgreementTable.ApprovedCreditLimit = mainAgreementTableItemView.ApprovedCreditLimit;
				mainAgreementTable.ApprovedCreditLimitLine = mainAgreementTableItemView.ApprovedCreditLimitLine;
				mainAgreementTable.BuyerAgreementDescription = mainAgreementTableItemView.BuyerAgreementDescription;
				mainAgreementTable.BuyerAgreementReferenceId = mainAgreementTableItemView.BuyerAgreementReferenceId;
				mainAgreementTable.BuyerName = mainAgreementTableItemView.BuyerName;
				mainAgreementTable.BuyerTableGUID = mainAgreementTableItemView.BuyerTableGUID.StringToGuidNull();
				mainAgreementTable.ConsortiumTableGUID = mainAgreementTableItemView.ConsortiumTableGUID.StringToGuidNull();
				mainAgreementTable.CreditAppRequestTableGUID = mainAgreementTableItemView.CreditAppRequestTableGUID.StringToGuid();
				mainAgreementTable.CreditAppTableGUID = mainAgreementTableItemView.CreditAppTableGUID.StringToGuid();
				mainAgreementTable.CreditLimitTypeGUID = mainAgreementTableItemView.CreditLimitTypeGUID.StringToGuid();
				mainAgreementTable.CustomerAltName = mainAgreementTableItemView.CustomerAltName;
				mainAgreementTable.CustomerName = mainAgreementTableItemView.CustomerName;
				mainAgreementTable.CustomerTableGUID = mainAgreementTableItemView.CustomerTableGUID.StringToGuid();
				mainAgreementTable.Description = mainAgreementTableItemView.Description;
				mainAgreementTable.DocumentReasonGUID = mainAgreementTableItemView.DocumentReasonGUID.StringToGuidNull();
				mainAgreementTable.DocumentStatusGUID = mainAgreementTableItemView.DocumentStatusGUID.StringToGuid();
				mainAgreementTable.ExpiryDate = mainAgreementTableItemView.ExpiryDate.StringNullToDateNull();
				mainAgreementTable.InternalMainAgreementId = mainAgreementTableItemView.InternalMainAgreementId;
				mainAgreementTable.MainAgreementId = mainAgreementTableItemView.MainAgreementId;
				mainAgreementTable.MaxInterestPct = mainAgreementTableItemView.MaxInterestPct;
				mainAgreementTable.ProductType = mainAgreementTableItemView.ProductType;
				mainAgreementTable.RefMainAgreementTableGUID = mainAgreementTableItemView.RefMainAgreementTableGUID.StringToGuidNull();
				mainAgreementTable.Remark = mainAgreementTableItemView.Remark;
				mainAgreementTable.SigningDate = mainAgreementTableItemView.SigningDate.StringNullToDateNull();
				mainAgreementTable.StartDate = mainAgreementTableItemView.StartDate.StringNullToDateNull();
				mainAgreementTable.TotalInterestPct = mainAgreementTableItemView.TotalInterestPct;
				mainAgreementTable.WithdrawalTableGUID = mainAgreementTableItemView.WithdrawalTableGUID.StringToGuidNull();
				
				mainAgreementTable.RowVersion = mainAgreementTableItemView.RowVersion;
				return mainAgreementTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<MainAgreementTable> ToMainAgreementTable(this IEnumerable<MainAgreementTableItemView> mainAgreementTableItemViews)
		{
			try
			{
				List<MainAgreementTable> mainAgreementTables = new List<MainAgreementTable>();
				foreach (MainAgreementTableItemView item in mainAgreementTableItemViews)
				{
					mainAgreementTables.Add(item.ToMainAgreementTable());
				}
				return mainAgreementTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static MainAgreementTableItemView ToMainAgreementTableItemView(this MainAgreementTable mainAgreementTable)
		{
			try
			{
				MainAgreementTableItemView mainAgreementTableItemView = new MainAgreementTableItemView();
				mainAgreementTableItemView.CompanyGUID = mainAgreementTable.CompanyGUID.GuidNullToString();
				mainAgreementTableItemView.CreatedBy = mainAgreementTable.CreatedBy;
				mainAgreementTableItemView.CreatedDateTime = mainAgreementTable.CreatedDateTime.DateTimeToString();
				mainAgreementTableItemView.ModifiedBy = mainAgreementTable.ModifiedBy;
				mainAgreementTableItemView.ModifiedDateTime = mainAgreementTable.ModifiedDateTime.DateTimeToString();
				mainAgreementTableItemView.Owner = mainAgreementTable.Owner;
				mainAgreementTableItemView.OwnerBusinessUnitGUID = mainAgreementTable.OwnerBusinessUnitGUID.GuidNullToString();
				mainAgreementTableItemView.MainAgreementTableGUID = mainAgreementTable.MainAgreementTableGUID.GuidNullToString();
				mainAgreementTableItemView.AgreementDate = mainAgreementTable.AgreementDate.DateToString();
				mainAgreementTableItemView.AgreementDocType = mainAgreementTable.AgreementDocType;
				mainAgreementTableItemView.Agreementextension = mainAgreementTable.Agreementextension;
				mainAgreementTableItemView.AgreementYear = mainAgreementTable.AgreementYear;
				mainAgreementTableItemView.ApprovedCreditLimit = mainAgreementTable.ApprovedCreditLimit;
				mainAgreementTableItemView.ApprovedCreditLimitLine = mainAgreementTable.ApprovedCreditLimitLine;
				mainAgreementTableItemView.BuyerAgreementDescription = mainAgreementTable.BuyerAgreementDescription;
				mainAgreementTableItemView.BuyerAgreementReferenceId = mainAgreementTable.BuyerAgreementReferenceId;
				mainAgreementTableItemView.BuyerName = mainAgreementTable.BuyerName;
				mainAgreementTableItemView.BuyerTableGUID = mainAgreementTable.BuyerTableGUID.GuidNullToString();
				mainAgreementTableItemView.ConsortiumTableGUID = mainAgreementTable.ConsortiumTableGUID.GuidNullToString();
				mainAgreementTableItemView.CreditAppRequestTableGUID = mainAgreementTable.CreditAppRequestTableGUID.GuidNullToString();
				mainAgreementTableItemView.CreditAppTableGUID = mainAgreementTable.CreditAppTableGUID.GuidNullToString();
				mainAgreementTableItemView.CreditLimitTypeGUID = mainAgreementTable.CreditLimitTypeGUID.GuidNullToString();
				mainAgreementTableItemView.CustomerAltName = mainAgreementTable.CustomerAltName;
				mainAgreementTableItemView.CustomerName = mainAgreementTable.CustomerName;
				mainAgreementTableItemView.CustomerTableGUID = mainAgreementTable.CustomerTableGUID.GuidNullToString();
				mainAgreementTableItemView.Description = mainAgreementTable.Description;
				mainAgreementTableItemView.DocumentReasonGUID = mainAgreementTable.DocumentReasonGUID.GuidNullToString();
				mainAgreementTableItemView.DocumentStatusGUID = mainAgreementTable.DocumentStatusGUID.GuidNullToString();
				mainAgreementTableItemView.ExpiryDate = mainAgreementTable.ExpiryDate.DateNullToString();
				mainAgreementTableItemView.InternalMainAgreementId = mainAgreementTable.InternalMainAgreementId;
				mainAgreementTableItemView.MainAgreementId = mainAgreementTable.MainAgreementId;
				mainAgreementTableItemView.MaxInterestPct = mainAgreementTable.MaxInterestPct;
				mainAgreementTableItemView.ProductType = mainAgreementTable.ProductType;
				mainAgreementTableItemView.RefMainAgreementTableGUID = mainAgreementTable.RefMainAgreementTableGUID.GuidNullToString();
				mainAgreementTableItemView.Remark = mainAgreementTable.Remark;
				mainAgreementTableItemView.SigningDate = mainAgreementTable.SigningDate.DateNullToString();
				mainAgreementTableItemView.StartDate = mainAgreementTable.StartDate.DateNullToString();
				mainAgreementTableItemView.TotalInterestPct = mainAgreementTable.TotalInterestPct;
				mainAgreementTableItemView.WithdrawalTableGUID = mainAgreementTable.WithdrawalTableGUID.GuidNullToString();
				
				mainAgreementTableItemView.RowVersion = mainAgreementTable.RowVersion;
				return mainAgreementTableItemView.GetMainAgreementTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion MainAgreementTableItemView
		#region ToDropDown
		public static SelectItem<MainAgreementTableItemView> ToDropDownItem(this MainAgreementTableItemView mainAgreementTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<MainAgreementTableItemView> selectItem = new SelectItem<MainAgreementTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(mainAgreementTableView.InternalMainAgreementId, mainAgreementTableView.Description);
				selectItem.Value = mainAgreementTableView.MainAgreementTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: mainAgreementTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<MainAgreementTableItemView>> ToDropDownItem(this IEnumerable<MainAgreementTableItemView> mainAgreementTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<MainAgreementTableItemView>> selectItems = new List<SelectItem<MainAgreementTableItemView>>();
				foreach (MainAgreementTableItemView item in mainAgreementTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

