using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class DocumentConditionTemplateLineHandler
	{
		#region DocumentConditionTemplateLineListView
		public static List<DocumentConditionTemplateLineListView> GetDocumentConditionTemplateLineListViewValidation(this List<DocumentConditionTemplateLineListView> documentConditionTemplateLineListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return documentConditionTemplateLineListViews;
			}
			var result = new List<DocumentConditionTemplateLineListView>();
			try
			{
				foreach (DocumentConditionTemplateLineListView item in documentConditionTemplateLineListViews)
				{
					result.Add(item.GetDocumentConditionTemplateLineListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static DocumentConditionTemplateLineListView GetDocumentConditionTemplateLineListViewValidation(this DocumentConditionTemplateLineListView documentConditionTemplateLineListView)
		{
			try
			{
				documentConditionTemplateLineListView.RowAuthorize = documentConditionTemplateLineListView.GetListRowAuthorize();
				return documentConditionTemplateLineListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this DocumentConditionTemplateLineListView documentConditionTemplateLineListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion DocumentConditionTemplateLineListView
		#region DocumentConditionTemplateLineItemView
		public static DocumentConditionTemplateLineItemView GetDocumentConditionTemplateLineItemViewValidation(this DocumentConditionTemplateLineItemView documentConditionTemplateLineItemView)
		{
			try
			{
				documentConditionTemplateLineItemView.RowAuthorize = documentConditionTemplateLineItemView.GetItemRowAuthorize();
				return documentConditionTemplateLineItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this DocumentConditionTemplateLineItemView documentConditionTemplateLineItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static DocumentConditionTemplateLine ToDocumentConditionTemplateLine(this DocumentConditionTemplateLineItemView documentConditionTemplateLineItemView)
		{
			try
			{
				DocumentConditionTemplateLine documentConditionTemplateLine = new DocumentConditionTemplateLine();
				documentConditionTemplateLine.CompanyGUID = documentConditionTemplateLineItemView.CompanyGUID.StringToGuid();
				documentConditionTemplateLine.CreatedBy = documentConditionTemplateLineItemView.CreatedBy;
				documentConditionTemplateLine.CreatedDateTime = documentConditionTemplateLineItemView.CreatedDateTime.StringToSystemDateTime();
				documentConditionTemplateLine.ModifiedBy = documentConditionTemplateLineItemView.ModifiedBy;
				documentConditionTemplateLine.ModifiedDateTime = documentConditionTemplateLineItemView.ModifiedDateTime.StringToSystemDateTime();
				documentConditionTemplateLine.Owner = documentConditionTemplateLineItemView.Owner;
				documentConditionTemplateLine.OwnerBusinessUnitGUID = documentConditionTemplateLineItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				documentConditionTemplateLine.DocumentConditionTemplateLineGUID = documentConditionTemplateLineItemView.DocumentConditionTemplateLineGUID.StringToGuid();
				documentConditionTemplateLine.DocumentConditionTemplateTableGUID = documentConditionTemplateLineItemView.DocumentConditionTemplateTableGUID.StringToGuidNull();
				documentConditionTemplateLine.DocumentTypeGUID = documentConditionTemplateLineItemView.DocumentTypeGUID.StringToGuid();
				documentConditionTemplateLine.Mandatory = documentConditionTemplateLineItemView.Mandatory;
				
				documentConditionTemplateLine.RowVersion = documentConditionTemplateLineItemView.RowVersion;
				return documentConditionTemplateLine;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<DocumentConditionTemplateLine> ToDocumentConditionTemplateLine(this IEnumerable<DocumentConditionTemplateLineItemView> documentConditionTemplateLineItemViews)
		{
			try
			{
				List<DocumentConditionTemplateLine> documentConditionTemplateLines = new List<DocumentConditionTemplateLine>();
				foreach (DocumentConditionTemplateLineItemView item in documentConditionTemplateLineItemViews)
				{
					documentConditionTemplateLines.Add(item.ToDocumentConditionTemplateLine());
				}
				return documentConditionTemplateLines;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static DocumentConditionTemplateLineItemView ToDocumentConditionTemplateLineItemView(this DocumentConditionTemplateLine documentConditionTemplateLine)
		{
			try
			{
				DocumentConditionTemplateLineItemView documentConditionTemplateLineItemView = new DocumentConditionTemplateLineItemView();
				documentConditionTemplateLineItemView.CompanyGUID = documentConditionTemplateLine.CompanyGUID.GuidNullToString();
				documentConditionTemplateLineItemView.CreatedBy = documentConditionTemplateLine.CreatedBy;
				documentConditionTemplateLineItemView.CreatedDateTime = documentConditionTemplateLine.CreatedDateTime.DateTimeToString();
				documentConditionTemplateLineItemView.ModifiedBy = documentConditionTemplateLine.ModifiedBy;
				documentConditionTemplateLineItemView.ModifiedDateTime = documentConditionTemplateLine.ModifiedDateTime.DateTimeToString();
				documentConditionTemplateLineItemView.Owner = documentConditionTemplateLine.Owner;
				documentConditionTemplateLineItemView.OwnerBusinessUnitGUID = documentConditionTemplateLine.OwnerBusinessUnitGUID.GuidNullToString();
				documentConditionTemplateLineItemView.DocumentConditionTemplateLineGUID = documentConditionTemplateLine.DocumentConditionTemplateLineGUID.GuidNullToString();
				documentConditionTemplateLineItemView.DocumentConditionTemplateTableGUID = documentConditionTemplateLine.DocumentConditionTemplateTableGUID.GuidNullToString();
				documentConditionTemplateLineItemView.DocumentTypeGUID = documentConditionTemplateLine.DocumentTypeGUID.GuidNullToString();
				documentConditionTemplateLineItemView.Mandatory = documentConditionTemplateLine.Mandatory;
				
				documentConditionTemplateLineItemView.RowVersion = documentConditionTemplateLine.RowVersion;
				return documentConditionTemplateLineItemView.GetDocumentConditionTemplateLineItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion DocumentConditionTemplateLineItemView
		#region ToDropDown
		public static SelectItem<DocumentConditionTemplateLineItemView> ToDropDownItem(this DocumentConditionTemplateLineItemView documentConditionTemplateLineView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<DocumentConditionTemplateLineItemView> selectItem = new SelectItem<DocumentConditionTemplateLineItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(documentConditionTemplateLineView.DocumentTypeGUID.ToString());
				selectItem.Value = documentConditionTemplateLineView.DocumentConditionTemplateLineGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: documentConditionTemplateLineView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<DocumentConditionTemplateLineItemView>> ToDropDownItem(this IEnumerable<DocumentConditionTemplateLineItemView> documentConditionTemplateLineItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<DocumentConditionTemplateLineItemView>> selectItems = new List<SelectItem<DocumentConditionTemplateLineItemView>>();
				foreach (DocumentConditionTemplateLineItemView item in documentConditionTemplateLineItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

