using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class JointVentureTransHandler
	{
		#region JointVentureTransListView
		public static List<JointVentureTransListView> GetJointVentureTransListViewValidation(this List<JointVentureTransListView> jointVentureTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return jointVentureTransListViews;
			}
			var result = new List<JointVentureTransListView>();
			try
			{
				foreach (JointVentureTransListView item in jointVentureTransListViews)
				{
					result.Add(item.GetJointVentureTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static JointVentureTransListView GetJointVentureTransListViewValidation(this JointVentureTransListView jointVentureTransListView)
		{
			try
			{
				jointVentureTransListView.RowAuthorize = jointVentureTransListView.GetListRowAuthorize();
				return jointVentureTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this JointVentureTransListView jointVentureTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion JointVentureTransListView
		#region JointVentureTransItemView
		public static JointVentureTransItemView GetJointVentureTransItemViewValidation(this JointVentureTransItemView jointVentureTransItemView)
		{
			try
			{
				jointVentureTransItemView.RowAuthorize = jointVentureTransItemView.GetItemRowAuthorize();
				return jointVentureTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this JointVentureTransItemView jointVentureTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static JointVentureTrans ToJointVentureTrans(this JointVentureTransItemView jointVentureTransItemView)
		{
			try
			{
				JointVentureTrans jointVentureTrans = new JointVentureTrans();
				jointVentureTrans.CompanyGUID = jointVentureTransItemView.CompanyGUID.StringToGuid();
				jointVentureTrans.CreatedBy = jointVentureTransItemView.CreatedBy;
				jointVentureTrans.CreatedDateTime = jointVentureTransItemView.CreatedDateTime.StringToSystemDateTime();
				jointVentureTrans.ModifiedBy = jointVentureTransItemView.ModifiedBy;
				jointVentureTrans.ModifiedDateTime = jointVentureTransItemView.ModifiedDateTime.StringToSystemDateTime();
				jointVentureTrans.Owner = jointVentureTransItemView.Owner;
				jointVentureTrans.OwnerBusinessUnitGUID = jointVentureTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				jointVentureTrans.JointVentureTransGUID = jointVentureTransItemView.JointVentureTransGUID.StringToGuid();
				jointVentureTrans.Address = jointVentureTransItemView.Address;
				jointVentureTrans.AuthorizedPersonTypeGUID = jointVentureTransItemView.AuthorizedPersonTypeGUID.StringToGuidNull();
				jointVentureTrans.Name = jointVentureTransItemView.Name;
				jointVentureTrans.OperatedBy = jointVentureTransItemView.OperatedBy;
				jointVentureTrans.Ordering = jointVentureTransItemView.Ordering;
				jointVentureTrans.RefGUID = jointVentureTransItemView.RefGUID.StringToGuid();
				jointVentureTrans.RefType = jointVentureTransItemView.RefType;
				jointVentureTrans.Remark = jointVentureTransItemView.Remark;
				
				jointVentureTrans.RowVersion = jointVentureTransItemView.RowVersion;
				return jointVentureTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<JointVentureTrans> ToJointVentureTrans(this IEnumerable<JointVentureTransItemView> jointVentureTransItemViews)
		{
			try
			{
				List<JointVentureTrans> jointVentureTranss = new List<JointVentureTrans>();
				foreach (JointVentureTransItemView item in jointVentureTransItemViews)
				{
					jointVentureTranss.Add(item.ToJointVentureTrans());
				}
				return jointVentureTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static JointVentureTransItemView ToJointVentureTransItemView(this JointVentureTrans jointVentureTrans)
		{
			try
			{
				JointVentureTransItemView jointVentureTransItemView = new JointVentureTransItemView();
				jointVentureTransItemView.CompanyGUID = jointVentureTrans.CompanyGUID.GuidNullToString();
				jointVentureTransItemView.CreatedBy = jointVentureTrans.CreatedBy;
				jointVentureTransItemView.CreatedDateTime = jointVentureTrans.CreatedDateTime.DateTimeToString();
				jointVentureTransItemView.ModifiedBy = jointVentureTrans.ModifiedBy;
				jointVentureTransItemView.ModifiedDateTime = jointVentureTrans.ModifiedDateTime.DateTimeToString();
				jointVentureTransItemView.Owner = jointVentureTrans.Owner;
				jointVentureTransItemView.OwnerBusinessUnitGUID = jointVentureTrans.OwnerBusinessUnitGUID.GuidNullToString();
				jointVentureTransItemView.JointVentureTransGUID = jointVentureTrans.JointVentureTransGUID.GuidNullToString();
				jointVentureTransItemView.Address = jointVentureTrans.Address;
				jointVentureTransItemView.AuthorizedPersonTypeGUID = jointVentureTrans.AuthorizedPersonTypeGUID.GuidNullToString();
				jointVentureTransItemView.Name = jointVentureTrans.Name;
				jointVentureTransItemView.OperatedBy = jointVentureTrans.OperatedBy;
				jointVentureTransItemView.Ordering = jointVentureTrans.Ordering;
				jointVentureTransItemView.RefGUID = jointVentureTrans.RefGUID.GuidNullToString();
				jointVentureTransItemView.RefType = jointVentureTrans.RefType;
				jointVentureTransItemView.Remark = jointVentureTrans.Remark;
				
				jointVentureTransItemView.RowVersion = jointVentureTrans.RowVersion;
				return jointVentureTransItemView.GetJointVentureTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion JointVentureTransItemView
		#region ToDropDown
		public static SelectItem<JointVentureTransItemView> ToDropDownItem(this JointVentureTransItemView jointVentureTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<JointVentureTransItemView> selectItem = new SelectItem<JointVentureTransItemView>();
				selectItem.Label = null;
				selectItem.Value = jointVentureTransView.JointVentureTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: jointVentureTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<JointVentureTransItemView>> ToDropDownItem(this IEnumerable<JointVentureTransItemView> jointVentureTransItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<JointVentureTransItemView>> selectItems = new List<SelectItem<JointVentureTransItemView>>();
				foreach (JointVentureTransItemView item in jointVentureTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

