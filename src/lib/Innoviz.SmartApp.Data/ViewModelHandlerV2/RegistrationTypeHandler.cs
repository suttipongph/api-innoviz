using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class RegistrationTypeHandler
	{
		#region RegistrationTypeListView
		public static List<RegistrationTypeListView> GetRegistrationTypeListViewValidation(this List<RegistrationTypeListView> registrationTypeListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return registrationTypeListViews;
			}
			var result = new List<RegistrationTypeListView>();
			try
			{
				foreach (RegistrationTypeListView item in registrationTypeListViews)
				{
					result.Add(item.GetRegistrationTypeListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static RegistrationTypeListView GetRegistrationTypeListViewValidation(this RegistrationTypeListView registrationTypeListView)
		{
			try
			{
				registrationTypeListView.RowAuthorize = registrationTypeListView.GetListRowAuthorize();
				return registrationTypeListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this RegistrationTypeListView registrationTypeListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion RegistrationTypeListView
		#region RegistrationTypeItemView
		public static RegistrationTypeItemView GetRegistrationTypeItemViewValidation(this RegistrationTypeItemView registrationTypeItemView)
		{
			try
			{
				registrationTypeItemView.RowAuthorize = registrationTypeItemView.GetItemRowAuthorize();
				return registrationTypeItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this RegistrationTypeItemView registrationTypeItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static RegistrationType ToRegistrationType(this RegistrationTypeItemView registrationTypeItemView)
		{
			try
			{
				RegistrationType registrationType = new RegistrationType();
				registrationType.CompanyGUID = registrationTypeItemView.CompanyGUID.StringToGuid();
				registrationType.CreatedBy = registrationTypeItemView.CreatedBy;
				registrationType.CreatedDateTime = registrationTypeItemView.CreatedDateTime.StringToSystemDateTime();
				registrationType.ModifiedBy = registrationTypeItemView.ModifiedBy;
				registrationType.ModifiedDateTime = registrationTypeItemView.ModifiedDateTime.StringToSystemDateTime();
				registrationType.Owner = registrationTypeItemView.Owner;
				registrationType.OwnerBusinessUnitGUID = registrationTypeItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				registrationType.RegistrationTypeGUID = registrationTypeItemView.RegistrationTypeGUID.StringToGuid();
				registrationType.Description = registrationTypeItemView.Description;
				registrationType.RegistrationTypeId = registrationTypeItemView.RegistrationTypeId;
				
				registrationType.RowVersion = registrationTypeItemView.RowVersion;
				return registrationType;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<RegistrationType> ToRegistrationType(this IEnumerable<RegistrationTypeItemView> registrationTypeItemViews)
		{
			try
			{
				List<RegistrationType> registrationTypes = new List<RegistrationType>();
				foreach (RegistrationTypeItemView item in registrationTypeItemViews)
				{
					registrationTypes.Add(item.ToRegistrationType());
				}
				return registrationTypes;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static RegistrationTypeItemView ToRegistrationTypeItemView(this RegistrationType registrationType)
		{
			try
			{
				RegistrationTypeItemView registrationTypeItemView = new RegistrationTypeItemView();
				registrationTypeItemView.CompanyGUID = registrationType.CompanyGUID.GuidNullToString();
				registrationTypeItemView.CreatedBy = registrationType.CreatedBy;
				registrationTypeItemView.CreatedDateTime = registrationType.CreatedDateTime.DateTimeToString();
				registrationTypeItemView.ModifiedBy = registrationType.ModifiedBy;
				registrationTypeItemView.ModifiedDateTime = registrationType.ModifiedDateTime.DateTimeToString();
				registrationTypeItemView.Owner = registrationType.Owner;
				registrationTypeItemView.OwnerBusinessUnitGUID = registrationType.OwnerBusinessUnitGUID.GuidNullToString();
				registrationTypeItemView.RegistrationTypeGUID = registrationType.RegistrationTypeGUID.GuidNullToString();
				registrationTypeItemView.Description = registrationType.Description;
				registrationTypeItemView.RegistrationTypeId = registrationType.RegistrationTypeId;
				
				registrationTypeItemView.RowVersion = registrationType.RowVersion;
				return registrationTypeItemView.GetRegistrationTypeItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion RegistrationTypeItemView
		#region ToDropDown
		public static SelectItem<RegistrationTypeItemView> ToDropDownItem(this RegistrationTypeItemView registrationTypeView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<RegistrationTypeItemView> selectItem = new SelectItem<RegistrationTypeItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(registrationTypeView.RegistrationTypeId, registrationTypeView.Description);
				selectItem.Value = registrationTypeView.RegistrationTypeGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: registrationTypeView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<RegistrationTypeItemView>> ToDropDownItem(this IEnumerable<RegistrationTypeItemView> registrationTypeItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<RegistrationTypeItemView>> selectItems = new List<SelectItem<RegistrationTypeItemView>>();
				foreach (RegistrationTypeItemView item in registrationTypeItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

