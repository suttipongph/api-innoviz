using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class ChequeTableHandler
	{
		#region ChequeTableListView
		public static List<ChequeTableListView> GetChequeTableListViewValidation(this List<ChequeTableListView> chequeTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return chequeTableListViews;
			}
			var result = new List<ChequeTableListView>();
			try
			{
				foreach (ChequeTableListView item in chequeTableListViews)
				{
					result.Add(item.GetChequeTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ChequeTableListView GetChequeTableListViewValidation(this ChequeTableListView chequeTableListView)
		{
			try
			{
				chequeTableListView.RowAuthorize = chequeTableListView.GetListRowAuthorize();
				return chequeTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this ChequeTableListView chequeTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion ChequeTableListView
		#region ChequeTableItemView
		public static ChequeTableItemView GetChequeTableItemViewValidation(this ChequeTableItemView chequeTableItemView)
		{
			try
			{
				chequeTableItemView.RowAuthorize = chequeTableItemView.GetItemRowAuthorize();
				return chequeTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this ChequeTableItemView chequeTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static ChequeTable ToChequeTable(this ChequeTableItemView chequeTableItemView)
		{
			try
			{
				ChequeTable chequeTable = new ChequeTable();
				chequeTable.CompanyGUID = chequeTableItemView.CompanyGUID.StringToGuid();
				chequeTable.CreatedBy = chequeTableItemView.CreatedBy;
				chequeTable.CreatedDateTime = chequeTableItemView.CreatedDateTime.StringToSystemDateTime();
				chequeTable.ModifiedBy = chequeTableItemView.ModifiedBy;
				chequeTable.ModifiedDateTime = chequeTableItemView.ModifiedDateTime.StringToSystemDateTime();
				chequeTable.Owner = chequeTableItemView.Owner;
				chequeTable.OwnerBusinessUnitGUID = chequeTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				chequeTable.ChequeTableGUID = chequeTableItemView.ChequeTableGUID.StringToGuid();
				chequeTable.Amount = chequeTableItemView.Amount;
				chequeTable.BuyerTableGUID = chequeTableItemView.BuyerTableGUID.StringToGuidNull();
				chequeTable.ChequeBankAccNo = chequeTableItemView.ChequeBankAccNo;
				chequeTable.ChequeBankGroupGUID = chequeTableItemView.ChequeBankGroupGUID.StringToGuid();
				chequeTable.ChequeBranch = chequeTableItemView.ChequeBranch;
				chequeTable.ChequeDate = chequeTableItemView.ChequeDate.StringToDate();
				chequeTable.ChequeNo = chequeTableItemView.ChequeNo;
				chequeTable.CompletedDate = chequeTableItemView.CompletedDate.StringNullToDateNull();
				chequeTable.CustomerTableGUID = chequeTableItemView.CustomerTableGUID.StringToGuid();
				chequeTable.DocumentStatusGUID = chequeTableItemView.DocumentStatusGUID.StringToGuid();
				chequeTable.ExpectedDepositDate = chequeTableItemView.ExpectedDepositDate.StringNullToDateNull();
				chequeTable.IssuedName = chequeTableItemView.IssuedName;
				chequeTable.PDC = chequeTableItemView.PDC;
				chequeTable.ReceivedFrom = chequeTableItemView.ReceivedFrom;
				chequeTable.RecipientName = chequeTableItemView.RecipientName;
				chequeTable.RefGUID = chequeTableItemView.RefGUID.StringToGuidNull();
				chequeTable.RefPDCGUID = chequeTableItemView.RefPDCGUID.StringToGuidNull();
				chequeTable.RefType = chequeTableItemView.RefType;
				chequeTable.Remark = chequeTableItemView.Remark;
				
				chequeTable.RowVersion = chequeTableItemView.RowVersion;
				return chequeTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<ChequeTable> ToChequeTable(this IEnumerable<ChequeTableItemView> chequeTableItemViews)
		{
			try
			{
				List<ChequeTable> chequeTables = new List<ChequeTable>();
				foreach (ChequeTableItemView item in chequeTableItemViews)
				{
					chequeTables.Add(item.ToChequeTable());
				}
				return chequeTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ChequeTableItemView ToChequeTableItemView(this ChequeTable chequeTable)
		{
			try
			{
				ChequeTableItemView chequeTableItemView = new ChequeTableItemView();
				chequeTableItemView.CompanyGUID = chequeTable.CompanyGUID.GuidNullToString();
				chequeTableItemView.CreatedBy = chequeTable.CreatedBy;
				chequeTableItemView.CreatedDateTime = chequeTable.CreatedDateTime.DateTimeToString();
				chequeTableItemView.ModifiedBy = chequeTable.ModifiedBy;
				chequeTableItemView.ModifiedDateTime = chequeTable.ModifiedDateTime.DateTimeToString();
				chequeTableItemView.Owner = chequeTable.Owner;
				chequeTableItemView.OwnerBusinessUnitGUID = chequeTable.OwnerBusinessUnitGUID.GuidNullToString();
				chequeTableItemView.ChequeTableGUID = chequeTable.ChequeTableGUID.GuidNullToString();
				chequeTableItemView.Amount = chequeTable.Amount;
				chequeTableItemView.BuyerTableGUID = chequeTable.BuyerTableGUID.GuidNullToString();
				chequeTableItemView.ChequeBankAccNo = chequeTable.ChequeBankAccNo;
				chequeTableItemView.ChequeBankGroupGUID = chequeTable.ChequeBankGroupGUID.GuidNullToString();
				chequeTableItemView.ChequeBranch = chequeTable.ChequeBranch;
				chequeTableItemView.ChequeDate = chequeTable.ChequeDate.DateToString();
				chequeTableItemView.ChequeNo = chequeTable.ChequeNo;
				chequeTableItemView.CompletedDate = chequeTable.CompletedDate.DateNullToString();
				chequeTableItemView.CustomerTableGUID = chequeTable.CustomerTableGUID.GuidNullToString();
				chequeTableItemView.DocumentStatusGUID = chequeTable.DocumentStatusGUID.GuidNullToString();
				chequeTableItemView.ExpectedDepositDate = chequeTable.ExpectedDepositDate.DateNullToString();
				chequeTableItemView.IssuedName = chequeTable.IssuedName;
				chequeTableItemView.PDC = chequeTable.PDC;
				chequeTableItemView.ReceivedFrom = chequeTable.ReceivedFrom;
				chequeTableItemView.RecipientName = chequeTable.RecipientName;
				chequeTableItemView.RefGUID = chequeTable.RefGUID.GuidNullToString();
				chequeTableItemView.RefPDCGUID = chequeTable.RefPDCGUID.GuidNullToString();
				chequeTableItemView.RefType = chequeTable.RefType;
				chequeTableItemView.Remark = chequeTable.Remark;
				
				chequeTableItemView.RowVersion = chequeTable.RowVersion;
				return chequeTableItemView.GetChequeTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ChequeTableItemView
		#region ToDropDown
		public static SelectItem<ChequeTableItemView> ToDropDownItem(this ChequeTableItemView chequeTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<ChequeTableItemView> selectItem = new SelectItem<ChequeTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(chequeTableView.ChequeNo, chequeTableView.ChequeDate.ToString());
				selectItem.Value = chequeTableView.ChequeTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: chequeTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<ChequeTableItemView>> ToDropDownItem(this IEnumerable<ChequeTableItemView> chequeTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<ChequeTableItemView>> selectItems = new List<SelectItem<ChequeTableItemView>>();
				foreach (ChequeTableItemView item in chequeTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

