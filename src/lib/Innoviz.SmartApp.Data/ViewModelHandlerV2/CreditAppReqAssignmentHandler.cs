using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CreditAppReqAssignmentHandler
	{
		#region CreditAppReqAssignmentListView
		public static List<CreditAppReqAssignmentListView> GetCreditAppReqAssignmentListViewValidation(this List<CreditAppReqAssignmentListView> creditAppReqAssignmentListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return creditAppReqAssignmentListViews;
			}
			var result = new List<CreditAppReqAssignmentListView>();
			try
			{
				foreach (CreditAppReqAssignmentListView item in creditAppReqAssignmentListViews)
				{
					result.Add(item.GetCreditAppReqAssignmentListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CreditAppReqAssignmentListView GetCreditAppReqAssignmentListViewValidation(this CreditAppReqAssignmentListView creditAppReqAssignmentListView)
		{
			try
			{
				creditAppReqAssignmentListView.RowAuthorize = creditAppReqAssignmentListView.GetListRowAuthorize();
				return creditAppReqAssignmentListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CreditAppReqAssignmentListView creditAppReqAssignmentListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CreditAppReqAssignmentListView
		#region CreditAppReqAssignmentItemView
		public static CreditAppReqAssignmentItemView GetCreditAppReqAssignmentItemViewValidation(this CreditAppReqAssignmentItemView creditAppReqAssignmentItemView)
		{
			try
			{
				creditAppReqAssignmentItemView.RowAuthorize = creditAppReqAssignmentItemView.GetItemRowAuthorize();
				return creditAppReqAssignmentItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CreditAppReqAssignmentItemView creditAppReqAssignmentItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CreditAppReqAssignment ToCreditAppReqAssignment(this CreditAppReqAssignmentItemView creditAppReqAssignmentItemView)
		{
			try
			{
				CreditAppReqAssignment creditAppReqAssignment = new CreditAppReqAssignment();
				creditAppReqAssignment.CompanyGUID = creditAppReqAssignmentItemView.CompanyGUID.StringToGuid();
				creditAppReqAssignment.CreatedBy = creditAppReqAssignmentItemView.CreatedBy;
				creditAppReqAssignment.CreatedDateTime = creditAppReqAssignmentItemView.CreatedDateTime.StringToSystemDateTime();
				creditAppReqAssignment.ModifiedBy = creditAppReqAssignmentItemView.ModifiedBy;
				creditAppReqAssignment.ModifiedDateTime = creditAppReqAssignmentItemView.ModifiedDateTime.StringToSystemDateTime();
				creditAppReqAssignment.Owner = creditAppReqAssignmentItemView.Owner;
				creditAppReqAssignment.OwnerBusinessUnitGUID = creditAppReqAssignmentItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				creditAppReqAssignment.CreditAppReqAssignmentGUID = creditAppReqAssignmentItemView.CreditAppReqAssignmentGUID.StringToGuid();
				creditAppReqAssignment.AssignmentAgreementAmount = creditAppReqAssignmentItemView.AssignmentAgreementAmount;
				creditAppReqAssignment.AssignmentAgreementTableGUID = creditAppReqAssignmentItemView.AssignmentAgreementTableGUID.StringToGuidNull();
				creditAppReqAssignment.AssignmentMethodGUID = creditAppReqAssignmentItemView.AssignmentMethodGUID.StringToGuid();
				creditAppReqAssignment.BuyerAgreementAmount = creditAppReqAssignmentItemView.BuyerAgreementAmount;
				creditAppReqAssignment.BuyerAgreementTableGUID = creditAppReqAssignmentItemView.BuyerAgreementTableGUID.StringToGuidNull();
				creditAppReqAssignment.BuyerTableGUID = creditAppReqAssignmentItemView.BuyerTableGUID.StringToGuid();
				creditAppReqAssignment.CreditAppRequestTableGUID = creditAppReqAssignmentItemView.CreditAppRequestTableGUID.StringToGuid();
				creditAppReqAssignment.CustomerTableGUID = creditAppReqAssignmentItemView.CustomerTableGUID.StringToGuid();
				creditAppReqAssignment.Description = creditAppReqAssignmentItemView.Description;
				creditAppReqAssignment.IsNew = creditAppReqAssignmentItemView.IsNew;
				creditAppReqAssignment.ReferenceAgreementId = creditAppReqAssignmentItemView.ReferenceAgreementId;
				creditAppReqAssignment.RemainingAmount = creditAppReqAssignmentItemView.RemainingAmount;
				
				creditAppReqAssignment.RowVersion = creditAppReqAssignmentItemView.RowVersion;
				return creditAppReqAssignment;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CreditAppReqAssignment> ToCreditAppReqAssignment(this IEnumerable<CreditAppReqAssignmentItemView> creditAppReqAssignmentItemViews)
		{
			try
			{
				List<CreditAppReqAssignment> creditAppReqAssignments = new List<CreditAppReqAssignment>();
				foreach (CreditAppReqAssignmentItemView item in creditAppReqAssignmentItemViews)
				{
					creditAppReqAssignments.Add(item.ToCreditAppReqAssignment());
				}
				return creditAppReqAssignments;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CreditAppReqAssignmentItemView ToCreditAppReqAssignmentItemView(this CreditAppReqAssignment creditAppReqAssignment)
		{
			try
			{
				CreditAppReqAssignmentItemView creditAppReqAssignmentItemView = new CreditAppReqAssignmentItemView();
				creditAppReqAssignmentItemView.CompanyGUID = creditAppReqAssignment.CompanyGUID.GuidNullToString();
				creditAppReqAssignmentItemView.CreatedBy = creditAppReqAssignment.CreatedBy;
				creditAppReqAssignmentItemView.CreatedDateTime = creditAppReqAssignment.CreatedDateTime.DateTimeToString();
				creditAppReqAssignmentItemView.ModifiedBy = creditAppReqAssignment.ModifiedBy;
				creditAppReqAssignmentItemView.ModifiedDateTime = creditAppReqAssignment.ModifiedDateTime.DateTimeToString();
				creditAppReqAssignmentItemView.Owner = creditAppReqAssignment.Owner;
				creditAppReqAssignmentItemView.OwnerBusinessUnitGUID = creditAppReqAssignment.OwnerBusinessUnitGUID.GuidNullToString();
				creditAppReqAssignmentItemView.CreditAppReqAssignmentGUID = creditAppReqAssignment.CreditAppReqAssignmentGUID.GuidNullToString();
				creditAppReqAssignmentItemView.AssignmentAgreementAmount = creditAppReqAssignment.AssignmentAgreementAmount;
				creditAppReqAssignmentItemView.AssignmentAgreementTableGUID = creditAppReqAssignment.AssignmentAgreementTableGUID.GuidNullToString();
				creditAppReqAssignmentItemView.AssignmentMethodGUID = creditAppReqAssignment.AssignmentMethodGUID.GuidNullToString();
				creditAppReqAssignmentItemView.BuyerAgreementAmount = creditAppReqAssignment.BuyerAgreementAmount;
				creditAppReqAssignmentItemView.BuyerAgreementTableGUID = creditAppReqAssignment.BuyerAgreementTableGUID.GuidNullToString();
				creditAppReqAssignmentItemView.BuyerTableGUID = creditAppReqAssignment.BuyerTableGUID.GuidNullToString();
				creditAppReqAssignmentItemView.CreditAppRequestTableGUID = creditAppReqAssignment.CreditAppRequestTableGUID.GuidNullToString();
				creditAppReqAssignmentItemView.CustomerTableGUID = creditAppReqAssignment.CustomerTableGUID.GuidNullToString();
				creditAppReqAssignmentItemView.Description = creditAppReqAssignment.Description;
				creditAppReqAssignmentItemView.IsNew = creditAppReqAssignment.IsNew;
				creditAppReqAssignmentItemView.ReferenceAgreementId = creditAppReqAssignment.ReferenceAgreementId;
				creditAppReqAssignmentItemView.RemainingAmount = creditAppReqAssignment.RemainingAmount;
				
				creditAppReqAssignmentItemView.RowVersion = creditAppReqAssignment.RowVersion;
				return creditAppReqAssignmentItemView.GetCreditAppReqAssignmentItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CreditAppReqAssignmentItemView
		#region ToDropDown
		public static SelectItem<CreditAppReqAssignmentItemView> ToDropDownItem(this CreditAppReqAssignmentItemView creditAppReqAssignmentView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CreditAppReqAssignmentItemView> selectItem = new SelectItem<CreditAppReqAssignmentItemView>();
				selectItem.Label = creditAppReqAssignmentView.Description;
				selectItem.Value = creditAppReqAssignmentView.CreditAppReqAssignmentGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: creditAppReqAssignmentView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CreditAppReqAssignmentItemView>> ToDropDownItem(this IEnumerable<CreditAppReqAssignmentItemView> creditAppReqAssignmentItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CreditAppReqAssignmentItemView>> selectItems = new List<SelectItem<CreditAppReqAssignmentItemView>>();
				foreach (CreditAppReqAssignmentItemView item in creditAppReqAssignmentItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

