using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class BlacklistStatusHandler
	{
		#region BlacklistStatusListView
		public static List<BlacklistStatusListView> GetBlacklistStatusListViewValidation(this List<BlacklistStatusListView> blacklistStatusListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return blacklistStatusListViews;
			}
			var result = new List<BlacklistStatusListView>();
			try
			{
				foreach (BlacklistStatusListView item in blacklistStatusListViews)
				{
					result.Add(item.GetBlacklistStatusListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BlacklistStatusListView GetBlacklistStatusListViewValidation(this BlacklistStatusListView blacklistStatusListView)
		{
			try
			{
				blacklistStatusListView.RowAuthorize = blacklistStatusListView.GetListRowAuthorize();
				return blacklistStatusListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this BlacklistStatusListView blacklistStatusListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion BlacklistStatusListView
		#region BlacklistStatusItemView
		public static BlacklistStatusItemView GetBlacklistStatusItemViewValidation(this BlacklistStatusItemView blacklistStatusItemView)
		{
			try
			{
				blacklistStatusItemView.RowAuthorize = blacklistStatusItemView.GetItemRowAuthorize();
				return blacklistStatusItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this BlacklistStatusItemView blacklistStatusItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static BlacklistStatus ToBlacklistStatus(this BlacklistStatusItemView blacklistStatusItemView)
		{
			try
			{
				BlacklistStatus blacklistStatus = new BlacklistStatus();
				blacklistStatus.CompanyGUID = blacklistStatusItemView.CompanyGUID.StringToGuid();
				blacklistStatus.CreatedBy = blacklistStatusItemView.CreatedBy;
				blacklistStatus.CreatedDateTime = blacklistStatusItemView.CreatedDateTime.StringToSystemDateTime();
				blacklistStatus.ModifiedBy = blacklistStatusItemView.ModifiedBy;
				blacklistStatus.ModifiedDateTime = blacklistStatusItemView.ModifiedDateTime.StringToSystemDateTime();
				blacklistStatus.Owner = blacklistStatusItemView.Owner;
				blacklistStatus.OwnerBusinessUnitGUID = blacklistStatusItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				blacklistStatus.BlacklistStatusGUID = blacklistStatusItemView.BlacklistStatusGUID.StringToGuid();
				blacklistStatus.BlacklistStatusId = blacklistStatusItemView.BlacklistStatusId;
				blacklistStatus.Description = blacklistStatusItemView.Description;
				
				blacklistStatus.RowVersion = blacklistStatusItemView.RowVersion;
				return blacklistStatus;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<BlacklistStatus> ToBlacklistStatus(this IEnumerable<BlacklistStatusItemView> blacklistStatusItemViews)
		{
			try
			{
				List<BlacklistStatus> blacklistStatuss = new List<BlacklistStatus>();
				foreach (BlacklistStatusItemView item in blacklistStatusItemViews)
				{
					blacklistStatuss.Add(item.ToBlacklistStatus());
				}
				return blacklistStatuss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BlacklistStatusItemView ToBlacklistStatusItemView(this BlacklistStatus blacklistStatus)
		{
			try
			{
				BlacklistStatusItemView blacklistStatusItemView = new BlacklistStatusItemView();
				blacklistStatusItemView.CompanyGUID = blacklistStatus.CompanyGUID.GuidNullToString();
				blacklistStatusItemView.CreatedBy = blacklistStatus.CreatedBy;
				blacklistStatusItemView.CreatedDateTime = blacklistStatus.CreatedDateTime.DateTimeToString();
				blacklistStatusItemView.ModifiedBy = blacklistStatus.ModifiedBy;
				blacklistStatusItemView.ModifiedDateTime = blacklistStatus.ModifiedDateTime.DateTimeToString();
				blacklistStatusItemView.Owner = blacklistStatus.Owner;
				blacklistStatusItemView.OwnerBusinessUnitGUID = blacklistStatus.OwnerBusinessUnitGUID.GuidNullToString();
				blacklistStatusItemView.BlacklistStatusGUID = blacklistStatus.BlacklistStatusGUID.GuidNullToString();
				blacklistStatusItemView.BlacklistStatusId = blacklistStatus.BlacklistStatusId;
				blacklistStatusItemView.Description = blacklistStatus.Description;
				
				blacklistStatusItemView.RowVersion = blacklistStatus.RowVersion;
				return blacklistStatusItemView.GetBlacklistStatusItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion BlacklistStatusItemView
		#region ToDropDown
		public static SelectItem<BlacklistStatusItemView> ToDropDownItem(this BlacklistStatusItemView blacklistStatusView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<BlacklistStatusItemView> selectItem = new SelectItem<BlacklistStatusItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(blacklistStatusView.BlacklistStatusId, blacklistStatusView.Description);
				selectItem.Value = blacklistStatusView.BlacklistStatusGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: blacklistStatusView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<BlacklistStatusItemView>> ToDropDownItem(this IEnumerable<BlacklistStatusItemView> blacklistStatusItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<BlacklistStatusItemView>> selectItems = new List<SelectItem<BlacklistStatusItemView>>();
				foreach (BlacklistStatusItemView item in blacklistStatusItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

