using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class PurchaseTableHandler
	{
		#region PurchaseTableListView
		public static List<PurchaseTableListView> GetPurchaseTableListViewValidation(this List<PurchaseTableListView> purchaseTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return purchaseTableListViews;
			}
			var result = new List<PurchaseTableListView>();
			try
			{
				foreach (PurchaseTableListView item in purchaseTableListViews)
				{
					result.Add(item.GetPurchaseTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static PurchaseTableListView GetPurchaseTableListViewValidation(this PurchaseTableListView purchaseTableListView)
		{
			try
			{
				purchaseTableListView.RowAuthorize = purchaseTableListView.GetListRowAuthorize();
				return purchaseTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this PurchaseTableListView purchaseTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion PurchaseTableListView
		#region PurchaseTableItemView
		public static PurchaseTableItemView GetPurchaseTableItemViewValidation(this PurchaseTableItemView purchaseTableItemView)
		{
			try
			{
				purchaseTableItemView.RowAuthorize = purchaseTableItemView.GetItemRowAuthorize();
				return purchaseTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this PurchaseTableItemView purchaseTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static PurchaseTable ToPurchaseTable(this PurchaseTableItemView purchaseTableItemView)
		{
			try
			{
				PurchaseTable purchaseTable = new PurchaseTable();
				purchaseTable.CompanyGUID = purchaseTableItemView.CompanyGUID.StringToGuid();
				purchaseTable.CreatedBy = purchaseTableItemView.CreatedBy;
				purchaseTable.CreatedDateTime = purchaseTableItemView.CreatedDateTime.StringToSystemDateTime();
				purchaseTable.ModifiedBy = purchaseTableItemView.ModifiedBy;
				purchaseTable.ModifiedDateTime = purchaseTableItemView.ModifiedDateTime.StringToSystemDateTime();
				purchaseTable.Owner = purchaseTableItemView.Owner;
				purchaseTable.OwnerBusinessUnitGUID = purchaseTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				purchaseTable.PurchaseTableGUID = purchaseTableItemView.PurchaseTableGUID.StringToGuid();
				purchaseTable.AdditionalPurchase = purchaseTableItemView.AdditionalPurchase;
				purchaseTable.CreditAppTableGUID = purchaseTableItemView.CreditAppTableGUID.StringToGuid();
				purchaseTable.CustomerTableGUID = purchaseTableItemView.CustomerTableGUID.StringToGuid();
				purchaseTable.Description = purchaseTableItemView.Description;
				purchaseTable.DiffChequeIssuedName = purchaseTableItemView.DiffChequeIssuedName;
				purchaseTable.Dimension1GUID = purchaseTableItemView.Dimension1GUID.StringToGuidNull();
				purchaseTable.Dimension2GUID = purchaseTableItemView.Dimension2GUID.StringToGuidNull();
				purchaseTable.Dimension3GUID = purchaseTableItemView.Dimension3GUID.StringToGuidNull();
				purchaseTable.Dimension4GUID = purchaseTableItemView.Dimension4GUID.StringToGuidNull();
				purchaseTable.Dimension5GUID = purchaseTableItemView.Dimension5GUID.StringToGuidNull();
				purchaseTable.DocumentStatusGUID = purchaseTableItemView.DocumentStatusGUID.StringToGuid();
				purchaseTable.OverCreditBuyer = purchaseTableItemView.OverCreditBuyer;
				purchaseTable.OverCreditCA = purchaseTableItemView.OverCreditCA;
				purchaseTable.OverCreditCALine = purchaseTableItemView.OverCreditCALine;
				purchaseTable.ProductType = purchaseTableItemView.ProductType;
				purchaseTable.PurchaseDate = purchaseTableItemView.PurchaseDate.StringToDate();
				purchaseTable.PurchaseId = purchaseTableItemView.PurchaseId;
				purchaseTable.ReceiptTempTableGUID = purchaseTableItemView.ReceiptTempTableGUID.StringToGuidNull();
				purchaseTable.RetentionCalculateBase = purchaseTableItemView.RetentionCalculateBase;
				purchaseTable.RetentionFixedAmount = purchaseTableItemView.RetentionFixedAmount;
				purchaseTable.RetentionPct = purchaseTableItemView.RetentionPct;
				purchaseTable.Rollbill = purchaseTableItemView.Rollbill;
				purchaseTable.SumPurchaseAmount = purchaseTableItemView.SumPurchaseAmount;
				purchaseTable.TotalInterestPct = purchaseTableItemView.TotalInterestPct;
				purchaseTable.ProcessInstanceId = purchaseTableItemView.ProcessInstanceId;
				purchaseTable.DocumentReasonGUID = purchaseTableItemView.DocumentReasonGUID.StringToGuidNull();
				purchaseTable.Remark = purchaseTableItemView.Remark;
				purchaseTable.OperReportSignatureGUID = purchaseTableItemView.OperReportSignatureGUID.StringToGuid();

				purchaseTable.RowVersion = purchaseTableItemView.RowVersion;
				return purchaseTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<PurchaseTable> ToPurchaseTable(this IEnumerable<PurchaseTableItemView> purchaseTableItemViews)
		{
			try
			{
				List<PurchaseTable> purchaseTables = new List<PurchaseTable>();
				foreach (PurchaseTableItemView item in purchaseTableItemViews)
				{
					purchaseTables.Add(item.ToPurchaseTable());
				}
				return purchaseTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static PurchaseTableItemView ToPurchaseTableItemView(this PurchaseTable purchaseTable)
		{
			try
			{
				PurchaseTableItemView purchaseTableItemView = new PurchaseTableItemView();
				purchaseTableItemView.CompanyGUID = purchaseTable.CompanyGUID.GuidNullToString();
				purchaseTableItemView.CreatedBy = purchaseTable.CreatedBy;
				purchaseTableItemView.CreatedDateTime = purchaseTable.CreatedDateTime.DateTimeToString();
				purchaseTableItemView.ModifiedBy = purchaseTable.ModifiedBy;
				purchaseTableItemView.ModifiedDateTime = purchaseTable.ModifiedDateTime.DateTimeToString();
				purchaseTableItemView.Owner = purchaseTable.Owner;
				purchaseTableItemView.OwnerBusinessUnitGUID = purchaseTable.OwnerBusinessUnitGUID.GuidNullToString();
				purchaseTableItemView.PurchaseTableGUID = purchaseTable.PurchaseTableGUID.GuidNullToString();
				purchaseTableItemView.AdditionalPurchase = purchaseTable.AdditionalPurchase;
				purchaseTableItemView.CreditAppTableGUID = purchaseTable.CreditAppTableGUID.GuidNullToString();
				purchaseTableItemView.CustomerTableGUID = purchaseTable.CustomerTableGUID.GuidNullToString();
				purchaseTableItemView.Description = purchaseTable.Description;
				purchaseTableItemView.DiffChequeIssuedName = purchaseTable.DiffChequeIssuedName;
				purchaseTableItemView.Dimension1GUID = purchaseTable.Dimension1GUID.GuidNullToString();
				purchaseTableItemView.Dimension2GUID = purchaseTable.Dimension2GUID.GuidNullToString();
				purchaseTableItemView.Dimension3GUID = purchaseTable.Dimension3GUID.GuidNullToString();
				purchaseTableItemView.Dimension4GUID = purchaseTable.Dimension4GUID.GuidNullToString();
				purchaseTableItemView.Dimension5GUID = purchaseTable.Dimension5GUID.GuidNullToString();
				purchaseTableItemView.DocumentStatusGUID = purchaseTable.DocumentStatusGUID.GuidNullToString();
				purchaseTableItemView.OverCreditBuyer = purchaseTable.OverCreditBuyer;
				purchaseTableItemView.OverCreditCA = purchaseTable.OverCreditCA;
				purchaseTableItemView.OverCreditCALine = purchaseTable.OverCreditCALine;
				purchaseTableItemView.ProductType = purchaseTable.ProductType;
				purchaseTableItemView.PurchaseDate = purchaseTable.PurchaseDate.DateToString();
				purchaseTableItemView.PurchaseId = purchaseTable.PurchaseId;
				purchaseTableItemView.ReceiptTempTableGUID = purchaseTable.ReceiptTempTableGUID.GuidNullToString();
				purchaseTableItemView.RetentionCalculateBase = purchaseTable.RetentionCalculateBase;
				purchaseTableItemView.RetentionFixedAmount = purchaseTable.RetentionFixedAmount;
				purchaseTableItemView.RetentionPct = purchaseTable.RetentionPct;
				purchaseTableItemView.Rollbill = purchaseTable.Rollbill;
				purchaseTableItemView.SumPurchaseAmount = purchaseTable.SumPurchaseAmount;
				purchaseTableItemView.TotalInterestPct = purchaseTable.TotalInterestPct;
				purchaseTableItemView.ProcessInstanceId = purchaseTable.ProcessInstanceId;
				purchaseTableItemView.DocumentReasonGUID = purchaseTable.DocumentReasonGUID.GuidNullToString();
				purchaseTableItemView.Remark = purchaseTable.Remark;
				purchaseTableItemView.OperReportSignatureGUID = purchaseTable.OperReportSignatureGUID.GuidNullToString();

				purchaseTableItemView.RowVersion = purchaseTable.RowVersion;
				return purchaseTableItemView.GetPurchaseTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion PurchaseTableItemView
		#region ToDropDown
		public static SelectItem<PurchaseTableItemView> ToDropDownItem(this PurchaseTableItemView purchaseTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<PurchaseTableItemView> selectItem = new SelectItem<PurchaseTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(purchaseTableView.PurchaseId, purchaseTableView.Description);
				selectItem.Value = purchaseTableView.PurchaseTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: purchaseTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<PurchaseTableItemView>> ToDropDownItem(this IEnumerable<PurchaseTableItemView> purchaseTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<PurchaseTableItemView>> selectItems = new List<SelectItem<PurchaseTableItemView>>();
				foreach (PurchaseTableItemView item in purchaseTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}
