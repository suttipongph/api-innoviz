using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class OccupationHandler
	{
		#region OccupationListView
		public static List<OccupationListView> GetOccupationListViewValidation(this List<OccupationListView> occupationListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return occupationListViews;
			}
			var result = new List<OccupationListView>();
			try
			{
				foreach (OccupationListView item in occupationListViews)
				{
					result.Add(item.GetOccupationListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static OccupationListView GetOccupationListViewValidation(this OccupationListView occupationListView)
		{
			try
			{
				occupationListView.RowAuthorize = occupationListView.GetListRowAuthorize();
				return occupationListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this OccupationListView occupationListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion OccupationListView
		#region OccupationItemView
		public static OccupationItemView GetOccupationItemViewValidation(this OccupationItemView occupationItemView)
		{
			try
			{
				occupationItemView.RowAuthorize = occupationItemView.GetItemRowAuthorize();
				return occupationItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this OccupationItemView occupationItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static Occupation ToOccupation(this OccupationItemView occupationItemView)
		{
			try
			{
				Occupation occupation = new Occupation();
				occupation.CompanyGUID = occupationItemView.CompanyGUID.StringToGuid();
				occupation.CreatedBy = occupationItemView.CreatedBy;
				occupation.CreatedDateTime = occupationItemView.CreatedDateTime.StringToSystemDateTime();
				occupation.ModifiedBy = occupationItemView.ModifiedBy;
				occupation.ModifiedDateTime = occupationItemView.ModifiedDateTime.StringToSystemDateTime();
				occupation.Owner = occupationItemView.Owner;
				occupation.OwnerBusinessUnitGUID = occupationItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				occupation.OccupationGUID = occupationItemView.OccupationGUID.StringToGuid();
				occupation.Description = occupationItemView.Description;
				occupation.OccupationId = occupationItemView.OccupationId;
				
				occupation.RowVersion = occupationItemView.RowVersion;
				return occupation;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<Occupation> ToOccupation(this IEnumerable<OccupationItemView> occupationItemViews)
		{
			try
			{
				List<Occupation> occupations = new List<Occupation>();
				foreach (OccupationItemView item in occupationItemViews)
				{
					occupations.Add(item.ToOccupation());
				}
				return occupations;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static OccupationItemView ToOccupationItemView(this Occupation occupation)
		{
			try
			{
				OccupationItemView occupationItemView = new OccupationItemView();
				occupationItemView.CompanyGUID = occupation.CompanyGUID.GuidNullToString();
				occupationItemView.CreatedBy = occupation.CreatedBy;
				occupationItemView.CreatedDateTime = occupation.CreatedDateTime.DateTimeToString();
				occupationItemView.ModifiedBy = occupation.ModifiedBy;
				occupationItemView.ModifiedDateTime = occupation.ModifiedDateTime.DateTimeToString();
				occupationItemView.Owner = occupation.Owner;
				occupationItemView.OwnerBusinessUnitGUID = occupation.OwnerBusinessUnitGUID.GuidNullToString();
				occupationItemView.OccupationGUID = occupation.OccupationGUID.GuidNullToString();
				occupationItemView.Description = occupation.Description;
				occupationItemView.OccupationId = occupation.OccupationId;
				
				occupationItemView.RowVersion = occupation.RowVersion;
				return occupationItemView.GetOccupationItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion OccupationItemView
		#region ToDropDown
		public static SelectItem<OccupationItemView> ToDropDownItem(this OccupationItemView occupationView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<OccupationItemView> selectItem = new SelectItem<OccupationItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(occupationView.OccupationId, occupationView.Description);
				selectItem.Value = occupationView.OccupationGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: occupationView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<OccupationItemView>> ToDropDownItem(this IEnumerable<OccupationItemView> occupationItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<OccupationItemView>> selectItems = new List<SelectItem<OccupationItemView>>();
				foreach (OccupationItemView item in occupationItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

