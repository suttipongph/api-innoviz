using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class DocumentStatusHandler
	{
		#region DocumentStatusListView
		public static List<DocumentStatusListView> GetDocumentStatusListViewValidation(this List<DocumentStatusListView> documentStatusListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return documentStatusListViews;
			}
			var result = new List<DocumentStatusListView>();
			try
			{
				foreach (DocumentStatusListView item in documentStatusListViews)
				{
					result.Add(item.GetDocumentStatusListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static DocumentStatusListView GetDocumentStatusListViewValidation(this DocumentStatusListView documentStatusListView)
		{
			try
			{
				documentStatusListView.RowAuthorize = documentStatusListView.GetListRowAuthorize();
				return documentStatusListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this DocumentStatusListView documentStatusListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion DocumentStatusListView
		#region DocumentStatusItemView
		public static DocumentStatusItemView GetDocumentStatusItemViewValidation(this DocumentStatusItemView documentStatusItemView)
		{
			try
			{
				documentStatusItemView.RowAuthorize = documentStatusItemView.GetItemRowAuthorize();
				return documentStatusItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this DocumentStatusItemView documentStatusItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static DocumentStatus ToDocumentStatus(this DocumentStatusItemView documentStatusItemView)
		{
			try
			{
				DocumentStatus documentStatus = new DocumentStatus();
				documentStatus.CreatedBy = documentStatusItemView.CreatedBy;
				documentStatus.CreatedDateTime = documentStatusItemView.CreatedDateTime.StringToSystemDateTime();
				documentStatus.ModifiedBy = documentStatusItemView.ModifiedBy;
				documentStatus.ModifiedDateTime = documentStatusItemView.ModifiedDateTime.StringToSystemDateTime();
				documentStatus.DocumentStatusGUID = documentStatusItemView.DocumentStatusGUID.StringToGuid();
				documentStatus.Description = documentStatusItemView.Description;
				documentStatus.DocumentProcessGUID = documentStatusItemView.DocumentProcessGUID.StringToGuid();
				documentStatus.StatusCode = documentStatusItemView.StatusCode;
				documentStatus.StatusId = documentStatusItemView.StatusId;
				
				documentStatus.RowVersion = documentStatusItemView.RowVersion;
				return documentStatus;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<DocumentStatus> ToDocumentStatus(this IEnumerable<DocumentStatusItemView> documentStatusItemViews)
		{
			try
			{
				List<DocumentStatus> documentStatuss = new List<DocumentStatus>();
				foreach (DocumentStatusItemView item in documentStatusItemViews)
				{
					documentStatuss.Add(item.ToDocumentStatus());
				}
				return documentStatuss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static DocumentStatusItemView ToDocumentStatusItemView(this DocumentStatus documentStatus)
		{
			try
			{
				DocumentStatusItemView documentStatusItemView = new DocumentStatusItemView();
				documentStatusItemView.CreatedBy = documentStatus.CreatedBy;
				documentStatusItemView.CreatedDateTime = documentStatus.CreatedDateTime.DateTimeToString();
				documentStatusItemView.ModifiedBy = documentStatus.ModifiedBy;
				documentStatusItemView.ModifiedDateTime = documentStatus.ModifiedDateTime.DateTimeToString();
				documentStatusItemView.DocumentStatusGUID = documentStatus.DocumentStatusGUID.GuidNullToString();
				documentStatusItemView.Description = documentStatus.Description;
				documentStatusItemView.DocumentProcessGUID = documentStatus.DocumentProcessGUID.GuidNullToString();
				documentStatusItemView.StatusCode = documentStatus.StatusCode;
				documentStatusItemView.StatusId = documentStatus.StatusId;
				
				documentStatusItemView.RowVersion = documentStatus.RowVersion;
				return documentStatusItemView.GetDocumentStatusItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion DocumentStatusItemView
		#region ToDropDown
		public static SelectItem<DocumentStatusItemView> ToDropDownItem(this DocumentStatusItemView documentStatusView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<DocumentStatusItemView> selectItem = new SelectItem<DocumentStatusItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(documentStatusView.Description);
				selectItem.Value = documentStatusView.DocumentStatusGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: documentStatusView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<DocumentStatusItemView>> ToDropDownItem(this IEnumerable<DocumentStatusItemView> documentStatusItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<DocumentStatusItemView>> selectItems = new List<SelectItem<DocumentStatusItemView>>();
				foreach (DocumentStatusItemView item in documentStatusItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

