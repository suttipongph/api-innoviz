using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CreditTermHandler
	{
		#region CreditTermListView
		public static List<CreditTermListView> GetCreditTermListViewValidation(this List<CreditTermListView> creditTermListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return creditTermListViews;
			}
			var result = new List<CreditTermListView>();
			try
			{
				foreach (CreditTermListView item in creditTermListViews)
				{
					result.Add(item.GetCreditTermListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CreditTermListView GetCreditTermListViewValidation(this CreditTermListView creditTermListView)
		{
			try
			{
				creditTermListView.RowAuthorize = creditTermListView.GetListRowAuthorize();
				return creditTermListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CreditTermListView creditTermListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CreditTermListView
		#region CreditTermItemView
		public static CreditTermItemView GetCreditTermItemViewValidation(this CreditTermItemView creditTermItemView)
		{
			try
			{
				creditTermItemView.RowAuthorize = creditTermItemView.GetItemRowAuthorize();
				return creditTermItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CreditTermItemView creditTermItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CreditTerm ToCreditTerm(this CreditTermItemView creditTermItemView)
		{
			try
			{
				CreditTerm creditTerm = new CreditTerm();
				creditTerm.CompanyGUID = creditTermItemView.CompanyGUID.StringToGuid();
				creditTerm.CreatedBy = creditTermItemView.CreatedBy;
				creditTerm.CreatedDateTime = creditTermItemView.CreatedDateTime.StringToSystemDateTime();
				creditTerm.ModifiedBy = creditTermItemView.ModifiedBy;
				creditTerm.ModifiedDateTime = creditTermItemView.ModifiedDateTime.StringToSystemDateTime();
				creditTerm.Owner = creditTermItemView.Owner;
				creditTerm.OwnerBusinessUnitGUID = creditTermItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				creditTerm.CreditTermGUID = creditTermItemView.CreditTermGUID.StringToGuid();
				creditTerm.CreditTermId = creditTermItemView.CreditTermId;
				creditTerm.Description = creditTermItemView.Description;
				creditTerm.NumberOfDays = creditTermItemView.NumberOfDays;
				
				creditTerm.RowVersion = creditTermItemView.RowVersion;
				return creditTerm;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CreditTerm> ToCreditTerm(this IEnumerable<CreditTermItemView> creditTermItemViews)
		{
			try
			{
				List<CreditTerm> creditTerms = new List<CreditTerm>();
				foreach (CreditTermItemView item in creditTermItemViews)
				{
					creditTerms.Add(item.ToCreditTerm());
				}
				return creditTerms;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CreditTermItemView ToCreditTermItemView(this CreditTerm creditTerm)
		{
			try
			{
				CreditTermItemView creditTermItemView = new CreditTermItemView();
				creditTermItemView.CompanyGUID = creditTerm.CompanyGUID.GuidNullToString();
				creditTermItemView.CreatedBy = creditTerm.CreatedBy;
				creditTermItemView.CreatedDateTime = creditTerm.CreatedDateTime.DateTimeToString();
				creditTermItemView.ModifiedBy = creditTerm.ModifiedBy;
				creditTermItemView.ModifiedDateTime = creditTerm.ModifiedDateTime.DateTimeToString();
				creditTermItemView.Owner = creditTerm.Owner;
				creditTermItemView.OwnerBusinessUnitGUID = creditTerm.OwnerBusinessUnitGUID.GuidNullToString();
				creditTermItemView.CreditTermGUID = creditTerm.CreditTermGUID.GuidNullToString();
				creditTermItemView.CreditTermId = creditTerm.CreditTermId;
				creditTermItemView.Description = creditTerm.Description;
				creditTermItemView.NumberOfDays = creditTerm.NumberOfDays;
				
				creditTermItemView.RowVersion = creditTerm.RowVersion;
				return creditTermItemView.GetCreditTermItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CreditTermItemView
		#region ToDropDown
		public static SelectItem<CreditTermItemView> ToDropDownItem(this CreditTermItemView creditTermView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CreditTermItemView> selectItem = new SelectItem<CreditTermItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(creditTermView.CreditTermId, creditTermView.Description);
				selectItem.Value = creditTermView.CreditTermGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: creditTermView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CreditTermItemView>> ToDropDownItem(this IEnumerable<CreditTermItemView> creditTermItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CreditTermItemView>> selectItems = new List<SelectItem<CreditTermItemView>>();
				foreach (CreditTermItemView item in creditTermItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

