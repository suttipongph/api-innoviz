using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class AttachmentHandler
	{
		#region AttachmentListView
		public static List<AttachmentListView> GetAttachmentListViewValidation(this List<AttachmentListView> attachmentListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return attachmentListViews;
			}
			var result = new List<AttachmentListView>();
			try
			{
				foreach (AttachmentListView item in attachmentListViews)
				{
					result.Add(item.GetAttachmentListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AttachmentListView GetAttachmentListViewValidation(this AttachmentListView attachmentListView)
		{
			try
			{
				attachmentListView.RowAuthorize = attachmentListView.GetListRowAuthorize();
				return attachmentListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this AttachmentListView attachmentListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion AttachmentListView
		#region AttachmentItemView
		public static AttachmentItemView GetAttachmentItemViewValidation(this AttachmentItemView attachmentItemView)
		{
			try
			{
				attachmentItemView.RowAuthorize = attachmentItemView.GetItemRowAuthorize();
				return attachmentItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this AttachmentItemView attachmentItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static Attachment ToAttachment(this AttachmentItemView attachmentItemView)
		{
			try
			{
				Attachment attachment = new Attachment();
				attachment.CreatedBy = attachmentItemView.CreatedBy;
				attachment.CreatedDateTime = attachmentItemView.CreatedDateTime.StringToSystemDateTime();
				attachment.ModifiedBy = attachmentItemView.ModifiedBy;
				attachment.ModifiedDateTime = attachmentItemView.ModifiedDateTime.StringToSystemDateTime();
				attachment.AttachmentGUID = attachmentItemView.AttachmentGUID.StringToGuid();
				attachment.ContentType = attachmentItemView.ContentType;
				attachment.FileName = attachmentItemView.FileName;
				attachment.FileDescription = attachmentItemView.FileDescription;
				attachment.FilePath = attachmentItemView.FilePath;
				attachment.Base64Data = attachmentItemView.Base64Data;
				attachment.RefType = attachmentItemView.RefType;
				attachment.RefGUID = attachmentItemView.RefGUID.StringToGuid();
				
				attachment.RowVersion = attachmentItemView.RowVersion;
				return attachment;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<Attachment> ToAttachment(this IEnumerable<AttachmentItemView> attachmentItemViews)
		{
			try
			{
				List<Attachment> attachments = new List<Attachment>();
				foreach (AttachmentItemView item in attachmentItemViews)
				{
					attachments.Add(item.ToAttachment());
				}
				return attachments;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AttachmentItemView ToAttachmentItemView(this Attachment attachment)
		{
			try
			{
				AttachmentItemView attachmentItemView = new AttachmentItemView();
				attachmentItemView.CreatedBy = attachment.CreatedBy;
				attachmentItemView.CreatedDateTime = attachment.CreatedDateTime.DateTimeToString();
				attachmentItemView.ModifiedBy = attachment.ModifiedBy;
				attachmentItemView.ModifiedDateTime = attachment.ModifiedDateTime.DateTimeToString();
				attachmentItemView.AttachmentGUID = attachment.AttachmentGUID.GuidNullToString();
				attachmentItemView.Base64Data = attachment.Base64Data;
				attachmentItemView.FileDescription = attachment.FileDescription;
				attachmentItemView.RefType = attachment.RefType;
				attachmentItemView.RefGUID = attachment.RefGUID.GuidNullToString();
				attachmentItemView.FileName = attachment.FileName;
				attachmentItemView.FilePath = attachment.FilePath;
				attachmentItemView.ContentType = attachment.ContentType;
				
				attachmentItemView.RowVersion = attachment.RowVersion;
				return attachmentItemView.GetAttachmentItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion AttachmentItemView
		#region ToDropDown
		public static SelectItem<AttachmentItemView> ToDropDownItem(this AttachmentItemView attachmentView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<AttachmentItemView> selectItem = new SelectItem<AttachmentItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(attachmentView.FileName, attachmentView.FileDescription);
				selectItem.Value = attachmentView.AttachmentGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: attachmentView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<AttachmentItemView>> ToDropDownItem(this IEnumerable<AttachmentItemView> attachmentItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<AttachmentItemView>> selectItems = new List<SelectItem<AttachmentItemView>>();
				foreach (AttachmentItemView item in attachmentItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

