using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class BusinessSizeHandler
	{
		#region BusinessSizeListView
		public static List<BusinessSizeListView> GetBusinessSizeListViewValidation(this List<BusinessSizeListView> businessSizeListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return businessSizeListViews;
			}
			var result = new List<BusinessSizeListView>();
			try
			{
				foreach (BusinessSizeListView item in businessSizeListViews)
				{
					result.Add(item.GetBusinessSizeListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BusinessSizeListView GetBusinessSizeListViewValidation(this BusinessSizeListView businessSizeListView)
		{
			try
			{
				businessSizeListView.RowAuthorize = businessSizeListView.GetListRowAuthorize();
				return businessSizeListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this BusinessSizeListView businessSizeListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion BusinessSizeListView
		#region BusinessSizeItemView
		public static BusinessSizeItemView GetBusinessSizeItemViewValidation(this BusinessSizeItemView businessSizeItemView)
		{
			try
			{
				businessSizeItemView.RowAuthorize = businessSizeItemView.GetItemRowAuthorize();
				return businessSizeItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this BusinessSizeItemView businessSizeItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static BusinessSize ToBusinessSize(this BusinessSizeItemView businessSizeItemView)
		{
			try
			{
				BusinessSize businessSize = new BusinessSize();
				businessSize.CompanyGUID = businessSizeItemView.CompanyGUID.StringToGuid();
				businessSize.CreatedBy = businessSizeItemView.CreatedBy;
				businessSize.CreatedDateTime = businessSizeItemView.CreatedDateTime.StringToSystemDateTime();
				businessSize.ModifiedBy = businessSizeItemView.ModifiedBy;
				businessSize.ModifiedDateTime = businessSizeItemView.ModifiedDateTime.StringToSystemDateTime();
				businessSize.Owner = businessSizeItemView.Owner;
				businessSize.OwnerBusinessUnitGUID = businessSizeItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				businessSize.BusinessSizeGUID = businessSizeItemView.BusinessSizeGUID.StringToGuid();
				businessSize.BusinessSizeId = businessSizeItemView.BusinessSizeId;
				businessSize.Description = businessSizeItemView.Description;
				
				businessSize.RowVersion = businessSizeItemView.RowVersion;
				return businessSize;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<BusinessSize> ToBusinessSize(this IEnumerable<BusinessSizeItemView> businessSizeItemViews)
		{
			try
			{
				List<BusinessSize> businessSizes = new List<BusinessSize>();
				foreach (BusinessSizeItemView item in businessSizeItemViews)
				{
					businessSizes.Add(item.ToBusinessSize());
				}
				return businessSizes;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BusinessSizeItemView ToBusinessSizeItemView(this BusinessSize businessSize)
		{
			try
			{
				BusinessSizeItemView businessSizeItemView = new BusinessSizeItemView();
				businessSizeItemView.CompanyGUID = businessSize.CompanyGUID.GuidNullToString();
				businessSizeItemView.CreatedBy = businessSize.CreatedBy;
				businessSizeItemView.CreatedDateTime = businessSize.CreatedDateTime.DateTimeToString();
				businessSizeItemView.ModifiedBy = businessSize.ModifiedBy;
				businessSizeItemView.ModifiedDateTime = businessSize.ModifiedDateTime.DateTimeToString();
				businessSizeItemView.Owner = businessSize.Owner;
				businessSizeItemView.OwnerBusinessUnitGUID = businessSize.OwnerBusinessUnitGUID.GuidNullToString();
				businessSizeItemView.BusinessSizeGUID = businessSize.BusinessSizeGUID.GuidNullToString();
				businessSizeItemView.BusinessSizeId = businessSize.BusinessSizeId;
				businessSizeItemView.Description = businessSize.Description;
				
				businessSizeItemView.RowVersion = businessSize.RowVersion;
				return businessSizeItemView.GetBusinessSizeItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion BusinessSizeItemView
		#region ToDropDown
		public static SelectItem<BusinessSizeItemView> ToDropDownItem(this BusinessSizeItemView businessSizeView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<BusinessSizeItemView> selectItem = new SelectItem<BusinessSizeItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(businessSizeView.BusinessSizeId, businessSizeView.Description);
				selectItem.Value = businessSizeView.BusinessSizeGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: businessSizeView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<BusinessSizeItemView>> ToDropDownItem(this IEnumerable<BusinessSizeItemView> businessSizeItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<BusinessSizeItemView>> selectItems = new List<SelectItem<BusinessSizeItemView>>();
				foreach (BusinessSizeItemView item in businessSizeItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

