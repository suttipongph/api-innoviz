using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class AddressCountryHandler
	{
		#region AddressCountryListView
		public static List<AddressCountryListView> GetAddressCountryListViewValidation(this List<AddressCountryListView> addressCountryListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return addressCountryListViews;
			}
			var result = new List<AddressCountryListView>();
			try
			{
				foreach (AddressCountryListView item in addressCountryListViews)
				{
					result.Add(item.GetAddressCountryListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AddressCountryListView GetAddressCountryListViewValidation(this AddressCountryListView addressCountryListView)
		{
			try
			{
				addressCountryListView.RowAuthorize = addressCountryListView.GetListRowAuthorize();
				return addressCountryListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this AddressCountryListView addressCountryListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion AddressCountryListView
		#region AddressCountryItemView
		public static AddressCountryItemView GetAddressCountryItemViewValidation(this AddressCountryItemView addressCountryItemView)
		{
			try
			{
				addressCountryItemView.RowAuthorize = addressCountryItemView.GetItemRowAuthorize();
				return addressCountryItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this AddressCountryItemView addressCountryItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static AddressCountry ToAddressCountry(this AddressCountryItemView addressCountryItemView)
		{
			try
			{
				AddressCountry addressCountry = new AddressCountry();
				addressCountry.CompanyGUID = addressCountryItemView.CompanyGUID.StringToGuid();
				addressCountry.CreatedBy = addressCountryItemView.CreatedBy;
				addressCountry.CreatedDateTime = addressCountryItemView.CreatedDateTime.StringToSystemDateTime();
				addressCountry.ModifiedBy = addressCountryItemView.ModifiedBy;
				addressCountry.ModifiedDateTime = addressCountryItemView.ModifiedDateTime.StringToSystemDateTime();
				addressCountry.Owner = addressCountryItemView.Owner;
				addressCountry.OwnerBusinessUnitGUID = addressCountryItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				addressCountry.AddressCountryGUID = addressCountryItemView.AddressCountryGUID.StringToGuid();
				addressCountry.CountryId = addressCountryItemView.CountryId;
				addressCountry.Name = addressCountryItemView.Name;
				
				addressCountry.RowVersion = addressCountryItemView.RowVersion;
				return addressCountry;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<AddressCountry> ToAddressCountry(this IEnumerable<AddressCountryItemView> addressCountryItemViews)
		{
			try
			{
				List<AddressCountry> addressCountrys = new List<AddressCountry>();
				foreach (AddressCountryItemView item in addressCountryItemViews)
				{
					addressCountrys.Add(item.ToAddressCountry());
				}
				return addressCountrys;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AddressCountryItemView ToAddressCountryItemView(this AddressCountry addressCountry)
		{
			try
			{
				AddressCountryItemView addressCountryItemView = new AddressCountryItemView();
				addressCountryItemView.CompanyGUID = addressCountry.CompanyGUID.GuidNullToString();
				addressCountryItemView.CreatedBy = addressCountry.CreatedBy;
				addressCountryItemView.CreatedDateTime = addressCountry.CreatedDateTime.DateTimeToString();
				addressCountryItemView.ModifiedBy = addressCountry.ModifiedBy;
				addressCountryItemView.ModifiedDateTime = addressCountry.ModifiedDateTime.DateTimeToString();
				addressCountryItemView.Owner = addressCountry.Owner;
				addressCountryItemView.OwnerBusinessUnitGUID = addressCountry.OwnerBusinessUnitGUID.GuidNullToString();
				addressCountryItemView.AddressCountryGUID = addressCountry.AddressCountryGUID.GuidNullToString();
				addressCountryItemView.CountryId = addressCountry.CountryId;
				addressCountryItemView.Name = addressCountry.Name;
				
				addressCountryItemView.RowVersion = addressCountry.RowVersion;
				return addressCountryItemView.GetAddressCountryItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion AddressCountryItemView
		#region ToDropDown
		public static SelectItem<AddressCountryItemView> ToDropDownItem(this AddressCountryItemView addressCountryView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<AddressCountryItemView> selectItem = new SelectItem<AddressCountryItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(addressCountryView.CountryId, addressCountryView.Name);
				selectItem.Value = addressCountryView.AddressCountryGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: addressCountryView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<AddressCountryItemView>> ToDropDownItem(this IEnumerable<AddressCountryItemView> addressCountryItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<AddressCountryItemView>> selectItems = new List<SelectItem<AddressCountryItemView>>();
				foreach (AddressCountryItemView item in addressCountryItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

