using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class RetentionConditionSetupHandler
	{
		#region RetentionConditionSetupListView
		public static List<RetentionConditionSetupListView> GetRetentionConditionSetupListViewValidation(this List<RetentionConditionSetupListView> retentionConditionSetupListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return retentionConditionSetupListViews;
			}
			var result = new List<RetentionConditionSetupListView>();
			try
			{
				foreach (RetentionConditionSetupListView item in retentionConditionSetupListViews)
				{
					result.Add(item.GetRetentionConditionSetupListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static RetentionConditionSetupListView GetRetentionConditionSetupListViewValidation(this RetentionConditionSetupListView retentionConditionSetupListView)
		{
			try
			{
				retentionConditionSetupListView.RowAuthorize = retentionConditionSetupListView.GetListRowAuthorize();
				return retentionConditionSetupListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this RetentionConditionSetupListView retentionConditionSetupListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion RetentionConditionSetupListView
		#region RetentionConditionSetupItemView
		public static RetentionConditionSetupItemView GetRetentionConditionSetupItemViewValidation(this RetentionConditionSetupItemView retentionConditionSetupItemView)
		{
			try
			{
				retentionConditionSetupItemView.RowAuthorize = retentionConditionSetupItemView.GetItemRowAuthorize();
				return retentionConditionSetupItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this RetentionConditionSetupItemView retentionConditionSetupItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static RetentionConditionSetup ToRetentionConditionSetup(this RetentionConditionSetupItemView retentionConditionSetupItemView)
		{
			try
			{
				RetentionConditionSetup retentionConditionSetup = new RetentionConditionSetup();
				retentionConditionSetup.CompanyGUID = retentionConditionSetupItemView.CompanyGUID.StringToGuid();
				retentionConditionSetup.CreatedBy = retentionConditionSetupItemView.CreatedBy;
				retentionConditionSetup.CreatedDateTime = retentionConditionSetupItemView.CreatedDateTime.StringToSystemDateTime();
				retentionConditionSetup.ModifiedBy = retentionConditionSetupItemView.ModifiedBy;
				retentionConditionSetup.ModifiedDateTime = retentionConditionSetupItemView.ModifiedDateTime.StringToSystemDateTime();
				retentionConditionSetup.Owner = retentionConditionSetupItemView.Owner;
				retentionConditionSetup.OwnerBusinessUnitGUID = retentionConditionSetupItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				retentionConditionSetup.RetentionConditionSetupGUID = retentionConditionSetupItemView.RetentionConditionSetupGUID.StringToGuid();
				retentionConditionSetup.ProductType = retentionConditionSetupItemView.ProductType;
				retentionConditionSetup.RetentionCalculateBase = retentionConditionSetupItemView.RetentionCalculateBase;
				retentionConditionSetup.RetentionDeductionMethod = retentionConditionSetupItemView.RetentionDeductionMethod;
				
				retentionConditionSetup.RowVersion = retentionConditionSetupItemView.RowVersion;
				return retentionConditionSetup;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<RetentionConditionSetup> ToRetentionConditionSetup(this IEnumerable<RetentionConditionSetupItemView> retentionConditionSetupItemViews)
		{
			try
			{
				List<RetentionConditionSetup> retentionConditionSetups = new List<RetentionConditionSetup>();
				foreach (RetentionConditionSetupItemView item in retentionConditionSetupItemViews)
				{
					retentionConditionSetups.Add(item.ToRetentionConditionSetup());
				}
				return retentionConditionSetups;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static RetentionConditionSetupItemView ToRetentionConditionSetupItemView(this RetentionConditionSetup retentionConditionSetup)
		{
			try
			{
				RetentionConditionSetupItemView retentionConditionSetupItemView = new RetentionConditionSetupItemView();
				retentionConditionSetupItemView.CompanyGUID = retentionConditionSetup.CompanyGUID.GuidNullToString();
				retentionConditionSetupItemView.CreatedBy = retentionConditionSetup.CreatedBy;
				retentionConditionSetupItemView.CreatedDateTime = retentionConditionSetup.CreatedDateTime.DateTimeToString();
				retentionConditionSetupItemView.ModifiedBy = retentionConditionSetup.ModifiedBy;
				retentionConditionSetupItemView.ModifiedDateTime = retentionConditionSetup.ModifiedDateTime.DateTimeToString();
				retentionConditionSetupItemView.Owner = retentionConditionSetup.Owner;
				retentionConditionSetupItemView.OwnerBusinessUnitGUID = retentionConditionSetup.OwnerBusinessUnitGUID.GuidNullToString();
				retentionConditionSetupItemView.RetentionConditionSetupGUID = retentionConditionSetup.RetentionConditionSetupGUID.GuidNullToString();
				retentionConditionSetupItemView.ProductType = retentionConditionSetup.ProductType;
				retentionConditionSetupItemView.RetentionCalculateBase = retentionConditionSetup.RetentionCalculateBase;
				retentionConditionSetupItemView.RetentionDeductionMethod = retentionConditionSetup.RetentionDeductionMethod;
				
				retentionConditionSetupItemView.RowVersion = retentionConditionSetup.RowVersion;
				return retentionConditionSetupItemView.GetRetentionConditionSetupItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion RetentionConditionSetupItemView
		#region ToDropDown
		public static SelectItem<RetentionConditionSetupItemView> ToDropDownItem(this RetentionConditionSetupItemView retentionConditionSetupView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<RetentionConditionSetupItemView> selectItem = new SelectItem<RetentionConditionSetupItemView>();
				selectItem.Label = null;
				selectItem.Value = retentionConditionSetupView.RetentionConditionSetupGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: retentionConditionSetupView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<RetentionConditionSetupItemView>> ToDropDownItem(this IEnumerable<RetentionConditionSetupItemView> retentionConditionSetupItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<RetentionConditionSetupItemView>> selectItems = new List<SelectItem<RetentionConditionSetupItemView>>();
				foreach (RetentionConditionSetupItemView item in retentionConditionSetupItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

