using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class BuyerCreditLimitByProductHandler
	{
		#region BuyerCreditLimitByProductListView
		public static List<BuyerCreditLimitByProductListView> GetBuyerCreditLimitByProductListViewValidation(this List<BuyerCreditLimitByProductListView> buyerCreditLimitByProductListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return buyerCreditLimitByProductListViews;
			}
			var result = new List<BuyerCreditLimitByProductListView>();
			try
			{
				foreach (BuyerCreditLimitByProductListView item in buyerCreditLimitByProductListViews)
				{
					result.Add(item.GetBuyerCreditLimitByProductListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BuyerCreditLimitByProductListView GetBuyerCreditLimitByProductListViewValidation(this BuyerCreditLimitByProductListView buyerCreditLimitByProductListView)
		{
			try
			{
				buyerCreditLimitByProductListView.RowAuthorize = buyerCreditLimitByProductListView.GetListRowAuthorize();
				return buyerCreditLimitByProductListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this BuyerCreditLimitByProductListView buyerCreditLimitByProductListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion BuyerCreditLimitByProductListView
		#region BuyerCreditLimitByProductItemView
		public static BuyerCreditLimitByProductItemView GetBuyerCreditLimitByProductItemViewValidation(this BuyerCreditLimitByProductItemView buyerCreditLimitByProductItemView)
		{
			try
			{
				buyerCreditLimitByProductItemView.RowAuthorize = buyerCreditLimitByProductItemView.GetItemRowAuthorize();
				return buyerCreditLimitByProductItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this BuyerCreditLimitByProductItemView buyerCreditLimitByProductItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static BuyerCreditLimitByProduct ToBuyerCreditLimitByProduct(this BuyerCreditLimitByProductItemView buyerCreditLimitByProductItemView)
		{
			try
			{
				BuyerCreditLimitByProduct buyerCreditLimitByProduct = new BuyerCreditLimitByProduct();
				buyerCreditLimitByProduct.CompanyGUID = buyerCreditLimitByProductItemView.CompanyGUID.StringToGuid();
				buyerCreditLimitByProduct.CreatedBy = buyerCreditLimitByProductItemView.CreatedBy;
				buyerCreditLimitByProduct.CreatedDateTime = buyerCreditLimitByProductItemView.CreatedDateTime.StringToSystemDateTime();
				buyerCreditLimitByProduct.ModifiedBy = buyerCreditLimitByProductItemView.ModifiedBy;
				buyerCreditLimitByProduct.ModifiedDateTime = buyerCreditLimitByProductItemView.ModifiedDateTime.StringToSystemDateTime();
				buyerCreditLimitByProduct.Owner = buyerCreditLimitByProductItemView.Owner;
				buyerCreditLimitByProduct.OwnerBusinessUnitGUID = buyerCreditLimitByProductItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				buyerCreditLimitByProduct.BuyerCreditLimitByProductGUID = buyerCreditLimitByProductItemView.BuyerCreditLimitByProductGUID.StringToGuid();
				buyerCreditLimitByProduct.BuyerTableGUID = buyerCreditLimitByProductItemView.BuyerTableGUID.StringToGuidNull();
				buyerCreditLimitByProduct.CreditLimit = buyerCreditLimitByProductItemView.CreditLimit;
				buyerCreditLimitByProduct.ProductType = buyerCreditLimitByProductItemView.ProductType;
				
				buyerCreditLimitByProduct.RowVersion = buyerCreditLimitByProductItemView.RowVersion;
				return buyerCreditLimitByProduct;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<BuyerCreditLimitByProduct> ToBuyerCreditLimitByProduct(this IEnumerable<BuyerCreditLimitByProductItemView> buyerCreditLimitByProductItemViews)
		{
			try
			{
				List<BuyerCreditLimitByProduct> buyerCreditLimitByProducts = new List<BuyerCreditLimitByProduct>();
				foreach (BuyerCreditLimitByProductItemView item in buyerCreditLimitByProductItemViews)
				{
					buyerCreditLimitByProducts.Add(item.ToBuyerCreditLimitByProduct());
				}
				return buyerCreditLimitByProducts;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BuyerCreditLimitByProductItemView ToBuyerCreditLimitByProductItemView(this BuyerCreditLimitByProduct buyerCreditLimitByProduct)
		{
			try
			{
				BuyerCreditLimitByProductItemView buyerCreditLimitByProductItemView = new BuyerCreditLimitByProductItemView();
				buyerCreditLimitByProductItemView.CompanyGUID = buyerCreditLimitByProduct.CompanyGUID.GuidNullToString();
				buyerCreditLimitByProductItemView.CreatedBy = buyerCreditLimitByProduct.CreatedBy;
				buyerCreditLimitByProductItemView.CreatedDateTime = buyerCreditLimitByProduct.CreatedDateTime.DateTimeToString();
				buyerCreditLimitByProductItemView.ModifiedBy = buyerCreditLimitByProduct.ModifiedBy;
				buyerCreditLimitByProductItemView.ModifiedDateTime = buyerCreditLimitByProduct.ModifiedDateTime.DateTimeToString();
				buyerCreditLimitByProductItemView.Owner = buyerCreditLimitByProduct.Owner;
				buyerCreditLimitByProductItemView.OwnerBusinessUnitGUID = buyerCreditLimitByProduct.OwnerBusinessUnitGUID.GuidNullToString();
				buyerCreditLimitByProductItemView.BuyerCreditLimitByProductGUID = buyerCreditLimitByProduct.BuyerCreditLimitByProductGUID.GuidNullToString();
				buyerCreditLimitByProductItemView.BuyerTableGUID = buyerCreditLimitByProduct.BuyerTableGUID.GuidNullToString();
				buyerCreditLimitByProductItemView.CreditLimit = buyerCreditLimitByProduct.CreditLimit;
				buyerCreditLimitByProductItemView.ProductType = buyerCreditLimitByProduct.ProductType;
				
				buyerCreditLimitByProductItemView.RowVersion = buyerCreditLimitByProduct.RowVersion;
				return buyerCreditLimitByProductItemView.GetBuyerCreditLimitByProductItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion BuyerCreditLimitByProductItemView
		#region ToDropDown
		public static SelectItem<BuyerCreditLimitByProductItemView> ToDropDownItem(this BuyerCreditLimitByProductItemView buyerCreditLimitByProductView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<BuyerCreditLimitByProductItemView> selectItem = new SelectItem<BuyerCreditLimitByProductItemView>();
				selectItem.Label = null;
				selectItem.Value = buyerCreditLimitByProductView.BuyerCreditLimitByProductGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: buyerCreditLimitByProductView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<BuyerCreditLimitByProductItemView>> ToDropDownItem(this IEnumerable<BuyerCreditLimitByProductItemView> buyerCreditLimitByProductItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<BuyerCreditLimitByProductItemView>> selectItems = new List<SelectItem<BuyerCreditLimitByProductItemView>>();
				foreach (BuyerCreditLimitByProductItemView item in buyerCreditLimitByProductItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

