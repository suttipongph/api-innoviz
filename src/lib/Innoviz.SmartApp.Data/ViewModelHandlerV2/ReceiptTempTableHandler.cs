using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class ReceiptTempTableHandler
	{
		#region ReceiptTempTableListView
		public static List<ReceiptTempTableListView> GetReceiptTempTableListViewValidation(this List<ReceiptTempTableListView> receiptTempTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return receiptTempTableListViews;
			}
			var result = new List<ReceiptTempTableListView>();
			try
			{
				foreach (ReceiptTempTableListView item in receiptTempTableListViews)
				{
					result.Add(item.GetReceiptTempTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ReceiptTempTableListView GetReceiptTempTableListViewValidation(this ReceiptTempTableListView receiptTempTableListView)
		{
			try
			{
				receiptTempTableListView.RowAuthorize = receiptTempTableListView.GetListRowAuthorize();
				return receiptTempTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this ReceiptTempTableListView receiptTempTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion ReceiptTempTableListView
		#region ReceiptTempTableItemView
		public static ReceiptTempTableItemView GetReceiptTempTableItemViewValidation(this ReceiptTempTableItemView receiptTempTableItemView)
		{
			try
			{
				receiptTempTableItemView.RowAuthorize = receiptTempTableItemView.GetItemRowAuthorize();
				return receiptTempTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this ReceiptTempTableItemView receiptTempTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static ReceiptTempTable ToReceiptTempTable(this ReceiptTempTableItemView receiptTempTableItemView)
		{
			try
			{
				ReceiptTempTable receiptTempTable = new ReceiptTempTable();
				receiptTempTable.CompanyGUID = receiptTempTableItemView.CompanyGUID.StringToGuid();
				receiptTempTable.CreatedBy = receiptTempTableItemView.CreatedBy;
				receiptTempTable.CreatedDateTime = receiptTempTableItemView.CreatedDateTime.StringToSystemDateTime();
				receiptTempTable.ModifiedBy = receiptTempTableItemView.ModifiedBy;
				receiptTempTable.ModifiedDateTime = receiptTempTableItemView.ModifiedDateTime.StringToSystemDateTime();
				receiptTempTable.Owner = receiptTempTableItemView.Owner;
				receiptTempTable.OwnerBusinessUnitGUID = receiptTempTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				receiptTempTable.ReceiptTempTableGUID = receiptTempTableItemView.ReceiptTempTableGUID.StringToGuid();
				receiptTempTable.BuyerReceiptTableGUID = receiptTempTableItemView.BuyerReceiptTableGUID.StringToGuidNull();
				receiptTempTable.BuyerTableGUID = receiptTempTableItemView.BuyerTableGUID.StringToGuidNull();
				receiptTempTable.CreditAppTableGUID = receiptTempTableItemView.CreditAppTableGUID.StringToGuidNull();
				receiptTempTable.CurrencyGUID = receiptTempTableItemView.CurrencyGUID.StringToGuidNull();
				receiptTempTable.CustomerTableGUID = receiptTempTableItemView.CustomerTableGUID.StringToGuid();
				receiptTempTable.Dimension1GUID = receiptTempTableItemView.Dimension1GUID.StringToGuidNull();
				receiptTempTable.Dimension2GUID = receiptTempTableItemView.Dimension2GUID.StringToGuidNull();
				receiptTempTable.Dimension3GUID = receiptTempTableItemView.Dimension3GUID.StringToGuidNull();
				receiptTempTable.Dimension4GUID = receiptTempTableItemView.Dimension4GUID.StringToGuidNull();
				receiptTempTable.Dimension5GUID = receiptTempTableItemView.Dimension5GUID.StringToGuidNull();
				receiptTempTable.DocumentReasonGUID = receiptTempTableItemView.DocumentReasonGUID.StringToGuidNull();
				receiptTempTable.DocumentStatusGUID = receiptTempTableItemView.DocumentStatusGUID.StringToGuid();
				receiptTempTable.ExchangeRate = receiptTempTableItemView.ExchangeRate;
				receiptTempTable.MainReceiptTempGUID = receiptTempTableItemView.MainReceiptTempGUID.StringToGuidNull();
				receiptTempTable.ProductType = receiptTempTableItemView.ProductType;
				receiptTempTable.ReceiptAmount = receiptTempTableItemView.ReceiptAmount;
				receiptTempTable.ReceiptDate = receiptTempTableItemView.ReceiptDate.StringToDate();
				receiptTempTable.ReceiptTempId = receiptTempTableItemView.ReceiptTempId;
				receiptTempTable.ReceiptTempRefType = receiptTempTableItemView.ReceiptTempRefType;
				receiptTempTable.ReceivedFrom = receiptTempTableItemView.ReceivedFrom;
				receiptTempTable.RefReceiptTempGUID = receiptTempTableItemView.RefReceiptTempGUID.StringToGuidNull();
				receiptTempTable.Remark = receiptTempTableItemView.Remark;
				receiptTempTable.SettleAmount = receiptTempTableItemView.SettleAmount;
				receiptTempTable.SettleFeeAmount = receiptTempTableItemView.SettleFeeAmount;
				receiptTempTable.SuspenseAmount = receiptTempTableItemView.SuspenseAmount;
				receiptTempTable.SuspenseInvoiceTypeGUID = receiptTempTableItemView.SuspenseInvoiceTypeGUID.StringToGuidNull();
				receiptTempTable.TransDate = receiptTempTableItemView.TransDate.StringToDate();
				
				receiptTempTable.RowVersion = receiptTempTableItemView.RowVersion;
				return receiptTempTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<ReceiptTempTable> ToReceiptTempTable(this IEnumerable<ReceiptTempTableItemView> receiptTempTableItemViews)
		{
			try
			{
				List<ReceiptTempTable> receiptTempTables = new List<ReceiptTempTable>();
				foreach (ReceiptTempTableItemView item in receiptTempTableItemViews)
				{
					receiptTempTables.Add(item.ToReceiptTempTable());
				}
				return receiptTempTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ReceiptTempTableItemView ToReceiptTempTableItemView(this ReceiptTempTable receiptTempTable)
		{
			try
			{
				ReceiptTempTableItemView receiptTempTableItemView = new ReceiptTempTableItemView();
				receiptTempTableItemView.CompanyGUID = receiptTempTable.CompanyGUID.GuidNullToString();
				receiptTempTableItemView.CreatedBy = receiptTempTable.CreatedBy;
				receiptTempTableItemView.CreatedDateTime = receiptTempTable.CreatedDateTime.DateTimeToString();
				receiptTempTableItemView.ModifiedBy = receiptTempTable.ModifiedBy;
				receiptTempTableItemView.ModifiedDateTime = receiptTempTable.ModifiedDateTime.DateTimeToString();
				receiptTempTableItemView.Owner = receiptTempTable.Owner;
				receiptTempTableItemView.OwnerBusinessUnitGUID = receiptTempTable.OwnerBusinessUnitGUID.GuidNullToString();
				receiptTempTableItemView.ReceiptTempTableGUID = receiptTempTable.ReceiptTempTableGUID.GuidNullToString();
				receiptTempTableItemView.BuyerReceiptTableGUID = receiptTempTable.BuyerReceiptTableGUID.GuidNullToString();
				receiptTempTableItemView.BuyerTableGUID = receiptTempTable.BuyerTableGUID.GuidNullToString();
				receiptTempTableItemView.CreditAppTableGUID = receiptTempTable.CreditAppTableGUID.GuidNullToString();
				receiptTempTableItemView.CurrencyGUID = receiptTempTable.CurrencyGUID.GuidNullToString();
				receiptTempTableItemView.CustomerTableGUID = receiptTempTable.CustomerTableGUID.GuidNullToString();
				receiptTempTableItemView.Dimension1GUID = receiptTempTable.Dimension1GUID.GuidNullToString();
				receiptTempTableItemView.Dimension2GUID = receiptTempTable.Dimension2GUID.GuidNullToString();
				receiptTempTableItemView.Dimension3GUID = receiptTempTable.Dimension3GUID.GuidNullToString();
				receiptTempTableItemView.Dimension4GUID = receiptTempTable.Dimension4GUID.GuidNullToString();
				receiptTempTableItemView.Dimension5GUID = receiptTempTable.Dimension5GUID.GuidNullToString();
				receiptTempTableItemView.DocumentReasonGUID = receiptTempTable.DocumentReasonGUID.GuidNullToString();
				receiptTempTableItemView.DocumentStatusGUID = receiptTempTable.DocumentStatusGUID.GuidNullToString();
				receiptTempTableItemView.ExchangeRate = receiptTempTable.ExchangeRate;
				receiptTempTableItemView.MainReceiptTempGUID = receiptTempTable.MainReceiptTempGUID.GuidNullToString();
				receiptTempTableItemView.ProductType = receiptTempTable.ProductType;
				receiptTempTableItemView.ReceiptAmount = receiptTempTable.ReceiptAmount;
				receiptTempTableItemView.ReceiptDate = receiptTempTable.ReceiptDate.DateToString();
				receiptTempTableItemView.ReceiptTempId = receiptTempTable.ReceiptTempId;
				receiptTempTableItemView.ReceiptTempRefType = receiptTempTable.ReceiptTempRefType;
				receiptTempTableItemView.ReceivedFrom = receiptTempTable.ReceivedFrom;
				receiptTempTableItemView.RefReceiptTempGUID = receiptTempTable.RefReceiptTempGUID.GuidNullToString();
				receiptTempTableItemView.Remark = receiptTempTable.Remark;
				receiptTempTableItemView.SettleAmount = receiptTempTable.SettleAmount;
				receiptTempTableItemView.SettleFeeAmount = receiptTempTable.SettleFeeAmount;
				receiptTempTableItemView.SuspenseAmount = receiptTempTable.SuspenseAmount;
				receiptTempTableItemView.SuspenseInvoiceTypeGUID = receiptTempTable.SuspenseInvoiceTypeGUID.GuidNullToString();
				receiptTempTableItemView.TransDate = receiptTempTable.TransDate.DateToString();
				
				receiptTempTableItemView.RowVersion = receiptTempTable.RowVersion;
				return receiptTempTableItemView.GetReceiptTempTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ReceiptTempTableItemView
		#region ToDropDown
		public static SelectItem<ReceiptTempTableItemView> ToDropDownItem(this ReceiptTempTableItemView receiptTempTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<ReceiptTempTableItemView> selectItem = new SelectItem<ReceiptTempTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(receiptTempTableView.ReceiptTempId, receiptTempTableView.ReceiptDate.ToString());
				selectItem.Value = receiptTempTableView.ReceiptTempTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: receiptTempTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<ReceiptTempTableItemView>> ToDropDownItem(this IEnumerable<ReceiptTempTableItemView> receiptTempTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<ReceiptTempTableItemView>> selectItems = new List<SelectItem<ReceiptTempTableItemView>>();
				foreach (ReceiptTempTableItemView item in receiptTempTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

