using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class MaritalStatusHandler
	{
		#region MaritalStatusListView
		public static List<MaritalStatusListView> GetMaritalStatusListViewValidation(this List<MaritalStatusListView> maritalStatusListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return maritalStatusListViews;
			}
			var result = new List<MaritalStatusListView>();
			try
			{
				foreach (MaritalStatusListView item in maritalStatusListViews)
				{
					result.Add(item.GetMaritalStatusListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static MaritalStatusListView GetMaritalStatusListViewValidation(this MaritalStatusListView maritalStatusListView)
		{
			try
			{
				maritalStatusListView.RowAuthorize = maritalStatusListView.GetListRowAuthorize();
				return maritalStatusListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this MaritalStatusListView maritalStatusListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion MaritalStatusListView
		#region MaritalStatusItemView
		public static MaritalStatusItemView GetMaritalStatusItemViewValidation(this MaritalStatusItemView maritalStatusItemView)
		{
			try
			{
				maritalStatusItemView.RowAuthorize = maritalStatusItemView.GetItemRowAuthorize();
				return maritalStatusItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this MaritalStatusItemView maritalStatusItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static MaritalStatus ToMaritalStatus(this MaritalStatusItemView maritalStatusItemView)
		{
			try
			{
				MaritalStatus maritalStatus = new MaritalStatus();
				maritalStatus.CompanyGUID = maritalStatusItemView.CompanyGUID.StringToGuid();
				maritalStatus.CreatedBy = maritalStatusItemView.CreatedBy;
				maritalStatus.CreatedDateTime = maritalStatusItemView.CreatedDateTime.StringToSystemDateTime();
				maritalStatus.ModifiedBy = maritalStatusItemView.ModifiedBy;
				maritalStatus.ModifiedDateTime = maritalStatusItemView.ModifiedDateTime.StringToSystemDateTime();
				maritalStatus.Owner = maritalStatusItemView.Owner;
				maritalStatus.OwnerBusinessUnitGUID = maritalStatusItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				maritalStatus.MaritalStatusGUID = maritalStatusItemView.MaritalStatusGUID.StringToGuid();
				maritalStatus.Description = maritalStatusItemView.Description;
				maritalStatus.MaritalStatusId = maritalStatusItemView.MaritalStatusId;
				
				maritalStatus.RowVersion = maritalStatusItemView.RowVersion;
				return maritalStatus;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<MaritalStatus> ToMaritalStatus(this IEnumerable<MaritalStatusItemView> maritalStatusItemViews)
		{
			try
			{
				List<MaritalStatus> maritalStatuss = new List<MaritalStatus>();
				foreach (MaritalStatusItemView item in maritalStatusItemViews)
				{
					maritalStatuss.Add(item.ToMaritalStatus());
				}
				return maritalStatuss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static MaritalStatusItemView ToMaritalStatusItemView(this MaritalStatus maritalStatus)
		{
			try
			{
				MaritalStatusItemView maritalStatusItemView = new MaritalStatusItemView();
				maritalStatusItemView.CompanyGUID = maritalStatus.CompanyGUID.GuidNullToString();
				maritalStatusItemView.CreatedBy = maritalStatus.CreatedBy;
				maritalStatusItemView.CreatedDateTime = maritalStatus.CreatedDateTime.DateTimeToString();
				maritalStatusItemView.ModifiedBy = maritalStatus.ModifiedBy;
				maritalStatusItemView.ModifiedDateTime = maritalStatus.ModifiedDateTime.DateTimeToString();
				maritalStatusItemView.Owner = maritalStatus.Owner;
				maritalStatusItemView.OwnerBusinessUnitGUID = maritalStatus.OwnerBusinessUnitGUID.GuidNullToString();
				maritalStatusItemView.MaritalStatusGUID = maritalStatus.MaritalStatusGUID.GuidNullToString();
				maritalStatusItemView.Description = maritalStatus.Description;
				maritalStatusItemView.MaritalStatusId = maritalStatus.MaritalStatusId;
				
				maritalStatusItemView.RowVersion = maritalStatus.RowVersion;
				return maritalStatusItemView.GetMaritalStatusItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion MaritalStatusItemView
		#region ToDropDown
		public static SelectItem<MaritalStatusItemView> ToDropDownItem(this MaritalStatusItemView maritalStatusView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<MaritalStatusItemView> selectItem = new SelectItem<MaritalStatusItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(maritalStatusView.MaritalStatusId, maritalStatusView.Description);
				selectItem.Value = maritalStatusView.MaritalStatusGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: maritalStatusView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<MaritalStatusItemView>> ToDropDownItem(this IEnumerable<MaritalStatusItemView> maritalStatusItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<MaritalStatusItemView>> selectItems = new List<SelectItem<MaritalStatusItemView>>();
				foreach (MaritalStatusItemView item in maritalStatusItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

