using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class InvoiceRevenueTypeHandler
	{
		#region InvoiceRevenueTypeListView
		public static List<InvoiceRevenueTypeListView> GetInvoiceRevenueTypeListViewValidation(this List<InvoiceRevenueTypeListView> invoiceRevenueTypeListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return invoiceRevenueTypeListViews;
			}
			var result = new List<InvoiceRevenueTypeListView>();
			try
			{
				foreach (InvoiceRevenueTypeListView item in invoiceRevenueTypeListViews)
				{
					result.Add(item.GetInvoiceRevenueTypeListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static InvoiceRevenueTypeListView GetInvoiceRevenueTypeListViewValidation(this InvoiceRevenueTypeListView invoiceRevenueTypeListView)
		{
			try
			{
				invoiceRevenueTypeListView.RowAuthorize = invoiceRevenueTypeListView.GetListRowAuthorize();
				return invoiceRevenueTypeListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this InvoiceRevenueTypeListView invoiceRevenueTypeListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion InvoiceRevenueTypeListView
		#region InvoiceRevenueTypeItemView
		public static InvoiceRevenueTypeItemView GetInvoiceRevenueTypeItemViewValidation(this InvoiceRevenueTypeItemView invoiceRevenueTypeItemView)
		{
			try
			{
				invoiceRevenueTypeItemView.RowAuthorize = invoiceRevenueTypeItemView.GetItemRowAuthorize();
				return invoiceRevenueTypeItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this InvoiceRevenueTypeItemView invoiceRevenueTypeItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static InvoiceRevenueType ToInvoiceRevenueType(this InvoiceRevenueTypeItemView invoiceRevenueTypeItemView)
		{
			try
			{
				InvoiceRevenueType invoiceRevenueType = new InvoiceRevenueType();
				invoiceRevenueType.CompanyGUID = invoiceRevenueTypeItemView.CompanyGUID.StringToGuid();
				invoiceRevenueType.CreatedBy = invoiceRevenueTypeItemView.CreatedBy;
				invoiceRevenueType.CreatedDateTime = invoiceRevenueTypeItemView.CreatedDateTime.StringToSystemDateTime();
				invoiceRevenueType.ModifiedBy = invoiceRevenueTypeItemView.ModifiedBy;
				invoiceRevenueType.ModifiedDateTime = invoiceRevenueTypeItemView.ModifiedDateTime.StringToSystemDateTime();
				invoiceRevenueType.Owner = invoiceRevenueTypeItemView.Owner;
				invoiceRevenueType.OwnerBusinessUnitGUID = invoiceRevenueTypeItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				invoiceRevenueType.InvoiceRevenueTypeGUID = invoiceRevenueTypeItemView.InvoiceRevenueTypeGUID.StringToGuid();
				invoiceRevenueType.AssetFeeType = invoiceRevenueTypeItemView.AssetFeeType;
				invoiceRevenueType.Description = invoiceRevenueTypeItemView.Description;
				invoiceRevenueType.FeeAmount = invoiceRevenueTypeItemView.FeeAmount;
				invoiceRevenueType.FeeInvoiceText = invoiceRevenueTypeItemView.FeeInvoiceText;
				invoiceRevenueType.FeeLedgerAccount = invoiceRevenueTypeItemView.FeeLedgerAccount;
				invoiceRevenueType.FeeTaxAmount = invoiceRevenueTypeItemView.FeeTaxAmount;
				invoiceRevenueType.FeeTaxGUID = invoiceRevenueTypeItemView.FeeTaxGUID.StringToGuidNull();
				invoiceRevenueType.FeeWHTGUID = invoiceRevenueTypeItemView.FeeWHTGUID.StringToGuidNull();
				invoiceRevenueType.IntercompanyTableGUID = invoiceRevenueTypeItemView.IntercompanyTableGUID.StringToGuidNull();
				invoiceRevenueType.ProductType = invoiceRevenueTypeItemView.ProductType;
				invoiceRevenueType.RevenueTypeId = invoiceRevenueTypeItemView.RevenueTypeId;
				invoiceRevenueType.ServiceFeeCategory = invoiceRevenueTypeItemView.ServiceFeeCategory;
				invoiceRevenueType.ServiceFeeRevenueTypeGUID = invoiceRevenueTypeItemView.ServiceFeeRevenueTypeGUID.StringToGuidNull();
				
				invoiceRevenueType.RowVersion = invoiceRevenueTypeItemView.RowVersion;
				return invoiceRevenueType;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<InvoiceRevenueType> ToInvoiceRevenueType(this IEnumerable<InvoiceRevenueTypeItemView> invoiceRevenueTypeItemViews)
		{
			try
			{
				List<InvoiceRevenueType> invoiceRevenueTypes = new List<InvoiceRevenueType>();
				foreach (InvoiceRevenueTypeItemView item in invoiceRevenueTypeItemViews)
				{
					invoiceRevenueTypes.Add(item.ToInvoiceRevenueType());
				}
				return invoiceRevenueTypes;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static InvoiceRevenueTypeItemView ToInvoiceRevenueTypeItemView(this InvoiceRevenueType invoiceRevenueType)
		{
			try
			{
				InvoiceRevenueTypeItemView invoiceRevenueTypeItemView = new InvoiceRevenueTypeItemView();
				invoiceRevenueTypeItemView.CompanyGUID = invoiceRevenueType.CompanyGUID.GuidNullToString();
				invoiceRevenueTypeItemView.CreatedBy = invoiceRevenueType.CreatedBy;
				invoiceRevenueTypeItemView.CreatedDateTime = invoiceRevenueType.CreatedDateTime.DateTimeToString();
				invoiceRevenueTypeItemView.ModifiedBy = invoiceRevenueType.ModifiedBy;
				invoiceRevenueTypeItemView.ModifiedDateTime = invoiceRevenueType.ModifiedDateTime.DateTimeToString();
				invoiceRevenueTypeItemView.Owner = invoiceRevenueType.Owner;
				invoiceRevenueTypeItemView.OwnerBusinessUnitGUID = invoiceRevenueType.OwnerBusinessUnitGUID.GuidNullToString();
				invoiceRevenueTypeItemView.InvoiceRevenueTypeGUID = invoiceRevenueType.InvoiceRevenueTypeGUID.GuidNullToString();
				invoiceRevenueTypeItemView.AssetFeeType = invoiceRevenueType.AssetFeeType;
				invoiceRevenueTypeItemView.Description = invoiceRevenueType.Description;
				invoiceRevenueTypeItemView.FeeAmount = invoiceRevenueType.FeeAmount;
				invoiceRevenueTypeItemView.FeeInvoiceText = invoiceRevenueType.FeeInvoiceText;
				invoiceRevenueTypeItemView.FeeLedgerAccount = invoiceRevenueType.FeeLedgerAccount;
				invoiceRevenueTypeItemView.FeeTaxAmount = invoiceRevenueType.FeeTaxAmount;
				invoiceRevenueTypeItemView.FeeTaxGUID = invoiceRevenueType.FeeTaxGUID.GuidNullToString();
				invoiceRevenueTypeItemView.FeeWHTGUID = invoiceRevenueType.FeeWHTGUID.GuidNullToString();
				invoiceRevenueTypeItemView.IntercompanyTableGUID = invoiceRevenueType.IntercompanyTableGUID.GuidNullToString();
				invoiceRevenueTypeItemView.ProductType = invoiceRevenueType.ProductType;
				invoiceRevenueTypeItemView.RevenueTypeId = invoiceRevenueType.RevenueTypeId;
				invoiceRevenueTypeItemView.ServiceFeeCategory = invoiceRevenueType.ServiceFeeCategory;
				invoiceRevenueTypeItemView.ServiceFeeRevenueTypeGUID = invoiceRevenueType.ServiceFeeRevenueTypeGUID.GuidNullToString();
				
				invoiceRevenueTypeItemView.RowVersion = invoiceRevenueType.RowVersion;
				return invoiceRevenueTypeItemView.GetInvoiceRevenueTypeItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion InvoiceRevenueTypeItemView
		#region ToDropDown
		public static SelectItem<InvoiceRevenueTypeItemView> ToDropDownItem(this InvoiceRevenueTypeItemView invoiceRevenueTypeView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<InvoiceRevenueTypeItemView> selectItem = new SelectItem<InvoiceRevenueTypeItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(invoiceRevenueTypeView.RevenueTypeId, invoiceRevenueTypeView.Description);
				selectItem.Value = invoiceRevenueTypeView.InvoiceRevenueTypeGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: invoiceRevenueTypeView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<InvoiceRevenueTypeItemView>> ToDropDownItem(this IEnumerable<InvoiceRevenueTypeItemView> invoiceRevenueTypeItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<InvoiceRevenueTypeItemView>> selectItems = new List<SelectItem<InvoiceRevenueTypeItemView>>();
				foreach (InvoiceRevenueTypeItemView item in invoiceRevenueTypeItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

