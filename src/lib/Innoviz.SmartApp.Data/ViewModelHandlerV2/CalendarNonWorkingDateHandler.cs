using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CalendarNonWorkingDateHandler
	{
		#region CalendarNonWorkingDateListView
		public static List<CalendarNonWorkingDateListView> GetCalendarNonWorkingDateListViewValidation(this List<CalendarNonWorkingDateListView> calendarNonWorkingDateListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return calendarNonWorkingDateListViews;
			}
			var result = new List<CalendarNonWorkingDateListView>();
			try
			{
				foreach (CalendarNonWorkingDateListView item in calendarNonWorkingDateListViews)
				{
					result.Add(item.GetCalendarNonWorkingDateListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CalendarNonWorkingDateListView GetCalendarNonWorkingDateListViewValidation(this CalendarNonWorkingDateListView calendarNonWorkingDateListView)
		{
			try
			{
				calendarNonWorkingDateListView.RowAuthorize = calendarNonWorkingDateListView.GetListRowAuthorize();
				return calendarNonWorkingDateListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CalendarNonWorkingDateListView calendarNonWorkingDateListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CalendarNonWorkingDateListView
		#region CalendarNonWorkingDateItemView
		public static CalendarNonWorkingDateItemView GetCalendarNonWorkingDateItemViewValidation(this CalendarNonWorkingDateItemView calendarNonWorkingDateItemView)
		{
			try
			{
				calendarNonWorkingDateItemView.RowAuthorize = calendarNonWorkingDateItemView.GetItemRowAuthorize();
				return calendarNonWorkingDateItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CalendarNonWorkingDateItemView calendarNonWorkingDateItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CalendarNonWorkingDate ToCalendarNonWorkingDate(this CalendarNonWorkingDateItemView calendarNonWorkingDateItemView)
		{
			try
			{
				CalendarNonWorkingDate calendarNonWorkingDate = new CalendarNonWorkingDate();
				calendarNonWorkingDate.CompanyGUID = calendarNonWorkingDateItemView.CompanyGUID.StringToGuid();
				calendarNonWorkingDate.CreatedBy = calendarNonWorkingDateItemView.CreatedBy;
				calendarNonWorkingDate.CreatedDateTime = calendarNonWorkingDateItemView.CreatedDateTime.StringToSystemDateTime();
				calendarNonWorkingDate.ModifiedBy = calendarNonWorkingDateItemView.ModifiedBy;
				calendarNonWorkingDate.ModifiedDateTime = calendarNonWorkingDateItemView.ModifiedDateTime.StringToSystemDateTime();
				calendarNonWorkingDate.Owner = calendarNonWorkingDateItemView.Owner;
				calendarNonWorkingDate.OwnerBusinessUnitGUID = calendarNonWorkingDateItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				calendarNonWorkingDate.CalendarNonWorkingDateGUID = calendarNonWorkingDateItemView.CalendarNonWorkingDateGUID.StringToGuid();
				calendarNonWorkingDate.CalendarDate = calendarNonWorkingDateItemView.CalendarDate.StringToDate();
				calendarNonWorkingDate.CalendarGroupGUID = calendarNonWorkingDateItemView.CalendarGroupGUID.StringToGuid();
				calendarNonWorkingDate.Description = calendarNonWorkingDateItemView.Description;
				calendarNonWorkingDate.HolidayType = calendarNonWorkingDateItemView.HolidayType;
				
				calendarNonWorkingDate.RowVersion = calendarNonWorkingDateItemView.RowVersion;
				return calendarNonWorkingDate;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CalendarNonWorkingDate> ToCalendarNonWorkingDate(this IEnumerable<CalendarNonWorkingDateItemView> calendarNonWorkingDateItemViews)
		{
			try
			{
				List<CalendarNonWorkingDate> calendarNonWorkingDates = new List<CalendarNonWorkingDate>();
				foreach (CalendarNonWorkingDateItemView item in calendarNonWorkingDateItemViews)
				{
					calendarNonWorkingDates.Add(item.ToCalendarNonWorkingDate());
				}
				return calendarNonWorkingDates;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CalendarNonWorkingDateItemView ToCalendarNonWorkingDateItemView(this CalendarNonWorkingDate calendarNonWorkingDate)
		{
			try
			{
				CalendarNonWorkingDateItemView calendarNonWorkingDateItemView = new CalendarNonWorkingDateItemView();
				calendarNonWorkingDateItemView.CompanyGUID = calendarNonWorkingDate.CompanyGUID.GuidNullToString();
				calendarNonWorkingDateItemView.CreatedBy = calendarNonWorkingDate.CreatedBy;
				calendarNonWorkingDateItemView.CreatedDateTime = calendarNonWorkingDate.CreatedDateTime.DateTimeToString();
				calendarNonWorkingDateItemView.ModifiedBy = calendarNonWorkingDate.ModifiedBy;
				calendarNonWorkingDateItemView.ModifiedDateTime = calendarNonWorkingDate.ModifiedDateTime.DateTimeToString();
				calendarNonWorkingDateItemView.Owner = calendarNonWorkingDate.Owner;
				calendarNonWorkingDateItemView.OwnerBusinessUnitGUID = calendarNonWorkingDate.OwnerBusinessUnitGUID.GuidNullToString();
				calendarNonWorkingDateItemView.CalendarNonWorkingDateGUID = calendarNonWorkingDate.CalendarNonWorkingDateGUID.GuidNullToString();
				calendarNonWorkingDateItemView.CalendarDate = calendarNonWorkingDate.CalendarDate.DateToString();
				calendarNonWorkingDateItemView.CalendarGroupGUID = calendarNonWorkingDate.CalendarGroupGUID.GuidNullToString();
				calendarNonWorkingDateItemView.Description = calendarNonWorkingDate.Description;
				calendarNonWorkingDateItemView.HolidayType = calendarNonWorkingDate.HolidayType;
				
				calendarNonWorkingDateItemView.RowVersion = calendarNonWorkingDate.RowVersion;
				return calendarNonWorkingDateItemView.GetCalendarNonWorkingDateItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CalendarNonWorkingDateItemView
		#region ToDropDown
		public static SelectItem<CalendarNonWorkingDateItemView> ToDropDownItem(this CalendarNonWorkingDateItemView calendarNonWorkingDateView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CalendarNonWorkingDateItemView> selectItem = new SelectItem<CalendarNonWorkingDateItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(calendarNonWorkingDateView.CalendarDate.ToString(), calendarNonWorkingDateView.Description);
				selectItem.Value = calendarNonWorkingDateView.CalendarNonWorkingDateGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: calendarNonWorkingDateView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CalendarNonWorkingDateItemView>> ToDropDownItem(this IEnumerable<CalendarNonWorkingDateItemView> calendarNonWorkingDateItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CalendarNonWorkingDateItemView>> selectItems = new List<SelectItem<CalendarNonWorkingDateItemView>>();
				foreach (CalendarNonWorkingDateItemView item in calendarNonWorkingDateItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

