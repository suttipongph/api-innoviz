using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CreditAppRequestLineHandler
	{
		#region CreditAppRequestLineListView
		public static List<CreditAppRequestLineListView> GetCreditAppRequestLineListViewValidation(this List<CreditAppRequestLineListView> creditAppRequestLineListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return creditAppRequestLineListViews;
			}
			var result = new List<CreditAppRequestLineListView>();
			try
			{
				foreach (CreditAppRequestLineListView item in creditAppRequestLineListViews)
				{
					result.Add(item.GetCreditAppRequestLineListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CreditAppRequestLineListView GetCreditAppRequestLineListViewValidation(this CreditAppRequestLineListView creditAppRequestLineListView)
		{
			try
			{
				creditAppRequestLineListView.RowAuthorize = creditAppRequestLineListView.GetListRowAuthorize();
				return creditAppRequestLineListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CreditAppRequestLineListView creditAppRequestLineListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CreditAppRequestLineListView
		#region CreditAppRequestLineItemView
		public static CreditAppRequestLineItemView GetCreditAppRequestLineItemViewValidation(this CreditAppRequestLineItemView creditAppRequestLineItemView)
		{
			try
			{
				creditAppRequestLineItemView.RowAuthorize = creditAppRequestLineItemView.GetItemRowAuthorize();
				return creditAppRequestLineItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CreditAppRequestLineItemView creditAppRequestLineItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CreditAppRequestLine ToCreditAppRequestLine(this CreditAppRequestLineItemView creditAppRequestLineItemView)
		{
			try
			{
				CreditAppRequestLine creditAppRequestLine = new CreditAppRequestLine();
				creditAppRequestLine.CompanyGUID = creditAppRequestLineItemView.CompanyGUID.StringToGuid();
				creditAppRequestLine.CreatedBy = creditAppRequestLineItemView.CreatedBy;
				creditAppRequestLine.CreatedDateTime = creditAppRequestLineItemView.CreatedDateTime.StringToSystemDateTime();
				creditAppRequestLine.ModifiedBy = creditAppRequestLineItemView.ModifiedBy;
				creditAppRequestLine.ModifiedDateTime = creditAppRequestLineItemView.ModifiedDateTime.StringToSystemDateTime();
				creditAppRequestLine.Owner = creditAppRequestLineItemView.Owner;
				creditAppRequestLine.OwnerBusinessUnitGUID = creditAppRequestLineItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				creditAppRequestLine.CreditAppRequestLineGUID = creditAppRequestLineItemView.CreditAppRequestLineGUID.StringToGuid();
				creditAppRequestLine.AcceptanceDocument = creditAppRequestLineItemView.AcceptanceDocument;
				creditAppRequestLine.AcceptanceDocumentDescription = creditAppRequestLineItemView.AcceptanceDocumentDescription;
				creditAppRequestLine.ApprovalDecision = creditAppRequestLineItemView.ApprovalDecision;
				creditAppRequestLine.ApprovedCreditLimitLineRequest = creditAppRequestLineItemView.ApprovedCreditLimitLineRequest;
				creditAppRequestLine.ApproverComment = creditAppRequestLineItemView.ApproverComment;
				creditAppRequestLine.AssignmentAgreementTableGUID = creditAppRequestLineItemView.AssignmentAgreementTableGUID.StringToGuidNull();
				creditAppRequestLine.AssignmentMethodGUID = creditAppRequestLineItemView.AssignmentMethodGUID.StringToGuidNull();
				creditAppRequestLine.AssignmentMethodRemark = creditAppRequestLineItemView.AssignmentMethodRemark;
				creditAppRequestLine.BillingAddressGUID = creditAppRequestLineItemView.BillingAddressGUID.StringToGuidNull();
				creditAppRequestLine.BillingContactPersonGUID = creditAppRequestLineItemView.BillingContactPersonGUID.StringToGuidNull();
				creditAppRequestLine.BillingDay = creditAppRequestLineItemView.BillingDay;
				creditAppRequestLine.BillingDescription = creditAppRequestLineItemView.BillingDescription;
				creditAppRequestLine.BillingRemark = creditAppRequestLineItemView.BillingRemark;
				creditAppRequestLine.BillingResponsibleByGUID = creditAppRequestLineItemView.BillingResponsibleByGUID.StringToGuidNull();
				creditAppRequestLine.BlacklistStatusGUID = creditAppRequestLineItemView.BlacklistStatusGUID.StringToGuidNull();
				creditAppRequestLine.BuyerCreditLimit = creditAppRequestLineItemView.BuyerCreditLimit;
				creditAppRequestLine.BuyerCreditTermGUID = creditAppRequestLineItemView.BuyerCreditTermGUID.StringToGuidNull();
				creditAppRequestLine.BuyerProduct = creditAppRequestLineItemView.BuyerProduct;
				creditAppRequestLine.BuyerTableGUID = creditAppRequestLineItemView.BuyerTableGUID.StringToGuid();
				creditAppRequestLine.CreditAppRequestTableGUID = creditAppRequestLineItemView.CreditAppRequestTableGUID.StringToGuid();
				creditAppRequestLine.CreditComment = creditAppRequestLineItemView.CreditComment;
				creditAppRequestLine.CreditLimitLineRequest = creditAppRequestLineItemView.CreditLimitLineRequest;
				creditAppRequestLine.CreditScoringGUID = creditAppRequestLineItemView.CreditScoringGUID.StringToGuidNull();
				creditAppRequestLine.CreditTermDescription = creditAppRequestLineItemView.CreditTermDescription;
				creditAppRequestLine.CustomerContactBuyerPeriod = creditAppRequestLineItemView.CustomerContactBuyerPeriod;
				creditAppRequestLine.CustomerRequest = creditAppRequestLineItemView.CustomerRequest;
				creditAppRequestLine.InsuranceCreditLimit = creditAppRequestLineItemView.InsuranceCreditLimit;
				creditAppRequestLine.InvoiceAddressGUID = creditAppRequestLineItemView.InvoiceAddressGUID.StringToGuidNull();
				creditAppRequestLine.KYCSetupGUID = creditAppRequestLineItemView.KYCSetupGUID.StringToGuidNull();
				creditAppRequestLine.LineNum = creditAppRequestLineItemView.LineNum;
				creditAppRequestLine.MailingReceiptAddressGUID = creditAppRequestLineItemView.MailingReceiptAddressGUID.StringToGuidNull();
				creditAppRequestLine.MarketingComment = creditAppRequestLineItemView.MarketingComment;
				creditAppRequestLine.MaxPurchasePct = creditAppRequestLineItemView.MaxPurchasePct;
				creditAppRequestLine.MethodOfBilling = creditAppRequestLineItemView.MethodOfBilling;
				creditAppRequestLine.MethodOfPaymentGUID = creditAppRequestLineItemView.MethodOfPaymentGUID.StringToGuidNull();
				creditAppRequestLine.PaymentCondition = creditAppRequestLineItemView.PaymentCondition;
				creditAppRequestLine.PurchaseFeeCalculateBase = creditAppRequestLineItemView.PurchaseFeeCalculateBase;
				creditAppRequestLine.PurchaseFeePct = creditAppRequestLineItemView.PurchaseFeePct;
				creditAppRequestLine.ReceiptAddressGUID = creditAppRequestLineItemView.ReceiptAddressGUID.StringToGuidNull();
				creditAppRequestLine.ReceiptContactPersonGUID = creditAppRequestLineItemView.ReceiptContactPersonGUID.StringToGuidNull();
				creditAppRequestLine.ReceiptDay = creditAppRequestLineItemView.ReceiptDay;
				creditAppRequestLine.ReceiptDescription = creditAppRequestLineItemView.ReceiptDescription;
				creditAppRequestLine.ReceiptRemark = creditAppRequestLineItemView.ReceiptRemark;
				//R02
				creditAppRequestLine.RemainingCreditLoanRequest = creditAppRequestLineItemView.RemainingCreditLoanRequest;
				creditAppRequestLine.AllCustomerBuyerOutstanding = creditAppRequestLineItemView.AllCustomerBuyerOutstanding;
				creditAppRequestLine.CustomerBuyerOutstanding = creditAppRequestLineItemView.CustomerBuyerOutstanding;
				creditAppRequestLine.RefCreditAppLineGUID = creditAppRequestLineItemView.RefCreditAppLineGUID.StringToGuidNull();
				creditAppRequestLine.LineCondition = creditAppRequestLineItemView.LineCondition;
				
				creditAppRequestLine.RowVersion = creditAppRequestLineItemView.RowVersion;
				return creditAppRequestLine;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CreditAppRequestLine> ToCreditAppRequestLine(this IEnumerable<CreditAppRequestLineItemView> creditAppRequestLineItemViews)
		{
			try
			{
				List<CreditAppRequestLine> creditAppRequestLines = new List<CreditAppRequestLine>();
				foreach (CreditAppRequestLineItemView item in creditAppRequestLineItemViews)
				{
					creditAppRequestLines.Add(item.ToCreditAppRequestLine());
				}
				return creditAppRequestLines;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CreditAppRequestLineItemView ToCreditAppRequestLineItemView(this CreditAppRequestLine creditAppRequestLine)
		{
			try
			{
				CreditAppRequestLineItemView creditAppRequestLineItemView = new CreditAppRequestLineItemView();
				creditAppRequestLineItemView.CompanyGUID = creditAppRequestLine.CompanyGUID.GuidNullToString();
				creditAppRequestLineItemView.CreatedBy = creditAppRequestLine.CreatedBy;
				creditAppRequestLineItemView.CreatedDateTime = creditAppRequestLine.CreatedDateTime.DateTimeToString();
				creditAppRequestLineItemView.ModifiedBy = creditAppRequestLine.ModifiedBy;
				creditAppRequestLineItemView.ModifiedDateTime = creditAppRequestLine.ModifiedDateTime.DateTimeToString();
				creditAppRequestLineItemView.Owner = creditAppRequestLine.Owner;
				creditAppRequestLineItemView.OwnerBusinessUnitGUID = creditAppRequestLine.OwnerBusinessUnitGUID.GuidNullToString();
				creditAppRequestLineItemView.CreditAppRequestLineGUID = creditAppRequestLine.CreditAppRequestLineGUID.GuidNullToString();
				creditAppRequestLineItemView.AcceptanceDocument = creditAppRequestLine.AcceptanceDocument;
				creditAppRequestLineItemView.AcceptanceDocumentDescription = creditAppRequestLine.AcceptanceDocumentDescription;
				creditAppRequestLineItemView.ApprovalDecision = creditAppRequestLine.ApprovalDecision;
				creditAppRequestLineItemView.ApprovedCreditLimitLineRequest = creditAppRequestLine.ApprovedCreditLimitLineRequest;
				creditAppRequestLineItemView.ApproverComment = creditAppRequestLine.ApproverComment;
				creditAppRequestLineItemView.AssignmentAgreementTableGUID = creditAppRequestLine.AssignmentAgreementTableGUID.GuidNullToString();
				creditAppRequestLineItemView.AssignmentMethodGUID = creditAppRequestLine.AssignmentMethodGUID.GuidNullToString();
				creditAppRequestLineItemView.AssignmentMethodRemark = creditAppRequestLine.AssignmentMethodRemark;
				creditAppRequestLineItemView.BillingAddressGUID = creditAppRequestLine.BillingAddressGUID.GuidNullToString();
				creditAppRequestLineItemView.BillingContactPersonGUID = creditAppRequestLine.BillingContactPersonGUID.GuidNullToString();
				creditAppRequestLineItemView.BillingDay = creditAppRequestLine.BillingDay;
				creditAppRequestLineItemView.BillingDescription = creditAppRequestLine.BillingDescription;
				creditAppRequestLineItemView.BillingRemark = creditAppRequestLine.BillingRemark;
				creditAppRequestLineItemView.BillingResponsibleByGUID = creditAppRequestLine.BillingResponsibleByGUID.GuidNullToString();
				creditAppRequestLineItemView.BlacklistStatusGUID = creditAppRequestLine.BlacklistStatusGUID.GuidNullToString();
				creditAppRequestLineItemView.BuyerCreditLimit = creditAppRequestLine.BuyerCreditLimit;
				creditAppRequestLineItemView.BuyerCreditTermGUID = creditAppRequestLine.BuyerCreditTermGUID.GuidNullToString();
				creditAppRequestLineItemView.BuyerProduct = creditAppRequestLine.BuyerProduct;
				creditAppRequestLineItemView.BuyerTableGUID = creditAppRequestLine.BuyerTableGUID.GuidNullToString();
				creditAppRequestLineItemView.CreditAppRequestTableGUID = creditAppRequestLine.CreditAppRequestTableGUID.GuidNullToString();
				creditAppRequestLineItemView.CreditComment = creditAppRequestLine.CreditComment;
				creditAppRequestLineItemView.CreditLimitLineRequest = creditAppRequestLine.CreditLimitLineRequest;
				creditAppRequestLineItemView.CreditScoringGUID = creditAppRequestLine.CreditScoringGUID.GuidNullToString();
				creditAppRequestLineItemView.CreditTermDescription = creditAppRequestLine.CreditTermDescription;
				creditAppRequestLineItemView.CustomerContactBuyerPeriod = creditAppRequestLine.CustomerContactBuyerPeriod;
				creditAppRequestLineItemView.CustomerRequest = creditAppRequestLine.CustomerRequest;
				creditAppRequestLineItemView.InsuranceCreditLimit = creditAppRequestLine.InsuranceCreditLimit;
				creditAppRequestLineItemView.InvoiceAddressGUID = creditAppRequestLine.InvoiceAddressGUID.GuidNullToString();
				creditAppRequestLineItemView.KYCSetupGUID = creditAppRequestLine.KYCSetupGUID.GuidNullToString();
				creditAppRequestLineItemView.LineNum = creditAppRequestLine.LineNum;
				creditAppRequestLineItemView.MailingReceiptAddressGUID = creditAppRequestLine.MailingReceiptAddressGUID.GuidNullToString();
				creditAppRequestLineItemView.MarketingComment = creditAppRequestLine.MarketingComment;
				creditAppRequestLineItemView.MaxPurchasePct = creditAppRequestLine.MaxPurchasePct;
				creditAppRequestLineItemView.MethodOfBilling = creditAppRequestLine.MethodOfBilling;
				creditAppRequestLineItemView.MethodOfPaymentGUID = creditAppRequestLine.MethodOfPaymentGUID.GuidNullToString();
				creditAppRequestLineItemView.PaymentCondition = creditAppRequestLine.PaymentCondition;
				creditAppRequestLineItemView.PurchaseFeeCalculateBase = creditAppRequestLine.PurchaseFeeCalculateBase;
				creditAppRequestLineItemView.PurchaseFeePct = creditAppRequestLine.PurchaseFeePct;
				creditAppRequestLineItemView.ReceiptAddressGUID = creditAppRequestLine.ReceiptAddressGUID.GuidNullToString();
				creditAppRequestLineItemView.ReceiptContactPersonGUID = creditAppRequestLine.ReceiptContactPersonGUID.GuidNullToString();
				creditAppRequestLineItemView.ReceiptDay = creditAppRequestLine.ReceiptDay;
				creditAppRequestLineItemView.ReceiptDescription = creditAppRequestLine.ReceiptDescription;
				creditAppRequestLineItemView.ReceiptRemark = creditAppRequestLine.ReceiptRemark;
				//R02
				creditAppRequestLineItemView.RemainingCreditLoanRequest = creditAppRequestLine.RemainingCreditLoanRequest;
				creditAppRequestLineItemView.AllCustomerBuyerOutstanding = creditAppRequestLine.AllCustomerBuyerOutstanding;
				creditAppRequestLineItemView.CustomerBuyerOutstanding = creditAppRequestLine.CustomerBuyerOutstanding;
				creditAppRequestLineItemView.RefCreditAppLineGUID = creditAppRequestLine.RefCreditAppLineGUID.GuidNullToString();
				creditAppRequestLineItemView.LineCondition = creditAppRequestLine.LineCondition;
				
				creditAppRequestLineItemView.RowVersion = creditAppRequestLine.RowVersion;
				return creditAppRequestLineItemView.GetCreditAppRequestLineItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CreditAppRequestLineItemView
		#region ToDropDown
		public static SelectItem<CreditAppRequestLineItemView> ToDropDownItem(this CreditAppRequestLineItemView creditAppRequestLineView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CreditAppRequestLineItemView> selectItem = new SelectItem<CreditAppRequestLineItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(creditAppRequestLineView.LineNum.ToString());
				selectItem.Value = creditAppRequestLineView.CreditAppRequestLineGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: creditAppRequestLineView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CreditAppRequestLineItemView>> ToDropDownItem(this IEnumerable<CreditAppRequestLineItemView> creditAppRequestLineItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CreditAppRequestLineItemView>> selectItems = new List<SelectItem<CreditAppRequestLineItemView>>();
				foreach (CreditAppRequestLineItemView item in creditAppRequestLineItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

