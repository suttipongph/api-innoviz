using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class GradeClassificationHandler
	{
		#region GradeClassificationListView
		public static List<GradeClassificationListView> GetGradeClassificationListViewValidation(this List<GradeClassificationListView> gradeClassificationListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return gradeClassificationListViews;
			}
			var result = new List<GradeClassificationListView>();
			try
			{
				foreach (GradeClassificationListView item in gradeClassificationListViews)
				{
					result.Add(item.GetGradeClassificationListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static GradeClassificationListView GetGradeClassificationListViewValidation(this GradeClassificationListView gradeClassificationListView)
		{
			try
			{
				gradeClassificationListView.RowAuthorize = gradeClassificationListView.GetListRowAuthorize();
				return gradeClassificationListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this GradeClassificationListView gradeClassificationListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion GradeClassificationListView
		#region GradeClassificationItemView
		public static GradeClassificationItemView GetGradeClassificationItemViewValidation(this GradeClassificationItemView gradeClassificationItemView)
		{
			try
			{
				gradeClassificationItemView.RowAuthorize = gradeClassificationItemView.GetItemRowAuthorize();
				return gradeClassificationItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this GradeClassificationItemView gradeClassificationItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static GradeClassification ToGradeClassification(this GradeClassificationItemView gradeClassificationItemView)
		{
			try
			{
				GradeClassification gradeClassification = new GradeClassification();
				gradeClassification.CompanyGUID = gradeClassificationItemView.CompanyGUID.StringToGuid();
				gradeClassification.CreatedBy = gradeClassificationItemView.CreatedBy;
				gradeClassification.CreatedDateTime = gradeClassificationItemView.CreatedDateTime.StringToSystemDateTime();
				gradeClassification.ModifiedBy = gradeClassificationItemView.ModifiedBy;
				gradeClassification.ModifiedDateTime = gradeClassificationItemView.ModifiedDateTime.StringToSystemDateTime();
				gradeClassification.Owner = gradeClassificationItemView.Owner;
				gradeClassification.OwnerBusinessUnitGUID = gradeClassificationItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				gradeClassification.GradeClassificationGUID = gradeClassificationItemView.GradeClassificationGUID.StringToGuid();
				gradeClassification.Description = gradeClassificationItemView.Description;
				gradeClassification.GradeClassificationId = gradeClassificationItemView.GradeClassificationId;
				
				gradeClassification.RowVersion = gradeClassificationItemView.RowVersion;
				return gradeClassification;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<GradeClassification> ToGradeClassification(this IEnumerable<GradeClassificationItemView> gradeClassificationItemViews)
		{
			try
			{
				List<GradeClassification> gradeClassifications = new List<GradeClassification>();
				foreach (GradeClassificationItemView item in gradeClassificationItemViews)
				{
					gradeClassifications.Add(item.ToGradeClassification());
				}
				return gradeClassifications;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static GradeClassificationItemView ToGradeClassificationItemView(this GradeClassification gradeClassification)
		{
			try
			{
				GradeClassificationItemView gradeClassificationItemView = new GradeClassificationItemView();
				gradeClassificationItemView.CompanyGUID = gradeClassification.CompanyGUID.GuidNullToString();
				gradeClassificationItemView.CreatedBy = gradeClassification.CreatedBy;
				gradeClassificationItemView.CreatedDateTime = gradeClassification.CreatedDateTime.DateTimeToString();
				gradeClassificationItemView.ModifiedBy = gradeClassification.ModifiedBy;
				gradeClassificationItemView.ModifiedDateTime = gradeClassification.ModifiedDateTime.DateTimeToString();
				gradeClassificationItemView.Owner = gradeClassification.Owner;
				gradeClassificationItemView.OwnerBusinessUnitGUID = gradeClassification.OwnerBusinessUnitGUID.GuidNullToString();
				gradeClassificationItemView.GradeClassificationGUID = gradeClassification.GradeClassificationGUID.GuidNullToString();
				gradeClassificationItemView.Description = gradeClassification.Description;
				gradeClassificationItemView.GradeClassificationId = gradeClassification.GradeClassificationId;
				
				gradeClassificationItemView.RowVersion = gradeClassification.RowVersion;
				return gradeClassificationItemView.GetGradeClassificationItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GradeClassificationItemView
		#region ToDropDown
		public static SelectItem<GradeClassificationItemView> ToDropDownItem(this GradeClassificationItemView gradeClassificationView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<GradeClassificationItemView> selectItem = new SelectItem<GradeClassificationItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(gradeClassificationView.GradeClassificationId, gradeClassificationView.Description);
				selectItem.Value = gradeClassificationView.GradeClassificationGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: gradeClassificationView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<GradeClassificationItemView>> ToDropDownItem(this IEnumerable<GradeClassificationItemView> gradeClassificationItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<GradeClassificationItemView>> selectItems = new List<SelectItem<GradeClassificationItemView>>();
				foreach (GradeClassificationItemView item in gradeClassificationItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

