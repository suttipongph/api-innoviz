using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class BookmarkDocumentTemplateLineHandler
	{
		#region BookmarkDocumentTemplateLineListView
		public static List<BookmarkDocumentTemplateLineListView> GetBookmarkDocumentTemplateLineListViewValidation(this List<BookmarkDocumentTemplateLineListView> bookmarkDocumentTemplateLineListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return bookmarkDocumentTemplateLineListViews;
			}
			var result = new List<BookmarkDocumentTemplateLineListView>();
			try
			{
				foreach (BookmarkDocumentTemplateLineListView item in bookmarkDocumentTemplateLineListViews)
				{
					result.Add(item.GetBookmarkDocumentTemplateLineListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BookmarkDocumentTemplateLineListView GetBookmarkDocumentTemplateLineListViewValidation(this BookmarkDocumentTemplateLineListView bookmarkDocumentTemplateLineListView)
		{
			try
			{
				bookmarkDocumentTemplateLineListView.RowAuthorize = bookmarkDocumentTemplateLineListView.GetListRowAuthorize();
				return bookmarkDocumentTemplateLineListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this BookmarkDocumentTemplateLineListView bookmarkDocumentTemplateLineListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion BookmarkDocumentTemplateLineListView
		#region BookmarkDocumentTemplateLineItemView
		public static BookmarkDocumentTemplateLineItemView GetBookmarkDocumentTemplateLineItemViewValidation(this BookmarkDocumentTemplateLineItemView bookmarkDocumentTemplateLineItemView)
		{
			try
			{
				bookmarkDocumentTemplateLineItemView.RowAuthorize = bookmarkDocumentTemplateLineItemView.GetItemRowAuthorize();
				return bookmarkDocumentTemplateLineItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this BookmarkDocumentTemplateLineItemView bookmarkDocumentTemplateLineItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static BookmarkDocumentTemplateLine ToBookmarkDocumentTemplateLine(this BookmarkDocumentTemplateLineItemView bookmarkDocumentTemplateLineItemView)
		{
			try
			{
				BookmarkDocumentTemplateLine bookmarkDocumentTemplateLine = new BookmarkDocumentTemplateLine();
				bookmarkDocumentTemplateLine.CompanyGUID = bookmarkDocumentTemplateLineItemView.CompanyGUID.StringToGuid();
				bookmarkDocumentTemplateLine.CreatedBy = bookmarkDocumentTemplateLineItemView.CreatedBy;
				bookmarkDocumentTemplateLine.CreatedDateTime = bookmarkDocumentTemplateLineItemView.CreatedDateTime.StringToSystemDateTime();
				bookmarkDocumentTemplateLine.ModifiedBy = bookmarkDocumentTemplateLineItemView.ModifiedBy;
				bookmarkDocumentTemplateLine.ModifiedDateTime = bookmarkDocumentTemplateLineItemView.ModifiedDateTime.StringToSystemDateTime();
				bookmarkDocumentTemplateLine.Owner = bookmarkDocumentTemplateLineItemView.Owner;
				bookmarkDocumentTemplateLine.OwnerBusinessUnitGUID = bookmarkDocumentTemplateLineItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				bookmarkDocumentTemplateLine.BookmarkDocumentTemplateLineGUID = bookmarkDocumentTemplateLineItemView.BookmarkDocumentTemplateLineGUID.StringToGuid();
				bookmarkDocumentTemplateLine.BookmarkDocumentGUID = bookmarkDocumentTemplateLineItemView.BookmarkDocumentGUID.StringToGuid();
				bookmarkDocumentTemplateLine.BookmarkDocumentTemplateTableGUID = bookmarkDocumentTemplateLineItemView.BookmarkDocumentTemplateTableGUID.StringToGuidNull();
				bookmarkDocumentTemplateLine.DocumentTemplateTableGUID = bookmarkDocumentTemplateLineItemView.DocumentTemplateTableGUID.StringToGuid();
				
				bookmarkDocumentTemplateLine.RowVersion = bookmarkDocumentTemplateLineItemView.RowVersion;
				return bookmarkDocumentTemplateLine;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<BookmarkDocumentTemplateLine> ToBookmarkDocumentTemplateLine(this IEnumerable<BookmarkDocumentTemplateLineItemView> bookmarkDocumentTemplateLineItemViews)
		{
			try
			{
				List<BookmarkDocumentTemplateLine> bookmarkDocumentTemplateLines = new List<BookmarkDocumentTemplateLine>();
				foreach (BookmarkDocumentTemplateLineItemView item in bookmarkDocumentTemplateLineItemViews)
				{
					bookmarkDocumentTemplateLines.Add(item.ToBookmarkDocumentTemplateLine());
				}
				return bookmarkDocumentTemplateLines;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BookmarkDocumentTemplateLineItemView ToBookmarkDocumentTemplateLineItemView(this BookmarkDocumentTemplateLine bookmarkDocumentTemplateLine)
		{
			try
			{
				BookmarkDocumentTemplateLineItemView bookmarkDocumentTemplateLineItemView = new BookmarkDocumentTemplateLineItemView();
				bookmarkDocumentTemplateLineItemView.CompanyGUID = bookmarkDocumentTemplateLine.CompanyGUID.GuidNullToString();
				bookmarkDocumentTemplateLineItemView.CreatedBy = bookmarkDocumentTemplateLine.CreatedBy;
				bookmarkDocumentTemplateLineItemView.CreatedDateTime = bookmarkDocumentTemplateLine.CreatedDateTime.DateTimeToString();
				bookmarkDocumentTemplateLineItemView.ModifiedBy = bookmarkDocumentTemplateLine.ModifiedBy;
				bookmarkDocumentTemplateLineItemView.ModifiedDateTime = bookmarkDocumentTemplateLine.ModifiedDateTime.DateTimeToString();
				bookmarkDocumentTemplateLineItemView.Owner = bookmarkDocumentTemplateLine.Owner;
				bookmarkDocumentTemplateLineItemView.OwnerBusinessUnitGUID = bookmarkDocumentTemplateLine.OwnerBusinessUnitGUID.GuidNullToString();
				bookmarkDocumentTemplateLineItemView.BookmarkDocumentTemplateLineGUID = bookmarkDocumentTemplateLine.BookmarkDocumentTemplateLineGUID.GuidNullToString();
				bookmarkDocumentTemplateLineItemView.BookmarkDocumentGUID = bookmarkDocumentTemplateLine.BookmarkDocumentGUID.GuidNullToString();
				bookmarkDocumentTemplateLineItemView.BookmarkDocumentTemplateTableGUID = bookmarkDocumentTemplateLine.BookmarkDocumentTemplateTableGUID.GuidNullToString();
				bookmarkDocumentTemplateLineItemView.DocumentTemplateTableGUID = bookmarkDocumentTemplateLine.DocumentTemplateTableGUID.GuidNullToString();
				
				bookmarkDocumentTemplateLineItemView.RowVersion = bookmarkDocumentTemplateLine.RowVersion;
				return bookmarkDocumentTemplateLineItemView.GetBookmarkDocumentTemplateLineItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion BookmarkDocumentTemplateLineItemView
		#region ToDropDown
		public static SelectItem<BookmarkDocumentTemplateLineItemView> ToDropDownItem(this BookmarkDocumentTemplateLineItemView bookmarkDocumentTemplateLineView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<BookmarkDocumentTemplateLineItemView> selectItem = new SelectItem<BookmarkDocumentTemplateLineItemView>();
				selectItem.Label = null;
				selectItem.Value = bookmarkDocumentTemplateLineView.BookmarkDocumentTemplateLineGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: bookmarkDocumentTemplateLineView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<BookmarkDocumentTemplateLineItemView>> ToDropDownItem(this IEnumerable<BookmarkDocumentTemplateLineItemView> bookmarkDocumentTemplateLineItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<BookmarkDocumentTemplateLineItemView>> selectItems = new List<SelectItem<BookmarkDocumentTemplateLineItemView>>();
				foreach (BookmarkDocumentTemplateLineItemView item in bookmarkDocumentTemplateLineItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

