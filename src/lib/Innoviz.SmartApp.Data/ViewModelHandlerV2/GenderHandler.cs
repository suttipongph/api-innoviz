using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class GenderHandler
	{
		#region GenderListView
		public static List<GenderListView> GetGenderListViewValidation(this List<GenderListView> genderListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return genderListViews;
			}
			var result = new List<GenderListView>();
			try
			{
				foreach (GenderListView item in genderListViews)
				{
					result.Add(item.GetGenderListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static GenderListView GetGenderListViewValidation(this GenderListView genderListView)
		{
			try
			{
				genderListView.RowAuthorize = genderListView.GetListRowAuthorize();
				return genderListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this GenderListView genderListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion GenderListView
		#region GenderItemView
		public static GenderItemView GetGenderItemViewValidation(this GenderItemView genderItemView)
		{
			try
			{
				genderItemView.RowAuthorize = genderItemView.GetItemRowAuthorize();
				return genderItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this GenderItemView genderItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static Gender ToGender(this GenderItemView genderItemView)
		{
			try
			{
				Gender gender = new Gender();
				gender.CompanyGUID = genderItemView.CompanyGUID.StringToGuid();
				gender.CreatedBy = genderItemView.CreatedBy;
				gender.CreatedDateTime = genderItemView.CreatedDateTime.StringToSystemDateTime();
				gender.ModifiedBy = genderItemView.ModifiedBy;
				gender.ModifiedDateTime = genderItemView.ModifiedDateTime.StringToSystemDateTime();
				gender.Owner = genderItemView.Owner;
				gender.OwnerBusinessUnitGUID = genderItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				gender.GenderGUID = genderItemView.GenderGUID.StringToGuid();
				gender.Description = genderItemView.Description;
				gender.GenderId = genderItemView.GenderId;
				
				gender.RowVersion = genderItemView.RowVersion;
				return gender;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<Gender> ToGender(this IEnumerable<GenderItemView> genderItemViews)
		{
			try
			{
				List<Gender> genders = new List<Gender>();
				foreach (GenderItemView item in genderItemViews)
				{
					genders.Add(item.ToGender());
				}
				return genders;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static GenderItemView ToGenderItemView(this Gender gender)
		{
			try
			{
				GenderItemView genderItemView = new GenderItemView();
				genderItemView.CompanyGUID = gender.CompanyGUID.GuidNullToString();
				genderItemView.CreatedBy = gender.CreatedBy;
				genderItemView.CreatedDateTime = gender.CreatedDateTime.DateTimeToString();
				genderItemView.ModifiedBy = gender.ModifiedBy;
				genderItemView.ModifiedDateTime = gender.ModifiedDateTime.DateTimeToString();
				genderItemView.Owner = gender.Owner;
				genderItemView.OwnerBusinessUnitGUID = gender.OwnerBusinessUnitGUID.GuidNullToString();
				genderItemView.GenderGUID = gender.GenderGUID.GuidNullToString();
				genderItemView.Description = gender.Description;
				genderItemView.GenderId = gender.GenderId;
				
				genderItemView.RowVersion = gender.RowVersion;
				return genderItemView.GetGenderItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GenderItemView
		#region ToDropDown
		public static SelectItem<GenderItemView> ToDropDownItem(this GenderItemView genderView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<GenderItemView> selectItem = new SelectItem<GenderItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(genderView.GenderId, genderView.Description);
				selectItem.Value = genderView.GenderGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: genderView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<GenderItemView>> ToDropDownItem(this IEnumerable<GenderItemView> genderItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<GenderItemView>> selectItems = new List<SelectItem<GenderItemView>>();
				foreach (GenderItemView item in genderItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

