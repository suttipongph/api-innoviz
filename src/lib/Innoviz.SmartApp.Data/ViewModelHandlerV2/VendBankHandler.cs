using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class VendBankHandler
	{
		#region VendBankListView
		public static List<VendBankListView> GetVendBankListViewValidation(this List<VendBankListView> vendBankListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return vendBankListViews;
			}
			var result = new List<VendBankListView>();
			try
			{
				foreach (VendBankListView item in vendBankListViews)
				{
					result.Add(item.GetVendBankListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static VendBankListView GetVendBankListViewValidation(this VendBankListView vendBankListView)
		{
			try
			{
				vendBankListView.RowAuthorize = vendBankListView.GetListRowAuthorize();
				return vendBankListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this VendBankListView vendBankListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion VendBankListView
		#region VendBankItemView
		public static VendBankItemView GetVendBankItemViewValidation(this VendBankItemView vendBankItemView)
		{
			try
			{
				vendBankItemView.RowAuthorize = vendBankItemView.GetItemRowAuthorize();
				return vendBankItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this VendBankItemView vendBankItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static VendBank ToVendBank(this VendBankItemView vendBankItemView)
		{
			try
			{
				VendBank vendBank = new VendBank();
				vendBank.CompanyGUID = vendBankItemView.CompanyGUID.StringToGuid();
				vendBank.CreatedBy = vendBankItemView.CreatedBy;
				vendBank.CreatedDateTime = vendBankItemView.CreatedDateTime.StringToSystemDateTime();
				vendBank.ModifiedBy = vendBankItemView.ModifiedBy;
				vendBank.ModifiedDateTime = vendBankItemView.ModifiedDateTime.StringToSystemDateTime();
				vendBank.Owner = vendBankItemView.Owner;
				vendBank.OwnerBusinessUnitGUID = vendBankItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				vendBank.VendBankGUID = vendBankItemView.VendBankGUID.StringToGuid();
				vendBank.BankAccount = vendBankItemView.BankAccount;
				vendBank.BankAccountName = vendBankItemView.BankAccountName;
				vendBank.BankBranch = vendBankItemView.BankBranch;
				vendBank.BankGroupGUID = vendBankItemView.BankGroupGUID.StringToGuid();
				vendBank.VendorTableGUID = vendBankItemView.VendorTableGUID.StringToGuid();
				vendBank.Primary = vendBankItemView.Primary;
				
				vendBank.RowVersion = vendBankItemView.RowVersion;
				return vendBank;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<VendBank> ToVendBank(this IEnumerable<VendBankItemView> vendBankItemViews)
		{
			try
			{
				List<VendBank> vendBanks = new List<VendBank>();
				foreach (VendBankItemView item in vendBankItemViews)
				{
					vendBanks.Add(item.ToVendBank());
				}
				return vendBanks;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static VendBankItemView ToVendBankItemView(this VendBank vendBank)
		{
			try
			{
				VendBankItemView vendBankItemView = new VendBankItemView();
				vendBankItemView.CompanyGUID = vendBank.CompanyGUID.GuidNullToString();
				vendBankItemView.CreatedBy = vendBank.CreatedBy;
				vendBankItemView.CreatedDateTime = vendBank.CreatedDateTime.DateTimeToString();
				vendBankItemView.ModifiedBy = vendBank.ModifiedBy;
				vendBankItemView.ModifiedDateTime = vendBank.ModifiedDateTime.DateTimeToString();
				vendBankItemView.Owner = vendBank.Owner;
				vendBankItemView.OwnerBusinessUnitGUID = vendBank.OwnerBusinessUnitGUID.GuidNullToString();
				vendBankItemView.VendBankGUID = vendBank.VendBankGUID.GuidNullToString();
				vendBankItemView.BankAccount = vendBank.BankAccount;
				vendBankItemView.BankAccountName = vendBank.BankAccountName;
				vendBankItemView.BankBranch = vendBank.BankBranch;
				vendBankItemView.BankGroupGUID = vendBank.BankGroupGUID.GuidNullToString();
				vendBankItemView.VendorTableGUID = vendBank.VendorTableGUID.GuidNullToString();
				vendBankItemView.Primary = vendBank.Primary;
				
				vendBankItemView.RowVersion = vendBank.RowVersion;
				return vendBankItemView.GetVendBankItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion VendBankItemView
		#region ToDropDown
		public static SelectItem<VendBankItemView> ToDropDownItem(this VendBankItemView vendBankView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<VendBankItemView> selectItem = new SelectItem<VendBankItemView>();
				selectItem.Label = null;
				selectItem.Value = vendBankView.VendBankGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: vendBankView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<VendBankItemView>> ToDropDownItem(this IEnumerable<VendBankItemView> vendBankItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<VendBankItemView>> selectItems = new List<SelectItem<VendBankItemView>>();
				foreach (VendBankItemView item in vendBankItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

