using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class ApplicationTableHandler
	{
		#region ApplicationTableListView
		public static List<ApplicationTableListView> GetApplicationTableListViewValidation(this List<ApplicationTableListView> applicationTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return applicationTableListViews;
			}
			var result = new List<ApplicationTableListView>();
			try
			{
				foreach (ApplicationTableListView item in applicationTableListViews)
				{
					result.Add(item.GetApplicationTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ApplicationTableListView GetApplicationTableListViewValidation(this ApplicationTableListView applicationTableListView)
		{
			try
			{
				applicationTableListView.RowAuthorize = applicationTableListView.GetListRowAuthorize();
				return applicationTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this ApplicationTableListView applicationTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion ApplicationTableListView
		#region ApplicationTableItemView
		public static ApplicationTableItemView GetApplicationTableItemViewValidation(this ApplicationTableItemView applicationTableItemView)
		{
			try
			{
				applicationTableItemView.RowAuthorize = applicationTableItemView.GetItemRowAuthorize();
				return applicationTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this ApplicationTableItemView applicationTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static ApplicationTable ToApplicationTable(this ApplicationTableItemView applicationTableItemView)
		{
			try
			{
				ApplicationTable applicationTable = new ApplicationTable();
				applicationTable.CompanyGUID = applicationTableItemView.CompanyGUID.StringToGuid();
				applicationTable.CreatedBy = applicationTableItemView.CreatedBy;
				applicationTable.CreatedDateTime = applicationTableItemView.CreatedDateTime.StringToSystemDateTime();
				applicationTable.ModifiedBy = applicationTableItemView.ModifiedBy;
				applicationTable.ModifiedDateTime = applicationTableItemView.ModifiedDateTime.StringToSystemDateTime();
				applicationTable.Owner = applicationTableItemView.Owner;
				applicationTable.OwnerBusinessUnitGUID = applicationTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				applicationTable.ApplicationTableGUID = applicationTableItemView.ApplicationTableGUID.StringToGuid();
				applicationTable.AgreementTypeGUID = applicationTableItemView.AgreementTypeGUID.StringToGuid();
				applicationTable.ApplicationDate = applicationTableItemView.ApplicationDate.StringNullToDateNull();
				applicationTable.ApplicationId = applicationTableItemView.ApplicationId;
				applicationTable.BillingAddressGUID = applicationTableItemView.BillingAddressGUID.StringToGuidNull();
				applicationTable.CalculationType = applicationTableItemView.CalculationType;
				applicationTable.CreditResultGUID = applicationTableItemView.CreditResultGUID.StringToGuidNull();
				applicationTable.CreditResultNotes = applicationTableItemView.CreditResultNotes;
				applicationTable.CurrencyGUID = applicationTableItemView.CurrencyGUID.StringToGuidNull();
				applicationTable.CustomerTableGUID = applicationTableItemView.CustomerTableGUID.StringToGuid();
				applicationTable.DeliveryAddressGUID = applicationTableItemView.DeliveryAddressGUID.StringToGuidNull();
				applicationTable.Dimension1GUID = applicationTableItemView.Dimension1GUID.StringToGuidNull();
				applicationTable.Dimension2GUID = applicationTableItemView.Dimension2GUID.StringToGuidNull();
				applicationTable.Dimension3GUID = applicationTableItemView.Dimension3GUID.StringToGuidNull();
				applicationTable.Dimension4GUID = applicationTableItemView.Dimension4GUID.StringToGuidNull();
				applicationTable.Dimension5GUID = applicationTableItemView.Dimension5GUID.StringToGuidNull();
				applicationTable.DocumentReasonGUID = applicationTableItemView.DocumentReasonGUID.StringToGuidNull();
				applicationTable.DocumentStatusGUID = applicationTableItemView.DocumentStatusGUID.StringToGuid();
				applicationTable.ExpectedExecuteDate = applicationTableItemView.ExpectedExecuteDate.StringNullToDateNull();
				applicationTable.ExpirationDate = applicationTableItemView.ExpirationDate.StringNullToDateNull();
				applicationTable.FlexInfo1 = applicationTableItemView.FlexInfo1;
				applicationTable.FlexInfo2 = applicationTableItemView.FlexInfo2;
				applicationTable.FlexSetupGUID1 = applicationTableItemView.FlexSetupGUID1.StringToGuidNull();
				applicationTable.FlexSetupGUID2 = applicationTableItemView.FlexSetupGUID2.StringToGuidNull();
				applicationTable.FollowupDate = applicationTableItemView.FollowupDate.StringNullToDateNull();
				applicationTable.IntroducedByGUID = applicationTableItemView.IntroducedByGUID.StringToGuidNull();
				applicationTable.InvoiceAddressGUID = applicationTableItemView.InvoiceAddressGUID.StringToGuidNull();
				applicationTable.LanguageGUID = applicationTableItemView.LanguageGUID.StringToGuidNull();
				applicationTable.LeaseSubTypeGUID = applicationTableItemView.LeaseSubTypeGUID.StringToGuidNull();
				applicationTable.LeaseTypeGUID = applicationTableItemView.LeaseTypeGUID.StringToGuid();
				applicationTable.MailingInvoiceAddressGUID = applicationTableItemView.MailingInvoiceAddressGUID.StringToGuidNull();
				applicationTable.MailingReceiptAddressGUID = applicationTableItemView.MailingReceiptAddressGUID.StringToGuidNull();
				applicationTable.NCBAccountStatusGUID = applicationTableItemView.NCBAccountStatusGUID.StringToGuidNull();
				applicationTable.NCBNotes = applicationTableItemView.NCBNotes;
				applicationTable.OriginalAgreementGUID = applicationTableItemView.OriginalAgreementGUID.StringToGuidNull();
				applicationTable.PaymentFrequencyGUID = applicationTableItemView.PaymentFrequencyGUID.StringToGuid();
				applicationTable.ProcessInstanceId = applicationTableItemView.ProcessInstanceId;
				applicationTable.PropertyAddressGUID = applicationTableItemView.PropertyAddressGUID.StringToGuidNull();
				applicationTable.ProspectTableGUID = applicationTableItemView.ProspectTableGUID.StringToGuidNull();
				applicationTable.ReasonRemark = applicationTableItemView.ReasonRemark;
				applicationTable.ReceiptAddressGUID = applicationTableItemView.ReceiptAddressGUID.StringToGuidNull();
				applicationTable.RefAgreementExtension = applicationTableItemView.RefAgreementExtension;
				applicationTable.RegisterAddressGUID = applicationTableItemView.RegisterAddressGUID.StringToGuidNull();
				applicationTable.Remark = applicationTableItemView.Remark;
				applicationTable.ResponsibleBy = applicationTableItemView.ResponsibleBy.StringToGuidNull();
				applicationTable.SalesLeaseBack = applicationTableItemView.SalesLeaseBack;
				applicationTable.WorkingAddressGUID = applicationTableItemView.WorkingAddressGUID.StringToGuidNull();
				
				applicationTable.RowVersion = applicationTableItemView.RowVersion;
				return applicationTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<ApplicationTable> ToApplicationTable(this IEnumerable<ApplicationTableItemView> applicationTableItemViews)
		{
			try
			{
				List<ApplicationTable> applicationTables = new List<ApplicationTable>();
				foreach (ApplicationTableItemView item in applicationTableItemViews)
				{
					applicationTables.Add(item.ToApplicationTable());
				}
				return applicationTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ApplicationTableItemView ToApplicationTableItemView(this ApplicationTable applicationTable)
		{
			try
			{
				ApplicationTableItemView applicationTableItemView = new ApplicationTableItemView();
				applicationTableItemView.CompanyGUID = applicationTable.CompanyGUID.GuidNullToString();
				applicationTableItemView.CreatedBy = applicationTable.CreatedBy;
				applicationTableItemView.CreatedDateTime = applicationTable.CreatedDateTime.DateTimeToString();
				applicationTableItemView.ModifiedBy = applicationTable.ModifiedBy;
				applicationTableItemView.ModifiedDateTime = applicationTable.ModifiedDateTime.DateTimeToString();
				applicationTableItemView.Owner = applicationTable.Owner;
				applicationTableItemView.OwnerBusinessUnitGUID = applicationTable.OwnerBusinessUnitGUID.GuidNullToString();
				applicationTableItemView.ApplicationTableGUID = applicationTable.ApplicationTableGUID.GuidNullToString();
				applicationTableItemView.AgreementTypeGUID = applicationTable.AgreementTypeGUID.GuidNullToString();
				applicationTableItemView.ApplicationDate = applicationTable.ApplicationDate.DateNullToString();
				applicationTableItemView.ApplicationId = applicationTable.ApplicationId;
				applicationTableItemView.BillingAddressGUID = applicationTable.BillingAddressGUID.GuidNullToString();
				applicationTableItemView.CalculationType = applicationTable.CalculationType;
				applicationTableItemView.CreditResultGUID = applicationTable.CreditResultGUID.GuidNullToString();
				applicationTableItemView.CreditResultNotes = applicationTable.CreditResultNotes;
				applicationTableItemView.CurrencyGUID = applicationTable.CurrencyGUID.GuidNullToString();
				applicationTableItemView.CustomerTableGUID = applicationTable.CustomerTableGUID.GuidNullToString();
				applicationTableItemView.DeliveryAddressGUID = applicationTable.DeliveryAddressGUID.GuidNullToString();
				applicationTableItemView.Dimension1GUID = applicationTable.Dimension1GUID.GuidNullToString();
				applicationTableItemView.Dimension2GUID = applicationTable.Dimension2GUID.GuidNullToString();
				applicationTableItemView.Dimension3GUID = applicationTable.Dimension3GUID.GuidNullToString();
				applicationTableItemView.Dimension4GUID = applicationTable.Dimension4GUID.GuidNullToString();
				applicationTableItemView.Dimension5GUID = applicationTable.Dimension5GUID.GuidNullToString();
				applicationTableItemView.DocumentReasonGUID = applicationTable.DocumentReasonGUID.GuidNullToString();
				applicationTableItemView.DocumentStatusGUID = applicationTable.DocumentStatusGUID.GuidNullToString();
				applicationTableItemView.ExpectedExecuteDate = applicationTable.ExpectedExecuteDate.DateNullToString();
				applicationTableItemView.ExpirationDate = applicationTable.ExpirationDate.DateNullToString();
				applicationTableItemView.FlexInfo1 = applicationTable.FlexInfo1;
				applicationTableItemView.FlexInfo2 = applicationTable.FlexInfo2;
				applicationTableItemView.FlexSetupGUID1 = applicationTable.FlexSetupGUID1.GuidNullToString();
				applicationTableItemView.FlexSetupGUID2 = applicationTable.FlexSetupGUID2.GuidNullToString();
				applicationTableItemView.FollowupDate = applicationTable.FollowupDate.DateNullToString();
				applicationTableItemView.IntroducedByGUID = applicationTable.IntroducedByGUID.GuidNullToString();
				applicationTableItemView.InvoiceAddressGUID = applicationTable.InvoiceAddressGUID.GuidNullToString();
				applicationTableItemView.LanguageGUID = applicationTable.LanguageGUID.GuidNullToString();
				applicationTableItemView.LeaseSubTypeGUID = applicationTable.LeaseSubTypeGUID.GuidNullToString();
				applicationTableItemView.LeaseTypeGUID = applicationTable.LeaseTypeGUID.GuidNullToString();
				applicationTableItemView.MailingInvoiceAddressGUID = applicationTable.MailingInvoiceAddressGUID.GuidNullToString();
				applicationTableItemView.MailingReceiptAddressGUID = applicationTable.MailingReceiptAddressGUID.GuidNullToString();
				applicationTableItemView.NCBAccountStatusGUID = applicationTable.NCBAccountStatusGUID.GuidNullToString();
				applicationTableItemView.NCBNotes = applicationTable.NCBNotes;
				applicationTableItemView.OriginalAgreementGUID = applicationTable.OriginalAgreementGUID.GuidNullToString();
				applicationTableItemView.PaymentFrequencyGUID = applicationTable.PaymentFrequencyGUID.GuidNullToString();
				applicationTableItemView.ProcessInstanceId = applicationTable.ProcessInstanceId;
				applicationTableItemView.PropertyAddressGUID = applicationTable.PropertyAddressGUID.GuidNullToString();
				applicationTableItemView.ProspectTableGUID = applicationTable.ProspectTableGUID.GuidNullToString();
				applicationTableItemView.ReasonRemark = applicationTable.ReasonRemark;
				applicationTableItemView.ReceiptAddressGUID = applicationTable.ReceiptAddressGUID.GuidNullToString();
				applicationTableItemView.RefAgreementExtension = applicationTable.RefAgreementExtension;
				applicationTableItemView.RegisterAddressGUID = applicationTable.RegisterAddressGUID.GuidNullToString();
				applicationTableItemView.Remark = applicationTable.Remark;
				applicationTableItemView.ResponsibleBy = applicationTable.ResponsibleBy.GuidNullToString();
				applicationTableItemView.SalesLeaseBack = applicationTable.SalesLeaseBack;
				applicationTableItemView.WorkingAddressGUID = applicationTable.WorkingAddressGUID.GuidNullToString();
				
				applicationTableItemView.RowVersion = applicationTable.RowVersion;
				return applicationTableItemView.GetApplicationTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ApplicationTableItemView
		#region ToDropDown
		public static SelectItem<ApplicationTableItemView> ToDropDownItem(this ApplicationTableItemView applicationTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<ApplicationTableItemView> selectItem = new SelectItem<ApplicationTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(applicationTableView.ApplicationId);
				selectItem.Value = applicationTableView.ApplicationTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: applicationTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<ApplicationTableItemView>> ToDropDownItem(this IEnumerable<ApplicationTableItemView> applicationTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<ApplicationTableItemView>> selectItems = new List<SelectItem<ApplicationTableItemView>>();
				foreach (ApplicationTableItemView item in applicationTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

