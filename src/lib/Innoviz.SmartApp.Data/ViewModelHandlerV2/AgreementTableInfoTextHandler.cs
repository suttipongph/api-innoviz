using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class AgreementTableInfoTextHandler
	{
		#region AgreementTableInfoTextListView
		public static List<AgreementTableInfoTextListView> GetAgreementTableInfoTextListViewValidation(this List<AgreementTableInfoTextListView> agreementTableInfoTextListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return agreementTableInfoTextListViews;
			}
			var result = new List<AgreementTableInfoTextListView>();
			try
			{
				foreach (AgreementTableInfoTextListView item in agreementTableInfoTextListViews)
				{
					result.Add(item.GetAgreementTableInfoTextListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AgreementTableInfoTextListView GetAgreementTableInfoTextListViewValidation(this AgreementTableInfoTextListView agreementTableInfoTextListView)
		{
			try
			{
				agreementTableInfoTextListView.RowAuthorize = agreementTableInfoTextListView.GetListRowAuthorize();
				return agreementTableInfoTextListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this AgreementTableInfoTextListView agreementTableInfoTextListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion AgreementTableInfoTextListView
		#region AgreementTableInfoTextItemView
		public static AgreementTableInfoTextItemView GetAgreementTableInfoTextItemViewValidation(this AgreementTableInfoTextItemView agreementTableInfoTextItemView)
		{
			try
			{
				agreementTableInfoTextItemView.RowAuthorize = agreementTableInfoTextItemView.GetItemRowAuthorize();
				return agreementTableInfoTextItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this AgreementTableInfoTextItemView agreementTableInfoTextItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static AgreementTableInfoText ToAgreementTableInfoText(this AgreementTableInfoTextItemView agreementTableInfoTextItemView)
		{
			try
			{
				AgreementTableInfoText agreementTableInfoText = new AgreementTableInfoText();
				agreementTableInfoText.CompanyGUID = agreementTableInfoTextItemView.CompanyGUID.StringToGuid();
				agreementTableInfoText.CreatedBy = agreementTableInfoTextItemView.CreatedBy;
				agreementTableInfoText.CreatedDateTime = agreementTableInfoTextItemView.CreatedDateTime.StringToSystemDateTime();
				agreementTableInfoText.ModifiedBy = agreementTableInfoTextItemView.ModifiedBy;
				agreementTableInfoText.ModifiedDateTime = agreementTableInfoTextItemView.ModifiedDateTime.StringToSystemDateTime();
				agreementTableInfoText.Owner = agreementTableInfoTextItemView.Owner;
				agreementTableInfoText.OwnerBusinessUnitGUID = agreementTableInfoTextItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				agreementTableInfoText.AgreementTableInfoTextGUID = agreementTableInfoTextItemView.AgreementTableInfoTextGUID.StringToGuid();
				agreementTableInfoText.AgreementTableInfoGUID = agreementTableInfoTextItemView.AgreementTableInfoGUID.StringToGuid();
				agreementTableInfoText.Text1 = agreementTableInfoTextItemView.Text1;
				agreementTableInfoText.Text2 = agreementTableInfoTextItemView.Text2;
				agreementTableInfoText.Text3 = agreementTableInfoTextItemView.Text3;
				agreementTableInfoText.Text4 = agreementTableInfoTextItemView.Text4;
				
				agreementTableInfoText.RowVersion = agreementTableInfoTextItemView.RowVersion;
				return agreementTableInfoText;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<AgreementTableInfoText> ToAgreementTableInfoText(this IEnumerable<AgreementTableInfoTextItemView> agreementTableInfoTextItemViews)
		{
			try
			{
				List<AgreementTableInfoText> agreementTableInfoTexts = new List<AgreementTableInfoText>();
				foreach (AgreementTableInfoTextItemView item in agreementTableInfoTextItemViews)
				{
					agreementTableInfoTexts.Add(item.ToAgreementTableInfoText());
				}
				return agreementTableInfoTexts;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AgreementTableInfoTextItemView ToAgreementTableInfoTextItemView(this AgreementTableInfoText agreementTableInfoText)
		{
			try
			{
				AgreementTableInfoTextItemView agreementTableInfoTextItemView = new AgreementTableInfoTextItemView();
				agreementTableInfoTextItemView.CompanyGUID = agreementTableInfoText.CompanyGUID.GuidNullToString();
				agreementTableInfoTextItemView.CreatedBy = agreementTableInfoText.CreatedBy;
				agreementTableInfoTextItemView.CreatedDateTime = agreementTableInfoText.CreatedDateTime.DateTimeToString();
				agreementTableInfoTextItemView.ModifiedBy = agreementTableInfoText.ModifiedBy;
				agreementTableInfoTextItemView.ModifiedDateTime = agreementTableInfoText.ModifiedDateTime.DateTimeToString();
				agreementTableInfoTextItemView.Owner = agreementTableInfoText.Owner;
				agreementTableInfoTextItemView.OwnerBusinessUnitGUID = agreementTableInfoText.OwnerBusinessUnitGUID.GuidNullToString();
				agreementTableInfoTextItemView.AgreementTableInfoTextGUID = agreementTableInfoText.AgreementTableInfoTextGUID.GuidNullToString();
				agreementTableInfoTextItemView.AgreementTableInfoGUID = agreementTableInfoText.AgreementTableInfoGUID.GuidNullToString();
				agreementTableInfoTextItemView.Text1 = agreementTableInfoText.Text1;
				agreementTableInfoTextItemView.Text2 = agreementTableInfoText.Text2;
				agreementTableInfoTextItemView.Text3 = agreementTableInfoText.Text3;
				agreementTableInfoTextItemView.Text4 = agreementTableInfoText.Text4;
				
				agreementTableInfoTextItemView.RowVersion = agreementTableInfoText.RowVersion;
				return agreementTableInfoTextItemView.GetAgreementTableInfoTextItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion AgreementTableInfoTextItemView
		#region ToDropDown
		public static SelectItem<AgreementTableInfoTextItemView> ToDropDownItem(this AgreementTableInfoTextItemView agreementTableInfoTextView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<AgreementTableInfoTextItemView> selectItem = new SelectItem<AgreementTableInfoTextItemView>();
				selectItem.Label = null;
				selectItem.Value = agreementTableInfoTextView.AgreementTableInfoTextGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: agreementTableInfoTextView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<AgreementTableInfoTextItemView>> ToDropDownItem(this IEnumerable<AgreementTableInfoTextItemView> agreementTableInfoTextItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<AgreementTableInfoTextItemView>> selectItems = new List<SelectItem<AgreementTableInfoTextItemView>>();
				foreach (AgreementTableInfoTextItemView item in agreementTableInfoTextItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

