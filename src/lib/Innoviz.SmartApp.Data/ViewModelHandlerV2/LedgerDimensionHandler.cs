using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class LedgerDimensionHandler
	{
		#region LedgerDimensionListView
		public static List<LedgerDimensionListView> GetLedgerDimensionListViewValidation(this List<LedgerDimensionListView> ledgerDimensionListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return ledgerDimensionListViews;
			}
			var result = new List<LedgerDimensionListView>();
			try
			{
				foreach (LedgerDimensionListView item in ledgerDimensionListViews)
				{
					result.Add(item.GetLedgerDimensionListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static LedgerDimensionListView GetLedgerDimensionListViewValidation(this LedgerDimensionListView ledgerDimensionListView)
		{
			try
			{
				ledgerDimensionListView.RowAuthorize = ledgerDimensionListView.GetListRowAuthorize();
				return ledgerDimensionListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this LedgerDimensionListView ledgerDimensionListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion LedgerDimensionListView
		#region LedgerDimensionItemView
		public static LedgerDimensionItemView GetLedgerDimensionItemViewValidation(this LedgerDimensionItemView ledgerDimensionItemView)
		{
			try
			{
				ledgerDimensionItemView.RowAuthorize = ledgerDimensionItemView.GetItemRowAuthorize();
				return ledgerDimensionItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this LedgerDimensionItemView ledgerDimensionItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static LedgerDimension ToLedgerDimension(this LedgerDimensionItemView ledgerDimensionItemView)
		{
			try
			{
				LedgerDimension ledgerDimension = new LedgerDimension();
				ledgerDimension.CompanyGUID = ledgerDimensionItemView.CompanyGUID.StringToGuid();
				ledgerDimension.CreatedBy = ledgerDimensionItemView.CreatedBy;
				ledgerDimension.CreatedDateTime = ledgerDimensionItemView.CreatedDateTime.StringToSystemDateTime();
				ledgerDimension.ModifiedBy = ledgerDimensionItemView.ModifiedBy;
				ledgerDimension.ModifiedDateTime = ledgerDimensionItemView.ModifiedDateTime.StringToSystemDateTime();
				ledgerDimension.Owner = ledgerDimensionItemView.Owner;
				ledgerDimension.OwnerBusinessUnitGUID = ledgerDimensionItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				ledgerDimension.LedgerDimensionGUID = ledgerDimensionItemView.LedgerDimensionGUID.StringToGuid();
				ledgerDimension.Description = ledgerDimensionItemView.Description;
				ledgerDimension.Dimension = ledgerDimensionItemView.Dimension;
				ledgerDimension.DimensionCode = ledgerDimensionItemView.DimensionCode;
				
				ledgerDimension.RowVersion = ledgerDimensionItemView.RowVersion;
				return ledgerDimension;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<LedgerDimension> ToLedgerDimension(this IEnumerable<LedgerDimensionItemView> ledgerDimensionItemViews)
		{
			try
			{
				List<LedgerDimension> ledgerDimensions = new List<LedgerDimension>();
				foreach (LedgerDimensionItemView item in ledgerDimensionItemViews)
				{
					ledgerDimensions.Add(item.ToLedgerDimension());
				}
				return ledgerDimensions;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static LedgerDimensionItemView ToLedgerDimensionItemView(this LedgerDimension ledgerDimension)
		{
			try
			{
				LedgerDimensionItemView ledgerDimensionItemView = new LedgerDimensionItemView();
				ledgerDimensionItemView.CompanyGUID = ledgerDimension.CompanyGUID.GuidNullToString();
				ledgerDimensionItemView.CreatedBy = ledgerDimension.CreatedBy;
				ledgerDimensionItemView.CreatedDateTime = ledgerDimension.CreatedDateTime.DateTimeToString();
				ledgerDimensionItemView.ModifiedBy = ledgerDimension.ModifiedBy;
				ledgerDimensionItemView.ModifiedDateTime = ledgerDimension.ModifiedDateTime.DateTimeToString();
				ledgerDimensionItemView.Owner = ledgerDimension.Owner;
				ledgerDimensionItemView.OwnerBusinessUnitGUID = ledgerDimension.OwnerBusinessUnitGUID.GuidNullToString();
				ledgerDimensionItemView.LedgerDimensionGUID = ledgerDimension.LedgerDimensionGUID.GuidNullToString();
				ledgerDimensionItemView.Description = ledgerDimension.Description;
				ledgerDimensionItemView.Dimension = ledgerDimension.Dimension;
				ledgerDimensionItemView.DimensionCode = ledgerDimension.DimensionCode;
				
				ledgerDimensionItemView.RowVersion = ledgerDimension.RowVersion;
				return ledgerDimensionItemView.GetLedgerDimensionItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion LedgerDimensionItemView
		#region ToDropDown
		public static SelectItem<LedgerDimensionItemView> ToDropDownItem(this LedgerDimensionItemView ledgerDimensionView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<LedgerDimensionItemView> selectItem = new SelectItem<LedgerDimensionItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(ledgerDimensionView.DimensionCode, ledgerDimensionView.Description);
				selectItem.Value = ledgerDimensionView.LedgerDimensionGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: ledgerDimensionView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<LedgerDimensionItemView>> ToDropDownItem(this IEnumerable<LedgerDimensionItemView> ledgerDimensionItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<LedgerDimensionItemView>> selectItems = new List<SelectItem<LedgerDimensionItemView>>();
				foreach (LedgerDimensionItemView item in ledgerDimensionItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

