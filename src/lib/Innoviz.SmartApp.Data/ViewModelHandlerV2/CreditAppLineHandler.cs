using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CreditAppLineHandler
	{
		#region CreditAppLineListView
		public static List<CreditAppLineListView> GetCreditAppLineListViewValidation(this List<CreditAppLineListView> creditAppLineListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return creditAppLineListViews;
			}
			var result = new List<CreditAppLineListView>();
			try
			{
				foreach (CreditAppLineListView item in creditAppLineListViews)
				{
					result.Add(item.GetCreditAppLineListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CreditAppLineListView GetCreditAppLineListViewValidation(this CreditAppLineListView creditAppLineListView)
		{
			try
			{
				creditAppLineListView.RowAuthorize = creditAppLineListView.GetListRowAuthorize();
				return creditAppLineListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CreditAppLineListView creditAppLineListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CreditAppLineListView
		#region CreditAppLineItemView
		public static CreditAppLineItemView GetCreditAppLineItemViewValidation(this CreditAppLineItemView creditAppLineItemView)
		{
			try
			{
				creditAppLineItemView.RowAuthorize = creditAppLineItemView.GetItemRowAuthorize();
				return creditAppLineItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CreditAppLineItemView creditAppLineItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CreditAppLine ToCreditAppLine(this CreditAppLineItemView creditAppLineItemView)
		{
			try
			{
				CreditAppLine creditAppLine = new CreditAppLine();
				creditAppLine.CompanyGUID = creditAppLineItemView.CompanyGUID.StringToGuid();
				creditAppLine.CreatedBy = creditAppLineItemView.CreatedBy;
				creditAppLine.CreatedDateTime = creditAppLineItemView.CreatedDateTime.StringToSystemDateTime();
				creditAppLine.ModifiedBy = creditAppLineItemView.ModifiedBy;
				creditAppLine.ModifiedDateTime = creditAppLineItemView.ModifiedDateTime.StringToSystemDateTime();
				creditAppLine.Owner = creditAppLineItemView.Owner;
				creditAppLine.OwnerBusinessUnitGUID = creditAppLineItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				creditAppLine.CreditAppLineGUID = creditAppLineItemView.CreditAppLineGUID.StringToGuid();
				creditAppLine.RefCreditAppRequestLineGUID = creditAppLineItemView.RefCreditAppRequestLineGUID.StringToGuidNull();
				creditAppLine.AcceptanceDocument = creditAppLineItemView.AcceptanceDocument;
				creditAppLine.AcceptanceDocumentDescription = creditAppLineItemView.AcceptanceDocumentDescription;
				creditAppLine.ApprovedCreditLimitLine = creditAppLineItemView.ApprovedCreditLimitLine;
				creditAppLine.AssignmentAgreementTableGUID = creditAppLineItemView.AssignmentAgreementTableGUID.StringToGuidNull();
				creditAppLine.AssignmentMethodGUID = creditAppLineItemView.AssignmentMethodGUID.StringToGuidNull();
				creditAppLine.AssignmentMethodRemark = creditAppLineItemView.AssignmentMethodRemark;
				creditAppLine.BillingAddressGUID = creditAppLineItemView.BillingAddressGUID.StringToGuidNull();
				creditAppLine.BillingContactPersonGUID = creditAppLineItemView.BillingContactPersonGUID.StringToGuidNull();
				creditAppLine.BillingDay = creditAppLineItemView.BillingDay;
				creditAppLine.BillingDescription = creditAppLineItemView.BillingDescription;
				creditAppLine.BillingRemark = creditAppLineItemView.BillingRemark;
				creditAppLine.BillingResponsibleByGUID = creditAppLineItemView.BillingResponsibleByGUID.StringToGuidNull();
				creditAppLine.BuyerTableGUID = creditAppLineItemView.BuyerTableGUID.StringToGuid();
				creditAppLine.CreditAppTableGUID = creditAppLineItemView.CreditAppTableGUID.StringToGuid();
				creditAppLine.CreditTermDescription = creditAppLineItemView.CreditTermDescription;
				creditAppLine.CreditTermGUID = creditAppLineItemView.CreditTermGUID.StringToGuidNull();
				creditAppLine.ExpiryDate = creditAppLineItemView.ExpiryDate.StringToDate();
				creditAppLine.InsuranceCreditLimit = creditAppLineItemView.InsuranceCreditLimit;
				creditAppLine.InvoiceAddressGUID = creditAppLineItemView.InvoiceAddressGUID.StringToGuidNull();
				creditAppLine.LineNum = creditAppLineItemView.LineNum;
				creditAppLine.MailingReceiptAddressGUID = creditAppLineItemView.MailingReceiptAddressGUID.StringToGuidNull();
				creditAppLine.MaxPurchasePct = creditAppLineItemView.MaxPurchasePct;
				creditAppLine.MethodOfBilling = creditAppLineItemView.MethodOfBilling;
				creditAppLine.MethodOfPaymentGUID = creditAppLineItemView.MethodOfPaymentGUID.StringToGuidNull();
				creditAppLine.PaymentCondition = creditAppLineItemView.PaymentCondition;
				creditAppLine.PurchaseFeeCalculateBase = creditAppLineItemView.PurchaseFeeCalculateBase;
				creditAppLine.PurchaseFeePct = creditAppLineItemView.PurchaseFeePct;
				creditAppLine.ReceiptAddressGUID = creditAppLineItemView.ReceiptAddressGUID.StringToGuidNull();
				creditAppLine.ReceiptContactPersonGUID = creditAppLineItemView.ReceiptContactPersonGUID.StringToGuidNull();
				creditAppLine.ReceiptDay = creditAppLineItemView.ReceiptDay;
				creditAppLine.ReceiptDescription = creditAppLineItemView.ReceiptDescription;
				creditAppLine.ReceiptRemark = creditAppLineItemView.ReceiptRemark;
				creditAppLine.ReviewDate = creditAppLineItemView.ReviewDate.StringNullToDateNull();
				creditAppLine.LineCondition = creditAppLineItemView.LineCondition;
				
				creditAppLine.RowVersion = creditAppLineItemView.RowVersion;
				return creditAppLine;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CreditAppLine> ToCreditAppLine(this IEnumerable<CreditAppLineItemView> creditAppLineItemViews)
		{
			try
			{
				List<CreditAppLine> creditAppLines = new List<CreditAppLine>();
				foreach (CreditAppLineItemView item in creditAppLineItemViews)
				{
					creditAppLines.Add(item.ToCreditAppLine());
				}
				return creditAppLines;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CreditAppLineItemView ToCreditAppLineItemView(this CreditAppLine creditAppLine)
		{
			try
			{
				CreditAppLineItemView creditAppLineItemView = new CreditAppLineItemView();
				creditAppLineItemView.CompanyGUID = creditAppLine.CompanyGUID.GuidNullToString();
				creditAppLineItemView.CreatedBy = creditAppLine.CreatedBy;
				creditAppLineItemView.CreatedDateTime = creditAppLine.CreatedDateTime.DateTimeToString();
				creditAppLineItemView.ModifiedBy = creditAppLine.ModifiedBy;
				creditAppLineItemView.ModifiedDateTime = creditAppLine.ModifiedDateTime.DateTimeToString();
				creditAppLineItemView.Owner = creditAppLine.Owner;
				creditAppLineItemView.OwnerBusinessUnitGUID = creditAppLine.OwnerBusinessUnitGUID.GuidNullToString();
				creditAppLineItemView.CreditAppLineGUID = creditAppLine.CreditAppLineGUID.GuidNullToString();
				creditAppLineItemView.RefCreditAppRequestLineGUID = creditAppLine.RefCreditAppRequestLineGUID.GuidNullToString();
				creditAppLineItemView.AcceptanceDocument = creditAppLine.AcceptanceDocument;
				creditAppLineItemView.AcceptanceDocumentDescription = creditAppLine.AcceptanceDocumentDescription;
				creditAppLineItemView.ApprovedCreditLimitLine = creditAppLine.ApprovedCreditLimitLine;
				creditAppLineItemView.AssignmentAgreementTableGUID = creditAppLine.AssignmentAgreementTableGUID.GuidNullToString();
				creditAppLineItemView.AssignmentMethodGUID = creditAppLine.AssignmentMethodGUID.GuidNullToString();
				creditAppLineItemView.AssignmentMethodRemark = creditAppLine.AssignmentMethodRemark;
				creditAppLineItemView.BillingAddressGUID = creditAppLine.BillingAddressGUID.GuidNullToString();
				creditAppLineItemView.BillingContactPersonGUID = creditAppLine.BillingContactPersonGUID.GuidNullToString();
				creditAppLineItemView.BillingDay = creditAppLine.BillingDay;
				creditAppLineItemView.BillingDescription = creditAppLine.BillingDescription;
				creditAppLineItemView.BillingRemark = creditAppLine.BillingRemark;
				creditAppLineItemView.BillingResponsibleByGUID = creditAppLine.BillingResponsibleByGUID.GuidNullToString();
				creditAppLineItemView.BuyerTableGUID = creditAppLine.BuyerTableGUID.GuidNullToString();
				creditAppLineItemView.CreditAppTableGUID = creditAppLine.CreditAppTableGUID.GuidNullToString();
				creditAppLineItemView.CreditTermDescription = creditAppLine.CreditTermDescription;
				creditAppLineItemView.CreditTermGUID = creditAppLine.CreditTermGUID.GuidNullToString();
				creditAppLineItemView.ExpiryDate = creditAppLine.ExpiryDate.DateToString();
				creditAppLineItemView.InsuranceCreditLimit = creditAppLine.InsuranceCreditLimit;
				creditAppLineItemView.InvoiceAddressGUID = creditAppLine.InvoiceAddressGUID.GuidNullToString();
				creditAppLineItemView.LineNum = creditAppLine.LineNum;
				creditAppLineItemView.MailingReceiptAddressGUID = creditAppLine.MailingReceiptAddressGUID.GuidNullToString();
				creditAppLineItemView.MaxPurchasePct = creditAppLine.MaxPurchasePct;
				creditAppLineItemView.MethodOfBilling = creditAppLine.MethodOfBilling;
				creditAppLineItemView.MethodOfPaymentGUID = creditAppLine.MethodOfPaymentGUID.GuidNullToString();
				creditAppLineItemView.PaymentCondition = creditAppLine.PaymentCondition;
				creditAppLineItemView.PurchaseFeeCalculateBase = creditAppLine.PurchaseFeeCalculateBase;
				creditAppLineItemView.PurchaseFeePct = creditAppLine.PurchaseFeePct;
				creditAppLineItemView.ReceiptAddressGUID = creditAppLine.ReceiptAddressGUID.GuidNullToString();
				creditAppLineItemView.ReceiptContactPersonGUID = creditAppLine.ReceiptContactPersonGUID.GuidNullToString();
				creditAppLineItemView.ReceiptDay = creditAppLine.ReceiptDay;
				creditAppLineItemView.ReceiptDescription = creditAppLine.ReceiptDescription;
				creditAppLineItemView.ReceiptRemark = creditAppLine.ReceiptRemark;
				creditAppLineItemView.ReviewDate = creditAppLine.ReviewDate.DateNullToString();
				creditAppLineItemView.LineCondition = creditAppLine.LineCondition;
				
				creditAppLineItemView.RowVersion = creditAppLine.RowVersion;
				return creditAppLineItemView.GetCreditAppLineItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CreditAppLineItemView
		#region ToDropDown
		public static SelectItem<CreditAppLineItemView> ToDropDownItem(this CreditAppLineItemView creditAppLineView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CreditAppLineItemView> selectItem = new SelectItem<CreditAppLineItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(creditAppLineView.LineNum.ToString(), creditAppLineView.ApprovedCreditLimitLine.ToString());
				selectItem.Value = creditAppLineView.CreditAppLineGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: creditAppLineView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CreditAppLineItemView>> ToDropDownItem(this IEnumerable<CreditAppLineItemView> creditAppLineItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CreditAppLineItemView>> selectItems = new List<SelectItem<CreditAppLineItemView>>();
				foreach (CreditAppLineItemView item in creditAppLineItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

