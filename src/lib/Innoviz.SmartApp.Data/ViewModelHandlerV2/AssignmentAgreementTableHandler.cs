using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class AssignmentAgreementTableHandler
	{
		#region AssignmentAgreementTableListView
		public static List<AssignmentAgreementTableListView> GetAssignmentAgreementTableListViewValidation(this List<AssignmentAgreementTableListView> assignmentAgreementTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return assignmentAgreementTableListViews;
			}
			var result = new List<AssignmentAgreementTableListView>();
			try
			{
				foreach (AssignmentAgreementTableListView item in assignmentAgreementTableListViews)
				{
					result.Add(item.GetAssignmentAgreementTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AssignmentAgreementTableListView GetAssignmentAgreementTableListViewValidation(this AssignmentAgreementTableListView assignmentAgreementTableListView)
		{
			try
			{
				assignmentAgreementTableListView.RowAuthorize = assignmentAgreementTableListView.GetListRowAuthorize();
				return assignmentAgreementTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this AssignmentAgreementTableListView assignmentAgreementTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion AssignmentAgreementTableListView
		#region AssignmentAgreementTableItemView
		public static AssignmentAgreementTableItemView GetAssignmentAgreementTableItemViewValidation(this AssignmentAgreementTableItemView assignmentAgreementTableItemView)
		{
			try
			{
				assignmentAgreementTableItemView.RowAuthorize = assignmentAgreementTableItemView.GetItemRowAuthorize();
				return assignmentAgreementTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this AssignmentAgreementTableItemView assignmentAgreementTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static AssignmentAgreementTable ToAssignmentAgreementTable(this AssignmentAgreementTableItemView assignmentAgreementTableItemView)
		{
			try
			{
				AssignmentAgreementTable assignmentAgreementTable = new AssignmentAgreementTable();
				assignmentAgreementTable.CompanyGUID = assignmentAgreementTableItemView.CompanyGUID.StringToGuid();
				assignmentAgreementTable.CreatedBy = assignmentAgreementTableItemView.CreatedBy;
				assignmentAgreementTable.CreatedDateTime = assignmentAgreementTableItemView.CreatedDateTime.StringToSystemDateTime();
				assignmentAgreementTable.ModifiedBy = assignmentAgreementTableItemView.ModifiedBy;
				assignmentAgreementTable.ModifiedDateTime = assignmentAgreementTableItemView.ModifiedDateTime.StringToSystemDateTime();
				assignmentAgreementTable.Owner = assignmentAgreementTableItemView.Owner;
				assignmentAgreementTable.OwnerBusinessUnitGUID = assignmentAgreementTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				assignmentAgreementTable.AssignmentAgreementTableGUID = assignmentAgreementTableItemView.AssignmentAgreementTableGUID.StringToGuid();
				assignmentAgreementTable.AcceptanceName = assignmentAgreementTableItemView.AcceptanceName;
				assignmentAgreementTable.AgreementDate = assignmentAgreementTableItemView.AgreementDate.StringToDate();
				assignmentAgreementTable.AgreementDocType = assignmentAgreementTableItemView.AgreementDocType;
				assignmentAgreementTable.AssignmentAgreementAmount = assignmentAgreementTableItemView.AssignmentAgreementAmount;
				assignmentAgreementTable.AssignmentAgreementId = assignmentAgreementTableItemView.AssignmentAgreementId;
				assignmentAgreementTable.AssignmentMethodGUID = assignmentAgreementTableItemView.AssignmentMethodGUID.StringToGuid();
				assignmentAgreementTable.AssignmentProductDescription = assignmentAgreementTableItemView.AssignmentProductDescription;
				assignmentAgreementTable.BuyerName = assignmentAgreementTableItemView.BuyerName;
				assignmentAgreementTable.BuyerTableGUID = assignmentAgreementTableItemView.BuyerTableGUID.StringToGuid();
				assignmentAgreementTable.CancelAuthorityPersonAddress = assignmentAgreementTableItemView.CancelAuthorityPersonAddress;
				assignmentAgreementTable.CancelAuthorityPersonID = assignmentAgreementTableItemView.CancelAuthorityPersonID;
				assignmentAgreementTable.CancelAuthorityPersonName = assignmentAgreementTableItemView.CancelAuthorityPersonName;
				assignmentAgreementTable.ConsortiumTableGUID = assignmentAgreementTableItemView.ConsortiumTableGUID.StringToGuidNull();
				assignmentAgreementTable.CustBankGUID = assignmentAgreementTableItemView.CustBankGUID.StringToGuidNull();
				assignmentAgreementTable.CustomerAltName = assignmentAgreementTableItemView.CustomerAltName;
				assignmentAgreementTable.CustomerName = assignmentAgreementTableItemView.CustomerName;
				assignmentAgreementTable.CustomerTableGUID = assignmentAgreementTableItemView.CustomerTableGUID.StringToGuid();
				assignmentAgreementTable.CustRegisteredLocation = assignmentAgreementTableItemView.CustRegisteredLocation;
				assignmentAgreementTable.Description = assignmentAgreementTableItemView.Description;
				assignmentAgreementTable.DocumentReasonGUID = assignmentAgreementTableItemView.DocumentReasonGUID.StringToGuidNull();
				assignmentAgreementTable.DocumentStatusGUID = assignmentAgreementTableItemView.DocumentStatusGUID.StringToGuidNull();
				assignmentAgreementTable.InternalAssignmentAgreementId = assignmentAgreementTableItemView.InternalAssignmentAgreementId;
				assignmentAgreementTable.RefAssignmentAgreementTableGUID = assignmentAgreementTableItemView.RefAssignmentAgreementTableGUID.StringToGuidNull();
				assignmentAgreementTable.ReferenceAgreementId = assignmentAgreementTableItemView.ReferenceAgreementId;
				assignmentAgreementTable.Remark = assignmentAgreementTableItemView.Remark;
				assignmentAgreementTable.SigningDate = assignmentAgreementTableItemView.SigningDate.StringNullToDateNull();
				assignmentAgreementTable.ToWhomConcern = assignmentAgreementTableItemView.ToWhomConcern;
				assignmentAgreementTable.CreditAppReqAssignmentGUID = assignmentAgreementTableItemView.CreditAppReqAssignmentGUID.StringToGuidNull();
				assignmentAgreementTable.RefCreditAppRequestTableGUID = assignmentAgreementTableItemView.RefCreditAppRequestTableGUID.StringToGuidNull();
				
				assignmentAgreementTable.RowVersion = assignmentAgreementTableItemView.RowVersion;
				return assignmentAgreementTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<AssignmentAgreementTable> ToAssignmentAgreementTable(this IEnumerable<AssignmentAgreementTableItemView> assignmentAgreementTableItemViews)
		{
			try
			{
				List<AssignmentAgreementTable> assignmentAgreementTables = new List<AssignmentAgreementTable>();
				foreach (AssignmentAgreementTableItemView item in assignmentAgreementTableItemViews)
				{
					assignmentAgreementTables.Add(item.ToAssignmentAgreementTable());
				}
				return assignmentAgreementTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AssignmentAgreementTableItemView ToAssignmentAgreementTableItemView(this AssignmentAgreementTable assignmentAgreementTable)
		{
			try
			{
				AssignmentAgreementTableItemView assignmentAgreementTableItemView = new AssignmentAgreementTableItemView();
				assignmentAgreementTableItemView.CompanyGUID = assignmentAgreementTable.CompanyGUID.GuidNullToString();
				assignmentAgreementTableItemView.CreatedBy = assignmentAgreementTable.CreatedBy;
				assignmentAgreementTableItemView.CreatedDateTime = assignmentAgreementTable.CreatedDateTime.DateTimeToString();
				assignmentAgreementTableItemView.ModifiedBy = assignmentAgreementTable.ModifiedBy;
				assignmentAgreementTableItemView.ModifiedDateTime = assignmentAgreementTable.ModifiedDateTime.DateTimeToString();
				assignmentAgreementTableItemView.Owner = assignmentAgreementTable.Owner;
				assignmentAgreementTableItemView.OwnerBusinessUnitGUID = assignmentAgreementTable.OwnerBusinessUnitGUID.GuidNullToString();
				assignmentAgreementTableItemView.AssignmentAgreementTableGUID = assignmentAgreementTable.AssignmentAgreementTableGUID.GuidNullToString();
				assignmentAgreementTableItemView.AcceptanceName = assignmentAgreementTable.AcceptanceName;
				assignmentAgreementTableItemView.AgreementDate = assignmentAgreementTable.AgreementDate.DateToString();
				assignmentAgreementTableItemView.AgreementDocType = assignmentAgreementTable.AgreementDocType;
				assignmentAgreementTableItemView.AssignmentAgreementAmount = assignmentAgreementTable.AssignmentAgreementAmount;
				assignmentAgreementTableItemView.AssignmentAgreementId = assignmentAgreementTable.AssignmentAgreementId;
				assignmentAgreementTableItemView.AssignmentMethodGUID = assignmentAgreementTable.AssignmentMethodGUID.GuidNullToString();
				assignmentAgreementTableItemView.AssignmentProductDescription = assignmentAgreementTable.AssignmentProductDescription;
				assignmentAgreementTableItemView.BuyerName = assignmentAgreementTable.BuyerName;
				assignmentAgreementTableItemView.BuyerTableGUID = assignmentAgreementTable.BuyerTableGUID.GuidNullToString();
				assignmentAgreementTableItemView.CancelAuthorityPersonAddress = assignmentAgreementTable.CancelAuthorityPersonAddress;
				assignmentAgreementTableItemView.CancelAuthorityPersonID = assignmentAgreementTable.CancelAuthorityPersonID;
				assignmentAgreementTableItemView.CancelAuthorityPersonName = assignmentAgreementTable.CancelAuthorityPersonName;
				assignmentAgreementTableItemView.ConsortiumTableGUID = assignmentAgreementTable.ConsortiumTableGUID.GuidNullToString();
				assignmentAgreementTableItemView.CustBankGUID = assignmentAgreementTable.CustBankGUID.GuidNullToString();
				assignmentAgreementTableItemView.CustomerAltName = assignmentAgreementTable.CustomerAltName;
				assignmentAgreementTableItemView.CustomerName = assignmentAgreementTable.CustomerName;
				assignmentAgreementTableItemView.CustomerTableGUID = assignmentAgreementTable.CustomerTableGUID.GuidNullToString();
				assignmentAgreementTableItemView.CustRegisteredLocation = assignmentAgreementTable.CustRegisteredLocation;
				assignmentAgreementTableItemView.Description = assignmentAgreementTable.Description;
				assignmentAgreementTableItemView.DocumentReasonGUID = assignmentAgreementTable.DocumentReasonGUID.GuidNullToString();
				assignmentAgreementTableItemView.DocumentStatusGUID = assignmentAgreementTable.DocumentStatusGUID.GuidNullToString();
				assignmentAgreementTableItemView.InternalAssignmentAgreementId = assignmentAgreementTable.InternalAssignmentAgreementId;
				assignmentAgreementTableItemView.RefAssignmentAgreementTableGUID = assignmentAgreementTable.RefAssignmentAgreementTableGUID.GuidNullToString();
				assignmentAgreementTableItemView.ReferenceAgreementId = assignmentAgreementTable.ReferenceAgreementId;
				assignmentAgreementTableItemView.Remark = assignmentAgreementTable.Remark;
				assignmentAgreementTableItemView.SigningDate = assignmentAgreementTable.SigningDate.DateNullToString();
				assignmentAgreementTableItemView.ToWhomConcern = assignmentAgreementTable.ToWhomConcern;
				assignmentAgreementTableItemView.CreditAppReqAssignmentGUID = assignmentAgreementTable.CreditAppReqAssignmentGUID.GuidNullToString();
				assignmentAgreementTableItemView.RefCreditAppRequestTableGUID = assignmentAgreementTable.RefCreditAppRequestTableGUID.GuidNullToString();
				
				assignmentAgreementTableItemView.RowVersion = assignmentAgreementTable.RowVersion;
				return assignmentAgreementTableItemView.GetAssignmentAgreementTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion AssignmentAgreementTableItemView
		#region ToDropDown
		public static SelectItem<AssignmentAgreementTableItemView> ToDropDownItem(this AssignmentAgreementTableItemView assignmentAgreementTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<AssignmentAgreementTableItemView> selectItem = new SelectItem<AssignmentAgreementTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(assignmentAgreementTableView.InternalAssignmentAgreementId, assignmentAgreementTableView.Description);
				selectItem.Value = assignmentAgreementTableView.AssignmentAgreementTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: assignmentAgreementTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<AssignmentAgreementTableItemView>> ToDropDownItem(this IEnumerable<AssignmentAgreementTableItemView> assignmentAgreementTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<AssignmentAgreementTableItemView>> selectItems = new List<SelectItem<AssignmentAgreementTableItemView>>();
				foreach (AssignmentAgreementTableItemView item in assignmentAgreementTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

