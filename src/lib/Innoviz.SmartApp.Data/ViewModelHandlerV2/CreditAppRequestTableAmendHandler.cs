using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CreditAppRequestTableAmendHandler
	{
		#region CreditAppRequestTableAmendListView
		public static List<CreditAppRequestTableAmendListView> GetCreditAppRequestTableAmendListViewValidation(this List<CreditAppRequestTableAmendListView> creditAppRequestTableAmendListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return creditAppRequestTableAmendListViews;
			}
			var result = new List<CreditAppRequestTableAmendListView>();
			try
			{
				foreach (CreditAppRequestTableAmendListView item in creditAppRequestTableAmendListViews)
				{
					result.Add(item.GetCreditAppRequestTableAmendListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CreditAppRequestTableAmendListView GetCreditAppRequestTableAmendListViewValidation(this CreditAppRequestTableAmendListView creditAppRequestTableAmendListView)
		{
			try
			{
				creditAppRequestTableAmendListView.RowAuthorize = creditAppRequestTableAmendListView.GetListRowAuthorize();
				return creditAppRequestTableAmendListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CreditAppRequestTableAmendListView creditAppRequestTableAmendListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CreditAppRequestTableAmendListView
		#region CreditAppRequestTableAmendItemView
		public static CreditAppRequestTableAmendItemView GetCreditAppRequestTableAmendItemViewValidation(this CreditAppRequestTableAmendItemView creditAppRequestTableAmendItemView)
		{
			try
			{
				creditAppRequestTableAmendItemView.RowAuthorize = creditAppRequestTableAmendItemView.GetItemRowAuthorize();
				return creditAppRequestTableAmendItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CreditAppRequestTableAmendItemView creditAppRequestTableAmendItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CreditAppRequestTableAmend ToCreditAppRequestTableAmend(this CreditAppRequestTableAmendItemView creditAppRequestTableAmendItemView)
		{
			try
			{
				CreditAppRequestTableAmend creditAppRequestTableAmend = new CreditAppRequestTableAmend();
				creditAppRequestTableAmend.CompanyGUID = creditAppRequestTableAmendItemView.CompanyGUID.StringToGuid();
				creditAppRequestTableAmend.CreatedBy = creditAppRequestTableAmendItemView.CreatedBy;
				creditAppRequestTableAmend.CreatedDateTime = creditAppRequestTableAmendItemView.CreatedDateTime.StringToSystemDateTime();
				creditAppRequestTableAmend.ModifiedBy = creditAppRequestTableAmendItemView.ModifiedBy;
				creditAppRequestTableAmend.ModifiedDateTime = creditAppRequestTableAmendItemView.ModifiedDateTime.StringToSystemDateTime();
				creditAppRequestTableAmend.Owner = creditAppRequestTableAmendItemView.Owner;
				creditAppRequestTableAmend.OwnerBusinessUnitGUID = creditAppRequestTableAmendItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				creditAppRequestTableAmend.CreditAppRequestTableAmendGUID = creditAppRequestTableAmendItemView.CreditAppRequestTableAmendGUID.StringToGuid();
				creditAppRequestTableAmend.AmendAuthorizedPerson = creditAppRequestTableAmendItemView.AmendAuthorizedPerson;
				creditAppRequestTableAmend.AmendGuarantor = creditAppRequestTableAmendItemView.AmendGuarantor;
				creditAppRequestTableAmend.AmendRate = creditAppRequestTableAmendItemView.AmendRate;
				creditAppRequestTableAmend.CreditAppRequestTableGUID = creditAppRequestTableAmendItemView.CreditAppRequestTableGUID.StringToGuid();
				creditAppRequestTableAmend.OriginalCreditLimit = creditAppRequestTableAmendItemView.OriginalCreditLimit;
				creditAppRequestTableAmend.OriginalCreditLimitRequestFeeAmount = creditAppRequestTableAmendItemView.OriginalCreditLimitRequestFeeAmount;
				creditAppRequestTableAmend.OriginalInterestAdjustmentPct = creditAppRequestTableAmendItemView.OriginalInterestAdjustmentPct;
				creditAppRequestTableAmend.OriginalInterestTypeGUID = creditAppRequestTableAmendItemView.OriginalInterestTypeGUID.StringToGuidNull();
				creditAppRequestTableAmend.OriginalMaxPurchasePct = creditAppRequestTableAmendItemView.OriginalMaxPurchasePct;
				creditAppRequestTableAmend.OriginalMaxRetentionAmount = creditAppRequestTableAmendItemView.OriginalMaxRetentionAmount;
				creditAppRequestTableAmend.OriginalMaxRetentionPct = creditAppRequestTableAmendItemView.OriginalMaxRetentionPct;
				creditAppRequestTableAmend.OriginalPurchaseFeeCalculateBase = creditAppRequestTableAmendItemView.OriginalPurchaseFeeCalculateBase;
				creditAppRequestTableAmend.OriginalPurchaseFeePct = creditAppRequestTableAmendItemView.OriginalPurchaseFeePct;
				creditAppRequestTableAmend.OriginalTotalInterestPct = creditAppRequestTableAmendItemView.OriginalTotalInterestPct;
				creditAppRequestTableAmend.CNReasonGUID = creditAppRequestTableAmendItemView.CNReasonGUID.StringToGuidNull();
				creditAppRequestTableAmend.OrigInvoiceAmount = creditAppRequestTableAmendItemView.OrigInvoiceAmount;
				creditAppRequestTableAmend.OrigInvoiceId = creditAppRequestTableAmendItemView.OrigInvoiceId;
				creditAppRequestTableAmend.OrigTaxInvoiceAmount = creditAppRequestTableAmendItemView.OrigTaxInvoiceAmount;
				creditAppRequestTableAmend.OrigTaxInvoiceId = creditAppRequestTableAmendItemView.OrigTaxInvoiceId;

				
				creditAppRequestTableAmend.RowVersion = creditAppRequestTableAmendItemView.RowVersion;
				return creditAppRequestTableAmend;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CreditAppRequestTableAmend> ToCreditAppRequestTableAmend(this IEnumerable<CreditAppRequestTableAmendItemView> creditAppRequestTableAmendItemViews)
		{
			try
			{
				List<CreditAppRequestTableAmend> creditAppRequestTableAmends = new List<CreditAppRequestTableAmend>();
				foreach (CreditAppRequestTableAmendItemView item in creditAppRequestTableAmendItemViews)
				{
					creditAppRequestTableAmends.Add(item.ToCreditAppRequestTableAmend());
				}
				return creditAppRequestTableAmends;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CreditAppRequestTableAmendItemView ToCreditAppRequestTableAmendItemView(this CreditAppRequestTableAmend creditAppRequestTableAmend)
		{
			try
			{
				CreditAppRequestTableAmendItemView creditAppRequestTableAmendItemView = new CreditAppRequestTableAmendItemView();
				creditAppRequestTableAmendItemView.CompanyGUID = creditAppRequestTableAmend.CompanyGUID.GuidNullToString();
				creditAppRequestTableAmendItemView.CreatedBy = creditAppRequestTableAmend.CreatedBy;
				creditAppRequestTableAmendItemView.CreatedDateTime = creditAppRequestTableAmend.CreatedDateTime.DateTimeToString();
				creditAppRequestTableAmendItemView.ModifiedBy = creditAppRequestTableAmend.ModifiedBy;
				creditAppRequestTableAmendItemView.ModifiedDateTime = creditAppRequestTableAmend.ModifiedDateTime.DateTimeToString();
				creditAppRequestTableAmendItemView.Owner = creditAppRequestTableAmend.Owner;
				creditAppRequestTableAmendItemView.OwnerBusinessUnitGUID = creditAppRequestTableAmend.OwnerBusinessUnitGUID.GuidNullToString();
				creditAppRequestTableAmendItemView.CreditAppRequestTableAmendGUID = creditAppRequestTableAmend.CreditAppRequestTableAmendGUID.GuidNullToString();
				creditAppRequestTableAmendItemView.AmendAuthorizedPerson = creditAppRequestTableAmend.AmendAuthorizedPerson;
				creditAppRequestTableAmendItemView.AmendGuarantor = creditAppRequestTableAmend.AmendGuarantor;
				creditAppRequestTableAmendItemView.AmendRate = creditAppRequestTableAmend.AmendRate;
				creditAppRequestTableAmendItemView.CreditAppRequestTableGUID = creditAppRequestTableAmend.CreditAppRequestTableGUID.GuidNullToString();
				creditAppRequestTableAmendItemView.OriginalCreditLimit = creditAppRequestTableAmend.OriginalCreditLimit;
				creditAppRequestTableAmendItemView.OriginalCreditLimitRequestFeeAmount = creditAppRequestTableAmend.OriginalCreditLimitRequestFeeAmount;
				creditAppRequestTableAmendItemView.OriginalInterestAdjustmentPct = creditAppRequestTableAmend.OriginalInterestAdjustmentPct;
				creditAppRequestTableAmendItemView.OriginalInterestTypeGUID = creditAppRequestTableAmend.OriginalInterestTypeGUID.GuidNullToString();
				creditAppRequestTableAmendItemView.OriginalMaxPurchasePct = creditAppRequestTableAmend.OriginalMaxPurchasePct;
				creditAppRequestTableAmendItemView.OriginalMaxRetentionAmount = creditAppRequestTableAmend.OriginalMaxRetentionAmount;
				creditAppRequestTableAmendItemView.OriginalMaxRetentionPct = creditAppRequestTableAmend.OriginalMaxRetentionPct;
				creditAppRequestTableAmendItemView.OriginalPurchaseFeeCalculateBase = creditAppRequestTableAmend.OriginalPurchaseFeeCalculateBase;
				creditAppRequestTableAmendItemView.OriginalPurchaseFeePct = creditAppRequestTableAmend.OriginalPurchaseFeePct;
				creditAppRequestTableAmendItemView.OriginalTotalInterestPct = creditAppRequestTableAmend.OriginalTotalInterestPct;
				creditAppRequestTableAmendItemView.CNReasonGUID = creditAppRequestTableAmend.CNReasonGUID.GuidNullToString();
				creditAppRequestTableAmendItemView.OrigInvoiceAmount = creditAppRequestTableAmend.OrigInvoiceAmount;
				creditAppRequestTableAmendItemView.OrigInvoiceId = creditAppRequestTableAmend.OrigInvoiceId;
				creditAppRequestTableAmendItemView.OrigTaxInvoiceAmount = creditAppRequestTableAmend.OrigTaxInvoiceAmount;
				creditAppRequestTableAmendItemView.OrigTaxInvoiceId = creditAppRequestTableAmend.OrigTaxInvoiceId;

				
				creditAppRequestTableAmendItemView.RowVersion = creditAppRequestTableAmend.RowVersion;
				return creditAppRequestTableAmendItemView.GetCreditAppRequestTableAmendItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CreditAppRequestTableAmendItemView
		#region ToDropDown
		public static SelectItem<CreditAppRequestTableAmendItemView> ToDropDownItem(this CreditAppRequestTableAmendItemView creditAppRequestTableAmendView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CreditAppRequestTableAmendItemView> selectItem = new SelectItem<CreditAppRequestTableAmendItemView>();
                selectItem.Label = SmartAppUtil.GetDropDownLabel(creditAppRequestTableAmendView.CreditAppRequestTable_CreditAppRequestId, creditAppRequestTableAmendView.CreditAppRequestTable_Description);
                selectItem.Value = creditAppRequestTableAmendView.CreditAppRequestTableAmendGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: creditAppRequestTableAmendView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CreditAppRequestTableAmendItemView>> ToDropDownItem(this IEnumerable<CreditAppRequestTableAmendItemView> creditAppRequestTableAmendItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CreditAppRequestTableAmendItemView>> selectItems = new List<SelectItem<CreditAppRequestTableAmendItemView>>();
				foreach (CreditAppRequestTableAmendItemView item in creditAppRequestTableAmendItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

