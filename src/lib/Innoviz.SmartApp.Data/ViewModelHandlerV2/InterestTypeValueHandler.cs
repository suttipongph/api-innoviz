using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class InterestTypeValueHandler
	{
		#region InterestTypeValueListView
		public static List<InterestTypeValueListView> GetInterestTypeValueListViewValidation(this List<InterestTypeValueListView> interestTypeValueListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return interestTypeValueListViews;
			}
			var result = new List<InterestTypeValueListView>();
			try
			{
				foreach (InterestTypeValueListView item in interestTypeValueListViews)
				{
					result.Add(item.GetInterestTypeValueListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static InterestTypeValueListView GetInterestTypeValueListViewValidation(this InterestTypeValueListView interestTypeValueListView)
		{
			try
			{
				interestTypeValueListView.RowAuthorize = interestTypeValueListView.GetListRowAuthorize();
				return interestTypeValueListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this InterestTypeValueListView interestTypeValueListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion InterestTypeValueListView
		#region InterestTypeValueItemView
		public static InterestTypeValueItemView GetInterestTypeValueItemViewValidation(this InterestTypeValueItemView interestTypeValueItemView)
		{
			try
			{
				interestTypeValueItemView.RowAuthorize = interestTypeValueItemView.GetItemRowAuthorize();
				return interestTypeValueItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this InterestTypeValueItemView interestTypeValueItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static InterestTypeValue ToInterestTypeValue(this InterestTypeValueItemView interestTypeValueItemView)
		{
			try
			{
				InterestTypeValue interestTypeValue = new InterestTypeValue();
				interestTypeValue.CompanyGUID = interestTypeValueItemView.CompanyGUID.StringToGuid();
				interestTypeValue.CreatedBy = interestTypeValueItemView.CreatedBy;
				interestTypeValue.CreatedDateTime = interestTypeValueItemView.CreatedDateTime.StringToSystemDateTime();
				interestTypeValue.ModifiedBy = interestTypeValueItemView.ModifiedBy;
				interestTypeValue.ModifiedDateTime = interestTypeValueItemView.ModifiedDateTime.StringToSystemDateTime();
				interestTypeValue.Owner = interestTypeValueItemView.Owner;
				interestTypeValue.OwnerBusinessUnitGUID = interestTypeValueItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				interestTypeValue.InterestTypeValueGUID = interestTypeValueItemView.InterestTypeValueGUID.StringToGuid();
				interestTypeValue.EffectiveFrom = interestTypeValueItemView.EffectiveFrom.StringToDate();
				interestTypeValue.EffectiveTo = interestTypeValueItemView.EffectiveTo.StringNullToDateNull();
				interestTypeValue.InterestTypeGUID = interestTypeValueItemView.InterestTypeGUID.StringToGuid();
				interestTypeValue.Value = interestTypeValueItemView.Value;
				
				interestTypeValue.RowVersion = interestTypeValueItemView.RowVersion;
				return interestTypeValue;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<InterestTypeValue> ToInterestTypeValue(this IEnumerable<InterestTypeValueItemView> interestTypeValueItemViews)
		{
			try
			{
				List<InterestTypeValue> interestTypeValues = new List<InterestTypeValue>();
				foreach (InterestTypeValueItemView item in interestTypeValueItemViews)
				{
					interestTypeValues.Add(item.ToInterestTypeValue());
				}
				return interestTypeValues;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static InterestTypeValueItemView ToInterestTypeValueItemView(this InterestTypeValue interestTypeValue)
		{
			try
			{
				InterestTypeValueItemView interestTypeValueItemView = new InterestTypeValueItemView();
				interestTypeValueItemView.CompanyGUID = interestTypeValue.CompanyGUID.GuidNullToString();
				interestTypeValueItemView.CreatedBy = interestTypeValue.CreatedBy;
				interestTypeValueItemView.CreatedDateTime = interestTypeValue.CreatedDateTime.DateTimeToString();
				interestTypeValueItemView.ModifiedBy = interestTypeValue.ModifiedBy;
				interestTypeValueItemView.ModifiedDateTime = interestTypeValue.ModifiedDateTime.DateTimeToString();
				interestTypeValueItemView.Owner = interestTypeValue.Owner;
				interestTypeValueItemView.OwnerBusinessUnitGUID = interestTypeValue.OwnerBusinessUnitGUID.GuidNullToString();
				interestTypeValueItemView.InterestTypeValueGUID = interestTypeValue.InterestTypeValueGUID.GuidNullToString();
				interestTypeValueItemView.EffectiveFrom = interestTypeValue.EffectiveFrom.DateToString();
				interestTypeValueItemView.EffectiveTo = interestTypeValue.EffectiveTo.DateNullToString();
				interestTypeValueItemView.InterestTypeGUID = interestTypeValue.InterestTypeGUID.GuidNullToString();
				interestTypeValueItemView.Value = interestTypeValue.Value;
				
				interestTypeValueItemView.RowVersion = interestTypeValue.RowVersion;
				return interestTypeValueItemView.GetInterestTypeValueItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion InterestTypeValueItemView
		#region ToDropDown
		public static SelectItem<InterestTypeValueItemView> ToDropDownItem(this InterestTypeValueItemView interestTypeValueView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<InterestTypeValueItemView> selectItem = new SelectItem<InterestTypeValueItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(interestTypeValueView.Value.ToString());
				selectItem.Value = interestTypeValueView.InterestTypeValueGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: interestTypeValueView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<InterestTypeValueItemView>> ToDropDownItem(this IEnumerable<InterestTypeValueItemView> interestTypeValueItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<InterestTypeValueItemView>> selectItems = new List<SelectItem<InterestTypeValueItemView>>();
				foreach (InterestTypeValueItemView item in interestTypeValueItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

