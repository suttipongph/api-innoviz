using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class BookmarkDocumentTemplateTableHandler
	{
		#region BookmarkDocumentTemplateTableListView
		public static List<BookmarkDocumentTemplateTableListView> GetBookmarkDocumentTemplateTableListViewValidation(this List<BookmarkDocumentTemplateTableListView> bookmarkDocumentTemplateTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return bookmarkDocumentTemplateTableListViews;
			}
			var result = new List<BookmarkDocumentTemplateTableListView>();
			try
			{
				foreach (BookmarkDocumentTemplateTableListView item in bookmarkDocumentTemplateTableListViews)
				{
					result.Add(item.GetBookmarkDocumentTemplateTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BookmarkDocumentTemplateTableListView GetBookmarkDocumentTemplateTableListViewValidation(this BookmarkDocumentTemplateTableListView bookmarkDocumentTemplateTableListView)
		{
			try
			{
				bookmarkDocumentTemplateTableListView.RowAuthorize = bookmarkDocumentTemplateTableListView.GetListRowAuthorize();
				return bookmarkDocumentTemplateTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this BookmarkDocumentTemplateTableListView bookmarkDocumentTemplateTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion BookmarkDocumentTemplateTableListView
		#region BookmarkDocumentTemplateTableItemView
		public static BookmarkDocumentTemplateTableItemView GetBookmarkDocumentTemplateTableItemViewValidation(this BookmarkDocumentTemplateTableItemView bookmarkDocumentTemplateTableItemView)
		{
			try
			{
				bookmarkDocumentTemplateTableItemView.RowAuthorize = bookmarkDocumentTemplateTableItemView.GetItemRowAuthorize();
				return bookmarkDocumentTemplateTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this BookmarkDocumentTemplateTableItemView bookmarkDocumentTemplateTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static BookmarkDocumentTemplateTable ToBookmarkDocumentTemplateTable(this BookmarkDocumentTemplateTableItemView bookmarkDocumentTemplateTableItemView)
		{
			try
			{
				BookmarkDocumentTemplateTable bookmarkDocumentTemplateTable = new BookmarkDocumentTemplateTable();
				bookmarkDocumentTemplateTable.CompanyGUID = bookmarkDocumentTemplateTableItemView.CompanyGUID.StringToGuid();
				bookmarkDocumentTemplateTable.CreatedBy = bookmarkDocumentTemplateTableItemView.CreatedBy;
				bookmarkDocumentTemplateTable.CreatedDateTime = bookmarkDocumentTemplateTableItemView.CreatedDateTime.StringToSystemDateTime();
				bookmarkDocumentTemplateTable.ModifiedBy = bookmarkDocumentTemplateTableItemView.ModifiedBy;
				bookmarkDocumentTemplateTable.ModifiedDateTime = bookmarkDocumentTemplateTableItemView.ModifiedDateTime.StringToSystemDateTime();
				bookmarkDocumentTemplateTable.Owner = bookmarkDocumentTemplateTableItemView.Owner;
				bookmarkDocumentTemplateTable.OwnerBusinessUnitGUID = bookmarkDocumentTemplateTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				bookmarkDocumentTemplateTable.BookmarkDocumentTemplateTableGUID = bookmarkDocumentTemplateTableItemView.BookmarkDocumentTemplateTableGUID.StringToGuid();
				bookmarkDocumentTemplateTable.BookmarkDocumentRefType = bookmarkDocumentTemplateTableItemView.BookmarkDocumentRefType;
				bookmarkDocumentTemplateTable.BookmarkDocumentTemplateId = bookmarkDocumentTemplateTableItemView.BookmarkDocumentTemplateId;
				bookmarkDocumentTemplateTable.Description = bookmarkDocumentTemplateTableItemView.Description;
				
				bookmarkDocumentTemplateTable.RowVersion = bookmarkDocumentTemplateTableItemView.RowVersion;
				return bookmarkDocumentTemplateTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<BookmarkDocumentTemplateTable> ToBookmarkDocumentTemplateTable(this IEnumerable<BookmarkDocumentTemplateTableItemView> bookmarkDocumentTemplateTableItemViews)
		{
			try
			{
				List<BookmarkDocumentTemplateTable> bookmarkDocumentTemplateTables = new List<BookmarkDocumentTemplateTable>();
				foreach (BookmarkDocumentTemplateTableItemView item in bookmarkDocumentTemplateTableItemViews)
				{
					bookmarkDocumentTemplateTables.Add(item.ToBookmarkDocumentTemplateTable());
				}
				return bookmarkDocumentTemplateTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BookmarkDocumentTemplateTableItemView ToBookmarkDocumentTemplateTableItemView(this BookmarkDocumentTemplateTable bookmarkDocumentTemplateTable)
		{
			try
			{
				BookmarkDocumentTemplateTableItemView bookmarkDocumentTemplateTableItemView = new BookmarkDocumentTemplateTableItemView();
				bookmarkDocumentTemplateTableItemView.CompanyGUID = bookmarkDocumentTemplateTable.CompanyGUID.GuidNullToString();
				bookmarkDocumentTemplateTableItemView.CreatedBy = bookmarkDocumentTemplateTable.CreatedBy;
				bookmarkDocumentTemplateTableItemView.CreatedDateTime = bookmarkDocumentTemplateTable.CreatedDateTime.DateTimeToString();
				bookmarkDocumentTemplateTableItemView.ModifiedBy = bookmarkDocumentTemplateTable.ModifiedBy;
				bookmarkDocumentTemplateTableItemView.ModifiedDateTime = bookmarkDocumentTemplateTable.ModifiedDateTime.DateTimeToString();
				bookmarkDocumentTemplateTableItemView.Owner = bookmarkDocumentTemplateTable.Owner;
				bookmarkDocumentTemplateTableItemView.OwnerBusinessUnitGUID = bookmarkDocumentTemplateTable.OwnerBusinessUnitGUID.GuidNullToString();
				bookmarkDocumentTemplateTableItemView.BookmarkDocumentTemplateTableGUID = bookmarkDocumentTemplateTable.BookmarkDocumentTemplateTableGUID.GuidNullToString();
				bookmarkDocumentTemplateTableItemView.BookmarkDocumentRefType = bookmarkDocumentTemplateTable.BookmarkDocumentRefType;
				bookmarkDocumentTemplateTableItemView.BookmarkDocumentTemplateId = bookmarkDocumentTemplateTable.BookmarkDocumentTemplateId;
				bookmarkDocumentTemplateTableItemView.Description = bookmarkDocumentTemplateTable.Description;
				
				bookmarkDocumentTemplateTableItemView.RowVersion = bookmarkDocumentTemplateTable.RowVersion;
				return bookmarkDocumentTemplateTableItemView.GetBookmarkDocumentTemplateTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion BookmarkDocumentTemplateTableItemView
		#region ToDropDown
		public static SelectItem<BookmarkDocumentTemplateTableItemView> ToDropDownItem(this BookmarkDocumentTemplateTableItemView bookmarkDocumentTemplateTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<BookmarkDocumentTemplateTableItemView> selectItem = new SelectItem<BookmarkDocumentTemplateTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(bookmarkDocumentTemplateTableView.BookmarkDocumentTemplateId, bookmarkDocumentTemplateTableView.Description);
				selectItem.Value = bookmarkDocumentTemplateTableView.BookmarkDocumentTemplateTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: bookmarkDocumentTemplateTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<BookmarkDocumentTemplateTableItemView>> ToDropDownItem(this IEnumerable<BookmarkDocumentTemplateTableItemView> bookmarkDocumentTemplateTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<BookmarkDocumentTemplateTableItemView>> selectItems = new List<SelectItem<BookmarkDocumentTemplateTableItemView>>();
				foreach (BookmarkDocumentTemplateTableItemView item in bookmarkDocumentTemplateTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

