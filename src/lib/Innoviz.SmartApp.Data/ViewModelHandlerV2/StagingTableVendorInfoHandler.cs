using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class StagingTableVendorInfoHandler
	{
		#region StagingTableVendorInfoListView
		public static List<StagingTableVendorInfoListView> GetStagingTableVendorInfoListViewValidation(this List<StagingTableVendorInfoListView> stagingTableVendorInfoListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return stagingTableVendorInfoListViews;
			}
			var result = new List<StagingTableVendorInfoListView>();
			try
			{
				foreach (StagingTableVendorInfoListView item in stagingTableVendorInfoListViews)
				{
					result.Add(item.GetStagingTableVendorInfoListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static StagingTableVendorInfoListView GetStagingTableVendorInfoListViewValidation(this StagingTableVendorInfoListView stagingTableVendorInfoListView)
		{
			try
			{
				stagingTableVendorInfoListView.RowAuthorize = stagingTableVendorInfoListView.GetListRowAuthorize();
				return stagingTableVendorInfoListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this StagingTableVendorInfoListView stagingTableVendorInfoListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion StagingTableVendorInfoListView
		#region StagingTableVendorInfoItemView
		public static StagingTableVendorInfoItemView GetStagingTableVendorInfoItemViewValidation(this StagingTableVendorInfoItemView stagingTableVendorInfoItemView)
		{
			try
			{
				stagingTableVendorInfoItemView.RowAuthorize = stagingTableVendorInfoItemView.GetItemRowAuthorize();
				return stagingTableVendorInfoItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this StagingTableVendorInfoItemView stagingTableVendorInfoItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static StagingTableVendorInfo ToStagingTableVendorInfo(this StagingTableVendorInfoItemView stagingTableVendorInfoItemView)
		{
			try
			{
				StagingTableVendorInfo stagingTableVendorInfo = new StagingTableVendorInfo();
				stagingTableVendorInfo.CompanyGUID = stagingTableVendorInfoItemView.CompanyGUID.StringToGuid();
				stagingTableVendorInfo.CreatedBy = stagingTableVendorInfoItemView.CreatedBy;
				stagingTableVendorInfo.CreatedDateTime = stagingTableVendorInfoItemView.CreatedDateTime.StringToSystemDateTime();
				stagingTableVendorInfo.ModifiedBy = stagingTableVendorInfoItemView.ModifiedBy;
				stagingTableVendorInfo.ModifiedDateTime = stagingTableVendorInfoItemView.ModifiedDateTime.StringToSystemDateTime();
				stagingTableVendorInfo.Owner = stagingTableVendorInfoItemView.Owner;
				stagingTableVendorInfo.OwnerBusinessUnitGUID = stagingTableVendorInfoItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				stagingTableVendorInfo.StagingTableVendorInfoGUID = stagingTableVendorInfoItemView.StagingTableVendorInfoGUID.StringToGuid();
				stagingTableVendorInfo.Address = stagingTableVendorInfoItemView.Address;
				stagingTableVendorInfo.AddressName = stagingTableVendorInfoItemView.AddressName;
				stagingTableVendorInfo.AltName = stagingTableVendorInfoItemView.AltName;
				stagingTableVendorInfo.BankAccount = stagingTableVendorInfoItemView.BankAccount;
				stagingTableVendorInfo.BankAccountName = stagingTableVendorInfoItemView.BankAccountName;
				stagingTableVendorInfo.BankBranch = stagingTableVendorInfoItemView.BankBranch;
				stagingTableVendorInfo.BankGroupId = stagingTableVendorInfoItemView.BankGroupId;
				stagingTableVendorInfo.CompanyId = stagingTableVendorInfoItemView.StagingTableCompanyId;
				stagingTableVendorInfo.CountryId = stagingTableVendorInfoItemView.CountryId;
				stagingTableVendorInfo.CurrencyId = stagingTableVendorInfoItemView.CurrencyId;
				stagingTableVendorInfo.DistrictId = stagingTableVendorInfoItemView.DistrictId;
				stagingTableVendorInfo.Name = stagingTableVendorInfoItemView.Name;
				stagingTableVendorInfo.PostalCode = stagingTableVendorInfoItemView.PostalCode;
				stagingTableVendorInfo.ProcessTransGUID = stagingTableVendorInfoItemView.ProcessTransGUID.StringToGuidNull();
				stagingTableVendorInfo.ProvinceId = stagingTableVendorInfoItemView.ProvinceId;
				stagingTableVendorInfo.RecordType = stagingTableVendorInfoItemView.RecordType;
				stagingTableVendorInfo.SubDistrictId = stagingTableVendorInfoItemView.SubDistrictId;
				stagingTableVendorInfo.TaxBranchId = stagingTableVendorInfoItemView.TaxBranchId;
				stagingTableVendorInfo.VendGroupId = stagingTableVendorInfoItemView.VendGroupId;
				stagingTableVendorInfo.VendorId = stagingTableVendorInfoItemView.VendorId;
				stagingTableVendorInfo.VendorPaymentTransGUID = stagingTableVendorInfoItemView.VendorPaymentTransGUID.StringToGuidNull();
				stagingTableVendorInfo.VendorTaxId = stagingTableVendorInfoItemView.VendorTaxId;
				
				stagingTableVendorInfo.RowVersion = stagingTableVendorInfoItemView.RowVersion;
				return stagingTableVendorInfo;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<StagingTableVendorInfo> ToStagingTableVendorInfo(this IEnumerable<StagingTableVendorInfoItemView> stagingTableVendorInfoItemViews)
		{
			try
			{
				List<StagingTableVendorInfo> stagingTableVendorInfos = new List<StagingTableVendorInfo>();
				foreach (StagingTableVendorInfoItemView item in stagingTableVendorInfoItemViews)
				{
					stagingTableVendorInfos.Add(item.ToStagingTableVendorInfo());
				}
				return stagingTableVendorInfos;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static StagingTableVendorInfoItemView ToStagingTableVendorInfoItemView(this StagingTableVendorInfo stagingTableVendorInfo)
		{
			try
			{
				StagingTableVendorInfoItemView stagingTableVendorInfoItemView = new StagingTableVendorInfoItemView();
				stagingTableVendorInfoItemView.CompanyGUID = stagingTableVendorInfo.CompanyGUID.GuidNullToString();
				stagingTableVendorInfoItemView.CreatedBy = stagingTableVendorInfo.CreatedBy;
				stagingTableVendorInfoItemView.CreatedDateTime = stagingTableVendorInfo.CreatedDateTime.DateTimeToString();
				stagingTableVendorInfoItemView.ModifiedBy = stagingTableVendorInfo.ModifiedBy;
				stagingTableVendorInfoItemView.ModifiedDateTime = stagingTableVendorInfo.ModifiedDateTime.DateTimeToString();
				stagingTableVendorInfoItemView.Owner = stagingTableVendorInfo.Owner;
				stagingTableVendorInfoItemView.OwnerBusinessUnitGUID = stagingTableVendorInfo.OwnerBusinessUnitGUID.GuidNullToString();
				stagingTableVendorInfoItemView.StagingTableVendorInfoGUID = stagingTableVendorInfo.StagingTableVendorInfoGUID.GuidNullToString();
				stagingTableVendorInfoItemView.Address = stagingTableVendorInfo.Address;
				stagingTableVendorInfoItemView.AddressName = stagingTableVendorInfo.AddressName;
				stagingTableVendorInfoItemView.AltName = stagingTableVendorInfo.AltName;
				stagingTableVendorInfoItemView.BankAccount = stagingTableVendorInfo.BankAccount;
				stagingTableVendorInfoItemView.BankAccountName = stagingTableVendorInfo.BankAccountName;
				stagingTableVendorInfoItemView.BankBranch = stagingTableVendorInfo.BankBranch;
				stagingTableVendorInfoItemView.BankGroupId = stagingTableVendorInfo.BankGroupId;
				stagingTableVendorInfoItemView.StagingTableCompanyId = stagingTableVendorInfo.CompanyId;
				stagingTableVendorInfoItemView.CountryId = stagingTableVendorInfo.CountryId;
				stagingTableVendorInfoItemView.CurrencyId = stagingTableVendorInfo.CurrencyId;
				stagingTableVendorInfoItemView.DistrictId = stagingTableVendorInfo.DistrictId;
				stagingTableVendorInfoItemView.Name = stagingTableVendorInfo.Name;
				stagingTableVendorInfoItemView.PostalCode = stagingTableVendorInfo.PostalCode;
				stagingTableVendorInfoItemView.ProcessTransGUID = stagingTableVendorInfo.ProcessTransGUID.GuidNullToString();
				stagingTableVendorInfoItemView.ProvinceId = stagingTableVendorInfo.ProvinceId;
				stagingTableVendorInfoItemView.RecordType = stagingTableVendorInfo.RecordType;
				stagingTableVendorInfoItemView.SubDistrictId = stagingTableVendorInfo.SubDistrictId;
				stagingTableVendorInfoItemView.TaxBranchId = stagingTableVendorInfo.TaxBranchId;
				stagingTableVendorInfoItemView.VendGroupId = stagingTableVendorInfo.VendGroupId;
				stagingTableVendorInfoItemView.VendorId = stagingTableVendorInfo.VendorId;
				stagingTableVendorInfoItemView.VendorPaymentTransGUID = stagingTableVendorInfo.VendorPaymentTransGUID.GuidNullToString();
				stagingTableVendorInfoItemView.VendorTaxId = stagingTableVendorInfo.VendorTaxId;
				
				stagingTableVendorInfoItemView.RowVersion = stagingTableVendorInfo.RowVersion;
				return stagingTableVendorInfoItemView.GetStagingTableVendorInfoItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion StagingTableVendorInfoItemView
		#region ToDropDown
		public static SelectItem<StagingTableVendorInfoItemView> ToDropDownItem(this StagingTableVendorInfoItemView stagingTableVendorInfoView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<StagingTableVendorInfoItemView> selectItem = new SelectItem<StagingTableVendorInfoItemView>();
				selectItem.Label = null;
				selectItem.Value = stagingTableVendorInfoView.StagingTableVendorInfoGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: stagingTableVendorInfoView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<StagingTableVendorInfoItemView>> ToDropDownItem(this IEnumerable<StagingTableVendorInfoItemView> stagingTableVendorInfoItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<StagingTableVendorInfoItemView>> selectItems = new List<SelectItem<StagingTableVendorInfoItemView>>();
				foreach (StagingTableVendorInfoItemView item in stagingTableVendorInfoItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

