using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class StagingTransTextHandler
	{
		#region StagingTransTextListView
		public static List<StagingTransTextListView> GetStagingTransTextListViewValidation(this List<StagingTransTextListView> stagingTransTextListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return stagingTransTextListViews;
			}
			var result = new List<StagingTransTextListView>();
			try
			{
				foreach (StagingTransTextListView item in stagingTransTextListViews)
				{
					result.Add(item.GetStagingTransTextListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static StagingTransTextListView GetStagingTransTextListViewValidation(this StagingTransTextListView stagingTransTextListView)
		{
			try
			{
				stagingTransTextListView.RowAuthorize = stagingTransTextListView.GetListRowAuthorize();
				return stagingTransTextListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this StagingTransTextListView stagingTransTextListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion StagingTransTextListView
		#region StagingTransTextItemView
		public static StagingTransTextItemView GetStagingTransTextItemViewValidation(this StagingTransTextItemView stagingTransTextItemView)
		{
			try
			{
				stagingTransTextItemView.RowAuthorize = stagingTransTextItemView.GetItemRowAuthorize();
				return stagingTransTextItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this StagingTransTextItemView stagingTransTextItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static StagingTransText ToStagingTransText(this StagingTransTextItemView stagingTransTextItemView)
		{
			try
			{
				StagingTransText stagingTransText = new StagingTransText();
				stagingTransText.CompanyGUID = stagingTransTextItemView.CompanyGUID.StringToGuid();
				stagingTransText.CreatedBy = stagingTransTextItemView.CreatedBy;
				stagingTransText.CreatedDateTime = stagingTransTextItemView.CreatedDateTime.StringToSystemDateTime();
				stagingTransText.ModifiedBy = stagingTransTextItemView.ModifiedBy;
				stagingTransText.ModifiedDateTime = stagingTransTextItemView.ModifiedDateTime.StringToSystemDateTime();
				stagingTransText.Owner = stagingTransTextItemView.Owner;
				stagingTransText.OwnerBusinessUnitGUID = stagingTransTextItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				stagingTransText.StagingTransTextGUID = stagingTransTextItemView.StagingTransTextGUID.StringToGuid();
				stagingTransText.ProcessTransType = stagingTransTextItemView.ProcessTransType;
				stagingTransText.TransText = stagingTransTextItemView.TransText;
				
				stagingTransText.RowVersion = stagingTransTextItemView.RowVersion;
				return stagingTransText;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<StagingTransText> ToStagingTransText(this IEnumerable<StagingTransTextItemView> stagingTransTextItemViews)
		{
			try
			{
				List<StagingTransText> stagingTransTexts = new List<StagingTransText>();
				foreach (StagingTransTextItemView item in stagingTransTextItemViews)
				{
					stagingTransTexts.Add(item.ToStagingTransText());
				}
				return stagingTransTexts;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static StagingTransTextItemView ToStagingTransTextItemView(this StagingTransText stagingTransText)
		{
			try
			{
				StagingTransTextItemView stagingTransTextItemView = new StagingTransTextItemView();
				stagingTransTextItemView.CompanyGUID = stagingTransText.CompanyGUID.GuidNullToString();
				stagingTransTextItemView.CreatedBy = stagingTransText.CreatedBy;
				stagingTransTextItemView.CreatedDateTime = stagingTransText.CreatedDateTime.DateTimeToString();
				stagingTransTextItemView.ModifiedBy = stagingTransText.ModifiedBy;
				stagingTransTextItemView.ModifiedDateTime = stagingTransText.ModifiedDateTime.DateTimeToString();
				stagingTransTextItemView.Owner = stagingTransText.Owner;
				stagingTransTextItemView.OwnerBusinessUnitGUID = stagingTransText.OwnerBusinessUnitGUID.GuidNullToString();
				stagingTransTextItemView.StagingTransTextGUID = stagingTransText.StagingTransTextGUID.GuidNullToString();
				stagingTransTextItemView.ProcessTransType = stagingTransText.ProcessTransType;
				stagingTransTextItemView.TransText = stagingTransText.TransText;
				
				stagingTransTextItemView.RowVersion = stagingTransText.RowVersion;
				return stagingTransTextItemView.GetStagingTransTextItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion StagingTransTextItemView
		#region ToDropDown
		public static SelectItem<StagingTransTextItemView> ToDropDownItem(this StagingTransTextItemView stagingTransTextView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<StagingTransTextItemView> selectItem = new SelectItem<StagingTransTextItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(stagingTransTextView.ProcessTransType.ToString(), stagingTransTextView.TransText);
				selectItem.Value = stagingTransTextView.StagingTransTextGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: stagingTransTextView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<StagingTransTextItemView>> ToDropDownItem(this IEnumerable<StagingTransTextItemView> stagingTransTextItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<StagingTransTextItemView>> selectItems = new List<SelectItem<StagingTransTextItemView>>();
				foreach (StagingTransTextItemView item in stagingTransTextItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

