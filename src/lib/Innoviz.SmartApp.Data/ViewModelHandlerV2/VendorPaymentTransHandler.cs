using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class VendorPaymentTransHandler
	{
		#region VendorPaymentTransListView
		public static List<VendorPaymentTransListView> GetVendorPaymentTransListViewValidation(this List<VendorPaymentTransListView> vendorPaymentTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return vendorPaymentTransListViews;
			}
			var result = new List<VendorPaymentTransListView>();
			try
			{
				foreach (VendorPaymentTransListView item in vendorPaymentTransListViews)
				{
					result.Add(item.GetVendorPaymentTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static VendorPaymentTransListView GetVendorPaymentTransListViewValidation(this VendorPaymentTransListView vendorPaymentTransListView)
		{
			try
			{
				vendorPaymentTransListView.RowAuthorize = vendorPaymentTransListView.GetListRowAuthorize();
				return vendorPaymentTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this VendorPaymentTransListView vendorPaymentTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion VendorPaymentTransListView
		#region VendorPaymentTransItemView
		public static VendorPaymentTransItemView GetVendorPaymentTransItemViewValidation(this VendorPaymentTransItemView vendorPaymentTransItemView)
		{
			try
			{
				vendorPaymentTransItemView.RowAuthorize = vendorPaymentTransItemView.GetItemRowAuthorize();
				return vendorPaymentTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this VendorPaymentTransItemView vendorPaymentTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static VendorPaymentTrans ToVendorPaymentTrans(this VendorPaymentTransItemView vendorPaymentTransItemView)
		{
			try
			{
				VendorPaymentTrans vendorPaymentTrans = new VendorPaymentTrans();
				vendorPaymentTrans.CompanyGUID = vendorPaymentTransItemView.CompanyGUID.StringToGuid();
				vendorPaymentTrans.CreatedBy = vendorPaymentTransItemView.CreatedBy;
				vendorPaymentTrans.CreatedDateTime = vendorPaymentTransItemView.CreatedDateTime.StringToSystemDateTime();
				vendorPaymentTrans.ModifiedBy = vendorPaymentTransItemView.ModifiedBy;
				vendorPaymentTrans.ModifiedDateTime = vendorPaymentTransItemView.ModifiedDateTime.StringToSystemDateTime();
				vendorPaymentTrans.Owner = vendorPaymentTransItemView.Owner;
				vendorPaymentTrans.OwnerBusinessUnitGUID = vendorPaymentTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				vendorPaymentTrans.VendorPaymentTransGUID = vendorPaymentTransItemView.VendorPaymentTransGUID.StringToGuid();
				vendorPaymentTrans.AmountBeforeTax = vendorPaymentTransItemView.AmountBeforeTax;
				vendorPaymentTrans.CreditAppTableGUID = vendorPaymentTransItemView.CreditAppTableGUID.StringToGuidNull();
				vendorPaymentTrans.Dimension1 = vendorPaymentTransItemView.Dimension1.StringToGuidNull();
				vendorPaymentTrans.Dimension2 = vendorPaymentTransItemView.Dimension2.StringToGuidNull();
				vendorPaymentTrans.Dimension3 = vendorPaymentTransItemView.Dimension3.StringToGuidNull();
				vendorPaymentTrans.Dimension4 = vendorPaymentTransItemView.Dimension4.StringToGuidNull();
				vendorPaymentTrans.Dimension5 = vendorPaymentTransItemView.Dimension5.StringToGuidNull();
				vendorPaymentTrans.DocumentStatusGUID = vendorPaymentTransItemView.DocumentStatusGUID.StringToGuidNull();
				vendorPaymentTrans.OffsetAccount = vendorPaymentTransItemView.OffsetAccount;
				vendorPaymentTrans.PaymentDate = vendorPaymentTransItemView.PaymentDate.StringNullToDateNull();
				vendorPaymentTrans.RefGUID = vendorPaymentTransItemView.RefGUID.StringToGuidNull();
				vendorPaymentTrans.RefType = vendorPaymentTransItemView.RefType;
				vendorPaymentTrans.TaxAmount = vendorPaymentTransItemView.TaxAmount;
				vendorPaymentTrans.TaxTableGUID = vendorPaymentTransItemView.TaxTableGUID.StringToGuidNull();
				vendorPaymentTrans.TotalAmount = vendorPaymentTransItemView.TotalAmount;
				vendorPaymentTrans.VendorTableGUID = vendorPaymentTransItemView.VendorTableGUID.StringToGuidNull();
				vendorPaymentTrans.VendorTaxInvoiceId = vendorPaymentTransItemView.VendorTaxInvoiceId;
				
				vendorPaymentTrans.RowVersion = vendorPaymentTransItemView.RowVersion;
				return vendorPaymentTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<VendorPaymentTrans> ToVendorPaymentTrans(this IEnumerable<VendorPaymentTransItemView> vendorPaymentTransItemViews)
		{
			try
			{
				List<VendorPaymentTrans> vendorPaymentTranss = new List<VendorPaymentTrans>();
				foreach (VendorPaymentTransItemView item in vendorPaymentTransItemViews)
				{
					vendorPaymentTranss.Add(item.ToVendorPaymentTrans());
				}
				return vendorPaymentTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static VendorPaymentTransItemView ToVendorPaymentTransItemView(this VendorPaymentTrans vendorPaymentTrans)
		{
			try
			{
				VendorPaymentTransItemView vendorPaymentTransItemView = new VendorPaymentTransItemView();
				vendorPaymentTransItemView.CompanyGUID = vendorPaymentTrans.CompanyGUID.GuidNullToString();
				vendorPaymentTransItemView.CreatedBy = vendorPaymentTrans.CreatedBy;
				vendorPaymentTransItemView.CreatedDateTime = vendorPaymentTrans.CreatedDateTime.DateTimeToString();
				vendorPaymentTransItemView.ModifiedBy = vendorPaymentTrans.ModifiedBy;
				vendorPaymentTransItemView.ModifiedDateTime = vendorPaymentTrans.ModifiedDateTime.DateTimeToString();
				vendorPaymentTransItemView.Owner = vendorPaymentTrans.Owner;
				vendorPaymentTransItemView.OwnerBusinessUnitGUID = vendorPaymentTrans.OwnerBusinessUnitGUID.GuidNullToString();
				vendorPaymentTransItemView.VendorPaymentTransGUID = vendorPaymentTrans.VendorPaymentTransGUID.GuidNullToString();
				vendorPaymentTransItemView.AmountBeforeTax = vendorPaymentTrans.AmountBeforeTax;
				vendorPaymentTransItemView.CreditAppTableGUID = vendorPaymentTrans.CreditAppTableGUID.GuidNullToString();
				vendorPaymentTransItemView.Dimension1 = vendorPaymentTrans.Dimension1.GuidNullToString();
				vendorPaymentTransItemView.Dimension2 = vendorPaymentTrans.Dimension2.GuidNullToString();
				vendorPaymentTransItemView.Dimension3 = vendorPaymentTrans.Dimension3.GuidNullToString();
				vendorPaymentTransItemView.Dimension4 = vendorPaymentTrans.Dimension4.GuidNullToString();
				vendorPaymentTransItemView.Dimension5 = vendorPaymentTrans.Dimension5.GuidNullToString();
				vendorPaymentTransItemView.DocumentStatusGUID = vendorPaymentTrans.DocumentStatusGUID.GuidNullToString();
				vendorPaymentTransItemView.OffsetAccount = vendorPaymentTrans.OffsetAccount;
				vendorPaymentTransItemView.PaymentDate = vendorPaymentTrans.PaymentDate.DateNullToString();
				vendorPaymentTransItemView.RefGUID = vendorPaymentTrans.RefGUID.GuidNullToString();
				vendorPaymentTransItemView.RefType = vendorPaymentTrans.RefType;
				vendorPaymentTransItemView.TaxAmount = vendorPaymentTrans.TaxAmount;
				vendorPaymentTransItemView.TaxTableGUID = vendorPaymentTrans.TaxTableGUID.GuidNullToString();
				vendorPaymentTransItemView.TotalAmount = vendorPaymentTrans.TotalAmount;
				vendorPaymentTransItemView.VendorTableGUID = vendorPaymentTrans.VendorTableGUID.GuidNullToString();
				vendorPaymentTransItemView.VendorTaxInvoiceId = vendorPaymentTrans.VendorTaxInvoiceId;
				
				vendorPaymentTransItemView.RowVersion = vendorPaymentTrans.RowVersion;
				return vendorPaymentTransItemView.GetVendorPaymentTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion VendorPaymentTransItemView
		#region ToDropDown
		public static SelectItem<VendorPaymentTransItemView> ToDropDownItem(this VendorPaymentTransItemView vendorPaymentTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<VendorPaymentTransItemView> selectItem = new SelectItem<VendorPaymentTransItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(vendorPaymentTransView.VendorTable_VendorId.ToString(), vendorPaymentTransView.TotalAmount.ToString());
				selectItem.Value = vendorPaymentTransView.VendorPaymentTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: vendorPaymentTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<VendorPaymentTransItemView>> ToDropDownItem(this IEnumerable<VendorPaymentTransItemView> vendorPaymentTransItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<VendorPaymentTransItemView>> selectItems = new List<SelectItem<VendorPaymentTransItemView>>();
				foreach (VendorPaymentTransItemView item in vendorPaymentTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

