using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class BuyerAgreementTransHandler
	{
		#region BuyerAgreementTransListView
		public static List<BuyerAgreementTransListView> GetBuyerAgreementTransListViewValidation(this List<BuyerAgreementTransListView> buyerAgreementTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return buyerAgreementTransListViews;
			}
			var result = new List<BuyerAgreementTransListView>();
			try
			{
				foreach (BuyerAgreementTransListView item in buyerAgreementTransListViews)
				{
					result.Add(item.GetBuyerAgreementTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BuyerAgreementTransListView GetBuyerAgreementTransListViewValidation(this BuyerAgreementTransListView buyerAgreementTransListView)
		{
			try
			{
				buyerAgreementTransListView.RowAuthorize = buyerAgreementTransListView.GetListRowAuthorize();
				return buyerAgreementTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this BuyerAgreementTransListView buyerAgreementTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion BuyerAgreementTransListView
		#region BuyerAgreementTransItemView
		public static BuyerAgreementTransItemView GetBuyerAgreementTransItemViewValidation(this BuyerAgreementTransItemView buyerAgreementTransItemView)
		{
			try
			{
				buyerAgreementTransItemView.RowAuthorize = buyerAgreementTransItemView.GetItemRowAuthorize();
				return buyerAgreementTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this BuyerAgreementTransItemView buyerAgreementTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static BuyerAgreementTrans ToBuyerAgreementTrans(this BuyerAgreementTransItemView buyerAgreementTransItemView)
		{
			try
			{
				BuyerAgreementTrans buyerAgreementTrans = new BuyerAgreementTrans();
				buyerAgreementTrans.CompanyGUID = buyerAgreementTransItemView.CompanyGUID.StringToGuid();
				buyerAgreementTrans.CreatedBy = buyerAgreementTransItemView.CreatedBy;
				buyerAgreementTrans.CreatedDateTime = buyerAgreementTransItemView.CreatedDateTime.StringToSystemDateTime();
				buyerAgreementTrans.ModifiedBy = buyerAgreementTransItemView.ModifiedBy;
				buyerAgreementTrans.ModifiedDateTime = buyerAgreementTransItemView.ModifiedDateTime.StringToSystemDateTime();
				buyerAgreementTrans.Owner = buyerAgreementTransItemView.Owner;
				buyerAgreementTrans.OwnerBusinessUnitGUID = buyerAgreementTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				buyerAgreementTrans.BuyerAgreementTransGUID = buyerAgreementTransItemView.BuyerAgreementTransGUID.StringToGuid();
				buyerAgreementTrans.BuyerAgreementLineGUID = buyerAgreementTransItemView.BuyerAgreementLineGUID.StringToGuid();
				buyerAgreementTrans.BuyerAgreementTableGUID = buyerAgreementTransItemView.BuyerAgreementTableGUID.StringToGuid();
				buyerAgreementTrans.RefGUID = buyerAgreementTransItemView.RefGUID.StringToGuid();
				buyerAgreementTrans.RefType = buyerAgreementTransItemView.RefType;
				
				buyerAgreementTrans.RowVersion = buyerAgreementTransItemView.RowVersion;
				return buyerAgreementTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<BuyerAgreementTrans> ToBuyerAgreementTrans(this IEnumerable<BuyerAgreementTransItemView> buyerAgreementTransItemViews)
		{
			try
			{
				List<BuyerAgreementTrans> buyerAgreementTranss = new List<BuyerAgreementTrans>();
				foreach (BuyerAgreementTransItemView item in buyerAgreementTransItemViews)
				{
					buyerAgreementTranss.Add(item.ToBuyerAgreementTrans());
				}
				return buyerAgreementTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BuyerAgreementTransItemView ToBuyerAgreementTransItemView(this BuyerAgreementTrans buyerAgreementTrans)
		{
			try
			{
				BuyerAgreementTransItemView buyerAgreementTransItemView = new BuyerAgreementTransItemView();
				buyerAgreementTransItemView.CompanyGUID = buyerAgreementTrans.CompanyGUID.GuidNullToString();
				buyerAgreementTransItemView.CreatedBy = buyerAgreementTrans.CreatedBy;
				buyerAgreementTransItemView.CreatedDateTime = buyerAgreementTrans.CreatedDateTime.DateTimeToString();
				buyerAgreementTransItemView.ModifiedBy = buyerAgreementTrans.ModifiedBy;
				buyerAgreementTransItemView.ModifiedDateTime = buyerAgreementTrans.ModifiedDateTime.DateTimeToString();
				buyerAgreementTransItemView.Owner = buyerAgreementTrans.Owner;
				buyerAgreementTransItemView.OwnerBusinessUnitGUID = buyerAgreementTrans.OwnerBusinessUnitGUID.GuidNullToString();
				buyerAgreementTransItemView.BuyerAgreementTransGUID = buyerAgreementTrans.BuyerAgreementTransGUID.GuidNullToString();
				buyerAgreementTransItemView.BuyerAgreementLineGUID = buyerAgreementTrans.BuyerAgreementLineGUID.GuidNullToString();
				buyerAgreementTransItemView.BuyerAgreementTableGUID = buyerAgreementTrans.BuyerAgreementTableGUID.GuidNullToString();
				buyerAgreementTransItemView.RefGUID = buyerAgreementTrans.RefGUID.GuidNullToString();
				buyerAgreementTransItemView.RefType = buyerAgreementTrans.RefType;
				
				buyerAgreementTransItemView.RowVersion = buyerAgreementTrans.RowVersion;
				return buyerAgreementTransItemView.GetBuyerAgreementTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion BuyerAgreementTransItemView
		#region ToDropDown
		public static SelectItem<BuyerAgreementTransItemView> ToDropDownItem(this BuyerAgreementTransItemView buyerAgreementTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<BuyerAgreementTransItemView> selectItem = new SelectItem<BuyerAgreementTransItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(buyerAgreementTransView.BuyerAgreementTableGUID.ToString(), buyerAgreementTransView.BuyerAgreementLineGUID.ToString());
				selectItem.Value = buyerAgreementTransView.BuyerAgreementTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: buyerAgreementTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<BuyerAgreementTransItemView>> ToDropDownItem(this IEnumerable<BuyerAgreementTransItemView> buyerAgreementTransItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<BuyerAgreementTransItemView>> selectItems = new List<SelectItem<BuyerAgreementTransItemView>>();
				foreach (BuyerAgreementTransItemView item in buyerAgreementTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}

		public static SelectItem<BuyerAgreementTransItemView> ToDropDownItemByWithdrawal(this BuyerAgreementTransItemView buyerAgreementTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<BuyerAgreementTransItemView> selectItem = new SelectItem<BuyerAgreementTransItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(buyerAgreementTransView.BuyerAgreementTable_BuyerAgreementId, buyerAgreementTransView.BuyerAgreementLine_Description);
				selectItem.Value = buyerAgreementTransView.BuyerAgreementTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: buyerAgreementTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<BuyerAgreementTransItemView>> ToDropDownItemByWithdrawal(this IEnumerable<BuyerAgreementTransItemView> buyerAgreementTransItemViews)
		{
			try
			{
				List<SelectItem<BuyerAgreementTransItemView>> selectItems = new List<SelectItem<BuyerAgreementTransItemView>>();
				foreach (BuyerAgreementTransItemView item in buyerAgreementTransItemViews)
				{
					selectItems.Add(item.ToDropDownItemByWithdrawal());
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

