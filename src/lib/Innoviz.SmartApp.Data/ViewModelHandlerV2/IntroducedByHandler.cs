using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class IntroducedByHandler
	{
		#region IntroducedByListView
		public static List<IntroducedByListView> GetIntroducedByListViewValidation(this List<IntroducedByListView> introducedByListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return introducedByListViews;
			}
			var result = new List<IntroducedByListView>();
			try
			{
				foreach (IntroducedByListView item in introducedByListViews)
				{
					result.Add(item.GetIntroducedByListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IntroducedByListView GetIntroducedByListViewValidation(this IntroducedByListView introducedByListView)
		{
			try
			{
				introducedByListView.RowAuthorize = introducedByListView.GetListRowAuthorize();
				return introducedByListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this IntroducedByListView introducedByListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion IntroducedByListView
		#region IntroducedByItemView
		public static IntroducedByItemView GetIntroducedByItemViewValidation(this IntroducedByItemView introducedByItemView)
		{
			try
			{
				introducedByItemView.RowAuthorize = introducedByItemView.GetItemRowAuthorize();
				return introducedByItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this IntroducedByItemView introducedByItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static IntroducedBy ToIntroducedBy(this IntroducedByItemView introducedByItemView)
		{
			try
			{
				IntroducedBy introducedBy = new IntroducedBy();
				introducedBy.CompanyGUID = introducedByItemView.CompanyGUID.StringToGuid();
				introducedBy.CreatedBy = introducedByItemView.CreatedBy;
				introducedBy.CreatedDateTime = introducedByItemView.CreatedDateTime.StringToSystemDateTime();
				introducedBy.ModifiedBy = introducedByItemView.ModifiedBy;
				introducedBy.ModifiedDateTime = introducedByItemView.ModifiedDateTime.StringToSystemDateTime();
				introducedBy.Owner = introducedByItemView.Owner;
				introducedBy.OwnerBusinessUnitGUID = introducedByItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				introducedBy.IntroducedByGUID = introducedByItemView.IntroducedByGUID.StringToGuid();
				introducedBy.Description = introducedByItemView.Description;
				introducedBy.IntroducedById = introducedByItemView.IntroducedById;
				introducedBy.VendorTableGUID = introducedByItemView.VendorTableGUID.StringToGuidNull();
				
				introducedBy.RowVersion = introducedByItemView.RowVersion;
				return introducedBy;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<IntroducedBy> ToIntroducedBy(this IEnumerable<IntroducedByItemView> introducedByItemViews)
		{
			try
			{
				List<IntroducedBy> introducedBys = new List<IntroducedBy>();
				foreach (IntroducedByItemView item in introducedByItemViews)
				{
					introducedBys.Add(item.ToIntroducedBy());
				}
				return introducedBys;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IntroducedByItemView ToIntroducedByItemView(this IntroducedBy introducedBy)
		{
			try
			{
				IntroducedByItemView introducedByItemView = new IntroducedByItemView();
				introducedByItemView.CompanyGUID = introducedBy.CompanyGUID.GuidNullToString();
				introducedByItemView.CreatedBy = introducedBy.CreatedBy;
				introducedByItemView.CreatedDateTime = introducedBy.CreatedDateTime.DateTimeToString();
				introducedByItemView.ModifiedBy = introducedBy.ModifiedBy;
				introducedByItemView.ModifiedDateTime = introducedBy.ModifiedDateTime.DateTimeToString();
				introducedByItemView.Owner = introducedBy.Owner;
				introducedByItemView.OwnerBusinessUnitGUID = introducedBy.OwnerBusinessUnitGUID.GuidNullToString();
				introducedByItemView.IntroducedByGUID = introducedBy.IntroducedByGUID.GuidNullToString();
				introducedByItemView.Description = introducedBy.Description;
				introducedByItemView.IntroducedById = introducedBy.IntroducedById;
				introducedByItemView.VendorTableGUID = introducedBy.VendorTableGUID.GuidNullToString();
				
				introducedByItemView.RowVersion = introducedBy.RowVersion;
				return introducedByItemView.GetIntroducedByItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion IntroducedByItemView
		#region ToDropDown
		public static SelectItem<IntroducedByItemView> ToDropDownItem(this IntroducedByItemView introducedByView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<IntroducedByItemView> selectItem = new SelectItem<IntroducedByItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(introducedByView.IntroducedById, introducedByView.Description);
				selectItem.Value = introducedByView.IntroducedByGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: introducedByView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<IntroducedByItemView>> ToDropDownItem(this IEnumerable<IntroducedByItemView> introducedByItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<IntroducedByItemView>> selectItems = new List<SelectItem<IntroducedByItemView>>();
				foreach (IntroducedByItemView item in introducedByItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

