using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class DepartmentHandler
	{
		#region DepartmentListView
		public static List<DepartmentListView> GetDepartmentListViewValidation(this List<DepartmentListView> departmentListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return departmentListViews;
			}
			var result = new List<DepartmentListView>();
			try
			{
				foreach (DepartmentListView item in departmentListViews)
				{
					result.Add(item.GetDepartmentListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static DepartmentListView GetDepartmentListViewValidation(this DepartmentListView departmentListView)
		{
			try
			{
				departmentListView.RowAuthorize = departmentListView.GetListRowAuthorize();
				return departmentListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this DepartmentListView departmentListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion DepartmentListView
		#region DepartmentItemView
		public static DepartmentItemView GetDepartmentItemViewValidation(this DepartmentItemView departmentItemView)
		{
			try
			{
				departmentItemView.RowAuthorize = departmentItemView.GetItemRowAuthorize();
				return departmentItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this DepartmentItemView departmentItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static Department ToDepartment(this DepartmentItemView departmentItemView)
		{
			try
			{
				Department department = new Department();
				department.CompanyGUID = departmentItemView.CompanyGUID.StringToGuid();
				department.CreatedBy = departmentItemView.CreatedBy;
				department.CreatedDateTime = departmentItemView.CreatedDateTime.StringToSystemDateTime();
				department.ModifiedBy = departmentItemView.ModifiedBy;
				department.ModifiedDateTime = departmentItemView.ModifiedDateTime.StringToSystemDateTime();
				department.Owner = departmentItemView.Owner;
				department.OwnerBusinessUnitGUID = departmentItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				department.DepartmentGUID = departmentItemView.DepartmentGUID.StringToGuid();
				department.DepartmentId = departmentItemView.DepartmentId;
				department.Description = departmentItemView.Description;
				
				department.RowVersion = departmentItemView.RowVersion;
				return department;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<Department> ToDepartment(this IEnumerable<DepartmentItemView> departmentItemViews)
		{
			try
			{
				List<Department> departments = new List<Department>();
				foreach (DepartmentItemView item in departmentItemViews)
				{
					departments.Add(item.ToDepartment());
				}
				return departments;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static DepartmentItemView ToDepartmentItemView(this Department department)
		{
			try
			{
				DepartmentItemView departmentItemView = new DepartmentItemView();
				departmentItemView.CompanyGUID = department.CompanyGUID.GuidNullToString();
				departmentItemView.CreatedBy = department.CreatedBy;
				departmentItemView.CreatedDateTime = department.CreatedDateTime.DateTimeToString();
				departmentItemView.ModifiedBy = department.ModifiedBy;
				departmentItemView.ModifiedDateTime = department.ModifiedDateTime.DateTimeToString();
				departmentItemView.Owner = department.Owner;
				departmentItemView.OwnerBusinessUnitGUID = department.OwnerBusinessUnitGUID.GuidNullToString();
				departmentItemView.DepartmentGUID = department.DepartmentGUID.GuidNullToString();
				departmentItemView.DepartmentId = department.DepartmentId;
				departmentItemView.Description = department.Description;
				
				departmentItemView.RowVersion = department.RowVersion;
				return departmentItemView.GetDepartmentItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion DepartmentItemView
		#region ToDropDown
		public static SelectItem<DepartmentItemView> ToDropDownItem(this DepartmentItemView departmentView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<DepartmentItemView> selectItem = new SelectItem<DepartmentItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(departmentView.DepartmentId, departmentView.Description);
				selectItem.Value = departmentView.DepartmentGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: departmentView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<DepartmentItemView>> ToDropDownItem(this IEnumerable<DepartmentItemView> departmentItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<DepartmentItemView>> selectItems = new List<SelectItem<DepartmentItemView>>();
				foreach (DepartmentItemView item in departmentItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

