using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class RetentionConditionTransHandler
	{
		#region RetentionConditionTransListView
		public static List<RetentionConditionTransListView> GetRetentionConditionTransListViewValidation(this List<RetentionConditionTransListView> retentionConditionTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return retentionConditionTransListViews;
			}
			var result = new List<RetentionConditionTransListView>();
			try
			{
				foreach (RetentionConditionTransListView item in retentionConditionTransListViews)
				{
					result.Add(item.GetRetentionConditionTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static RetentionConditionTransListView GetRetentionConditionTransListViewValidation(this RetentionConditionTransListView retentionConditionTransListView)
		{
			try
			{
				retentionConditionTransListView.RowAuthorize = retentionConditionTransListView.GetListRowAuthorize();
				return retentionConditionTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this RetentionConditionTransListView retentionConditionTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion RetentionConditionTransListView
		#region RetentionConditionTransItemView
		public static RetentionConditionTransItemView GetRetentionConditionTransItemViewValidation(this RetentionConditionTransItemView retentionConditionTransItemView)
		{
			try
			{
				retentionConditionTransItemView.RowAuthorize = retentionConditionTransItemView.GetItemRowAuthorize();
				return retentionConditionTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this RetentionConditionTransItemView retentionConditionTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static RetentionConditionTrans ToRetentionConditionTrans(this RetentionConditionTransItemView retentionConditionTransItemView)
		{
			try
			{
				RetentionConditionTrans retentionConditionTrans = new RetentionConditionTrans();
				retentionConditionTrans.CompanyGUID = retentionConditionTransItemView.CompanyGUID.StringToGuid();
				retentionConditionTrans.CreatedBy = retentionConditionTransItemView.CreatedBy;
				retentionConditionTrans.CreatedDateTime = retentionConditionTransItemView.CreatedDateTime.StringToSystemDateTime();
				retentionConditionTrans.ModifiedBy = retentionConditionTransItemView.ModifiedBy;
				retentionConditionTrans.ModifiedDateTime = retentionConditionTransItemView.ModifiedDateTime.StringToSystemDateTime();
				retentionConditionTrans.Owner = retentionConditionTransItemView.Owner;
				retentionConditionTrans.OwnerBusinessUnitGUID = retentionConditionTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				retentionConditionTrans.RetentionConditionTransGUID = retentionConditionTransItemView.RetentionConditionTransGUID.StringToGuid();
				retentionConditionTrans.ProductType = retentionConditionTransItemView.ProductType;
				retentionConditionTrans.RefGUID = retentionConditionTransItemView.RefGUID.StringToGuid();
				retentionConditionTrans.RefType = retentionConditionTransItemView.RefType;
				retentionConditionTrans.RetentionAmount = retentionConditionTransItemView.RetentionAmount;
				retentionConditionTrans.RetentionCalculateBase = retentionConditionTransItemView.RetentionCalculateBase;
				retentionConditionTrans.RetentionDeductionMethod = retentionConditionTransItemView.RetentionDeductionMethod;
				retentionConditionTrans.RetentionPct = retentionConditionTransItemView.RetentionPct;
				
				retentionConditionTrans.RowVersion = retentionConditionTransItemView.RowVersion;
				return retentionConditionTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<RetentionConditionTrans> ToRetentionConditionTrans(this IEnumerable<RetentionConditionTransItemView> retentionConditionTransItemViews)
		{
			try
			{
				List<RetentionConditionTrans> retentionConditionTranss = new List<RetentionConditionTrans>();
				foreach (RetentionConditionTransItemView item in retentionConditionTransItemViews)
				{
					retentionConditionTranss.Add(item.ToRetentionConditionTrans());
				}
				return retentionConditionTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static RetentionConditionTransItemView ToRetentionConditionTransItemView(this RetentionConditionTrans retentionConditionTrans)
		{
			try
			{
				RetentionConditionTransItemView retentionConditionTransItemView = new RetentionConditionTransItemView();
				retentionConditionTransItemView.CompanyGUID = retentionConditionTrans.CompanyGUID.GuidNullToString();
				retentionConditionTransItemView.CreatedBy = retentionConditionTrans.CreatedBy;
				retentionConditionTransItemView.CreatedDateTime = retentionConditionTrans.CreatedDateTime.DateTimeToString();
				retentionConditionTransItemView.ModifiedBy = retentionConditionTrans.ModifiedBy;
				retentionConditionTransItemView.ModifiedDateTime = retentionConditionTrans.ModifiedDateTime.DateTimeToString();
				retentionConditionTransItemView.Owner = retentionConditionTrans.Owner;
				retentionConditionTransItemView.OwnerBusinessUnitGUID = retentionConditionTrans.OwnerBusinessUnitGUID.GuidNullToString();
				retentionConditionTransItemView.RetentionConditionTransGUID = retentionConditionTrans.RetentionConditionTransGUID.GuidNullToString();
				retentionConditionTransItemView.ProductType = retentionConditionTrans.ProductType;
				retentionConditionTransItemView.RefGUID = retentionConditionTrans.RefGUID.GuidNullToString();
				retentionConditionTransItemView.RefType = retentionConditionTrans.RefType;
				retentionConditionTransItemView.RetentionAmount = retentionConditionTrans.RetentionAmount;
				retentionConditionTransItemView.RetentionCalculateBase = retentionConditionTrans.RetentionCalculateBase;
				retentionConditionTransItemView.RetentionDeductionMethod = retentionConditionTrans.RetentionDeductionMethod;
				retentionConditionTransItemView.RetentionPct = retentionConditionTrans.RetentionPct;
				
				retentionConditionTransItemView.RowVersion = retentionConditionTrans.RowVersion;
				return retentionConditionTransItemView.GetRetentionConditionTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion RetentionConditionTransItemView
		#region ToDropDown
		public static SelectItem<RetentionConditionTransItemView> ToDropDownItem(this RetentionConditionTransItemView retentionConditionTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<RetentionConditionTransItemView> selectItem = new SelectItem<RetentionConditionTransItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(retentionConditionTransView.RetentionDeductionMethod.ToString(), retentionConditionTransView.RetentionCalculateBase.ToString());
				selectItem.Value = retentionConditionTransView.RetentionConditionTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: retentionConditionTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<RetentionConditionTransItemView>> ToDropDownItem(this IEnumerable<RetentionConditionTransItemView> retentionConditionTransItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<RetentionConditionTransItemView>> selectItems = new List<SelectItem<RetentionConditionTransItemView>>();
				foreach (RetentionConditionTransItemView item in retentionConditionTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

