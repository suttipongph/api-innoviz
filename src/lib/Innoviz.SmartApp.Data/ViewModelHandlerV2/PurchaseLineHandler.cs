using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class PurchaseLineHandler
	{
		#region PurchaseLineListView
		public static List<PurchaseLineListView> GetPurchaseLineListViewValidation(this List<PurchaseLineListView> purchaseLineListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return purchaseLineListViews;
			}
			var result = new List<PurchaseLineListView>();
			try
			{
				foreach (PurchaseLineListView item in purchaseLineListViews)
				{
					result.Add(item.GetPurchaseLineListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static PurchaseLineListView GetPurchaseLineListViewValidation(this PurchaseLineListView purchaseLineListView)
		{
			try
			{
				purchaseLineListView.RowAuthorize = purchaseLineListView.GetListRowAuthorize();
				return purchaseLineListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this PurchaseLineListView purchaseLineListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion PurchaseLineListView
		#region PurchaseLineItemView
		public static PurchaseLineItemView GetPurchaseLineItemViewValidation(this PurchaseLineItemView purchaseLineItemView)
		{
			try
			{
				purchaseLineItemView.RowAuthorize = purchaseLineItemView.GetItemRowAuthorize();
				return purchaseLineItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this PurchaseLineItemView purchaseLineItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static PurchaseLine ToPurchaseLine(this PurchaseLineItemView purchaseLineItemView)
		{
			try
			{
				PurchaseLine purchaseLine = new PurchaseLine();
				purchaseLine.CompanyGUID = purchaseLineItemView.CompanyGUID.StringToGuid();
				purchaseLine.CreatedBy = purchaseLineItemView.CreatedBy;
				purchaseLine.CreatedDateTime = purchaseLineItemView.CreatedDateTime.StringToSystemDateTime();
				purchaseLine.ModifiedBy = purchaseLineItemView.ModifiedBy;
				purchaseLine.ModifiedDateTime = purchaseLineItemView.ModifiedDateTime.StringToSystemDateTime();
				purchaseLine.Owner = purchaseLineItemView.Owner;
				purchaseLine.OwnerBusinessUnitGUID = purchaseLineItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				purchaseLine.PurchaseLineGUID = purchaseLineItemView.PurchaseLineGUID.StringToGuid();
				purchaseLine.AssignmentAgreementTableGUID = purchaseLineItemView.AssignmentAgreementTableGUID.StringToGuid();
				purchaseLine.AssignmentAmount = purchaseLineItemView.AssignmentAmount;
				purchaseLine.BillingDate = purchaseLineItemView.BillingDate.StringNullToDateNull();
				purchaseLine.BillingFeeAmount = purchaseLineItemView.BillingFeeAmount;
				purchaseLine.BuyerAgreementTableGUID = purchaseLineItemView.BuyerAgreementTableGUID.StringToGuidNull();
				purchaseLine.BuyerInvoiceAmount = purchaseLineItemView.BuyerInvoiceAmount;
				purchaseLine.BuyerInvoiceTableGUID = purchaseLineItemView.BuyerInvoiceTableGUID.StringToGuid();
				purchaseLine.BuyerPDCTableGUID = purchaseLineItemView.BuyerPDCTableGUID.StringToGuidNull();
				purchaseLine.BuyerTableGUID = purchaseLineItemView.BuyerTableGUID.StringToGuid();
				purchaseLine.ClosedForAdditionalPurchase = purchaseLineItemView.ClosedForAdditionalPurchase;
				purchaseLine.CollectionDate = purchaseLineItemView.CollectionDate.StringToDate();
				purchaseLine.CreditAppLineGUID  = purchaseLineItemView.CreditAppLineGUID .StringToGuid();
				purchaseLine.CustomerPDCTableGUID = purchaseLineItemView.CustomerPDCTableGUID.StringToGuidNull();
				purchaseLine.Dimension1GUID = purchaseLineItemView.Dimension1GUID.StringToGuidNull();
				purchaseLine.Dimension2GUID = purchaseLineItemView.Dimension2GUID.StringToGuidNull();
				purchaseLine.Dimension3GUID = purchaseLineItemView.Dimension3GUID.StringToGuidNull();
				purchaseLine.Dimension4GUID = purchaseLineItemView.Dimension4GUID.StringToGuidNull();
				purchaseLine.Dimension5GUID = purchaseLineItemView.Dimension5GUID.StringToGuidNull();
				purchaseLine.DueDate = purchaseLineItemView.DueDate.StringToDate();
				purchaseLine.InterestAmount = purchaseLineItemView.InterestAmount;
				purchaseLine.InterestDate = purchaseLineItemView.InterestDate.StringToDate();
				purchaseLine.InterestDay = purchaseLineItemView.InterestDay;
				purchaseLine.InterestRefundAmount = purchaseLineItemView.InterestRefundAmount;
				purchaseLine.LineNum = purchaseLineItemView.LineNum;
				purchaseLine.LinePurchaseAmount = purchaseLineItemView.LinePurchaseAmount;
				purchaseLine.LinePurchasePct = purchaseLineItemView.LinePurchasePct;
				purchaseLine.MethodOfPaymentGUID = purchaseLineItemView.MethodOfPaymentGUID.StringToGuidNull();
				purchaseLine.NetPurchaseAmount = purchaseLineItemView.NetPurchaseAmount;
				purchaseLine.NumberOfRollbill = purchaseLineItemView.NumberOfRollbill;
				purchaseLine.OriginalInterestDate = purchaseLineItemView.OriginalInterestDate.StringNullToDateNull();
				purchaseLine.OriginalPurchaseLineGUID = purchaseLineItemView.OriginalPurchaseLineGUID.StringToGuidNull();
				purchaseLine.OutstandingBuyerInvoiceAmount  = purchaseLineItemView.OutstandingBuyerInvoiceAmount ;
				purchaseLine.PurchaseAmount = purchaseLineItemView.PurchaseAmount;
				purchaseLine.PurchaseFeeAmount = purchaseLineItemView.PurchaseFeeAmount;
				purchaseLine.PurchaseFeeCalculateBase = purchaseLineItemView.PurchaseFeeCalculateBase;
				purchaseLine.PurchaseFeePct = purchaseLineItemView.PurchaseFeePct;
				purchaseLine.PurchaseLineInvoiceTableGUID  = purchaseLineItemView.PurchaseLineInvoiceTableGUID .StringToGuidNull();
				purchaseLine.PurchasePct = purchaseLineItemView.PurchasePct;
				purchaseLine.PurchaseTableGUID = purchaseLineItemView.PurchaseTableGUID.StringToGuid();
				purchaseLine.ReceiptFeeAmount = purchaseLineItemView.ReceiptFeeAmount;
				purchaseLine.RefPurchaseLineGUID = purchaseLineItemView.RefPurchaseLineGUID.StringToGuidNull();
				purchaseLine.ReserveAmount = purchaseLineItemView.ReserveAmount;
				purchaseLine.RetentionAmount = purchaseLineItemView.RetentionAmount;
				purchaseLine.RollbillInterestAmount = purchaseLineItemView.RollbillInterestAmount;
				purchaseLine.RollbillInterestDay = purchaseLineItemView.RollbillInterestDay;
				purchaseLine.RollbillInterestPct = purchaseLineItemView.RollbillInterestPct;
				purchaseLine.RollbillPurchaseLineGUID = purchaseLineItemView.RollbillPurchaseLineGUID.StringToGuidNull();
				purchaseLine.SettleBillingFeeAmount = purchaseLineItemView.SettleBillingFeeAmount;
				purchaseLine.SettleInterestAmount = purchaseLineItemView.SettleInterestAmount;
				purchaseLine.SettlePurchaseFeeAmount = purchaseLineItemView.SettlePurchaseFeeAmount;
				purchaseLine.SettleReceiptFeeAmount = purchaseLineItemView.SettleReceiptFeeAmount;
				purchaseLine.SettleRollBillInterestAmount = purchaseLineItemView.SettleRollBillInterestAmount;
				
				purchaseLine.RowVersion = purchaseLineItemView.RowVersion;
				return purchaseLine;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<PurchaseLine> ToPurchaseLine(this IEnumerable<PurchaseLineItemView> purchaseLineItemViews)
		{
			try
			{
				List<PurchaseLine> purchaseLines = new List<PurchaseLine>();
				foreach (PurchaseLineItemView item in purchaseLineItemViews)
				{
					purchaseLines.Add(item.ToPurchaseLine());
				}
				return purchaseLines;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static PurchaseLineItemView ToPurchaseLineItemView(this PurchaseLine purchaseLine)
		{
			try
			{
				PurchaseLineItemView purchaseLineItemView = new PurchaseLineItemView();
				purchaseLineItemView.CompanyGUID = purchaseLine.CompanyGUID.GuidNullToString();
				purchaseLineItemView.CreatedBy = purchaseLine.CreatedBy;
				purchaseLineItemView.CreatedDateTime = purchaseLine.CreatedDateTime.DateTimeToString();
				purchaseLineItemView.ModifiedBy = purchaseLine.ModifiedBy;
				purchaseLineItemView.ModifiedDateTime = purchaseLine.ModifiedDateTime.DateTimeToString();
				purchaseLineItemView.Owner = purchaseLine.Owner;
				purchaseLineItemView.OwnerBusinessUnitGUID = purchaseLine.OwnerBusinessUnitGUID.GuidNullToString();
				purchaseLineItemView.PurchaseLineGUID = purchaseLine.PurchaseLineGUID.GuidNullToString();
				purchaseLineItemView.AssignmentAgreementTableGUID = purchaseLine.AssignmentAgreementTableGUID.GuidNullToString();
				purchaseLineItemView.AssignmentAmount = purchaseLine.AssignmentAmount;
				purchaseLineItemView.BillingDate = purchaseLine.BillingDate.DateNullToString();
				purchaseLineItemView.BillingFeeAmount = purchaseLine.BillingFeeAmount;
				purchaseLineItemView.BuyerAgreementTableGUID = purchaseLine.BuyerAgreementTableGUID.GuidNullToString();
				purchaseLineItemView.BuyerInvoiceAmount = purchaseLine.BuyerInvoiceAmount;
				purchaseLineItemView.BuyerInvoiceTableGUID = purchaseLine.BuyerInvoiceTableGUID.GuidNullToString();
				purchaseLineItemView.BuyerPDCTableGUID = purchaseLine.BuyerPDCTableGUID.GuidNullToString();
				purchaseLineItemView.BuyerTableGUID = purchaseLine.BuyerTableGUID.GuidNullToString();
				purchaseLineItemView.ClosedForAdditionalPurchase = purchaseLine.ClosedForAdditionalPurchase;
				purchaseLineItemView.CollectionDate = purchaseLine.CollectionDate.DateToString();
				purchaseLineItemView.CreditAppLineGUID  = purchaseLine.CreditAppLineGUID .GuidNullToString();
				purchaseLineItemView.CustomerPDCTableGUID = purchaseLine.CustomerPDCTableGUID.GuidNullToString();
				purchaseLineItemView.Dimension1GUID = purchaseLine.Dimension1GUID.GuidNullToString();
				purchaseLineItemView.Dimension2GUID = purchaseLine.Dimension2GUID.GuidNullToString();
				purchaseLineItemView.Dimension3GUID = purchaseLine.Dimension3GUID.GuidNullToString();
				purchaseLineItemView.Dimension4GUID = purchaseLine.Dimension4GUID.GuidNullToString();
				purchaseLineItemView.Dimension5GUID = purchaseLine.Dimension5GUID.GuidNullToString();
				purchaseLineItemView.DueDate = purchaseLine.DueDate.DateToString();
				purchaseLineItemView.InterestAmount = purchaseLine.InterestAmount;
				purchaseLineItemView.InterestDate = purchaseLine.InterestDate.DateToString();
				purchaseLineItemView.InterestDay = purchaseLine.InterestDay;
				purchaseLineItemView.InterestRefundAmount = purchaseLine.InterestRefundAmount;
				purchaseLineItemView.LineNum = purchaseLine.LineNum;
				purchaseLineItemView.LinePurchaseAmount = purchaseLine.LinePurchaseAmount;
				purchaseLineItemView.LinePurchasePct = purchaseLine.LinePurchasePct;
				purchaseLineItemView.MethodOfPaymentGUID = purchaseLine.MethodOfPaymentGUID.GuidNullToString();
				purchaseLineItemView.NetPurchaseAmount = purchaseLine.NetPurchaseAmount;
				purchaseLineItemView.NumberOfRollbill = purchaseLine.NumberOfRollbill;
				purchaseLineItemView.OriginalInterestDate = purchaseLine.OriginalInterestDate.DateNullToString();
				purchaseLineItemView.OriginalPurchaseLineGUID = purchaseLine.OriginalPurchaseLineGUID.GuidNullToString();
				purchaseLineItemView.OutstandingBuyerInvoiceAmount  = purchaseLine.OutstandingBuyerInvoiceAmount ;
				purchaseLineItemView.PurchaseAmount = purchaseLine.PurchaseAmount;
				purchaseLineItemView.PurchaseFeeAmount = purchaseLine.PurchaseFeeAmount;
				purchaseLineItemView.PurchaseFeeCalculateBase = purchaseLine.PurchaseFeeCalculateBase;
				purchaseLineItemView.PurchaseFeePct = purchaseLine.PurchaseFeePct;
				purchaseLineItemView.PurchaseLineInvoiceTableGUID  = purchaseLine.PurchaseLineInvoiceTableGUID .GuidNullToString();
				purchaseLineItemView.PurchasePct = purchaseLine.PurchasePct;
				purchaseLineItemView.PurchaseTableGUID = purchaseLine.PurchaseTableGUID.GuidNullToString();
				purchaseLineItemView.ReceiptFeeAmount = purchaseLine.ReceiptFeeAmount;
				purchaseLineItemView.RefPurchaseLineGUID = purchaseLine.RefPurchaseLineGUID.GuidNullToString();
				purchaseLineItemView.ReserveAmount = purchaseLine.ReserveAmount;
				purchaseLineItemView.RetentionAmount = purchaseLine.RetentionAmount;
				purchaseLineItemView.RollbillInterestAmount = purchaseLine.RollbillInterestAmount;
				purchaseLineItemView.RollbillInterestDay = purchaseLine.RollbillInterestDay;
				purchaseLineItemView.RollbillInterestPct = purchaseLine.RollbillInterestPct;
				purchaseLineItemView.RollbillPurchaseLineGUID = purchaseLine.RollbillPurchaseLineGUID.GuidNullToString();
				purchaseLineItemView.SettleBillingFeeAmount = purchaseLine.SettleBillingFeeAmount;
				purchaseLineItemView.SettleInterestAmount = purchaseLine.SettleInterestAmount;
				purchaseLineItemView.SettlePurchaseFeeAmount = purchaseLine.SettlePurchaseFeeAmount;
				purchaseLineItemView.SettleReceiptFeeAmount = purchaseLine.SettleReceiptFeeAmount;
				purchaseLineItemView.SettleRollBillInterestAmount = purchaseLine.SettleRollBillInterestAmount;
				
				purchaseLineItemView.RowVersion = purchaseLine.RowVersion;
				return purchaseLineItemView.GetPurchaseLineItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion PurchaseLineItemView
		#region ToDropDown
		public static SelectItem<PurchaseLineItemView> ToDropDownItem(this PurchaseLineItemView purchaseLineView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<PurchaseLineItemView> selectItem = new SelectItem<PurchaseLineItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(purchaseLineView.PurchaseTable_PurchaseId, purchaseLineView.LineNum.ToString());
				selectItem.Value = purchaseLineView.PurchaseLineGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: purchaseLineView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<PurchaseLineItemView>> ToDropDownItem(this IEnumerable<PurchaseLineItemView> purchaseLineItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<PurchaseLineItemView>> selectItems = new List<SelectItem<PurchaseLineItemView>>();
				foreach (PurchaseLineItemView item in purchaseLineItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

