using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class IntercompanyHandler
	{
		#region IntercompanyListView
		public static List<IntercompanyListView> GetIntercompanyListViewValidation(this List<IntercompanyListView> intercompanyListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return intercompanyListViews;
			}
			var result = new List<IntercompanyListView>();
			try
			{
				foreach (IntercompanyListView item in intercompanyListViews)
				{
					result.Add(item.GetIntercompanyListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IntercompanyListView GetIntercompanyListViewValidation(this IntercompanyListView intercompanyListView)
		{
			try
			{
				intercompanyListView.RowAuthorize = intercompanyListView.GetListRowAuthorize();
				return intercompanyListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this IntercompanyListView intercompanyListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion IntercompanyListView
		#region IntercompanyItemView
		public static IntercompanyItemView GetIntercompanyItemViewValidation(this IntercompanyItemView intercompanyItemView)
		{
			try
			{
				intercompanyItemView.RowAuthorize = intercompanyItemView.GetItemRowAuthorize();
				return intercompanyItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this IntercompanyItemView intercompanyItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static Intercompany ToIntercompany(this IntercompanyItemView intercompanyItemView)
		{
			try
			{
				Intercompany intercompany = new Intercompany();
				intercompany.CompanyGUID = intercompanyItemView.CompanyGUID.StringToGuid();
				intercompany.CreatedBy = intercompanyItemView.CreatedBy;
				intercompany.CreatedDateTime = intercompanyItemView.CreatedDateTime.StringToSystemDateTime();
				intercompany.ModifiedBy = intercompanyItemView.ModifiedBy;
				intercompany.ModifiedDateTime = intercompanyItemView.ModifiedDateTime.StringToSystemDateTime();
				intercompany.Owner = intercompanyItemView.Owner;
				intercompany.OwnerBusinessUnitGUID = intercompanyItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				intercompany.IntercompanyGUID = intercompanyItemView.IntercompanyGUID.StringToGuid();
				intercompany.Description = intercompanyItemView.Description;
				intercompany.IntercompanyId = intercompanyItemView.IntercompanyId;
				
				intercompany.RowVersion = intercompanyItemView.RowVersion;
				return intercompany;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<Intercompany> ToIntercompany(this IEnumerable<IntercompanyItemView> intercompanyItemViews)
		{
			try
			{
				List<Intercompany> intercompanys = new List<Intercompany>();
				foreach (IntercompanyItemView item in intercompanyItemViews)
				{
					intercompanys.Add(item.ToIntercompany());
				}
				return intercompanys;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IntercompanyItemView ToIntercompanyItemView(this Intercompany intercompany)
		{
			try
			{
				IntercompanyItemView intercompanyItemView = new IntercompanyItemView();
				intercompanyItemView.CompanyGUID = intercompany.CompanyGUID.GuidNullToString();
				intercompanyItemView.CreatedBy = intercompany.CreatedBy;
				intercompanyItemView.CreatedDateTime = intercompany.CreatedDateTime.DateTimeToString();
				intercompanyItemView.ModifiedBy = intercompany.ModifiedBy;
				intercompanyItemView.ModifiedDateTime = intercompany.ModifiedDateTime.DateTimeToString();
				intercompanyItemView.Owner = intercompany.Owner;
				intercompanyItemView.OwnerBusinessUnitGUID = intercompany.OwnerBusinessUnitGUID.GuidNullToString();
				intercompanyItemView.IntercompanyGUID = intercompany.IntercompanyGUID.GuidNullToString();
				intercompanyItemView.Description = intercompany.Description;
				intercompanyItemView.IntercompanyId = intercompany.IntercompanyId;
				
				intercompanyItemView.RowVersion = intercompany.RowVersion;
				return intercompanyItemView.GetIntercompanyItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion IntercompanyItemView
		#region ToDropDown
		public static SelectItem<IntercompanyItemView> ToDropDownItem(this IntercompanyItemView intercompanyView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<IntercompanyItemView> selectItem = new SelectItem<IntercompanyItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(intercompanyView.IntercompanyId, intercompanyView.Description);
				selectItem.Value = intercompanyView.IntercompanyGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: intercompanyView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<IntercompanyItemView>> ToDropDownItem(this IEnumerable<IntercompanyItemView> intercompanyItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<IntercompanyItemView>> selectItems = new List<SelectItem<IntercompanyItemView>>();
				foreach (IntercompanyItemView item in intercompanyItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

