using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class BuyerAgreementLineHandler
	{
		#region BuyerAgreementLineListView
		public static List<BuyerAgreementLineListView> GetBuyerAgreementLineListViewValidation(this List<BuyerAgreementLineListView> buyerAgreementLineListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return buyerAgreementLineListViews;
			}
			var result = new List<BuyerAgreementLineListView>();
			try
			{
				foreach (BuyerAgreementLineListView item in buyerAgreementLineListViews)
				{
					result.Add(item.GetBuyerAgreementLineListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BuyerAgreementLineListView GetBuyerAgreementLineListViewValidation(this BuyerAgreementLineListView buyerAgreementLineListView)
		{
			try
			{
				buyerAgreementLineListView.RowAuthorize = buyerAgreementLineListView.GetListRowAuthorize();
				return buyerAgreementLineListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this BuyerAgreementLineListView buyerAgreementLineListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion BuyerAgreementLineListView
		#region BuyerAgreementLineItemView
		public static BuyerAgreementLineItemView GetBuyerAgreementLineItemViewValidation(this BuyerAgreementLineItemView buyerAgreementLineItemView)
		{
			try
			{
				buyerAgreementLineItemView.RowAuthorize = buyerAgreementLineItemView.GetItemRowAuthorize();
				return buyerAgreementLineItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this BuyerAgreementLineItemView buyerAgreementLineItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static BuyerAgreementLine ToBuyerAgreementLine(this BuyerAgreementLineItemView buyerAgreementLineItemView)
		{
			try
			{
				BuyerAgreementLine buyerAgreementLine = new BuyerAgreementLine();
				buyerAgreementLine.CompanyGUID = buyerAgreementLineItemView.CompanyGUID.StringToGuid();
				buyerAgreementLine.CreatedBy = buyerAgreementLineItemView.CreatedBy;
				buyerAgreementLine.CreatedDateTime = buyerAgreementLineItemView.CreatedDateTime.StringToSystemDateTime();
				buyerAgreementLine.ModifiedBy = buyerAgreementLineItemView.ModifiedBy;
				buyerAgreementLine.ModifiedDateTime = buyerAgreementLineItemView.ModifiedDateTime.StringToSystemDateTime();
				buyerAgreementLine.Owner = buyerAgreementLineItemView.Owner;
				buyerAgreementLine.OwnerBusinessUnitGUID = buyerAgreementLineItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				buyerAgreementLine.BuyerAgreementLineGUID = buyerAgreementLineItemView.BuyerAgreementLineGUID.StringToGuid();
				buyerAgreementLine.AlreadyInvoiced = buyerAgreementLineItemView.AlreadyInvoiced;
				buyerAgreementLine.Amount = buyerAgreementLineItemView.Amount;
				buyerAgreementLine.BuyerAgreementTableGUID = buyerAgreementLineItemView.BuyerAgreementTableGUID.StringToGuid();
				buyerAgreementLine.Description = buyerAgreementLineItemView.Description;
				buyerAgreementLine.DueDate = buyerAgreementLineItemView.DueDate.StringNullToDateNull();
				buyerAgreementLine.Period = buyerAgreementLineItemView.Period;
				buyerAgreementLine.Remark = buyerAgreementLineItemView.Remark;
				
				buyerAgreementLine.RowVersion = buyerAgreementLineItemView.RowVersion;
				return buyerAgreementLine;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<BuyerAgreementLine> ToBuyerAgreementLine(this IEnumerable<BuyerAgreementLineItemView> buyerAgreementLineItemViews)
		{
			try
			{
				List<BuyerAgreementLine> buyerAgreementLines = new List<BuyerAgreementLine>();
				foreach (BuyerAgreementLineItemView item in buyerAgreementLineItemViews)
				{
					buyerAgreementLines.Add(item.ToBuyerAgreementLine());
				}
				return buyerAgreementLines;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BuyerAgreementLineItemView ToBuyerAgreementLineItemView(this BuyerAgreementLine buyerAgreementLine)
		{
			try
			{
				BuyerAgreementLineItemView buyerAgreementLineItemView = new BuyerAgreementLineItemView();
				buyerAgreementLineItemView.CompanyGUID = buyerAgreementLine.CompanyGUID.GuidNullToString();
				buyerAgreementLineItemView.CreatedBy = buyerAgreementLine.CreatedBy;
				buyerAgreementLineItemView.CreatedDateTime = buyerAgreementLine.CreatedDateTime.DateTimeToString();
				buyerAgreementLineItemView.ModifiedBy = buyerAgreementLine.ModifiedBy;
				buyerAgreementLineItemView.ModifiedDateTime = buyerAgreementLine.ModifiedDateTime.DateTimeToString();
				buyerAgreementLineItemView.Owner = buyerAgreementLine.Owner;
				buyerAgreementLineItemView.OwnerBusinessUnitGUID = buyerAgreementLine.OwnerBusinessUnitGUID.GuidNullToString();
				buyerAgreementLineItemView.BuyerAgreementLineGUID = buyerAgreementLine.BuyerAgreementLineGUID.GuidNullToString();
				buyerAgreementLineItemView.AlreadyInvoiced = buyerAgreementLine.AlreadyInvoiced;
				buyerAgreementLineItemView.Amount = buyerAgreementLine.Amount;
				buyerAgreementLineItemView.BuyerAgreementTableGUID = buyerAgreementLine.BuyerAgreementTableGUID.GuidNullToString();
				buyerAgreementLineItemView.Description = buyerAgreementLine.Description;
				buyerAgreementLineItemView.DueDate = buyerAgreementLine.DueDate.DateNullToString();
				buyerAgreementLineItemView.Period = buyerAgreementLine.Period;
				buyerAgreementLineItemView.Remark = buyerAgreementLine.Remark;
				
				buyerAgreementLineItemView.RowVersion = buyerAgreementLine.RowVersion;
				return buyerAgreementLineItemView.GetBuyerAgreementLineItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion BuyerAgreementLineItemView
		#region ToDropDown
		public static SelectItem<BuyerAgreementLineItemView> ToDropDownItem(this BuyerAgreementLineItemView buyerAgreementLineView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<BuyerAgreementLineItemView> selectItem = new SelectItem<BuyerAgreementLineItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(buyerAgreementLineView.Period.ToString(), buyerAgreementLineView.Description);
				selectItem.Value = buyerAgreementLineView.BuyerAgreementLineGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: buyerAgreementLineView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<BuyerAgreementLineItemView>> ToDropDownItem(this IEnumerable<BuyerAgreementLineItemView> buyerAgreementLineItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<BuyerAgreementLineItemView>> selectItems = new List<SelectItem<BuyerAgreementLineItemView>>();
				foreach (BuyerAgreementLineItemView item in buyerAgreementLineItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

