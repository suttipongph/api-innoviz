using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class BusinessUnitHandler
	{
		#region BusinessUnitListView
		public static List<BusinessUnitListView> GetBusinessUnitListViewValidation(this List<BusinessUnitListView> businessUnitListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return businessUnitListViews;
			}
			var result = new List<BusinessUnitListView>();
			try
			{
				foreach (BusinessUnitListView item in businessUnitListViews)
				{
					result.Add(item.GetBusinessUnitListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BusinessUnitListView GetBusinessUnitListViewValidation(this BusinessUnitListView businessUnitListView)
		{
			try
			{
				businessUnitListView.RowAuthorize = businessUnitListView.GetListRowAuthorize();
				return businessUnitListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this BusinessUnitListView businessUnitListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion BusinessUnitListView
		#region BusinessUnitItemView
		public static BusinessUnitItemView GetBusinessUnitItemViewValidation(this BusinessUnitItemView businessUnitItemView)
		{
			try
			{
				businessUnitItemView.RowAuthorize = businessUnitItemView.GetItemRowAuthorize();
				return businessUnitItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this BusinessUnitItemView businessUnitItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static BusinessUnit ToBusinessUnit(this BusinessUnitItemView businessUnitItemView)
		{
			try
			{
				BusinessUnit businessUnit = new BusinessUnit();
				businessUnit.CompanyGUID = businessUnitItemView.CompanyGUID.StringToGuid();
				businessUnit.CreatedBy = businessUnitItemView.CreatedBy;
				businessUnit.CreatedDateTime = businessUnitItemView.CreatedDateTime.StringToSystemDateTime();
				businessUnit.ModifiedBy = businessUnitItemView.ModifiedBy;
				businessUnit.ModifiedDateTime = businessUnitItemView.ModifiedDateTime.StringToSystemDateTime();
				businessUnit.Owner = businessUnitItemView.Owner;
				businessUnit.OwnerBusinessUnitGUID = businessUnitItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				businessUnit.BusinessUnitGUID = businessUnitItemView.BusinessUnitGUID.StringToGuid();
				businessUnit.BusinessUnitId = businessUnitItemView.BusinessUnitId;
				businessUnit.Description = businessUnitItemView.Description;
				businessUnit.ParentBusinessUnitGUID = businessUnitItemView.ParentBusinessUnitGUID.StringToGuidNull();
				
				businessUnit.RowVersion = businessUnitItemView.RowVersion;
				return businessUnit;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<BusinessUnit> ToBusinessUnit(this IEnumerable<BusinessUnitItemView> businessUnitItemViews)
		{
			try
			{
				List<BusinessUnit> businessUnits = new List<BusinessUnit>();
				foreach (BusinessUnitItemView item in businessUnitItemViews)
				{
					businessUnits.Add(item.ToBusinessUnit());
				}
				return businessUnits;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BusinessUnitItemView ToBusinessUnitItemView(this BusinessUnit businessUnit)
		{
			try
			{
				BusinessUnitItemView businessUnitItemView = new BusinessUnitItemView();
				businessUnitItemView.CompanyGUID = businessUnit.CompanyGUID.GuidNullToString();
				businessUnitItemView.CreatedBy = businessUnit.CreatedBy;
				businessUnitItemView.CreatedDateTime = businessUnit.CreatedDateTime.DateTimeToString();
				businessUnitItemView.ModifiedBy = businessUnit.ModifiedBy;
				businessUnitItemView.ModifiedDateTime = businessUnit.ModifiedDateTime.DateTimeToString();
				businessUnitItemView.Owner = businessUnit.Owner;
				businessUnitItemView.OwnerBusinessUnitGUID = businessUnit.OwnerBusinessUnitGUID.GuidNullToString();
				businessUnitItemView.BusinessUnitGUID = businessUnit.BusinessUnitGUID.GuidNullToString();
				businessUnitItemView.BusinessUnitId = businessUnit.BusinessUnitId;
				businessUnitItemView.Description = businessUnit.Description;
				businessUnitItemView.ParentBusinessUnitGUID = businessUnit.ParentBusinessUnitGUID.GuidNullToString();
				
				businessUnitItemView.RowVersion = businessUnit.RowVersion;
				return businessUnitItemView.GetBusinessUnitItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion BusinessUnitItemView
		#region ToDropDown
		public static SelectItem<BusinessUnitItemView> ToDropDownItem(this BusinessUnitItemView businessUnitView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<BusinessUnitItemView> selectItem = new SelectItem<BusinessUnitItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(businessUnitView.BusinessUnitId, businessUnitView.Description);
				selectItem.Value = businessUnitView.BusinessUnitGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: businessUnitView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<BusinessUnitItemView>> ToDropDownItem(this IEnumerable<BusinessUnitItemView> businessUnitItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<BusinessUnitItemView>> selectItems = new List<SelectItem<BusinessUnitItemView>>();
				foreach (BusinessUnitItemView item in businessUnitItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

