using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CustGroupHandler
	{
		#region CustGroupListView
		public static List<CustGroupListView> GetCustGroupListViewValidation(this List<CustGroupListView> custGroupListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return custGroupListViews;
			}
			var result = new List<CustGroupListView>();
			try
			{
				foreach (CustGroupListView item in custGroupListViews)
				{
					result.Add(item.GetCustGroupListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CustGroupListView GetCustGroupListViewValidation(this CustGroupListView custGroupListView)
		{
			try
			{
				custGroupListView.RowAuthorize = custGroupListView.GetListRowAuthorize();
				return custGroupListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CustGroupListView custGroupListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CustGroupListView
		#region CustGroupItemView
		public static CustGroupItemView GetCustGroupItemViewValidation(this CustGroupItemView custGroupItemView)
		{
			try
			{
				custGroupItemView.RowAuthorize = custGroupItemView.GetItemRowAuthorize();
				return custGroupItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CustGroupItemView custGroupItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CustGroup ToCustGroup(this CustGroupItemView custGroupItemView)
		{
			try
			{
				CustGroup custGroup = new CustGroup();
				custGroup.CompanyGUID = custGroupItemView.CompanyGUID.StringToGuid();
				custGroup.CreatedBy = custGroupItemView.CreatedBy;
				custGroup.CreatedDateTime = custGroupItemView.CreatedDateTime.StringToSystemDateTime();
				custGroup.ModifiedBy = custGroupItemView.ModifiedBy;
				custGroup.ModifiedDateTime = custGroupItemView.ModifiedDateTime.StringToSystemDateTime();
				custGroup.Owner = custGroupItemView.Owner;
				custGroup.OwnerBusinessUnitGUID = custGroupItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				custGroup.CustGroupGUID = custGroupItemView.CustGroupGUID.StringToGuid();
				custGroup.CustGroupId = custGroupItemView.CustGroupId;
				custGroup.Description = custGroupItemView.Description;
				custGroup.NumberSeqTableGUID = custGroupItemView.NumberSeqTableGUID.StringToGuid();
				
				custGroup.RowVersion = custGroupItemView.RowVersion;
				return custGroup;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CustGroup> ToCustGroup(this IEnumerable<CustGroupItemView> custGroupItemViews)
		{
			try
			{
				List<CustGroup> custGroups = new List<CustGroup>();
				foreach (CustGroupItemView item in custGroupItemViews)
				{
					custGroups.Add(item.ToCustGroup());
				}
				return custGroups;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CustGroupItemView ToCustGroupItemView(this CustGroup custGroup)
		{
			try
			{
				CustGroupItemView custGroupItemView = new CustGroupItemView();
				custGroupItemView.CompanyGUID = custGroup.CompanyGUID.GuidNullToString();
				custGroupItemView.CreatedBy = custGroup.CreatedBy;
				custGroupItemView.CreatedDateTime = custGroup.CreatedDateTime.DateTimeToString();
				custGroupItemView.ModifiedBy = custGroup.ModifiedBy;
				custGroupItemView.ModifiedDateTime = custGroup.ModifiedDateTime.DateTimeToString();
				custGroupItemView.Owner = custGroup.Owner;
				custGroupItemView.OwnerBusinessUnitGUID = custGroup.OwnerBusinessUnitGUID.GuidNullToString();
				custGroupItemView.CustGroupGUID = custGroup.CustGroupGUID.GuidNullToString();
				custGroupItemView.CustGroupId = custGroup.CustGroupId;
				custGroupItemView.Description = custGroup.Description;
				custGroupItemView.NumberSeqTableGUID = custGroup.NumberSeqTableGUID.GuidNullToString();
				
				custGroupItemView.RowVersion = custGroup.RowVersion;
				return custGroupItemView.GetCustGroupItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CustGroupItemView
		#region ToDropDown
		public static SelectItem<CustGroupItemView> ToDropDownItem(this CustGroupItemView custGroupView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CustGroupItemView> selectItem = new SelectItem<CustGroupItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(custGroupView.CustGroupId, custGroupView.Description);
				selectItem.Value = custGroupView.CustGroupGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: custGroupView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CustGroupItemView>> ToDropDownItem(this IEnumerable<CustGroupItemView> custGroupItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CustGroupItemView>> selectItems = new List<SelectItem<CustGroupItemView>>();
				foreach (CustGroupItemView item in custGroupItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

