using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class FinancialStatementTransHandler
	{
		#region FinancialStatementTransListView
		public static List<FinancialStatementTransListView> GetFinancialStatementTransListViewValidation(this List<FinancialStatementTransListView> financialStatementTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return financialStatementTransListViews;
			}
			var result = new List<FinancialStatementTransListView>();
			try
			{
				foreach (FinancialStatementTransListView item in financialStatementTransListViews)
				{
					result.Add(item.GetFinancialStatementTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static FinancialStatementTransListView GetFinancialStatementTransListViewValidation(this FinancialStatementTransListView financialStatementTransListView)
		{
			try
			{
				financialStatementTransListView.RowAuthorize = financialStatementTransListView.GetListRowAuthorize();
				return financialStatementTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this FinancialStatementTransListView financialStatementTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion FinancialStatementTransListView
		#region FinancialStatementTransItemView
		public static FinancialStatementTransItemView GetFinancialStatementTransItemViewValidation(this FinancialStatementTransItemView financialStatementTransItemView)
		{
			try
			{
				financialStatementTransItemView.RowAuthorize = financialStatementTransItemView.GetItemRowAuthorize();
				return financialStatementTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this FinancialStatementTransItemView financialStatementTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static FinancialStatementTrans ToFinancialStatementTrans(this FinancialStatementTransItemView financialStatementTransItemView)
		{
			try
			{
				FinancialStatementTrans financialStatementTrans = new FinancialStatementTrans();
				financialStatementTrans.CompanyGUID = financialStatementTransItemView.CompanyGUID.StringToGuid();
				financialStatementTrans.CreatedBy = financialStatementTransItemView.CreatedBy;
				financialStatementTrans.CreatedDateTime = financialStatementTransItemView.CreatedDateTime.StringToSystemDateTime();
				financialStatementTrans.ModifiedBy = financialStatementTransItemView.ModifiedBy;
				financialStatementTrans.ModifiedDateTime = financialStatementTransItemView.ModifiedDateTime.StringToSystemDateTime();
				financialStatementTrans.Owner = financialStatementTransItemView.Owner;
				financialStatementTrans.OwnerBusinessUnitGUID = financialStatementTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				financialStatementTrans.FinancialStatementTransGUID = financialStatementTransItemView.FinancialStatementTransGUID.StringToGuid();
				financialStatementTrans.Amount = financialStatementTransItemView.Amount;
				financialStatementTrans.Description = financialStatementTransItemView.Description;
				financialStatementTrans.Ordering = financialStatementTransItemView.Ordering;
				financialStatementTrans.RefGUID = financialStatementTransItemView.RefGUID.StringToGuid();
				financialStatementTrans.RefType = financialStatementTransItemView.RefType;
				financialStatementTrans.Year = financialStatementTransItemView.Year;
				
				financialStatementTrans.RowVersion = financialStatementTransItemView.RowVersion;
				return financialStatementTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<FinancialStatementTrans> ToFinancialStatementTrans(this IEnumerable<FinancialStatementTransItemView> financialStatementTransItemViews)
		{
			try
			{
				List<FinancialStatementTrans> financialStatementTranss = new List<FinancialStatementTrans>();
				foreach (FinancialStatementTransItemView item in financialStatementTransItemViews)
				{
					financialStatementTranss.Add(item.ToFinancialStatementTrans());
				}
				return financialStatementTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static FinancialStatementTransItemView ToFinancialStatementTransItemView(this FinancialStatementTrans financialStatementTrans)
		{
			try
			{
				FinancialStatementTransItemView financialStatementTransItemView = new FinancialStatementTransItemView();
				financialStatementTransItemView.CompanyGUID = financialStatementTrans.CompanyGUID.GuidNullToString();
				financialStatementTransItemView.CreatedBy = financialStatementTrans.CreatedBy;
				financialStatementTransItemView.CreatedDateTime = financialStatementTrans.CreatedDateTime.DateTimeToString();
				financialStatementTransItemView.ModifiedBy = financialStatementTrans.ModifiedBy;
				financialStatementTransItemView.ModifiedDateTime = financialStatementTrans.ModifiedDateTime.DateTimeToString();
				financialStatementTransItemView.Owner = financialStatementTrans.Owner;
				financialStatementTransItemView.OwnerBusinessUnitGUID = financialStatementTrans.OwnerBusinessUnitGUID.GuidNullToString();
				financialStatementTransItemView.FinancialStatementTransGUID = financialStatementTrans.FinancialStatementTransGUID.GuidNullToString();
				financialStatementTransItemView.Amount = financialStatementTrans.Amount;
				financialStatementTransItemView.Description = financialStatementTrans.Description;
				financialStatementTransItemView.Ordering = financialStatementTrans.Ordering;
				financialStatementTransItemView.RefGUID = financialStatementTrans.RefGUID.GuidNullToString();
				financialStatementTransItemView.RefType = financialStatementTrans.RefType;
				financialStatementTransItemView.Year = financialStatementTrans.Year;
				
				financialStatementTransItemView.RowVersion = financialStatementTrans.RowVersion;
				return financialStatementTransItemView.GetFinancialStatementTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion FinancialStatementTransItemView
		#region ToDropDown
		public static SelectItem<FinancialStatementTransItemView> ToDropDownItem(this FinancialStatementTransItemView financialStatementTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<FinancialStatementTransItemView> selectItem = new SelectItem<FinancialStatementTransItemView>();
				selectItem.Label = null;
				selectItem.Value = financialStatementTransView.FinancialStatementTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: financialStatementTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<FinancialStatementTransItemView>> ToDropDownItem(this IEnumerable<FinancialStatementTransItemView> financialStatementTransItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<FinancialStatementTransItemView>> selectItems = new List<SelectItem<FinancialStatementTransItemView>>();
				foreach (FinancialStatementTransItemView item in financialStatementTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

