using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class InterestRealizedTransHandler
	{
		#region InterestRealizedTransListView
		public static List<InterestRealizedTransListView> GetInterestRealizedTransListViewValidation(this List<InterestRealizedTransListView> interestRealizedTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return interestRealizedTransListViews;
			}
			var result = new List<InterestRealizedTransListView>();
			try
			{
				foreach (InterestRealizedTransListView item in interestRealizedTransListViews)
				{
					result.Add(item.GetInterestRealizedTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static InterestRealizedTransListView GetInterestRealizedTransListViewValidation(this InterestRealizedTransListView interestRealizedTransListView)
		{
			try
			{
				interestRealizedTransListView.RowAuthorize = interestRealizedTransListView.GetListRowAuthorize();
				return interestRealizedTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this InterestRealizedTransListView interestRealizedTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion InterestRealizedTransListView
		#region InterestRealizedTransItemView
		public static InterestRealizedTransItemView GetInterestRealizedTransItemViewValidation(this InterestRealizedTransItemView interestRealizedTransItemView)
		{
			try
			{
				interestRealizedTransItemView.RowAuthorize = interestRealizedTransItemView.GetItemRowAuthorize();
				return interestRealizedTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this InterestRealizedTransItemView interestRealizedTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static InterestRealizedTrans ToInterestRealizedTrans(this InterestRealizedTransItemView interestRealizedTransItemView)
		{
			try
			{
				InterestRealizedTrans interestRealizedTrans = new InterestRealizedTrans();
				interestRealizedTrans.CompanyGUID = interestRealizedTransItemView.CompanyGUID.StringToGuid();
				interestRealizedTrans.CreatedBy = interestRealizedTransItemView.CreatedBy;
				interestRealizedTrans.CreatedDateTime = interestRealizedTransItemView.CreatedDateTime.StringToSystemDateTime();
				interestRealizedTrans.ModifiedBy = interestRealizedTransItemView.ModifiedBy;
				interestRealizedTrans.ModifiedDateTime = interestRealizedTransItemView.ModifiedDateTime.StringToSystemDateTime();
				interestRealizedTrans.Owner = interestRealizedTransItemView.Owner;
				interestRealizedTrans.OwnerBusinessUnitGUID = interestRealizedTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				interestRealizedTrans.InterestRealizedTransGUID = interestRealizedTransItemView.InterestRealizedTransGUID.StringToGuid();
				interestRealizedTrans.AccInterestAmount = interestRealizedTransItemView.AccInterestAmount;
				interestRealizedTrans.AccountingDate = interestRealizedTransItemView.AccountingDate.StringToDate();
				interestRealizedTrans.Accrued = interestRealizedTransItemView.Accrued;
				interestRealizedTrans.DocumentId = interestRealizedTransItemView.DocumentId;
				interestRealizedTrans.EndDate = interestRealizedTransItemView.EndDate.StringToDate();
				interestRealizedTrans.InterestDay = interestRealizedTransItemView.InterestDay;
				interestRealizedTrans.InterestPerDay = interestRealizedTransItemView.InterestPerDay;
				interestRealizedTrans.LineNum = interestRealizedTransItemView.LineNum;
				interestRealizedTrans.ProductType = interestRealizedTransItemView.ProductType;
				interestRealizedTrans.RefGUID = interestRealizedTransItemView.RefGUID.StringToGuid();
				interestRealizedTrans.RefType = interestRealizedTransItemView.RefType;
				interestRealizedTrans.StartDate = interestRealizedTransItemView.StartDate.StringToDate();
				interestRealizedTrans.Cancelled = interestRealizedTransItemView.Cancelled;
				interestRealizedTrans.RefProcessTransGUID = interestRealizedTransItemView.RefProcessTransGUID.StringToGuidNull();
				interestRealizedTrans.Dimension1GUID = interestRealizedTransItemView.Dimension1GUID.StringToGuidNull();
				interestRealizedTrans.Dimension2GUID = interestRealizedTransItemView.Dimension2GUID.StringToGuidNull();
				interestRealizedTrans.Dimension3GUID = interestRealizedTransItemView.Dimension3GUID.StringToGuidNull();
				interestRealizedTrans.Dimension4GUID = interestRealizedTransItemView.Dimension4GUID.StringToGuidNull();
				interestRealizedTrans.Dimension5GUID = interestRealizedTransItemView.Dimension5GUID.StringToGuidNull();

				
				interestRealizedTrans.RowVersion = interestRealizedTransItemView.RowVersion;
				return interestRealizedTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<InterestRealizedTrans> ToInterestRealizedTrans(this IEnumerable<InterestRealizedTransItemView> interestRealizedTransItemViews)
		{
			try
			{
				List<InterestRealizedTrans> interestRealizedTranss = new List<InterestRealizedTrans>();
				foreach (InterestRealizedTransItemView item in interestRealizedTransItemViews)
				{
					interestRealizedTranss.Add(item.ToInterestRealizedTrans());
				}
				return interestRealizedTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static InterestRealizedTransItemView ToInterestRealizedTransItemView(this InterestRealizedTrans interestRealizedTrans)
		{
			try
			{
				InterestRealizedTransItemView interestRealizedTransItemView = new InterestRealizedTransItemView();
				interestRealizedTransItemView.CompanyGUID = interestRealizedTrans.CompanyGUID.GuidNullToString();
				interestRealizedTransItemView.CreatedBy = interestRealizedTrans.CreatedBy;
				interestRealizedTransItemView.CreatedDateTime = interestRealizedTrans.CreatedDateTime.DateTimeToString();
				interestRealizedTransItemView.ModifiedBy = interestRealizedTrans.ModifiedBy;
				interestRealizedTransItemView.ModifiedDateTime = interestRealizedTrans.ModifiedDateTime.DateTimeToString();
				interestRealizedTransItemView.Owner = interestRealizedTrans.Owner;
				interestRealizedTransItemView.OwnerBusinessUnitGUID = interestRealizedTrans.OwnerBusinessUnitGUID.GuidNullToString();
				interestRealizedTransItemView.InterestRealizedTransGUID = interestRealizedTrans.InterestRealizedTransGUID.GuidNullToString();
				interestRealizedTransItemView.AccInterestAmount = interestRealizedTrans.AccInterestAmount;
				interestRealizedTransItemView.AccountingDate = interestRealizedTrans.AccountingDate.DateToString();
				interestRealizedTransItemView.Accrued = interestRealizedTrans.Accrued;
				interestRealizedTransItemView.DocumentId = interestRealizedTrans.DocumentId;
				interestRealizedTransItemView.EndDate = interestRealizedTrans.EndDate.DateToString();
				interestRealizedTransItemView.InterestDay = interestRealizedTrans.InterestDay;
				interestRealizedTransItemView.InterestPerDay = interestRealizedTrans.InterestPerDay;
				interestRealizedTransItemView.LineNum = interestRealizedTrans.LineNum;
				interestRealizedTransItemView.ProductType = interestRealizedTrans.ProductType;
				interestRealizedTransItemView.RefGUID = interestRealizedTrans.RefGUID.GuidNullToString();
				interestRealizedTransItemView.RefType = interestRealizedTrans.RefType;
				interestRealizedTransItemView.StartDate = interestRealizedTrans.StartDate.DateToString();
				interestRealizedTransItemView.Cancelled = interestRealizedTrans.Cancelled;
				interestRealizedTransItemView.RefProcessTransGUID = interestRealizedTrans.RefProcessTransGUID.GuidNullToString();
				interestRealizedTransItemView.Dimension1GUID = interestRealizedTrans.Dimension1GUID.GuidNullToString();
				interestRealizedTransItemView.Dimension2GUID = interestRealizedTrans.Dimension2GUID.GuidNullToString();
				interestRealizedTransItemView.Dimension3GUID = interestRealizedTrans.Dimension3GUID.GuidNullToString();
				interestRealizedTransItemView.Dimension4GUID = interestRealizedTrans.Dimension4GUID.GuidNullToString();
				interestRealizedTransItemView.Dimension5GUID = interestRealizedTrans.Dimension5GUID.GuidNullToString();

				
				interestRealizedTransItemView.RowVersion = interestRealizedTrans.RowVersion;
				return interestRealizedTransItemView.GetInterestRealizedTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion InterestRealizedTransItemView
		#region ToDropDown
		public static SelectItem<InterestRealizedTransItemView> ToDropDownItem(this InterestRealizedTransItemView interestRealizedTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<InterestRealizedTransItemView> selectItem = new SelectItem<InterestRealizedTransItemView>();
				selectItem.Label = null;
				selectItem.Value = interestRealizedTransView.InterestRealizedTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: interestRealizedTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<InterestRealizedTransItemView>> ToDropDownItem(this IEnumerable<InterestRealizedTransItemView> interestRealizedTransItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<InterestRealizedTransItemView>> selectItems = new List<SelectItem<InterestRealizedTransItemView>>();
				foreach (InterestRealizedTransItemView item in interestRealizedTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

