using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class NationalityHandler
	{
		#region NationalityListView
		public static List<NationalityListView> GetNationalityListViewValidation(this List<NationalityListView> nationalityListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return nationalityListViews;
			}
			var result = new List<NationalityListView>();
			try
			{
				foreach (NationalityListView item in nationalityListViews)
				{
					result.Add(item.GetNationalityListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static NationalityListView GetNationalityListViewValidation(this NationalityListView nationalityListView)
		{
			try
			{
				nationalityListView.RowAuthorize = nationalityListView.GetListRowAuthorize();
				return nationalityListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this NationalityListView nationalityListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion NationalityListView
		#region NationalityItemView
		public static NationalityItemView GetNationalityItemViewValidation(this NationalityItemView nationalityItemView)
		{
			try
			{
				nationalityItemView.RowAuthorize = nationalityItemView.GetItemRowAuthorize();
				return nationalityItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this NationalityItemView nationalityItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static Nationality ToNationality(this NationalityItemView nationalityItemView)
		{
			try
			{
				Nationality nationality = new Nationality();
				nationality.CompanyGUID = nationalityItemView.CompanyGUID.StringToGuid();
				nationality.CreatedBy = nationalityItemView.CreatedBy;
				nationality.CreatedDateTime = nationalityItemView.CreatedDateTime.StringToSystemDateTime();
				nationality.ModifiedBy = nationalityItemView.ModifiedBy;
				nationality.ModifiedDateTime = nationalityItemView.ModifiedDateTime.StringToSystemDateTime();
				nationality.Owner = nationalityItemView.Owner;
				nationality.OwnerBusinessUnitGUID = nationalityItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				nationality.NationalityGUID = nationalityItemView.NationalityGUID.StringToGuid();
				nationality.Description = nationalityItemView.Description;
				nationality.NationalityId = nationalityItemView.NationalityId;
				
				nationality.RowVersion = nationalityItemView.RowVersion;
				return nationality;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<Nationality> ToNationality(this IEnumerable<NationalityItemView> nationalityItemViews)
		{
			try
			{
				List<Nationality> nationalitys = new List<Nationality>();
				foreach (NationalityItemView item in nationalityItemViews)
				{
					nationalitys.Add(item.ToNationality());
				}
				return nationalitys;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static NationalityItemView ToNationalityItemView(this Nationality nationality)
		{
			try
			{
				NationalityItemView nationalityItemView = new NationalityItemView();
				nationalityItemView.CompanyGUID = nationality.CompanyGUID.GuidNullToString();
				nationalityItemView.CreatedBy = nationality.CreatedBy;
				nationalityItemView.CreatedDateTime = nationality.CreatedDateTime.DateTimeToString();
				nationalityItemView.ModifiedBy = nationality.ModifiedBy;
				nationalityItemView.ModifiedDateTime = nationality.ModifiedDateTime.DateTimeToString();
				nationalityItemView.Owner = nationality.Owner;
				nationalityItemView.OwnerBusinessUnitGUID = nationality.OwnerBusinessUnitGUID.GuidNullToString();
				nationalityItemView.NationalityGUID = nationality.NationalityGUID.GuidNullToString();
				nationalityItemView.Description = nationality.Description;
				nationalityItemView.NationalityId = nationality.NationalityId;
				
				nationalityItemView.RowVersion = nationality.RowVersion;
				return nationalityItemView.GetNationalityItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion NationalityItemView
		#region ToDropDown
		public static SelectItem<NationalityItemView> ToDropDownItem(this NationalityItemView nationalityView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<NationalityItemView> selectItem = new SelectItem<NationalityItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(nationalityView.NationalityId, nationalityView.Description);
				selectItem.Value = nationalityView.NationalityGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: nationalityView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<NationalityItemView>> ToDropDownItem(this IEnumerable<NationalityItemView> nationalityItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<NationalityItemView>> selectItems = new List<SelectItem<NationalityItemView>>();
				foreach (NationalityItemView item in nationalityItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

