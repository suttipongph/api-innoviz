using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class AddressDistrictHandler
	{
		#region AddressDistrictListView
		public static List<AddressDistrictListView> GetAddressDistrictListViewValidation(this List<AddressDistrictListView> addressDistrictListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return addressDistrictListViews;
			}
			var result = new List<AddressDistrictListView>();
			try
			{
				foreach (AddressDistrictListView item in addressDistrictListViews)
				{
					result.Add(item.GetAddressDistrictListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AddressDistrictListView GetAddressDistrictListViewValidation(this AddressDistrictListView addressDistrictListView)
		{
			try
			{
				addressDistrictListView.RowAuthorize = addressDistrictListView.GetListRowAuthorize();
				return addressDistrictListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this AddressDistrictListView addressDistrictListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion AddressDistrictListView
		#region AddressDistrictItemView
		public static AddressDistrictItemView GetAddressDistrictItemViewValidation(this AddressDistrictItemView addressDistrictItemView)
		{
			try
			{
				addressDistrictItemView.RowAuthorize = addressDistrictItemView.GetItemRowAuthorize();
				return addressDistrictItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this AddressDistrictItemView addressDistrictItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static AddressDistrict ToAddressDistrict(this AddressDistrictItemView addressDistrictItemView)
		{
			try
			{
				AddressDistrict addressDistrict = new AddressDistrict();
				addressDistrict.CompanyGUID = addressDistrictItemView.CompanyGUID.StringToGuid();
				addressDistrict.CreatedBy = addressDistrictItemView.CreatedBy;
				addressDistrict.CreatedDateTime = addressDistrictItemView.CreatedDateTime.StringToSystemDateTime();
				addressDistrict.ModifiedBy = addressDistrictItemView.ModifiedBy;
				addressDistrict.ModifiedDateTime = addressDistrictItemView.ModifiedDateTime.StringToSystemDateTime();
				addressDistrict.Owner = addressDistrictItemView.Owner;
				addressDistrict.OwnerBusinessUnitGUID = addressDistrictItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				addressDistrict.AddressDistrictGUID = addressDistrictItemView.AddressDistrictGUID.StringToGuid();
				addressDistrict.AddressProvinceGUID = addressDistrictItemView.AddressProvinceGUID.StringToGuid();
				addressDistrict.DistrictId = addressDistrictItemView.DistrictId;
				addressDistrict.Name = addressDistrictItemView.Name;
				
				addressDistrict.RowVersion = addressDistrictItemView.RowVersion;
				return addressDistrict;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<AddressDistrict> ToAddressDistrict(this IEnumerable<AddressDistrictItemView> addressDistrictItemViews)
		{
			try
			{
				List<AddressDistrict> addressDistricts = new List<AddressDistrict>();
				foreach (AddressDistrictItemView item in addressDistrictItemViews)
				{
					addressDistricts.Add(item.ToAddressDistrict());
				}
				return addressDistricts;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AddressDistrictItemView ToAddressDistrictItemView(this AddressDistrict addressDistrict)
		{
			try
			{
				AddressDistrictItemView addressDistrictItemView = new AddressDistrictItemView();
				addressDistrictItemView.CompanyGUID = addressDistrict.CompanyGUID.GuidNullToString();
				addressDistrictItemView.CreatedBy = addressDistrict.CreatedBy;
				addressDistrictItemView.CreatedDateTime = addressDistrict.CreatedDateTime.DateTimeToString();
				addressDistrictItemView.ModifiedBy = addressDistrict.ModifiedBy;
				addressDistrictItemView.ModifiedDateTime = addressDistrict.ModifiedDateTime.DateTimeToString();
				addressDistrictItemView.Owner = addressDistrict.Owner;
				addressDistrictItemView.OwnerBusinessUnitGUID = addressDistrict.OwnerBusinessUnitGUID.GuidNullToString();
				addressDistrictItemView.AddressDistrictGUID = addressDistrict.AddressDistrictGUID.GuidNullToString();
				addressDistrictItemView.AddressProvinceGUID = addressDistrict.AddressProvinceGUID.GuidNullToString();
				addressDistrictItemView.DistrictId = addressDistrict.DistrictId;
				addressDistrictItemView.Name = addressDistrict.Name;
				
				addressDistrictItemView.RowVersion = addressDistrict.RowVersion;
				return addressDistrictItemView.GetAddressDistrictItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion AddressDistrictItemView
		#region ToDropDown
		public static SelectItem<AddressDistrictItemView> ToDropDownItem(this AddressDistrictItemView addressDistrictView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<AddressDistrictItemView> selectItem = new SelectItem<AddressDistrictItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(addressDistrictView.DistrictId, addressDistrictView.Name);
				selectItem.Value = addressDistrictView.AddressDistrictGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: addressDistrictView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<AddressDistrictItemView>> ToDropDownItem(this IEnumerable<AddressDistrictItemView> addressDistrictItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<AddressDistrictItemView>> selectItems = new List<SelectItem<AddressDistrictItemView>>();
				foreach (AddressDistrictItemView item in addressDistrictItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

