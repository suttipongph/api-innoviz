using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CompanyHandler
	{
		#region CompanyListView
		public static List<CompanyListView> GetCompanyListViewValidation(this List<CompanyListView> companyListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return companyListViews;
			}
			var result = new List<CompanyListView>();
			try
			{
				foreach (CompanyListView item in companyListViews)
				{
					result.Add(item.GetCompanyListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CompanyListView GetCompanyListViewValidation(this CompanyListView companyListView)
		{
			try
			{
				companyListView.RowAuthorize = companyListView.GetListRowAuthorize();
				return companyListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CompanyListView companyListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CompanyListView
		#region CompanyItemView
		public static CompanyItemView GetCompanyItemViewValidation(this CompanyItemView companyItemView)
		{
			try
			{
				companyItemView.RowAuthorize = companyItemView.GetItemRowAuthorize();
				return companyItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CompanyItemView companyItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static Company ToCompany(this CompanyItemView companyItemView)
		{
			try
			{
				Company company = new Company();
				company.CompanyGUID = companyItemView.CompanyGUID.StringToGuid();
				company.CreatedBy = companyItemView.CreatedBy;
				company.CreatedDateTime = companyItemView.CreatedDateTime.StringToSystemDateTime();
				company.ModifiedBy = companyItemView.ModifiedBy;
				company.ModifiedDateTime = companyItemView.ModifiedDateTime.StringToSystemDateTime();
				company.CompanyGUID = companyItemView.CompanyGUID.StringToGuid();
				company.CompanyId = companyItemView.CompanyId;
				company.CompanyLogo = companyItemView.CompanyLogo;
				company.Name = companyItemView.Name;
				company.SecondName = companyItemView.SecondName;
				company.AltName = companyItemView.AltName;
				company.DefaultBranchGUID = companyItemView.DefaultBranchGUID.StringToGuidNull();
				company.TaxId = companyItemView.TaxId;
				
				company.RowVersion = companyItemView.RowVersion;
				return company;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<Company> ToCompany(this IEnumerable<CompanyItemView> companyItemViews)
		{
			try
			{
				List<Company> companys = new List<Company>();
				foreach (CompanyItemView item in companyItemViews)
				{
					companys.Add(item.ToCompany());
				}
				return companys;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CompanyItemView ToCompanyItemView(this Company company)
		{
			try
			{
				CompanyItemView companyItemView = new CompanyItemView();
				companyItemView.CompanyGUID = company.CompanyGUID.GuidNullToString();
				companyItemView.CreatedBy = company.CreatedBy;
				companyItemView.CreatedDateTime = company.CreatedDateTime.DateTimeToString();
				companyItemView.ModifiedBy = company.ModifiedBy;
				companyItemView.ModifiedDateTime = company.ModifiedDateTime.DateTimeToString();
				companyItemView.CompanyGUID = company.CompanyGUID.GuidNullToString();
				companyItemView.CompanyId = company.CompanyId;
				companyItemView.CompanyLogo = company.CompanyLogo;
				companyItemView.Name = company.Name;
				companyItemView.SecondName = company.SecondName;
				companyItemView.AltName = company.AltName;
				companyItemView.DefaultBranchGUID = company.DefaultBranchGUID.GuidNullToString();
				companyItemView.TaxId = company.TaxId;

				companyItemView.RowVersion = company.RowVersion;
				return companyItemView.GetCompanyItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CompanyItemView> ToCompanyItemView(this IEnumerable<Company> companys)
		{
			try
			{
				List<CompanyItemView> companyViews = new List<CompanyItemView>();
				foreach (Company item in companys)
				{
					companyViews.Add(item.ToCompanyItemView());
				}
				return companyViews;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CompanyItemView
		#region ToDropDown
		public static SelectItem<CompanyItemView> ToDropDownItem(this CompanyItemView companyView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CompanyItemView> selectItem = new SelectItem<CompanyItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(companyView.CompanyId, companyView.Name);
				selectItem.Value = companyView.CompanyGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: companyView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CompanyItemView>> ToDropDownItem(this IEnumerable<CompanyItemView> companyItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CompanyItemView>> selectItems = new List<SelectItem<CompanyItemView>>();
				foreach (CompanyItemView item in companyItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

