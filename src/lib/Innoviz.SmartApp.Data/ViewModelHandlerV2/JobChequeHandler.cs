using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class JobChequeHandler
	{
		#region JobChequeListView
		public static List<JobChequeListView> GetJobChequeListViewValidation(this List<JobChequeListView> jobChequeListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return jobChequeListViews;
			}
			var result = new List<JobChequeListView>();
			try
			{
				foreach (JobChequeListView item in jobChequeListViews)
				{
					result.Add(item.GetJobChequeListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static JobChequeListView GetJobChequeListViewValidation(this JobChequeListView jobChequeListView)
		{
			try
			{
				jobChequeListView.RowAuthorize = jobChequeListView.GetListRowAuthorize();
				return jobChequeListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this JobChequeListView jobChequeListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion JobChequeListView
		#region JobChequeItemView
		public static JobChequeItemView GetJobChequeItemViewValidation(this JobChequeItemView jobChequeItemView)
		{
			try
			{
				jobChequeItemView.RowAuthorize = jobChequeItemView.GetItemRowAuthorize();
				return jobChequeItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this JobChequeItemView jobChequeItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static JobCheque ToJobCheque(this JobChequeItemView jobChequeItemView)
		{
			try
			{
				JobCheque jobCheque = new JobCheque();
				jobCheque.CompanyGUID = jobChequeItemView.CompanyGUID.StringToGuid();
				jobCheque.CreatedBy = jobChequeItemView.CreatedBy;
				jobCheque.CreatedDateTime = jobChequeItemView.CreatedDateTime.StringToSystemDateTime();
				jobCheque.ModifiedBy = jobChequeItemView.ModifiedBy;
				jobCheque.ModifiedDateTime = jobChequeItemView.ModifiedDateTime.StringToSystemDateTime();
				jobCheque.Owner = jobChequeItemView.Owner;
				jobCheque.OwnerBusinessUnitGUID = jobChequeItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				jobCheque.JobChequeGUID = jobChequeItemView.JobChequeGUID.StringToGuid();
				jobCheque.Amount = jobChequeItemView.Amount;
				jobCheque.ChequeBankAccNo = jobChequeItemView.ChequeBankAccNo;
				jobCheque.ChequeBankGroupGUID = jobChequeItemView.ChequeBankGroupGUID.StringToGuidNull();
				jobCheque.ChequeBranch = jobChequeItemView.ChequeBranch;
				jobCheque.ChequeDate = jobChequeItemView.ChequeDate.StringToDate();
				jobCheque.ChequeNo = jobChequeItemView.ChequeNo;
				jobCheque.ChequeSource = jobChequeItemView.ChequeSource;
				jobCheque.ChequeTableGUID = jobChequeItemView.ChequeTableGUID.StringToGuidNull();
				jobCheque.CollectionFollowUpGUID = jobChequeItemView.CollectionFollowUpGUID.StringToGuidNull();
				jobCheque.MessengerJobTableGUID = jobChequeItemView.MessengerJobTableGUID.StringToGuid();
				jobCheque.RecipientName = jobChequeItemView.RecipientName;
				
				jobCheque.RowVersion = jobChequeItemView.RowVersion;
				return jobCheque;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<JobCheque> ToJobCheque(this IEnumerable<JobChequeItemView> jobChequeItemViews)
		{
			try
			{
				List<JobCheque> jobCheques = new List<JobCheque>();
				foreach (JobChequeItemView item in jobChequeItemViews)
				{
					jobCheques.Add(item.ToJobCheque());
				}
				return jobCheques;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static JobChequeItemView ToJobChequeItemView(this JobCheque jobCheque)
		{
			try
			{
				JobChequeItemView jobChequeItemView = new JobChequeItemView();
				jobChequeItemView.CompanyGUID = jobCheque.CompanyGUID.GuidNullToString();
				jobChequeItemView.CreatedBy = jobCheque.CreatedBy;
				jobChequeItemView.CreatedDateTime = jobCheque.CreatedDateTime.DateTimeToString();
				jobChequeItemView.ModifiedBy = jobCheque.ModifiedBy;
				jobChequeItemView.ModifiedDateTime = jobCheque.ModifiedDateTime.DateTimeToString();
				jobChequeItemView.Owner = jobCheque.Owner;
				jobChequeItemView.OwnerBusinessUnitGUID = jobCheque.OwnerBusinessUnitGUID.GuidNullToString();
				jobChequeItemView.JobChequeGUID = jobCheque.JobChequeGUID.GuidNullToString();
				jobChequeItemView.Amount = jobCheque.Amount;
				jobChequeItemView.ChequeBankAccNo = jobCheque.ChequeBankAccNo;
				jobChequeItemView.ChequeBankGroupGUID = jobCheque.ChequeBankGroupGUID.GuidNullToString();
				jobChequeItemView.ChequeBranch = jobCheque.ChequeBranch;
				jobChequeItemView.ChequeDate = jobCheque.ChequeDate.DateToString();
				jobChequeItemView.ChequeNo = jobCheque.ChequeNo;
				jobChequeItemView.ChequeSource = jobCheque.ChequeSource;
				jobChequeItemView.ChequeTableGUID = jobCheque.ChequeTableGUID.GuidNullToString();
				jobChequeItemView.CollectionFollowUpGUID = jobCheque.CollectionFollowUpGUID.GuidNullToString();
				jobChequeItemView.MessengerJobTableGUID = jobCheque.MessengerJobTableGUID.GuidNullToString();
				jobChequeItemView.RecipientName = jobCheque.RecipientName;
				
				jobChequeItemView.RowVersion = jobCheque.RowVersion;
				return jobChequeItemView.GetJobChequeItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion JobChequeItemView
		#region ToDropDown
		public static SelectItem<JobChequeItemView> ToDropDownItem(this JobChequeItemView jobChequeView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<JobChequeItemView> selectItem = new SelectItem<JobChequeItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(jobChequeView.ChequeNo);
				selectItem.Value = jobChequeView.JobChequeGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: jobChequeView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<JobChequeItemView>> ToDropDownItem(this IEnumerable<JobChequeItemView> jobChequeItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<JobChequeItemView>> selectItems = new List<SelectItem<JobChequeItemView>>();
				foreach (JobChequeItemView item in jobChequeItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

