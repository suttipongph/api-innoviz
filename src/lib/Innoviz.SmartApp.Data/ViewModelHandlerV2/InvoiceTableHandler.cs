using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class InvoiceTableHandler
	{
		#region InvoiceTableListView
		public static List<InvoiceTableListView> GetInvoiceTableListViewValidation(this List<InvoiceTableListView> invoiceTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return invoiceTableListViews;
			}
			var result = new List<InvoiceTableListView>();
			try
			{
				foreach (InvoiceTableListView item in invoiceTableListViews)
				{
					result.Add(item.GetInvoiceTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static InvoiceTableListView GetInvoiceTableListViewValidation(this InvoiceTableListView invoiceTableListView)
		{
			try
			{
				invoiceTableListView.RowAuthorize = invoiceTableListView.GetListRowAuthorize();
				return invoiceTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this InvoiceTableListView invoiceTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion InvoiceTableListView
		#region InvoiceTableItemView
		public static InvoiceTableItemView GetInvoiceTableItemViewValidation(this InvoiceTableItemView invoiceTableItemView)
		{
			try
			{
				invoiceTableItemView.RowAuthorize = invoiceTableItemView.GetItemRowAuthorize();
				return invoiceTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this InvoiceTableItemView invoiceTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static InvoiceTable ToInvoiceTable(this InvoiceTableItemView invoiceTableItemView)
		{
			try
			{
				InvoiceTable invoiceTable = new InvoiceTable();
				invoiceTable.CompanyGUID = invoiceTableItemView.CompanyGUID.StringToGuid();
				invoiceTable.BranchGUID = invoiceTableItemView.BranchGUID.StringToGuid();
				invoiceTable.CreatedBy = invoiceTableItemView.CreatedBy;
				invoiceTable.CreatedDateTime = invoiceTableItemView.CreatedDateTime.StringToSystemDateTime();
				invoiceTable.ModifiedBy = invoiceTableItemView.ModifiedBy;
				invoiceTable.ModifiedDateTime = invoiceTableItemView.ModifiedDateTime.StringToSystemDateTime();
				invoiceTable.Owner = invoiceTableItemView.Owner;
				invoiceTable.OwnerBusinessUnitGUID = invoiceTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				invoiceTable.InvoiceTableGUID = invoiceTableItemView.InvoiceTableGUID.StringToGuid();
				invoiceTable.AccountingPeriod = invoiceTableItemView.AccountingPeriod;
				invoiceTable.BuyerAgreementTableGUID = invoiceTableItemView.BuyerAgreementTableGUID.StringToGuidNull();
				invoiceTable.BuyerInvoiceTableGUID = invoiceTableItemView.BuyerInvoiceTableGUID.StringToGuidNull();
				invoiceTable.BuyerTableGUID = invoiceTableItemView.BuyerTableGUID.StringToGuidNull();
				invoiceTable.CNReasonGUID = invoiceTableItemView.CNReasonGUID.StringToGuidNull();
				invoiceTable.CreditAppTableGUID = invoiceTableItemView.CreditAppTableGUID.StringToGuidNull();
				invoiceTable.CurrencyGUID = invoiceTableItemView.CurrencyGUID.StringToGuid();
				invoiceTable.CustomerName = invoiceTableItemView.CustomerName;
				invoiceTable.CustomerTableGUID = invoiceTableItemView.CustomerTableGUID.StringToGuid();
				invoiceTable.Dimension1GUID = invoiceTableItemView.Dimension1GUID.StringToGuidNull();
				invoiceTable.Dimension2GUID = invoiceTableItemView.Dimension2GUID.StringToGuidNull();
				invoiceTable.Dimension3GUID = invoiceTableItemView.Dimension3GUID.StringToGuidNull();
				invoiceTable.Dimension4GUID = invoiceTableItemView.Dimension4GUID.StringToGuidNull();
				invoiceTable.Dimension5GUID = invoiceTableItemView.Dimension5GUID.StringToGuidNull();
				invoiceTable.DocumentId = invoiceTableItemView.DocumentId;
				invoiceTable.DocumentStatusGUID = invoiceTableItemView.DocumentStatusGUID.StringToGuid();
				invoiceTable.DueDate = invoiceTableItemView.DueDate.StringToDate();
				invoiceTable.ExchangeRate = invoiceTableItemView.ExchangeRate;
				invoiceTable.InvoiceAddress1 = invoiceTableItemView.InvoiceAddress1;
				invoiceTable.InvoiceAddress2 = invoiceTableItemView.InvoiceAddress2;
				invoiceTable.InvoiceAmount = invoiceTableItemView.InvoiceAmount;
				invoiceTable.InvoiceAmountBeforeTax = invoiceTableItemView.InvoiceAmountBeforeTax;
				invoiceTable.InvoiceAmountBeforeTaxMST = invoiceTableItemView.InvoiceAmountBeforeTaxMST;
				invoiceTable.InvoiceAmountMST = invoiceTableItemView.InvoiceAmountMST;
				invoiceTable.InvoiceId = invoiceTableItemView.InvoiceId;
				invoiceTable.InvoiceTypeGUID = invoiceTableItemView.InvoiceTypeGUID.StringToGuid();
				invoiceTable.IssuedDate = invoiceTableItemView.IssuedDate.StringToDate();
				invoiceTable.MailingInvoiceAddress1 = invoiceTableItemView.MailingInvoiceAddress1;
				invoiceTable.MailingInvoiceAddress2 = invoiceTableItemView.MailingInvoiceAddress2;
				invoiceTable.MarketingPeriod = invoiceTableItemView.MarketingPeriod;
				invoiceTable.MethodOfPaymentGUID = invoiceTableItemView.MethodOfPaymentGUID.StringToGuidNull();
				invoiceTable.OrigInvoiceAmount = invoiceTableItemView.OrigInvoiceAmount;
				invoiceTable.OrigInvoiceId = invoiceTableItemView.OrigInvoiceId;
				invoiceTable.OrigTaxInvoiceAmount = invoiceTableItemView.OrigTaxInvoiceAmount;
				invoiceTable.OrigTaxInvoiceId = invoiceTableItemView.OrigTaxInvoiceId;
				invoiceTable.ProductInvoice = invoiceTableItemView.ProductInvoice;
				invoiceTable.ProductType = invoiceTableItemView.ProductType;
				invoiceTable.ReceiptTempTableGUID = invoiceTableItemView.ReceiptTempTableGUID.StringToGuidNull();
				invoiceTable.RefGUID = invoiceTableItemView.RefGUID.StringToGuid();
				invoiceTable.RefInvoiceGUID = invoiceTableItemView.RefInvoiceGUID.StringToGuidNull();
				invoiceTable.RefTaxInvoiceGUID = invoiceTableItemView.RefTaxInvoiceGUID.StringToGuidNull();
				invoiceTable.RefType = invoiceTableItemView.RefType;
				invoiceTable.Remark = invoiceTableItemView.Remark;
				invoiceTable.SuspenseInvoiceType = invoiceTableItemView.SuspenseInvoiceType;
				invoiceTable.TaxAmount = invoiceTableItemView.TaxAmount;
				invoiceTable.TaxAmountMST = invoiceTableItemView.TaxAmountMST;
				invoiceTable.TaxBranchId = invoiceTableItemView.TaxBranchId;
				invoiceTable.TaxBranchName = invoiceTableItemView.TaxBranchName;
				invoiceTable.WHTAmount = invoiceTableItemView.WHTAmount;
				invoiceTable.WHTBaseAmount = invoiceTableItemView.WHTBaseAmount;
				
				invoiceTable.RowVersion = invoiceTableItemView.RowVersion;
				return invoiceTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<InvoiceTable> ToInvoiceTable(this IEnumerable<InvoiceTableItemView> invoiceTableItemViews)
		{
			try
			{
				List<InvoiceTable> invoiceTables = new List<InvoiceTable>();
				foreach (InvoiceTableItemView item in invoiceTableItemViews)
				{
					invoiceTables.Add(item.ToInvoiceTable());
				}
				return invoiceTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static InvoiceTableItemView ToInvoiceTableItemView(this InvoiceTable invoiceTable)
		{
			try
			{
				InvoiceTableItemView invoiceTableItemView = new InvoiceTableItemView();
				invoiceTableItemView.CompanyGUID = invoiceTable.CompanyGUID.GuidNullToString();
				invoiceTableItemView.BranchGUID = invoiceTable.BranchGUID.GuidNullToString();
				invoiceTableItemView.CreatedBy = invoiceTable.CreatedBy;
				invoiceTableItemView.CreatedDateTime = invoiceTable.CreatedDateTime.DateTimeToString();
				invoiceTableItemView.ModifiedBy = invoiceTable.ModifiedBy;
				invoiceTableItemView.ModifiedDateTime = invoiceTable.ModifiedDateTime.DateTimeToString();
				invoiceTableItemView.Owner = invoiceTable.Owner;
				invoiceTableItemView.OwnerBusinessUnitGUID = invoiceTable.OwnerBusinessUnitGUID.GuidNullToString();
				invoiceTableItemView.InvoiceTableGUID = invoiceTable.InvoiceTableGUID.GuidNullToString();
				invoiceTableItemView.AccountingPeriod = invoiceTable.AccountingPeriod;
				invoiceTableItemView.BuyerAgreementTableGUID = invoiceTable.BuyerAgreementTableGUID.GuidNullToString();
				invoiceTableItemView.BuyerInvoiceTableGUID = invoiceTable.BuyerInvoiceTableGUID.GuidNullToString();
				invoiceTableItemView.BuyerTableGUID = invoiceTable.BuyerTableGUID.GuidNullToString();
				invoiceTableItemView.CNReasonGUID = invoiceTable.CNReasonGUID.GuidNullToString();
				invoiceTableItemView.CreditAppTableGUID = invoiceTable.CreditAppTableGUID.GuidNullToString();
				invoiceTableItemView.CurrencyGUID = invoiceTable.CurrencyGUID.GuidNullToString();
				invoiceTableItemView.CustomerName = invoiceTable.CustomerName;
				invoiceTableItemView.CustomerTableGUID = invoiceTable.CustomerTableGUID.GuidNullToString();
				invoiceTableItemView.Dimension1GUID = invoiceTable.Dimension1GUID.GuidNullToString();
				invoiceTableItemView.Dimension2GUID = invoiceTable.Dimension2GUID.GuidNullToString();
				invoiceTableItemView.Dimension3GUID = invoiceTable.Dimension3GUID.GuidNullToString();
				invoiceTableItemView.Dimension4GUID = invoiceTable.Dimension4GUID.GuidNullToString();
				invoiceTableItemView.Dimension5GUID = invoiceTable.Dimension5GUID.GuidNullToString();
				invoiceTableItemView.DocumentId = invoiceTable.DocumentId;
				invoiceTableItemView.DocumentStatusGUID = invoiceTable.DocumentStatusGUID.GuidNullToString();
				invoiceTableItemView.DueDate = invoiceTable.DueDate.DateToString();
				invoiceTableItemView.ExchangeRate = invoiceTable.ExchangeRate;
				invoiceTableItemView.InvoiceAddress1 = invoiceTable.InvoiceAddress1;
				invoiceTableItemView.InvoiceAddress2 = invoiceTable.InvoiceAddress2;
				invoiceTableItemView.InvoiceAmount = invoiceTable.InvoiceAmount;
				invoiceTableItemView.InvoiceAmountBeforeTax = invoiceTable.InvoiceAmountBeforeTax;
				invoiceTableItemView.InvoiceAmountBeforeTaxMST = invoiceTable.InvoiceAmountBeforeTaxMST;
				invoiceTableItemView.InvoiceAmountMST = invoiceTable.InvoiceAmountMST;
				invoiceTableItemView.InvoiceId = invoiceTable.InvoiceId;
				invoiceTableItemView.InvoiceTypeGUID = invoiceTable.InvoiceTypeGUID.GuidNullToString();
				invoiceTableItemView.IssuedDate = invoiceTable.IssuedDate.DateToString();
				invoiceTableItemView.MailingInvoiceAddress1 = invoiceTable.MailingInvoiceAddress1;
				invoiceTableItemView.MailingInvoiceAddress2 = invoiceTable.MailingInvoiceAddress2;
				invoiceTableItemView.MarketingPeriod = invoiceTable.MarketingPeriod;
				invoiceTableItemView.MethodOfPaymentGUID = invoiceTable.MethodOfPaymentGUID.GuidNullToString();
				invoiceTableItemView.OrigInvoiceAmount = invoiceTable.OrigInvoiceAmount;
				invoiceTableItemView.OrigInvoiceId = invoiceTable.OrigInvoiceId;
				invoiceTableItemView.OrigTaxInvoiceAmount = invoiceTable.OrigTaxInvoiceAmount;
				invoiceTableItemView.OrigTaxInvoiceId = invoiceTable.OrigTaxInvoiceId;
				invoiceTableItemView.ProductInvoice = invoiceTable.ProductInvoice;
				invoiceTableItemView.ProductType = invoiceTable.ProductType;
				invoiceTableItemView.ReceiptTempTableGUID = invoiceTable.ReceiptTempTableGUID.GuidNullToString();
				invoiceTableItemView.RefGUID = invoiceTable.RefGUID.GuidNullToString();
				invoiceTableItemView.RefInvoiceGUID = invoiceTable.RefInvoiceGUID.GuidNullToString();
				invoiceTableItemView.RefTaxInvoiceGUID = invoiceTable.RefTaxInvoiceGUID.GuidNullToString();
				invoiceTableItemView.RefType = invoiceTable.RefType;
				invoiceTableItemView.Remark = invoiceTable.Remark;
				invoiceTableItemView.SuspenseInvoiceType = invoiceTable.SuspenseInvoiceType;
				invoiceTableItemView.TaxAmount = invoiceTable.TaxAmount;
				invoiceTableItemView.TaxAmountMST = invoiceTable.TaxAmountMST;
				invoiceTableItemView.TaxBranchId = invoiceTable.TaxBranchId;
				invoiceTableItemView.TaxBranchName = invoiceTable.TaxBranchName;
				invoiceTableItemView.WHTAmount = invoiceTable.WHTAmount;
				invoiceTableItemView.WHTBaseAmount = invoiceTable.WHTBaseAmount;
				
				invoiceTableItemView.RowVersion = invoiceTable.RowVersion;
				return invoiceTableItemView.GetInvoiceTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion InvoiceTableItemView
		#region ToDropDown
		public static SelectItem<InvoiceTableItemView> ToDropDownItem(this InvoiceTableItemView invoiceTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<InvoiceTableItemView> selectItem = new SelectItem<InvoiceTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(invoiceTableView.InvoiceId, invoiceTableView.IssuedDate);
				selectItem.Value = invoiceTableView.InvoiceTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: invoiceTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<InvoiceTableItemView>> ToDropDownItem(this IEnumerable<InvoiceTableItemView> invoiceTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<InvoiceTableItemView>> selectItems = new List<SelectItem<InvoiceTableItemView>>();
				foreach (InvoiceTableItemView item in invoiceTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

