using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class RetentionTransHandler
	{
		#region RetentionTransListView
		public static List<RetentionTransListView> GetRetentionTransListViewValidation(this List<RetentionTransListView> retentionTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return retentionTransListViews;
			}
			var result = new List<RetentionTransListView>();
			try
			{
				foreach (RetentionTransListView item in retentionTransListViews)
				{
					result.Add(item.GetRetentionTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static RetentionTransListView GetRetentionTransListViewValidation(this RetentionTransListView retentionTransListView)
		{
			try
			{
				retentionTransListView.RowAuthorize = retentionTransListView.GetListRowAuthorize();
				return retentionTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this RetentionTransListView retentionTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion RetentionTransListView
		#region RetentionTransItemView
		public static RetentionTransItemView GetRetentionTransItemViewValidation(this RetentionTransItemView retentionTransItemView)
		{
			try
			{
				retentionTransItemView.RowAuthorize = retentionTransItemView.GetItemRowAuthorize();
				return retentionTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this RetentionTransItemView retentionTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static RetentionTrans ToRetentionTrans(this RetentionTransItemView retentionTransItemView)
		{
			try
			{
				RetentionTrans retentionTrans = new RetentionTrans();
				retentionTrans.CompanyGUID = retentionTransItemView.CompanyGUID.StringToGuid();
				retentionTrans.CreatedBy = retentionTransItemView.CreatedBy;
				retentionTrans.CreatedDateTime = retentionTransItemView.CreatedDateTime.StringToSystemDateTime();
				retentionTrans.ModifiedBy = retentionTransItemView.ModifiedBy;
				retentionTrans.ModifiedDateTime = retentionTransItemView.ModifiedDateTime.StringToSystemDateTime();
				retentionTrans.Owner = retentionTransItemView.Owner;
				retentionTrans.OwnerBusinessUnitGUID = retentionTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				retentionTrans.RetentionTransGUID = retentionTransItemView.RetentionTransGUID.StringToGuid();
				retentionTrans.Amount = retentionTransItemView.Amount;
				retentionTrans.BuyerAgreementTableGUID = retentionTransItemView.BuyerAgreementTableGUID.StringToGuidNull();
				retentionTrans.BuyerTableGUID = retentionTransItemView.BuyerTableGUID.StringToGuidNull();
				retentionTrans.CreditAppTableGUID = retentionTransItemView.CreditAppTableGUID.StringToGuid();
				retentionTrans.CustomerTableGUID = retentionTransItemView.CustomerTableGUID.StringToGuid();
				retentionTrans.DocumentId = retentionTransItemView.DocumentId;
				retentionTrans.ProductType = retentionTransItemView.ProductType;
				retentionTrans.RefGUID = retentionTransItemView.RefGUID.StringToGuid();
				retentionTrans.RefType = retentionTransItemView.RefType;
				retentionTrans.TransDate = retentionTransItemView.TransDate.StringToDate();
				
				retentionTrans.RowVersion = retentionTransItemView.RowVersion;
				return retentionTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<RetentionTrans> ToRetentionTrans(this IEnumerable<RetentionTransItemView> retentionTransItemViews)
		{
			try
			{
				List<RetentionTrans> retentionTranss = new List<RetentionTrans>();
				foreach (RetentionTransItemView item in retentionTransItemViews)
				{
					retentionTranss.Add(item.ToRetentionTrans());
				}
				return retentionTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static RetentionTransItemView ToRetentionTransItemView(this RetentionTrans retentionTrans)
		{
			try
			{
				RetentionTransItemView retentionTransItemView = new RetentionTransItemView();
				retentionTransItemView.CompanyGUID = retentionTrans.CompanyGUID.GuidNullToString();
				retentionTransItemView.CreatedBy = retentionTrans.CreatedBy;
				retentionTransItemView.CreatedDateTime = retentionTrans.CreatedDateTime.DateTimeToString();
				retentionTransItemView.ModifiedBy = retentionTrans.ModifiedBy;
				retentionTransItemView.ModifiedDateTime = retentionTrans.ModifiedDateTime.DateTimeToString();
				retentionTransItemView.Owner = retentionTrans.Owner;
				retentionTransItemView.OwnerBusinessUnitGUID = retentionTrans.OwnerBusinessUnitGUID.GuidNullToString();
				retentionTransItemView.RetentionTransGUID = retentionTrans.RetentionTransGUID.GuidNullToString();
				retentionTransItemView.Amount = retentionTrans.Amount;
				retentionTransItemView.BuyerAgreementTableGUID = retentionTrans.BuyerAgreementTableGUID.GuidNullToString();
				retentionTransItemView.BuyerTableGUID = retentionTrans.BuyerTableGUID.GuidNullToString();
				retentionTransItemView.CreditAppTableGUID = retentionTrans.CreditAppTableGUID.GuidNullToString();
				retentionTransItemView.CustomerTableGUID = retentionTrans.CustomerTableGUID.GuidNullToString();
				retentionTransItemView.DocumentId = retentionTrans.DocumentId;
				retentionTransItemView.ProductType = retentionTrans.ProductType;
				retentionTransItemView.RefGUID = retentionTrans.RefGUID.GuidNullToString();
				retentionTransItemView.RefType = retentionTrans.RefType;
				retentionTransItemView.TransDate = retentionTrans.TransDate.DateToString();
				
				retentionTransItemView.RowVersion = retentionTrans.RowVersion;
				return retentionTransItemView.GetRetentionTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion RetentionTransItemView
		#region ToDropDown
		public static SelectItem<RetentionTransItemView> ToDropDownItem(this RetentionTransItemView retentionTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<RetentionTransItemView> selectItem = new SelectItem<RetentionTransItemView>();
				selectItem.Label = null;
				selectItem.Value = retentionTransView.RetentionTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: retentionTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<RetentionTransItemView>> ToDropDownItem(this IEnumerable<RetentionTransItemView> retentionTransItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<RetentionTransItemView>> selectItems = new List<SelectItem<RetentionTransItemView>>();
				foreach (RetentionTransItemView item in retentionTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

