﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CustomerAgingHandler
	{
		public static CustomerAgingViewMap ToCustomerAgingViewMap(this CustomerAgingView customerAgingView)
		{
			try
			{
				CustomerAgingViewMap customerAgingViewMap = new CustomerAgingViewMap();
				customerAgingViewMap.CustomerId = customerAgingView.CustomerId;
				customerAgingViewMap.CustomerName = customerAgingView.CustomerName;
				customerAgingViewMap.BuyerId = customerAgingView.BuyerId;
				customerAgingViewMap.BuyerName = customerAgingView.BuyerName;
				customerAgingViewMap.ProductType = customerAgingView.ProductType;
				customerAgingViewMap.InvoiceTypeId = customerAgingView.InvoiceTypeId;
				customerAgingViewMap.InvoiceId = customerAgingView.InvoiceId;
				customerAgingViewMap.DocumentId = customerAgingView.DocumentId;
				customerAgingViewMap.BuyerInvoiceId = customerAgingView.BuyerInvoiceId;
				customerAgingViewMap.BuyerInvoiceDate = customerAgingView.BuyerInvoiceDate.StringNullToDateNull();
				customerAgingViewMap.PurchaseDate = customerAgingView.PurchaseDate.StringNullToDateNull();
				customerAgingViewMap.WithdrawalDate = customerAgingView.WithdrawalDate.StringNullToDateNull();
				customerAgingViewMap.InterestDate = customerAgingView.InterestDate.StringToDate();
				customerAgingViewMap.AsOfDate = customerAgingView.AsOfDate.StringToDate();
				customerAgingViewMap.Days = customerAgingView.Days;
				customerAgingViewMap.InvoiceAmount = customerAgingView.InvoiceAmount;
				customerAgingViewMap.PurchaseAmount = customerAgingView.PurchaseAmount;
				customerAgingViewMap.ReserveAmount = customerAgingView.ReserveAmount;
				customerAgingViewMap.AROutstanding = customerAgingView.AROutstanding;
				customerAgingViewMap.PurchaseOutstanding = customerAgingView.PurchaseOutstanding;
				customerAgingViewMap.UnearnedInterestBalance = customerAgingView.UnearnedInterestBalance;
				customerAgingViewMap.AccruedIntBalance = customerAgingView.AccruedIntBalance;
				customerAgingViewMap.FeeBalance = customerAgingView.FeeBalance;
				customerAgingViewMap.NetAR = customerAgingView.NetAR;
				customerAgingViewMap.NetARBucket1 = customerAgingView.NetARBucket1;
				customerAgingViewMap.NetARBucket2 = customerAgingView.NetARBucket2;
				customerAgingViewMap.NetARBucket3 = customerAgingView.NetARBucket3;
				customerAgingViewMap.NetARBucket4 = customerAgingView.NetARBucket4;
				customerAgingViewMap.NetARBucket5 = customerAgingView.NetARBucket5;
				customerAgingViewMap.NetARBucket6 = customerAgingView.NetARBucket6;
				customerAgingViewMap.ResponsibleName = customerAgingView.ResponsibleName;
				customerAgingViewMap.BusinessUnitId = customerAgingView.BusinessUnitId;
				customerAgingViewMap.BusinessTypeDesc = customerAgingView.BusinessTypeDesc;
				customerAgingViewMap.CreditAppId = customerAgingView.CreditAppId;
				customerAgingViewMap.MainAgreementId = customerAgingView.MainAgreementId;
				return customerAgingViewMap;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CustomerAgingView ToCustomerAgingView(this CustomerAgingViewMap customerAgingViewMap)
		{
			try
			{
				CustomerAgingView customerAgingView = new CustomerAgingView();
				customerAgingView.CustomerId = customerAgingViewMap.CustomerId;
				customerAgingView.CustomerName = customerAgingViewMap.CustomerName;
				customerAgingView.BuyerId = customerAgingViewMap.BuyerId;
				customerAgingView.BuyerName = customerAgingViewMap.BuyerName;
				customerAgingView.ProductType = customerAgingViewMap.ProductType;
				customerAgingView.InvoiceTypeId = customerAgingViewMap.InvoiceTypeId;
				customerAgingView.InvoiceId = customerAgingViewMap.InvoiceId;
				customerAgingView.DocumentId = customerAgingViewMap.DocumentId;
				customerAgingView.BuyerInvoiceId = customerAgingViewMap.BuyerInvoiceId;
				customerAgingView.BuyerInvoiceDate = customerAgingViewMap.BuyerInvoiceDate.DateNullToString();
				customerAgingView.PurchaseDate = customerAgingViewMap.PurchaseDate.DateNullToString();
				customerAgingView.WithdrawalDate = customerAgingViewMap.WithdrawalDate.DateNullToString();
				customerAgingView.InterestDate = customerAgingViewMap.InterestDate.DateToString();
				customerAgingView.AsOfDate = customerAgingViewMap.AsOfDate.DateToString();
				customerAgingView.Days = customerAgingViewMap.Days;
				customerAgingView.InvoiceAmount = customerAgingViewMap.InvoiceAmount;
				customerAgingView.PurchaseAmount = customerAgingViewMap.PurchaseAmount;
				customerAgingView.ReserveAmount = customerAgingViewMap.ReserveAmount;
				customerAgingView.AROutstanding = customerAgingViewMap.AROutstanding;
				customerAgingView.PurchaseOutstanding = customerAgingViewMap.PurchaseOutstanding;
				customerAgingView.UnearnedInterestBalance = customerAgingViewMap.UnearnedInterestBalance;
				customerAgingView.AccruedIntBalance = customerAgingViewMap.AccruedIntBalance;
				customerAgingView.FeeBalance = customerAgingViewMap.FeeBalance;
				customerAgingView.NetAR = customerAgingViewMap.NetAR;
				customerAgingView.NetARBucket1 = customerAgingViewMap.NetARBucket1;
				customerAgingView.NetARBucket2 = customerAgingViewMap.NetARBucket2;
				customerAgingView.NetARBucket3 = customerAgingViewMap.NetARBucket3;
				customerAgingView.NetARBucket4 = customerAgingViewMap.NetARBucket4;
				customerAgingView.NetARBucket5 = customerAgingViewMap.NetARBucket5;
				customerAgingView.NetARBucket6 = customerAgingViewMap.NetARBucket6;
				customerAgingView.ResponsibleName = customerAgingViewMap.ResponsibleName;
				customerAgingView.BusinessUnitId = customerAgingViewMap.BusinessUnitId;
				customerAgingView.BusinessTypeDesc = customerAgingViewMap.BusinessTypeDesc;
				customerAgingView.CreditAppId = customerAgingViewMap.CreditAppId;
				customerAgingView.MainAgreementId = customerAgingViewMap.MainAgreementId;
				return customerAgingView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
	}
}
