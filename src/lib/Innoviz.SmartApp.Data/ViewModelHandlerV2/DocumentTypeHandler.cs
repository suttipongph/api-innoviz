using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class DocumentTypeHandler
	{
		#region DocumentTypeListView
		public static List<DocumentTypeListView> GetDocumentTypeListViewValidation(this List<DocumentTypeListView> documentTypeListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return documentTypeListViews;
			}
			var result = new List<DocumentTypeListView>();
			try
			{
				foreach (DocumentTypeListView item in documentTypeListViews)
				{
					result.Add(item.GetDocumentTypeListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static DocumentTypeListView GetDocumentTypeListViewValidation(this DocumentTypeListView documentTypeListView)
		{
			try
			{
				documentTypeListView.RowAuthorize = documentTypeListView.GetListRowAuthorize();
				return documentTypeListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this DocumentTypeListView documentTypeListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion DocumentTypeListView
		#region DocumentTypeItemView
		public static DocumentTypeItemView GetDocumentTypeItemViewValidation(this DocumentTypeItemView documentTypeItemView)
		{
			try
			{
				documentTypeItemView.RowAuthorize = documentTypeItemView.GetItemRowAuthorize();
				return documentTypeItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this DocumentTypeItemView documentTypeItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static DocumentType ToDocumentType(this DocumentTypeItemView documentTypeItemView)
		{
			try
			{
				DocumentType documentType = new DocumentType();
				documentType.CompanyGUID = documentTypeItemView.CompanyGUID.StringToGuid();
				documentType.CreatedBy = documentTypeItemView.CreatedBy;
				documentType.CreatedDateTime = documentTypeItemView.CreatedDateTime.StringToSystemDateTime();
				documentType.ModifiedBy = documentTypeItemView.ModifiedBy;
				documentType.ModifiedDateTime = documentTypeItemView.ModifiedDateTime.StringToSystemDateTime();
				documentType.Owner = documentTypeItemView.Owner;
				documentType.OwnerBusinessUnitGUID = documentTypeItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				documentType.DocumentTypeGUID = documentTypeItemView.DocumentTypeGUID.StringToGuid();
				documentType.Description = documentTypeItemView.Description;
				documentType.DocumentTypeId = documentTypeItemView.DocumentTypeId;
				
				documentType.RowVersion = documentTypeItemView.RowVersion;
				return documentType;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<DocumentType> ToDocumentType(this IEnumerable<DocumentTypeItemView> documentTypeItemViews)
		{
			try
			{
				List<DocumentType> documentTypes = new List<DocumentType>();
				foreach (DocumentTypeItemView item in documentTypeItemViews)
				{
					documentTypes.Add(item.ToDocumentType());
				}
				return documentTypes;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static DocumentTypeItemView ToDocumentTypeItemView(this DocumentType documentType)
		{
			try
			{
				DocumentTypeItemView documentTypeItemView = new DocumentTypeItemView();
				documentTypeItemView.CompanyGUID = documentType.CompanyGUID.GuidNullToString();
				documentTypeItemView.CreatedBy = documentType.CreatedBy;
				documentTypeItemView.CreatedDateTime = documentType.CreatedDateTime.DateTimeToString();
				documentTypeItemView.ModifiedBy = documentType.ModifiedBy;
				documentTypeItemView.ModifiedDateTime = documentType.ModifiedDateTime.DateTimeToString();
				documentTypeItemView.Owner = documentType.Owner;
				documentTypeItemView.OwnerBusinessUnitGUID = documentType.OwnerBusinessUnitGUID.GuidNullToString();
				documentTypeItemView.DocumentTypeGUID = documentType.DocumentTypeGUID.GuidNullToString();
				documentTypeItemView.Description = documentType.Description;
				documentTypeItemView.DocumentTypeId = documentType.DocumentTypeId;
				
				documentTypeItemView.RowVersion = documentType.RowVersion;
				return documentTypeItemView.GetDocumentTypeItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion DocumentTypeItemView
		#region ToDropDown
		public static SelectItem<DocumentTypeItemView> ToDropDownItem(this DocumentTypeItemView documentTypeView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<DocumentTypeItemView> selectItem = new SelectItem<DocumentTypeItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(documentTypeView.DocumentTypeId, documentTypeView.Description);
				selectItem.Value = documentTypeView.DocumentTypeGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: documentTypeView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<DocumentTypeItemView>> ToDropDownItem(this IEnumerable<DocumentTypeItemView> documentTypeItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<DocumentTypeItemView>> selectItems = new List<SelectItem<DocumentTypeItemView>>();
				foreach (DocumentTypeItemView item in documentTypeItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

