using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class AddressPostalCodeHandler
	{
		#region AddressPostalCodeListView
		public static List<AddressPostalCodeListView> GetAddressPostalCodeListViewValidation(this List<AddressPostalCodeListView> addressPostalCodeListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return addressPostalCodeListViews;
			}
			var result = new List<AddressPostalCodeListView>();
			try
			{
				foreach (AddressPostalCodeListView item in addressPostalCodeListViews)
				{
					result.Add(item.GetAddressPostalCodeListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AddressPostalCodeListView GetAddressPostalCodeListViewValidation(this AddressPostalCodeListView addressPostalCodeListView)
		{
			try
			{
				addressPostalCodeListView.RowAuthorize = addressPostalCodeListView.GetListRowAuthorize();
				return addressPostalCodeListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this AddressPostalCodeListView addressPostalCodeListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion AddressPostalCodeListView
		#region AddressPostalCodeItemView
		public static AddressPostalCodeItemView GetAddressPostalCodeItemViewValidation(this AddressPostalCodeItemView addressPostalCodeItemView)
		{
			try
			{
				addressPostalCodeItemView.RowAuthorize = addressPostalCodeItemView.GetItemRowAuthorize();
				return addressPostalCodeItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this AddressPostalCodeItemView addressPostalCodeItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static AddressPostalCode ToAddressPostalCode(this AddressPostalCodeItemView addressPostalCodeItemView)
		{
			try
			{
				AddressPostalCode addressPostalCode = new AddressPostalCode();
				addressPostalCode.CompanyGUID = addressPostalCodeItemView.CompanyGUID.StringToGuid();
				addressPostalCode.CreatedBy = addressPostalCodeItemView.CreatedBy;
				addressPostalCode.CreatedDateTime = addressPostalCodeItemView.CreatedDateTime.StringToSystemDateTime();
				addressPostalCode.ModifiedBy = addressPostalCodeItemView.ModifiedBy;
				addressPostalCode.ModifiedDateTime = addressPostalCodeItemView.ModifiedDateTime.StringToSystemDateTime();
				addressPostalCode.Owner = addressPostalCodeItemView.Owner;
				addressPostalCode.OwnerBusinessUnitGUID = addressPostalCodeItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				addressPostalCode.AddressPostalCodeGUID = addressPostalCodeItemView.AddressPostalCodeGUID.StringToGuid();
				addressPostalCode.PostalCode = addressPostalCodeItemView.PostalCode;
				addressPostalCode.AddressSubDistrictGUID = addressPostalCodeItemView.AddressSubDistrictGUID.StringToGuid();

				
				addressPostalCode.RowVersion = addressPostalCodeItemView.RowVersion;
				return addressPostalCode;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<AddressPostalCode> ToAddressPostalCode(this IEnumerable<AddressPostalCodeItemView> addressPostalCodeItemViews)
		{
			try
			{
				List<AddressPostalCode> addressPostalCodes = new List<AddressPostalCode>();
				foreach (AddressPostalCodeItemView item in addressPostalCodeItemViews)
				{
					addressPostalCodes.Add(item.ToAddressPostalCode());
				}
				return addressPostalCodes;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AddressPostalCodeItemView ToAddressPostalCodeItemView(this AddressPostalCode addressPostalCode)
		{
			try
			{
				AddressPostalCodeItemView addressPostalCodeItemView = new AddressPostalCodeItemView();
				addressPostalCodeItemView.CompanyGUID = addressPostalCode.CompanyGUID.GuidNullToString();
				addressPostalCodeItemView.CreatedBy = addressPostalCode.CreatedBy;
				addressPostalCodeItemView.CreatedDateTime = addressPostalCode.CreatedDateTime.DateTimeToString();
				addressPostalCodeItemView.ModifiedBy = addressPostalCode.ModifiedBy;
				addressPostalCodeItemView.ModifiedDateTime = addressPostalCode.ModifiedDateTime.DateTimeToString();
				addressPostalCodeItemView.Owner = addressPostalCode.Owner;
				addressPostalCodeItemView.OwnerBusinessUnitGUID = addressPostalCode.OwnerBusinessUnitGUID.GuidNullToString();
				addressPostalCodeItemView.AddressPostalCodeGUID = addressPostalCode.AddressPostalCodeGUID.GuidNullToString();
				addressPostalCodeItemView.PostalCode = addressPostalCode.PostalCode;
				addressPostalCodeItemView.AddressSubDistrictGUID = addressPostalCode.AddressSubDistrictGUID.GuidNullToString();
				
				addressPostalCodeItemView.RowVersion = addressPostalCode.RowVersion;
				return addressPostalCodeItemView.GetAddressPostalCodeItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion AddressPostalCodeItemView
		#region ToDropDown
		public static SelectItem<AddressPostalCodeItemView> ToDropDownItem(this AddressPostalCodeItemView addressPostalCodeView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<AddressPostalCodeItemView> selectItem = new SelectItem<AddressPostalCodeItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(addressPostalCodeView.PostalCode);
				selectItem.Value = addressPostalCodeView.AddressPostalCodeGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: addressPostalCodeView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<AddressPostalCodeItemView>> ToDropDownItem(this IEnumerable<AddressPostalCodeItemView> addressPostalCodeItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<AddressPostalCodeItemView>> selectItems = new List<SelectItem<AddressPostalCodeItemView>>();
				foreach (AddressPostalCodeItemView item in addressPostalCodeItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

