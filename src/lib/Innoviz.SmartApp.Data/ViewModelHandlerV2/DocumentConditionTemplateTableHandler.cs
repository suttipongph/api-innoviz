using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class DocumentConditionTemplateTableHandler
	{
		#region DocumentConditionTemplateTableListView
		public static List<DocumentConditionTemplateTableListView> GetDocumentConditionTemplateTableListViewValidation(this List<DocumentConditionTemplateTableListView> documentConditionTemplateTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return documentConditionTemplateTableListViews;
			}
			var result = new List<DocumentConditionTemplateTableListView>();
			try
			{
				foreach (DocumentConditionTemplateTableListView item in documentConditionTemplateTableListViews)
				{
					result.Add(item.GetDocumentConditionTemplateTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static DocumentConditionTemplateTableListView GetDocumentConditionTemplateTableListViewValidation(this DocumentConditionTemplateTableListView documentConditionTemplateTableListView)
		{
			try
			{
				documentConditionTemplateTableListView.RowAuthorize = documentConditionTemplateTableListView.GetListRowAuthorize();
				return documentConditionTemplateTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this DocumentConditionTemplateTableListView documentConditionTemplateTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion DocumentConditionTemplateTableListView
		#region DocumentConditionTemplateTableItemView
		public static DocumentConditionTemplateTableItemView GetDocumentConditionTemplateTableItemViewValidation(this DocumentConditionTemplateTableItemView documentConditionTemplateTableItemView)
		{
			try
			{
				documentConditionTemplateTableItemView.RowAuthorize = documentConditionTemplateTableItemView.GetItemRowAuthorize();
				return documentConditionTemplateTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this DocumentConditionTemplateTableItemView documentConditionTemplateTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static DocumentConditionTemplateTable ToDocumentConditionTemplateTable(this DocumentConditionTemplateTableItemView documentConditionTemplateTableItemView)
		{
			try
			{
				DocumentConditionTemplateTable documentConditionTemplateTable = new DocumentConditionTemplateTable();
				documentConditionTemplateTable.CompanyGUID = documentConditionTemplateTableItemView.CompanyGUID.StringToGuid();
				documentConditionTemplateTable.CreatedBy = documentConditionTemplateTableItemView.CreatedBy;
				documentConditionTemplateTable.CreatedDateTime = documentConditionTemplateTableItemView.CreatedDateTime.StringToSystemDateTime();
				documentConditionTemplateTable.ModifiedBy = documentConditionTemplateTableItemView.ModifiedBy;
				documentConditionTemplateTable.ModifiedDateTime = documentConditionTemplateTableItemView.ModifiedDateTime.StringToSystemDateTime();
				documentConditionTemplateTable.Owner = documentConditionTemplateTableItemView.Owner;
				documentConditionTemplateTable.OwnerBusinessUnitGUID = documentConditionTemplateTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				documentConditionTemplateTable.DocumentConditionTemplateTableGUID = documentConditionTemplateTableItemView.DocumentConditionTemplateTableGUID.StringToGuid();
				documentConditionTemplateTable.Description = documentConditionTemplateTableItemView.Description;
				documentConditionTemplateTable.DocumentConditionTemplateTableId = documentConditionTemplateTableItemView.DocumentConditionTemplateTableId;
				
				documentConditionTemplateTable.RowVersion = documentConditionTemplateTableItemView.RowVersion;
				return documentConditionTemplateTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<DocumentConditionTemplateTable> ToDocumentConditionTemplateTable(this IEnumerable<DocumentConditionTemplateTableItemView> documentConditionTemplateTableItemViews)
		{
			try
			{
				List<DocumentConditionTemplateTable> documentConditionTemplateTables = new List<DocumentConditionTemplateTable>();
				foreach (DocumentConditionTemplateTableItemView item in documentConditionTemplateTableItemViews)
				{
					documentConditionTemplateTables.Add(item.ToDocumentConditionTemplateTable());
				}
				return documentConditionTemplateTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static DocumentConditionTemplateTableItemView ToDocumentConditionTemplateTableItemView(this DocumentConditionTemplateTable documentConditionTemplateTable)
		{
			try
			{
				DocumentConditionTemplateTableItemView documentConditionTemplateTableItemView = new DocumentConditionTemplateTableItemView();
				documentConditionTemplateTableItemView.CompanyGUID = documentConditionTemplateTable.CompanyGUID.GuidNullToString();
				documentConditionTemplateTableItemView.CreatedBy = documentConditionTemplateTable.CreatedBy;
				documentConditionTemplateTableItemView.CreatedDateTime = documentConditionTemplateTable.CreatedDateTime.DateTimeToString();
				documentConditionTemplateTableItemView.ModifiedBy = documentConditionTemplateTable.ModifiedBy;
				documentConditionTemplateTableItemView.ModifiedDateTime = documentConditionTemplateTable.ModifiedDateTime.DateTimeToString();
				documentConditionTemplateTableItemView.Owner = documentConditionTemplateTable.Owner;
				documentConditionTemplateTableItemView.OwnerBusinessUnitGUID = documentConditionTemplateTable.OwnerBusinessUnitGUID.GuidNullToString();
				documentConditionTemplateTableItemView.DocumentConditionTemplateTableGUID = documentConditionTemplateTable.DocumentConditionTemplateTableGUID.GuidNullToString();
				documentConditionTemplateTableItemView.Description = documentConditionTemplateTable.Description;
				documentConditionTemplateTableItemView.DocumentConditionTemplateTableId = documentConditionTemplateTable.DocumentConditionTemplateTableId;
				
				documentConditionTemplateTableItemView.RowVersion = documentConditionTemplateTable.RowVersion;
				return documentConditionTemplateTableItemView.GetDocumentConditionTemplateTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion DocumentConditionTemplateTableItemView
		#region ToDropDown
		public static SelectItem<DocumentConditionTemplateTableItemView> ToDropDownItem(this DocumentConditionTemplateTableItemView documentConditionTemplateTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<DocumentConditionTemplateTableItemView> selectItem = new SelectItem<DocumentConditionTemplateTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(documentConditionTemplateTableView.DocumentConditionTemplateTableId, documentConditionTemplateTableView.Description);
				selectItem.Value = documentConditionTemplateTableView.DocumentConditionTemplateTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: documentConditionTemplateTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<DocumentConditionTemplateTableItemView>> ToDropDownItem(this IEnumerable<DocumentConditionTemplateTableItemView> documentConditionTemplateTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<DocumentConditionTemplateTableItemView>> selectItems = new List<SelectItem<DocumentConditionTemplateTableItemView>>();
				foreach (DocumentConditionTemplateTableItemView item in documentConditionTemplateTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

