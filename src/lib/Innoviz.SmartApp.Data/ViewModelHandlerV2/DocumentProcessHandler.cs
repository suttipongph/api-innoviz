using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class DocumentProcessHandler
	{
		#region DocumentProcessListView
		public static List<DocumentProcessListView> GetDocumentProcessListViewValidation(this List<DocumentProcessListView> documentProcessListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return documentProcessListViews;
			}
			var result = new List<DocumentProcessListView>();
			try
			{
				foreach (DocumentProcessListView item in documentProcessListViews)
				{
					result.Add(item.GetDocumentProcessListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static DocumentProcessListView GetDocumentProcessListViewValidation(this DocumentProcessListView documentProcessListView)
		{
			try
			{
				documentProcessListView.RowAuthorize = documentProcessListView.GetListRowAuthorize();
				return documentProcessListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this DocumentProcessListView documentProcessListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion DocumentProcessListView
		#region DocumentProcessItemView
		public static DocumentProcessItemView GetDocumentProcessItemViewValidation(this DocumentProcessItemView documentProcessItemView)
		{
			try
			{
				documentProcessItemView.RowAuthorize = documentProcessItemView.GetItemRowAuthorize();
				return documentProcessItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this DocumentProcessItemView documentProcessItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static DocumentProcess ToDocumentProcess(this DocumentProcessItemView documentProcessItemView)
		{
			try
			{
				DocumentProcess documentProcess = new DocumentProcess();
				documentProcess.CreatedBy = documentProcessItemView.CreatedBy;
				documentProcess.CreatedDateTime = documentProcessItemView.CreatedDateTime.StringToSystemDateTime();
				documentProcess.ModifiedBy = documentProcessItemView.ModifiedBy;
				documentProcess.ModifiedDateTime = documentProcessItemView.ModifiedDateTime.StringToSystemDateTime();
				documentProcess.DocumentProcessGUID = documentProcessItemView.DocumentProcessGUID.StringToGuid();
				documentProcess.Description = documentProcessItemView.Description;
				documentProcess.ProcessId = documentProcessItemView.ProcessId;
				
				documentProcess.RowVersion = documentProcessItemView.RowVersion;
				return documentProcess;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<DocumentProcess> ToDocumentProcess(this IEnumerable<DocumentProcessItemView> documentProcessItemViews)
		{
			try
			{
				List<DocumentProcess> documentProcesss = new List<DocumentProcess>();
				foreach (DocumentProcessItemView item in documentProcessItemViews)
				{
					documentProcesss.Add(item.ToDocumentProcess());
				}
				return documentProcesss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static DocumentProcessItemView ToDocumentProcessItemView(this DocumentProcess documentProcess)
		{
			try
			{
				DocumentProcessItemView documentProcessItemView = new DocumentProcessItemView();
				documentProcessItemView.CreatedBy = documentProcess.CreatedBy;
				documentProcessItemView.CreatedDateTime = documentProcess.CreatedDateTime.DateTimeToString();
				documentProcessItemView.ModifiedBy = documentProcess.ModifiedBy;
				documentProcessItemView.ModifiedDateTime = documentProcess.ModifiedDateTime.DateTimeToString();
				documentProcessItemView.DocumentProcessGUID = documentProcess.DocumentProcessGUID.GuidNullToString();
				documentProcessItemView.Description = documentProcess.Description;
				documentProcessItemView.ProcessId = documentProcess.ProcessId;
				
				documentProcessItemView.RowVersion = documentProcess.RowVersion;
				return documentProcessItemView.GetDocumentProcessItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion DocumentProcessItemView
		#region ToDropDown
		public static SelectItem<DocumentProcessItemView> ToDropDownItem(this DocumentProcessItemView documentProcessView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<DocumentProcessItemView> selectItem = new SelectItem<DocumentProcessItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(documentProcessView.Description);
				selectItem.Value = documentProcessView.DocumentProcessGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: documentProcessView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<DocumentProcessItemView>> ToDropDownItem(this IEnumerable<DocumentProcessItemView> documentProcessItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<DocumentProcessItemView>> selectItems = new List<SelectItem<DocumentProcessItemView>>();
				foreach (DocumentProcessItemView item in documentProcessItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

