using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CompanyParameterHandler
	{
		#region CompanyParameterListView
		public static List<CompanyParameterListView> GetCompanyParameterListViewValidation(this List<CompanyParameterListView> companyParameterListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return companyParameterListViews;
			}
			var result = new List<CompanyParameterListView>();
			try
			{
				foreach (CompanyParameterListView item in companyParameterListViews)
				{
					result.Add(item.GetCompanyParameterListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CompanyParameterListView GetCompanyParameterListViewValidation(this CompanyParameterListView companyParameterListView)
		{
			try
			{
				companyParameterListView.RowAuthorize = companyParameterListView.GetListRowAuthorize();
				return companyParameterListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CompanyParameterListView companyParameterListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CompanyParameterListView
		#region CompanyParameterItemView
		public static CompanyParameterItemView GetCompanyParameterItemViewValidation(this CompanyParameterItemView companyParameterItemView)
		{
			try
			{
				companyParameterItemView.RowAuthorize = companyParameterItemView.GetItemRowAuthorize();
				return companyParameterItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CompanyParameterItemView companyParameterItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CompanyParameter ToCompanyParameter(this CompanyParameterItemView companyParameterItemView)
		{
			try
			{
				CompanyParameter companyParameter = new CompanyParameter();
				companyParameter.CompanyGUID = companyParameterItemView.CompanyGUID.StringToGuid();
				companyParameter.CreatedBy = companyParameterItemView.CreatedBy;
				companyParameter.CreatedDateTime = companyParameterItemView.CreatedDateTime.StringToSystemDateTime();
				companyParameter.ModifiedBy = companyParameterItemView.ModifiedBy;
				companyParameter.ModifiedDateTime = companyParameterItemView.ModifiedDateTime.StringToSystemDateTime();
				companyParameter.Owner = companyParameterItemView.Owner;
				companyParameter.OwnerBusinessUnitGUID = companyParameterItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				companyParameter.CompanyParameterGUID = companyParameterItemView.CompanyParameterGUID.StringToGuid();
				companyParameter.AgreementType = companyParameterItemView.AgreementType;
				companyParameter.AutoSettleToleranceLedgerAcc = companyParameterItemView.AutoSettleToleranceLedgerAcc;
				companyParameter.BOTRealization = companyParameterItemView.BOTRealization;
				companyParameter.CalcSheetIncAdditionalCost = companyParameterItemView.CalcSheetIncAdditionalCost;
				companyParameter.CalcSheetIncTax = companyParameterItemView.CalcSheetIncTax;
				companyParameter.CollectionFeeMethod = companyParameterItemView.CollectionFeeMethod;
				companyParameter.CollectionFeeMinBalCal = companyParameterItemView.CollectionFeeMinBalCal;
				companyParameter.CollectionInvTypeGUID = companyParameterItemView.CollectionInvTypeGUID.StringToGuidNull();
				companyParameter.CollectionOverdueDay = companyParameterItemView.CollectionOverdueDay;
				companyParameter.CompulsoryInvTypeGUID = companyParameterItemView.CompulsoryInvTypeGUID.StringToGuidNull();
				companyParameter.CompulsoryTaxGUID = companyParameterItemView.CompulsoryTaxGUID.StringToGuidNull();
				companyParameter.DepositReturnInvTypeGUID = companyParameterItemView.DepositReturnInvTypeGUID.StringToGuidNull();
				companyParameter.DiscountMethodOfPaymentGUID = companyParameterItemView.DiscountMethodOfPaymentGUID.StringToGuidNull();
				companyParameter.EarlyPayOffToleranceInvTypeGUID = companyParameterItemView.EarlyPayOffToleranceInvTypeGUID.StringToGuidNull();
				companyParameter.EarlyToleranceMethodOfPaymentGUID = companyParameterItemView.EarlyToleranceMethodOfPaymentGUID.StringToGuidNull();
				companyParameter.EarlyToleranceOverAmount = companyParameterItemView.EarlyToleranceOverAmount;
				companyParameter.EarlyToleranceOverLedgerAcc = companyParameterItemView.EarlyToleranceOverLedgerAcc;
				companyParameter.EarlyToleranceUnderAmount = companyParameterItemView.EarlyToleranceUnderAmount;
				companyParameter.EarlyToleranceUnderLedgerAcc = companyParameterItemView.EarlyToleranceUnderLedgerAcc;
				companyParameter.HomeCurrencyGUID = companyParameterItemView.HomeCurrencyGUID.StringToGuidNull();
				companyParameter.InitInvAdvInvTypeGUID = companyParameterItemView.InitInvAdvInvTypeGUID.StringToGuidNull();
				companyParameter.InitInvDepInvTypeGUID = companyParameterItemView.InitInvDepInvTypeGUID.StringToGuidNull();
				companyParameter.InitInvDownInvTypeGUID = companyParameterItemView.InitInvDownInvTypeGUID.StringToGuidNull();
				companyParameter.InstallAdvInvTypeGUID = companyParameterItemView.InstallAdvInvTypeGUID.StringToGuidNull();
				companyParameter.InstallInvTypeGUID = companyParameterItemView.InstallInvTypeGUID.StringToGuidNull();
				companyParameter.InsuranceInvTypeGUID = companyParameterItemView.InsuranceInvTypeGUID.StringToGuidNull();
				companyParameter.InsuranceTaxGUID = companyParameterItemView.InsuranceTaxGUID.StringToGuidNull();
				companyParameter.InvoiceIssuing = companyParameterItemView.InvoiceIssuing;
				companyParameter.InvoiceIssuingDay = companyParameterItemView.InvoiceIssuingDay;
				companyParameter.LeaseSubType = companyParameterItemView.LeaseSubType;
				companyParameter.LeaseType = companyParameterItemView.LeaseType;
				companyParameter.MaintenanceTaxGUID = companyParameterItemView.MaintenanceTaxGUID.StringToGuidNull();
				companyParameter.MaximumPennyDifference = companyParameterItemView.MaximumPennyDifference;
				companyParameter.NPLMethod = companyParameterItemView.NPLMethod;
				companyParameter.NumberSeqVariable = companyParameterItemView.NumberSeqVariable;
				companyParameter.OverdueType = companyParameterItemView.OverdueType;
				companyParameter.PaymStructureRefNum = companyParameterItemView.PaymStructureRefNum;
				companyParameter.PenaltyBase = companyParameterItemView.PenaltyBase;
				companyParameter.PenaltyCalculationMethod = companyParameterItemView.PenaltyCalculationMethod;
				companyParameter.PenaltyInvTypeGUID = companyParameterItemView.PenaltyInvTypeGUID.StringToGuidNull();
				companyParameter.PennyDifferenceLedgerAcc = companyParameterItemView.PennyDifferenceLedgerAcc;
				companyParameter.Product = companyParameterItemView.Product;
				companyParameter.ProductGroup = companyParameterItemView.ProductGroup;
				companyParameter.ProvisionMethod = companyParameterItemView.ProvisionMethod;
				companyParameter.ProvisionRateBy = companyParameterItemView.ProvisionRateBy;
				companyParameter.RealizedGainExchRateLedgerAcc = companyParameterItemView.RealizedGainExchRateLedgerAcc;
				companyParameter.RealizedLossExchRateLedgerAcc = companyParameterItemView.RealizedLossExchRateLedgerAcc;
				companyParameter.ReceiptGenBy = companyParameterItemView.ReceiptGenBy;
				companyParameter.RecordType = companyParameterItemView.RecordType;
				companyParameter.SettleByPeriod = companyParameterItemView.SettleByPeriod;
				companyParameter.SettlementGraceDay = companyParameterItemView.SettlementGraceDay;
				companyParameter.VehicleTaxInvTypeGUID = companyParameterItemView.VehicleTaxInvTypeGUID.StringToGuidNull();
				companyParameter.VehicleTaxServiceFee = companyParameterItemView.VehicleTaxServiceFee;
				companyParameter.VehicleTaxServiceFeeTaxGUID = companyParameterItemView.VehicleTaxServiceFeeTaxGUID.StringToGuidNull();
				companyParameter.WaiveMethodOfPaymentGUID = companyParameterItemView.WaiveMethodOfPaymentGUID.StringToGuidNull();
				companyParameter.BuyerReviewedDay = companyParameterItemView.BuyerReviewedDay;
				companyParameter.ChequeToleranceAmount = companyParameterItemView.ChequeToleranceAmount;
				companyParameter.CompanyCalendarGroupGUID = companyParameterItemView.CompanyCalendarGroupGUID.StringToGuidNull();
				companyParameter.BOTCalendarGroupGUID = companyParameterItemView.BOTCalendarGroupGUID.StringToGuidNull();
				companyParameter.CreditReqInvRevenueTypeGUID = companyParameterItemView.CreditReqInvRevenueTypeGUID.StringToGuidNull();
				companyParameter.DaysPerYear = companyParameterItemView.DaysPerYear;
				companyParameter.MaxCopyFinancialStatementNumOfYear = companyParameterItemView.MaxCopyFinancialStatementNumOfYear;
				companyParameter.MaxCopyTaxReportNumOfMonth = companyParameterItemView.MaxCopyTaxReportNumOfMonth;
				companyParameter.MaxInterestPct = companyParameterItemView.MaxInterestPct;
				companyParameter.MinUpcountryChequeFeeAmount = companyParameterItemView.MinUpcountryChequeFeeAmount;
				companyParameter.ServiceFeeInvTypeGUID = companyParameterItemView.ServiceFeeInvTypeGUID.StringToGuidNull();
				companyParameter.SettlementMethodOfPaymentGUID = companyParameterItemView.SettlementMethodOfPaymentGUID.StringToGuidNull();
				companyParameter.SuspenseMethodOfPaymentGUID = companyParameterItemView.SuspenseMethodOfPaymentGUID.StringToGuidNull();
				companyParameter.UpcountryChequeFeePct = companyParameterItemView.UpcountryChequeFeePct;
				companyParameter.FTBillingFeeInvRevenueTypeGUID = companyParameterItemView.FTBillingFeeInvRevenueTypeGUID.StringToGuidNull();
				companyParameter.FTInterestInvTypeGUID = companyParameterItemView.FTInterestInvTypeGUID.StringToGuidNull();
				companyParameter.FTInterestRefundInvTypeGUID = companyParameterItemView.FTInterestRefundInvTypeGUID.StringToGuidNull();
				companyParameter.FTMaxRollbillInterestDay = companyParameterItemView.FTMaxRollbillInterestDay;
				companyParameter.FTMinInterestDay = companyParameterItemView.FTMinInterestDay;
				companyParameter.FTFeeInvTypeGUID = companyParameterItemView.FTFeeInvTypeGUID.StringToGuidNull();
				companyParameter.FTInvTypeGUID = companyParameterItemView.FTInvTypeGUID.StringToGuidNull();
				companyParameter.FTReceiptFeeInvRevenueTypeGUID = companyParameterItemView.FTReceiptFeeInvRevenueTypeGUID.StringToGuidNull();
				companyParameter.FTReserveInvRevenueTypeGUID = companyParameterItemView.FTReserveInvRevenueTypeGUID.StringToGuidNull();
				companyParameter.FTReserveToBeRefundedInvTypeGUID = companyParameterItemView.FTReserveToBeRefundedInvTypeGUID.StringToGuidNull();
				companyParameter.FTRetentionInvTypeGUID = companyParameterItemView.FTRetentionInvTypeGUID.StringToGuidNull();
				companyParameter.FTRollbillInterestDay = companyParameterItemView.FTRollbillInterestDay;
				companyParameter.FTUnearnedInterestInvTypeGUID = companyParameterItemView.FTUnearnedInterestInvTypeGUID.StringToGuidNull();
				companyParameter.PFInterestCutDay = companyParameterItemView.PFInterestCutDay;
				companyParameter.PFInvTypeGUID = companyParameterItemView.PFInvTypeGUID.StringToGuidNull();
				companyParameter.PFUnearnedInterestInvTypeGUID = companyParameterItemView.PFUnearnedInterestInvTypeGUID.StringToGuidNull();
				companyParameter.PFInterestInvTypeGUID = companyParameterItemView.PFInterestInvTypeGUID.StringToGuidNull();
				companyParameter.PFInterestRefundInvTypeGUID = companyParameterItemView.PFInterestRefundInvTypeGUID.StringToGuidNull();
				companyParameter.PFRetentionInvTypeGUID = companyParameterItemView.PFRetentionInvTypeGUID.StringToGuidNull();
				companyParameter.PFMaxPostponeCollectionDay = companyParameterItemView.PFMaxPostponeCollectionDay;
				companyParameter.PFExtendCreditTermGUID = companyParameterItemView.PFExtendCreditTermGUID.StringToGuidNull();
				companyParameter.PFTermExtensionInvRevenueTypeGUID = companyParameterItemView.PFTermExtensionInvRevenueTypeGUID.StringToGuidNull();
				companyParameter.PFTermExtensionDaysPerFeeAmount = companyParameterItemView.PFTermExtensionDaysPerFeeAmount;
				companyParameter.BondRetentionInvTypeGUID = companyParameterItemView.BondRetentionInvTypeGUID.StringToGuidNull();
				companyParameter.LCRetentionInvTypeGUID = companyParameterItemView.LCRetentionInvTypeGUID.StringToGuidNull();
				companyParameter.FTRollbillUnearnedInterestInvTypeGUID = companyParameterItemView.FTRollbillUnearnedInterestInvTypeGUID.StringToGuidNull();
				companyParameter.OperReportSignatureGUID = companyParameterItemView.OperReportSignatureGUID.StringToGuidNull();
				companyParameter.FTMinSettleInterestDayMaxPct = companyParameterItemView.FTMinSettleInterestDayMaxPct;
				companyParameter.MaxAccruedIntDay = companyParameterItemView.MaxAccruedIntDay;
				companyParameter.NormalAccruedIntDay = companyParameterItemView.NormalAccruedIntDay;
				
				companyParameter.RowVersion = companyParameterItemView.RowVersion;
				return companyParameter;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CompanyParameter> ToCompanyParameter(this IEnumerable<CompanyParameterItemView> companyParameterItemViews)
		{
			try
			{
				List<CompanyParameter> companyParameters = new List<CompanyParameter>();
				foreach (CompanyParameterItemView item in companyParameterItemViews)
				{
					companyParameters.Add(item.ToCompanyParameter());
				}
				return companyParameters;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CompanyParameterItemView ToCompanyParameterItemView(this CompanyParameter companyParameter)
		{
			try
			{
				CompanyParameterItemView companyParameterItemView = new CompanyParameterItemView();
				companyParameterItemView.CompanyGUID = companyParameter.CompanyGUID.GuidNullToString();
				companyParameterItemView.CreatedBy = companyParameter.CreatedBy;
				companyParameterItemView.CreatedDateTime = companyParameter.CreatedDateTime.DateTimeToString();
				companyParameterItemView.ModifiedBy = companyParameter.ModifiedBy;
				companyParameterItemView.ModifiedDateTime = companyParameter.ModifiedDateTime.DateTimeToString();
				companyParameterItemView.Owner = companyParameter.Owner;
				companyParameterItemView.OwnerBusinessUnitGUID = companyParameter.OwnerBusinessUnitGUID.GuidNullToString();
				companyParameterItemView.CompanyParameterGUID = companyParameter.CompanyParameterGUID.GuidNullToString();
				companyParameterItemView.AgreementType = companyParameter.AgreementType;
				companyParameterItemView.AutoSettleToleranceLedgerAcc = companyParameter.AutoSettleToleranceLedgerAcc;
				companyParameterItemView.BOTRealization = companyParameter.BOTRealization;
				companyParameterItemView.CalcSheetIncAdditionalCost = companyParameter.CalcSheetIncAdditionalCost;
				companyParameterItemView.CalcSheetIncTax = companyParameter.CalcSheetIncTax;
				companyParameterItemView.CollectionFeeMethod = companyParameter.CollectionFeeMethod;
				companyParameterItemView.CollectionFeeMinBalCal = companyParameter.CollectionFeeMinBalCal;
				companyParameterItemView.CollectionInvTypeGUID = companyParameter.CollectionInvTypeGUID.GuidNullToString();
				companyParameterItemView.CollectionOverdueDay = companyParameter.CollectionOverdueDay;
				companyParameterItemView.CompulsoryInvTypeGUID = companyParameter.CompulsoryInvTypeGUID.GuidNullToString();
				companyParameterItemView.CompulsoryTaxGUID = companyParameter.CompulsoryTaxGUID.GuidNullToString();
				companyParameterItemView.DepositReturnInvTypeGUID = companyParameter.DepositReturnInvTypeGUID.GuidNullToString();
				companyParameterItemView.DiscountMethodOfPaymentGUID = companyParameter.DiscountMethodOfPaymentGUID.GuidNullToString();
				companyParameterItemView.EarlyPayOffToleranceInvTypeGUID = companyParameter.EarlyPayOffToleranceInvTypeGUID.GuidNullToString();
				companyParameterItemView.EarlyToleranceMethodOfPaymentGUID = companyParameter.EarlyToleranceMethodOfPaymentGUID.GuidNullToString();
				companyParameterItemView.EarlyToleranceOverAmount = companyParameter.EarlyToleranceOverAmount;
				companyParameterItemView.EarlyToleranceOverLedgerAcc = companyParameter.EarlyToleranceOverLedgerAcc;
				companyParameterItemView.EarlyToleranceUnderAmount = companyParameter.EarlyToleranceUnderAmount;
				companyParameterItemView.EarlyToleranceUnderLedgerAcc = companyParameter.EarlyToleranceUnderLedgerAcc;
				companyParameterItemView.HomeCurrencyGUID = companyParameter.HomeCurrencyGUID.GuidNullToString();
				companyParameterItemView.InitInvAdvInvTypeGUID = companyParameter.InitInvAdvInvTypeGUID.GuidNullToString();
				companyParameterItemView.InitInvDepInvTypeGUID = companyParameter.InitInvDepInvTypeGUID.GuidNullToString();
				companyParameterItemView.InitInvDownInvTypeGUID = companyParameter.InitInvDownInvTypeGUID.GuidNullToString();
				companyParameterItemView.InstallAdvInvTypeGUID = companyParameter.InstallAdvInvTypeGUID.GuidNullToString();
				companyParameterItemView.InstallInvTypeGUID = companyParameter.InstallInvTypeGUID.GuidNullToString();
				companyParameterItemView.InsuranceInvTypeGUID = companyParameter.InsuranceInvTypeGUID.GuidNullToString();
				companyParameterItemView.InsuranceTaxGUID = companyParameter.InsuranceTaxGUID.GuidNullToString();
				companyParameterItemView.InvoiceIssuing = companyParameter.InvoiceIssuing;
				companyParameterItemView.InvoiceIssuingDay = companyParameter.InvoiceIssuingDay;
				companyParameterItemView.LeaseSubType = companyParameter.LeaseSubType;
				companyParameterItemView.LeaseType = companyParameter.LeaseType;
				companyParameterItemView.MaintenanceTaxGUID = companyParameter.MaintenanceTaxGUID.GuidNullToString();
				companyParameterItemView.MaximumPennyDifference = companyParameter.MaximumPennyDifference;
				companyParameterItemView.NPLMethod = companyParameter.NPLMethod;
				companyParameterItemView.NumberSeqVariable = companyParameter.NumberSeqVariable;
				companyParameterItemView.OverdueType = companyParameter.OverdueType;
				companyParameterItemView.PaymStructureRefNum = companyParameter.PaymStructureRefNum;
				companyParameterItemView.PenaltyBase = companyParameter.PenaltyBase;
				companyParameterItemView.PenaltyCalculationMethod = companyParameter.PenaltyCalculationMethod;
				companyParameterItemView.PenaltyInvTypeGUID = companyParameter.PenaltyInvTypeGUID.GuidNullToString();
				companyParameterItemView.PennyDifferenceLedgerAcc = companyParameter.PennyDifferenceLedgerAcc;
				companyParameterItemView.Product = companyParameter.Product;
				companyParameterItemView.ProductGroup = companyParameter.ProductGroup;
				companyParameterItemView.ProvisionMethod = companyParameter.ProvisionMethod;
				companyParameterItemView.ProvisionRateBy = companyParameter.ProvisionRateBy;
				companyParameterItemView.RealizedGainExchRateLedgerAcc = companyParameter.RealizedGainExchRateLedgerAcc;
				companyParameterItemView.RealizedLossExchRateLedgerAcc = companyParameter.RealizedLossExchRateLedgerAcc;
				companyParameterItemView.ReceiptGenBy = companyParameter.ReceiptGenBy;
				companyParameterItemView.RecordType = companyParameter.RecordType;
				companyParameterItemView.SettleByPeriod = companyParameter.SettleByPeriod;
				companyParameterItemView.SettlementGraceDay = companyParameter.SettlementGraceDay;
				companyParameterItemView.VehicleTaxInvTypeGUID = companyParameter.VehicleTaxInvTypeGUID.GuidNullToString();
				companyParameterItemView.VehicleTaxServiceFee = companyParameter.VehicleTaxServiceFee;
				companyParameterItemView.VehicleTaxServiceFeeTaxGUID = companyParameter.VehicleTaxServiceFeeTaxGUID.GuidNullToString();
				companyParameterItemView.WaiveMethodOfPaymentGUID = companyParameter.WaiveMethodOfPaymentGUID.GuidNullToString();
				companyParameterItemView.BuyerReviewedDay = companyParameter.BuyerReviewedDay;
				companyParameterItemView.ChequeToleranceAmount = companyParameter.ChequeToleranceAmount;
				companyParameterItemView.CompanyCalendarGroupGUID = companyParameter.CompanyCalendarGroupGUID.GuidNullToString();
				companyParameterItemView.BOTCalendarGroupGUID = companyParameter.BOTCalendarGroupGUID.GuidNullToString();
				companyParameterItemView.CreditReqInvRevenueTypeGUID = companyParameter.CreditReqInvRevenueTypeGUID.GuidNullToString();
				companyParameterItemView.DaysPerYear = companyParameter.DaysPerYear;
				companyParameterItemView.MaxCopyFinancialStatementNumOfYear = companyParameter.MaxCopyFinancialStatementNumOfYear;
				companyParameterItemView.MaxCopyTaxReportNumOfMonth = companyParameter.MaxCopyTaxReportNumOfMonth;
				companyParameterItemView.MaxInterestPct = companyParameter.MaxInterestPct;
				companyParameterItemView.MinUpcountryChequeFeeAmount = companyParameter.MinUpcountryChequeFeeAmount;
				companyParameterItemView.ServiceFeeInvTypeGUID = companyParameter.ServiceFeeInvTypeGUID.GuidNullToString();
				companyParameterItemView.SettlementMethodOfPaymentGUID = companyParameter.SettlementMethodOfPaymentGUID.GuidNullToString();
				companyParameterItemView.SuspenseMethodOfPaymentGUID = companyParameter.SuspenseMethodOfPaymentGUID.GuidNullToString();
				companyParameterItemView.UpcountryChequeFeePct = companyParameter.UpcountryChequeFeePct;
				companyParameterItemView.FTBillingFeeInvRevenueTypeGUID = companyParameter.FTBillingFeeInvRevenueTypeGUID.GuidNullToString();
				companyParameterItemView.FTInterestInvTypeGUID = companyParameter.FTInterestInvTypeGUID.GuidNullToString();
				companyParameterItemView.FTInterestRefundInvTypeGUID = companyParameter.FTInterestRefundInvTypeGUID.GuidNullToString();
				companyParameterItemView.FTMaxRollbillInterestDay = companyParameter.FTMaxRollbillInterestDay;
				companyParameterItemView.FTMinInterestDay = companyParameter.FTMinInterestDay;
				companyParameterItemView.FTFeeInvTypeGUID = companyParameter.FTFeeInvTypeGUID.GuidNullToString();
				companyParameterItemView.FTInvTypeGUID = companyParameter.FTInvTypeGUID.GuidNullToString();
				companyParameterItemView.FTReceiptFeeInvRevenueTypeGUID = companyParameter.FTReceiptFeeInvRevenueTypeGUID.GuidNullToString();
				companyParameterItemView.FTReserveInvRevenueTypeGUID = companyParameter.FTReserveInvRevenueTypeGUID.GuidNullToString();
				companyParameterItemView.FTReserveToBeRefundedInvTypeGUID = companyParameter.FTReserveToBeRefundedInvTypeGUID.GuidNullToString();
				companyParameterItemView.FTRetentionInvTypeGUID = companyParameter.FTRetentionInvTypeGUID.GuidNullToString();
				companyParameterItemView.FTRollbillInterestDay = companyParameter.FTRollbillInterestDay;
				companyParameterItemView.FTUnearnedInterestInvTypeGUID = companyParameter.FTUnearnedInterestInvTypeGUID.GuidNullToString();
				companyParameterItemView.PFInterestCutDay = companyParameter.PFInterestCutDay;
				companyParameterItemView.PFInvTypeGUID = companyParameter.PFInvTypeGUID.GuidNullToString();
				companyParameterItemView.PFUnearnedInterestInvTypeGUID = companyParameter.PFUnearnedInterestInvTypeGUID.GuidNullToString();
				companyParameterItemView.PFInterestInvTypeGUID = companyParameter.PFInterestInvTypeGUID.GuidNullToString();
				companyParameterItemView.PFInterestRefundInvTypeGUID = companyParameter.PFInterestRefundInvTypeGUID.GuidNullToString();
				companyParameterItemView.PFRetentionInvTypeGUID = companyParameter.PFRetentionInvTypeGUID.GuidNullToString();
				companyParameterItemView.PFMaxPostponeCollectionDay = companyParameter.PFMaxPostponeCollectionDay;
				companyParameterItemView.PFExtendCreditTermGUID = companyParameter.PFExtendCreditTermGUID.GuidNullToString();
				companyParameterItemView.PFTermExtensionInvRevenueTypeGUID = companyParameter.PFTermExtensionInvRevenueTypeGUID.GuidNullToString();
				companyParameterItemView.PFTermExtensionDaysPerFeeAmount = companyParameter.PFTermExtensionDaysPerFeeAmount;
				companyParameterItemView.BondRetentionInvTypeGUID = companyParameter.BondRetentionInvTypeGUID.GuidNullToString();
				companyParameterItemView.LCRetentionInvTypeGUID = companyParameter.LCRetentionInvTypeGUID.GuidNullToString();
				companyParameterItemView.FTRollbillUnearnedInterestInvTypeGUID = companyParameter.FTRollbillUnearnedInterestInvTypeGUID.GuidNullToString();
				companyParameterItemView.OperReportSignatureGUID = companyParameter.OperReportSignatureGUID.GuidNullToString();
				companyParameterItemView.FTMinSettleInterestDayMaxPct = companyParameter.FTMinSettleInterestDayMaxPct;
				companyParameterItemView.NormalAccruedIntDay = companyParameter.NormalAccruedIntDay;
				companyParameterItemView.MaxAccruedIntDay = companyParameter.MaxAccruedIntDay;
				
				companyParameterItemView.RowVersion = companyParameter.RowVersion;
				return companyParameterItemView.GetCompanyParameterItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CompanyParameterItemView
		#region ToDropDown
		public static SelectItem<CompanyParameterItemView> ToDropDownItem(this CompanyParameterItemView companyParameterView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CompanyParameterItemView> selectItem = new SelectItem<CompanyParameterItemView>();
				selectItem.Label = null;
				selectItem.Value = companyParameterView.CompanyParameterGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: companyParameterView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CompanyParameterItemView>> ToDropDownItem(this IEnumerable<CompanyParameterItemView> companyParameterItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CompanyParameterItemView>> selectItems = new List<SelectItem<CompanyParameterItemView>>();
				foreach (CompanyParameterItemView item in companyParameterItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

