using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CustTransHandler
	{
		#region CustTransListView
		public static List<CustTransListView> GetCustTransListViewValidation(this List<CustTransListView> custTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return custTransListViews;
			}
			var result = new List<CustTransListView>();
			try
			{
				foreach (CustTransListView item in custTransListViews)
				{
					result.Add(item.GetCustTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CustTransListView GetCustTransListViewValidation(this CustTransListView custTransListView)
		{
			try
			{
				custTransListView.RowAuthorize = custTransListView.GetListRowAuthorize();
				return custTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CustTransListView custTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CustTransListView
		#region CustTransItemView
		public static CustTransItemView GetCustTransItemViewValidation(this CustTransItemView custTransItemView)
		{
			try
			{
				custTransItemView.RowAuthorize = custTransItemView.GetItemRowAuthorize();
				return custTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CustTransItemView custTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CustTrans ToCustTrans(this CustTransItemView custTransItemView)
		{
			try
			{
				CustTrans custTrans = new CustTrans();
				custTrans.CompanyGUID = custTransItemView.CompanyGUID.StringToGuid();
				custTrans.BranchGUID = custTransItemView.BranchGUID.StringToGuid();
				custTrans.CreatedBy = custTransItemView.CreatedBy;
				custTrans.CreatedDateTime = custTransItemView.CreatedDateTime.StringToSystemDateTime();
				custTrans.ModifiedBy = custTransItemView.ModifiedBy;
				custTrans.ModifiedDateTime = custTransItemView.ModifiedDateTime.StringToSystemDateTime();
				custTrans.Owner = custTransItemView.Owner;
				custTrans.OwnerBusinessUnitGUID = custTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				custTrans.CustTransGUID = custTransItemView.CustTransGUID.StringToGuid();
				custTrans.BuyerTableGUID = custTransItemView.BuyerTableGUID.StringToGuidNull();
				custTrans.CreditAppTableGUID = custTransItemView.CreditAppTableGUID.StringToGuidNull();
				custTrans.CurrencyGUID = custTransItemView.CurrencyGUID.StringToGuid();
				custTrans.CustomerTableGUID = custTransItemView.CustomerTableGUID.StringToGuid();
				custTrans.CustTransStatus = custTransItemView.CustTransStatus;
				custTrans.Description = custTransItemView.Description;
				custTrans.DueDate = custTransItemView.DueDate.StringNullToDateNull();
				custTrans.ExchangeRate = custTransItemView.ExchangeRate;
				custTrans.InvoiceTableGUID = custTransItemView.InvoiceTableGUID.StringToGuid();
				custTrans.InvoiceTypeGUID = custTransItemView.InvoiceTypeGUID.StringToGuid();
				custTrans.LastSettleDate = custTransItemView.LastSettleDate.StringNullToDateNull();
				custTrans.ProductType = custTransItemView.ProductType;
				custTrans.SettleAmount = custTransItemView.SettleAmount;
				custTrans.SettleAmountMST = custTransItemView.SettleAmountMST;
				custTrans.TransAmount = custTransItemView.TransAmount;
				custTrans.TransAmountMST = custTransItemView.TransAmountMST;
				custTrans.TransDate = custTransItemView.TransDate.StringToDate();
				
				custTrans.RowVersion = custTransItemView.RowVersion;
				return custTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CustTrans> ToCustTrans(this IEnumerable<CustTransItemView> custTransItemViews)
		{
			try
			{
				List<CustTrans> custTranss = new List<CustTrans>();
				foreach (CustTransItemView item in custTransItemViews)
				{
					custTranss.Add(item.ToCustTrans());
				}
				return custTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CustTransItemView ToCustTransItemView(this CustTrans custTrans)
		{
			try
			{
				CustTransItemView custTransItemView = new CustTransItemView();
				custTransItemView.CompanyGUID = custTrans.CompanyGUID.GuidNullToString();
				custTransItemView.BranchGUID = custTrans.BranchGUID.GuidNullToString();
				custTransItemView.CreatedBy = custTrans.CreatedBy;
				custTransItemView.CreatedDateTime = custTrans.CreatedDateTime.DateTimeToString();
				custTransItemView.ModifiedBy = custTrans.ModifiedBy;
				custTransItemView.ModifiedDateTime = custTrans.ModifiedDateTime.DateTimeToString();
				custTransItemView.Owner = custTrans.Owner;
				custTransItemView.OwnerBusinessUnitGUID = custTrans.OwnerBusinessUnitGUID.GuidNullToString();
				custTransItemView.CustTransGUID = custTrans.CustTransGUID.GuidNullToString();
				custTransItemView.BuyerTableGUID = custTrans.BuyerTableGUID.GuidNullToString();
				custTransItemView.CreditAppTableGUID = custTrans.CreditAppTableGUID.GuidNullToString();
				custTransItemView.CurrencyGUID = custTrans.CurrencyGUID.GuidNullToString();
				custTransItemView.CustomerTableGUID = custTrans.CustomerTableGUID.GuidNullToString();
				custTransItemView.CustTransStatus = custTrans.CustTransStatus;
				custTransItemView.Description = custTrans.Description;
				custTransItemView.DueDate = custTrans.DueDate.DateNullToString();
				custTransItemView.ExchangeRate = custTrans.ExchangeRate;
				custTransItemView.InvoiceTableGUID = custTrans.InvoiceTableGUID.GuidNullToString();
				custTransItemView.InvoiceTypeGUID = custTrans.InvoiceTypeGUID.GuidNullToString();
				custTransItemView.LastSettleDate = custTrans.LastSettleDate.DateNullToString();
				custTransItemView.ProductType = custTrans.ProductType;
				custTransItemView.SettleAmount = custTrans.SettleAmount;
				custTransItemView.SettleAmountMST = custTrans.SettleAmountMST;
				custTransItemView.TransAmount = custTrans.TransAmount;
				custTransItemView.TransAmountMST = custTrans.TransAmountMST;
				custTransItemView.TransDate = custTrans.TransDate.DateToString();
				
				custTransItemView.RowVersion = custTrans.RowVersion;
				return custTransItemView.GetCustTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CustTransItemView
		#region ToDropDown
		public static SelectItem<CustTransItemView> ToDropDownItem(this CustTransItemView custTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CustTransItemView> selectItem = new SelectItem<CustTransItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(custTransView.InvoiceTableGUID.ToString(), custTransView.Description);
				selectItem.Value = custTransView.CustTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: custTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CustTransItemView>> ToDropDownItem(this IEnumerable<CustTransItemView> custTransItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CustTransItemView>> selectItems = new List<SelectItem<CustTransItemView>>();
				foreach (CustTransItemView item in custTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

