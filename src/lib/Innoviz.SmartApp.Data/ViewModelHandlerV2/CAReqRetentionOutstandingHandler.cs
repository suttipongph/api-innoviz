using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CAReqRetentionOutstandingHandler
	{
		#region CAReqRetentionOutstandingListView
		public static List<CAReqRetentionOutstandingListView> GetCAReqRetentionOutstandingListViewValidation(this List<CAReqRetentionOutstandingListView> caReqRetentionOutstandingListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return caReqRetentionOutstandingListViews;
			}
			var result = new List<CAReqRetentionOutstandingListView>();
			try
			{
				foreach (CAReqRetentionOutstandingListView item in caReqRetentionOutstandingListViews)
				{
					result.Add(item.GetCAReqRetentionOutstandingListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CAReqRetentionOutstandingListView GetCAReqRetentionOutstandingListViewValidation(this CAReqRetentionOutstandingListView caReqRetentionOutstandingListView)
		{
			try
			{
				caReqRetentionOutstandingListView.RowAuthorize = caReqRetentionOutstandingListView.GetListRowAuthorize();
				return caReqRetentionOutstandingListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CAReqRetentionOutstandingListView caReqRetentionOutstandingListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CAReqRetentionOutstandingListView
		#region CAReqRetentionOutstandingItemView
		public static CAReqRetentionOutstandingItemView GetCAReqRetentionOutstandingItemViewValidation(this CAReqRetentionOutstandingItemView caReqRetentionOutstandingItemView)
		{
			try
			{
				caReqRetentionOutstandingItemView.RowAuthorize = caReqRetentionOutstandingItemView.GetItemRowAuthorize();
				return caReqRetentionOutstandingItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CAReqRetentionOutstandingItemView caReqRetentionOutstandingItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CAReqRetentionOutstanding ToCAReqRetentionOutstanding(this CAReqRetentionOutstandingItemView caReqRetentionOutstandingItemView)
		{
			try
			{
				CAReqRetentionOutstanding caReqRetentionOutstanding = new CAReqRetentionOutstanding();
				caReqRetentionOutstanding.CompanyGUID = caReqRetentionOutstandingItemView.CompanyGUID.StringToGuid();
				caReqRetentionOutstanding.CreatedBy = caReqRetentionOutstandingItemView.CreatedBy;
				caReqRetentionOutstanding.CreatedDateTime = caReqRetentionOutstandingItemView.CreatedDateTime.StringToSystemDateTime();
				caReqRetentionOutstanding.ModifiedBy = caReqRetentionOutstandingItemView.ModifiedBy;
				caReqRetentionOutstanding.ModifiedDateTime = caReqRetentionOutstandingItemView.ModifiedDateTime.StringToSystemDateTime();
				caReqRetentionOutstanding.Owner = caReqRetentionOutstandingItemView.Owner;
				caReqRetentionOutstanding.OwnerBusinessUnitGUID = caReqRetentionOutstandingItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				caReqRetentionOutstanding.CAReqRetentionOutstandingGUID = caReqRetentionOutstandingItemView.CAReqRetentionOutstandingGUID.StringToGuid();
				caReqRetentionOutstanding.AccumRetentionAmount = caReqRetentionOutstandingItemView.AccumRetentionAmount;
				caReqRetentionOutstanding.CreditAppRequestTableGUID = caReqRetentionOutstandingItemView.CreditAppRequestTableGUID.StringToGuidNull();
				caReqRetentionOutstanding.CreditAppTableGUID = caReqRetentionOutstandingItemView.CreditAppTableGUID.StringToGuidNull();
				caReqRetentionOutstanding.CustomerTableGUID = caReqRetentionOutstandingItemView.CustomerTableGUID.StringToGuidNull();
				caReqRetentionOutstanding.MaximumRetention = caReqRetentionOutstandingItemView.MaximumRetention;
				caReqRetentionOutstanding.ProductType = caReqRetentionOutstandingItemView.ProductType;
				caReqRetentionOutstanding.RemainingAmount = caReqRetentionOutstandingItemView.RemainingAmount;
				
				caReqRetentionOutstanding.RowVersion = caReqRetentionOutstandingItemView.RowVersion;
				return caReqRetentionOutstanding;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CAReqRetentionOutstanding> ToCAReqRetentionOutstanding(this IEnumerable<CAReqRetentionOutstandingItemView> caReqRetentionOutstandingItemViews)
		{
			try
			{
				List<CAReqRetentionOutstanding> caReqRetentionOutstandings = new List<CAReqRetentionOutstanding>();
				foreach (CAReqRetentionOutstandingItemView item in caReqRetentionOutstandingItemViews)
				{
					caReqRetentionOutstandings.Add(item.ToCAReqRetentionOutstanding());
				}
				return caReqRetentionOutstandings;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CAReqRetentionOutstandingItemView ToCAReqRetentionOutstandingItemView(this CAReqRetentionOutstanding caReqRetentionOutstanding)
		{
			try
			{
				CAReqRetentionOutstandingItemView caReqRetentionOutstandingItemView = new CAReqRetentionOutstandingItemView();
				caReqRetentionOutstandingItemView.CompanyGUID = caReqRetentionOutstanding.CompanyGUID.GuidNullToString();
				caReqRetentionOutstandingItemView.CreatedBy = caReqRetentionOutstanding.CreatedBy;
				caReqRetentionOutstandingItemView.CreatedDateTime = caReqRetentionOutstanding.CreatedDateTime.DateTimeToString();
				caReqRetentionOutstandingItemView.ModifiedBy = caReqRetentionOutstanding.ModifiedBy;
				caReqRetentionOutstandingItemView.ModifiedDateTime = caReqRetentionOutstanding.ModifiedDateTime.DateTimeToString();
				caReqRetentionOutstandingItemView.Owner = caReqRetentionOutstanding.Owner;
				caReqRetentionOutstandingItemView.OwnerBusinessUnitGUID = caReqRetentionOutstanding.OwnerBusinessUnitGUID.GuidNullToString();
				caReqRetentionOutstandingItemView.CAReqRetentionOutstandingGUID = caReqRetentionOutstanding.CAReqRetentionOutstandingGUID.GuidNullToString();
				caReqRetentionOutstandingItemView.AccumRetentionAmount = caReqRetentionOutstanding.AccumRetentionAmount;
				caReqRetentionOutstandingItemView.CreditAppRequestTableGUID = caReqRetentionOutstanding.CreditAppRequestTableGUID.GuidNullToString();
				caReqRetentionOutstandingItemView.CreditAppTableGUID = caReqRetentionOutstanding.CreditAppTableGUID.GuidNullToString();
				caReqRetentionOutstandingItemView.CustomerTableGUID = caReqRetentionOutstanding.CustomerTableGUID.GuidNullToString();
				caReqRetentionOutstandingItemView.MaximumRetention = caReqRetentionOutstanding.MaximumRetention;
				caReqRetentionOutstandingItemView.ProductType = caReqRetentionOutstanding.ProductType;
				caReqRetentionOutstandingItemView.RemainingAmount = caReqRetentionOutstanding.RemainingAmount;
				
				caReqRetentionOutstandingItemView.RowVersion = caReqRetentionOutstanding.RowVersion;
				return caReqRetentionOutstandingItemView.GetCAReqRetentionOutstandingItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CAReqRetentionOutstandingItemView
		#region ToDropDown
		public static SelectItem<CAReqRetentionOutstandingItemView> ToDropDownItem(this CAReqRetentionOutstandingItemView caReqRetentionOutstandingView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CAReqRetentionOutstandingItemView> selectItem = new SelectItem<CAReqRetentionOutstandingItemView>();
				selectItem.Label = null;
				selectItem.Value = caReqRetentionOutstandingView.CAReqRetentionOutstandingGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: caReqRetentionOutstandingView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CAReqRetentionOutstandingItemView>> ToDropDownItem(this IEnumerable<CAReqRetentionOutstandingItemView> caReqRetentionOutstandingItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CAReqRetentionOutstandingItemView>> selectItems = new List<SelectItem<CAReqRetentionOutstandingItemView>>();
				foreach (CAReqRetentionOutstandingItemView item in caReqRetentionOutstandingItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

