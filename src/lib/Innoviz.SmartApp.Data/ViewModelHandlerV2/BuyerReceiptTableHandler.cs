using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class BuyerReceiptTableHandler
	{
		#region BuyerReceiptTableListView
		public static List<BuyerReceiptTableListView> GetBuyerReceiptTableListViewValidation(this List<BuyerReceiptTableListView> buyerReceiptTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return buyerReceiptTableListViews;
			}
			var result = new List<BuyerReceiptTableListView>();
			try
			{
				foreach (BuyerReceiptTableListView item in buyerReceiptTableListViews)
				{
					result.Add(item.GetBuyerReceiptTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BuyerReceiptTableListView GetBuyerReceiptTableListViewValidation(this BuyerReceiptTableListView buyerReceiptTableListView)
		{
			try
			{
				buyerReceiptTableListView.RowAuthorize = buyerReceiptTableListView.GetListRowAuthorize();
				return buyerReceiptTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this BuyerReceiptTableListView buyerReceiptTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion BuyerReceiptTableListView
		#region BuyerReceiptTableItemView
		public static BuyerReceiptTableItemView GetBuyerReceiptTableItemViewValidation(this BuyerReceiptTableItemView buyerReceiptTableItemView)
		{
			try
			{
				buyerReceiptTableItemView.RowAuthorize = buyerReceiptTableItemView.GetItemRowAuthorize();
				return buyerReceiptTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this BuyerReceiptTableItemView buyerReceiptTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static BuyerReceiptTable ToBuyerReceiptTable(this BuyerReceiptTableItemView buyerReceiptTableItemView)
		{
			try
			{
				BuyerReceiptTable buyerReceiptTable = new BuyerReceiptTable();
				buyerReceiptTable.CompanyGUID = buyerReceiptTableItemView.CompanyGUID.StringToGuid();
				buyerReceiptTable.CreatedBy = buyerReceiptTableItemView.CreatedBy;
				buyerReceiptTable.CreatedDateTime = buyerReceiptTableItemView.CreatedDateTime.StringToSystemDateTime();
				buyerReceiptTable.ModifiedBy = buyerReceiptTableItemView.ModifiedBy;
				buyerReceiptTable.ModifiedDateTime = buyerReceiptTableItemView.ModifiedDateTime.StringToSystemDateTime();
				buyerReceiptTable.Owner = buyerReceiptTableItemView.Owner;
				buyerReceiptTable.OwnerBusinessUnitGUID = buyerReceiptTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				buyerReceiptTable.BuyerReceiptTableGUID = buyerReceiptTableItemView.BuyerReceiptTableGUID.StringToGuid();
				buyerReceiptTable.AssignmentAgreementTableGUID = buyerReceiptTableItemView.AssignmentAgreementTableGUID.StringToGuidNull();
				buyerReceiptTable.BuyerAgreementTableGUID = buyerReceiptTableItemView.BuyerAgreementTableGUID.StringToGuidNull();
				buyerReceiptTable.BuyerReceiptAddress = buyerReceiptTableItemView.BuyerReceiptAddress;
				buyerReceiptTable.BuyerReceiptAmount = buyerReceiptTableItemView.BuyerReceiptAmount;
				buyerReceiptTable.BuyerReceiptDate = buyerReceiptTableItemView.BuyerReceiptDate.StringToDate();
				buyerReceiptTable.BuyerReceiptId = buyerReceiptTableItemView.BuyerReceiptId;
				buyerReceiptTable.BuyerTableGUID = buyerReceiptTableItemView.BuyerTableGUID.StringToGuid();
				buyerReceiptTable.Cancel = buyerReceiptTableItemView.Cancel;
				buyerReceiptTable.ChequeDate = buyerReceiptTableItemView.ChequeDate.StringNullToDateNull();
				buyerReceiptTable.ChequeNo = buyerReceiptTableItemView.ChequeNo;
				buyerReceiptTable.CustomerTableGUID = buyerReceiptTableItemView.CustomerTableGUID.StringToGuid();
				buyerReceiptTable.MethodOfPaymentGUID = buyerReceiptTableItemView.MethodOfPaymentGUID.StringToGuidNull();
				buyerReceiptTable.RefGUID = buyerReceiptTableItemView.RefGUID.StringToGuidNull();
				buyerReceiptTable.RefType = buyerReceiptTableItemView.RefType;
				buyerReceiptTable.Remark = buyerReceiptTableItemView.Remark;
				buyerReceiptTable.ShowRemarkOnly = buyerReceiptTableItemView.ShowRemarkOnly;
				
				buyerReceiptTable.RowVersion = buyerReceiptTableItemView.RowVersion;
				return buyerReceiptTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<BuyerReceiptTable> ToBuyerReceiptTable(this IEnumerable<BuyerReceiptTableItemView> buyerReceiptTableItemViews)
		{
			try
			{
				List<BuyerReceiptTable> buyerReceiptTables = new List<BuyerReceiptTable>();
				foreach (BuyerReceiptTableItemView item in buyerReceiptTableItemViews)
				{
					buyerReceiptTables.Add(item.ToBuyerReceiptTable());
				}
				return buyerReceiptTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BuyerReceiptTableItemView ToBuyerReceiptTableItemView(this BuyerReceiptTable buyerReceiptTable)
		{
			try
			{
				BuyerReceiptTableItemView buyerReceiptTableItemView = new BuyerReceiptTableItemView();
				buyerReceiptTableItemView.CompanyGUID = buyerReceiptTable.CompanyGUID.GuidNullToString();
				buyerReceiptTableItemView.CreatedBy = buyerReceiptTable.CreatedBy;
				buyerReceiptTableItemView.CreatedDateTime = buyerReceiptTable.CreatedDateTime.DateTimeToString();
				buyerReceiptTableItemView.ModifiedBy = buyerReceiptTable.ModifiedBy;
				buyerReceiptTableItemView.ModifiedDateTime = buyerReceiptTable.ModifiedDateTime.DateTimeToString();
				buyerReceiptTableItemView.Owner = buyerReceiptTable.Owner;
				buyerReceiptTableItemView.OwnerBusinessUnitGUID = buyerReceiptTable.OwnerBusinessUnitGUID.GuidNullToString();
				buyerReceiptTableItemView.BuyerReceiptTableGUID = buyerReceiptTable.BuyerReceiptTableGUID.GuidNullToString();
				buyerReceiptTableItemView.AssignmentAgreementTableGUID = buyerReceiptTable.AssignmentAgreementTableGUID.GuidNullToString();
				buyerReceiptTableItemView.BuyerAgreementTableGUID = buyerReceiptTable.BuyerAgreementTableGUID.GuidNullToString();
				buyerReceiptTableItemView.BuyerReceiptAddress = buyerReceiptTable.BuyerReceiptAddress;
				buyerReceiptTableItemView.BuyerReceiptAmount = buyerReceiptTable.BuyerReceiptAmount;
				buyerReceiptTableItemView.BuyerReceiptDate = buyerReceiptTable.BuyerReceiptDate.DateToString();
				buyerReceiptTableItemView.BuyerReceiptId = buyerReceiptTable.BuyerReceiptId;
				buyerReceiptTableItemView.BuyerTableGUID = buyerReceiptTable.BuyerTableGUID.GuidNullToString();
				buyerReceiptTableItemView.Cancel = buyerReceiptTable.Cancel;
				buyerReceiptTableItemView.ChequeDate = buyerReceiptTable.ChequeDate.DateNullToString();
				buyerReceiptTableItemView.ChequeNo = buyerReceiptTable.ChequeNo;
				buyerReceiptTableItemView.CustomerTableGUID = buyerReceiptTable.CustomerTableGUID.GuidNullToString();
				buyerReceiptTableItemView.MethodOfPaymentGUID = buyerReceiptTable.MethodOfPaymentGUID.GuidNullToString();
				buyerReceiptTableItemView.RefGUID = buyerReceiptTable.RefGUID.GuidNullToString();
				buyerReceiptTableItemView.RefType = buyerReceiptTable.RefType;
				buyerReceiptTableItemView.Remark = buyerReceiptTable.Remark;
				buyerReceiptTableItemView.ShowRemarkOnly = buyerReceiptTable.ShowRemarkOnly;
				
				buyerReceiptTableItemView.RowVersion = buyerReceiptTable.RowVersion;
				return buyerReceiptTableItemView.GetBuyerReceiptTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion BuyerReceiptTableItemView
		#region ToDropDown
		public static SelectItem<BuyerReceiptTableItemView> ToDropDownItem(this BuyerReceiptTableItemView buyerReceiptTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<BuyerReceiptTableItemView> selectItem = new SelectItem<BuyerReceiptTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(buyerReceiptTableView.BuyerReceiptId, buyerReceiptTableView.BuyerReceiptDate.ToString());
				selectItem.Value = buyerReceiptTableView.BuyerReceiptTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: buyerReceiptTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<BuyerReceiptTableItemView>> ToDropDownItem(this IEnumerable<BuyerReceiptTableItemView> buyerReceiptTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<BuyerReceiptTableItemView>> selectItems = new List<SelectItem<BuyerReceiptTableItemView>>();
				foreach (BuyerReceiptTableItemView item in buyerReceiptTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

