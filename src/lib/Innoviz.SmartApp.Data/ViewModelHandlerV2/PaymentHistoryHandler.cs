using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class PaymentHistoryHandler
	{
		#region PaymentHistoryListView
		public static List<PaymentHistoryListView> GetPaymentHistoryListViewValidation(this List<PaymentHistoryListView> paymentHistoryListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return paymentHistoryListViews;
			}
			var result = new List<PaymentHistoryListView>();
			try
			{
				foreach (PaymentHistoryListView item in paymentHistoryListViews)
				{
					result.Add(item.GetPaymentHistoryListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static PaymentHistoryListView GetPaymentHistoryListViewValidation(this PaymentHistoryListView paymentHistoryListView)
		{
			try
			{
				paymentHistoryListView.RowAuthorize = paymentHistoryListView.GetListRowAuthorize();
				return paymentHistoryListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this PaymentHistoryListView paymentHistoryListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion PaymentHistoryListView
		#region PaymentHistoryItemView
		public static PaymentHistoryItemView GetPaymentHistoryItemViewValidation(this PaymentHistoryItemView paymentHistoryItemView)
		{
			try
			{
				paymentHistoryItemView.RowAuthorize = paymentHistoryItemView.GetItemRowAuthorize();
				return paymentHistoryItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this PaymentHistoryItemView paymentHistoryItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static PaymentHistory ToPaymentHistory(this PaymentHistoryItemView paymentHistoryItemView)
		{
			try
			{
				PaymentHistory paymentHistory = new PaymentHistory();
				paymentHistory.CompanyGUID = paymentHistoryItemView.CompanyGUID.StringToGuid();
				paymentHistory.BranchGUID = paymentHistoryItemView.BranchGUID.StringToGuid();
				paymentHistory.CreatedBy = paymentHistoryItemView.CreatedBy;
				paymentHistory.CreatedDateTime = paymentHistoryItemView.CreatedDateTime.StringToSystemDateTime();
				paymentHistory.ModifiedBy = paymentHistoryItemView.ModifiedBy;
				paymentHistory.ModifiedDateTime = paymentHistoryItemView.ModifiedDateTime.StringToSystemDateTime();
				paymentHistory.Owner = paymentHistoryItemView.Owner;
				paymentHistory.OwnerBusinessUnitGUID = paymentHistoryItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				paymentHistory.PaymentHistoryGUID = paymentHistoryItemView.PaymentHistoryGUID.StringToGuid();
				paymentHistory.Cancel = paymentHistoryItemView.Cancel;
				paymentHistory.CreditAppTableGUID = paymentHistoryItemView.CreditAppTableGUID.StringToGuidNull();
				paymentHistory.CurrencyGUID = paymentHistoryItemView.CurrencyGUID.StringToGuid();
				paymentHistory.CustomerTableGUID = paymentHistoryItemView.CustomerTableGUID.StringToGuid();
				paymentHistory.CustTransGUID = paymentHistoryItemView.CustTransGUID.StringToGuid();
				paymentHistory.DueDate = paymentHistoryItemView.DueDate.StringNullToDateNull();
				paymentHistory.ExchangeRate = paymentHistoryItemView.ExchangeRate;
				paymentHistory.InvoiceSettlementDetailGUID = paymentHistoryItemView.InvoiceSettlementDetailGUID.StringToGuidNull();
				paymentHistory.InvoiceTableGUID = paymentHistoryItemView.InvoiceTableGUID.StringToGuid();
				paymentHistory.OrigTaxTableGUID = paymentHistoryItemView.OrigTaxTableGUID.StringToGuidNull();
				paymentHistory.PaymentAmount = paymentHistoryItemView.PaymentAmount;
				paymentHistory.PaymentAmountMST = paymentHistoryItemView.PaymentAmountMST;
				paymentHistory.PaymentBaseAmount = paymentHistoryItemView.PaymentBaseAmount;
				paymentHistory.PaymentBaseAmountMST = paymentHistoryItemView.PaymentBaseAmountMST;
				paymentHistory.PaymentDate = paymentHistoryItemView.PaymentDate.StringToDate();
				paymentHistory.PaymentTaxAmount = paymentHistoryItemView.PaymentTaxAmount;
				paymentHistory.PaymentTaxAmountMST = paymentHistoryItemView.PaymentTaxAmountMST;
				paymentHistory.PaymentTaxBaseAmount = paymentHistoryItemView.PaymentTaxBaseAmount;
				paymentHistory.PaymentTaxBaseAmountMST = paymentHistoryItemView.PaymentTaxBaseAmountMST;
				paymentHistory.ProductType = paymentHistoryItemView.ProductType;
				paymentHistory.ReceiptTableGUID = paymentHistoryItemView.ReceiptTableGUID.StringToGuidNull();
				paymentHistory.ReceivedFrom = paymentHistoryItemView.ReceivedFrom;
				paymentHistory.ReceivedDate = paymentHistoryItemView.ReceivedDate.StringToDate();
				paymentHistory.RefGUID = paymentHistoryItemView.RefGUID.StringToGuidNull();
				paymentHistory.RefType = paymentHistoryItemView.RefType;
				paymentHistory.WHTAmount = paymentHistoryItemView.WHTAmount;
				paymentHistory.WHTAmountMST = paymentHistoryItemView.WHTAmountMST;
				paymentHistory.WithholdingTaxTableGUID = paymentHistoryItemView.WithholdingTaxTableGUID.StringToGuidNull();
				paymentHistory.WHTSlipReceivedByCustomer = paymentHistoryItemView.WHTSlipReceivedByCustomer;
				
				paymentHistory.RowVersion = paymentHistoryItemView.RowVersion;
				return paymentHistory;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<PaymentHistory> ToPaymentHistory(this IEnumerable<PaymentHistoryItemView> paymentHistoryItemViews)
		{
			try
			{
				List<PaymentHistory> paymentHistorys = new List<PaymentHistory>();
				foreach (PaymentHistoryItemView item in paymentHistoryItemViews)
				{
					paymentHistorys.Add(item.ToPaymentHistory());
				}
				return paymentHistorys;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static PaymentHistoryItemView ToPaymentHistoryItemView(this PaymentHistory paymentHistory)
		{
			try
			{
				PaymentHistoryItemView paymentHistoryItemView = new PaymentHistoryItemView();
				paymentHistoryItemView.CompanyGUID = paymentHistory.CompanyGUID.GuidNullToString();
				paymentHistoryItemView.BranchGUID = paymentHistory.BranchGUID.GuidNullToString();
				paymentHistoryItemView.CreatedBy = paymentHistory.CreatedBy;
				paymentHistoryItemView.CreatedDateTime = paymentHistory.CreatedDateTime.DateTimeToString();
				paymentHistoryItemView.ModifiedBy = paymentHistory.ModifiedBy;
				paymentHistoryItemView.ModifiedDateTime = paymentHistory.ModifiedDateTime.DateTimeToString();
				paymentHistoryItemView.Owner = paymentHistory.Owner;
				paymentHistoryItemView.OwnerBusinessUnitGUID = paymentHistory.OwnerBusinessUnitGUID.GuidNullToString();
				paymentHistoryItemView.PaymentHistoryGUID = paymentHistory.PaymentHistoryGUID.GuidNullToString();
				paymentHistoryItemView.Cancel = paymentHistory.Cancel;
				paymentHistoryItemView.CreditAppTableGUID = paymentHistory.CreditAppTableGUID.GuidNullToString();
				paymentHistoryItemView.CurrencyGUID = paymentHistory.CurrencyGUID.GuidNullToString();
				paymentHistoryItemView.CustomerTableGUID = paymentHistory.CustomerTableGUID.GuidNullToString();
				paymentHistoryItemView.CustTransGUID = paymentHistory.CustTransGUID.GuidNullToString();
				paymentHistoryItemView.DueDate = paymentHistory.DueDate.DateNullToString();
				paymentHistoryItemView.ExchangeRate = paymentHistory.ExchangeRate;
				paymentHistoryItemView.InvoiceSettlementDetailGUID = paymentHistory.InvoiceSettlementDetailGUID.GuidNullToString();
				paymentHistoryItemView.InvoiceTableGUID = paymentHistory.InvoiceTableGUID.GuidNullToString();
				paymentHistoryItemView.OrigTaxTableGUID = paymentHistory.OrigTaxTableGUID.GuidNullToString();
				paymentHistoryItemView.PaymentAmount = paymentHistory.PaymentAmount;
				paymentHistoryItemView.PaymentAmountMST = paymentHistory.PaymentAmountMST;
				paymentHistoryItemView.PaymentBaseAmount = paymentHistory.PaymentBaseAmount;
				paymentHistoryItemView.PaymentBaseAmountMST = paymentHistory.PaymentBaseAmountMST;
				paymentHistoryItemView.PaymentDate = paymentHistory.PaymentDate.DateToString();
				paymentHistoryItemView.PaymentTaxAmount = paymentHistory.PaymentTaxAmount;
				paymentHistoryItemView.PaymentTaxAmountMST = paymentHistory.PaymentTaxAmountMST;
				paymentHistoryItemView.PaymentTaxBaseAmount = paymentHistory.PaymentTaxBaseAmount;
				paymentHistoryItemView.PaymentTaxBaseAmountMST = paymentHistory.PaymentTaxBaseAmountMST;
				paymentHistoryItemView.ProductType = paymentHistory.ProductType;
				paymentHistoryItemView.ReceiptTableGUID = paymentHistory.ReceiptTableGUID.GuidNullToString();
				paymentHistoryItemView.ReceivedFrom = paymentHistory.ReceivedFrom;
				paymentHistoryItemView.ReceivedDate = paymentHistory.ReceivedDate.DateToString();
				paymentHistoryItemView.RefGUID = paymentHistory.RefGUID.GuidNullToString();
				paymentHistoryItemView.RefType = paymentHistory.RefType;
				paymentHistoryItemView.WHTAmount = paymentHistory.WHTAmount;
				paymentHistoryItemView.WHTAmountMST = paymentHistory.WHTAmountMST;
				paymentHistoryItemView.WithholdingTaxTableGUID = paymentHistory.WithholdingTaxTableGUID.GuidNullToString();
				paymentHistoryItemView.WHTSlipReceivedByCustomer = paymentHistory.WHTSlipReceivedByCustomer;
				
				paymentHistoryItemView.RowVersion = paymentHistory.RowVersion;
				return paymentHistoryItemView.GetPaymentHistoryItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion PaymentHistoryItemView
		#region ToDropDown
		public static SelectItem<PaymentHistoryItemView> ToDropDownItem(this PaymentHistoryItemView paymentHistoryView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<PaymentHistoryItemView> selectItem = new SelectItem<PaymentHistoryItemView>();
				selectItem.Label = null;
				selectItem.Value = paymentHistoryView.PaymentHistoryGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: paymentHistoryView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<PaymentHistoryItemView>> ToDropDownItem(this IEnumerable<PaymentHistoryItemView> paymentHistoryItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<PaymentHistoryItemView>> selectItems = new List<SelectItem<PaymentHistoryItemView>>();
				foreach (PaymentHistoryItemView item in paymentHistoryItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

