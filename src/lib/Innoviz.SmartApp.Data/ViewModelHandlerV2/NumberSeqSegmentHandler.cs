using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class NumberSeqSegmentHandler
	{
		#region NumberSeqSegmentListView
		public static List<NumberSeqSegmentListView> GetNumberSeqSegmentListViewValidation(this List<NumberSeqSegmentListView> numberSeqSegmentListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return numberSeqSegmentListViews;
			}
			var result = new List<NumberSeqSegmentListView>();
			try
			{
				foreach (NumberSeqSegmentListView item in numberSeqSegmentListViews)
				{
					result.Add(item.GetNumberSeqSegmentListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static NumberSeqSegmentListView GetNumberSeqSegmentListViewValidation(this NumberSeqSegmentListView numberSeqSegmentListView)
		{
			try
			{
				numberSeqSegmentListView.RowAuthorize = numberSeqSegmentListView.GetListRowAuthorize();
				return numberSeqSegmentListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this NumberSeqSegmentListView numberSeqSegmentListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion NumberSeqSegmentListView
		#region NumberSeqSegmentItemView
		public static NumberSeqSegmentItemView GetNumberSeqSegmentItemViewValidation(this NumberSeqSegmentItemView numberSeqSegmentItemView)
		{
			try
			{
				numberSeqSegmentItemView.RowAuthorize = numberSeqSegmentItemView.GetItemRowAuthorize();
				return numberSeqSegmentItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this NumberSeqSegmentItemView numberSeqSegmentItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static NumberSeqSegment ToNumberSeqSegment(this NumberSeqSegmentItemView numberSeqSegmentItemView)
		{
			try
			{
				NumberSeqSegment numberSeqSegment = new NumberSeqSegment();
				numberSeqSegment.CompanyGUID = numberSeqSegmentItemView.CompanyGUID.StringToGuid();
				numberSeqSegment.CreatedBy = numberSeqSegmentItemView.CreatedBy;
				numberSeqSegment.CreatedDateTime = numberSeqSegmentItemView.CreatedDateTime.StringToSystemDateTime();
				numberSeqSegment.ModifiedBy = numberSeqSegmentItemView.ModifiedBy;
				numberSeqSegment.ModifiedDateTime = numberSeqSegmentItemView.ModifiedDateTime.StringToSystemDateTime();
				numberSeqSegment.Owner = numberSeqSegmentItemView.Owner;
				numberSeqSegment.OwnerBusinessUnitGUID = numberSeqSegmentItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				numberSeqSegment.NumberSeqSegmentGUID = numberSeqSegmentItemView.NumberSeqSegmentGUID.StringToGuid();
				numberSeqSegment.NumberSeqTableGUID = numberSeqSegmentItemView.NumberSeqTableGUID.StringToGuid();
				numberSeqSegment.Ordering = numberSeqSegmentItemView.Ordering;
				numberSeqSegment.SegmentType = numberSeqSegmentItemView.SegmentType;
				numberSeqSegment.SegmentValue = numberSeqSegmentItemView.SegmentValue;
				
				numberSeqSegment.RowVersion = numberSeqSegmentItemView.RowVersion;
				return numberSeqSegment;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<NumberSeqSegment> ToNumberSeqSegment(this IEnumerable<NumberSeqSegmentItemView> numberSeqSegmentItemViews)
		{
			try
			{
				List<NumberSeqSegment> numberSeqSegments = new List<NumberSeqSegment>();
				foreach (NumberSeqSegmentItemView item in numberSeqSegmentItemViews)
				{
					numberSeqSegments.Add(item.ToNumberSeqSegment());
				}
				return numberSeqSegments;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static NumberSeqSegmentItemView ToNumberSeqSegmentItemView(this NumberSeqSegment numberSeqSegment)
		{
			try
			{
				NumberSeqSegmentItemView numberSeqSegmentItemView = new NumberSeqSegmentItemView();
				numberSeqSegmentItemView.CompanyGUID = numberSeqSegment.CompanyGUID.GuidNullToString();
				numberSeqSegmentItemView.CreatedBy = numberSeqSegment.CreatedBy;
				numberSeqSegmentItemView.CreatedDateTime = numberSeqSegment.CreatedDateTime.DateTimeToString();
				numberSeqSegmentItemView.ModifiedBy = numberSeqSegment.ModifiedBy;
				numberSeqSegmentItemView.ModifiedDateTime = numberSeqSegment.ModifiedDateTime.DateTimeToString();
				numberSeqSegmentItemView.Owner = numberSeqSegment.Owner;
				numberSeqSegmentItemView.OwnerBusinessUnitGUID = numberSeqSegment.OwnerBusinessUnitGUID.GuidNullToString();
				numberSeqSegmentItemView.NumberSeqSegmentGUID = numberSeqSegment.NumberSeqSegmentGUID.GuidNullToString();
				numberSeqSegmentItemView.NumberSeqTableGUID = numberSeqSegment.NumberSeqTableGUID.GuidNullToString();
				numberSeqSegmentItemView.Ordering = numberSeqSegment.Ordering;
				numberSeqSegmentItemView.SegmentType = numberSeqSegment.SegmentType;
				numberSeqSegmentItemView.SegmentValue = numberSeqSegment.SegmentValue;
				
				numberSeqSegmentItemView.RowVersion = numberSeqSegment.RowVersion;
				return numberSeqSegmentItemView.GetNumberSeqSegmentItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion NumberSeqSegmentItemView
		#region ToDropDown
		public static SelectItem<NumberSeqSegmentItemView> ToDropDownItem(this NumberSeqSegmentItemView numberSeqSegmentView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<NumberSeqSegmentItemView> selectItem = new SelectItem<NumberSeqSegmentItemView>();
				selectItem.Label = null;
				selectItem.Value = numberSeqSegmentView.NumberSeqSegmentGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: numberSeqSegmentView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<NumberSeqSegmentItemView>> ToDropDownItem(this IEnumerable<NumberSeqSegmentItemView> numberSeqSegmentItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<NumberSeqSegmentItemView>> selectItems = new List<SelectItem<NumberSeqSegmentItemView>>();
				foreach (NumberSeqSegmentItemView item in numberSeqSegmentItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

