using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class VendorTableHandler
	{
		#region VendorTableListView
		public static List<VendorTableListView> GetVendorTableListViewValidation(this List<VendorTableListView> vendorTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return vendorTableListViews;
			}
			var result = new List<VendorTableListView>();
			try
			{
				foreach (VendorTableListView item in vendorTableListViews)
				{
					result.Add(item.GetVendorTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static VendorTableListView GetVendorTableListViewValidation(this VendorTableListView vendorTableListView)
		{
			try
			{
				vendorTableListView.RowAuthorize = vendorTableListView.GetListRowAuthorize();
				return vendorTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this VendorTableListView vendorTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion VendorTableListView
		#region VendorTableItemView
		public static VendorTableItemView GetVendorTableItemViewValidation(this VendorTableItemView vendorTableItemView)
		{
			try
			{
				vendorTableItemView.RowAuthorize = vendorTableItemView.GetItemRowAuthorize();
				return vendorTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this VendorTableItemView vendorTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static VendorTable ToVendorTable(this VendorTableItemView vendorTableItemView)
		{
			try
			{
				VendorTable vendorTable = new VendorTable();
				vendorTable.CompanyGUID = vendorTableItemView.CompanyGUID.StringToGuid();
				vendorTable.CreatedBy = vendorTableItemView.CreatedBy;
				vendorTable.CreatedDateTime = vendorTableItemView.CreatedDateTime.StringToSystemDateTime();
				vendorTable.ModifiedBy = vendorTableItemView.ModifiedBy;
				vendorTable.ModifiedDateTime = vendorTableItemView.ModifiedDateTime.StringToSystemDateTime();
				vendorTable.Owner = vendorTableItemView.Owner;
				vendorTable.OwnerBusinessUnitGUID = vendorTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				vendorTable.VendorTableGUID = vendorTableItemView.VendorTableGUID.StringToGuid();
				vendorTable.AltName = vendorTableItemView.AltName;
				vendorTable.CurrencyGUID = vendorTableItemView.CurrencyGUID.StringToGuid();
				vendorTable.ExternalCode = vendorTableItemView.ExternalCode;
				vendorTable.Name = vendorTableItemView.Name;
				vendorTable.RecordType = vendorTableItemView.RecordType;
				vendorTable.SentToOtherSystem = vendorTableItemView.SentToOtherSystem;
				vendorTable.TaxId = vendorTableItemView.TaxId;
				vendorTable.VendGroupGUID = vendorTableItemView.VendGroupGUID.StringToGuid();
				vendorTable.VendorId = vendorTableItemView.VendorId;
				
				vendorTable.RowVersion = vendorTableItemView.RowVersion;
				return vendorTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<VendorTable> ToVendorTable(this IEnumerable<VendorTableItemView> vendorTableItemViews)
		{
			try
			{
				List<VendorTable> vendorTables = new List<VendorTable>();
				foreach (VendorTableItemView item in vendorTableItemViews)
				{
					vendorTables.Add(item.ToVendorTable());
				}
				return vendorTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static VendorTableItemView ToVendorTableItemView(this VendorTable vendorTable)
		{
			try
			{
				VendorTableItemView vendorTableItemView = new VendorTableItemView();
				vendorTableItemView.CompanyGUID = vendorTable.CompanyGUID.GuidNullToString();
				vendorTableItemView.CreatedBy = vendorTable.CreatedBy;
				vendorTableItemView.CreatedDateTime = vendorTable.CreatedDateTime.DateTimeToString();
				vendorTableItemView.ModifiedBy = vendorTable.ModifiedBy;
				vendorTableItemView.ModifiedDateTime = vendorTable.ModifiedDateTime.DateTimeToString();
				vendorTableItemView.Owner = vendorTable.Owner;
				vendorTableItemView.OwnerBusinessUnitGUID = vendorTable.OwnerBusinessUnitGUID.GuidNullToString();
				vendorTableItemView.VendorTableGUID = vendorTable.VendorTableGUID.GuidNullToString();
				vendorTableItemView.AltName = vendorTable.AltName;
				vendorTableItemView.CurrencyGUID = vendorTable.CurrencyGUID.GuidNullToString();
				vendorTableItemView.ExternalCode = vendorTable.ExternalCode;
				vendorTableItemView.Name = vendorTable.Name;
				vendorTableItemView.RecordType = vendorTable.RecordType;
				vendorTableItemView.SentToOtherSystem = vendorTable.SentToOtherSystem;
				vendorTableItemView.TaxId = vendorTable.TaxId;
				vendorTableItemView.VendGroupGUID = vendorTable.VendGroupGUID.GuidNullToString();
				vendorTableItemView.VendorId = vendorTable.VendorId;
				
				vendorTableItemView.RowVersion = vendorTable.RowVersion;
				return vendorTableItemView.GetVendorTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion VendorTableItemView
		#region ToDropDown
		public static SelectItem<VendorTableItemView> ToDropDownItem(this VendorTableItemView vendorTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<VendorTableItemView> selectItem = new SelectItem<VendorTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(vendorTableView.VendorId, vendorTableView.Name);
				selectItem.Value = vendorTableView.VendorTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: vendorTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<VendorTableItemView>> ToDropDownItem(this IEnumerable<VendorTableItemView> vendorTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<VendorTableItemView>> selectItems = new List<SelectItem<VendorTableItemView>>();
				foreach (VendorTableItemView item in vendorTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

