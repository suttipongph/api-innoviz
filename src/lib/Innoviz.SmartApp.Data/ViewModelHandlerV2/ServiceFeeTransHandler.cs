using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class ServiceFeeTransHandler
	{
		#region ServiceFeeTransListView
		public static List<ServiceFeeTransListView> GetServiceFeeTransListViewValidation(this List<ServiceFeeTransListView> serviceFeeTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return serviceFeeTransListViews;
			}
			var result = new List<ServiceFeeTransListView>();
			try
			{
				foreach (ServiceFeeTransListView item in serviceFeeTransListViews)
				{
					result.Add(item.GetServiceFeeTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ServiceFeeTransListView GetServiceFeeTransListViewValidation(this ServiceFeeTransListView serviceFeeTransListView)
		{
			try
			{
				serviceFeeTransListView.RowAuthorize = serviceFeeTransListView.GetListRowAuthorize();
				return serviceFeeTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this ServiceFeeTransListView serviceFeeTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion ServiceFeeTransListView
		#region ServiceFeeTransItemView
		public static ServiceFeeTransItemView GetServiceFeeTransItemViewValidation(this ServiceFeeTransItemView serviceFeeTransItemView)
		{
			try
			{
				serviceFeeTransItemView.RowAuthorize = serviceFeeTransItemView.GetItemRowAuthorize();
				return serviceFeeTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this ServiceFeeTransItemView serviceFeeTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static ServiceFeeTrans ToServiceFeeTrans(this ServiceFeeTransItemView serviceFeeTransItemView)
		{
			try
			{
				ServiceFeeTrans serviceFeeTrans = new ServiceFeeTrans();
				serviceFeeTrans.CompanyGUID = serviceFeeTransItemView.CompanyGUID.StringToGuid();
				serviceFeeTrans.CreatedBy = serviceFeeTransItemView.CreatedBy;
				serviceFeeTrans.CreatedDateTime = serviceFeeTransItemView.CreatedDateTime.StringToSystemDateTime();
				serviceFeeTrans.ModifiedBy = serviceFeeTransItemView.ModifiedBy;
				serviceFeeTrans.ModifiedDateTime = serviceFeeTransItemView.ModifiedDateTime.StringToSystemDateTime();
				serviceFeeTrans.Owner = serviceFeeTransItemView.Owner;
				serviceFeeTrans.OwnerBusinessUnitGUID = serviceFeeTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				serviceFeeTrans.ServiceFeeTransGUID = serviceFeeTransItemView.ServiceFeeTransGUID.StringToGuid();
				serviceFeeTrans.AmountBeforeTax = serviceFeeTransItemView.AmountBeforeTax;
				serviceFeeTrans.AmountIncludeTax = serviceFeeTransItemView.AmountIncludeTax;
				serviceFeeTrans.CNReasonGUID = serviceFeeTransItemView.CNReasonGUID.StringToGuidNull();
				serviceFeeTrans.Description = serviceFeeTransItemView.Description;
				serviceFeeTrans.Dimension1GUID = serviceFeeTransItemView.Dimension1GUID.StringToGuidNull();
				serviceFeeTrans.Dimension2GUID = serviceFeeTransItemView.Dimension2GUID.StringToGuidNull();
				serviceFeeTrans.Dimension3GUID = serviceFeeTransItemView.Dimension3GUID.StringToGuidNull();
				serviceFeeTrans.Dimension4GUID = serviceFeeTransItemView.Dimension4GUID.StringToGuidNull();
				serviceFeeTrans.Dimension5GUID = serviceFeeTransItemView.Dimension5GUID.StringToGuidNull();
				serviceFeeTrans.FeeAmount = serviceFeeTransItemView.FeeAmount;
				serviceFeeTrans.IncludeTax = serviceFeeTransItemView.IncludeTax;
				serviceFeeTrans.InvoiceRevenueTypeGUID = serviceFeeTransItemView.InvoiceRevenueTypeGUID.StringToGuid();
				serviceFeeTrans.Ordering = serviceFeeTransItemView.Ordering;
				serviceFeeTrans.OrigInvoiceAmount = serviceFeeTransItemView.OrigInvoiceAmount;
				serviceFeeTrans.OrigInvoiceId = serviceFeeTransItemView.OrigInvoiceId;
				serviceFeeTrans.OrigTaxInvoiceAmount = serviceFeeTransItemView.OrigTaxInvoiceAmount;
				serviceFeeTrans.OrigTaxInvoiceId = serviceFeeTransItemView.OrigTaxInvoiceId;
				serviceFeeTrans.RefGUID = serviceFeeTransItemView.RefGUID.StringToGuid();
				serviceFeeTrans.RefType = serviceFeeTransItemView.RefType;
				serviceFeeTrans.SettleAmount = serviceFeeTransItemView.SettleAmount;
				serviceFeeTrans.SettleInvoiceAmount = serviceFeeTransItemView.SettleInvoiceAmount;
				serviceFeeTrans.SettleWHTAmount = serviceFeeTransItemView.SettleWHTAmount;
				serviceFeeTrans.TaxAmount = serviceFeeTransItemView.TaxAmount;
				serviceFeeTrans.TaxTableGUID = serviceFeeTransItemView.TaxTableGUID.StringToGuidNull();
				serviceFeeTrans.WHTAmount = serviceFeeTransItemView.WHTAmount;
				serviceFeeTrans.WithholdingTaxTableGUID = serviceFeeTransItemView.WithholdingTaxTableGUID.StringToGuidNull();
				serviceFeeTrans.WHTSlipReceivedByCustomer = serviceFeeTransItemView.WHTSlipReceivedByCustomer;
				
				serviceFeeTrans.RowVersion = serviceFeeTransItemView.RowVersion;
				return serviceFeeTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<ServiceFeeTrans> ToServiceFeeTrans(this IEnumerable<ServiceFeeTransItemView> serviceFeeTransItemViews)
		{
			try
			{
				List<ServiceFeeTrans> serviceFeeTranss = new List<ServiceFeeTrans>();
				foreach (ServiceFeeTransItemView item in serviceFeeTransItemViews)
				{
					serviceFeeTranss.Add(item.ToServiceFeeTrans());
				}
				return serviceFeeTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ServiceFeeTransItemView ToServiceFeeTransItemView(this ServiceFeeTrans serviceFeeTrans)
		{
			try
			{
				ServiceFeeTransItemView serviceFeeTransItemView = new ServiceFeeTransItemView();
				serviceFeeTransItemView.CompanyGUID = serviceFeeTrans.CompanyGUID.GuidNullToString();
				serviceFeeTransItemView.CreatedBy = serviceFeeTrans.CreatedBy;
				serviceFeeTransItemView.CreatedDateTime = serviceFeeTrans.CreatedDateTime.DateTimeToString();
				serviceFeeTransItemView.ModifiedBy = serviceFeeTrans.ModifiedBy;
				serviceFeeTransItemView.ModifiedDateTime = serviceFeeTrans.ModifiedDateTime.DateTimeToString();
				serviceFeeTransItemView.Owner = serviceFeeTrans.Owner;
				serviceFeeTransItemView.OwnerBusinessUnitGUID = serviceFeeTrans.OwnerBusinessUnitGUID.GuidNullToString();
				serviceFeeTransItemView.ServiceFeeTransGUID = serviceFeeTrans.ServiceFeeTransGUID.GuidNullToString();
				serviceFeeTransItemView.AmountBeforeTax = serviceFeeTrans.AmountBeforeTax;
				serviceFeeTransItemView.AmountIncludeTax = serviceFeeTrans.AmountIncludeTax;
				serviceFeeTransItemView.CNReasonGUID = serviceFeeTrans.CNReasonGUID.GuidNullToString();
				serviceFeeTransItemView.Description = serviceFeeTrans.Description;
				serviceFeeTransItemView.Dimension1GUID = serviceFeeTrans.Dimension1GUID.GuidNullToString();
				serviceFeeTransItemView.Dimension2GUID = serviceFeeTrans.Dimension2GUID.GuidNullToString();
				serviceFeeTransItemView.Dimension3GUID = serviceFeeTrans.Dimension3GUID.GuidNullToString();
				serviceFeeTransItemView.Dimension4GUID = serviceFeeTrans.Dimension4GUID.GuidNullToString();
				serviceFeeTransItemView.Dimension5GUID = serviceFeeTrans.Dimension5GUID.GuidNullToString();
				serviceFeeTransItemView.FeeAmount = serviceFeeTrans.FeeAmount;
				serviceFeeTransItemView.IncludeTax = serviceFeeTrans.IncludeTax;
				serviceFeeTransItemView.InvoiceRevenueTypeGUID = serviceFeeTrans.InvoiceRevenueTypeGUID.GuidNullToString();
				serviceFeeTransItemView.Ordering = serviceFeeTrans.Ordering;
				serviceFeeTransItemView.OrigInvoiceAmount = serviceFeeTrans.OrigInvoiceAmount;
				serviceFeeTransItemView.OrigInvoiceId = serviceFeeTrans.OrigInvoiceId;
				serviceFeeTransItemView.OrigTaxInvoiceAmount = serviceFeeTrans.OrigTaxInvoiceAmount;
				serviceFeeTransItemView.OrigTaxInvoiceId = serviceFeeTrans.OrigTaxInvoiceId;
				serviceFeeTransItemView.RefGUID = serviceFeeTrans.RefGUID.GuidNullToString();
				serviceFeeTransItemView.RefType = serviceFeeTrans.RefType;
				serviceFeeTransItemView.SettleAmount = serviceFeeTrans.SettleAmount;
				serviceFeeTransItemView.SettleInvoiceAmount = serviceFeeTrans.SettleInvoiceAmount;
				serviceFeeTransItemView.SettleWHTAmount = serviceFeeTrans.SettleWHTAmount;
				serviceFeeTransItemView.TaxAmount = serviceFeeTrans.TaxAmount;
				serviceFeeTransItemView.TaxTableGUID = serviceFeeTrans.TaxTableGUID.GuidNullToString();
				serviceFeeTransItemView.WHTAmount = serviceFeeTrans.WHTAmount;
				serviceFeeTransItemView.WithholdingTaxTableGUID = serviceFeeTrans.WithholdingTaxTableGUID.GuidNullToString();
				
				serviceFeeTransItemView.RowVersion = serviceFeeTrans.RowVersion;
				return serviceFeeTransItemView.GetServiceFeeTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ServiceFeeTransItemView
		#region ToDropDown
		public static SelectItem<ServiceFeeTransItemView> ToDropDownItem(this ServiceFeeTransItemView serviceFeeTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<ServiceFeeTransItemView> selectItem = new SelectItem<ServiceFeeTransItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(serviceFeeTransView.InvoiceRevenueTypeGUID.ToString(), serviceFeeTransView.Description);
				selectItem.Value = serviceFeeTransView.ServiceFeeTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: serviceFeeTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<ServiceFeeTransItemView>> ToDropDownItem(this IEnumerable<ServiceFeeTransItemView> serviceFeeTransItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<ServiceFeeTransItemView>> selectItems = new List<SelectItem<ServiceFeeTransItemView>>();
				foreach (ServiceFeeTransItemView item in serviceFeeTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

