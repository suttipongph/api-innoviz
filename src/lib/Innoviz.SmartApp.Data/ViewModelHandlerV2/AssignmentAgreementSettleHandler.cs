using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class AssignmentAgreementSettleHandler
	{
		#region AssignmentAgreementSettleListView
		public static List<AssignmentAgreementSettleListView> GetAssignmentAgreementSettleListViewValidation(this List<AssignmentAgreementSettleListView> assignmentAgreementSettleListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return assignmentAgreementSettleListViews;
			}
			var result = new List<AssignmentAgreementSettleListView>();
			try
			{
				foreach (AssignmentAgreementSettleListView item in assignmentAgreementSettleListViews)
				{
					result.Add(item.GetAssignmentAgreementSettleListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AssignmentAgreementSettleListView GetAssignmentAgreementSettleListViewValidation(this AssignmentAgreementSettleListView assignmentAgreementSettleListView)
		{
			try
			{
				assignmentAgreementSettleListView.RowAuthorize = assignmentAgreementSettleListView.GetListRowAuthorize();
				return assignmentAgreementSettleListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this AssignmentAgreementSettleListView assignmentAgreementSettleListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion AssignmentAgreementSettleListView
		#region AssignmentAgreementSettleItemView
		public static AssignmentAgreementSettleItemView GetAssignmentAgreementSettleItemViewValidation(this AssignmentAgreementSettleItemView assignmentAgreementSettleItemView)
		{
			try
			{
				assignmentAgreementSettleItemView.RowAuthorize = assignmentAgreementSettleItemView.GetItemRowAuthorize();
				return assignmentAgreementSettleItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this AssignmentAgreementSettleItemView assignmentAgreementSettleItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static AssignmentAgreementSettle ToAssignmentAgreementSettle(this AssignmentAgreementSettleItemView assignmentAgreementSettleItemView)
		{
			try
			{
				AssignmentAgreementSettle assignmentAgreementSettle = new AssignmentAgreementSettle();
				assignmentAgreementSettle.CompanyGUID = assignmentAgreementSettleItemView.CompanyGUID.StringToGuid();
				assignmentAgreementSettle.CreatedBy = assignmentAgreementSettleItemView.CreatedBy;
				assignmentAgreementSettle.CreatedDateTime = assignmentAgreementSettleItemView.CreatedDateTime.StringToSystemDateTime();
				assignmentAgreementSettle.ModifiedBy = assignmentAgreementSettleItemView.ModifiedBy;
				assignmentAgreementSettle.ModifiedDateTime = assignmentAgreementSettleItemView.ModifiedDateTime.StringToSystemDateTime();
				assignmentAgreementSettle.Owner = assignmentAgreementSettleItemView.Owner;
				assignmentAgreementSettle.OwnerBusinessUnitGUID = assignmentAgreementSettleItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				assignmentAgreementSettle.AssignmentAgreementSettleGUID = assignmentAgreementSettleItemView.AssignmentAgreementSettleGUID.StringToGuid();
				assignmentAgreementSettle.AssignmentAgreementTableGUID = assignmentAgreementSettleItemView.AssignmentAgreementTableGUID.StringToGuid();
				assignmentAgreementSettle.BuyerAgreementTableGUID = assignmentAgreementSettleItemView.BuyerAgreementTableGUID.StringToGuidNull();
				assignmentAgreementSettle.DocumentId = assignmentAgreementSettleItemView.DocumentId;
				assignmentAgreementSettle.DocumentReasonGUID = assignmentAgreementSettleItemView.DocumentReasonGUID.StringToGuidNull();
				assignmentAgreementSettle.InvoiceTableGUID = assignmentAgreementSettleItemView.InvoiceTableGUID.StringToGuidNull();
				assignmentAgreementSettle.RefAssignmentAgreementSettleGUID = assignmentAgreementSettleItemView.RefAssignmentAgreementSettleGUID.StringToGuidNull();
				assignmentAgreementSettle.RefGUID = assignmentAgreementSettleItemView.RefGUID.StringToGuidNull();
				assignmentAgreementSettle.RefType = assignmentAgreementSettleItemView.RefType;
				assignmentAgreementSettle.SettledAmount = assignmentAgreementSettleItemView.SettledAmount;
				assignmentAgreementSettle.SettledDate = assignmentAgreementSettleItemView.SettledDate.StringToDate();
				
				assignmentAgreementSettle.RowVersion = assignmentAgreementSettleItemView.RowVersion;
				return assignmentAgreementSettle;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<AssignmentAgreementSettle> ToAssignmentAgreementSettle(this IEnumerable<AssignmentAgreementSettleItemView> assignmentAgreementSettleItemViews)
		{
			try
			{
				List<AssignmentAgreementSettle> assignmentAgreementSettles = new List<AssignmentAgreementSettle>();
				foreach (AssignmentAgreementSettleItemView item in assignmentAgreementSettleItemViews)
				{
					assignmentAgreementSettles.Add(item.ToAssignmentAgreementSettle());
				}
				return assignmentAgreementSettles;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AssignmentAgreementSettleItemView ToAssignmentAgreementSettleItemView(this AssignmentAgreementSettle assignmentAgreementSettle)
		{
			try
			{
				AssignmentAgreementSettleItemView assignmentAgreementSettleItemView = new AssignmentAgreementSettleItemView();
				assignmentAgreementSettleItemView.CompanyGUID = assignmentAgreementSettle.CompanyGUID.GuidNullToString();
				assignmentAgreementSettleItemView.CreatedBy = assignmentAgreementSettle.CreatedBy;
				assignmentAgreementSettleItemView.CreatedDateTime = assignmentAgreementSettle.CreatedDateTime.DateTimeToString();
				assignmentAgreementSettleItemView.ModifiedBy = assignmentAgreementSettle.ModifiedBy;
				assignmentAgreementSettleItemView.ModifiedDateTime = assignmentAgreementSettle.ModifiedDateTime.DateTimeToString();
				assignmentAgreementSettleItemView.Owner = assignmentAgreementSettle.Owner;
				assignmentAgreementSettleItemView.OwnerBusinessUnitGUID = assignmentAgreementSettle.OwnerBusinessUnitGUID.GuidNullToString();
				assignmentAgreementSettleItemView.AssignmentAgreementSettleGUID = assignmentAgreementSettle.AssignmentAgreementSettleGUID.GuidNullToString();
				assignmentAgreementSettleItemView.AssignmentAgreementTableGUID = assignmentAgreementSettle.AssignmentAgreementTableGUID.GuidNullToString();
				assignmentAgreementSettleItemView.BuyerAgreementTableGUID = assignmentAgreementSettle.BuyerAgreementTableGUID.GuidNullToString();
				assignmentAgreementSettleItemView.DocumentId = assignmentAgreementSettle.DocumentId;
				assignmentAgreementSettleItemView.DocumentReasonGUID = assignmentAgreementSettle.DocumentReasonGUID.GuidNullToString();
				assignmentAgreementSettleItemView.InvoiceTableGUID = assignmentAgreementSettle.InvoiceTableGUID.GuidNullToString();
				assignmentAgreementSettleItemView.RefAssignmentAgreementSettleGUID = assignmentAgreementSettle.RefAssignmentAgreementSettleGUID.GuidNullToString();
				assignmentAgreementSettleItemView.RefGUID = assignmentAgreementSettle.RefGUID.GuidNullToString();
				assignmentAgreementSettleItemView.RefType = assignmentAgreementSettle.RefType;
				assignmentAgreementSettleItemView.SettledAmount = assignmentAgreementSettle.SettledAmount;
				assignmentAgreementSettleItemView.SettledDate = assignmentAgreementSettle.SettledDate.DateToString();
				
				assignmentAgreementSettleItemView.RowVersion = assignmentAgreementSettle.RowVersion;
				return assignmentAgreementSettleItemView.GetAssignmentAgreementSettleItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion AssignmentAgreementSettleItemView
		#region ToDropDown
		public static SelectItem<AssignmentAgreementSettleItemView> ToDropDownItem(this AssignmentAgreementSettleItemView assignmentAgreementSettleView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<AssignmentAgreementSettleItemView> selectItem = new SelectItem<AssignmentAgreementSettleItemView>();
				selectItem.Label = null;
				selectItem.Value = assignmentAgreementSettleView.AssignmentAgreementSettleGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: assignmentAgreementSettleView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<AssignmentAgreementSettleItemView>> ToDropDownItem(this IEnumerable<AssignmentAgreementSettleItemView> assignmentAgreementSettleItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<AssignmentAgreementSettleItemView>> selectItems = new List<SelectItem<AssignmentAgreementSettleItemView>>();
				foreach (AssignmentAgreementSettleItemView item in assignmentAgreementSettleItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

