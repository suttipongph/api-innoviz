using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class BankTypeHandler
	{
		#region BankTypeListView
		public static List<BankTypeListView> GetBankTypeListViewValidation(this List<BankTypeListView> bankTypeListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return bankTypeListViews;
			}
			var result = new List<BankTypeListView>();
			try
			{
				foreach (BankTypeListView item in bankTypeListViews)
				{
					result.Add(item.GetBankTypeListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BankTypeListView GetBankTypeListViewValidation(this BankTypeListView bankTypeListView)
		{
			try
			{
				bankTypeListView.RowAuthorize = bankTypeListView.GetListRowAuthorize();
				return bankTypeListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this BankTypeListView bankTypeListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion BankTypeListView
		#region BankTypeItemView
		public static BankTypeItemView GetBankTypeItemViewValidation(this BankTypeItemView bankTypeItemView)
		{
			try
			{
				bankTypeItemView.RowAuthorize = bankTypeItemView.GetItemRowAuthorize();
				return bankTypeItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this BankTypeItemView bankTypeItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static BankType ToBankType(this BankTypeItemView bankTypeItemView)
		{
			try
			{
				BankType bankType = new BankType();
				bankType.CompanyGUID = bankTypeItemView.CompanyGUID.StringToGuid();
				bankType.CreatedBy = bankTypeItemView.CreatedBy;
				bankType.CreatedDateTime = bankTypeItemView.CreatedDateTime.StringToSystemDateTime();
				bankType.ModifiedBy = bankTypeItemView.ModifiedBy;
				bankType.ModifiedDateTime = bankTypeItemView.ModifiedDateTime.StringToSystemDateTime();
				bankType.Owner = bankTypeItemView.Owner;
				bankType.OwnerBusinessUnitGUID = bankTypeItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				bankType.BankTypeGUID = bankTypeItemView.BankTypeGUID.StringToGuid();
				bankType.BankTypeId = bankTypeItemView.BankTypeId;
				bankType.Description = bankTypeItemView.Description;
				
				bankType.RowVersion = bankTypeItemView.RowVersion;
				return bankType;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<BankType> ToBankType(this IEnumerable<BankTypeItemView> bankTypeItemViews)
		{
			try
			{
				List<BankType> bankTypes = new List<BankType>();
				foreach (BankTypeItemView item in bankTypeItemViews)
				{
					bankTypes.Add(item.ToBankType());
				}
				return bankTypes;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BankTypeItemView ToBankTypeItemView(this BankType bankType)
		{
			try
			{
				BankTypeItemView bankTypeItemView = new BankTypeItemView();
				bankTypeItemView.CompanyGUID = bankType.CompanyGUID.GuidNullToString();
				bankTypeItemView.CreatedBy = bankType.CreatedBy;
				bankTypeItemView.CreatedDateTime = bankType.CreatedDateTime.DateTimeToString();
				bankTypeItemView.ModifiedBy = bankType.ModifiedBy;
				bankTypeItemView.ModifiedDateTime = bankType.ModifiedDateTime.DateTimeToString();
				bankTypeItemView.Owner = bankType.Owner;
				bankTypeItemView.OwnerBusinessUnitGUID = bankType.OwnerBusinessUnitGUID.GuidNullToString();
				bankTypeItemView.BankTypeGUID = bankType.BankTypeGUID.GuidNullToString();
				bankTypeItemView.BankTypeId = bankType.BankTypeId;
				bankTypeItemView.Description = bankType.Description;
				
				bankTypeItemView.RowVersion = bankType.RowVersion;
				return bankTypeItemView.GetBankTypeItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion BankTypeItemView
		#region ToDropDown
		public static SelectItem<BankTypeItemView> ToDropDownItem(this BankTypeItemView bankTypeView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<BankTypeItemView> selectItem = new SelectItem<BankTypeItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(bankTypeView.BankTypeId, bankTypeView.Description);
				selectItem.Value = bankTypeView.BankTypeGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: bankTypeView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<BankTypeItemView>> ToDropDownItem(this IEnumerable<BankTypeItemView> bankTypeItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<BankTypeItemView>> selectItems = new List<SelectItem<BankTypeItemView>>();
				foreach (BankTypeItemView item in bankTypeItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

