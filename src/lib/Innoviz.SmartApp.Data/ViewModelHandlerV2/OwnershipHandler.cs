using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class OwnershipHandler
	{
		#region OwnershipListView
		public static List<OwnershipListView> GetOwnershipListViewValidation(this List<OwnershipListView> ownershipListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return ownershipListViews;
			}
			var result = new List<OwnershipListView>();
			try
			{
				foreach (OwnershipListView item in ownershipListViews)
				{
					result.Add(item.GetOwnershipListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static OwnershipListView GetOwnershipListViewValidation(this OwnershipListView ownershipListView)
		{
			try
			{
				ownershipListView.RowAuthorize = ownershipListView.GetListRowAuthorize();
				return ownershipListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this OwnershipListView ownershipListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion OwnershipListView
		#region OwnershipItemView
		public static OwnershipItemView GetOwnershipItemViewValidation(this OwnershipItemView ownershipItemView)
		{
			try
			{
				ownershipItemView.RowAuthorize = ownershipItemView.GetItemRowAuthorize();
				return ownershipItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this OwnershipItemView ownershipItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static Ownership ToOwnership(this OwnershipItemView ownershipItemView)
		{
			try
			{
				Ownership ownership = new Ownership();
				ownership.CompanyGUID = ownershipItemView.CompanyGUID.StringToGuid();
				ownership.CreatedBy = ownershipItemView.CreatedBy;
				ownership.CreatedDateTime = ownershipItemView.CreatedDateTime.StringToSystemDateTime();
				ownership.ModifiedBy = ownershipItemView.ModifiedBy;
				ownership.ModifiedDateTime = ownershipItemView.ModifiedDateTime.StringToSystemDateTime();
				ownership.Owner = ownershipItemView.Owner;
				ownership.OwnerBusinessUnitGUID = ownershipItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				ownership.OwnershipGUID = ownershipItemView.OwnershipGUID.StringToGuid();
				ownership.Description = ownershipItemView.Description;
				ownership.OwnershipId = ownershipItemView.OwnershipId;
				
				ownership.RowVersion = ownershipItemView.RowVersion;
				return ownership;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<Ownership> ToOwnership(this IEnumerable<OwnershipItemView> ownershipItemViews)
		{
			try
			{
				List<Ownership> ownerships = new List<Ownership>();
				foreach (OwnershipItemView item in ownershipItemViews)
				{
					ownerships.Add(item.ToOwnership());
				}
				return ownerships;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static OwnershipItemView ToOwnershipItemView(this Ownership ownership)
		{
			try
			{
				OwnershipItemView ownershipItemView = new OwnershipItemView();
				ownershipItemView.CompanyGUID = ownership.CompanyGUID.GuidNullToString();
				ownershipItemView.CreatedBy = ownership.CreatedBy;
				ownershipItemView.CreatedDateTime = ownership.CreatedDateTime.DateTimeToString();
				ownershipItemView.ModifiedBy = ownership.ModifiedBy;
				ownershipItemView.ModifiedDateTime = ownership.ModifiedDateTime.DateTimeToString();
				ownershipItemView.Owner = ownership.Owner;
				ownershipItemView.OwnerBusinessUnitGUID = ownership.OwnerBusinessUnitGUID.GuidNullToString();
				ownershipItemView.OwnershipGUID = ownership.OwnershipGUID.GuidNullToString();
				ownershipItemView.Description = ownership.Description;
				ownershipItemView.OwnershipId = ownership.OwnershipId;
				
				ownershipItemView.RowVersion = ownership.RowVersion;
				return ownershipItemView.GetOwnershipItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion OwnershipItemView
		#region ToDropDown
		public static SelectItem<OwnershipItemView> ToDropDownItem(this OwnershipItemView ownershipView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<OwnershipItemView> selectItem = new SelectItem<OwnershipItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(ownershipView.OwnershipId,ownershipView.Description);
				selectItem.Value = ownershipView.OwnershipGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: ownershipView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<OwnershipItemView>> ToDropDownItem(this IEnumerable<OwnershipItemView> ownershipItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<OwnershipItemView>> selectItems = new List<SelectItem<OwnershipItemView>>();
				foreach (OwnershipItemView item in ownershipItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

