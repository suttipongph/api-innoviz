using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class BookmarkDocumentTransHandler
	{
		#region BookmarkDocumentTransListView
		public static List<BookmarkDocumentTransListView> GetBookmarkDocumentTransListViewValidation(this List<BookmarkDocumentTransListView> bookmarkDocumentTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return bookmarkDocumentTransListViews;
			}
			var result = new List<BookmarkDocumentTransListView>();
			try
			{
				foreach (BookmarkDocumentTransListView item in bookmarkDocumentTransListViews)
				{
					result.Add(item.GetBookmarkDocumentTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BookmarkDocumentTransListView GetBookmarkDocumentTransListViewValidation(this BookmarkDocumentTransListView bookmarkDocumentTransListView)
		{
			try
			{
				bookmarkDocumentTransListView.RowAuthorize = bookmarkDocumentTransListView.GetListRowAuthorize();
				return bookmarkDocumentTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this BookmarkDocumentTransListView bookmarkDocumentTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion BookmarkDocumentTransListView
		#region BookmarkDocumentTransItemView
		public static BookmarkDocumentTransItemView GetBookmarkDocumentTransItemViewValidation(this BookmarkDocumentTransItemView bookmarkDocumentTransItemView)
		{
			try
			{
				bookmarkDocumentTransItemView.RowAuthorize = bookmarkDocumentTransItemView.GetItemRowAuthorize();
				return bookmarkDocumentTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this BookmarkDocumentTransItemView bookmarkDocumentTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static BookmarkDocumentTrans ToBookmarkDocumentTrans(this BookmarkDocumentTransItemView bookmarkDocumentTransItemView)
		{
			try
			{
				BookmarkDocumentTrans bookmarkDocumentTrans = new BookmarkDocumentTrans();
				bookmarkDocumentTrans.CompanyGUID = bookmarkDocumentTransItemView.CompanyGUID.StringToGuid();
				bookmarkDocumentTrans.CreatedBy = bookmarkDocumentTransItemView.CreatedBy;
				bookmarkDocumentTrans.CreatedDateTime = bookmarkDocumentTransItemView.CreatedDateTime.StringToSystemDateTime();
				bookmarkDocumentTrans.ModifiedBy = bookmarkDocumentTransItemView.ModifiedBy;
				bookmarkDocumentTrans.ModifiedDateTime = bookmarkDocumentTransItemView.ModifiedDateTime.StringToSystemDateTime();
				bookmarkDocumentTrans.Owner = bookmarkDocumentTransItemView.Owner;
				bookmarkDocumentTrans.OwnerBusinessUnitGUID = bookmarkDocumentTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				bookmarkDocumentTrans.BookmarkDocumentTransGUID = bookmarkDocumentTransItemView.BookmarkDocumentTransGUID.StringToGuid();
				bookmarkDocumentTrans.BookmarkDocumentGUID = bookmarkDocumentTransItemView.BookmarkDocumentGUID.StringToGuid();
				bookmarkDocumentTrans.DocumentStatusGUID = bookmarkDocumentTransItemView.DocumentStatusGUID.StringToGuid();
				bookmarkDocumentTrans.DocumentTemplateTableGUID = bookmarkDocumentTransItemView.DocumentTemplateTableGUID.StringToGuid();
				bookmarkDocumentTrans.ReferenceExternalDate = bookmarkDocumentTransItemView.ReferenceExternalDate;
				bookmarkDocumentTrans.ReferenceExternalId = bookmarkDocumentTransItemView.ReferenceExternalId;
				bookmarkDocumentTrans.RefGUID = bookmarkDocumentTransItemView.RefGUID.StringToGuid();
				bookmarkDocumentTrans.RefType = bookmarkDocumentTransItemView.RefType;
				bookmarkDocumentTrans.Remark = bookmarkDocumentTransItemView.Remark;
				
				bookmarkDocumentTrans.RowVersion = bookmarkDocumentTransItemView.RowVersion;
				return bookmarkDocumentTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<BookmarkDocumentTrans> ToBookmarkDocumentTrans(this IEnumerable<BookmarkDocumentTransItemView> bookmarkDocumentTransItemViews)
		{
			try
			{
				List<BookmarkDocumentTrans> bookmarkDocumentTranss = new List<BookmarkDocumentTrans>();
				foreach (BookmarkDocumentTransItemView item in bookmarkDocumentTransItemViews)
				{
					bookmarkDocumentTranss.Add(item.ToBookmarkDocumentTrans());
				}
				return bookmarkDocumentTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BookmarkDocumentTransItemView ToBookmarkDocumentTransItemView(this BookmarkDocumentTrans bookmarkDocumentTrans)
		{
			try
			{
				BookmarkDocumentTransItemView bookmarkDocumentTransItemView = new BookmarkDocumentTransItemView();
				bookmarkDocumentTransItemView.CompanyGUID = bookmarkDocumentTrans.CompanyGUID.GuidNullToString();
				bookmarkDocumentTransItemView.CreatedBy = bookmarkDocumentTrans.CreatedBy;
				bookmarkDocumentTransItemView.CreatedDateTime = bookmarkDocumentTrans.CreatedDateTime.DateTimeToString();
				bookmarkDocumentTransItemView.ModifiedBy = bookmarkDocumentTrans.ModifiedBy;
				bookmarkDocumentTransItemView.ModifiedDateTime = bookmarkDocumentTrans.ModifiedDateTime.DateTimeToString();
				bookmarkDocumentTransItemView.Owner = bookmarkDocumentTrans.Owner;
				bookmarkDocumentTransItemView.OwnerBusinessUnitGUID = bookmarkDocumentTrans.OwnerBusinessUnitGUID.GuidNullToString();
				bookmarkDocumentTransItemView.BookmarkDocumentTransGUID = bookmarkDocumentTrans.BookmarkDocumentTransGUID.GuidNullToString();
				bookmarkDocumentTransItemView.BookmarkDocumentGUID = bookmarkDocumentTrans.BookmarkDocumentGUID.GuidNullToString();
				bookmarkDocumentTransItemView.DocumentStatusGUID = bookmarkDocumentTrans.DocumentStatusGUID.GuidNullToString();
				bookmarkDocumentTransItemView.DocumentTemplateTableGUID = bookmarkDocumentTrans.DocumentTemplateTableGUID.GuidNullToString();
				bookmarkDocumentTransItemView.ReferenceExternalId = bookmarkDocumentTrans.ReferenceExternalId;
				bookmarkDocumentTransItemView.RefGUID = bookmarkDocumentTrans.RefGUID.GuidNullToString();
				bookmarkDocumentTransItemView.RefType = bookmarkDocumentTrans.RefType;
				bookmarkDocumentTransItemView.Remark = bookmarkDocumentTrans.Remark;
				
				bookmarkDocumentTransItemView.RowVersion = bookmarkDocumentTrans.RowVersion;
				return bookmarkDocumentTransItemView.GetBookmarkDocumentTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion BookmarkDocumentTransItemView
		#region ToDropDown
		public static SelectItem<BookmarkDocumentTransItemView> ToDropDownItem(this BookmarkDocumentTransItemView bookmarkDocumentTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<BookmarkDocumentTransItemView> selectItem = new SelectItem<BookmarkDocumentTransItemView>();
				selectItem.Label = null;
				selectItem.Value = bookmarkDocumentTransView.BookmarkDocumentTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: bookmarkDocumentTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<BookmarkDocumentTransItemView>> ToDropDownItem(this IEnumerable<BookmarkDocumentTransItemView> bookmarkDocumentTransItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<BookmarkDocumentTransItemView>> selectItems = new List<SelectItem<BookmarkDocumentTransItemView>>();
				foreach (BookmarkDocumentTransItemView item in bookmarkDocumentTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

