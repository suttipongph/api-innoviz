using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CustVisitingTransHandler
	{
		#region CustVisitingTransListView
		public static List<CustVisitingTransListView> GetCustVisitingTransListViewValidation(this List<CustVisitingTransListView> custVisitingTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return custVisitingTransListViews;
			}
			var result = new List<CustVisitingTransListView>();
			try
			{
				foreach (CustVisitingTransListView item in custVisitingTransListViews)
				{
					result.Add(item.GetCustVisitingTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CustVisitingTransListView GetCustVisitingTransListViewValidation(this CustVisitingTransListView custVisitingTransListView)
		{
			try
			{
				custVisitingTransListView.RowAuthorize = custVisitingTransListView.GetListRowAuthorize();
				return custVisitingTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CustVisitingTransListView custVisitingTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CustVisitingTransListView
		#region CustVisitingTransItemView
		public static CustVisitingTransItemView GetCustVisitingTransItemViewValidation(this CustVisitingTransItemView custVisitingTransItemView)
		{
			try
			{
				custVisitingTransItemView.RowAuthorize = custVisitingTransItemView.GetItemRowAuthorize();
				return custVisitingTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CustVisitingTransItemView custVisitingTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CustVisitingTrans ToCustVisitingTrans(this CustVisitingTransItemView custVisitingTransItemView)
		{
			try
			{
				CustVisitingTrans custVisitingTrans = new CustVisitingTrans();
				custVisitingTrans.CompanyGUID = custVisitingTransItemView.CompanyGUID.StringToGuid();
				custVisitingTrans.CreatedBy = custVisitingTransItemView.CreatedBy;
				custVisitingTrans.CreatedDateTime = custVisitingTransItemView.CreatedDateTime.StringToSystemDateTime();
				custVisitingTrans.ModifiedBy = custVisitingTransItemView.ModifiedBy;
				custVisitingTrans.ModifiedDateTime = custVisitingTransItemView.ModifiedDateTime.StringToSystemDateTime();
				custVisitingTrans.Owner = custVisitingTransItemView.Owner;
				custVisitingTrans.OwnerBusinessUnitGUID = custVisitingTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				custVisitingTrans.CustVisitingTransGUID = custVisitingTransItemView.CustVisitingTransGUID.StringToGuid();
				custVisitingTrans.CustVisitingDate = custVisitingTransItemView.CustVisitingDate.StringToDate();
				custVisitingTrans.CustVisitingMemo = custVisitingTransItemView.CustVisitingMemo;
				custVisitingTrans.Description = custVisitingTransItemView.Description;
				custVisitingTrans.RefGUID = custVisitingTransItemView.RefGUID.StringToGuidNull();
				custVisitingTrans.RefType = custVisitingTransItemView.RefType;
				custVisitingTrans.ResponsibleByGUID = custVisitingTransItemView.ResponsibleByGUID.StringToGuid();
				
				custVisitingTrans.RowVersion = custVisitingTransItemView.RowVersion;
				return custVisitingTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CustVisitingTrans> ToCustVisitingTrans(this IEnumerable<CustVisitingTransItemView> custVisitingTransItemViews)
		{
			try
			{
				List<CustVisitingTrans> custVisitingTranss = new List<CustVisitingTrans>();
				foreach (CustVisitingTransItemView item in custVisitingTransItemViews)
				{
					custVisitingTranss.Add(item.ToCustVisitingTrans());
				}
				return custVisitingTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CustVisitingTransItemView ToCustVisitingTransItemView(this CustVisitingTrans custVisitingTrans)
		{
			try
			{
				CustVisitingTransItemView custVisitingTransItemView = new CustVisitingTransItemView();
				custVisitingTransItemView.CompanyGUID = custVisitingTrans.CompanyGUID.GuidNullToString();
				custVisitingTransItemView.CreatedBy = custVisitingTrans.CreatedBy;
				custVisitingTransItemView.CreatedDateTime = custVisitingTrans.CreatedDateTime.DateTimeToString();
				custVisitingTransItemView.ModifiedBy = custVisitingTrans.ModifiedBy;
				custVisitingTransItemView.ModifiedDateTime = custVisitingTrans.ModifiedDateTime.DateTimeToString();
				custVisitingTransItemView.Owner = custVisitingTrans.Owner;
				custVisitingTransItemView.OwnerBusinessUnitGUID = custVisitingTrans.OwnerBusinessUnitGUID.GuidNullToString();
				custVisitingTransItemView.CustVisitingTransGUID = custVisitingTrans.CustVisitingTransGUID.GuidNullToString();
				custVisitingTransItemView.CustVisitingDate = custVisitingTrans.CustVisitingDate.DateToString();
				custVisitingTransItemView.CustVisitingMemo = custVisitingTrans.CustVisitingMemo;
				custVisitingTransItemView.Description = custVisitingTrans.Description;
				custVisitingTransItemView.RefGUID = custVisitingTrans.RefGUID.GuidNullToString();
				custVisitingTransItemView.RefType = custVisitingTrans.RefType;
				custVisitingTransItemView.ResponsibleByGUID = custVisitingTrans.ResponsibleByGUID.GuidNullToString();
				
				custVisitingTransItemView.RowVersion = custVisitingTrans.RowVersion;
				return custVisitingTransItemView.GetCustVisitingTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CustVisitingTransItemView
		#region ToDropDown
		public static SelectItem<CustVisitingTransItemView> ToDropDownItem(this CustVisitingTransItemView custVisitingTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CustVisitingTransItemView> selectItem = new SelectItem<CustVisitingTransItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(custVisitingTransView.CustVisitingDate.ToString(), custVisitingTransView.Description);
				selectItem.Value = custVisitingTransView.CustVisitingTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: custVisitingTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CustVisitingTransItemView>> ToDropDownItem(this IEnumerable<CustVisitingTransItemView> custVisitingTransItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CustVisitingTransItemView>> selectItems = new List<SelectItem<CustVisitingTransItemView>>();
				foreach (CustVisitingTransItemView item in custVisitingTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

