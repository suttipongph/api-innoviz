using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class ReceiptLineHandler
	{
		#region ReceiptLineListView
		public static List<ReceiptLineListView> GetReceiptLineListViewValidation(this List<ReceiptLineListView> receiptLineListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return receiptLineListViews;
			}
			var result = new List<ReceiptLineListView>();
			try
			{
				foreach (ReceiptLineListView item in receiptLineListViews)
				{
					result.Add(item.GetReceiptLineListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ReceiptLineListView GetReceiptLineListViewValidation(this ReceiptLineListView receiptLineListView)
		{
			try
			{
				receiptLineListView.RowAuthorize = receiptLineListView.GetListRowAuthorize();
				return receiptLineListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this ReceiptLineListView receiptLineListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion ReceiptLineListView
		#region ReceiptLineItemView
		public static ReceiptLineItemView GetReceiptLineItemViewValidation(this ReceiptLineItemView receiptLineItemView)
		{
			try
			{
				receiptLineItemView.RowAuthorize = receiptLineItemView.GetItemRowAuthorize();
				return receiptLineItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this ReceiptLineItemView receiptLineItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static ReceiptLine ToReceiptLine(this ReceiptLineItemView receiptLineItemView)
		{
			try
			{
				ReceiptLine receiptLine = new ReceiptLine();
				receiptLine.CompanyGUID = receiptLineItemView.CompanyGUID.StringToGuid();
				receiptLine.BranchGUID = receiptLineItemView.BranchGUID.StringToGuid();
				receiptLine.CreatedBy = receiptLineItemView.CreatedBy;
				receiptLine.CreatedDateTime = receiptLineItemView.CreatedDateTime.StringToSystemDateTime();
				receiptLine.ModifiedBy = receiptLineItemView.ModifiedBy;
				receiptLine.ModifiedDateTime = receiptLineItemView.ModifiedDateTime.StringToSystemDateTime();
				receiptLine.Owner = receiptLineItemView.Owner;
				receiptLine.OwnerBusinessUnitGUID = receiptLineItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				receiptLine.ReceiptLineGUID = receiptLineItemView.ReceiptLineGUID.StringToGuid();
				receiptLine.DueDate = receiptLineItemView.DueDate.StringNullToDateNull();
				receiptLine.InvoiceAmount = receiptLineItemView.InvoiceAmount;
				receiptLine.InvoiceTableGUID = receiptLineItemView.InvoiceTableGUID.StringToGuidNull();
				receiptLine.LineNum = receiptLineItemView.LineNum;
				receiptLine.ReceiptTableGUID = receiptLineItemView.ReceiptTableGUID.StringToGuid();
				receiptLine.SettleAmount = receiptLineItemView.SettleAmount;
				receiptLine.SettleAmountMST = receiptLineItemView.SettleAmountMST;
				receiptLine.SettleBaseAmount = receiptLineItemView.SettleBaseAmount;
				receiptLine.SettleBaseAmountMST = receiptLineItemView.SettleBaseAmountMST;
				receiptLine.SettleTaxAmount = receiptLineItemView.SettleTaxAmount;
				receiptLine.SettleTaxAmountMST = receiptLineItemView.SettleTaxAmountMST;
				receiptLine.TaxAmount = receiptLineItemView.TaxAmount;
				receiptLine.TaxTableGUID = receiptLineItemView.TaxTableGUID.StringToGuidNull();
				receiptLine.WHTAmount = receiptLineItemView.WHTAmount;
				receiptLine.WithholdingTaxTableGUID = receiptLineItemView.WithholdingTaxTableGUID.StringToGuidNull();
				
				receiptLine.RowVersion = receiptLineItemView.RowVersion;
				return receiptLine;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<ReceiptLine> ToReceiptLine(this IEnumerable<ReceiptLineItemView> receiptLineItemViews)
		{
			try
			{
				List<ReceiptLine> receiptLines = new List<ReceiptLine>();
				foreach (ReceiptLineItemView item in receiptLineItemViews)
				{
					receiptLines.Add(item.ToReceiptLine());
				}
				return receiptLines;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ReceiptLineItemView ToReceiptLineItemView(this ReceiptLine receiptLine)
		{
			try
			{
				ReceiptLineItemView receiptLineItemView = new ReceiptLineItemView();
				receiptLineItemView.CompanyGUID = receiptLine.CompanyGUID.GuidNullToString();
				receiptLineItemView.BranchGUID = receiptLine.BranchGUID.GuidNullToString();
				receiptLineItemView.CreatedBy = receiptLine.CreatedBy;
				receiptLineItemView.CreatedDateTime = receiptLine.CreatedDateTime.DateTimeToString();
				receiptLineItemView.ModifiedBy = receiptLine.ModifiedBy;
				receiptLineItemView.ModifiedDateTime = receiptLine.ModifiedDateTime.DateTimeToString();
				receiptLineItemView.Owner = receiptLine.Owner;
				receiptLineItemView.OwnerBusinessUnitGUID = receiptLine.OwnerBusinessUnitGUID.GuidNullToString();
				receiptLineItemView.ReceiptLineGUID = receiptLine.ReceiptLineGUID.GuidNullToString();
				receiptLineItemView.DueDate = receiptLine.DueDate.DateNullToString();
				receiptLineItemView.InvoiceAmount = receiptLine.InvoiceAmount;
				receiptLineItemView.InvoiceTableGUID = receiptLine.InvoiceTableGUID.GuidNullToString();
				receiptLineItemView.LineNum = receiptLine.LineNum;
				receiptLineItemView.ReceiptTableGUID = receiptLine.ReceiptTableGUID.GuidNullToString();
				receiptLineItemView.SettleAmount = receiptLine.SettleAmount;
				receiptLineItemView.SettleAmountMST = receiptLine.SettleAmountMST;
				receiptLineItemView.SettleBaseAmount = receiptLine.SettleBaseAmount;
				receiptLineItemView.SettleBaseAmountMST = receiptLine.SettleBaseAmountMST;
				receiptLineItemView.SettleTaxAmount = receiptLine.SettleTaxAmount;
				receiptLineItemView.SettleTaxAmountMST = receiptLine.SettleTaxAmountMST;
				receiptLineItemView.TaxAmount = receiptLine.TaxAmount;
				receiptLineItemView.TaxTableGUID = receiptLine.TaxTableGUID.GuidNullToString();
				receiptLineItemView.WHTAmount = receiptLine.WHTAmount;
				receiptLineItemView.WithholdingTaxTableGUID = receiptLine.WithholdingTaxTableGUID.GuidNullToString();
				
				receiptLineItemView.RowVersion = receiptLine.RowVersion;
				return receiptLineItemView.GetReceiptLineItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ReceiptLineItemView
		#region ToDropDown
		public static SelectItem<ReceiptLineItemView> ToDropDownItem(this ReceiptLineItemView receiptLineView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<ReceiptLineItemView> selectItem = new SelectItem<ReceiptLineItemView>();
				selectItem.Label = null;
				selectItem.Value = receiptLineView.ReceiptLineGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: receiptLineView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<ReceiptLineItemView>> ToDropDownItem(this IEnumerable<ReceiptLineItemView> receiptLineItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<ReceiptLineItemView>> selectItems = new List<SelectItem<ReceiptLineItemView>>();
				foreach (ReceiptLineItemView item in receiptLineItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

