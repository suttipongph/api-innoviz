using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CustomerRefundTableHandler
	{
		#region CustomerRefundTableListView
		public static List<CustomerRefundTableListView> GetCustomerRefundTableListViewValidation(this List<CustomerRefundTableListView> customerRefundTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return customerRefundTableListViews;
			}
			var result = new List<CustomerRefundTableListView>();
			try
			{
				foreach (CustomerRefundTableListView item in customerRefundTableListViews)
				{
					result.Add(item.GetCustomerRefundTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CustomerRefundTableListView GetCustomerRefundTableListViewValidation(this CustomerRefundTableListView customerRefundTableListView)
		{
			try
			{
				customerRefundTableListView.RowAuthorize = customerRefundTableListView.GetListRowAuthorize();
				return customerRefundTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CustomerRefundTableListView customerRefundTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CustomerRefundTableListView
		#region CustomerRefundTableItemView
		public static CustomerRefundTableItemView GetCustomerRefundTableItemViewValidation(this CustomerRefundTableItemView customerRefundTableItemView)
		{
			try
			{
				customerRefundTableItemView.RowAuthorize = customerRefundTableItemView.GetItemRowAuthorize();
				return customerRefundTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CustomerRefundTableItemView customerRefundTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CustomerRefundTable ToCustomerRefundTable(this CustomerRefundTableItemView customerRefundTableItemView)
		{
			try
			{
				CustomerRefundTable customerRefundTable = new CustomerRefundTable();
				customerRefundTable.CompanyGUID = customerRefundTableItemView.CompanyGUID.StringToGuid();
				customerRefundTable.CreatedBy = customerRefundTableItemView.CreatedBy;
				customerRefundTable.CreatedDateTime = customerRefundTableItemView.CreatedDateTime.StringToSystemDateTime();
				customerRefundTable.ModifiedBy = customerRefundTableItemView.ModifiedBy;
				customerRefundTable.ModifiedDateTime = customerRefundTableItemView.ModifiedDateTime.StringToSystemDateTime();
				customerRefundTable.Owner = customerRefundTableItemView.Owner;
				customerRefundTable.OwnerBusinessUnitGUID = customerRefundTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				customerRefundTable.CustomerRefundTableGUID = customerRefundTableItemView.CustomerRefundTableGUID.StringToGuid();
				customerRefundTable.CreditAppTableGUID = customerRefundTableItemView.CreditAppTableGUID.StringToGuidNull();
				customerRefundTable.CurrencyGUID = customerRefundTableItemView.CurrencyGUID.StringToGuid();
				customerRefundTable.CustomerRefundId = customerRefundTableItemView.CustomerRefundId;
				customerRefundTable.CustomerTableGUID = customerRefundTableItemView.CustomerTableGUID.StringToGuid();
				customerRefundTable.Description = customerRefundTableItemView.Description;
				customerRefundTable.Dimension1GUID = customerRefundTableItemView.Dimension1GUID.StringToGuidNull();
				customerRefundTable.Dimension2GUID = customerRefundTableItemView.Dimension2GUID.StringToGuidNull();
				customerRefundTable.Dimension3GUID = customerRefundTableItemView.Dimension3GUID.StringToGuidNull();
				customerRefundTable.Dimension4GUID = customerRefundTableItemView.Dimension4GUID.StringToGuidNull();
				customerRefundTable.Dimension5GUID = customerRefundTableItemView.Dimension5GUID.StringToGuidNull();
				customerRefundTable.DocumentReasonGUID = customerRefundTableItemView.DocumentReasonGUID.StringToGuidNull();
				customerRefundTable.DocumentStatusGUID = customerRefundTableItemView.DocumentStatusGUID.StringToGuidNull();
				customerRefundTable.ExchangeRate = customerRefundTableItemView.ExchangeRate;
				customerRefundTable.OperReportSignatureGUID = customerRefundTableItemView.OperReportSignatureGUID.StringToGuid();
				customerRefundTable.PaymentAmount = customerRefundTableItemView.PaymentAmount;
				customerRefundTable.ProductType = customerRefundTableItemView.ProductType;
				customerRefundTable.RetentionAmount = customerRefundTableItemView.RetentionAmount;
				customerRefundTable.RetentionCalculateBase = customerRefundTableItemView.RetentionCalculateBase;
				customerRefundTable.RetentionPct = customerRefundTableItemView.RetentionPct;
				customerRefundTable.ServiceFeeAmount = customerRefundTableItemView.ServiceFeeAmount;
				customerRefundTable.SettleAmount = customerRefundTableItemView.SettleAmount;
				customerRefundTable.SuspenseAmount = customerRefundTableItemView.SuspenseAmount;
				customerRefundTable.SuspenseInvoiceType = customerRefundTableItemView.SuspenseInvoiceType;
				customerRefundTable.TransDate = customerRefundTableItemView.TransDate.StringToDate();
				customerRefundTable.Remark = customerRefundTableItemView.Remark;
				
				customerRefundTable.RowVersion = customerRefundTableItemView.RowVersion;
				return customerRefundTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CustomerRefundTable> ToCustomerRefundTable(this IEnumerable<CustomerRefundTableItemView> customerRefundTableItemViews)
		{
			try
			{
				List<CustomerRefundTable> customerRefundTables = new List<CustomerRefundTable>();
				foreach (CustomerRefundTableItemView item in customerRefundTableItemViews)
				{
					customerRefundTables.Add(item.ToCustomerRefundTable());
				}
				return customerRefundTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CustomerRefundTableItemView ToCustomerRefundTableItemView(this CustomerRefundTable customerRefundTable)
		{
			try
			{
				CustomerRefundTableItemView customerRefundTableItemView = new CustomerRefundTableItemView();
				customerRefundTableItemView.CompanyGUID = customerRefundTable.CompanyGUID.GuidNullToString();
				customerRefundTableItemView.CreatedBy = customerRefundTable.CreatedBy;
				customerRefundTableItemView.CreatedDateTime = customerRefundTable.CreatedDateTime.DateTimeToString();
				customerRefundTableItemView.ModifiedBy = customerRefundTable.ModifiedBy;
				customerRefundTableItemView.ModifiedDateTime = customerRefundTable.ModifiedDateTime.DateTimeToString();
				customerRefundTableItemView.Owner = customerRefundTable.Owner;
				customerRefundTableItemView.OwnerBusinessUnitGUID = customerRefundTable.OwnerBusinessUnitGUID.GuidNullToString();
				customerRefundTableItemView.CustomerRefundTableGUID = customerRefundTable.CustomerRefundTableGUID.GuidNullToString();
				customerRefundTableItemView.CreditAppTableGUID = customerRefundTable.CreditAppTableGUID.GuidNullToString();
				customerRefundTableItemView.CurrencyGUID = customerRefundTable.CurrencyGUID.GuidNullToString();
				customerRefundTableItemView.CustomerRefundId = customerRefundTable.CustomerRefundId;
				customerRefundTableItemView.CustomerTableGUID = customerRefundTable.CustomerTableGUID.GuidNullToString();
				customerRefundTableItemView.Description = customerRefundTable.Description;
				customerRefundTableItemView.Dimension1GUID = customerRefundTable.Dimension1GUID.GuidNullToString();
				customerRefundTableItemView.Dimension2GUID = customerRefundTable.Dimension2GUID.GuidNullToString();
				customerRefundTableItemView.Dimension3GUID = customerRefundTable.Dimension3GUID.GuidNullToString();
				customerRefundTableItemView.Dimension4GUID = customerRefundTable.Dimension4GUID.GuidNullToString();
				customerRefundTableItemView.Dimension5GUID = customerRefundTable.Dimension5GUID.GuidNullToString();
				customerRefundTableItemView.DocumentReasonGUID = customerRefundTable.DocumentReasonGUID.GuidNullToString();
				customerRefundTableItemView.DocumentStatusGUID = customerRefundTable.DocumentStatusGUID.GuidNullToString();
				customerRefundTableItemView.ExchangeRate = customerRefundTable.ExchangeRate;
				customerRefundTableItemView.OperReportSignatureGUID = customerRefundTable.OperReportSignatureGUID.GuidNullToString();
				customerRefundTableItemView.PaymentAmount = customerRefundTable.PaymentAmount;
				customerRefundTableItemView.ProductType = customerRefundTable.ProductType;
				customerRefundTableItemView.RetentionAmount = customerRefundTable.RetentionAmount;
				customerRefundTableItemView.RetentionCalculateBase = customerRefundTable.RetentionCalculateBase;
				customerRefundTableItemView.RetentionPct = customerRefundTable.RetentionPct;
				customerRefundTableItemView.ServiceFeeAmount = customerRefundTable.ServiceFeeAmount;
				customerRefundTableItemView.SettleAmount = customerRefundTable.SettleAmount;
				customerRefundTableItemView.SuspenseAmount = customerRefundTable.SuspenseAmount;
				customerRefundTableItemView.SuspenseInvoiceType = customerRefundTable.SuspenseInvoiceType;
				customerRefundTableItemView.TransDate = customerRefundTable.TransDate.DateToString();
				customerRefundTableItemView.Remark = customerRefundTable.Remark;
				
				customerRefundTableItemView.RowVersion = customerRefundTable.RowVersion;
				return customerRefundTableItemView.GetCustomerRefundTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CustomerRefundTableItemView
		#region ToDropDown
		public static SelectItem<CustomerRefundTableItemView> ToDropDownItem(this CustomerRefundTableItemView customerRefundTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CustomerRefundTableItemView> selectItem = new SelectItem<CustomerRefundTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(customerRefundTableView.CustomerRefundId, customerRefundTableView.Description);
				selectItem.Value = customerRefundTableView.CustomerRefundTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: customerRefundTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CustomerRefundTableItemView>> ToDropDownItem(this IEnumerable<CustomerRefundTableItemView> customerRefundTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CustomerRefundTableItemView>> selectItems = new List<SelectItem<CustomerRefundTableItemView>>();
				foreach (CustomerRefundTableItemView item in customerRefundTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

