using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class PaymentDetailHandler
	{
		#region PaymentDetailListView
		public static List<PaymentDetailListView> GetPaymentDetailListViewValidation(this List<PaymentDetailListView> paymentDetailListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return paymentDetailListViews;
			}
			var result = new List<PaymentDetailListView>();
			try
			{
				foreach (PaymentDetailListView item in paymentDetailListViews)
				{
					result.Add(item.GetPaymentDetailListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static PaymentDetailListView GetPaymentDetailListViewValidation(this PaymentDetailListView paymentDetailListView)
		{
			try
			{
				paymentDetailListView.RowAuthorize = paymentDetailListView.GetListRowAuthorize();
				return paymentDetailListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this PaymentDetailListView paymentDetailListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion PaymentDetailListView
		#region PaymentDetailItemView
		public static PaymentDetailItemView GetPaymentDetailItemViewValidation(this PaymentDetailItemView paymentDetailItemView)
		{
			try
			{
				paymentDetailItemView.RowAuthorize = paymentDetailItemView.GetItemRowAuthorize();
				return paymentDetailItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this PaymentDetailItemView paymentDetailItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static PaymentDetail ToPaymentDetail(this PaymentDetailItemView paymentDetailItemView)
		{
			try
			{
				PaymentDetail paymentDetail = new PaymentDetail();
				paymentDetail.CompanyGUID = paymentDetailItemView.CompanyGUID.StringToGuid();
				paymentDetail.CreatedBy = paymentDetailItemView.CreatedBy;
				paymentDetail.CreatedDateTime = paymentDetailItemView.CreatedDateTime.StringToSystemDateTime();
				paymentDetail.ModifiedBy = paymentDetailItemView.ModifiedBy;
				paymentDetail.ModifiedDateTime = paymentDetailItemView.ModifiedDateTime.StringToSystemDateTime();
				paymentDetail.Owner = paymentDetailItemView.Owner;
				paymentDetail.OwnerBusinessUnitGUID = paymentDetailItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				paymentDetail.PaymentDetailGUID = paymentDetailItemView.PaymentDetailGUID.StringToGuid();
				paymentDetail.CustomerTableGUID = paymentDetailItemView.CustomerTableGUID.StringToGuidNull();
				paymentDetail.InvoiceTypeGUID = paymentDetailItemView.InvoiceTypeGUID.StringToGuidNull();
				paymentDetail.PaidToType = paymentDetailItemView.PaidToType;
				paymentDetail.PaymentAmount = paymentDetailItemView.PaymentAmount;
				paymentDetail.RefGUID = paymentDetailItemView.RefGUID.StringToGuidNull();
				paymentDetail.RefType = paymentDetailItemView.RefType;
				paymentDetail.SuspenseTransfer = paymentDetailItemView.SuspenseTransfer;
				paymentDetail.VendorTableGUID = paymentDetailItemView.VendorTableGUID.StringToGuidNull();
				
				paymentDetail.RowVersion = paymentDetailItemView.RowVersion;
				return paymentDetail;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<PaymentDetail> ToPaymentDetail(this IEnumerable<PaymentDetailItemView> paymentDetailItemViews)
		{
			try
			{
				List<PaymentDetail> paymentDetails = new List<PaymentDetail>();
				foreach (PaymentDetailItemView item in paymentDetailItemViews)
				{
					paymentDetails.Add(item.ToPaymentDetail());
				}
				return paymentDetails;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static PaymentDetailItemView ToPaymentDetailItemView(this PaymentDetail paymentDetail)
		{
			try
			{
				PaymentDetailItemView paymentDetailItemView = new PaymentDetailItemView();
				paymentDetailItemView.CompanyGUID = paymentDetail.CompanyGUID.GuidNullToString();
				paymentDetailItemView.CreatedBy = paymentDetail.CreatedBy;
				paymentDetailItemView.CreatedDateTime = paymentDetail.CreatedDateTime.DateTimeToString();
				paymentDetailItemView.ModifiedBy = paymentDetail.ModifiedBy;
				paymentDetailItemView.ModifiedDateTime = paymentDetail.ModifiedDateTime.DateTimeToString();
				paymentDetailItemView.Owner = paymentDetail.Owner;
				paymentDetailItemView.OwnerBusinessUnitGUID = paymentDetail.OwnerBusinessUnitGUID.GuidNullToString();
				paymentDetailItemView.PaymentDetailGUID = paymentDetail.PaymentDetailGUID.GuidNullToString();
				paymentDetailItemView.CustomerTableGUID = paymentDetail.CustomerTableGUID.GuidNullToString();
				paymentDetailItemView.InvoiceTypeGUID = paymentDetail.InvoiceTypeGUID.GuidNullToString();
				paymentDetailItemView.PaidToType = paymentDetail.PaidToType;
				paymentDetailItemView.PaymentAmount = paymentDetail.PaymentAmount;
				paymentDetailItemView.RefGUID = paymentDetail.RefGUID.GuidNullToString();
				paymentDetailItemView.RefType = paymentDetail.RefType;
				paymentDetailItemView.SuspenseTransfer = paymentDetail.SuspenseTransfer;
				paymentDetailItemView.VendorTableGUID = paymentDetail.VendorTableGUID.GuidNullToString();
				
				paymentDetailItemView.RowVersion = paymentDetail.RowVersion;
				return paymentDetailItemView.GetPaymentDetailItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion PaymentDetailItemView
		#region ToDropDown
		public static SelectItem<PaymentDetailItemView> ToDropDownItem(this PaymentDetailItemView paymentDetailView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<PaymentDetailItemView> selectItem = new SelectItem<PaymentDetailItemView>();
				selectItem.Label = null;
				selectItem.Value = paymentDetailView.PaymentDetailGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: paymentDetailView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<PaymentDetailItemView>> ToDropDownItem(this IEnumerable<PaymentDetailItemView> paymentDetailItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<PaymentDetailItemView>> selectItems = new List<SelectItem<PaymentDetailItemView>>();
				foreach (PaymentDetailItemView item in paymentDetailItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

