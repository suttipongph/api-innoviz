using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class EmplTeamHandler
	{
		#region EmplTeamListView
		public static List<EmplTeamListView> GetEmplTeamListViewValidation(this List<EmplTeamListView> emplTeamListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return emplTeamListViews;
			}
			var result = new List<EmplTeamListView>();
			try
			{
				foreach (EmplTeamListView item in emplTeamListViews)
				{
					result.Add(item.GetEmplTeamListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static EmplTeamListView GetEmplTeamListViewValidation(this EmplTeamListView emplTeamListView)
		{
			try
			{
				emplTeamListView.RowAuthorize = emplTeamListView.GetListRowAuthorize();
				return emplTeamListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this EmplTeamListView emplTeamListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion EmplTeamListView
		#region EmplTeamItemView
		public static EmplTeamItemView GetEmplTeamItemViewValidation(this EmplTeamItemView emplTeamItemView)
		{
			try
			{
				emplTeamItemView.RowAuthorize = emplTeamItemView.GetItemRowAuthorize();
				return emplTeamItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this EmplTeamItemView emplTeamItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static EmplTeam ToEmplTeam(this EmplTeamItemView emplTeamItemView)
		{
			try
			{
				EmplTeam emplTeam = new EmplTeam();
				emplTeam.CompanyGUID = emplTeamItemView.CompanyGUID.StringToGuid();
				emplTeam.CreatedBy = emplTeamItemView.CreatedBy;
				emplTeam.CreatedDateTime = emplTeamItemView.CreatedDateTime.StringToSystemDateTime();
				emplTeam.ModifiedBy = emplTeamItemView.ModifiedBy;
				emplTeam.ModifiedDateTime = emplTeamItemView.ModifiedDateTime.StringToSystemDateTime();
				emplTeam.Owner = emplTeamItemView.Owner;
				emplTeam.OwnerBusinessUnitGUID = emplTeamItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				emplTeam.EmplTeamGUID = emplTeamItemView.EmplTeamGUID.StringToGuid();
				emplTeam.Name = emplTeamItemView.Name;
				emplTeam.TeamId = emplTeamItemView.TeamId;
				
				emplTeam.RowVersion = emplTeamItemView.RowVersion;
				return emplTeam;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<EmplTeam> ToEmplTeam(this IEnumerable<EmplTeamItemView> emplTeamItemViews)
		{
			try
			{
				List<EmplTeam> emplTeams = new List<EmplTeam>();
				foreach (EmplTeamItemView item in emplTeamItemViews)
				{
					emplTeams.Add(item.ToEmplTeam());
				}
				return emplTeams;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static EmplTeamItemView ToEmplTeamItemView(this EmplTeam emplTeam)
		{
			try
			{
				EmplTeamItemView emplTeamItemView = new EmplTeamItemView();
				emplTeamItemView.CompanyGUID = emplTeam.CompanyGUID.GuidNullToString();
				emplTeamItemView.CreatedBy = emplTeam.CreatedBy;
				emplTeamItemView.CreatedDateTime = emplTeam.CreatedDateTime.DateTimeToString();
				emplTeamItemView.ModifiedBy = emplTeam.ModifiedBy;
				emplTeamItemView.ModifiedDateTime = emplTeam.ModifiedDateTime.DateTimeToString();
				emplTeamItemView.Owner = emplTeam.Owner;
				emplTeamItemView.OwnerBusinessUnitGUID = emplTeam.OwnerBusinessUnitGUID.GuidNullToString();
				emplTeamItemView.EmplTeamGUID = emplTeam.EmplTeamGUID.GuidNullToString();
				emplTeamItemView.Name = emplTeam.Name;
				emplTeamItemView.TeamId = emplTeam.TeamId;
				
				emplTeamItemView.RowVersion = emplTeam.RowVersion;
				return emplTeamItemView.GetEmplTeamItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion EmplTeamItemView
		#region ToDropDown
		public static SelectItem<EmplTeamItemView> ToDropDownItem(this EmplTeamItemView emplTeamView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<EmplTeamItemView> selectItem = new SelectItem<EmplTeamItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(emplTeamView.TeamId, emplTeamView.Name);
				selectItem.Value = emplTeamView.EmplTeamGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: emplTeamView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<EmplTeamItemView>> ToDropDownItem(this IEnumerable<EmplTeamItemView> emplTeamItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<EmplTeamItemView>> selectItems = new List<SelectItem<EmplTeamItemView>>();
				foreach (EmplTeamItemView item in emplTeamItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

