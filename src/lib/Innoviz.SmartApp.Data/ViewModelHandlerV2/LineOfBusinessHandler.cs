using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class LineOfBusinessHandler
	{
		#region LineOfBusinessListView
		public static List<LineOfBusinessListView> GetLineOfBusinessListViewValidation(this List<LineOfBusinessListView> lineOfBusinessListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return lineOfBusinessListViews;
			}
			var result = new List<LineOfBusinessListView>();
			try
			{
				foreach (LineOfBusinessListView item in lineOfBusinessListViews)
				{
					result.Add(item.GetLineOfBusinessListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static LineOfBusinessListView GetLineOfBusinessListViewValidation(this LineOfBusinessListView lineOfBusinessListView)
		{
			try
			{
				lineOfBusinessListView.RowAuthorize = lineOfBusinessListView.GetListRowAuthorize();
				return lineOfBusinessListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this LineOfBusinessListView lineOfBusinessListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion LineOfBusinessListView
		#region LineOfBusinessItemView
		public static LineOfBusinessItemView GetLineOfBusinessItemViewValidation(this LineOfBusinessItemView lineOfBusinessItemView)
		{
			try
			{
				lineOfBusinessItemView.RowAuthorize = lineOfBusinessItemView.GetItemRowAuthorize();
				return lineOfBusinessItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this LineOfBusinessItemView lineOfBusinessItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static LineOfBusiness ToLineOfBusiness(this LineOfBusinessItemView lineOfBusinessItemView)
		{
			try
			{
				LineOfBusiness lineOfBusiness = new LineOfBusiness();
				lineOfBusiness.CompanyGUID = lineOfBusinessItemView.CompanyGUID.StringToGuid();
				lineOfBusiness.CreatedBy = lineOfBusinessItemView.CreatedBy;
				lineOfBusiness.CreatedDateTime = lineOfBusinessItemView.CreatedDateTime.StringToSystemDateTime();
				lineOfBusiness.ModifiedBy = lineOfBusinessItemView.ModifiedBy;
				lineOfBusiness.ModifiedDateTime = lineOfBusinessItemView.ModifiedDateTime.StringToSystemDateTime();
				lineOfBusiness.Owner = lineOfBusinessItemView.Owner;
				lineOfBusiness.OwnerBusinessUnitGUID = lineOfBusinessItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				lineOfBusiness.LineOfBusinessGUID = lineOfBusinessItemView.LineOfBusinessGUID.StringToGuid();
				lineOfBusiness.Description = lineOfBusinessItemView.Description;
				lineOfBusiness.LineOfBusinessId = lineOfBusinessItemView.LineOfBusinessId;
				
				lineOfBusiness.RowVersion = lineOfBusinessItemView.RowVersion;
				return lineOfBusiness;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<LineOfBusiness> ToLineOfBusiness(this IEnumerable<LineOfBusinessItemView> lineOfBusinessItemViews)
		{
			try
			{
				List<LineOfBusiness> lineOfBusinesss = new List<LineOfBusiness>();
				foreach (LineOfBusinessItemView item in lineOfBusinessItemViews)
				{
					lineOfBusinesss.Add(item.ToLineOfBusiness());
				}
				return lineOfBusinesss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static LineOfBusinessItemView ToLineOfBusinessItemView(this LineOfBusiness lineOfBusiness)
		{
			try
			{
				LineOfBusinessItemView lineOfBusinessItemView = new LineOfBusinessItemView();
				lineOfBusinessItemView.CompanyGUID = lineOfBusiness.CompanyGUID.GuidNullToString();
				lineOfBusinessItemView.CreatedBy = lineOfBusiness.CreatedBy;
				lineOfBusinessItemView.CreatedDateTime = lineOfBusiness.CreatedDateTime.DateTimeToString();
				lineOfBusinessItemView.ModifiedBy = lineOfBusiness.ModifiedBy;
				lineOfBusinessItemView.ModifiedDateTime = lineOfBusiness.ModifiedDateTime.DateTimeToString();
				lineOfBusinessItemView.Owner = lineOfBusiness.Owner;
				lineOfBusinessItemView.OwnerBusinessUnitGUID = lineOfBusiness.OwnerBusinessUnitGUID.GuidNullToString();
				lineOfBusinessItemView.LineOfBusinessGUID = lineOfBusiness.LineOfBusinessGUID.GuidNullToString();
				lineOfBusinessItemView.Description = lineOfBusiness.Description;
				lineOfBusinessItemView.LineOfBusinessId = lineOfBusiness.LineOfBusinessId;
				
				lineOfBusinessItemView.RowVersion = lineOfBusiness.RowVersion;
				return lineOfBusinessItemView.GetLineOfBusinessItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion LineOfBusinessItemView
		#region ToDropDown
		public static SelectItem<LineOfBusinessItemView> ToDropDownItem(this LineOfBusinessItemView lineOfBusinessView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<LineOfBusinessItemView> selectItem = new SelectItem<LineOfBusinessItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(lineOfBusinessView.LineOfBusinessId, lineOfBusinessView.Description);
				selectItem.Value = lineOfBusinessView.LineOfBusinessGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: lineOfBusinessView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<LineOfBusinessItemView>> ToDropDownItem(this IEnumerable<LineOfBusinessItemView> lineOfBusinessItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<LineOfBusinessItemView>> selectItems = new List<SelectItem<LineOfBusinessItemView>>();
				foreach (LineOfBusinessItemView item in lineOfBusinessItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

