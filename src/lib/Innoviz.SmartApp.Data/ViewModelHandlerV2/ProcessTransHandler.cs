using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class ProcessTransHandler
	{
		#region ProcessTransListView
		public static List<ProcessTransListView> GetProcessTransListViewValidation(this List<ProcessTransListView> processTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return processTransListViews;
			}
			var result = new List<ProcessTransListView>();
			try
			{
				foreach (ProcessTransListView item in processTransListViews)
				{
					result.Add(item.GetProcessTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ProcessTransListView GetProcessTransListViewValidation(this ProcessTransListView processTransListView)
		{
			try
			{
				processTransListView.RowAuthorize = processTransListView.GetListRowAuthorize();
				return processTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this ProcessTransListView processTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion ProcessTransListView
		#region ProcessTransItemView
		public static ProcessTransItemView GetProcessTransItemViewValidation(this ProcessTransItemView processTransItemView)
		{
			try
			{
				processTransItemView.RowAuthorize = processTransItemView.GetItemRowAuthorize();
				return processTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this ProcessTransItemView processTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static ProcessTrans ToProcessTrans(this ProcessTransItemView processTransItemView)
		{
			try
			{
				ProcessTrans processTrans = new ProcessTrans();
				processTrans.CompanyGUID = processTransItemView.CompanyGUID.StringToGuid();
				processTrans.CreatedBy = processTransItemView.CreatedBy;
				processTrans.CreatedDateTime = processTransItemView.CreatedDateTime.StringToSystemDateTime();
				processTrans.ModifiedBy = processTransItemView.ModifiedBy;
				processTrans.ModifiedDateTime = processTransItemView.ModifiedDateTime.StringToSystemDateTime();
				processTrans.Owner = processTransItemView.Owner;
				processTrans.OwnerBusinessUnitGUID = processTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				processTrans.ProcessTransGUID = processTransItemView.ProcessTransGUID.StringToGuid();
				processTrans.Amount = processTransItemView.Amount;
				processTrans.AmountMST = processTransItemView.AmountMST;
				processTrans.ARLedgerAccount = processTransItemView.ARLedgerAccount;
				processTrans.CreditAppTableGUID = processTransItemView.CreditAppTableGUID.StringToGuidNull();
				processTrans.CurrencyGUID = processTransItemView.CurrencyGUID.StringToGuid();
				processTrans.CustomerTableGUID = processTransItemView.CustomerTableGUID.StringToGuid();
				processTrans.DocumentId = processTransItemView.DocumentId;
				processTrans.DocumentReasonGUID = processTransItemView.DocumentReasonGUID.StringToGuidNull();
				processTrans.ExchangeRate = processTransItemView.ExchangeRate;
				processTrans.OrigTaxTableGUID = processTransItemView.OrigTaxTableGUID.StringToGuidNull();
				processTrans.PaymentHistoryGUID = processTransItemView.PaymentHistoryGUID.StringToGuidNull();
				processTrans.PaymentDetailGUID = processTransItemView.PaymentDetailGUID.StringToGuidNull();
				processTrans.ProcessTransType = processTransItemView.ProcessTransType;
				processTrans.ProductType = processTransItemView.ProductType;
				processTrans.RefGUID = processTransItemView.RefGUID.StringToGuidNull();
				processTrans.RefProcessTransGUID = processTransItemView.RefProcessTransGUID.StringToGuidNull();
				processTrans.RefTaxInvoiceGUID = processTransItemView.RefTaxInvoiceGUID.StringToGuidNull();
				processTrans.RefType = processTransItemView.RefType;
				processTrans.Revert = processTransItemView.Revert;
				processTrans.StagingBatchId = processTransItemView.StagingBatchId;
				processTrans.StagingBatchStatus = processTransItemView.StagingBatchStatus;
				processTrans.TaxAmount = processTransItemView.TaxAmount;
				processTrans.TaxAmountMST = processTransItemView.TaxAmountMST;
				processTrans.TaxTableGUID = processTransItemView.TaxTableGUID.StringToGuidNull();
				processTrans.TransDate = processTransItemView.TransDate.StringToDate();
				processTrans.InvoiceTableGUID = processTransItemView.InvoiceTableGUID.StringToGuidNull();
				processTrans.SourceRefType = processTransItemView.SourceRefType;
				processTrans.SourceRefId = processTransItemView.SourceRefId;
				
				processTrans.RowVersion = processTransItemView.RowVersion;
				return processTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<ProcessTrans> ToProcessTrans(this IEnumerable<ProcessTransItemView> processTransItemViews)
		{
			try
			{
				List<ProcessTrans> processTranss = new List<ProcessTrans>();
				foreach (ProcessTransItemView item in processTransItemViews)
				{
					processTranss.Add(item.ToProcessTrans());
				}
				return processTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ProcessTransItemView ToProcessTransItemView(this ProcessTrans processTrans)
		{
			try
			{
				ProcessTransItemView processTransItemView = new ProcessTransItemView();
				processTransItemView.CompanyGUID = processTrans.CompanyGUID.GuidNullToString();
				processTransItemView.CreatedBy = processTrans.CreatedBy;
				processTransItemView.CreatedDateTime = processTrans.CreatedDateTime.DateTimeToString();
				processTransItemView.ModifiedBy = processTrans.ModifiedBy;
				processTransItemView.ModifiedDateTime = processTrans.ModifiedDateTime.DateTimeToString();
				processTransItemView.Owner = processTrans.Owner;
				processTransItemView.OwnerBusinessUnitGUID = processTrans.OwnerBusinessUnitGUID.GuidNullToString();
				processTransItemView.ProcessTransGUID = processTrans.ProcessTransGUID.GuidNullToString();
				processTransItemView.Amount = processTrans.Amount;
				processTransItemView.AmountMST = processTrans.AmountMST;
				processTransItemView.ARLedgerAccount = processTrans.ARLedgerAccount;
				processTransItemView.CreditAppTableGUID = processTrans.CreditAppTableGUID.GuidNullToString();
				processTransItemView.CurrencyGUID = processTrans.CurrencyGUID.GuidNullToString();
				processTransItemView.CustomerTableGUID = processTrans.CustomerTableGUID.GuidNullToString();
				processTransItemView.DocumentId = processTrans.DocumentId;
				processTransItemView.DocumentReasonGUID = processTrans.DocumentReasonGUID.GuidNullToString();
				processTransItemView.ExchangeRate = processTrans.ExchangeRate;
				processTransItemView.OrigTaxTableGUID = processTrans.OrigTaxTableGUID.GuidNullToString();
				processTransItemView.PaymentHistoryGUID = processTrans.PaymentHistoryGUID.GuidNullToString();
				processTransItemView.PaymentDetailGUID = processTrans.PaymentDetailGUID.GuidNullToString();
				processTransItemView.ProcessTransType = processTrans.ProcessTransType;
				processTransItemView.ProductType = processTrans.ProductType;
				processTransItemView.RefGUID = processTrans.RefGUID.GuidNullToString();
				processTransItemView.RefProcessTransGUID = processTrans.RefProcessTransGUID.GuidNullToString();
				processTransItemView.RefTaxInvoiceGUID = processTrans.RefTaxInvoiceGUID.GuidNullToString();
				processTransItemView.RefType = processTrans.RefType;
				processTransItemView.Revert = processTrans.Revert;
				processTransItemView.StagingBatchId = processTrans.StagingBatchId;
				processTransItemView.StagingBatchStatus = processTrans.StagingBatchStatus;
				processTransItemView.TaxAmount = processTrans.TaxAmount;
				processTransItemView.TaxAmountMST = processTrans.TaxAmountMST;
				processTransItemView.TaxTableGUID = processTrans.TaxTableGUID.GuidNullToString();
				processTransItemView.TransDate = processTrans.TransDate.DateToString();
				processTransItemView.InvoiceTableGUID = processTrans.InvoiceTableGUID.GuidNullOrEmptyToString();
				
				processTransItemView.RowVersion = processTrans.RowVersion;
				return processTransItemView.GetProcessTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ProcessTransItemView
		#region ToDropDown
		public static SelectItem<ProcessTransItemView> ToDropDownItem(this ProcessTransItemView processTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<ProcessTransItemView> selectItem = new SelectItem<ProcessTransItemView>();    
				selectItem.Label = SmartAppUtil.GetDropDownLabel(((ProcessTransType)processTransView.ProcessTransType).ToString(),processTransView.DocumentId);
				selectItem.Value = processTransView.ProcessTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: processTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<ProcessTransItemView>> ToDropDownItem(this IEnumerable<ProcessTransItemView> processTransItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<ProcessTransItemView>> selectItems = new List<SelectItem<ProcessTransItemView>>();
				foreach (ProcessTransItemView item in processTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

