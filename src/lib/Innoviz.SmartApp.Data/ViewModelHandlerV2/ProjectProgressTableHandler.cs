using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class ProjectProgressTableHandler
	{
		#region ProjectProgressTableListView
		public static List<ProjectProgressTableListView> GetProjectProgressTableListViewValidation(this List<ProjectProgressTableListView> projectProgressTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return projectProgressTableListViews;
			}
			var result = new List<ProjectProgressTableListView>();
			try
			{
				foreach (ProjectProgressTableListView item in projectProgressTableListViews)
				{
					result.Add(item.GetProjectProgressTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ProjectProgressTableListView GetProjectProgressTableListViewValidation(this ProjectProgressTableListView projectProgressTableListView)
		{
			try
			{
				projectProgressTableListView.RowAuthorize = projectProgressTableListView.GetListRowAuthorize();
				return projectProgressTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this ProjectProgressTableListView projectProgressTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion ProjectProgressTableListView
		#region ProjectProgressTableItemView
		public static ProjectProgressTableItemView GetProjectProgressTableItemViewValidation(this ProjectProgressTableItemView projectProgressTableItemView)
		{
			try
			{
				projectProgressTableItemView.RowAuthorize = projectProgressTableItemView.GetItemRowAuthorize();
				return projectProgressTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this ProjectProgressTableItemView projectProgressTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static ProjectProgressTable ToProjectProgressTable(this ProjectProgressTableItemView projectProgressTableItemView)
		{
			try
			{
				ProjectProgressTable projectProgressTable = new ProjectProgressTable();
				projectProgressTable.CompanyGUID = projectProgressTableItemView.CompanyGUID.StringToGuid();
				projectProgressTable.CreatedBy = projectProgressTableItemView.CreatedBy;
				projectProgressTable.CreatedDateTime = projectProgressTableItemView.CreatedDateTime.StringToSystemDateTime();
				projectProgressTable.ModifiedBy = projectProgressTableItemView.ModifiedBy;
				projectProgressTable.ModifiedDateTime = projectProgressTableItemView.ModifiedDateTime.StringToSystemDateTime();
				projectProgressTable.Owner = projectProgressTableItemView.Owner;
				projectProgressTable.OwnerBusinessUnitGUID = projectProgressTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				projectProgressTable.ProjectProgressTableGUID = projectProgressTableItemView.ProjectProgressTableGUID.StringToGuid();
				projectProgressTable.Description = projectProgressTableItemView.Description;
				projectProgressTable.ProjectProgressId = projectProgressTableItemView.ProjectProgressId;
				projectProgressTable.Remark = projectProgressTableItemView.Remark;
				projectProgressTable.TransDate = projectProgressTableItemView.TransDate.StringToDate();
				projectProgressTable.WithdrawalTableGUID = projectProgressTableItemView.WithdrawalTableGUID.StringToGuid();
				
				projectProgressTable.RowVersion = projectProgressTableItemView.RowVersion;
				return projectProgressTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<ProjectProgressTable> ToProjectProgressTable(this IEnumerable<ProjectProgressTableItemView> projectProgressTableItemViews)
		{
			try
			{
				List<ProjectProgressTable> projectProgressTables = new List<ProjectProgressTable>();
				foreach (ProjectProgressTableItemView item in projectProgressTableItemViews)
				{
					projectProgressTables.Add(item.ToProjectProgressTable());
				}
				return projectProgressTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ProjectProgressTableItemView ToProjectProgressTableItemView(this ProjectProgressTable projectProgressTable)
		{
			try
			{
				ProjectProgressTableItemView projectProgressTableItemView = new ProjectProgressTableItemView();
				projectProgressTableItemView.CompanyGUID = projectProgressTable.CompanyGUID.GuidNullToString();
				projectProgressTableItemView.CreatedBy = projectProgressTable.CreatedBy;
				projectProgressTableItemView.CreatedDateTime = projectProgressTable.CreatedDateTime.DateTimeToString();
				projectProgressTableItemView.ModifiedBy = projectProgressTable.ModifiedBy;
				projectProgressTableItemView.ModifiedDateTime = projectProgressTable.ModifiedDateTime.DateTimeToString();
				projectProgressTableItemView.Owner = projectProgressTable.Owner;
				projectProgressTableItemView.OwnerBusinessUnitGUID = projectProgressTable.OwnerBusinessUnitGUID.GuidNullToString();
				projectProgressTableItemView.ProjectProgressTableGUID = projectProgressTable.ProjectProgressTableGUID.GuidNullToString();
				projectProgressTableItemView.Description = projectProgressTable.Description;
				projectProgressTableItemView.ProjectProgressId = projectProgressTable.ProjectProgressId;
				projectProgressTableItemView.Remark = projectProgressTable.Remark;
				projectProgressTableItemView.TransDate = projectProgressTable.TransDate.DateToString();
				projectProgressTableItemView.WithdrawalTableGUID = projectProgressTable.WithdrawalTableGUID.GuidNullToString();
				
				projectProgressTableItemView.RowVersion = projectProgressTable.RowVersion;
				return projectProgressTableItemView.GetProjectProgressTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ProjectProgressTableItemView
		#region ToDropDown
		public static SelectItem<ProjectProgressTableItemView> ToDropDownItem(this ProjectProgressTableItemView projectProgressTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<ProjectProgressTableItemView> selectItem = new SelectItem<ProjectProgressTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(projectProgressTableView.ProjectProgressId, projectProgressTableView.Description);
				selectItem.Value = projectProgressTableView.ProjectProgressTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: projectProgressTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<ProjectProgressTableItemView>> ToDropDownItem(this IEnumerable<ProjectProgressTableItemView> projectProgressTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<ProjectProgressTableItemView>> selectItems = new List<SelectItem<ProjectProgressTableItemView>>();
				foreach (ProjectProgressTableItemView item in projectProgressTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

