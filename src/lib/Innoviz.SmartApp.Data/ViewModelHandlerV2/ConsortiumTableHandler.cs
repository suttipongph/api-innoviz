using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class ConsortiumTableHandler
	{
		#region ConsortiumTableListView
		public static List<ConsortiumTableListView> GetConsortiumTableListViewValidation(this List<ConsortiumTableListView> consortiumTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return consortiumTableListViews;
			}
			var result = new List<ConsortiumTableListView>();
			try
			{
				foreach (ConsortiumTableListView item in consortiumTableListViews)
				{
					result.Add(item.GetConsortiumTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ConsortiumTableListView GetConsortiumTableListViewValidation(this ConsortiumTableListView consortiumTableListView)
		{
			try
			{
				consortiumTableListView.RowAuthorize = consortiumTableListView.GetListRowAuthorize();
				return consortiumTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this ConsortiumTableListView consortiumTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion ConsortiumTableListView
		#region ConsortiumTableItemView
		public static ConsortiumTableItemView GetConsortiumTableItemViewValidation(this ConsortiumTableItemView consortiumTableItemView)
		{
			try
			{
				consortiumTableItemView.RowAuthorize = consortiumTableItemView.GetItemRowAuthorize();
				return consortiumTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this ConsortiumTableItemView consortiumTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static ConsortiumTable ToConsortiumTable(this ConsortiumTableItemView consortiumTableItemView)
		{
			try
			{
				ConsortiumTable consortiumTable = new ConsortiumTable();
				consortiumTable.CompanyGUID = consortiumTableItemView.CompanyGUID.StringToGuid();
				consortiumTable.CreatedBy = consortiumTableItemView.CreatedBy;
				consortiumTable.CreatedDateTime = consortiumTableItemView.CreatedDateTime.StringToSystemDateTime();
				consortiumTable.ModifiedBy = consortiumTableItemView.ModifiedBy;
				consortiumTable.ModifiedDateTime = consortiumTableItemView.ModifiedDateTime.StringToSystemDateTime();
				consortiumTable.Owner = consortiumTableItemView.Owner;
				consortiumTable.OwnerBusinessUnitGUID = consortiumTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				consortiumTable.ConsortiumTableGUID = consortiumTableItemView.ConsortiumTableGUID.StringToGuid();
				consortiumTable.ConsortiumId = consortiumTableItemView.ConsortiumId;
				consortiumTable.Description = consortiumTableItemView.Description;
				consortiumTable.DocumentStatusGUID = consortiumTableItemView.DocumentStatusGUID.StringToGuid();
				
				consortiumTable.RowVersion = consortiumTableItemView.RowVersion;
				return consortiumTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<ConsortiumTable> ToConsortiumTable(this IEnumerable<ConsortiumTableItemView> consortiumTableItemViews)
		{
			try
			{
				List<ConsortiumTable> consortiumTables = new List<ConsortiumTable>();
				foreach (ConsortiumTableItemView item in consortiumTableItemViews)
				{
					consortiumTables.Add(item.ToConsortiumTable());
				}
				return consortiumTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ConsortiumTableItemView ToConsortiumTableItemView(this ConsortiumTable consortiumTable)
		{
			try
			{
				ConsortiumTableItemView consortiumTableItemView = new ConsortiumTableItemView();
				consortiumTableItemView.CompanyGUID = consortiumTable.CompanyGUID.GuidNullToString();
				consortiumTableItemView.CreatedBy = consortiumTable.CreatedBy;
				consortiumTableItemView.CreatedDateTime = consortiumTable.CreatedDateTime.DateTimeToString();
				consortiumTableItemView.ModifiedBy = consortiumTable.ModifiedBy;
				consortiumTableItemView.ModifiedDateTime = consortiumTable.ModifiedDateTime.DateTimeToString();
				consortiumTableItemView.Owner = consortiumTable.Owner;
				consortiumTableItemView.OwnerBusinessUnitGUID = consortiumTable.OwnerBusinessUnitGUID.GuidNullToString();
				consortiumTableItemView.ConsortiumTableGUID = consortiumTable.ConsortiumTableGUID.GuidNullToString();
				consortiumTableItemView.ConsortiumId = consortiumTable.ConsortiumId;
				consortiumTableItemView.Description = consortiumTable.Description;
				consortiumTableItemView.DocumentStatusGUID = consortiumTable.DocumentStatusGUID.GuidNullToString();
				
				consortiumTableItemView.RowVersion = consortiumTable.RowVersion;
				return consortiumTableItemView.GetConsortiumTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ConsortiumTableItemView
		#region ToDropDown
		public static SelectItem<ConsortiumTableItemView> ToDropDownItem(this ConsortiumTableItemView consortiumTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<ConsortiumTableItemView> selectItem = new SelectItem<ConsortiumTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(consortiumTableView.ConsortiumId, consortiumTableView.Description);
				selectItem.Value = consortiumTableView.ConsortiumTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: consortiumTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<ConsortiumTableItemView>> ToDropDownItem(this IEnumerable<ConsortiumTableItemView> consortiumTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<ConsortiumTableItemView>> selectItems = new List<SelectItem<ConsortiumTableItemView>>();
				foreach (ConsortiumTableItemView item in consortiumTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

