using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CreditAppTransHandler
	{
		#region CreditAppTransListView
		public static List<CreditAppTransListView> GetCreditAppTransListViewValidation(this List<CreditAppTransListView> creditAppTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return creditAppTransListViews;
			}
			var result = new List<CreditAppTransListView>();
			try
			{
				foreach (CreditAppTransListView item in creditAppTransListViews)
				{
					result.Add(item.GetCreditAppTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CreditAppTransListView GetCreditAppTransListViewValidation(this CreditAppTransListView creditAppTransListView)
		{
			try
			{
				creditAppTransListView.RowAuthorize = creditAppTransListView.GetListRowAuthorize();
				return creditAppTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CreditAppTransListView creditAppTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CreditAppTransListView
		#region CreditAppTransItemView
		public static CreditAppTransItemView GetCreditAppTransItemViewValidation(this CreditAppTransItemView creditAppTransItemView)
		{
			try
			{
				creditAppTransItemView.RowAuthorize = creditAppTransItemView.GetItemRowAuthorize();
				return creditAppTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CreditAppTransItemView creditAppTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CreditAppTrans ToCreditAppTrans(this CreditAppTransItemView creditAppTransItemView)
		{
			try
			{
				CreditAppTrans creditAppTrans = new CreditAppTrans();
				creditAppTrans.CompanyGUID = creditAppTransItemView.CompanyGUID.StringToGuid();
				creditAppTrans.CreatedBy = creditAppTransItemView.CreatedBy;
				creditAppTrans.CreatedDateTime = creditAppTransItemView.CreatedDateTime.StringToSystemDateTime();
				creditAppTrans.ModifiedBy = creditAppTransItemView.ModifiedBy;
				creditAppTrans.ModifiedDateTime = creditAppTransItemView.ModifiedDateTime.StringToSystemDateTime();
				creditAppTrans.Owner = creditAppTransItemView.Owner;
				creditAppTrans.OwnerBusinessUnitGUID = creditAppTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				creditAppTrans.CreditAppTransGUID = creditAppTransItemView.CreditAppTransGUID.StringToGuid();
				creditAppTrans.BuyerTableGUID = creditAppTransItemView.BuyerTableGUID.StringToGuidNull();
				creditAppTrans.CreditAppLineGUID = creditAppTransItemView.CreditAppLineGUID.StringToGuidNull();
				creditAppTrans.CreditAppTableGUID = creditAppTransItemView.CreditAppTableGUID.StringToGuid();
				creditAppTrans.CreditDeductAmount = creditAppTransItemView.CreditDeductAmount;
				creditAppTrans.CustomerTableGUID = creditAppTransItemView.CustomerTableGUID.StringToGuid();
				creditAppTrans.DocumentId = creditAppTransItemView.DocumentId;
				creditAppTrans.InvoiceAmount = creditAppTransItemView.InvoiceAmount;
				creditAppTrans.ProductType = creditAppTransItemView.ProductType;
				creditAppTrans.RefGUID = creditAppTransItemView.RefGUID.StringToGuidNull();
				creditAppTrans.RefType = creditAppTransItemView.RefType;
				creditAppTrans.TransDate = creditAppTransItemView.TransDate.StringToDate();
				
				creditAppTrans.RowVersion = creditAppTransItemView.RowVersion;
				return creditAppTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CreditAppTrans> ToCreditAppTrans(this IEnumerable<CreditAppTransItemView> creditAppTransItemViews)
		{
			try
			{
				List<CreditAppTrans> creditAppTranss = new List<CreditAppTrans>();
				foreach (CreditAppTransItemView item in creditAppTransItemViews)
				{
					creditAppTranss.Add(item.ToCreditAppTrans());
				}
				return creditAppTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CreditAppTransItemView ToCreditAppTransItemView(this CreditAppTrans creditAppTrans)
		{
			try
			{
				CreditAppTransItemView creditAppTransItemView = new CreditAppTransItemView();
				creditAppTransItemView.CompanyGUID = creditAppTrans.CompanyGUID.GuidNullToString();
				creditAppTransItemView.CreatedBy = creditAppTrans.CreatedBy;
				creditAppTransItemView.CreatedDateTime = creditAppTrans.CreatedDateTime.DateTimeToString();
				creditAppTransItemView.ModifiedBy = creditAppTrans.ModifiedBy;
				creditAppTransItemView.ModifiedDateTime = creditAppTrans.ModifiedDateTime.DateTimeToString();
				creditAppTransItemView.Owner = creditAppTrans.Owner;
				creditAppTransItemView.OwnerBusinessUnitGUID = creditAppTrans.OwnerBusinessUnitGUID.GuidNullToString();
				creditAppTransItemView.CreditAppTransGUID = creditAppTrans.CreditAppTransGUID.GuidNullToString();
				creditAppTransItemView.BuyerTableGUID = creditAppTrans.BuyerTableGUID.GuidNullToString();
				creditAppTransItemView.CreditAppLineGUID = creditAppTrans.CreditAppLineGUID.GuidNullToString();
				creditAppTransItemView.CreditAppTableGUID = creditAppTrans.CreditAppTableGUID.GuidNullToString();
				creditAppTransItemView.CreditDeductAmount = creditAppTrans.CreditDeductAmount;
				creditAppTransItemView.CustomerTableGUID = creditAppTrans.CustomerTableGUID.GuidNullToString();
				creditAppTransItemView.DocumentId = creditAppTrans.DocumentId;
				creditAppTransItemView.InvoiceAmount = creditAppTrans.InvoiceAmount;
				creditAppTransItemView.ProductType = creditAppTrans.ProductType;
				creditAppTransItemView.RefGUID = creditAppTrans.RefGUID.GuidNullToString();
				creditAppTransItemView.RefType = creditAppTrans.RefType;
				creditAppTransItemView.TransDate = creditAppTrans.TransDate.DateToString();
				
				creditAppTransItemView.RowVersion = creditAppTrans.RowVersion;
				return creditAppTransItemView.GetCreditAppTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CreditAppTransItemView
		#region ToDropDown
		public static SelectItem<CreditAppTransItemView> ToDropDownItem(this CreditAppTransItemView creditAppTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CreditAppTransItemView> selectItem = new SelectItem<CreditAppTransItemView>();
				selectItem.Label = null;
				selectItem.Value = creditAppTransView.CreditAppTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: creditAppTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CreditAppTransItemView>> ToDropDownItem(this IEnumerable<CreditAppTransItemView> creditAppTransItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CreditAppTransItemView>> selectItems = new List<SelectItem<CreditAppTransItemView>>();
				foreach (CreditAppTransItemView item in creditAppTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

