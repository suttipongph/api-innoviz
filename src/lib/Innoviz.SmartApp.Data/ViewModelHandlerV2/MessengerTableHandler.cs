using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class MessengerTableHandler
	{
		#region MessengerTableListView
		public static List<MessengerTableListView> GetMessengerTableListViewValidation(this List<MessengerTableListView> messengerTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return messengerTableListViews;
			}
			var result = new List<MessengerTableListView>();
			try
			{
				foreach (MessengerTableListView item in messengerTableListViews)
				{
					result.Add(item.GetMessengerTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static MessengerTableListView GetMessengerTableListViewValidation(this MessengerTableListView messengerTableListView)
		{
			try
			{
				messengerTableListView.RowAuthorize = messengerTableListView.GetListRowAuthorize();
				return messengerTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this MessengerTableListView messengerTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion MessengerTableListView
		#region MessengerTableItemView
		public static MessengerTableItemView GetMessengerTableItemViewValidation(this MessengerTableItemView messengerTableItemView)
		{
			try
			{
				messengerTableItemView.RowAuthorize = messengerTableItemView.GetItemRowAuthorize();
				return messengerTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this MessengerTableItemView messengerTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static MessengerTable ToMessengerTable(this MessengerTableItemView messengerTableItemView)
		{
			try
			{
				MessengerTable messengerTable = new MessengerTable();
				messengerTable.CompanyGUID = messengerTableItemView.CompanyGUID.StringToGuid();
				messengerTable.CreatedBy = messengerTableItemView.CreatedBy;
				messengerTable.CreatedDateTime = messengerTableItemView.CreatedDateTime.StringToSystemDateTime();
				messengerTable.ModifiedBy = messengerTableItemView.ModifiedBy;
				messengerTable.ModifiedDateTime = messengerTableItemView.ModifiedDateTime.StringToSystemDateTime();
				messengerTable.Owner = messengerTableItemView.Owner;
				messengerTable.OwnerBusinessUnitGUID = messengerTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				messengerTable.MessengerTableGUID = messengerTableItemView.MessengerTableGUID.StringToGuid();
				messengerTable.Address = messengerTableItemView.Address;
				messengerTable.DriverLicenseId = messengerTableItemView.DriverLicenseId;
				messengerTable.Name = messengerTableItemView.Name;
				messengerTable.Phone = messengerTableItemView.Phone;
				messengerTable.PlateNumber = messengerTableItemView.PlateNumber;
				messengerTable.TaxId = messengerTableItemView.TaxId;
				messengerTable.VendorTableGUID = messengerTableItemView.VendorTableGUID.StringToGuidNull();
				
				messengerTable.RowVersion = messengerTableItemView.RowVersion;
				return messengerTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<MessengerTable> ToMessengerTable(this IEnumerable<MessengerTableItemView> messengerTableItemViews)
		{
			try
			{
				List<MessengerTable> messengerTables = new List<MessengerTable>();
				foreach (MessengerTableItemView item in messengerTableItemViews)
				{
					messengerTables.Add(item.ToMessengerTable());
				}
				return messengerTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static MessengerTableItemView ToMessengerTableItemView(this MessengerTable messengerTable)
		{
			try
			{
				MessengerTableItemView messengerTableItemView = new MessengerTableItemView();
				messengerTableItemView.CompanyGUID = messengerTable.CompanyGUID.GuidNullToString();
				messengerTableItemView.CreatedBy = messengerTable.CreatedBy;
				messengerTableItemView.CreatedDateTime = messengerTable.CreatedDateTime.DateTimeToString();
				messengerTableItemView.ModifiedBy = messengerTable.ModifiedBy;
				messengerTableItemView.ModifiedDateTime = messengerTable.ModifiedDateTime.DateTimeToString();
				messengerTableItemView.Owner = messengerTable.Owner;
				messengerTableItemView.OwnerBusinessUnitGUID = messengerTable.OwnerBusinessUnitGUID.GuidNullToString();
				messengerTableItemView.MessengerTableGUID = messengerTable.MessengerTableGUID.GuidNullToString();
				messengerTableItemView.Address = messengerTable.Address;
				messengerTableItemView.DriverLicenseId = messengerTable.DriverLicenseId;
				messengerTableItemView.Name = messengerTable.Name;
				messengerTableItemView.Phone = messengerTable.Phone;
				messengerTableItemView.PlateNumber = messengerTable.PlateNumber;
				messengerTableItemView.TaxId = messengerTable.TaxId;
				messengerTableItemView.VendorTableGUID = messengerTable.VendorTableGUID.GuidNullToString();
				
				messengerTableItemView.RowVersion = messengerTable.RowVersion;
				return messengerTableItemView.GetMessengerTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion MessengerTableItemView
		#region ToDropDown
		public static SelectItem<MessengerTableItemView> ToDropDownItem(this MessengerTableItemView messengerTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<MessengerTableItemView> selectItem = new SelectItem<MessengerTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(messengerTableView.Name, messengerTableView.VendorTable_VendorId);
				selectItem.Value = messengerTableView.MessengerTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: messengerTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<MessengerTableItemView>> ToDropDownItem(this IEnumerable<MessengerTableItemView> messengerTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<MessengerTableItemView>> selectItems = new List<SelectItem<MessengerTableItemView>>();
				foreach (MessengerTableItemView item in messengerTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

