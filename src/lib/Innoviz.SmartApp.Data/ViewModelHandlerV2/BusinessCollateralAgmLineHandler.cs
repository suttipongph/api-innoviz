using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class BusinessCollateralAgmLineHandler
	{
		#region BusinessCollateralAgmLineListView
		public static List<BusinessCollateralAgmLineListView> GetBusinessCollateralAgmLineListViewValidation(this List<BusinessCollateralAgmLineListView> businessCollateralAgmLineListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return businessCollateralAgmLineListViews;
			}
			var result = new List<BusinessCollateralAgmLineListView>();
			try
			{
				foreach (BusinessCollateralAgmLineListView item in businessCollateralAgmLineListViews)
				{
					result.Add(item.GetBusinessCollateralAgmLineListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BusinessCollateralAgmLineListView GetBusinessCollateralAgmLineListViewValidation(this BusinessCollateralAgmLineListView businessCollateralAgmLineListView)
		{
			try
			{
				businessCollateralAgmLineListView.RowAuthorize = businessCollateralAgmLineListView.GetListRowAuthorize();
				return businessCollateralAgmLineListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this BusinessCollateralAgmLineListView businessCollateralAgmLineListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion BusinessCollateralAgmLineListView
		#region BusinessCollateralAgmLineItemView
		public static BusinessCollateralAgmLineItemView GetBusinessCollateralAgmLineItemViewValidation(this BusinessCollateralAgmLineItemView businessCollateralAgmLineItemView)
		{
			try
			{
				businessCollateralAgmLineItemView.RowAuthorize = businessCollateralAgmLineItemView.GetItemRowAuthorize();
				return businessCollateralAgmLineItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this BusinessCollateralAgmLineItemView businessCollateralAgmLineItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static BusinessCollateralAgmLine ToBusinessCollateralAgmLine(this BusinessCollateralAgmLineItemView businessCollateralAgmLineItemView)
		{
			try
			{
				BusinessCollateralAgmLine businessCollateralAgmLine = new BusinessCollateralAgmLine();
				businessCollateralAgmLine.CompanyGUID = businessCollateralAgmLineItemView.CompanyGUID.StringToGuid();
				businessCollateralAgmLine.CreatedBy = businessCollateralAgmLineItemView.CreatedBy;
				businessCollateralAgmLine.CreatedDateTime = businessCollateralAgmLineItemView.CreatedDateTime.StringToSystemDateTime();
				businessCollateralAgmLine.ModifiedBy = businessCollateralAgmLineItemView.ModifiedBy;
				businessCollateralAgmLine.ModifiedDateTime = businessCollateralAgmLineItemView.ModifiedDateTime.StringToSystemDateTime();
				businessCollateralAgmLine.Owner = businessCollateralAgmLineItemView.Owner;
				businessCollateralAgmLine.OwnerBusinessUnitGUID = businessCollateralAgmLineItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				businessCollateralAgmLine.BusinessCollateralAgmLineGUID = businessCollateralAgmLineItemView.BusinessCollateralAgmLineGUID.StringToGuid();
				businessCollateralAgmLine.AccountNumber = businessCollateralAgmLineItemView.AccountNumber;
				businessCollateralAgmLine.AttachmentText = businessCollateralAgmLineItemView.AttachmentText;
				businessCollateralAgmLine.BankGroupGUID = businessCollateralAgmLineItemView.BankGroupGUID.StringToGuidNull();
				businessCollateralAgmLine.BankTypeGUID = businessCollateralAgmLineItemView.BankTypeGUID.StringToGuidNull();
				businessCollateralAgmLine.BusinessCollateralAgmTableGUID = businessCollateralAgmLineItemView.BusinessCollateralAgmTableGUID.StringToGuid();
				businessCollateralAgmLine.BusinessCollateralSubTypeGUID = businessCollateralAgmLineItemView.BusinessCollateralSubTypeGUID.StringToGuid();
				businessCollateralAgmLine.BusinessCollateralTypeGUID = businessCollateralAgmLineItemView.BusinessCollateralTypeGUID.StringToGuid();
				businessCollateralAgmLine.BusinessCollateralValue = businessCollateralAgmLineItemView.BusinessCollateralValue;
				businessCollateralAgmLine.BuyerName = businessCollateralAgmLineItemView.BuyerName;
				businessCollateralAgmLine.BuyerTableGUID = businessCollateralAgmLineItemView.BuyerTableGUID.StringToGuidNull();
				businessCollateralAgmLine.BuyerTaxIdentificationId = businessCollateralAgmLineItemView.BuyerTaxIdentificationId;
				businessCollateralAgmLine.CapitalValuation = businessCollateralAgmLineItemView.CapitalValuation;
				businessCollateralAgmLine.ChassisNumber = businessCollateralAgmLineItemView.ChassisNumber;
				businessCollateralAgmLine.CreditAppReqBusinessCollateralGUID = businessCollateralAgmLineItemView.CreditAppReqBusinessCollateralGUID.StringToGuid();
				businessCollateralAgmLine.DateOfValuation = businessCollateralAgmLineItemView.DateOfValuation.StringNullToDateNull();
				businessCollateralAgmLine.Description = businessCollateralAgmLineItemView.Description;
				businessCollateralAgmLine.GuaranteeAmount = businessCollateralAgmLineItemView.GuaranteeAmount;
				businessCollateralAgmLine.Lessee = businessCollateralAgmLineItemView.Lessee;
				businessCollateralAgmLine.Lessor = businessCollateralAgmLineItemView.Lessor;
				businessCollateralAgmLine.LineNum = businessCollateralAgmLineItemView.LineNum;
				businessCollateralAgmLine.MachineNumber = businessCollateralAgmLineItemView.MachineNumber;
				businessCollateralAgmLine.MachineRegisteredStatus = businessCollateralAgmLineItemView.MachineRegisteredStatus;
				businessCollateralAgmLine.Ownership = businessCollateralAgmLineItemView.Ownership;
				businessCollateralAgmLine.PreferentialCreditorNumber = businessCollateralAgmLineItemView.PreferentialCreditorNumber;
				businessCollateralAgmLine.ProjectName = businessCollateralAgmLineItemView.ProjectName;
				businessCollateralAgmLine.Quantity = businessCollateralAgmLineItemView.Quantity;
				businessCollateralAgmLine.RefAgreementDate = businessCollateralAgmLineItemView.RefAgreementDate.StringNullToDateNull();
				businessCollateralAgmLine.RefAgreementId = businessCollateralAgmLineItemView.RefAgreementId;
				businessCollateralAgmLine.RegisteredPlace = businessCollateralAgmLineItemView.RegisteredPlace;
				businessCollateralAgmLine.RegistrationPlateNumber = businessCollateralAgmLineItemView.RegistrationPlateNumber;
				businessCollateralAgmLine.TitleDeedDistrict = businessCollateralAgmLineItemView.TitleDeedDistrict;
				businessCollateralAgmLine.TitleDeedNumber = businessCollateralAgmLineItemView.TitleDeedNumber;
				businessCollateralAgmLine.TitleDeedProvince = businessCollateralAgmLineItemView.TitleDeedProvince;
				businessCollateralAgmLine.TitleDeedSubDistrict = businessCollateralAgmLineItemView.TitleDeedSubDistrict;
				businessCollateralAgmLine.Unit = businessCollateralAgmLineItemView.Unit;
				businessCollateralAgmLine.ValuationCommittee = businessCollateralAgmLineItemView.ValuationCommittee;
				businessCollateralAgmLine.Product = businessCollateralAgmLineItemView.Product;
				businessCollateralAgmLine.OriginalBusinessCollateralAgreementLineGUID = businessCollateralAgmLineItemView.OriginalBusinessCollateralAgreementLineGUID.StringToGuidNull();
				businessCollateralAgmLine.OriginalBusinessCollateralAgreementTableGUID = businessCollateralAgmLineItemView.OriginalBusinessCollateralAgreementTableGUID.StringToGuidNull();
				businessCollateralAgmLine.Cancelled = businessCollateralAgmLineItemView.Cancelled;
				businessCollateralAgmLine.BusinessCollateralStatusGUID = businessCollateralAgmLineItemView.BusinessCollateralStatusGUID.StringToGuidNull();
				
				businessCollateralAgmLine.RowVersion = businessCollateralAgmLineItemView.RowVersion;
				return businessCollateralAgmLine;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<BusinessCollateralAgmLine> ToBusinessCollateralAgmLine(this IEnumerable<BusinessCollateralAgmLineItemView> businessCollateralAgmLineItemViews)
		{
			try
			{
				List<BusinessCollateralAgmLine> businessCollateralAgmLines = new List<BusinessCollateralAgmLine>();
				foreach (BusinessCollateralAgmLineItemView item in businessCollateralAgmLineItemViews)
				{
					businessCollateralAgmLines.Add(item.ToBusinessCollateralAgmLine());
				}
				return businessCollateralAgmLines;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BusinessCollateralAgmLineItemView ToBusinessCollateralAgmLineItemView(this BusinessCollateralAgmLine businessCollateralAgmLine)
		{
			try
			{
				BusinessCollateralAgmLineItemView businessCollateralAgmLineItemView = new BusinessCollateralAgmLineItemView();
				businessCollateralAgmLineItemView.CompanyGUID = businessCollateralAgmLine.CompanyGUID.GuidNullToString();
				businessCollateralAgmLineItemView.CreatedBy = businessCollateralAgmLine.CreatedBy;
				businessCollateralAgmLineItemView.CreatedDateTime = businessCollateralAgmLine.CreatedDateTime.DateTimeToString();
				businessCollateralAgmLineItemView.ModifiedBy = businessCollateralAgmLine.ModifiedBy;
				businessCollateralAgmLineItemView.ModifiedDateTime = businessCollateralAgmLine.ModifiedDateTime.DateTimeToString();
				businessCollateralAgmLineItemView.Owner = businessCollateralAgmLine.Owner;
				businessCollateralAgmLineItemView.OwnerBusinessUnitGUID = businessCollateralAgmLine.OwnerBusinessUnitGUID.GuidNullToString();
				businessCollateralAgmLineItemView.BusinessCollateralAgmLineGUID = businessCollateralAgmLine.BusinessCollateralAgmLineGUID.GuidNullToString();
				businessCollateralAgmLineItemView.AccountNumber = businessCollateralAgmLine.AccountNumber;
				businessCollateralAgmLineItemView.AttachmentText = businessCollateralAgmLine.AttachmentText;
				businessCollateralAgmLineItemView.BankGroupGUID = businessCollateralAgmLine.BankGroupGUID.GuidNullToString();
				businessCollateralAgmLineItemView.BankTypeGUID = businessCollateralAgmLine.BankTypeGUID.GuidNullToString();
				businessCollateralAgmLineItemView.BusinessCollateralAgmTableGUID = businessCollateralAgmLine.BusinessCollateralAgmTableGUID.GuidNullToString();
				businessCollateralAgmLineItemView.BusinessCollateralSubTypeGUID = businessCollateralAgmLine.BusinessCollateralSubTypeGUID.GuidNullToString();
				businessCollateralAgmLineItemView.BusinessCollateralTypeGUID = businessCollateralAgmLine.BusinessCollateralTypeGUID.GuidNullToString();
				businessCollateralAgmLineItemView.BusinessCollateralValue = businessCollateralAgmLine.BusinessCollateralValue;
				businessCollateralAgmLineItemView.BuyerName = businessCollateralAgmLine.BuyerName;
				businessCollateralAgmLineItemView.BuyerTableGUID = businessCollateralAgmLine.BuyerTableGUID.GuidNullToString();
				businessCollateralAgmLineItemView.BuyerTaxIdentificationId = businessCollateralAgmLine.BuyerTaxIdentificationId;
				businessCollateralAgmLineItemView.CapitalValuation = businessCollateralAgmLine.CapitalValuation;
				businessCollateralAgmLineItemView.ChassisNumber = businessCollateralAgmLine.ChassisNumber;
				businessCollateralAgmLineItemView.CreditAppReqBusinessCollateralGUID = businessCollateralAgmLine.CreditAppReqBusinessCollateralGUID.GuidNullToString();
				businessCollateralAgmLineItemView.DateOfValuation = businessCollateralAgmLine.DateOfValuation.DateNullToString();
				businessCollateralAgmLineItemView.Description = businessCollateralAgmLine.Description;
				businessCollateralAgmLineItemView.GuaranteeAmount = businessCollateralAgmLine.GuaranteeAmount;
				businessCollateralAgmLineItemView.Lessee = businessCollateralAgmLine.Lessee;
				businessCollateralAgmLineItemView.Lessor = businessCollateralAgmLine.Lessor;
				businessCollateralAgmLineItemView.LineNum = businessCollateralAgmLine.LineNum;
				businessCollateralAgmLineItemView.MachineNumber = businessCollateralAgmLine.MachineNumber;
				businessCollateralAgmLineItemView.MachineRegisteredStatus = businessCollateralAgmLine.MachineRegisteredStatus;
				businessCollateralAgmLineItemView.Ownership = businessCollateralAgmLine.Ownership;
				businessCollateralAgmLineItemView.PreferentialCreditorNumber = businessCollateralAgmLine.PreferentialCreditorNumber;
				businessCollateralAgmLineItemView.ProjectName = businessCollateralAgmLine.ProjectName;
				businessCollateralAgmLineItemView.Quantity = businessCollateralAgmLine.Quantity;
				businessCollateralAgmLineItemView.RefAgreementDate = businessCollateralAgmLine.RefAgreementDate.DateNullToString();
				businessCollateralAgmLineItemView.RefAgreementId = businessCollateralAgmLine.RefAgreementId;
				businessCollateralAgmLineItemView.RegisteredPlace = businessCollateralAgmLine.RegisteredPlace;
				businessCollateralAgmLineItemView.RegistrationPlateNumber = businessCollateralAgmLine.RegistrationPlateNumber;
				businessCollateralAgmLineItemView.TitleDeedDistrict = businessCollateralAgmLine.TitleDeedDistrict;
				businessCollateralAgmLineItemView.TitleDeedNumber = businessCollateralAgmLine.TitleDeedNumber;
				businessCollateralAgmLineItemView.TitleDeedProvince = businessCollateralAgmLine.TitleDeedProvince;
				businessCollateralAgmLineItemView.TitleDeedSubDistrict = businessCollateralAgmLine.TitleDeedSubDistrict;
				businessCollateralAgmLineItemView.Unit = businessCollateralAgmLine.Unit;
				businessCollateralAgmLineItemView.ValuationCommittee = businessCollateralAgmLine.ValuationCommittee;
				businessCollateralAgmLineItemView.Product = businessCollateralAgmLine.Product;
				businessCollateralAgmLineItemView.Cancelled = businessCollateralAgmLine.Cancelled;
				businessCollateralAgmLineItemView.OriginalBusinessCollateralAgreementLineGUID = businessCollateralAgmLine.OriginalBusinessCollateralAgreementLineGUID.GuidNullToString();
				businessCollateralAgmLineItemView.OriginalBusinessCollateralAgreementTableGUID = businessCollateralAgmLine.OriginalBusinessCollateralAgreementTableGUID.GuidNullToString();
				businessCollateralAgmLineItemView.BusinessCollateralStatusGUID = businessCollateralAgmLine.BusinessCollateralStatusGUID.GuidNullToString();

				
				businessCollateralAgmLineItemView.RowVersion = businessCollateralAgmLine.RowVersion;
				return businessCollateralAgmLineItemView.GetBusinessCollateralAgmLineItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion BusinessCollateralAgmLineItemView
		#region ToDropDown
		public static SelectItem<BusinessCollateralAgmLineItemView> ToDropDownItem(this BusinessCollateralAgmLineItemView businessCollateralAgmLineView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<BusinessCollateralAgmLineItemView> selectItem = new SelectItem<BusinessCollateralAgmLineItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(businessCollateralAgmLineView.LineNum.ToString(),businessCollateralAgmLineView.Description);
				selectItem.Value = businessCollateralAgmLineView.BusinessCollateralAgmLineGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: businessCollateralAgmLineView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<BusinessCollateralAgmLineItemView>> ToDropDownItem(this IEnumerable<BusinessCollateralAgmLineItemView> businessCollateralAgmLineItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<BusinessCollateralAgmLineItemView>> selectItems = new List<SelectItem<BusinessCollateralAgmLineItemView>>();
				foreach (BusinessCollateralAgmLineItemView item in businessCollateralAgmLineItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

