using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CalendarGroupHandler
	{
		#region CalendarGroupListView
		public static List<CalendarGroupListView> GetCalendarGroupListViewValidation(this List<CalendarGroupListView> calendarGroupListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return calendarGroupListViews;
			}
			var result = new List<CalendarGroupListView>();
			try
			{
				foreach (CalendarGroupListView item in calendarGroupListViews)
				{
					result.Add(item.GetCalendarGroupListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CalendarGroupListView GetCalendarGroupListViewValidation(this CalendarGroupListView calendarGroupListView)
		{
			try
			{
				calendarGroupListView.RowAuthorize = calendarGroupListView.GetListRowAuthorize();
				return calendarGroupListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CalendarGroupListView calendarGroupListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CalendarGroupListView
		#region CalendarGroupItemView
		public static CalendarGroupItemView GetCalendarGroupItemViewValidation(this CalendarGroupItemView calendarGroupItemView)
		{
			try
			{
				calendarGroupItemView.RowAuthorize = calendarGroupItemView.GetItemRowAuthorize();
				return calendarGroupItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CalendarGroupItemView calendarGroupItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CalendarGroup ToCalendarGroup(this CalendarGroupItemView calendarGroupItemView)
		{
			try
			{
				CalendarGroup calendarGroup = new CalendarGroup();
				calendarGroup.CompanyGUID = calendarGroupItemView.CompanyGUID.StringToGuid();
				calendarGroup.CreatedBy = calendarGroupItemView.CreatedBy;
				calendarGroup.CreatedDateTime = calendarGroupItemView.CreatedDateTime.StringToSystemDateTime();
				calendarGroup.ModifiedBy = calendarGroupItemView.ModifiedBy;
				calendarGroup.ModifiedDateTime = calendarGroupItemView.ModifiedDateTime.StringToSystemDateTime();
				calendarGroup.Owner = calendarGroupItemView.Owner;
				calendarGroup.OwnerBusinessUnitGUID = calendarGroupItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				calendarGroup.CalendarGroupGUID = calendarGroupItemView.CalendarGroupGUID.StringToGuid();
				calendarGroup.CalendarGroupId = calendarGroupItemView.CalendarGroupId;
				calendarGroup.Description = calendarGroupItemView.Description;
				calendarGroup.WorkingDay = calendarGroupItemView.WorkingDay;
				
				calendarGroup.RowVersion = calendarGroupItemView.RowVersion;
				return calendarGroup;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CalendarGroup> ToCalendarGroup(this IEnumerable<CalendarGroupItemView> calendarGroupItemViews)
		{
			try
			{
				List<CalendarGroup> calendarGroups = new List<CalendarGroup>();
				foreach (CalendarGroupItemView item in calendarGroupItemViews)
				{
					calendarGroups.Add(item.ToCalendarGroup());
				}
				return calendarGroups;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CalendarGroupItemView ToCalendarGroupItemView(this CalendarGroup calendarGroup)
		{
			try
			{
				CalendarGroupItemView calendarGroupItemView = new CalendarGroupItemView();
				calendarGroupItemView.CompanyGUID = calendarGroup.CompanyGUID.GuidNullToString();
				calendarGroupItemView.CreatedBy = calendarGroup.CreatedBy;
				calendarGroupItemView.CreatedDateTime = calendarGroup.CreatedDateTime.DateTimeToString();
				calendarGroupItemView.ModifiedBy = calendarGroup.ModifiedBy;
				calendarGroupItemView.ModifiedDateTime = calendarGroup.ModifiedDateTime.DateTimeToString();
				calendarGroupItemView.Owner = calendarGroup.Owner;
				calendarGroupItemView.OwnerBusinessUnitGUID = calendarGroup.OwnerBusinessUnitGUID.GuidNullToString();
				calendarGroupItemView.CalendarGroupGUID = calendarGroup.CalendarGroupGUID.GuidNullToString();
				calendarGroupItemView.CalendarGroupId = calendarGroup.CalendarGroupId;
				calendarGroupItemView.Description = calendarGroup.Description;
				calendarGroupItemView.WorkingDay = calendarGroup.WorkingDay;
				
				calendarGroupItemView.RowVersion = calendarGroup.RowVersion;
				return calendarGroupItemView.GetCalendarGroupItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CalendarGroupItemView
		#region ToDropDown
		public static SelectItem<CalendarGroupItemView> ToDropDownItem(this CalendarGroupItemView calendarGroupView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CalendarGroupItemView> selectItem = new SelectItem<CalendarGroupItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(calendarGroupView.CalendarGroupId, calendarGroupView.Description);	
				selectItem.Value = calendarGroupView.CalendarGroupGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: calendarGroupView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CalendarGroupItemView>> ToDropDownItem(this IEnumerable<CalendarGroupItemView> calendarGroupItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CalendarGroupItemView>> selectItems = new List<SelectItem<CalendarGroupItemView>>();
				foreach (CalendarGroupItemView item in calendarGroupItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

