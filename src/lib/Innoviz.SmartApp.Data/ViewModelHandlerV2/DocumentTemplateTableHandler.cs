using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class DocumentTemplateTableHandler
	{
		#region DocumentTemplateTableListView
		public static List<DocumentTemplateTableListView> GetDocumentTemplateTableListViewValidation(this List<DocumentTemplateTableListView> documentTemplateTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return documentTemplateTableListViews;
			}
			var result = new List<DocumentTemplateTableListView>();
			try
			{
				foreach (DocumentTemplateTableListView item in documentTemplateTableListViews)
				{
					result.Add(item.GetDocumentTemplateTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static DocumentTemplateTableListView GetDocumentTemplateTableListViewValidation(this DocumentTemplateTableListView documentTemplateTableListView)
		{
			try
			{
				documentTemplateTableListView.RowAuthorize = documentTemplateTableListView.GetListRowAuthorize();
				return documentTemplateTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this DocumentTemplateTableListView documentTemplateTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion DocumentTemplateTableListView
		#region DocumentTemplateTableItemView
		public static DocumentTemplateTableItemView GetDocumentTemplateTableItemViewValidation(this DocumentTemplateTableItemView documentTemplateTableItemView)
		{
			try
			{
				documentTemplateTableItemView.RowAuthorize = documentTemplateTableItemView.GetItemRowAuthorize();
				return documentTemplateTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this DocumentTemplateTableItemView documentTemplateTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static DocumentTemplateTable ToDocumentTemplateTable(this DocumentTemplateTableItemView documentTemplateTableItemView)
		{
			try
			{
				DocumentTemplateTable documentTemplateTable = new DocumentTemplateTable();
				documentTemplateTable.CompanyGUID = documentTemplateTableItemView.CompanyGUID.StringToGuid();
				documentTemplateTable.CreatedBy = documentTemplateTableItemView.CreatedBy;
				documentTemplateTable.CreatedDateTime = documentTemplateTableItemView.CreatedDateTime.StringToSystemDateTime();
				documentTemplateTable.ModifiedBy = documentTemplateTableItemView.ModifiedBy;
				documentTemplateTable.ModifiedDateTime = documentTemplateTableItemView.ModifiedDateTime.StringToSystemDateTime();
				documentTemplateTable.Owner = documentTemplateTableItemView.Owner;
				documentTemplateTable.OwnerBusinessUnitGUID = documentTemplateTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				documentTemplateTable.DocumentTemplateTableGUID = documentTemplateTableItemView.DocumentTemplateTableGUID.StringToGuid();
				documentTemplateTable.Base64Data = documentTemplateTableItemView.Base64Data;
				documentTemplateTable.Description = documentTemplateTableItemView.Description;
				documentTemplateTable.DocumentTemplateType = documentTemplateTableItemView.DocumentTemplateType;
				documentTemplateTable.FileName = documentTemplateTableItemView.FileName;
				documentTemplateTable.FilePath = documentTemplateTableItemView.FilePath;
				documentTemplateTable.TemplateId = documentTemplateTableItemView.TemplateId;
				
				documentTemplateTable.RowVersion = documentTemplateTableItemView.RowVersion;
				return documentTemplateTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<DocumentTemplateTable> ToDocumentTemplateTable(this IEnumerable<DocumentTemplateTableItemView> documentTemplateTableItemViews)
		{
			try
			{
				List<DocumentTemplateTable> documentTemplateTables = new List<DocumentTemplateTable>();
				foreach (DocumentTemplateTableItemView item in documentTemplateTableItemViews)
				{
					documentTemplateTables.Add(item.ToDocumentTemplateTable());
				}
				return documentTemplateTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static DocumentTemplateTableItemView ToDocumentTemplateTableItemView(this DocumentTemplateTable documentTemplateTable)
		{
			try
			{
				DocumentTemplateTableItemView documentTemplateTableItemView = new DocumentTemplateTableItemView();
				documentTemplateTableItemView.CompanyGUID = documentTemplateTable.CompanyGUID.GuidNullToString();
				documentTemplateTableItemView.CreatedBy = documentTemplateTable.CreatedBy;
				documentTemplateTableItemView.CreatedDateTime = documentTemplateTable.CreatedDateTime.DateTimeToString();
				documentTemplateTableItemView.ModifiedBy = documentTemplateTable.ModifiedBy;
				documentTemplateTableItemView.ModifiedDateTime = documentTemplateTable.ModifiedDateTime.DateTimeToString();
				documentTemplateTableItemView.Owner = documentTemplateTable.Owner;
				documentTemplateTableItemView.OwnerBusinessUnitGUID = documentTemplateTable.OwnerBusinessUnitGUID.GuidNullToString();
				documentTemplateTableItemView.DocumentTemplateTableGUID = documentTemplateTable.DocumentTemplateTableGUID.GuidNullToString();
				documentTemplateTableItemView.Base64Data = documentTemplateTable.Base64Data;
				documentTemplateTableItemView.Description = documentTemplateTable.Description;
				documentTemplateTableItemView.DocumentTemplateType = documentTemplateTable.DocumentTemplateType;
				documentTemplateTableItemView.FileName = documentTemplateTable.FileName;
				documentTemplateTableItemView.FilePath = documentTemplateTable.FilePath;
				documentTemplateTableItemView.TemplateId = documentTemplateTable.TemplateId;
				
				documentTemplateTableItemView.RowVersion = documentTemplateTable.RowVersion;
				return documentTemplateTableItemView.GetDocumentTemplateTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion DocumentTemplateTableItemView
		#region ToDropDown
		public static SelectItem<DocumentTemplateTableItemView> ToDropDownItem(this DocumentTemplateTableItemView documentTemplateTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<DocumentTemplateTableItemView> selectItem = new SelectItem<DocumentTemplateTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(documentTemplateTableView.TemplateId,documentTemplateTableView.Description);
				selectItem.Value = documentTemplateTableView.DocumentTemplateTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: documentTemplateTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<DocumentTemplateTableItemView>> ToDropDownItem(this IEnumerable<DocumentTemplateTableItemView> documentTemplateTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<DocumentTemplateTableItemView>> selectItems = new List<SelectItem<DocumentTemplateTableItemView>>();
				foreach (DocumentTemplateTableItemView item in documentTemplateTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

