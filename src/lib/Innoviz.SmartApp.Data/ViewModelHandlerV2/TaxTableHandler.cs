using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class TaxTableHandler
	{
		#region TaxTableListView
		public static List<TaxTableListView> GetTaxTableListViewValidation(this List<TaxTableListView> taxTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return taxTableListViews;
			}
			var result = new List<TaxTableListView>();
			try
			{
				foreach (TaxTableListView item in taxTableListViews)
				{
					result.Add(item.GetTaxTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static TaxTableListView GetTaxTableListViewValidation(this TaxTableListView taxTableListView)
		{
			try
			{
				taxTableListView.RowAuthorize = taxTableListView.GetListRowAuthorize();
				return taxTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this TaxTableListView taxTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion TaxTableListView
		#region TaxTableItemView
		public static TaxTableItemView GetTaxTableItemViewValidation(this TaxTableItemView taxTableItemView)
		{
			try
			{
				taxTableItemView.RowAuthorize = taxTableItemView.GetItemRowAuthorize();
				return taxTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this TaxTableItemView taxTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static TaxTable ToTaxTable(this TaxTableItemView taxTableItemView)
		{
			try
			{
				TaxTable taxTable = new TaxTable();
				taxTable.CompanyGUID = taxTableItemView.CompanyGUID.StringToGuid();
				taxTable.CreatedBy = taxTableItemView.CreatedBy;
				taxTable.CreatedDateTime = taxTableItemView.CreatedDateTime.StringToSystemDateTime();
				taxTable.ModifiedBy = taxTableItemView.ModifiedBy;
				taxTable.ModifiedDateTime = taxTableItemView.ModifiedDateTime.StringToSystemDateTime();
				taxTable.Owner = taxTableItemView.Owner;
				taxTable.OwnerBusinessUnitGUID = taxTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				taxTable.TaxTableGUID = taxTableItemView.TaxTableGUID.StringToGuid();
				taxTable.Description = taxTableItemView.Description;
				taxTable.InputTaxLedgerAccount = taxTableItemView.InputTaxLedgerAccount;
				taxTable.OutputTaxLedgerAccount = taxTableItemView.OutputTaxLedgerAccount;
				taxTable.PaymentTaxTableGUID = taxTableItemView.PaymentTaxTableGUID.StringToGuidNull();
				taxTable.TaxCode = taxTableItemView.TaxCode;
				
				taxTable.RowVersion = taxTableItemView.RowVersion;
				return taxTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<TaxTable> ToTaxTable(this IEnumerable<TaxTableItemView> taxTableItemViews)
		{
			try
			{
				List<TaxTable> taxTables = new List<TaxTable>();
				foreach (TaxTableItemView item in taxTableItemViews)
				{
					taxTables.Add(item.ToTaxTable());
				}
				return taxTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static TaxTableItemView ToTaxTableItemView(this TaxTable taxTable)
		{
			try
			{
				TaxTableItemView taxTableItemView = new TaxTableItemView();
				taxTableItemView.CompanyGUID = taxTable.CompanyGUID.GuidNullToString();
				taxTableItemView.CreatedBy = taxTable.CreatedBy;
				taxTableItemView.CreatedDateTime = taxTable.CreatedDateTime.DateTimeToString();
				taxTableItemView.ModifiedBy = taxTable.ModifiedBy;
				taxTableItemView.ModifiedDateTime = taxTable.ModifiedDateTime.DateTimeToString();
				taxTableItemView.Owner = taxTable.Owner;
				taxTableItemView.OwnerBusinessUnitGUID = taxTable.OwnerBusinessUnitGUID.GuidNullToString();
				taxTableItemView.TaxTableGUID = taxTable.TaxTableGUID.GuidNullToString();
				taxTableItemView.Description = taxTable.Description;
				taxTableItemView.InputTaxLedgerAccount = taxTable.InputTaxLedgerAccount;
				taxTableItemView.OutputTaxLedgerAccount = taxTable.OutputTaxLedgerAccount;
				taxTableItemView.PaymentTaxTableGUID = taxTable.PaymentTaxTableGUID.GuidNullToString();
				taxTableItemView.TaxCode = taxTable.TaxCode;
				
				taxTableItemView.RowVersion = taxTable.RowVersion;
				return taxTableItemView.GetTaxTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion TaxTableItemView
		#region ToDropDown
		public static SelectItem<TaxTableItemView> ToDropDownItem(this TaxTableItemView taxTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<TaxTableItemView> selectItem = new SelectItem<TaxTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(taxTableView.TaxCode, taxTableView.Description);
				selectItem.Value = taxTableView.TaxTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: taxTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<TaxTableItemView>> ToDropDownItem(this IEnumerable<TaxTableItemView> taxTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<TaxTableItemView>> selectItems = new List<SelectItem<TaxTableItemView>>();
				foreach (TaxTableItemView item in taxTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

