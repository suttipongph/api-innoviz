using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CustomerCreditLimitByProductHandler
	{
		#region CustomerCreditLimitByProductListView
		public static List<CustomerCreditLimitByProductListView> GetCustomerCreditLimitByProductListViewValidation(this List<CustomerCreditLimitByProductListView> customerCreditLimitByProductListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return customerCreditLimitByProductListViews;
			}
			var result = new List<CustomerCreditLimitByProductListView>();
			try
			{
				foreach (CustomerCreditLimitByProductListView item in customerCreditLimitByProductListViews)
				{
					result.Add(item.GetCustomerCreditLimitByProductListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CustomerCreditLimitByProductListView GetCustomerCreditLimitByProductListViewValidation(this CustomerCreditLimitByProductListView customerCreditLimitByProductListView)
		{
			try
			{
				customerCreditLimitByProductListView.RowAuthorize = customerCreditLimitByProductListView.GetListRowAuthorize();
				return customerCreditLimitByProductListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CustomerCreditLimitByProductListView customerCreditLimitByProductListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CustomerCreditLimitByProductListView
		#region CustomerCreditLimitByProductItemView
		public static CustomerCreditLimitByProductItemView GetCustomerCreditLimitByProductItemViewValidation(this CustomerCreditLimitByProductItemView customerCreditLimitByProductItemView)
		{
			try
			{
				customerCreditLimitByProductItemView.RowAuthorize = customerCreditLimitByProductItemView.GetItemRowAuthorize();
				return customerCreditLimitByProductItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CustomerCreditLimitByProductItemView customerCreditLimitByProductItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CustomerCreditLimitByProduct ToCustomerCreditLimitByProduct(this CustomerCreditLimitByProductItemView customerCreditLimitByProductItemView)
		{
			try
			{
				CustomerCreditLimitByProduct customerCreditLimitByProduct = new CustomerCreditLimitByProduct();
				customerCreditLimitByProduct.CompanyGUID = customerCreditLimitByProductItemView.CompanyGUID.StringToGuid();
				customerCreditLimitByProduct.CreatedBy = customerCreditLimitByProductItemView.CreatedBy;
				customerCreditLimitByProduct.CreatedDateTime = customerCreditLimitByProductItemView.CreatedDateTime.StringToSystemDateTime();
				customerCreditLimitByProduct.ModifiedBy = customerCreditLimitByProductItemView.ModifiedBy;
				customerCreditLimitByProduct.ModifiedDateTime = customerCreditLimitByProductItemView.ModifiedDateTime.StringToSystemDateTime();
				customerCreditLimitByProduct.Owner = customerCreditLimitByProductItemView.Owner;
				customerCreditLimitByProduct.OwnerBusinessUnitGUID = customerCreditLimitByProductItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				customerCreditLimitByProduct.CustomerCreditLimitByProductGUID = customerCreditLimitByProductItemView.CustomerCreditLimitByProductGUID.StringToGuid();
				customerCreditLimitByProduct.CreditLimit = customerCreditLimitByProductItemView.CreditLimit;
				customerCreditLimitByProduct.CustomerTableGUID = customerCreditLimitByProductItemView.CustomerTableGUID.StringToGuid();
				customerCreditLimitByProduct.ProductType = customerCreditLimitByProductItemView.ProductType;
				
				customerCreditLimitByProduct.RowVersion = customerCreditLimitByProductItemView.RowVersion;
				return customerCreditLimitByProduct;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CustomerCreditLimitByProduct> ToCustomerCreditLimitByProduct(this IEnumerable<CustomerCreditLimitByProductItemView> customerCreditLimitByProductItemViews)
		{
			try
			{
				List<CustomerCreditLimitByProduct> customerCreditLimitByProducts = new List<CustomerCreditLimitByProduct>();
				foreach (CustomerCreditLimitByProductItemView item in customerCreditLimitByProductItemViews)
				{
					customerCreditLimitByProducts.Add(item.ToCustomerCreditLimitByProduct());
				}
				return customerCreditLimitByProducts;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CustomerCreditLimitByProductItemView ToCustomerCreditLimitByProductItemView(this CustomerCreditLimitByProduct customerCreditLimitByProduct)
		{
			try
			{
				CustomerCreditLimitByProductItemView customerCreditLimitByProductItemView = new CustomerCreditLimitByProductItemView();
				customerCreditLimitByProductItemView.CompanyGUID = customerCreditLimitByProduct.CompanyGUID.GuidNullToString();
				customerCreditLimitByProductItemView.CreatedBy = customerCreditLimitByProduct.CreatedBy;
				customerCreditLimitByProductItemView.CreatedDateTime = customerCreditLimitByProduct.CreatedDateTime.DateTimeToString();
				customerCreditLimitByProductItemView.ModifiedBy = customerCreditLimitByProduct.ModifiedBy;
				customerCreditLimitByProductItemView.ModifiedDateTime = customerCreditLimitByProduct.ModifiedDateTime.DateTimeToString();
				customerCreditLimitByProductItemView.Owner = customerCreditLimitByProduct.Owner;
				customerCreditLimitByProductItemView.OwnerBusinessUnitGUID = customerCreditLimitByProduct.OwnerBusinessUnitGUID.GuidNullToString();
				customerCreditLimitByProductItemView.CustomerCreditLimitByProductGUID = customerCreditLimitByProduct.CustomerCreditLimitByProductGUID.GuidNullToString();
				customerCreditLimitByProductItemView.CreditLimit = customerCreditLimitByProduct.CreditLimit;
				customerCreditLimitByProductItemView.CustomerTableGUID = customerCreditLimitByProduct.CustomerTableGUID.GuidNullToString();
				customerCreditLimitByProductItemView.ProductType = customerCreditLimitByProduct.ProductType;
				
				customerCreditLimitByProductItemView.RowVersion = customerCreditLimitByProduct.RowVersion;
				return customerCreditLimitByProductItemView.GetCustomerCreditLimitByProductItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CustomerCreditLimitByProductItemView
		#region ToDropDown
		public static SelectItem<CustomerCreditLimitByProductItemView> ToDropDownItem(this CustomerCreditLimitByProductItemView customerCreditLimitByProductView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CustomerCreditLimitByProductItemView> selectItem = new SelectItem<CustomerCreditLimitByProductItemView>();
				selectItem.Label = null;
				selectItem.Value = customerCreditLimitByProductView.CustomerCreditLimitByProductGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: customerCreditLimitByProductView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CustomerCreditLimitByProductItemView>> ToDropDownItem(this IEnumerable<CustomerCreditLimitByProductItemView> customerCreditLimitByProductItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CustomerCreditLimitByProductItemView>> selectItems = new List<SelectItem<CustomerCreditLimitByProductItemView>>();
				foreach (CustomerCreditLimitByProductItemView item in customerCreditLimitByProductItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

