using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class IntercompanyInvoiceTableHandler
	{
		#region IntercompanyInvoiceTableListView
		public static List<IntercompanyInvoiceTableListView> GetIntercompanyInvoiceTableListViewValidation(this List<IntercompanyInvoiceTableListView> intercompanyInvoiceTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return intercompanyInvoiceTableListViews;
			}
			var result = new List<IntercompanyInvoiceTableListView>();
			try
			{
				foreach (IntercompanyInvoiceTableListView item in intercompanyInvoiceTableListViews)
				{
					result.Add(item.GetIntercompanyInvoiceTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IntercompanyInvoiceTableListView GetIntercompanyInvoiceTableListViewValidation(this IntercompanyInvoiceTableListView intercompanyInvoiceTableListView)
		{
			try
			{
				intercompanyInvoiceTableListView.RowAuthorize = intercompanyInvoiceTableListView.GetListRowAuthorize();
				return intercompanyInvoiceTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this IntercompanyInvoiceTableListView intercompanyInvoiceTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion IntercompanyInvoiceTableListView
		#region IntercompanyInvoiceTableItemView
		public static IntercompanyInvoiceTableItemView GetIntercompanyInvoiceTableItemViewValidation(this IntercompanyInvoiceTableItemView intercompanyInvoiceTableItemView)
		{
			try
			{
				intercompanyInvoiceTableItemView.RowAuthorize = intercompanyInvoiceTableItemView.GetItemRowAuthorize();
				return intercompanyInvoiceTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this IntercompanyInvoiceTableItemView intercompanyInvoiceTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static IntercompanyInvoiceTable ToIntercompanyInvoiceTable(this IntercompanyInvoiceTableItemView intercompanyInvoiceTableItemView)
		{
			try
			{
				IntercompanyInvoiceTable intercompanyInvoiceTable = new IntercompanyInvoiceTable();
				intercompanyInvoiceTable.CompanyGUID = intercompanyInvoiceTableItemView.CompanyGUID.StringToGuid();
				intercompanyInvoiceTable.CreatedBy = intercompanyInvoiceTableItemView.CreatedBy;
				intercompanyInvoiceTable.CreatedDateTime = intercompanyInvoiceTableItemView.CreatedDateTime.StringToSystemDateTime();
				intercompanyInvoiceTable.ModifiedBy = intercompanyInvoiceTableItemView.ModifiedBy;
				intercompanyInvoiceTable.ModifiedDateTime = intercompanyInvoiceTableItemView.ModifiedDateTime.StringToSystemDateTime();
				intercompanyInvoiceTable.Owner = intercompanyInvoiceTableItemView.Owner;
				intercompanyInvoiceTable.OwnerBusinessUnitGUID = intercompanyInvoiceTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				intercompanyInvoiceTable.IntercompanyInvoiceTableGUID = intercompanyInvoiceTableItemView.IntercompanyInvoiceTableGUID.StringToGuid();
				intercompanyInvoiceTable.CNReasonGUID = intercompanyInvoiceTableItemView.CNReasonGUID.StringToGuidNull();
				intercompanyInvoiceTable.CreditAppTableGUID = intercompanyInvoiceTableItemView.CreditAppTableGUID.StringToGuidNull();
				intercompanyInvoiceTable.CurrencyGUID = intercompanyInvoiceTableItemView.CurrencyGUID.StringToGuid();
				intercompanyInvoiceTable.CustomerName = intercompanyInvoiceTableItemView.CustomerName;
				intercompanyInvoiceTable.CustomerTableGUID = intercompanyInvoiceTableItemView.CustomerTableGUID.StringToGuid();
				intercompanyInvoiceTable.Dimension1GUID = intercompanyInvoiceTableItemView.Dimension1GUID.StringToGuidNull();
				intercompanyInvoiceTable.Dimension2GUID = intercompanyInvoiceTableItemView.Dimension2GUID.StringToGuidNull();
				intercompanyInvoiceTable.Dimension3GUID = intercompanyInvoiceTableItemView.Dimension3GUID.StringToGuidNull();
				intercompanyInvoiceTable.Dimension4GUID = intercompanyInvoiceTableItemView.Dimension4GUID.StringToGuidNull();
				intercompanyInvoiceTable.Dimension5GUID = intercompanyInvoiceTableItemView.Dimension5GUID.StringToGuidNull();
				intercompanyInvoiceTable.DocumentId = intercompanyInvoiceTableItemView.DocumentId;
				intercompanyInvoiceTable.DocumentStatusGUID = intercompanyInvoiceTableItemView.DocumentStatusGUID.StringToGuidNull();
				intercompanyInvoiceTable.DueDate = intercompanyInvoiceTableItemView.DueDate.StringToDate();
				intercompanyInvoiceTable.ExchangeRate = intercompanyInvoiceTableItemView.ExchangeRate;
				intercompanyInvoiceTable.IntercompanyGUID = intercompanyInvoiceTableItemView.IntercompanyGUID.StringToGuid();
				intercompanyInvoiceTable.IntercompanyInvoiceId = intercompanyInvoiceTableItemView.IntercompanyInvoiceId;
				intercompanyInvoiceTable.InvoiceAddressTransGUID = intercompanyInvoiceTableItemView.InvoiceAddressTransGUID.StringToGuid();
				intercompanyInvoiceTable.InvoiceAmount = intercompanyInvoiceTableItemView.InvoiceAmount;
				intercompanyInvoiceTable.InvoiceRevenueTypeGUID = intercompanyInvoiceTableItemView.InvoiceRevenueTypeGUID.StringToGuid();
				intercompanyInvoiceTable.InvoiceText = intercompanyInvoiceTableItemView.InvoiceText;
				intercompanyInvoiceTable.InvoiceTypeGUID = intercompanyInvoiceTableItemView.InvoiceTypeGUID.StringToGuid();
				intercompanyInvoiceTable.IssuedDate = intercompanyInvoiceTableItemView.IssuedDate.StringToDate();
				intercompanyInvoiceTable.OrigInvoiceAmount = intercompanyInvoiceTableItemView.OrigInvoiceAmount;
				intercompanyInvoiceTable.OrigInvoiceId = intercompanyInvoiceTableItemView.OrigInvoiceId;
				intercompanyInvoiceTable.OrigTaxInvoiceAmount = intercompanyInvoiceTableItemView.OrigTaxInvoiceAmount;
				intercompanyInvoiceTable.OrigTaxInvoiceId = intercompanyInvoiceTableItemView.OrigTaxInvoiceId;
				intercompanyInvoiceTable.ProductType = intercompanyInvoiceTableItemView.ProductType;
				intercompanyInvoiceTable.RefGUID = intercompanyInvoiceTableItemView.RefGUID.StringToGuid();
				intercompanyInvoiceTable.RefType = intercompanyInvoiceTableItemView.RefType;
				intercompanyInvoiceTable.TaxBranchId = intercompanyInvoiceTableItemView.TaxBranchId;
				intercompanyInvoiceTable.TaxBranchName = intercompanyInvoiceTableItemView.TaxBranchName;
				intercompanyInvoiceTable.TaxTableGUID = intercompanyInvoiceTableItemView.TaxTableGUID.StringToGuidNull();
				intercompanyInvoiceTable.WithholdingTaxTableGUID = intercompanyInvoiceTableItemView.WithholdingTaxTableGUID.StringToGuidNull();
				intercompanyInvoiceTable.FeeLedgerAccount = intercompanyInvoiceTableItemView.FeeLedgerAccount;
				
				intercompanyInvoiceTable.RowVersion = intercompanyInvoiceTableItemView.RowVersion;
				return intercompanyInvoiceTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<IntercompanyInvoiceTable> ToIntercompanyInvoiceTable(this IEnumerable<IntercompanyInvoiceTableItemView> intercompanyInvoiceTableItemViews)
		{
			try
			{
				List<IntercompanyInvoiceTable> intercompanyInvoiceTables = new List<IntercompanyInvoiceTable>();
				foreach (IntercompanyInvoiceTableItemView item in intercompanyInvoiceTableItemViews)
				{
					intercompanyInvoiceTables.Add(item.ToIntercompanyInvoiceTable());
				}
				return intercompanyInvoiceTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IntercompanyInvoiceTableItemView ToIntercompanyInvoiceTableItemView(this IntercompanyInvoiceTable intercompanyInvoiceTable)
		{
			try
			{
				IntercompanyInvoiceTableItemView intercompanyInvoiceTableItemView = new IntercompanyInvoiceTableItemView();
				intercompanyInvoiceTableItemView.CompanyGUID = intercompanyInvoiceTable.CompanyGUID.GuidNullToString();
				intercompanyInvoiceTableItemView.CreatedBy = intercompanyInvoiceTable.CreatedBy;
				intercompanyInvoiceTableItemView.CreatedDateTime = intercompanyInvoiceTable.CreatedDateTime.DateTimeToString();
				intercompanyInvoiceTableItemView.ModifiedBy = intercompanyInvoiceTable.ModifiedBy;
				intercompanyInvoiceTableItemView.ModifiedDateTime = intercompanyInvoiceTable.ModifiedDateTime.DateTimeToString();
				intercompanyInvoiceTableItemView.Owner = intercompanyInvoiceTable.Owner;
				intercompanyInvoiceTableItemView.OwnerBusinessUnitGUID = intercompanyInvoiceTable.OwnerBusinessUnitGUID.GuidNullToString();
				intercompanyInvoiceTableItemView.IntercompanyInvoiceTableGUID = intercompanyInvoiceTable.IntercompanyInvoiceTableGUID.GuidNullToString();
				intercompanyInvoiceTableItemView.CNReasonGUID = intercompanyInvoiceTable.CNReasonGUID.GuidNullToString();
				intercompanyInvoiceTableItemView.CreditAppTableGUID = intercompanyInvoiceTable.CreditAppTableGUID.GuidNullToString();
				intercompanyInvoiceTableItemView.CurrencyGUID = intercompanyInvoiceTable.CurrencyGUID.GuidNullToString();
				intercompanyInvoiceTableItemView.CustomerName = intercompanyInvoiceTable.CustomerName;
				intercompanyInvoiceTableItemView.CustomerTableGUID = intercompanyInvoiceTable.CustomerTableGUID.GuidNullToString();
				intercompanyInvoiceTableItemView.Dimension1GUID = intercompanyInvoiceTable.Dimension1GUID.GuidNullToString();
				intercompanyInvoiceTableItemView.Dimension2GUID = intercompanyInvoiceTable.Dimension2GUID.GuidNullToString();
				intercompanyInvoiceTableItemView.Dimension3GUID = intercompanyInvoiceTable.Dimension3GUID.GuidNullToString();
				intercompanyInvoiceTableItemView.Dimension4GUID = intercompanyInvoiceTable.Dimension4GUID.GuidNullToString();
				intercompanyInvoiceTableItemView.Dimension5GUID = intercompanyInvoiceTable.Dimension5GUID.GuidNullToString();
				intercompanyInvoiceTableItemView.DocumentId = intercompanyInvoiceTable.DocumentId;
				intercompanyInvoiceTableItemView.DocumentStatusGUID = intercompanyInvoiceTable.DocumentStatusGUID.GuidNullToString();
				intercompanyInvoiceTableItemView.DueDate = intercompanyInvoiceTable.DueDate.DateToString();
				intercompanyInvoiceTableItemView.ExchangeRate = intercompanyInvoiceTable.ExchangeRate;
				intercompanyInvoiceTableItemView.IntercompanyGUID = intercompanyInvoiceTable.IntercompanyGUID.GuidNullToString();
				intercompanyInvoiceTableItemView.IntercompanyInvoiceId = intercompanyInvoiceTable.IntercompanyInvoiceId;
				intercompanyInvoiceTableItemView.InvoiceAddressTransGUID = intercompanyInvoiceTable.InvoiceAddressTransGUID.GuidNullToString();
				intercompanyInvoiceTableItemView.InvoiceAmount = intercompanyInvoiceTable.InvoiceAmount;
				intercompanyInvoiceTableItemView.InvoiceRevenueTypeGUID = intercompanyInvoiceTable.InvoiceRevenueTypeGUID.GuidNullToString();
				intercompanyInvoiceTableItemView.InvoiceText = intercompanyInvoiceTable.InvoiceText;
				intercompanyInvoiceTableItemView.InvoiceTypeGUID = intercompanyInvoiceTable.InvoiceTypeGUID.GuidNullToString();
				intercompanyInvoiceTableItemView.IssuedDate = intercompanyInvoiceTable.IssuedDate.DateToString();
				intercompanyInvoiceTableItemView.OrigInvoiceAmount = intercompanyInvoiceTable.OrigInvoiceAmount;
				intercompanyInvoiceTableItemView.OrigInvoiceId = intercompanyInvoiceTable.OrigInvoiceId;
				intercompanyInvoiceTableItemView.OrigTaxInvoiceAmount = intercompanyInvoiceTable.OrigTaxInvoiceAmount;
				intercompanyInvoiceTableItemView.OrigTaxInvoiceId = intercompanyInvoiceTable.OrigTaxInvoiceId;
				intercompanyInvoiceTableItemView.ProductType = intercompanyInvoiceTable.ProductType;
				intercompanyInvoiceTableItemView.RefGUID = intercompanyInvoiceTable.RefGUID.GuidNullToString();
				intercompanyInvoiceTableItemView.TaxBranchId = intercompanyInvoiceTable.TaxBranchId;
				intercompanyInvoiceTableItemView.TaxBranchName = intercompanyInvoiceTable.TaxBranchName;
				intercompanyInvoiceTableItemView.TaxTableGUID = intercompanyInvoiceTable.TaxTableGUID.GuidNullToString();
				intercompanyInvoiceTableItemView.WithholdingTaxTableGUID = intercompanyInvoiceTable.WithholdingTaxTableGUID.GuidNullToString();
				
				intercompanyInvoiceTableItemView.RowVersion = intercompanyInvoiceTable.RowVersion;
				return intercompanyInvoiceTableItemView.GetIntercompanyInvoiceTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion IntercompanyInvoiceTableItemView
		#region ToDropDown
		public static SelectItem<IntercompanyInvoiceTableItemView> ToDropDownItem(this IntercompanyInvoiceTableItemView intercompanyInvoiceTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<IntercompanyInvoiceTableItemView> selectItem = new SelectItem<IntercompanyInvoiceTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(intercompanyInvoiceTableView.IntercompanyInvoiceId, intercompanyInvoiceTableView.IssuedDate.ToString());
				selectItem.Value = intercompanyInvoiceTableView.IntercompanyInvoiceTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: intercompanyInvoiceTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<IntercompanyInvoiceTableItemView>> ToDropDownItem(this IEnumerable<IntercompanyInvoiceTableItemView> intercompanyInvoiceTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<IntercompanyInvoiceTableItemView>> selectItems = new List<SelectItem<IntercompanyInvoiceTableItemView>>();
				foreach (IntercompanyInvoiceTableItemView item in intercompanyInvoiceTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

