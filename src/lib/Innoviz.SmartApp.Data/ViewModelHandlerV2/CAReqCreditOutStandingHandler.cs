using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CAReqCreditOutStandingHandler
	{
		#region CAReqCreditOutStandingListView
		public static List<CAReqCreditOutStandingListView> GetCAReqCreditOutStandingListViewValidation(this List<CAReqCreditOutStandingListView> caReqCreditOutStandingListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return caReqCreditOutStandingListViews;
			}
			var result = new List<CAReqCreditOutStandingListView>();
			try
			{
				foreach (CAReqCreditOutStandingListView item in caReqCreditOutStandingListViews)
				{
					result.Add(item.GetCAReqCreditOutStandingListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CAReqCreditOutStandingListView GetCAReqCreditOutStandingListViewValidation(this CAReqCreditOutStandingListView caReqCreditOutStandingListView)
		{
			try
			{
				caReqCreditOutStandingListView.RowAuthorize = caReqCreditOutStandingListView.GetListRowAuthorize();
				return caReqCreditOutStandingListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CAReqCreditOutStandingListView caReqCreditOutStandingListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CAReqCreditOutStandingListView
		#region CAReqCreditOutStandingItemView
		public static CAReqCreditOutStandingItemView GetCAReqCreditOutStandingItemViewValidation(this CAReqCreditOutStandingItemView caReqCreditOutStandingItemView)
		{
			try
			{
				caReqCreditOutStandingItemView.RowAuthorize = caReqCreditOutStandingItemView.GetItemRowAuthorize();
				return caReqCreditOutStandingItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CAReqCreditOutStandingItemView caReqCreditOutStandingItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CAReqCreditOutStanding ToCAReqCreditOutStanding(this CAReqCreditOutStandingItemView caReqCreditOutStandingItemView)
		{
			try
			{
				CAReqCreditOutStanding caReqCreditOutStanding = new CAReqCreditOutStanding();
				caReqCreditOutStanding.CompanyGUID = caReqCreditOutStandingItemView.CompanyGUID.StringToGuid();
				caReqCreditOutStanding.CreatedBy = caReqCreditOutStandingItemView.CreatedBy;
				caReqCreditOutStanding.CreatedDateTime = caReqCreditOutStandingItemView.CreatedDateTime.StringToSystemDateTime();
				caReqCreditOutStanding.ModifiedBy = caReqCreditOutStandingItemView.ModifiedBy;
				caReqCreditOutStanding.ModifiedDateTime = caReqCreditOutStandingItemView.ModifiedDateTime.StringToSystemDateTime();
				caReqCreditOutStanding.Owner = caReqCreditOutStandingItemView.Owner;
				caReqCreditOutStanding.OwnerBusinessUnitGUID = caReqCreditOutStandingItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				caReqCreditOutStanding.CAReqCreditOutStandingGUID = caReqCreditOutStandingItemView.CAReqCreditOutStandingGUID.StringToGuid();
				caReqCreditOutStanding.AccumRetentionAmount = caReqCreditOutStandingItemView.AccumRetentionAmount;
				caReqCreditOutStanding.ApprovedCreditLimit = caReqCreditOutStandingItemView.ApprovedCreditLimit;
				caReqCreditOutStanding.ARBalance = caReqCreditOutStandingItemView.ARBalance;
				caReqCreditOutStanding.AsOfDate = caReqCreditOutStandingItemView.AsOfDate.StringNullToDateNull();
				caReqCreditOutStanding.CreditAppRequestTableGUID = caReqCreditOutStandingItemView.CreditAppRequestTableGUID.StringToGuid();
				caReqCreditOutStanding.CreditAppTableGUID = caReqCreditOutStandingItemView.CreditAppTableGUID.StringToGuidNull();
				caReqCreditOutStanding.CreditLimitBalance = caReqCreditOutStandingItemView.CreditLimitBalance;
				caReqCreditOutStanding.CreditLimitTypeGUID = caReqCreditOutStandingItemView.CreditLimitTypeGUID.StringToGuidNull();
				caReqCreditOutStanding.CustomerTableGUID = caReqCreditOutStandingItemView.CustomerTableGUID.StringToGuidNull();
				caReqCreditOutStanding.ProductType = caReqCreditOutStandingItemView.ProductType;
				caReqCreditOutStanding.ReserveToBeRefund = caReqCreditOutStandingItemView.ReserveToBeRefund;
				
				caReqCreditOutStanding.RowVersion = caReqCreditOutStandingItemView.RowVersion;
				return caReqCreditOutStanding;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CAReqCreditOutStanding> ToCAReqCreditOutStanding(this IEnumerable<CAReqCreditOutStandingItemView> caReqCreditOutStandingItemViews)
		{
			try
			{
				List<CAReqCreditOutStanding> caReqCreditOutStandings = new List<CAReqCreditOutStanding>();
				foreach (CAReqCreditOutStandingItemView item in caReqCreditOutStandingItemViews)
				{
					caReqCreditOutStandings.Add(item.ToCAReqCreditOutStanding());
				}
				return caReqCreditOutStandings;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CAReqCreditOutStandingItemView ToCAReqCreditOutStandingItemView(this CAReqCreditOutStanding caReqCreditOutStanding)
		{
			try
			{
				CAReqCreditOutStandingItemView caReqCreditOutStandingItemView = new CAReqCreditOutStandingItemView();
				caReqCreditOutStandingItemView.CompanyGUID = caReqCreditOutStanding.CompanyGUID.GuidNullToString();
				caReqCreditOutStandingItemView.CreatedBy = caReqCreditOutStanding.CreatedBy;
				caReqCreditOutStandingItemView.CreatedDateTime = caReqCreditOutStanding.CreatedDateTime.DateTimeToString();
				caReqCreditOutStandingItemView.ModifiedBy = caReqCreditOutStanding.ModifiedBy;
				caReqCreditOutStandingItemView.ModifiedDateTime = caReqCreditOutStanding.ModifiedDateTime.DateTimeToString();
				caReqCreditOutStandingItemView.Owner = caReqCreditOutStanding.Owner;
				caReqCreditOutStandingItemView.OwnerBusinessUnitGUID = caReqCreditOutStanding.OwnerBusinessUnitGUID.GuidNullToString();
				caReqCreditOutStandingItemView.CAReqCreditOutStandingGUID = caReqCreditOutStanding.CAReqCreditOutStandingGUID.GuidNullToString();
				caReqCreditOutStandingItemView.AccumRetentionAmount = caReqCreditOutStanding.AccumRetentionAmount;
				caReqCreditOutStandingItemView.ApprovedCreditLimit = caReqCreditOutStanding.ApprovedCreditLimit;
				caReqCreditOutStandingItemView.ARBalance = caReqCreditOutStanding.ARBalance;
				caReqCreditOutStandingItemView.AsOfDate = caReqCreditOutStanding.AsOfDate.DateNullToString();
				caReqCreditOutStandingItemView.CreditAppRequestTableGUID = caReqCreditOutStanding.CreditAppRequestTableGUID.GuidNullToString();
				caReqCreditOutStandingItemView.CreditAppTableGUID = caReqCreditOutStanding.CreditAppTableGUID.GuidNullToString();
				caReqCreditOutStandingItemView.CreditLimitBalance = caReqCreditOutStanding.CreditLimitBalance;
				caReqCreditOutStandingItemView.CreditLimitTypeGUID = caReqCreditOutStanding.CreditLimitTypeGUID.GuidNullToString();
				caReqCreditOutStandingItemView.CustomerTableGUID = caReqCreditOutStanding.CustomerTableGUID.GuidNullToString();
				caReqCreditOutStandingItemView.ProductType = caReqCreditOutStanding.ProductType;
				caReqCreditOutStandingItemView.ReserveToBeRefund = caReqCreditOutStanding.ReserveToBeRefund;
				
				caReqCreditOutStandingItemView.RowVersion = caReqCreditOutStanding.RowVersion;
				return caReqCreditOutStandingItemView.GetCAReqCreditOutStandingItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CAReqCreditOutStandingItemView
		#region ToDropDown
		public static SelectItem<CAReqCreditOutStandingItemView> ToDropDownItem(this CAReqCreditOutStandingItemView caReqCreditOutStandingView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CAReqCreditOutStandingItemView> selectItem = new SelectItem<CAReqCreditOutStandingItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(caReqCreditOutStandingView.ProductType.ToString(), caReqCreditOutStandingView.CreditLimitTypeGUID.ToString());
				selectItem.Value = caReqCreditOutStandingView.CAReqCreditOutStandingGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: caReqCreditOutStandingView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CAReqCreditOutStandingItemView>> ToDropDownItem(this IEnumerable<CAReqCreditOutStandingItemView> caReqCreditOutStandingItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CAReqCreditOutStandingItemView>> selectItems = new List<SelectItem<CAReqCreditOutStandingItemView>>();
				foreach (CAReqCreditOutStandingItemView item in caReqCreditOutStandingItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

