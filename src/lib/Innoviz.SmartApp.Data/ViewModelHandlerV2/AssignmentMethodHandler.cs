using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class AssignmentMethodHandler
	{
		#region AssignmentMethodListView
		public static List<AssignmentMethodListView> GetAssignmentMethodListViewValidation(this List<AssignmentMethodListView> assignmentMethodListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return assignmentMethodListViews;
			}
			var result = new List<AssignmentMethodListView>();
			try
			{
				foreach (AssignmentMethodListView item in assignmentMethodListViews)
				{
					result.Add(item.GetAssignmentMethodListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AssignmentMethodListView GetAssignmentMethodListViewValidation(this AssignmentMethodListView assignmentMethodListView)
		{
			try
			{
				assignmentMethodListView.RowAuthorize = assignmentMethodListView.GetListRowAuthorize();
				return assignmentMethodListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this AssignmentMethodListView assignmentMethodListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion AssignmentMethodListView
		#region AssignmentMethodItemView
		public static AssignmentMethodItemView GetAssignmentMethodItemViewValidation(this AssignmentMethodItemView assignmentMethodItemView)
		{
			try
			{
				assignmentMethodItemView.RowAuthorize = assignmentMethodItemView.GetItemRowAuthorize();
				return assignmentMethodItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this AssignmentMethodItemView assignmentMethodItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static AssignmentMethod ToAssignmentMethod(this AssignmentMethodItemView assignmentMethodItemView)
		{
			try
			{
				AssignmentMethod assignmentMethod = new AssignmentMethod();
				assignmentMethod.CompanyGUID = assignmentMethodItemView.CompanyGUID.StringToGuid();
				assignmentMethod.CreatedBy = assignmentMethodItemView.CreatedBy;
				assignmentMethod.CreatedDateTime = assignmentMethodItemView.CreatedDateTime.StringToSystemDateTime();
				assignmentMethod.ModifiedBy = assignmentMethodItemView.ModifiedBy;
				assignmentMethod.ModifiedDateTime = assignmentMethodItemView.ModifiedDateTime.StringToSystemDateTime();
				assignmentMethod.Owner = assignmentMethodItemView.Owner;
				assignmentMethod.OwnerBusinessUnitGUID = assignmentMethodItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				assignmentMethod.AssignmentMethodGUID = assignmentMethodItemView.AssignmentMethodGUID.StringToGuid();
				assignmentMethod.AssignmentMethodId = assignmentMethodItemView.AssignmentMethodId;
				assignmentMethod.Description = assignmentMethodItemView.Description;
				assignmentMethod.ValidateAssignmentBalance = assignmentMethodItemView.ValidateAssignmentBalance;

				assignmentMethod.RowVersion = assignmentMethodItemView.RowVersion;
				return assignmentMethod;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<AssignmentMethod> ToAssignmentMethod(this IEnumerable<AssignmentMethodItemView> assignmentMethodItemViews)
		{
			try
			{
				List<AssignmentMethod> assignmentMethods = new List<AssignmentMethod>();
				foreach (AssignmentMethodItemView item in assignmentMethodItemViews)
				{
					assignmentMethods.Add(item.ToAssignmentMethod());
				}
				return assignmentMethods;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AssignmentMethodItemView ToAssignmentMethodItemView(this AssignmentMethod assignmentMethod)
		{
			try
			{
				AssignmentMethodItemView assignmentMethodItemView = new AssignmentMethodItemView();
				assignmentMethodItemView.CompanyGUID = assignmentMethod.CompanyGUID.GuidNullToString();
				assignmentMethodItemView.CreatedBy = assignmentMethod.CreatedBy;
				assignmentMethodItemView.CreatedDateTime = assignmentMethod.CreatedDateTime.DateTimeToString();
				assignmentMethodItemView.ModifiedBy = assignmentMethod.ModifiedBy;
				assignmentMethodItemView.ModifiedDateTime = assignmentMethod.ModifiedDateTime.DateTimeToString();
				assignmentMethodItemView.Owner = assignmentMethod.Owner;
				assignmentMethodItemView.OwnerBusinessUnitGUID = assignmentMethod.OwnerBusinessUnitGUID.GuidNullToString();
				assignmentMethodItemView.AssignmentMethodGUID = assignmentMethod.AssignmentMethodGUID.GuidNullToString();
				assignmentMethodItemView.AssignmentMethodId = assignmentMethod.AssignmentMethodId;
				assignmentMethodItemView.Description = assignmentMethod.Description;
				assignmentMethodItemView.ValidateAssignmentBalance = assignmentMethod.ValidateAssignmentBalance;

				assignmentMethodItemView.RowVersion = assignmentMethod.RowVersion;
				return assignmentMethodItemView.GetAssignmentMethodItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion AssignmentMethodItemView
		#region ToDropDown
		public static SelectItem<AssignmentMethodItemView> ToDropDownItem(this AssignmentMethodItemView assignmentMethodView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<AssignmentMethodItemView> selectItem = new SelectItem<AssignmentMethodItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(assignmentMethodView.AssignmentMethodId, assignmentMethodView.Description);
				selectItem.Value = assignmentMethodView.AssignmentMethodGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: assignmentMethodView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<AssignmentMethodItemView>> ToDropDownItem(this IEnumerable<AssignmentMethodItemView> assignmentMethodItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<AssignmentMethodItemView>> selectItems = new List<SelectItem<AssignmentMethodItemView>>();
				foreach (AssignmentMethodItemView item in assignmentMethodItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

