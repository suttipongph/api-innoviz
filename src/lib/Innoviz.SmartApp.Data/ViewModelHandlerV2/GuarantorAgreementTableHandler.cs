using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class GuarantorAgreementTableHandler
	{
		#region GuarantorAgreementTableListView
		public static List<GuarantorAgreementTableListView> GetGuarantorAgreementTableListViewValidation(this List<GuarantorAgreementTableListView> guarantorAgreementTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return guarantorAgreementTableListViews;
			}
			var result = new List<GuarantorAgreementTableListView>();
			try
			{
				foreach (GuarantorAgreementTableListView item in guarantorAgreementTableListViews)
				{
					result.Add(item.GetGuarantorAgreementTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static GuarantorAgreementTableListView GetGuarantorAgreementTableListViewValidation(this GuarantorAgreementTableListView guarantorAgreementTableListView)
		{
			try
			{
				guarantorAgreementTableListView.RowAuthorize = guarantorAgreementTableListView.GetListRowAuthorize();
				return guarantorAgreementTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this GuarantorAgreementTableListView guarantorAgreementTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion GuarantorAgreementTableListView
		#region GuarantorAgreementTableItemView
		public static GuarantorAgreementTableItemView GetGuarantorAgreementTableItemViewValidation(this GuarantorAgreementTableItemView guarantorAgreementTableItemView)
		{
			try
			{
				guarantorAgreementTableItemView.RowAuthorize = guarantorAgreementTableItemView.GetItemRowAuthorize();
				return guarantorAgreementTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this GuarantorAgreementTableItemView guarantorAgreementTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static GuarantorAgreementTable ToGuarantorAgreementTable(this GuarantorAgreementTableItemView guarantorAgreementTableItemView)
		{
			try
			{
				GuarantorAgreementTable guarantorAgreementTable = new GuarantorAgreementTable();
				guarantorAgreementTable.CompanyGUID = guarantorAgreementTableItemView.CompanyGUID.StringToGuid();
				guarantorAgreementTable.CreatedBy = guarantorAgreementTableItemView.CreatedBy;
				guarantorAgreementTable.CreatedDateTime = guarantorAgreementTableItemView.CreatedDateTime.StringToSystemDateTime();
				guarantorAgreementTable.ModifiedBy = guarantorAgreementTableItemView.ModifiedBy;
				guarantorAgreementTable.ModifiedDateTime = guarantorAgreementTableItemView.ModifiedDateTime.StringToSystemDateTime();
				guarantorAgreementTable.Owner = guarantorAgreementTableItemView.Owner;
				guarantorAgreementTable.OwnerBusinessUnitGUID = guarantorAgreementTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				guarantorAgreementTable.GuarantorAgreementTableGUID = guarantorAgreementTableItemView.GuarantorAgreementTableGUID.StringToGuid();
				guarantorAgreementTable.Affiliate = guarantorAgreementTableItemView.Affiliate;
				guarantorAgreementTable.AgreementDate = guarantorAgreementTableItemView.AgreementDate.StringToDate();
				guarantorAgreementTable.AgreementDocType = guarantorAgreementTableItemView.AgreementDocType;
				guarantorAgreementTable.AgreementExtension = guarantorAgreementTableItemView.AgreementExtension;
				guarantorAgreementTable.AgreementYear = guarantorAgreementTableItemView.AgreementYear;
				guarantorAgreementTable.Description = guarantorAgreementTableItemView.Description;
				guarantorAgreementTable.DocumentReasonGUID = guarantorAgreementTableItemView.DocumentReasonGUID.StringToGuidNull();
				guarantorAgreementTable.DocumentStatusGUID = guarantorAgreementTableItemView.DocumentStatusGUID.StringToGuidNull();
				guarantorAgreementTable.ExpiryDate = guarantorAgreementTableItemView.ExpiryDate.StringToDate();
				guarantorAgreementTable.GuarantorAgreementId = guarantorAgreementTableItemView.GuarantorAgreementId;
				guarantorAgreementTable.InternalGuarantorAgreementId = guarantorAgreementTableItemView.InternalGuarantorAgreementId;
				guarantorAgreementTable.MainAgreementTableGUID = guarantorAgreementTableItemView.MainAgreementTableGUID.StringToGuid();
				guarantorAgreementTable.ParentCompanyGUID = guarantorAgreementTableItemView.ParentCompanyGUID.StringToGuidNull();
				guarantorAgreementTable.RefGuarantorAgreementTableGUID = guarantorAgreementTableItemView.RefGuarantorAgreementTableGUID.StringToGuidNull();
				guarantorAgreementTable.SigningDate = guarantorAgreementTableItemView.SigningDate.StringNullToDateNull();
				guarantorAgreementTable.WitnessCompany1GUID = guarantorAgreementTableItemView.WitnessCompany1GUID.StringToGuidNull();
				guarantorAgreementTable.WitnessCompany2GUID = guarantorAgreementTableItemView.WitnessCompany2GUID.StringToGuidNull();
				guarantorAgreementTable.WitnessCustomer1 = guarantorAgreementTableItemView.WitnessCustomer1;
				guarantorAgreementTable.WitnessCustomer2 = guarantorAgreementTableItemView.WitnessCustomer2;
				guarantorAgreementTable.Remark = guarantorAgreementTableItemView.Remark;

				
				guarantorAgreementTable.RowVersion = guarantorAgreementTableItemView.RowVersion;
				return guarantorAgreementTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<GuarantorAgreementTable> ToGuarantorAgreementTable(this IEnumerable<GuarantorAgreementTableItemView> guarantorAgreementTableItemViews)
		{
			try
			{
				List<GuarantorAgreementTable> guarantorAgreementTables = new List<GuarantorAgreementTable>();
				foreach (GuarantorAgreementTableItemView item in guarantorAgreementTableItemViews)
				{
					guarantorAgreementTables.Add(item.ToGuarantorAgreementTable());
				}
				return guarantorAgreementTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static GuarantorAgreementTableItemView ToGuarantorAgreementTableItemView(this GuarantorAgreementTable guarantorAgreementTable)
		{
			try
			{
				GuarantorAgreementTableItemView guarantorAgreementTableItemView = new GuarantorAgreementTableItemView();
				guarantorAgreementTableItemView.CompanyGUID = guarantorAgreementTable.CompanyGUID.GuidNullToString();
				guarantorAgreementTableItemView.CreatedBy = guarantorAgreementTable.CreatedBy;
				guarantorAgreementTableItemView.CreatedDateTime = guarantorAgreementTable.CreatedDateTime.DateTimeToString();
				guarantorAgreementTableItemView.ModifiedBy = guarantorAgreementTable.ModifiedBy;
				guarantorAgreementTableItemView.ModifiedDateTime = guarantorAgreementTable.ModifiedDateTime.DateTimeToString();
				guarantorAgreementTableItemView.Owner = guarantorAgreementTable.Owner;
				guarantorAgreementTableItemView.OwnerBusinessUnitGUID = guarantorAgreementTable.OwnerBusinessUnitGUID.GuidNullToString();
				guarantorAgreementTableItemView.GuarantorAgreementTableGUID = guarantorAgreementTable.GuarantorAgreementTableGUID.GuidNullToString();
				guarantorAgreementTableItemView.Affiliate = guarantorAgreementTable.Affiliate;
				guarantorAgreementTableItemView.AgreementDate = guarantorAgreementTable.AgreementDate.DateToString();
				guarantorAgreementTableItemView.AgreementDocType = guarantorAgreementTable.AgreementDocType;
				guarantorAgreementTableItemView.AgreementExtension = guarantorAgreementTable.AgreementExtension;
				guarantorAgreementTableItemView.AgreementYear = guarantorAgreementTable.AgreementYear;
				guarantorAgreementTableItemView.Description = guarantorAgreementTable.Description;
				guarantorAgreementTableItemView.DocumentReasonGUID = guarantorAgreementTable.DocumentReasonGUID.GuidNullToString();
				guarantorAgreementTableItemView.DocumentStatusGUID = guarantorAgreementTable.DocumentStatusGUID.GuidNullToString();
				guarantorAgreementTableItemView.ExpiryDate = guarantorAgreementTable.ExpiryDate.DateToString();
				guarantorAgreementTableItemView.GuarantorAgreementId = guarantorAgreementTable.GuarantorAgreementId;
				guarantorAgreementTableItemView.InternalGuarantorAgreementId = guarantorAgreementTable.InternalGuarantorAgreementId;
				guarantorAgreementTableItemView.MainAgreementTableGUID = guarantorAgreementTable.MainAgreementTableGUID.GuidNullToString();
				guarantorAgreementTableItemView.ParentCompanyGUID = guarantorAgreementTable.ParentCompanyGUID.GuidNullToString();
				guarantorAgreementTableItemView.RefGuarantorAgreementTableGUID = guarantorAgreementTable.RefGuarantorAgreementTableGUID.GuidNullToString();
				guarantorAgreementTableItemView.SigningDate = guarantorAgreementTable.SigningDate.DateNullToString();
				guarantorAgreementTableItemView.WitnessCompany1GUID = guarantorAgreementTable.WitnessCompany1GUID.GuidNullToString();
				guarantorAgreementTableItemView.WitnessCompany2GUID = guarantorAgreementTable.WitnessCompany2GUID.GuidNullToString();
				guarantorAgreementTableItemView.WitnessCustomer1 = guarantorAgreementTable.WitnessCustomer1;
				guarantorAgreementTableItemView.WitnessCustomer2 = guarantorAgreementTable.WitnessCustomer2;
				guarantorAgreementTableItemView.Remark = guarantorAgreementTable.Remark;

				
				guarantorAgreementTableItemView.RowVersion = guarantorAgreementTable.RowVersion;
				return guarantorAgreementTableItemView.GetGuarantorAgreementTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GuarantorAgreementTableItemView
		#region ToDropDown
		public static SelectItem<GuarantorAgreementTableItemView> ToDropDownItem(this GuarantorAgreementTableItemView guarantorAgreementTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<GuarantorAgreementTableItemView> selectItem = new SelectItem<GuarantorAgreementTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(guarantorAgreementTableView.InternalGuarantorAgreementId, guarantorAgreementTableView.Description);
				selectItem.Value = guarantorAgreementTableView.GuarantorAgreementTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: guarantorAgreementTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<GuarantorAgreementTableItemView>> ToDropDownItem(this IEnumerable<GuarantorAgreementTableItemView> guarantorAgreementTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<GuarantorAgreementTableItemView>> selectItems = new List<SelectItem<GuarantorAgreementTableItemView>>();
				foreach (GuarantorAgreementTableItemView item in guarantorAgreementTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

