using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class GuarantorTypeHandler
	{
		#region GuarantorTypeListView
		public static List<GuarantorTypeListView> GetGuarantorTypeListViewValidation(this List<GuarantorTypeListView> guarantorTypeListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return guarantorTypeListViews;
			}
			var result = new List<GuarantorTypeListView>();
			try
			{
				foreach (GuarantorTypeListView item in guarantorTypeListViews)
				{
					result.Add(item.GetGuarantorTypeListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static GuarantorTypeListView GetGuarantorTypeListViewValidation(this GuarantorTypeListView guarantorTypeListView)
		{
			try
			{
				guarantorTypeListView.RowAuthorize = guarantorTypeListView.GetListRowAuthorize();
				return guarantorTypeListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this GuarantorTypeListView guarantorTypeListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion GuarantorTypeListView
		#region GuarantorTypeItemView
		public static GuarantorTypeItemView GetGuarantorTypeItemViewValidation(this GuarantorTypeItemView guarantorTypeItemView)
		{
			try
			{
				guarantorTypeItemView.RowAuthorize = guarantorTypeItemView.GetItemRowAuthorize();
				return guarantorTypeItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this GuarantorTypeItemView guarantorTypeItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static GuarantorType ToGuarantorType(this GuarantorTypeItemView guarantorTypeItemView)
		{
			try
			{
				GuarantorType guarantorType = new GuarantorType();
				guarantorType.CompanyGUID = guarantorTypeItemView.CompanyGUID.StringToGuid();
				guarantorType.CreatedBy = guarantorTypeItemView.CreatedBy;
				guarantorType.CreatedDateTime = guarantorTypeItemView.CreatedDateTime.StringToSystemDateTime();
				guarantorType.ModifiedBy = guarantorTypeItemView.ModifiedBy;
				guarantorType.ModifiedDateTime = guarantorTypeItemView.ModifiedDateTime.StringToSystemDateTime();
				guarantorType.Owner = guarantorTypeItemView.Owner;
				guarantorType.OwnerBusinessUnitGUID = guarantorTypeItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				guarantorType.GuarantorTypeGUID = guarantorTypeItemView.GuarantorTypeGUID.StringToGuid();
				guarantorType.Description = guarantorTypeItemView.Description;
				guarantorType.GuarantorTypeId = guarantorTypeItemView.GuarantorTypeId;
				
				guarantorType.RowVersion = guarantorTypeItemView.RowVersion;
				return guarantorType;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<GuarantorType> ToGuarantorType(this IEnumerable<GuarantorTypeItemView> guarantorTypeItemViews)
		{
			try
			{
				List<GuarantorType> guarantorTypes = new List<GuarantorType>();
				foreach (GuarantorTypeItemView item in guarantorTypeItemViews)
				{
					guarantorTypes.Add(item.ToGuarantorType());
				}
				return guarantorTypes;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static GuarantorTypeItemView ToGuarantorTypeItemView(this GuarantorType guarantorType)
		{
			try
			{
				GuarantorTypeItemView guarantorTypeItemView = new GuarantorTypeItemView();
				guarantorTypeItemView.CompanyGUID = guarantorType.CompanyGUID.GuidNullToString();
				guarantorTypeItemView.CreatedBy = guarantorType.CreatedBy;
				guarantorTypeItemView.CreatedDateTime = guarantorType.CreatedDateTime.DateTimeToString();
				guarantorTypeItemView.ModifiedBy = guarantorType.ModifiedBy;
				guarantorTypeItemView.ModifiedDateTime = guarantorType.ModifiedDateTime.DateTimeToString();
				guarantorTypeItemView.Owner = guarantorType.Owner;
				guarantorTypeItemView.OwnerBusinessUnitGUID = guarantorType.OwnerBusinessUnitGUID.GuidNullToString();
				guarantorTypeItemView.GuarantorTypeGUID = guarantorType.GuarantorTypeGUID.GuidNullToString();
				guarantorTypeItemView.Description = guarantorType.Description;
				guarantorTypeItemView.GuarantorTypeId = guarantorType.GuarantorTypeId;
				
				guarantorTypeItemView.RowVersion = guarantorType.RowVersion;
				return guarantorTypeItemView.GetGuarantorTypeItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GuarantorTypeItemView
		#region ToDropDown
		public static SelectItem<GuarantorTypeItemView> ToDropDownItem(this GuarantorTypeItemView guarantorTypeView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<GuarantorTypeItemView> selectItem = new SelectItem<GuarantorTypeItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(guarantorTypeView.GuarantorTypeId, guarantorTypeView.Description);
				selectItem.Value = guarantorTypeView.GuarantorTypeGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: guarantorTypeView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<GuarantorTypeItemView>> ToDropDownItem(this IEnumerable<GuarantorTypeItemView> guarantorTypeItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<GuarantorTypeItemView>> selectItems = new List<SelectItem<GuarantorTypeItemView>>();
				foreach (GuarantorTypeItemView item in guarantorTypeItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

