using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class ServiceFeeCondTemplateLineHandler
	{
		#region ServiceFeeCondTemplateLineListView
		public static List<ServiceFeeCondTemplateLineListView> GetServiceFeeCondTemplateLineListViewValidation(this List<ServiceFeeCondTemplateLineListView> serviceFeeCondTemplateLineListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return serviceFeeCondTemplateLineListViews;
			}
			var result = new List<ServiceFeeCondTemplateLineListView>();
			try
			{
				foreach (ServiceFeeCondTemplateLineListView item in serviceFeeCondTemplateLineListViews)
				{
					result.Add(item.GetServiceFeeCondTemplateLineListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ServiceFeeCondTemplateLineListView GetServiceFeeCondTemplateLineListViewValidation(this ServiceFeeCondTemplateLineListView serviceFeeCondTemplateLineListView)
		{
			try
			{
				serviceFeeCondTemplateLineListView.RowAuthorize = serviceFeeCondTemplateLineListView.GetListRowAuthorize();
				return serviceFeeCondTemplateLineListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this ServiceFeeCondTemplateLineListView serviceFeeCondTemplateLineListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion ServiceFeeCondTemplateLineListView
		#region ServiceFeeCondTemplateLineItemView
		public static ServiceFeeCondTemplateLineItemView GetServiceFeeCondTemplateLineItemViewValidation(this ServiceFeeCondTemplateLineItemView serviceFeeCondTemplateLineItemView)
		{
			try
			{
				serviceFeeCondTemplateLineItemView.RowAuthorize = serviceFeeCondTemplateLineItemView.GetItemRowAuthorize();
				return serviceFeeCondTemplateLineItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this ServiceFeeCondTemplateLineItemView serviceFeeCondTemplateLineItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static ServiceFeeCondTemplateLine ToServiceFeeCondTemplateLine(this ServiceFeeCondTemplateLineItemView serviceFeeCondTemplateLineItemView)
		{
			try
			{
				ServiceFeeCondTemplateLine serviceFeeCondTemplateLine = new ServiceFeeCondTemplateLine();
				serviceFeeCondTemplateLine.CompanyGUID = serviceFeeCondTemplateLineItemView.CompanyGUID.StringToGuid();
				serviceFeeCondTemplateLine.CreatedBy = serviceFeeCondTemplateLineItemView.CreatedBy;
				serviceFeeCondTemplateLine.CreatedDateTime = serviceFeeCondTemplateLineItemView.CreatedDateTime.StringToSystemDateTime();
				serviceFeeCondTemplateLine.ModifiedBy = serviceFeeCondTemplateLineItemView.ModifiedBy;
				serviceFeeCondTemplateLine.ModifiedDateTime = serviceFeeCondTemplateLineItemView.ModifiedDateTime.StringToSystemDateTime();
				serviceFeeCondTemplateLine.Owner = serviceFeeCondTemplateLineItemView.Owner;
				serviceFeeCondTemplateLine.OwnerBusinessUnitGUID = serviceFeeCondTemplateLineItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				serviceFeeCondTemplateLine.ServiceFeeCondTemplateLineGUID = serviceFeeCondTemplateLineItemView.ServiceFeeCondTemplateLineGUID.StringToGuid();
				serviceFeeCondTemplateLine.AmountBeforeTax = serviceFeeCondTemplateLineItemView.AmountBeforeTax;
				serviceFeeCondTemplateLine.Description = serviceFeeCondTemplateLineItemView.Description;
				serviceFeeCondTemplateLine.InvoiceRevenueTypeGUID = serviceFeeCondTemplateLineItemView.InvoiceRevenueTypeGUID.StringToGuid();
				serviceFeeCondTemplateLine.Ordering = serviceFeeCondTemplateLineItemView.Ordering;
				serviceFeeCondTemplateLine.ServiceFeeCondTemplateTableGUID = serviceFeeCondTemplateLineItemView.ServiceFeeCondTemplateTableGUID.StringToGuidNull();
				
				serviceFeeCondTemplateLine.RowVersion = serviceFeeCondTemplateLineItemView.RowVersion;
				return serviceFeeCondTemplateLine;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<ServiceFeeCondTemplateLine> ToServiceFeeCondTemplateLine(this IEnumerable<ServiceFeeCondTemplateLineItemView> serviceFeeCondTemplateLineItemViews)
		{
			try
			{
				List<ServiceFeeCondTemplateLine> serviceFeeCondTemplateLines = new List<ServiceFeeCondTemplateLine>();
				foreach (ServiceFeeCondTemplateLineItemView item in serviceFeeCondTemplateLineItemViews)
				{
					serviceFeeCondTemplateLines.Add(item.ToServiceFeeCondTemplateLine());
				}
				return serviceFeeCondTemplateLines;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ServiceFeeCondTemplateLineItemView ToServiceFeeCondTemplateLineItemView(this ServiceFeeCondTemplateLine serviceFeeCondTemplateLine)
		{
			try
			{
				ServiceFeeCondTemplateLineItemView serviceFeeCondTemplateLineItemView = new ServiceFeeCondTemplateLineItemView();
				serviceFeeCondTemplateLineItemView.CompanyGUID = serviceFeeCondTemplateLine.CompanyGUID.GuidNullToString();
				serviceFeeCondTemplateLineItemView.CreatedBy = serviceFeeCondTemplateLine.CreatedBy;
				serviceFeeCondTemplateLineItemView.CreatedDateTime = serviceFeeCondTemplateLine.CreatedDateTime.DateTimeToString();
				serviceFeeCondTemplateLineItemView.ModifiedBy = serviceFeeCondTemplateLine.ModifiedBy;
				serviceFeeCondTemplateLineItemView.ModifiedDateTime = serviceFeeCondTemplateLine.ModifiedDateTime.DateTimeToString();
				serviceFeeCondTemplateLineItemView.Owner = serviceFeeCondTemplateLine.Owner;
				serviceFeeCondTemplateLineItemView.OwnerBusinessUnitGUID = serviceFeeCondTemplateLine.OwnerBusinessUnitGUID.GuidNullToString();
				serviceFeeCondTemplateLineItemView.ServiceFeeCondTemplateLineGUID = serviceFeeCondTemplateLine.ServiceFeeCondTemplateLineGUID.GuidNullToString();
				serviceFeeCondTemplateLineItemView.AmountBeforeTax = serviceFeeCondTemplateLine.AmountBeforeTax;
				serviceFeeCondTemplateLineItemView.Description = serviceFeeCondTemplateLine.Description;
				serviceFeeCondTemplateLineItemView.InvoiceRevenueTypeGUID = serviceFeeCondTemplateLine.InvoiceRevenueTypeGUID.GuidNullToString();
				serviceFeeCondTemplateLineItemView.Ordering = serviceFeeCondTemplateLine.Ordering;
				serviceFeeCondTemplateLineItemView.ServiceFeeCondTemplateTableGUID = serviceFeeCondTemplateLine.ServiceFeeCondTemplateTableGUID.GuidNullToString();
				
				serviceFeeCondTemplateLineItemView.RowVersion = serviceFeeCondTemplateLine.RowVersion;
				return serviceFeeCondTemplateLineItemView.GetServiceFeeCondTemplateLineItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ServiceFeeCondTemplateLineItemView
		#region ToDropDown
		public static SelectItem<ServiceFeeCondTemplateLineItemView> ToDropDownItem(this ServiceFeeCondTemplateLineItemView serviceFeeCondTemplateLineView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<ServiceFeeCondTemplateLineItemView> selectItem = new SelectItem<ServiceFeeCondTemplateLineItemView>();
				selectItem.Label = null;
				selectItem.Value = serviceFeeCondTemplateLineView.ServiceFeeCondTemplateLineGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: serviceFeeCondTemplateLineView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<ServiceFeeCondTemplateLineItemView>> ToDropDownItem(this IEnumerable<ServiceFeeCondTemplateLineItemView> serviceFeeCondTemplateLineItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<ServiceFeeCondTemplateLineItemView>> selectItems = new List<SelectItem<ServiceFeeCondTemplateLineItemView>>();
				foreach (ServiceFeeCondTemplateLineItemView item in serviceFeeCondTemplateLineItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

