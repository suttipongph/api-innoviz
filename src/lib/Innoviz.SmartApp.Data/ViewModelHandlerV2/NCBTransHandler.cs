using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class NCBTransHandler
	{
		#region NCBTransListView
		public static List<NCBTransListView> GetNCBTransListViewValidation(this List<NCBTransListView> ncbTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return ncbTransListViews;
			}
			var result = new List<NCBTransListView>();
			try
			{
				foreach (NCBTransListView item in ncbTransListViews)
				{
					result.Add(item.GetNCBTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static NCBTransListView GetNCBTransListViewValidation(this NCBTransListView ncbTransListView)
		{
			try
			{
				ncbTransListView.RowAuthorize = ncbTransListView.GetListRowAuthorize();
				return ncbTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this NCBTransListView ncbTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion NCBTransListView
		#region NCBTransItemView
		public static NCBTransItemView GetNCBTransItemViewValidation(this NCBTransItemView ncbTransItemView)
		{
			try
			{
				ncbTransItemView.RowAuthorize = ncbTransItemView.GetItemRowAuthorize();
				return ncbTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this NCBTransItemView ncbTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static NCBTrans ToNCBTrans(this NCBTransItemView ncbTransItemView)
		{
			try
			{
				NCBTrans ncbTrans = new NCBTrans();
				ncbTrans.CompanyGUID = ncbTransItemView.CompanyGUID.StringToGuid();
				ncbTrans.CreatedBy = ncbTransItemView.CreatedBy;
				ncbTrans.CreatedDateTime = ncbTransItemView.CreatedDateTime.StringToSystemDateTime();
				ncbTrans.ModifiedBy = ncbTransItemView.ModifiedBy;
				ncbTrans.ModifiedDateTime = ncbTransItemView.ModifiedDateTime.StringToSystemDateTime();
				ncbTrans.Owner = ncbTransItemView.Owner;
				ncbTrans.OwnerBusinessUnitGUID = ncbTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				ncbTrans.NCBTransGUID = ncbTransItemView.NCBTransGUID.StringToGuid();
				ncbTrans.BankGroupGUID = ncbTransItemView.BankGroupGUID.StringToGuid();
				ncbTrans.CreditLimit = ncbTransItemView.CreditLimit;
				ncbTrans.CreditTypeGUID = ncbTransItemView.CreditTypeGUID.StringToGuid();
				ncbTrans.EndDate = ncbTransItemView.EndDate.StringToDate();
				ncbTrans.MonthlyRepayment = ncbTransItemView.MonthlyRepayment;
				ncbTrans.NCBAccountStatusGUID = ncbTransItemView.NCBAccountStatusGUID.StringToGuid();
				ncbTrans.OutstandingAR = ncbTransItemView.OutstandingAR;
				ncbTrans.RefGUID = ncbTransItemView.RefGUID.StringToGuid();
				ncbTrans.RefType = ncbTransItemView.RefType;
				
				ncbTrans.RowVersion = ncbTransItemView.RowVersion;
				return ncbTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<NCBTrans> ToNCBTrans(this IEnumerable<NCBTransItemView> ncbTransItemViews)
		{
			try
			{
				List<NCBTrans> ncbTranss = new List<NCBTrans>();
				foreach (NCBTransItemView item in ncbTransItemViews)
				{
					ncbTranss.Add(item.ToNCBTrans());
				}
				return ncbTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static NCBTransItemView ToNCBTransItemView(this NCBTrans ncbTrans)
		{
			try
			{
				NCBTransItemView ncbTransItemView = new NCBTransItemView();
				ncbTransItemView.CompanyGUID = ncbTrans.CompanyGUID.GuidNullToString();
				ncbTransItemView.CreatedBy = ncbTrans.CreatedBy;
				ncbTransItemView.CreatedDateTime = ncbTrans.CreatedDateTime.DateTimeToString();
				ncbTransItemView.ModifiedBy = ncbTrans.ModifiedBy;
				ncbTransItemView.ModifiedDateTime = ncbTrans.ModifiedDateTime.DateTimeToString();
				ncbTransItemView.Owner = ncbTrans.Owner;
				ncbTransItemView.OwnerBusinessUnitGUID = ncbTrans.OwnerBusinessUnitGUID.GuidNullToString();
				ncbTransItemView.NCBTransGUID = ncbTrans.NCBTransGUID.GuidNullToString();
				ncbTransItemView.BankGroupGUID = ncbTrans.BankGroupGUID.GuidNullToString();
				ncbTransItemView.CreditLimit = ncbTrans.CreditLimit;
				ncbTransItemView.CreditTypeGUID = ncbTrans.CreditTypeGUID.GuidNullToString();
				ncbTransItemView.EndDate = ncbTrans.EndDate.DateToString();
				ncbTransItemView.MonthlyRepayment = ncbTrans.MonthlyRepayment;
				ncbTransItemView.NCBAccountStatusGUID = ncbTrans.NCBAccountStatusGUID.GuidNullToString();
				ncbTransItemView.OutstandingAR = ncbTrans.OutstandingAR;
				ncbTransItemView.RefGUID = ncbTrans.RefGUID.GuidNullToString();
				ncbTransItemView.RefType = ncbTrans.RefType;
				
				ncbTransItemView.RowVersion = ncbTrans.RowVersion;
				return ncbTransItemView.GetNCBTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion NCBTransItemView
		#region ToDropDown
		public static SelectItem<NCBTransItemView> ToDropDownItem(this NCBTransItemView ncbTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<NCBTransItemView> selectItem = new SelectItem<NCBTransItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(ncbTransView.CreditTypeGUID.ToString(), ncbTransView.CreditLimit.ToString());
				selectItem.Value = ncbTransView.NCBTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: ncbTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<NCBTransItemView>> ToDropDownItem(this IEnumerable<NCBTransItemView> ncbTransItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<NCBTransItemView>> selectItems = new List<SelectItem<NCBTransItemView>>();
				foreach (NCBTransItemView item in ncbTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

