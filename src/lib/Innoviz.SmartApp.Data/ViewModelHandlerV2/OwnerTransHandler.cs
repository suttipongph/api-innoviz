using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class OwnerTransHandler
	{
		#region OwnerTransListView
		public static List<OwnerTransListView> GetOwnerTransListViewValidation(this List<OwnerTransListView> ownerTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return ownerTransListViews;
			}
			var result = new List<OwnerTransListView>();
			try
			{
				foreach (OwnerTransListView item in ownerTransListViews)
				{
					result.Add(item.GetOwnerTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static OwnerTransListView GetOwnerTransListViewValidation(this OwnerTransListView ownerTransListView)
		{
			try
			{
				ownerTransListView.RowAuthorize = ownerTransListView.GetListRowAuthorize();
				return ownerTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this OwnerTransListView ownerTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion OwnerTransListView
		#region OwnerTransItemView
		public static OwnerTransItemView GetOwnerTransItemViewValidation(this OwnerTransItemView ownerTransItemView)
		{
			try
			{
				ownerTransItemView.RowAuthorize = ownerTransItemView.GetItemRowAuthorize();
				return ownerTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this OwnerTransItemView ownerTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static OwnerTrans ToOwnerTrans(this OwnerTransItemView ownerTransItemView)
		{
			try
			{
				OwnerTrans ownerTrans = new OwnerTrans();
				ownerTrans.CompanyGUID = ownerTransItemView.CompanyGUID.StringToGuid();
				ownerTrans.CreatedBy = ownerTransItemView.CreatedBy;
				ownerTrans.CreatedDateTime = ownerTransItemView.CreatedDateTime.StringToSystemDateTime();
				ownerTrans.ModifiedBy = ownerTransItemView.ModifiedBy;
				ownerTrans.ModifiedDateTime = ownerTransItemView.ModifiedDateTime.StringToSystemDateTime();
				ownerTrans.Owner = ownerTransItemView.Owner;
				ownerTrans.OwnerBusinessUnitGUID = ownerTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				ownerTrans.OwnerTransGUID = ownerTransItemView.OwnerTransGUID.StringToGuid();
				ownerTrans.InActive = ownerTransItemView.InActive;
				ownerTrans.Ordering = ownerTransItemView.Ordering;
				ownerTrans.PropotionOfShareholderPct = ownerTransItemView.PropotionOfShareholderPct;
				ownerTrans.RefGUID = ownerTransItemView.RefGUID.StringToGuid();
				ownerTrans.RefType = ownerTransItemView.RefType;
				ownerTrans.RelatedPersonTableGUID = ownerTransItemView.RelatedPersonTableGUID.StringToGuid();
				
				ownerTrans.RowVersion = ownerTransItemView.RowVersion;
				return ownerTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<OwnerTrans> ToOwnerTrans(this IEnumerable<OwnerTransItemView> ownerTransItemViews)
		{
			try
			{
				List<OwnerTrans> ownerTranss = new List<OwnerTrans>();
				foreach (OwnerTransItemView item in ownerTransItemViews)
				{
					ownerTranss.Add(item.ToOwnerTrans());
				}
				return ownerTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static OwnerTransItemView ToOwnerTransItemView(this OwnerTrans ownerTrans)
		{
			try
			{
				OwnerTransItemView ownerTransItemView = new OwnerTransItemView();
				ownerTransItemView.CompanyGUID = ownerTrans.CompanyGUID.GuidNullToString();
				ownerTransItemView.CreatedBy = ownerTrans.CreatedBy;
				ownerTransItemView.CreatedDateTime = ownerTrans.CreatedDateTime.DateTimeToString();
				ownerTransItemView.ModifiedBy = ownerTrans.ModifiedBy;
				ownerTransItemView.ModifiedDateTime = ownerTrans.ModifiedDateTime.DateTimeToString();
				ownerTransItemView.Owner = ownerTrans.Owner;
				ownerTransItemView.OwnerBusinessUnitGUID = ownerTrans.OwnerBusinessUnitGUID.GuidNullToString();
				ownerTransItemView.OwnerTransGUID = ownerTrans.OwnerTransGUID.GuidNullToString();
				ownerTransItemView.InActive = ownerTrans.InActive;
				ownerTransItemView.Ordering = ownerTrans.Ordering;
				ownerTransItemView.PropotionOfShareholderPct = ownerTrans.PropotionOfShareholderPct;
				ownerTransItemView.RefGUID = ownerTrans.RefGUID.GuidNullToString();
				ownerTransItemView.RefType = ownerTrans.RefType;
				ownerTransItemView.RelatedPersonTableGUID = ownerTrans.RelatedPersonTableGUID.GuidNullToString();
				
				ownerTransItemView.RowVersion = ownerTrans.RowVersion;
				return ownerTransItemView.GetOwnerTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion OwnerTransItemView
		#region ToDropDown
		public static SelectItem<OwnerTransItemView> ToDropDownItem(this OwnerTransItemView ownerTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<OwnerTransItemView> selectItem = new SelectItem<OwnerTransItemView>();
				selectItem.Label = null;
				selectItem.Value = ownerTransView.OwnerTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: ownerTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<OwnerTransItemView>> ToDropDownItem(this IEnumerable<OwnerTransItemView> ownerTransItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<OwnerTransItemView>> selectItems = new List<SelectItem<OwnerTransItemView>>();
				foreach (OwnerTransItemView item in ownerTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

