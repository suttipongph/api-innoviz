using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class BookmarkDocumentHandler
	{
		#region BookmarkDocumentListView
		public static List<BookmarkDocumentListView> GetBookmarkDocumentListViewValidation(this List<BookmarkDocumentListView> bookmarkDocumentListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return bookmarkDocumentListViews;
			}
			var result = new List<BookmarkDocumentListView>();
			try
			{
				foreach (BookmarkDocumentListView item in bookmarkDocumentListViews)
				{
					result.Add(item.GetBookmarkDocumentListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BookmarkDocumentListView GetBookmarkDocumentListViewValidation(this BookmarkDocumentListView bookmarkDocumentListView)
		{
			try
			{
				bookmarkDocumentListView.RowAuthorize = bookmarkDocumentListView.GetListRowAuthorize();
				return bookmarkDocumentListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this BookmarkDocumentListView bookmarkDocumentListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion BookmarkDocumentListView
		#region BookmarkDocumentItemView
		public static BookmarkDocumentItemView GetBookmarkDocumentItemViewValidation(this BookmarkDocumentItemView bookmarkDocumentItemView)
		{
			try
			{
				bookmarkDocumentItemView.RowAuthorize = bookmarkDocumentItemView.GetItemRowAuthorize();
				return bookmarkDocumentItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this BookmarkDocumentItemView bookmarkDocumentItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static BookmarkDocument ToBookmarkDocument(this BookmarkDocumentItemView bookmarkDocumentItemView)
		{
			try
			{
				BookmarkDocument bookmarkDocument = new BookmarkDocument();
				bookmarkDocument.CompanyGUID = bookmarkDocumentItemView.CompanyGUID.StringToGuid();
				bookmarkDocument.CreatedBy = bookmarkDocumentItemView.CreatedBy;
				bookmarkDocument.CreatedDateTime = bookmarkDocumentItemView.CreatedDateTime.StringToSystemDateTime();
				bookmarkDocument.ModifiedBy = bookmarkDocumentItemView.ModifiedBy;
				bookmarkDocument.ModifiedDateTime = bookmarkDocumentItemView.ModifiedDateTime.StringToSystemDateTime();
				bookmarkDocument.Owner = bookmarkDocumentItemView.Owner;
				bookmarkDocument.OwnerBusinessUnitGUID = bookmarkDocumentItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				bookmarkDocument.BookmarkDocumentGUID = bookmarkDocumentItemView.BookmarkDocumentGUID.StringToGuid();
				bookmarkDocument.BookmarkDocumentId = bookmarkDocumentItemView.BookmarkDocumentId;
				bookmarkDocument.BookmarkDocumentRefType = bookmarkDocumentItemView.BookmarkDocumentRefType;
				bookmarkDocument.Description = bookmarkDocumentItemView.Description;
				bookmarkDocument.DocumentTemplateType = bookmarkDocumentItemView.DocumentTemplateType;
				
				bookmarkDocument.RowVersion = bookmarkDocumentItemView.RowVersion;
				return bookmarkDocument;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<BookmarkDocument> ToBookmarkDocument(this IEnumerable<BookmarkDocumentItemView> bookmarkDocumentItemViews)
		{
			try
			{
				List<BookmarkDocument> bookmarkDocuments = new List<BookmarkDocument>();
				foreach (BookmarkDocumentItemView item in bookmarkDocumentItemViews)
				{
					bookmarkDocuments.Add(item.ToBookmarkDocument());
				}
				return bookmarkDocuments;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BookmarkDocumentItemView ToBookmarkDocumentItemView(this BookmarkDocument bookmarkDocument)
		{
			try
			{
				BookmarkDocumentItemView bookmarkDocumentItemView = new BookmarkDocumentItemView();
				bookmarkDocumentItemView.CompanyGUID = bookmarkDocument.CompanyGUID.GuidNullToString();
				bookmarkDocumentItemView.CreatedBy = bookmarkDocument.CreatedBy;
				bookmarkDocumentItemView.CreatedDateTime = bookmarkDocument.CreatedDateTime.DateTimeToString();
				bookmarkDocumentItemView.ModifiedBy = bookmarkDocument.ModifiedBy;
				bookmarkDocumentItemView.ModifiedDateTime = bookmarkDocument.ModifiedDateTime.DateTimeToString();
				bookmarkDocumentItemView.Owner = bookmarkDocument.Owner;
				bookmarkDocumentItemView.OwnerBusinessUnitGUID = bookmarkDocument.OwnerBusinessUnitGUID.GuidNullToString();
				bookmarkDocumentItemView.BookmarkDocumentGUID = bookmarkDocument.BookmarkDocumentGUID.GuidNullToString();
				bookmarkDocumentItemView.BookmarkDocumentId = bookmarkDocument.BookmarkDocumentId;
				bookmarkDocumentItemView.BookmarkDocumentRefType = bookmarkDocument.BookmarkDocumentRefType;
				bookmarkDocumentItemView.Description = bookmarkDocument.Description;
				bookmarkDocumentItemView.DocumentTemplateType = bookmarkDocument.DocumentTemplateType;
				
				bookmarkDocumentItemView.RowVersion = bookmarkDocument.RowVersion;
				return bookmarkDocumentItemView.GetBookmarkDocumentItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion BookmarkDocumentItemView
		#region ToDropDown
		public static SelectItem<BookmarkDocumentItemView> ToDropDownItem(this BookmarkDocumentItemView bookmarkDocumentView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<BookmarkDocumentItemView> selectItem = new SelectItem<BookmarkDocumentItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(bookmarkDocumentView.BookmarkDocumentId, bookmarkDocumentView.Description);
				selectItem.Value = bookmarkDocumentView.BookmarkDocumentGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: bookmarkDocumentView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<BookmarkDocumentItemView>> ToDropDownItem(this IEnumerable<BookmarkDocumentItemView> bookmarkDocumentItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<BookmarkDocumentItemView>> selectItems = new List<SelectItem<BookmarkDocumentItemView>>();
				foreach (BookmarkDocumentItemView item in bookmarkDocumentItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

