using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class BusinessTypeHandler
	{
		#region BusinessTypeListView
		public static List<BusinessTypeListView> GetBusinessTypeListViewValidation(this List<BusinessTypeListView> businessTypeListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return businessTypeListViews;
			}
			var result = new List<BusinessTypeListView>();
			try
			{
				foreach (BusinessTypeListView item in businessTypeListViews)
				{
					result.Add(item.GetBusinessTypeListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BusinessTypeListView GetBusinessTypeListViewValidation(this BusinessTypeListView businessTypeListView)
		{
			try
			{
				businessTypeListView.RowAuthorize = businessTypeListView.GetListRowAuthorize();
				return businessTypeListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this BusinessTypeListView businessTypeListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion BusinessTypeListView
		#region BusinessTypeItemView
		public static BusinessTypeItemView GetBusinessTypeItemViewValidation(this BusinessTypeItemView businessTypeItemView)
		{
			try
			{
				businessTypeItemView.RowAuthorize = businessTypeItemView.GetItemRowAuthorize();
				return businessTypeItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this BusinessTypeItemView businessTypeItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static BusinessType ToBusinessType(this BusinessTypeItemView businessTypeItemView)
		{
			try
			{
				BusinessType businessType = new BusinessType();
				businessType.CompanyGUID = businessTypeItemView.CompanyGUID.StringToGuid();
				businessType.CreatedBy = businessTypeItemView.CreatedBy;
				businessType.CreatedDateTime = businessTypeItemView.CreatedDateTime.StringToSystemDateTime();
				businessType.ModifiedBy = businessTypeItemView.ModifiedBy;
				businessType.ModifiedDateTime = businessTypeItemView.ModifiedDateTime.StringToSystemDateTime();
				businessType.Owner = businessTypeItemView.Owner;
				businessType.OwnerBusinessUnitGUID = businessTypeItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				businessType.BusinessTypeGUID = businessTypeItemView.BusinessTypeGUID.StringToGuid();
				businessType.BusinessTypeId = businessTypeItemView.BusinessTypeId;
				businessType.Description = businessTypeItemView.Description;
				
				businessType.RowVersion = businessTypeItemView.RowVersion;
				return businessType;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<BusinessType> ToBusinessType(this IEnumerable<BusinessTypeItemView> businessTypeItemViews)
		{
			try
			{
				List<BusinessType> businessTypes = new List<BusinessType>();
				foreach (BusinessTypeItemView item in businessTypeItemViews)
				{
					businessTypes.Add(item.ToBusinessType());
				}
				return businessTypes;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BusinessTypeItemView ToBusinessTypeItemView(this BusinessType businessType)
		{
			try
			{
				BusinessTypeItemView businessTypeItemView = new BusinessTypeItemView();
				businessTypeItemView.CompanyGUID = businessType.CompanyGUID.GuidNullToString();
				businessTypeItemView.CreatedBy = businessType.CreatedBy;
				businessTypeItemView.CreatedDateTime = businessType.CreatedDateTime.DateTimeToString();
				businessTypeItemView.ModifiedBy = businessType.ModifiedBy;
				businessTypeItemView.ModifiedDateTime = businessType.ModifiedDateTime.DateTimeToString();
				businessTypeItemView.Owner = businessType.Owner;
				businessTypeItemView.OwnerBusinessUnitGUID = businessType.OwnerBusinessUnitGUID.GuidNullToString();
				businessTypeItemView.BusinessTypeGUID = businessType.BusinessTypeGUID.GuidNullToString();
				businessTypeItemView.BusinessTypeId = businessType.BusinessTypeId;
				businessTypeItemView.Description = businessType.Description;
				
				businessTypeItemView.RowVersion = businessType.RowVersion;
				return businessTypeItemView.GetBusinessTypeItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion BusinessTypeItemView
		#region ToDropDown
		public static SelectItem<BusinessTypeItemView> ToDropDownItem(this BusinessTypeItemView businessTypeView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<BusinessTypeItemView> selectItem = new SelectItem<BusinessTypeItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(businessTypeView.BusinessTypeId, businessTypeView.Description);
				selectItem.Value = businessTypeView.BusinessTypeGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: businessTypeView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<BusinessTypeItemView>> ToDropDownItem(this IEnumerable<BusinessTypeItemView> businessTypeItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<BusinessTypeItemView>> selectItems = new List<SelectItem<BusinessTypeItemView>>();
				foreach (BusinessTypeItemView item in businessTypeItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

