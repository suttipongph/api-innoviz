using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CAReqBuyerCreditOutstandingHandler
	{
		#region CAReqBuyerCreditOutstandingListView
		public static List<CAReqBuyerCreditOutstandingListView> GetCAReqBuyerCreditOutstandingListViewValidation(this List<CAReqBuyerCreditOutstandingListView> caReqBuyerCreditOutstandingListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return caReqBuyerCreditOutstandingListViews;
			}
			var result = new List<CAReqBuyerCreditOutstandingListView>();
			try
			{
				foreach (CAReqBuyerCreditOutstandingListView item in caReqBuyerCreditOutstandingListViews)
				{
					result.Add(item.GetCAReqBuyerCreditOutstandingListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CAReqBuyerCreditOutstandingListView GetCAReqBuyerCreditOutstandingListViewValidation(this CAReqBuyerCreditOutstandingListView caReqBuyerCreditOutstandingListView)
		{
			try
			{
				caReqBuyerCreditOutstandingListView.RowAuthorize = caReqBuyerCreditOutstandingListView.GetListRowAuthorize();
				return caReqBuyerCreditOutstandingListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CAReqBuyerCreditOutstandingListView caReqBuyerCreditOutstandingListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CAReqBuyerCreditOutstandingListView
		#region CAReqBuyerCreditOutstandingItemView
		public static CAReqBuyerCreditOutstandingItemView GetCAReqBuyerCreditOutstandingItemViewValidation(this CAReqBuyerCreditOutstandingItemView caReqBuyerCreditOutstandingItemView)
		{
			try
			{
				caReqBuyerCreditOutstandingItemView.RowAuthorize = caReqBuyerCreditOutstandingItemView.GetItemRowAuthorize();
				return caReqBuyerCreditOutstandingItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CAReqBuyerCreditOutstandingItemView caReqBuyerCreditOutstandingItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CAReqBuyerCreditOutstanding ToCAReqBuyerCreditOutstanding(this CAReqBuyerCreditOutstandingItemView caReqBuyerCreditOutstandingItemView)
		{
			try
			{
				CAReqBuyerCreditOutstanding caReqBuyerCreditOutstanding = new CAReqBuyerCreditOutstanding();
				caReqBuyerCreditOutstanding.CompanyGUID = caReqBuyerCreditOutstandingItemView.CompanyGUID.StringToGuid();
				caReqBuyerCreditOutstanding.CreatedBy = caReqBuyerCreditOutstandingItemView.CreatedBy;
				caReqBuyerCreditOutstanding.CreatedDateTime = caReqBuyerCreditOutstandingItemView.CreatedDateTime.StringToSystemDateTime();
				caReqBuyerCreditOutstanding.ModifiedBy = caReqBuyerCreditOutstandingItemView.ModifiedBy;
				caReqBuyerCreditOutstanding.ModifiedDateTime = caReqBuyerCreditOutstandingItemView.ModifiedDateTime.StringToSystemDateTime();
				caReqBuyerCreditOutstanding.Owner = caReqBuyerCreditOutstandingItemView.Owner;
				caReqBuyerCreditOutstanding.OwnerBusinessUnitGUID = caReqBuyerCreditOutstandingItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				caReqBuyerCreditOutstanding.CAReqBuyerCreditOutstandingGUID = caReqBuyerCreditOutstandingItemView.CAReqBuyerCreditOutstandingGUID.StringToGuid();
				caReqBuyerCreditOutstanding.ApprovedCreditLimitLine = caReqBuyerCreditOutstandingItemView.ApprovedCreditLimitLine;
				caReqBuyerCreditOutstanding.ARBalance = caReqBuyerCreditOutstandingItemView.ARBalance;
				caReqBuyerCreditOutstanding.AssignmentMethodGUID = caReqBuyerCreditOutstandingItemView.AssignmentMethodGUID.StringToGuidNull();
				caReqBuyerCreditOutstanding.BillingResponsibleByGUID = caReqBuyerCreditOutstandingItemView.BillingResponsibleByGUID.StringToGuidNull();
				caReqBuyerCreditOutstanding.BuyerTableGUID = caReqBuyerCreditOutstandingItemView.BuyerTableGUID.StringToGuidNull();
				caReqBuyerCreditOutstanding.CreditAppLineGUID = caReqBuyerCreditOutstandingItemView.CreditAppLineGUID.StringToGuidNull();
				caReqBuyerCreditOutstanding.CreditAppRequestTableGUID = caReqBuyerCreditOutstandingItemView.CreditAppRequestTableGUID.StringToGuidNull();
				caReqBuyerCreditOutstanding.CreditAppTableGUID = caReqBuyerCreditOutstandingItemView.CreditAppTableGUID.StringToGuidNull();
				caReqBuyerCreditOutstanding.LineNum = caReqBuyerCreditOutstandingItemView.LineNum;
				caReqBuyerCreditOutstanding.MaxPurchasePct = caReqBuyerCreditOutstandingItemView.MaxPurchasePct;
				caReqBuyerCreditOutstanding.MethodOfPaymentGUID = caReqBuyerCreditOutstandingItemView.MethodOfPaymentGUID.StringToGuidNull();
				caReqBuyerCreditOutstanding.ProductType = caReqBuyerCreditOutstandingItemView.ProductType;
				caReqBuyerCreditOutstanding.Status = caReqBuyerCreditOutstandingItemView.Status;
				
				caReqBuyerCreditOutstanding.RowVersion = caReqBuyerCreditOutstandingItemView.RowVersion;
				return caReqBuyerCreditOutstanding;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CAReqBuyerCreditOutstanding> ToCAReqBuyerCreditOutstanding(this IEnumerable<CAReqBuyerCreditOutstandingItemView> caReqBuyerCreditOutstandingItemViews)
		{
			try
			{
				List<CAReqBuyerCreditOutstanding> caReqBuyerCreditOutstandings = new List<CAReqBuyerCreditOutstanding>();
				foreach (CAReqBuyerCreditOutstandingItemView item in caReqBuyerCreditOutstandingItemViews)
				{
					caReqBuyerCreditOutstandings.Add(item.ToCAReqBuyerCreditOutstanding());
				}
				return caReqBuyerCreditOutstandings;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CAReqBuyerCreditOutstandingItemView ToCAReqBuyerCreditOutstandingItemView(this CAReqBuyerCreditOutstanding caReqBuyerCreditOutstanding)
		{
			try
			{
				CAReqBuyerCreditOutstandingItemView caReqBuyerCreditOutstandingItemView = new CAReqBuyerCreditOutstandingItemView();
				caReqBuyerCreditOutstandingItemView.CompanyGUID = caReqBuyerCreditOutstanding.CompanyGUID.GuidNullToString();
				caReqBuyerCreditOutstandingItemView.CreatedBy = caReqBuyerCreditOutstanding.CreatedBy;
				caReqBuyerCreditOutstandingItemView.CreatedDateTime = caReqBuyerCreditOutstanding.CreatedDateTime.DateTimeToString();
				caReqBuyerCreditOutstandingItemView.ModifiedBy = caReqBuyerCreditOutstanding.ModifiedBy;
				caReqBuyerCreditOutstandingItemView.ModifiedDateTime = caReqBuyerCreditOutstanding.ModifiedDateTime.DateTimeToString();
				caReqBuyerCreditOutstandingItemView.Owner = caReqBuyerCreditOutstanding.Owner;
				caReqBuyerCreditOutstandingItemView.OwnerBusinessUnitGUID = caReqBuyerCreditOutstanding.OwnerBusinessUnitGUID.GuidNullToString();
				caReqBuyerCreditOutstandingItemView.CAReqBuyerCreditOutstandingGUID = caReqBuyerCreditOutstanding.CAReqBuyerCreditOutstandingGUID.GuidNullToString();
				caReqBuyerCreditOutstandingItemView.ApprovedCreditLimitLine = caReqBuyerCreditOutstanding.ApprovedCreditLimitLine;
				caReqBuyerCreditOutstandingItemView.ARBalance = caReqBuyerCreditOutstanding.ARBalance;
				caReqBuyerCreditOutstandingItemView.AssignmentMethodGUID = caReqBuyerCreditOutstanding.AssignmentMethodGUID.GuidNullToString();
				caReqBuyerCreditOutstandingItemView.BillingResponsibleByGUID = caReqBuyerCreditOutstanding.BillingResponsibleByGUID.GuidNullToString();
				caReqBuyerCreditOutstandingItemView.BuyerTableGUID = caReqBuyerCreditOutstanding.BuyerTableGUID.GuidNullToString();
				caReqBuyerCreditOutstandingItemView.CreditAppLineGUID = caReqBuyerCreditOutstanding.CreditAppLineGUID.GuidNullToString();
				caReqBuyerCreditOutstandingItemView.CreditAppRequestTableGUID = caReqBuyerCreditOutstanding.CreditAppRequestTableGUID.GuidNullToString();
				caReqBuyerCreditOutstandingItemView.CreditAppTableGUID = caReqBuyerCreditOutstanding.CreditAppTableGUID.GuidNullToString();
				caReqBuyerCreditOutstandingItemView.LineNum = caReqBuyerCreditOutstanding.LineNum;
				caReqBuyerCreditOutstandingItemView.MaxPurchasePct = caReqBuyerCreditOutstanding.MaxPurchasePct;
				caReqBuyerCreditOutstandingItemView.MethodOfPaymentGUID = caReqBuyerCreditOutstanding.MethodOfPaymentGUID.GuidNullToString();
				caReqBuyerCreditOutstandingItemView.ProductType = caReqBuyerCreditOutstanding.ProductType;
				caReqBuyerCreditOutstandingItemView.Status = caReqBuyerCreditOutstanding.Status;
				
				caReqBuyerCreditOutstandingItemView.RowVersion = caReqBuyerCreditOutstanding.RowVersion;
				return caReqBuyerCreditOutstandingItemView.GetCAReqBuyerCreditOutstandingItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CAReqBuyerCreditOutstandingItemView
		#region ToDropDown
		public static SelectItem<CAReqBuyerCreditOutstandingItemView> ToDropDownItem(this CAReqBuyerCreditOutstandingItemView caReqBuyerCreditOutstandingView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CAReqBuyerCreditOutstandingItemView> selectItem = new SelectItem<CAReqBuyerCreditOutstandingItemView>();
				selectItem.Label = null;
				selectItem.Value = caReqBuyerCreditOutstandingView.CAReqBuyerCreditOutstandingGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: caReqBuyerCreditOutstandingView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CAReqBuyerCreditOutstandingItemView>> ToDropDownItem(this IEnumerable<CAReqBuyerCreditOutstandingItemView> caReqBuyerCreditOutstandingItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CAReqBuyerCreditOutstandingItemView>> selectItems = new List<SelectItem<CAReqBuyerCreditOutstandingItemView>>();
				foreach (CAReqBuyerCreditOutstandingItemView item in caReqBuyerCreditOutstandingItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

