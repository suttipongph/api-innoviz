using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CreditAppRequestTableHandler
	{
		#region CreditAppRequestTableListView
		public static List<CreditAppRequestTableListView> GetCreditAppRequestTableListViewValidation(this List<CreditAppRequestTableListView> creditAppRequestTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return creditAppRequestTableListViews;
			}
			var result = new List<CreditAppRequestTableListView>();
			try
			{
				foreach (CreditAppRequestTableListView item in creditAppRequestTableListViews)
				{
					result.Add(item.GetCreditAppRequestTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CreditAppRequestTableListView GetCreditAppRequestTableListViewValidation(this CreditAppRequestTableListView creditAppRequestTableListView)
		{
			try
			{
				creditAppRequestTableListView.RowAuthorize = creditAppRequestTableListView.GetListRowAuthorize();
				return creditAppRequestTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CreditAppRequestTableListView creditAppRequestTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CreditAppRequestTableListView
		#region CreditAppRequestTableItemView
		public static CreditAppRequestTableItemView GetCreditAppRequestTableItemViewValidation(this CreditAppRequestTableItemView creditAppRequestTableItemView)
		{
			try
			{
				creditAppRequestTableItemView.RowAuthorize = creditAppRequestTableItemView.GetItemRowAuthorize();
				return creditAppRequestTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CreditAppRequestTableItemView creditAppRequestTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CreditAppRequestTable ToCreditAppRequestTable(this CreditAppRequestTableItemView creditAppRequestTableItemView)
		{
			try
			{
				CreditAppRequestTable creditAppRequestTable = new CreditAppRequestTable();
				creditAppRequestTable.CompanyGUID = creditAppRequestTableItemView.CompanyGUID.StringToGuid();
				creditAppRequestTable.CreatedBy = creditAppRequestTableItemView.CreatedBy;
				creditAppRequestTable.CreatedDateTime = creditAppRequestTableItemView.CreatedDateTime.StringToSystemDateTime();
				creditAppRequestTable.ModifiedBy = creditAppRequestTableItemView.ModifiedBy;
				creditAppRequestTable.ModifiedDateTime = creditAppRequestTableItemView.ModifiedDateTime.StringToSystemDateTime();
				creditAppRequestTable.Owner = creditAppRequestTableItemView.Owner;
				creditAppRequestTable.OwnerBusinessUnitGUID = creditAppRequestTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				creditAppRequestTable.CreditAppRequestTableGUID = creditAppRequestTableItemView.CreditAppRequestTableGUID.StringToGuid();
				creditAppRequestTable.ApplicationTableGUID = creditAppRequestTableItemView.ApplicationTableGUID.StringToGuidNull();
				creditAppRequestTable.ApprovedCreditLimitRequest = creditAppRequestTableItemView.ApprovedCreditLimitRequest;
				creditAppRequestTable.ApproverComment = creditAppRequestTableItemView.ApproverComment;
				creditAppRequestTable.BankAccountControlGUID = creditAppRequestTableItemView.BankAccountControlGUID.StringToGuidNull();
				creditAppRequestTable.BillingAddressGUID = creditAppRequestTableItemView.BillingAddressGUID.StringToGuidNull();
				creditAppRequestTable.BillingContactPersonGUID = creditAppRequestTableItemView.BillingContactPersonGUID.StringToGuidNull();
				creditAppRequestTable.BlacklistStatusGUID = creditAppRequestTableItemView.BlacklistStatusGUID.StringToGuidNull();
				creditAppRequestTable.CACondition = creditAppRequestTableItemView.CACondition;
				creditAppRequestTable.ConsortiumTableGUID = creditAppRequestTableItemView.ConsortiumTableGUID.StringToGuidNull();
				creditAppRequestTable.CreditAppRequestId = creditAppRequestTableItemView.CreditAppRequestId;
				creditAppRequestTable.CreditAppRequestType = creditAppRequestTableItemView.CreditAppRequestType;
				creditAppRequestTable.CreditComment = creditAppRequestTableItemView.CreditComment;
				creditAppRequestTable.CreditLimitExpiration = creditAppRequestTableItemView.CreditLimitExpiration;
				creditAppRequestTable.CreditLimitRemark = creditAppRequestTableItemView.CreditLimitRemark;
				creditAppRequestTable.CreditLimitRequest = creditAppRequestTableItemView.CreditLimitRequest;
				creditAppRequestTable.CreditLimitTypeGUID = creditAppRequestTableItemView.CreditLimitTypeGUID.StringToGuid();
				creditAppRequestTable.CreditRequestFeeAmount = creditAppRequestTableItemView.CreditRequestFeeAmount;
				creditAppRequestTable.CreditRequestFeePct = creditAppRequestTableItemView.CreditRequestFeePct;
				creditAppRequestTable.CreditScoringGUID = creditAppRequestTableItemView.CreditScoringGUID.StringToGuidNull();
				creditAppRequestTable.CreditTermGUID = creditAppRequestTableItemView.CreditTermGUID.StringToGuidNull();
				creditAppRequestTable.CustomerCreditLimit = creditAppRequestTableItemView.CustomerCreditLimit;
				creditAppRequestTable.CustomerTableGUID = creditAppRequestTableItemView.CustomerTableGUID.StringToGuid();
				creditAppRequestTable.Description = creditAppRequestTableItemView.Description;
				creditAppRequestTable.Dimension1GUID = creditAppRequestTableItemView.Dimension1GUID.StringToGuidNull();
				creditAppRequestTable.Dimension2GUID = creditAppRequestTableItemView.Dimension2GUID.StringToGuidNull();
				creditAppRequestTable.Dimension3GUID = creditAppRequestTableItemView.Dimension3GUID.StringToGuidNull();
				creditAppRequestTable.Dimension4GUID = creditAppRequestTableItemView.Dimension4GUID.StringToGuidNull();
				creditAppRequestTable.Dimension5GUID = creditAppRequestTableItemView.Dimension5GUID.StringToGuidNull();
				creditAppRequestTable.DocumentReasonGUID = creditAppRequestTableItemView.DocumentReasonGUID.StringToGuidNull();
				creditAppRequestTable.DocumentStatusGUID = creditAppRequestTableItemView.DocumentStatusGUID.StringToGuid();
				creditAppRequestTable.ExpiryDate = creditAppRequestTableItemView.ExpiryDate.StringNullToDateNull();
				creditAppRequestTable.ExtensionServiceFeeCondTemplateGUID = creditAppRequestTableItemView.ExtensionServiceFeeCondTemplateGUID.StringToGuidNull();
				creditAppRequestTable.FinancialCheckedBy = creditAppRequestTableItemView.FinancialCheckedBy;
				creditAppRequestTable.FinancialCreditCheckedDate = creditAppRequestTableItemView.FinancialCreditCheckedDate.StringNullToDateNull();
				creditAppRequestTable.InterestAdjustment = creditAppRequestTableItemView.InterestAdjustment;
				creditAppRequestTable.InterestTypeGUID = creditAppRequestTableItemView.InterestTypeGUID.StringToGuid();
				creditAppRequestTable.InvoiceAddressGUID = creditAppRequestTableItemView.InvoiceAddressGUID.StringToGuidNull();
				creditAppRequestTable.KYCGUID = creditAppRequestTableItemView.KYCGUID.StringToGuidNull();
				creditAppRequestTable.MailingReceiptAddressGUID = creditAppRequestTableItemView.MailingReceiptAddressGUID.StringToGuidNull();
				creditAppRequestTable.MarketingComment = creditAppRequestTableItemView.MarketingComment;
				creditAppRequestTable.MaxPurchasePct = creditAppRequestTableItemView.MaxPurchasePct;
				creditAppRequestTable.MaxRetentionAmount = creditAppRequestTableItemView.MaxRetentionAmount;
				creditAppRequestTable.MaxRetentionPct = creditAppRequestTableItemView.MaxRetentionPct;
				creditAppRequestTable.NCBCheckedBy = creditAppRequestTableItemView.NCBCheckedBy;
				creditAppRequestTable.NCBCheckedDate = creditAppRequestTableItemView.NCBCheckedDate.StringNullToDateNull();
				creditAppRequestTable.PDCBankGUID = creditAppRequestTableItemView.PDCBankGUID.StringToGuidNull();
				creditAppRequestTable.ProductSubTypeGUID = creditAppRequestTableItemView.ProductSubTypeGUID.StringToGuid();
				creditAppRequestTable.ProductType = creditAppRequestTableItemView.ProductType;
				creditAppRequestTable.PurchaseFeeCalculateBase = creditAppRequestTableItemView.PurchaseFeeCalculateBase;
				creditAppRequestTable.PurchaseFeePct = creditAppRequestTableItemView.PurchaseFeePct;
				creditAppRequestTable.ReceiptAddressGUID = creditAppRequestTableItemView.ReceiptAddressGUID.StringToGuidNull();
				creditAppRequestTable.ReceiptContactPersonGUID = creditAppRequestTableItemView.ReceiptContactPersonGUID.StringToGuidNull();
				creditAppRequestTable.RefCreditAppTableGUID = creditAppRequestTableItemView.RefCreditAppTableGUID.StringToGuidNull();
				creditAppRequestTable.RegisteredAddressGUID = creditAppRequestTableItemView.RegisteredAddressGUID.StringToGuid();
				creditAppRequestTable.Remark = creditAppRequestTableItemView.Remark;
				creditAppRequestTable.RequestDate = creditAppRequestTableItemView.RequestDate.StringToDate();
				creditAppRequestTable.SalesAvgPerMonth = creditAppRequestTableItemView.SalesAvgPerMonth;
				creditAppRequestTable.SigningCondition = creditAppRequestTableItemView.SigningCondition;
				creditAppRequestTable.StartDate = creditAppRequestTableItemView.StartDate.StringNullToDateNull();
				//R02
				creditAppRequestTable.CustomerAllBuyerOutstanding = creditAppRequestTableItemView.CustomerAllBuyerOutstanding;
				creditAppRequestTable.ProcessInstanceId = creditAppRequestTableItemView.ProcessInstanceId;
				creditAppRequestTable.ApprovedDate = creditAppRequestTableItemView.ApprovedDate.StringNullToDateNull();
				creditAppRequestTable.AssignmentMethodGUID = creditAppRequestTableItemView.AssignmentMethodGUID.StringToGuidNull();
				creditAppRequestTable.PurchaseWithdrawalCondition = creditAppRequestTableItemView.PurchaseWithdrawalCondition;

				creditAppRequestTable.RowVersion = creditAppRequestTableItemView.RowVersion;
				return creditAppRequestTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CreditAppRequestTable> ToCreditAppRequestTable(this IEnumerable<CreditAppRequestTableItemView> creditAppRequestTableItemViews)
		{
			try
			{
				List<CreditAppRequestTable> creditAppRequestTables = new List<CreditAppRequestTable>();
				foreach (CreditAppRequestTableItemView item in creditAppRequestTableItemViews)
				{
					creditAppRequestTables.Add(item.ToCreditAppRequestTable());
				}
				return creditAppRequestTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CreditAppRequestTableItemView ToCreditAppRequestTableItemView(this CreditAppRequestTable creditAppRequestTable)
		{
			try
			{
				CreditAppRequestTableItemView creditAppRequestTableItemView = new CreditAppRequestTableItemView();
				creditAppRequestTableItemView.CompanyGUID = creditAppRequestTable.CompanyGUID.GuidNullToString();
				creditAppRequestTableItemView.CreatedBy = creditAppRequestTable.CreatedBy;
				creditAppRequestTableItemView.CreatedDateTime = creditAppRequestTable.CreatedDateTime.DateTimeToString();
				creditAppRequestTableItemView.ModifiedBy = creditAppRequestTable.ModifiedBy;
				creditAppRequestTableItemView.ModifiedDateTime = creditAppRequestTable.ModifiedDateTime.DateTimeToString();
				creditAppRequestTableItemView.Owner = creditAppRequestTable.Owner;
				creditAppRequestTableItemView.OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID.GuidNullToString();
				creditAppRequestTableItemView.CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID.GuidNullToString();
				creditAppRequestTableItemView.ApplicationTableGUID = creditAppRequestTable.ApplicationTableGUID.GuidNullToString();
				creditAppRequestTableItemView.ApprovedCreditLimitRequest = creditAppRequestTable.ApprovedCreditLimitRequest;
				creditAppRequestTableItemView.ApproverComment = creditAppRequestTable.ApproverComment;
				creditAppRequestTableItemView.BankAccountControlGUID = creditAppRequestTable.BankAccountControlGUID.GuidNullToString();
				creditAppRequestTableItemView.BillingAddressGUID = creditAppRequestTable.BillingAddressGUID.GuidNullToString();
				creditAppRequestTableItemView.BillingContactPersonGUID = creditAppRequestTable.BillingContactPersonGUID.GuidNullToString();
				creditAppRequestTableItemView.BlacklistStatusGUID = creditAppRequestTable.BlacklistStatusGUID.GuidNullToString();
				creditAppRequestTableItemView.CACondition = creditAppRequestTable.CACondition;
				creditAppRequestTableItemView.ConsortiumTableGUID = creditAppRequestTable.ConsortiumTableGUID.GuidNullToString();
				creditAppRequestTableItemView.CreditAppRequestId = creditAppRequestTable.CreditAppRequestId;
				creditAppRequestTableItemView.CreditAppRequestType = creditAppRequestTable.CreditAppRequestType;
				creditAppRequestTableItemView.CreditComment = creditAppRequestTable.CreditComment;
				creditAppRequestTableItemView.CreditLimitExpiration = creditAppRequestTable.CreditLimitExpiration;
				creditAppRequestTableItemView.CreditLimitRemark = creditAppRequestTable.CreditLimitRemark;
				creditAppRequestTableItemView.CreditLimitRequest = creditAppRequestTable.CreditLimitRequest;
				creditAppRequestTableItemView.CreditLimitTypeGUID = creditAppRequestTable.CreditLimitTypeGUID.GuidNullToString();
				creditAppRequestTableItemView.CreditRequestFeeAmount = creditAppRequestTable.CreditRequestFeeAmount;
				creditAppRequestTableItemView.CreditRequestFeePct = creditAppRequestTable.CreditRequestFeePct;
				creditAppRequestTableItemView.CreditScoringGUID = creditAppRequestTable.CreditScoringGUID.GuidNullToString();
				creditAppRequestTableItemView.CreditTermGUID = creditAppRequestTable.CreditTermGUID.GuidNullToString();
				creditAppRequestTableItemView.CustomerCreditLimit = creditAppRequestTable.CustomerCreditLimit;
				creditAppRequestTableItemView.CustomerTableGUID = creditAppRequestTable.CustomerTableGUID.GuidNullToString();
				creditAppRequestTableItemView.Description = creditAppRequestTable.Description;
				creditAppRequestTableItemView.Dimension1GUID = creditAppRequestTable.Dimension1GUID.GuidNullToString();
				creditAppRequestTableItemView.Dimension2GUID = creditAppRequestTable.Dimension2GUID.GuidNullToString();
				creditAppRequestTableItemView.Dimension3GUID = creditAppRequestTable.Dimension3GUID.GuidNullToString();
				creditAppRequestTableItemView.Dimension4GUID = creditAppRequestTable.Dimension4GUID.GuidNullToString();
				creditAppRequestTableItemView.Dimension5GUID = creditAppRequestTable.Dimension5GUID.GuidNullToString();
				creditAppRequestTableItemView.DocumentReasonGUID = creditAppRequestTable.DocumentReasonGUID.GuidNullToString();
				creditAppRequestTableItemView.DocumentStatusGUID = creditAppRequestTable.DocumentStatusGUID.GuidNullToString();
				creditAppRequestTableItemView.ExpiryDate = creditAppRequestTable.ExpiryDate.DateNullToString();
				creditAppRequestTableItemView.ExtensionServiceFeeCondTemplateGUID = creditAppRequestTable.ExtensionServiceFeeCondTemplateGUID.GuidNullToString();
				creditAppRequestTableItemView.FinancialCheckedBy = creditAppRequestTable.FinancialCheckedBy;
				creditAppRequestTableItemView.FinancialCreditCheckedDate = creditAppRequestTable.FinancialCreditCheckedDate.DateNullToString();
				creditAppRequestTableItemView.InterestAdjustment = creditAppRequestTable.InterestAdjustment;
				creditAppRequestTableItemView.InterestTypeGUID = creditAppRequestTable.InterestTypeGUID.GuidNullToString();
				creditAppRequestTableItemView.InvoiceAddressGUID = creditAppRequestTable.InvoiceAddressGUID.GuidNullToString();
				creditAppRequestTableItemView.KYCGUID = creditAppRequestTable.KYCGUID.GuidNullToString();
				creditAppRequestTableItemView.MailingReceiptAddressGUID = creditAppRequestTable.MailingReceiptAddressGUID.GuidNullToString();
				creditAppRequestTableItemView.MarketingComment = creditAppRequestTable.MarketingComment;
				creditAppRequestTableItemView.MaxPurchasePct = creditAppRequestTable.MaxPurchasePct;
				creditAppRequestTableItemView.MaxRetentionAmount = creditAppRequestTable.MaxRetentionAmount;
				creditAppRequestTableItemView.MaxRetentionPct = creditAppRequestTable.MaxRetentionPct;
				creditAppRequestTableItemView.NCBCheckedBy = creditAppRequestTable.NCBCheckedBy;
				creditAppRequestTableItemView.NCBCheckedDate = creditAppRequestTable.NCBCheckedDate.DateNullToString();
				creditAppRequestTableItemView.PDCBankGUID = creditAppRequestTable.PDCBankGUID.GuidNullToString();
				creditAppRequestTableItemView.ProductSubTypeGUID = creditAppRequestTable.ProductSubTypeGUID.GuidNullToString();
				creditAppRequestTableItemView.ProductType = creditAppRequestTable.ProductType;
				creditAppRequestTableItemView.PurchaseFeeCalculateBase = creditAppRequestTable.PurchaseFeeCalculateBase;
				creditAppRequestTableItemView.PurchaseFeePct = creditAppRequestTable.PurchaseFeePct;
				creditAppRequestTableItemView.ReceiptAddressGUID = creditAppRequestTable.ReceiptAddressGUID.GuidNullToString();
				creditAppRequestTableItemView.ReceiptContactPersonGUID = creditAppRequestTable.ReceiptContactPersonGUID.GuidNullToString();
				creditAppRequestTableItemView.RefCreditAppTableGUID = creditAppRequestTable.RefCreditAppTableGUID.GuidNullToString();
				creditAppRequestTableItemView.RegisteredAddressGUID = creditAppRequestTable.RegisteredAddressGUID.GuidNullToString();
				creditAppRequestTableItemView.Remark = creditAppRequestTable.Remark;
				creditAppRequestTableItemView.RequestDate = creditAppRequestTable.RequestDate.DateToString();
				creditAppRequestTableItemView.SalesAvgPerMonth = creditAppRequestTable.SalesAvgPerMonth;
				creditAppRequestTableItemView.SigningCondition = creditAppRequestTable.SigningCondition;
				creditAppRequestTableItemView.StartDate = creditAppRequestTable.StartDate.DateNullToString();
				//R02
				creditAppRequestTableItemView.CustomerAllBuyerOutstanding = creditAppRequestTable.CustomerAllBuyerOutstanding;
				creditAppRequestTableItemView.ProcessInstanceId = creditAppRequestTable.ProcessInstanceId;
				creditAppRequestTableItemView.ApprovedDate = creditAppRequestTable.ApprovedDate.DateToString();
				creditAppRequestTableItemView.AssignmentMethodGUID = creditAppRequestTable.AssignmentMethodGUID.GuidNullToString();
				creditAppRequestTableItemView.PurchaseWithdrawalCondition = creditAppRequestTable.PurchaseWithdrawalCondition;

				creditAppRequestTableItemView.RowVersion = creditAppRequestTable.RowVersion;

				return creditAppRequestTableItemView.GetCreditAppRequestTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CreditAppRequestTableItemView
		#region ToDropDown
		public static SelectItem<CreditAppRequestTableItemView> ToDropDownItem(this CreditAppRequestTableItemView creditAppRequestTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CreditAppRequestTableItemView> selectItem = new SelectItem<CreditAppRequestTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(creditAppRequestTableView.CreditAppRequestId, creditAppRequestTableView.Description);
				selectItem.Value = creditAppRequestTableView.CreditAppRequestTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: creditAppRequestTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CreditAppRequestTableItemView>> ToDropDownItem(this IEnumerable<CreditAppRequestTableItemView> creditAppRequestTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CreditAppRequestTableItemView>> selectItems = new List<SelectItem<CreditAppRequestTableItemView>>();
				foreach (CreditAppRequestTableItemView item in creditAppRequestTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}
