using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class DocumentReturnLineHandler
	{
		#region DocumentReturnLineListView
		public static List<DocumentReturnLineListView> GetDocumentReturnLineListViewValidation(this List<DocumentReturnLineListView> documentReturnLineListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return documentReturnLineListViews;
			}
			var result = new List<DocumentReturnLineListView>();
			try
			{
				foreach (DocumentReturnLineListView item in documentReturnLineListViews)
				{
					result.Add(item.GetDocumentReturnLineListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static DocumentReturnLineListView GetDocumentReturnLineListViewValidation(this DocumentReturnLineListView documentReturnLineListView)
		{
			try
			{
				documentReturnLineListView.RowAuthorize = documentReturnLineListView.GetListRowAuthorize();
				return documentReturnLineListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this DocumentReturnLineListView documentReturnLineListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion DocumentReturnLineListView
		#region DocumentReturnLineItemView
		public static DocumentReturnLineItemView GetDocumentReturnLineItemViewValidation(this DocumentReturnLineItemView documentReturnLineItemView)
		{
			try
			{
				documentReturnLineItemView.RowAuthorize = documentReturnLineItemView.GetItemRowAuthorize();
				return documentReturnLineItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this DocumentReturnLineItemView documentReturnLineItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static DocumentReturnLine ToDocumentReturnLine(this DocumentReturnLineItemView documentReturnLineItemView)
		{
			try
			{
				DocumentReturnLine documentReturnLine = new DocumentReturnLine();
				documentReturnLine.CompanyGUID = documentReturnLineItemView.CompanyGUID.StringToGuid();
				documentReturnLine.CreatedBy = documentReturnLineItemView.CreatedBy;
				documentReturnLine.CreatedDateTime = documentReturnLineItemView.CreatedDateTime.StringToSystemDateTime();
				documentReturnLine.ModifiedBy = documentReturnLineItemView.ModifiedBy;
				documentReturnLine.ModifiedDateTime = documentReturnLineItemView.ModifiedDateTime.StringToSystemDateTime();
				documentReturnLine.Owner = documentReturnLineItemView.Owner;
				documentReturnLine.OwnerBusinessUnitGUID = documentReturnLineItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				documentReturnLine.DocumentReturnLineGUID = documentReturnLineItemView.DocumentReturnLineGUID.StringToGuid();
				documentReturnLine.Address = documentReturnLineItemView.Address;
				documentReturnLine.BuyerTableGUID = documentReturnLineItemView.BuyerTableGUID.StringToGuidNull();
				documentReturnLine.ContactPersonName = documentReturnLineItemView.ContactPersonName;
				documentReturnLine.DocumentNo = documentReturnLineItemView.DocumentNo;
				documentReturnLine.DocumentReturnTableGUID = documentReturnLineItemView.DocumentReturnTableGUID.StringToGuid();
				documentReturnLine.DocumentTypeGUID = documentReturnLineItemView.DocumentTypeGUID.StringToGuidNull();
				documentReturnLine.LineNum = documentReturnLineItemView.LineNum;
				documentReturnLine.Remark = documentReturnLineItemView.Remark;
				
				documentReturnLine.RowVersion = documentReturnLineItemView.RowVersion;
				return documentReturnLine;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<DocumentReturnLine> ToDocumentReturnLine(this IEnumerable<DocumentReturnLineItemView> documentReturnLineItemViews)
		{
			try
			{
				List<DocumentReturnLine> documentReturnLines = new List<DocumentReturnLine>();
				foreach (DocumentReturnLineItemView item in documentReturnLineItemViews)
				{
					documentReturnLines.Add(item.ToDocumentReturnLine());
				}
				return documentReturnLines;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static DocumentReturnLineItemView ToDocumentReturnLineItemView(this DocumentReturnLine documentReturnLine)
		{
			try
			{
				DocumentReturnLineItemView documentReturnLineItemView = new DocumentReturnLineItemView();
				documentReturnLineItemView.CompanyGUID = documentReturnLine.CompanyGUID.GuidNullToString();
				documentReturnLineItemView.CreatedBy = documentReturnLine.CreatedBy;
				documentReturnLineItemView.CreatedDateTime = documentReturnLine.CreatedDateTime.DateTimeToString();
				documentReturnLineItemView.ModifiedBy = documentReturnLine.ModifiedBy;
				documentReturnLineItemView.ModifiedDateTime = documentReturnLine.ModifiedDateTime.DateTimeToString();
				documentReturnLineItemView.Owner = documentReturnLine.Owner;
				documentReturnLineItemView.OwnerBusinessUnitGUID = documentReturnLine.OwnerBusinessUnitGUID.GuidNullToString();
				documentReturnLineItemView.DocumentReturnLineGUID = documentReturnLine.DocumentReturnLineGUID.GuidNullToString();
				documentReturnLineItemView.Address = documentReturnLine.Address;
				documentReturnLineItemView.BuyerTableGUID = documentReturnLine.BuyerTableGUID.GuidNullToString();
				documentReturnLineItemView.ContactPersonName = documentReturnLine.ContactPersonName;
				documentReturnLineItemView.DocumentNo = documentReturnLine.DocumentNo;
				documentReturnLineItemView.DocumentReturnTableGUID = documentReturnLine.DocumentReturnTableGUID.GuidNullToString();
				documentReturnLineItemView.DocumentTypeGUID = documentReturnLine.DocumentTypeGUID.GuidNullToString();
				documentReturnLineItemView.LineNum = documentReturnLine.LineNum;
				documentReturnLineItemView.Remark = documentReturnLine.Remark;
				
				documentReturnLineItemView.RowVersion = documentReturnLine.RowVersion;
				return documentReturnLineItemView.GetDocumentReturnLineItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion DocumentReturnLineItemView
		#region ToDropDown
		public static SelectItem<DocumentReturnLineItemView> ToDropDownItem(this DocumentReturnLineItemView documentReturnLineView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<DocumentReturnLineItemView> selectItem = new SelectItem<DocumentReturnLineItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(documentReturnLineView.LineNum.ToString(), documentReturnLineView.DocumentNo);
				selectItem.Value = documentReturnLineView.DocumentReturnLineGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: documentReturnLineView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<DocumentReturnLineItemView>> ToDropDownItem(this IEnumerable<DocumentReturnLineItemView> documentReturnLineItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<DocumentReturnLineItemView>> selectItems = new List<SelectItem<DocumentReturnLineItemView>>();
				foreach (DocumentReturnLineItemView item in documentReturnLineItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

