using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class WithholdingTaxTableHandler
	{
		#region WithholdingTaxTableListView
		public static List<WithholdingTaxTableListView> GetWithholdingTaxTableListViewValidation(this List<WithholdingTaxTableListView> withholdingTaxTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return withholdingTaxTableListViews;
			}
			var result = new List<WithholdingTaxTableListView>();
			try
			{
				foreach (WithholdingTaxTableListView item in withholdingTaxTableListViews)
				{
					result.Add(item.GetWithholdingTaxTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static WithholdingTaxTableListView GetWithholdingTaxTableListViewValidation(this WithholdingTaxTableListView withholdingTaxTableListView)
		{
			try
			{
				withholdingTaxTableListView.RowAuthorize = withholdingTaxTableListView.GetListRowAuthorize();
				return withholdingTaxTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this WithholdingTaxTableListView withholdingTaxTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion WithholdingTaxTableListView
		#region WithholdingTaxTableItemView
		public static WithholdingTaxTableItemView GetWithholdingTaxTableItemViewValidation(this WithholdingTaxTableItemView withholdingTaxTableItemView)
		{
			try
			{
				withholdingTaxTableItemView.RowAuthorize = withholdingTaxTableItemView.GetItemRowAuthorize();
				return withholdingTaxTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this WithholdingTaxTableItemView withholdingTaxTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static WithholdingTaxTable ToWithholdingTaxTable(this WithholdingTaxTableItemView withholdingTaxTableItemView)
		{
			try
			{
				WithholdingTaxTable withholdingTaxTable = new WithholdingTaxTable();
				withholdingTaxTable.CompanyGUID = withholdingTaxTableItemView.CompanyGUID.StringToGuid();
				withholdingTaxTable.CreatedBy = withholdingTaxTableItemView.CreatedBy;
				withholdingTaxTable.CreatedDateTime = withholdingTaxTableItemView.CreatedDateTime.StringToSystemDateTime();
				withholdingTaxTable.ModifiedBy = withholdingTaxTableItemView.ModifiedBy;
				withholdingTaxTable.ModifiedDateTime = withholdingTaxTableItemView.ModifiedDateTime.StringToSystemDateTime();
				withholdingTaxTable.Owner = withholdingTaxTableItemView.Owner;
				withholdingTaxTable.OwnerBusinessUnitGUID = withholdingTaxTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				withholdingTaxTable.WithholdingTaxTableGUID = withholdingTaxTableItemView.WithholdingTaxTableGUID.StringToGuid();
				withholdingTaxTable.Description = withholdingTaxTableItemView.Description;
				withholdingTaxTable.LedgerAccount = withholdingTaxTableItemView.LedgerAccount;
				withholdingTaxTable.WHTCode = withholdingTaxTableItemView.WHTCode;
				
				withholdingTaxTable.RowVersion = withholdingTaxTableItemView.RowVersion;
				return withholdingTaxTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<WithholdingTaxTable> ToWithholdingTaxTable(this IEnumerable<WithholdingTaxTableItemView> withholdingTaxTableItemViews)
		{
			try
			{
				List<WithholdingTaxTable> withholdingTaxTables = new List<WithholdingTaxTable>();
				foreach (WithholdingTaxTableItemView item in withholdingTaxTableItemViews)
				{
					withholdingTaxTables.Add(item.ToWithholdingTaxTable());
				}
				return withholdingTaxTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static WithholdingTaxTableItemView ToWithholdingTaxTableItemView(this WithholdingTaxTable withholdingTaxTable)
		{
			try
			{
				WithholdingTaxTableItemView withholdingTaxTableItemView = new WithholdingTaxTableItemView();
				withholdingTaxTableItemView.CompanyGUID = withholdingTaxTable.CompanyGUID.GuidNullToString();
				withholdingTaxTableItemView.CreatedBy = withholdingTaxTable.CreatedBy;
				withholdingTaxTableItemView.CreatedDateTime = withholdingTaxTable.CreatedDateTime.DateTimeToString();
				withholdingTaxTableItemView.ModifiedBy = withholdingTaxTable.ModifiedBy;
				withholdingTaxTableItemView.ModifiedDateTime = withholdingTaxTable.ModifiedDateTime.DateTimeToString();
				withholdingTaxTableItemView.Owner = withholdingTaxTable.Owner;
				withholdingTaxTableItemView.OwnerBusinessUnitGUID = withholdingTaxTable.OwnerBusinessUnitGUID.GuidNullToString();
				withholdingTaxTableItemView.WithholdingTaxTableGUID = withholdingTaxTable.WithholdingTaxTableGUID.GuidNullToString();
				withholdingTaxTableItemView.Description = withholdingTaxTable.Description;
				withholdingTaxTableItemView.LedgerAccount = withholdingTaxTable.LedgerAccount;
				withholdingTaxTableItemView.WHTCode = withholdingTaxTable.WHTCode;
				
				withholdingTaxTableItemView.RowVersion = withholdingTaxTable.RowVersion;
				return withholdingTaxTableItemView.GetWithholdingTaxTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion WithholdingTaxTableItemView
		#region ToDropDown
		public static SelectItem<WithholdingTaxTableItemView> ToDropDownItem(this WithholdingTaxTableItemView withholdingTaxTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<WithholdingTaxTableItemView> selectItem = new SelectItem<WithholdingTaxTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(withholdingTaxTableView.WHTCode, withholdingTaxTableView.Description);
				selectItem.Value = withholdingTaxTableView.WithholdingTaxTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: withholdingTaxTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<WithholdingTaxTableItemView>> ToDropDownItem(this IEnumerable<WithholdingTaxTableItemView> withholdingTaxTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<WithholdingTaxTableItemView>> selectItems = new List<SelectItem<WithholdingTaxTableItemView>>();
				foreach (WithholdingTaxTableItemView item in withholdingTaxTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

