using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class ExchangeRateHandler
	{
		#region ExchangeRateListView
		public static List<ExchangeRateListView> GetExchangeRateListViewValidation(this List<ExchangeRateListView> exchangeRateListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return exchangeRateListViews;
			}
			var result = new List<ExchangeRateListView>();
			try
			{
				foreach (ExchangeRateListView item in exchangeRateListViews)
				{
					result.Add(item.GetExchangeRateListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ExchangeRateListView GetExchangeRateListViewValidation(this ExchangeRateListView exchangeRateListView)
		{
			try
			{
				exchangeRateListView.RowAuthorize = exchangeRateListView.GetListRowAuthorize();
				return exchangeRateListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this ExchangeRateListView exchangeRateListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion ExchangeRateListView
		#region ExchangeRateItemView
		public static ExchangeRateItemView GetExchangeRateItemViewValidation(this ExchangeRateItemView exchangeRateItemView)
		{
			try
			{
				exchangeRateItemView.RowAuthorize = exchangeRateItemView.GetItemRowAuthorize();
				return exchangeRateItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this ExchangeRateItemView exchangeRateItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static ExchangeRate ToExchangeRate(this ExchangeRateItemView exchangeRateItemView)
		{
			try
			{
				ExchangeRate exchangeRate = new ExchangeRate();
				exchangeRate.CompanyGUID = exchangeRateItemView.CompanyGUID.StringToGuid();
				exchangeRate.CreatedBy = exchangeRateItemView.CreatedBy;
				exchangeRate.CreatedDateTime = exchangeRateItemView.CreatedDateTime.StringToSystemDateTime();
				exchangeRate.ModifiedBy = exchangeRateItemView.ModifiedBy;
				exchangeRate.ModifiedDateTime = exchangeRateItemView.ModifiedDateTime.StringToSystemDateTime();
				exchangeRate.Owner = exchangeRateItemView.Owner;
				exchangeRate.OwnerBusinessUnitGUID = exchangeRateItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				exchangeRate.ExchangeRateGUID = exchangeRateItemView.ExchangeRateGUID.StringToGuid();
				exchangeRate.CurrencyGUID = exchangeRateItemView.CurrencyGUID.StringToGuid();
				exchangeRate.EffectiveFrom = exchangeRateItemView.EffectiveFrom.StringToDate();
				exchangeRate.EffectiveTo = exchangeRateItemView.EffectiveTo.StringNullToDateNull();
				exchangeRate.HomeCurrencyGUID = exchangeRateItemView.HomeCurrencyGUID.StringToGuidNull();
				exchangeRate.Rate = exchangeRateItemView.Rate;
				
				exchangeRate.RowVersion = exchangeRateItemView.RowVersion;
				return exchangeRate;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<ExchangeRate> ToExchangeRate(this IEnumerable<ExchangeRateItemView> exchangeRateItemViews)
		{
			try
			{
				List<ExchangeRate> exchangeRates = new List<ExchangeRate>();
				foreach (ExchangeRateItemView item in exchangeRateItemViews)
				{
					exchangeRates.Add(item.ToExchangeRate());
				}
				return exchangeRates;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ExchangeRateItemView ToExchangeRateItemView(this ExchangeRate exchangeRate)
		{
			try
			{
				ExchangeRateItemView exchangeRateItemView = new ExchangeRateItemView();
				exchangeRateItemView.CompanyGUID = exchangeRate.CompanyGUID.GuidNullToString();
				exchangeRateItemView.CreatedBy = exchangeRate.CreatedBy;
				exchangeRateItemView.CreatedDateTime = exchangeRate.CreatedDateTime.DateTimeToString();
				exchangeRateItemView.ModifiedBy = exchangeRate.ModifiedBy;
				exchangeRateItemView.ModifiedDateTime = exchangeRate.ModifiedDateTime.DateTimeToString();
				exchangeRateItemView.Owner = exchangeRate.Owner;
				exchangeRateItemView.OwnerBusinessUnitGUID = exchangeRate.OwnerBusinessUnitGUID.GuidNullToString();
				exchangeRateItemView.ExchangeRateGUID = exchangeRate.ExchangeRateGUID.GuidNullToString();
				exchangeRateItemView.CurrencyGUID = exchangeRate.CurrencyGUID.GuidNullToString();
				exchangeRateItemView.EffectiveFrom = exchangeRate.EffectiveFrom.DateToString();
				exchangeRateItemView.EffectiveTo = exchangeRate.EffectiveTo.DateNullToString();
				exchangeRateItemView.HomeCurrencyGUID = exchangeRate.HomeCurrencyGUID.GuidNullToString();
				exchangeRateItemView.Rate = exchangeRate.Rate;
				
				exchangeRateItemView.RowVersion = exchangeRate.RowVersion;
				return exchangeRateItemView.GetExchangeRateItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ExchangeRateItemView
		#region ToDropDown
		public static SelectItem<ExchangeRateItemView> ToDropDownItem(this ExchangeRateItemView exchangeRateView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<ExchangeRateItemView> selectItem = new SelectItem<ExchangeRateItemView>();
				selectItem.Label = null;
				selectItem.Value = exchangeRateView.ExchangeRateGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: exchangeRateView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<ExchangeRateItemView>> ToDropDownItem(this IEnumerable<ExchangeRateItemView> exchangeRateItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<ExchangeRateItemView>> selectItems = new List<SelectItem<ExchangeRateItemView>>();
				foreach (ExchangeRateItemView item in exchangeRateItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

