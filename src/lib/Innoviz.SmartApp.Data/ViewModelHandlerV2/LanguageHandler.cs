using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class LanguageHandler
	{
		#region LanguageListView
		public static List<LanguageListView> GetLanguageListViewValidation(this List<LanguageListView> languageListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return languageListViews;
			}
			var result = new List<LanguageListView>();
			try
			{
				foreach (LanguageListView item in languageListViews)
				{
					result.Add(item.GetLanguageListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static LanguageListView GetLanguageListViewValidation(this LanguageListView languageListView)
		{
			try
			{
				languageListView.RowAuthorize = languageListView.GetListRowAuthorize();
				return languageListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this LanguageListView languageListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion LanguageListView
		#region LanguageItemView
		public static LanguageItemView GetLanguageItemViewValidation(this LanguageItemView languageItemView)
		{
			try
			{
				languageItemView.RowAuthorize = languageItemView.GetItemRowAuthorize();
				return languageItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this LanguageItemView languageItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static Language ToLanguage(this LanguageItemView languageItemView)
		{
			try
			{
				Language language = new Language();
				language.CompanyGUID = languageItemView.CompanyGUID.StringToGuid();
				language.CreatedBy = languageItemView.CreatedBy;
				language.CreatedDateTime = languageItemView.CreatedDateTime.StringToSystemDateTime();
				language.ModifiedBy = languageItemView.ModifiedBy;
				language.ModifiedDateTime = languageItemView.ModifiedDateTime.StringToSystemDateTime();
				language.Owner = languageItemView.Owner;
				language.OwnerBusinessUnitGUID = languageItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				language.LanguageGUID = languageItemView.LanguageGUID.StringToGuid();
				language.LanguageId = languageItemView.LanguageId;
				language.Name = languageItemView.Name;
				
				language.RowVersion = languageItemView.RowVersion;
				return language;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<Language> ToLanguage(this IEnumerable<LanguageItemView> languageItemViews)
		{
			try
			{
				List<Language> languages = new List<Language>();
				foreach (LanguageItemView item in languageItemViews)
				{
					languages.Add(item.ToLanguage());
				}
				return languages;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static LanguageItemView ToLanguageItemView(this Language language)
		{
			try
			{
				LanguageItemView languageItemView = new LanguageItemView();
				languageItemView.CompanyGUID = language.CompanyGUID.GuidNullToString();
				languageItemView.CreatedBy = language.CreatedBy;
				languageItemView.CreatedDateTime = language.CreatedDateTime.DateTimeToString();
				languageItemView.ModifiedBy = language.ModifiedBy;
				languageItemView.ModifiedDateTime = language.ModifiedDateTime.DateTimeToString();
				languageItemView.Owner = language.Owner;
				languageItemView.OwnerBusinessUnitGUID = language.OwnerBusinessUnitGUID.GuidNullToString();
				languageItemView.LanguageGUID = language.LanguageGUID.GuidNullToString();
				languageItemView.LanguageId = language.LanguageId;
				languageItemView.Name = language.Name;
				
				languageItemView.RowVersion = language.RowVersion;
				return languageItemView.GetLanguageItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion LanguageItemView
		#region ToDropDown
		public static SelectItem<LanguageItemView> ToDropDownItem(this LanguageItemView languageView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<LanguageItemView> selectItem = new SelectItem<LanguageItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(languageView.LanguageId, languageView.Name);
				selectItem.Value = languageView.LanguageGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: languageView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<LanguageItemView>> ToDropDownItem(this IEnumerable<LanguageItemView> languageItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<LanguageItemView>> selectItems = new List<SelectItem<LanguageItemView>>();
				foreach (LanguageItemView item in languageItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

