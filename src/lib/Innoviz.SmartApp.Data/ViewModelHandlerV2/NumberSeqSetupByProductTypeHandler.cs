using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class NumberSeqSetupByProductTypeHandler
	{
		#region NumberSeqSetupByProductTypeListView
		public static List<NumberSeqSetupByProductTypeListView> GetNumberSeqSetupByProductTypeListViewValidation(this List<NumberSeqSetupByProductTypeListView> numberSeqSetupByProductTypeListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return numberSeqSetupByProductTypeListViews;
			}
			var result = new List<NumberSeqSetupByProductTypeListView>();
			try
			{
				foreach (NumberSeqSetupByProductTypeListView item in numberSeqSetupByProductTypeListViews)
				{
					result.Add(item.GetNumberSeqSetupByProductTypeListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static NumberSeqSetupByProductTypeListView GetNumberSeqSetupByProductTypeListViewValidation(this NumberSeqSetupByProductTypeListView numberSeqSetupByProductTypeListView)
		{
			try
			{
				numberSeqSetupByProductTypeListView.RowAuthorize = numberSeqSetupByProductTypeListView.GetListRowAuthorize();
				return numberSeqSetupByProductTypeListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this NumberSeqSetupByProductTypeListView numberSeqSetupByProductTypeListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion NumberSeqSetupByProductTypeListView
		#region NumberSeqSetupByProductTypeItemView
		public static NumberSeqSetupByProductTypeItemView GetNumberSeqSetupByProductTypeItemViewValidation(this NumberSeqSetupByProductTypeItemView numberSeqSetupByProductTypeItemView)
		{
			try
			{
				numberSeqSetupByProductTypeItemView.RowAuthorize = numberSeqSetupByProductTypeItemView.GetItemRowAuthorize();
				return numberSeqSetupByProductTypeItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this NumberSeqSetupByProductTypeItemView numberSeqSetupByProductTypeItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static NumberSeqSetupByProductType ToNumberSeqSetupByProductType(this NumberSeqSetupByProductTypeItemView numberSeqSetupByProductTypeItemView)
		{
			try
			{
				NumberSeqSetupByProductType numberSeqSetupByProductType = new NumberSeqSetupByProductType();
				numberSeqSetupByProductType.CompanyGUID = numberSeqSetupByProductTypeItemView.CompanyGUID.StringToGuid();
				numberSeqSetupByProductType.CreatedBy = numberSeqSetupByProductTypeItemView.CreatedBy;
				numberSeqSetupByProductType.CreatedDateTime = numberSeqSetupByProductTypeItemView.CreatedDateTime.StringToSystemDateTime();
				numberSeqSetupByProductType.ModifiedBy = numberSeqSetupByProductTypeItemView.ModifiedBy;
				numberSeqSetupByProductType.ModifiedDateTime = numberSeqSetupByProductTypeItemView.ModifiedDateTime.StringToSystemDateTime();
				numberSeqSetupByProductType.Owner = numberSeqSetupByProductTypeItemView.Owner;
				numberSeqSetupByProductType.OwnerBusinessUnitGUID = numberSeqSetupByProductTypeItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				numberSeqSetupByProductType.NumberSeqSetupByProductTypeGUID = numberSeqSetupByProductTypeItemView.NumberSeqSetupByProductTypeGUID.StringToGuid();
				numberSeqSetupByProductType.CreditAppNumberSeqGUID = numberSeqSetupByProductTypeItemView.CreditAppNumberSeqGUID.StringToGuidNull();
				numberSeqSetupByProductType.CreditAppRequestNumberSeqGUID = numberSeqSetupByProductTypeItemView.CreditAppRequestNumberSeqGUID.StringToGuidNull();
				numberSeqSetupByProductType.GuarantorAgreementNumberSeqGUID = numberSeqSetupByProductTypeItemView.GuarantorAgreementNumberSeqGUID.StringToGuidNull();
				numberSeqSetupByProductType.InternalGuarantorAgreementNumberSeqGUID = numberSeqSetupByProductTypeItemView.InternalGuarantorAgreementNumberSeqGUID.StringToGuidNull();
				numberSeqSetupByProductType.InternalMainAgreementNumberSeqGUID = numberSeqSetupByProductTypeItemView.InternalMainAgreementNumberSeqGUID.StringToGuidNull();
				numberSeqSetupByProductType.MainAgreementNumberSeqGUID = numberSeqSetupByProductTypeItemView.MainAgreementNumberSeqGUID.StringToGuidNull();
				numberSeqSetupByProductType.TaxInvoiceNumberSeqGUID = numberSeqSetupByProductTypeItemView.TaxInvoiceNumberSeqGUID.StringToGuidNull();
				numberSeqSetupByProductType.TaxCreditNoteNumberSeqGUID = numberSeqSetupByProductTypeItemView.TaxCreditNoteNumberSeqGUID.StringToGuidNull();
				numberSeqSetupByProductType.ReceiptNumberSeqGUID = numberSeqSetupByProductTypeItemView.ReceiptNumberSeqGUID.StringToGuidNull();
				numberSeqSetupByProductType.ProductType = numberSeqSetupByProductTypeItemView.ProductType;
				
				numberSeqSetupByProductType.RowVersion = numberSeqSetupByProductTypeItemView.RowVersion;
				return numberSeqSetupByProductType;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<NumberSeqSetupByProductType> ToNumberSeqSetupByProductType(this IEnumerable<NumberSeqSetupByProductTypeItemView> numberSeqSetupByProductTypeItemViews)
		{
			try
			{
				List<NumberSeqSetupByProductType> numberSeqSetupByProductTypes = new List<NumberSeqSetupByProductType>();
				foreach (NumberSeqSetupByProductTypeItemView item in numberSeqSetupByProductTypeItemViews)
				{
					numberSeqSetupByProductTypes.Add(item.ToNumberSeqSetupByProductType());
				}
				return numberSeqSetupByProductTypes;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static NumberSeqSetupByProductTypeItemView ToNumberSeqSetupByProductTypeItemView(this NumberSeqSetupByProductType numberSeqSetupByProductType)
		{
			try
			{
				NumberSeqSetupByProductTypeItemView numberSeqSetupByProductTypeItemView = new NumberSeqSetupByProductTypeItemView();
				numberSeqSetupByProductTypeItemView.CompanyGUID = numberSeqSetupByProductType.CompanyGUID.GuidNullToString();
				numberSeqSetupByProductTypeItemView.CreatedBy = numberSeqSetupByProductType.CreatedBy;
				numberSeqSetupByProductTypeItemView.CreatedDateTime = numberSeqSetupByProductType.CreatedDateTime.DateTimeToString();
				numberSeqSetupByProductTypeItemView.ModifiedBy = numberSeqSetupByProductType.ModifiedBy;
				numberSeqSetupByProductTypeItemView.ModifiedDateTime = numberSeqSetupByProductType.ModifiedDateTime.DateTimeToString();
				numberSeqSetupByProductTypeItemView.Owner = numberSeqSetupByProductType.Owner;
				numberSeqSetupByProductTypeItemView.OwnerBusinessUnitGUID = numberSeqSetupByProductType.OwnerBusinessUnitGUID.GuidNullToString();
				numberSeqSetupByProductTypeItemView.NumberSeqSetupByProductTypeGUID = numberSeqSetupByProductType.NumberSeqSetupByProductTypeGUID.GuidNullToString();
				numberSeqSetupByProductTypeItemView.CreditAppNumberSeqGUID = numberSeqSetupByProductType.CreditAppNumberSeqGUID.GuidNullToString();
				numberSeqSetupByProductTypeItemView.CreditAppRequestNumberSeqGUID = numberSeqSetupByProductType.CreditAppRequestNumberSeqGUID.GuidNullToString();
				numberSeqSetupByProductTypeItemView.GuarantorAgreementNumberSeqGUID = numberSeqSetupByProductType.GuarantorAgreementNumberSeqGUID.GuidNullToString();
				numberSeqSetupByProductTypeItemView.InternalGuarantorAgreementNumberSeqGUID = numberSeqSetupByProductType.InternalGuarantorAgreementNumberSeqGUID.GuidNullToString();
				numberSeqSetupByProductTypeItemView.InternalMainAgreementNumberSeqGUID = numberSeqSetupByProductType.InternalMainAgreementNumberSeqGUID.GuidNullToString();
				numberSeqSetupByProductTypeItemView.MainAgreementNumberSeqGUID = numberSeqSetupByProductType.MainAgreementNumberSeqGUID.GuidNullToString();
				numberSeqSetupByProductTypeItemView.TaxInvoiceNumberSeqGUID = numberSeqSetupByProductType.TaxInvoiceNumberSeqGUID.GuidNullToString();
				numberSeqSetupByProductTypeItemView.TaxCreditNoteNumberSeqGUID = numberSeqSetupByProductType.TaxCreditNoteNumberSeqGUID.GuidNullToString();
				numberSeqSetupByProductTypeItemView.ReceiptNumberSeqGUID = numberSeqSetupByProductType.ReceiptNumberSeqGUID.GuidNullToString();
				numberSeqSetupByProductTypeItemView.ProductType = numberSeqSetupByProductType.ProductType;
				
				numberSeqSetupByProductTypeItemView.RowVersion = numberSeqSetupByProductType.RowVersion;
				return numberSeqSetupByProductTypeItemView.GetNumberSeqSetupByProductTypeItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion NumberSeqSetupByProductTypeItemView
		#region ToDropDown
		public static SelectItem<NumberSeqSetupByProductTypeItemView> ToDropDownItem(this NumberSeqSetupByProductTypeItemView numberSeqSetupByProductTypeView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<NumberSeqSetupByProductTypeItemView> selectItem = new SelectItem<NumberSeqSetupByProductTypeItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(numberSeqSetupByProductTypeView.ProductType.ToString());
				selectItem.Value = numberSeqSetupByProductTypeView.NumberSeqSetupByProductTypeGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: numberSeqSetupByProductTypeView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<NumberSeqSetupByProductTypeItemView>> ToDropDownItem(this IEnumerable<NumberSeqSetupByProductTypeItemView> numberSeqSetupByProductTypeItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<NumberSeqSetupByProductTypeItemView>> selectItems = new List<SelectItem<NumberSeqSetupByProductTypeItemView>>();
				foreach (NumberSeqSetupByProductTypeItemView item in numberSeqSetupByProductTypeItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

