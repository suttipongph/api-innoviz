using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class NumberSeqParameterHandler
	{
		#region NumberSeqParameterListView
		public static List<NumberSeqParameterListView> GetNumberSeqParameterListViewValidation(this List<NumberSeqParameterListView> numberSeqParameterListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return numberSeqParameterListViews;
			}
			var result = new List<NumberSeqParameterListView>();
			try
			{
				foreach (NumberSeqParameterListView item in numberSeqParameterListViews)
				{
					result.Add(item.GetNumberSeqParameterListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static NumberSeqParameterListView GetNumberSeqParameterListViewValidation(this NumberSeqParameterListView numberSeqParameterListView)
		{
			try
			{
				numberSeqParameterListView.RowAuthorize = numberSeqParameterListView.GetListRowAuthorize();
				return numberSeqParameterListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this NumberSeqParameterListView numberSeqParameterListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion NumberSeqParameterListView
		#region NumberSeqParameterItemView
		public static NumberSeqParameterItemView GetNumberSeqParameterItemViewValidation(this NumberSeqParameterItemView numberSeqParameterItemView)
		{
			try
			{
				numberSeqParameterItemView.RowAuthorize = numberSeqParameterItemView.GetItemRowAuthorize();
				return numberSeqParameterItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this NumberSeqParameterItemView numberSeqParameterItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static NumberSeqParameter ToNumberSeqParameter(this NumberSeqParameterItemView numberSeqParameterItemView)
		{
			try
			{
				NumberSeqParameter numberSeqParameter = new NumberSeqParameter();
				numberSeqParameter.CompanyGUID = numberSeqParameterItemView.CompanyGUID.StringToGuid();
				numberSeqParameter.CreatedBy = numberSeqParameterItemView.CreatedBy;
				numberSeqParameter.CreatedDateTime = numberSeqParameterItemView.CreatedDateTime.StringToSystemDateTime();
				numberSeqParameter.ModifiedBy = numberSeqParameterItemView.ModifiedBy;
				numberSeqParameter.ModifiedDateTime = numberSeqParameterItemView.ModifiedDateTime.StringToSystemDateTime();
				numberSeqParameter.Owner = numberSeqParameterItemView.Owner;
				numberSeqParameter.OwnerBusinessUnitGUID = numberSeqParameterItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				numberSeqParameter.NumberSeqParameterGUID = numberSeqParameterItemView.NumberSeqParameterGUID.StringToGuid();
				numberSeqParameter.NumberSeqTableGUID = numberSeqParameterItemView.NumberSeqTableGUID.StringToGuidNull();
				numberSeqParameter.ReferenceId = numberSeqParameterItemView.ReferenceId;
				
				numberSeqParameter.RowVersion = numberSeqParameterItemView.RowVersion;
				return numberSeqParameter;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<NumberSeqParameter> ToNumberSeqParameter(this IEnumerable<NumberSeqParameterItemView> numberSeqParameterItemViews)
		{
			try
			{
				List<NumberSeqParameter> numberSeqParameters = new List<NumberSeqParameter>();
				foreach (NumberSeqParameterItemView item in numberSeqParameterItemViews)
				{
					numberSeqParameters.Add(item.ToNumberSeqParameter());
				}
				return numberSeqParameters;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static NumberSeqParameterItemView ToNumberSeqParameterItemView(this NumberSeqParameter numberSeqParameter)
		{
			try
			{
				NumberSeqParameterItemView numberSeqParameterItemView = new NumberSeqParameterItemView();
				numberSeqParameterItemView.CompanyGUID = numberSeqParameter.CompanyGUID.GuidNullToString();
				numberSeqParameterItemView.CreatedBy = numberSeqParameter.CreatedBy;
				numberSeqParameterItemView.CreatedDateTime = numberSeqParameter.CreatedDateTime.DateTimeToString();
				numberSeqParameterItemView.ModifiedBy = numberSeqParameter.ModifiedBy;
				numberSeqParameterItemView.ModifiedDateTime = numberSeqParameter.ModifiedDateTime.DateTimeToString();
				numberSeqParameterItemView.Owner = numberSeqParameter.Owner;
				numberSeqParameterItemView.OwnerBusinessUnitGUID = numberSeqParameter.OwnerBusinessUnitGUID.GuidNullToString();
				numberSeqParameterItemView.NumberSeqParameterGUID = numberSeqParameter.NumberSeqParameterGUID.GuidNullToString();
				numberSeqParameterItemView.NumberSeqTableGUID = numberSeqParameter.NumberSeqTableGUID.GuidNullToString();
				numberSeqParameterItemView.ReferenceId = numberSeqParameter.ReferenceId;
				
				numberSeqParameterItemView.RowVersion = numberSeqParameter.RowVersion;
				return numberSeqParameterItemView.GetNumberSeqParameterItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion NumberSeqParameterItemView
		#region ToDropDown
		public static SelectItem<NumberSeqParameterItemView> ToDropDownItem(this NumberSeqParameterItemView numberSeqParameterView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<NumberSeqParameterItemView> selectItem = new SelectItem<NumberSeqParameterItemView>();
				selectItem.Label = null;
				selectItem.Value = numberSeqParameterView.NumberSeqParameterGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: numberSeqParameterView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<NumberSeqParameterItemView>> ToDropDownItem(this IEnumerable<NumberSeqParameterItemView> numberSeqParameterItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<NumberSeqParameterItemView>> selectItems = new List<SelectItem<NumberSeqParameterItemView>>();
				foreach (NumberSeqParameterItemView item in numberSeqParameterItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

