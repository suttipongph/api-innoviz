using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class LedgerFiscalPeriodHandler
	{
		#region LedgerFiscalPeriodListView
		public static List<LedgerFiscalPeriodListView> GetLedgerFiscalPeriodListViewValidation(this List<LedgerFiscalPeriodListView> ledgerFiscalPeriodListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return ledgerFiscalPeriodListViews;
			}
			var result = new List<LedgerFiscalPeriodListView>();
			try
			{
				foreach (LedgerFiscalPeriodListView item in ledgerFiscalPeriodListViews)
				{
					result.Add(item.GetLedgerFiscalPeriodListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static LedgerFiscalPeriodListView GetLedgerFiscalPeriodListViewValidation(this LedgerFiscalPeriodListView ledgerFiscalPeriodListView)
		{
			try
			{
				ledgerFiscalPeriodListView.RowAuthorize = ledgerFiscalPeriodListView.GetListRowAuthorize();
				return ledgerFiscalPeriodListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this LedgerFiscalPeriodListView ledgerFiscalPeriodListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion LedgerFiscalPeriodListView
		#region LedgerFiscalPeriodItemView
		public static LedgerFiscalPeriodItemView GetLedgerFiscalPeriodItemViewValidation(this LedgerFiscalPeriodItemView ledgerFiscalPeriodItemView)
		{
			try
			{
				ledgerFiscalPeriodItemView.RowAuthorize = ledgerFiscalPeriodItemView.GetItemRowAuthorize();
				return ledgerFiscalPeriodItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this LedgerFiscalPeriodItemView ledgerFiscalPeriodItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static LedgerFiscalPeriod ToLedgerFiscalPeriod(this LedgerFiscalPeriodItemView ledgerFiscalPeriodItemView)
		{
			try
			{
				LedgerFiscalPeriod ledgerFiscalPeriod = new LedgerFiscalPeriod();
				ledgerFiscalPeriod.CompanyGUID = ledgerFiscalPeriodItemView.CompanyGUID.StringToGuid();
				ledgerFiscalPeriod.CreatedBy = ledgerFiscalPeriodItemView.CreatedBy;
				ledgerFiscalPeriod.CreatedDateTime = ledgerFiscalPeriodItemView.CreatedDateTime.StringToSystemDateTime();
				ledgerFiscalPeriod.ModifiedBy = ledgerFiscalPeriodItemView.ModifiedBy;
				ledgerFiscalPeriod.ModifiedDateTime = ledgerFiscalPeriodItemView.ModifiedDateTime.StringToSystemDateTime();
				ledgerFiscalPeriod.Owner = ledgerFiscalPeriodItemView.Owner;
				ledgerFiscalPeriod.OwnerBusinessUnitGUID = ledgerFiscalPeriodItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				ledgerFiscalPeriod.LedgerFiscalPeriodGUID = ledgerFiscalPeriodItemView.LedgerFiscalPeriodGUID.StringToGuid();
				ledgerFiscalPeriod.EndDate = ledgerFiscalPeriodItemView.EndDate.StringToDate();
				ledgerFiscalPeriod.LedgerFiscalYearGUID = ledgerFiscalPeriodItemView.LedgerFiscalYearGUID.StringToGuid();
				ledgerFiscalPeriod.PeriodStatus = ledgerFiscalPeriodItemView.PeriodStatus;
				ledgerFiscalPeriod.StartDate = ledgerFiscalPeriodItemView.StartDate.StringToDate();
				
				ledgerFiscalPeriod.RowVersion = ledgerFiscalPeriodItemView.RowVersion;
				return ledgerFiscalPeriod;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<LedgerFiscalPeriod> ToLedgerFiscalPeriod(this IEnumerable<LedgerFiscalPeriodItemView> ledgerFiscalPeriodItemViews)
		{
			try
			{
				List<LedgerFiscalPeriod> ledgerFiscalPeriods = new List<LedgerFiscalPeriod>();
				foreach (LedgerFiscalPeriodItemView item in ledgerFiscalPeriodItemViews)
				{
					ledgerFiscalPeriods.Add(item.ToLedgerFiscalPeriod());
				}
				return ledgerFiscalPeriods;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static LedgerFiscalPeriodItemView ToLedgerFiscalPeriodItemView(this LedgerFiscalPeriod ledgerFiscalPeriod)
		{
			try
			{
				LedgerFiscalPeriodItemView ledgerFiscalPeriodItemView = new LedgerFiscalPeriodItemView();
				ledgerFiscalPeriodItemView.CompanyGUID = ledgerFiscalPeriod.CompanyGUID.GuidNullToString();
				ledgerFiscalPeriodItemView.CreatedBy = ledgerFiscalPeriod.CreatedBy;
				ledgerFiscalPeriodItemView.CreatedDateTime = ledgerFiscalPeriod.CreatedDateTime.DateTimeToString();
				ledgerFiscalPeriodItemView.ModifiedBy = ledgerFiscalPeriod.ModifiedBy;
				ledgerFiscalPeriodItemView.ModifiedDateTime = ledgerFiscalPeriod.ModifiedDateTime.DateTimeToString();
				ledgerFiscalPeriodItemView.Owner = ledgerFiscalPeriod.Owner;
				ledgerFiscalPeriodItemView.OwnerBusinessUnitGUID = ledgerFiscalPeriod.OwnerBusinessUnitGUID.GuidNullToString();
				ledgerFiscalPeriodItemView.LedgerFiscalPeriodGUID = ledgerFiscalPeriod.LedgerFiscalPeriodGUID.GuidNullToString();
				ledgerFiscalPeriodItemView.EndDate = ledgerFiscalPeriod.EndDate.DateToString();
				ledgerFiscalPeriodItemView.LedgerFiscalYearGUID = ledgerFiscalPeriod.LedgerFiscalYearGUID.GuidNullToString();
				ledgerFiscalPeriodItemView.PeriodStatus = ledgerFiscalPeriod.PeriodStatus;
				ledgerFiscalPeriodItemView.StartDate = ledgerFiscalPeriod.StartDate.DateToString();
				
				ledgerFiscalPeriodItemView.RowVersion = ledgerFiscalPeriod.RowVersion;
				return ledgerFiscalPeriodItemView.GetLedgerFiscalPeriodItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion LedgerFiscalPeriodItemView
		#region ToDropDown
		public static SelectItem<LedgerFiscalPeriodItemView> ToDropDownItem(this LedgerFiscalPeriodItemView ledgerFiscalPeriodView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<LedgerFiscalPeriodItemView> selectItem = new SelectItem<LedgerFiscalPeriodItemView>();
				selectItem.Label = null;
				selectItem.Value = ledgerFiscalPeriodView.LedgerFiscalPeriodGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: ledgerFiscalPeriodView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<LedgerFiscalPeriodItemView>> ToDropDownItem(this IEnumerable<LedgerFiscalPeriodItemView> ledgerFiscalPeriodItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<LedgerFiscalPeriodItemView>> selectItems = new List<SelectItem<LedgerFiscalPeriodItemView>>();
				foreach (LedgerFiscalPeriodItemView item in ledgerFiscalPeriodItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

