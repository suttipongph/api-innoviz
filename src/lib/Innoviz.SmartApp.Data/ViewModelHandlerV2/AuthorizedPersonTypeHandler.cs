using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class AuthorizedPersonTypeHandler
	{
		#region AuthorizedPersonTypeListView
		public static List<AuthorizedPersonTypeListView> GetAuthorizedPersonTypeListViewValidation(this List<AuthorizedPersonTypeListView> authorizedPersonTypeListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return authorizedPersonTypeListViews;
			}
			var result = new List<AuthorizedPersonTypeListView>();
			try
			{
				foreach (AuthorizedPersonTypeListView item in authorizedPersonTypeListViews)
				{
					result.Add(item.GetAuthorizedPersonTypeListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AuthorizedPersonTypeListView GetAuthorizedPersonTypeListViewValidation(this AuthorizedPersonTypeListView authorizedPersonTypeListView)
		{
			try
			{
				authorizedPersonTypeListView.RowAuthorize = authorizedPersonTypeListView.GetListRowAuthorize();
				return authorizedPersonTypeListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this AuthorizedPersonTypeListView authorizedPersonTypeListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion AuthorizedPersonTypeListView
		#region AuthorizedPersonTypeItemView
		public static AuthorizedPersonTypeItemView GetAuthorizedPersonTypeItemViewValidation(this AuthorizedPersonTypeItemView authorizedPersonTypeItemView)
		{
			try
			{
				authorizedPersonTypeItemView.RowAuthorize = authorizedPersonTypeItemView.GetItemRowAuthorize();
				return authorizedPersonTypeItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this AuthorizedPersonTypeItemView authorizedPersonTypeItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static AuthorizedPersonType ToAuthorizedPersonType(this AuthorizedPersonTypeItemView authorizedPersonTypeItemView)
		{
			try
			{
				AuthorizedPersonType authorizedPersonType = new AuthorizedPersonType();
				authorizedPersonType.CompanyGUID = authorizedPersonTypeItemView.CompanyGUID.StringToGuid();
				authorizedPersonType.CreatedBy = authorizedPersonTypeItemView.CreatedBy;
				authorizedPersonType.CreatedDateTime = authorizedPersonTypeItemView.CreatedDateTime.StringToSystemDateTime();
				authorizedPersonType.ModifiedBy = authorizedPersonTypeItemView.ModifiedBy;
				authorizedPersonType.ModifiedDateTime = authorizedPersonTypeItemView.ModifiedDateTime.StringToSystemDateTime();
				authorizedPersonType.Owner = authorizedPersonTypeItemView.Owner;
				authorizedPersonType.OwnerBusinessUnitGUID = authorizedPersonTypeItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				authorizedPersonType.AuthorizedPersonTypeGUID = authorizedPersonTypeItemView.AuthorizedPersonTypeGUID.StringToGuid();
				authorizedPersonType.AuthorizedPersonTypeId = authorizedPersonTypeItemView.AuthorizedPersonTypeId;
				authorizedPersonType.Description = authorizedPersonTypeItemView.Description;
				
				authorizedPersonType.RowVersion = authorizedPersonTypeItemView.RowVersion;
				return authorizedPersonType;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<AuthorizedPersonType> ToAuthorizedPersonType(this IEnumerable<AuthorizedPersonTypeItemView> authorizedPersonTypeItemViews)
		{
			try
			{
				List<AuthorizedPersonType> authorizedPersonTypes = new List<AuthorizedPersonType>();
				foreach (AuthorizedPersonTypeItemView item in authorizedPersonTypeItemViews)
				{
					authorizedPersonTypes.Add(item.ToAuthorizedPersonType());
				}
				return authorizedPersonTypes;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AuthorizedPersonTypeItemView ToAuthorizedPersonTypeItemView(this AuthorizedPersonType authorizedPersonType)
		{
			try
			{
				AuthorizedPersonTypeItemView authorizedPersonTypeItemView = new AuthorizedPersonTypeItemView();
				authorizedPersonTypeItemView.CompanyGUID = authorizedPersonType.CompanyGUID.GuidNullToString();
				authorizedPersonTypeItemView.CreatedBy = authorizedPersonType.CreatedBy;
				authorizedPersonTypeItemView.CreatedDateTime = authorizedPersonType.CreatedDateTime.DateTimeToString();
				authorizedPersonTypeItemView.ModifiedBy = authorizedPersonType.ModifiedBy;
				authorizedPersonTypeItemView.ModifiedDateTime = authorizedPersonType.ModifiedDateTime.DateTimeToString();
				authorizedPersonTypeItemView.Owner = authorizedPersonType.Owner;
				authorizedPersonTypeItemView.OwnerBusinessUnitGUID = authorizedPersonType.OwnerBusinessUnitGUID.GuidNullToString();
				authorizedPersonTypeItemView.AuthorizedPersonTypeGUID = authorizedPersonType.AuthorizedPersonTypeGUID.GuidNullToString();
				authorizedPersonTypeItemView.AuthorizedPersonTypeId = authorizedPersonType.AuthorizedPersonTypeId;
				authorizedPersonTypeItemView.Description = authorizedPersonType.Description;
				
				authorizedPersonTypeItemView.RowVersion = authorizedPersonType.RowVersion;
				return authorizedPersonTypeItemView.GetAuthorizedPersonTypeItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion AuthorizedPersonTypeItemView
		#region ToDropDown
		public static SelectItem<AuthorizedPersonTypeItemView> ToDropDownItem(this AuthorizedPersonTypeItemView authorizedPersonTypeView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<AuthorizedPersonTypeItemView> selectItem = new SelectItem<AuthorizedPersonTypeItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(authorizedPersonTypeView.AuthorizedPersonTypeId, authorizedPersonTypeView.Description);
				selectItem.Value = authorizedPersonTypeView.AuthorizedPersonTypeGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: authorizedPersonTypeView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<AuthorizedPersonTypeItemView>> ToDropDownItem(this IEnumerable<AuthorizedPersonTypeItemView> authorizedPersonTypeItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<AuthorizedPersonTypeItemView>> selectItems = new List<SelectItem<AuthorizedPersonTypeItemView>>();
				foreach (AuthorizedPersonTypeItemView item in authorizedPersonTypeItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

