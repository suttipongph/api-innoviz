using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class BillingResponsibleByHandler
	{
		#region BillingResponsibleByListView
		public static List<BillingResponsibleByListView> GetBillingResponsibleByListViewValidation(this List<BillingResponsibleByListView> billingResponsibleByListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return billingResponsibleByListViews;
			}
			var result = new List<BillingResponsibleByListView>();
			try
			{
				foreach (BillingResponsibleByListView item in billingResponsibleByListViews)
				{
					result.Add(item.GetBillingResponsibleByListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BillingResponsibleByListView GetBillingResponsibleByListViewValidation(this BillingResponsibleByListView billingResponsibleByListView)
		{
			try
			{
				billingResponsibleByListView.RowAuthorize = billingResponsibleByListView.GetListRowAuthorize();
				return billingResponsibleByListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this BillingResponsibleByListView billingResponsibleByListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion BillingResponsibleByListView
		#region BillingResponsibleByItemView
		public static BillingResponsibleByItemView GetBillingResponsibleByItemViewValidation(this BillingResponsibleByItemView billingResponsibleByItemView)
		{
			try
			{
				billingResponsibleByItemView.RowAuthorize = billingResponsibleByItemView.GetItemRowAuthorize();
				return billingResponsibleByItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this BillingResponsibleByItemView billingResponsibleByItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static BillingResponsibleBy ToBillingResponsibleBy(this BillingResponsibleByItemView billingResponsibleByItemView)
		{
			try
			{
				BillingResponsibleBy billingResponsibleBy = new BillingResponsibleBy();
				billingResponsibleBy.CompanyGUID = billingResponsibleByItemView.CompanyGUID.StringToGuid();
				billingResponsibleBy.CreatedBy = billingResponsibleByItemView.CreatedBy;
				billingResponsibleBy.CreatedDateTime = billingResponsibleByItemView.CreatedDateTime.StringToSystemDateTime();
				billingResponsibleBy.ModifiedBy = billingResponsibleByItemView.ModifiedBy;
				billingResponsibleBy.ModifiedDateTime = billingResponsibleByItemView.ModifiedDateTime.StringToSystemDateTime();
				billingResponsibleBy.Owner = billingResponsibleByItemView.Owner;
				billingResponsibleBy.OwnerBusinessUnitGUID = billingResponsibleByItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				billingResponsibleBy.BillingResponsibleByGUID = billingResponsibleByItemView.BillingResponsibleByGUID.StringToGuid();
				billingResponsibleBy.BillingBy = billingResponsibleByItemView.BillingBy;
				billingResponsibleBy.BillingResponsibleById = billingResponsibleByItemView.BillingResponsibleById;
				billingResponsibleBy.Description = billingResponsibleByItemView.Description;
				
				billingResponsibleBy.RowVersion = billingResponsibleByItemView.RowVersion;
				return billingResponsibleBy;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<BillingResponsibleBy> ToBillingResponsibleBy(this IEnumerable<BillingResponsibleByItemView> billingResponsibleByItemViews)
		{
			try
			{
				List<BillingResponsibleBy> billingResponsibleBys = new List<BillingResponsibleBy>();
				foreach (BillingResponsibleByItemView item in billingResponsibleByItemViews)
				{
					billingResponsibleBys.Add(item.ToBillingResponsibleBy());
				}
				return billingResponsibleBys;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BillingResponsibleByItemView ToBillingResponsibleByItemView(this BillingResponsibleBy billingResponsibleBy)
		{
			try
			{
				BillingResponsibleByItemView billingResponsibleByItemView = new BillingResponsibleByItemView();
				billingResponsibleByItemView.CompanyGUID = billingResponsibleBy.CompanyGUID.GuidNullToString();
				billingResponsibleByItemView.CreatedBy = billingResponsibleBy.CreatedBy;
				billingResponsibleByItemView.CreatedDateTime = billingResponsibleBy.CreatedDateTime.DateTimeToString();
				billingResponsibleByItemView.ModifiedBy = billingResponsibleBy.ModifiedBy;
				billingResponsibleByItemView.ModifiedDateTime = billingResponsibleBy.ModifiedDateTime.DateTimeToString();
				billingResponsibleByItemView.Owner = billingResponsibleBy.Owner;
				billingResponsibleByItemView.OwnerBusinessUnitGUID = billingResponsibleBy.OwnerBusinessUnitGUID.GuidNullToString();
				billingResponsibleByItemView.BillingResponsibleByGUID = billingResponsibleBy.BillingResponsibleByGUID.GuidNullToString();
				billingResponsibleByItemView.BillingBy = billingResponsibleBy.BillingBy;
				billingResponsibleByItemView.BillingResponsibleById = billingResponsibleBy.BillingResponsibleById;
				billingResponsibleByItemView.Description = billingResponsibleBy.Description;
				
				billingResponsibleByItemView.RowVersion = billingResponsibleBy.RowVersion;
				return billingResponsibleByItemView.GetBillingResponsibleByItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion BillingResponsibleByItemView
		#region ToDropDown
		public static SelectItem<BillingResponsibleByItemView> ToDropDownItem(this BillingResponsibleByItemView billingResponsibleByView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<BillingResponsibleByItemView> selectItem = new SelectItem<BillingResponsibleByItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(billingResponsibleByView.BillingResponsibleById, billingResponsibleByView.Description);
				selectItem.Value = billingResponsibleByView.BillingResponsibleByGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: billingResponsibleByView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<BillingResponsibleByItemView>> ToDropDownItem(this IEnumerable<BillingResponsibleByItemView> billingResponsibleByItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<BillingResponsibleByItemView>> selectItems = new List<SelectItem<BillingResponsibleByItemView>>();
				foreach (BillingResponsibleByItemView item in billingResponsibleByItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

