using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class BusinessCollateralSubTypeHandler
	{
		#region BusinessCollateralSubTypeListView
		public static List<BusinessCollateralSubTypeListView> GetBusinessCollateralSubTypeListViewValidation(this List<BusinessCollateralSubTypeListView> businessCollateralSubTypeListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return businessCollateralSubTypeListViews;
			}
			var result = new List<BusinessCollateralSubTypeListView>();
			try
			{
				foreach (BusinessCollateralSubTypeListView item in businessCollateralSubTypeListViews)
				{
					result.Add(item.GetBusinessCollateralSubTypeListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BusinessCollateralSubTypeListView GetBusinessCollateralSubTypeListViewValidation(this BusinessCollateralSubTypeListView businessCollateralSubTypeListView)
		{
			try
			{
				businessCollateralSubTypeListView.RowAuthorize = businessCollateralSubTypeListView.GetListRowAuthorize();
				return businessCollateralSubTypeListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this BusinessCollateralSubTypeListView businessCollateralSubTypeListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion BusinessCollateralSubTypeListView
		#region BusinessCollateralSubTypeItemView
		public static BusinessCollateralSubTypeItemView GetBusinessCollateralSubTypeItemViewValidation(this BusinessCollateralSubTypeItemView businessCollateralSubTypeItemView)
		{
			try
			{
				businessCollateralSubTypeItemView.RowAuthorize = businessCollateralSubTypeItemView.GetItemRowAuthorize();
				return businessCollateralSubTypeItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this BusinessCollateralSubTypeItemView businessCollateralSubTypeItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static BusinessCollateralSubType ToBusinessCollateralSubType(this BusinessCollateralSubTypeItemView businessCollateralSubTypeItemView)
		{
			try
			{
				BusinessCollateralSubType businessCollateralSubType = new BusinessCollateralSubType();
				businessCollateralSubType.CompanyGUID = businessCollateralSubTypeItemView.CompanyGUID.StringToGuid();
				businessCollateralSubType.CreatedBy = businessCollateralSubTypeItemView.CreatedBy;
				businessCollateralSubType.CreatedDateTime = businessCollateralSubTypeItemView.CreatedDateTime.StringToSystemDateTime();
				businessCollateralSubType.ModifiedBy = businessCollateralSubTypeItemView.ModifiedBy;
				businessCollateralSubType.ModifiedDateTime = businessCollateralSubTypeItemView.ModifiedDateTime.StringToSystemDateTime();
				businessCollateralSubType.Owner = businessCollateralSubTypeItemView.Owner;
				businessCollateralSubType.OwnerBusinessUnitGUID = businessCollateralSubTypeItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				businessCollateralSubType.BusinessCollateralSubTypeGUID = businessCollateralSubTypeItemView.BusinessCollateralSubTypeGUID.StringToGuid();
				businessCollateralSubType.AgreementOrdering = businessCollateralSubTypeItemView.AgreementOrdering;
				businessCollateralSubType.AgreementRefText = businessCollateralSubTypeItemView.AgreementRefText;
				businessCollateralSubType.BusinessCollateralSubTypeId = businessCollateralSubTypeItemView.BusinessCollateralSubTypeId;
				businessCollateralSubType.BusinessCollateralTypeGUID = businessCollateralSubTypeItemView.BusinessCollateralTypeGUID.StringToGuid();
				businessCollateralSubType.Description = businessCollateralSubTypeItemView.Description;
				businessCollateralSubType.DebtorClaims = businessCollateralSubTypeItemView.DebtorClaims;

				businessCollateralSubType.RowVersion = businessCollateralSubTypeItemView.RowVersion;
				return businessCollateralSubType;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<BusinessCollateralSubType> ToBusinessCollateralSubType(this IEnumerable<BusinessCollateralSubTypeItemView> businessCollateralSubTypeItemViews)
		{
			try
			{
				List<BusinessCollateralSubType> businessCollateralSubTypes = new List<BusinessCollateralSubType>();
				foreach (BusinessCollateralSubTypeItemView item in businessCollateralSubTypeItemViews)
				{
					businessCollateralSubTypes.Add(item.ToBusinessCollateralSubType());
				}
				return businessCollateralSubTypes;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BusinessCollateralSubTypeItemView ToBusinessCollateralSubTypeItemView(this BusinessCollateralSubType businessCollateralSubType)
		{
			try
			{
				BusinessCollateralSubTypeItemView businessCollateralSubTypeItemView = new BusinessCollateralSubTypeItemView();
				businessCollateralSubTypeItemView.CompanyGUID = businessCollateralSubType.CompanyGUID.GuidNullToString();
				businessCollateralSubTypeItemView.CreatedBy = businessCollateralSubType.CreatedBy;
				businessCollateralSubTypeItemView.CreatedDateTime = businessCollateralSubType.CreatedDateTime.DateTimeToString();
				businessCollateralSubTypeItemView.ModifiedBy = businessCollateralSubType.ModifiedBy;
				businessCollateralSubTypeItemView.ModifiedDateTime = businessCollateralSubType.ModifiedDateTime.DateTimeToString();
				businessCollateralSubTypeItemView.Owner = businessCollateralSubType.Owner;
				businessCollateralSubTypeItemView.OwnerBusinessUnitGUID = businessCollateralSubType.OwnerBusinessUnitGUID.GuidNullToString();
				businessCollateralSubTypeItemView.BusinessCollateralSubTypeGUID = businessCollateralSubType.BusinessCollateralSubTypeGUID.GuidNullToString();
				businessCollateralSubTypeItemView.AgreementOrdering = businessCollateralSubType.AgreementOrdering;
				businessCollateralSubTypeItemView.AgreementRefText = businessCollateralSubType.AgreementRefText;
				businessCollateralSubTypeItemView.BusinessCollateralSubTypeId = businessCollateralSubType.BusinessCollateralSubTypeId;
				businessCollateralSubTypeItemView.BusinessCollateralTypeGUID = businessCollateralSubType.BusinessCollateralTypeGUID.GuidNullToString();
				businessCollateralSubTypeItemView.Description = businessCollateralSubType.Description;
				businessCollateralSubTypeItemView.DebtorClaims = businessCollateralSubType.DebtorClaims;

				businessCollateralSubTypeItemView.RowVersion = businessCollateralSubType.RowVersion;
				return businessCollateralSubTypeItemView.GetBusinessCollateralSubTypeItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion BusinessCollateralSubTypeItemView
		#region ToDropDown
		public static SelectItem<BusinessCollateralSubTypeItemView> ToDropDownItem(this BusinessCollateralSubTypeItemView businessCollateralSubTypeView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<BusinessCollateralSubTypeItemView> selectItem = new SelectItem<BusinessCollateralSubTypeItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(businessCollateralSubTypeView.BusinessCollateralSubTypeId, businessCollateralSubTypeView.Description);
				selectItem.Value = businessCollateralSubTypeView.BusinessCollateralSubTypeGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: businessCollateralSubTypeView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<BusinessCollateralSubTypeItemView>> ToDropDownItem(this IEnumerable<BusinessCollateralSubTypeItemView> businessCollateralSubTypeItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<BusinessCollateralSubTypeItemView>> selectItems = new List<SelectItem<BusinessCollateralSubTypeItemView>>();
				foreach (BusinessCollateralSubTypeItemView item in businessCollateralSubTypeItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}
