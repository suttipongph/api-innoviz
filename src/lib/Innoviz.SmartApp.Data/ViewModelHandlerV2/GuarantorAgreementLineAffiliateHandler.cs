using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class GuarantorAgreementLineAffiliateHandler
	{
		#region GuarantorAgreementLineAffiliateListView
		public static List<GuarantorAgreementLineAffiliateListView> GetGuarantorAgreementLineAffiliateListViewValidation(this List<GuarantorAgreementLineAffiliateListView> guarantorAgreementLineAffiliateListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return guarantorAgreementLineAffiliateListViews;
			}
			var result = new List<GuarantorAgreementLineAffiliateListView>();
			try
			{
				foreach (GuarantorAgreementLineAffiliateListView item in guarantorAgreementLineAffiliateListViews)
				{
					result.Add(item.GetGuarantorAgreementLineAffiliateListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static GuarantorAgreementLineAffiliateListView GetGuarantorAgreementLineAffiliateListViewValidation(this GuarantorAgreementLineAffiliateListView guarantorAgreementLineAffiliateListView)
		{
			try
			{
				guarantorAgreementLineAffiliateListView.RowAuthorize = guarantorAgreementLineAffiliateListView.GetListRowAuthorize();
				return guarantorAgreementLineAffiliateListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this GuarantorAgreementLineAffiliateListView guarantorAgreementLineAffiliateListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion GuarantorAgreementLineAffiliateListView
		#region GuarantorAgreementLineAffiliateItemView
		public static GuarantorAgreementLineAffiliateItemView GetGuarantorAgreementLineAffiliateItemViewValidation(this GuarantorAgreementLineAffiliateItemView guarantorAgreementLineAffiliateItemView)
		{
			try
			{
				guarantorAgreementLineAffiliateItemView.RowAuthorize = guarantorAgreementLineAffiliateItemView.GetItemRowAuthorize();
				return guarantorAgreementLineAffiliateItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this GuarantorAgreementLineAffiliateItemView guarantorAgreementLineAffiliateItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static GuarantorAgreementLineAffiliate ToGuarantorAgreementLineAffiliate(this GuarantorAgreementLineAffiliateItemView guarantorAgreementLineAffiliateItemView)
		{
			try
			{
				GuarantorAgreementLineAffiliate guarantorAgreementLineAffiliate = new GuarantorAgreementLineAffiliate();
				guarantorAgreementLineAffiliate.CompanyGUID = guarantorAgreementLineAffiliateItemView.CompanyGUID.StringToGuid();
				guarantorAgreementLineAffiliate.CreatedBy = guarantorAgreementLineAffiliateItemView.CreatedBy;
				guarantorAgreementLineAffiliate.CreatedDateTime = guarantorAgreementLineAffiliateItemView.CreatedDateTime.StringToSystemDateTime();
				guarantorAgreementLineAffiliate.ModifiedBy = guarantorAgreementLineAffiliateItemView.ModifiedBy;
				guarantorAgreementLineAffiliate.ModifiedDateTime = guarantorAgreementLineAffiliateItemView.ModifiedDateTime.StringToSystemDateTime();
				guarantorAgreementLineAffiliate.Owner = guarantorAgreementLineAffiliateItemView.Owner;
				guarantorAgreementLineAffiliate.OwnerBusinessUnitGUID = guarantorAgreementLineAffiliateItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				guarantorAgreementLineAffiliate.GuarantorAgreementLineAffiliateGUID = guarantorAgreementLineAffiliateItemView.GuarantorAgreementLineAffiliateGUID.StringToGuid();
				guarantorAgreementLineAffiliate.CustomerTableGUID = guarantorAgreementLineAffiliateItemView.CustomerTableGUID.StringToGuid();
				guarantorAgreementLineAffiliate.GuarantorAgreementLineGUID = guarantorAgreementLineAffiliateItemView.GuarantorAgreementLineGUID.StringToGuid();
				guarantorAgreementLineAffiliate.MainAgreementTableGUID = guarantorAgreementLineAffiliateItemView.MainAgreementTableGUID.StringToGuid();
				guarantorAgreementLineAffiliate.Ordering = guarantorAgreementLineAffiliateItemView.Ordering;
				
				guarantorAgreementLineAffiliate.RowVersion = guarantorAgreementLineAffiliateItemView.RowVersion;
				return guarantorAgreementLineAffiliate;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<GuarantorAgreementLineAffiliate> ToGuarantorAgreementLineAffiliate(this IEnumerable<GuarantorAgreementLineAffiliateItemView> guarantorAgreementLineAffiliateItemViews)
		{
			try
			{
				List<GuarantorAgreementLineAffiliate> guarantorAgreementLineAffiliates = new List<GuarantorAgreementLineAffiliate>();
				foreach (GuarantorAgreementLineAffiliateItemView item in guarantorAgreementLineAffiliateItemViews)
				{
					guarantorAgreementLineAffiliates.Add(item.ToGuarantorAgreementLineAffiliate());
				}
				return guarantorAgreementLineAffiliates;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static GuarantorAgreementLineAffiliateItemView ToGuarantorAgreementLineAffiliateItemView(this GuarantorAgreementLineAffiliate guarantorAgreementLineAffiliate)
		{
			try
			{
				GuarantorAgreementLineAffiliateItemView guarantorAgreementLineAffiliateItemView = new GuarantorAgreementLineAffiliateItemView();
				guarantorAgreementLineAffiliateItemView.CompanyGUID = guarantorAgreementLineAffiliate.CompanyGUID.GuidNullToString();
				guarantorAgreementLineAffiliateItemView.CreatedBy = guarantorAgreementLineAffiliate.CreatedBy;
				guarantorAgreementLineAffiliateItemView.CreatedDateTime = guarantorAgreementLineAffiliate.CreatedDateTime.DateTimeToString();
				guarantorAgreementLineAffiliateItemView.ModifiedBy = guarantorAgreementLineAffiliate.ModifiedBy;
				guarantorAgreementLineAffiliateItemView.ModifiedDateTime = guarantorAgreementLineAffiliate.ModifiedDateTime.DateTimeToString();
				guarantorAgreementLineAffiliateItemView.Owner = guarantorAgreementLineAffiliate.Owner;
				guarantorAgreementLineAffiliateItemView.OwnerBusinessUnitGUID = guarantorAgreementLineAffiliate.OwnerBusinessUnitGUID.GuidNullToString();
				guarantorAgreementLineAffiliateItemView.GuarantorAgreementLineAffiliateGUID = guarantorAgreementLineAffiliate.GuarantorAgreementLineAffiliateGUID.GuidNullToString();
				guarantorAgreementLineAffiliateItemView.CustomerTableGUID = guarantorAgreementLineAffiliate.CustomerTableGUID.GuidNullToString();
				guarantorAgreementLineAffiliateItemView.GuarantorAgreementLineGUID = guarantorAgreementLineAffiliate.GuarantorAgreementLineGUID.GuidNullToString();
				guarantorAgreementLineAffiliateItemView.MainAgreementTableGUID = guarantorAgreementLineAffiliate.MainAgreementTableGUID.GuidNullToString();
				guarantorAgreementLineAffiliateItemView.Ordering = guarantorAgreementLineAffiliate.Ordering;
				
				guarantorAgreementLineAffiliateItemView.RowVersion = guarantorAgreementLineAffiliate.RowVersion;
				return guarantorAgreementLineAffiliateItemView.GetGuarantorAgreementLineAffiliateItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GuarantorAgreementLineAffiliateItemView
		#region ToDropDown
		public static SelectItem<GuarantorAgreementLineAffiliateItemView> ToDropDownItem(this GuarantorAgreementLineAffiliateItemView guarantorAgreementLineAffiliateView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<GuarantorAgreementLineAffiliateItemView> selectItem = new SelectItem<GuarantorAgreementLineAffiliateItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(guarantorAgreementLineAffiliateView.Ordering.ToString(), guarantorAgreementLineAffiliateView.CustomerTableGUID.ToString());
				selectItem.Value = guarantorAgreementLineAffiliateView.GuarantorAgreementLineAffiliateGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: guarantorAgreementLineAffiliateView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<GuarantorAgreementLineAffiliateItemView>> ToDropDownItem(this IEnumerable<GuarantorAgreementLineAffiliateItemView> guarantorAgreementLineAffiliateItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<GuarantorAgreementLineAffiliateItemView>> selectItems = new List<SelectItem<GuarantorAgreementLineAffiliateItemView>>();
				foreach (GuarantorAgreementLineAffiliateItemView item in guarantorAgreementLineAffiliateItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

