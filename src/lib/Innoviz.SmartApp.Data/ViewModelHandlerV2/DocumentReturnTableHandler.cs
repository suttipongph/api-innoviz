using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class DocumentReturnTableHandler
	{
		#region DocumentReturnTableListView
		public static List<DocumentReturnTableListView> GetDocumentReturnTableListViewValidation(this List<DocumentReturnTableListView> documentReturnTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return documentReturnTableListViews;
			}
			var result = new List<DocumentReturnTableListView>();
			try
			{
				foreach (DocumentReturnTableListView item in documentReturnTableListViews)
				{
					result.Add(item.GetDocumentReturnTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static DocumentReturnTableListView GetDocumentReturnTableListViewValidation(this DocumentReturnTableListView documentReturnTableListView)
		{
			try
			{
				documentReturnTableListView.RowAuthorize = documentReturnTableListView.GetListRowAuthorize();
				return documentReturnTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this DocumentReturnTableListView documentReturnTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion DocumentReturnTableListView
		#region DocumentReturnTableItemView
		public static DocumentReturnTableItemView GetDocumentReturnTableItemViewValidation(this DocumentReturnTableItemView documentReturnTableItemView)
		{
			try
			{
				documentReturnTableItemView.RowAuthorize = documentReturnTableItemView.GetItemRowAuthorize();
				return documentReturnTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this DocumentReturnTableItemView documentReturnTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static DocumentReturnTable ToDocumentReturnTable(this DocumentReturnTableItemView documentReturnTableItemView)
		{
			try
			{
				DocumentReturnTable documentReturnTable = new DocumentReturnTable();
				documentReturnTable.CompanyGUID = documentReturnTableItemView.CompanyGUID.StringToGuid();
				documentReturnTable.CreatedBy = documentReturnTableItemView.CreatedBy;
				documentReturnTable.CreatedDateTime = documentReturnTableItemView.CreatedDateTime.StringToSystemDateTime();
				documentReturnTable.ModifiedBy = documentReturnTableItemView.ModifiedBy;
				documentReturnTable.ModifiedDateTime = documentReturnTableItemView.ModifiedDateTime.StringToSystemDateTime();
				documentReturnTable.Owner = documentReturnTableItemView.Owner;
				documentReturnTable.OwnerBusinessUnitGUID = documentReturnTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				documentReturnTable.DocumentReturnTableGUID = documentReturnTableItemView.DocumentReturnTableGUID.StringToGuid();
				documentReturnTable.ActualReturnDate = documentReturnTableItemView.ActualReturnDate.StringNullToDateNull();
				documentReturnTable.Address = documentReturnTableItemView.Address;
				documentReturnTable.BuyerTableGUID = documentReturnTableItemView.BuyerTableGUID.StringToGuidNull();
				documentReturnTable.ContactPersonName = documentReturnTableItemView.ContactPersonName;
				documentReturnTable.ContactTo = documentReturnTableItemView.ContactTo;
				documentReturnTable.CustomerTableGUID = documentReturnTableItemView.CustomerTableGUID.StringToGuidNull();
				documentReturnTable.DocumentReturnId = documentReturnTableItemView.DocumentReturnId;
				documentReturnTable.DocumentReturnMethodGUID = documentReturnTableItemView.DocumentReturnMethodGUID.StringToGuid();
				documentReturnTable.DocumentStatusGUID = documentReturnTableItemView.DocumentStatusGUID.StringToGuid();
				documentReturnTable.ExpectedReturnDate = documentReturnTableItemView.ExpectedReturnDate.StringNullToDateNull();
				documentReturnTable.PostAcknowledgementNo = documentReturnTableItemView.PostAcknowledgementNo;
				documentReturnTable.PostNumber = documentReturnTableItemView.PostNumber;
				documentReturnTable.ReceivedBy = documentReturnTableItemView.ReceivedBy;
				documentReturnTable.Remark = documentReturnTableItemView.Remark;
				documentReturnTable.RequestorGUID = documentReturnTableItemView.RequestorGUID.StringToGuid();
				documentReturnTable.ReturnByGUID = documentReturnTableItemView.ReturnByGUID.StringToGuidNull();
				
				documentReturnTable.RowVersion = documentReturnTableItemView.RowVersion;
				return documentReturnTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<DocumentReturnTable> ToDocumentReturnTable(this IEnumerable<DocumentReturnTableItemView> documentReturnTableItemViews)
		{
			try
			{
				List<DocumentReturnTable> documentReturnTables = new List<DocumentReturnTable>();
				foreach (DocumentReturnTableItemView item in documentReturnTableItemViews)
				{
					documentReturnTables.Add(item.ToDocumentReturnTable());
				}
				return documentReturnTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static DocumentReturnTableItemView ToDocumentReturnTableItemView(this DocumentReturnTable documentReturnTable)
		{
			try
			{
				DocumentReturnTableItemView documentReturnTableItemView = new DocumentReturnTableItemView();
				documentReturnTableItemView.CompanyGUID = documentReturnTable.CompanyGUID.GuidNullToString();
				documentReturnTableItemView.CreatedBy = documentReturnTable.CreatedBy;
				documentReturnTableItemView.CreatedDateTime = documentReturnTable.CreatedDateTime.DateTimeToString();
				documentReturnTableItemView.ModifiedBy = documentReturnTable.ModifiedBy;
				documentReturnTableItemView.ModifiedDateTime = documentReturnTable.ModifiedDateTime.DateTimeToString();
				documentReturnTableItemView.Owner = documentReturnTable.Owner;
				documentReturnTableItemView.OwnerBusinessUnitGUID = documentReturnTable.OwnerBusinessUnitGUID.GuidNullToString();
				documentReturnTableItemView.DocumentReturnTableGUID = documentReturnTable.DocumentReturnTableGUID.GuidNullToString();
				documentReturnTableItemView.ActualReturnDate = documentReturnTable.ActualReturnDate.DateNullToString();
				documentReturnTableItemView.Address = documentReturnTable.Address;
				documentReturnTableItemView.BuyerTableGUID = documentReturnTable.BuyerTableGUID.GuidNullToString();
				documentReturnTableItemView.ContactPersonName = documentReturnTable.ContactPersonName;
				documentReturnTableItemView.ContactTo = documentReturnTable.ContactTo;
				documentReturnTableItemView.CustomerTableGUID = documentReturnTable.CustomerTableGUID.GuidNullToString();
				documentReturnTableItemView.DocumentReturnId = documentReturnTable.DocumentReturnId;
				documentReturnTableItemView.DocumentReturnMethodGUID = documentReturnTable.DocumentReturnMethodGUID.GuidNullToString();
				documentReturnTableItemView.DocumentStatusGUID = documentReturnTable.DocumentStatusGUID.GuidNullToString();
				documentReturnTableItemView.ExpectedReturnDate = documentReturnTable.ExpectedReturnDate.DateNullToString();
				documentReturnTableItemView.PostAcknowledgementNo = documentReturnTable.PostAcknowledgementNo;
				documentReturnTableItemView.PostNumber = documentReturnTable.PostNumber;
				documentReturnTableItemView.ReceivedBy = documentReturnTable.ReceivedBy;
				documentReturnTableItemView.Remark = documentReturnTable.Remark;
				documentReturnTableItemView.RequestorGUID = documentReturnTable.RequestorGUID.GuidNullToString();
				documentReturnTableItemView.ReturnByGUID = documentReturnTable.ReturnByGUID.GuidNullToString();
				
				documentReturnTableItemView.RowVersion = documentReturnTable.RowVersion;
				return documentReturnTableItemView.GetDocumentReturnTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion DocumentReturnTableItemView
		#region ToDropDown
		public static SelectItem<DocumentReturnTableItemView> ToDropDownItem(this DocumentReturnTableItemView documentReturnTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<DocumentReturnTableItemView> selectItem = new SelectItem<DocumentReturnTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(documentReturnTableView.DocumentReturnId, documentReturnTableView.ContactPersonName);
				selectItem.Value = documentReturnTableView.DocumentReturnTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: documentReturnTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<DocumentReturnTableItemView>> ToDropDownItem(this IEnumerable<DocumentReturnTableItemView> documentReturnTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<DocumentReturnTableItemView>> selectItems = new List<SelectItem<DocumentReturnTableItemView>>();
				foreach (DocumentReturnTableItemView item in documentReturnTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

