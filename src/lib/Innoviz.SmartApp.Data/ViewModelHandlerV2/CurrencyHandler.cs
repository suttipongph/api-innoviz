using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CurrencyHandler
	{
		#region CurrencyListView
		public static List<CurrencyListView> GetCurrencyListViewValidation(this List<CurrencyListView> currencyListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return currencyListViews;
			}
			var result = new List<CurrencyListView>();
			try
			{
				foreach (CurrencyListView item in currencyListViews)
				{
					result.Add(item.GetCurrencyListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CurrencyListView GetCurrencyListViewValidation(this CurrencyListView currencyListView)
		{
			try
			{
				currencyListView.RowAuthorize = currencyListView.GetListRowAuthorize();
				return currencyListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CurrencyListView currencyListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CurrencyListView
		#region CurrencyItemView
		public static CurrencyItemView GetCurrencyItemViewValidation(this CurrencyItemView currencyItemView)
		{
			try
			{
				currencyItemView.RowAuthorize = currencyItemView.GetItemRowAuthorize();
				return currencyItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CurrencyItemView currencyItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static Currency ToCurrency(this CurrencyItemView currencyItemView)
		{
			try
			{
				Currency currency = new Currency();
				currency.CompanyGUID = currencyItemView.CompanyGUID.StringToGuid();
				currency.CreatedBy = currencyItemView.CreatedBy;
				currency.CreatedDateTime = currencyItemView.CreatedDateTime.StringToSystemDateTime();
				currency.ModifiedBy = currencyItemView.ModifiedBy;
				currency.ModifiedDateTime = currencyItemView.ModifiedDateTime.StringToSystemDateTime();
				currency.Owner = currencyItemView.Owner;
				currency.OwnerBusinessUnitGUID = currencyItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				currency.CurrencyGUID = currencyItemView.CurrencyGUID.StringToGuid();
				currency.CurrencyId = currencyItemView.CurrencyId;
				currency.Name = currencyItemView.Name;
				
				currency.RowVersion = currencyItemView.RowVersion;
				return currency;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<Currency> ToCurrency(this IEnumerable<CurrencyItemView> currencyItemViews)
		{
			try
			{
				List<Currency> currencys = new List<Currency>();
				foreach (CurrencyItemView item in currencyItemViews)
				{
					currencys.Add(item.ToCurrency());
				}
				return currencys;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CurrencyItemView ToCurrencyItemView(this Currency currency)
		{
			try
			{
				CurrencyItemView currencyItemView = new CurrencyItemView();
				currencyItemView.CompanyGUID = currency.CompanyGUID.GuidNullToString();
				currencyItemView.CreatedBy = currency.CreatedBy;
				currencyItemView.CreatedDateTime = currency.CreatedDateTime.DateTimeToString();
				currencyItemView.ModifiedBy = currency.ModifiedBy;
				currencyItemView.ModifiedDateTime = currency.ModifiedDateTime.DateTimeToString();
				currencyItemView.Owner = currency.Owner;
				currencyItemView.OwnerBusinessUnitGUID = currency.OwnerBusinessUnitGUID.GuidNullToString();
				currencyItemView.CurrencyGUID = currency.CurrencyGUID.GuidNullToString();
				currencyItemView.CurrencyId = currency.CurrencyId;
				currencyItemView.Name = currency.Name;
				
				currencyItemView.RowVersion = currency.RowVersion;
				return currencyItemView.GetCurrencyItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CurrencyItemView
		#region ToDropDown
		public static SelectItem<CurrencyItemView> ToDropDownItem(this CurrencyItemView currencyView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CurrencyItemView> selectItem = new SelectItem<CurrencyItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(currencyView.CurrencyId, currencyView.Name);
				selectItem.Value = currencyView.CurrencyGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: currencyView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CurrencyItemView>> ToDropDownItem(this IEnumerable<CurrencyItemView> currencyItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CurrencyItemView>> selectItems = new List<SelectItem<CurrencyItemView>>();
				foreach (CurrencyItemView item in currencyItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

