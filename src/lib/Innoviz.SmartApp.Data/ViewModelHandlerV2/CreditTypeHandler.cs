using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CreditTypeHandler
	{
		#region CreditTypeListView
		public static List<CreditTypeListView> GetCreditTypeListViewValidation(this List<CreditTypeListView> creditTypeListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return creditTypeListViews;
			}
			var result = new List<CreditTypeListView>();
			try
			{
				foreach (CreditTypeListView item in creditTypeListViews)
				{
					result.Add(item.GetCreditTypeListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CreditTypeListView GetCreditTypeListViewValidation(this CreditTypeListView creditTypeListView)
		{
			try
			{
				creditTypeListView.RowAuthorize = creditTypeListView.GetListRowAuthorize();
				return creditTypeListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CreditTypeListView creditTypeListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CreditTypeListView
		#region CreditTypeItemView
		public static CreditTypeItemView GetCreditTypeItemViewValidation(this CreditTypeItemView creditTypeItemView)
		{
			try
			{
				creditTypeItemView.RowAuthorize = creditTypeItemView.GetItemRowAuthorize();
				return creditTypeItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CreditTypeItemView creditTypeItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CreditType ToCreditType(this CreditTypeItemView creditTypeItemView)
		{
			try
			{
				CreditType creditType = new CreditType();
				creditType.CompanyGUID = creditTypeItemView.CompanyGUID.StringToGuid();
				creditType.CreatedBy = creditTypeItemView.CreatedBy;
				creditType.CreatedDateTime = creditTypeItemView.CreatedDateTime.StringToSystemDateTime();
				creditType.ModifiedBy = creditTypeItemView.ModifiedBy;
				creditType.ModifiedDateTime = creditTypeItemView.ModifiedDateTime.StringToSystemDateTime();
				creditType.Owner = creditTypeItemView.Owner;
				creditType.OwnerBusinessUnitGUID = creditTypeItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				creditType.CreditTypeGUID = creditTypeItemView.CreditTypeGUID.StringToGuid();
				creditType.CreditTypeId = creditTypeItemView.CreditTypeId;
				creditType.Description = creditTypeItemView.Description;
				
				creditType.RowVersion = creditTypeItemView.RowVersion;
				return creditType;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CreditType> ToCreditType(this IEnumerable<CreditTypeItemView> creditTypeItemViews)
		{
			try
			{
				List<CreditType> creditTypes = new List<CreditType>();
				foreach (CreditTypeItemView item in creditTypeItemViews)
				{
					creditTypes.Add(item.ToCreditType());
				}
				return creditTypes;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CreditTypeItemView ToCreditTypeItemView(this CreditType creditType)
		{
			try
			{
				CreditTypeItemView creditTypeItemView = new CreditTypeItemView();
				creditTypeItemView.CompanyGUID = creditType.CompanyGUID.GuidNullToString();
				creditTypeItemView.CreatedBy = creditType.CreatedBy;
				creditTypeItemView.CreatedDateTime = creditType.CreatedDateTime.DateTimeToString();
				creditTypeItemView.ModifiedBy = creditType.ModifiedBy;
				creditTypeItemView.ModifiedDateTime = creditType.ModifiedDateTime.DateTimeToString();
				creditTypeItemView.Owner = creditType.Owner;
				creditTypeItemView.OwnerBusinessUnitGUID = creditType.OwnerBusinessUnitGUID.GuidNullToString();
				creditTypeItemView.CreditTypeGUID = creditType.CreditTypeGUID.GuidNullToString();
				creditTypeItemView.CreditTypeId = creditType.CreditTypeId;
				creditTypeItemView.Description = creditType.Description;
				
				creditTypeItemView.RowVersion = creditType.RowVersion;
				return creditTypeItemView.GetCreditTypeItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CreditTypeItemView
		#region ToDropDown
		public static SelectItem<CreditTypeItemView> ToDropDownItem(this CreditTypeItemView creditTypeView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CreditTypeItemView> selectItem = new SelectItem<CreditTypeItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(creditTypeView.CreditTypeId, creditTypeView.Description);
				selectItem.Value = creditTypeView.CreditTypeGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: creditTypeView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CreditTypeItemView>> ToDropDownItem(this IEnumerable<CreditTypeItemView> creditTypeItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CreditTypeItemView>> selectItems = new List<SelectItem<CreditTypeItemView>>();
				foreach (CreditTypeItemView item in creditTypeItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

