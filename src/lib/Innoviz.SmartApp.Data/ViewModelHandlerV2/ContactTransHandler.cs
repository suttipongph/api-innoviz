using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class ContactTransHandler
	{
		#region ContactTransListView
		public static List<ContactTransListView> GetContactTransListViewValidation(this List<ContactTransListView> contactTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return contactTransListViews;
			}
			var result = new List<ContactTransListView>();
			try
			{
				foreach (ContactTransListView item in contactTransListViews)
				{
					result.Add(item.GetContactTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ContactTransListView GetContactTransListViewValidation(this ContactTransListView contactTransListView)
		{
			try
			{
				contactTransListView.RowAuthorize = contactTransListView.GetListRowAuthorize();
				return contactTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this ContactTransListView contactTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion ContactTransListView
		#region ContactTransItemView
		public static ContactTransItemView GetContactTransItemViewValidation(this ContactTransItemView contactTransItemView)
		{
			try
			{
				contactTransItemView.RowAuthorize = contactTransItemView.GetItemRowAuthorize();
				return contactTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this ContactTransItemView contactTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static ContactTrans ToContactTrans(this ContactTransItemView contactTransItemView)
		{
			try
			{
				ContactTrans contactTrans = new ContactTrans();
				contactTrans.CompanyGUID = contactTransItemView.CompanyGUID.StringToGuid();
				contactTrans.CreatedBy = contactTransItemView.CreatedBy;
				contactTrans.CreatedDateTime = contactTransItemView.CreatedDateTime.StringToSystemDateTime();
				contactTrans.ModifiedBy = contactTransItemView.ModifiedBy;
				contactTrans.ModifiedDateTime = contactTransItemView.ModifiedDateTime.StringToSystemDateTime();
				contactTrans.Owner = contactTransItemView.Owner;
				contactTrans.OwnerBusinessUnitGUID = contactTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				contactTrans.ContactTransGUID = contactTransItemView.ContactTransGUID.StringToGuid();
				contactTrans.ContactType = contactTransItemView.ContactType;
				contactTrans.ContactValue = contactTransItemView.ContactValue;
				contactTrans.Description = contactTransItemView.Description;
				contactTrans.Extension = contactTransItemView.Extension;
				contactTrans.PrimaryContact = contactTransItemView.PrimaryContact;
				contactTrans.RefGUID = contactTransItemView.RefGUID.StringToGuid();
				contactTrans.RefType = contactTransItemView.RefType;
				
				contactTrans.RowVersion = contactTransItemView.RowVersion;
				return contactTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<ContactTrans> ToContactTrans(this IEnumerable<ContactTransItemView> contactTransItemViews)
		{
			try
			{
				List<ContactTrans> contactTranss = new List<ContactTrans>();
				foreach (ContactTransItemView item in contactTransItemViews)
				{
					contactTranss.Add(item.ToContactTrans());
				}
				return contactTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ContactTransItemView ToContactTransItemView(this ContactTrans contactTrans)
		{
			try
			{
				ContactTransItemView contactTransItemView = new ContactTransItemView();
				contactTransItemView.CompanyGUID = contactTrans.CompanyGUID.GuidNullToString();
				contactTransItemView.CreatedBy = contactTrans.CreatedBy;
				contactTransItemView.CreatedDateTime = contactTrans.CreatedDateTime.DateTimeToString();
				contactTransItemView.ModifiedBy = contactTrans.ModifiedBy;
				contactTransItemView.ModifiedDateTime = contactTrans.ModifiedDateTime.DateTimeToString();
				contactTransItemView.Owner = contactTrans.Owner;
				contactTransItemView.OwnerBusinessUnitGUID = contactTrans.OwnerBusinessUnitGUID.GuidNullToString();
				contactTransItemView.ContactTransGUID = contactTrans.ContactTransGUID.GuidNullToString();
				contactTransItemView.ContactType = contactTrans.ContactType;
				contactTransItemView.ContactValue = contactTrans.ContactValue;
				contactTransItemView.Description = contactTrans.Description;
				contactTransItemView.Extension = contactTrans.Extension;
				contactTransItemView.PrimaryContact = contactTrans.PrimaryContact;
				contactTransItemView.RefGUID = contactTrans.RefGUID.GuidNullToString();
				contactTransItemView.RefType = contactTrans.RefType;
				
				contactTransItemView.RowVersion = contactTrans.RowVersion;
				return contactTransItemView.GetContactTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ContactTransItemView
		#region ToDropDown
		public static SelectItem<ContactTransItemView> ToDropDownItem(this ContactTransItemView contactTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<ContactTransItemView> selectItem = new SelectItem<ContactTransItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(contactTransView.ContactType.ToString(), contactTransView.ContactValue);
				selectItem.Value = contactTransView.ContactTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: contactTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<ContactTransItemView>> ToDropDownItem(this IEnumerable<ContactTransItemView> contactTransItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<ContactTransItemView>> selectItems = new List<SelectItem<ContactTransItemView>>();
				foreach (ContactTransItemView item in contactTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

