using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class KYCSetupHandler
	{
		#region KYCSetupListView
		public static List<KYCSetupListView> GetKYCSetupListViewValidation(this List<KYCSetupListView> kycSetupListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return kycSetupListViews;
			}
			var result = new List<KYCSetupListView>();
			try
			{
				foreach (KYCSetupListView item in kycSetupListViews)
				{
					result.Add(item.GetKYCSetupListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static KYCSetupListView GetKYCSetupListViewValidation(this KYCSetupListView kycSetupListView)
		{
			try
			{
				kycSetupListView.RowAuthorize = kycSetupListView.GetListRowAuthorize();
				return kycSetupListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this KYCSetupListView kycSetupListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion KYCSetupListView
		#region KYCSetupItemView
		public static KYCSetupItemView GetKYCSetupItemViewValidation(this KYCSetupItemView kycSetupItemView)
		{
			try
			{
				kycSetupItemView.RowAuthorize = kycSetupItemView.GetItemRowAuthorize();
				return kycSetupItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this KYCSetupItemView kycSetupItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static KYCSetup ToKYCSetup(this KYCSetupItemView kycSetupItemView)
		{
			try
			{
				KYCSetup kycSetup = new KYCSetup();
				kycSetup.CompanyGUID = kycSetupItemView.CompanyGUID.StringToGuid();
				kycSetup.CreatedBy = kycSetupItemView.CreatedBy;
				kycSetup.CreatedDateTime = kycSetupItemView.CreatedDateTime.StringToSystemDateTime();
				kycSetup.ModifiedBy = kycSetupItemView.ModifiedBy;
				kycSetup.ModifiedDateTime = kycSetupItemView.ModifiedDateTime.StringToSystemDateTime();
				kycSetup.Owner = kycSetupItemView.Owner;
				kycSetup.OwnerBusinessUnitGUID = kycSetupItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				kycSetup.KYCSetupGUID = kycSetupItemView.KYCSetupGUID.StringToGuid();
				kycSetup.Description = kycSetupItemView.Description;
				kycSetup.KYCId = kycSetupItemView.KYCId;
				
				kycSetup.RowVersion = kycSetupItemView.RowVersion;
				return kycSetup;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<KYCSetup> ToKYCSetup(this IEnumerable<KYCSetupItemView> kycSetupItemViews)
		{
			try
			{
				List<KYCSetup> kycSetups = new List<KYCSetup>();
				foreach (KYCSetupItemView item in kycSetupItemViews)
				{
					kycSetups.Add(item.ToKYCSetup());
				}
				return kycSetups;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static KYCSetupItemView ToKYCSetupItemView(this KYCSetup kycSetup)
		{
			try
			{
				KYCSetupItemView kycSetupItemView = new KYCSetupItemView();
				kycSetupItemView.CompanyGUID = kycSetup.CompanyGUID.GuidNullToString();
				kycSetupItemView.CreatedBy = kycSetup.CreatedBy;
				kycSetupItemView.CreatedDateTime = kycSetup.CreatedDateTime.DateTimeToString();
				kycSetupItemView.ModifiedBy = kycSetup.ModifiedBy;
				kycSetupItemView.ModifiedDateTime = kycSetup.ModifiedDateTime.DateTimeToString();
				kycSetupItemView.Owner = kycSetup.Owner;
				kycSetupItemView.OwnerBusinessUnitGUID = kycSetup.OwnerBusinessUnitGUID.GuidNullToString();
				kycSetupItemView.KYCSetupGUID = kycSetup.KYCSetupGUID.GuidNullToString();
				kycSetupItemView.Description = kycSetup.Description;
				kycSetupItemView.KYCId = kycSetup.KYCId;
				
				kycSetupItemView.RowVersion = kycSetup.RowVersion;
				return kycSetupItemView.GetKYCSetupItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion KYCSetupItemView
		#region ToDropDown
		public static SelectItem<KYCSetupItemView> ToDropDownItem(this KYCSetupItemView kycSetupView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<KYCSetupItemView> selectItem = new SelectItem<KYCSetupItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(kycSetupView.KYCId, kycSetupView.Description);
				selectItem.Value = kycSetupView.KYCSetupGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: kycSetupView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<KYCSetupItemView>> ToDropDownItem(this IEnumerable<KYCSetupItemView> kycSetupItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<KYCSetupItemView>> selectItems = new List<SelectItem<KYCSetupItemView>>();
				foreach (KYCSetupItemView item in kycSetupItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

