using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CreditAppTableHandler
	{
		#region CreditAppTableListView
		public static List<CreditAppTableListView> GetCreditAppTableListViewValidation(this List<CreditAppTableListView> creditAppTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return creditAppTableListViews;
			}
			var result = new List<CreditAppTableListView>();
			try
			{
				foreach (CreditAppTableListView item in creditAppTableListViews)
				{
					result.Add(item.GetCreditAppTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CreditAppTableListView GetCreditAppTableListViewValidation(this CreditAppTableListView creditAppTableListView)
		{
			try
			{
				creditAppTableListView.RowAuthorize = creditAppTableListView.GetListRowAuthorize();
				return creditAppTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CreditAppTableListView creditAppTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CreditAppTableListView
		#region CreditAppTableItemView
		public static CreditAppTableItemView GetCreditAppTableItemViewValidation(this CreditAppTableItemView creditAppTableItemView)
		{
			try
			{
				creditAppTableItemView.RowAuthorize = creditAppTableItemView.GetItemRowAuthorize();
				return creditAppTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CreditAppTableItemView creditAppTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CreditAppTable ToCreditAppTable(this CreditAppTableItemView creditAppTableItemView)
		{
			try
			{
				CreditAppTable creditAppTable = new CreditAppTable();
				creditAppTable.CompanyGUID = creditAppTableItemView.CompanyGUID.StringToGuid();
				creditAppTable.CreatedBy = creditAppTableItemView.CreatedBy;
				creditAppTable.CreatedDateTime = creditAppTableItemView.CreatedDateTime.StringToSystemDateTime();
				creditAppTable.ModifiedBy = creditAppTableItemView.ModifiedBy;
				creditAppTable.ModifiedDateTime = creditAppTableItemView.ModifiedDateTime.StringToSystemDateTime();
				creditAppTable.Owner = creditAppTableItemView.Owner;
				creditAppTable.OwnerBusinessUnitGUID = creditAppTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				creditAppTable.CreditAppTableGUID = creditAppTableItemView.CreditAppTableGUID.StringToGuid();
				creditAppTable.ApplicationTableGUID = creditAppTableItemView.ApplicationTableGUID.StringToGuidNull();
				creditAppTable.ApprovedCreditLimit = creditAppTableItemView.ApprovedCreditLimit;
				creditAppTable.ApprovedDate = creditAppTableItemView.ApprovedDate.StringToDate();
				creditAppTable.BankAccountControlGUID = creditAppTableItemView.BankAccountControlGUID.StringToGuidNull();
				creditAppTable.BillingAddressGUID = creditAppTableItemView.BillingAddressGUID.StringToGuidNull();
				creditAppTable.BillingContactPersonGUID = creditAppTableItemView.BillingContactPersonGUID.StringToGuidNull();
				creditAppTable.CACondition = creditAppTableItemView.CACondition;
				creditAppTable.ConsortiumTableGUID = creditAppTableItemView.ConsortiumTableGUID.StringToGuidNull();
				creditAppTable.CreditAppId = creditAppTableItemView.CreditAppId;
				creditAppTable.CreditLimitExpiration = creditAppTableItemView.CreditLimitExpiration;
				creditAppTable.CreditLimitRemark = creditAppTableItemView.CreditLimitRemark;
				creditAppTable.CreditLimitTypeGUID = creditAppTableItemView.CreditLimitTypeGUID.StringToGuid();
				creditAppTable.CreditRequestFeePct = creditAppTableItemView.CreditRequestFeePct;
				creditAppTable.CreditTermGUID = creditAppTableItemView.CreditTermGUID.StringToGuidNull();
				creditAppTable.CustomerTableGUID = creditAppTableItemView.CustomerTableGUID.StringToGuid();
				creditAppTable.Description = creditAppTableItemView.Description;
				creditAppTable.Dimension1GUID = creditAppTableItemView.Dimension1GUID.StringToGuidNull();
				creditAppTable.Dimension2GUID = creditAppTableItemView.Dimension2GUID.StringToGuidNull();
				creditAppTable.Dimension3GUID = creditAppTableItemView.Dimension3GUID.StringToGuidNull();
				creditAppTable.Dimension4GUID = creditAppTableItemView.Dimension4GUID.StringToGuidNull();
				creditAppTable.Dimension5GUID = creditAppTableItemView.Dimension5GUID.StringToGuidNull();
				creditAppTable.DocumentReasonGUID = creditAppTableItemView.DocumentReasonGUID.StringToGuidNull();
				creditAppTable.ExpectedAgreementSigningDate = creditAppTableItemView.ExpectedAgreementSigningDate.StringNullToDateNull();
				creditAppTable.ExpiryDate = creditAppTableItemView.ExpiryDate.StringToDate();
				creditAppTable.ExtensionServiceFeeCondTemplateGUID = creditAppTableItemView.ExtensionServiceFeeCondTemplateGUID.StringToGuidNull();
				creditAppTable.InactiveDate = creditAppTableItemView.InactiveDate.StringNullToDateNull();
				creditAppTable.InterestAdjustment = creditAppTableItemView.InterestAdjustment;
				creditAppTable.InterestTypeGUID = creditAppTableItemView.InterestTypeGUID.StringToGuid();
				creditAppTable.InvoiceAddressGUID = creditAppTableItemView.InvoiceAddressGUID.StringToGuidNull();
				creditAppTable.MailingReceipAddressGUID = creditAppTableItemView.MailingReceipAddressGUID.StringToGuidNull();
				creditAppTable.MaxPurchasePct = creditAppTableItemView.MaxPurchasePct;
				creditAppTable.MaxRetentionAmount = creditAppTableItemView.MaxRetentionAmount;
				creditAppTable.MaxRetentionPct = creditAppTableItemView.MaxRetentionPct;
				creditAppTable.PDCBankGUID = creditAppTableItemView.PDCBankGUID.StringToGuidNull();
				creditAppTable.ProductSubTypeGUID = creditAppTableItemView.ProductSubTypeGUID.StringToGuid();
				creditAppTable.ProductType = creditAppTableItemView.ProductType;
				creditAppTable.PurchaseFeeCalculateBase = creditAppTableItemView.PurchaseFeeCalculateBase;
				creditAppTable.PurchaseFeePct = creditAppTableItemView.PurchaseFeePct;
				creditAppTable.ReceiptAddressGUID = creditAppTableItemView.ReceiptAddressGUID.StringToGuidNull();
				creditAppTable.ReceiptContactPersonGUID = creditAppTableItemView.ReceiptContactPersonGUID.StringToGuidNull();
				creditAppTable.RefCreditAppRequestTableGUID = creditAppTableItemView.RefCreditAppRequestTableGUID.StringToGuid();
				creditAppTable.RegisteredAddressGUID = creditAppTableItemView.RegisteredAddressGUID.StringToGuidNull();
				creditAppTable.Remark = creditAppTableItemView.Remark;
				creditAppTable.SalesAvgPerMonth = creditAppTableItemView.SalesAvgPerMonth;
				creditAppTable.StartDate = creditAppTableItemView.StartDate.StringToDate();
				creditAppTable.TotalInterestPct = creditAppTableItemView.TotalInterestPct;
				creditAppTable.AssignmentMethodGUID = creditAppTableItemView.AssignmentMethodGUID.StringToGuidNull();
				creditAppTable.PurchaseWithdrawalCondition = creditAppTableItemView.PurchaseWithdrawalCondition;
				
				creditAppTable.RowVersion = creditAppTableItemView.RowVersion;
				return creditAppTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CreditAppTable> ToCreditAppTable(this IEnumerable<CreditAppTableItemView> creditAppTableItemViews)
		{
			try
			{
				List<CreditAppTable> creditAppTables = new List<CreditAppTable>();
				foreach (CreditAppTableItemView item in creditAppTableItemViews)
				{
					creditAppTables.Add(item.ToCreditAppTable());
				}
				return creditAppTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CreditAppTableItemView ToCreditAppTableItemView(this CreditAppTable creditAppTable)
		{
			try
			{
				CreditAppTableItemView creditAppTableItemView = new CreditAppTableItemView();
				creditAppTableItemView.CompanyGUID = creditAppTable.CompanyGUID.GuidNullToString();
				creditAppTableItemView.CreatedBy = creditAppTable.CreatedBy;
				creditAppTableItemView.CreatedDateTime = creditAppTable.CreatedDateTime.DateTimeToString();
				creditAppTableItemView.ModifiedBy = creditAppTable.ModifiedBy;
				creditAppTableItemView.ModifiedDateTime = creditAppTable.ModifiedDateTime.DateTimeToString();
				creditAppTableItemView.Owner = creditAppTable.Owner;
				creditAppTableItemView.OwnerBusinessUnitGUID = creditAppTable.OwnerBusinessUnitGUID.GuidNullToString();
				creditAppTableItemView.CreditAppTableGUID = creditAppTable.CreditAppTableGUID.GuidNullToString();
				creditAppTableItemView.ApplicationTableGUID = creditAppTable.ApplicationTableGUID.GuidNullToString();
				creditAppTableItemView.ApprovedCreditLimit = creditAppTable.ApprovedCreditLimit;
				creditAppTableItemView.ApprovedDate = creditAppTable.ApprovedDate.DateToString();
				creditAppTableItemView.BankAccountControlGUID = creditAppTable.BankAccountControlGUID.GuidNullToString();
				creditAppTableItemView.BillingAddressGUID = creditAppTable.BillingAddressGUID.GuidNullToString();
				creditAppTableItemView.BillingContactPersonGUID = creditAppTable.BillingContactPersonGUID.GuidNullToString();
				creditAppTableItemView.CACondition = creditAppTable.CACondition;
				creditAppTableItemView.ConsortiumTableGUID = creditAppTable.ConsortiumTableGUID.GuidNullToString();
				creditAppTableItemView.CreditAppId = creditAppTable.CreditAppId;
				creditAppTableItemView.CreditLimitExpiration = creditAppTable.CreditLimitExpiration;
				creditAppTableItemView.CreditLimitRemark = creditAppTable.CreditLimitRemark;
				creditAppTableItemView.CreditLimitTypeGUID = creditAppTable.CreditLimitTypeGUID.GuidNullToString();
				creditAppTableItemView.CreditRequestFeePct = creditAppTable.CreditRequestFeePct;
				creditAppTableItemView.CreditTermGUID = creditAppTable.CreditTermGUID.GuidNullToString();
				creditAppTableItemView.CustomerTableGUID = creditAppTable.CustomerTableGUID.GuidNullToString();
				creditAppTableItemView.Description = creditAppTable.Description;
				creditAppTableItemView.Dimension1GUID = creditAppTable.Dimension1GUID.GuidNullToString();
				creditAppTableItemView.Dimension2GUID = creditAppTable.Dimension2GUID.GuidNullToString();
				creditAppTableItemView.Dimension3GUID = creditAppTable.Dimension3GUID.GuidNullToString();
				creditAppTableItemView.Dimension4GUID = creditAppTable.Dimension4GUID.GuidNullToString();
				creditAppTableItemView.Dimension5GUID = creditAppTable.Dimension5GUID.GuidNullToString();
				creditAppTableItemView.DocumentReasonGUID = creditAppTable.DocumentReasonGUID.GuidNullToString();
				creditAppTableItemView.ExpectedAgreementSigningDate = creditAppTable.ExpectedAgreementSigningDate.DateNullToString();
				creditAppTableItemView.ExpiryDate = creditAppTable.ExpiryDate.DateToString();
				creditAppTableItemView.ExtensionServiceFeeCondTemplateGUID = creditAppTable.ExtensionServiceFeeCondTemplateGUID.GuidNullToString();
				creditAppTableItemView.InactiveDate = creditAppTable.InactiveDate.DateNullToString();
				creditAppTableItemView.InterestAdjustment = creditAppTable.InterestAdjustment;
				creditAppTableItemView.InterestTypeGUID = creditAppTable.InterestTypeGUID.GuidNullToString();
				creditAppTableItemView.InvoiceAddressGUID = creditAppTable.InvoiceAddressGUID.GuidNullToString();
				creditAppTableItemView.MailingReceipAddressGUID = creditAppTable.MailingReceipAddressGUID.GuidNullToString();
				creditAppTableItemView.MaxPurchasePct = creditAppTable.MaxPurchasePct;
				creditAppTableItemView.MaxRetentionAmount = creditAppTable.MaxRetentionAmount;
				creditAppTableItemView.MaxRetentionPct = creditAppTable.MaxRetentionPct;
				creditAppTableItemView.PDCBankGUID = creditAppTable.PDCBankGUID.GuidNullToString();
				creditAppTableItemView.ProductSubTypeGUID = creditAppTable.ProductSubTypeGUID.GuidNullToString();
				creditAppTableItemView.ProductType = creditAppTable.ProductType;
				creditAppTableItemView.PurchaseFeeCalculateBase = creditAppTable.PurchaseFeeCalculateBase;
				creditAppTableItemView.PurchaseFeePct = creditAppTable.PurchaseFeePct;
				creditAppTableItemView.ReceiptAddressGUID = creditAppTable.ReceiptAddressGUID.GuidNullToString();
				creditAppTableItemView.ReceiptContactPersonGUID = creditAppTable.ReceiptContactPersonGUID.GuidNullToString();
				creditAppTableItemView.RefCreditAppRequestTableGUID = creditAppTable.RefCreditAppRequestTableGUID.GuidNullToString();
				creditAppTableItemView.RegisteredAddressGUID = creditAppTable.RegisteredAddressGUID.GuidNullToString();
				creditAppTableItemView.Remark = creditAppTable.Remark;
				creditAppTableItemView.SalesAvgPerMonth = creditAppTable.SalesAvgPerMonth;
				creditAppTableItemView.StartDate = creditAppTable.StartDate.DateToString();
				creditAppTableItemView.TotalInterestPct = creditAppTable.TotalInterestPct;
				creditAppTableItemView.AssignmentMethodGUID = creditAppTable.AssignmentMethodGUID.GuidNullToString();
				creditAppTableItemView.PurchaseWithdrawalCondition = creditAppTable.PurchaseWithdrawalCondition;
				
				creditAppTableItemView.RowVersion = creditAppTable.RowVersion;
				return creditAppTableItemView.GetCreditAppTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CreditAppTableItemView
		#region ToDropDown
		public static SelectItem<CreditAppTableItemView> ToDropDownItem(this CreditAppTableItemView creditAppTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CreditAppTableItemView> selectItem = new SelectItem<CreditAppTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(creditAppTableView.CreditAppId, creditAppTableView.Description);
				selectItem.Value = creditAppTableView.CreditAppTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: creditAppTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CreditAppTableItemView>> ToDropDownItem(this IEnumerable<CreditAppTableItemView> creditAppTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CreditAppTableItemView>> selectItems = new List<SelectItem<CreditAppTableItemView>>();
				foreach (CreditAppTableItemView item in creditAppTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

