using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class WithholdingTaxGroupHandler
	{
		#region WithholdingTaxGroupListView
		public static List<WithholdingTaxGroupListView> GetWithholdingTaxGroupListViewValidation(this List<WithholdingTaxGroupListView> withholdingTaxGroupListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return withholdingTaxGroupListViews;
			}
			var result = new List<WithholdingTaxGroupListView>();
			try
			{
				foreach (WithholdingTaxGroupListView item in withholdingTaxGroupListViews)
				{
					result.Add(item.GetWithholdingTaxGroupListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static WithholdingTaxGroupListView GetWithholdingTaxGroupListViewValidation(this WithholdingTaxGroupListView withholdingTaxGroupListView)
		{
			try
			{
				withholdingTaxGroupListView.RowAuthorize = withholdingTaxGroupListView.GetListRowAuthorize();
				return withholdingTaxGroupListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this WithholdingTaxGroupListView withholdingTaxGroupListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion WithholdingTaxGroupListView
		#region WithholdingTaxGroupItemView
		public static WithholdingTaxGroupItemView GetWithholdingTaxGroupItemViewValidation(this WithholdingTaxGroupItemView withholdingTaxGroupItemView)
		{
			try
			{
				withholdingTaxGroupItemView.RowAuthorize = withholdingTaxGroupItemView.GetItemRowAuthorize();
				return withholdingTaxGroupItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this WithholdingTaxGroupItemView withholdingTaxGroupItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static WithholdingTaxGroup ToWithholdingTaxGroup(this WithholdingTaxGroupItemView withholdingTaxGroupItemView)
		{
			try
			{
				WithholdingTaxGroup withholdingTaxGroup = new WithholdingTaxGroup();
				withholdingTaxGroup.CompanyGUID = withholdingTaxGroupItemView.CompanyGUID.StringToGuid();
				withholdingTaxGroup.CreatedBy = withholdingTaxGroupItemView.CreatedBy;
				withholdingTaxGroup.CreatedDateTime = withholdingTaxGroupItemView.CreatedDateTime.StringToSystemDateTime();
				withholdingTaxGroup.ModifiedBy = withholdingTaxGroupItemView.ModifiedBy;
				withholdingTaxGroup.ModifiedDateTime = withholdingTaxGroupItemView.ModifiedDateTime.StringToSystemDateTime();
				withholdingTaxGroup.Owner = withholdingTaxGroupItemView.Owner;
				withholdingTaxGroup.OwnerBusinessUnitGUID = withholdingTaxGroupItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				withholdingTaxGroup.WithholdingTaxGroupGUID = withholdingTaxGroupItemView.WithholdingTaxGroupGUID.StringToGuid();
				withholdingTaxGroup.Description = withholdingTaxGroupItemView.Description;
				withholdingTaxGroup.WHTGroupId = withholdingTaxGroupItemView.WHTGroupId;
				
				withholdingTaxGroup.RowVersion = withholdingTaxGroupItemView.RowVersion;
				return withholdingTaxGroup;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<WithholdingTaxGroup> ToWithholdingTaxGroup(this IEnumerable<WithholdingTaxGroupItemView> withholdingTaxGroupItemViews)
		{
			try
			{
				List<WithholdingTaxGroup> withholdingTaxGroups = new List<WithholdingTaxGroup>();
				foreach (WithholdingTaxGroupItemView item in withholdingTaxGroupItemViews)
				{
					withholdingTaxGroups.Add(item.ToWithholdingTaxGroup());
				}
				return withholdingTaxGroups;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static WithholdingTaxGroupItemView ToWithholdingTaxGroupItemView(this WithholdingTaxGroup withholdingTaxGroup)
		{
			try
			{
				WithholdingTaxGroupItemView withholdingTaxGroupItemView = new WithholdingTaxGroupItemView();
				withholdingTaxGroupItemView.CompanyGUID = withholdingTaxGroup.CompanyGUID.GuidNullToString();
				withholdingTaxGroupItemView.CreatedBy = withholdingTaxGroup.CreatedBy;
				withholdingTaxGroupItemView.CreatedDateTime = withholdingTaxGroup.CreatedDateTime.DateTimeToString();
				withholdingTaxGroupItemView.ModifiedBy = withholdingTaxGroup.ModifiedBy;
				withholdingTaxGroupItemView.ModifiedDateTime = withholdingTaxGroup.ModifiedDateTime.DateTimeToString();
				withholdingTaxGroupItemView.Owner = withholdingTaxGroup.Owner;
				withholdingTaxGroupItemView.OwnerBusinessUnitGUID = withholdingTaxGroup.OwnerBusinessUnitGUID.GuidNullToString();
				withholdingTaxGroupItemView.WithholdingTaxGroupGUID = withholdingTaxGroup.WithholdingTaxGroupGUID.GuidNullToString();
				withholdingTaxGroupItemView.Description = withholdingTaxGroup.Description;
				withholdingTaxGroupItemView.WHTGroupId = withholdingTaxGroup.WHTGroupId;
				
				withholdingTaxGroupItemView.RowVersion = withholdingTaxGroup.RowVersion;
				return withholdingTaxGroupItemView.GetWithholdingTaxGroupItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion WithholdingTaxGroupItemView
		#region ToDropDown
		public static SelectItem<WithholdingTaxGroupItemView> ToDropDownItem(this WithholdingTaxGroupItemView withholdingTaxGroupView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<WithholdingTaxGroupItemView> selectItem = new SelectItem<WithholdingTaxGroupItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(withholdingTaxGroupView.WHTGroupId,withholdingTaxGroupView.Description);
				selectItem.Value = withholdingTaxGroupView.WithholdingTaxGroupGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: withholdingTaxGroupView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<WithholdingTaxGroupItemView>> ToDropDownItem(this IEnumerable<WithholdingTaxGroupItemView> withholdingTaxGroupItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<WithholdingTaxGroupItemView>> selectItems = new List<SelectItem<WithholdingTaxGroupItemView>>();
				foreach (WithholdingTaxGroupItemView item in withholdingTaxGroupItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

