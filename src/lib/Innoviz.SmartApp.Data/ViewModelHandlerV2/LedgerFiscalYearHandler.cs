using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class LedgerFiscalYearHandler
	{
		#region LedgerFiscalYearListView
		public static List<LedgerFiscalYearListView> GetLedgerFiscalYearListViewValidation(this List<LedgerFiscalYearListView> ledgerFiscalYearListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return ledgerFiscalYearListViews;
			}
			var result = new List<LedgerFiscalYearListView>();
			try
			{
				foreach (LedgerFiscalYearListView item in ledgerFiscalYearListViews)
				{
					result.Add(item.GetLedgerFiscalYearListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static LedgerFiscalYearListView GetLedgerFiscalYearListViewValidation(this LedgerFiscalYearListView ledgerFiscalYearListView)
		{
			try
			{
				ledgerFiscalYearListView.RowAuthorize = ledgerFiscalYearListView.GetListRowAuthorize();
				return ledgerFiscalYearListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this LedgerFiscalYearListView ledgerFiscalYearListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion LedgerFiscalYearListView
		#region LedgerFiscalYearItemView
		public static LedgerFiscalYearItemView GetLedgerFiscalYearItemViewValidation(this LedgerFiscalYearItemView ledgerFiscalYearItemView)
		{
			try
			{
				ledgerFiscalYearItemView.RowAuthorize = ledgerFiscalYearItemView.GetItemRowAuthorize();
				return ledgerFiscalYearItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this LedgerFiscalYearItemView ledgerFiscalYearItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static LedgerFiscalYear ToLedgerFiscalYear(this LedgerFiscalYearItemView ledgerFiscalYearItemView)
		{
			try
			{
				LedgerFiscalYear ledgerFiscalYear = new LedgerFiscalYear();
				ledgerFiscalYear.CompanyGUID = ledgerFiscalYearItemView.CompanyGUID.StringToGuid();
				ledgerFiscalYear.CreatedBy = ledgerFiscalYearItemView.CreatedBy;
				ledgerFiscalYear.CreatedDateTime = ledgerFiscalYearItemView.CreatedDateTime.StringToSystemDateTime();
				ledgerFiscalYear.ModifiedBy = ledgerFiscalYearItemView.ModifiedBy;
				ledgerFiscalYear.ModifiedDateTime = ledgerFiscalYearItemView.ModifiedDateTime.StringToSystemDateTime();
				ledgerFiscalYear.Owner = ledgerFiscalYearItemView.Owner;
				ledgerFiscalYear.OwnerBusinessUnitGUID = ledgerFiscalYearItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				ledgerFiscalYear.LedgerFiscalYearGUID = ledgerFiscalYearItemView.LedgerFiscalYearGUID.StringToGuid();
				ledgerFiscalYear.Description = ledgerFiscalYearItemView.Description;
				ledgerFiscalYear.EndDate = ledgerFiscalYearItemView.EndDate.StringToDate();
				ledgerFiscalYear.FiscalYearId = ledgerFiscalYearItemView.FiscalYearId;
				ledgerFiscalYear.LengthOfPeriod = ledgerFiscalYearItemView.LengthOfPeriod;
				ledgerFiscalYear.PeriodUnit = ledgerFiscalYearItemView.PeriodUnit;
				ledgerFiscalYear.StartDate = ledgerFiscalYearItemView.StartDate.StringToDate();
				
				ledgerFiscalYear.RowVersion = ledgerFiscalYearItemView.RowVersion;
				return ledgerFiscalYear;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<LedgerFiscalYear> ToLedgerFiscalYear(this IEnumerable<LedgerFiscalYearItemView> ledgerFiscalYearItemViews)
		{
			try
			{
				List<LedgerFiscalYear> ledgerFiscalYears = new List<LedgerFiscalYear>();
				foreach (LedgerFiscalYearItemView item in ledgerFiscalYearItemViews)
				{
					ledgerFiscalYears.Add(item.ToLedgerFiscalYear());
				}
				return ledgerFiscalYears;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static LedgerFiscalYearItemView ToLedgerFiscalYearItemView(this LedgerFiscalYear ledgerFiscalYear)
		{
			try
			{
				LedgerFiscalYearItemView ledgerFiscalYearItemView = new LedgerFiscalYearItemView();
				ledgerFiscalYearItemView.CompanyGUID = ledgerFiscalYear.CompanyGUID.GuidNullToString();
				ledgerFiscalYearItemView.CreatedBy = ledgerFiscalYear.CreatedBy;
				ledgerFiscalYearItemView.CreatedDateTime = ledgerFiscalYear.CreatedDateTime.DateTimeToString();
				ledgerFiscalYearItemView.ModifiedBy = ledgerFiscalYear.ModifiedBy;
				ledgerFiscalYearItemView.ModifiedDateTime = ledgerFiscalYear.ModifiedDateTime.DateTimeToString();
				ledgerFiscalYearItemView.Owner = ledgerFiscalYear.Owner;
				ledgerFiscalYearItemView.OwnerBusinessUnitGUID = ledgerFiscalYear.OwnerBusinessUnitGUID.GuidNullToString();
				ledgerFiscalYearItemView.LedgerFiscalYearGUID = ledgerFiscalYear.LedgerFiscalYearGUID.GuidNullToString();
				ledgerFiscalYearItemView.Description = ledgerFiscalYear.Description;
				ledgerFiscalYearItemView.EndDate = ledgerFiscalYear.EndDate.DateToString();
				ledgerFiscalYearItemView.FiscalYearId = ledgerFiscalYear.FiscalYearId;
				ledgerFiscalYearItemView.LengthOfPeriod = ledgerFiscalYear.LengthOfPeriod;
				ledgerFiscalYearItemView.PeriodUnit = ledgerFiscalYear.PeriodUnit;
				ledgerFiscalYearItemView.StartDate = ledgerFiscalYear.StartDate.DateToString();
				
				ledgerFiscalYearItemView.RowVersion = ledgerFiscalYear.RowVersion;
				return ledgerFiscalYearItemView.GetLedgerFiscalYearItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion LedgerFiscalYearItemView
		#region ToDropDown
		public static SelectItem<LedgerFiscalYearItemView> ToDropDownItem(this LedgerFiscalYearItemView ledgerFiscalYearView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<LedgerFiscalYearItemView> selectItem = new SelectItem<LedgerFiscalYearItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(ledgerFiscalYearView.FiscalYearId,ledgerFiscalYearView.Description);
				selectItem.Value = ledgerFiscalYearView.LedgerFiscalYearGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: ledgerFiscalYearView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<LedgerFiscalYearItemView>> ToDropDownItem(this IEnumerable<LedgerFiscalYearItemView> ledgerFiscalYearItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<LedgerFiscalYearItemView>> selectItems = new List<SelectItem<LedgerFiscalYearItemView>>();
				foreach (LedgerFiscalYearItemView item in ledgerFiscalYearItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

