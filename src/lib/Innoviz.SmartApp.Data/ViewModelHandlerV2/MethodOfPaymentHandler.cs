using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class MethodOfPaymentHandler
	{
		#region MethodOfPaymentListView
		public static List<MethodOfPaymentListView> GetMethodOfPaymentListViewValidation(this List<MethodOfPaymentListView> methodOfPaymentListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return methodOfPaymentListViews;
			}
			var result = new List<MethodOfPaymentListView>();
			try
			{
				foreach (MethodOfPaymentListView item in methodOfPaymentListViews)
				{
					result.Add(item.GetMethodOfPaymentListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static MethodOfPaymentListView GetMethodOfPaymentListViewValidation(this MethodOfPaymentListView methodOfPaymentListView)
		{
			try
			{
				methodOfPaymentListView.RowAuthorize = methodOfPaymentListView.GetListRowAuthorize();
				return methodOfPaymentListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this MethodOfPaymentListView methodOfPaymentListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion MethodOfPaymentListView
		#region MethodOfPaymentItemView
		public static MethodOfPaymentItemView GetMethodOfPaymentItemViewValidation(this MethodOfPaymentItemView methodOfPaymentItemView)
		{
			try
			{
				methodOfPaymentItemView.RowAuthorize = methodOfPaymentItemView.GetItemRowAuthorize();
				return methodOfPaymentItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this MethodOfPaymentItemView methodOfPaymentItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static MethodOfPayment ToMethodOfPayment(this MethodOfPaymentItemView methodOfPaymentItemView)
		{
			try
			{
				MethodOfPayment methodOfPayment = new MethodOfPayment();
				methodOfPayment.CompanyGUID = methodOfPaymentItemView.CompanyGUID.StringToGuid();
				methodOfPayment.CreatedBy = methodOfPaymentItemView.CreatedBy;
				methodOfPayment.CreatedDateTime = methodOfPaymentItemView.CreatedDateTime.StringToSystemDateTime();
				methodOfPayment.ModifiedBy = methodOfPaymentItemView.ModifiedBy;
				methodOfPayment.ModifiedDateTime = methodOfPaymentItemView.ModifiedDateTime.StringToSystemDateTime();
				methodOfPayment.Owner = methodOfPaymentItemView.Owner;
				methodOfPayment.OwnerBusinessUnitGUID = methodOfPaymentItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				methodOfPayment.MethodOfPaymentGUID = methodOfPaymentItemView.MethodOfPaymentGUID.StringToGuid();
				methodOfPayment.CompanyBankGUID = methodOfPaymentItemView.CompanyBankGUID.StringToGuidNull();
				methodOfPayment.Description = methodOfPaymentItemView.Description;
				methodOfPayment.MethodOfPaymentId = methodOfPaymentItemView.MethodOfPaymentId;
				methodOfPayment.PaymentRemark = methodOfPaymentItemView.PaymentRemark;
				methodOfPayment.PaymentType = methodOfPaymentItemView.PaymentType;
				methodOfPayment.AccountNum = methodOfPaymentItemView.AccountNum;
				methodOfPayment.AccountType = methodOfPaymentItemView.AccountType;
				
				methodOfPayment.RowVersion = methodOfPaymentItemView.RowVersion;
				return methodOfPayment;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<MethodOfPayment> ToMethodOfPayment(this IEnumerable<MethodOfPaymentItemView> methodOfPaymentItemViews)
		{
			try
			{
				List<MethodOfPayment> methodOfPayments = new List<MethodOfPayment>();
				foreach (MethodOfPaymentItemView item in methodOfPaymentItemViews)
				{
					methodOfPayments.Add(item.ToMethodOfPayment());
				}
				return methodOfPayments;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static MethodOfPaymentItemView ToMethodOfPaymentItemView(this MethodOfPayment methodOfPayment)
		{
			try
			{
				MethodOfPaymentItemView methodOfPaymentItemView = new MethodOfPaymentItemView();
				methodOfPaymentItemView.CompanyGUID = methodOfPayment.CompanyGUID.GuidNullToString();
				methodOfPaymentItemView.CreatedBy = methodOfPayment.CreatedBy;
				methodOfPaymentItemView.CreatedDateTime = methodOfPayment.CreatedDateTime.DateTimeToString();
				methodOfPaymentItemView.ModifiedBy = methodOfPayment.ModifiedBy;
				methodOfPaymentItemView.ModifiedDateTime = methodOfPayment.ModifiedDateTime.DateTimeToString();
				methodOfPaymentItemView.Owner = methodOfPayment.Owner;
				methodOfPaymentItemView.OwnerBusinessUnitGUID = methodOfPayment.OwnerBusinessUnitGUID.GuidNullToString();
				methodOfPaymentItemView.MethodOfPaymentGUID = methodOfPayment.MethodOfPaymentGUID.GuidNullToString();
				methodOfPaymentItemView.CompanyBankGUID = methodOfPayment.CompanyBankGUID.GuidNullToString();
				methodOfPaymentItemView.Description = methodOfPayment.Description;
				methodOfPaymentItemView.MethodOfPaymentId = methodOfPayment.MethodOfPaymentId;
				methodOfPaymentItemView.PaymentRemark = methodOfPayment.PaymentRemark;
				methodOfPaymentItemView.PaymentType = methodOfPayment.PaymentType;
				
				methodOfPaymentItemView.RowVersion = methodOfPayment.RowVersion;
				return methodOfPaymentItemView.GetMethodOfPaymentItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion MethodOfPaymentItemView
		#region ToDropDown
		public static SelectItem<MethodOfPaymentItemView> ToDropDownItem(this MethodOfPaymentItemView methodOfPaymentView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<MethodOfPaymentItemView> selectItem = new SelectItem<MethodOfPaymentItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(methodOfPaymentView.MethodOfPaymentId, methodOfPaymentView.Description);
				selectItem.Value = methodOfPaymentView.MethodOfPaymentGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: methodOfPaymentView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<MethodOfPaymentItemView>> ToDropDownItem(this IEnumerable<MethodOfPaymentItemView> methodOfPaymentItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<MethodOfPaymentItemView>> selectItems = new List<SelectItem<MethodOfPaymentItemView>>();
				foreach (MethodOfPaymentItemView item in methodOfPaymentItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

