using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class VerificationTableHandler
	{
		#region VerificationTableListView
		public static List<VerificationTableListView> GetVerificationTableListViewValidation(this List<VerificationTableListView> verificationTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return verificationTableListViews;
			}
			var result = new List<VerificationTableListView>();
			try
			{
				foreach (VerificationTableListView item in verificationTableListViews)
				{
					result.Add(item.GetVerificationTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static VerificationTableListView GetVerificationTableListViewValidation(this VerificationTableListView verificationTableListView)
		{
			try
			{
				verificationTableListView.RowAuthorize = verificationTableListView.GetListRowAuthorize();
				return verificationTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this VerificationTableListView verificationTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion VerificationTableListView
		#region VerificationTableItemView
		public static VerificationTableItemView GetVerificationTableItemViewValidation(this VerificationTableItemView verificationTableItemView)
		{
			try
			{
				verificationTableItemView.RowAuthorize = verificationTableItemView.GetItemRowAuthorize();
				return verificationTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this VerificationTableItemView verificationTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static VerificationTable ToVerificationTable(this VerificationTableItemView verificationTableItemView)
		{
			try
			{
				VerificationTable verificationTable = new VerificationTable();
				verificationTable.CompanyGUID = verificationTableItemView.CompanyGUID.StringToGuid();
				verificationTable.CreatedBy = verificationTableItemView.CreatedBy;
				verificationTable.CreatedDateTime = verificationTableItemView.CreatedDateTime.StringToSystemDateTime();
				verificationTable.ModifiedBy = verificationTableItemView.ModifiedBy;
				verificationTable.ModifiedDateTime = verificationTableItemView.ModifiedDateTime.StringToSystemDateTime();
				verificationTable.Owner = verificationTableItemView.Owner;
				verificationTable.OwnerBusinessUnitGUID = verificationTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				verificationTable.VerificationTableGUID = verificationTableItemView.VerificationTableGUID.StringToGuid();
				verificationTable.BuyerTableGUID = verificationTableItemView.BuyerTableGUID.StringToGuid();
				verificationTable.CreditAppTableGUID = verificationTableItemView.CreditAppTableGUID.StringToGuid();
				verificationTable.CustomerTableGUID = verificationTableItemView.CustomerTableGUID.StringToGuid();
				verificationTable.Description = verificationTableItemView.Description;
				verificationTable.DocumentStatusGUID = verificationTableItemView.DocumentStatusGUID.StringToGuid();
				verificationTable.Remark = verificationTableItemView.Remark;
				verificationTable.VerificationDate = verificationTableItemView.VerificationDate.StringToDate();
				verificationTable.VerificationId = verificationTableItemView.VerificationId;
				
				verificationTable.RowVersion = verificationTableItemView.RowVersion;
				return verificationTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<VerificationTable> ToVerificationTable(this IEnumerable<VerificationTableItemView> verificationTableItemViews)
		{
			try
			{
				List<VerificationTable> verificationTables = new List<VerificationTable>();
				foreach (VerificationTableItemView item in verificationTableItemViews)
				{
					verificationTables.Add(item.ToVerificationTable());
				}
				return verificationTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static VerificationTableItemView ToVerificationTableItemView(this VerificationTable verificationTable)
		{
			try
			{
				VerificationTableItemView verificationTableItemView = new VerificationTableItemView();
				verificationTableItemView.CompanyGUID = verificationTable.CompanyGUID.GuidNullToString();
				verificationTableItemView.CreatedBy = verificationTable.CreatedBy;
				verificationTableItemView.CreatedDateTime = verificationTable.CreatedDateTime.DateTimeToString();
				verificationTableItemView.ModifiedBy = verificationTable.ModifiedBy;
				verificationTableItemView.ModifiedDateTime = verificationTable.ModifiedDateTime.DateTimeToString();
				verificationTableItemView.Owner = verificationTable.Owner;
				verificationTableItemView.OwnerBusinessUnitGUID = verificationTable.OwnerBusinessUnitGUID.GuidNullToString();
				verificationTableItemView.VerificationTableGUID = verificationTable.VerificationTableGUID.GuidNullToString();
				verificationTableItemView.BuyerTableGUID = verificationTable.BuyerTableGUID.GuidNullToString();
				verificationTableItemView.CreditAppTableGUID = verificationTable.CreditAppTableGUID.GuidNullToString();
				verificationTableItemView.CustomerTableGUID = verificationTable.CustomerTableGUID.GuidNullToString();
				verificationTableItemView.Description = verificationTable.Description;
				verificationTableItemView.DocumentStatusGUID = verificationTable.DocumentStatusGUID.GuidNullToString();
				verificationTableItemView.Remark = verificationTable.Remark;
				verificationTableItemView.VerificationDate = verificationTable.VerificationDate.DateToString();
				verificationTableItemView.VerificationId = verificationTable.VerificationId;
				
				verificationTableItemView.RowVersion = verificationTable.RowVersion;
				return verificationTableItemView.GetVerificationTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion VerificationTableItemView
		#region ToDropDown
		public static SelectItem<VerificationTableItemView> ToDropDownItem(this VerificationTableItemView verificationTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<VerificationTableItemView> selectItem = new SelectItem<VerificationTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(verificationTableView.VerificationId, verificationTableView.Description);
				selectItem.Value = verificationTableView.VerificationTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: verificationTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<VerificationTableItemView>> ToDropDownItem(this IEnumerable<VerificationTableItemView> verificationTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<VerificationTableItemView>> selectItems = new List<SelectItem<VerificationTableItemView>>();
				foreach (VerificationTableItemView item in verificationTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

