using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class InterestTypeHandler
	{
		#region InterestTypeListView
		public static List<InterestTypeListView> GetInterestTypeListViewValidation(this List<InterestTypeListView> interestTypeListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return interestTypeListViews;
			}
			var result = new List<InterestTypeListView>();
			try
			{
				foreach (InterestTypeListView item in interestTypeListViews)
				{
					result.Add(item.GetInterestTypeListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static InterestTypeListView GetInterestTypeListViewValidation(this InterestTypeListView interestTypeListView)
		{
			try
			{
				interestTypeListView.RowAuthorize = interestTypeListView.GetListRowAuthorize();
				return interestTypeListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this InterestTypeListView interestTypeListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion InterestTypeListView
		#region InterestTypeItemView
		public static InterestTypeItemView GetInterestTypeItemViewValidation(this InterestTypeItemView interestTypeItemView)
		{
			try
			{
				interestTypeItemView.RowAuthorize = interestTypeItemView.GetItemRowAuthorize();
				return interestTypeItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this InterestTypeItemView interestTypeItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static InterestType ToInterestType(this InterestTypeItemView interestTypeItemView)
		{
			try
			{
				InterestType interestType = new InterestType();
				interestType.CompanyGUID = interestTypeItemView.CompanyGUID.StringToGuid();
				interestType.CreatedBy = interestTypeItemView.CreatedBy;
				interestType.CreatedDateTime = interestTypeItemView.CreatedDateTime.StringToSystemDateTime();
				interestType.ModifiedBy = interestTypeItemView.ModifiedBy;
				interestType.ModifiedDateTime = interestTypeItemView.ModifiedDateTime.StringToSystemDateTime();
				interestType.Owner = interestTypeItemView.Owner;
				interestType.OwnerBusinessUnitGUID = interestTypeItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				interestType.InterestTypeGUID = interestTypeItemView.InterestTypeGUID.StringToGuid();
				interestType.Description = interestTypeItemView.Description;
				interestType.InterestTypeId = interestTypeItemView.InterestTypeId;
				
				interestType.RowVersion = interestTypeItemView.RowVersion;
				return interestType;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<InterestType> ToInterestType(this IEnumerable<InterestTypeItemView> interestTypeItemViews)
		{
			try
			{
				List<InterestType> interestTypes = new List<InterestType>();
				foreach (InterestTypeItemView item in interestTypeItemViews)
				{
					interestTypes.Add(item.ToInterestType());
				}
				return interestTypes;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static InterestTypeItemView ToInterestTypeItemView(this InterestType interestType)
		{
			try
			{
				InterestTypeItemView interestTypeItemView = new InterestTypeItemView();
				interestTypeItemView.CompanyGUID = interestType.CompanyGUID.GuidNullToString();
				interestTypeItemView.CreatedBy = interestType.CreatedBy;
				interestTypeItemView.CreatedDateTime = interestType.CreatedDateTime.DateTimeToString();
				interestTypeItemView.ModifiedBy = interestType.ModifiedBy;
				interestTypeItemView.ModifiedDateTime = interestType.ModifiedDateTime.DateTimeToString();
				interestTypeItemView.Owner = interestType.Owner;
				interestTypeItemView.OwnerBusinessUnitGUID = interestType.OwnerBusinessUnitGUID.GuidNullToString();
				interestTypeItemView.InterestTypeGUID = interestType.InterestTypeGUID.GuidNullToString();
				interestTypeItemView.Description = interestType.Description;
				interestTypeItemView.InterestTypeId = interestType.InterestTypeId;
				
				interestTypeItemView.RowVersion = interestType.RowVersion;
				return interestTypeItemView.GetInterestTypeItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion InterestTypeItemView
		#region ToDropDown
		public static SelectItem<InterestTypeItemView> ToDropDownItem(this InterestTypeItemView interestTypeView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<InterestTypeItemView> selectItem = new SelectItem<InterestTypeItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(interestTypeView.InterestTypeId, interestTypeView.Description);
				selectItem.Value = interestTypeView.InterestTypeGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: interestTypeView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<InterestTypeItemView>> ToDropDownItem(this IEnumerable<InterestTypeItemView> interestTypeItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<InterestTypeItemView>> selectItems = new List<SelectItem<InterestTypeItemView>>();
				foreach (InterestTypeItemView item in interestTypeItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

