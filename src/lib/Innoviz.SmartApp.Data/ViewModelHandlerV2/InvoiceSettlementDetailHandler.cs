using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class InvoiceSettlementDetailHandler
	{
		#region InvoiceSettlementDetailListView
		public static List<InvoiceSettlementDetailListView> GetInvoiceSettlementDetailListViewValidation(this List<InvoiceSettlementDetailListView> invoiceSettlementDetailListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return invoiceSettlementDetailListViews;
			}
			var result = new List<InvoiceSettlementDetailListView>();
			try
			{
				foreach (InvoiceSettlementDetailListView item in invoiceSettlementDetailListViews)
				{
					result.Add(item.GetInvoiceSettlementDetailListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static InvoiceSettlementDetailListView GetInvoiceSettlementDetailListViewValidation(this InvoiceSettlementDetailListView invoiceSettlementDetailListView)
		{
			try
			{
				invoiceSettlementDetailListView.RowAuthorize = invoiceSettlementDetailListView.GetListRowAuthorize();
				return invoiceSettlementDetailListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this InvoiceSettlementDetailListView invoiceSettlementDetailListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion InvoiceSettlementDetailListView
		#region InvoiceSettlementDetailItemView
		public static InvoiceSettlementDetailItemView GetInvoiceSettlementDetailItemViewValidation(this InvoiceSettlementDetailItemView invoiceSettlementDetailItemView)
		{
			try
			{
				invoiceSettlementDetailItemView.RowAuthorize = invoiceSettlementDetailItemView.GetItemRowAuthorize();
				return invoiceSettlementDetailItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this InvoiceSettlementDetailItemView invoiceSettlementDetailItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static InvoiceSettlementDetail ToInvoiceSettlementDetail(this InvoiceSettlementDetailItemView invoiceSettlementDetailItemView)
		{
			try
			{
				InvoiceSettlementDetail invoiceSettlementDetail = new InvoiceSettlementDetail();
				invoiceSettlementDetail.CompanyGUID = invoiceSettlementDetailItemView.CompanyGUID.StringToGuid();
				invoiceSettlementDetail.CreatedBy = invoiceSettlementDetailItemView.CreatedBy;
				invoiceSettlementDetail.CreatedDateTime = invoiceSettlementDetailItemView.CreatedDateTime.StringToSystemDateTime();
				invoiceSettlementDetail.ModifiedBy = invoiceSettlementDetailItemView.ModifiedBy;
				invoiceSettlementDetail.ModifiedDateTime = invoiceSettlementDetailItemView.ModifiedDateTime.StringToSystemDateTime();
				invoiceSettlementDetail.Owner = invoiceSettlementDetailItemView.Owner;
				invoiceSettlementDetail.OwnerBusinessUnitGUID = invoiceSettlementDetailItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				invoiceSettlementDetail.InvoiceSettlementDetailGUID = invoiceSettlementDetailItemView.InvoiceSettlementDetailGUID.StringToGuid();
				invoiceSettlementDetail.AssignmentAgreementTableGUID = invoiceSettlementDetailItemView.AssignmentAgreementTableGUID.StringToGuidNull();
				invoiceSettlementDetail.BalanceAmount = invoiceSettlementDetailItemView.BalanceAmount;
				invoiceSettlementDetail.BuyerAgreementTableGUID = invoiceSettlementDetailItemView.BuyerAgreementTableGUID.StringToGuidNull();
				invoiceSettlementDetail.DocumentId = invoiceSettlementDetailItemView.DocumentId;
				invoiceSettlementDetail.InterestCalculationMethod = invoiceSettlementDetailItemView.InterestCalculationMethod;
				invoiceSettlementDetail.InvoiceAmount = invoiceSettlementDetailItemView.InvoiceAmount;
				invoiceSettlementDetail.InvoiceDueDate = invoiceSettlementDetailItemView.InvoiceDueDate.StringNullToDateNull();
				invoiceSettlementDetail.InvoiceTableGUID = invoiceSettlementDetailItemView.InvoiceTableGUID.StringToGuidNull();
				invoiceSettlementDetail.InvoiceTypeGUID = invoiceSettlementDetailItemView.InvoiceTypeGUID.StringToGuidNull();
				invoiceSettlementDetail.ProductType = invoiceSettlementDetailItemView.ProductType;
				invoiceSettlementDetail.PurchaseAmount = invoiceSettlementDetailItemView.PurchaseAmount;
				invoiceSettlementDetail.PurchaseAmountBalance = invoiceSettlementDetailItemView.PurchaseAmountBalance;
				invoiceSettlementDetail.RefGUID = invoiceSettlementDetailItemView.RefGUID.StringToGuidNull();
				invoiceSettlementDetail.RefReceiptTempPaymDetailGUID = invoiceSettlementDetailItemView.RefReceiptTempPaymDetailGUID.StringToGuidNull();
				invoiceSettlementDetail.RefType = invoiceSettlementDetailItemView.RefType;
				invoiceSettlementDetail.RetentionAmountAccum = invoiceSettlementDetailItemView.RetentionAmountAccum;
				invoiceSettlementDetail.SettleAmount = invoiceSettlementDetailItemView.SettleAmount;
				invoiceSettlementDetail.SettleAssignmentAmount = invoiceSettlementDetailItemView.SettleAssignmentAmount;
				invoiceSettlementDetail.SettleInvoiceAmount = invoiceSettlementDetailItemView.SettleInvoiceAmount;
				invoiceSettlementDetail.SettlePurchaseAmount = invoiceSettlementDetailItemView.SettlePurchaseAmount;
				invoiceSettlementDetail.SettleReserveAmount = invoiceSettlementDetailItemView.SettleReserveAmount;
				invoiceSettlementDetail.SettleTaxAmount = invoiceSettlementDetailItemView.SettleTaxAmount;
				invoiceSettlementDetail.SuspenseInvoiceType = invoiceSettlementDetailItemView.SuspenseInvoiceType;
				invoiceSettlementDetail.WHTAmount = invoiceSettlementDetailItemView.WHTAmount;
				invoiceSettlementDetail.WHTSlipReceivedByBuyer = invoiceSettlementDetailItemView.WHTSlipReceivedByBuyer;
				invoiceSettlementDetail.WHTSlipReceivedByCustomer = invoiceSettlementDetailItemView.WHTSlipReceivedByCustomer;
				
				invoiceSettlementDetail.RowVersion = invoiceSettlementDetailItemView.RowVersion;
				return invoiceSettlementDetail;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<InvoiceSettlementDetail> ToInvoiceSettlementDetail(this IEnumerable<InvoiceSettlementDetailItemView> invoiceSettlementDetailItemViews)
		{
			try
			{
				List<InvoiceSettlementDetail> invoiceSettlementDetails = new List<InvoiceSettlementDetail>();
				foreach (InvoiceSettlementDetailItemView item in invoiceSettlementDetailItemViews)
				{
					invoiceSettlementDetails.Add(item.ToInvoiceSettlementDetail());
				}
				return invoiceSettlementDetails;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static InvoiceSettlementDetailItemView ToInvoiceSettlementDetailItemView(this InvoiceSettlementDetail invoiceSettlementDetail)
		{
			try
			{
				InvoiceSettlementDetailItemView invoiceSettlementDetailItemView = new InvoiceSettlementDetailItemView();
				invoiceSettlementDetailItemView.CompanyGUID = invoiceSettlementDetail.CompanyGUID.GuidNullToString();
				invoiceSettlementDetailItemView.CreatedBy = invoiceSettlementDetail.CreatedBy;
				invoiceSettlementDetailItemView.CreatedDateTime = invoiceSettlementDetail.CreatedDateTime.DateTimeToString();
				invoiceSettlementDetailItemView.ModifiedBy = invoiceSettlementDetail.ModifiedBy;
				invoiceSettlementDetailItemView.ModifiedDateTime = invoiceSettlementDetail.ModifiedDateTime.DateTimeToString();
				invoiceSettlementDetailItemView.Owner = invoiceSettlementDetail.Owner;
				invoiceSettlementDetailItemView.OwnerBusinessUnitGUID = invoiceSettlementDetail.OwnerBusinessUnitGUID.GuidNullToString();
				invoiceSettlementDetailItemView.InvoiceSettlementDetailGUID = invoiceSettlementDetail.InvoiceSettlementDetailGUID.GuidNullToString();
				invoiceSettlementDetailItemView.AssignmentAgreementTableGUID = invoiceSettlementDetail.AssignmentAgreementTableGUID.GuidNullToString();
				invoiceSettlementDetailItemView.BalanceAmount = invoiceSettlementDetail.BalanceAmount;
				invoiceSettlementDetailItemView.BuyerAgreementTableGUID = invoiceSettlementDetail.BuyerAgreementTableGUID.GuidNullToString();
				invoiceSettlementDetailItemView.DocumentId = invoiceSettlementDetail.DocumentId;
				invoiceSettlementDetailItemView.InterestCalculationMethod = invoiceSettlementDetail.InterestCalculationMethod;
				invoiceSettlementDetailItemView.InvoiceAmount = invoiceSettlementDetail.InvoiceAmount;
				invoiceSettlementDetailItemView.InvoiceDueDate = invoiceSettlementDetail.InvoiceDueDate.DateNullToString();
				invoiceSettlementDetailItemView.InvoiceTableGUID = invoiceSettlementDetail.InvoiceTableGUID.GuidNullToString();
				invoiceSettlementDetailItemView.InvoiceTypeGUID = invoiceSettlementDetail.InvoiceTypeGUID.GuidNullToString();
				invoiceSettlementDetailItemView.ProductType = invoiceSettlementDetail.ProductType;
				invoiceSettlementDetailItemView.PurchaseAmount = invoiceSettlementDetail.PurchaseAmount;
				invoiceSettlementDetailItemView.PurchaseAmountBalance = invoiceSettlementDetail.PurchaseAmountBalance;
				invoiceSettlementDetailItemView.RefGUID = invoiceSettlementDetail.RefGUID.GuidNullToString();
				invoiceSettlementDetailItemView.RefReceiptTempPaymDetailGUID = invoiceSettlementDetail.RefReceiptTempPaymDetailGUID.GuidNullToString();
				invoiceSettlementDetailItemView.RefType = invoiceSettlementDetail.RefType;
				invoiceSettlementDetailItemView.RetentionAmountAccum = invoiceSettlementDetail.RetentionAmountAccum;
				invoiceSettlementDetailItemView.SettleAmount = invoiceSettlementDetail.SettleAmount;
				invoiceSettlementDetailItemView.SettleAssignmentAmount = invoiceSettlementDetail.SettleAssignmentAmount;
				invoiceSettlementDetailItemView.SettleInvoiceAmount = invoiceSettlementDetail.SettleInvoiceAmount;
				invoiceSettlementDetailItemView.SettlePurchaseAmount = invoiceSettlementDetail.SettlePurchaseAmount;
				invoiceSettlementDetailItemView.SettleReserveAmount = invoiceSettlementDetail.SettleReserveAmount;
				invoiceSettlementDetailItemView.SettleTaxAmount = invoiceSettlementDetail.SettleTaxAmount;
				invoiceSettlementDetailItemView.SuspenseInvoiceType = invoiceSettlementDetail.SuspenseInvoiceType;
				invoiceSettlementDetailItemView.WHTAmount = invoiceSettlementDetail.WHTAmount;
				invoiceSettlementDetailItemView.WHTSlipReceivedByBuyer = invoiceSettlementDetail.WHTSlipReceivedByBuyer;
				invoiceSettlementDetailItemView.WHTSlipReceivedByCustomer = invoiceSettlementDetail.WHTSlipReceivedByCustomer;
				
				invoiceSettlementDetailItemView.RowVersion = invoiceSettlementDetail.RowVersion;
				return invoiceSettlementDetailItemView.GetInvoiceSettlementDetailItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion InvoiceSettlementDetailItemView
		#region ToDropDown
		public static SelectItem<InvoiceSettlementDetailItemView> ToDropDownItem(this InvoiceSettlementDetailItemView invoiceSettlementDetailView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<InvoiceSettlementDetailItemView> selectItem = new SelectItem<InvoiceSettlementDetailItemView>();
				selectItem.Label = invoiceSettlementDetailView.InvoiceSettlementDetail_Values;
				selectItem.Value = invoiceSettlementDetailView.InvoiceSettlementDetailGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: invoiceSettlementDetailView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<InvoiceSettlementDetailItemView>> ToDropDownItem(this IEnumerable<InvoiceSettlementDetailItemView> invoiceSettlementDetailItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<InvoiceSettlementDetailItemView>> selectItems = new List<SelectItem<InvoiceSettlementDetailItemView>>();
				foreach (InvoiceSettlementDetailItemView item in invoiceSettlementDetailItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

