using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class StagingTableHandler
	{
		#region StagingTableListView
		public static List<StagingTableListView> GetStagingTableListViewValidation(this List<StagingTableListView> stagingTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return stagingTableListViews;
			}
			var result = new List<StagingTableListView>();
			try
			{
				foreach (StagingTableListView item in stagingTableListViews)
				{
					result.Add(item.GetStagingTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static StagingTableListView GetStagingTableListViewValidation(this StagingTableListView stagingTableListView)
		{
			try
			{
				stagingTableListView.RowAuthorize = stagingTableListView.GetListRowAuthorize();
				return stagingTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this StagingTableListView stagingTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion StagingTableListView
		#region StagingTableItemView
		public static StagingTableItemView GetStagingTableItemViewValidation(this StagingTableItemView stagingTableItemView)
		{
			try
			{
				stagingTableItemView.RowAuthorize = stagingTableItemView.GetItemRowAuthorize();
				return stagingTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this StagingTableItemView stagingTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static StagingTable ToStagingTable(this StagingTableItemView stagingTableItemView)
		{
			try
			{
				StagingTable stagingTable = new StagingTable();
				stagingTable.CompanyGUID = stagingTableItemView.CompanyGUID.StringToGuid();
				stagingTable.CreatedBy = stagingTableItemView.CreatedBy;
				stagingTable.CreatedDateTime = stagingTableItemView.CreatedDateTime.StringToSystemDateTime();
				stagingTable.ModifiedBy = stagingTableItemView.ModifiedBy;
				stagingTable.ModifiedDateTime = stagingTableItemView.ModifiedDateTime.StringToSystemDateTime();
				stagingTable.Owner = stagingTableItemView.Owner;
				stagingTable.OwnerBusinessUnitGUID = stagingTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				stagingTable.StagingTableGUID = stagingTableItemView.StagingTableGUID.StringToGuid();
				stagingTable.AccountNum = stagingTableItemView.AccountNum;
				stagingTable.AccountType = stagingTableItemView.AccountType;
				stagingTable.AmountMST = stagingTableItemView.AmountMST;
				stagingTable.CompanyId = stagingTableItemView.StagingTableCompanyId;
				stagingTable.CompanyTaxBranchId = stagingTableItemView.CompanyTaxBranchId;
				stagingTable.DimensionCode1 = stagingTableItemView.DimensionCode1;
				stagingTable.DimensionCode2 = stagingTableItemView.DimensionCode2;
				stagingTable.DimensionCode3 = stagingTableItemView.DimensionCode3;
				stagingTable.DimensionCode4 = stagingTableItemView.DimensionCode4;
				stagingTable.DimensionCode5 = stagingTableItemView.DimensionCode5;
				stagingTable.DocumentId = stagingTableItemView.DocumentId;
				stagingTable.InterfaceStagingBatchId = stagingTableItemView.InterfaceStagingBatchId;
				stagingTable.InterfaceStatus = stagingTableItemView.InterfaceStatus;
				stagingTable.ProcessTransGUID = stagingTableItemView.ProcessTransGUID.StringToGuidNull();
				stagingTable.ProcessTransType = stagingTableItemView.ProcessTransType;
				stagingTable.ProductType = stagingTableItemView.ProductType;
				stagingTable.RefGUID = stagingTableItemView.RefGUID.StringToGuidNull();
				stagingTable.RefType = stagingTableItemView.RefType;
				stagingTable.StagingBatchId = stagingTableItemView.StagingBatchId;
				stagingTable.TaxAccountId = stagingTableItemView.TaxAccountId;
				stagingTable.TaxAccountName = stagingTableItemView.TaxAccountName;
				stagingTable.TaxAddress = stagingTableItemView.TaxAddress;
				stagingTable.TaxBaseAmount = stagingTableItemView.TaxBaseAmount;
				stagingTable.TaxBranchId = stagingTableItemView.TaxBranchId;
				stagingTable.TaxCode = stagingTableItemView.TaxCode;
				stagingTable.TaxId = stagingTableItemView.TaxId;
				stagingTable.TaxInvoiceId = stagingTableItemView.TaxInvoiceId;
				stagingTable.TransDate = stagingTableItemView.TransDate.StringToDate();
				stagingTable.TransText = stagingTableItemView.TransText;
				stagingTable.VendorTaxInvoiceId = stagingTableItemView.VendorTaxInvoiceId;
				stagingTable.WHTCode = stagingTableItemView.WHTCode;
				stagingTable.SourceRefType = stagingTableItemView.SourceRefType;
				stagingTable.SourceRefId = stagingTableItemView.SourceRefId;
				
				stagingTable.RowVersion = stagingTableItemView.RowVersion;
				return stagingTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<StagingTable> ToStagingTable(this IEnumerable<StagingTableItemView> stagingTableItemViews)
		{
			try
			{
				List<StagingTable> stagingTables = new List<StagingTable>();
				foreach (StagingTableItemView item in stagingTableItemViews)
				{
					stagingTables.Add(item.ToStagingTable());
				}
				return stagingTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static StagingTableItemView ToStagingTableItemView(this StagingTable stagingTable)
		{
			try
			{
				StagingTableItemView stagingTableItemView = new StagingTableItemView();
				stagingTableItemView.CompanyGUID = stagingTable.CompanyGUID.GuidNullToString();
				stagingTableItemView.CreatedBy = stagingTable.CreatedBy;
				stagingTableItemView.CreatedDateTime = stagingTable.CreatedDateTime.DateTimeToString();
				stagingTableItemView.ModifiedBy = stagingTable.ModifiedBy;
				stagingTableItemView.ModifiedDateTime = stagingTable.ModifiedDateTime.DateTimeToString();
				stagingTableItemView.Owner = stagingTable.Owner;
				stagingTableItemView.OwnerBusinessUnitGUID = stagingTable.OwnerBusinessUnitGUID.GuidNullToString();
				stagingTableItemView.StagingTableGUID = stagingTable.StagingTableGUID.GuidNullToString();
				stagingTableItemView.AccountNum = stagingTable.AccountNum;
				stagingTableItemView.AccountType = stagingTable.AccountType;
				stagingTableItemView.AmountMST = stagingTable.AmountMST;
				stagingTableItemView.StagingTableCompanyId = stagingTable.CompanyId;
				stagingTableItemView.CompanyTaxBranchId = stagingTable.CompanyTaxBranchId;
				stagingTableItemView.DimensionCode1 = stagingTable.DimensionCode1;
				stagingTableItemView.DimensionCode2 = stagingTable.DimensionCode2;
				stagingTableItemView.DimensionCode3 = stagingTable.DimensionCode3;
				stagingTableItemView.DimensionCode4 = stagingTable.DimensionCode4;
				stagingTableItemView.DimensionCode5 = stagingTable.DimensionCode5;
				stagingTableItemView.DocumentId = stagingTable.DocumentId;
				stagingTableItemView.InterfaceStagingBatchId = stagingTable.InterfaceStagingBatchId;
				stagingTableItemView.InterfaceStatus = stagingTable.InterfaceStatus;
				stagingTableItemView.ProcessTransGUID = stagingTable.ProcessTransGUID.GuidNullToString();
				stagingTableItemView.ProcessTransType = stagingTable.ProcessTransType;
				stagingTableItemView.ProductType = stagingTable.ProductType;
				stagingTableItemView.RefGUID = stagingTable.RefGUID.GuidNullToString();
				stagingTableItemView.RefType = stagingTable.RefType;
				stagingTableItemView.StagingBatchId = stagingTable.StagingBatchId;
				stagingTableItemView.TaxAccountId = stagingTable.TaxAccountId;
				stagingTableItemView.TaxAccountName = stagingTable.TaxAccountName;
				stagingTableItemView.TaxAddress = stagingTable.TaxAddress;
				stagingTableItemView.TaxBaseAmount = stagingTable.TaxBaseAmount;
				stagingTableItemView.TaxBranchId = stagingTable.TaxBranchId;
				stagingTableItemView.TaxCode = stagingTable.TaxCode;
				stagingTableItemView.TaxId = stagingTable.TaxId;
				stagingTableItemView.TaxInvoiceId = stagingTable.TaxInvoiceId;
				stagingTableItemView.TransDate = stagingTable.TransDate.DateToString();
				stagingTableItemView.TransText = stagingTable.TransText;
				stagingTableItemView.VendorTaxInvoiceId = stagingTable.VendorTaxInvoiceId;
				stagingTableItemView.WHTCode = stagingTable.WHTCode;
				stagingTableItemView.SourceRefType = stagingTable.SourceRefType;
				stagingTableItemView.SourceRefId = stagingTable.SourceRefId;
				
				stagingTableItemView.RowVersion = stagingTable.RowVersion;
				return stagingTableItemView.GetStagingTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion StagingTableItemView
		#region ToDropDown
		public static SelectItem<StagingTableItemView> ToDropDownItem(this StagingTableItemView stagingTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<StagingTableItemView> selectItem = new SelectItem<StagingTableItemView>();
				selectItem.Label = null;
				selectItem.Value = stagingTableView.StagingTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: stagingTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<StagingTableItemView>> ToDropDownItem(this IEnumerable<StagingTableItemView> stagingTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<StagingTableItemView>> selectItems = new List<SelectItem<StagingTableItemView>>();
				foreach (StagingTableItemView item in stagingTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

