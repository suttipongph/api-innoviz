using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class StagingTableIntercoInvSettleHandler
	{
		#region StagingTableIntercoInvSettleListView
		public static List<StagingTableIntercoInvSettleListView> GetStagingTableIntercoInvSettleListViewValidation(this List<StagingTableIntercoInvSettleListView> stagingTableIntercoInvSettleListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return stagingTableIntercoInvSettleListViews;
			}
			var result = new List<StagingTableIntercoInvSettleListView>();
			try
			{
				foreach (StagingTableIntercoInvSettleListView item in stagingTableIntercoInvSettleListViews)
				{
					result.Add(item.GetStagingTableIntercoInvSettleListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static StagingTableIntercoInvSettleListView GetStagingTableIntercoInvSettleListViewValidation(this StagingTableIntercoInvSettleListView stagingTableIntercoInvSettleListView)
		{
			try
			{
				stagingTableIntercoInvSettleListView.RowAuthorize = stagingTableIntercoInvSettleListView.GetListRowAuthorize();
				return stagingTableIntercoInvSettleListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this StagingTableIntercoInvSettleListView stagingTableIntercoInvSettleListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion StagingTableIntercoInvSettleListView
		#region StagingTableIntercoInvSettleItemView
		public static StagingTableIntercoInvSettleItemView GetStagingTableIntercoInvSettleItemViewValidation(this StagingTableIntercoInvSettleItemView stagingTableIntercoInvSettleItemView)
		{
			try
			{
				stagingTableIntercoInvSettleItemView.RowAuthorize = stagingTableIntercoInvSettleItemView.GetItemRowAuthorize();
				return stagingTableIntercoInvSettleItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this StagingTableIntercoInvSettleItemView stagingTableIntercoInvSettleItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static StagingTableIntercoInvSettle ToStagingTableIntercoInvSettle(this StagingTableIntercoInvSettleItemView stagingTableIntercoInvSettleItemView)
		{
			try
			{
				StagingTableIntercoInvSettle stagingTableIntercoInvSettle = new StagingTableIntercoInvSettle();
				stagingTableIntercoInvSettle.CompanyGUID = stagingTableIntercoInvSettleItemView.CompanyGUID.StringToGuid();
				stagingTableIntercoInvSettle.CreatedBy = stagingTableIntercoInvSettleItemView.CreatedBy;
				stagingTableIntercoInvSettle.CreatedDateTime = stagingTableIntercoInvSettleItemView.CreatedDateTime.StringToSystemDateTime();
				stagingTableIntercoInvSettle.ModifiedBy = stagingTableIntercoInvSettleItemView.ModifiedBy;
				stagingTableIntercoInvSettle.ModifiedDateTime = stagingTableIntercoInvSettleItemView.ModifiedDateTime.StringToSystemDateTime();
				stagingTableIntercoInvSettle.Owner = stagingTableIntercoInvSettleItemView.Owner;
				stagingTableIntercoInvSettle.OwnerBusinessUnitGUID = stagingTableIntercoInvSettleItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				stagingTableIntercoInvSettle.StagingTableIntercoInvSettleGUID = stagingTableIntercoInvSettleItemView.StagingTableIntercoInvSettleGUID.StringToGuid();
				stagingTableIntercoInvSettle.Address1 = stagingTableIntercoInvSettleItemView.Address1;
				stagingTableIntercoInvSettle.AltName = stagingTableIntercoInvSettleItemView.AltName;
				stagingTableIntercoInvSettle.CNReasonId = stagingTableIntercoInvSettleItemView.CNReasonId;
				stagingTableIntercoInvSettle.CompanyId = stagingTableIntercoInvSettleItemView.CompanyId;
				stagingTableIntercoInvSettle.CountryId = stagingTableIntercoInvSettleItemView.CountryId;
				stagingTableIntercoInvSettle.CurrencyId = stagingTableIntercoInvSettleItemView.CurrencyId;
				stagingTableIntercoInvSettle.CustFaxValue = stagingTableIntercoInvSettleItemView.CustFaxValue;
				stagingTableIntercoInvSettle.CustGroupId = stagingTableIntercoInvSettleItemView.CustGroupId;
				stagingTableIntercoInvSettle.CustomerId = stagingTableIntercoInvSettleItemView.CustomerId;
				stagingTableIntercoInvSettle.CompanyTaxBranchId = stagingTableIntercoInvSettleItemView.CompanyTaxBranchId;
				stagingTableIntercoInvSettle.CustPhoneValue = stagingTableIntercoInvSettleItemView.CustPhoneValue;
				stagingTableIntercoInvSettle.DimensionCode1 = stagingTableIntercoInvSettleItemView.DimensionCode1;
				stagingTableIntercoInvSettle.DimensionCode2 = stagingTableIntercoInvSettleItemView.DimensionCode2;
				stagingTableIntercoInvSettle.DimensionCode3 = stagingTableIntercoInvSettleItemView.DimensionCode3;
				stagingTableIntercoInvSettle.DimensionCode4 = stagingTableIntercoInvSettleItemView.DimensionCode4;
				stagingTableIntercoInvSettle.DimensionCode5 = stagingTableIntercoInvSettleItemView.DimensionCode5;
				stagingTableIntercoInvSettle.DistrictId = stagingTableIntercoInvSettleItemView.DistrictId;
				stagingTableIntercoInvSettle.DueDate = stagingTableIntercoInvSettleItemView.DueDate.StringToDate();
				stagingTableIntercoInvSettle.FeeLedgerAccount = stagingTableIntercoInvSettleItemView.FeeLedgerAccount;
				stagingTableIntercoInvSettle.IntercompanyInvoiceSettlementGUID = stagingTableIntercoInvSettleItemView.IntercompanyInvoiceSettlementGUID.StringToGuid();
				stagingTableIntercoInvSettle.IntercompanyInvoiceSettlementId = stagingTableIntercoInvSettleItemView.IntercompanyInvoiceSettlementId;
				stagingTableIntercoInvSettle.InterfaceStagingBatchId = stagingTableIntercoInvSettleItemView.InterfaceStagingBatchId;
				stagingTableIntercoInvSettle.InterfaceStatus = stagingTableIntercoInvSettleItemView.InterfaceStatus;
				stagingTableIntercoInvSettle.InvoiceDate = stagingTableIntercoInvSettleItemView.InvoiceDate.StringToDate();
				stagingTableIntercoInvSettle.InvoiceText = stagingTableIntercoInvSettleItemView.InvoiceText;
				stagingTableIntercoInvSettle.MethodOfPaymentId = stagingTableIntercoInvSettleItemView.MethodOfPaymentId;
				stagingTableIntercoInvSettle.Name = stagingTableIntercoInvSettleItemView.Name;
				stagingTableIntercoInvSettle.OrigInvoiceAmount = stagingTableIntercoInvSettleItemView.OrigInvoiceAmount;
				stagingTableIntercoInvSettle.OrigInvoiceId = stagingTableIntercoInvSettleItemView.OrigInvoiceId;
				stagingTableIntercoInvSettle.PostalCode = stagingTableIntercoInvSettleItemView.PostalCode;
				stagingTableIntercoInvSettle.ProvinceId = stagingTableIntercoInvSettleItemView.ProvinceId;
				stagingTableIntercoInvSettle.RecordType = stagingTableIntercoInvSettleItemView.RecordType;
				stagingTableIntercoInvSettle.SettleInvoiceAmount = stagingTableIntercoInvSettleItemView.SettleInvoiceAmount;
				stagingTableIntercoInvSettle.SubDistrictId = stagingTableIntercoInvSettleItemView.SubDistrictId;
				stagingTableIntercoInvSettle.TaxBranchId = stagingTableIntercoInvSettleItemView.TaxBranchId;
				stagingTableIntercoInvSettle.TaxCode = stagingTableIntercoInvSettleItemView.TaxCode;
				stagingTableIntercoInvSettle.TaxId = stagingTableIntercoInvSettleItemView.TaxId;
				
				stagingTableIntercoInvSettle.RowVersion = stagingTableIntercoInvSettleItemView.RowVersion;
				return stagingTableIntercoInvSettle;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<StagingTableIntercoInvSettle> ToStagingTableIntercoInvSettle(this IEnumerable<StagingTableIntercoInvSettleItemView> stagingTableIntercoInvSettleItemViews)
		{
			try
			{
				List<StagingTableIntercoInvSettle> stagingTableIntercoInvSettles = new List<StagingTableIntercoInvSettle>();
				foreach (StagingTableIntercoInvSettleItemView item in stagingTableIntercoInvSettleItemViews)
				{
					stagingTableIntercoInvSettles.Add(item.ToStagingTableIntercoInvSettle());
				}
				return stagingTableIntercoInvSettles;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static StagingTableIntercoInvSettleItemView ToStagingTableIntercoInvSettleItemView(this StagingTableIntercoInvSettle stagingTableIntercoInvSettle)
		{
			try
			{
				StagingTableIntercoInvSettleItemView stagingTableIntercoInvSettleItemView = new StagingTableIntercoInvSettleItemView();
				stagingTableIntercoInvSettleItemView.CompanyGUID = stagingTableIntercoInvSettle.CompanyGUID.GuidNullToString();
				stagingTableIntercoInvSettleItemView.CreatedBy = stagingTableIntercoInvSettle.CreatedBy;
				stagingTableIntercoInvSettleItemView.CreatedDateTime = stagingTableIntercoInvSettle.CreatedDateTime.DateTimeToString();
				stagingTableIntercoInvSettleItemView.ModifiedBy = stagingTableIntercoInvSettle.ModifiedBy;
				stagingTableIntercoInvSettleItemView.ModifiedDateTime = stagingTableIntercoInvSettle.ModifiedDateTime.DateTimeToString();
				stagingTableIntercoInvSettleItemView.Owner = stagingTableIntercoInvSettle.Owner;
				stagingTableIntercoInvSettleItemView.OwnerBusinessUnitGUID = stagingTableIntercoInvSettle.OwnerBusinessUnitGUID.GuidNullToString();
				stagingTableIntercoInvSettleItemView.StagingTableIntercoInvSettleGUID = stagingTableIntercoInvSettle.StagingTableIntercoInvSettleGUID.GuidNullToString();
				stagingTableIntercoInvSettleItemView.Address1 = stagingTableIntercoInvSettle.Address1;
				stagingTableIntercoInvSettleItemView.AltName = stagingTableIntercoInvSettle.AltName;
				stagingTableIntercoInvSettleItemView.CNReasonId = stagingTableIntercoInvSettle.CNReasonId;
				stagingTableIntercoInvSettleItemView.CompanyId = stagingTableIntercoInvSettle.CompanyId;
				stagingTableIntercoInvSettleItemView.CountryId = stagingTableIntercoInvSettle.CountryId;
				stagingTableIntercoInvSettleItemView.CurrencyId = stagingTableIntercoInvSettle.CurrencyId;
				stagingTableIntercoInvSettleItemView.CustFaxValue = stagingTableIntercoInvSettle.CustFaxValue;
				stagingTableIntercoInvSettleItemView.CustGroupId = stagingTableIntercoInvSettle.CustGroupId;
				stagingTableIntercoInvSettleItemView.CustomerId = stagingTableIntercoInvSettle.CustomerId;
				stagingTableIntercoInvSettleItemView.CustPhoneValue = stagingTableIntercoInvSettle.CustPhoneValue;
				stagingTableIntercoInvSettleItemView.DimensionCode1 = stagingTableIntercoInvSettle.DimensionCode1;
				stagingTableIntercoInvSettleItemView.DimensionCode2 = stagingTableIntercoInvSettle.DimensionCode2;
				stagingTableIntercoInvSettleItemView.DimensionCode3 = stagingTableIntercoInvSettle.DimensionCode3;
				stagingTableIntercoInvSettleItemView.DimensionCode4 = stagingTableIntercoInvSettle.DimensionCode4;
				stagingTableIntercoInvSettleItemView.DimensionCode5 = stagingTableIntercoInvSettle.DimensionCode5;
				stagingTableIntercoInvSettleItemView.CompanyTaxBranchId = stagingTableIntercoInvSettle.CompanyTaxBranchId;
				stagingTableIntercoInvSettleItemView.DistrictId = stagingTableIntercoInvSettle.DistrictId;
				stagingTableIntercoInvSettleItemView.DueDate = stagingTableIntercoInvSettle.DueDate.DateToString();
				stagingTableIntercoInvSettleItemView.FeeLedgerAccount = stagingTableIntercoInvSettle.FeeLedgerAccount;
				stagingTableIntercoInvSettleItemView.IntercompanyInvoiceSettlementGUID = stagingTableIntercoInvSettle.IntercompanyInvoiceSettlementGUID.GuidNullToString();
				stagingTableIntercoInvSettleItemView.IntercompanyInvoiceSettlementId = stagingTableIntercoInvSettle.IntercompanyInvoiceSettlementId;
				stagingTableIntercoInvSettleItemView.InterfaceStagingBatchId = stagingTableIntercoInvSettle.InterfaceStagingBatchId;
				stagingTableIntercoInvSettleItemView.InterfaceStatus = stagingTableIntercoInvSettle.InterfaceStatus;
				stagingTableIntercoInvSettleItemView.InvoiceDate = stagingTableIntercoInvSettle.InvoiceDate.DateToString();
				stagingTableIntercoInvSettleItemView.InvoiceText = stagingTableIntercoInvSettle.InvoiceText;
				stagingTableIntercoInvSettleItemView.MethodOfPaymentId = stagingTableIntercoInvSettle.MethodOfPaymentId;
				stagingTableIntercoInvSettleItemView.Name = stagingTableIntercoInvSettle.Name;
				stagingTableIntercoInvSettleItemView.OrigInvoiceAmount = stagingTableIntercoInvSettle.OrigInvoiceAmount;
				stagingTableIntercoInvSettleItemView.OrigInvoiceId = stagingTableIntercoInvSettle.OrigInvoiceId;
				stagingTableIntercoInvSettleItemView.PostalCode = stagingTableIntercoInvSettle.PostalCode;
				stagingTableIntercoInvSettleItemView.ProvinceId = stagingTableIntercoInvSettle.ProvinceId;
				stagingTableIntercoInvSettleItemView.RecordType = stagingTableIntercoInvSettle.RecordType;
				stagingTableIntercoInvSettleItemView.SettleInvoiceAmount = stagingTableIntercoInvSettle.SettleInvoiceAmount;
				stagingTableIntercoInvSettleItemView.SubDistrictId = stagingTableIntercoInvSettle.SubDistrictId;
				stagingTableIntercoInvSettleItemView.TaxBranchId = stagingTableIntercoInvSettle.TaxBranchId;
				stagingTableIntercoInvSettleItemView.TaxCode = stagingTableIntercoInvSettle.TaxCode;
				stagingTableIntercoInvSettleItemView.TaxId = stagingTableIntercoInvSettle.TaxId;
				
				stagingTableIntercoInvSettleItemView.RowVersion = stagingTableIntercoInvSettle.RowVersion;
				return stagingTableIntercoInvSettleItemView.GetStagingTableIntercoInvSettleItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion StagingTableIntercoInvSettleItemView
		#region ToDropDown
		public static SelectItem<StagingTableIntercoInvSettleItemView> ToDropDownItem(this StagingTableIntercoInvSettleItemView stagingTableIntercoInvSettleView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<StagingTableIntercoInvSettleItemView> selectItem = new SelectItem<StagingTableIntercoInvSettleItemView>();
				selectItem.Label = null;
				selectItem.Value = stagingTableIntercoInvSettleView.StagingTableIntercoInvSettleGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: stagingTableIntercoInvSettleView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<StagingTableIntercoInvSettleItemView>> ToDropDownItem(this IEnumerable<StagingTableIntercoInvSettleItemView> stagingTableIntercoInvSettleItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<StagingTableIntercoInvSettleItemView>> selectItems = new List<SelectItem<StagingTableIntercoInvSettleItemView>>();
				foreach (StagingTableIntercoInvSettleItemView item in stagingTableIntercoInvSettleItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

