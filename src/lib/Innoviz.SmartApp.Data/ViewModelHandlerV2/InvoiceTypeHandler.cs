using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class InvoiceTypeHandler
	{
		#region InvoiceTypeListView
		public static List<InvoiceTypeListView> GetInvoiceTypeListViewValidation(this List<InvoiceTypeListView> invoiceTypeListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return invoiceTypeListViews;
			}
			var result = new List<InvoiceTypeListView>();
			try
			{
				foreach (InvoiceTypeListView item in invoiceTypeListViews)
				{
					result.Add(item.GetInvoiceTypeListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static InvoiceTypeListView GetInvoiceTypeListViewValidation(this InvoiceTypeListView invoiceTypeListView)
		{
			try
			{
				invoiceTypeListView.RowAuthorize = invoiceTypeListView.GetListRowAuthorize();
				return invoiceTypeListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this InvoiceTypeListView invoiceTypeListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion InvoiceTypeListView
		#region InvoiceTypeItemView
		public static InvoiceTypeItemView GetInvoiceTypeItemViewValidation(this InvoiceTypeItemView invoiceTypeItemView)
		{
			try
			{
				invoiceTypeItemView.RowAuthorize = invoiceTypeItemView.GetItemRowAuthorize();
				return invoiceTypeItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this InvoiceTypeItemView invoiceTypeItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static InvoiceType ToInvoiceType(this InvoiceTypeItemView invoiceTypeItemView)
		{
			try
			{
				InvoiceType invoiceType = new InvoiceType();
				invoiceType.CompanyGUID = invoiceTypeItemView.CompanyGUID.StringToGuid();
				invoiceType.CreatedBy = invoiceTypeItemView.CreatedBy;
				invoiceType.CreatedDateTime = invoiceTypeItemView.CreatedDateTime.StringToSystemDateTime();
				invoiceType.ModifiedBy = invoiceTypeItemView.ModifiedBy;
				invoiceType.ModifiedDateTime = invoiceTypeItemView.ModifiedDateTime.StringToSystemDateTime();
				invoiceType.Owner = invoiceTypeItemView.Owner;
				invoiceType.OwnerBusinessUnitGUID = invoiceTypeItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				invoiceType.InvoiceTypeGUID = invoiceTypeItemView.InvoiceTypeGUID.StringToGuid();
				invoiceType.DirectReceipt = invoiceTypeItemView.DirectReceipt;
				invoiceType.SuspenseInvoiceType = invoiceTypeItemView.SuspenseInvoiceType;
				invoiceType.ARLedgerAccount = invoiceTypeItemView.ARLedgerAccount;
				invoiceType.AutoGen = invoiceTypeItemView.AutoGen;
				invoiceType.Description = invoiceTypeItemView.Description;
				invoiceType.ValidateDirectReceiveByCA = invoiceTypeItemView.ValidateDirectReceiveByCA;
				invoiceType.InvoiceTypeId = invoiceTypeItemView.InvoiceTypeId;
				invoiceType.ProductType = invoiceTypeItemView.ProductType;
				invoiceType.ProductInvoice = invoiceTypeItemView.ProductInvoice;
				invoiceType.AutoGenInvoiceRevenueTypeGUID = invoiceTypeItemView.AutoGenInvoiceRevenueTypeGUID.StringToGuidNull();
				
				invoiceType.RowVersion = invoiceTypeItemView.RowVersion;
				return invoiceType;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<InvoiceType> ToInvoiceType(this IEnumerable<InvoiceTypeItemView> invoiceTypeItemViews)
		{
			try
			{
				List<InvoiceType> invoiceTypes = new List<InvoiceType>();
				foreach (InvoiceTypeItemView item in invoiceTypeItemViews)
				{
					invoiceTypes.Add(item.ToInvoiceType());
				}
				return invoiceTypes;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static InvoiceTypeItemView ToInvoiceTypeItemView(this InvoiceType invoiceType)
		{
			try
			{
				InvoiceTypeItemView invoiceTypeItemView = new InvoiceTypeItemView();
				invoiceTypeItemView.CompanyGUID = invoiceType.CompanyGUID.GuidNullToString();
				invoiceTypeItemView.CreatedBy = invoiceType.CreatedBy;
				invoiceTypeItemView.CreatedDateTime = invoiceType.CreatedDateTime.DateTimeToString();
				invoiceTypeItemView.ModifiedBy = invoiceType.ModifiedBy;
				invoiceTypeItemView.ModifiedDateTime = invoiceType.ModifiedDateTime.DateTimeToString();
				invoiceTypeItemView.Owner = invoiceType.Owner;
				invoiceTypeItemView.OwnerBusinessUnitGUID = invoiceType.OwnerBusinessUnitGUID.GuidNullToString();
				invoiceTypeItemView.InvoiceTypeGUID = invoiceType.InvoiceTypeGUID.GuidNullToString();
				invoiceTypeItemView.DirectReceipt = invoiceType.DirectReceipt;
				invoiceTypeItemView.ValidateDirectReceiveByCA = invoiceType.ValidateDirectReceiveByCA;
				invoiceTypeItemView.ARLedgerAccount = invoiceType.ARLedgerAccount;
				invoiceTypeItemView.AutoGen = invoiceType.AutoGen;
				invoiceTypeItemView.Description = invoiceType.Description;
				invoiceTypeItemView.ProductInvoice = invoiceType.ProductInvoice;
				invoiceTypeItemView.InvoiceTypeId = invoiceType.InvoiceTypeId;
				invoiceTypeItemView.ProductType = invoiceType.ProductType;
				invoiceTypeItemView.SuspenseInvoiceType = invoiceType.SuspenseInvoiceType;
				invoiceTypeItemView.AutoGenInvoiceRevenueTypeGUID = invoiceType.AutoGenInvoiceRevenueTypeGUID.GuidNullToString();
				
				invoiceTypeItemView.RowVersion = invoiceType.RowVersion;
				return invoiceTypeItemView.GetInvoiceTypeItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion InvoiceTypeItemView
		#region ToDropDown
		public static SelectItem<InvoiceTypeItemView> ToDropDownItem(this InvoiceTypeItemView invoiceTypeView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<InvoiceTypeItemView> selectItem = new SelectItem<InvoiceTypeItemView>();
				selectItem.Label = "[" + invoiceTypeView.InvoiceTypeId + "] " + invoiceTypeView.Description;
				selectItem.Value = invoiceTypeView.InvoiceTypeGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: invoiceTypeView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<InvoiceTypeItemView>> ToDropDownItem(this IEnumerable<InvoiceTypeItemView> invoiceTypeItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<InvoiceTypeItemView>> selectItems = new List<SelectItem<InvoiceTypeItemView>>();
				foreach (InvoiceTypeItemView item in invoiceTypeItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

