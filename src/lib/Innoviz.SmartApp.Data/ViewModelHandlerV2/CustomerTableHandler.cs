using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CustomerTableHandler
	{
		#region CustomerTableListView
		public static List<CustomerTableListView> GetCustomerTableListViewValidation(this List<CustomerTableListView> customerTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return customerTableListViews;
			}
			var result = new List<CustomerTableListView>();
			try
			{
				foreach (CustomerTableListView item in customerTableListViews)
				{
					result.Add(item.GetCustomerTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CustomerTableListView GetCustomerTableListViewValidation(this CustomerTableListView customerTableListView)
		{
			try
			{
				customerTableListView.RowAuthorize = customerTableListView.GetListRowAuthorize();
				return customerTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CustomerTableListView customerTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CustomerTableListView
		#region CustomerTableItemView
		public static CustomerTableItemView GetCustomerTableItemViewValidation(this CustomerTableItemView customerTableItemView)
		{
			try
			{
				customerTableItemView.RowAuthorize = customerTableItemView.GetItemRowAuthorize();
				return customerTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CustomerTableItemView customerTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CustomerTable ToCustomerTable(this CustomerTableItemView customerTableItemView)
		{
			try
			{
				CustomerTable customerTable = new CustomerTable();
				customerTable.CompanyGUID = customerTableItemView.CompanyGUID.StringToGuid();
				customerTable.CreatedBy = customerTableItemView.CreatedBy;
				customerTable.CreatedDateTime = customerTableItemView.CreatedDateTime.StringToSystemDateTime();
				customerTable.ModifiedBy = customerTableItemView.ModifiedBy;
				customerTable.ModifiedDateTime = customerTableItemView.ModifiedDateTime.StringToSystemDateTime();
				customerTable.Owner = customerTableItemView.Owner;
				customerTable.OwnerBusinessUnitGUID = customerTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				customerTable.CustomerTableGUID = customerTableItemView.CustomerTableGUID.StringToGuid();
				customerTable.AltName = customerTableItemView.AltName;
				customerTable.BlacklistStatusGUID = customerTableItemView.BlacklistStatusGUID.StringToGuidNull();
				customerTable.BOTRating = customerTableItemView.BOTRating;
				customerTable.BusinessSegmentGUID = customerTableItemView.BusinessSegmentGUID.StringToGuidNull();
				customerTable.BusinessSizeGUID = customerTableItemView.BusinessSizeGUID.StringToGuidNull();
				customerTable.BusinessTypeGUID = customerTableItemView.BusinessTypeGUID.StringToGuidNull();
				customerTable.CompanyName = customerTableItemView.CompanyName;
				customerTable.CompanyWebsite = customerTableItemView.CompanyWebsite;
				customerTable.CreditScoringGUID = customerTableItemView.CreditScoringGUID.StringToGuidNull();
				customerTable.CurrencyGUID = customerTableItemView.CurrencyGUID.StringToGuid();
				customerTable.CustGroupGUID = customerTableItemView.CustGroupGUID.StringToGuid();
				customerTable.CustomerId = customerTableItemView.CustomerId;
				customerTable.DateOfBirth = customerTableItemView.DateOfBirth.StringNullToDateNull();
				customerTable.DateOfEstablish = customerTableItemView.DateOfEstablish.StringNullToDateNull();
				customerTable.DateOfExpiry = customerTableItemView.DateOfExpiry.StringNullToDateNull();
				customerTable.DateOfIssue = customerTableItemView.DateOfIssue.StringNullToDateNull();
				customerTable.Dimension1GUID = customerTableItemView.Dimension1GUID.StringToGuidNull();
				customerTable.Dimension2GUID = customerTableItemView.Dimension2GUID.StringToGuidNull();
				customerTable.Dimension3GUID = customerTableItemView.Dimension3GUID.StringToGuidNull();
				customerTable.Dimension4GUID = customerTableItemView.Dimension4GUID.StringToGuidNull();
				customerTable.Dimension5GUID = customerTableItemView.Dimension5GUID.StringToGuidNull();
				customerTable.DocumentStatusGUID = customerTableItemView.DocumentStatusGUID.StringToGuidNull();
				customerTable.DueDay = customerTableItemView.DueDay;
				customerTable.ExposureGroupGUID = customerTableItemView.ExposureGroupGUID.StringToGuidNull();
				customerTable.FixedAsset = customerTableItemView.FixedAsset;
				customerTable.GenderGUID = customerTableItemView.GenderGUID.StringToGuidNull();
				customerTable.GradeClassificationGUID = customerTableItemView.GradeClassificationGUID.StringToGuidNull();
				customerTable.IdentificationType = customerTableItemView.IdentificationType;
				customerTable.Income = customerTableItemView.Income;
				customerTable.IntroducedByGUID = customerTableItemView.IntroducedByGUID.StringToGuidNull();
				customerTable.IntroducedByRemark = customerTableItemView.IntroducedByRemark;
				customerTable.InvoiceIssuingDay = customerTableItemView.InvoiceIssuingDay;
				customerTable.IssuedBy = customerTableItemView.IssuedBy;
				customerTable.KYCSetupGUID = customerTableItemView.KYCSetupGUID.StringToGuidNull();
				customerTable.Labor = customerTableItemView.Labor;
				customerTable.LineOfBusinessGUID = customerTableItemView.LineOfBusinessGUID.StringToGuidNull();
				customerTable.MaritalStatusGUID = customerTableItemView.MaritalStatusGUID.StringToGuidNull();
				customerTable.MethodOfPaymentGUID = customerTableItemView.MethodOfPaymentGUID.StringToGuidNull();
				customerTable.Name = customerTableItemView.Name;
				customerTable.NationalityGUID = customerTableItemView.NationalityGUID.StringToGuidNull();
				customerTable.NCBAccountStatusGUID = customerTableItemView.NCBAccountStatusGUID.StringToGuidNull();
				customerTable.OccupationGUID = customerTableItemView.OccupationGUID.StringToGuidNull();
				customerTable.OtherIncome = customerTableItemView.OtherIncome;
				customerTable.OtherSourceIncome = customerTableItemView.OtherSourceIncome;
				customerTable.PaidUpCapital = customerTableItemView.PaidUpCapital;
				customerTable.ParentCompanyGUID = customerTableItemView.ParentCompanyGUID.StringToGuidNull();
				customerTable.PassportID = customerTableItemView.PassportID;
				customerTable.Position = customerTableItemView.Position;
				customerTable.PrivateARPct = customerTableItemView.PrivateARPct;
				customerTable.PublicARPct = customerTableItemView.PublicARPct;
				customerTable.RaceGUID = customerTableItemView.RaceGUID.StringToGuidNull();
				customerTable.RecordType = customerTableItemView.RecordType;
				customerTable.ReferenceId = customerTableItemView.ReferenceId;
				customerTable.RegisteredCapital = customerTableItemView.RegisteredCapital;
				customerTable.RegistrationTypeGUID = customerTableItemView.RegistrationTypeGUID.StringToGuidNull();
				customerTable.ResponsibleByGUID = customerTableItemView.ResponsibleByGUID.StringToGuid();
				customerTable.SigningCondition = customerTableItemView.SigningCondition;
				customerTable.SpouseName = customerTableItemView.SpouseName;
				customerTable.TaxID = customerTableItemView.TaxID;
				customerTable.TerritoryGUID = customerTableItemView.TerritoryGUID.StringToGuidNull();
				customerTable.VendorTableGUID = customerTableItemView.VendorTableGUID.StringToGuidNull();
				customerTable.WithholdingTaxGroupGUID = customerTableItemView.WithholdingTaxGroupGUID.StringToGuidNull();
				customerTable.WorkExperienceMonth = customerTableItemView.WorkExperienceMonth;
				customerTable.WorkExperienceYear = customerTableItemView.WorkExperienceYear;
				customerTable.WorkPermitID = customerTableItemView.WorkPermitID;
				
				customerTable.RowVersion = customerTableItemView.RowVersion;
				return customerTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CustomerTable> ToCustomerTable(this IEnumerable<CustomerTableItemView> customerTableItemViews)
		{
			try
			{
				List<CustomerTable> customerTables = new List<CustomerTable>();
				foreach (CustomerTableItemView item in customerTableItemViews)
				{
					customerTables.Add(item.ToCustomerTable());
				}
				return customerTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CustomerTableItemView ToCustomerTableItemView(this CustomerTable customerTable)
		{
			try
			{
				CustomerTableItemView customerTableItemView = new CustomerTableItemView();
				customerTableItemView.CompanyGUID = customerTable.CompanyGUID.GuidNullToString();
				customerTableItemView.CreatedBy = customerTable.CreatedBy;
				customerTableItemView.CreatedDateTime = customerTable.CreatedDateTime.DateTimeToString();
				customerTableItemView.ModifiedBy = customerTable.ModifiedBy;
				customerTableItemView.ModifiedDateTime = customerTable.ModifiedDateTime.DateTimeToString();
				customerTableItemView.Owner = customerTable.Owner;
				customerTableItemView.OwnerBusinessUnitGUID = customerTable.OwnerBusinessUnitGUID.GuidNullToString();
				customerTableItemView.CustomerTableGUID = customerTable.CustomerTableGUID.GuidNullToString();
				customerTableItemView.AltName = customerTable.AltName;
				customerTableItemView.BlacklistStatusGUID = customerTable.BlacklistStatusGUID.GuidNullToString();
				customerTableItemView.BOTRating = customerTable.BOTRating;
				customerTableItemView.BusinessSegmentGUID = customerTable.BusinessSegmentGUID.GuidNullToString();
				customerTableItemView.BusinessSizeGUID = customerTable.BusinessSizeGUID.GuidNullToString();
				customerTableItemView.BusinessTypeGUID = customerTable.BusinessTypeGUID.GuidNullToString();
				customerTableItemView.CompanyName = customerTable.CompanyName;
				customerTableItemView.CompanyWebsite = customerTable.CompanyWebsite;
				customerTableItemView.CreditScoringGUID = customerTable.CreditScoringGUID.GuidNullToString();
				customerTableItemView.CurrencyGUID = customerTable.CurrencyGUID.GuidNullToString();
				customerTableItemView.CustGroupGUID = customerTable.CustGroupGUID.GuidNullToString();
				customerTableItemView.CustomerId = customerTable.CustomerId;
				customerTableItemView.DateOfBirth = customerTable.DateOfBirth.DateNullToString();
				customerTableItemView.DateOfEstablish = customerTable.DateOfEstablish.DateNullToString();
				customerTableItemView.DateOfExpiry = customerTable.DateOfExpiry.DateNullToString();
				customerTableItemView.DateOfIssue = customerTable.DateOfIssue.DateNullToString();
				customerTableItemView.Dimension1GUID = customerTable.Dimension1GUID.GuidNullToString();
				customerTableItemView.Dimension2GUID = customerTable.Dimension2GUID.GuidNullToString();
				customerTableItemView.Dimension3GUID = customerTable.Dimension3GUID.GuidNullToString();
				customerTableItemView.Dimension4GUID = customerTable.Dimension4GUID.GuidNullToString();
				customerTableItemView.Dimension5GUID = customerTable.Dimension5GUID.GuidNullToString();
				customerTableItemView.DocumentStatusGUID = customerTable.DocumentStatusGUID.GuidNullToString();
				customerTableItemView.DueDay = customerTable.DueDay;
				customerTableItemView.ExposureGroupGUID = customerTable.ExposureGroupGUID.GuidNullToString();
				customerTableItemView.FixedAsset = customerTable.FixedAsset;
				customerTableItemView.GenderGUID = customerTable.GenderGUID.GuidNullToString();
				customerTableItemView.GradeClassificationGUID = customerTable.GradeClassificationGUID.GuidNullToString();
				customerTableItemView.IdentificationType = customerTable.IdentificationType;
				customerTableItemView.Income = customerTable.Income;
				customerTableItemView.IntroducedByGUID = customerTable.IntroducedByGUID.GuidNullToString();
				customerTableItemView.IntroducedByRemark = customerTable.IntroducedByRemark;
				customerTableItemView.InvoiceIssuingDay = customerTable.InvoiceIssuingDay;
				customerTableItemView.IssuedBy = customerTable.IssuedBy;
				customerTableItemView.KYCSetupGUID = customerTable.KYCSetupGUID.GuidNullToString();
				customerTableItemView.Labor = customerTable.Labor;
				customerTableItemView.LineOfBusinessGUID = customerTable.LineOfBusinessGUID.GuidNullToString();
				customerTableItemView.MaritalStatusGUID = customerTable.MaritalStatusGUID.GuidNullToString();
				customerTableItemView.MethodOfPaymentGUID = customerTable.MethodOfPaymentGUID.GuidNullToString();
				customerTableItemView.Name = customerTable.Name;
				customerTableItemView.NationalityGUID = customerTable.NationalityGUID.GuidNullToString();
				customerTableItemView.NCBAccountStatusGUID = customerTable.NCBAccountStatusGUID.GuidNullToString();
				customerTableItemView.OccupationGUID = customerTable.OccupationGUID.GuidNullToString();
				customerTableItemView.OtherIncome = customerTable.OtherIncome;
				customerTableItemView.OtherSourceIncome = customerTable.OtherSourceIncome;
				customerTableItemView.PaidUpCapital = customerTable.PaidUpCapital;
				customerTableItemView.ParentCompanyGUID = customerTable.ParentCompanyGUID.GuidNullToString();
				customerTableItemView.PassportID = customerTable.PassportID;
				customerTableItemView.Position = customerTable.Position;
				customerTableItemView.PrivateARPct = customerTable.PrivateARPct;
				customerTableItemView.PublicARPct = customerTable.PublicARPct;
				customerTableItemView.RaceGUID = customerTable.RaceGUID.GuidNullToString();
				customerTableItemView.RecordType = customerTable.RecordType;
				customerTableItemView.ReferenceId = customerTable.ReferenceId;
				customerTableItemView.RegisteredCapital = customerTable.RegisteredCapital;
				customerTableItemView.RegistrationTypeGUID = customerTable.RegistrationTypeGUID.GuidNullToString();
				customerTableItemView.ResponsibleByGUID = customerTable.ResponsibleByGUID.GuidNullToString();
				customerTableItemView.SigningCondition = customerTable.SigningCondition;
				customerTableItemView.SpouseName = customerTable.SpouseName;
				customerTableItemView.TaxID = customerTable.TaxID;
				customerTableItemView.TerritoryGUID = customerTable.TerritoryGUID.GuidNullToString();
				customerTableItemView.VendorTableGUID = customerTable.VendorTableGUID.GuidNullToString();
				customerTableItemView.WithholdingTaxGroupGUID = customerTable.WithholdingTaxGroupGUID.GuidNullToString();
				customerTableItemView.WorkExperienceMonth = customerTable.WorkExperienceMonth;
				customerTableItemView.WorkExperienceYear = customerTable.WorkExperienceYear;
				customerTableItemView.WorkPermitID = customerTable.WorkPermitID;
				
				customerTableItemView.RowVersion = customerTable.RowVersion;
				return customerTableItemView.GetCustomerTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CustomerTableItemView
		#region ToDropDown
		public static SelectItem<CustomerTableItemView> ToDropDownItem(this CustomerTableItemView customerTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CustomerTableItemView> selectItem = new SelectItem<CustomerTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(customerTableView.CustomerId, customerTableView.Name);
				selectItem.Value = customerTableView.CustomerTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: customerTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CustomerTableItemView>> ToDropDownItem(this IEnumerable<CustomerTableItemView> customerTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CustomerTableItemView>> selectItems = new List<SelectItem<CustomerTableItemView>>();
				foreach (CustomerTableItemView item in customerTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

