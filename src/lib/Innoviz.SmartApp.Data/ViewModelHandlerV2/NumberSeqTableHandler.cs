using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class NumberSeqTableHandler
	{
		#region NumberSeqTableListView
		public static List<NumberSeqTableListView> GetNumberSeqTableListViewValidation(this List<NumberSeqTableListView> numberSeqTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return numberSeqTableListViews;
			}
			var result = new List<NumberSeqTableListView>();
			try
			{
				foreach (NumberSeqTableListView item in numberSeqTableListViews)
				{
					result.Add(item.GetNumberSeqTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static NumberSeqTableListView GetNumberSeqTableListViewValidation(this NumberSeqTableListView numberSeqTableListView)
		{
			try
			{
				numberSeqTableListView.RowAuthorize = numberSeqTableListView.GetListRowAuthorize();
				return numberSeqTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this NumberSeqTableListView numberSeqTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion NumberSeqTableListView
		#region NumberSeqTableItemView
		public static NumberSeqTableItemView GetNumberSeqTableItemViewValidation(this NumberSeqTableItemView numberSeqTableItemView)
		{
			try
			{
				numberSeqTableItemView.RowAuthorize = numberSeqTableItemView.GetItemRowAuthorize();
				return numberSeqTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this NumberSeqTableItemView numberSeqTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static NumberSeqTable ToNumberSeqTable(this NumberSeqTableItemView numberSeqTableItemView)
		{
			try
			{
				NumberSeqTable numberSeqTable = new NumberSeqTable();
				numberSeqTable.CompanyGUID = numberSeqTableItemView.CompanyGUID.StringToGuid();
				numberSeqTable.CreatedBy = numberSeqTableItemView.CreatedBy;
				numberSeqTable.CreatedDateTime = numberSeqTableItemView.CreatedDateTime.StringToSystemDateTime();
				numberSeqTable.ModifiedBy = numberSeqTableItemView.ModifiedBy;
				numberSeqTable.ModifiedDateTime = numberSeqTableItemView.ModifiedDateTime.StringToSystemDateTime();
				numberSeqTable.Owner = numberSeqTableItemView.Owner;
				numberSeqTable.OwnerBusinessUnitGUID = numberSeqTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				numberSeqTable.NumberSeqTableGUID = numberSeqTableItemView.NumberSeqTableGUID.StringToGuid();
				numberSeqTable.Description = numberSeqTableItemView.Description;
				numberSeqTable.Largest = numberSeqTableItemView.Largest;
				numberSeqTable.Manual = numberSeqTableItemView.Manual;
				numberSeqTable.Next = numberSeqTableItemView.Next;
				numberSeqTable.NumberSeqCode = numberSeqTableItemView.NumberSeqCode;
				numberSeqTable.RowVersion = numberSeqTableItemView.RowVersion;
				numberSeqTable.Smallest = numberSeqTableItemView.Smallest;
				return numberSeqTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<NumberSeqTable> ToNumberSeqTable(this IEnumerable<NumberSeqTableItemView> numberSeqTableItemViews)
		{
			try
			{
				List<NumberSeqTable> numberSeqTables = new List<NumberSeqTable>();
				foreach (NumberSeqTableItemView item in numberSeqTableItemViews)
				{
					numberSeqTables.Add(item.ToNumberSeqTable());
				}
				return numberSeqTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static NumberSeqTableItemView ToNumberSeqTableItemView(this NumberSeqTable numberSeqTable)
		{
			try
			{
				NumberSeqTableItemView numberSeqTableItemView = new NumberSeqTableItemView();
				numberSeqTableItemView.CompanyGUID = numberSeqTable.CompanyGUID.GuidNullToString();
				numberSeqTableItemView.CreatedBy = numberSeqTable.CreatedBy;
				numberSeqTableItemView.CreatedDateTime = numberSeqTable.CreatedDateTime.DateTimeToString();
				numberSeqTableItemView.ModifiedBy = numberSeqTable.ModifiedBy;
				numberSeqTableItemView.ModifiedDateTime = numberSeqTable.ModifiedDateTime.DateTimeToString();
				numberSeqTableItemView.Owner = numberSeqTable.Owner;
				numberSeqTableItemView.OwnerBusinessUnitGUID = numberSeqTable.OwnerBusinessUnitGUID.GuidNullToString();
				numberSeqTableItemView.NumberSeqTableGUID = numberSeqTable.NumberSeqTableGUID.GuidNullToString();
				numberSeqTableItemView.Description = numberSeqTable.Description;
				numberSeqTableItemView.Largest = numberSeqTable.Largest;
				numberSeqTableItemView.Manual = numberSeqTable.Manual;
				numberSeqTableItemView.Next = numberSeqTable.Next;
				numberSeqTableItemView.NumberSeqCode = numberSeqTable.NumberSeqCode;
				numberSeqTableItemView.RowVersion = numberSeqTable.RowVersion;
				numberSeqTableItemView.Smallest = numberSeqTable.Smallest;
				return numberSeqTableItemView.GetNumberSeqTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion NumberSeqTableItemView
		#region ToDropDown
		public static SelectItem<NumberSeqTableItemView> ToDropDownItem(this NumberSeqTableItemView numberSeqTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<NumberSeqTableItemView> selectItem = new SelectItem<NumberSeqTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(numberSeqTableView.NumberSeqCode, numberSeqTableView.Description);
				selectItem.Value = numberSeqTableView.NumberSeqTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: numberSeqTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<NumberSeqTableItemView>> ToDropDownItem(this IEnumerable<NumberSeqTableItemView> numberSeqTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<NumberSeqTableItemView>> selectItems = new List<SelectItem<NumberSeqTableItemView>>();
				foreach (NumberSeqTableItemView item in numberSeqTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}
