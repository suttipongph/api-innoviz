using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class ExposureGroupByProductHandler
	{
		#region ExposureGroupByProductListView
		public static List<ExposureGroupByProductListView> GetExposureGroupByProductListViewValidation(this List<ExposureGroupByProductListView> exposureGroupByProductListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return exposureGroupByProductListViews;
			}
			var result = new List<ExposureGroupByProductListView>();
			try
			{
				foreach (ExposureGroupByProductListView item in exposureGroupByProductListViews)
				{
					result.Add(item.GetExposureGroupByProductListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ExposureGroupByProductListView GetExposureGroupByProductListViewValidation(this ExposureGroupByProductListView exposureGroupByProductListView)
		{
			try
			{
				exposureGroupByProductListView.RowAuthorize = exposureGroupByProductListView.GetListRowAuthorize();
				return exposureGroupByProductListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this ExposureGroupByProductListView exposureGroupByProductListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion ExposureGroupByProductListView
		#region ExposureGroupByProductItemView
		public static ExposureGroupByProductItemView GetExposureGroupByProductItemViewValidation(this ExposureGroupByProductItemView exposureGroupByProductItemView)
		{
			try
			{
				exposureGroupByProductItemView.RowAuthorize = exposureGroupByProductItemView.GetItemRowAuthorize();
				return exposureGroupByProductItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this ExposureGroupByProductItemView exposureGroupByProductItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static ExposureGroupByProduct ToExposureGroupByProduct(this ExposureGroupByProductItemView exposureGroupByProductItemView)
		{
			try
			{
				ExposureGroupByProduct exposureGroupByProduct = new ExposureGroupByProduct();
				exposureGroupByProduct.CompanyGUID = exposureGroupByProductItemView.CompanyGUID.StringToGuid();
				exposureGroupByProduct.CreatedBy = exposureGroupByProductItemView.CreatedBy;
				exposureGroupByProduct.CreatedDateTime = exposureGroupByProductItemView.CreatedDateTime.StringToSystemDateTime();
				exposureGroupByProduct.ModifiedBy = exposureGroupByProductItemView.ModifiedBy;
				exposureGroupByProduct.ModifiedDateTime = exposureGroupByProductItemView.ModifiedDateTime.StringToSystemDateTime();
				exposureGroupByProduct.Owner = exposureGroupByProductItemView.Owner;
				exposureGroupByProduct.OwnerBusinessUnitGUID = exposureGroupByProductItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				exposureGroupByProduct.ExposureGroupByProductGUID = exposureGroupByProductItemView.ExposureGroupByProductGUID.StringToGuid();
				exposureGroupByProduct.ExposureAmount = exposureGroupByProductItemView.ExposureAmount;
				exposureGroupByProduct.ExposureGroupGUID = exposureGroupByProductItemView.ExposureGroupGUID.StringToGuid();
				exposureGroupByProduct.ProductType = exposureGroupByProductItemView.ProductType;
				
				exposureGroupByProduct.RowVersion = exposureGroupByProductItemView.RowVersion;
				return exposureGroupByProduct;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<ExposureGroupByProduct> ToExposureGroupByProduct(this IEnumerable<ExposureGroupByProductItemView> exposureGroupByProductItemViews)
		{
			try
			{
				List<ExposureGroupByProduct> exposureGroupByProducts = new List<ExposureGroupByProduct>();
				foreach (ExposureGroupByProductItemView item in exposureGroupByProductItemViews)
				{
					exposureGroupByProducts.Add(item.ToExposureGroupByProduct());
				}
				return exposureGroupByProducts;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ExposureGroupByProductItemView ToExposureGroupByProductItemView(this ExposureGroupByProduct exposureGroupByProduct)
		{
			try
			{
				ExposureGroupByProductItemView exposureGroupByProductItemView = new ExposureGroupByProductItemView();
				exposureGroupByProductItemView.CompanyGUID = exposureGroupByProduct.CompanyGUID.GuidNullToString();
				exposureGroupByProductItemView.CreatedBy = exposureGroupByProduct.CreatedBy;
				exposureGroupByProductItemView.CreatedDateTime = exposureGroupByProduct.CreatedDateTime.DateTimeToString();
				exposureGroupByProductItemView.ModifiedBy = exposureGroupByProduct.ModifiedBy;
				exposureGroupByProductItemView.ModifiedDateTime = exposureGroupByProduct.ModifiedDateTime.DateTimeToString();
				exposureGroupByProductItemView.Owner = exposureGroupByProduct.Owner;
				exposureGroupByProductItemView.OwnerBusinessUnitGUID = exposureGroupByProduct.OwnerBusinessUnitGUID.GuidNullToString();
				exposureGroupByProductItemView.ExposureGroupByProductGUID = exposureGroupByProduct.ExposureGroupByProductGUID.GuidNullToString();
				exposureGroupByProductItemView.ExposureAmount = exposureGroupByProduct.ExposureAmount;
				exposureGroupByProductItemView.ExposureGroupGUID = exposureGroupByProduct.ExposureGroupGUID.GuidNullToString();
				exposureGroupByProductItemView.ProductType = exposureGroupByProduct.ProductType;
				
				exposureGroupByProductItemView.RowVersion = exposureGroupByProduct.RowVersion;
				return exposureGroupByProductItemView.GetExposureGroupByProductItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ExposureGroupByProductItemView
		#region ToDropDown
		public static SelectItem<ExposureGroupByProductItemView> ToDropDownItem(this ExposureGroupByProductItemView exposureGroupByProductView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<ExposureGroupByProductItemView> selectItem = new SelectItem<ExposureGroupByProductItemView>();
				selectItem.Label = null;
				selectItem.Value = exposureGroupByProductView.ExposureGroupByProductGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: exposureGroupByProductView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<ExposureGroupByProductItemView>> ToDropDownItem(this IEnumerable<ExposureGroupByProductItemView> exposureGroupByProductItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<ExposureGroupByProductItemView>> selectItems = new List<SelectItem<ExposureGroupByProductItemView>>();
				foreach (ExposureGroupByProductItemView item in exposureGroupByProductItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

