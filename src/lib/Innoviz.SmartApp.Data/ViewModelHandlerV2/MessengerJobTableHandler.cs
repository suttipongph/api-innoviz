using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class MessengerJobTableHandler
	{
		#region MessengerJobTableListView
		public static List<MessengerJobTableListView> GetMessengerJobTableListViewValidation(this List<MessengerJobTableListView> messengerJobTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return messengerJobTableListViews;
			}
			var result = new List<MessengerJobTableListView>();
			try
			{
				foreach (MessengerJobTableListView item in messengerJobTableListViews)
				{
					result.Add(item.GetMessengerJobTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static MessengerJobTableListView GetMessengerJobTableListViewValidation(this MessengerJobTableListView messengerJobTableListView)
		{
			try
			{
				messengerJobTableListView.RowAuthorize = messengerJobTableListView.GetListRowAuthorize();
				return messengerJobTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this MessengerJobTableListView messengerJobTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion MessengerJobTableListView
		#region MessengerJobTableItemView
		public static MessengerJobTableItemView GetMessengerJobTableItemViewValidation(this MessengerJobTableItemView messengerJobTableItemView)
		{
			try
			{
				messengerJobTableItemView.RowAuthorize = messengerJobTableItemView.GetItemRowAuthorize();
				return messengerJobTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this MessengerJobTableItemView messengerJobTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static MessengerJobTable ToMessengerJobTable(this MessengerJobTableItemView messengerJobTableItemView)
		{
			try
			{
				MessengerJobTable messengerJobTable = new MessengerJobTable();
				messengerJobTable.CompanyGUID = messengerJobTableItemView.CompanyGUID.StringToGuid();
				messengerJobTable.CreatedBy = messengerJobTableItemView.CreatedBy;
				messengerJobTable.CreatedDateTime = messengerJobTableItemView.CreatedDateTime.StringToSystemDateTime();
				messengerJobTable.ModifiedBy = messengerJobTableItemView.ModifiedBy;
				messengerJobTable.ModifiedDateTime = messengerJobTableItemView.ModifiedDateTime.StringToSystemDateTime();
				messengerJobTable.Owner = messengerJobTableItemView.Owner;
				messengerJobTable.OwnerBusinessUnitGUID = messengerJobTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				messengerJobTable.MessengerJobTableGUID = messengerJobTableItemView.MessengerJobTableGUID.StringToGuid();
				messengerJobTable.Address1 = messengerJobTableItemView.Address1;
				messengerJobTable.Address2 = messengerJobTableItemView.Address2;
				messengerJobTable.BuyerTableGUID = messengerJobTableItemView.BuyerTableGUID.StringToGuidNull();
				messengerJobTable.ContactPersonExtension = messengerJobTableItemView.ContactPersonExtension;
				messengerJobTable.ContactPersonMobile = messengerJobTableItemView.ContactPersonMobile;
				messengerJobTable.ContactPersonName = messengerJobTableItemView.ContactPersonName;
				messengerJobTable.ContactPersonPhone = messengerJobTableItemView.ContactPersonPhone;
				messengerJobTable.ContactPersonPosition = messengerJobTableItemView.ContactPersonPosition;
				messengerJobTable.ContactTo = messengerJobTableItemView.ContactTo;
				messengerJobTable.CustomerTableGUID = messengerJobTableItemView.CustomerTableGUID.StringToGuidNull();
				messengerJobTable.DocumentStatusGUID = messengerJobTableItemView.DocumentStatusGUID.StringToGuid();
				messengerJobTable.FeeAmount = messengerJobTableItemView.FeeAmount;
				messengerJobTable.JobDate = messengerJobTableItemView.JobDate.StringNullToDateNull();
				messengerJobTable.JobDetail = messengerJobTableItemView.JobDetail;
				messengerJobTable.JobId = messengerJobTableItemView.JobId;
				messengerJobTable.JobStartTime = messengerJobTableItemView.JobStartTime.TimeStringToDateTimeNull();
				messengerJobTable.JobEndTime = messengerJobTableItemView.JobEndTime.TimeStringToDateTimeNull();
				messengerJobTable.JobTypeGUID = messengerJobTableItemView.JobTypeGUID.StringToGuid();
				messengerJobTable.Map = messengerJobTableItemView.Map;
				messengerJobTable.MessengerTableGUID = messengerJobTableItemView.MessengerTableGUID.StringToGuidNull();
				messengerJobTable.Priority = messengerJobTableItemView.Priority;
				messengerJobTable.ProductType = messengerJobTableItemView.ProductType;
				messengerJobTable.RefCreditAppLineGUID = messengerJobTableItemView.RefCreditAppLineGUID.StringToGuidNull();
				messengerJobTable.RefGUID = messengerJobTableItemView.RefGUID.StringToGuidNull();
				messengerJobTable.RefMessengerJobTableGUID = messengerJobTableItemView.RefMessengerJobTableGUID.StringToGuidNull();
				messengerJobTable.RefType = messengerJobTableItemView.RefType;
				messengerJobTable.RequestorGUID = messengerJobTableItemView.RequestorGUID.StringToGuid();
				messengerJobTable.Result = messengerJobTableItemView.Result;
				messengerJobTable.ResultRemark = messengerJobTableItemView.ResultRemark;
				messengerJobTable.ContactName = messengerJobTableItemView.ContactName;
				messengerJobTable.AssignmentAgreementTableGUID = messengerJobTableItemView.AssignmentAgreementTableGUID.StringToGuidNull();
				messengerJobTable.ContactPersonDepartment = messengerJobTableItemView.ContactPersonDepartment;
				messengerJobTable.ExpenseAmount = messengerJobTableItemView.ExpenseAmount;
				
				messengerJobTable.RowVersion = messengerJobTableItemView.RowVersion;
				return messengerJobTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<MessengerJobTable> ToMessengerJobTable(this IEnumerable<MessengerJobTableItemView> messengerJobTableItemViews)
		{
			try
			{
				List<MessengerJobTable> messengerJobTables = new List<MessengerJobTable>();
				foreach (MessengerJobTableItemView item in messengerJobTableItemViews)
				{
					messengerJobTables.Add(item.ToMessengerJobTable());
				}
				return messengerJobTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static MessengerJobTableItemView ToMessengerJobTableItemView(this MessengerJobTable messengerJobTable)
		{
			try
			{
				MessengerJobTableItemView messengerJobTableItemView = new MessengerJobTableItemView();
				messengerJobTableItemView.CompanyGUID = messengerJobTable.CompanyGUID.GuidNullToString();
				messengerJobTableItemView.CreatedBy = messengerJobTable.CreatedBy;
				messengerJobTableItemView.CreatedDateTime = messengerJobTable.CreatedDateTime.DateTimeToString();
				messengerJobTableItemView.ModifiedBy = messengerJobTable.ModifiedBy;
				messengerJobTableItemView.ModifiedDateTime = messengerJobTable.ModifiedDateTime.DateTimeToString();
				messengerJobTableItemView.Owner = messengerJobTable.Owner;
				messengerJobTableItemView.OwnerBusinessUnitGUID = messengerJobTable.OwnerBusinessUnitGUID.GuidNullToString();
				messengerJobTableItemView.MessengerJobTableGUID = messengerJobTable.MessengerJobTableGUID.GuidNullToString();
				messengerJobTableItemView.Address1 = messengerJobTable.Address1;
				messengerJobTableItemView.Address2 = messengerJobTable.Address2;
				messengerJobTableItemView.BuyerTableGUID = messengerJobTable.BuyerTableGUID.GuidNullToString();
				messengerJobTableItemView.ContactPersonExtension = messengerJobTable.ContactPersonExtension;
				messengerJobTableItemView.ContactPersonMobile = messengerJobTable.ContactPersonMobile;
				messengerJobTableItemView.ContactPersonName = messengerJobTable.ContactPersonName;
				messengerJobTableItemView.ContactPersonPhone = messengerJobTable.ContactPersonPhone;
				messengerJobTableItemView.ContactPersonPosition = messengerJobTable.ContactPersonPosition;
				messengerJobTableItemView.ContactTo = messengerJobTable.ContactTo;
				messengerJobTableItemView.CustomerTableGUID = messengerJobTable.CustomerTableGUID.GuidNullToString();
				messengerJobTableItemView.DocumentStatusGUID = messengerJobTable.DocumentStatusGUID.GuidNullToString();
				messengerJobTableItemView.FeeAmount = messengerJobTable.FeeAmount;
				messengerJobTableItemView.JobDate = messengerJobTable.JobDate.DateNullToString();
				messengerJobTableItemView.JobDetail = messengerJobTable.JobDetail;
				messengerJobTableItemView.JobId = messengerJobTable.JobId;
				messengerJobTableItemView.JobStartTime = messengerJobTable.JobStartTime.DateTimeNullToTimeString();
				messengerJobTableItemView.JobEndTime = messengerJobTable.JobEndTime.DateTimeNullToTimeString();
				messengerJobTableItemView.JobTypeGUID = messengerJobTable.JobTypeGUID.GuidNullToString();
				messengerJobTableItemView.Map = messengerJobTable.Map;
				messengerJobTableItemView.MessengerTableGUID = messengerJobTable.MessengerTableGUID.GuidNullToString();
				messengerJobTableItemView.Priority = messengerJobTable.Priority;
				messengerJobTableItemView.ProductType = messengerJobTable.ProductType;
				messengerJobTableItemView.RefCreditAppLineGUID = messengerJobTable.RefCreditAppLineGUID.GuidNullToString();
				messengerJobTableItemView.RefGUID = messengerJobTable.RefGUID.GuidNullToString();
				messengerJobTableItemView.RefMessengerJobTableGUID = messengerJobTable.RefMessengerJobTableGUID.GuidNullToString();
				messengerJobTableItemView.RefType = messengerJobTable.RefType;
				messengerJobTableItemView.RequestorGUID = messengerJobTable.RequestorGUID.GuidNullToString();
				messengerJobTableItemView.Result = messengerJobTable.Result;
				messengerJobTableItemView.ResultRemark = messengerJobTable.ResultRemark;
				messengerJobTableItemView.AssignmentAgreementTableGUID = messengerJobTable.AssignmentAgreementTableGUID.GuidNullToString();
				messengerJobTableItemView.ContactPersonDepartment = messengerJobTable.ContactPersonDepartment;
				messengerJobTableItemView.ExpenseAmount = messengerJobTable.ExpenseAmount;
				
				messengerJobTableItemView.RowVersion = messengerJobTable.RowVersion;
				return messengerJobTableItemView.GetMessengerJobTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion MessengerJobTableItemView
		#region ToDropDown
		public static SelectItem<MessengerJobTableItemView> ToDropDownItem(this MessengerJobTableItemView messengerJobTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<MessengerJobTableItemView> selectItem = new SelectItem<MessengerJobTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(messengerJobTableView.JobId, messengerJobTableView.JobDate);
				selectItem.Value = messengerJobTableView.MessengerJobTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: messengerJobTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<MessengerJobTableItemView>> ToDropDownItem(this IEnumerable<MessengerJobTableItemView> messengerJobTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<MessengerJobTableItemView>> selectItems = new List<SelectItem<MessengerJobTableItemView>>();
				foreach (MessengerJobTableItemView item in messengerJobTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

