using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class ConsortiumLineHandler
	{
		#region ConsortiumLineListView
		public static List<ConsortiumLineListView> GetConsortiumLineListViewValidation(this List<ConsortiumLineListView> consortiumLineListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return consortiumLineListViews;
			}
			var result = new List<ConsortiumLineListView>();
			try
			{
				foreach (ConsortiumLineListView item in consortiumLineListViews)
				{
					result.Add(item.GetConsortiumLineListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ConsortiumLineListView GetConsortiumLineListViewValidation(this ConsortiumLineListView consortiumLineListView)
		{
			try
			{
				consortiumLineListView.RowAuthorize = consortiumLineListView.GetListRowAuthorize();
				return consortiumLineListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this ConsortiumLineListView consortiumLineListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion ConsortiumLineListView
		#region ConsortiumLineItemView
		public static ConsortiumLineItemView GetConsortiumLineItemViewValidation(this ConsortiumLineItemView consortiumLineItemView)
		{
			try
			{
				consortiumLineItemView.RowAuthorize = consortiumLineItemView.GetItemRowAuthorize();
				return consortiumLineItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this ConsortiumLineItemView consortiumLineItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static ConsortiumLine ToConsortiumLine(this ConsortiumLineItemView consortiumLineItemView)
		{
			try
			{
				ConsortiumLine consortiumLine = new ConsortiumLine();
				consortiumLine.CompanyGUID = consortiumLineItemView.CompanyGUID.StringToGuid();
				consortiumLine.CreatedBy = consortiumLineItemView.CreatedBy;
				consortiumLine.CreatedDateTime = consortiumLineItemView.CreatedDateTime.StringToSystemDateTime();
				consortiumLine.ModifiedBy = consortiumLineItemView.ModifiedBy;
				consortiumLine.ModifiedDateTime = consortiumLineItemView.ModifiedDateTime.StringToSystemDateTime();
				consortiumLine.Owner = consortiumLineItemView.Owner;
				consortiumLine.OwnerBusinessUnitGUID = consortiumLineItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				consortiumLine.ConsortiumLineGUID = consortiumLineItemView.ConsortiumLineGUID.StringToGuid();
				consortiumLine.Address = consortiumLineItemView.Address;
				consortiumLine.AuthorizedPersonTypeGUID = consortiumLineItemView.AuthorizedPersonTypeGUID.StringToGuidNull();
				consortiumLine.ConsortiumTableGUID = consortiumLineItemView.ConsortiumTableGUID.StringToGuid();
				consortiumLine.CustomerName = consortiumLineItemView.CustomerName;
				consortiumLine.IsMain = consortiumLineItemView.IsMain;
				consortiumLine.OperatedBy = consortiumLineItemView.OperatedBy;
				consortiumLine.Position = consortiumLineItemView.Position;
				consortiumLine.ProportionOfShareholderPct = consortiumLineItemView.ProportionOfShareholderPct;
				consortiumLine.Remark = consortiumLineItemView.Remark;
				consortiumLine.Ordering = consortiumLineItemView.Ordering;
				
				consortiumLine.RowVersion = consortiumLineItemView.RowVersion;
				return consortiumLine;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<ConsortiumLine> ToConsortiumLine(this IEnumerable<ConsortiumLineItemView> consortiumLineItemViews)
		{
			try
			{
				List<ConsortiumLine> consortiumLines = new List<ConsortiumLine>();
				foreach (ConsortiumLineItemView item in consortiumLineItemViews)
				{
					consortiumLines.Add(item.ToConsortiumLine());
				}
				return consortiumLines;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ConsortiumLineItemView ToConsortiumLineItemView(this ConsortiumLine consortiumLine)
		{
			try
			{
				ConsortiumLineItemView consortiumLineItemView = new ConsortiumLineItemView();
				consortiumLineItemView.CompanyGUID = consortiumLine.CompanyGUID.GuidNullToString();
				consortiumLineItemView.CreatedBy = consortiumLine.CreatedBy;
				consortiumLineItemView.CreatedDateTime = consortiumLine.CreatedDateTime.DateTimeToString();
				consortiumLineItemView.ModifiedBy = consortiumLine.ModifiedBy;
				consortiumLineItemView.ModifiedDateTime = consortiumLine.ModifiedDateTime.DateTimeToString();
				consortiumLineItemView.Owner = consortiumLine.Owner;
				consortiumLineItemView.OwnerBusinessUnitGUID = consortiumLine.OwnerBusinessUnitGUID.GuidNullToString();
				consortiumLineItemView.ConsortiumLineGUID = consortiumLine.ConsortiumLineGUID.GuidNullToString();
				consortiumLineItemView.Address = consortiumLine.Address;
				consortiumLineItemView.AuthorizedPersonTypeGUID = consortiumLine.AuthorizedPersonTypeGUID.GuidNullToString();
				consortiumLineItemView.ConsortiumTableGUID = consortiumLine.ConsortiumTableGUID.GuidNullToString();
				consortiumLineItemView.CustomerName = consortiumLine.CustomerName;
				consortiumLineItemView.IsMain = consortiumLine.IsMain;
				consortiumLineItemView.OperatedBy = consortiumLine.OperatedBy;
				consortiumLineItemView.Position = consortiumLine.Position;
				consortiumLineItemView.ProportionOfShareholderPct = consortiumLine.ProportionOfShareholderPct;
				consortiumLineItemView.Remark = consortiumLine.Remark;
				consortiumLineItemView.Ordering = consortiumLine.Ordering;
				
				consortiumLineItemView.RowVersion = consortiumLine.RowVersion;
				return consortiumLineItemView.GetConsortiumLineItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ConsortiumLineItemView
		#region ToDropDown
		public static SelectItem<ConsortiumLineItemView> ToDropDownItem(this ConsortiumLineItemView consortiumLineView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<ConsortiumLineItemView> selectItem = new SelectItem<ConsortiumLineItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(consortiumLineView.Ordering.ToString(), consortiumLineView.CustomerName);
				selectItem.Value = consortiumLineView.ConsortiumLineGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: consortiumLineView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<ConsortiumLineItemView>> ToDropDownItem(this IEnumerable<ConsortiumLineItemView> consortiumLineItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<ConsortiumLineItemView>> selectItems = new List<SelectItem<ConsortiumLineItemView>>();
				foreach (ConsortiumLineItemView item in consortiumLineItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

