using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class JobTypeHandler
	{
		#region JobTypeListView
		public static List<JobTypeListView> GetJobTypeListViewValidation(this List<JobTypeListView> jobTypeListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return jobTypeListViews;
			}
			var result = new List<JobTypeListView>();
			try
			{
				foreach (JobTypeListView item in jobTypeListViews)
				{
					result.Add(item.GetJobTypeListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static JobTypeListView GetJobTypeListViewValidation(this JobTypeListView jobTypeListView)
		{
			try
			{
				jobTypeListView.RowAuthorize = jobTypeListView.GetListRowAuthorize();
				return jobTypeListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this JobTypeListView jobTypeListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion JobTypeListView
		#region JobTypeItemView
		public static JobTypeItemView GetJobTypeItemViewValidation(this JobTypeItemView jobTypeItemView)
		{
			try
			{
				jobTypeItemView.RowAuthorize = jobTypeItemView.GetItemRowAuthorize();
				return jobTypeItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this JobTypeItemView jobTypeItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static JobType ToJobType(this JobTypeItemView jobTypeItemView)
		{
			try
			{
				JobType jobType = new JobType();
				jobType.CompanyGUID = jobTypeItemView.CompanyGUID.StringToGuid();
				jobType.CreatedBy = jobTypeItemView.CreatedBy;
				jobType.CreatedDateTime = jobTypeItemView.CreatedDateTime.StringToSystemDateTime();
				jobType.ModifiedBy = jobTypeItemView.ModifiedBy;
				jobType.ModifiedDateTime = jobTypeItemView.ModifiedDateTime.StringToSystemDateTime();
				jobType.Owner = jobTypeItemView.Owner;
				jobType.OwnerBusinessUnitGUID = jobTypeItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				jobType.JobTypeGUID = jobTypeItemView.JobTypeGUID.StringToGuid();
				jobType.Description = jobTypeItemView.Description;
				jobType.JobTypeId = jobTypeItemView.JobTypeId;
				jobType.ShowDocConVerifyType = jobTypeItemView.ShowDocConVerifyType;
				jobType.Assignment = jobTypeItemView.Assignment;
				
				jobType.RowVersion = jobTypeItemView.RowVersion;
				return jobType;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<JobType> ToJobType(this IEnumerable<JobTypeItemView> jobTypeItemViews)
		{
			try
			{
				List<JobType> jobTypes = new List<JobType>();
				foreach (JobTypeItemView item in jobTypeItemViews)
				{
					jobTypes.Add(item.ToJobType());
				}
				return jobTypes;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static JobTypeItemView ToJobTypeItemView(this JobType jobType)
		{
			try
			{
				JobTypeItemView jobTypeItemView = new JobTypeItemView();
				jobTypeItemView.CompanyGUID = jobType.CompanyGUID.GuidNullToString();
				jobTypeItemView.CreatedBy = jobType.CreatedBy;
				jobTypeItemView.CreatedDateTime = jobType.CreatedDateTime.DateTimeToString();
				jobTypeItemView.ModifiedBy = jobType.ModifiedBy;
				jobTypeItemView.ModifiedDateTime = jobType.ModifiedDateTime.DateTimeToString();
				jobTypeItemView.Owner = jobType.Owner;
				jobTypeItemView.OwnerBusinessUnitGUID = jobType.OwnerBusinessUnitGUID.GuidNullToString();
				jobTypeItemView.JobTypeGUID = jobType.JobTypeGUID.GuidNullToString();
				jobTypeItemView.Description = jobType.Description;
				jobTypeItemView.JobTypeId = jobType.JobTypeId;
				jobTypeItemView.ShowDocConVerifyType = jobType.ShowDocConVerifyType;
				jobTypeItemView.Assignment = jobType.Assignment;
				
				jobTypeItemView.RowVersion = jobType.RowVersion;
				return jobTypeItemView.GetJobTypeItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion JobTypeItemView
		#region ToDropDown
		public static SelectItem<JobTypeItemView> ToDropDownItem(this JobTypeItemView jobTypeView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<JobTypeItemView> selectItem = new SelectItem<JobTypeItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(jobTypeView.JobTypeId, jobTypeView.Description);
				selectItem.Value = jobTypeView.JobTypeGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: jobTypeView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<JobTypeItemView>> ToDropDownItem(this IEnumerable<JobTypeItemView> jobTypeItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<JobTypeItemView>> selectItems = new List<SelectItem<JobTypeItemView>>();
				foreach (JobTypeItemView item in jobTypeItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

