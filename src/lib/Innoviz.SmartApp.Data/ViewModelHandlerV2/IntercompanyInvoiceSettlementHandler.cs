using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class IntercompanyInvoiceSettlementHandler
	{
		#region IntercompanyInvoiceSettlementListView
		public static List<IntercompanyInvoiceSettlementListView> GetIntercompanyInvoiceSettlementListViewValidation(this List<IntercompanyInvoiceSettlementListView> intercompanyInvoiceSettlementListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return intercompanyInvoiceSettlementListViews;
			}
			var result = new List<IntercompanyInvoiceSettlementListView>();
			try
			{
				foreach (IntercompanyInvoiceSettlementListView item in intercompanyInvoiceSettlementListViews)
				{
					result.Add(item.GetIntercompanyInvoiceSettlementListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IntercompanyInvoiceSettlementListView GetIntercompanyInvoiceSettlementListViewValidation(this IntercompanyInvoiceSettlementListView intercompanyInvoiceSettlementListView)
		{
			try
			{
				intercompanyInvoiceSettlementListView.RowAuthorize = intercompanyInvoiceSettlementListView.GetListRowAuthorize();
				return intercompanyInvoiceSettlementListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this IntercompanyInvoiceSettlementListView intercompanyInvoiceSettlementListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion IntercompanyInvoiceSettlementListView
		#region IntercompanyInvoiceSettlementItemView
		public static IntercompanyInvoiceSettlementItemView GetIntercompanyInvoiceSettlementItemViewValidation(this IntercompanyInvoiceSettlementItemView intercompanyInvoiceSettlementItemView)
		{
			try
			{
				intercompanyInvoiceSettlementItemView.RowAuthorize = intercompanyInvoiceSettlementItemView.GetItemRowAuthorize();
				return intercompanyInvoiceSettlementItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this IntercompanyInvoiceSettlementItemView intercompanyInvoiceSettlementItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static IntercompanyInvoiceSettlement ToIntercompanyInvoiceSettlement(this IntercompanyInvoiceSettlementItemView intercompanyInvoiceSettlementItemView)
		{
			try
			{
				IntercompanyInvoiceSettlement intercompanyInvoiceSettlement = new IntercompanyInvoiceSettlement();
				intercompanyInvoiceSettlement.CompanyGUID = intercompanyInvoiceSettlementItemView.CompanyGUID.StringToGuid();
				intercompanyInvoiceSettlement.CreatedBy = intercompanyInvoiceSettlementItemView.CreatedBy;
				intercompanyInvoiceSettlement.CreatedDateTime = intercompanyInvoiceSettlementItemView.CreatedDateTime.StringToSystemDateTime();
				intercompanyInvoiceSettlement.ModifiedBy = intercompanyInvoiceSettlementItemView.ModifiedBy;
				intercompanyInvoiceSettlement.ModifiedDateTime = intercompanyInvoiceSettlementItemView.ModifiedDateTime.StringToSystemDateTime();
				intercompanyInvoiceSettlement.Owner = intercompanyInvoiceSettlementItemView.Owner;
				intercompanyInvoiceSettlement.OwnerBusinessUnitGUID = intercompanyInvoiceSettlementItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				intercompanyInvoiceSettlement.IntercompanyInvoiceSettlementGUID = intercompanyInvoiceSettlementItemView.IntercompanyInvoiceSettlementGUID.StringToGuid();
				intercompanyInvoiceSettlement.CNReasonGUID = intercompanyInvoiceSettlementItemView.CNReasonGUID.StringToGuidNull();
				intercompanyInvoiceSettlement.DocumentStatusGUID = intercompanyInvoiceSettlementItemView.DocumentStatusGUID.StringToGuidNull();
				intercompanyInvoiceSettlement.IntercompanyInvoiceSettlementId = intercompanyInvoiceSettlementItemView.IntercompanyInvoiceSettlementId;
				intercompanyInvoiceSettlement.IntercompanyInvoiceTableGUID = intercompanyInvoiceSettlementItemView.IntercompanyInvoiceTableGUID.StringToGuid();
				intercompanyInvoiceSettlement.MethodOfPaymentGUID = intercompanyInvoiceSettlementItemView.MethodOfPaymentGUID.StringToGuidNull();
				intercompanyInvoiceSettlement.OrigInvoiceAmount = intercompanyInvoiceSettlementItemView.OrigInvoiceAmount;
				intercompanyInvoiceSettlement.OrigInvoiceId = intercompanyInvoiceSettlementItemView.OrigInvoiceId;
				intercompanyInvoiceSettlement.OrigTaxInvoiceAmount = intercompanyInvoiceSettlementItemView.OrigTaxInvoiceAmount;
				intercompanyInvoiceSettlement.OrigTaxInvoiceId = intercompanyInvoiceSettlementItemView.OrigTaxInvoiceId;
				intercompanyInvoiceSettlement.RefIntercompanyInvoiceSettlementGUID = intercompanyInvoiceSettlementItemView.RefIntercompanyInvoiceSettlementGUID.StringToGuidNull();
				intercompanyInvoiceSettlement.SettleAmount = intercompanyInvoiceSettlementItemView.SettleAmount;
				intercompanyInvoiceSettlement.SettleInvoiceAmount = intercompanyInvoiceSettlementItemView.SettleInvoiceAmount;
				intercompanyInvoiceSettlement.SettleWHTAmount = intercompanyInvoiceSettlementItemView.SettleWHTAmount;
				
				intercompanyInvoiceSettlement.TransDate = intercompanyInvoiceSettlementItemView.TransDate.StringToDate();
				intercompanyInvoiceSettlement.WHTSlipReceivedByCustomer = intercompanyInvoiceSettlementItemView.WHTSlipReceivedByCustomer;
				
				intercompanyInvoiceSettlement.RowVersion = intercompanyInvoiceSettlementItemView.RowVersion;
				return intercompanyInvoiceSettlement;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<IntercompanyInvoiceSettlement> ToIntercompanyInvoiceSettlement(this IEnumerable<IntercompanyInvoiceSettlementItemView> intercompanyInvoiceSettlementItemViews)
		{
			try
			{
				List<IntercompanyInvoiceSettlement> intercompanyInvoiceSettlements = new List<IntercompanyInvoiceSettlement>();
				foreach (IntercompanyInvoiceSettlementItemView item in intercompanyInvoiceSettlementItemViews)
				{
					intercompanyInvoiceSettlements.Add(item.ToIntercompanyInvoiceSettlement());
				}
				return intercompanyInvoiceSettlements;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IntercompanyInvoiceSettlementItemView ToIntercompanyInvoiceSettlementItemView(this IntercompanyInvoiceSettlement intercompanyInvoiceSettlement)
		{
			try
			{
				IntercompanyInvoiceSettlementItemView intercompanyInvoiceSettlementItemView = new IntercompanyInvoiceSettlementItemView();
				intercompanyInvoiceSettlementItemView.CompanyGUID = intercompanyInvoiceSettlement.CompanyGUID.GuidNullToString();
				intercompanyInvoiceSettlementItemView.CreatedBy = intercompanyInvoiceSettlement.CreatedBy;
				intercompanyInvoiceSettlementItemView.CreatedDateTime = intercompanyInvoiceSettlement.CreatedDateTime.DateTimeToString();
				intercompanyInvoiceSettlementItemView.ModifiedBy = intercompanyInvoiceSettlement.ModifiedBy;
				intercompanyInvoiceSettlementItemView.ModifiedDateTime = intercompanyInvoiceSettlement.ModifiedDateTime.DateTimeToString();
				intercompanyInvoiceSettlementItemView.Owner = intercompanyInvoiceSettlement.Owner;
				intercompanyInvoiceSettlementItemView.OwnerBusinessUnitGUID = intercompanyInvoiceSettlement.OwnerBusinessUnitGUID.GuidNullToString();
				intercompanyInvoiceSettlementItemView.IntercompanyInvoiceSettlementGUID = intercompanyInvoiceSettlement.IntercompanyInvoiceSettlementGUID.GuidNullToString();
				intercompanyInvoiceSettlementItemView.CNReasonGUID = intercompanyInvoiceSettlement.CNReasonGUID.GuidNullToString();
				intercompanyInvoiceSettlementItemView.DocumentStatusGUID = intercompanyInvoiceSettlement.DocumentStatusGUID.GuidNullToString();
				intercompanyInvoiceSettlementItemView.IntercompanyInvoiceSettlementId = intercompanyInvoiceSettlement.IntercompanyInvoiceSettlementId;
				intercompanyInvoiceSettlementItemView.IntercompanyInvoiceTableGUID = intercompanyInvoiceSettlement.IntercompanyInvoiceTableGUID.GuidNullToString();
				intercompanyInvoiceSettlementItemView.MethodOfPaymentGUID = intercompanyInvoiceSettlement.MethodOfPaymentGUID.GuidNullToString();
				intercompanyInvoiceSettlementItemView.OrigInvoiceAmount = intercompanyInvoiceSettlement.OrigInvoiceAmount;
				intercompanyInvoiceSettlementItemView.OrigInvoiceId = intercompanyInvoiceSettlement.OrigInvoiceId;
				intercompanyInvoiceSettlementItemView.OrigTaxInvoiceAmount = intercompanyInvoiceSettlement.OrigTaxInvoiceAmount;
				intercompanyInvoiceSettlementItemView.OrigTaxInvoiceId = intercompanyInvoiceSettlement.OrigTaxInvoiceId;
				intercompanyInvoiceSettlementItemView.RefIntercompanyInvoiceSettlementGUID = intercompanyInvoiceSettlement.RefIntercompanyInvoiceSettlementGUID.GuidNullToString();
				intercompanyInvoiceSettlementItemView.SettleAmount = intercompanyInvoiceSettlement.SettleAmount;
				intercompanyInvoiceSettlementItemView.SettleInvoiceAmount = intercompanyInvoiceSettlement.SettleInvoiceAmount;
				intercompanyInvoiceSettlementItemView.SettleWHTAmount = intercompanyInvoiceSettlement.SettleWHTAmount;
				intercompanyInvoiceSettlementItemView.TransDate = intercompanyInvoiceSettlement.TransDate.DateToString();
				intercompanyInvoiceSettlementItemView.WHTSlipReceivedByCustomer = intercompanyInvoiceSettlement.WHTSlipReceivedByCustomer;
				
				intercompanyInvoiceSettlementItemView.RowVersion = intercompanyInvoiceSettlement.RowVersion;
				return intercompanyInvoiceSettlementItemView.GetIntercompanyInvoiceSettlementItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion IntercompanyInvoiceSettlementItemView
		#region ToDropDown
		public static SelectItem<IntercompanyInvoiceSettlementItemView> ToDropDownItem(this IntercompanyInvoiceSettlementItemView intercompanyInvoiceSettlementView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<IntercompanyInvoiceSettlementItemView> selectItem = new SelectItem<IntercompanyInvoiceSettlementItemView>();
				selectItem.Label = null;
				selectItem.Value = intercompanyInvoiceSettlementView.IntercompanyInvoiceSettlementGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: intercompanyInvoiceSettlementView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<IntercompanyInvoiceSettlementItemView>> ToDropDownItem(this IEnumerable<IntercompanyInvoiceSettlementItemView> intercompanyInvoiceSettlementItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<IntercompanyInvoiceSettlementItemView>> selectItems = new List<SelectItem<IntercompanyInvoiceSettlementItemView>>();
				foreach (IntercompanyInvoiceSettlementItemView item in intercompanyInvoiceSettlementItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

