using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CollectionGroupHandler
	{
		#region CollectionGroupListView
		public static List<CollectionGroupListView> GetCollectionGroupListViewValidation(this List<CollectionGroupListView> collectionGroupListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return collectionGroupListViews;
			}
			var result = new List<CollectionGroupListView>();
			try
			{
				foreach (CollectionGroupListView item in collectionGroupListViews)
				{
					result.Add(item.GetCollectionGroupListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CollectionGroupListView GetCollectionGroupListViewValidation(this CollectionGroupListView collectionGroupListView)
		{
			try
			{
				collectionGroupListView.RowAuthorize = collectionGroupListView.GetListRowAuthorize();
				return collectionGroupListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CollectionGroupListView collectionGroupListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CollectionGroupListView
		#region CollectionGroupItemView
		public static CollectionGroupItemView GetCollectionGroupItemViewValidation(this CollectionGroupItemView collectionGroupItemView)
		{
			try
			{
				collectionGroupItemView.RowAuthorize = collectionGroupItemView.GetItemRowAuthorize();
				return collectionGroupItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CollectionGroupItemView collectionGroupItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CollectionGroup ToCollectionGroup(this CollectionGroupItemView collectionGroupItemView)
		{
			try
			{
				CollectionGroup collectionGroup = new CollectionGroup();
				collectionGroup.CompanyGUID = collectionGroupItemView.CompanyGUID.StringToGuid();
				collectionGroup.CreatedBy = collectionGroupItemView.CreatedBy;
				collectionGroup.CreatedDateTime = collectionGroupItemView.CreatedDateTime.StringToSystemDateTime();
				collectionGroup.ModifiedBy = collectionGroupItemView.ModifiedBy;
				collectionGroup.ModifiedDateTime = collectionGroupItemView.ModifiedDateTime.StringToSystemDateTime();
				collectionGroup.Owner = collectionGroupItemView.Owner;
				collectionGroup.OwnerBusinessUnitGUID = collectionGroupItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				collectionGroup.CollectionGroupGUID = collectionGroupItemView.CollectionGroupGUID.StringToGuid();
				collectionGroup.AgreementBranchId = collectionGroupItemView.AgreementBranchId;
				collectionGroup.AgreementFromOverdueDay = collectionGroupItemView.AgreementFromOverdueDay;
				collectionGroup.AgreementLeaseTypeId = collectionGroupItemView.AgreementLeaseTypeId;
				collectionGroup.AgreementToOverdueDay = collectionGroupItemView.AgreementToOverdueDay;
				collectionGroup.CollectionGroupId = collectionGroupItemView.CollectionGroupId;
				collectionGroup.CustGroupId = collectionGroupItemView.CustGroupId;
				collectionGroup.CustTerritoryId = collectionGroupItemView.CustTerritoryId;
				collectionGroup.Description = collectionGroupItemView.Description;
				
				collectionGroup.RowVersion = collectionGroupItemView.RowVersion;
				return collectionGroup;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CollectionGroup> ToCollectionGroup(this IEnumerable<CollectionGroupItemView> collectionGroupItemViews)
		{
			try
			{
				List<CollectionGroup> collectionGroups = new List<CollectionGroup>();
				foreach (CollectionGroupItemView item in collectionGroupItemViews)
				{
					collectionGroups.Add(item.ToCollectionGroup());
				}
				return collectionGroups;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CollectionGroupItemView ToCollectionGroupItemView(this CollectionGroup collectionGroup)
		{
			try
			{
				CollectionGroupItemView collectionGroupItemView = new CollectionGroupItemView();
				collectionGroupItemView.CompanyGUID = collectionGroup.CompanyGUID.GuidNullToString();
				collectionGroupItemView.CreatedBy = collectionGroup.CreatedBy;
				collectionGroupItemView.CreatedDateTime = collectionGroup.CreatedDateTime.DateTimeToString();
				collectionGroupItemView.ModifiedBy = collectionGroup.ModifiedBy;
				collectionGroupItemView.ModifiedDateTime = collectionGroup.ModifiedDateTime.DateTimeToString();
				collectionGroupItemView.Owner = collectionGroup.Owner;
				collectionGroupItemView.OwnerBusinessUnitGUID = collectionGroup.OwnerBusinessUnitGUID.GuidNullToString();
				collectionGroupItemView.CollectionGroupGUID = collectionGroup.CollectionGroupGUID.GuidNullToString();
				collectionGroupItemView.AgreementBranchId = collectionGroup.AgreementBranchId;
				collectionGroupItemView.AgreementFromOverdueDay = collectionGroup.AgreementFromOverdueDay;
				collectionGroupItemView.AgreementLeaseTypeId = collectionGroup.AgreementLeaseTypeId;
				collectionGroupItemView.AgreementToOverdueDay = collectionGroup.AgreementToOverdueDay;
				collectionGroupItemView.CollectionGroupId = collectionGroup.CollectionGroupId;
				collectionGroupItemView.CustGroupId = collectionGroup.CustGroupId;
				collectionGroupItemView.CustTerritoryId = collectionGroup.CustTerritoryId;
				collectionGroupItemView.Description = collectionGroup.Description;
				
				collectionGroupItemView.RowVersion = collectionGroup.RowVersion;
				return collectionGroupItemView.GetCollectionGroupItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CollectionGroupItemView
		#region ToDropDown
		public static SelectItem<CollectionGroupItemView> ToDropDownItem(this CollectionGroupItemView collectionGroupView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CollectionGroupItemView> selectItem = new SelectItem<CollectionGroupItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(collectionGroupView.CollectionGroupId, collectionGroupView.Description);
				selectItem.Value = collectionGroupView.CollectionGroupGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: collectionGroupView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CollectionGroupItemView>> ToDropDownItem(this IEnumerable<CollectionGroupItemView> collectionGroupItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CollectionGroupItemView>> selectItems = new List<SelectItem<CollectionGroupItemView>>();
				foreach (CollectionGroupItemView item in collectionGroupItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

