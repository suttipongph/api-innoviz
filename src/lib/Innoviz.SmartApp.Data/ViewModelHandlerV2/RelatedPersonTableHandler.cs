using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class RelatedPersonTableHandler
	{
		#region RelatedPersonTableListView
		public static List<RelatedPersonTableListView> GetRelatedPersonTableListViewValidation(this List<RelatedPersonTableListView> relatedPersonTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return relatedPersonTableListViews;
			}
			var result = new List<RelatedPersonTableListView>();
			try
			{
				foreach (RelatedPersonTableListView item in relatedPersonTableListViews)
				{
					result.Add(item.GetRelatedPersonTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static RelatedPersonTableListView GetRelatedPersonTableListViewValidation(this RelatedPersonTableListView relatedPersonTableListView)
		{
			try
			{
				relatedPersonTableListView.RowAuthorize = relatedPersonTableListView.GetListRowAuthorize();
				return relatedPersonTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this RelatedPersonTableListView relatedPersonTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion RelatedPersonTableListView
		#region RelatedPersonTableItemView
		public static RelatedPersonTableItemView GetRelatedPersonTableItemViewValidation(this RelatedPersonTableItemView relatedPersonTableItemView)
		{
			try
			{
				relatedPersonTableItemView.RowAuthorize = relatedPersonTableItemView.GetItemRowAuthorize();
				return relatedPersonTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this RelatedPersonTableItemView relatedPersonTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static RelatedPersonTable ToRelatedPersonTable(this RelatedPersonTableItemView relatedPersonTableItemView)
		{
			try
			{
				RelatedPersonTable relatedPersonTable = new RelatedPersonTable();
				relatedPersonTable.CompanyGUID = relatedPersonTableItemView.CompanyGUID.StringToGuid();
				relatedPersonTable.CreatedBy = relatedPersonTableItemView.CreatedBy;
				relatedPersonTable.CreatedDateTime = relatedPersonTableItemView.CreatedDateTime.StringToSystemDateTime();
				relatedPersonTable.ModifiedBy = relatedPersonTableItemView.ModifiedBy;
				relatedPersonTable.ModifiedDateTime = relatedPersonTableItemView.ModifiedDateTime.StringToSystemDateTime();
				relatedPersonTable.Owner = relatedPersonTableItemView.Owner;
				relatedPersonTable.OwnerBusinessUnitGUID = relatedPersonTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				relatedPersonTable.RelatedPersonTableGUID = relatedPersonTableItemView.RelatedPersonTableGUID.StringToGuid();
				relatedPersonTable.BackgroundSummary = relatedPersonTableItemView.BackgroundSummary;
				relatedPersonTable.CompanyName = relatedPersonTableItemView.CompanyName;
				relatedPersonTable.DateOfBirth = relatedPersonTableItemView.DateOfBirth.StringNullToDateNull();
				relatedPersonTable.DateOfEstablishment = relatedPersonTableItemView.DateOfEstablishment.StringNullToDateNull();
				relatedPersonTable.DateOfExpiry = relatedPersonTableItemView.DateOfExpiry.StringNullToDateNull();
				relatedPersonTable.DateOfIssue = relatedPersonTableItemView.DateOfIssue.StringNullToDateNull();
				relatedPersonTable.Email = relatedPersonTableItemView.Email;
				relatedPersonTable.Extension = relatedPersonTableItemView.Extension;
				relatedPersonTable.Fax = relatedPersonTableItemView.Fax;
				relatedPersonTable.GenderGUID = relatedPersonTableItemView.GenderGUID.StringToGuidNull();
				relatedPersonTable.IdentificationType = relatedPersonTableItemView.IdentificationType;
				relatedPersonTable.Income = relatedPersonTableItemView.Income;
				relatedPersonTable.IssuedBy = relatedPersonTableItemView.IssuedBy;
				relatedPersonTable.LineId = relatedPersonTableItemView.LineId;
				relatedPersonTable.MaritalStatusGUID = relatedPersonTableItemView.MaritalStatusGUID.StringToGuidNull();
				relatedPersonTable.Mobile = relatedPersonTableItemView.Mobile;
				relatedPersonTable.Name = relatedPersonTableItemView.Name;
				relatedPersonTable.NationalityGUID = relatedPersonTableItemView.NationalityGUID.StringToGuidNull();
				relatedPersonTable.OccupationGUID = relatedPersonTableItemView.OccupationGUID.StringToGuidNull();
				relatedPersonTable.OtherIncome = relatedPersonTableItemView.OtherIncome;
				relatedPersonTable.OtherSourceIncome = relatedPersonTableItemView.OtherSourceIncome;
				relatedPersonTable.PaidUpCapital = relatedPersonTableItemView.PaidUpCapital;
				relatedPersonTable.PassportId = relatedPersonTableItemView.PassportId;
				relatedPersonTable.Phone = relatedPersonTableItemView.Phone;
				relatedPersonTable.Position = relatedPersonTableItemView.Position;
				relatedPersonTable.RaceGUID = relatedPersonTableItemView.RaceGUID.StringToGuidNull();
				relatedPersonTable.RecordType = relatedPersonTableItemView.RecordType;
				relatedPersonTable.RegisteredCapital = relatedPersonTableItemView.RegisteredCapital;
				relatedPersonTable.RegistrationTypeGUID = relatedPersonTableItemView.RegistrationTypeGUID.StringToGuidNull();
				relatedPersonTable.RelatedPersonId = relatedPersonTableItemView.RelatedPersonId;
				relatedPersonTable.SpouseName = relatedPersonTableItemView.SpouseName;
				relatedPersonTable.TaxId = relatedPersonTableItemView.TaxId;
				relatedPersonTable.WorkExperienceMonth = relatedPersonTableItemView.WorkExperienceMonth;
				relatedPersonTable.WorkExperienceYear = relatedPersonTableItemView.WorkExperienceYear;
				relatedPersonTable.WorkPermitId = relatedPersonTableItemView.WorkPermitId;
				relatedPersonTable.OperatedBy = relatedPersonTableItemView.OperatedBy;
				
				relatedPersonTable.RowVersion = relatedPersonTableItemView.RowVersion;
				return relatedPersonTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<RelatedPersonTable> ToRelatedPersonTable(this IEnumerable<RelatedPersonTableItemView> relatedPersonTableItemViews)
		{
			try
			{
				List<RelatedPersonTable> relatedPersonTables = new List<RelatedPersonTable>();
				foreach (RelatedPersonTableItemView item in relatedPersonTableItemViews)
				{
					relatedPersonTables.Add(item.ToRelatedPersonTable());
				}
				return relatedPersonTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static RelatedPersonTableItemView ToRelatedPersonTableItemView(this RelatedPersonTable relatedPersonTable)
		{
			try
			{
				RelatedPersonTableItemView relatedPersonTableItemView = new RelatedPersonTableItemView();
				relatedPersonTableItemView.CompanyGUID = relatedPersonTable.CompanyGUID.GuidNullToString();
				relatedPersonTableItemView.CreatedBy = relatedPersonTable.CreatedBy;
				relatedPersonTableItemView.CreatedDateTime = relatedPersonTable.CreatedDateTime.DateTimeToString();
				relatedPersonTableItemView.ModifiedBy = relatedPersonTable.ModifiedBy;
				relatedPersonTableItemView.ModifiedDateTime = relatedPersonTable.ModifiedDateTime.DateTimeToString();
				relatedPersonTableItemView.Owner = relatedPersonTable.Owner;
				relatedPersonTableItemView.OwnerBusinessUnitGUID = relatedPersonTable.OwnerBusinessUnitGUID.GuidNullToString();
				relatedPersonTableItemView.RelatedPersonTableGUID = relatedPersonTable.RelatedPersonTableGUID.GuidNullToString();
				relatedPersonTableItemView.BackgroundSummary = relatedPersonTable.BackgroundSummary;
				relatedPersonTableItemView.CompanyName = relatedPersonTable.CompanyName;
				relatedPersonTableItemView.DateOfBirth = relatedPersonTable.DateOfBirth.DateNullToString();
				relatedPersonTableItemView.DateOfEstablishment = relatedPersonTable.DateOfEstablishment.DateNullToString();
				relatedPersonTableItemView.DateOfExpiry = relatedPersonTable.DateOfExpiry.DateNullToString();
				relatedPersonTableItemView.DateOfIssue = relatedPersonTable.DateOfIssue.DateNullToString();
				relatedPersonTableItemView.Email = relatedPersonTable.Email;
				relatedPersonTableItemView.Extension = relatedPersonTable.Extension;
				relatedPersonTableItemView.Fax = relatedPersonTable.Fax;
				relatedPersonTableItemView.GenderGUID = relatedPersonTable.GenderGUID.GuidNullToString();
				relatedPersonTableItemView.IdentificationType = relatedPersonTable.IdentificationType;
				relatedPersonTableItemView.Income = relatedPersonTable.Income;
				relatedPersonTableItemView.IssuedBy = relatedPersonTable.IssuedBy;
				relatedPersonTableItemView.LineId = relatedPersonTable.LineId;
				relatedPersonTableItemView.MaritalStatusGUID = relatedPersonTable.MaritalStatusGUID.GuidNullToString();
				relatedPersonTableItemView.Mobile = relatedPersonTable.Mobile;
				relatedPersonTableItemView.Name = relatedPersonTable.Name;
				relatedPersonTableItemView.NationalityGUID = relatedPersonTable.NationalityGUID.GuidNullToString();
				relatedPersonTableItemView.OccupationGUID = relatedPersonTable.OccupationGUID.GuidNullToString();
				relatedPersonTableItemView.OtherIncome = relatedPersonTable.OtherIncome;
				relatedPersonTableItemView.OtherSourceIncome = relatedPersonTable.OtherSourceIncome;
				relatedPersonTableItemView.PaidUpCapital = relatedPersonTable.PaidUpCapital;
				relatedPersonTableItemView.PassportId = relatedPersonTable.PassportId;
				relatedPersonTableItemView.Phone = relatedPersonTable.Phone;
				relatedPersonTableItemView.Position = relatedPersonTable.Position;
				relatedPersonTableItemView.RaceGUID = relatedPersonTable.RaceGUID.GuidNullToString();
				relatedPersonTableItemView.RecordType = relatedPersonTable.RecordType;
				relatedPersonTableItemView.RegisteredCapital = relatedPersonTable.RegisteredCapital;
				relatedPersonTableItemView.RegistrationTypeGUID = relatedPersonTable.RegistrationTypeGUID.GuidNullToString();
				relatedPersonTableItemView.RelatedPersonId = relatedPersonTable.RelatedPersonId;
				relatedPersonTableItemView.SpouseName = relatedPersonTable.SpouseName;
				relatedPersonTableItemView.TaxId = relatedPersonTable.TaxId;
				relatedPersonTableItemView.WorkExperienceMonth = relatedPersonTable.WorkExperienceMonth;
				relatedPersonTableItemView.WorkExperienceYear = relatedPersonTable.WorkExperienceYear;
				relatedPersonTableItemView.WorkPermitId = relatedPersonTable.WorkPermitId;
				relatedPersonTableItemView.OperatedBy = relatedPersonTable.OperatedBy;

				
				relatedPersonTableItemView.RowVersion = relatedPersonTable.RowVersion;
				return relatedPersonTableItemView.GetRelatedPersonTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion RelatedPersonTableItemView
		#region ToDropDown
		public static SelectItem<RelatedPersonTableItemView> ToDropDownItem(this RelatedPersonTableItemView relatedPersonTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<RelatedPersonTableItemView> selectItem = new SelectItem<RelatedPersonTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(relatedPersonTableView.RelatedPersonId, relatedPersonTableView.Name);
				selectItem.Value = relatedPersonTableView.RelatedPersonTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: relatedPersonTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<RelatedPersonTableItemView>> ToDropDownItem(this IEnumerable<RelatedPersonTableItemView> relatedPersonTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<RelatedPersonTableItemView>> selectItems = new List<SelectItem<RelatedPersonTableItemView>>();
				foreach (RelatedPersonTableItemView item in relatedPersonTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

