using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class ProductSubTypeHandler
	{
		#region ProductSubTypeListView
		public static List<ProductSubTypeListView> GetProductSubTypeListViewValidation(this List<ProductSubTypeListView> productSubTypeListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return productSubTypeListViews;
			}
			var result = new List<ProductSubTypeListView>();
			try
			{
				foreach (ProductSubTypeListView item in productSubTypeListViews)
				{
					result.Add(item.GetProductSubTypeListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ProductSubTypeListView GetProductSubTypeListViewValidation(this ProductSubTypeListView productSubTypeListView)
		{
			try
			{
				productSubTypeListView.RowAuthorize = productSubTypeListView.GetListRowAuthorize();
				return productSubTypeListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this ProductSubTypeListView productSubTypeListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion ProductSubTypeListView
		#region ProductSubTypeItemView
		public static ProductSubTypeItemView GetProductSubTypeItemViewValidation(this ProductSubTypeItemView productSubTypeItemView)
		{
			try
			{
				productSubTypeItemView.RowAuthorize = productSubTypeItemView.GetItemRowAuthorize();
				return productSubTypeItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this ProductSubTypeItemView productSubTypeItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static ProductSubType ToProductSubType(this ProductSubTypeItemView productSubTypeItemView)
		{
			try
			{
				ProductSubType productSubType = new ProductSubType();
				productSubType.CompanyGUID = productSubTypeItemView.CompanyGUID.StringToGuid();
				productSubType.CreatedBy = productSubTypeItemView.CreatedBy;
				productSubType.CreatedDateTime = productSubTypeItemView.CreatedDateTime.StringToSystemDateTime();
				productSubType.ModifiedBy = productSubTypeItemView.ModifiedBy;
				productSubType.ModifiedDateTime = productSubTypeItemView.ModifiedDateTime.StringToSystemDateTime();
				productSubType.Owner = productSubTypeItemView.Owner;
				productSubType.OwnerBusinessUnitGUID = productSubTypeItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				productSubType.ProductSubTypeGUID = productSubTypeItemView.ProductSubTypeGUID.StringToGuid();
				productSubType.Description = productSubTypeItemView.Description;
				productSubType.GuarantorAgreementYear = productSubTypeItemView.GuarantorAgreementYear;
				productSubType.MaxInterestFeePct  = productSubTypeItemView.MaxInterestFeePct ;
				productSubType.ProductSubTypeId = productSubTypeItemView.ProductSubTypeId;
				productSubType.ProductType = productSubTypeItemView.ProductType;
				productSubType.CalcInterestMethod = productSubTypeItemView.CalcInterestMethod;
				productSubType.CalcInterestDayMethod = productSubTypeItemView.CalcInterestDayMethod;
				
				productSubType.RowVersion = productSubTypeItemView.RowVersion;
				return productSubType;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<ProductSubType> ToProductSubType(this IEnumerable<ProductSubTypeItemView> productSubTypeItemViews)
		{
			try
			{
				List<ProductSubType> productSubTypes = new List<ProductSubType>();
				foreach (ProductSubTypeItemView item in productSubTypeItemViews)
				{
					productSubTypes.Add(item.ToProductSubType());
				}
				return productSubTypes;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ProductSubTypeItemView ToProductSubTypeItemView(this ProductSubType productSubType)
		{
			try
			{
				ProductSubTypeItemView productSubTypeItemView = new ProductSubTypeItemView();
				productSubTypeItemView.CompanyGUID = productSubType.CompanyGUID.GuidNullToString();
				productSubTypeItemView.CreatedBy = productSubType.CreatedBy;
				productSubTypeItemView.CreatedDateTime = productSubType.CreatedDateTime.DateTimeToString();
				productSubTypeItemView.ModifiedBy = productSubType.ModifiedBy;
				productSubTypeItemView.ModifiedDateTime = productSubType.ModifiedDateTime.DateTimeToString();
				productSubTypeItemView.Owner = productSubType.Owner;
				productSubTypeItemView.OwnerBusinessUnitGUID = productSubType.OwnerBusinessUnitGUID.GuidNullToString();
				productSubTypeItemView.ProductSubTypeGUID = productSubType.ProductSubTypeGUID.GuidNullToString();
				productSubTypeItemView.Description = productSubType.Description;
				productSubTypeItemView.GuarantorAgreementYear = productSubType.GuarantorAgreementYear;
				productSubTypeItemView.MaxInterestFeePct  = productSubType.MaxInterestFeePct ;
				productSubTypeItemView.ProductSubTypeId = productSubType.ProductSubTypeId;
				productSubTypeItemView.ProductType = productSubType.ProductType;
				productSubTypeItemView.CalcInterestMethod = productSubType.CalcInterestMethod;
				productSubTypeItemView.CalcInterestDayMethod = productSubType.CalcInterestDayMethod;
				
				productSubTypeItemView.RowVersion = productSubType.RowVersion;
				return productSubTypeItemView.GetProductSubTypeItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ProductSubTypeItemView
		#region ToDropDown
		public static SelectItem<ProductSubTypeItemView> ToDropDownItem(this ProductSubTypeItemView productSubTypeView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<ProductSubTypeItemView> selectItem = new SelectItem<ProductSubTypeItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(productSubTypeView.ProductSubTypeId, productSubTypeView.Description);
				selectItem.Value = productSubTypeView.ProductSubTypeGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: productSubTypeView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<ProductSubTypeItemView>> ToDropDownItem(this IEnumerable<ProductSubTypeItemView> productSubTypeItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<ProductSubTypeItemView>> selectItems = new List<SelectItem<ProductSubTypeItemView>>();
				foreach (ProductSubTypeItemView item in productSubTypeItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

