using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class ReceiptTableHandler
	{
		#region ReceiptTableListView
		public static List<ReceiptTableListView> GetReceiptTableListViewValidation(this List<ReceiptTableListView> receiptTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return receiptTableListViews;
			}
			var result = new List<ReceiptTableListView>();
			try
			{
				foreach (ReceiptTableListView item in receiptTableListViews)
				{
					result.Add(item.GetReceiptTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ReceiptTableListView GetReceiptTableListViewValidation(this ReceiptTableListView receiptTableListView)
		{
			try
			{
				receiptTableListView.RowAuthorize = receiptTableListView.GetListRowAuthorize();
				return receiptTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this ReceiptTableListView receiptTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion ReceiptTableListView
		#region ReceiptTableItemView
		public static ReceiptTableItemView GetReceiptTableItemViewValidation(this ReceiptTableItemView receiptTableItemView)
		{
			try
			{
				receiptTableItemView.RowAuthorize = receiptTableItemView.GetItemRowAuthorize();
				return receiptTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this ReceiptTableItemView receiptTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static ReceiptTable ToReceiptTable(this ReceiptTableItemView receiptTableItemView)
		{
			try
			{
				ReceiptTable receiptTable = new ReceiptTable();
				receiptTable.CompanyGUID = receiptTableItemView.CompanyGUID.StringToGuid();
				receiptTable.BranchGUID = receiptTableItemView.BranchGUID.StringToGuid();
				receiptTable.CreatedBy = receiptTableItemView.CreatedBy;
				receiptTable.CreatedDateTime = receiptTableItemView.CreatedDateTime.StringToSystemDateTime();
				receiptTable.ModifiedBy = receiptTableItemView.ModifiedBy;
				receiptTable.ModifiedDateTime = receiptTableItemView.ModifiedDateTime.StringToSystemDateTime();
				receiptTable.Owner = receiptTableItemView.Owner;
				receiptTable.OwnerBusinessUnitGUID = receiptTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				receiptTable.ReceiptTableGUID = receiptTableItemView.ReceiptTableGUID.StringToGuid();
				receiptTable.ChequeBankGroupGUID = receiptTableItemView.ChequeBankGroupGUID.StringToGuidNull();
				receiptTable.ChequeBranch = receiptTableItemView.ChequeBranch;
				receiptTable.ChequeDate = receiptTableItemView.ChequeDate.StringNullToDateNull();
				receiptTable.ChequeNo = receiptTableItemView.ChequeNo;
				receiptTable.CurrencyGUID = receiptTableItemView.CurrencyGUID.StringToGuidNull();
				receiptTable.CustomerTableGUID = receiptTableItemView.CustomerTableGUID.StringToGuid();
				receiptTable.DocumentStatusGUID = receiptTableItemView.DocumentStatusGUID.StringToGuidNull();
				receiptTable.ExchangeRate = receiptTableItemView.ExchangeRate;
				receiptTable.InvoiceSettlementDetailGUID = receiptTableItemView.InvoiceSettlementDetailGUID.StringToGuidNull();
				receiptTable.MethodOfPaymentGUID = receiptTableItemView.MethodOfPaymentGUID.StringToGuidNull();
				receiptTable.OverUnderAmount = receiptTableItemView.OverUnderAmount;
				receiptTable.ReceiptAddress1 = receiptTableItemView.ReceiptAddress1;
				receiptTable.ReceiptAddress2 = receiptTableItemView.ReceiptAddress2;
				receiptTable.ReceiptDate = receiptTableItemView.ReceiptDate.StringToDate();
				receiptTable.ReceiptId = receiptTableItemView.ReceiptId;
				receiptTable.RefGUID = receiptTableItemView.RefGUID.StringToGuid();
				receiptTable.RefType = receiptTableItemView.RefType;
				receiptTable.Remark = receiptTableItemView.Remark;
				receiptTable.SettleAmount = receiptTableItemView.SettleAmount;
				receiptTable.SettleAmountMST = receiptTableItemView.SettleAmountMST;
				receiptTable.SettleBaseAmount = receiptTableItemView.SettleBaseAmount;
				receiptTable.SettleBaseAmountMST = receiptTableItemView.SettleBaseAmountMST;
				receiptTable.SettleTaxAmount = receiptTableItemView.SettleTaxAmount;
				receiptTable.SettleTaxAmountMST = receiptTableItemView.SettleTaxAmountMST;
				receiptTable.TransDate = receiptTableItemView.TransDate.StringToDate();
				
				receiptTable.RowVersion = receiptTableItemView.RowVersion;
				return receiptTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<ReceiptTable> ToReceiptTable(this IEnumerable<ReceiptTableItemView> receiptTableItemViews)
		{
			try
			{
				List<ReceiptTable> receiptTables = new List<ReceiptTable>();
				foreach (ReceiptTableItemView item in receiptTableItemViews)
				{
					receiptTables.Add(item.ToReceiptTable());
				}
				return receiptTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ReceiptTableItemView ToReceiptTableItemView(this ReceiptTable receiptTable)
		{
			try
			{
				ReceiptTableItemView receiptTableItemView = new ReceiptTableItemView();
				receiptTableItemView.CompanyGUID = receiptTable.CompanyGUID.GuidNullToString();
				receiptTableItemView.BranchGUID = receiptTable.BranchGUID.GuidNullToString();
				receiptTableItemView.CreatedBy = receiptTable.CreatedBy;
				receiptTableItemView.CreatedDateTime = receiptTable.CreatedDateTime.DateTimeToString();
				receiptTableItemView.ModifiedBy = receiptTable.ModifiedBy;
				receiptTableItemView.ModifiedDateTime = receiptTable.ModifiedDateTime.DateTimeToString();
				receiptTableItemView.Owner = receiptTable.Owner;
				receiptTableItemView.OwnerBusinessUnitGUID = receiptTable.OwnerBusinessUnitGUID.GuidNullToString();
				receiptTableItemView.ReceiptTableGUID = receiptTable.ReceiptTableGUID.GuidNullToString();
				receiptTableItemView.ChequeBankGroupGUID = receiptTable.ChequeBankGroupGUID.GuidNullToString();
				receiptTableItemView.ChequeBranch = receiptTable.ChequeBranch;
				receiptTableItemView.ChequeDate = receiptTable.ChequeDate.DateNullToString();
				receiptTableItemView.ChequeNo = receiptTable.ChequeNo;
				receiptTableItemView.CurrencyGUID = receiptTable.CurrencyGUID.GuidNullToString();
				receiptTableItemView.CustomerTableGUID = receiptTable.CustomerTableGUID.GuidNullToString();
				receiptTableItemView.DocumentStatusGUID = receiptTable.DocumentStatusGUID.GuidNullToString();
				receiptTableItemView.ExchangeRate = receiptTable.ExchangeRate;
				receiptTableItemView.InvoiceSettlementDetailGUID = receiptTable.InvoiceSettlementDetailGUID.GuidNullToString();
				receiptTableItemView.MethodOfPaymentGUID = receiptTable.MethodOfPaymentGUID.GuidNullToString();
				receiptTableItemView.OverUnderAmount = receiptTable.OverUnderAmount;
				receiptTableItemView.ReceiptAddress1 = receiptTable.ReceiptAddress1;
				receiptTableItemView.ReceiptAddress2 = receiptTable.ReceiptAddress2;
				receiptTableItemView.ReceiptDate = receiptTable.ReceiptDate.DateToString();
				receiptTableItemView.ReceiptId = receiptTable.ReceiptId;
				receiptTableItemView.RefGUID = receiptTable.RefGUID.GuidNullToString();
				receiptTableItemView.RefType = receiptTable.RefType;
				receiptTableItemView.Remark = receiptTable.Remark;
				receiptTableItemView.SettleAmount = receiptTable.SettleAmount;
				receiptTableItemView.SettleAmountMST = receiptTable.SettleAmountMST;
				receiptTableItemView.SettleBaseAmount = receiptTable.SettleBaseAmount;
				receiptTableItemView.SettleBaseAmountMST = receiptTable.SettleBaseAmountMST;
				receiptTableItemView.SettleTaxAmount = receiptTable.SettleTaxAmount;
				receiptTableItemView.SettleTaxAmountMST = receiptTable.SettleTaxAmountMST;
				receiptTableItemView.TransDate = receiptTable.TransDate.DateToString();
				
				receiptTableItemView.RowVersion = receiptTable.RowVersion;
				return receiptTableItemView.GetReceiptTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ReceiptTableItemView
		#region ToDropDown
		public static SelectItem<ReceiptTableItemView> ToDropDownItem(this ReceiptTableItemView receiptTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<ReceiptTableItemView> selectItem = new SelectItem<ReceiptTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(receiptTableView.ReceiptId, receiptTableView.ReceiptDate.ToString());
				selectItem.Value = receiptTableView.ReceiptTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: receiptTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<ReceiptTableItemView>> ToDropDownItem(this IEnumerable<ReceiptTableItemView> receiptTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<ReceiptTableItemView>> selectItems = new List<SelectItem<ReceiptTableItemView>>();
				foreach (ReceiptTableItemView item in receiptTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

