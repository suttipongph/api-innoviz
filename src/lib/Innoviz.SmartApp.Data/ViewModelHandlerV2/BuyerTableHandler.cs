using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class BuyerTableHandler
	{
		#region BuyerTableListView
		public static List<BuyerTableListView> GetBuyerTableListViewValidation(this List<BuyerTableListView> buyerTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return buyerTableListViews;
			}
			var result = new List<BuyerTableListView>();
			try
			{
				foreach (BuyerTableListView item in buyerTableListViews)
				{
					result.Add(item.GetBuyerTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BuyerTableListView GetBuyerTableListViewValidation(this BuyerTableListView buyerTableListView)
		{
			try
			{
				buyerTableListView.RowAuthorize = buyerTableListView.GetListRowAuthorize();
				return buyerTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this BuyerTableListView buyerTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion BuyerTableListView
		#region BuyerTableItemView
		public static BuyerTableItemView GetBuyerTableItemViewValidation(this BuyerTableItemView buyerTableItemView)
		{
			try
			{
				buyerTableItemView.RowAuthorize = buyerTableItemView.GetItemRowAuthorize();
				return buyerTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this BuyerTableItemView buyerTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static BuyerTable ToBuyerTable(this BuyerTableItemView buyerTableItemView)
		{
			try
			{
				BuyerTable buyerTable = new BuyerTable();
				buyerTable.CompanyGUID = buyerTableItemView.CompanyGUID.StringToGuid();
				buyerTable.CreatedBy = buyerTableItemView.CreatedBy;
				buyerTable.CreatedDateTime = buyerTableItemView.CreatedDateTime.StringToSystemDateTime();
				buyerTable.ModifiedBy = buyerTableItemView.ModifiedBy;
				buyerTable.ModifiedDateTime = buyerTableItemView.ModifiedDateTime.StringToSystemDateTime();
				buyerTable.Owner = buyerTableItemView.Owner;
				buyerTable.OwnerBusinessUnitGUID = buyerTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				buyerTable.BuyerTableGUID = buyerTableItemView.BuyerTableGUID.StringToGuid();
				buyerTable.AltName = buyerTableItemView.AltName;
				buyerTable.BlacklistStatusGUID = buyerTableItemView.BlacklistStatusGUID.StringToGuidNull();
				buyerTable.BusinessSegmentGUID = buyerTableItemView.BusinessSegmentGUID.StringToGuid();
				buyerTable.BusinessSizeGUID = buyerTableItemView.BusinessSizeGUID.StringToGuidNull();
				buyerTable.BusinessTypeGUID = buyerTableItemView.BusinessTypeGUID.StringToGuidNull();
				buyerTable.BuyerId = buyerTableItemView.BuyerId;
				buyerTable.BuyerName = buyerTableItemView.BuyerName;
				buyerTable.CreditScoringGUID = buyerTableItemView.CreditScoringGUID.StringToGuidNull();
				buyerTable.CurrencyGUID = buyerTableItemView.CurrencyGUID.StringToGuid();
				buyerTable.DateOfEstablish = buyerTableItemView.DateOfEstablish.StringNullToDateNull();
				buyerTable.DateOfExpiry = buyerTableItemView.DateOfExpiry.StringNullToDateNull();
				buyerTable.DateOfIssue = buyerTableItemView.DateOfIssue.StringNullToDateNull();
				buyerTable.DocumentStatusGUID = buyerTableItemView.DocumentStatusGUID.StringToGuidNull();
				buyerTable.FixedAssets = buyerTableItemView.FixedAssets;
				buyerTable.IdentificationType = buyerTableItemView.IdentificationType;
				buyerTable.IssuedBy = buyerTableItemView.IssuedBy;
				buyerTable.KYCSetupGUID = buyerTableItemView.KYCSetupGUID.StringToGuidNull();
				buyerTable.Labor = buyerTableItemView.Labor;
				buyerTable.LineOfBusinessGUID = buyerTableItemView.LineOfBusinessGUID.StringToGuidNull();
				buyerTable.PaidUpCapital = buyerTableItemView.PaidUpCapital;
				buyerTable.PassportId = buyerTableItemView.PassportId;
				buyerTable.ReferenceId = buyerTableItemView.ReferenceId;
				buyerTable.RegisteredCapital = buyerTableItemView.RegisteredCapital;
				buyerTable.TaxId = buyerTableItemView.TaxId;
				buyerTable.WorkPermitId = buyerTableItemView.WorkPermitId;
				
				buyerTable.RowVersion = buyerTableItemView.RowVersion;
				return buyerTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<BuyerTable> ToBuyerTable(this IEnumerable<BuyerTableItemView> buyerTableItemViews)
		{
			try
			{
				List<BuyerTable> buyerTables = new List<BuyerTable>();
				foreach (BuyerTableItemView item in buyerTableItemViews)
				{
					buyerTables.Add(item.ToBuyerTable());
				}
				return buyerTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BuyerTableItemView ToBuyerTableItemView(this BuyerTable buyerTable)
		{
			try
			{
				BuyerTableItemView buyerTableItemView = new BuyerTableItemView();
				buyerTableItemView.CompanyGUID = buyerTable.CompanyGUID.GuidNullToString();
				buyerTableItemView.CreatedBy = buyerTable.CreatedBy;
				buyerTableItemView.CreatedDateTime = buyerTable.CreatedDateTime.DateTimeToString();
				buyerTableItemView.ModifiedBy = buyerTable.ModifiedBy;
				buyerTableItemView.ModifiedDateTime = buyerTable.ModifiedDateTime.DateTimeToString();
				buyerTableItemView.Owner = buyerTable.Owner;
				buyerTableItemView.OwnerBusinessUnitGUID = buyerTable.OwnerBusinessUnitGUID.GuidNullToString();
				buyerTableItemView.BuyerTableGUID = buyerTable.BuyerTableGUID.GuidNullToString();
				buyerTableItemView.AltName = buyerTable.AltName;
				buyerTableItemView.BlacklistStatusGUID = buyerTable.BlacklistStatusGUID.GuidNullToString();
				buyerTableItemView.BusinessSegmentGUID = buyerTable.BusinessSegmentGUID.GuidNullToString();
				buyerTableItemView.BusinessSizeGUID = buyerTable.BusinessSizeGUID.GuidNullToString();
				buyerTableItemView.BusinessTypeGUID = buyerTable.BusinessTypeGUID.GuidNullToString();
				buyerTableItemView.BuyerId = buyerTable.BuyerId;
				buyerTableItemView.BuyerName = buyerTable.BuyerName;
				buyerTableItemView.CreditScoringGUID = buyerTable.CreditScoringGUID.GuidNullToString();
				buyerTableItemView.CurrencyGUID = buyerTable.CurrencyGUID.GuidNullToString();
				buyerTableItemView.DateOfEstablish = buyerTable.DateOfEstablish.DateNullToString();
				buyerTableItemView.DateOfExpiry = buyerTable.DateOfExpiry.DateNullToString();
				buyerTableItemView.DateOfIssue = buyerTable.DateOfIssue.DateNullToString();
				buyerTableItemView.DocumentStatusGUID = buyerTable.DocumentStatusGUID.GuidNullToString();
				buyerTableItemView.FixedAssets = buyerTable.FixedAssets;
				buyerTableItemView.IdentificationType = buyerTable.IdentificationType;
				buyerTableItemView.IssuedBy = buyerTable.IssuedBy;
				buyerTableItemView.KYCSetupGUID = buyerTable.KYCSetupGUID.GuidNullToString();
				buyerTableItemView.Labor = buyerTable.Labor;
				buyerTableItemView.LineOfBusinessGUID = buyerTable.LineOfBusinessGUID.GuidNullToString();
				buyerTableItemView.PaidUpCapital = buyerTable.PaidUpCapital;
				buyerTableItemView.PassportId = buyerTable.PassportId;
				buyerTableItemView.ReferenceId = buyerTable.ReferenceId;
				buyerTableItemView.RegisteredCapital = buyerTable.RegisteredCapital;
				buyerTableItemView.TaxId = buyerTable.TaxId;
				buyerTableItemView.WorkPermitId = buyerTable.WorkPermitId;
				
				buyerTableItemView.RowVersion = buyerTable.RowVersion;
				return buyerTableItemView.GetBuyerTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion BuyerTableItemView
		#region ToDropDown
		public static SelectItem<BuyerTableItemView> ToDropDownItem(this BuyerTableItemView buyerTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<BuyerTableItemView> selectItem = new SelectItem<BuyerTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(buyerTableView.BuyerId, buyerTableView.BuyerName);
				selectItem.Value = buyerTableView.BuyerTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: buyerTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<BuyerTableItemView>> ToDropDownItem(this IEnumerable<BuyerTableItemView> buyerTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<BuyerTableItemView>> selectItems = new List<SelectItem<BuyerTableItemView>>();
				foreach (BuyerTableItemView item in buyerTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

