using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class ProjectReferenceTransHandler
	{
		#region ProjectReferenceTransListView
		public static List<ProjectReferenceTransListView> GetProjectReferenceTransListViewValidation(this List<ProjectReferenceTransListView> projectReferenceTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return projectReferenceTransListViews;
			}
			var result = new List<ProjectReferenceTransListView>();
			try
			{
				foreach (ProjectReferenceTransListView item in projectReferenceTransListViews)
				{
					result.Add(item.GetProjectReferenceTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ProjectReferenceTransListView GetProjectReferenceTransListViewValidation(this ProjectReferenceTransListView projectReferenceTransListView)
		{
			try
			{
				projectReferenceTransListView.RowAuthorize = projectReferenceTransListView.GetListRowAuthorize();
				return projectReferenceTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this ProjectReferenceTransListView projectReferenceTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion ProjectReferenceTransListView
		#region ProjectReferenceTransItemView
		public static ProjectReferenceTransItemView GetProjectReferenceTransItemViewValidation(this ProjectReferenceTransItemView projectReferenceTransItemView)
		{
			try
			{
				projectReferenceTransItemView.RowAuthorize = projectReferenceTransItemView.GetItemRowAuthorize();
				return projectReferenceTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this ProjectReferenceTransItemView projectReferenceTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static ProjectReferenceTrans ToProjectReferenceTrans(this ProjectReferenceTransItemView projectReferenceTransItemView)
		{
			try
			{
				ProjectReferenceTrans projectReferenceTrans = new ProjectReferenceTrans();
				projectReferenceTrans.CompanyGUID = projectReferenceTransItemView.CompanyGUID.StringToGuid();
				projectReferenceTrans.CreatedBy = projectReferenceTransItemView.CreatedBy;
				projectReferenceTrans.CreatedDateTime = projectReferenceTransItemView.CreatedDateTime.StringToSystemDateTime();
				projectReferenceTrans.ModifiedBy = projectReferenceTransItemView.ModifiedBy;
				projectReferenceTrans.ModifiedDateTime = projectReferenceTransItemView.ModifiedDateTime.StringToSystemDateTime();
				projectReferenceTrans.Owner = projectReferenceTransItemView.Owner;
				projectReferenceTrans.OwnerBusinessUnitGUID = projectReferenceTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				projectReferenceTrans.ProjectReferenceTransGUID = projectReferenceTransItemView.ProjectReferenceTransGUID.StringToGuid();
				projectReferenceTrans.ProjectCompanyName = projectReferenceTransItemView.ProjectCompanyName;
				projectReferenceTrans.ProjectCompletion = projectReferenceTransItemView.ProjectCompletion;
				projectReferenceTrans.ProjectName = projectReferenceTransItemView.ProjectName;
				projectReferenceTrans.ProjectStatus = projectReferenceTransItemView.ProjectStatus;
				projectReferenceTrans.ProjectValue = projectReferenceTransItemView.ProjectValue;
				projectReferenceTrans.RefGUID = projectReferenceTransItemView.RefGUID.StringToGuidNull();
				projectReferenceTrans.RefType = projectReferenceTransItemView.RefType;
				projectReferenceTrans.Remark = projectReferenceTransItemView.Remark;
				
				projectReferenceTrans.RowVersion = projectReferenceTransItemView.RowVersion;
				return projectReferenceTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<ProjectReferenceTrans> ToProjectReferenceTrans(this IEnumerable<ProjectReferenceTransItemView> projectReferenceTransItemViews)
		{
			try
			{
				List<ProjectReferenceTrans> projectReferenceTranss = new List<ProjectReferenceTrans>();
				foreach (ProjectReferenceTransItemView item in projectReferenceTransItemViews)
				{
					projectReferenceTranss.Add(item.ToProjectReferenceTrans());
				}
				return projectReferenceTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ProjectReferenceTransItemView ToProjectReferenceTransItemView(this ProjectReferenceTrans projectReferenceTrans)
		{
			try
			{
				ProjectReferenceTransItemView projectReferenceTransItemView = new ProjectReferenceTransItemView();
				projectReferenceTransItemView.CompanyGUID = projectReferenceTrans.CompanyGUID.GuidNullToString();
				projectReferenceTransItemView.CreatedBy = projectReferenceTrans.CreatedBy;
				projectReferenceTransItemView.CreatedDateTime = projectReferenceTrans.CreatedDateTime.DateTimeToString();
				projectReferenceTransItemView.ModifiedBy = projectReferenceTrans.ModifiedBy;
				projectReferenceTransItemView.ModifiedDateTime = projectReferenceTrans.ModifiedDateTime.DateTimeToString();
				projectReferenceTransItemView.Owner = projectReferenceTrans.Owner;
				projectReferenceTransItemView.OwnerBusinessUnitGUID = projectReferenceTrans.OwnerBusinessUnitGUID.GuidNullToString();
				projectReferenceTransItemView.ProjectReferenceTransGUID = projectReferenceTrans.ProjectReferenceTransGUID.GuidNullToString();
				projectReferenceTransItemView.ProjectCompanyName = projectReferenceTrans.ProjectCompanyName;
				projectReferenceTransItemView.ProjectCompletion = projectReferenceTrans.ProjectCompletion;
				projectReferenceTransItemView.ProjectName = projectReferenceTrans.ProjectName;
				projectReferenceTransItemView.ProjectStatus = projectReferenceTrans.ProjectStatus;
				projectReferenceTransItemView.ProjectValue = projectReferenceTrans.ProjectValue;
				projectReferenceTransItemView.RefGUID = projectReferenceTrans.RefGUID.GuidNullToString();
				projectReferenceTransItemView.RefType = projectReferenceTrans.RefType;
				projectReferenceTransItemView.Remark = projectReferenceTrans.Remark;
				
				projectReferenceTransItemView.RowVersion = projectReferenceTrans.RowVersion;
				return projectReferenceTransItemView.GetProjectReferenceTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ProjectReferenceTransItemView
		#region ToDropDown
		public static SelectItem<ProjectReferenceTransItemView> ToDropDownItem(this ProjectReferenceTransItemView projectReferenceTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<ProjectReferenceTransItemView> selectItem = new SelectItem<ProjectReferenceTransItemView>();
				selectItem.Label = null;
				selectItem.Value = projectReferenceTransView.ProjectReferenceTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: projectReferenceTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<ProjectReferenceTransItemView>> ToDropDownItem(this IEnumerable<ProjectReferenceTransItemView> projectReferenceTransItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<ProjectReferenceTransItemView>> selectItems = new List<SelectItem<ProjectReferenceTransItemView>>();
				foreach (ProjectReferenceTransItemView item in projectReferenceTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

