using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class ReceiptTempPaymDetailHandler
	{
		#region ReceiptTempPaymDetailListView
		public static List<ReceiptTempPaymDetailListView> GetReceiptTempPaymDetailListViewValidation(this List<ReceiptTempPaymDetailListView> receiptTempPaymDetailListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return receiptTempPaymDetailListViews;
			}
			var result = new List<ReceiptTempPaymDetailListView>();
			try
			{
				foreach (ReceiptTempPaymDetailListView item in receiptTempPaymDetailListViews)
				{
					result.Add(item.GetReceiptTempPaymDetailListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ReceiptTempPaymDetailListView GetReceiptTempPaymDetailListViewValidation(this ReceiptTempPaymDetailListView receiptTempPaymDetailListView)
		{
			try
			{
				receiptTempPaymDetailListView.RowAuthorize = receiptTempPaymDetailListView.GetListRowAuthorize();
				return receiptTempPaymDetailListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this ReceiptTempPaymDetailListView receiptTempPaymDetailListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion ReceiptTempPaymDetailListView
		#region ReceiptTempPaymDetailItemView
		public static ReceiptTempPaymDetailItemView GetReceiptTempPaymDetailItemViewValidation(this ReceiptTempPaymDetailItemView receiptTempPaymDetailItemView)
		{
			try
			{
				receiptTempPaymDetailItemView.RowAuthorize = receiptTempPaymDetailItemView.GetItemRowAuthorize();
				return receiptTempPaymDetailItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this ReceiptTempPaymDetailItemView receiptTempPaymDetailItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static ReceiptTempPaymDetail ToReceiptTempPaymDetail(this ReceiptTempPaymDetailItemView receiptTempPaymDetailItemView)
		{
			try
			{
				ReceiptTempPaymDetail receiptTempPaymDetail = new ReceiptTempPaymDetail();
				receiptTempPaymDetail.CompanyGUID = receiptTempPaymDetailItemView.CompanyGUID.StringToGuid();
				receiptTempPaymDetail.CreatedBy = receiptTempPaymDetailItemView.CreatedBy;
				receiptTempPaymDetail.CreatedDateTime = receiptTempPaymDetailItemView.CreatedDateTime.StringToSystemDateTime();
				receiptTempPaymDetail.ModifiedBy = receiptTempPaymDetailItemView.ModifiedBy;
				receiptTempPaymDetail.ModifiedDateTime = receiptTempPaymDetailItemView.ModifiedDateTime.StringToSystemDateTime();
				receiptTempPaymDetail.Owner = receiptTempPaymDetailItemView.Owner;
				receiptTempPaymDetail.OwnerBusinessUnitGUID = receiptTempPaymDetailItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				receiptTempPaymDetail.ReceiptTempPaymDetailGUID = receiptTempPaymDetailItemView.ReceiptTempPaymDetailGUID.StringToGuid();
				receiptTempPaymDetail.ChequeBankGroupGUID = receiptTempPaymDetailItemView.ChequeBankGroupGUID.StringToGuidNull();
				receiptTempPaymDetail.ChequeBranch = receiptTempPaymDetailItemView.ChequeBranch;
				receiptTempPaymDetail.ChequeTableGUID = receiptTempPaymDetailItemView.ChequeTableGUID.StringToGuidNull();
				receiptTempPaymDetail.MethodOfPaymentGUID = receiptTempPaymDetailItemView.MethodOfPaymentGUID.StringToGuid();
				receiptTempPaymDetail.ReceiptAmount = receiptTempPaymDetailItemView.ReceiptAmount;
				receiptTempPaymDetail.ReceiptTempTableGUID = receiptTempPaymDetailItemView.ReceiptTempTableGUID.StringToGuid();
				receiptTempPaymDetail.TransferReference = receiptTempPaymDetailItemView.TransferReference;
				
				receiptTempPaymDetail.RowVersion = receiptTempPaymDetailItemView.RowVersion;
				return receiptTempPaymDetail;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<ReceiptTempPaymDetail> ToReceiptTempPaymDetail(this IEnumerable<ReceiptTempPaymDetailItemView> receiptTempPaymDetailItemViews)
		{
			try
			{
				List<ReceiptTempPaymDetail> receiptTempPaymDetails = new List<ReceiptTempPaymDetail>();
				foreach (ReceiptTempPaymDetailItemView item in receiptTempPaymDetailItemViews)
				{
					receiptTempPaymDetails.Add(item.ToReceiptTempPaymDetail());
				}
				return receiptTempPaymDetails;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ReceiptTempPaymDetailItemView ToReceiptTempPaymDetailItemView(this ReceiptTempPaymDetail receiptTempPaymDetail)
		{
			try
			{
				ReceiptTempPaymDetailItemView receiptTempPaymDetailItemView = new ReceiptTempPaymDetailItemView();
				receiptTempPaymDetailItemView.CompanyGUID = receiptTempPaymDetail.CompanyGUID.GuidNullToString();
				receiptTempPaymDetailItemView.CreatedBy = receiptTempPaymDetail.CreatedBy;
				receiptTempPaymDetailItemView.CreatedDateTime = receiptTempPaymDetail.CreatedDateTime.DateTimeToString();
				receiptTempPaymDetailItemView.ModifiedBy = receiptTempPaymDetail.ModifiedBy;
				receiptTempPaymDetailItemView.ModifiedDateTime = receiptTempPaymDetail.ModifiedDateTime.DateTimeToString();
				receiptTempPaymDetailItemView.Owner = receiptTempPaymDetail.Owner;
				receiptTempPaymDetailItemView.OwnerBusinessUnitGUID = receiptTempPaymDetail.OwnerBusinessUnitGUID.GuidNullToString();
				receiptTempPaymDetailItemView.ReceiptTempPaymDetailGUID = receiptTempPaymDetail.ReceiptTempPaymDetailGUID.GuidNullToString();
				receiptTempPaymDetailItemView.ChequeBankGroupGUID = receiptTempPaymDetail.ChequeBankGroupGUID.GuidNullToString();
				receiptTempPaymDetailItemView.ChequeBranch = receiptTempPaymDetail.ChequeBranch;
				receiptTempPaymDetailItemView.ChequeTableGUID = receiptTempPaymDetail.ChequeTableGUID.GuidNullToString();
				receiptTempPaymDetailItemView.MethodOfPaymentGUID = receiptTempPaymDetail.MethodOfPaymentGUID.GuidNullToString();
				receiptTempPaymDetailItemView.ReceiptAmount = receiptTempPaymDetail.ReceiptAmount;
				receiptTempPaymDetailItemView.ReceiptTempTableGUID = receiptTempPaymDetail.ReceiptTempTableGUID.GuidNullToString();
				receiptTempPaymDetailItemView.TransferReference = receiptTempPaymDetail.TransferReference;
				
				receiptTempPaymDetailItemView.RowVersion = receiptTempPaymDetail.RowVersion;
				return receiptTempPaymDetailItemView.GetReceiptTempPaymDetailItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ReceiptTempPaymDetailItemView
		#region ToDropDown
		public static SelectItem<ReceiptTempPaymDetailItemView> ToDropDownItem(this ReceiptTempPaymDetailItemView receiptTempPaymDetailView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<ReceiptTempPaymDetailItemView> selectItem = new SelectItem<ReceiptTempPaymDetailItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(receiptTempPaymDetailView.ReceiptTempTableGUID.ToString(), receiptTempPaymDetailView.MethodOfPaymentGUID.ToString());
				selectItem.Value = receiptTempPaymDetailView.ReceiptTempPaymDetailGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: receiptTempPaymDetailView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<ReceiptTempPaymDetailItemView>> ToDropDownItem(this IEnumerable<ReceiptTempPaymDetailItemView> receiptTempPaymDetailItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<ReceiptTempPaymDetailItemView>> selectItems = new List<SelectItem<ReceiptTempPaymDetailItemView>>();
				foreach (ReceiptTempPaymDetailItemView item in receiptTempPaymDetailItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

