using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class VerificationTypeHandler
	{
		#region VerificationTypeListView
		public static List<VerificationTypeListView> GetVerificationTypeListViewValidation(this List<VerificationTypeListView> verificationTypeListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return verificationTypeListViews;
			}
			var result = new List<VerificationTypeListView>();
			try
			{
				foreach (VerificationTypeListView item in verificationTypeListViews)
				{
					result.Add(item.GetVerificationTypeListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static VerificationTypeListView GetVerificationTypeListViewValidation(this VerificationTypeListView verificationTypeListView)
		{
			try
			{
				verificationTypeListView.RowAuthorize = verificationTypeListView.GetListRowAuthorize();
				return verificationTypeListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this VerificationTypeListView verificationTypeListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion VerificationTypeListView
		#region VerificationTypeItemView
		public static VerificationTypeItemView GetVerificationTypeItemViewValidation(this VerificationTypeItemView verificationTypeItemView)
		{
			try
			{
				verificationTypeItemView.RowAuthorize = verificationTypeItemView.GetItemRowAuthorize();
				return verificationTypeItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this VerificationTypeItemView verificationTypeItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static VerificationType ToVerificationType(this VerificationTypeItemView verificationTypeItemView)
		{
			try
			{
				VerificationType verificationType = new VerificationType();
				verificationType.CompanyGUID = verificationTypeItemView.CompanyGUID.StringToGuid();
				verificationType.CreatedBy = verificationTypeItemView.CreatedBy;
				verificationType.CreatedDateTime = verificationTypeItemView.CreatedDateTime.StringToSystemDateTime();
				verificationType.ModifiedBy = verificationTypeItemView.ModifiedBy;
				verificationType.ModifiedDateTime = verificationTypeItemView.ModifiedDateTime.StringToSystemDateTime();
				verificationType.Owner = verificationTypeItemView.Owner;
				verificationType.OwnerBusinessUnitGUID = verificationTypeItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				verificationType.VerificationTypeGUID = verificationTypeItemView.VerificationTypeGUID.StringToGuid();
				verificationType.Description = verificationTypeItemView.Description;
				verificationType.VerificationTypeId = verificationTypeItemView.VerificationTypeId;
				verificationType.VerifyType = verificationTypeItemView.VerifyType;
				
				verificationType.RowVersion = verificationTypeItemView.RowVersion;
				return verificationType;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<VerificationType> ToVerificationType(this IEnumerable<VerificationTypeItemView> verificationTypeItemViews)
		{
			try
			{
				List<VerificationType> verificationTypes = new List<VerificationType>();
				foreach (VerificationTypeItemView item in verificationTypeItemViews)
				{
					verificationTypes.Add(item.ToVerificationType());
				}
				return verificationTypes;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static VerificationTypeItemView ToVerificationTypeItemView(this VerificationType verificationType)
		{
			try
			{
				VerificationTypeItemView verificationTypeItemView = new VerificationTypeItemView();
				verificationTypeItemView.CompanyGUID = verificationType.CompanyGUID.GuidNullToString();
				verificationTypeItemView.CreatedBy = verificationType.CreatedBy;
				verificationTypeItemView.CreatedDateTime = verificationType.CreatedDateTime.DateTimeToString();
				verificationTypeItemView.ModifiedBy = verificationType.ModifiedBy;
				verificationTypeItemView.ModifiedDateTime = verificationType.ModifiedDateTime.DateTimeToString();
				verificationTypeItemView.Owner = verificationType.Owner;
				verificationTypeItemView.OwnerBusinessUnitGUID = verificationType.OwnerBusinessUnitGUID.GuidNullToString();
				verificationTypeItemView.VerificationTypeGUID = verificationType.VerificationTypeGUID.GuidNullToString();
				verificationTypeItemView.Description = verificationType.Description;
				verificationTypeItemView.VerificationTypeId = verificationType.VerificationTypeId;
				verificationTypeItemView.VerifyType = verificationType.VerifyType;
				
				verificationTypeItemView.RowVersion = verificationType.RowVersion;
				return verificationTypeItemView.GetVerificationTypeItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion VerificationTypeItemView
		#region ToDropDown
		public static SelectItem<VerificationTypeItemView> ToDropDownItem(this VerificationTypeItemView verificationTypeView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<VerificationTypeItemView> selectItem = new SelectItem<VerificationTypeItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(verificationTypeView.VerificationTypeId, verificationTypeView.Description);
				selectItem.Value = verificationTypeView.VerificationTypeGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: verificationTypeView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<VerificationTypeItemView>> ToDropDownItem(this IEnumerable<VerificationTypeItemView> verificationTypeItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<VerificationTypeItemView>> selectItems = new List<SelectItem<VerificationTypeItemView>>();
				foreach (VerificationTypeItemView item in verificationTypeItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

