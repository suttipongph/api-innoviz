using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class ContactPersonTransHandler
	{
		#region ContactPersonTransListView
		public static List<ContactPersonTransListView> GetContactPersonTransListViewValidation(this List<ContactPersonTransListView> contactPersonTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return contactPersonTransListViews;
			}
			var result = new List<ContactPersonTransListView>();
			try
			{
				foreach (ContactPersonTransListView item in contactPersonTransListViews)
				{
					result.Add(item.GetContactPersonTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ContactPersonTransListView GetContactPersonTransListViewValidation(this ContactPersonTransListView contactPersonTransListView)
		{
			try
			{
				contactPersonTransListView.RowAuthorize = contactPersonTransListView.GetListRowAuthorize();
				return contactPersonTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this ContactPersonTransListView contactPersonTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion ContactPersonTransListView
		#region ContactPersonTransItemView
		public static ContactPersonTransItemView GetContactPersonTransItemViewValidation(this ContactPersonTransItemView contactPersonTransItemView)
		{
			try
			{
				contactPersonTransItemView.RowAuthorize = contactPersonTransItemView.GetItemRowAuthorize();
				return contactPersonTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this ContactPersonTransItemView contactPersonTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static ContactPersonTrans ToContactPersonTrans(this ContactPersonTransItemView contactPersonTransItemView)
		{
			try
			{
				ContactPersonTrans contactPersonTrans = new ContactPersonTrans();
				contactPersonTrans.CompanyGUID = contactPersonTransItemView.CompanyGUID.StringToGuid();
				contactPersonTrans.CreatedBy = contactPersonTransItemView.CreatedBy;
				contactPersonTrans.CreatedDateTime = contactPersonTransItemView.CreatedDateTime.StringToSystemDateTime();
				contactPersonTrans.ModifiedBy = contactPersonTransItemView.ModifiedBy;
				contactPersonTrans.ModifiedDateTime = contactPersonTransItemView.ModifiedDateTime.StringToSystemDateTime();
				contactPersonTrans.Owner = contactPersonTransItemView.Owner;
				contactPersonTrans.OwnerBusinessUnitGUID = contactPersonTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				contactPersonTrans.ContactPersonTransGUID = contactPersonTransItemView.ContactPersonTransGUID.StringToGuid();
				contactPersonTrans.InActive = contactPersonTransItemView.InActive;
				contactPersonTrans.RefGUID = contactPersonTransItemView.RefGUID.StringToGuid();
				contactPersonTrans.RefType = contactPersonTransItemView.RefType;
				contactPersonTrans.RelatedPersonTableGUID = contactPersonTransItemView.RelatedPersonTableGUID.StringToGuid();
				
				contactPersonTrans.RowVersion = contactPersonTransItemView.RowVersion;
				return contactPersonTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<ContactPersonTrans> ToContactPersonTrans(this IEnumerable<ContactPersonTransItemView> contactPersonTransItemViews)
		{
			try
			{
				List<ContactPersonTrans> contactPersonTranss = new List<ContactPersonTrans>();
				foreach (ContactPersonTransItemView item in contactPersonTransItemViews)
				{
					contactPersonTranss.Add(item.ToContactPersonTrans());
				}
				return contactPersonTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ContactPersonTransItemView ToContactPersonTransItemView(this ContactPersonTrans contactPersonTrans)
		{
			try
			{
				ContactPersonTransItemView contactPersonTransItemView = new ContactPersonTransItemView();
				contactPersonTransItemView.CompanyGUID = contactPersonTrans.CompanyGUID.GuidNullToString();
				contactPersonTransItemView.CreatedBy = contactPersonTrans.CreatedBy;
				contactPersonTransItemView.CreatedDateTime = contactPersonTrans.CreatedDateTime.DateTimeToString();
				contactPersonTransItemView.ModifiedBy = contactPersonTrans.ModifiedBy;
				contactPersonTransItemView.ModifiedDateTime = contactPersonTrans.ModifiedDateTime.DateTimeToString();
				contactPersonTransItemView.Owner = contactPersonTrans.Owner;
				contactPersonTransItemView.OwnerBusinessUnitGUID = contactPersonTrans.OwnerBusinessUnitGUID.GuidNullToString();
				contactPersonTransItemView.ContactPersonTransGUID = contactPersonTrans.ContactPersonTransGUID.GuidNullToString();
				contactPersonTransItemView.InActive = contactPersonTrans.InActive;
				contactPersonTransItemView.RefGUID = contactPersonTrans.RefGUID.GuidNullToString();
				contactPersonTransItemView.RefType = contactPersonTrans.RefType;
				contactPersonTransItemView.RelatedPersonTableGUID = contactPersonTrans.RelatedPersonTableGUID.GuidNullToString();
				
				contactPersonTransItemView.RowVersion = contactPersonTrans.RowVersion;
				return contactPersonTransItemView.GetContactPersonTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ContactPersonTransItemView
		#region ToDropDown
		public static SelectItem<ContactPersonTransItemView> ToDropDownItem(this ContactPersonTransItemView contactPersonTransView, bool excludeRowData = false, bool useValues = false)
		{
			try
			{
				SelectItem<ContactPersonTransItemView> selectItem = new SelectItem<ContactPersonTransItemView>();
                selectItem.Label = useValues ? contactPersonTransView.ContactPersonTrans_Values : SmartAppUtil.GetDropDownLabel(contactPersonTransView.RelatedPersonTable_RelatedPersonId, contactPersonTransView.RelatedPersonTable_Name);
                selectItem.Value = contactPersonTransView.ContactPersonTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: contactPersonTransView;
				return selectItem;
			}
			catch (Exception ex) 
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<ContactPersonTransItemView>> ToDropDownItem(this IEnumerable<ContactPersonTransItemView> contactPersonTransItemViews, bool excludeRowData = false, bool useValues = false)
		{
			try
			{
				List<SelectItem<ContactPersonTransItemView>> selectItems = new List<SelectItem<ContactPersonTransItemView>>();
				foreach (ContactPersonTransItemView item in contactPersonTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData, useValues));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

