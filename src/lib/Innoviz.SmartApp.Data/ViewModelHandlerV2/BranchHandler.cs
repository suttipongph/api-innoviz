using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class BranchHandler
	{
		#region BranchListView
		public static List<BranchListView> GetBranchListViewValidation(this List<BranchListView> branchListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return branchListViews;
			}
			var result = new List<BranchListView>();
			try
			{
				foreach (BranchListView item in branchListViews)
				{
					result.Add(item.GetBranchListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BranchListView GetBranchListViewValidation(this BranchListView branchListView)
		{
			try
			{
				branchListView.RowAuthorize = branchListView.GetListRowAuthorize();
				return branchListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this BranchListView branchListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion BranchListView
		#region BranchItemView
		public static BranchItemView GetBranchItemViewValidation(this BranchItemView branchItemView)
		{
			try
			{
				branchItemView.RowAuthorize = branchItemView.GetItemRowAuthorize();
				return branchItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this BranchItemView branchItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static Branch ToBranch(this BranchItemView branchItemView)
		{
			try
			{
				Branch branch = new Branch();
				branch.CompanyGUID = branchItemView.CompanyGUID.StringToGuid();
				branch.CreatedBy = branchItemView.CreatedBy;
				branch.CreatedDateTime = branchItemView.CreatedDateTime.StringToSystemDateTime();
				branch.ModifiedBy = branchItemView.ModifiedBy;
				branch.ModifiedDateTime = branchItemView.ModifiedDateTime.StringToSystemDateTime();
				branch.Owner = branchItemView.Owner;
				branch.OwnerBusinessUnitGUID = branchItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				branch.BranchGUID = branchItemView.BranchGUID.StringToGuid();
				branch.BranchId = branchItemView.BranchId;
				branch.CompanyId = branchItemView.CompanyId;
				branch.Name = branchItemView.Name;
				branch.TaxBranchGUID = branchItemView.TaxBranchGUID.StringToGuidNull();
				
				branch.RowVersion = branchItemView.RowVersion;
				return branch;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<Branch> ToBranch(this IEnumerable<BranchItemView> branchItemViews)
		{
			try
			{
				List<Branch> branchs = new List<Branch>();
				foreach (BranchItemView item in branchItemViews)
				{
					branchs.Add(item.ToBranch());
				}
				return branchs;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BranchItemView ToBranchItemView(this Branch branch)
		{
			try
			{
				BranchItemView branchItemView = new BranchItemView();
				branchItemView.CompanyGUID = branch.CompanyGUID.GuidNullToString();
				branchItemView.CreatedBy = branch.CreatedBy;
				branchItemView.CreatedDateTime = branch.CreatedDateTime.DateTimeToString();
				branchItemView.ModifiedBy = branch.ModifiedBy;
				branchItemView.ModifiedDateTime = branch.ModifiedDateTime.DateTimeToString();
				branchItemView.Owner = branch.Owner;
				branchItemView.OwnerBusinessUnitGUID = branch.OwnerBusinessUnitGUID.GuidNullToString();
				branchItemView.BranchGUID = branch.BranchGUID.GuidNullToString();
				branchItemView.BranchId = branch.BranchId;
				branchItemView.CompanyId = branch.CompanyId;
				branchItemView.Name = branch.Name;
				branchItemView.TaxBranchGUID = branch.TaxBranchGUID.GuidNullToString();
				
				branchItemView.RowVersion = branch.RowVersion;
				return branchItemView.GetBranchItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion BranchItemView
		#region ToDropDown
		public static SelectItem<BranchItemView> ToDropDownItem(this BranchItemView branchView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<BranchItemView> selectItem = new SelectItem<BranchItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(branchView.BranchId, branchView.Name);
				selectItem.Value = branchView.BranchGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: branchView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<BranchItemView>> ToDropDownItem(this IEnumerable<BranchItemView> branchItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<BranchItemView>> selectItems = new List<SelectItem<BranchItemView>>();
				foreach (BranchItemView item in branchItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

