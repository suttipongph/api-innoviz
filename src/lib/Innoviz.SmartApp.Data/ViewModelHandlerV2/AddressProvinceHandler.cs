using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class AddressProvinceHandler
	{
		#region AddressProvinceListView
		public static List<AddressProvinceListView> GetAddressProvinceListViewValidation(this List<AddressProvinceListView> addressProvinceListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return addressProvinceListViews;
			}
			var result = new List<AddressProvinceListView>();
			try
			{
				foreach (AddressProvinceListView item in addressProvinceListViews)
				{
					result.Add(item.GetAddressProvinceListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AddressProvinceListView GetAddressProvinceListViewValidation(this AddressProvinceListView addressProvinceListView)
		{
			try
			{
				addressProvinceListView.RowAuthorize = addressProvinceListView.GetListRowAuthorize();
				return addressProvinceListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this AddressProvinceListView addressProvinceListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion AddressProvinceListView
		#region AddressProvinceItemView
		public static AddressProvinceItemView GetAddressProvinceItemViewValidation(this AddressProvinceItemView addressProvinceItemView)
		{
			try
			{
				addressProvinceItemView.RowAuthorize = addressProvinceItemView.GetItemRowAuthorize();
				return addressProvinceItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this AddressProvinceItemView addressProvinceItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static AddressProvince ToAddressProvince(this AddressProvinceItemView addressProvinceItemView)
		{
			try
			{
				AddressProvince addressProvince = new AddressProvince();
				addressProvince.CompanyGUID = addressProvinceItemView.CompanyGUID.StringToGuid();
				addressProvince.CreatedBy = addressProvinceItemView.CreatedBy;
				addressProvince.CreatedDateTime = addressProvinceItemView.CreatedDateTime.StringToSystemDateTime();
				addressProvince.ModifiedBy = addressProvinceItemView.ModifiedBy;
				addressProvince.ModifiedDateTime = addressProvinceItemView.ModifiedDateTime.StringToSystemDateTime();
				addressProvince.Owner = addressProvinceItemView.Owner;
				addressProvince.OwnerBusinessUnitGUID = addressProvinceItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				addressProvince.AddressProvinceGUID = addressProvinceItemView.AddressProvinceGUID.StringToGuid();
				addressProvince.AddressCountryGUID = addressProvinceItemView.AddressCountryGUID.StringToGuid();
				addressProvince.Name = addressProvinceItemView.Name;
				addressProvince.ProvinceId = addressProvinceItemView.ProvinceId;
				
				addressProvince.RowVersion = addressProvinceItemView.RowVersion;
				return addressProvince;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<AddressProvince> ToAddressProvince(this IEnumerable<AddressProvinceItemView> addressProvinceItemViews)
		{
			try
			{
				List<AddressProvince> addressProvinces = new List<AddressProvince>();
				foreach (AddressProvinceItemView item in addressProvinceItemViews)
				{
					addressProvinces.Add(item.ToAddressProvince());
				}
				return addressProvinces;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AddressProvinceItemView ToAddressProvinceItemView(this AddressProvince addressProvince)
		{
			try
			{
				AddressProvinceItemView addressProvinceItemView = new AddressProvinceItemView();
				addressProvinceItemView.CompanyGUID = addressProvince.CompanyGUID.GuidNullToString();
				addressProvinceItemView.CreatedBy = addressProvince.CreatedBy;
				addressProvinceItemView.CreatedDateTime = addressProvince.CreatedDateTime.DateTimeToString();
				addressProvinceItemView.ModifiedBy = addressProvince.ModifiedBy;
				addressProvinceItemView.ModifiedDateTime = addressProvince.ModifiedDateTime.DateTimeToString();
				addressProvinceItemView.Owner = addressProvince.Owner;
				addressProvinceItemView.OwnerBusinessUnitGUID = addressProvince.OwnerBusinessUnitGUID.GuidNullToString();
				addressProvinceItemView.AddressProvinceGUID = addressProvince.AddressProvinceGUID.GuidNullToString();
				addressProvinceItemView.AddressCountryGUID = addressProvince.AddressCountryGUID.GuidNullToString();
				addressProvinceItemView.Name = addressProvince.Name;
				addressProvinceItemView.ProvinceId = addressProvince.ProvinceId;
				
				addressProvinceItemView.RowVersion = addressProvince.RowVersion;
				return addressProvinceItemView.GetAddressProvinceItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion AddressProvinceItemView
		#region ToDropDown
		public static SelectItem<AddressProvinceItemView> ToDropDownItem(this AddressProvinceItemView addressProvinceView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<AddressProvinceItemView> selectItem = new SelectItem<AddressProvinceItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(addressProvinceView.ProvinceId, addressProvinceView.Name);
				selectItem.Value = addressProvinceView.AddressProvinceGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: addressProvinceView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<AddressProvinceItemView>> ToDropDownItem(this IEnumerable<AddressProvinceItemView> addressProvinceItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<AddressProvinceItemView>> selectItems = new List<SelectItem<AddressProvinceItemView>>();
				foreach (AddressProvinceItemView item in addressProvinceItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

