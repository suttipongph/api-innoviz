using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class PropertyTypeHandler
	{
		#region PropertyTypeListView
		public static List<PropertyTypeListView> GetPropertyTypeListViewValidation(this List<PropertyTypeListView> propertyTypeListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return propertyTypeListViews;
			}
			var result = new List<PropertyTypeListView>();
			try
			{
				foreach (PropertyTypeListView item in propertyTypeListViews)
				{
					result.Add(item.GetPropertyTypeListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static PropertyTypeListView GetPropertyTypeListViewValidation(this PropertyTypeListView propertyTypeListView)
		{
			try
			{
				propertyTypeListView.RowAuthorize = propertyTypeListView.GetListRowAuthorize();
				return propertyTypeListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this PropertyTypeListView propertyTypeListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion PropertyTypeListView
		#region PropertyTypeItemView
		public static PropertyTypeItemView GetPropertyTypeItemViewValidation(this PropertyTypeItemView propertyTypeItemView)
		{
			try
			{
				propertyTypeItemView.RowAuthorize = propertyTypeItemView.GetItemRowAuthorize();
				return propertyTypeItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this PropertyTypeItemView propertyTypeItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static PropertyType ToPropertyType(this PropertyTypeItemView propertyTypeItemView)
		{
			try
			{
				PropertyType propertyType = new PropertyType();
				propertyType.CompanyGUID = propertyTypeItemView.CompanyGUID.StringToGuid();
				propertyType.CreatedBy = propertyTypeItemView.CreatedBy;
				propertyType.CreatedDateTime = propertyTypeItemView.CreatedDateTime.StringToSystemDateTime();
				propertyType.ModifiedBy = propertyTypeItemView.ModifiedBy;
				propertyType.ModifiedDateTime = propertyTypeItemView.ModifiedDateTime.StringToSystemDateTime();
				propertyType.Owner = propertyTypeItemView.Owner;
				propertyType.OwnerBusinessUnitGUID = propertyTypeItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				propertyType.PropertyTypeGUID = propertyTypeItemView.PropertyTypeGUID.StringToGuid();
				propertyType.Description = propertyTypeItemView.Description;
				propertyType.PropertyTypeId = propertyTypeItemView.PropertyTypeId;
				
				propertyType.RowVersion = propertyTypeItemView.RowVersion;
				return propertyType;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<PropertyType> ToPropertyType(this IEnumerable<PropertyTypeItemView> propertyTypeItemViews)
		{
			try
			{
				List<PropertyType> propertyTypes = new List<PropertyType>();
				foreach (PropertyTypeItemView item in propertyTypeItemViews)
				{
					propertyTypes.Add(item.ToPropertyType());
				}
				return propertyTypes;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static PropertyTypeItemView ToPropertyTypeItemView(this PropertyType propertyType)
		{
			try
			{
				PropertyTypeItemView propertyTypeItemView = new PropertyTypeItemView();
				propertyTypeItemView.CompanyGUID = propertyType.CompanyGUID.GuidNullToString();
				propertyTypeItemView.CreatedBy = propertyType.CreatedBy;
				propertyTypeItemView.CreatedDateTime = propertyType.CreatedDateTime.DateTimeToString();
				propertyTypeItemView.ModifiedBy = propertyType.ModifiedBy;
				propertyTypeItemView.ModifiedDateTime = propertyType.ModifiedDateTime.DateTimeToString();
				propertyTypeItemView.Owner = propertyType.Owner;
				propertyTypeItemView.OwnerBusinessUnitGUID = propertyType.OwnerBusinessUnitGUID.GuidNullToString();
				propertyTypeItemView.PropertyTypeGUID = propertyType.PropertyTypeGUID.GuidNullToString();
				propertyTypeItemView.Description = propertyType.Description;
				propertyTypeItemView.PropertyTypeId = propertyType.PropertyTypeId;
				
				propertyTypeItemView.RowVersion = propertyType.RowVersion;
				return propertyTypeItemView.GetPropertyTypeItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion PropertyTypeItemView
		#region ToDropDown
		public static SelectItem<PropertyTypeItemView> ToDropDownItem(this PropertyTypeItemView propertyTypeView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<PropertyTypeItemView> selectItem = new SelectItem<PropertyTypeItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(propertyTypeView.PropertyTypeId, propertyTypeView.Description);
				selectItem.Value = propertyTypeView.PropertyTypeGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: propertyTypeView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<PropertyTypeItemView>> ToDropDownItem(this IEnumerable<PropertyTypeItemView> propertyTypeItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<PropertyTypeItemView>> selectItems = new List<SelectItem<PropertyTypeItemView>>();
				foreach (PropertyTypeItemView item in propertyTypeItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

