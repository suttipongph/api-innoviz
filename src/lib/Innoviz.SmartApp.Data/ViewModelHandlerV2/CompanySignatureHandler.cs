using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CompanySignatureHandler
	{
		#region CompanySignatureListView
		public static List<CompanySignatureListView> GetCompanySignatureListViewValidation(this List<CompanySignatureListView> companySignatureListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return companySignatureListViews;
			}
			var result = new List<CompanySignatureListView>();
			try
			{
				foreach (CompanySignatureListView item in companySignatureListViews)
				{
					result.Add(item.GetCompanySignatureListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CompanySignatureListView GetCompanySignatureListViewValidation(this CompanySignatureListView companySignatureListView)
		{
			try
			{
				companySignatureListView.RowAuthorize = companySignatureListView.GetListRowAuthorize();
				return companySignatureListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CompanySignatureListView companySignatureListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CompanySignatureListView
		#region CompanySignatureItemView
		public static CompanySignatureItemView GetCompanySignatureItemViewValidation(this CompanySignatureItemView companySignatureItemView)
		{
			try
			{
				companySignatureItemView.RowAuthorize = companySignatureItemView.GetItemRowAuthorize();
				return companySignatureItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CompanySignatureItemView companySignatureItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CompanySignature ToCompanySignature(this CompanySignatureItemView companySignatureItemView)
		{
			try
			{
				CompanySignature companySignature = new CompanySignature();
				companySignature.BranchGUID = companySignatureItemView.DefaultBranchGUID.StringToGuid();
				companySignature.CompanyGUID = companySignatureItemView.CompanyGUID.StringToGuid();
				companySignature.CreatedBy = companySignatureItemView.CreatedBy;
				companySignature.CreatedDateTime = companySignatureItemView.CreatedDateTime.StringToSystemDateTime();
				companySignature.ModifiedBy = companySignatureItemView.ModifiedBy;
				companySignature.ModifiedDateTime = companySignatureItemView.ModifiedDateTime.StringToSystemDateTime();
				companySignature.Owner = companySignatureItemView.Owner;
				companySignature.OwnerBusinessUnitGUID = companySignatureItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				companySignature.CompanySignatureGUID = companySignatureItemView.CompanySignatureGUID.StringToGuid();
				companySignature.EmployeeTableGUID = companySignatureItemView.EmployeeTableGUID.StringToGuid();
				companySignature.Ordering = companySignatureItemView.Ordering;
				companySignature.RefType = companySignatureItemView.RefType;
				
				companySignature.RowVersion = companySignatureItemView.RowVersion;
				return companySignature;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CompanySignature> ToCompanySignature(this IEnumerable<CompanySignatureItemView> companySignatureItemViews)
		{
			try
			{
				List<CompanySignature> companySignatures = new List<CompanySignature>();
				foreach (CompanySignatureItemView item in companySignatureItemViews)
				{
					companySignatures.Add(item.ToCompanySignature());
				}
				return companySignatures;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CompanySignatureItemView ToCompanySignatureItemView(this CompanySignature companySignature)
		{
			try
			{
				CompanySignatureItemView companySignatureItemView = new CompanySignatureItemView();
				companySignatureItemView.BranchGUID = companySignature.BranchGUID.GuidNullToString();
				companySignatureItemView.CompanyGUID = companySignature.CompanyGUID.GuidNullToString();
				companySignatureItemView.CreatedBy = companySignature.CreatedBy;
				companySignatureItemView.CreatedDateTime = companySignature.CreatedDateTime.DateTimeToString();
				companySignatureItemView.ModifiedBy = companySignature.ModifiedBy;
				companySignatureItemView.ModifiedDateTime = companySignature.ModifiedDateTime.DateTimeToString();
				companySignatureItemView.Owner = companySignature.Owner;
				companySignatureItemView.OwnerBusinessUnitGUID = companySignature.OwnerBusinessUnitGUID.GuidNullToString();
				companySignatureItemView.CompanySignatureGUID = companySignature.CompanySignatureGUID.GuidNullToString();
				companySignatureItemView.EmployeeTableGUID = companySignature.EmployeeTableGUID.GuidNullToString();
				companySignatureItemView.Ordering = companySignature.Ordering;
				companySignatureItemView.RefType = companySignature.RefType;
				companySignatureItemView.DefaultBranchGUID = companySignature.BranchGUID.GuidNullToString();
				
				companySignatureItemView.RowVersion = companySignature.RowVersion;
				return companySignatureItemView.GetCompanySignatureItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CompanySignatureItemView
		#region ToDropDown
		public static SelectItem<CompanySignatureItemView> ToDropDownItem(this CompanySignatureItemView companySignatureView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CompanySignatureItemView> selectItem = new SelectItem<CompanySignatureItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(companySignatureView.EmployeeTable_EmployeeId, companySignatureView.EmployeeTable_Name);
				selectItem.Value = companySignatureView.CompanySignatureGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: companySignatureView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CompanySignatureItemView>> ToDropDownItem(this IEnumerable<CompanySignatureItemView> companySignatureItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CompanySignatureItemView>> selectItems = new List<SelectItem<CompanySignatureItemView>>();
				foreach (CompanySignatureItemView item in companySignatureItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

