using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class AgreementTableInfoHandler
	{
		#region AgreementTableInfoListView
		public static List<AgreementTableInfoListView> GetAgreementTableInfoListViewValidation(this List<AgreementTableInfoListView> agreementTableInfoListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return agreementTableInfoListViews;
			}
			var result = new List<AgreementTableInfoListView>();
			try
			{
				foreach (AgreementTableInfoListView item in agreementTableInfoListViews)
				{
					result.Add(item.GetAgreementTableInfoListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AgreementTableInfoListView GetAgreementTableInfoListViewValidation(this AgreementTableInfoListView agreementTableInfoListView)
		{
			try
			{
				agreementTableInfoListView.RowAuthorize = agreementTableInfoListView.GetListRowAuthorize();
				return agreementTableInfoListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this AgreementTableInfoListView agreementTableInfoListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion AgreementTableInfoListView
		#region AgreementTableInfoItemView
		public static AgreementTableInfoItemView GetAgreementTableInfoItemViewValidation(this AgreementTableInfoItemView agreementTableInfoItemView)
		{
			try
			{
				agreementTableInfoItemView.RowAuthorize = agreementTableInfoItemView.GetItemRowAuthorize();
				return agreementTableInfoItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this AgreementTableInfoItemView agreementTableInfoItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static AgreementTableInfo ToAgreementTableInfo(this AgreementTableInfoItemView agreementTableInfoItemView)
		{
			try
			{
				AgreementTableInfo agreementTableInfo = new AgreementTableInfo();
				agreementTableInfo.CompanyGUID = agreementTableInfoItemView.CompanyGUID.StringToGuid();
				agreementTableInfo.CreatedBy = agreementTableInfoItemView.CreatedBy;
				agreementTableInfo.CreatedDateTime = agreementTableInfoItemView.CreatedDateTime.StringToSystemDateTime();
				agreementTableInfo.ModifiedBy = agreementTableInfoItemView.ModifiedBy;
				agreementTableInfo.ModifiedDateTime = agreementTableInfoItemView.ModifiedDateTime.StringToSystemDateTime();
				agreementTableInfo.Owner = agreementTableInfoItemView.Owner;
				agreementTableInfo.OwnerBusinessUnitGUID = agreementTableInfoItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				agreementTableInfo.AgreementTableInfoGUID = agreementTableInfoItemView.AgreementTableInfoGUID.StringToGuid();
				agreementTableInfo.AuthorizedPersonTransBuyer1GUID = agreementTableInfoItemView.AuthorizedPersonTransBuyer1GUID.StringToGuidNull();
				agreementTableInfo.AuthorizedPersonTransBuyer2GUID = agreementTableInfoItemView.AuthorizedPersonTransBuyer2GUID.StringToGuidNull();
				agreementTableInfo.AuthorizedPersonTransBuyer3GUID = agreementTableInfoItemView.AuthorizedPersonTransBuyer3GUID.StringToGuidNull();
				agreementTableInfo.AuthorizedPersonTransCompany1GUID = agreementTableInfoItemView.AuthorizedPersonTransCompany1GUID.StringToGuidNull();
				agreementTableInfo.AuthorizedPersonTransCompany2GUID = agreementTableInfoItemView.AuthorizedPersonTransCompany2GUID.StringToGuidNull();
				agreementTableInfo.AuthorizedPersonTransCompany3GUID = agreementTableInfoItemView.AuthorizedPersonTransCompany3GUID.StringToGuidNull();
				agreementTableInfo.AuthorizedPersonTransCustomer1GUID = agreementTableInfoItemView.AuthorizedPersonTransCustomer1GUID.StringToGuidNull();
				agreementTableInfo.AuthorizedPersonTransCustomer2GUID = agreementTableInfoItemView.AuthorizedPersonTransCustomer2GUID.StringToGuidNull();
				agreementTableInfo.AuthorizedPersonTransCustomer3GUID = agreementTableInfoItemView.AuthorizedPersonTransCustomer3GUID.StringToGuidNull();
				agreementTableInfo.AuthorizedPersonTypeBuyerGUID = agreementTableInfoItemView.AuthorizedPersonTypeBuyerGUID.StringToGuidNull();
				agreementTableInfo.AuthorizedPersonTypeCompanyGUID = agreementTableInfoItemView.AuthorizedPersonTypeCompanyGUID.StringToGuidNull();
				agreementTableInfo.AuthorizedPersonTypeCustomerGUID = agreementTableInfoItemView.AuthorizedPersonTypeCustomerGUID.StringToGuidNull();
				agreementTableInfo.BuyerAddress1 = agreementTableInfoItemView.BuyerAddress1;
				agreementTableInfo.BuyerAddress2 = agreementTableInfoItemView.BuyerAddress2;
				agreementTableInfo.CompanyAddress1 = agreementTableInfoItemView.CompanyAddress1;
				agreementTableInfo.CompanyAddress2 = agreementTableInfoItemView.CompanyAddress2;
				agreementTableInfo.CompanyAltName = agreementTableInfoItemView.CompanyAltName;
				agreementTableInfo.CompanyBankGUID = agreementTableInfoItemView.CompanyBankGUID.StringToGuidNull();
				agreementTableInfo.CompanyFax = agreementTableInfoItemView.CompanyFax;
				agreementTableInfo.CompanyName = agreementTableInfoItemView.CompanyName;
				agreementTableInfo.CompanyPhone = agreementTableInfoItemView.CompanyPhone;
				agreementTableInfo.CustomerAddress1 = agreementTableInfoItemView.CustomerAddress1;
				agreementTableInfo.CustomerAddress2 = agreementTableInfoItemView.CustomerAddress2;
				agreementTableInfo.GuarantorName = agreementTableInfoItemView.GuarantorName;
				agreementTableInfo.GuaranteeText = agreementTableInfoItemView.GuaranteeText;
				agreementTableInfo.RefGUID = agreementTableInfoItemView.RefGUID.StringToGuid();
				agreementTableInfo.RefType = agreementTableInfoItemView.RefType;
				agreementTableInfo.WitnessCompany1GUID = agreementTableInfoItemView.WitnessCompany1GUID.StringToGuidNull();
				agreementTableInfo.WitnessCompany2GUID = agreementTableInfoItemView.WitnessCompany2GUID.StringToGuidNull();
				agreementTableInfo.WitnessCustomer1 = agreementTableInfoItemView.WitnessCustomer1;
				agreementTableInfo.WitnessCustomer2 = agreementTableInfoItemView.WitnessCustomer2;
				
				agreementTableInfo.RowVersion = agreementTableInfoItemView.RowVersion;
				return agreementTableInfo;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<AgreementTableInfo> ToAgreementTableInfo(this IEnumerable<AgreementTableInfoItemView> agreementTableInfoItemViews)
		{
			try
			{
				List<AgreementTableInfo> agreementTableInfos = new List<AgreementTableInfo>();
				foreach (AgreementTableInfoItemView item in agreementTableInfoItemViews)
				{
					agreementTableInfos.Add(item.ToAgreementTableInfo());
				}
				return agreementTableInfos;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AgreementTableInfoItemView ToAgreementTableInfoItemView(this AgreementTableInfo agreementTableInfo)
		{
			try
			{
				AgreementTableInfoItemView agreementTableInfoItemView = new AgreementTableInfoItemView();
				agreementTableInfoItemView.CompanyGUID = agreementTableInfo.CompanyGUID.GuidNullToString();
				agreementTableInfoItemView.CreatedBy = agreementTableInfo.CreatedBy;
				agreementTableInfoItemView.CreatedDateTime = agreementTableInfo.CreatedDateTime.DateTimeToString();
				agreementTableInfoItemView.ModifiedBy = agreementTableInfo.ModifiedBy;
				agreementTableInfoItemView.ModifiedDateTime = agreementTableInfo.ModifiedDateTime.DateTimeToString();
				agreementTableInfoItemView.Owner = agreementTableInfo.Owner;
				agreementTableInfoItemView.OwnerBusinessUnitGUID = agreementTableInfo.OwnerBusinessUnitGUID.GuidNullToString();
				agreementTableInfoItemView.AgreementTableInfoGUID = agreementTableInfo.AgreementTableInfoGUID.GuidNullToString();
				agreementTableInfoItemView.AuthorizedPersonTransBuyer1GUID = agreementTableInfo.AuthorizedPersonTransBuyer1GUID.GuidNullToString();
				agreementTableInfoItemView.AuthorizedPersonTransBuyer2GUID = agreementTableInfo.AuthorizedPersonTransBuyer2GUID.GuidNullToString();
				agreementTableInfoItemView.AuthorizedPersonTransBuyer3GUID = agreementTableInfo.AuthorizedPersonTransBuyer3GUID.GuidNullToString();
				agreementTableInfoItemView.AuthorizedPersonTransCompany1GUID = agreementTableInfo.AuthorizedPersonTransCompany1GUID.GuidNullToString();
				agreementTableInfoItemView.AuthorizedPersonTransCompany2GUID = agreementTableInfo.AuthorizedPersonTransCompany2GUID.GuidNullToString();
				agreementTableInfoItemView.AuthorizedPersonTransCompany3GUID = agreementTableInfo.AuthorizedPersonTransCompany3GUID.GuidNullToString();
				agreementTableInfoItemView.AuthorizedPersonTransCustomer1GUID = agreementTableInfo.AuthorizedPersonTransCustomer1GUID.GuidNullToString();
				agreementTableInfoItemView.AuthorizedPersonTransCustomer2GUID = agreementTableInfo.AuthorizedPersonTransCustomer2GUID.GuidNullToString();
				agreementTableInfoItemView.AuthorizedPersonTransCustomer3GUID = agreementTableInfo.AuthorizedPersonTransCustomer3GUID.GuidNullToString();
				agreementTableInfoItemView.AuthorizedPersonTypeBuyerGUID = agreementTableInfo.AuthorizedPersonTypeBuyerGUID.GuidNullToString();
				agreementTableInfoItemView.AuthorizedPersonTypeCompanyGUID = agreementTableInfo.AuthorizedPersonTypeCompanyGUID.GuidNullToString();
				agreementTableInfoItemView.AuthorizedPersonTypeCustomerGUID = agreementTableInfo.AuthorizedPersonTypeCustomerGUID.GuidNullToString();
				agreementTableInfoItemView.BuyerAddress1 = agreementTableInfo.BuyerAddress1;
				agreementTableInfoItemView.BuyerAddress2 = agreementTableInfo.BuyerAddress2;
				agreementTableInfoItemView.CompanyAddress1 = agreementTableInfo.CompanyAddress1;
				agreementTableInfoItemView.CompanyAddress2 = agreementTableInfo.CompanyAddress2;
				agreementTableInfoItemView.CompanyAltName = agreementTableInfo.CompanyAltName;
				agreementTableInfoItemView.CompanyBankGUID = agreementTableInfo.CompanyBankGUID.GuidNullToString();
				agreementTableInfoItemView.CompanyFax = agreementTableInfo.CompanyFax;
				agreementTableInfoItemView.CompanyName = agreementTableInfo.CompanyName;
				agreementTableInfoItemView.CompanyPhone = agreementTableInfo.CompanyPhone;
				agreementTableInfoItemView.CustomerAddress1 = agreementTableInfo.CustomerAddress1;
				agreementTableInfoItemView.CustomerAddress2 = agreementTableInfo.CustomerAddress2;
				agreementTableInfoItemView.GuarantorName = agreementTableInfo.GuarantorName;
				agreementTableInfoItemView.RefGUID = agreementTableInfo.RefGUID.GuidNullToString();
				agreementTableInfoItemView.RefType = agreementTableInfo.RefType;
				agreementTableInfoItemView.WitnessCompany1GUID = agreementTableInfo.WitnessCompany1GUID.GuidNullToString();
				agreementTableInfoItemView.WitnessCompany2GUID = agreementTableInfo.WitnessCompany2GUID.GuidNullToString();
				agreementTableInfoItemView.WitnessCustomer1 = agreementTableInfo.WitnessCustomer1;
				agreementTableInfoItemView.WitnessCustomer2 = agreementTableInfo.WitnessCustomer2;
				
				agreementTableInfoItemView.RowVersion = agreementTableInfo.RowVersion;
				return agreementTableInfoItemView.GetAgreementTableInfoItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion AgreementTableInfoItemView
		#region ToDropDown
		public static SelectItem<AgreementTableInfoItemView> ToDropDownItem(this AgreementTableInfoItemView agreementTableInfoView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<AgreementTableInfoItemView> selectItem = new SelectItem<AgreementTableInfoItemView>();
				selectItem.Label = null;
				selectItem.Value = agreementTableInfoView.AgreementTableInfoGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: agreementTableInfoView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<AgreementTableInfoItemView>> ToDropDownItem(this IEnumerable<AgreementTableInfoItemView> agreementTableInfoItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<AgreementTableInfoItemView>> selectItems = new List<SelectItem<AgreementTableInfoItemView>>();
				foreach (AgreementTableInfoItemView item in agreementTableInfoItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

