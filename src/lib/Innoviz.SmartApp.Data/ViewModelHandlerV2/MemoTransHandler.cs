using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class MemoTransHandler
	{
		#region MemoTransListView
		public static List<MemoTransListView> GetMemoTransListViewValidation(this List<MemoTransListView> memoTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return memoTransListViews;
			}
			var result = new List<MemoTransListView>();
			try
			{
				foreach (MemoTransListView item in memoTransListViews)
				{
					result.Add(item.GetMemoTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static MemoTransListView GetMemoTransListViewValidation(this MemoTransListView memoTransListView)
		{
			try
			{
				memoTransListView.RowAuthorize = memoTransListView.GetListRowAuthorize();
				return memoTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this MemoTransListView memoTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion MemoTransListView
		#region MemoTransItemView
		public static MemoTransItemView GetMemoTransItemViewValidation(this MemoTransItemView memoTransItemView)
		{
			try
			{
				memoTransItemView.RowAuthorize = memoTransItemView.GetItemRowAuthorize();
				return memoTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this MemoTransItemView memoTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static MemoTrans ToMemoTrans(this MemoTransItemView memoTransItemView)
		{
			try
			{
				MemoTrans memoTrans = new MemoTrans();
				memoTrans.CompanyGUID = memoTransItemView.CompanyGUID.StringToGuid();
				memoTrans.CreatedBy = memoTransItemView.CreatedBy;
				memoTrans.CreatedDateTime = memoTransItemView.CreatedDateTime.StringToSystemDateTime();
				memoTrans.ModifiedBy = memoTransItemView.ModifiedBy;
				memoTrans.ModifiedDateTime = memoTransItemView.ModifiedDateTime.StringToSystemDateTime();
				memoTrans.Owner = memoTransItemView.Owner;
				memoTrans.OwnerBusinessUnitGUID = memoTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				memoTrans.MemoTransGUID = memoTransItemView.MemoTransGUID.StringToGuid();
				memoTrans.Memo = memoTransItemView.Memo;
                memoTrans.RefGUID = memoTransItemView.RefGUID.StringToGuid();
				memoTrans.RefType = memoTransItemView.RefType;
				memoTrans.Topic = memoTransItemView.Topic;
				
				memoTrans.RowVersion = memoTransItemView.RowVersion;
				return memoTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<MemoTrans> ToMemoTrans(this IEnumerable<MemoTransItemView> memoTransItemViews)
		{
			try
			{
				List<MemoTrans> memoTranss = new List<MemoTrans>();
				foreach (MemoTransItemView item in memoTransItemViews)
				{
					memoTranss.Add(item.ToMemoTrans());
				}
				return memoTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static MemoTransItemView ToMemoTransItemView(this MemoTrans memoTrans)
		{
			try
			{
				MemoTransItemView memoTransItemView = new MemoTransItemView();
				memoTransItemView.CompanyGUID = memoTrans.CompanyGUID.GuidNullToString();
				memoTransItemView.CreatedBy = memoTrans.CreatedBy;
				memoTransItemView.CreatedDateTime = memoTrans.CreatedDateTime.DateTimeToString();
				memoTransItemView.ModifiedBy = memoTrans.ModifiedBy;
				memoTransItemView.ModifiedDateTime = memoTrans.ModifiedDateTime.DateTimeToString();
				memoTransItemView.Owner = memoTrans.Owner;
				memoTransItemView.OwnerBusinessUnitGUID = memoTrans.OwnerBusinessUnitGUID.GuidNullToString();
				memoTransItemView.MemoTransGUID = memoTrans.MemoTransGUID.GuidNullToString();
				memoTransItemView.Memo = memoTrans.Memo;
                memoTransItemView.RefGUID = memoTrans.RefGUID.GuidNullToString();
				memoTransItemView.RefType = memoTrans.RefType;
				memoTransItemView.Topic = memoTrans.Topic;
				
				memoTransItemView.RowVersion = memoTrans.RowVersion;
				return memoTransItemView.GetMemoTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion MemoTransItemView
		#region ToDropDown
		public static SelectItem<MemoTransItemView> ToDropDownItem(this MemoTransItemView memoTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<MemoTransItemView> selectItem = new SelectItem<MemoTransItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(memoTransView.Topic, memoTransView.Memo);
				selectItem.Value = memoTransView.MemoTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: memoTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<MemoTransItemView>> ToDropDownItem(this IEnumerable<MemoTransItemView> memoTransItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<MemoTransItemView>> selectItems = new List<SelectItem<MemoTransItemView>>();
				foreach (MemoTransItemView item in memoTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

