using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class BuyerAgreementTableHandler
	{
		#region BuyerAgreementTableListView
		public static List<BuyerAgreementTableListView> GetBuyerAgreementTableListViewValidation(this List<BuyerAgreementTableListView> buyerAgreementTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return buyerAgreementTableListViews;
			}
			var result = new List<BuyerAgreementTableListView>();
			try
			{
				foreach (BuyerAgreementTableListView item in buyerAgreementTableListViews)
				{
					result.Add(item.GetBuyerAgreementTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BuyerAgreementTableListView GetBuyerAgreementTableListViewValidation(this BuyerAgreementTableListView buyerAgreementTableListView)
		{
			try
			{
				buyerAgreementTableListView.RowAuthorize = buyerAgreementTableListView.GetListRowAuthorize();
				return buyerAgreementTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this BuyerAgreementTableListView buyerAgreementTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion BuyerAgreementTableListView
		#region BuyerAgreementTableItemView
		public static BuyerAgreementTableItemView GetBuyerAgreementTableItemViewValidation(this BuyerAgreementTableItemView buyerAgreementTableItemView)
		{
			try
			{
				buyerAgreementTableItemView.RowAuthorize = buyerAgreementTableItemView.GetItemRowAuthorize();
				return buyerAgreementTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this BuyerAgreementTableItemView buyerAgreementTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static BuyerAgreementTable ToBuyerAgreementTable(this BuyerAgreementTableItemView buyerAgreementTableItemView)
		{
			try
			{
				BuyerAgreementTable buyerAgreementTable = new BuyerAgreementTable();
				buyerAgreementTable.CompanyGUID = buyerAgreementTableItemView.CompanyGUID.StringToGuid();
				buyerAgreementTable.CreatedBy = buyerAgreementTableItemView.CreatedBy;
				buyerAgreementTable.CreatedDateTime = buyerAgreementTableItemView.CreatedDateTime.StringToSystemDateTime();
				buyerAgreementTable.ModifiedBy = buyerAgreementTableItemView.ModifiedBy;
				buyerAgreementTable.ModifiedDateTime = buyerAgreementTableItemView.ModifiedDateTime.StringToSystemDateTime();
				buyerAgreementTable.Owner = buyerAgreementTableItemView.Owner;
				buyerAgreementTable.OwnerBusinessUnitGUID = buyerAgreementTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				buyerAgreementTable.BuyerAgreementTableGUID = buyerAgreementTableItemView.BuyerAgreementTableGUID.StringToGuid();
				buyerAgreementTable.BuyerAgreementId = buyerAgreementTableItemView.BuyerAgreementId;
				buyerAgreementTable.BuyerTableGUID = buyerAgreementTableItemView.BuyerTableGUID.StringToGuid();
				buyerAgreementTable.CustomerTableGUID = buyerAgreementTableItemView.CustomerTableGUID.StringToGuid();
				buyerAgreementTable.Description = buyerAgreementTableItemView.Description;
				buyerAgreementTable.EndDate = buyerAgreementTableItemView.EndDate.StringToDate();
				buyerAgreementTable.MaximumCreditLimit = buyerAgreementTableItemView.MaximumCreditLimit;
				buyerAgreementTable.Penalty = buyerAgreementTableItemView.Penalty;
				buyerAgreementTable.ReferenceAgreementID = buyerAgreementTableItemView.ReferenceAgreementID;
				buyerAgreementTable.Remark = buyerAgreementTableItemView.Remark;
				buyerAgreementTable.StartDate = buyerAgreementTableItemView.StartDate.StringToDate();
				
				buyerAgreementTable.RowVersion = buyerAgreementTableItemView.RowVersion;
				return buyerAgreementTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<BuyerAgreementTable> ToBuyerAgreementTable(this IEnumerable<BuyerAgreementTableItemView> buyerAgreementTableItemViews)
		{
			try
			{
				List<BuyerAgreementTable> buyerAgreementTables = new List<BuyerAgreementTable>();
				foreach (BuyerAgreementTableItemView item in buyerAgreementTableItemViews)
				{
					buyerAgreementTables.Add(item.ToBuyerAgreementTable());
				}
				return buyerAgreementTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BuyerAgreementTableItemView ToBuyerAgreementTableItemView(this BuyerAgreementTable buyerAgreementTable)
		{
			try
			{
				BuyerAgreementTableItemView buyerAgreementTableItemView = new BuyerAgreementTableItemView();
				buyerAgreementTableItemView.CompanyGUID = buyerAgreementTable.CompanyGUID.GuidNullToString();
				buyerAgreementTableItemView.CreatedBy = buyerAgreementTable.CreatedBy;
				buyerAgreementTableItemView.CreatedDateTime = buyerAgreementTable.CreatedDateTime.DateTimeToString();
				buyerAgreementTableItemView.ModifiedBy = buyerAgreementTable.ModifiedBy;
				buyerAgreementTableItemView.ModifiedDateTime = buyerAgreementTable.ModifiedDateTime.DateTimeToString();
				buyerAgreementTableItemView.Owner = buyerAgreementTable.Owner;
				buyerAgreementTableItemView.OwnerBusinessUnitGUID = buyerAgreementTable.OwnerBusinessUnitGUID.GuidNullToString();
				buyerAgreementTableItemView.BuyerAgreementTableGUID = buyerAgreementTable.BuyerAgreementTableGUID.GuidNullToString();
				buyerAgreementTableItemView.BuyerAgreementId = buyerAgreementTable.BuyerAgreementId;
				buyerAgreementTableItemView.BuyerTableGUID = buyerAgreementTable.BuyerTableGUID.GuidNullToString();
				buyerAgreementTableItemView.CustomerTableGUID = buyerAgreementTable.CustomerTableGUID.GuidNullToString();
				buyerAgreementTableItemView.Description = buyerAgreementTable.Description;
				buyerAgreementTableItemView.EndDate = buyerAgreementTable.EndDate.DateToString();
				buyerAgreementTableItemView.MaximumCreditLimit = buyerAgreementTable.MaximumCreditLimit;
				buyerAgreementTableItemView.Penalty = buyerAgreementTable.Penalty;
				buyerAgreementTableItemView.ReferenceAgreementID = buyerAgreementTable.ReferenceAgreementID;
				buyerAgreementTableItemView.Remark = buyerAgreementTable.Remark;
				buyerAgreementTableItemView.StartDate = buyerAgreementTable.StartDate.DateToString();
				
				buyerAgreementTableItemView.RowVersion = buyerAgreementTable.RowVersion;
				return buyerAgreementTableItemView.GetBuyerAgreementTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion BuyerAgreementTableItemView
		#region ToDropDown
		public static SelectItem<BuyerAgreementTableItemView> ToDropDownItem(this BuyerAgreementTableItemView buyerAgreementTableView, bool excludeRowData, bool useValues)
		{
			try
			{
				SelectItem<BuyerAgreementTableItemView> selectItem = new SelectItem<BuyerAgreementTableItemView>();
				selectItem.Label = useValues ? buyerAgreementTableView.BuyerAgreementTable_Values : SmartAppUtil.GetDropDownLabel(buyerAgreementTableView.ReferenceAgreementID, buyerAgreementTableView.Description);
				selectItem.Value = buyerAgreementTableView.BuyerAgreementTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: buyerAgreementTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<BuyerAgreementTableItemView>> ToDropDownItem(this IEnumerable<BuyerAgreementTableItemView> buyerAgreementTableItemViews, bool excludeRowData, bool useValues = false)
		{
			try
			{
				List<SelectItem<BuyerAgreementTableItemView>> selectItems = new List<SelectItem<BuyerAgreementTableItemView>>();
				foreach (BuyerAgreementTableItemView item in buyerAgreementTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData, useValues));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

