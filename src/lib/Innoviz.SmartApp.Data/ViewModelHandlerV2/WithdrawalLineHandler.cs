using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class WithdrawalLineHandler
	{
		#region WithdrawalLineListView
		public static List<WithdrawalLineListView> GetWithdrawalLineListViewValidation(this List<WithdrawalLineListView> withdrawalLineListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return withdrawalLineListViews;
			}
			var result = new List<WithdrawalLineListView>();
			try
			{
				foreach (WithdrawalLineListView item in withdrawalLineListViews)
				{
					result.Add(item.GetWithdrawalLineListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static WithdrawalLineListView GetWithdrawalLineListViewValidation(this WithdrawalLineListView withdrawalLineListView)
		{
			try
			{
				withdrawalLineListView.RowAuthorize = withdrawalLineListView.GetListRowAuthorize();
				return withdrawalLineListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this WithdrawalLineListView withdrawalLineListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion WithdrawalLineListView
		#region WithdrawalLineItemView
		public static WithdrawalLineItemView GetWithdrawalLineItemViewValidation(this WithdrawalLineItemView withdrawalLineItemView)
		{
			try
			{
				withdrawalLineItemView.RowAuthorize = withdrawalLineItemView.GetItemRowAuthorize();
				return withdrawalLineItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this WithdrawalLineItemView withdrawalLineItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static WithdrawalLine ToWithdrawalLine(this WithdrawalLineItemView withdrawalLineItemView)
		{
			try
			{
				WithdrawalLine withdrawalLine = new WithdrawalLine();
				withdrawalLine.LineNum = withdrawalLineItemView.LineNum;
				withdrawalLine.CompanyGUID = withdrawalLineItemView.CompanyGUID.StringToGuid();
				withdrawalLine.CreatedBy = withdrawalLineItemView.CreatedBy;
				withdrawalLine.CreatedDateTime = withdrawalLineItemView.CreatedDateTime.StringToSystemDateTime();
				withdrawalLine.ModifiedBy = withdrawalLineItemView.ModifiedBy;
				withdrawalLine.ModifiedDateTime = withdrawalLineItemView.ModifiedDateTime.StringToSystemDateTime();
				withdrawalLine.Owner = withdrawalLineItemView.Owner;
				withdrawalLine.OwnerBusinessUnitGUID = withdrawalLineItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				withdrawalLine.WithdrawalLineGUID = withdrawalLineItemView.WithdrawalLineGUID.StringToGuid();
				withdrawalLine.BillingDate = withdrawalLineItemView.BillingDate.StringNullToDateNull();
				withdrawalLine.BuyerPDCTableGUID = withdrawalLineItemView.BuyerPDCTableGUID.StringToGuidNull();
				withdrawalLine.ClosedForTermExtension = withdrawalLineItemView.ClosedForTermExtension;
				withdrawalLine.CollectionDate = withdrawalLineItemView.CollectionDate.StringToDate();
				withdrawalLine.CustomerPDCTableGUID = withdrawalLineItemView.CustomerPDCTableGUID.StringToGuidNull();
				withdrawalLine.Dimension1GUID = withdrawalLineItemView.Dimension1GUID.StringToGuidNull();
				withdrawalLine.Dimension2GUID = withdrawalLineItemView.Dimension2GUID.StringToGuidNull();
				withdrawalLine.Dimension3GUID = withdrawalLineItemView.Dimension3GUID.StringToGuidNull();
				withdrawalLine.Dimension4GUID = withdrawalLineItemView.Dimension4GUID.StringToGuidNull();
				withdrawalLine.Dimension5GUID = withdrawalLineItemView.Dimension5GUID.StringToGuidNull();
				withdrawalLine.DueDate = withdrawalLineItemView.DueDate.StringToDate();
				withdrawalLine.InterestAmount = withdrawalLineItemView.InterestAmount;
				withdrawalLine.InterestDate = withdrawalLineItemView.InterestDate.StringToDate();
				withdrawalLine.InterestDay = withdrawalLineItemView.InterestDay;
				withdrawalLine.StartDate = withdrawalLineItemView.StartDate.StringToDate();
				withdrawalLine.WithdrawalLineInvoiceTableGUID = withdrawalLineItemView.WithdrawalLineInvoiceTableGUID.StringToGuidNull();
				withdrawalLine.WithdrawalLineType = withdrawalLineItemView.WithdrawalLineType;
				withdrawalLine.WithdrawalTableGUID = withdrawalLineItemView.WithdrawalTableGUID.StringToGuid();
				
				withdrawalLine.RowVersion = withdrawalLineItemView.RowVersion;
				return withdrawalLine;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<WithdrawalLine> ToWithdrawalLine(this IEnumerable<WithdrawalLineItemView> withdrawalLineItemViews)
		{
			try
			{
				List<WithdrawalLine> withdrawalLines = new List<WithdrawalLine>();
				foreach (WithdrawalLineItemView item in withdrawalLineItemViews)
				{
					withdrawalLines.Add(item.ToWithdrawalLine());
				}
				return withdrawalLines;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static WithdrawalLineItemView ToWithdrawalLineItemView(this WithdrawalLine withdrawalLine)
		{
			try
			{
				WithdrawalLineItemView withdrawalLineItemView = new WithdrawalLineItemView();
				withdrawalLineItemView.LineNum = withdrawalLine.LineNum;
				withdrawalLineItemView.CompanyGUID = withdrawalLine.CompanyGUID.GuidNullToString();
				withdrawalLineItemView.CreatedBy = withdrawalLine.CreatedBy;
				withdrawalLineItemView.CreatedDateTime = withdrawalLine.CreatedDateTime.DateTimeToString();
				withdrawalLineItemView.ModifiedBy = withdrawalLine.ModifiedBy;
				withdrawalLineItemView.ModifiedDateTime = withdrawalLine.ModifiedDateTime.DateTimeToString();
				withdrawalLineItemView.Owner = withdrawalLine.Owner;
				withdrawalLineItemView.OwnerBusinessUnitGUID = withdrawalLine.OwnerBusinessUnitGUID.GuidNullToString();
				withdrawalLineItemView.WithdrawalLineGUID = withdrawalLine.WithdrawalLineGUID.GuidNullToString();
				withdrawalLineItemView.BillingDate = withdrawalLine.BillingDate.DateNullToString();
				withdrawalLineItemView.BuyerPDCTableGUID = withdrawalLine.BuyerPDCTableGUID.GuidNullToString();
				withdrawalLineItemView.ClosedForTermExtension = withdrawalLine.ClosedForTermExtension;
				withdrawalLineItemView.CollectionDate = withdrawalLine.CollectionDate.DateToString();
				withdrawalLineItemView.CustomerPDCTableGUID = withdrawalLine.CustomerPDCTableGUID.GuidNullToString();
				withdrawalLineItemView.Dimension1GUID = withdrawalLine.Dimension1GUID.GuidNullToString();
				withdrawalLineItemView.Dimension2GUID = withdrawalLine.Dimension2GUID.GuidNullToString();
				withdrawalLineItemView.Dimension3GUID = withdrawalLine.Dimension3GUID.GuidNullToString();
				withdrawalLineItemView.Dimension4GUID = withdrawalLine.Dimension4GUID.GuidNullToString();
				withdrawalLineItemView.Dimension5GUID = withdrawalLine.Dimension5GUID.GuidNullToString();
				withdrawalLineItemView.DueDate = withdrawalLine.DueDate.DateToString();
				withdrawalLineItemView.InterestAmount = withdrawalLine.InterestAmount;
				withdrawalLineItemView.InterestDate = withdrawalLine.InterestDate.DateToString();
				withdrawalLineItemView.InterestDay = withdrawalLine.InterestDay;
				withdrawalLineItemView.StartDate = withdrawalLine.StartDate.DateToString();
				withdrawalLineItemView.WithdrawalLineInvoiceTableGUID = withdrawalLine.WithdrawalLineInvoiceTableGUID.GuidNullToString();
				withdrawalLineItemView.WithdrawalLineType = withdrawalLine.WithdrawalLineType;
				withdrawalLineItemView.WithdrawalTableGUID = withdrawalLine.WithdrawalTableGUID.GuidNullToString();
				
				withdrawalLineItemView.RowVersion = withdrawalLine.RowVersion;
				return withdrawalLineItemView.GetWithdrawalLineItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion WithdrawalLineItemView
		#region ToDropDown
		public static SelectItem<WithdrawalLineItemView> ToDropDownItem(this WithdrawalLineItemView withdrawalLineView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<WithdrawalLineItemView> selectItem = new SelectItem<WithdrawalLineItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(withdrawalLineView.WithdrawalLineType.ToString(), withdrawalLineView.StartDate.ToString());
				selectItem.Value = withdrawalLineView.WithdrawalLineGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: withdrawalLineView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<WithdrawalLineItemView>> ToDropDownItem(this IEnumerable<WithdrawalLineItemView> withdrawalLineItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<WithdrawalLineItemView>> selectItems = new List<SelectItem<WithdrawalLineItemView>>();
				foreach (WithdrawalLineItemView item in withdrawalLineItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

