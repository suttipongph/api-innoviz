using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class FinancialCreditTransHandler
	{
		#region FinancialCreditTransListView
		public static List<FinancialCreditTransListView> GetFinancialCreditTransListViewValidation(this List<FinancialCreditTransListView> financialCreditTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return financialCreditTransListViews;
			}
			var result = new List<FinancialCreditTransListView>();
			try
			{
				foreach (FinancialCreditTransListView item in financialCreditTransListViews)
				{
					result.Add(item.GetFinancialCreditTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static FinancialCreditTransListView GetFinancialCreditTransListViewValidation(this FinancialCreditTransListView financialCreditTransListView)
		{
			try
			{
				financialCreditTransListView.RowAuthorize = financialCreditTransListView.GetListRowAuthorize();
				return financialCreditTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this FinancialCreditTransListView financialCreditTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion FinancialCreditTransListView
		#region FinancialCreditTransItemView
		public static FinancialCreditTransItemView GetFinancialCreditTransItemViewValidation(this FinancialCreditTransItemView financialCreditTransItemView)
		{
			try
			{
				financialCreditTransItemView.RowAuthorize = financialCreditTransItemView.GetItemRowAuthorize();
				return financialCreditTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this FinancialCreditTransItemView financialCreditTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static FinancialCreditTrans ToFinancialCreditTrans(this FinancialCreditTransItemView financialCreditTransItemView)
		{
			try
			{
				FinancialCreditTrans financialCreditTrans = new FinancialCreditTrans();
				financialCreditTrans.CompanyGUID = financialCreditTransItemView.CompanyGUID.StringToGuid();
				financialCreditTrans.CreatedBy = financialCreditTransItemView.CreatedBy;
				financialCreditTrans.CreatedDateTime = financialCreditTransItemView.CreatedDateTime.StringToSystemDateTime();
				financialCreditTrans.ModifiedBy = financialCreditTransItemView.ModifiedBy;
				financialCreditTrans.ModifiedDateTime = financialCreditTransItemView.ModifiedDateTime.StringToSystemDateTime();
				financialCreditTrans.Owner = financialCreditTransItemView.Owner;
				financialCreditTrans.OwnerBusinessUnitGUID = financialCreditTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				financialCreditTrans.FinancialCreditTransGUID = financialCreditTransItemView.FinancialCreditTransGUID.StringToGuid();
				financialCreditTrans.Amount = financialCreditTransItemView.Amount;
				financialCreditTrans.BankGroupGUID = financialCreditTransItemView.BankGroupGUID.StringToGuid();
				financialCreditTrans.CreditTypeGUID = financialCreditTransItemView.CreditTypeGUID.StringToGuid();
				financialCreditTrans.RefGUID = financialCreditTransItemView.RefGUID.StringToGuid();
				financialCreditTrans.RefType = financialCreditTransItemView.RefType;
				
				financialCreditTrans.RowVersion = financialCreditTransItemView.RowVersion;
				return financialCreditTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<FinancialCreditTrans> ToFinancialCreditTrans(this IEnumerable<FinancialCreditTransItemView> financialCreditTransItemViews)
		{
			try
			{
				List<FinancialCreditTrans> financialCreditTranss = new List<FinancialCreditTrans>();
				foreach (FinancialCreditTransItemView item in financialCreditTransItemViews)
				{
					financialCreditTranss.Add(item.ToFinancialCreditTrans());
				}
				return financialCreditTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static FinancialCreditTransItemView ToFinancialCreditTransItemView(this FinancialCreditTrans financialCreditTrans)
		{
			try
			{
				FinancialCreditTransItemView financialCreditTransItemView = new FinancialCreditTransItemView();
				financialCreditTransItemView.CompanyGUID = financialCreditTrans.CompanyGUID.GuidNullToString();
				financialCreditTransItemView.CreatedBy = financialCreditTrans.CreatedBy;
				financialCreditTransItemView.CreatedDateTime = financialCreditTrans.CreatedDateTime.DateTimeToString();
				financialCreditTransItemView.ModifiedBy = financialCreditTrans.ModifiedBy;
				financialCreditTransItemView.ModifiedDateTime = financialCreditTrans.ModifiedDateTime.DateTimeToString();
				financialCreditTransItemView.Owner = financialCreditTrans.Owner;
				financialCreditTransItemView.OwnerBusinessUnitGUID = financialCreditTrans.OwnerBusinessUnitGUID.GuidNullToString();
				financialCreditTransItemView.FinancialCreditTransGUID = financialCreditTrans.FinancialCreditTransGUID.GuidNullToString();
				financialCreditTransItemView.Amount = financialCreditTrans.Amount;
				financialCreditTransItemView.BankGroupGUID = financialCreditTrans.BankGroupGUID.GuidNullToString();
				financialCreditTransItemView.CreditTypeGUID = financialCreditTrans.CreditTypeGUID.GuidNullToString();
				financialCreditTransItemView.RefGUID = financialCreditTrans.RefGUID.GuidNullToString();
				financialCreditTransItemView.RefType = financialCreditTrans.RefType;
				
				financialCreditTransItemView.RowVersion = financialCreditTrans.RowVersion;
				return financialCreditTransItemView.GetFinancialCreditTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion FinancialCreditTransItemView
		#region ToDropDown
		public static SelectItem<FinancialCreditTransItemView> ToDropDownItem(this FinancialCreditTransItemView financialCreditTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<FinancialCreditTransItemView> selectItem = new SelectItem<FinancialCreditTransItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(financialCreditTransView.BankGroupGUID.ToString(), financialCreditTransView.CreditTypeGUID.ToString());
				selectItem.Value = financialCreditTransView.FinancialCreditTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: financialCreditTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<FinancialCreditTransItemView>> ToDropDownItem(this IEnumerable<FinancialCreditTransItemView> financialCreditTransItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<FinancialCreditTransItemView>> selectItems = new List<SelectItem<FinancialCreditTransItemView>>();
				foreach (FinancialCreditTransItemView item in financialCreditTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

