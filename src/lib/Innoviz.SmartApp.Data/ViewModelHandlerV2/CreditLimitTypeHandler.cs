using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CreditLimitTypeHandler
	{
		#region CreditLimitTypeListView
		public static List<CreditLimitTypeListView> GetCreditLimitTypeListViewValidation(this List<CreditLimitTypeListView> creditLimitTypeListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return creditLimitTypeListViews;
			}
			var result = new List<CreditLimitTypeListView>();
			try
			{
				foreach (CreditLimitTypeListView item in creditLimitTypeListViews)
				{
					result.Add(item.GetCreditLimitTypeListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CreditLimitTypeListView GetCreditLimitTypeListViewValidation(this CreditLimitTypeListView creditLimitTypeListView)
		{
			try
			{
				creditLimitTypeListView.RowAuthorize = creditLimitTypeListView.GetListRowAuthorize();
				return creditLimitTypeListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CreditLimitTypeListView creditLimitTypeListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CreditLimitTypeListView
		#region CreditLimitTypeItemView
		public static CreditLimitTypeItemView GetCreditLimitTypeItemViewValidation(this CreditLimitTypeItemView creditLimitTypeItemView)
		{
			try
			{
				creditLimitTypeItemView.RowAuthorize = creditLimitTypeItemView.GetItemRowAuthorize();
				return creditLimitTypeItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CreditLimitTypeItemView creditLimitTypeItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CreditLimitType ToCreditLimitType(this CreditLimitTypeItemView creditLimitTypeItemView)
		{
			try
			{
				CreditLimitType creditLimitType = new CreditLimitType();
				creditLimitType.CompanyGUID = creditLimitTypeItemView.CompanyGUID.StringToGuid();
				creditLimitType.CreatedBy = creditLimitTypeItemView.CreatedBy;
				creditLimitType.CreatedDateTime = creditLimitTypeItemView.CreatedDateTime.StringToSystemDateTime();
				creditLimitType.ModifiedBy = creditLimitTypeItemView.ModifiedBy;
				creditLimitType.ModifiedDateTime = creditLimitTypeItemView.ModifiedDateTime.StringToSystemDateTime();
				creditLimitType.Owner = creditLimitTypeItemView.Owner;
				creditLimitType.OwnerBusinessUnitGUID = creditLimitTypeItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				creditLimitType.CreditLimitTypeGUID = creditLimitTypeItemView.CreditLimitTypeGUID.StringToGuid();
				creditLimitType.AllowAddLine = creditLimitTypeItemView.AllowAddLine;
				creditLimitType.BuyerMatchingAndLoanRequest = creditLimitTypeItemView.BuyerMatchingAndLoanRequest;
				creditLimitType.CloseDay = creditLimitTypeItemView.CloseDay;
				creditLimitType.CreditLimitConditionType = creditLimitTypeItemView.CreditLimitConditionType;
				creditLimitType.CreditLimitExpiration = creditLimitTypeItemView.CreditLimitExpiration;
				creditLimitType.CreditLimitExpiryDay = creditLimitTypeItemView.CreditLimitExpiryDay;
				creditLimitType.CreditLimitRemark = creditLimitTypeItemView.CreditLimitRemark;
				creditLimitType.CreditLimitTypeId = creditLimitTypeItemView.CreditLimitTypeId;
				creditLimitType.Description = creditLimitTypeItemView.Description;
				creditLimitType.InactiveDay = creditLimitTypeItemView.InactiveDay;
				creditLimitType.ParentCreditLimitTypeGUID = creditLimitTypeItemView.ParentCreditLimitTypeGUID.StringToGuidNull();
				creditLimitType.ProductType = creditLimitTypeItemView.ProductType;
				creditLimitType.Revolving = creditLimitTypeItemView.Revolving;
				creditLimitType.ValidateBuyerAgreement = creditLimitTypeItemView.ValidateBuyerAgreement;
				creditLimitType.ValidateCreditLimitType = creditLimitTypeItemView.ValidateCreditLimitType;
				creditLimitType.BookmarkOrdering = creditLimitTypeItemView.BookmarkOrdering;
				
				creditLimitType.RowVersion = creditLimitTypeItemView.RowVersion;
				return creditLimitType;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CreditLimitType> ToCreditLimitType(this IEnumerable<CreditLimitTypeItemView> creditLimitTypeItemViews)
		{
			try
			{
				List<CreditLimitType> creditLimitTypes = new List<CreditLimitType>();
				foreach (CreditLimitTypeItemView item in creditLimitTypeItemViews)
				{
					creditLimitTypes.Add(item.ToCreditLimitType());
				}
				return creditLimitTypes;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CreditLimitTypeItemView ToCreditLimitTypeItemView(this CreditLimitType creditLimitType)
		{
			try
			{
				CreditLimitTypeItemView creditLimitTypeItemView = new CreditLimitTypeItemView();
				creditLimitTypeItemView.CompanyGUID = creditLimitType.CompanyGUID.GuidNullToString();
				creditLimitTypeItemView.CreatedBy = creditLimitType.CreatedBy;
				creditLimitTypeItemView.CreatedDateTime = creditLimitType.CreatedDateTime.DateTimeToString();
				creditLimitTypeItemView.ModifiedBy = creditLimitType.ModifiedBy;
				creditLimitTypeItemView.ModifiedDateTime = creditLimitType.ModifiedDateTime.DateTimeToString();
				creditLimitTypeItemView.Owner = creditLimitType.Owner;
				creditLimitTypeItemView.OwnerBusinessUnitGUID = creditLimitType.OwnerBusinessUnitGUID.GuidNullToString();
				creditLimitTypeItemView.CreditLimitTypeGUID = creditLimitType.CreditLimitTypeGUID.GuidNullToString();
				creditLimitTypeItemView.AllowAddLine = creditLimitType.AllowAddLine;
				creditLimitTypeItemView.BuyerMatchingAndLoanRequest = creditLimitType.BuyerMatchingAndLoanRequest;
				creditLimitTypeItemView.CloseDay = creditLimitType.CloseDay;
				creditLimitTypeItemView.CreditLimitConditionType = creditLimitType.CreditLimitConditionType;
				creditLimitTypeItemView.CreditLimitExpiration = creditLimitType.CreditLimitExpiration;
				creditLimitTypeItemView.CreditLimitExpiryDay = creditLimitType.CreditLimitExpiryDay;
				creditLimitTypeItemView.CreditLimitRemark = creditLimitType.CreditLimitRemark;
				creditLimitTypeItemView.CreditLimitTypeId = creditLimitType.CreditLimitTypeId;
				creditLimitTypeItemView.Description = creditLimitType.Description;
				creditLimitTypeItemView.InactiveDay = creditLimitType.InactiveDay;
				creditLimitTypeItemView.ParentCreditLimitTypeGUID = creditLimitType.ParentCreditLimitTypeGUID.GuidNullToString();
				creditLimitTypeItemView.ProductType = creditLimitType.ProductType;
				creditLimitTypeItemView.Revolving = creditLimitType.Revolving;
				creditLimitTypeItemView.ValidateBuyerAgreement = creditLimitType.ValidateBuyerAgreement;
				creditLimitTypeItemView.ValidateCreditLimitType = creditLimitType.ValidateCreditLimitType;
				creditLimitTypeItemView.BookmarkOrdering = creditLimitType.BookmarkOrdering;
				
				creditLimitTypeItemView.RowVersion = creditLimitType.RowVersion;
				return creditLimitTypeItemView.GetCreditLimitTypeItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CreditLimitTypeItemView
		#region ToDropDown
		public static SelectItem<CreditLimitTypeItemView> ToDropDownItem(this CreditLimitTypeItemView creditLimitTypeView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CreditLimitTypeItemView> selectItem = new SelectItem<CreditLimitTypeItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(creditLimitTypeView.CreditLimitTypeId, creditLimitTypeView.Description);
				selectItem.Value = creditLimitTypeView.CreditLimitTypeGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: creditLimitTypeView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CreditLimitTypeItemView>> ToDropDownItem(this IEnumerable<CreditLimitTypeItemView> creditLimitTypeItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CreditLimitTypeItemView>> selectItems = new List<SelectItem<CreditLimitTypeItemView>>();
				foreach (CreditLimitTypeItemView item in creditLimitTypeItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

