using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class WithholdingTaxValueHandler
	{
		#region WithholdingTaxValueListView
		public static List<WithholdingTaxValueListView> GetWithholdingTaxValueListViewValidation(this List<WithholdingTaxValueListView> withholdingTaxValueListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return withholdingTaxValueListViews;
			}
			var result = new List<WithholdingTaxValueListView>();
			try
			{
				foreach (WithholdingTaxValueListView item in withholdingTaxValueListViews)
				{
					result.Add(item.GetWithholdingTaxValueListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static WithholdingTaxValueListView GetWithholdingTaxValueListViewValidation(this WithholdingTaxValueListView withholdingTaxValueListView)
		{
			try
			{
				withholdingTaxValueListView.RowAuthorize = withholdingTaxValueListView.GetListRowAuthorize();
				return withholdingTaxValueListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this WithholdingTaxValueListView withholdingTaxValueListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion WithholdingTaxValueListView
		#region WithholdingTaxValueItemView
		public static WithholdingTaxValueItemView GetWithholdingTaxValueItemViewValidation(this WithholdingTaxValueItemView withholdingTaxValueItemView)
		{
			try
			{
				withholdingTaxValueItemView.RowAuthorize = withholdingTaxValueItemView.GetItemRowAuthorize();
				return withholdingTaxValueItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this WithholdingTaxValueItemView withholdingTaxValueItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static WithholdingTaxValue ToWithholdingTaxValue(this WithholdingTaxValueItemView withholdingTaxValueItemView)
		{
			try
			{
				WithholdingTaxValue withholdingTaxValue = new WithholdingTaxValue();
				withholdingTaxValue.CompanyGUID = withholdingTaxValueItemView.CompanyGUID.StringToGuid();
				withholdingTaxValue.CreatedBy = withholdingTaxValueItemView.CreatedBy;
				withholdingTaxValue.CreatedDateTime = withholdingTaxValueItemView.CreatedDateTime.StringToSystemDateTime();
				withholdingTaxValue.ModifiedBy = withholdingTaxValueItemView.ModifiedBy;
				withholdingTaxValue.ModifiedDateTime = withholdingTaxValueItemView.ModifiedDateTime.StringToSystemDateTime();
				withholdingTaxValue.Owner = withholdingTaxValueItemView.Owner;
				withholdingTaxValue.OwnerBusinessUnitGUID = withholdingTaxValueItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				withholdingTaxValue.WithholdingTaxValueGUID = withholdingTaxValueItemView.WithholdingTaxValueGUID.StringToGuid();
				withholdingTaxValue.EffectiveFrom = withholdingTaxValueItemView.EffectiveFrom.StringToDate();
				withholdingTaxValue.EffectiveTo = withholdingTaxValueItemView.EffectiveTo.StringNullToDateNull();
				withholdingTaxValue.Value = withholdingTaxValueItemView.Value;
				withholdingTaxValue.WithholdingTaxTableGUID = withholdingTaxValueItemView.WithholdingTaxTableGUID.StringToGuid();
				
				withholdingTaxValue.RowVersion = withholdingTaxValueItemView.RowVersion;
				return withholdingTaxValue;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<WithholdingTaxValue> ToWithholdingTaxValue(this IEnumerable<WithholdingTaxValueItemView> withholdingTaxValueItemViews)
		{
			try
			{
				List<WithholdingTaxValue> withholdingTaxValues = new List<WithholdingTaxValue>();
				foreach (WithholdingTaxValueItemView item in withholdingTaxValueItemViews)
				{
					withholdingTaxValues.Add(item.ToWithholdingTaxValue());
				}
				return withholdingTaxValues;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static WithholdingTaxValueItemView ToWithholdingTaxValueItemView(this WithholdingTaxValue withholdingTaxValue)
		{
			try
			{
				WithholdingTaxValueItemView withholdingTaxValueItemView = new WithholdingTaxValueItemView();
				withholdingTaxValueItemView.CompanyGUID = withholdingTaxValue.CompanyGUID.GuidNullToString();
				withholdingTaxValueItemView.CreatedBy = withholdingTaxValue.CreatedBy;
				withholdingTaxValueItemView.CreatedDateTime = withholdingTaxValue.CreatedDateTime.DateTimeToString();
				withholdingTaxValueItemView.ModifiedBy = withholdingTaxValue.ModifiedBy;
				withholdingTaxValueItemView.ModifiedDateTime = withholdingTaxValue.ModifiedDateTime.DateTimeToString();
				withholdingTaxValueItemView.Owner = withholdingTaxValue.Owner;
				withholdingTaxValueItemView.OwnerBusinessUnitGUID = withholdingTaxValue.OwnerBusinessUnitGUID.GuidNullToString();
				withholdingTaxValueItemView.WithholdingTaxValueGUID = withholdingTaxValue.WithholdingTaxValueGUID.GuidNullToString();
				withholdingTaxValueItemView.EffectiveFrom = withholdingTaxValue.EffectiveFrom.DateToString();
				withholdingTaxValueItemView.EffectiveTo = withholdingTaxValue.EffectiveTo.DateNullToString();
				withholdingTaxValueItemView.Value = withholdingTaxValue.Value;
				withholdingTaxValueItemView.WithholdingTaxTableGUID = withholdingTaxValue.WithholdingTaxTableGUID.GuidNullToString();
				
				withholdingTaxValueItemView.RowVersion = withholdingTaxValue.RowVersion;
				return withholdingTaxValueItemView.GetWithholdingTaxValueItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion WithholdingTaxValueItemView
		#region ToDropDown
		public static SelectItem<WithholdingTaxValueItemView> ToDropDownItem(this WithholdingTaxValueItemView withholdingTaxValueView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<WithholdingTaxValueItemView> selectItem = new SelectItem<WithholdingTaxValueItemView>();
				selectItem.Label = null;
				selectItem.Value = withholdingTaxValueView.WithholdingTaxValueGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: withholdingTaxValueView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<WithholdingTaxValueItemView>> ToDropDownItem(this IEnumerable<WithholdingTaxValueItemView> withholdingTaxValueItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<WithholdingTaxValueItemView>> selectItems = new List<SelectItem<WithholdingTaxValueItemView>>();
				foreach (WithholdingTaxValueItemView item in withholdingTaxValueItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

