using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CAReqAssignmentOutstandingHandler
	{
		#region CAReqAssignmentOutstandingListView
		public static List<CAReqAssignmentOutstandingListView> GetCAReqAssignmentOutstandingListViewValidation(this List<CAReqAssignmentOutstandingListView> caReqAssignmentOutstandingListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return caReqAssignmentOutstandingListViews;
			}
			var result = new List<CAReqAssignmentOutstandingListView>();
			try
			{
				foreach (CAReqAssignmentOutstandingListView item in caReqAssignmentOutstandingListViews)
				{
					result.Add(item.GetCAReqAssignmentOutstandingListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CAReqAssignmentOutstandingListView GetCAReqAssignmentOutstandingListViewValidation(this CAReqAssignmentOutstandingListView caReqAssignmentOutstandingListView)
		{
			try
			{
				caReqAssignmentOutstandingListView.RowAuthorize = caReqAssignmentOutstandingListView.GetListRowAuthorize();
				return caReqAssignmentOutstandingListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CAReqAssignmentOutstandingListView caReqAssignmentOutstandingListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CAReqAssignmentOutstandingListView
		#region CAReqAssignmentOutstandingItemView
		public static CAReqAssignmentOutstandingItemView GetCAReqAssignmentOutstandingItemViewValidation(this CAReqAssignmentOutstandingItemView caReqAssignmentOutstandingItemView)
		{
			try
			{
				caReqAssignmentOutstandingItemView.RowAuthorize = caReqAssignmentOutstandingItemView.GetItemRowAuthorize();
				return caReqAssignmentOutstandingItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CAReqAssignmentOutstandingItemView caReqAssignmentOutstandingItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CAReqAssignmentOutstanding ToCAReqAssignmentOutstanding(this CAReqAssignmentOutstandingItemView caReqAssignmentOutstandingItemView)
		{
			try
			{
				CAReqAssignmentOutstanding caReqAssignmentOutstanding = new CAReqAssignmentOutstanding();
				caReqAssignmentOutstanding.CompanyGUID = caReqAssignmentOutstandingItemView.CompanyGUID.StringToGuid();
				caReqAssignmentOutstanding.CreatedBy = caReqAssignmentOutstandingItemView.CreatedBy;
				caReqAssignmentOutstanding.CreatedDateTime = caReqAssignmentOutstandingItemView.CreatedDateTime.StringToSystemDateTime();
				caReqAssignmentOutstanding.ModifiedBy = caReqAssignmentOutstandingItemView.ModifiedBy;
				caReqAssignmentOutstanding.ModifiedDateTime = caReqAssignmentOutstandingItemView.ModifiedDateTime.StringToSystemDateTime();
				caReqAssignmentOutstanding.Owner = caReqAssignmentOutstandingItemView.Owner;
				caReqAssignmentOutstanding.OwnerBusinessUnitGUID = caReqAssignmentOutstandingItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				caReqAssignmentOutstanding.CAReqAssignmentOutstandingGUID = caReqAssignmentOutstandingItemView.CAReqAssignmentOutstandingGUID.StringToGuid();
				caReqAssignmentOutstanding.AssignmentAgreementAmount = caReqAssignmentOutstandingItemView.AssignmentAgreementAmount;
				caReqAssignmentOutstanding.AssignmentAgreementTableGUID = caReqAssignmentOutstandingItemView.AssignmentAgreementTableGUID.StringToGuidNull();
				caReqAssignmentOutstanding.BuyerTableGUID = caReqAssignmentOutstandingItemView.BuyerTableGUID.StringToGuidNull();
				caReqAssignmentOutstanding.CreditAppRequestTableGUID = caReqAssignmentOutstandingItemView.CreditAppRequestTableGUID.StringToGuidNull();
				caReqAssignmentOutstanding.CustomerTableGUID = caReqAssignmentOutstandingItemView.CustomerTableGUID.StringToGuidNull();
				caReqAssignmentOutstanding.RemainingAmount = caReqAssignmentOutstandingItemView.RemainingAmount;
				caReqAssignmentOutstanding.SettleAmount = caReqAssignmentOutstandingItemView.SettleAmount;
				
				caReqAssignmentOutstanding.RowVersion = caReqAssignmentOutstandingItemView.RowVersion;
				return caReqAssignmentOutstanding;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CAReqAssignmentOutstanding> ToCAReqAssignmentOutstanding(this IEnumerable<CAReqAssignmentOutstandingItemView> caReqAssignmentOutstandingItemViews)
		{
			try
			{
				List<CAReqAssignmentOutstanding> caReqAssignmentOutstandings = new List<CAReqAssignmentOutstanding>();
				foreach (CAReqAssignmentOutstandingItemView item in caReqAssignmentOutstandingItemViews)
				{
					caReqAssignmentOutstandings.Add(item.ToCAReqAssignmentOutstanding());
				}
				return caReqAssignmentOutstandings;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CAReqAssignmentOutstandingItemView ToCAReqAssignmentOutstandingItemView(this CAReqAssignmentOutstanding caReqAssignmentOutstanding)
		{
			try
			{
				CAReqAssignmentOutstandingItemView caReqAssignmentOutstandingItemView = new CAReqAssignmentOutstandingItemView();
				caReqAssignmentOutstandingItemView.CompanyGUID = caReqAssignmentOutstanding.CompanyGUID.GuidNullToString();
				caReqAssignmentOutstandingItemView.CreatedBy = caReqAssignmentOutstanding.CreatedBy;
				caReqAssignmentOutstandingItemView.CreatedDateTime = caReqAssignmentOutstanding.CreatedDateTime.DateTimeToString();
				caReqAssignmentOutstandingItemView.ModifiedBy = caReqAssignmentOutstanding.ModifiedBy;
				caReqAssignmentOutstandingItemView.ModifiedDateTime = caReqAssignmentOutstanding.ModifiedDateTime.DateTimeToString();
				caReqAssignmentOutstandingItemView.Owner = caReqAssignmentOutstanding.Owner;
				caReqAssignmentOutstandingItemView.OwnerBusinessUnitGUID = caReqAssignmentOutstanding.OwnerBusinessUnitGUID.GuidNullToString();
				caReqAssignmentOutstandingItemView.CAReqAssignmentOutstandingGUID = caReqAssignmentOutstanding.CAReqAssignmentOutstandingGUID.GuidNullToString();
				caReqAssignmentOutstandingItemView.AssignmentAgreementAmount = caReqAssignmentOutstanding.AssignmentAgreementAmount;
				caReqAssignmentOutstandingItemView.AssignmentAgreementTableGUID = caReqAssignmentOutstanding.AssignmentAgreementTableGUID.GuidNullToString();
				caReqAssignmentOutstandingItemView.BuyerTableGUID = caReqAssignmentOutstanding.BuyerTableGUID.GuidNullToString();
				caReqAssignmentOutstandingItemView.CreditAppRequestTableGUID = caReqAssignmentOutstanding.CreditAppRequestTableGUID.GuidNullToString();
				caReqAssignmentOutstandingItemView.CustomerTableGUID = caReqAssignmentOutstanding.CustomerTableGUID.GuidNullToString();
				caReqAssignmentOutstandingItemView.RemainingAmount = caReqAssignmentOutstanding.RemainingAmount;
				caReqAssignmentOutstandingItemView.SettleAmount = caReqAssignmentOutstanding.SettleAmount;
				
				caReqAssignmentOutstandingItemView.RowVersion = caReqAssignmentOutstanding.RowVersion;
				return caReqAssignmentOutstandingItemView.GetCAReqAssignmentOutstandingItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CAReqAssignmentOutstandingItemView
		#region ToDropDown
		public static SelectItem<CAReqAssignmentOutstandingItemView> ToDropDownItem(this CAReqAssignmentOutstandingItemView caReqAssignmentOutstandingView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CAReqAssignmentOutstandingItemView> selectItem = new SelectItem<CAReqAssignmentOutstandingItemView>();
				selectItem.Label = null;
				selectItem.Value = caReqAssignmentOutstandingView.CAReqAssignmentOutstandingGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: caReqAssignmentOutstandingView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CAReqAssignmentOutstandingItemView>> ToDropDownItem(this IEnumerable<CAReqAssignmentOutstandingItemView> caReqAssignmentOutstandingItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CAReqAssignmentOutstandingItemView>> selectItems = new List<SelectItem<CAReqAssignmentOutstandingItemView>>();
				foreach (CAReqAssignmentOutstandingItemView item in caReqAssignmentOutstandingItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

