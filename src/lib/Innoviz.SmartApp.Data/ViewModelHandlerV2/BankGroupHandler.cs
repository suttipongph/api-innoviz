using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class BankGroupHandler
	{
		#region BankGroupListView
		public static List<BankGroupListView> GetBankGroupListViewValidation(this List<BankGroupListView> bankGroupListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return bankGroupListViews;
			}
			var result = new List<BankGroupListView>();
			try
			{
				foreach (BankGroupListView item in bankGroupListViews)
				{
					result.Add(item.GetBankGroupListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BankGroupListView GetBankGroupListViewValidation(this BankGroupListView bankGroupListView)
		{
			try
			{
				bankGroupListView.RowAuthorize = bankGroupListView.GetListRowAuthorize();
				return bankGroupListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this BankGroupListView bankGroupListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion BankGroupListView
		#region BankGroupItemView
		public static BankGroupItemView GetBankGroupItemViewValidation(this BankGroupItemView bankGroupItemView)
		{
			try
			{
				bankGroupItemView.RowAuthorize = bankGroupItemView.GetItemRowAuthorize();
				return bankGroupItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this BankGroupItemView bankGroupItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static BankGroup ToBankGroup(this BankGroupItemView bankGroupItemView)
		{
			try
			{
				BankGroup bankGroup = new BankGroup();
				bankGroup.CompanyGUID = bankGroupItemView.CompanyGUID.StringToGuid();
				bankGroup.CreatedBy = bankGroupItemView.CreatedBy;
				bankGroup.CreatedDateTime = bankGroupItemView.CreatedDateTime.StringToSystemDateTime();
				bankGroup.ModifiedBy = bankGroupItemView.ModifiedBy;
				bankGroup.ModifiedDateTime = bankGroupItemView.ModifiedDateTime.StringToSystemDateTime();
				bankGroup.Owner = bankGroupItemView.Owner;
				bankGroup.OwnerBusinessUnitGUID = bankGroupItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				bankGroup.BankGroupGUID = bankGroupItemView.BankGroupGUID.StringToGuid();
				bankGroup.Alias = bankGroupItemView.Alias;
				bankGroup.BankGroupId = bankGroupItemView.BankGroupId;
				bankGroup.Description = bankGroupItemView.Description;
				
				bankGroup.RowVersion = bankGroupItemView.RowVersion;
				return bankGroup;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<BankGroup> ToBankGroup(this IEnumerable<BankGroupItemView> bankGroupItemViews)
		{
			try
			{
				List<BankGroup> bankGroups = new List<BankGroup>();
				foreach (BankGroupItemView item in bankGroupItemViews)
				{
					bankGroups.Add(item.ToBankGroup());
				}
				return bankGroups;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BankGroupItemView ToBankGroupItemView(this BankGroup bankGroup)
		{
			try
			{
				BankGroupItemView bankGroupItemView = new BankGroupItemView();
				bankGroupItemView.CompanyGUID = bankGroup.CompanyGUID.GuidNullToString();
				bankGroupItemView.CreatedBy = bankGroup.CreatedBy;
				bankGroupItemView.CreatedDateTime = bankGroup.CreatedDateTime.DateTimeToString();
				bankGroupItemView.ModifiedBy = bankGroup.ModifiedBy;
				bankGroupItemView.ModifiedDateTime = bankGroup.ModifiedDateTime.DateTimeToString();
				bankGroupItemView.Owner = bankGroup.Owner;
				bankGroupItemView.OwnerBusinessUnitGUID = bankGroup.OwnerBusinessUnitGUID.GuidNullToString();
				bankGroupItemView.BankGroupGUID = bankGroup.BankGroupGUID.GuidNullToString();
				bankGroupItemView.Alias = bankGroup.Alias;
				bankGroupItemView.BankGroupId = bankGroup.BankGroupId;
				bankGroupItemView.Description = bankGroup.Description;
				
				bankGroupItemView.RowVersion = bankGroup.RowVersion;
				return bankGroupItemView.GetBankGroupItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion BankGroupItemView
		#region ToDropDown
		public static SelectItem<BankGroupItemView> ToDropDownItem(this BankGroupItemView bankGroupView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<BankGroupItemView> selectItem = new SelectItem<BankGroupItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(bankGroupView.BankGroupId, bankGroupView.Description);
				selectItem.Value = bankGroupView.BankGroupGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: bankGroupView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<BankGroupItemView>> ToDropDownItem(this IEnumerable<BankGroupItemView> bankGroupItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<BankGroupItemView>> selectItems = new List<SelectItem<BankGroupItemView>>();
				foreach (BankGroupItemView item in bankGroupItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

