using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class BuyerInvoiceTableHandler
	{
		#region BuyerInvoiceTableListView
		public static List<BuyerInvoiceTableListView> GetBuyerInvoiceTableListViewValidation(this List<BuyerInvoiceTableListView> buyerInvoiceTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return buyerInvoiceTableListViews;
			}
			var result = new List<BuyerInvoiceTableListView>();
			try
			{
				foreach (BuyerInvoiceTableListView item in buyerInvoiceTableListViews)
				{
					result.Add(item.GetBuyerInvoiceTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BuyerInvoiceTableListView GetBuyerInvoiceTableListViewValidation(this BuyerInvoiceTableListView buyerInvoiceTableListView)
		{
			try
			{
				buyerInvoiceTableListView.RowAuthorize = buyerInvoiceTableListView.GetListRowAuthorize();
				return buyerInvoiceTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this BuyerInvoiceTableListView buyerInvoiceTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion BuyerInvoiceTableListView
		#region BuyerInvoiceTableItemView
		public static BuyerInvoiceTableItemView GetBuyerInvoiceTableItemViewValidation(this BuyerInvoiceTableItemView buyerInvoiceTableItemView)
		{
			try
			{
				buyerInvoiceTableItemView.RowAuthorize = buyerInvoiceTableItemView.GetItemRowAuthorize();
				return buyerInvoiceTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this BuyerInvoiceTableItemView buyerInvoiceTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static BuyerInvoiceTable ToBuyerInvoiceTable(this BuyerInvoiceTableItemView buyerInvoiceTableItemView)
		{
			try
			{
				BuyerInvoiceTable buyerInvoiceTable = new BuyerInvoiceTable();
				buyerInvoiceTable.CompanyGUID = buyerInvoiceTableItemView.CompanyGUID.StringToGuid();
				buyerInvoiceTable.CreatedBy = buyerInvoiceTableItemView.CreatedBy;
				buyerInvoiceTable.CreatedDateTime = buyerInvoiceTableItemView.CreatedDateTime.StringToSystemDateTime();
				buyerInvoiceTable.ModifiedBy = buyerInvoiceTableItemView.ModifiedBy;
				buyerInvoiceTable.ModifiedDateTime = buyerInvoiceTableItemView.ModifiedDateTime.StringToSystemDateTime();
				buyerInvoiceTable.Owner = buyerInvoiceTableItemView.Owner;
				buyerInvoiceTable.OwnerBusinessUnitGUID = buyerInvoiceTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				buyerInvoiceTable.BuyerInvoiceTableGUID = buyerInvoiceTableItemView.BuyerInvoiceTableGUID.StringToGuid();
				buyerInvoiceTable.Amount = buyerInvoiceTableItemView.Amount;
				buyerInvoiceTable.BuyerAgreementLineGUID = buyerInvoiceTableItemView.BuyerAgreementLineGUID.StringToGuidNull();
				buyerInvoiceTable.BuyerAgreementTableGUID = buyerInvoiceTableItemView.BuyerAgreementTableGUID.StringToGuidNull();
				buyerInvoiceTable.BuyerInvoiceId = buyerInvoiceTableItemView.BuyerInvoiceId;
				buyerInvoiceTable.CreditAppLineGUID = buyerInvoiceTableItemView.CreditAppLineGUID.StringToGuid();
				buyerInvoiceTable.DueDate = buyerInvoiceTableItemView.DueDate.StringToDate();
				buyerInvoiceTable.InvoiceDate = buyerInvoiceTableItemView.InvoiceDate.StringToDate();
				buyerInvoiceTable.Remark = buyerInvoiceTableItemView.Remark;
				
				buyerInvoiceTable.RowVersion = buyerInvoiceTableItemView.RowVersion;
				return buyerInvoiceTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<BuyerInvoiceTable> ToBuyerInvoiceTable(this IEnumerable<BuyerInvoiceTableItemView> buyerInvoiceTableItemViews)
		{
			try
			{
				List<BuyerInvoiceTable> buyerInvoiceTables = new List<BuyerInvoiceTable>();
				foreach (BuyerInvoiceTableItemView item in buyerInvoiceTableItemViews)
				{
					buyerInvoiceTables.Add(item.ToBuyerInvoiceTable());
				}
				return buyerInvoiceTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BuyerInvoiceTableItemView ToBuyerInvoiceTableItemView(this BuyerInvoiceTable buyerInvoiceTable)
		{
			try
			{
				BuyerInvoiceTableItemView buyerInvoiceTableItemView = new BuyerInvoiceTableItemView();
				buyerInvoiceTableItemView.CompanyGUID = buyerInvoiceTable.CompanyGUID.GuidNullToString();
				buyerInvoiceTableItemView.CreatedBy = buyerInvoiceTable.CreatedBy;
				buyerInvoiceTableItemView.CreatedDateTime = buyerInvoiceTable.CreatedDateTime.DateTimeToString();
				buyerInvoiceTableItemView.ModifiedBy = buyerInvoiceTable.ModifiedBy;
				buyerInvoiceTableItemView.ModifiedDateTime = buyerInvoiceTable.ModifiedDateTime.DateTimeToString();
				buyerInvoiceTableItemView.Owner = buyerInvoiceTable.Owner;
				buyerInvoiceTableItemView.OwnerBusinessUnitGUID = buyerInvoiceTable.OwnerBusinessUnitGUID.GuidNullToString();
				buyerInvoiceTableItemView.BuyerInvoiceTableGUID = buyerInvoiceTable.BuyerInvoiceTableGUID.GuidNullToString();
				buyerInvoiceTableItemView.Amount = buyerInvoiceTable.Amount;
				buyerInvoiceTableItemView.BuyerAgreementLineGUID = buyerInvoiceTable.BuyerAgreementLineGUID.GuidNullToString();
				buyerInvoiceTableItemView.BuyerAgreementTableGUID = buyerInvoiceTable.BuyerAgreementTableGUID.GuidNullToString();
				buyerInvoiceTableItemView.BuyerInvoiceId = buyerInvoiceTable.BuyerInvoiceId;
				buyerInvoiceTableItemView.CreditAppLineGUID = buyerInvoiceTable.CreditAppLineGUID.GuidNullToString();
				buyerInvoiceTableItemView.DueDate = buyerInvoiceTable.DueDate.DateToString();
				buyerInvoiceTableItemView.InvoiceDate = buyerInvoiceTable.InvoiceDate.DateToString();
				buyerInvoiceTableItemView.Remark = buyerInvoiceTable.Remark;
				
				buyerInvoiceTableItemView.RowVersion = buyerInvoiceTable.RowVersion;
				return buyerInvoiceTableItemView.GetBuyerInvoiceTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion BuyerInvoiceTableItemView
		#region ToDropDown
		public static SelectItem<BuyerInvoiceTableItemView> ToDropDownItem(this BuyerInvoiceTableItemView buyerInvoiceTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<BuyerInvoiceTableItemView> selectItem = new SelectItem<BuyerInvoiceTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(buyerInvoiceTableView.BuyerInvoiceId);
				selectItem.Value = buyerInvoiceTableView.BuyerInvoiceTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: buyerInvoiceTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<BuyerInvoiceTableItemView>> ToDropDownItem(this IEnumerable<BuyerInvoiceTableItemView> buyerInvoiceTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<BuyerInvoiceTableItemView>> selectItems = new List<SelectItem<BuyerInvoiceTableItemView>>();
				foreach (BuyerInvoiceTableItemView item in buyerInvoiceTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

