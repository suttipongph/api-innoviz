using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class IntercompanyInvoiceAdjustmentHandler
	{
		#region IntercompanyInvoiceAdjustmentListView
		public static List<IntercompanyInvoiceAdjustmentListView> GetIntercompanyInvoiceAdjustmentListViewValidation(this List<IntercompanyInvoiceAdjustmentListView> intercompanyInvoiceAdjustmentListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return intercompanyInvoiceAdjustmentListViews;
			}
			var result = new List<IntercompanyInvoiceAdjustmentListView>();
			try
			{
				foreach (IntercompanyInvoiceAdjustmentListView item in intercompanyInvoiceAdjustmentListViews)
				{
					result.Add(item.GetIntercompanyInvoiceAdjustmentListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IntercompanyInvoiceAdjustmentListView GetIntercompanyInvoiceAdjustmentListViewValidation(this IntercompanyInvoiceAdjustmentListView intercompanyInvoiceAdjustmentListView)
		{
			try
			{
				intercompanyInvoiceAdjustmentListView.RowAuthorize = intercompanyInvoiceAdjustmentListView.GetListRowAuthorize();
				return intercompanyInvoiceAdjustmentListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this IntercompanyInvoiceAdjustmentListView intercompanyInvoiceAdjustmentListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion IntercompanyInvoiceAdjustmentListView
		#region IntercompanyInvoiceAdjustmentItemView
		public static IntercompanyInvoiceAdjustmentItemView GetIntercompanyInvoiceAdjustmentItemViewValidation(this IntercompanyInvoiceAdjustmentItemView intercompanyInvoiceAdjustmentItemView)
		{
			try
			{
				intercompanyInvoiceAdjustmentItemView.RowAuthorize = intercompanyInvoiceAdjustmentItemView.GetItemRowAuthorize();
				return intercompanyInvoiceAdjustmentItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this IntercompanyInvoiceAdjustmentItemView intercompanyInvoiceAdjustmentItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static IntercompanyInvoiceAdjustment ToIntercompanyInvoiceAdjustment(this IntercompanyInvoiceAdjustmentItemView intercompanyInvoiceAdjustmentItemView)
		{
			try
			{
				IntercompanyInvoiceAdjustment intercompanyInvoiceAdjustment = new IntercompanyInvoiceAdjustment();
				intercompanyInvoiceAdjustment.CompanyGUID = intercompanyInvoiceAdjustmentItemView.CompanyGUID.StringToGuid();
				intercompanyInvoiceAdjustment.CreatedBy = intercompanyInvoiceAdjustmentItemView.CreatedBy;
				intercompanyInvoiceAdjustment.CreatedDateTime = intercompanyInvoiceAdjustmentItemView.CreatedDateTime.StringToSystemDateTime();
				intercompanyInvoiceAdjustment.ModifiedBy = intercompanyInvoiceAdjustmentItemView.ModifiedBy;
				intercompanyInvoiceAdjustment.ModifiedDateTime = intercompanyInvoiceAdjustmentItemView.ModifiedDateTime.StringToSystemDateTime();
				intercompanyInvoiceAdjustment.Owner = intercompanyInvoiceAdjustmentItemView.Owner;
				intercompanyInvoiceAdjustment.OwnerBusinessUnitGUID = intercompanyInvoiceAdjustmentItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				intercompanyInvoiceAdjustment.IntercompanyInvoiceAdjustmentGUID = intercompanyInvoiceAdjustmentItemView.IntercompanyInvoiceAdjustmentGUID.StringToGuid();
				intercompanyInvoiceAdjustment.Adjustment = intercompanyInvoiceAdjustmentItemView.Adjustment;
				intercompanyInvoiceAdjustment.DocumentReasonGUID = intercompanyInvoiceAdjustmentItemView.DocumentReasonGUID.StringToGuid();
				intercompanyInvoiceAdjustment.IntercompanyInvoiceTableGUID = intercompanyInvoiceAdjustmentItemView.IntercompanyInvoiceTableGUID.StringToGuid();
				intercompanyInvoiceAdjustment.OriginalAmount = intercompanyInvoiceAdjustmentItemView.OriginalAmount;
				
				intercompanyInvoiceAdjustment.RowVersion = intercompanyInvoiceAdjustmentItemView.RowVersion;
				return intercompanyInvoiceAdjustment;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<IntercompanyInvoiceAdjustment> ToIntercompanyInvoiceAdjustment(this IEnumerable<IntercompanyInvoiceAdjustmentItemView> intercompanyInvoiceAdjustmentItemViews)
		{
			try
			{
				List<IntercompanyInvoiceAdjustment> intercompanyInvoiceAdjustments = new List<IntercompanyInvoiceAdjustment>();
				foreach (IntercompanyInvoiceAdjustmentItemView item in intercompanyInvoiceAdjustmentItemViews)
				{
					intercompanyInvoiceAdjustments.Add(item.ToIntercompanyInvoiceAdjustment());
				}
				return intercompanyInvoiceAdjustments;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IntercompanyInvoiceAdjustmentItemView ToIntercompanyInvoiceAdjustmentItemView(this IntercompanyInvoiceAdjustment intercompanyInvoiceAdjustment)
		{
			try
			{
				IntercompanyInvoiceAdjustmentItemView intercompanyInvoiceAdjustmentItemView = new IntercompanyInvoiceAdjustmentItemView();
				intercompanyInvoiceAdjustmentItemView.CompanyGUID = intercompanyInvoiceAdjustment.CompanyGUID.GuidNullToString();
				intercompanyInvoiceAdjustmentItemView.CreatedBy = intercompanyInvoiceAdjustment.CreatedBy;
				intercompanyInvoiceAdjustmentItemView.CreatedDateTime = intercompanyInvoiceAdjustment.CreatedDateTime.DateTimeToString();
				intercompanyInvoiceAdjustmentItemView.ModifiedBy = intercompanyInvoiceAdjustment.ModifiedBy;
				intercompanyInvoiceAdjustmentItemView.ModifiedDateTime = intercompanyInvoiceAdjustment.ModifiedDateTime.DateTimeToString();
				intercompanyInvoiceAdjustmentItemView.Owner = intercompanyInvoiceAdjustment.Owner;
				intercompanyInvoiceAdjustmentItemView.OwnerBusinessUnitGUID = intercompanyInvoiceAdjustment.OwnerBusinessUnitGUID.GuidNullToString();
				intercompanyInvoiceAdjustmentItemView.IntercompanyInvoiceAdjustmentGUID = intercompanyInvoiceAdjustment.IntercompanyInvoiceAdjustmentGUID.GuidNullToString();
				intercompanyInvoiceAdjustmentItemView.Adjustment = intercompanyInvoiceAdjustment.Adjustment;
				intercompanyInvoiceAdjustmentItemView.DocumentReasonGUID = intercompanyInvoiceAdjustment.DocumentReasonGUID.GuidNullToString();
				intercompanyInvoiceAdjustmentItemView.IntercompanyInvoiceTableGUID = intercompanyInvoiceAdjustment.IntercompanyInvoiceTableGUID.GuidNullToString();
				intercompanyInvoiceAdjustmentItemView.OriginalAmount = intercompanyInvoiceAdjustment.OriginalAmount;
				
				intercompanyInvoiceAdjustmentItemView.RowVersion = intercompanyInvoiceAdjustment.RowVersion;
				return intercompanyInvoiceAdjustmentItemView.GetIntercompanyInvoiceAdjustmentItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion IntercompanyInvoiceAdjustmentItemView
		#region ToDropDown
		public static SelectItem<IntercompanyInvoiceAdjustmentItemView> ToDropDownItem(this IntercompanyInvoiceAdjustmentItemView intercompanyInvoiceAdjustmentView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<IntercompanyInvoiceAdjustmentItemView> selectItem = new SelectItem<IntercompanyInvoiceAdjustmentItemView>();
				selectItem.Label = null;
				selectItem.Value = intercompanyInvoiceAdjustmentView.IntercompanyInvoiceAdjustmentGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: intercompanyInvoiceAdjustmentView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<IntercompanyInvoiceAdjustmentItemView>> ToDropDownItem(this IEnumerable<IntercompanyInvoiceAdjustmentItemView> intercompanyInvoiceAdjustmentItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<IntercompanyInvoiceAdjustmentItemView>> selectItems = new List<SelectItem<IntercompanyInvoiceAdjustmentItemView>>();
				foreach (IntercompanyInvoiceAdjustmentItemView item in intercompanyInvoiceAdjustmentItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

