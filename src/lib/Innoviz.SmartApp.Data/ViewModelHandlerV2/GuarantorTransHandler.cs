using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class GuarantorTransHandler
	{
		#region GuarantorTransListView
		public static List<GuarantorTransListView> GetGuarantorTransListViewValidation(this List<GuarantorTransListView> guarantorTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return guarantorTransListViews;
			}
			var result = new List<GuarantorTransListView>();
			try
			{
				foreach (GuarantorTransListView item in guarantorTransListViews)
				{
					result.Add(item.GetGuarantorTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static GuarantorTransListView GetGuarantorTransListViewValidation(this GuarantorTransListView guarantorTransListView)
		{
			try
			{
				guarantorTransListView.RowAuthorize = guarantorTransListView.GetListRowAuthorize();
				return guarantorTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this GuarantorTransListView guarantorTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion GuarantorTransListView
		#region GuarantorTransItemView
		public static GuarantorTransItemView GetGuarantorTransItemViewValidation(this GuarantorTransItemView guarantorTransItemView)
		{
			try
			{
				guarantorTransItemView.RowAuthorize = guarantorTransItemView.GetItemRowAuthorize();
				return guarantorTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this GuarantorTransItemView guarantorTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static GuarantorTrans ToGuarantorTrans(this GuarantorTransItemView guarantorTransItemView)
		{
			try
			{
				GuarantorTrans guarantorTrans = new GuarantorTrans();
				guarantorTrans.CompanyGUID = guarantorTransItemView.CompanyGUID.StringToGuid();
				guarantorTrans.CreatedBy = guarantorTransItemView.CreatedBy;
				guarantorTrans.CreatedDateTime = guarantorTransItemView.CreatedDateTime.StringToSystemDateTime();
				guarantorTrans.ModifiedBy = guarantorTransItemView.ModifiedBy;
				guarantorTrans.ModifiedDateTime = guarantorTransItemView.ModifiedDateTime.StringToSystemDateTime();
				guarantorTrans.Owner = guarantorTransItemView.Owner;
				guarantorTrans.OwnerBusinessUnitGUID = guarantorTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				guarantorTrans.GuarantorTransGUID = guarantorTransItemView.GuarantorTransGUID.StringToGuid();
				guarantorTrans.Affiliate = guarantorTransItemView.Affiliate;
				guarantorTrans.GuarantorTypeGUID = guarantorTransItemView.GuarantorTypeGUID.StringToGuid();
				guarantorTrans.InActive = guarantorTransItemView.InActive;
				guarantorTrans.Ordering = guarantorTransItemView.Ordering;
				guarantorTrans.RefGuarantorTransGUID = guarantorTransItemView.RefGuarantorTransGUID.StringToGuidNull();
				guarantorTrans.RefGUID = guarantorTransItemView.RefGUID.StringToGuid();
				guarantorTrans.RefType = guarantorTransItemView.RefType;
				guarantorTrans.RelatedPersonTableGUID = guarantorTransItemView.RelatedPersonTableGUID.StringToGuid();
				guarantorTrans.MaximumGuaranteeAmount = guarantorTransItemView.MaximumGuaranteeAmount;
				
				guarantorTrans.RowVersion = guarantorTransItemView.RowVersion;
				return guarantorTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<GuarantorTrans> ToGuarantorTrans(this IEnumerable<GuarantorTransItemView> guarantorTransItemViews)
		{
			try
			{
				List<GuarantorTrans> guarantorTranss = new List<GuarantorTrans>();
				foreach (GuarantorTransItemView item in guarantorTransItemViews)
				{
					guarantorTranss.Add(item.ToGuarantorTrans());
				}
				return guarantorTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static GuarantorTransItemView ToGuarantorTransItemView(this GuarantorTrans guarantorTrans)
		{
			try
			{
				GuarantorTransItemView guarantorTransItemView = new GuarantorTransItemView();
				guarantorTransItemView.CompanyGUID = guarantorTrans.CompanyGUID.GuidNullToString();
				guarantorTransItemView.CreatedBy = guarantorTrans.CreatedBy;
				guarantorTransItemView.CreatedDateTime = guarantorTrans.CreatedDateTime.DateTimeToString();
				guarantorTransItemView.ModifiedBy = guarantorTrans.ModifiedBy;
				guarantorTransItemView.ModifiedDateTime = guarantorTrans.ModifiedDateTime.DateTimeToString();
				guarantorTransItemView.Owner = guarantorTrans.Owner;
				guarantorTransItemView.OwnerBusinessUnitGUID = guarantorTrans.OwnerBusinessUnitGUID.GuidNullToString();
				guarantorTransItemView.GuarantorTransGUID = guarantorTrans.GuarantorTransGUID.GuidNullToString();
				guarantorTransItemView.Affiliate = guarantorTrans.Affiliate;
				guarantorTransItemView.GuarantorTypeGUID = guarantorTrans.GuarantorTypeGUID.GuidNullToString();
				guarantorTransItemView.InActive = guarantorTrans.InActive;
				guarantorTransItemView.Ordering = guarantorTrans.Ordering;
				guarantorTransItemView.RefGuarantorTransGUID = guarantorTrans.RefGuarantorTransGUID.GuidNullToString();
				guarantorTransItemView.RefGUID = guarantorTrans.RefGUID.GuidNullToString();
				guarantorTransItemView.RefType = guarantorTrans.RefType;
				guarantorTransItemView.RelatedPersonTableGUID = guarantorTrans.RelatedPersonTableGUID.GuidNullToString();
				
				guarantorTransItemView.RowVersion = guarantorTrans.RowVersion;
				return guarantorTransItemView.GetGuarantorTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GuarantorTransItemView
		#region ToDropDown
		public static SelectItem<GuarantorTransItemView> ToDropDownItem(this GuarantorTransItemView guarantorTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<GuarantorTransItemView> selectItem = new SelectItem<GuarantorTransItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(guarantorTransView.RelatedPersonTable_RelatedPersonId.ToString(), guarantorTransView.RelatedPersonTable_Name.ToString());
				selectItem.Value = guarantorTransView.GuarantorTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: guarantorTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<GuarantorTransItemView>> ToDropDownItem(this IEnumerable<GuarantorTransItemView> guarantorTransItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<GuarantorTransItemView>> selectItems = new List<SelectItem<GuarantorTransItemView>>();
				foreach (GuarantorTransItemView item in guarantorTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

