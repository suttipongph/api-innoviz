using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CustBusinessCollateralHandler
	{
		#region CustBusinessCollateralListView
		public static List<CustBusinessCollateralListView> GetCustBusinessCollateralListViewValidation(this List<CustBusinessCollateralListView> custBusinessCollateralListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return custBusinessCollateralListViews;
			}
			var result = new List<CustBusinessCollateralListView>();
			try
			{
				foreach (CustBusinessCollateralListView item in custBusinessCollateralListViews)
				{
					result.Add(item.GetCustBusinessCollateralListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CustBusinessCollateralListView GetCustBusinessCollateralListViewValidation(this CustBusinessCollateralListView custBusinessCollateralListView)
		{
			try
			{
				custBusinessCollateralListView.RowAuthorize = custBusinessCollateralListView.GetListRowAuthorize();
				return custBusinessCollateralListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CustBusinessCollateralListView custBusinessCollateralListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CustBusinessCollateralListView
		#region CustBusinessCollateralItemView
		public static CustBusinessCollateralItemView GetCustBusinessCollateralItemViewValidation(this CustBusinessCollateralItemView custBusinessCollateralItemView)
		{
			try
			{
				custBusinessCollateralItemView.RowAuthorize = custBusinessCollateralItemView.GetItemRowAuthorize();
				return custBusinessCollateralItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CustBusinessCollateralItemView custBusinessCollateralItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CustBusinessCollateral ToCustBusinessCollateral(this CustBusinessCollateralItemView custBusinessCollateralItemView)
		{
			try
			{
				CustBusinessCollateral custBusinessCollateral = new CustBusinessCollateral();
				custBusinessCollateral.CompanyGUID = custBusinessCollateralItemView.CompanyGUID.StringToGuid();
				custBusinessCollateral.CreatedBy = custBusinessCollateralItemView.CreatedBy;
				custBusinessCollateral.CreatedDateTime = custBusinessCollateralItemView.CreatedDateTime.StringToSystemDateTime();
				custBusinessCollateral.ModifiedBy = custBusinessCollateralItemView.ModifiedBy;
				custBusinessCollateral.ModifiedDateTime = custBusinessCollateralItemView.ModifiedDateTime.StringToSystemDateTime();
				custBusinessCollateral.Owner = custBusinessCollateralItemView.Owner;
				custBusinessCollateral.OwnerBusinessUnitGUID = custBusinessCollateralItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				custBusinessCollateral.CustBusinessCollateralGUID = custBusinessCollateralItemView.CustBusinessCollateralGUID.StringToGuid();
				custBusinessCollateral.AccountNumber = custBusinessCollateralItemView.AccountNumber;
				custBusinessCollateral.BankGroupGUID = custBusinessCollateralItemView.BankGroupGUID.StringToGuidNull();
				custBusinessCollateral.BankTypeGUID = custBusinessCollateralItemView.BankTypeGUID.StringToGuidNull();
				custBusinessCollateral.BusinessCollateralAgmLineGUID = custBusinessCollateralItemView.BusinessCollateralAgmLineGUID.StringToGuid();
				custBusinessCollateral.BusinessCollateralStatusGUID = custBusinessCollateralItemView.BusinessCollateralStatusGUID.StringToGuidNull();
				custBusinessCollateral.BusinessCollateralSubTypeGUID = custBusinessCollateralItemView.BusinessCollateralSubTypeGUID.StringToGuid();
				custBusinessCollateral.BusinessCollateralTypeGUID = custBusinessCollateralItemView.BusinessCollateralTypeGUID.StringToGuid();
				custBusinessCollateral.BusinessCollateralValue = custBusinessCollateralItemView.BusinessCollateralValue;
				custBusinessCollateral.BuyerName = custBusinessCollateralItemView.BuyerName;
				custBusinessCollateral.BuyerTableGUID = custBusinessCollateralItemView.BuyerTableGUID.StringToGuidNull();
				custBusinessCollateral.BuyerTaxIdentificationId = custBusinessCollateralItemView.BuyerTaxIdentificationId;
				custBusinessCollateral.Cancelled = custBusinessCollateralItemView.Cancelled;
				custBusinessCollateral.CapitalValuation = custBusinessCollateralItemView.CapitalValuation;
				custBusinessCollateral.ChassisNumber = custBusinessCollateralItemView.ChassisNumber;
				custBusinessCollateral.CreditAppRequestTableGUID = custBusinessCollateralItemView.CreditAppRequestTableGUID.StringToGuid();
				custBusinessCollateral.CustBusinessCollateralId = custBusinessCollateralItemView.CustBusinessCollateralId;
				custBusinessCollateral.CustomerTableGUID = custBusinessCollateralItemView.CustomerTableGUID.StringToGuid();
				custBusinessCollateral.DateOfValuation = custBusinessCollateralItemView.DateOfValuation.StringNullToDateNull();
				custBusinessCollateral.DBDRegistrationAmount = custBusinessCollateralItemView.DBDRegistrationAmount;
				custBusinessCollateral.DBDRegistrationDate = custBusinessCollateralItemView.DBDRegistrationDate.StringNullToDateNull();
				custBusinessCollateral.DBDRegistrationDescription = custBusinessCollateralItemView.DBDRegistrationDescription;
				custBusinessCollateral.DBDRegistrationId = custBusinessCollateralItemView.DBDRegistrationId;
				custBusinessCollateral.Description = custBusinessCollateralItemView.Description;
				custBusinessCollateral.GuaranteeAmount = custBusinessCollateralItemView.GuaranteeAmount;
				custBusinessCollateral.Lessee = custBusinessCollateralItemView.Lessee;
				custBusinessCollateral.Lessor = custBusinessCollateralItemView.Lessor;
				custBusinessCollateral.MachineNumber = custBusinessCollateralItemView.MachineNumber;
				custBusinessCollateral.MachineRegisteredStatus = custBusinessCollateralItemView.MachineRegisteredStatus;
				custBusinessCollateral.Ownership = custBusinessCollateralItemView.Ownership;
				custBusinessCollateral.PreferentialCreditorNumber = custBusinessCollateralItemView.PreferentialCreditorNumber;
				custBusinessCollateral.ProjectName = custBusinessCollateralItemView.ProjectName;
				custBusinessCollateral.Quantity = custBusinessCollateralItemView.Quantity;
				custBusinessCollateral.RefAgreementDate = custBusinessCollateralItemView.RefAgreementDate.StringNullToDateNull();
				custBusinessCollateral.RefAgreementId = custBusinessCollateralItemView.RefAgreementId;
				custBusinessCollateral.RegisteredPlace = custBusinessCollateralItemView.RegisteredPlace;
				custBusinessCollateral.RegistrationPlateNumber = custBusinessCollateralItemView.RegistrationPlateNumber;
				custBusinessCollateral.Remark = custBusinessCollateralItemView.Remark;
				custBusinessCollateral.TitleDeedDistrict = custBusinessCollateralItemView.TitleDeedDistrict;
				custBusinessCollateral.TitleDeedNumber = custBusinessCollateralItemView.TitleDeedNumber;
				custBusinessCollateral.TitleDeedProvince = custBusinessCollateralItemView.TitleDeedProvince;
				custBusinessCollateral.TitleDeedSubDistrict = custBusinessCollateralItemView.TitleDeedSubDistrict;
				custBusinessCollateral.Unit = custBusinessCollateralItemView.Unit;
				custBusinessCollateral.ValuationCommittee = custBusinessCollateralItemView.ValuationCommittee;
				
				custBusinessCollateral.RowVersion = custBusinessCollateralItemView.RowVersion;
				return custBusinessCollateral;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CustBusinessCollateral> ToCustBusinessCollateral(this IEnumerable<CustBusinessCollateralItemView> custBusinessCollateralItemViews)
		{
			try
			{
				List<CustBusinessCollateral> custBusinessCollaterals = new List<CustBusinessCollateral>();
				foreach (CustBusinessCollateralItemView item in custBusinessCollateralItemViews)
				{
					custBusinessCollaterals.Add(item.ToCustBusinessCollateral());
				}
				return custBusinessCollaterals;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CustBusinessCollateralItemView ToCustBusinessCollateralItemView(this CustBusinessCollateral custBusinessCollateral)
		{
			try
			{
				CustBusinessCollateralItemView custBusinessCollateralItemView = new CustBusinessCollateralItemView();
				custBusinessCollateralItemView.CompanyGUID = custBusinessCollateral.CompanyGUID.GuidNullToString();
				custBusinessCollateralItemView.CreatedBy = custBusinessCollateral.CreatedBy;
				custBusinessCollateralItemView.CreatedDateTime = custBusinessCollateral.CreatedDateTime.DateTimeToString();
				custBusinessCollateralItemView.ModifiedBy = custBusinessCollateral.ModifiedBy;
				custBusinessCollateralItemView.ModifiedDateTime = custBusinessCollateral.ModifiedDateTime.DateTimeToString();
				custBusinessCollateralItemView.Owner = custBusinessCollateral.Owner;
				custBusinessCollateralItemView.OwnerBusinessUnitGUID = custBusinessCollateral.OwnerBusinessUnitGUID.GuidNullToString();
				custBusinessCollateralItemView.CustBusinessCollateralGUID = custBusinessCollateral.CustBusinessCollateralGUID.GuidNullToString();
				custBusinessCollateralItemView.AccountNumber = custBusinessCollateral.AccountNumber;
				custBusinessCollateralItemView.BankGroupGUID = custBusinessCollateral.BankGroupGUID.GuidNullToString();
				custBusinessCollateralItemView.BankTypeGUID = custBusinessCollateral.BankTypeGUID.GuidNullToString();
				custBusinessCollateralItemView.BusinessCollateralAgmLineGUID = custBusinessCollateral.BusinessCollateralAgmLineGUID.GuidNullToString();
				custBusinessCollateralItemView.BusinessCollateralStatusGUID = custBusinessCollateral.BusinessCollateralStatusGUID.GuidNullToString();
				custBusinessCollateralItemView.BusinessCollateralSubTypeGUID = custBusinessCollateral.BusinessCollateralSubTypeGUID.GuidNullToString();
				custBusinessCollateralItemView.BusinessCollateralTypeGUID = custBusinessCollateral.BusinessCollateralTypeGUID.GuidNullToString();
				custBusinessCollateralItemView.BusinessCollateralValue = custBusinessCollateral.BusinessCollateralValue;
				custBusinessCollateralItemView.BuyerName = custBusinessCollateral.BuyerName;
				custBusinessCollateralItemView.BuyerTableGUID = custBusinessCollateral.BuyerTableGUID.GuidNullToString();
				custBusinessCollateralItemView.BuyerTaxIdentificationId = custBusinessCollateral.BuyerTaxIdentificationId;
				custBusinessCollateralItemView.Cancelled = custBusinessCollateral.Cancelled;
				custBusinessCollateralItemView.CapitalValuation = custBusinessCollateral.CapitalValuation;
				custBusinessCollateralItemView.ChassisNumber = custBusinessCollateral.ChassisNumber;
				custBusinessCollateralItemView.CreditAppRequestTableGUID = custBusinessCollateral.CreditAppRequestTableGUID.GuidNullToString();
				custBusinessCollateralItemView.CustBusinessCollateralId = custBusinessCollateral.CustBusinessCollateralId;
				custBusinessCollateralItemView.CustomerTableGUID = custBusinessCollateral.CustomerTableGUID.GuidNullToString();
				custBusinessCollateralItemView.DateOfValuation = custBusinessCollateral.DateOfValuation.DateNullToString();
				custBusinessCollateralItemView.DBDRegistrationAmount = custBusinessCollateral.DBDRegistrationAmount;
				custBusinessCollateralItemView.DBDRegistrationDate = custBusinessCollateral.DBDRegistrationDate.DateNullToString();
				custBusinessCollateralItemView.DBDRegistrationDescription = custBusinessCollateral.DBDRegistrationDescription;
				custBusinessCollateralItemView.DBDRegistrationId = custBusinessCollateral.DBDRegistrationId;
				custBusinessCollateralItemView.Description = custBusinessCollateral.Description;
				custBusinessCollateralItemView.GuaranteeAmount = custBusinessCollateral.GuaranteeAmount;
				custBusinessCollateralItemView.Lessee = custBusinessCollateral.Lessee;
				custBusinessCollateralItemView.Lessor = custBusinessCollateral.Lessor;
				custBusinessCollateralItemView.MachineNumber = custBusinessCollateral.MachineNumber;
				custBusinessCollateralItemView.MachineRegisteredStatus = custBusinessCollateral.MachineRegisteredStatus;
				custBusinessCollateralItemView.Ownership = custBusinessCollateral.Ownership;
				custBusinessCollateralItemView.PreferentialCreditorNumber = custBusinessCollateral.PreferentialCreditorNumber;
				custBusinessCollateralItemView.ProjectName = custBusinessCollateral.ProjectName;
				custBusinessCollateralItemView.Quantity = custBusinessCollateral.Quantity;
				custBusinessCollateralItemView.RefAgreementDate = custBusinessCollateral.RefAgreementDate.DateNullToString();
				custBusinessCollateralItemView.RefAgreementId = custBusinessCollateral.RefAgreementId;
				custBusinessCollateralItemView.RegisteredPlace = custBusinessCollateral.RegisteredPlace;
				custBusinessCollateralItemView.RegistrationPlateNumber = custBusinessCollateral.RegistrationPlateNumber;
				custBusinessCollateralItemView.Remark = custBusinessCollateral.Remark;
				custBusinessCollateralItemView.TitleDeedDistrict = custBusinessCollateral.TitleDeedDistrict;
				custBusinessCollateralItemView.TitleDeedNumber = custBusinessCollateral.TitleDeedNumber;
				custBusinessCollateralItemView.TitleDeedProvince = custBusinessCollateral.TitleDeedProvince;
				custBusinessCollateralItemView.TitleDeedSubDistrict = custBusinessCollateral.TitleDeedSubDistrict;
				custBusinessCollateralItemView.Unit = custBusinessCollateral.Unit;
				custBusinessCollateralItemView.ValuationCommittee = custBusinessCollateral.ValuationCommittee;
				
				custBusinessCollateralItemView.RowVersion = custBusinessCollateral.RowVersion;
				return custBusinessCollateralItemView.GetCustBusinessCollateralItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CustBusinessCollateralItemView
		#region ToDropDown
		public static SelectItem<CustBusinessCollateralItemView> ToDropDownItem(this CustBusinessCollateralItemView custBusinessCollateralView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CustBusinessCollateralItemView> selectItem = new SelectItem<CustBusinessCollateralItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(custBusinessCollateralView.CustBusinessCollateralId, custBusinessCollateralView.Description);
				selectItem.Value = custBusinessCollateralView.CustBusinessCollateralGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: custBusinessCollateralView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CustBusinessCollateralItemView>> ToDropDownItem(this IEnumerable<CustBusinessCollateralItemView> custBusinessCollateralItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CustBusinessCollateralItemView>> selectItems = new List<SelectItem<CustBusinessCollateralItemView>>();
				foreach (CustBusinessCollateralItemView item in custBusinessCollateralItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

