using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class VerificationTransHandler
	{
		#region VerificationTransListView
		public static List<VerificationTransListView> GetVerificationTransListViewValidation(this List<VerificationTransListView> verificationTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return verificationTransListViews;
			}
			var result = new List<VerificationTransListView>();
			try
			{
				foreach (VerificationTransListView item in verificationTransListViews)
				{
					result.Add(item.GetVerificationTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static VerificationTransListView GetVerificationTransListViewValidation(this VerificationTransListView verificationTransListView)
		{
			try
			{
				verificationTransListView.RowAuthorize = verificationTransListView.GetListRowAuthorize();
				return verificationTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this VerificationTransListView verificationTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion VerificationTransListView
		#region VerificationTransItemView
		public static VerificationTransItemView GetVerificationTransItemViewValidation(this VerificationTransItemView verificationTransItemView)
		{
			try
			{
				verificationTransItemView.RowAuthorize = verificationTransItemView.GetItemRowAuthorize();
				return verificationTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this VerificationTransItemView verificationTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static VerificationTrans ToVerificationTrans(this VerificationTransItemView verificationTransItemView)
		{
			try
			{
				VerificationTrans verificationTrans = new VerificationTrans();
				verificationTrans.CompanyGUID = verificationTransItemView.CompanyGUID.StringToGuid();
				verificationTrans.CreatedBy = verificationTransItemView.CreatedBy;
				verificationTrans.CreatedDateTime = verificationTransItemView.CreatedDateTime.StringToSystemDateTime();
				verificationTrans.ModifiedBy = verificationTransItemView.ModifiedBy;
				verificationTrans.ModifiedDateTime = verificationTransItemView.ModifiedDateTime.StringToSystemDateTime();
				verificationTrans.Owner = verificationTransItemView.Owner;
				verificationTrans.OwnerBusinessUnitGUID = verificationTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				verificationTrans.VerificationTransGUID = verificationTransItemView.VerificationTransGUID.StringToGuid();
				verificationTrans.DocumentId = verificationTransItemView.DocumentId;
				verificationTrans.RefGUID = verificationTransItemView.RefGUID.StringToGuid();
				verificationTrans.RefType = verificationTransItemView.RefType;
				verificationTrans.VerificationTableGUID = verificationTransItemView.VerificationTableGUID.StringToGuid();
				
				verificationTrans.RowVersion = verificationTransItemView.RowVersion;
				return verificationTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<VerificationTrans> ToVerificationTrans(this IEnumerable<VerificationTransItemView> verificationTransItemViews)
		{
			try
			{
				List<VerificationTrans> verificationTranss = new List<VerificationTrans>();
				foreach (VerificationTransItemView item in verificationTransItemViews)
				{
					verificationTranss.Add(item.ToVerificationTrans());
				}
				return verificationTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static VerificationTransItemView ToVerificationTransItemView(this VerificationTrans verificationTrans)
		{
			try
			{
				VerificationTransItemView verificationTransItemView = new VerificationTransItemView();
				verificationTransItemView.CompanyGUID = verificationTrans.CompanyGUID.GuidNullToString();
				verificationTransItemView.CreatedBy = verificationTrans.CreatedBy;
				verificationTransItemView.CreatedDateTime = verificationTrans.CreatedDateTime.DateTimeToString();
				verificationTransItemView.ModifiedBy = verificationTrans.ModifiedBy;
				verificationTransItemView.ModifiedDateTime = verificationTrans.ModifiedDateTime.DateTimeToString();
				verificationTransItemView.Owner = verificationTrans.Owner;
				verificationTransItemView.OwnerBusinessUnitGUID = verificationTrans.OwnerBusinessUnitGUID.GuidNullToString();
				verificationTransItemView.VerificationTransGUID = verificationTrans.VerificationTransGUID.GuidNullToString();
				verificationTransItemView.DocumentId = verificationTrans.DocumentId;
				verificationTransItemView.RefGUID = verificationTrans.RefGUID.GuidNullToString();
				verificationTransItemView.RefType = verificationTrans.RefType;
				verificationTransItemView.VerificationTableGUID = verificationTrans.VerificationTableGUID.GuidNullToString();
				
				verificationTransItemView.RowVersion = verificationTrans.RowVersion;
				return verificationTransItemView.GetVerificationTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion VerificationTransItemView
		#region ToDropDown
		public static SelectItem<VerificationTransItemView> ToDropDownItem(this VerificationTransItemView verificationTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<VerificationTransItemView> selectItem = new SelectItem<VerificationTransItemView>();
				selectItem.Label = null;
				selectItem.Value = verificationTransView.VerificationTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: verificationTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<VerificationTransItemView>> ToDropDownItem(this IEnumerable<VerificationTransItemView> verificationTransItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<VerificationTransItemView>> selectItems = new List<SelectItem<VerificationTransItemView>>();
				foreach (VerificationTransItemView item in verificationTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

