using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class TerritoryHandler
	{
		#region TerritoryListView
		public static List<TerritoryListView> GetTerritoryListViewValidation(this List<TerritoryListView> territoryListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return territoryListViews;
			}
			var result = new List<TerritoryListView>();
			try
			{
				foreach (TerritoryListView item in territoryListViews)
				{
					result.Add(item.GetTerritoryListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static TerritoryListView GetTerritoryListViewValidation(this TerritoryListView territoryListView)
		{
			try
			{
				territoryListView.RowAuthorize = territoryListView.GetListRowAuthorize();
				return territoryListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this TerritoryListView territoryListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion TerritoryListView
		#region TerritoryItemView
		public static TerritoryItemView GetTerritoryItemViewValidation(this TerritoryItemView territoryItemView)
		{
			try
			{
				territoryItemView.RowAuthorize = territoryItemView.GetItemRowAuthorize();
				return territoryItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this TerritoryItemView territoryItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static Territory ToTerritory(this TerritoryItemView territoryItemView)
		{
			try
			{
				Territory territory = new Territory();
				territory.CompanyGUID = territoryItemView.CompanyGUID.StringToGuid();
				territory.CreatedBy = territoryItemView.CreatedBy;
				territory.CreatedDateTime = territoryItemView.CreatedDateTime.StringToSystemDateTime();
				territory.ModifiedBy = territoryItemView.ModifiedBy;
				territory.ModifiedDateTime = territoryItemView.ModifiedDateTime.StringToSystemDateTime();
				territory.Owner = territoryItemView.Owner;
				territory.OwnerBusinessUnitGUID = territoryItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				territory.TerritoryGUID = territoryItemView.TerritoryGUID.StringToGuid();
				territory.Description = territoryItemView.Description;
				territory.TerritoryId = territoryItemView.TerritoryId;
				
				territory.RowVersion = territoryItemView.RowVersion;
				return territory;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<Territory> ToTerritory(this IEnumerable<TerritoryItemView> territoryItemViews)
		{
			try
			{
				List<Territory> territorys = new List<Territory>();
				foreach (TerritoryItemView item in territoryItemViews)
				{
					territorys.Add(item.ToTerritory());
				}
				return territorys;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static TerritoryItemView ToTerritoryItemView(this Territory territory)
		{
			try
			{
				TerritoryItemView territoryItemView = new TerritoryItemView();
				territoryItemView.CompanyGUID = territory.CompanyGUID.GuidNullToString();
				territoryItemView.CreatedBy = territory.CreatedBy;
				territoryItemView.CreatedDateTime = territory.CreatedDateTime.DateTimeToString();
				territoryItemView.ModifiedBy = territory.ModifiedBy;
				territoryItemView.ModifiedDateTime = territory.ModifiedDateTime.DateTimeToString();
				territoryItemView.Owner = territory.Owner;
				territoryItemView.OwnerBusinessUnitGUID = territory.OwnerBusinessUnitGUID.GuidNullToString();
				territoryItemView.TerritoryGUID = territory.TerritoryGUID.GuidNullToString();
				territoryItemView.Description = territory.Description;
				territoryItemView.TerritoryId = territory.TerritoryId;
				
				territoryItemView.RowVersion = territory.RowVersion;
				return territoryItemView.GetTerritoryItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion TerritoryItemView
		#region ToDropDown
		public static SelectItem<TerritoryItemView> ToDropDownItem(this TerritoryItemView territoryView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<TerritoryItemView> selectItem = new SelectItem<TerritoryItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(territoryView.TerritoryId,territoryView.Description);
				selectItem.Value = territoryView.TerritoryGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: territoryView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<TerritoryItemView>> ToDropDownItem(this IEnumerable<TerritoryItemView> territoryItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<TerritoryItemView>> selectItems = new List<SelectItem<TerritoryItemView>>();
				foreach (TerritoryItemView item in territoryItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

