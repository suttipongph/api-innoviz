using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class BusinessCollateralStatusHandler
	{
		#region BusinessCollateralStatusListView
		public static List<BusinessCollateralStatusListView> GetBusinessCollateralStatusListViewValidation(this List<BusinessCollateralStatusListView> businessCollateralStatusListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return businessCollateralStatusListViews;
			}
			var result = new List<BusinessCollateralStatusListView>();
			try
			{
				foreach (BusinessCollateralStatusListView item in businessCollateralStatusListViews)
				{
					result.Add(item.GetBusinessCollateralStatusListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BusinessCollateralStatusListView GetBusinessCollateralStatusListViewValidation(this BusinessCollateralStatusListView businessCollateralStatusListView)
		{
			try
			{
				businessCollateralStatusListView.RowAuthorize = businessCollateralStatusListView.GetListRowAuthorize();
				return businessCollateralStatusListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this BusinessCollateralStatusListView businessCollateralStatusListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion BusinessCollateralStatusListView
		#region BusinessCollateralStatusItemView
		public static BusinessCollateralStatusItemView GetBusinessCollateralStatusItemViewValidation(this BusinessCollateralStatusItemView businessCollateralStatusItemView)
		{
			try
			{
				businessCollateralStatusItemView.RowAuthorize = businessCollateralStatusItemView.GetItemRowAuthorize();
				return businessCollateralStatusItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this BusinessCollateralStatusItemView businessCollateralStatusItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static BusinessCollateralStatus ToBusinessCollateralStatus(this BusinessCollateralStatusItemView businessCollateralStatusItemView)
		{
			try
			{
				BusinessCollateralStatus businessCollateralStatus = new BusinessCollateralStatus();
				businessCollateralStatus.CompanyGUID = businessCollateralStatusItemView.CompanyGUID.StringToGuid();
				businessCollateralStatus.CreatedBy = businessCollateralStatusItemView.CreatedBy;
				businessCollateralStatus.CreatedDateTime = businessCollateralStatusItemView.CreatedDateTime.StringToSystemDateTime();
				businessCollateralStatus.ModifiedBy = businessCollateralStatusItemView.ModifiedBy;
				businessCollateralStatus.ModifiedDateTime = businessCollateralStatusItemView.ModifiedDateTime.StringToSystemDateTime();
				businessCollateralStatus.Owner = businessCollateralStatusItemView.Owner;
				businessCollateralStatus.OwnerBusinessUnitGUID = businessCollateralStatusItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				businessCollateralStatus.BusinessCollateralStatusGUID = businessCollateralStatusItemView.BusinessCollateralStatusGUID.StringToGuid();
				businessCollateralStatus.BusinessCollateralStatusId = businessCollateralStatusItemView.BusinessCollateralStatusId;
				businessCollateralStatus.Description = businessCollateralStatusItemView.Description;
				
				businessCollateralStatus.RowVersion = businessCollateralStatusItemView.RowVersion;
				return businessCollateralStatus;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<BusinessCollateralStatus> ToBusinessCollateralStatus(this IEnumerable<BusinessCollateralStatusItemView> businessCollateralStatusItemViews)
		{
			try
			{
				List<BusinessCollateralStatus> businessCollateralStatuss = new List<BusinessCollateralStatus>();
				foreach (BusinessCollateralStatusItemView item in businessCollateralStatusItemViews)
				{
					businessCollateralStatuss.Add(item.ToBusinessCollateralStatus());
				}
				return businessCollateralStatuss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BusinessCollateralStatusItemView ToBusinessCollateralStatusItemView(this BusinessCollateralStatus businessCollateralStatus)
		{
			try
			{
				BusinessCollateralStatusItemView businessCollateralStatusItemView = new BusinessCollateralStatusItemView();
				businessCollateralStatusItemView.CompanyGUID = businessCollateralStatus.CompanyGUID.GuidNullToString();
				businessCollateralStatusItemView.CreatedBy = businessCollateralStatus.CreatedBy;
				businessCollateralStatusItemView.CreatedDateTime = businessCollateralStatus.CreatedDateTime.DateTimeToString();
				businessCollateralStatusItemView.ModifiedBy = businessCollateralStatus.ModifiedBy;
				businessCollateralStatusItemView.ModifiedDateTime = businessCollateralStatus.ModifiedDateTime.DateTimeToString();
				businessCollateralStatusItemView.Owner = businessCollateralStatus.Owner;
				businessCollateralStatusItemView.OwnerBusinessUnitGUID = businessCollateralStatus.OwnerBusinessUnitGUID.GuidNullToString();
				businessCollateralStatusItemView.BusinessCollateralStatusGUID = businessCollateralStatus.BusinessCollateralStatusGUID.GuidNullToString();
				businessCollateralStatusItemView.BusinessCollateralStatusId = businessCollateralStatus.BusinessCollateralStatusId;
				businessCollateralStatusItemView.Description = businessCollateralStatus.Description;
				
				businessCollateralStatusItemView.RowVersion = businessCollateralStatus.RowVersion;
				return businessCollateralStatusItemView.GetBusinessCollateralStatusItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion BusinessCollateralStatusItemView
		#region ToDropDown
		public static SelectItem<BusinessCollateralStatusItemView> ToDropDownItem(this BusinessCollateralStatusItemView businessCollateralStatusView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<BusinessCollateralStatusItemView> selectItem = new SelectItem<BusinessCollateralStatusItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(businessCollateralStatusView.BusinessCollateralStatusId, businessCollateralStatusView.Description);
				selectItem.Value = businessCollateralStatusView.BusinessCollateralStatusGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: businessCollateralStatusView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<BusinessCollateralStatusItemView>> ToDropDownItem(this IEnumerable<BusinessCollateralStatusItemView> businessCollateralStatusItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<BusinessCollateralStatusItemView>> selectItems = new List<SelectItem<BusinessCollateralStatusItemView>>();
				foreach (BusinessCollateralStatusItemView item in businessCollateralStatusItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

